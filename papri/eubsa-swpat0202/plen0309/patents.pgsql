--
-- PostgreSQL database dump
--

SET search_path = public, pg_catalog;

--
-- TOC entry 55 (OID 16976)
-- Name: plpgsql_call_handler (); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION plpgsql_call_handler () RETURNS language_handler
    AS '/usr/lib/postgresql/lib/plpgsql.so', 'plpgsql_call_handler'
    LANGUAGE c;


--
-- TOC entry 54 (OID 16977)
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: public; Owner: 
--

CREATE TRUSTED PROCEDURAL LANGUAGE plpgsql HANDLER plpgsql_call_handler;


SET search_path = public, pg_catalog;

--
-- TOC entry 2 (OID 23646)
-- Name: item; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE item (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    item text
);


--
-- TOC entry 3 (OID 23654)
-- Name: name; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE name (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    name text
);


--
-- TOC entry 4 (OID 23662)
-- Name: cec; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cec (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cec text
);


--
-- TOC entry 5 (OID 23671)
-- Name: am; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE am (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    am text
);


--
-- TOC entry 6 (OID 23679)
-- Name: auth; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE auth (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    auth text
);


--
-- TOC entry 7 (OID 23688)
-- Name: cm; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cm (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cm text
);


--
-- TOC entry 8 (OID 23696)
-- Name: juri; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE juri (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    juri text
);


--
-- TOC entry 9 (OID 23704)
-- Name: parl; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE parl (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    parl text
);


--
-- TOC entry 10 (OID 23712)
-- Name: acpt; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE acpt (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    acpt text
);


--
-- TOC entry 11 (OID 23720)
-- Name: ffii; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE ffii (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    ffii text
);


--
-- TOC entry 12 (OID 23728)
-- Name: just; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE just (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    just text
);


--
-- TOC entry 13 (OID 23751)
-- Name: itre; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE itre (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    itre text
);


--
-- TOC entry 14 (OID 23763)
-- Name: itre_just; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE itre_just (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    itre_just text
);


--
-- TOC entry 15 (OID 23783)
-- Name: parl_cec; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE parl_cec (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    parl_cec text
);


--
-- TOC entry 16 (OID 23814)
-- Name: cult; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cult (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cult text
);


--
-- TOC entry 17 (OID 23827)
-- Name: cult_just; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cult_just (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cult_just text
);


--
-- TOC entry 18 (OID 23867)
-- Name: juri_just; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE juri_just (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    juri_just text
);


--
-- TOC entry 19 (OID 23880)
-- Name: juri_cec; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE juri_cec (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    juri_cec text
);


--
-- TOC entry 20 (OID 24012)
-- Name: cult_cec; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cult_cec (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cult_cec text
);


--
-- TOC entry 21 (OID 24020)
-- Name: itre_cec; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE itre_cec (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    itre_cec text
);


--
-- TOC entry 22 (OID 24107)
-- Name: cult_item; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cult_item (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cult_item text
);


--
-- TOC entry 23 (OID 24115)
-- Name: itre_item; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE itre_item (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    itre_item text
);


--
-- TOC entry 24 (OID 24309)
-- Name: juri_item; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE juri_item (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    juri_item text
);


--
-- TOC entry 25 (OID 26321)
-- Name: cec_item; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE cec_item (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    cec_item text
);


--
-- TOC entry 26 (OID 26975)
-- Name: caps; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE caps (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    caps text
);


--
-- TOC entry 27 (OID 26984)
-- Name: comm; Type: TABLE; Schema: public; Owner: gibus
--

CREATE TABLE comm (
    id character varying(16) NOT NULL,
    lang character(2) NOT NULL,
    comm text
);


--
-- Data for TOC entry 56 (OID 23646)
-- Name: item; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY item (id, lang, item) FROM stdin;
rec	da	
rec1	da	Betragtning 1
rec2	da	Betragtning 2
rec3	da	Betragtning 3
rec4	da	Betragtning 4
rec5	da	Betragtning 5
rec5a	da	Betragtning 5 a (ny)
rec6	da	Betragtning 6
rec7	da	Betragtning 7
rec7a0	da	
rec7a	da	Betragtning 7 a (ny)
rec7b	da	Betragtning 7 b (ny)
rec8	da	Betragtning 8
rec9	da	Betragtning 9
rec10	da	Betragtning 10
rec11	da	Betragtning 11
rec12	da	Betragtning 12
rec13	da	Betragtning 13
rec13a	da	Betragtning 13 a (ny)
rec13b	da	Betragtning 13 b (ny)
rec13c	da	Betragtning 13 c (ny)
rec13d	da	Betragtning 13 d (ny)
rec14	da	Betragtning 14
rec15	da	Betragtning 15
rec16	da	Betragtning 16
rec17	da	Betragtning 17
rec18	da	Betragtning 18
rec18a	da	Betragtning 18 a (ny)
rec19	da	Betragtning 19
art1	da	Artikel 1
art2	da	Artikel 2
art2a	da	Artikel 2, litra a
art2b	da	Artikel 2, litra b
art2ba	da	Artikel 2, litra b a (nyt)
art2bb	da	Artikel 2, litra b b (nyt)
art3	da	Artikel 3
art3a	da	Artikel 3 a (ny)
art4	da	Artikel 4
art4_1	da	
art4_2	da	
art4_3	da	
art4_3a	da	
art4a	da	Artikel 4 a (ny)
art4b	da	Artikel 4 b (ny)
art5	da	Artikel 5
art5_1	da	Artikel 5, stk. 1
art5_1a	da	Artikel 5, stk. 1 a (nyt)
art5_1b	da	Artikel 5, stk. 1 b (nyt)
art5_1c	da	Artikel 5, stk. 1 c og 1 d (nye)
art5_1d	da	
art6	da	Artikel 6
art6a	da	Artikel 6 a (ny)
art7	da	Artikel 7
art8	da	Artikel 8
art8a	da	Artikel 8 a (ny)
art8b	da	Artikel 8, litra b
art8c	da	
art8ca	da	Artikel 8, litra c a (nyt)
art8cb	da	Artikel 8, litra c b (nyt)
art8cc	da	Artikel 8, litra c c (nyt)
art8cd	da	Artikel 8, litra c d (nyt)
art8ce	da	Artikel 8, litra c e (nyt)
art8cf	da	Artikel 8, litra c f (nyt)
art8cg	da	Artikel 8, litra c g (nyt)
art8_1a	da	Artikel 8, stk. 1 a (nyt)
art9	da	Artikel 9
art9_1	da	Artikel 9, stk. 1, f�rste afsnit
art9_2	da	
art10	da	Artikel 10
art11	da	Artikel 11
rec	de	
rec1	de	Erw�gung 1
rec2	de	Erw�gung 2
rec3	de	Erw�gung 3
rec4	de	Erw�gung 4
rec5	de	Erw�gung 5
rec5a	de	Erw�gung 5a (neu)
rec6	de	Erw�gung 6
rec7	de	Erw�gung 7
rec7a0	de	
rec7a	de	Erw�gung 7a (neu)
rec7b	de	Erw�gung 7b (neu)
rec8	de	Erw�gung 8
rec9	de	Erw�gung 9
rec10	de	Erw�gung 10
rec11	de	Erw�gung 11
rec12	de	Erw�gung 12
rec13	de	Erw�gung 13
rec13a	de	Erw�gung 13a (neu)
rec13b	de	Erw�gung 13b (neu)
rec13c	de	Erw�gung 13c (neu)
rec13d	de	Erw�gung 13d (neu)
rec14	de	Erw�gung 14
rec15	de	Erw�gung 15
rec16	de	Erw�gung 16
rec17	de	Erw�gung 17
rec18	de	Erw�gung 18
rec18a	de	Erw�gung 18a (neu)
rec19	de	Erw�gung 19
art1	de	Artikel 1
art2	de	Artikel 2
art2a	de	Artikel 2 Buchstabe a
art2b	de	Artikel 2 Buchstabe b
art2ba	de	Artikel 2 Buchstabe ba (neu)
art2bb	de	Artikel 2 Buchstabe bb (neu)
art3	de	Artikel 3
art3a	de	Artikel 3a (neu)
art4	de	Artikel 4
art4_1	de	
art4_2	de	
art4_3	de	
art4_3a	de	
art4a	de	Artikel 4a (neu)
art4b	de	Artikel 4b (neu)
art5	de	Artikel 5
art5_1	de	Artikel 5 Absatz 1
art5_1a	de	Artikel 5 Absatz 1a (neu)
art5_1b	de	Artikel 5 Absatz 1b (neu)
art5_1c	de	Artikel 5 Abs�tze 1c und 1 d (neu)
art5_1d	de	
art6	de	Artikel 6
art6a	de	Artikel 6a (neu)
art7	de	Artikel 7
art8	de	Artikel 8
art8a	de	Artikel 8a (neu)
art8b	de	Artikel 8 Buchstabe b
art8c	de	
art8ca	de	Artikel 8 Buchstabe ca (neu)
art8cb	de	Artikel 8 Buchstabe cb (neu)
art8cc	de	Artikel 8 Buchstabe cc (neu)
art8cd	de	Artikel 8 Buchstabe cd (neu)
art8ce	de	Artikel 8 Buchstabe ce (neu) ???????
art8cf	de	Artikel 8 Buchstabe cf (neu)
art8cg	de	Artikel 8 Buchstabe cg (neu)
art8_1a	de	Artikel 8 Absatz 1a (neu)
art9	de	Artikel 9
art9_1	de	
art9_2	de	
art10	de	Artikel 10
art11	de	Artikel 11
rec	en	
rec1	en	Recital 1
rec2	en	Recital 2
rec3	en	Recital 3
rec4	en	Recital 4
rec5	en	Recital 5
rec5a	en	Recital 5 a (new)
rec6	en	Recital 6
rec7	en	Recital 7
rec7a0	en	
rec7a	en	Recital 7a (new)
rec7b	en	Recital 7b (new)
rec8	en	Recital 8
rec9	en	Recital 9
rec10	en	Recital 10
rec11	en	Recital 11
rec12	en	Recital 12
rec13	en	Recital 13
rec13a	en	Recital 13a (new)
rec13b	en	Recital 13b (new)
rec13c	en	Recital 13c (new)
rec13d	en	Recital 13d (new)
rec14	en	Recital 14
rec15	en	Recital 15
rec16	en	Recital 16
rec17	en	Recital 17
rec18	en	Recital 18
rec18a	en	Recital 18 a (new)
rec19	en	Recital 19
art1	en	Article 1
art2	en	Article 2
art2a	en	Article 2, point (a)
art2b	en	Article 2, point (b)
art2ba	en	Article 2, point (ba) (new)
art2bb	en	Article 2, point (bb) (new)
art3	en	Article 3
art3a	en	Article 3a (new)
art4	en	Article 4
art4_1	en	
art4_2	en	
art4_3	en	
art4_3a	en	
art4a	en	Article 4a (new)
art4b	en	Article 4b (new)
art5	en	Article 5
art5_1	en	Article 5, paragraph 1
art5_1a	en	Article 5, paragraph 1 a (new)
art5_1b	en	Article 5, paragraph 1 b (new)
art5_1c	en	Article 5, paragraphs 1 c and 1 d (new)
art5_1d	en	
art6	en	Article 6
art6a	en	Article 6a (new)
art7	en	Article 7
art8	en	Article 8
art8a	en	Article 8a (new)
art8b	en	Article 8, point (b)
art8c	en	
art8ca	en	Article 8, point (ca) (new)
art8cb	en	Article 8, point (cb) (new)
art8cc	en	Article 8, point (cc) (new)
art8cd	en	Article 8, point (cd) (new)
art8ce	en	Article 8, point (c e) (new)
art8cf	en	Article 8, point (cf) (new)
art8cg	en	Article 8, point (cg) (new)
art8_1a	en	Article 8, paragraph 1a (new)
art9	en	Article 9
art9_1	en	Article 9, paragraph 1, subparagraph 1
art9_2	en	
art10	en	Article 10
art11	en	Article 11
rec	es	
rec1	es	Considerando 1
rec2	es	Considerando 2
rec3	es	Considerando 3
rec4	es	Considerando 4
rec5	es	Considerando 5
rec5a	es	Considerando 5 bis (nuevo)
rec6	es	Considerando 6
rec7	es	Considerando 7
rec7a0	es	
rec7a	es	Considerando 7 bis (nuevo)
rec7b	es	Considerando 7 ter (nuevo)
rec8	es	Considerando 8
rec9	es	Considerando 9
rec10	es	Considerando 10
rec11	es	Considerando 11
rec12	es	Considerando 12
rec13	es	Considerando 13
rec13a	es	Considerando 13 bis (nuevo)
rec13b	es	Considerando 13 ter (nuevo)
rec13c	es	Considerando 13 qu�ter (nuevo)
rec13d	es	Considerando 13 quinquies (nuevo)
rec14	es	Considerando 14
rec15	es	Considerando 15
rec16	es	Considerando 16
rec17	es	Considerando 17
rec18	es	Considerando 18
rec18a	es	Considerando 18 bis (nuevo)
rec19	es	Considerando 19
art1	es	
art2	es	
art2a	es	Art�culo 2, letra (a)
art2b	es	Art�culo 2, letra b)
art2ba	es	Art�culo 2, letra b bis) (nueva)
art2bb	es	Art�culo 2, letra b ter) (nueva)
art3	es	Art�culo 3
art3a	es	Art�culo 3 bis (nuevo)
art4	es	Art�culo 4
art4_1	es	
art4_2	es	
art4_3	es	
art4_3a	es	
art4a	es	Art�culo 4 bis (nuevo)
art4b	es	Art�culo 4 ter (nuevo)
art5	es	
art5_1	es	Art�culo 5, p�rrafo 1
art5_1a	es	Art�culo 5, apartado 1 bis (nuevo)
art5_1b	es	Art�culo 5, apartado 1 ter (nuevo)
art5_1c	es	Art�culo 5, apartados 1 qu�ter y 1 quinquies (nuevos)
art5_1d	es	
art6	es	Art�culo 6
art6a	es	Art�culo 6 bis (nuevo)
art7	es	Art�culo 7
art8	es	
art8a	es	Art�culo 8 bis (nuevo)
art8b	es	Art�culo 8, letra b)
art8c	es	
art8ca	es	Art�culo 8, letra c bis) (nueva)
art8cb	es	Art�culo 8, letra c ter) (nueva)
art8cc	es	Art�culo 8, letra c qu�ter) (nueva)
art8cd	es	Art�culo 8, letra c quinquies) (nueva)
art8ce	es	Art�culo 8, letra c sexies) (nueva)
art8cf	es	Art�culo 8, letra c septies) (nueva)
art8cg	es	Art�culo 8, letra c octies) (nueva)
art8_1a	es	Art�culo 8, p�rrafo 1 bis (nuevo)
art9	es	
art9_1	es	Art�culo 9, apartado 1, p�rrafo 1
art9_2	es	
art10	es	
art11	es	
rec	fi	
rec1	fi	Johdanto-osan 1 kappale
rec2	fi	Recital 2
rec3	fi	Recital 3
rec4	fi	Recital 4
rec5	fi	Johdanto-osan 5 kappale
rec5a	fi	Johdanto-osan 5 a kappale (uusi)
rec6	fi	Johdanto-osan 6 kappale
rec7	fi	Johdanto-osan 7 kappale
rec7a0	fi	
rec7a	fi	Johdanto-osan 7 a kappale (uusi)
rec7b	fi	Johdanto-osan 7 b kappale (uusi)
rec8	fi	Recital 8
rec9	fi	Recital 9
rec10	fi	Recital 10
rec11	fi	Johdanto-osan 11 kappale
rec12	fi	Johdanto-osan 12 kappale
rec13	fi	Johdanto-osan 13 kappale
rec13a	fi	Johdanto-osan 13 a kappale (uusi)
rec13b	fi	Johdanto-osan 13 b kappale (uusi)
rec13c	fi	Johdanto-osan 13 c kappale (uusi)
rec13d	fi	Johdanto-osan 13 d kappale (uusi)
rec14	fi	Johdanto-osan 14 kappale
rec15	fi	Recital 15
rec16	fi	Johdanto-osan 16 kappale
rec17	fi	Johdanto-osan 17 kappale
rec18	fi	Johdanto-osan 18 kappale
rec18a	fi	Johdanto-osan 18 a kappale (uusi)
rec19	fi	Recital 19
art1	fi	1 artikla
art2	fi	
art2a	fi	2 artiklan a alakohta
art2b	fi	2 artiklan b alakohta
art2ba	fi	2 artiklan b a alakohta (uusi)
art2bb	fi	2 artiklan b b alakohta (uusi)
art3	fi	3 artikla
art3a	fi	3 a artikla (uusi)
art4	fi	4 artikla
art4_1	fi	
art4_2	fi	
art4_3	fi	
art4_3a	fi	
art4a	fi	4 a artikla (uusi)
art4b	fi	4 b artikla (uusi)
art5	fi	5 artiklan 1 c ja 1 d kohta (uusi)
art5_1	fi	
art5_1a	fi	5 artiklan 1 a kohta (uusi)
art5_1b	fi	5 artiklan 1 b kohta (uusi)
art5_1c	fi	
art5_1d	fi	
art6	fi	6 artikla
art6a	fi	6 a artikla (uusi)
art7	fi	7 artikla
art8	fi	8 artikla
art8a	fi	8 a artikla (uusi)
art8b	fi	8 artiklan b alakohta
art8c	fi	
art8ca	fi	8 artiklan c a alakohta (uusi)
art8cb	fi	8 artiklan c b alakohta (uusi)
art8cc	fi	8 artiklan c c kohta (uusi)
art8cd	fi	8 artiklan c d kohta (uusi)
art8ce	fi	8 artiklan c e alakohta (uusi)
art8cf	fi	8 artiklan c f kohta (uusi)
art8cg	fi	8 artiklan c g alakohta (uusi)
art8_1a	fi	8 artiklan 1 a kohta (uusi)
art9	fi	9 artikla
art9_1	fi	9 artiklan 1 kohdan 1 alakohta
art9_2	fi	
art10	fi	10 artikla
art11	fi	11 artikla
rec	fr	
rec1	fr	Consid�rant 1
rec2	fr	Consid�rant 2
rec3	fr	Consid�rant 3
rec4	fr	Consid�rant 4
rec5	fr	Consid�rant 5
rec5a	fr	Consid�rant 5 bis (nouveau)
rec6	fr	Consid�rant 6
rec7	fr	Consid�rant 7
rec7a0	fr	
rec7a	fr	Consid�rant 7 bis ( nouveau)
rec7b	fr	Consid�rant 7 ter (nouveau)
rec8	fr	Consid�rant 8
rec9	fr	Consid�rant 9
rec10	fr	Consid�rant 10
rec11	fr	Consid�rant 11
rec12	fr	Consid�rant 12
rec13	fr	Consid�rant 13
rec13a	fr	Consid�rant 13 bis (nouveau)
rec13b	fr	Consid�rant 13 ter (nouveau)
rec13c	fr	Consid�rant 13 quater (nouveau)
rec13d	fr	Consid�rant 13 quinquies (nouveau)
rec14	fr	Consid�rant 14
rec15	fr	Consid�rant 15
rec16	fr	Consid�rant 16
rec17	fr	Consid�rant 17
rec18	fr	Consid�rant 18
rec18a	fr	Consid�rant 18 bis (nouveau)
rec19	fr	Consid�rant 19
art1	fr	
art2	fr	Article 2
art2a	fr	Article 2, point a)
art2b	fr	Article 2, point b)
art2ba	fr	Article 2, point b bis) (nouveau)
art2bb	fr	Article 2, point b ter) (nouveau)
art3	fr	Article 3
art3a	fr	Article 3 bis (nouveau)
art4	fr	Article 4
art4_1	fr	
art4_2	fr	
art4_3	fr	
art4_3a	fr	
art4a	fr	Article 4 bis (nouveau)
art4b	fr	Article 4 ter (nouveau)
art5	fr	Article 5
art5_1	fr	Article 5, alin�a 1
art5_1a	fr	Article 5, paragraphe 1 bis (nouveau)
art5_1b	fr	Article 5, paragraphe 1 ter (nouveau)
art5_1c	fr	Article 5, paragraphes 1 quater et 1 quinquies (nouveaux)
art5_1d	fr	
art6	fr	Article 6
art6a	fr	Article 6 bis (nouveau)
art7	fr	Article 7
art8	fr	Article 8
art8a	fr	Article 8 bis (nouveau)
art8b	fr	Article 8, point b)
art8c	fr	
art8ca	fr	Article 8, point c bis) (nouveau)
art8cb	fr	Article 8, point c ter) (nouveau)
art8cc	fr	Article 8, point c quater) (nouveau)
art8cd	fr	Article 8, point c quinquies) (nouveau)
art8ce	fr	Article 8, point c sexies) (nouveau)
art8cf	fr	Article 8, point c septies) (nouveau)
art8cg	fr	Article 8, point c octies) (nouveau)
art8_1a	fr	Article 8, alin�a 1 bis (nouveau)
art9	fr	Article 9
art9_1	fr	Article 9, paragraphe 1, alin�a 1
art9_2	fr	
art10	fr	Article 10
art11	fr	Article 11
rec	it	
rec1	it	Considerando 1
rec2	it	Considerando 2
rec3	it	Considerando 3
rec4	it	Considerando 4
rec5	it	Considerando Considerando 5 bis (nuovo)
rec5a	it	
rec6	it	Considerando 6
rec7	it	Considerando 7
rec7a0	it	
rec7a	it	Considerando 7 bis (nuovo)
rec7b	it	Considerando 7 ter (nuovo)
rec8	it	Considerando 8
rec9	it	Considerando 9
rec10	it	Considerando 10
rec11	it	Considerando 11
rec12	it	Considerando 12
rec13	it	Considerando 13
rec13a	it	Considerando 13 bis (nuovo)
rec13b	it	Considerando 13 ter (nuovo)
rec13c	it	Considerando 13 quater (nuovo)
rec13d	it	Considerando 13 quinquies (nuovo)
rec14	it	Considerando 14
rec15	it	Considerando 15
rec16	it	Considerando 16
rec17	it	Considerando 17
rec18	it	Considerando 18
rec18a	it	Considerando 18 bis (nuovo)
rec19	it	Considerando 19
art1	it	Articolo 1
art2	it	Articolo 2
art2a	it	Articolo 2, lettera a)
art2b	it	Articolo 2, lettera b)
art2ba	it	Articolo 2, lettera b bis) (nuova)
art2bb	it	Articolo 2, lettera b) ter (nuova)
art3	it	Articolo 3
art3a	it	Articolo 3 bis (nuovo)
art4	it	Articolo 4
art4_1	it	
art4_2	it	
art4_3	it	
art4_3a	it	
art4a	it	Articolo 4 bis (nuovo)
art4b	it	Articolo 4 ter (nuovo)
art5	it	Articolo 5
art5_1	it	Articolo 5, comma 1
art5_1a	it	Articolo 5, paragrafo 1 bis (nuovo)
art5_1b	it	Articolo 5, paragrafo 1 ter) (nuovo)
art5_1c	it	Articolo 5, paragrafi 1 quater e 1 quinquies (nuovi)
art5_1d	it	
art6	it	Articolo 6
art6a	it	Articolo 6 bis (nuovo)
art7	it	Articolo 7
art8	it	Articolo 8
art8a	it	Articolo 8 bis (nuovo)
art8b	it	Articolo 8, lettera b)
art8c	it	
art8ca	it	Articolo 8, lettera c bis) (nuova)
art8cb	it	Articolo 8, lettera c ter) (nuova)
art8cc	it	Articolo 8, lettera c quater) (nuova)
art8cd	it	Articolo 8, lettera c quinquies) (nuova)
art8ce	it	Articolo 8, lettera c) sexies (nuova)
art8cf	it	Articolo 8, lettera c septies) (nuova)
art8cg	it	Articolo 8, lettera c octies) (nuova)
art8_1a	it	Articolo 8, comma 1 bis (nuovo)
art9	it	Articolo 9
art9_1	it	Articolo 9, paragrafo 1, comma 1
art9_2	it	
art10	it	Articolo 10
art11	it	Articolo 11
rec	nl	
rec1	nl	Overweging 1
rec2	nl	Overweging 2
rec3	nl	Overweging 3
rec4	nl	Overweging 4
rec5	nl	Overweging 5
rec5a	nl	Overweging 5 bis (nieuw)
rec6	nl	Overweging 6
rec7	nl	Overweging 7
rec7a0	nl	
rec7a	nl	Overweging 7 bis (nieuw)
rec7b	nl	Overweging 7 ter (nieuw)
rec8	nl	Overweging 8
rec9	nl	Overweging 9
rec10	nl	Overweging 10
rec11	nl	Overweging 11
rec12	nl	Overweging 12
rec13	nl	Overweging 13
rec13a	nl	Overweging 13 bis (nieuw)
rec13b	nl	Overweging 13 ter (nieuw)
rec13c	nl	Overweging 13 quater (nieuw)
rec13d	nl	Overweging 13 quinquies (nieuw)
rec14	nl	Overweging 14
rec15	nl	Overweging 15
rec16	nl	Overweging 16
rec17	nl	Overweging 17
rec18	nl	Overweging 18
rec18a	nl	Overweging 18 bis (nieuw)
rec19	nl	Overweging 19
art1	nl	Artikel 1
art2	nl	Artikel 2
art2a	nl	Artikel 2, letter a)
art2b	nl	Artikel 2, letter b)
art2ba	nl	Artikel 2, letter b bis) (nieuw)
art2bb	nl	Artikel 2, letter b) ter (nieuw)
art3	nl	Artikel 3
art3a	nl	Artikel 3 bis (nieuw)
art4	nl	Artikel 4
art4_1	nl	
art4_2	nl	
art4_3	nl	
art4_3a	nl	
art4a	nl	Artikel 4 bis (nieuw)
art4b	nl	Artikel 4 ter (nieuw)
art5	nl	Artikel 5
art5_1	nl	Artikel 5, alinea 1
art5_1a	nl	Artikel 5, lid 1 bis (nieuw)
art5_1b	nl	
art5_1c	nl	Artikel 5, leden 1 quater en 1 quinquies (nieuw)
art5_1d	nl	
art6	nl	Artikel 6
art6a	nl	Artikel 6 bis (nieuw)
art7	nl	Artikel 7
art8	nl	Artikel 8
art8a	nl	Artikel 8 bis (nieuw)
art8b	nl	Artikel 8, letter b)
art8c	nl	
art8ca	nl	Artikel 8, letter c bis (nieuw)
art8cb	nl	Artikel 8, letter c ter) (nieuw)
art8cc	nl	Artikel 8, letter c quater) (nieuw)
art8cd	nl	Artikel 8, letter c quinquies) (nieuw)
art8ce	nl	Artikel 8, letter c sexies) (nieuw)
art8cf	nl	Artikel 8, letter c septies (nieuw)
art8cg	nl	
art8_1a	nl	Artikel 8, alinea 1 bis (nieuw)
art9	nl	Artikel 9
art9_1	nl	Artikel 9, lid 1, alinea 1
art9_2	nl	
art10	nl	Artikel 10
art11	nl	Artikel 11
rec	pt	
rec1	pt	Considerando 1
rec2	pt	Considerando 2
rec3	pt	Considerando 3
rec4	pt	Considerando 4
rec5	pt	Considerando 5
rec5a	pt	Considerando 5 bis (novo)
rec6	pt	Considerando 6
rec7	pt	Considerando 7
rec7a0	pt	
rec7a	pt	Considerando 7 bis (novo)
rec7b	pt	Considerando 7 ter (novo)
rec8	pt	Considerando 8
rec9	pt	Considerando 9
rec10	pt	Considerando 10
rec11	pt	Considerando 11
rec12	pt	Considerando 12
rec13	pt	Considerando 13
rec13a	pt	Considerando 13 bis (novo)
rec13b	pt	Considerando 13 ter (novo)
rec13c	pt	Considerando 13 quater (novo)
rec13d	pt	Considerando 13 quinquies
rec14	pt	Considerando 14
rec15	pt	Considerando 15
rec16	pt	Considerando 16
rec17	pt	Considerando 17
rec18	pt	Considerando 18
rec18a	pt	Considerando 18 bis (novo)
rec19	pt	Considerando 19
art1	pt	Artigo 1.�
art2	pt	Artigo 2.�
art2a	pt	Artigo 2, al�nea a)
art2b	pt	Artigo 2, al�nea b)
art2ba	pt	Artigo 2, al�nea b bis) (nova)
art2bb	pt	Artigo 2, al�nea b) ter (nova)
art3	pt	Artigo 3
art3a	pt	Artigo 3 bis (novo)
art4	pt	Artigo 4
art4_1	pt	
art4_2	pt	
art4_3	pt	
art4_3a	pt	
art4a	pt	Artigo 4 bis (novo)
art4b	pt	Artigo 4 ter (novo)
art5	pt	Artigo 5.�
art5_1	pt	Artigo 5, par�grafo 1
art5_1a	pt	Artigo 5, n� 1 bis (novo)
art5_1b	pt	Artigo 5, n� 1 ter (novo)
art5_1c	pt	Artigo 5, n�s 1 quater e 1 quinquies (novos)
art5_1d	pt	
art6	pt	Artigo 6
art6a	pt	Artigo 6 bis (novo)
art7	pt	Artigo 7
art8	pt	Artigo 8.�
art8a	pt	Artigo 8 bis (novo)
art8b	pt	Artigo 8, al�nea b)
art8c	pt	
art8ca	pt	Artigo 8, al�nea c bis) (nova)
art8cb	pt	Artigo 8, al�nea c ter) (nova)
art8cc	pt	Artigo 8, al�nea c quater) (nova)
art8cd	pt	Artigo 8, al�nea c quinquies) (novo)
art8ce	pt	Artigo 8, al�nea c sexies) (nova)
art8cf	pt	Artigo 8, al�nea c septies) (nova)
art8cg	pt	Artigo 8, al�nea c octies) (nova)
art8_1a	pt	Artigo 8, par�grafo 1 bis (novo)
art9	pt	Artigo 9.�
art9_1	pt	Artigo 9, n� 1, par�grafo 1
art9_2	pt	
art10	pt	Artigo 10.�
art11	pt	Artigo 11.�
rec	sv	
rec1	sv	Sk�l 1
rec2	sv	Sk�l 2
rec3	sv	Sk�l 3
rec4	sv	Sk�l 4
rec5	sv	Sk�l 5
rec5a	sv	Sk�l 5a (nytt)
rec6	sv	Sk�l 6
rec7	sv	Sk�l 7
rec7a0	sv	
rec7a	sv	Sk�l 7a (nytt)
rec7b	sv	Sk�l 7b (nytt)
rec8	sv	Sk�l 8
rec9	sv	Sk�l 9
rec10	sv	Sk�l 10
rec11	sv	Sk�l 11
rec12	sv	Sk�l 12
rec13	sv	Sk�l 13
rec13a	sv	Sk�l 13a (nytt)
rec13b	sv	Sk�l 13b (nytt)
rec13c	sv	Sk�l 13c (nytt)
rec13d	sv	Sk�l 13d (nytt)
rec14	sv	Sk�l 14
rec15	sv	Sk�l 15
rec16	sv	Sk�l 16
rec17	sv	Sk�l 17
rec18	sv	Sk�l 18
rec18a	sv	Sk�l 18a (nytt)
rec19	sv	Sk�l 19
art1	sv	Artikel 1
art2	sv	Artikel 2
art2a	sv	Artikel 2, led a
art2b	sv	Artikel 2, led b
art2ba	sv	Artikel 2, led ba (nytt)
art2bb	sv	Artikel 2, led bb (nytt)
art3	sv	Artikel 3
art3a	sv	Artikel 3a (ny)
art4	sv	Artikel 4
art4_1	sv	
art4_2	sv	
art4_3	sv	
art4_3a	sv	
art4a	sv	Artikel 4a (ny)
art4b	sv	Artikel 4b (ny)
art5	sv	Artikel 5
art5_1	sv	Artikel 5, punkt 1
art5_1a	sv	Artikel 5, punkt 1a (ny)
art5_1b	sv	Artikel 5, punkt 1b (nytt)
art5_1c	sv	Artikel 5, punkt 1c och 1d (nya)
art5_1d	sv	
art6	sv	Artikel 6
art6a	sv	Artikel 6a (ny)
art7	sv	Artikel 7
art8	sv	Artikel 8
art8a	sv	Artikel 8a (ny)
art8b	sv	Artikel 8, led b
art8c	sv	
art8ca	sv	Artikel 8, led ca (nytt)
art8cb	sv	Artikel 8, led cb (nytt)
art8cc	sv	Artikel 8, led cc (nytt)
art8cd	sv	Artikel 8, led cd (nytt)
art8ce	sv	Artikel 8, led ce (nytt)
art8cf	sv	Artikel 8, led cf (nytt)
art8cg	sv	Artikel 8, led cg (nytt)
art8_1a	sv	Artikel 8, stycke 1a (nytt)
art9	sv	Artikel 9
art9_1	sv	Artikel 9, punkt 1, stycke 1
art9_2	sv	
art10	sv	Artikel 10
art11	sv	Artikel 11
\.


--
-- Data for TOC entry 57 (OID 23654)
-- Name: name; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY name (id, lang, name) FROM stdin;
rec	da	
art1	da	Anvendelsesomr�de
art2	da	Definitioner
art3	da	Computer-implementerede opfindelser som teknologisk omr�de
art4	da	Betingelser for patenterbarhed
art4a	da	
art5	da	Patentkravs form
art5_1	da	
art5_1d	da	
art6	da	Forbindelse med direktiv 91/250/E�F
art6a	da	
art7	da	Overv�gning
art8	da	Rapport om direktivets virkninger
art8a	da	
art9	da	Gennemf�relse
art9_1	da	
art10	da	Ikrafttr�den
art11	da	Adressater
rec	de	
art1	de	Anwendungsbereich
art2	de	Begriffsbestimmungen
art3	de	Gebiet der Technik
art4	de	Voraussetzungen der Patentierbarkeit
art4a	de	
art5	de	Form des Patentanspruchs
art5_1	de	
art5_1d	de	
art6	de	Konkurrenz zur Richtlinie 91/250/EWG
art6a	de	
art7	de	Beobachtung
art8	de	Bericht �ber die Auswirkungen der Richtlinie
art8a	de	
art9	de	Umsetzung
art9_1	de	
art10	de	Inkrafttreten
art11	de	Adressaten
rec	en	
art1	en	Scope
art2	en	Definitions
art3	en	Computer-implemented inventions as a field of technology
art4	en	Conditions for patentability
art4a	en	
art5	en	Form of claims
art5_1	en	
art5_1d	en	
art6	en	Relationship with Directive 91/250 EC
art6a	en	
art7	en	Monitoring
art8	en	Report on the effects of the Directive
art8a	en	
art9	en	Implementation
art9_1	en	
art10	en	Entry into force
art11	en	Addressees
rec	es	
art1	es	
art2	es	
art3	es	
art4	es	
art4a	es	
art5	es	
art5_1	es	
art5_1d	es	
art6	es	
art6a	es	
art7	es	
art8	es	
art8a	es	
art9	es	
art9_1	es	
art10	es	
art11	es	
rec	fi	
art1	fi	M��ritelm�t
art2	fi	
art3	fi	Tietokoneella toteutetut keksinn�t osana tekniikanalaa
art4	fi	Patentoitavuuden edellytykset
art4a	fi	
art5	fi	Patenttivaatimuksen muoto
art5_1	fi	
art5_1d	fi	
art6	fi	Suhde direktiiviin 91/250/ETY
art6a	fi	
art7	fi	Seuranta
art8	fi	Kertomus direktiivin vaikutuksista
art8a	fi	
art9	fi	T�yt�nt��npano
art9_1	fi	
art10	fi	Voimaantulo
art11	fi	Osoitus
rec	fr	
art1	fr	
art2	fr	D�finitions
art3	fr	Domaine technique
art4	fr	Conditions de brevetabilit�
art4a	fr	
art5	fr	Forme des revendications
art5_1	fr	
art5_1d	fr	
art6	fr	Rapport avec la directive 91/250/CE
art6a	fr	
art7	fr	Suivi
art8	fr	Rapport sur les effets de la directive
art8a	fr	
art9	fr	Mise en oeuvre
art9_1	fr	
art10	fr	Entr�e en vigueur
art11	fr	Destinataires
rec	it	
art1	it	Campo d'applicazione
art2	it	Definizioni
art3	it	Appartenenza ad un settore della tecnologia
art4	it	Condizioni della brevettabilit�
art4a	it	
art5	it	Forma delle rivendicazioni
art5_1	it	
art5_1d	it	
art6	it	Relazione con la direttiva 91/250 CEE
art6a	it	
art7	it	Monitoraggio
art8	it	Relazione sugli effetti della direttiva
art8a	it	
art9	it	Attuazione
art9_1	it	
art10	it	Entrata in vigore
art11	it	Destinatari
rec	nl	
art1	nl	Werkingssfeer
art2	nl	Definities
art3	nl	In computers ge�mplementeerde uitvindingen als een gebied van de technologie
art4	nl	Voorwaarden voor octrooieerbaarheid
art4a	nl	
art5	nl	Vorm van de conclusies
art5_1	nl	
art5_1d	nl	
art6	nl	Verband met Richtlijn 91/250/EEG
art6a	nl	
art7	nl	Toezicht
art8	nl	Verslag over de gevolgen van de richtlijn
art8a	nl	
art9	nl	Omzetting
art9_1	nl	
art10	nl	Inwerkingtreding
art11	nl	Adressaten
rec	pt	
art1	pt	�mbito de aplica��o
art2	pt	Defini��es
art3	pt	Inventos que implicam programas de computador enquanto dom�nio da tecnologia
art4	pt	Condi��es de patenteabilidade
art4a	pt	
art5	pt	Forma das reivindica��es
art5_1	pt	
art5_1d	pt	
art6	pt	Rela��o com a Directiva 91/250/CEE
art6a	pt	
art7	pt	Acompanhamento
art8	pt	Relat�rio sobre os efeitos da directiva
art8a	pt	
art9	pt	Execu��o
art9_1	pt	
art10	pt	Entrada em vigor
art11	pt	Destinat�rios
rec	sv	
art1	sv	R�ckvidd
art2	sv	Definitioner
art3	sv	Datorrelaterade uppfinningar som teknikomr�de
art4	sv	Villkor f�r patenterbarhet
art4a	sv	
art5	sv	Typ av krav
art5_1	sv	
art5_1d	sv	
art6	sv	F�rh�llande till direktiv 91/250/EEG
art6a	sv	
art7	sv	Uppf�ljning
art8	sv	Rapport om direktivets verkan
art8a	sv	
art9	sv	Genomf�rande
art9_1	sv	
art10	sv	Ikrafttr�dande
art11	sv	Adressater
\.


--
-- Data for TOC entry 58 (OID 23662)
-- Name: cec; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cec (id, lang, cec) FROM stdin;
rec	da	
rec1	da	Gennemf�relsen af det indre marked indeb�rer, at hindringerne for den frie bev�gelighed og konkurrencefordrejningerne fjernes, og at der samtidig skabes et gunstigt klima for innovation og investeringer. I den forbindelse er beskyttelsen af opfindelser ved hj�lp af patenter en v�sentlig betingelse for et vellykket indre marked. Det er vigtigt med en effektiv og harmoniseret beskyttelse af computer-implementerede opfindelser i alle medlemsstater for at opretholde og fremme investeringerne p� dette omr�de.
rec2	da	Der er forskel p� beskyttelsen af computer-implementerede opfindelser i de forskellige medlemsstaters administrative praksis og retspraksis. Forskellene kan skabe handelshindringer og dermed forhindre det indre marked i at fungere efter hensigten.
rec3	da	De forskelle, der er opst�et, kan blive endnu st�rre, efterh�nden som medlemsstaterne indf�rer nye og forskellige administrative praksisser, og de nationale domstoles retspraksis i forbindelse med fortolkningen af den eksisterende lovgivning udvikler sig i forskellig retning.
rec4	da	Den stadig st�rre udbredelse og anvendelse af edb-programmer p� alle teknologiske omr�der og deres verdensomsp�ndende udbredelse via Internet er en kritisk faktor for teknologisk innovation. Det er derfor n�dvendigt at sikre et optimalt klima for udviklere og brugere af edb-programmer i F�llesskabet.
rec5	da	Medlemsstaternes fortolkning af lovbestemmelserne b�r derfor harmoniseres, og lovgivningen vedr�rende computer-implementerede opfindelsers patenterbarhed b�r g�res mere gennemsigtig. Den retssikkerhed, der herved opn�s, skal g�re det muligt for virksomhederne at f� det fulde udbytte af patenter p� computer-implementerede opfindelser og skal udg�re et incitament til investering og innovation.
rec5a	da	
rec6	da	F�llesskabet og dets medlemsstater er bundet af aftalen om handelsrelaterede intellektuelle ejendomsrettigheder (TRIPS), der blev godkendt ved R�dets afg�relse 94/800/EF af 22. december 1994 om indg�else p� Det Europ�iske F�llesskabs vegne af de aftaler, der er resultatet af de multilaterale forhandlinger i Uruguay-rundens regi (1986-1994), for s� vidt ang�r de omr�der, der h�rer under F�llesskabets kompetence [42]. I henhold til artikel 27, stk. 1, i TRIPS-aftalen skal alle opfindelser p� alle teknologiske omr�der, b�de produkter og processer, kunne patenteres, forudsat at de er nye, har opfindelsesh�jde og kan anvendes industrielt. I henhold til TRIPS-aftalen skal det desuden v�re muligt at opn� patentrettigheder og anvende disse uden forskelsbehandling p� grund af teknologisk omr�de. Disse principper b�r f�lgelig finde anvendelse p� computer-implementerede opfindelser. [42] EFT L 336 af 23.12.1994, s. 1.
rec7	da	I henhold til konventionen om meddelelse af europ�iske patenter, der blev undertegnet i M�nchen den 5. oktober 1973, og medlemsstaternes patentlove er edb-programmer s�vel som opdagelser, videnskabelige teorier, matematiske metoder, �stetiske frembringelser, planer, regler og metoder for intellektuel virksomhed, for spil eller for erhvervsvirksomhed (forretningskoncepter) og freml�ggelse af information udtrykkeligt ikke betragtet som opfindelser og kan derfor ikke patenteres. Disse undtagelser g�lder dog kun og er kun berettigede i det omfang, hvor en patentans�gning eller et patent vedr�rer disse genstande eller aktiviteter som s�dan, fordi de som s�dan ikke henh�rer under et teknologisk omr�de.
rec7a0	da	
rec7a	da	
rec7b	da	
rec8	da	Patentbeskyttelse giver innovatorer mulighed for at drage fordel af deres kreativitet. Patentrettigheder beskytter innovation i samfundets interesse som helhed; de m� ikke anvendes konkurrencebegr�nsende.
rec9	da	I henhold til R�dets direktiv 91/250/EF af 14. maj 1991 om retlig beskyttelse af edb-programmer [43] beskyttes enhver form, hvori et originalt edb-program udtrykkes, ophavsretligt som et litter�rt v�rk. Men de id�er og principper, der ligger til grund for de enkelte elementer i et edb-program, beskyttes ikke ophavsretligt. [43] EFT L 122 af 17.5.1991, s. 42. Direktivet er �ndret ved direktiv 93/98/E�F (EFT L 290 af 24.11.1993, s. 9).
rec10	da	For at en opfindelse kan betragtes som patenterbar, b�r den v�re af teknisk karakter, hvilket betyder, at den henh�rer under et teknologisk omr�de.
rec11	da	Selv om computer-implementerede opfindelser betragtes som henh�rende under et teknologisk omr�de, b�r de for at have opfindelsesh�jde ligesom alle andre opfindelser yde et bidrag til det aktuelle tekniske niveau.
rec12	da	Hvis en opfindelse derfor ikke yder et bidrag til det aktuelle tekniske niveau, hvilket er tilf�ldet, hvis det specifikke bidrag ikke er af teknisk karakter, har opfindelsen ikke opfindelsesh�jde og er derfor ikke patenterbar.
rec13	da	En specificeret procedure eller sekvens af handlinger, som udf�res ved hj�lp af en maskine som f.eks. en computer, kan yde et bidrag til det aktuelle tekniske niveau og dermed udg�re en patenterbar opfindelse. Men en algoritme, der er defineret uafh�ngigt af et fysisk milj�, er grundl�ggende af ikke-teknisk karakter og kan derfor ikke udg�re en patenterbar opfindelse.
rec13a	da	
rec13b	da	
rec13c	da	
rec13d	da	
rec14	da	Retlig beskyttelse af computer-implementerede opfindelser b�r ikke kr�ve indf�relse af en s�rlig lovgivning til erstatning af medlemsstaternes patentlovgivning. Det prim�re grundlag for retlig beskyttelse af computer-implementerede opfindelser b�r fortsat v�re medlemsstaternes patentlovgivning, som b�r tilpasses og suppleres p� en r�kke konkrete punkter som anf�rt i dette direktiv.
rec15	da	Direktivet b�r begr�nses til fastl�ggelsen af visse principper, s�ledes som de finder anvendelse p� patenterbarheden af s�danne opfindelser, idet de navnlig skal sikre, at opfindelser, der henh�rer under et teknologisk omr�de og yder et teknisk bidrag, kan opn� beskyttelse, og omvendt, at opfindelser, der ikke yder et teknisk bidrag, ikke kan.
rec16	da	EU's erhvervslivs konkurrenceevne i forhold til EU's vigtigste handelspartnere ville blive forbedret, hvis de nuv�rende forskelle i den retlige beskyttelse af computer-implementerede opfindelser blev fjernet og retstilstanden var gennemsigtig.
rec17	da	Dette direktiv ber�rer ikke anvendelsen af konkurrencereglerne, herunder is�r traktatens artikel 81 og 82.
rec18	da	Handlinger, der er tilladt i henhold til direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r bestemmelserne om dekompilering og interoperabilitet, og bestemmelserne om halvlederes topografi og varem�rker ber�res ikke af den beskyttelse, der ved hj�lp af patenter gives opfindelser, som er omfattet af dette direktiv.
rec18a	da	
rec19	da	M�lene for dette direktiv, nemlig at harmonisere de nationale bestemmelser om computer-implementerede opfindelser, kan ikke i tilstr�kkelig grad opfyldes af medlemsstaterne og kan derfor p� grund af direktivets omfang eller virkninger bedre gennemf�res p� f�llesskabsplan; F�llesskabet kan derfor tr�ffe foranstaltninger i overensstemmelse med subsidiaritetsprincippet, jf. traktatens artikel 5. I overensstemmelse med proportionalitetsprincippet, jf. n�vnte artikel, g�r direktivet ikke ud over, hvad der er n�dvendigt for at n� disse m�l - UDSTEDT F�LGENDE DIREKTIV
art1	da	I dette direktiv fastl�gges bestemmelser om computer-implementerede opfindelsers patenterbarhed.
art2	da	Med henblik p� anvendelsen af dette direktiv g�lder f�lgende definitioner
art2a	da	
art2b	da	
art2ba	da	
art2bb	da	
art3	da	Medlemsstaterne sikrer, at en computer-implementeret opfindelse betragtes som henh�rende under et teknologisk omr�de.
art3a	da	
art4	da	1. Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny og har opfindelsesh�jde. 2. Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag. 3. Det tekniske bidrag vurderes som forskellen mellem patentkravets genstand som helhed, hvoraf nogle dele kan omfatte b�de tekniske og ikke-tekniske karakteristika, og det aktuelle tekniske niveau.
art4_1	da	
art4_2	da	
art4_3	da	
art4_3a	da	
art4a	da	
art4b	da	
art5	da	Medlemsstaterne sikrer, at der kan ans�ges om patent p� en computer-implementeret opfindelse som et produkt, dvs. som en programmeret computer, et programmeret computernet eller en anden programmeret maskine, eller som en proces, der udf�res af computeren, computernettet eller maskinen ved k�rsel af software.
art5_1	da	
art5_1a	da	
art5_1b	da	
art5_1c	da	
art5_1d	da	
art6	da	Handlinger, der er tilladt i henhold til direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r bestemmelserne om dekompilering og interoperabilitet, og bestemmelserne om halvlederes topografi og varem�rker ber�res ikke af den beskyttelse, der ved hj�lp af patenter gives opfindelser, som er omfattet af dette direktiv.
art6a	da	
art7	da	Kommissionen overv�ger computer-implementerede opfindelsers indvirkning p� innovation og konkurrence, b�de i Europa og internationalt, og p� EU's erhvervsliv, herunder elektronisk handel.
art8	da	Kommissionen afl�gger rapport til Europa-Parlamentet og R�det senest den [DATO (tre �r efter den i artikel 9, stk. 1, anf�rte dato)] om (a) virkningerne af patenter p� computer-implementerede opfindelser p� de i artikel 7 omhandlede faktorer (b) hvorvidt reglerne for fastl�ggelse af kriterierne for patenterbarhed, herunder is�r nyhedskarakter, opfindelsesh�jde og patentkravets genstand, er fyldestg�rende, og (c) hvorvidt der har v�ret vanskeligheder med medlemsstater, hvor kravene om nyhedskarakter og opfindelsesh�jde ikke unders�ges, inden der udstedes patenter, og i bekr�ftende fald, om der b�r tr�ffes foranstaltninger til l�sning af disse vanskeligheder.
art8a	da	
art8b	da	
art8c	da	
art8ca	da	
art8cb	da	
art8cc	da	
art8cd	da	
art8ce	da	
art8cf	da	
art8cg	da	
art8_1a	da	
art9	da	1. Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at efterkomme dette direktiv senest den [DATO (sidste dag i en m�ned)]. De underretter straks Kommissionen herom. Disse love og bestemmelser skal ved vedtagelsen indeholde en henvisning til dette direktiv eller skal ved offentligg�relsen ledsages af en s�dan henvisning. De n�rmere regler for henvisningen fasts�ttes af medlemsstaterne. 2. Medlemsstaterne meddeler Kommissionen de nationale retsforskrifter, som de udsteder p� det omr�de, der er omfattet af dette direktiv.
art9_1	da	
art9_2	da	
art10	da	Dette direktiv tr�der i kraft p� tyvendedagen efter offentligg�relsen i De Europ�iske F�llesskabers Tidende.
art11	da	Dette direktiv er rettet til medlemsstaterne.
rec	de	
rec1	de	Damit der Binnenmarkt verwirklicht wird, m�ssen Beschr�nkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen beg�nstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium f�r den Erfolg des Binnenmarkts. Es ist unerl�sslich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam und einheitlich gesch�tzt sind, wenn Investitionen auf diesem Gebiet gesichert und gef�rdert werden sollen.
rec2	de	Die Patentpraxis und die Rechtsprechung in den einzelnen Mitgliedstaaten hat zu Unterschieden beim Schutz computerimplementierter Erfindungen gef�hrt. Solche Unterschiede k�nnten den Handel st�ren und somit verhindern, dass der Binnenmarkt reibungslos funktioniert.
rec3	de	Die Ursachen f�r die Unterschiede liegen darin begr�ndet, dass die Mitgliedstaaten neue, voneinander abweichende Verwaltungspraktiken eingef�hrt oder die nationalen Gerichte die geltenden Rechtsvorschriften unterschiedlich ausgelegt haben; diese Unterschiede k�nnten mit der Zeit noch gr��er werden.
rec4	de	Die zunehmende Verbreitung und Nutzung von Computerprogrammen auf allen Gebieten der Technik und die weltumspannenden Verbreitungswege durch das Internet sind ein kritischer Faktor f�r die technologische Innovation. Deshalb sollte sichergestellt sein, dass die Entwickler und Nutzer von Computerprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden.
rec5	de	Aus diesen Gr�nden sollten die Rechtsvorschriften, so wie sie von den Gerichten in den Mitgliedstaaten ausgelegten werden, vereinheitlicht und die Vorschriften �ber die Patentierbarkeit computerimplementierter Erfindungen transparent gemacht werden. Die dadurch gew�hrte Rechtssicherheit sollte dazu f�hren, dass Unternehmen den gr��tm�glichen Nutzen aus Patenten f�r computerimplementierte Erfindungen ziehen, und sie sollte Anreize f�r Investitionen und Innovationen schaffen.
rec5a	de	
rec6	de	Die Gemeinschaft und ihre Mitgliedstaaten sind auf das �bereinkommen �ber handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-�bereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 �ber den Abschluss der �bereink�nfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europ�ischen Gemeinschaft in Bezug auf die in ihre Zust�ndigkeiten fallenden Bereiche [42]. Nach Artikel 27 Absatz 1 des TRIPS-�bereinkommens sollen Patente f�r Erfindungen auf allen Gebieten der Technik erh�ltlich sein, sowohl f�r Erzeugnisse als auch f�r Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen T�tigkeit und sind gewerblich anwendbar. Gem�� dem TRIPS-�bereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erh�ltlich sein und Patentrechte ausge�bt werden k�nnen. Diese Grunds�tze sollten demgem�� auch f�r computerimplementierte Erfindungen gelten. [42] ABl. L 336 vom 23.12.1994, S. 1.
rec7	de	Nach dem �bereinkommen �ber die Erteilung europ�ischer Patente (Europ�isches Patent�bereinkommen) vom 5. Oktober 1973 (EP�) und den Patentgesetzen der Mitgliedstaaten gelten Programme f�r Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, �sthetische Formsch�pfungen, Pl�ne, Regeln und Verfahren f�r gedankliche T�tigkeiten, f�r Spiele oder f�r gesch�ftliche T�tigkeiten sowie die Wiedergabe von Informationen ausdr�cklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt jedoch nur, und hat auch ihre Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenst�nde oder T�tigkeiten als solche bezieht, da die besagten Gegenst�nde und T�tigkeiten als solche keinem Gebiet der Technik zugeh�ren.
rec7a0	de	
rec7a	de	
rec7b	de	
rec8	de	Der Patentschutz versetzt die Innovatoren in die Lage, Nutzen aus ihrer Kreativit�t zu ziehen. Patentrechte sch�tzen zwar Innovationen im Interesse der Gesellschaft allgemein; sie sollten aber nicht in wettbewerbswidriger Weise genutzt werden.
rec9	de	Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 �ber den Rechtsschutz von Computerprogrammen [43] sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht gesch�tzt. Die Ideen und Grunds�tze, die einem Element eines Computerprogramms zugrunde liegen, sind dagegen nicht durch das Urheberrecht gesch�tzt. [43] ABl. L 122 vom 17.5.1991, S. 42 - ge�ndert durch Richtlinie 93/98/EWG (ABl. L 290 vom 24.11.1993, S. 9).
rec10	de	Damit eine Erfindung als patentierbar gilt, sollte sie technischen Charakter haben und somit einem Gebiet der Technik zuzuordnen sein.
rec11	de	Zwar werden computerimplementierte Erfindungen einem Gebiet der Technik zugerechnet, aber um das Kriterium der erfinderischen T�tigkeit zu erf�llen, sollten sie wie alle Erfindungen einen technischen Beitrag zum Stand der Technik leisten.
rec12	de	Folglich erf�llt eine Erfindung, die keinen technischen Beitrag zum Stand der Technik leistet, z. B. weil dem besonderen Beitrag die Technizit�t fehlt, nicht das Kriterium der erfinderischen T�tigkeit und ist somit nicht patentierbar.
rec13	de	Wenn eine festgelegte Prozedur oder Handlungsfolge in einer Vorrichtung, z. B. einem Computer, abl�uft, kann sie einen technischen Beitrag zum Stand der Technik leisten und somit eine patentierbare Erfindung darstellen. Dagegen besitzt ein Algorithmus, der ohne Bezug zu einer physischen Umgebung definiert ist, keinen technischen Charakter; er stellt somit keine patentierbare Erfindung dar.
rec13a	de	
rec13b	de	
rec13c	de	
rec13d	de	
rec14	de	Um computerimplementierte Erfindungen rechtlich zu sch�tzen, sollten keine getrennten Rechtsvorschriften erforderlich sein, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sollten auch weiterhin die Hauptgrundlage f�r den Rechtschutz computerimplementierter Erfindungen liefern, und lediglich in bestimmten Punkten, die in dieser Richtlinie dargelegt sind, angepasst oder erg�nzt werden.
rec15	de	Diese Richtlinie sollte sich auf die Festlegung bestimmter Patentierbarkeitsgrunds�tze beschr�nken; im Wesentlichen sollen diese Grunds�tze einerseits die Schutzf�higkeit von Erfindungen sicherstellen, die einem Gebiet der Technik zugeh�ren und einen technischen Beitrag zum Stand der Technik leisten, andererseits Erfindungen vom Schutz ausschlie�en, die keinen technischen Beitrag zum Stand der Technik leisten.
rec16	de	Die Wettbewerbsposition der europ�ischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern w�rde sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausger�umt w�rden und die Rechtslage transparenter w�re.
rec17	de	Diese Richtlinie ber�hrt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.
rec18	de	Urheberrechtlich zul�ssige Handlungen gem�� der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere deren Vorschriften �ber die Dekompilierung und die Interoperabilit�t, oder die Vorschriften �ber Marken oder Halbleitertopografien sollen unber�hrt bleiben von dem Patentschutz f�r Erfindungen aufgrund diese Richtlinie.
rec18a	de	
rec19	de	Gem�� Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiarit�tsprinzip t�tig werden, da die Ziele der vorgeschlagenen Ma�nahme, also die Harmonisierung der nationalen Vorschriften f�r computerimplementierte Erfindungen, ,auf Ebene der Mitgliedstaaten nicht ausreichend erreicht werden k�nnen und daher wegen ihres Umfangs oder ihrer Wirkung besser auf Gemeinschaftsebene erreicht werden k�nnen". Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verh�ltnism��igkeit, da sie nicht �ber das f�r die Erreichung der Ziele erforderliche Ma� hinausgeht - HABEN FOLGENDE RICHTLINIE ERLASSEN
art1	de	Diese Richtlinie legt Vorschriften f�r die Patentierbarkeit computerimplementierter Erfindungen fest.
art2	de	F�r die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen
art2a	de	
art2b	de	
art2ba	de	
art2bb	de	
art3	de	Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugeh�rig gilt.
art3a	de	
art4	de	1. Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen T�tigkeit beruht. 2. Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet. 3. Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmalen umfassen kann, vom Stand der Technik abhebt.
art4_1	de	
art4_2	de	
art4_3	de	
art4_3a	de	
art4a	de	
art4b	de	
art5	de	Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf�hrung von Software verwirklicht wird.
art5_1	de	
art5_1a	de	
art5_1b	de	
art5_1c	de	
art5_1d	de	
art6	de	Zul�ssige Handlungen im Sinne der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften �ber die Dekompilierung und die Interoperabilit�t, oder im Sinne der Vorschriften �ber Marken oder Halbleitertopografien bleiben vom Patentschutz f�r Erfindungen aufgrund dieser Richtlinie unber�hrt.
art6a	de	
art7	de	Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen und den elektronischen Gesch�ftsverkehr auswirken.
art8	de	Die Kommission legt dem Europ�ischen Parlament und dem Rat sp�testens am [DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor �ber
art8a	de	
art8b	de	
art8c	de	
art8ca	de	
art8cb	de	
art8cc	de	
art8cd	de	
art8ce	de	
art8cf	de	
art8cg	de	
art8_1a	de	
art9	de	1. Die Mitgliedstaaten erlassen die erforderlichen Rechts- und Verwaltungsvorschriften, um dieser Richtlinie sp�testens am [DATUM (letzter Tag des betreffenden Monats)] nachzukommen. Sie setzen die Kommission unverz�glich davon in Kenntnis. Bei Erlass dieser Vorschriften nehmen die Mitgliedstaaten in den Vorschriften selbst oder durch einen Hinweis bei der amtlichen Ver�ffentlichung auf diese Richtlinie Bezug. Die Mitgliedstaaten regeln die Einzelheiten der Bezugnahme. 2. Die Mitgliedstaaten �bermitteln der Kommission den Wortlaut der innerstaatlichen Rechtsvorschriften, die sie im Geltungsbereich dieser Richtlinie erlassen.
art9_1	de	
art9_2	de	
art10	de	Diese Richtlinie tritt am zwanzigsten Tag nach ihrer Ver�ffentlichung im Amtsblatt der Europ�ischen Gemeinschaften in Kraft.
art11	de	Diese Richtlinie ist an die Mitgliedstaaten gerichtet.
rec	en	
rec1	en	The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.
rec2	en	Differences exist in the protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market.
rec3	en	Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently.
rec4	en	The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.
rec5	en	Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.
rec5a	en	
rec6	en	The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) [42]. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions. [42] OJ L 336, 23.12.1994, p. 1
rec7	en	Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.
rec7a0	en	
rec7a	en	
rec7b	en	
rec8	en	Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.
rec9	en	In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs [43], the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright. [43] OJ L 122 , 17.5.1991 p. 42- Directive amended by Directive 93/98/EEC (OJ L 290, 24.11.1993, p. 9).
rec10	en	In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology.
rec11	en	Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.
rec12	en	Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.
rec13	en	A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.
rec13a	en	
rec13b	en	
rec13c	en	
rec13d	en	
rec14	en	The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.
rec15	en	This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.
rec16	en	The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.
rec17	en	This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.
rec18	en	Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.
rec18a	en	
rec19	en	Since the objectives of the proposed action, namely to harmonise national rules on computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives. HAVE ADOPTED THIS DIRECTIVE
art1	en	This Directive lays down rules for the patentability of computer-implemented inventions.
art2	en	For the purposes of this Directive the following definitions shall apply
art2a	en	
art2b	en	
art2ba	en	
art2bb	en	
art3	en	Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.
art3a	en	
art4	en	1. Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step. 2. Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution. 3. The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.
art4_1	en	
art4_2	en	
art4_3	en	
art4_3a	en	
art4a	en	
art4b	en	
art5	en	Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.
art5_1	en	
art5_1a	en	
art5_1b	en	
art5_1c	en	
art5_1d	en	
art6	en	Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.
art6a	en	
art7	en	The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.
art8	en	The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on (a) the impact of patents for computer-implemented inventions on the factors referred to in Article 7; (b) whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and (c) whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.
art8a	en	
art8b	en	
art8c	en	
art8ca	en	
art8cb	en	
art8cc	en	
art8cd	en	
art8ce	en	
art8cf	en	
art8cg	en	
art8_1a	en	
art9	en	1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof. When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. 2. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive.
art9_1	en	
art9_2	en	
art10	en	This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
art11	en	This Directive is addressed to the Member States.
rec	es	
rec1	es	La realizaci�n del mercado interior implica la eliminaci�n de las restricciones a la libre circulaci�n y de las distorsiones de la competencia, as� como la creaci�n de un entorno que sea favorable a la innovaci�n y a las inversiones. En este contexto, la protecci�n de las invenciones mediante patentes constituye un elemento esencial para el �xito del mercado interior. Una protecci�n efectiva y armonizada de las invenciones implementadas en ordenador en todos los Estados miembros es esencial para mantener y fomentar las inversiones en este �mbito.
rec2	es	Existen diferencias en la protecci�n de las invenciones implementadas en ordenador que otorgan las pr�cticas administrativas y la jurisprudencia de los Estados miembros. Estas diferencias podr�an crear obst�culos al comercio e impedir as� el correcto funcionamiento del mercado interior.
rec3	es	Estas disparidades se han desarrollado y podr�an incrementarse a medida que los Estados miembros adopten nuevas leyes y pr�cticas administrativas diferentes y que sus interpretaciones jurisprudenciales nacionales se desarrollen de manera diversa.
rec4	es	El incremento constante de la difusi�n y utilizaci�n de programas de ordenador en todos los �mbitos de la tecnolog�a y de su difusi�n mundial a trav�s de Internet constituye un factor crucial de la innovaci�n tecnol�gica. As� pues, es necesario garantizar la existencia de un entorno �ptimo para los creadores y usuarios de programas inform�ticos en la Comunidad.
rec5	es	Por consiguiente, las normas jur�dicas, seg�n se interpretan en los �rganos jurisdiccionales de los Estados miembros, deber�an armonizarse y la legislaci�n relativa a la patentabilidad de las invenciones implementadas en ordenador deber�a hacerse m�s transparente. La seguridad jur�dica resultante permitir� a las empresas obtener el m�ximo beneficio de las patentes para las invenciones implementadas en ordenador e impulsar� la inversi�n y la innovaci�n.
rec5a	es	
rec6	es	La Comunidad y sus Estados miembros son signatarios del Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual relacionados con el Comercio (ADPIC), aprobado mediante la Decisi�n 94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la celebraci�n en nombre de la Comunidad Europea, por lo que respecta a los temas de su competencia, de los acuerdos resultantes de las negociaciones multilaterales de la Ronda Uruguay (1986-1994) [42]. El apartado 1 del art�culo 27 del acuerdo ADPIC establece que las patentes podr�n obtenerse por todas las invenciones, sean de productos o de procedimientos, en todos los campos de la tecnolog�a, siempre que sean nuevas, entra�en una actividad inventiva y sean susceptibles de aplicaci�n industrial. Adem�s, seg�n este mismo Acuerdo, las patentes se podr�n obtener y los derechos de patente se podr�n gozar sin discriminaci�n por el campo de la tecnolog�a. En consecuencia, estos principios deben aplicarse a las invenciones implementadas en ordenador. [42] DO L 336 de 23.12.1994, p. 1.
rec7	es	Con arreglo al Convenio sobre la concesi�n de patentes europeas, firmado en Munich el 5 de octubre de 1973, y las legislaciones sobre patentes de los Estados miembros, no se consideran invenciones, y quedan por tanto excluidos de la patentabilidad, los programas de ordenadores, as� como los descubrimientos, las teor�as cient�ficas, los m�todos matem�ticos, las creaciones est�ticas, los planes, principios y m�todos para el ejercicio de actividades intelectuales, para juegos o para actividades econ�micas, y las formas de presentar informaciones. No obstante, esta excepci�n se aplica y se justifica �nicamente en la medida en que la solicitud de patente o la patente se refiera a uno de esos elementos o actividades considerados como tales, porque dichos elementos y actividades como tales no pertenecen al campo de la tecnolog�a.
rec7a0	es	
rec7a	es	
rec7b	es	
rec8	es	La protecci�n que otorgan las patentes permite a los innovadores beneficiarse de su creatividad. Habida cuenta de que los derechos de patente protegen la innovaci�n en inter�s de toda la sociedad, no deber�an utilizarse de forma anticompetitiva.
rec9	es	De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo de 1991, sobre la protecci�n jur�dica de los programas de ordenador [43], cualquier forma de expresi�n de un programa de ordenador original estar� protegido por los derechos de autor como obra literaria. No obstante, las ideas y principios en los que se basa cualquiera de los elementos de un programa de ordenador no est�n protegidos por los derechos de autor. [43] DO L 122 de 17.5.1991, p. 42. Directiva modificada en �ltimo lugar por la Directiva 93/98/CEE (DO L 290 de 24.11.1993, p. 9).
rec10	es	Para que una invenci�n se considere patentable, deber� tener car�cter t�cnico y pertenecer, por tanto, a un campo de la tecnolog�a.
rec11	es	Aunque se considera que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a, para que entra�en una actividad inventiva, como las invenciones en general, deber�n aportar una contribuci�n t�cnica al estado de la t�cnica.
rec12	es	En consecuencia, si una invenci�n no aporta una contribuci�n t�cnica al estado de la t�cnica, como ser�a el caso, por ejemplo, si su contribuci�n espec�fica careciera de car�cter t�cnico, la invenci�n no implicar� actividad inventiva y no podr� ser patentable.
rec13	es	Un procedimiento determinado o una secuencia de acciones podr�n aportar una contribuci�n t�cnica al estado de la t�cnica y constituir as� una invenci�n patentable cuando se ejecuten en el contexto de un aparato, como por ejemplo un ordenador. Sin embargo, un algoritmo definido sin referencia a un entorno f�sico es esencialmente no t�cnico y no puede constituir por tanto una invenci�n patentable.
rec13a	es	
rec13b	es	
rec13c	es	
rec13d	es	
rec14	es	La protecci�n jur�dica de las invenciones implementadas en ordenador no deber�a precisar la creaci�n de una disposici�n jur�dica independiente que sustituya a las normas de la legislaci�n nacional sobre patentes. Las normas de la legislaci�n nacional sobre patentes deben seguir siendo la referencia b�sica para la protecci�n jur�dica de las invenciones implementadas en ordenador, con las adaptaciones o a�adidos que sean necesarios en algunos casos espec�ficos de acuerdo con la Directiva.
rec15	es	La presente Directiva deber� limitarse al establecimiento de determinados principios aplicables a la patentabilidad de este tipo de invenciones. Dichos principios pretenden garantizar que las invenciones que pertenezcan a un campo de la tecnolog�a y aporten una contribuci�n t�cnica puedan ser objeto de protecci�n y que, por el contrario, aquellas invenciones que no aporten una contribuci�n t�cnica no lo sean.
rec16	es	La posici�n competitiva de la industria europea respecto a sus principales socios comerciales mejorar� si se eliminan las diferencias existentes en la protecci�n jur�dica de las invenciones implementadas en ordenador y se garantiza la transparencia de la situaci�n jur�dica.
rec17	es	La presente Directiva se entender� sin perjuicio de la aplicaci�n de las normas sobre competencia, en particular los art�culos 81 y 82 del Tratado.
rec18	es	Los actos permitidos en el marco de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular sus preceptos relativos a la descompilaci�n y la interoperabilidad, o las disposiciones relativas a la topograf�a de los productos semiconductores o las marcas comerciales, no se ver�n afectados por la protecci�n que las patentes otorgan a las invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva.
rec18a	es	
rec19	es	Puesto que los objetivos de la acci�n propuesta, a saber, la armonizaci�n de las normas nacionales relativas a las invenciones implementadas en ordenador, no pueden ser alcanzados de manera suficiente por los Estados miembros y, por consiguiente, pueden lograrse mejor, debido a la dimensi�n o a los efectos de la acci�n contemplada, a nivel comunitario, la Comunidad podr� adoptar medidas, con arreglo al principio de subsidiariedad establecido en el art�culo 5 del Tratado. Con arreglo al principio de proporcionalidad, establecido en ese mismo art�culo, la presente Directiva no excede de lo necesario para alcanzar los objetivos mencionados. HAN ADOPTADO LA PRESENTE DIRECTIVA
art1	es	
art2	es	
art2a	es	
art2b	es	
art2ba	es	
art2bb	es	
art3	es	
art3a	es	
art4	es	
art4_1	es	
art4_2	es	
art4_3	es	
art4_3a	es	
art4a	es	
art4b	es	
art5	es	
art5_1	es	
art5_1a	es	
art5_1b	es	
art5_1c	es	
art5_1d	es	
art6	es	
art6a	es	
art7	es	
art8	es	
art8a	es	
art8b	es	
art8c	es	
art8ca	es	
art8cb	es	
art8cc	es	
art8cd	es	
art8ce	es	
art8cf	es	
art8cg	es	
art8_1a	es	
art9	es	
art9_1	es	
art9_2	es	
art10	es	
art11	es	
rec	fi	
rec1	fi	Sis�markkinoiden toteutumisen my�t� vapaan liikkuvuuden esteet ja kilpailua v��rist�v�t tekij�t poistuvat ja luodaan innovaatiotoiminnalle ja investoinneille suotuisa toimintaymp�rist�. T�ss� yhteydess� keksint�jen suojaaminen patentein on sis�markkinoiden onnistumista edist�v� keskeinen tekij�. Tietokoneella toteutettujen keksint�jen tehokas ja yhdenmukainen suoja kaikissa j�senvaltioissa on olennaisen t�rke�� alan investointien jatkumiseksi ja kannustamiseksi.
rec2	fi	Tietokoneella toteutetuille keksinn�ille eri j�senvaltioiden hallintok�yt�nn�n ja oikeusk�yt�nn�n nojalla my�nnett�v�ss� oikeudellisessa suojassa on eroja. Kyseiset erot voivat luoda kaupan esteit� ja t�ll� tavoin haitata sis�markkinoiden moitteetonta toimintaa.
rec3	fi	Kyseisi� eroja on syntynyt ja ne saattavat edelleen kasvaa, kun j�senvaltiot hyv�ksyv�t uusia, toisistaan poikkeavia hallintok�yt�nteit� tai kun voimassaolevaa lains��d�nt�� tulkitseva oikeusk�yt�nt� kehittyy eri tavalla eri j�senvaltioissa.
rec4	fi	Tietokoneohjelmien levityksen ja k�yt�n tasainen kasvu kaikilla tekniikanaloilla ja maailmanlaajuisesti Internetin kautta on keskeinen tekij� teknologiainnovaatioiden kannalta. T�m�n vuoksi on tarpeen varmistaa, ett� tietokoneohjelmien kehitt�j�t ja k�ytt�j�t voivat toimia yhteis�ss� mahdollisimman suotuisassa ymp�rist�ss�.
rec5	fi	N�in ollen oikeudelliset s��nn�t, sellaisina kuin j�senvaltioiden tuomioistuimet ovat niit� tulkinneet, olisi yhdenmukaistettava, ja tietokoneella toteutettujen keksint�jen patentoitavuutta s��ntelev� lains��d�nt� olisi teht�v� avoimeksi. N�iden toimien tuloksena saavutettavan oikeusvarmuuden ansiosta yritykset voivat saada mahdollisimman paljon etua tietokoneella toteutetuille keksinn�ille my�nnett�vist� patenteista, ja oikeusvarmuuden avulla edistet��n investointeja ja innovaatiotoimintaa.
rec5a	fi	
rec6	fi	Euroopan yhteis� ja sen j�senvaltiot ovat sitoutuneet noudattamaan sopimusta teollis- ja tekij�noikeuksien kauppaan liittyvist� n�k�kohdista (j�ljemp�n� 'TRIPS-sopimus'), joka on hyv�ksytty 22 p�iv�n� joulukuuta 1994 tehdyll� neuvoston p��t�ksell� 94/800/EY Uruguayn kierroksen monenv�lisiss� kauppaneuvotteluissa (1986-1994) laadittujen sopimusten tekemisest� Euroopan yhteis�n puolesta yhteis�n toimivaltaan kuuluvissa asioissa [42]. TRIPS-sopimuksen 27 artiklan 1 kohdan mukaan milt� tekniikan alalta tahansa olevaa tuotetta tai menetelm�� koskevan keksinn�n tulee olla patentoitavissa edellytt�en, ett� se on uusi ja keksinn�llinen ja ett� sit� voidaan k�ytt�� teollisesti hyv�ksi. Lis�ksi TRIPS-sopimuksen mukaan patenttien saaminen ja patenttioikeuksista nauttiminen eiv�t saa olla riippuvaisia siit�, mik� tekniikanala on kyseess�. N�it� periaatteita olisi sovellettava my�s tietokoneella toteutettuihin keksint�ihin. [42] EYVL L 336, 23.12.1994, s. 1.
rec7	fi	Eurooppapatenttien my�nt�misest� M�ncheniss� 5 p�iv�n� lokakuuta 1973 allekirjoitetun yleissopimuksen ja j�senvaltioiden patenttis��d�sten mukaan keksint�in� ei nimenomaisesti pidet� tietokoneohjelmia eik� l�yt�j�, tieteellisi� teorioita, matemaattisia menetelmi�, taiteellisia luomuksia eik� �lyllist� toimintaa, pelej� tai liiketoimintaa varten tarkoitettuja suunnitelmia, s��nt�j� tai menetelmi� eik� tietojen esitt�mist�, joten n�m� kohteet eiv�t ole patentoitavissa. T�t� poikkeusta kuitenkin sovelletaan ja se on perusteltavissa ainoastaan silt� osin kuin patenttihakemus tai patentti koskee kyseist� kohdetta tai toimintaa sin�ns�, koska mainitut kohteet tai toiminnot sin�ns� eiv�t kuulu mihink��n tekniikanalaan.
rec7a0	fi	
rec7a	fi	
rec7b	fi	
rec8	fi	Patenttisuoja antaa keksij�lle mahdollisuuden hy�ty� luovuudestaan. Vaikka innovaatioiden suojaaminen patenttioikeuksin on koko yhteiskunnan edun mukaista, patenttioikeuksia ei saisi k�ytt�� kilpailua v��rist�v�ll� tavalla.
rec9	fi	Tietokoneohjelmien oikeudellisesta suojasta 14 p�iv�n� toukokuuta 1991 annetun neuvoston direktiivin 91/250/ETY [43] mukaisesti tekij�noikeuden suoja koskee kirjalliseksi teokseksi katsottavan alkuper�isen tietokoneohjelman eri ilmaisumuotoja. Sen sijaan ideat ja periaatteet, jotka ovat tietokoneohjelman mink� tahansa osan perustana, eiv�t saa tekij�noikeudellista suojaa. [43] EYVL L 122, 17.5.1991, s. 42. Direktiivi sellaisena kuin se on muutettuna direktiivill� 93/98/ETY (EYVL L 290, 24.11.1993, s. 9).
rec10	fi	Jotta keksint� on patentoitavissa, sen on oltava luonteeltaan tekninen ja n�in ollen kuuluttava johonkin tekniikanalaan.
rec11	fi	Vaikka tietokoneella toteutettuja keksint�j� pidet��n tekniikanalaan kuuluvina, niilt� edellytet��n muiden keksint�jen tapaan keksinn�llisyytt�, eli niiden on tuotava lis�ys vallitsevaan tekniikan tasoon.
rec12	fi	N�in ollen siin� tapauksessa, ett� keksint� ei tuo lis�yst� vallitsevaan tekniikan tasoon, esimerkiksi silloin, kun lis�ys vallitsevaan tasoon ei ole luonteeltaan tekninen, keksint� ei ole keksinn�llinen, joten se ei ole patentoitavissa.
rec13	fi	M��ritelty menettely tai toimintojen sarja, joka suoritetaan tietokoneen kaltaisella laitteella, voi tuoda lis�yksen vallitsevaan tekniikan tasoon, joten sit� voidaan pit�� patentoitavissa olevana keksint�n�. Kuitenkaan sellainen algoritmi, joka m��ritell��n ilman viittausta fyysiseen ymp�rist��n, ei ole luonteeltaan tekninen, joten sit� ei voida pit�� patentoitavissa olevana keksint�n�.
rec13a	fi	
rec13b	fi	
rec13c	fi	
rec13d	fi	
rec14	fi	Tietokoneella toteutettujen keksint�jen oikeudellinen suoja ei saisi edellytt�� erillist� s��ntely�, joka korvaisi kansallisten patenttis��d�sten s��nn�t. Tietokoneella toteutettujen keksint�jen oikeudellisen suojan olisi edelleen perustuttava p��osin kansallisten patenttis��d�sten s��nt�ihin sellaisina kuin niit� on t�m�n direktiivin mukaisesti tietyilt� osin mukautettu tai t�ydennetty.
rec15	fi	T�ss� direktiiviss� olisi ainoastaan vahvistettava tietyt periaatteet, joita sovelletaan kyseisten keksint�jen patentoitavuuteen ja joiden tarkoituksena on erityisesti sen varmistaminen, ett� tekniikanalaan kuuluvat ja lis�yksen tekniikan tasoon tuovat keksinn�t voidaan suojata, ja toisaalta sen varmistaminen, ett� keksint�j�, jotka eiv�t tuo lis�yst� tekniikan tasoon, ei voi suojata.
rec16	fi	Euroopan teollisuuden kilpailuasema suhteessa t�rkeimpiin kauppakumppaneihin paranee, jos tietokoneella toteutettujen keksint�jen oikeudellisen suojan nykyiset erot poistetaan ja oikeudellisen tilanteen avoimuutta lis�t��n.
rec17	fi	T�m� direktiivi ei rajoita kilpailus��nt�jen eik� etenk��n perustamissopimuksen 81 ja 82 artiklan soveltamista.
rec18	fi	T�m�n direktiivin soveltamisalaan kuuluviin keksint�ihin liittyv� patenttisuoja ei vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY mukaisesti sallittuihin toimiin eik� kyseisess� direktiiviss� analysoinnista ja yhteentoimivuudesta annettujen s��nn�sten soveltamiseen, eik� puolijohdetuotteiden piirimalleista tai tavaramerkeist� annettujen s��nn�sten soveltamiseen.
rec18a	fi	
rec19	fi	Koska j�senvaltiot eiv�t voi riitt�v�ll� tavalla toteuttaa suunnitellun toiminnan tavoitteita, joiden avulla pyrit��n yhdenmukaistamaan tietokoneella toteutetuista keksinn�ist� annetut kansalliset s��nn�t, vaan tavoitteet voidaan toiminnan laajuuden tai vaikutusten takia toteuttaa paremmin yhteis�n tasolla, yhteis� voi ryhty� toimiin perustamissopimuksen 5 artiklassa m��r�tyn toissijaisuusperiaatteen mukaisesti. Suhteellisuusperiaatteen mukaisesti, sellaisena kuin se esitet��n sanotussa artiklassa, t�ll� direktiivill� ei ylitet� sit�, mik� on tarpeen n�iden tavoitteiden saavuttamiseksi, OVAT ANTANEET T�M�N DIREKTIIVIN
art1	fi	T�ss� direktiiviss� sovelletaan seuraavia m��ritelmi�
art2	fi	
art2a	fi	
art2b	fi	
art2ba	fi	
art2bb	fi	
art3	fi	J�senvaltioiden on varmistettava, ett� tietokoneella toteutettua keksint�� pidet��n johonkin tekniikanalaan kuuluvana.
art3a	fi	
art4	fi	1. J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi ja keksinn�llinen. 2. J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon. 3. Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen, jonka osat voivat k�sitt�� sek� teknisi� ett� muita kuin teknisi� piirteit�, ja vallitsevan tason v�lill�.
art4_1	fi	
art4_2	fi	
art4_3	fi	
art4_3a	fi	
art4a	fi	
art4b	fi	
art5	fi	J�senvaltioiden on varmistettava, ett� tietokoneella toteutetulle keksinn�lle voidaan hakea patenttisuojaa tuotteena, toisin sanoen ohjelmoituna tietokoneena, ohjelmoituna tietokoneiden verkkona tai muuna ohjelmoituna laitteena, tai kyseisen tietokoneen, tietokoneiden verkon tai laitteen suorittamana prosessina, joka pannaan t�yt�nt��n ohjelmiston avulla.
art5_1	fi	
art5_1a	fi	
art5_1b	fi	
art5_1c	fi	
art5_1d	fi	
art6	fi	T�m�n direktiivin soveltamisalaan kuuluviin keksint�ihin liittyv� patenttisuoja ei vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY mukaisesti sallittuihin toimiin eik� kyseisess� direktiiviss� analysoinnista ja yhteentoimivuudesta annettujen s��nn�sten soveltamiseen, eik� puolijohdetuotteiden piirimalleista tai tavaramerkeist� annettujen s��nn�sten soveltamiseen.
art6a	fi	
art7	fi	Komissio seuraa tietokoneella toteutettujen keksint�jen vaikutuksia innovaatiotoimintaan ja kilpailuun Euroopan laajuisesti ja kansainv�lisesti sek� niiden vaikutuksia eurooppalaisiin yrityksiin, mukaan luettuna s�hk�inen kaupank�ynti.
art8	fi	Komissio antaa Euroopan parlamentille ja neuvostolle viimeist��n [ p�iv�n� kuuta (kolmen vuoden kuluttua 9 artiklan 1 kohdassa tarkoitetusta p�iv�st�] kertomuksen (a) tietokoneella toteutettuja keksint�j� koskevien patenttien vaikutuksista 7 artiklassa tarkoitettuihin seikkoihin; (b) siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; ja (c) siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi.
art8a	fi	
art8b	fi	
art8c	fi	
art8ca	fi	
art8cb	fi	
art8cc	fi	
art8cd	fi	
art8ce	fi	
art8cf	fi	
art8cg	fi	
art8_1a	fi	
art9	fi	1. J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n [ p�iv�n� kuuta (kuukauden viimeinen p�iv�)]. Niiden on ilmoitettava t�st� komissiolle viipym�tt�. N�iss� j�senvaltioiden antamissa s��d�ksiss� on viitattava t�h�n direktiiviin tai niihin on liitett�v� t�llainen viittaus, kun ne virallisesti julkaistaan. J�senvaltioiden on s��dett�v� siit�, miten viittaukset tehd��n. 2. J�senvaltioiden on annettava komissiolle tiedoksi kansalliset s��d�kset, jotka ne antavat t�m�n direktiivin soveltamisalaan kuuluvista kysymyksist�.
art9_1	fi	
art9_2	fi	
art10	fi	T�m� direktiivi tulee kahdentenakymmenenten� p�iv�n� sen j�lkeen, kun se on julkaistu Euroopan yhteis�jen virallisessa lehdess�.
art11	fi	T�m� direktiivi on osoitettu kaikille j�senvaltioille.
rec	fr	
rec1	fr	La r�alisation du march� int�rieur implique que l'on �limine les restrictions � la libre circulation et les distorsions � la concurrence, tout en cr�ant un environnement favorable � l'innovation et � l'investissement. Dans ce contexte, la protection des inventions par brevet est un �l�ment essentiel du succ�s du march� int�rieur. Une protection effective et harmonis�e des inventions mises en oeuvre par ordinateur dans tous les �tats membres est essentielle pour maintenir et encourager les investissements dans ce domaine.
rec2	fr	Des diff�rences existent dans la protection des inventions mises en oeuvre par ordinateur conf�r�es par les pratiques administratives et la jurisprudence des �tats membres. Ces diff�rences pourraient cr�er des entraves aux �changes et faire ainsi obstacle au bon fonctionnement du march� int�rieur.
rec3	fr	De telles diff�rences r�sultent du fait que les �tats membres adoptent de nouvelles pratiques administratives qui diff�rent les unes des autres ou que les jurisprudences nationales interpr�tant la l�gislation actuelle �voluent diff�remment.
rec4	fr	Ces diff�rences pourraient prendre de l'ampleur avec le temps. La diffusion et l'utilisation croissantes de programmes d'ordinateurs dans tous les domaines de la technique et les moyens de diffusion mondiale via l'Internet sont un facteur critique de l'innovation technologique. Il convient donc de veiller � ce que les d�veloppeurs et les utilisateurs de programmes d'ordinateurs dans la Communaut� b�n�ficient d'un environnement optimal.
rec5	fr	En cons�quence, les r�gles de droit telles qu'interpr�t�es par les tribunaux des �tats membres doivent �tre harmonis�es et les dispositions r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre rendues transparentes. La s�curit� juridique qui en r�sulte devrait permettre aux entreprises de tirer le meilleur parti des brevets pour les inventions mises en oeuvre par ordinateur et stimuler l'investissement et l'innovation.
rec5a	fr	
rec6	fr	La Communaut� et ses �tats membres sont li�s par l'accord relatif aux aspects des droits de propri�t� intellectuelle qui touchent au commerce (ADPIC), approuv� par la d�cision 94/800/CE du Conseil, du 22 d�cembre 1994, relative � la conclusion au nom de la Communaut� europ�enne, pour ce qui concerne les mati�res relevant de ses comp�tences, des accords des n�gociations multilat�rales du cycle de l'Uruguay (1986-1994) [42]. L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra �tre obtenu pour toute invention, de produit ou de proc�d�, dans tous les domaines techniques, � condition qu'elle soit nouvelle, qu'elle implique une activit� inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent �tre obtenus et des droits de brevets exerc�s sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur. [42] JO L 336, 23.12.1994, p. 1
rec7	fr	En vertu de la Convention sur la d�livrance de brevets europ�ens sign�e � Munich, le 5 octobre 1973, et du droit des brevets des �tats membres, les programmes d'ordinateurs ainsi que les d�couvertes, th�ories scientifiques, m�thodes math�matiques, cr�ations esth�tiques, plans, principes et m�thodes dans l'exercice d'activit�s intellectuelles, en mati�re de jeu ou dans le domaine des activit�s �conomiques et les pr�sentations d'informations, ne sont pas consid�r�s comme des inventions et sont donc exclus de la brevetabilit�. Cette exception ne s'applique cependant et n'est justifi�e que dans la mesure o� la demande de brevet ou le brevet concerne ces objets ou ces activit�s en tant que tels parce que lesdits objets et activit�s en tant que tels n'appartiennent � aucun domaine technique.
rec7a0	fr	
rec7a	fr	
rec7b	fr	
rec8	fr	La protection par brevet permet aux innovateurs de tirer profit de leur cr�ativit�. Les droits de brevet prot�gent l'innovation dans l'int�r�t de la soci�t� dans son ensemble mais ils ne doivent pas �tre utilis�s d'une mani�re anticoncurrentielle.
rec9	fr	Conform�ment � la directive du Conseil 91/250/CEE du 14 mai 1991 concernant la protection juridique des programmes d'ordinateurs [43], toute expression d'un programme d'ordinateur original est prot�g�e par un droit d'auteur en tant qu'oeuvre litt�raire. Toutefois, les id�es et principes qui sont � la base de quelques �l�ments que ce soit d'un programme d'ordinateur ne sont pas prot�g�s par le droit d'auteur. [43] JO 122 , 17.5.1991 p. 42- directive modifi�e par la directive 93/98/CEE (JO L 290, 24.11.1993, p. 9).
rec10	fr	Pour �tre consid�r�e comme brevetable, une invention doit pr�senter un caract�re technique et donc appartenir � un domaine technique.
rec11	fr	Bien que les inventions mises en oeuvre par ordinateur soient consid�r�es comme appartenant � un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique � l'�tat de la technique pour r�pondre au crit�re de l'activit� inventive.
rec12	fr	En cons�quence, lorsqu'une invention n'apporte pas de contribution technique � l'�tat de la technique, parce que, par exemple, sa contribution sp�cifique ne rev�t pas un caract�re technique, elle ne r�pond pas au crit�re de l'activit� inventive et ne peut donc faire l'objet d'un brevet.
rec13	fr	Une proc�dure d�finie ou une s�quence d'actions ex�cut�es sur un appareil tel qu'un ordinateur, peut apporter une contribution technique � l'�tat de la technique et constituer ainsi une invention brevetable. Par contre, un algorithme d�fini sans r�f�rence � un environnement physique ne pr�sente pas un caract�re technique et ne peut donc constituer une invention brevetable.
rec13a	fr	
rec13b	fr	
rec13c	fr	
rec13d	fr	
rec14	fr	La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas n�cessiter l'�tablissement d'une l�gislation distincte en lieu et place des dispositions du droit national des brevets. Les r�gles du droit national des brevets doivent continuer de former la base de r�f�rence de la protection juridique des inventions mises en oeuvre par ordinateur, m�me si elles doivent �tre adapt�es ou ajout�es en fonction de certaines contraintes sp�cifiques d�finies dans la directive.
rec15	fr	La pr�sente directive devrait se borner � fixer certains principes s'appliquant � la brevetabilit� de ce type d'inventions, ces principes ayant notamment pour but d'assurer que les inventions appartenant � un domaine technique et apportant une contribution technique peuvent faire l'objet d'une protection et inversement d'assurer que les inventions qui n'apportent pas de contribution technique ne peuvent b�n�ficier d'une protection.
rec16	fr	La position concurrentielle de l'industrie europ�enne vis-�-vis de ses principaux partenaires commerciaux serait am�lior�e si les diff�rences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur �taient �limin�es et si la situation juridique �tait transparente.
rec17	fr	La pr�sente directive ne pr�juge pas de l'application des r�gles de concurrence, en particulier des articles 81 et 82 du trait�.
rec18	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.,
rec18a	fr	
rec19	fr	Dans la mesure o� les objectifs de l'action envisag�e ne peuvent pas �tre r�alis�s de mani�re suffisante par les �tats membres et peuvent donc, en raison des dimensions ou des effets de l'action envisag�e, �tre mieux r�alis�s au niveau communautaire, la Communaut� est en droit d'adopter des mesures conform�ment au principe de subsidiarit� �nonc� � l'article 5 du trait�. Conform�ment au principe de proportionnalit�, tel qu'�nonc� dans cet article, la pr�sente directive ne va pas au-del� de ce. qui est n�cessaire pour atteindre les objectifs fix�s, ONT ARR�T� LA PR�SENTE DIRECTIVE
art1	fr	
art2	fr	Aux fins de la pr�sente directive, les d�finitions suivantes s'appliquent
art2a	fr	
art2b	fr	
art2ba	fr	
art2bb	fr	
art3	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit consid�r�e comme appartenant � un domaine technique.
art3a	fr	
art4	fr	1. Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activit� inventive. 2. Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique. 3. La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet de la revendication de brevet consid�r� dans son ensemble, dont les �l�ments peuvent comprendre des caract�ristiques techniques et non techniques, et l'�tat de la technique.
art4_1	fr	
art4_2	fr	
art4_3	fr	
art4_3a	fr	
art4a	fr	
art4b	fr	
art5	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme.
art5_1	fr	
art5_1a	fr	
art5_1b	fr	
art5_1c	fr	
art5_1d	fr	
art6	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.,
art6a	fr	
art7	fr	La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes y compris le commerce �lectronique.
art8	fr	La Commission soumet au Parlement europ�en et au Conseil, pour le [DATE (trois ans � compter de la date sp�cifi�e � l'article 9 (1))] au plus tard, un rapport indiquant
art8a	fr	
art8b	fr	
art8c	fr	
art8ca	fr	
art8cb	fr	
art8cc	fr	
art8cd	fr	
art8ce	fr	
art8cf	fr	
art8cg	fr	
art8_1a	fr	
art9	fr	1. Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent imm�diatement la Commission. Lorsque les �tats membre adoptent ces dispositions, celles-ci contiennent une r�f�rence � la pr�sente directive ou sont accompagn�es d'une telle r�f�rence lors de leur publication officielle. Les �tats membres d�terminent la mani�re dont cette r�f�rence doit �tre faite. 2. Les �tats membres communiquent � la Commission le texte des dispositions de droit interne qu'ils adoptent dans le domaine couvert par la pr�sente directive.
art9_1	fr	
art9_2	fr	
art10	fr	La pr�sente directive entre en vigueur le vingti�me jour suivant celui de sa publication au Journal officiel des Communaut�s europ�ennes.
art11	fr	Les �tats membres sont destinataires de la pr�sente directive.
rec	it	
rec1	it	La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza nonch� la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto la protezione delle invenzioni mediante i brevetti � un elemento essenziale per il successo del mercato interno. Una protezione efficace ed armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri � indispensabile per mantenere e stimolare gli investimenti in questo campo.
rec2	it	Esistono discrepanze nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici assicurata dalle pratiche amministrative e dalla giurisprudenza dei vari Stati membri. Tali divergenze possono creare ostacoli agli scambi commerciali e quindi al buon funzionamento del mercato interno.
rec3	it	Tali differenze sono sorte e potrebbero accentuarsi in conseguenza del fatto che gli Stati membri adottano nuove e differenti pratiche amministrative o del fatto che le giurisprudenze nazionali che interpretano la legislazione in vigore evolvono in modo diverso.
rec4	it	Il costante aumento della diffusione e dell'uso dei programmi per elaboratori in tutti i campi della tecnologia e della loro diffusione in tutto il mondo tramite Internet � un fattore decisivo dell'innovazione tecnologica. � quindi necessario fare in modo che i creatori e gli utilizzatori di programmi per elaboratore possano beneficiare nella Comunit� delle migliori condizioni possibili.
rec5	it	� pertanto necessario armonizzare le disposizioni di legge e la loro interpretazione da parte dei tribunali degli Stati membri e rendere trasparenti le norme che disciplinano la brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici. La certezza giuridica che ne risulter� dovrebbe permettere alle imprese di ricavare il massimo vantaggio dai brevetti di invenzioni attuate per mezzo di elaboratori elettronici e stimolare gli investimenti e l'innovazione.
rec5a	it	
rec6	it	La Comunit� e i suoi Stati membri sono parti dell'accordo sugli aspetti dei diritti di propriet� intellettuali attinenti al commercio, approvato con la decisione del Consiglio 94/800/CE, del 22 dicembre 1994, relativa alla conclusione a nome della Comunit� europea, per le materie di sua competenza, degli accordi dei negoziati multilaterali dell'Uruguay Round (1986-1994) [42]. L'articolo 27, paragrafo 1 di detto accordo dispone che possono costituire oggetto di brevetto tutte le invenzioni, di prodotti o di processi, in tutti i campi della tecnologia, che presentino carattere di novit�, implichino un'attivit� inventiva e siano atte ad un'applicazione industriale. Inoltre, in base all'accordo, i brevetti possono essere ottenuti e i relativi diritti possono essere esercitati senza discriminazioni quanto al settore della tecnologia. Questi principi valgono di conseguenza per le invenzioni attuate per mezzo di elaboratori elettronici. [42] GU L 336, 23.12.1994, p. 1.
rec7	it	Secondo la convenzione sul rilascio dei brevetti europei, firmata a Monaco di Baviera il 5 ottobre 1973, e secondo le legislazioni degli Stati membri in materia di brevetti, i programmi per elaboratore, nonch� le scoperte, le teorie scientifiche, i metodi matematici, le creazioni estetiche, i piani, i principi e i metodi per attivit� intellettuali, giochi o attivit� commerciali e le presentazioni di informazioni sono espressamente non considerati invenzioni e sono quindi esclusi dalla brevettabilit�. Questa eccezione, tuttavia, si applica ed � giustificata soltanto nella misura in cui una domanda di brevetto o un brevetto si riferisce a tali materie o attivit� in quanto tali, perch� tali materie o attivit� in quanto tali non appartengono ad un settore della tecnologia.
rec7a0	it	
rec7a	it	
rec7b	it	
rec8	it	La tutela del brevetto permette agli innovatori di trarre beneficio dalla loro attivit� creativa. Poich� i brevetti tutelano l'innovazione nell'interesse della societ� nel suo insieme, non devono essere utilizzati in modo da ostacolare la concorrenza.
rec9	it	A norma della direttiva 91/250/CEE del Consiglio, del 14 marzo 1991, relativa alla tutela giuridica dei programmi per elaboratore [43], qualsiasi forma di espressione di un programma per elaboratore � tutelata dal diritto d'autore in quanto opera letteraria. Tuttavia, le idee e i principi alla base di qualsiasi elemento di un programma per elaboratore non sono tutelati dal diritto d'autore. [43] GU L 122, 17.5.1991, p. 42. Direttiva modificata dalla direttiva 93/98/CEE (GU L 290, 24.11.1993, p. 9).
rec10	it	Perch� sia considerata brevettabile, un'invenzione deve presentare un carattere tecnico e quindi appartenere ad un settore della tecnologia.
rec11	it	Bench� siano considerate appartenenti ad un settore della tecnologia, le invenzioni attuate per mezzo di elaboratori elettronici devono, come le invenzioni in generale, costituire un contributo tecnico allo stato dell'arte per poter essere considerate implicanti un'attivit� inventiva.
rec12	it	Di conseguenza, se un'invenzione non costituisce un contributo tecnico allo stato dell'arte, come nel caso in cui, ad esempio, il suo contributo specifico non presenta un carattere tecnico, non pu� essere considerata implicante un'attivit� inventiva e quindi non � brevettabile.
rec13	it	Un processo o una sequenza di azioni determinati, eseguiti per mezzo di un apparecchio, come un elaboratore, pu� apportare un contributo tecnico allo stato dell'arte e quindi costituire un'invenzione brevettabile. Un algoritmo definito senza riferimento ad un ambiente fisico non presenta invece un carattere tecnico e non pu� quindi costituire un'invenzione brevettabile.
rec13a	it	
rec13b	it	
rec13c	it	
rec13d	it	
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non deve richiedere una legislazione specifica che sostituisca la norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici, con le modifiche o le integrazioni relative a specifici aspetti richieste dalla presente direttiva.
rec15	it	La direttiva deve limitarsi all'enunciazione di taluni principi che si applicano alla brevettabilit� di tali invenzioni, al fine in modo particolare di assicurare la tutela delle invenzioni che appartengono ad un settore della tecnologia e costituiscono un contributo tecnico e, inversamente, di escludere da tale tutela le invenzioni che non costituiscono un contributo tecnico.
rec16	it	La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali sarebbe rafforzata dall'eliminazione delle differenze attuali nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici e dalla trasparenza della situazione giuridica.
rec17	it	La presente direttiva lascia impregiudicata l'applicazione delle norme in materia di concorrenza, in particolare gli articoli 81 e 82 del trattato.
rec18	it	La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facolt� riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilit� o le disposizioni relative alle topografie dei semiconduttori o ai marchi commerciali.
rec18a	it	
rec19	it	Poich� gli obiettivi del provvedimento proposto, ossia l'armonizzazione delle norme nazionali relative alle invenzioni attuate per mezzo di elaboratori elettronici, non possono essere sufficientemente realizzati dagli Stati membri e possono dunque, a motivo delle dimensioni o degli effetti dell'azione in questione, essere realizzati meglio a livello comunitario, l'intervento della Comunit� � giustificato in base al principio della sussidiariet�, enunciato all'articolo 5 del trattato. Conformemente al principio della proporzionalit�, enunciato in questo stesso articolo, la presente direttiva non va al di l� di quanto necessario per il raggiungimento di tali obiettivi. HANNO ADOTTATO LA PRESENTE DIRETTIVA
art1	it	La presente direttiva stabilisce norme relative alla brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici.
art2	it	Ai fini della presente direttiva, s'intende per
art2a	it	
art2b	it	
art2ba	it	
art2bb	it	
art3	it	Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.
art3a	it	
art4	it	1. Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e implichi un'attivit� inventiva. 2. Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico. 3. Il contributo tecnico � valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.
art4_1	it	
art4_2	it	
art4_3	it	
art4_3a	it	
art4a	it	
art4b	it	
art5	it	Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata come prodotto, ossia come elaboratore programmato, rete di elaboratori programmati o altro apparecchio programmato, o come processo realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software.
art5_1	it	
art5_1a	it	
art5_1b	it	
art5_1c	it	
art5_1d	it	
art6	it	La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facolt� riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilit� o le disposizioni relative alle topografie dei semiconduttori o ai marchi commerciali.
art6a	it	
art7	it	La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, compreso il commercio elettronico.
art8	it	La Commissione riferisce al Parlamento europeo e al Consiglio, entro [DATA (tre anni dalla data di cui all'articolo 9, paragrafo 1)], su
art8a	it	
art8b	it	
art8c	it	
art8ca	it	
art8cb	it	
art8cc	it	
art8cd	it	
art8ce	it	
art8cf	it	
art8cg	it	
art8_1a	it	
art9	it	1. Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro il [Data (ultimo giorno di un mese)]. Essi ne informano immediatamente la Commissione. Quando gli Stati membri adottano tali disposizioni, queste contengono un riferimento alla presente direttiva o sono corredati di un siffatto riferimento all'atto della pubblicazione ufficiale. Le modalit� del riferimento sono decise dagli Stati membri. 2. Gli Stati membri comunicano alla Commissione il testo delle disposizioni di diritto interno che essi adottano nella materia disciplinata dalla presente direttiva.
art9_1	it	
art9_2	it	
art10	it	La presente direttiva entra in vigore il ventesimo giorno seguente quello della sua pubblicazione nella Gazzetta ufficiale delle Comunit� europee.
art11	it	Gli Stati membri sono i destinatari della presente direttiva.
rec	nl	
rec1	nl	Voor de totstandbrenging van de interne markt is het nodig beperkingen voor het vrije verkeer en concurrentieverstoringen op te heffen, en tegelijkertijd een gunstig klimaat te scheppen voor innovatie en investeringen. In deze context is de bescherming van uitvindingen een essentieel element voor het succes van de interne markt. Doeltreffende en geharmoniseerde bescherming van in computers ge�mplementeerde uitvindingen in alle lidstaten is van essentieel belang om de investeringen op dit gebied in stand te houden en aan te moedigen.
rec2	nl	Er bestaan verschillen in de bescherming van in computers ge�mplementeerde uitvindingen die wordt geboden door de bestuursrechtelijke praktijken en de jurisprudentie van de verschillende lidstaten. Deze verschillen kunnen leiden tot handelsbelemmeringen en aldus de goede werking van de interne markt verhinderen.
rec3	nl	Deze verschillen zijn ontstaan en kunnen groter worden naarmate de lidstaten nieuwe en uiteenlopende bestuursrechtelijke praktijken goedkeuren, of wanneer de nationale jurisprudentie die de bestaande wetgeving interpreteert, op ongelijke wijze evolueert.
rec4	nl	De gestadige toename van de verspreiding en het gebruik van computerprogramma's op alle gebieden van de technologie en van de wereldwijde verspreiding ervan via internet is een kritieke factor voor de technologische innovatie. Daarom moet worden voorzien in een optimale omgeving voor de ontwikkelaars en de gebruikers van computerprogramma's in de Gemeenschap.
rec5	nl	Om die reden moeten de rechtsregels, zoals ze door de rechtbanken in de lidstaten worden ge�nterpreteerd, worden geharmoniseerd en moet het recht betreffende de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen transparant worden gemaakt. De daaruit voortvloeiende rechtszekerheid moet de ondernemingen in staat stellen optimaal profijt te trekken van octrooien voor in computers ge�mplementeerde uitvindingen en moet een stimulans zijn voor investeringen en innovatie.
rec5a	nl	
rec6	nl	De Gemeenschap en haar lidstaten zijn gebonden door de Overeenkomst inzake de handelsaspecten van de intellectuele eigendom (TRIPS-Overeenkomst), goedgekeurd bij Besluit 94/800/EG van de Raad van 22 december 1994 betreffende de sluiting, namens de Europese Gemeenschap voor wat betreft de onder haar bevoegdheid vallende aangelegenheden, van de uit de multilaterale handelsbesprekingen in het kader van de Uruguay-Ronde (1986-1994) voortvloeiende overeenkomsten [42]. Artikel 27, lid 1, van de TRIPS-Overeenkomst bepaalt dat octrooi kan worden verleend voor uitvindingen, producten dan wel werkwijzen, op alle gebieden van de technologie, mits zij nieuw zijn, op uitvinderswerkzaamheid berusten en vatbaar zijn voor industri�le toepassing. Bovendien kan volgens de TRIPS-Overeenkomst octrooi worden verleend en kunnen octrooirechten worden genoten zonder onderscheid op grond van het gebied van de technologie. Deze beginselen moeten dienovereenkomstig gelden voor in computers ge�mplementeerde uitvindingen. [42] PB L 336 van 23.12.1994, blz. 1.
rec7	nl	Overeenkomstig het Verdrag inzake de verlening van Europese octrooien, ondertekend in M�nchen op 5 oktober 1973, en de octrooiwetten van de lidstaten worden computerprogramma's alsmede ontdekkingen, natuurwetenschappelijke theorie�n, wiskundige methoden, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten. Deze uitzondering is echter alleen van toepassing en gerechtvaardigd voorzover een octrooiaanvrage of octrooi betrekking heeft op deze onderwerpen of werkzaamheden als zodanig, omdat de genoemde onderwerpen en werkzaamheden als zodanig niet tot een gebied van de technologie behoren.
rec7a0	nl	
rec7a	nl	
rec7b	nl	
rec8	nl	Dankzij octrooibescherming kunnen innovatoren profijt trekken van hun creativiteit. Octrooirechten bieden bescherming voor innovatie in het belang van de maatschappij, maar mogen niet worden gebruikt op een wijze die de concurrentie beperkt.
rec9	nl	Overeenkomstig Richtlijn 91/250/EEG van de Raad van 14 mei 1991 betreffende de rechtsbescherming van computerprogramma's [43] wordt de uitdrukkingswijze, in welke vorm dan ook, van een oorspronkelijk computerprogramma auteursrechtelijk beschermd als werk van letterkunde. De idee�n en beginselen die aan enig element van een computerprogramma ten grondslag liggen, worden echter niet auteursrechtelijk beschermd. [43] PB L 122 van 17.5.1991, blz. 42. Richtlijn gewijzigd bij Richtlijn 93/98/EEG (PB L 290 van 24.11.1993, blz. 9).
rec10	nl	Opdat een uitvinding als octrooieerbaar kan worden beschouwd, moet deze een technisch karakter hebben, en aldus behoren tot een gebied van de technologie.
rec11	nl	Hoewel van in computers ge�mplementeerde uitvindingen wordt aangenomen dat ze tot een gebied van de technologie behoren, moeten zij, om op uitvinderswerkzaamheid te berusten zoals uitvindingen in het algemeen, een technische bijdrage tot de stand van de techniek leveren.
rec12	nl	Dienovereenkomstig zal een uitvinding die geen technische bijdrage tot de stand van de techniek levert, zoals bijvoorbeeld het geval zou zijn wanneer de specifieke bijdrage ervan geen technisch karakter heeft, niet op uitvinderswerkzaamheid berusten en bijgevolg niet octrooieerbaar zijn.
rec13	nl	Een gedefinieerde procedure of opeenvolging van verrichtingen kan, wanneer deze wordt uitgevoerd in de context van een apparaat zoals een computer, een technische bijdrage tot de stand van de techniek leveren en daardoor een octrooieerbare uitvinding vormen. Een algoritme dat zonder verwijzing naar een fysieke omgeving wordt gedefinieerd, is echter inherent niet-technisch en kan daarom geen octrooieerbare uitvinding vormen.
rec13a	nl	
rec13b	nl	
rec13c	nl	
rec13d	nl	
rec14	nl	De rechtsbescherming van in computers ge�mplementeerde uitvindingen mag niet vereisen dat wordt voorzien in een afzonderlijk rechtsinstrument ter vervanging van de regels van het nationale octrooirecht. De regels van het nationale octrooirecht moeten, zoals aangepast of aangevuld overeenkomstig het bepaalde in deze richtlijn, de essenti�le basis blijven voor de rechtsbescherming van in computers ge�mplementeerde uitvindingen.
rec15	nl	De richtlijn moet worden beperkt tot het vaststellen van bepaalde beginselen met betrekking tot de octrooieerbaarheid van dergelijke uitvindingen. Deze beginselen dienen er met name voor te zorgen dat uitvindingen die tot een gebied van de technologie behoren en een technische bijdrage leveren, voor bescherming in aanmerking komen, en omgekeerd dat uitvindingen die geen technische bijdrage leveren, daar niet voor in aanmerking komen.
rec16	nl	De concurrentiepositie van het Europese bedrijfsleven ten opzichte van zijn voornaamste handelspartners zou worden verbeterd indien de bestaande verschillen in de rechtsbescherming van in computers ge�mplementeerde uitvindingen zouden worden opgeheven en de rechtssituatie transparant zou zijn.
rec17	nl	Deze richtlijn doet geen afbreuk aan de toepassing van de mededingingsregels, inzonderheid de artikelen 81 en 82 van het Verdrag.
rec18	nl	Handelingen die zijn toegestaan op grond van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit, of de bepalingen betreffende topografie�n van halfgeleiderproducten of merken, worden onverlet gelaten door de bescherming die binnen de werkingssfeer van deze richtlijn door octrooien voor uitvindingen wordt verleend.
rec18a	nl	
rec19	nl	Aangezien de doelstellingen van het voorgestelde optreden, namelijk de harmonisatie van de nationale voorschriften betreffende in computers ge�mplementeerde uitvindingen, niet voldoende door de lidstaten kunnen worden verwezenlijkt en derhalve vanwege de omvang of de gevolgen van het optreden beter door de Gemeenschap kunnen worden verwezenlijkt, mag de Gemeenschap overeenkomstig het in artikel 5 van het Verdrag bedoelde subsidiariteitsbeginsel maatregelen treffen. Overeenkomstig het in dat artikel bedoelde evenredigheidsbeginsel gaat deze richtlijn niet verder dan wat nodig is om deze doelstellingen te verwezenlijken. HEBBEN DE VOLGENDE RICHTLIJN VASTGESTELD
art1	nl	Deze richtlijn stelt regels vast voor de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen.
art2	nl	Voor de toepassing van deze richtlijn gelden de volgende definities
art2a	nl	
art2b	nl	
art2ba	nl	
art2bb	nl	
art3	nl	De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding wordt beschouwd als behorende tot een gebied van de technologie.
art3a	nl	
art4	nl	1. De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is en op uitvinderswerkzaamheid berust. 2. De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert. 3. De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.
art4_1	nl	
art4_2	nl	
art4_3	nl	
art4_3a	nl	
art4a	nl	
art4b	nl	
art5	nl	De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding kan worden geclaimd als product, dat wil zeggen als een geprogrammeerde computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat, of als een werkwijze die door zo een computer, computernetwerk of apparaat middels het uitvoeren van software wordt toegepast.
art5_1	nl	
art5_1a	nl	
art5_1b	nl	
art5_1c	nl	
art5_1d	nl	
art6	nl	Handelingen die zijn toegestaan op grond van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit, of de bepalingen betreffende topografie�n van halfgeleiderproducten of merken, worden onverlet gelaten door de bescherming die binnen de werkingssfeer van deze richtlijn door octrooien voor uitvindingen wordt verleend.
art6a	nl	
art7	nl	De Commissie volgt welke invloed in computers ge�mplementeerde uitvindingen hebben op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, met inbegrip van de elektronische handel.
art8	nl	De Commissie brengt bij het Europees Parlement en de Raad tegen uiterlijk [DATUM (drie jaar vanaf de in artikel 9, lid 1, vermelde datum)] verslag uit over
art8a	nl	
art8b	nl	
art8c	nl	
art8ca	nl	
art8cb	nl	
art8cc	nl	
art8cd	nl	
art8ce	nl	
art8cf	nl	
art8cg	nl	
art8_1a	nl	
art9	nl	1. De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om uiterlijk op [DATUM (laatste dag van een maand)] aan deze richtlijn te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis. Wanneer de lidstaten die bepalingen aannemen, wordt in die bepalingen zelf of bij de offici�le bekendmaking daarvan naar deze richtlijn verwezen. De regels voor deze verwijzing worden vastgesteld door de lidstaten. 2. De lidstaten delen de Commissie de bepalingen van nationaal recht mee die zij vaststellen op het gebied waarop deze richtlijn van toepassing is.
art9_1	nl	
art9_2	nl	
art10	nl	Deze richtlijn treedt in werking op de twintigste dag volgende op die van haar bekendmaking in het Publicatieblad van de Europese Gemeenschappen.
art11	nl	Deze richtlijn is gericht tot de lidstaten.
rec	pt	
rec1	pt	A realiza��o do mercado interno implica a elimina��o das restri��es � liberdade de circula��o e �s distor��es da concorr�ncia, criando um ambiente que seja favor�vel � inova��o e ao investimento. Neste contexto, a protec��o dos inventos por meio de patentes � um elemento essencial para o �xito do mercado interno. A protec��o eficaz e harmonizada dos inventos que implicam programas de computador, em todos os Estados-Membros, � essencial para manter e incentivar o investimento neste dom�nio;
rec2	pt	H� diferen�as na protec��o dos inventos que implicam programas de computador, apresentadas pelas pr�ticas administrativas e pela jurisprud�ncia dos diferentes Estados-Membros. Essas diferen�as podem criar barreiras ao com�rcio, dificultando, assim, o bom funcionamento do mercado interno;
rec3	pt	Tais diferen�as desenvolveram-se e podem aumentar � medida que os Estados-Membros adoptam pr�ticas administrativas novas e diferentes ou nos casos em que a jurisprud�ncia nacional que interpreta a legisla��o em vigor evolui de modo diferente;
rec4	pt	O aumento constante da distribui��o e utiliza��o de programas de computador, em todos os ramos tecnol�gicos e na sua distribui��o mundial via Internet, � um factor fundamental da inova��o tecnol�gica. Por isso, � necess�rio garantir a exist�ncia de um ambiente �ptimo para os criadores e utilizadores de programas de computador, na Comunidade;
rec5	pt	Consequentemente, as normas jur�dicas, conforme interpretadas pelos tribunais dos Estados-Membros, devem ser harmonizadas e a lei que rege a patenteabilidade dos inventos que implicam programas de computador deve tornar-se transparente. A certeza jur�dica da� resultante deve permitir �s empresas tirarem o m�ximo partido das patentes dos inventos que implicam programas de computador e dar um incentivo ao investimento e � inova��o;
rec5a	pt	
rec6	pt	A Comunidade e os seus Estados-Membros est�o vinculados pelo Acordo sobre os Aspectos dos Direitos de Propriedade Intelectual Relacionados com o Com�rcio (TRIPS), aprovado pela Decis�o 94/800/CE do Conselho, de 22 de Dezembro de 1994, relativa � celebra��o, em nome da Comunidade Europeia e em rela��o �s mat�rias da sua compet�ncia, dos acordos resultantes das negocia��es multilaterais do Uruguay Round (1986/1994) [42]. O artigo 27.� do Acordo TRIPS prev�, no seu n.� 1, que dever�o existir patentes para quaisquer inventos, quer se trate de produtos ou processos, em todos os dom�nios da tecnologia, desde que esses inventos sejam novos, impliquem uma actividade inventiva e sejam suscept�veis de aplica��o industrial. Al�m disso, segundo o Acordo TRIPS, deve haver direitos de patentes que possam ser usufru�dos sem discrimina��o quanto ao dom�nio da tecnologia. Tais princ�pios devem, nesse sentido, aplicar-se a todos os inventos que implicam programas de computador; [42] JO L 336 de 23.12.1994, p. 1.
rec7	pt	Segundo a Conven��o sobre a Concess�o de Patentes Europeias, assinada em Munique em 5 de Outubro de 1973, e a legisla��o em mat�ria de patentes dos Estados-Membros, os programas de computador, em conjunto com as descobertas, teorias cient�ficas, m�todos matem�ticos, cria��es est�ticas, esquemas, regras e m�todos para execu��o de actividades intelectuais, jogos ou actividades comerciais, assim como exposi��es de informa��o, s�o expressamente n�o considerados inventos, sendo, por isso, exclu�dos da patenteabilidade. Esta excep��o, por�m, aplica-se e justifica-se apenas na medida em que um pedido de patente ou uma patente se relaciona com esses temas ou actividades em si, porque esses temas e actividades, enquanto tais, n�o pertencem a um dom�nio da tecnologia;
rec7a0	pt	
rec7a	pt	
rec7b	pt	
rec8	pt	A protec��o ao abrigo de uma patente permite aos inovadores tirarem partido da sua criatividade. Considerando que os direitos de patente protegem a inova��o no interesse da sociedade em geral, n�o devem ser usados de forma que seja anti-concorrencial;
rec9	pt	De acordo com a Directiva 91/250/CEE do Conselho, de 14 de Maio de 1991, relativa � protec��o jur�dica dos programas de computador [43], a express�o de qualquer forma de um programa de computador original � protegida por direitos de autor como uma obra liter�ria. Contudo, os princ�pios e ideias subjacentes a qualquer elemento de um programa de computador n�o s�o protegidos por direitos de autor; [43] JO L 122 de 17.5.1991, p. 42. Directiva alterada pela Directiva 93/98/CEE (JO L 290 de 24.11.1993, p. 9).
rec10	pt	Para que qualquer invento seja considerado patente�vel, dever ter um car�cter t�cnico e, consequentemente, pertencer a um dom�nio da tecnologia;
rec11	pt	Embora se considere que os inventos que implicam programas de computador pertencem a um dom�nio da tecnologia, para implicarem uma actividade inventiva, em comum com os inventos em geral, devem dar um contributo t�cnico para o progresso tecnol�gico;
rec12	pt	Deste modo, se um invento n�o der um contributo t�cnico para o progresso tecnol�gico, como aconteceria, por exemplo, se o seu contributo espec�fico n�o tivesse car�cter t�cnico, o invento n�o apresentar� uma actividade inventiva, pelo que n�o ser� patente�vel;
rec13	pt	Um processo ou uma sequ�ncia de ac��es definidos, quando executados no contexto de um aparelho, como, por exemplo, um computador, podem dar um contributo t�cnico para o progresso tecnol�gico e, desse modo, constituir um invento patente�vel. No entanto, um algoritmo que � definido sem refer�ncia a um ambiente f�sico � inerentemente n�o t�cnico e n�o pode, por isso, constituir um invento patente�vel;
rec13a	pt	
rec13b	pt	
rec13c	pt	
rec13d	pt	
rec14	pt	Para a protec��o jur�dica dos inventos que implicam programas de computador n�o � necess�rio criar um organismo de aplica��o das normas em vigor da legisla��o nacional em mat�ria de patentes. As regras da legisla��o nacional em mat�ria de patentes devem permanecer a base essencial para a protec��o jur�dica dos inventos que implicam programas de computador, adaptadas ou acrescentadas em certas circunst�ncias espec�ficas, conforme se indica na presente directiva;
rec15	pt	A presente directiva deve limitar-se a fixar certos princ�pios na medida em que se aplicam � patenteabilidade desses inventos, destinando-se esse princ�pios, em particular, a garantir que os inventos pertencentes a um dom�nio da tecnologia e com um contributo t�cnico sejam suscept�veis de protec��o e, por oposi��o, a garantir que os inventos sem contributo t�cnico o n�o sejam;
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhoraria se fossem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos que implicam programas de computador e se a situa��o jur�dica fosse transparente;
rec17	pt	A presente directiva aplicar-se-� sem preju�zo das regras de concorr�ncia, em particular dos artigos 81.� e 82.� do Tratado;
rec18	pt	Os actos permitidos ao abrigo da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular das suas disposi��es relacionadas com a descompila��o e a interoperabilidade ou das disposi��es relativas a topografias de semicondutores ou a marcas comerciais, n�o ser�o afectados pela protec��o concedida pelas patentes aos inventos no �mbito de aplica��o da presente directiva;
rec18a	pt	
rec19	pt	Dado que os objectivos da ac��o proposta, nomeadamente para harmonizar as regras nacionais respeitantes aos inventos que implicam programas de computador n�o podem ser suficientemente alcan�adas pelos Estados-Membros, podendo, por isso, devido � escala ou aos efeitos da ac��o, ser alcan�adas de melhor forma a n�vel comunit�rio, a Comunidade poder� adoptar medidas, de acordo com o princ�pio da subsidiariedade, conforme estabelecido no artigo 5.� do Tratado. De acordo com o princ�pio da proporcionalidade, conforme determinado nesse artigo, a presente directiva n�o vai mais longe que o necess�rio para alcan�ar esses objectivos, ADOPTARAM A PRESENTE DIRECTIVA
art1	pt	A presente directiva estabelece regras para a patenteabilidade dos inventos que implicam programas de computador.
art2	pt	Para efeitos da presente directiva, aplicar-se-�o as seguintes defini��es
art2a	pt	
art2b	pt	
art2ba	pt	
art2bb	pt	
art3	pt	Os Estados-Membros assegurar�o que um invento que implica programas de computador seja considerado como pertencendo a um dom�nio da tecnologia.
art3a	pt	
art4	pt	1. Os Estados-Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva. 2. Os Estados-Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva. 3. O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito da reivindica��o de patente considerada no seu conjunto, cujos elementos possam incluir caracter�sticas t�cnicas e n�o t�cnicas, e o progresso tecnol�gico.
art4_1	pt	
art4_2	pt	
art4_3	pt	
art4_3a	pt	
art4a	pt	
art4b	pt	
art5	pt	Os Estados-Membros garantir�o que um invento que implica programas de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda como processo executado por esse computador, rede inform�tica ou aparelho, pela execu��o do software.
art5_1	pt	
art5_1a	pt	
art5_1b	pt	
art5_1c	pt	
art5_1d	pt	
art6	pt	Os actos permitidos ao abrigo da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular das suas disposi��es relacionadas com a descompila��o e a interoperabilidade ou das disposi��es relativas a topografias de semicondutores ou a marcas comerciais, n�o ser�o afectados pela protec��o concedida pelas patentes aos inventos no �mbito de aplica��o da presente directiva.
art6a	pt	
art7	pt	A Comiss�o acompanhar� o impacto, na inova��o e na concorr�ncia, dos inventos que implicam programas de computador, tanto na Europa como a n�vel internacional, bem como nas empresas europeias, inclusive no com�rcio electr�nico.
art8	pt	A Comiss�o apresentar� ao Parlamento Europeu e ao Conselho, at� [DATA (no prazo de tr�s anos a contar da data especificada no n.� 1 do artigo 9.�)] um relat�rio sobre (a) o impacto das patentes dos inventos que implicam programas de computador nos factores mencionados no artigo 7.�; (b) se as normas que regem a determina��o dos requisitos de patenteabilidade e, mais especificamente, de novidade, de actividade inventiva e do �mbito apropriado das reivindica��es, s�o adequadas; (c) se houve dificuldades relativamente aos Estados-Membros onde os requisitos de novidade e de actividade inventiva n�o s�o examinados antes da emiss�o de uma patente e, nesse caso, se ser� desej�vel tomar medidas para solucionar essas dificuldades.
art8a	pt	
art8b	pt	
art8c	pt	
art8ca	pt	
art8cb	pt	
art8cc	pt	
art8cd	pt	
art8ce	pt	
art8cf	pt	
art8cg	pt	
art8_1a	pt	
art9	pt	1. Os Estados-Membros por�o em vigor as disposi��es legislativas, regulamentares e administrativas necess�rias para darem cumprimento � presente directiva at� [DATA (�ltimo dia de um determinado m�s)]. Desse facto informar�o imediatamente a Comiss�o. Sempre que os Estados-Membros adoptarem tais disposi��es, estas devem incluir uma refer�ncia � presente directiva ou ser acompanhadas dessa refer�ncia aquando da sua publica��o oficial. Os Estados-Membros determinar�o as modalidades dessa refer�ncia. 2. Os Estados-Membros comunicar�o � Comiss�o o texto das disposi��es de direito interno que adoptarem no dom�nio abrangido pela presente directiva.
art9_1	pt	
art9_2	pt	
art10	pt	A presente directiva entra em vigor no vig�simo dia seguinte ao da sua publica��o no Jornal Oficial das Comunidades Europeias.
art11	pt	Os Estados-Membros s�o os destinat�rios da presente directiva.
rec	sv	
rec1	sv	F�r att genomf�ra den inre marknaden m�ste man dels undanr�ja hinder f�r fri r�rlighet och motverka snedvridna konkurrensf�rh�llanden, dels skapa f�rh�llanden som gynnar innovation och investeringar. I detta sammanhang �r skydd f�r uppfinningar genom patent en v�sentlig f�ruts�ttning f�r att den inre marknaden skall bli en framg�ng. Ett verksamt och harmoniserat skydd av datorrelaterade uppfinningar i alla medlemsstater �r viktigt f�r att trygga och stimulera investeringsviljan.
rec2	sv	Det r�ttsskydd f�r datorrelaterade uppfinningar som meddelas i olika medlemsstater uppvisar skillnader p� grund av f�rvaltningspraxis och r�ttspraxis. S�dana skillnader kan skapa handelshinder och motverka den inre marknadens funktion.
rec3	sv	Skillnaderna har vuxit fram och kan bli st�rre genom att medlemsstaterna till�mpar ny och egen f�rvaltningspraxis, eller genom att tolkningen av g�llande lag utvecklas p� olika s�tt i nationell r�ttspraxis.
rec4	sv	Den stadiga �kningen n�r det g�ller distribution och anv�ndning av datorprogram p� alla teknikomr�den samt spridningen av programmen globalt via Internet �r en kritisk faktor f�r teknisk innovation. Det �r d�rf�r n�dv�ndigt att borga f�r optimala f�rh�llanden f�r programanv�ndare och programutvecklare i gemenskapen.
rec5	sv	D�rf�r b�r de lagregler som medlemsstaternas domstolar tolkar harmoniseras och den lag som reglerar patenterbarhet f�r datorrelaterade uppfinningar g�ras genomsynlig. Ty �tf�ljande r�ttss�kerhet b�r ge f�retagen m�jlighet att utnyttja patent p� datorrelaterade uppfinningar optimalt samt stimulera investeringar och innovation.
rec5a	sv	
rec6	sv	Gemenskapen och dess medlemsstater �r bundna av avtalet om handelsrelaterade aspekter av immaterialr�tter (TRIPS), som godk�ndes genom r�dets beslut av den 22 december 1994 om att i Europeiska gemenskapens namn, i fr�gor inom dess beh�righet, sluta avtal enligt de multilaterala f�rhandlingarna inom Uruguayrundan (1986-1994) [42]. Artikel 27.1 i TRIPS stadgar att patent skall meddelas f�r alla uppfinningar, s�v�l anordningar som f�rfaranden, p� alla teknikomr�den, f�rutsatt att de �r nya, har uppfinningsh�jd och kan tillgodog�ras industriellt. Vidare skall det, enligt TRIPS, g� att erh�lla och utnyttja patentskydd p� alla teknikomr�den utan diskriminering. Dessa principer b�r g�lla f�ljdenligt f�r datorrelaterade uppfinningar. [42] EGT L 336, 23.12.1994, s. 1.
rec7	sv	Enligt Konventionen om meddelande av europeiska patent (EPC), som undertecknades i M�nchen den 5 oktober 1973, och patentlagarna i medlemsstaterna kan datorprogram liksom uppt�ckter, vetenskapliga teorier, matematiska metoder, konstn�rliga skapelser, planer, regler eller metoder f�r intellektuell verksamhet, f�r spel eller f�r aff�rsverksamhet samt framl�gganden av information uttryckligen inte betraktas som uppfinningar och �r d�rf�r undantagna fr�n det patenterbara omr�det. Dessa undantag �r emellertid endast till�mpliga och ber�ttigade i den m�n en patentans�kan eller ett patent h�nf�r sig till s�dana f�rem�l eller verksamheter i sig, eftersom de n�mnda f�rem�len och verksamheterna i sig inte tillh�r n�got teknikomr�de.
rec7a0	sv	
rec7a	sv	
rec7b	sv	
rec8	sv	Patentskyddet till�ter uppfinnaren att nyttigg�ra sin kreativitet. Patentskyddet sl�r vakt om innovation i hela samh�llets intresse, men det f�r inte anv�ndas p� ett s�tt som motverkar konkurrens.
rec9	sv	Enligt r�dets direktiv 91/250/EEG av den 14 maj 1991 om r�ttsligt skydd f�r datorprogram [43] skall ett originellt datorprogram i alla dess uttrycksformer �tnjuta upphovsr�ttsligt skydd som litter�rt verk. Men id�er och principer som ligger bakom de olika detaljerna i ett datorprogram �r inte upphovsr�ttsligt skyddade. [43] EGT L 122, 17.5.1991 s. 42. Direktiv �ndrat genom direktiv 93/98/EEG (EGT L 290, 24.11.1993, s. 9.)
rec10	sv	F�r att en uppfinning skall anses vara patenterbar b�r den ha teknisk karakt�r och d�rmed tillh�ra ett teknikomr�de.
rec11	sv	�ven om datorrelaterade uppfinningar anses tillh�ra ett teknikomr�de b�r de, f�r att i likhet med vad som g�ller f�r uppfinningar i allm�nhet tillerk�nnas uppfinningsh�jd, utg�ra ett tekniskt bidrag till teknikens st�ndpunkt.
rec12	sv	Om en uppfinning s�ledes inte utg�r ett tekniskt bidrag till teknikens st�ndpunkt, vilket t.ex. �r fallet om bidraget saknar teknisk karakt�r, skall den anses sakna uppfinningsh�jd och d�rmed inte vara patenterbar.
rec13	sv	En specificerad �tg�rdssekvens som utf�rs i f�rbindelse med en anordning s�som en dator kan utg�ra ett tekniskt bidrag till teknikens st�ndpunkt och d�rigenom vara en patenterbar uppfinning. Men en algoritm som utformas utan h�nvisning till n�gon fysisk omgivning �r till sin natur icke-teknisk och kan d�rf�r inte vara en patenterbar uppfinning.
rec13a	sv	
rec13b	sv	
rec13c	sv	
rec13d	sv	
rec14	sv	F�r att inf�ra patentskydd f�r datorrelaterade uppfinningar kr�vs det inte n�got nytt regelverk som ers�ttning f�r de nationella patentlagarna. De nationella patentlagarna b�r bibeh�llas som huvudsaklig grund f�r patentskydd n�r det g�ller datorrelaterade uppfinningar, men de b�r anpassas eller kompletteras p� vissa punkter enligt vad som anges i detta direktiv.
rec15	sv	Direktivets inneh�ll b�r begr�nsas till vissa principer som b�r reglera uppfinningarnas patenterbarhet, varvid dessa principer s�rskilt b�r borga f�r att uppfinningar som tillh�r ett teknikomr�de och utg�r ett tekniskt bidrag g�r att patentskydda, samt omv�nt att uppfinningar som inte utg�r n�got tekniskt bidrag inte g�r att patentskydda.
rec16	sv	Den europeiska industrins konkurrensl�ge gentemot dess viktigaste handelspartner skulle st�rkas om r�dande olikheter i det r�ttsskydd som finns f�r datorrelaterade uppfinningar undanr�jdes och r�ttsl�get klargjordes.
rec17	sv	Detta direktiv skall inte inverka p� till�mpningen av konkurrensreglerna, s�rskilt artikel 81 och 82 i f�rdraget.
rec18	sv	Handlingar som �r till�tna enligt direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga eller dem om kretsm�nster p� halvledare eller varum�rken, skall inte p�verkas av det patentskydd f�r uppfinningar som g�ller inom detta direktivs r�ckvidd.
rec18a	sv	
rec19	sv	Eftersom m�len f�r f�rslaget, n�mligen att harmonisera nationella regler om datorrelaterade uppfinningar, inte i tillr�cklig utstr�ckning kan uppn�s av medlemsstaterna och d�rf�r, p� grund av den planerade �tg�rdens omfattning eller verkningar, b�ttre kan uppn�s p� gemenskapsniv�, f�r gemenskapen anta best�mmelser i kraft av subsidiaritetsprincipen i artikel 5 i f�rdraget. I �verensst�mmelse med proportionalitetsprincipen, som fastst�lls i den artikeln, g�r direktivet inte l�ngre �n vad som �r n�dv�ndigt f�r att uppn� dessa m�l. H�RIGENOM F�RESKRIVS F�LJANDE.
art1	sv	I detta direktiv fastst�lls de regler som skall g�lla patenterbarhet f�r datorrelaterade uppfinningar.
art2	sv	F�r detta direktivs �ndam�l g�ller f�ljande definitioner
art2a	sv	
art2b	sv	
art2ba	sv	
art2bb	sv	
art3	sv	Medlemsstaterna skall se till att en datorrelaterad uppfinning uppfattas som tillh�rande ett teknikomr�de.
art3a	sv	
art4	sv	1. Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny och har uppfinningsh�jd. 2. Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag. 3. Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samlade skyddsomf�ng, vari s�v�l tekniska som icke-tekniska k�nnetecken kan ing�, och teknikens st�ndpunkt.
art4_1	sv	
art4_2	sv	
art4_3	sv	
art4_3a	sv	
art4a	sv	
art4b	sv	
art5	sv	Medlemsstaterna skall se till att en datorrelaterad uppfinning kan patentskyddas s�som anordning, dvs. en programmerad dator, ett programmerat n�tverk eller n�gon annan programmerad anordning, eller s�som f�rfarande utf�rt av ovann�mnda dator, n�tverk eller anordning genom att en mjukvara k�rs p� anordningen.
art5_1	sv	
art5_1a	sv	
art5_1b	sv	
art5_1c	sv	
art5_1d	sv	
art6	sv	Handlingar som �r till�tna enligt direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga eller dem om kretsm�nster p� halvledare eller varum�rken, skall inte p�verkas av det patentskydd f�r uppfinningar som g�ller inom detta direktivs r�ckvidd.
art6a	sv	
art7	sv	Kommissionen skall f�lja hur datorrelaterade uppfinningar p�verkar innovation och konkurrens, b�de i Europa och internationellt, samt europeiskt n�ringsliv, �ven n�thandel.
art8	sv	Senast f�re [DATE (three years from the date specified in Article 9(1))] skall kommissionen rapportera till Europaparlamentet och r�det om a) hur datorrelaterade uppfinningar p�verkar de faktorer som n�mns i artikel 7, b) huruvida reglerna f�r bed�mning av patenterbarhet, n�rmare best�mt nyhet, uppfinningsh�jd och l�mpligt skyddsomf�ng f�r kraven, �r �ndam�lsenliga, och c) huruvida de medlemsstater som inte granskar nyhet och uppfinningsh�jd innan patent utf�rdas har haft sv�righeter och, om s� �r fallet, huruvida det kr�vs �tg�rder f�r att angripa dessa sv�righeter.
art8a	sv	
art8b	sv	
art8c	sv	
art8ca	sv	
art8cb	sv	
art8cc	sv	
art8cd	sv	
art8ce	sv	
art8cf	sv	
art8cg	sv	
art8_1a	sv	
art9	sv	1. Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast den och skall genast underr�tta kommissionen om detta. N�r en medlemsstat antar dessa best�mmelser skall de inneh�lla en h�nvisning till detta direktiv eller �tf�ljas av en s�dan h�nvisning n�r de offentligg�rs. N�rmare f�reskrifter om hur h�nvisningen skall g�ras skall varje medlemsstat sj�lv utf�rda. 2. Medlemsstaterna skall till kommissionen �verl�mna texterna till de best�mmelser i nationell lagstiftning som antas inom det omr�de som omfattas av detta direktiv.
art9_1	sv	
art9_2	sv	
art10	sv	Detta direktiv tr�der i kraft den tjugonde dagen efter det att det har offentliggjorts i Europeiska gemenskapernas officiella tidning.
art11	sv	Detta direktiv riktar sig till medlemsstaterna.
\.


--
-- Data for TOC entry 59 (OID 23671)
-- Name: am; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY am (id, lang, am) FROM stdin;
rec1	da	�ndring 1
rec5	da	�ndring 2
rec5a	da	�ndring 88
rec6	da	�ndring 31
rec7	da	�ndring 32/corr + 112
rec7a	da	�ndring 3
rec7b	da	�ndring 95
rec11	da	�ndring 84
rec12	da	�ndring 114 + 125
rec13	da	�ndring 34 + 115
rec13a	da	�ndring 85
rec13b	da	�ndring 7
rec13c	da	�ndring 8
rec13d	da	�ndring 9
rec14	da	�ndring 86
rec16	da	�ndring 11
rec17	da	�ndring 12
rec18	da	�ndring 13
rec18a	da	�ndring 75
art2a	da	�ndring 36 + 42 + 117
art2b	da	�ndring 107 + 69
art2ba	da	�ndring 55/rev. + 97 + 108
art2bb	da	�ndring 38 + 44 + 118
art3	da	�ndring 15
art3a	da	�ndring 45
art4_1	da	
art4_2	da	
art4_3	da	
art4_3a	da	
art4a	da	�ndring 17
art4b	da	�ndring 60
art5_1	da	�ndring 102 + 111
art5_1a	da	�ndring 72
art5_1b	da	�ndring 103 + 119
art5_1c	da	�ndring 104 + 120
art5_1d	da	
art6	da	�ndring 19
art6a	da	�ndring 76
art7	da	�ndring 71
art8a	da	�ndring 27
art8b	da	�ndring 92
art8ca	da	�ndring 23
art8cb	da	�ndring 24
art8cc	da	�ndring 25
art8cd	da	�ndring 26
art8ce	da	�ndring 81
art8cf	da	�ndring 89
art8cg	da	�ndring 93
art8_1a	da	�ndring 94
art9_1	da	�ndring 28
rec1	de	Ab�nderung 1
rec5	de	Ab�nderung 2
rec5a	de	Ab�nderung 88
rec6	de	Ab�nderung 31
rec7	de	Ab�nderungen 32 und 112
rec7a	de	Ab�nderung 3
rec7b	de	Ab�nderung 95
rec11	de	Ab�nderung 84
rec12	de	Ab�nderungen 114 und 125
rec13	de	Ab�nderungen 34 und 115
rec13a	de	Ab�nderung 85
rec13b	de	Ab�nderung 7
rec13c	de	Ab�nderung 8
rec13d	de	Ab�nderung 9
rec14	de	Ab�nderung 86
rec16	de	Ab�nderung 11
rec17	de	Ab�nderung 12
rec18	de	Ab�nderung 13
rec18a	de	Ab�nderung 75
art2a	de	Ab�nderungen 36, 42 und 117
art2b	de	Ab�nderungen 107 und 69
art2ba	de	Ab�nderungen 55/rev, 97 und 108
art2bb	de	Ab�nderungen 38, 44 und 118
art3	de	Ab�nderung 15
art3a	de	Ab�nderung 45
art4_1	de	
art4_2	de	
art4_3	de	
art4_3a	de	
art4a	de	Ab�nderung 17
art4b	de	Ab�nderung 60
art5_1	de	Ab�nderungen 102 und 111
art5_1a	de	Ab�nderung 72
art5_1b	de	Ab�nderungen 103 und 119
art5_1c	de	Ab�nderungen 104 und 120 /NumAm&gt;
art5_1d	de	
art6	de	Ab�nderung 19
art6a	de	Ab�nderung 76
art7	de	Ab�nderung NumAm&gt;71
art8a	de	Ab�nderung 27
art8b	de	Ab�nderung 92
art8ca	de	Ab�nderung 23
art8cb	de	Ab�nderung 24
art8cc	de	Ab�nderung 25
art8cd	de	Ab�nderung 26
art8ce	de	Ab�nderung 81
art8cf	de	Ab�nderung 89
art8cg	de	Ab�nderung 93
art8_1a	de	Ab�nderung 94
art9_1	de	
rec1	en	Amendment 1
rec5	en	Amendment 2
rec5a	en	Amendment 88
rec6	en	Amendment 31
rec7	en	Amendments 32 and 112
rec7a	en	Amendment 3
rec7b	en	Amendment 95
rec11	en	Amendment 84
rec12	en	Amendments 114 and 125
rec13	en	Amendments 34 and 115
rec13a	en	Amendment 85
rec13b	en	Amendment 7
rec13c	en	Amendment 8
rec13d	en	Amendment 9
rec14	en	Amendment 86
rec16	en	Amendment 11
rec17	en	Amendment 12
rec18	en	Amendment 13
rec18a	en	Amendment 75
art2a	en	Amendments 36, 42 and 117
art2b	en	Amendments 107 and 69
art2ba	en	Amendments 55/rev, 97 and 108
art2bb	en	Amendments 38, 44 and 118
art3	en	Amendment 15
art3a	en	Amendment 45
art4_1	en	
art4_2	en	
art4_3	en	
art4_3a	en	
art4a	en	Amendment 17
art4b	en	Amendment 60
art5_1	en	Amendments 102 and 111
art5_1a	en	Amendment 72
art5_1b	en	Amendments 103 and 119
art5_1c	en	Amendments 104 and 120
art5_1d	en	
art6	en	Amendment 19
art6a	en	Amendment 76
art7	en	Amendment 71
art8a	en	Amendment 27
art8b	en	Amendment 92
art8ca	en	Amendment 23
art8cb	en	Amendment 24
art8cc	en	Amendment 25
art8cd	en	Amendment 26
art8ce	en	Amendment 81
art8cf	en	Amendment 89
art8cg	en	Amendment 93
art8_1a	en	Amendment 94
art9_1	en	Amendment 28
rec1	es	Enmienda 1
rec5	es	Enmienda 2
rec5a	es	Enmienda 88
rec6	es	Enmienda 31
rec7	es	Enmiendas 32 y 112
rec7a	es	Enmienda 3
rec7b	es	Enmienda 95
rec11	es	Enmienda 84
rec12	es	Enmiendas 114 y 125
rec13	es	Enmiendas 34 y 115
rec13a	es	Enmienda 85
rec13b	es	Enmienda 7
rec13c	es	Enmienda 8
rec13d	es	Enmienda 9
rec14	es	Enmienda 86
rec16	es	Enmienda 11
rec17	es	Enmienda 12
rec18	es	Enmienda 13
rec18a	es	Enmienda 75
art2a	es	Enmiendas 36, 42 y 117
art2b	es	Enmiendas 107 y 69
art2ba	es	Enmiendas 55/rev, 97 y 108
art2bb	es	Enmiendas 38, 44 y 118
art3	es	Enmienda 15
art3a	es	Enmienda 45
art4_1	es	
art4_2	es	
art4_3	es	
art4_3a	es	
art4a	es	Enmienda 17
art4b	es	Enmienda 60
art5_1	es	Enmienda 102 y 111
art5_1a	es	Enmienda 72
art5_1b	es	Enmiendas 103 y 119
art5_1c	es	Enmiendas 104 y 120
art5_1d	es	
art6	es	Enmienda 19
art6a	es	Enmienda 76
art7	es	Enmienda 71
art8a	es	Enmienda 27
art8b	es	Enmienda 92
art8ca	es	Enmienda 23
art8cb	es	Enmienda 24
art8cc	es	Enmienda 25
art8cd	es	Enmienda 26
art8ce	es	Enmienda 81
art8cf	es	Enmienda 89
art8cg	es	Enmienda 93
art8_1a	es	Enmienda 94
art9_1	es	Enmienda 28
rec1	fi	Tarkistus 1
rec5	fi	Tarkistus 2
rec5a	fi	Tarkistus 88
rec6	fi	Tarkistus 31
rec7	fi	Tarkistukset 32 ja 112
rec7a	fi	Tarkistus 3
rec7b	fi	Tarkistus 95
rec11	fi	Tarkistus 84
rec12	fi	Tarkistukset 114 ja 125
rec13	fi	Tarkistukset 34 ja 115
rec13a	fi	Tarkistus 85
rec13b	fi	Tarkistus 7
rec13c	fi	Tarkistus 8
rec13d	fi	Tarkistus 9
rec14	fi	Tarkistus 86
rec16	fi	Tarkistus 11
rec17	fi	Tarkistus 12
rec18	fi	Tarkistus 13
rec18a	fi	Tarkistus 75
art2a	fi	Tarkistukset 36, 42 ja 117
art2b	fi	Tarkistukset 107 ja 69
art2ba	fi	Tarkistukset 55/rev, 97 ja 108
art2bb	fi	Tarkistukset 38, 44 ja 118
art3	fi	Tarkistus 15
art3a	fi	Tarkistus 45
art4_1	fi	
art4_2	fi	
art4_3	fi	
art4_3a	fi	
art4a	fi	Tarkistus 17
art4b	fi	Tarkistus 60
art5_1	fi	
art5_1a	fi	Tarkistus 72
art5_1b	fi	Tarkistukset 103 ja 119
art5_1c	fi	
art5_1d	fi	
art6	fi	Tarkistus 19
art6a	fi	Tarkistus 76
art7	fi	Tarkistus 71
art8a	fi	Tarkistus 27
art8b	fi	Tarkistus 92
art8ca	fi	Tarkistus 23
art8cb	fi	Tarkistus 24
art8cc	fi	Tarkistus 25
art8cd	fi	Tarkistus 26
art8ce	fi	Tarkistus 81
art8cf	fi	Tarkistus 89
art8cg	fi	Tarkistus 93
art8_1a	fi	Tarkistus 94
art9_1	fi	Tarkistus 28
rec1	fr	Amendement 1
rec5	fr	Amendement 2
rec5a	fr	Amendement 88
rec6	fr	Amendement 31
rec7	fr	Amendements 32 et 112
rec7a	fr	Amendement 3
rec7b	fr	Amendement 95
rec11	fr	Amendement 84
rec12	fr	Amendements 114 et 125
rec13	fr	Amendements 34 et 115
rec13a	fr	Amendement 85
rec13b	fr	Amendement 7
rec13c	fr	Amendement 8
rec13d	fr	Amendement 9
rec14	fr	Amendement 86
rec16	fr	Amendement 11
rec17	fr	Amendement 12
rec18	fr	Amendement 13
rec18a	fr	Amendement 75
art2a	fr	Amendements 36, 42 et 117
art2b	fr	Amendements 107 et 69
art2ba	fr	Amendements 55/r�v., 97 et 108
art2bb	fr	Amendements 38, 44 et 118
art3	fr	Amendement 15
art3a	fr	Amendement 45
art4_1	fr	
art4_2	fr	
art4_3	fr	
art4_3a	fr	
art4a	fr	Amendement 17
art4b	fr	Amendement 60
art5_1	fr	Amendements 102 et 111
art5_1a	fr	Amendement 72
art5_1b	fr	Amendements 103 et 119
art5_1c	fr	Amendements 104 et 120
art5_1d	fr	
art6	fr	Amendement 19
art6a	fr	Amendement 76
art7	fr	Amendement 71
art8a	fr	Amendement 27
art8b	fr	Amendement 92
art8ca	fr	Amendement 23
art8cb	fr	Amendement 24
art8cc	fr	Amendement 25
art8cd	fr	Amendement 26
art8ce	fr	Amendement 81
art8cf	fr	Amendement 89
art8cg	fr	Amendement 93
art8_1a	fr	Amendement 94
art9_1	fr	Amendement 28
rec1	it	Emendamento 1
rec5	it	Emendamento 88
rec5a	it	
rec6	it	Emendamento 31
rec7	it	Emendamenti 32 e 112
rec7a	it	Emendamento 3
rec7b	it	Emendamento 95
rec11	it	Emendamento 84
rec12	it	Emendamenti 114 e 125
rec13	it	Emendamenti 34 e 115
rec13a	it	Emendamento 85
rec13b	it	Emendamento 7
rec13c	it	Emendamento 8
rec13d	it	Emendamento 9
rec14	it	Emendamento 86
rec16	it	Emendamento 11
rec17	it	Emendamento 12
rec18	it	Emendamento 13
rec18a	it	Emendamento 75
art2a	it	Emendamento 36, 42 e 117
art2b	it	Emendamento 107 e 69
art2ba	it	Emendamento 55/riv., 97 e 108
art2bb	it	Emendamenti 38, 44 e 118
art3	it	Emendamento 15
art3a	it	Emendamento 45
art4_1	it	
art4_2	it	
art4_3	it	
art4_3a	it	
art4a	it	Emendamento 17
art4b	it	Emendamento 60
art5_1	it	Emendamenti 102 e 111
art5_1a	it	Emendamento 72
art5_1b	it	Emendamenti 103 e 119
art5_1c	it	Emendamenti 104 e 120
art5_1d	it	
art6	it	Emendamento 19
art6a	it	Emendamento 76
art7	it	Emendamento 71
art8a	it	Emendamento 27
art8b	it	Emendamento 92
art8ca	it	Emendamento 23
art8cb	it	Emendamento 24
art8cc	it	Emendamento 25
art8cd	it	Emendamento 26
art8ce	it	Emendamento 81
art8cf	it	Emendamento 89
art8cg	it	Emendamento 93
art8_1a	it	Emendamento 94
art9_1	it	Emendamento 28
rec1	nl	Amendement 1
rec5	nl	Amendement 2
rec5a	nl	Amendement 88
rec6	nl	Amendement 31
rec7	nl	Amendementen 32 en 112
rec7a	nl	Amendement 95
rec7b	nl	Amendement 3
rec11	nl	Amendement 84
rec12	nl	Amendementen 114 en 125
rec13	nl	Amendementen 34 en 115
rec13a	nl	Amendement 85
rec13b	nl	Amendement 7
rec13c	nl	Amendement 8
rec13d	nl	Amendement 9
rec14	nl	Amendement 86
rec16	nl	Amendement 11
rec17	nl	Amendement 12
rec18	nl	Amendement 13
rec18a	nl	Amendement 75
art2a	nl	Amendementen 36, 42 en 117
art2b	nl	Amendementen 107 en 69
art2ba	nl	Amendementen 55/rev, 97 en 108
art2bb	nl	Amendementen 38, 44 en 118
art3	nl	Amendement 15
art3a	nl	Amendement 45
art4_1	nl	
art4_2	nl	
art4_3	nl	
art4_3a	nl	
art4a	nl	Amendement 17
art4b	nl	Amendement 60
art5_1	nl	Amendementen 102 en 111
art5_1a	nl	Amendement 72
art5_1b	nl	
art5_1c	nl	Amendementen 104 en 120
art5_1d	nl	
art6	nl	Amendement 19
art6a	nl	Amendement 76
art7	nl	Amendement 71
art8a	nl	Amendement 27
art8b	nl	Amendement 92
art8ca	nl	Amendement 93/CORR
art8cb	nl	Amendement 24
art8cc	nl	Amendement 25
art8cd	nl	Amendement 26
art8ce	nl	Amendement 81
art8cf	nl	Amendement 89
art8cg	nl	
art8_1a	nl	Amendement 94/CORR
art9_1	nl	Amendement 28
rec1	pt	Altera��o 1
rec5	pt	Altera��o 2
rec5a	pt	Altera��o 88
rec6	pt	Altera��o 31
rec7	pt	Altera��es 32 e 112
rec7a	pt	Altera��o 3
rec7b	pt	Altera��o 95
rec11	pt	Altera��o 84
rec12	pt	Altera��es 114 e 125
rec13	pt	Altera��es 34 e 115
rec13a	pt	Altera��o 85
rec13b	pt	Altera��o 7
rec13c	pt	Altera��o 8
rec13d	pt	Altera��o 9
rec14	pt	Altera��o 86
rec16	pt	Altera��o 11
rec17	pt	Altera��o 12
rec18	pt	Altera��o 13
rec18a	pt	Altera��o 75
art2a	pt	Altera��es 36, 42 e 117
art2b	pt	Altera��es 69 e 107
art2ba	pt	Altera��es 55/rev, 97 e 108
art2bb	pt	Altera��es 38, 44 e 118
art3	pt	Altera��o 15
art3a	pt	Altera��o 45
art4_1	pt	
art4_2	pt	
art4_3	pt	
art4_3a	pt	
art4a	pt	Altera��o 17
art4b	pt	Altera��o 60
art5_1	pt	Altera��es 102 e 111
art5_1a	pt	Altera��o 72
art5_1b	pt	Altera��es 103 e 119
art5_1c	pt	Altera��es 104 e 120
art5_1d	pt	
art6	pt	Altera��o 19
art6a	pt	Altera��o 76
art7	pt	Altera��o 71
art8a	pt	Altera��o 27
art8b	pt	Altera��o 92
art8ca	pt	Altera��o 23
art8cb	pt	Altera��o 24
art8cc	pt	Altera��o 25
art8cd	pt	Altera��o 26
art8ce	pt	Altera��o 81
art8cf	pt	Altera��o 89
art8cg	pt	Altera��o 93
art8_1a	pt	Altera��o 94
art9_1	pt	Altera��o 28
rec1	sv	�ndring 1
rec5	sv	�ndring 2
rec5a	sv	�ndring 88
rec6	sv	�ndring 31
rec7	sv	�ndring 32 och 112
rec7a	sv	�ndring 3
rec7b	sv	�ndring 95
rec11	sv	�ndring 84
rec12	sv	�ndring 114 och 125 /NumAm&gt;
rec13	sv	�ndring 34 och 115
rec13a	sv	�ndring 85
rec13b	sv	�ndring 7
rec13c	sv	�ndring 8
rec13d	sv	�ndring 9
rec14	sv	�ndring 86
rec16	sv	�ndring 11
rec17	sv	�ndring 12
rec18	sv	�ndring 13
rec18a	sv	�ndring 75
art2a	sv	�ndring 36, 42 och 117
art2b	sv	�ndring 107 och 69
art2ba	sv	�ndring 55/rev, 97 och 108
art2bb	sv	�ndring 38, 44 och 118
art3	sv	�ndring 15
art3a	sv	�ndring 45
art4_1	sv	
art4_2	sv	
art4_3	sv	
art4_3a	sv	
art4a	sv	�ndring 17
art4b	sv	�ndring 60
art5_1	sv	�ndring 102 och 111
art5_1a	sv	�ndring 72
art5_1b	sv	�ndring 103 och 119
art5_1c	sv	�ndring 104 och 120
art5_1d	sv	
art6	sv	�ndring 19
art6a	sv	�ndring 76
art7	sv	�ndring 71
art8a	sv	�ndring 27
art8b	sv	�ndring 92
art8ca	sv	�ndring 23
art8cb	sv	�ndring 24
art8cc	sv	�ndring 25
art8cd	sv	�ndring 26
art8ce	sv	�ndring 81
art8cf	sv	�ndring 89
art8cg	sv	�ndring 93
art8_1a	sv	�ndring 94
art9_1	sv	�ndring 28
\.


--
-- Data for TOC entry 60 (OID 23679)
-- Name: auth; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY auth (id, lang, auth) FROM stdin;
rec1	da	JURI
rec5	da	JURI
rec5a	da	PPE-DE (Wuermelung)
rec6	da	EDD
rec7	da	EDD, Kauppi
rec7a	da	JURI
rec7b	da	ELDR
rec11	da	PPE-DE (Wuermeling)
rec12	da	Kauppi, GUE/NGL
rec13	da	EDD, Kauppi
rec13a	da	PPE-DE (Wuermeling)
rec13b	da	JURI
rec13c	da	JURI
rec13d	da	JURI
rec14	da	PPE-DE (Wuermeling)
rec16	da	JURI
rec17	da	JURI
rec18	da	JURI
rec18a	da	PSE (Ortega)
art2a	da	EDD, UEN, Kauppi
art2b	da	Kauppi; PSE(Medina)
art2ba	da	VERD+GUE, ELDR, Kauppi
art2bb	da	EDD, UEN, Kauppi
art3	da	JURI
art3a	da	UEN
art4_1	da	JURI
art4_2	da	JURI
art4_3	da	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	da	PSE
art4a	da	JURI
art4b	da	VERD+GUE
art5_1	da	ELDR, Kauppi
art5_1a	da	PSE
art5_1b	da	ELDR, Kauppi
art5_1c	da	ELDR, Kauppi
art5_1d	da	ELDR, Kauppi
art6	da	JURI
art6a	da	ITRE; PSE
art7	da	PSE (part)
art8a	da	JURI
art8b	da	PPE-DE (Thyssen)
art8ca	da	JURI
art8cb	da	JURI
art8cc	da	JURI
art8cd	da	JURI
art8ce	da	VERT+GUE
art8cf	da	PPE-DE (Wuermeling)
art8cg	da	PPE-DE (Thyssen)
art8_1a	da	PPE-DE (Thyssen)
art9_1	da	JURI
rec1	de	JURI
rec5	de	JURI
rec5a	de	PPE-DE (Wuermelung)
rec6	de	EDD
rec7	de	EDD, Kauppi
rec7a	de	JURI
rec7b	de	ELDR
rec11	de	PPE-DE (Wuermeling)
rec12	de	Kauppi, GUE/NGL
rec13	de	EDD, Kauppi
rec13a	de	PPE-DE (Wuermeling)
rec13b	de	JURI
rec13c	de	JURI
rec13d	de	JURI
rec14	de	PPE-DE (Wuermeling)
rec16	de	JURI
rec17	de	JURI
rec18	de	JURI
rec18a	de	PSE (Ortega)
art2a	de	EDD, UEN, Kauppi
art2b	de	Kauppi; PSE(Medina)
art2ba	de	VERD+GUE, ELDR, Kauppi
art2bb	de	EDD, UEN, Kauppi
art3	de	JURI
art3a	de	UEN
art4_1	de	JURI
art4_2	de	JURI
art4_3	de	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	de	PSE
art4a	de	JURI
art4b	de	VERD+GUE
art5_1	de	ELDR, Kauppi
art5_1a	de	PSE
art5_1b	de	ELDR, Kauppi
art5_1c	de	ELDR, Kauppi
art5_1d	de	ELDR, Kauppi
art6	de	JURI
art6a	de	ITRE; PSE
art7	de	PSE (part)
art8a	de	JURI
art8b	de	PPE-DE (Thyssen)
art8ca	de	JURI
art8cb	de	JURI
art8cc	de	JURI
art8cd	de	JURI
art8ce	de	VERT+GUE
art8cf	de	PPE-DE (Wuermeling)
art8cg	de	PPE-DE (Thyssen)
art8_1a	de	PPE-DE (Thyssen)
art9_1	de	JURI
rec1	en	JURI
rec5	en	JURI
rec5a	en	PPE-DE (Wuermelung)
rec6	en	EDD
rec7	en	EDD, Kauppi
rec7a	en	JURI
rec7b	en	ELDR
rec11	en	PPE-DE (Wuermeling)
rec12	en	Kauppi, GUE/NGL
rec13	en	EDD, Kauppi
rec13a	en	PPE-DE (Wuermeling)
rec13b	en	JURI
rec13c	en	JURI
rec13d	en	JURI
rec14	en	PPE-DE (Wuermeling)
rec16	en	JURI
rec17	en	JURI
rec18	en	JURI
rec18a	en	PSE (Ortega)
art2a	en	EDD, UEN, Kauppi
art2b	en	Kauppi; PSE(Medina)
art2ba	en	VERD+GUE, ELDR, Kauppi
art2bb	en	EDD, UEN, Kauppi
art3	en	JURI
art3a	en	UEN
art4_1	en	JURI
art4_2	en	JURI
art4_3	en	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	en	PSE
art4a	en	JURI
art4b	en	VERD+GUE
art5_1	en	ELDR, Kauppi
art5_1a	en	PSE
art5_1b	en	ELDR, Kauppi
art5_1c	en	ELDR, Kauppi
art5_1d	en	ELDR, Kauppi
art6	en	JURI
art6a	en	ITRE; PSE
art7	en	PSE (part)
art8a	en	JURI
art8b	en	PPE-DE (Thyssen)
art8ca	en	JURI
art8cb	en	JURI
art8cc	en	JURI
art8cd	en	JURI
art8ce	en	VERT+GUE
art8cf	en	PPE-DE (Wuermeling)
art8cg	en	PPE-DE (Thyssen)
art8_1a	en	PPE-DE (Thyssen)
art9_1	en	JURI
rec1	es	JURI
rec5	es	JURI
rec5a	es	PPE-DE (Wuermelung)
rec6	es	EDD
rec7	es	EDD, Kauppi
rec7a	es	JURI
rec7b	es	ELDR
rec11	es	PPE-DE (Wuermeling)
rec12	es	Kauppi, GUE/NGL
rec13	es	EDD, Kauppi
rec13a	es	PPE-DE (Wuermeling)
rec13b	es	JURI
rec13c	es	JURI
rec13d	es	JURI
rec14	es	PPE-DE (Wuermeling)
rec16	es	JURI
rec17	es	JURI
rec18	es	JURI
rec18a	es	PSE (Ortega)
art2a	es	EDD, UEN, Kauppi
art2b	es	Kauppi; PSE(Medina)
art2ba	es	VERD+GUE, ELDR, Kauppi
art2bb	es	EDD, UEN, Kauppi
art3	es	JURI
art3a	es	UEN
art4_1	es	JURI
art4_2	es	JURI
art4_3	es	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	es	PSE
art4a	es	JURI
art4b	es	VERD+GUE
art5_1	es	ELDR, Kauppi
art5_1a	es	PSE
art5_1b	es	ELDR, Kauppi
art5_1c	es	ELDR, Kauppi
art5_1d	es	ELDR, Kauppi
art6	es	JURI
art6a	es	ITRE; PSE
art7	es	PSE (part)
art8a	es	JURI
art8b	es	PPE-DE (Thyssen)
art8ca	es	JURI
art8cb	es	JURI
art8cc	es	JURI
art8cd	es	JURI
art8ce	es	VERT+GUE
art8cf	es	PPE-DE (Wuermeling)
art8cg	es	PPE-DE (Thyssen)
art8_1a	es	PPE-DE (Thyssen)
art9_1	es	JURI
rec1	fi	JURI
rec5	fi	JURI
rec5a	fi	PPE-DE (Wuermelung)
rec6	fi	EDD
rec7	fi	EDD, Kauppi
rec7a	fi	JURI
rec7b	fi	ELDR
rec11	fi	PPE-DE (Wuermeling)
rec12	fi	Kauppi, GUE/NGL
rec13	fi	EDD, Kauppi
rec13a	fi	PPE-DE (Wuermeling)
rec13b	fi	JURI
rec13c	fi	JURI
rec13d	fi	JURI
rec14	fi	PPE-DE (Wuermeling)
rec16	fi	JURI
rec17	fi	JURI
rec18	fi	JURI
rec18a	fi	PSE (Ortega)
art2a	fi	EDD, UEN, Kauppi
art2b	fi	Kauppi; PSE(Medina)
art2ba	fi	VERD+GUE, ELDR, Kauppi
art2bb	fi	EDD, UEN, Kauppi
art3	fi	JURI
art3a	fi	UEN
art4_1	fi	JURI
art4_2	fi	JURI
art4_3	fi	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	fi	PSE
art4a	fi	JURI
art4b	fi	VERD+GUE
art5_1	fi	ELDR, Kauppi
art5_1a	fi	PSE
art5_1b	fi	ELDR, Kauppi
art5_1c	fi	ELDR, Kauppi
art5_1d	fi	ELDR, Kauppi
art6	fi	JURI
art6a	fi	ITRE; PSE
art7	fi	PSE (part)
art8a	fi	JURI
art8b	fi	PPE-DE (Thyssen)
art8ca	fi	JURI
art8cb	fi	JURI
art8cc	fi	JURI
art8cd	fi	JURI
art8ce	fi	VERT+GUE
art8cf	fi	PPE-DE (Wuermeling)
art8cg	fi	PPE-DE (Thyssen)
art8_1a	fi	PPE-DE (Thyssen)
art9_1	fi	JURI
rec1	fr	JURI
rec5	fr	JURI
rec5a	fr	PPE-DE (Wuermelung)
rec6	fr	EDD
rec7	fr	EDD, Kauppi
rec7a	fr	JURI
rec7b	fr	ELDR
rec11	fr	PPE-DE (Wuermeling)
rec12	fr	Kauppi, GUE/NGL
rec13	fr	EDD, Kauppi
rec13a	fr	PPE-DE (Wuermeling)
rec13b	fr	JURI
rec13c	fr	JURI
rec13d	fr	JURI
rec14	fr	PPE-DE (Wuermeling)
rec16	fr	JURI
rec17	fr	JURI
rec18	fr	JURI
rec18a	fr	PSE (Ortega)
art2a	fr	EDD, UEN, Kauppi
art2b	fr	Kauppi; PSE(Medina)
art2ba	fr	VERD+GUE, ELDR, Kauppi
art2bb	fr	EDD, UEN, Kauppi
art3	fr	JURI
art3a	fr	UEN
art4_1	fr	JURI
art4_2	fr	JURI
art4_3	fr	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	fr	PSE
art4a	fr	JURI
art4b	fr	VERD+GUE
art5_1	fr	ELDR, Kauppi
art5_1a	fr	PSE
art5_1b	fr	ELDR, Kauppi
art5_1c	fr	ELDR, Kauppi
art5_1d	fr	ELDR, Kauppi
art6	fr	JURI
art6a	fr	ITRE; PSE
art7	fr	PSE (part)
art8a	fr	JURI
art8b	fr	PPE-DE (Thyssen)
art8ca	fr	JURI
art8cb	fr	JURI
art8cc	fr	JURI
art8cd	fr	JURI
art8ce	fr	VERT+GUE
art8cf	fr	PPE-DE (Wuermeling)
art8cg	fr	PPE-DE (Thyssen)
art8_1a	fr	PPE-DE (Thyssen)
art9_1	fr	JURI
rec1	it	JURI
rec5	it	JURI
rec5a	it	PPE-DE (Wuermelung)
rec6	it	EDD
rec7	it	EDD, Kauppi
rec7a	it	JURI
rec7b	it	ELDR
rec11	it	PPE-DE (Wuermeling)
rec12	it	Kauppi, GUE/NGL
rec13	it	EDD, Kauppi
rec13a	it	PPE-DE (Wuermeling)
rec13b	it	JURI
rec13c	it	JURI
rec13d	it	JURI
rec14	it	PPE-DE (Wuermeling)
rec16	it	JURI
rec17	it	JURI
rec18	it	JURI
rec18a	it	PSE (Ortega)
art2a	it	EDD, UEN, Kauppi
art2b	it	Kauppi; PSE(Medina)
art2ba	it	VERD+GUE, ELDR, Kauppi
art2bb	it	EDD, UEN, Kauppi
art3	it	JURI
art3a	it	UEN
art4_1	it	JURI
art4_2	it	JURI
art4_3	it	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	it	PSE
art4a	it	JURI
art4b	it	VERD+GUE
art5_1	it	ELDR, Kauppi
art5_1a	it	PSE
art5_1b	it	ELDR, Kauppi
art5_1c	it	ELDR, Kauppi
art5_1d	it	ELDR, Kauppi
art6	it	JURI
art6a	it	ITRE; PSE
art7	it	PSE (part)
art8a	it	JURI
art8b	it	PPE-DE (Thyssen)
art8ca	it	JURI
art8cb	it	JURI
art8cc	it	JURI
art8cd	it	JURI
art8ce	it	VERT+GUE
art8cf	it	PPE-DE (Wuermeling)
art8cg	it	PPE-DE (Thyssen)
art8_1a	it	PPE-DE (Thyssen)
art9_1	it	JURI
rec1	nl	JURI
rec5	nl	JURI
rec5a	nl	PPE-DE (Wuermelung)
rec6	nl	EDD
rec7	nl	EDD, Kauppi
rec7a	nl	JURI
rec7b	nl	ELDR
rec11	nl	PPE-DE (Wuermeling)
rec12	nl	Kauppi, GUE/NGL
rec13	nl	EDD, Kauppi
rec13a	nl	PPE-DE (Wuermeling)
rec13b	nl	JURI
rec13c	nl	JURI
rec13d	nl	JURI
rec14	nl	PPE-DE (Wuermeling)
rec16	nl	JURI
rec17	nl	JURI
rec18	nl	JURI
rec18a	nl	PSE (Ortega)
art2a	nl	EDD, UEN, Kauppi
art2b	nl	Kauppi; PSE(Medina)
art2ba	nl	VERD+GUE, ELDR, Kauppi
art2bb	nl	EDD, UEN, Kauppi
art3	nl	JURI
art3a	nl	UEN
art4_1	nl	JURI
art4_2	nl	JURI
art4_3	nl	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	nl	PSE
art4a	nl	JURI
art4b	nl	VERD+GUE
art5_1	nl	ELDR, Kauppi
art5_1a	nl	PSE
art5_1b	nl	ELDR, Kauppi
art5_1c	nl	ELDR, Kauppi
art5_1d	nl	ELDR, Kauppi
art6	nl	JURI
art6a	nl	ITRE; PSE
art7	nl	PSE (part)
art8a	nl	JURI
art8b	nl	PPE-DE (Thyssen)
art8ca	nl	JURI
art8cb	nl	JURI
art8cc	nl	JURI
art8cd	nl	JURI
art8ce	nl	VERT+GUE
art8cf	nl	PPE-DE (Wuermeling)
art8cg	nl	PPE-DE (Thyssen)
art8_1a	nl	PPE-DE (Thyssen)
art9_1	nl	JURI
rec1	pt	JURI
rec5	pt	JURI
rec5a	pt	PPE-DE (Wuermelung)
rec6	pt	EDD
rec7	pt	EDD, Kauppi
rec7a	pt	JURI
rec7b	pt	ELDR
rec11	pt	PPE-DE (Wuermeling)
rec12	pt	Kauppi, GUE/NGL
rec13	pt	EDD, Kauppi
rec13a	pt	PPE-DE (Wuermeling)
rec13b	pt	JURI
rec13c	pt	JURI
rec13d	pt	JURI
rec14	pt	PPE-DE (Wuermeling)
rec16	pt	JURI
rec17	pt	JURI
rec18	pt	JURI
rec18a	pt	PSE (Ortega)
art2a	pt	EDD, UEN, Kauppi
art2b	pt	Kauppi; PSE(Medina)
art2ba	pt	VERD+GUE, ELDR, Kauppi
art2bb	pt	EDD, UEN, Kauppi
art3	pt	JURI
art3a	pt	UEN
art4_1	pt	JURI
art4_2	pt	JURI
art4_3	pt	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	pt	PSE
art4a	pt	JURI
art4b	pt	VERD+GUE
art5_1	pt	ELDR, Kauppi
art5_1a	pt	PSE
art5_1b	pt	ELDR, Kauppi
art5_1c	pt	ELDR, Kauppi
art5_1d	pt	ELDR, Kauppi
art6	pt	JURI
art6a	pt	ITRE; PSE
art7	pt	PSE (part)
art8a	pt	JURI
art8b	pt	PPE-DE (Thyssen)
art8ca	pt	JURI
art8cb	pt	JURI
art8cc	pt	JURI
art8cd	pt	JURI
art8ce	pt	VERT+GUE
art8cf	pt	PPE-DE (Wuermeling)
art8cg	pt	PPE-DE (Thyssen)
art8_1a	pt	PPE-DE (Thyssen)
art9_1	pt	JURI
rec1	sv	JURI
rec5	sv	JURI
rec5a	sv	PPE-DE (Wuermelung)
rec6	sv	EDD
rec7	sv	EDD, Kauppi
rec7a	sv	JURI
rec7b	sv	ELDR
rec11	sv	PPE-DE (Wuermeling)
rec12	sv	Kauppi, GUE/NGL
rec13	sv	EDD, Kauppi
rec13a	sv	PPE-DE (Wuermeling)
rec13b	sv	JURI
rec13c	sv	JURI
rec13d	sv	JURI
rec14	sv	PPE-DE (Wuermeling)
rec16	sv	JURI
rec17	sv	JURI
rec18	sv	JURI
rec18a	sv	PSE (Ortega)
art2a	sv	EDD, UEN, Kauppi
art2b	sv	Kauppi; PSE(Medina)
art2ba	sv	VERD+GUE, ELDR, Kauppi
art2bb	sv	EDD, UEN, Kauppi
art3	sv	JURI
art3a	sv	UEN
art4_1	sv	JURI
art4_2	sv	JURI
art4_3	sv	JURI; ELDR; VERD+GUE, ELDR, Kauppi
art4_3a	sv	PSE
art4a	sv	JURI
art4b	sv	VERD+GUE
art5_1	sv	ELDR, Kauppi
art5_1a	sv	PSE
art5_1b	sv	ELDR, Kauppi
art5_1c	sv	ELDR, Kauppi
art5_1d	sv	ELDR, Kauppi
art6	sv	JURI
art6a	sv	ITRE; PSE
art7	sv	PSE (part)
art8a	sv	JURI
art8b	sv	PPE-DE (Thyssen)
art8ca	sv	JURI
art8cb	sv	JURI
art8cc	sv	JURI
art8cd	sv	JURI
art8ce	sv	VERT+GUE
art8cf	sv	PPE-DE (Wuermeling)
art8cg	sv	PPE-DE (Thyssen)
art8_1a	sv	PPE-DE (Thyssen)
art9_1	sv	JURI
\.


--
-- Data for TOC entry 61 (OID 23688)
-- Name: cm; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cm (id, lang, cm) FROM stdin;
rec1	da	Gennemf�relsen af det indre marked indeb�rer, at hindringer for den frie bev�gelighed og konkurrenceforvridninger fjernes, og at der samtidig skabes et gunstigt klima for innovation og investeringer. I den forbindelse er beskyttelsen af opfindelser ved hj�lp af patenter en v�- sentlig betingelse for et vellykket indre marked. En effektiv og harmoniseret beskyttelse af computer-implementerede opfindelser i alle medlemsstater er v�sentlig for at opretholde og fremme investeringer p� dette omr�de.
rec2	da	Der er forskel p� beskyttelsen af computer-implementerede opfindelser i de forskellige med- lemsstaters administrative praksis og retspraksis. Forskellene kan skabe handelshindringer og dermed forhindre det indre marked i at fungere efter hensigten.
rec3	da	De forskelle, der er opst�et, kan blive endnu st�rre, efterh�nden som medlemsstaterne indf�rer nye og forskellige former for administrativ praksis, og de nationale domstoles retspraksis i forbindelse med fortolkningen af den eksisterende lovgivning udvikler sig i forskellig retning.
rec4	da	Den stadig st�rre udbredelse og anvendelse af edb-programmer p� alle teknologiske omr�der og deres verdensomsp�ndende udbredelse via internettet er en kritisk faktor for teknologisk innovation. Det er derfor n�dvendigt at sikre et optimalt klima for udviklere og brugere af edb-programmer i F�llesskabet.
rec5	da	Medlemsstaternes fortolkning af lovbestemmelserne b�r derfor harmoniseres, og lovgivnin- gen vedr�rende computer-implementerede opfindelsers patenterbarhed b�r g�res mere gen- nemsigtig. Den juridiske sikkerhed, der herved opn�s, skulle g�re det muligt for virksomhe- derne at f� det fulde udbytte af patenter p� computer-implementerede opfindelser og udg�re et incitament til investering og innovation.
rec6	da	F�llesskabet og dets medlemsstater er bundet af aftalen om handelsrelaterede intellektuelle ejendomsrettigheder (TRIPS), der blev godkendt ved R�dets afg�relse 94/800/EF af 22. de- cember 1994 om indg�else p� Det Europ�iske F�llesskabs vegne af de aftaler, der er resulta- tet af de multilaterale forhandlinger i Uruguay-rundens regi (1986-1994), for s� vidt ang�r de omr�der, der h�rer under F�llesskabets kompetence5. I henhold til artikel 27, stk. 1, i TRIPS- aftalen skal alle opfindelser p� alle teknologiske omr�der, b�de produkter og processer, kunne patenteres, forudsat at de er nye, har opfindelsesh�jde og kan anvendes industrielt. I henhold til TRIPS-aftalen skal det desuden v�re muligt at opn� patentrettigheder og anvende disse uden forskelsbehandling p� grund af teknologisk omr�de. Disse principper b�r f�lgelig finde anvendelse p� computer-implementerede opfindelser.
rec7	da	I henhold til konventionen om meddelelse af europ�iske patenter, der blev undertegnet i M�nchen den 5. oktober 1973, og medlemsstaternes patentlove er edb-programmer s�vel som opdagelser, videnskabelige teorier, matematiske metoder, �stetiske frembringelser, planer, regler og metoder for intellektuel virksomhed, for spil eller for erhvervsvirksomhed (forret- ningskoncepter) og freml�ggelse af information udtrykkeligt ikke betragtet som opfindelser og kan derfor ikke patenteres. Disse undtagelser g�lder dog kun og er kun berettigede i det omfang, hvor en patentans�gning eller et patent vedr�rer disse genstande eller aktiviteter som s�dan, fordi de som s�danne ikke henh�rer under et teknologisk omr�de.
rec7a	da	Et edb-program som s�dant, navnlig et edb-program udtrykt i kildekode eller objektkode eller i en anden form, kan derfor ikke udg�re en patenterbar opfindelse.
rec8	da	Patentbeskyttelse giver innovatorer mulighed for at drage fordel af deres kreativitet. Patent- rettigheder beskytter innovation i samfundets interesse som helhed
rec9	da	I henhold til R�dets direktiv 91/250/EF af 14. maj 1991 om retlig beskyttelse af edb-program- mer6 beskyttes enhver form, hvori et originalt edb-program udtrykkes, ophavsretligt som et litter�rt v�rk. Men de id�er og principper, der ligger til grund for de enkelte elementer i et edb-program, beskyttes ikke ophavsretligt.
rec10	da	
rec11	da	Det er en betingelse for opfindelser i almindelighed, at de for at have opfindelsesh�jde yder et bidrag til det aktuelle tekniske niveau.
rec12	da	Selv om en computer-implementeret opfindelse betragtes som henh�rende under et teknolo- gisk omr�de, har den ikke opfindelsesh�jde og er derfor ikke patenterbar, hvis den ikke yder et bidrag til det aktuelle tekniske niveau, hvilket er tilf�ldet, hvis det specifikke bidrag ikke er af teknisk karakter7.
rec13	da	En specificeret procedure eller sekvens af handlinger, som udf�res ved hj�lp af en maskine som f.eks. en computer, kan yde et teknisk bidrag til det aktuelle tekniske niveau og dermed udg�re en patenterbar opfindelse.
rec13a	da	Den blotte implementering af en metode, der ellers ikke er patenterbar, p� en maskine som f.eks. en computer, er dog ikke i sig selv nok til, at man kan sige, at der ydes et teknisk bidrag. Et computer-implementeret forretningskoncept eller en anden metode, hvor det eneste bidrag til det aktuelle niveau er ikke-teknisk, er s�ledes ikke en patenterbar opfindelse.
rec13b	da	Hvis bidraget til det aktuelle tekniske niveau udelukkende vedr�rer en ikke-patenterbar gen- stand, er der heller ikke tale om en patenterbar opfindelse, uanset hvordan genstanden pr�- senteres i kravene.
rec13c	da	Desuden er en algoritme, der er defineret uafh�ngigt af et fysisk milj�, grundl�ggende af ikke-teknisk karakter og kan derfor ikke udg�re en patenterbar opfindelse.
rec14	da	Retlig beskyttelse af computer-implementerede opfindelser b�r ikke kr�ve indf�relse af en s�rlig lovgivning til erstatning af medlemsstaternes patentlovgivning. Det prim�re grundlag for retlig beskyttelse af computer-implementerede opfindelser b�r fortsat v�re medlemssta- ternes patentlovgivning, som b�r tilpasses og suppleres p� en r�kke konkrete punkter som an- f�rt i dette direktiv.
rec15	da	Direktivet b�r begr�nses til fastl�ggelsen af visse principper, s�ledes som de finder anven- delse p� patenterbarheden af s�danne opfindelser, idet de navnlig skal sikre, at opfindelser, der henh�rer under et teknologisk omr�de og yder et teknisk bidrag, kan opn� beskyttelse, og omvendt, at opfindelser, der ikke yder et teknisk bidrag, ikke kan.
rec16	da	EU's erhvervslivs konkurrenceevne i forhold til EU's vigtigste handelspartnere vil blive for- bedret, hvis de nuv�rende forskelle i den retlige beskyttelse af computer-implementerede op- findelser fjernes og retstilstanden bliver gennemsigtig.
rec17	da	Dette direktiv ber�rer ikke anvendelsen af konkurrencereglerne, herunder is�r traktatens arti- kel 81 og 82.
rec18	da	Handlinger, der er tilladt i henhold til direktiv 91/250/E�F om retlig beskyttelse af edb-pro- grammer ved ophavsret, herunder is�r bestemmelserne om dekompilering og interoperabili- tet, og bestemmelserne om halvlederes topografi og varem�rker ber�res ikke af den beskyt- telse, der ved hj�lp af patenter gives opfindelser, som er omfattet af dette direktiv.
rec19	da	M�lene for dette direktiv, som er at harmonisere de nationale bestemmelser om computer- implementerede opfindelser, kan ikke i tilstr�kkelig grad opfyldes af medlemsstaterne og kan derfor p� grund af direktivets omfang eller virkninger bedre gennemf�res p� f�llesskabsplan. F�llesskabet kan derfor tr�ffe foranstaltninger i overensstemmelse med subsidiaritetsprincip- pet, jf. traktatens artikel 5. I overensstemmelse med proportionalitetsprincippet, jf. n�vnte ar- tikel, g�r direktivet ikke ud over, hvad der er n�dvendigt for at n� disse m�l �
art1	da	
art2	da	Med henblik p� anvendelsen af dette direktiv g�lder f�lgende definitioner
art3	da	- Udg�r -8
art4	da	For at en computer-implementeret opfindelse kan v�re patenterbar, skal den v�re ny, have opfin- delsesh�jde og kunne anvendes industrielt. Det er en betingelse for at have opfindelsesh�jde, at en computer-implementeret opfindelse yder et teknisk bidrag.
art4a	da	En computer-implementeret opfindelse anses ikke for at yde et teknisk bidrag, blot fordi den inde- b�rer anvendelse af en computer eller en anden maskine. Opfindelser, som omfatter edb-program- mer, der implementerer forretningskoncepter eller matematiske eller andre metoder, og som ikke frembringer en teknisk effekt ud over den normale fysiske interaktion mellem et program og com- puteren, computernettet eller en anden programmerbar maskine, som de k�res p�, er derfor ikke patenterbare.
art5	da	1. Medlemsstaterne sikrer, at der kan ans�ges om patent p� en computer-implementeret opfin- delse som et produkt, dvs. som en programmeret computer, et programmeret computernet el- ler en anden programmeret maskine, eller som en proces, der udf�res af computeren, compu- ternettet eller maskinen ved k�rsel af software. 2. Der kan ikke g�res krav p� et edb-program, enten alene eller p� et b�remedium, medmindre der med programmet, n�r det er indl�st i og k�res p� en computer, et programmeret compu- ternet eller en anden programmerbar maskine, aktiveres et produkt eller en proces, som der g�res krav p� i samme patentans�gning som omhandlet i stk. 1.
art6	da	De rettigheder, som patenter p� opfindelser, der er omfattet af dette direktiv, giver, ber�rer ikke handlinger, der er tilladt i henhold til direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r dette direktivs bestemmelser om dekompilering og interoperabilitet.
art7	da	Kommissionen overv�ger indvirkningen af patentbeskyttelse af computer-implementerede opfindel- ser p� innovation og konkurrence, b�de i Europa og internationalt, og p� EU's erhvervsliv, herunder elektronisk handel.
art8	da	Kommissionen afl�gger rapport til Europa-Parlamentet og R�det senest den [DATO (tre �r efter den i artikel 9, stk. 1, anf�rte dato)] om a) virkningerne af patenter p� computer-implementerede opfindelser p� de i artikel 7 omhand- lede faktorer b) hvorvidt reglerne for fastl�ggelse af kriterierne for patenterbarhed, herunder is�r nyhedska- rakter, opfindelsesh�jde og patentkravets genstand, er fyldestg�rende, c) hvorvidt der har v�ret vanskeligheder med medlemsstater, hvor kravene om nyhedskarakter og opfindelsesh�jde ikke unders�ges, inden der udstedes patenter, og i bekr�ftende fald om der b�r tr�ffes foranstaltninger til at overvinde disse vanskeligheder, og d) hvorvidt der har v�ret vanskeligheder med forholdet mellem patentbeskyttelse af computer- implementerede opfindelser og ophavsretlig beskyttelse af edb-programmer som omhandlet i direktiv 91/250/EF
art9	da	1. Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at ef- terkomme dette direktiv senest den [DATO (sidste dag i en m�ned)]. De underretter straks Kommissionen herom. Disse love og bestemmelser skal ved vedtagelsen indeholde en henvisning til dette direktiv eller skal ved offentligg�relsen ledsages af en s�dan henvisning. De n�rmere regler for hen- visningen fasts�ttes af medlemsstaterne. 2. Medlemsstaterne meddeler Kommissionen de nationale retsforskrifter, som de udsteder p� det omr�de, der er omfattet af dette direktiv.
art10	da	
art11	da	
rec1	de	Die Mitgliedstaaten setzen die Rechts- und Verwaltungsvorschriften in Kraft, die erfor- derlich sind, um dieser Richtlinie sp�testens ab dem [DATUM (letzter Tag des betreffenden Monats)] nachzukommen. Sie setzen die Kommission unverz�glich davon in Kenntnis. Wenn die Mitgliedstaaten diese Vorschriften erlassen, nehmen sie in den Vorschriften selbst oder durch einen Hinweis bei der amtlichen Ver�ffentlichung auf diese Richtlinie Bezug. Die Mitglied- staaten regeln die Einzelheiten der Bezugnahme.
rec2	de	Die Mitgliedstaaten teilen der Kommission den Wortlaut der innerstaatlichen Rechts- vorschriften mit, die sie auf dem unter diese Richtlinie fallenden Gebiet erlassen.
rec3	de	
rec4	de	Die zunehmende Verbreitung und Nutzung von Computerprogrammen auf allen Gebieten der Technik und die weltumspannenden Verbreitungswege �ber das Internet sind ein kritischer Faktor f�r die technologische Innovation. Deshalb sollte sichergestellt sein, dass die Ent- wickler und Nutzer von Computerprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden.
rec5	de	Aus diesen Gr�nden sollten die Rechtsvorschriften, so wie sie von den Gerichten in den Mit- gliedstaaten ausgelegt werden, vereinheitlicht und die Vorschriften �ber die Patentierbarkeit computerimplementierter Erfindungen transparent gemacht werden. Die dadurch gew�hrte Rechtssicherheit sollte dazu f�hren, dass Unternehmen den gr��tm�glichen Nutzen aus Patenten f�r computerimplementierte Erfindungen ziehen, und sie sollte Anreize f�r Investi- tionen und Innovationen schaffen.
rec6	de	Die Gemeinschaft und ihre Mitgliedstaaten sind auf das �bereinkommen �ber handels- bezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-�bereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 �ber den Abschluss der �bereink�nfte im Rahmen der multilateralen Verhandlungen der Uruguay- Runde (1986-1994) im Namen der Europ�ischen Gemeinschaft in Bezug auf die in ihre Zust�ndigkeiten fallenden Bereiche . Nach Artikel 27 Absatz 1 des TRIPS-�bereinkommens sollen Patente f�r Erfindungen auf allen Gebieten der Technik erh�ltlich sein, sowohl f�r Erzeugnisse als auch f�r Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinde- rischen T�tigkeit und sind gewerblich anwendbar. Gem�� dem TRIPS-�bereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erh�ltlich sein und Patentrechte ausge�bt werden k�nnen. Diese Grunds�tze sollten demgem�� auch f�r computerimplementierte Erfindungen gelten.
rec7	de	Nach dem �bereinkommen �ber die Erteilung europ�ischer Patente (Europ�isches Patent- �bereinkommen) vom 5. Oktober 1973 (EP�) und den Patentgesetzen der Mitgliedstaaten gelten Programme f�r Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, �sthetische Formsch�pfungen, Pl�ne, Regeln und Verfahren f�r gedankliche T�tigkeiten, f�r Spiele oder f�r gesch�ftliche T�tigkeiten sowie die Wiedergabe von Informationen ausdr�cklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt jedoch nur, und hat auch ihre Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenst�nde oder T�tigkeiten als solche bezieht, da die besagten Gegenst�nde und T�tig- keiten als solche keinem Gebiet der Technik zugeh�ren.
rec7a	de	Ein Computerprogramm "als solches, insbesondere die Ausdrucksform eines Computer- programms in Quellcode oder Objektcode oder in jeder anderen Form kann folglich keine patentierbare Erfindung darstellen.
rec8	de	
rec9	de	Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 �ber den Rechtsschutz von Computerprogrammen sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht gesch�tzt. Die Ideen und Grunds�tze, die einem Element eines Computerprogramms zugrunde liegen, sind dagegen nicht durch das Urheberrecht gesch�tzt.
rec10	de	
rec11	de	F�r Erfindungen gilt ganz allgemein die Voraussetzung, dass sie, um das Kriterium der erfinderischen T�tigkeit zu erf�llen, einen technischen Beitrag zum Stand der Technik leisten sollten.
rec12	de	Eine computerimplementierte Erfindung erf�llt folglich trotz der Tatsache, dass sie einem Gebiet der Technik zugerechnet wird, sofern sie keinen technischen Beitrag zum Stand der Technik leistet, z.B. weil dem besonderen Beitrag die Technizit�t fehlt, nicht das Kriterium der erfinderischen T�tigkeit und ist somit nicht patentierbar.7
rec13	de	Wenn eine festgelegte Prozedur oder Handlungsfolge in einer Vorrichtung, z.B. einem Computer, abl�uft, kann sie einen technischen Beitrag zum Stand der Technik leisten und somit eine patentierbare Erfindung darstellen.
rec13a	de	Dagegen reicht die alleinige Implementierung einer an und f�r sich nicht patentierbaren Methode auf einer Vorrichtung wie einem Computer als solche nicht aus, um die Feststellung zu rechtfertigen, dass ein technischer Beitrag vorliegt. Somit stellt eine computerimplemen- tierte Gesch�ftsmethode oder eine andere Methode, deren einziger Beitrag zum Stand der Technik nicht technischer Art ist, keine patentierbare Erfindung dar.
rec13b	de	Wenn sich ferner der Beitrag zum Stand der Technik allein auf eine nicht patentierbare Sache bezieht, so liegt keine patentierbare Erfindung vor, unabh�ngig davon, wie dies in den Patent- anspr�chen dargelegt wird.
rec13c	de	Au�erdem besitzt ein Algorithmus, der ohne Bezug zu einer physischen Umgebung definiert ist, keinen technischen Charakter; er stellt somit keine patentierbare Erfindung dar.
rec14	de	
rec15	de	
rec16	de	Die Wettbewerbsposition der europ�ischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern w�rde sich verbessern, wenn die bestehenden Unterschiede beim Recht- schutz computerimplementierter Erfindungen ausger�umt w�rden und die Rechtslage trans- parenter w�re.
rec17	de	
rec18	de	Urheberrechtlich zul�ssige Handlungen gem�� der Richtlinie 91/250/EWG �ber den Rechts- schutz von Computerprogrammen, insbesondere deren Vorschriften �ber die Dekompilierung und die Interoperabilit�t, oder die Vorschriften �ber Marken oder Halbleitertopografien bleiben von dem Patentschutz f�r Erfindungen aufgrund dieser Richtlinie unber�hrt.
rec19	de	Gem�� Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiarit�tsprinzip t�tig werden, da die Ziele der vorgeschlagenen Ma�nahme, also die Harmonisierung der nationalen Vorschriften f�r computerimplementierte Erfindungen, "auf Ebene der Mitgliedstaaten nicht ausreichend erreicht werden k�nnen und daher wegen ihres Umfangs oder ihrer Wirkung besser auf Gemeinschaftsebene erreicht werden k�nnen". Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verh�ltnism��igkeit, da sie nicht �ber das f�r die Erreichung der Ziele erforderliche Ma� hinausgeht - HABEN FOLGENDE RICHTLINIE ERLASSEN
art1	de	
art2	de	F�r die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen
art3	de	- Gestrichen - 8
art4	de	Voraussetzung der Patentierbarkeit einer computerimplementierten Erfindung ist, dass sie neu ist, auf einer erfinderischen T�tigkeit beruht und gewerblich anwendbar ist. Die Voraussetzung der erfinderischen T�tigkeit ist nur erf�llt, wenn eine computerimplementierte Erfindung einen tech- nischen Beitrag leistet.
art4a	de	Eine computerimplementierte Erfindung kann nicht allein aufgrund der Tatsache, dass ein Computer oder eine sonstige Vorrichtung eingesetzt wird, als technischer Beitrag betrachtet werden. Demzufolge sind Erfindungen, die Computerprogramme f�r die Implementierung von Gesch�ftsmethoden, mathematischen oder sonstigen Methoden beinhalten und die neben der normalen physikalischen Wechselwirkung zwischen einem Programm und dem Computer, dem Computernetz oder einer sonstigen Vorrichtung, auf dem/der das Programm abl�uft, keinerlei technische Wirkungen haben, nicht patentierbar.
art5	de	
art6	de	Die Rechte aus einem f�r eine Erfindung aufgrund dieser Richtlinie erteilten Patent lassen zul�ssige Handlungen im Sinne der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogram- men durch das Urheberrecht, insbesondere der darin enthaltenen Vorschriften �ber die Dekompilie- rung und die Interoperabilit�t, unber�hrt.
art7	de	Die Kommission beobachtet, wie sich der Patentschutz f�r computerimplementierte Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die euro- p�ischen Unternehmen und den elektronischen Gesch�ftsverkehr auswirkt.
art8	de	Die Kommission legt dem Europ�ischen Parlament und dem Rat sp�testens am [DATUM (drei Jahre nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor �ber
art9	de	
art10	de	
art11	de	
rec1	en	
rec2	en	
rec3	en	
rec4	en	
rec5	en	
rec6	en	The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) [OJ L 336, 23.12.1994, p. 1]. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer- implemented inventions.
rec7	en	
rec7a	en	Accordingly, a computer program as such, in particular the expression of a computer program in source code or object code or in any other form, cannot constitute a patentable invention.
rec8	en	
rec9	en	In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs6, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright.
rec10	en	
rec11	en	It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art.
rec12	en	Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable.7
rec13	en	A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention.
rec13a	en	However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Thus, a computer-implemented business or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.
rec13b	en	Moreover, if the contribution to the state of the art relates solely to unpatentable matter, then there can be no patentable invention irrespective of how that matter is presented in the claims.
rec13c	en	Furthermore, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.
rec14	en	
rec15	en	
rec16	en	The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer- implemented inventions were eliminated and the legal situation was transparent.
rec17	en	
rec18	en	
rec19	en	
art1	en	
art2	en	For the purposes of this Directive the following definitions shall apply
art3	en	- Deleted -8
art4	en	In order to be patentable a computer-implemented invention must be new, involve an inventive step and be susceptible of industrial application. It is a condition for involving an inventive step that a computer-implemented invention makes a technical contribution.
art4a	en	A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, or other apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods, which inventions do not produce any technical effects beyond the normal physical interactions between a program and the computer, network, or other apparatus in which it is run, shall not be patentable.
art5	en	1. Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software. 2. A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into force a product or process daimed in the same patent application in accordance with paragraph 1. 9 The F delegation suggests adding the following new Article
art6	en	The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.
art7	en	more specifically novelty, inventive step and the proper scope of claims, are adequate; (c) whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties; and (d) whether difficulties have been experienced in respect of the relationship between the protection by patents of computer-implemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EC.
art8	en	The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on (a)  the impact of patents for computer-implemented inventions on the factors referred in
art9	en	1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof. When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. 2.  Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive.
art10	en	
art11	en	
rec1	es	
rec2	es	
rec3	es	
rec4	es	
rec5	es	
rec6	es	La Comunidad y sus Estados miembros son signatarios del Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual relacionados con el Comercio (ADPIC), aprobado mediante la Decisi�n 94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la celebraci�n en nombre de la Comunidad Europea, por lo que respecta a los temas de su competencia, de los acuerdos resultantes de las negociaciones multilaterales de la Ronda Uruguay (1986-1994) . El apartado 1 del art�culo 27 del acuerdo ADPIC 5 establece que las patentes podr�n obtenerse por todas las invenciones, sean de productos o de procedimientos, en todos los campos de la tecnolog�a, siempre que sean nuevas, entra�en una actividad inventiva y sean susceptibles de aplicaci�n industrial. Adem�s, seg�n este mismo Acuerdo, las patentes se podr�n obtener y los derechos de patente se podr�n gozar sin discriminaci�n por el campo de la tecnolog�a. En consecuencia, estos principios deben aplicarse a las invenciones implementadas en ordenador.
rec7	es	Con arreglo al Convenio sobre la concesi�n de patentes europeas, firmado en Munich el 5 de octubre de 1973, y las legislaciones sobre patentes de los Estados miembros, no se consideran invenciones, y quedan por tanto excluidos de la patentabilidad, los programas de ordenadores, as� como los descubrimientos, las teor�as cient�ficas, los m�todos matem�ticos, las creaciones est�ticas, los planes, principios y m�todos para el ejercicio de actividades intelectuales, para juegos o para actividades econ�micas, y las formas de presentar informaciones. No obstante, esta excepci�n se aplica y se justifica �nicamente en la medida en que la solicitud de patente o la patente se refiera a uno de esos elementos o actividades considerados como tales, porque dichos elementos y actividades como tales no pertenecen al campo de la tecnolog�a. (7 bis) Por lo tanto, un programa de ordenador como tal, en particular la expresi�n de un programa de ordenador en c�digo fuente, en c�digo objeto o en cualquier otra forma no puede constituir una invenci�n patentable.
rec8	es	
rec9	es	De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo de 1991, sobre la protecci�n jur�dica de los programas de ordenador , cualquier forma de expresi�n de un programa de ordenador original estar� protegido por los derechos de autor como obra literaria. No obstante, las ideas y principios en los que se basa cualquiera de los elementos de un programa de ordenador no est�n protegidos por los derechos de autor.
rec10	es	
rec11	es	Condici�n de las invenciones implementadas en ordenador en general es que , para que entra�en una actividad inventiva, deber�n aportar una contribuci�n t�cnica al estado de la t�cnica.
rec12	es	En consecuencia, si bien una invenci�n implementada en ordenador pertenece a un campo de la tecnolog�a, si no aporta una contribuci�n t�cnica al estado de la t�cnica, como ser�a el caso, por ejemplo, si una contribuci�n espec�fica careciera de car�cter t�cnico, la invenci�n no implicar� actividad inventiva y no podr� ser patentable .7
rec13	es	Un procedimiento determinado o una secuencia de acciones podr�n aportar una contribuci�n t�cnica al estado de la t�cnica y constituir as� una invenci�n patentable cuando se ejecuten mediante un aparato, como por ejemplo un ordenador. (13bis) No obstante, la simple ejecuci�n de un m�todo no patentable de otro modo en un aparato como un ordenador no ser� en s� suficiente para justificar la afirmaci�n de que existe una contribuci�n t�cnica. Por lo tanto un m�todo comercial o de otro tipo ejecutado en ordenador cuya �nica contribuci�n al estado de la t�cnica no revista car�cter t�cnico no podr� constituir una invenci�n patentable. (13ter) Adem�s, si la contribuci�n al estado de la t�cnica se refiere �nicamente a aspectos no patentables, no podr� considerarse que existe una invenci�n patentable, con independencia de c�mo se presente en la reivindicaci�n. (13quarter) Adem�s, un algoritmo definido sin referencia a un entorno f�sico es esencialmente no t�cnico y no podr� constituir, por tanto, una invenci�n patentable.
rec14	es	
rec15	es	
rec16	es	
rec17	es	
rec18	es	
rec19	es	Puesto que los objetivos de la acci�n propuesta, a saber, la armonizaci�n de las normas nacionales relativas a las invenciones implementadas en ordenador, no pueden ser alcanzados de manera suficiente por los Estados miembros y, por consiguiente, pueden lograrse mejor, debido a la dimensi�n o a los efectos de la acci�n contemplada, a nivel comunitario, la Comunidad podr� adoptar medidas, con arreglo al principio de subsidiariedad establecido en el art�culo 5 del Tratado. Con arreglo al principio de proporcionalidad, establecido en ese mismo art�culo, la presente Directiva no excede de lo necesario para alcanzar los objetivos mencionados.
art1	es	La presente Directiva establece normas para la patentabilidad de las invenciones implementadas en ordenador.
art2	es	A efectos de la presente Directiva se aplicar�n las siguientes definiciones
art3	es	- Suprimido - 8
art4	es	Para ser patentables, las invenciones implementadas en ordenador deber�n ser nuevas, suponer una actividad inventiva y tener una aplicaci�n industrial. Para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deber�n aportar una contribuci�n t�cnica. Art�culo 4 bis Exclusiones de patentabilidad No se considerar� que una invenci�n implementada en ordenador aporta una contribuci�n t�cnica �nicamente por implicar la utilizaci�n de un ordenador u otro aparato. Por lo tanto, las invenciones que incorporen programas inform�ticos que implementen m�todos comerciales, matem�ticos o de otro tipo y que no produzcan ning�n efecto t�cnico aparte de las interacciones f�sicas normales entre un programa y el ordenador, la red inform�tica u otro aparato programable en el que funcionan, no ser�n patentables.
art5	es	1. Los Estados miembros garantizar�n que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red inform�tica programada u otro aparato programado, o como procedimiento realizado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa. 2. Una reivindicaci�n de un programa inform�tico, por s� solo o en un soporte, no se considerar� como admitida a menos que dicho programa, si estuviera cargado y ejecutado en un ordenador, una red inform�tica programada u otro aparato programado, ejecutara un producto o un proceso reivindicado en la misma aplicaci�n de patente de conformidad con el apartado 1.
art6	es	Los derechos conferidos por patentes concedidas a invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva no afectar�n a los actos permitidos en el marco de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular con arreglo a sus preceptos relativos a la descompilaci�n y la interoperabilidad.
art7	es	La Comisi�n seguir� de cerca el impacto de la protecci�n mediante patentes de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, incluido el comercio electr�nico.
art8	es	La Comisi�n enviar� al Parlamento Europeo y al Consejo, a m�s tardar el [FECHA] (tres a�os a partir de la fecha que se especifica en el apartado 1 del art�culo 9),], un informe sobre a) el impacto de las patentes concedidas a invenciones implementadas en ordenador sobre los factores mencionados en el art�culo 7; b) si las normas que rigen la determinaci�n de los requisitos de patentabilidad, y m�s concretamente la novedad, la actividad inventiva y el �mbito concreto de las reivindicaciones, son adecuadas; c) si los Estados miembros han tenido dificultades cuando los requisitos de novedad y actividad inventiva no se han examinado antes de la concesi�n de una patente, y en caso afirmativo, si conviene introducir otras medidas para resolver estas dificultades; y d) si se han experimentado dificultades en cuanto a la relaci�n entre la protecci�n mediante patentes de invenciones implementadas en ordenador y la protecci�n mediante derechos de autor de programas de ordenador, tal como establece la Directiva 91/250/CE.
art9	es	1. Los Estados miembros adoptar�n, a m�s tardar el [FECHA (�ltimo d�a de un mes)], las disposiciones legislativas, reglamentarias y administrativas necesarias para dar cumplimiento a lo dispuesto en la presente Directiva. Informar�n de ello inmediatamente a la Comisi�n. Cuando los Estados miembros adopten dichas disposiciones, �stas har�n referencia a la presente Directiva o ir�n acompa�adas de dicha referencia en su publicaci�n oficial. Los Estados miembros establecer�n las modalidades de la mencionada referencia. 2. Los Estados miembros comunicar�n a la Comisi�n el texto de las disposiciones de Derecho interno que adopten en el �mbito regulado por la presente Directiva.
art10	es	La presente Directiva entrar� en vigor el vig�simo d�a siguiente a su publicaci�n en el Diario Oficial de las Comunidades Europeas.
art11	es	Los destinatarios de la presente Directiva ser�n los Estados miembros.
rec1	fi	Sis�markkinoiden toteutumisen my�t� vapaan liikkuvuuden esteet ja kilpailua v��rist�v�t tekij�t poistuvat ja luodaan innovaatiotoiminnalle ja investoinneille suotuisa toimintaymp�rist�. T�ss� yhteydess� keksint�jen suojaaminen patentein on sis�markkinoiden onnistumisen kannalta keskeinen tekij�. Tietokoneella toteutettujen keksint�jen tehokas ja yhdenmukainen suoja kaikissa j�senvaltioissa on olennaisen t�rke�� alan investointien jatkumiseksi ja kannustamiseksi.
rec2	fi	
rec3	fi	
rec4	fi	
rec5	fi	
rec6	fi	Euroopan yhteis� ja sen j�senvaltiot ovat sitoutuneet noudattamaan sopimusta teollis- ja tekij�noikeuksien kauppaan liittyvist� n�k�kohdista (j�ljemp�n� 'TRIPS-sopimus'), joka on hyv�ksytty 22 p�iv�n� joulukuuta 1994 tehdyll� neuvoston p��t�ksell� 94/800/EY Uruguayn kierroksen monenv�lisiss� kauppaneuvotteluissa (1986�1994) laadittujen sopimusten tekemisest� Euroopan yhteis�n puolesta yhteis�n toimivaltaan kuuluvissa asioissa5. TRIPS- sopimuksen 27 artiklan 1 kohdan mukaan milt� tekniikan alalta tahansa olevaa tuotetta tai menetelm�� koskevan keksinn�n tulee olla patentoitavissa edellytt�en, ett� se on uusi ja keksinn�llinen ja ett� sit� voidaan k�ytt�� teollisesti hyv�ksi. Lis�ksi TRIPS-sopimuksen mukaan patenttien saaminen ja patenttioikeuksista nauttiminen eiv�t saa olla riippuvaisia siit�, mik� tekniikanala on kyseess�. N�it� periaatteita olisi sovellettava my�s tietokoneella toteutettuihin keksint�ihin.
rec7	fi	Eurooppapatenttien my�nt�misest� M�ncheniss� 5 p�iv�n� lokakuuta 1973 allekirjoitetun yleissopimuksen ja j�senvaltioiden patenttis��d�sten mukaan keksint�in� ei nimenomaisesti pidet� tietokoneohjelmia eik� l�yt�j�, tieteellisi� teorioita, matemaattisia menetelmi�, taiteellisia luomuksia eik� �lyllist� toimintaa, pelej� tai liiketoimintaa varten tarkoitettuja suunnitelmia, s��nt�j� tai menetelmi� eik� tietojen esitt�mist�, joten n�m� kohteet eiv�t ole patentoitavissa. T�t� poikkeusta kuitenkin sovelletaan ja se on perusteltavissa ainoastaan silt� osin kuin patenttihakemus tai patentti koskee kyseist� kohdetta tai toimintaa sin�ns�, koska mainitut kohteet tai toiminnot sin�ns� eiv�t kuulu mihink��n tekniikanalaan. 7 a) N�in ollen tietokoneohjelma sin�ll��n, eik� erityisesti l�hde- tai kohdekoodina tai miss��n muussa muodossa oleva tietokoneohjelma, ei voi olla patentoitava keksint�.
rec8	fi	
rec9	fi	Tietokoneohjelmien oikeudellisesta suojasta 14 p�iv�n� toukokuuta 1991 annetun neuvoston direktiivin 91/250/ETY6 mukaisesti tekij�noikeuden suoja koskee kirjalliseksi teokseksi katsottavan alkuper�isen tietokoneohjelman eri ilmaisumuotoja. Sen sijaan ideat ja periaatteet, jotka ovat tietokoneohjelman mink� tahansa osan perustana, eiv�t saa tekij�noikeudellista suojaa.
rec10	fi	
rec11	fi	Keksinn�ilt� edellytet��n yleisesti, ett� ollakseen keksinn�llisi� niiden on tuotava lis�ys vallitsevaan tekniikan tasoon.
rec12	fi	N�in ollen vaikka tietokoneella toteutettu keksint� kuuluu johonkin tekniikanalaan, jos se ei tuo lis�yst� vallitsevaan tekniikan tasoon, kuten silloin esimerkiksi, kun lis�ys vallitsevaan tasoon ei ole luonteeltaan tekninen, keksint� ei ole keksinn�llinen, joten se ei ole patentoitavissa.7
rec13	fi	M��ritelty menettely tai toimintojen sarja, joka suoritetaan tietokoneen kaltaisella laitteella, voi tuoda lis�yksen vallitsevaan tekniikan tasoon, joten sit� voidaan pit�� patentoitavissa olevana keksint�n�.
rec13a	fi	Kuitenkaan sellaisen toiminnon, joka ei muutoin ole patentoitavissa, pelkk� soveltaminen sellaiseen laitteeseen kuin tietokone, ei sin�ns� ole riitt�v�, jotta sen perusteella voitaisiin p��tell� kyseess� olevan lis�ys tekniikan tasoon. N�in ollen tietokoneella toteutettu liiketoimi tai muu toiminto, jossa ainoa lis�ys vallitsevaan tasoon on muu kuin tekninen, ei voi olla patentoitavissa oleva keksint�.
rec13b	fi	Sit� paitsi, jos lis�ys vallitsevaan tasoon liittyy ainoastaan sellaiseen asiaan, jota ei voida patentoida, kyseess� ei voi olla patentoitavissa oleva keksint� huolimatta siit�, kuinka kyseinen asia on esitetty patenttivaatimuksessa.
rec13c	fi	Lis�ksi algoritmi, joka m��ritell��n ilman viittausta fyysiseen ymp�rist��n, ei ole luonteeltaan tekninen, joten sit� ei voida pit�� patentoitavissa olevana keksint�n�.
rec14	fi	
rec15	fi	
rec16	fi	
rec17	fi	
rec18	fi	
rec19	fi	Koska j�senvaltiot eiv�t voi riitt�v�ll� tavalla toteuttaa suunnitellun toiminnan tavoitteita, joiden avulla pyrit��n yhdenmukaistamaan tietokoneella toteutetuista keksinn�ist� annetut kansalliset s��nn�t, vaan tavoitteet voidaan toiminnan laajuuden tai vaikutusten takia toteuttaa paremmin yhteis�n tasolla, yhteis� voi ryhty� toimiin perustamissopimuksen 5 artiklassa vahvistetun toissijaisuusperiaatteen mukaisesti. Suhteellisuusperiaatteen mukaisesti, sellaisena kuin se esitet��n sanotussa artiklassa, t�ss� direktiiviss� ei ylitet� sit�, mik� on n�iden tavoitteiden saavuttamiseksi tarpeen,
art1	fi	T�ss� direktiiviss� vahvistetaan tietokoneella toteutettujen keksint�jen patentoitavuutta koskevat s��nn�t.
art2	fi	T�ss� direktiiviss� sovelletaan seuraavia m��ritelmi�
art3	fi	� Poistettu � 8
art4	fi	Jotta tietokoneella toteutettu keksint� on patentoitavissa, sen on oltava uusi, keksinn�llinen ja teollisesti k�ytt�kelpoinen. Keksinn�llisyyden edellytyksen� on se, ett� tietokoneella toteutettu keksint� merkitsee lis�yst� tekniikan tasoon.
art4a	fi	Tietokoneella toteutetun keksinn�n ei katsota olevan lis�ys tekniikan tasoon ainoastaan siksi, ett� siihen liittyy tietokoneen tai muun laitteen k�ytt�. N�in ollen patentoitavissa eiv�t ole keksinn�t, jotka koskevat liiketaloudellisten, matemaattisten tai muiden toimintojen toteuttamiseen suunniteltuja tietokoneohjelmia ja joilla ei ole ohjelman ja tietokoneen, verkon tai muun laitteen v�lisen tavanomaisen fyysisen vuorovaikutuksen lis�ksi muuta teknist� vaikutusta.
art5	fi	1. J�senvaltioiden on varmistettava, ett� tietokoneella toteutetulle keksinn�lle voidaan hakea patenttisuojaa tuotteena, toisin sanoen ohjelmoituna tietokoneena, ohjelmoituna tietokoneiden verkkona tai muuna ohjelmoituna laitteena, tai kyseisen tietokoneen, tietokoneiden verkon tai laitteen suorittamana prosessina, joka pannaan t�yt�nt��n ohjelmiston avulla. 2. Patenttivaatimusta, joka koskee tietokoneohjelmaa joko sellaisenaan tai tietov�lineell�, ei voida tehd�, ellei kyseinen ohjelma tietokoneeseen, ohjelmoituun tietokoneverkkoon tai muuhun ohjelmoitavaan laitteeseen ladattuna ja sellaisessa k�ytettyn� aktivoi tuotetta tai prosessia, jolle on haettu patenttisuojaa samalla patenttihakemuksella 1 kohdan mukaisesti.
art6	fi	T�m�n direktiivin soveltamisalaan kuuluviin keksint�ihin my�nnetyist� patenteista saadut oikeudet eiv�t vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY mukaisesti sallittuihin toimiin, erityisesti kyseisen direktiivin analysoinnista ja yhteentoimivuudesta annettujen s��nn�sten nojalla.
art7	fi	Komissio seuraa tietokoneella toteutettujen keksint�jen patenttisuojan vaikutuksia innovaatiotoimintaan ja kilpailuun Euroopan laajuisesti ja kansainv�lisesti samoin kuin vaikutuksia eurooppalaisiin yrityksiin, s�hk�inen kaupank�ynti mukaan luettuna.
art8	fi	Komissio antaa Euroopan parlamentille ja neuvostolle viimeist��n [... p�iv�n� ...kuuta ... (kolmen vuoden kuluttua 9 artiklan 1 kohdassa tarkoitetusta p�iv�st�)] kertomuksen a) tietokoneella toteutettuja keksint�j� koskevien patenttien vaikutuksista 7 artiklassa tarkoitettuihin seikkoihin; b) siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��ritt�misest� annetut s��nn�t asianmukaisia; c) siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi; ja d) siit�, onko havaittu ongelmia tietokoneella toteutettujen keksint�jen patentilla suojaamisen ja direktiivin 91/250/EY mukaisen tietokoneohjelmien tekij�noikeuksien suojaamisen v�lill�.
art9	fi	1. J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n [ p�iv�n� kuuta (kuukauden viimeinen p�iv�)]. Niiden on ilmoitettava t�st� komissiolle viipym�tt�. N�iss� j�senvaltioiden antamissa s��d�ksiss� on viitattava t�h�n direktiiviin tai niihin on liitett�v� t�llainen viittaus, kun ne virallisesti julkaistaan. J�senvaltioiden on s��dett�v� siit�, miten viittaukset tehd��n. 2. J�senvaltioiden on toimitettava t�ss� direktiiviss� tarkoitetuista kysymyksist� antamansa kansalliset s��nn�kset kirjallisina komissiolle.
art10	fi	
art11	fi	
rec1	fr	
rec2	fr	Des diff�rences existent dans la protection des inventions mises en oeuvre par ordinateur conf�r�e par les pratiques administratives et la jurisprudence des �tats membres. Ces diff�rences pourraient cr�er des entraves aux �changes et faire ainsi obstacle au bon fonctionnement du march� int�rieur.
rec3	fr	De telles diff�rences r�sultent du fait que les �tats membres adoptent de nouvelles pratiques administratives qui diff�rent les unes des autres ou du fait que les jurisprudences nationales interpr�tant la l�gislation actuelle �voluent diff�remment. Ces diff�rences pourraient prendre de l'ampleur avec le temps.
rec4	fr	La diffusion et l'utilisation croissantes de programmes d'ordinateurs dans tous les domaines de la technique et les moyens de diffusion mondiale via l'Internet sont un facteur critique de l'innovation technologique. Il convient donc de veiller � ce que les d�veloppeurs et les utilisateurs de programmes d'ordinateurs dans la Communaut� b�n�ficient d'un environnement optimal.
rec5	fr	En cons�quence, les r�gles de droit telles qu'elles sont interpr�t�es par les tribunaux des �tats membres doivent �tre harmonis�es et les dispositions r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre rendues transparentes. La s�curit� juridique qui en r�sultera devrait permettre aux entreprises de tirer le meilleur parti des brevets pour les inventions mises en oeuvre par ordinateur et stimuler l'investissement et l'innovation.
rec6	fr	La Communaut� et ses �tats membres sont li�s par l'accord relatif aux aspects des droits de propri�t� intellectuelle qui touchent au commerce (ADPIC), approuv� par la d�cision 94/800/CE du Conseil, du 22 d�cembre 1994, relative � la conclusion au nom de la Communaut� europ�enne, pour ce qui concerne les mati�res relevant de ses comp�tences, des accords des n�gociations multilat�rales du cycle de l'Uruguay (1986-1994) . 5 L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra �tre obtenu pour toute invention, de produit ou de proc�d�, dans tous les domaines techniques, � condition qu'elle soit nouvelle, qu'elle implique une activit� inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent �tre obtenus et des droits de brevets exerc�s sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur.
rec7	fr	En vertu de la Convention sur la d�livrance de brevets europ�ens, sign�e � Munich le 5 octobre 1973, et du droit des brevets des �tats membres, les programmes d'ordinateurs ainsi que les d�couvertes, th�ories scientifiques, m�thodes math�matiques, cr�ations esth�tiques, plans, principes et m�thodes dans l'exercice d'activit�s intellectuelles, en mati�re de jeu ou dans le domaine des activit�s �conomiques et les pr�sentations d'informations, ne sont pas consid�r�s comme des inventions et sont donc exclus de la brevetabilit�. Cette exception ne s'applique cependant et n'est justifi�e que dans la mesure o� la demande de brevet ou le brevet concerne ces objets ou ces activit�s en tant que tels parce que lesdits objets et activit�s en tant que tels n'appartiennent � aucun domaine technique. (7bis) En cons�quence, un programme d'ordinateur en tant que tel, notamment l'expression d'un programme d'ordinateur en code source, en code objet ou sous toute autre forme ne peut constituer une invention brevetable.
rec8	fr	
rec9	fr	Conform�ment � la directive du Conseil 91/250/CEE, du 14 mai 1991, concernant la protection juridique des programmes d'ordinateurs , toute expression d'un programme d'ordinateur original est prot�g�e par un droit d'auteur en tant qu'oeuvre litt�raire. Toutefois, les id�es et principes qui sont � la base de quelque �l�ment que ce soit d'un programme d'ordinateur ne sont pas prot�g�s par le droit d'auteur.
rec10	fr	
rec11	fr	D'une mani�re g�n�rale, pour r�pondre au crit�re de l'activit� inventive, les inventions devraient apporter une contribution technique � l'�tat de l'art.
rec12	fr	En cons�quence, bien que les inventions mises en oeuvre par ordinateur appartiennent � un domaine technique, lorsqu'une invention n'apporte pas de contribution technique � l'�tat de l'art, parce que, par exemple, la contribution en question ne rev�t pas un caract�re technique, elle ne r�pond pas au crit�re de l'activit� inventive et n'est donc pas brevetable . 7
rec13	fr	Une proc�dure d�finie ou une s�quence d'actions ex�cut�es sur un appareil tel qu'un ordinateur peut apporter une contribution technique � l'�tat de l'art et constituer ainsi une invention brevetable. (13bis) Toutefois, la simple mise en oeuvre d'une m�thode autrement non brevetable sur un appareil tel qu'un ordinateur n'est pas en elle-m�me suffisante pour consid�rer qu'il y a contribution technique. Ainsi, une m�thode pour l'exercice d'une activit� �conomique, ou une autre m�thode, mise en oeuvre par ordinateur, dans laquelle la seule contribution � l'�tat de l'art ne pr�sente pas de caract�re technique ne peut pas constituer une invention brevetable. (13ter) De plus, si la contribution � l'�tat de l'art porte uniquement sur un sujet non brevetable, il ne peut y avoir invention brevetable, ind�pendamment de la fa�on dont le sujet est pr�sent� dans les revendications. (13quater)En outre, un algorithme d�fini sans r�f�rence � un environnement physique ne pr�sente pas un caract�re technique et ne peut donc constituer une invention brevetable.
rec14	fr	
rec15	fr	
rec16	fr	
rec17	fr	
rec18	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
rec19	fr	Dans la mesure o� les objectifs de l'action envisag�e ne peuvent pas �tre r�alis�s de mani�re suffisante par les �tats membres et peuvent donc, en raison des dimensions ou des effets de l'action envisag�e, �tre mieux r�alis�s au niveau communautaire, la Communaut� est en droit d'adopter des mesures conform�ment au principe de subsidiarit� �nonc� � l'article 5 du trait�. Conform�ment au principe de proportionnalit�, tel qu'�nonc� dans cet article, la pr�sente directive ne va pas au-del� de ce qui est n�cessaire pour atteindre les objectifs fix�s,
art1	fr	La pr�sente directive �tablit des r�gles concernant la brevetabilit� des inventions mises en oeuvre par ordinateur.
art2	fr	Aux fins de la pr�sente directive, les d�finitions suivantes s'appliquent
art3	fr	� Supprim� � 8
art4	fr	Pour �tre brevetable, une invention mise en oeuvre par ordinateur doit �tre nouvelle, impliquer une activit� inventive et �tre susceptible d'application industrielle. Pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur doit apporter une contribution technique. Article 4 bis Exclusions de brevetabilit� Une invention mise en oeuvre par ordinateur n'est pas consid�r�e comme apportant une contribution technique simplement parce qu'elle implique l'utilisation d'un ordinateur ou d'un autre appareil. En cons�quence, ne sont pas brevetables les inventions consistant en des programmes d'ordinateur qui mettent en oeuvre des m�thodes pour l'exercice d'activit�s �conomiques, des m�thodes math�matiques ou d'autres m�thodes, lorsque ces inventions ne produisent pas d'effets techniques au-del� des interactions physiques normales entre un programme et l'ordinateur, r�seau ou autre appareil, sur lequel celui-ci est ex�cut�.
art5	fr	1. Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau informatique ou autre appareil � travers l'ex�cution d'un logiciel. 2. Une revendication pour un programme d'ordinateur, seul ou sur support, n'est autoris�e que si ce programme, lorsqu'il est charg� et ex�cut� dans un ordinateur, un r�seau informatique programm� ou un autre appareil programmable, met en oeuvre un produit ou un proc�d� revendiqu� dans la m�me demande de brevet, conform�ment au paragraphe 1.
art6	fr	Les droits conf�r�s par un brevet d�livr� pour une invention relevant du champ d'application de la pr�sente directive n'affectent pas les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment de ses dispositions relatives � la d�compilation et � l'interop�rabilit�.
art7	fr	La Commission surveille l'incidence de la protection par brevet des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier, ainsi que sur les entreprises europ�ennes, y compris le commerce �lectronique.
art8	fr	La Commission pr�sente au Parlement europ�en et au Conseil, pour le [DATE (trois ans � compter de la date sp�cifi�e � l'article 9, paragraphe 1)] au plus tard, un rapport indiquant
art9	fr	1. Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent imm�diatement la Commission. Lorsque les �tats membres adoptent ces dispositions, celles-ci contiennent une r�f�rence � la pr�sente directive ou sont accompagn�es d'une telle r�f�rence lors de leur publication officielle. Les �tats membres d�terminent la mani�re dont cette r�f�rence doit �tre faite. 2. Les �tats membres communiquent � la Commission le texte des dispositions de droit interne qu'ils adoptent dans le domaine couvert par la pr�sente directive.
art10	fr	
art11	fr	
rec1	it	La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza, come pure la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto, la protezione delle invenzioni mediante i brevetti � un elemento essenziale per il successo del mercato interno. Una protezione efficace e armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri � indispensabile per mantenere e stimolare gli investimenti in questo campo.
rec2	it	
rec3	it	Le suddette discrepanze sono sorte e potrebbero accentuarsi in conseguenza del fatto che gli Stati membri adottano nuove e differenti pratiche amministrative o del fatto che le giurisprudenze nazionali che interpretano la legislazione in vigore evolvono in modo diverso.
rec4	it	
rec5	it	
rec6	it	La Comunit� e i suoi Stati membri sono parti dell'accordo sugli aspetti dei diritti di propriet� intellettuali attinenti al commercio, approvato con la decisione del Consiglio 94/800/CE, del 22 dicembre 1994, relativa alla conclusione a nome della Comunit� europea, per le materie di sua competenza, degli accordi dei negoziati multilaterali dell'Uruguay Round (1986-1994) . L'articolo 27, paragrafo 1 di detto accordo dispone che possono costituire oggetto di brevetto tutte le invenzioni, di prodotti o di processi, in tutti i campi della tecnologia, che presentino carattere di novit�, implichino un'attivit� inventiva e siano atte ad un'applicazione industriale. Inoltre, in base all'accordo, i brevetti possono essere ottenuti e i relativi diritti possono essere esercitati senza discriminazioni quanto al settore della tecnologia. Questi principi valgono di conseguenza per le invenzioni attuate per mezzo di elaboratori elettronici.
rec7	it	Secondo la convenzione sul rilascio dei brevetti europei, firmata a Monaco di Baviera il 5 ottobre 1973, e secondo le legislazioni degli Stati membri in materia di brevetti, i programmi per elaboratore, nonch� le scoperte, le teorie scientifiche, i metodi matematici, le creazioni estetiche, i piani, i principi e i metodi per attivit� intellettuali, giochi o attivit� commerciali e le presentazioni di informazioni sono espressamente non considerati invenzioni e sono quindi esclusi dalla brevettabilit�. Questa eccezione, tuttavia, si applica ed � giustificata soltanto nella misura in cui una domanda di brevetto o un brevetto si riferisce a tali materie o attivit� in quanto tali, perch� tali materie o attivit� in quanto tali non appartengono a un settore della tecnologia. (7 bis) Pertanto, un programma per elaboratore in quanto tale, in particolare l'espressione di un programma per elaboratore in codice sorgente, in codice oggetto o in qualsiasi altra forma non pu� costituire un'invenzione brevettabile.
rec8	it	
rec9	it	A norma della direttiva 91/250/CEE del Consiglio, del 14 maggio 1991, relativa alla tutela giuridica dei programmi per elaboratore , qualsiasi forma di espressione di un programma per elaboratore � tutelata dal diritto d'autore in quanto opera letteraria. Tuttavia, le idee e i principi alla base di qualsiasi elemento di un programma per elaboratore non sono tutelati dal diritto d'autore.
rec10	it	Affinch� siano considerate implicanti un'attivit� inventiva, le invenzioni in generale devono apportare un contributo tecnico allo stato dell'arte.
rec11	it	Affinch� siano considerate implicanti un'attivit� inventiva, le invenzioni in generale devono apportare un contributo tecnico allo stato dell'arte.
rec12	it	Di conseguenza, bench� sia considerata appartenente a un settore della tecnologia, se un'invenzione attuata per mezzo di elaboratori elettronici non apporta un contributo tecnico allo stato dell'arte, come ad esempio quando il suo contributo specifico non presenta un carattere tecnico, essa non pu� essere considerata implicante un'attivit� inventiva e quindi non � brevettabile.
rec13	it	Un processo o una sequenza di azioni determinati, eseguiti per mezzo di un apparecchio, come un elaboratore, pu� apportare un contributo tecnico allo stato dell'arte e quindi costituire un'invenzione brevettabile. (13 bis) Tuttavia, la semplice attuazione di un metodo altrimenti non brevettabile su un apparecchio come un elaboratore elettronico non � sufficiente di per s� a giustificare la presenza di un contributo tecnico. Pertanto, un metodo informatico per l'esercizio di attivit� commerciali o un altro metodo il cui unico contributo allo stato dell'arte � di natura non tecnica non possono costituire un'invenzione brevettabile. (13 ter) Per di pi�, se il contributo allo stato dell'arte riguarda unicamente materie non brevettabili, non pu� esservi un'invenzione brevettabile indipendentemente dal modo in cui la materia � presentata nelle rivendicazioni. (13 quater) Inoltre, un algoritmo definito senza riferimento ad un ambiente fisico non presenta un carattere tecnico e non pu� quindi costituire un'invenzione brevettabile.
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non dovrebbe richiedere una legislazione specifica che sostituisca la norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti dovrebbero restare la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici, con le modifiche o le integrazioni relative a specifici aspetti richieste dalla presente direttiva.
rec15	it	La presente direttiva dovrebbe limitarsi all'enunciazione di taluni principi che si applicano alla brevettabilit� di tali invenzioni, in particolare al fine di assicurare la tutela delle invenzioni che appartengono a un settore della tecnologia e costituiscono un contributo tecnico e, inversamente, di escludere da tale tutela le invenzioni che non costituiscono un contributo tecnico.
rec16	it	
rec17	it	
rec18	it	
rec19	it	Poich� gli obiettivi del provvedimento proposto, ossia l'armonizzazione delle norme nazionali relative alle invenzioni attuate per mezzo di elaboratori elettronici, non possono essere sufficientemente realizzati dagli Stati membri e possono dunque, a motivo delle dimensioni o degli effetti dell'azione in questione, essere realizzati meglio a livello comunitario, l'intervento della Comunit� � giustificato in base al principio della sussidiariet�, enunciato all'articolo 5 del trattato. Conformemente al principio della proporzionalit�, enunciato nello stesso articolo, la presente direttiva non va al di l� di quanto necessario per il raggiungimento di tali obiettivi,
art1	it	
art2	it	Ai fini della presente direttiva, s'intende per
art3	it	- Soppresso - 8
art4	it	Per essere brevettabile un'invenzione attuata per mezzo di elaboratori elettronici deve presentare un carattere di novit�, implicare un'attivit� inventiva ed essere atta ad un'applicazione industriale. Affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici deve apportare un contributo tecnico. Articolo 4 bis Esclusione dalla brevettabilit� Un'invenzione attuata per mezzo di elaboratori elettronici non � considerata apportare un contributo tecnico per il solo fatto di implicare l'uso di un elaboratore elettronico, o di un altro apparecchio. Pertanto, invenzioni che includono programmi per elaboratori elettronici che attuano metodi commerciali, matematici o di altro tipo, invenzioni che non producono effetti tecnici al di l� delle normali interazioni fisiche tra un programma e l'elaboratore elettronico, la rete o altre apparecchiature in funzione non sono brevettabili.
art5	it	1. Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata come prodotto, ossia come elaboratore programmato, rete di elaboratori programmati o altro apparecchio programmato, o come processo realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software. 2. La rivendicazione riguardante un programma per elaboratore, da solo o su un vettore, non � ammessa a meno che il programma, caricato su un elaboratore elettronico, su una rete di elaboratori programmati o su un altro apparecchio programmabile, attivi un prodotto o processo oggetto della medesima applicazione di brevetto, conformemente al paragrafo 1.
art6	it	La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facolt� riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilit�.
art7	it	
art8	it	La Commissione riferisce al Parlamento europeo e al Consiglio, entro [DATA (tre anni dalla data di cui all'articolo 9, paragrafo 1)], su
art9	it	1. Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro il [DATA (ultimo giorno di un mese)]. Essi ne informano immediatamente la Commissione. Quando gli Stati membri adottano tali disposizioni, queste contengono un riferimento alla presente direttiva o sono corredati di un siffatto riferimento all'atto della pubblicazione ufficiale. Le modalit� del riferimento sono decise dagli Stati membri. 2. Gli Stati membri comunicano alla Commissione il testo delle disposizioni di diritto interno che essi adottano nella materia disciplinata dalla presente direttiva.
art10	it	La presente direttiva entra in vigore il ventesimo giorno successivo alla pubblicazione nella Gazzetta ufficiale delle Comunit� europee.
art11	it	Gli Stati membri sono destinatari della presente direttiva.
rec1	nl	Voor de totstandbrenging van de interne markt is het nodig beperkingen voor het vrije verkeer en concurrentieverstoringen op te heffen, en tegelijkertijd een gunstig klimaat te scheppen voor innovatie en investeringen. In deze context is de bescherming van uitvindingen door octrooien een essentieel element voor het succes van de interne markt. Doeltreffende en geharmoniseerde bescherming van in computers toegepaste uitvindingen in alle lidstaten is van essentieel belang om de investeringen op dit gebied in stand te houden en aan te moedigen.
rec2	nl	Er bestaan verschillen in de bescherming van in computers toegepaste uitvindingen die wordt geboden door de bestuursrechtelijke praktijken en de jurisprudentie van de verschillende lid- staten. Deze verschillen kunnen leiden tot handelsbelemmeringen en aldus de goede werking van de interne markt verhinderen.
rec3	nl	Deze verschillen zijn ontstaan en kunnen groter worden naarmate de lidstaten nieuwe en uiteenlopende bestuursrechtelijke praktijken goedkeuren, of wanneer de nationale juris- prudentie die de bestaande wetgeving uitlegt, op ongelijke wijze evolueert.
rec4	nl	De gestage toename van de verspreiding en het gebruik van computerprogramma's op alle gebieden van de technologie en van de wereldwijde verspreiding ervan via internet is een kritieke factor voor de technologische innovatie. Daarom moet worden voorzien in een optimale omgeving voor de ontwikkelaars en de gebruikers van computerprogramma's in de Gemeenschap.
rec5	nl	Om die reden moeten de rechtsregels, zoals ze door de rechtbanken in de lidstaten worden uitgelegd, worden geharmoniseerd en moet het recht betreffende de octrooieerbaarheid van in computers toegepaste uitvindingen transparant worden gemaakt. De daaruit voort- vloeiende rechtszekerheid moet de ondernemingen in staat stellen optimaal profijt te trekken van octrooien voor in computers toegepaste uitvindingen en moet een stimulans zijn voor investeringen en innovatie.
rec6	nl	De Gemeenschap en haar lidstaten zijn gebonden door de Overeenkomst inzake de handels- aspecten van de intellectuele eigendom (TRIPS-Overeenkomst), aangenomen bij Besluit 94/800/EG van de Raad van 22 december 1994 betreffende de sluiting, namens de Europese Gemeenschap voor wat betreft de onder haar bevoegdheid vallende aangelegen- heden, van de uit de multilaterale handelsbesprekingen in het kader van de Uruguay-Ronde (1986-1994) voortvloeiende overeenkomsten5. Artikel 27, lid 1, van de TRIPS-Overeen- komst bepaalt dat octrooi kan worden verleend voor uitvindingen, producten dan wel werk- wijzen, op alle gebieden van de technologie, mits zij nieuw zijn, op uitvinderswerkzaamheid berusten en vatbaar zijn voor industri�le toepassing. Bovendien kan volgens de TRIPS- Overeenkomst octrooi worden verleend en kunnen octrooirechten worden genoten zonder onderscheid op grond van het gebied van de technologie. Deze beginselen moeten dienover- eenkomstig gelden voor in computers toegepaste uitvindingen.
rec7	nl	Uit hoofde van het op 5 oktober 1973 te M�nchen ondertekende Verdrag inzake de verlening van Europese octrooien en de octrooiwetten van de lidstaten worden computer- programma's alsmede ontdekkingen, natuurwetenschappelijke theorie�n, wiskundige methoden, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten. Deze uitzondering is echter alleen van toepassing en gerechtvaardigd voorzover een octrooiaanvrage of octrooi betrekking heeft op deze onderwerpen of werkzaamheden als zodanig, omdat de genoemde onderwerpen en werkzaamheden als zodanig niet tot een gebied van de technologie behoren. (7 bis) Een computerprogramma als zodanig, en met name het uitdrukken van een computer- programma in broncode, doelcode of enige andere vorm kan derhalve geen octrooieerbare uitvinding zijn.
rec8	nl	Dankzij octrooibescherming kunnen innovatoren profijt trekken van hun creativiteit. Octrooirechten bieden bescherming voor innovatie in het belang van de maatschappij in haar geheel, maar mogen niet worden gebruikt op een wijze die de concurrentie verstoort.
rec9	nl	Overeenkomstig Richtlijn 91/250/EEG van de Raad van 14 mei 1991 betreffende de rechts- bescherming van computerprogramma's wordt de uitdrukkingswijze, in welke vorm dan ook, van een oorspronkelijk computerprogramma auteursrechtelijk beschermd als werk van letterkunde. De idee�n en beginselen die aan enig element van een computerprogramma ten grondslag liggen, worden echter niet auteursrechtelijk beschermd.
rec10	nl	Opdat een uitvinding als octrooieerbaar kan worden beschouwd, moet zij een technisch karakter hebben, en aldus behoren tot een gebied van de technologie.
rec11	nl	Om op uitvinderswerkzaamheid te berusten, moeten uitvindingen over het algemeen een technische bijdrage tot de stand van de techniek leveren.
rec12	nl	Bijgevolg zal, hoewel een in computers toegepaste uitvinding tot een gebied van de techno- logie behoort, een uitvinding die geen technische bijdrage tot de stand van de techniek levert, zoals bijvoorbeeld het geval zou zijn wanneer de specifieke bijdrage ervan geen technisch karakter heeft, niet op uitvinderswerkzaamheid berusten en bijgevolg niet octrooieerbaar zijn.7
rec13	nl	Een gedefinieerde procedure of opeenvolging van handelingen kan, bij uitvoering in de context van een apparaat zoals een computer, een technische bijdrage tot de stand van de techniek leveren en daardoor een octrooieerbare uitvinding vormen. (13 bis) Het louter toepassen van een anderszins niet-octrooieerbare methode op een apparaat zoals een computer is, op zich, echter niet voldoende om te kunnen concluderen dat er sprake is van een technische bijdrage. Een met de computer toegepaste bedrijfs- of andere methode waar- van de enige bijdrage aan de stand van de techniek van niet-technische aard is, kan derhalve geen octrooieerbare uitvinding vormen. (13 ter) Indien de bijdrage aan de stand van de techniek daarenboven uitsluitend betrekking heeft op niet-octrooieerbare materie, kan er geen sprake zijn van een octrooieerbare uitvinding, ongeacht de manier waarop die materie in de conclusies wordt voorgesteld. (13 quater) Voorts is een algoritme dat zonder verwijzing naar een fysieke omgeving wordt gedefinieerd, inherent niet-technisch en kan daarom geen octrooieerbare uitvinding vormen.
rec14	nl	De rechtsbescherming van in computers toegepaste uitvindingen mag geen afzonderlijk rechtsinstrument vergen ter vervanging van de regels van het nationale octrooirecht. De regels van het nationale octrooirecht moeten, zoals aangepast of aangevuld overeenkomstig het bepaalde in deze richtlijn, de essenti�le basis blijven voor de rechtsbescherming van in computers toegepaste uitvindingen.
rec15	nl	Deze richtlijn moet worden beperkt tot het vaststellen van bepaalde beginselen met betrekking tot de octrooieerbaarheid van dergelijke uitvindingen. Deze beginselen dienen er met name voor te zorgen dat uitvindingen die tot een gebied van de technologie behoren en een technische bijdrage leveren, voor bescherming in aanmerking komen, en omgekeerd dat uitvindingen die geen technische bijdrage leveren, daar niet voor in aanmerking komen.
rec16	nl	De concurrentiepositie van het Europese bedrijfsleven ten opzichte van zijn voornaamste handelspartners zou worden verbeterd indien de bestaande verschillen in de rechts- bescherming van in computers toegepaste uitvindingen zouden worden opgeheven en de rechtssituatie transparant zou zijn.
rec17	nl	
rec18	nl	Handelingen die zijn toegestaan op grond van Richtlijn 91/250/EEG betreffende de auteurs- rechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit, of de bepalingen betreffende topogra- fie�n van halfgeleiderproducten of merken, worden onverlet gelaten door de bescherming die binnen de werkingssfeer van deze richtlijn door octrooien voor uitvindingen wordt verleend.
rec19	nl	Aangezien de doelstellingen van het voorgestelde optreden, namelijk de harmonisatie van de nationale voorschriften betreffende in computers toegepaste uitvindingen, niet voldoende door de lidstaten kunnen worden verwezenlijkt en derhalve vanwege de omvang of de gevolgen van het optreden beter door de Gemeenschap kunnen worden verwezenlijkt, mag de Gemeen- schap overeenkomstig het in artikel 5 van het Verdrag bedoelde subsidiariteitsbeginsel maat- regelen treffen. Overeenkomstig het in dat artikel bedoelde evenredigheidsbeginsel gaat deze richtlijn niet verder dan wat nodig is om deze doelstellingen te verwezenlijken,
art1	nl	Deze richtlijn stelt regels vast voor de octrooieerbaarheid van in computers toegepaste uitvindingen.
art2	nl	Voor de toepassing van deze richtlijn gelden de volgende definities
art3	nl	- Geschrapt -8
art4	nl	Om octrooieerbaar te zijn, moet een in computers toegepaste uitvinding nieuw zijn, op uitvinders- werkzaamheid berusten en industrieel toepasbaar zijn. Een in computers toegepaste uitvinding berust slechts op uitvinderswerkzaamheid, indien zij een technische bijdrage levert. Artikel 4 bis Uitsluiting van octrooieerbaarheid Een in computers toegepaste uitvinding levert geen technische bijdrage enkel en alleen omdat zij berust op het gebruik van een computer of een ander apparaat. Uitvindingen die berusten op computerprogramma's die bedrijfs-, wiskundige of andere methoden toepassen en die, naast de normale fysieke wisselwerking tussen het programma en de computer, het netwerk of een ander apparaat waarop het draait, geen andere technische effecten sorteren, zijn derhalve niet octrooieer- baar.
art5	nl	1. De lidstaten zorgen ervoor dat een in computers toegepaste uitvinding kan worden geclaimd als product, dat wil zeggen als een geprogrammeerde computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat, of als een werkwijze die door zo een computer, computer- netwerk of apparaat middels het uitvoeren van software wordt toegepast. 2. Een conclusie betreffende een computerprogramma, hetzij op zichzelf hetzij op een drager, is slechts toegestaan indien dat programma, wanneer het is ingevoerd en wordt uitgevoerd in een computer, een geprogrammeerd computernetwerk of een ander programmeerbaar apparaat, een product of proc�d� activeert dat in dezelfde octrooiaanvrage overeenkomstig lid 1 is geclaimd.
art6	nl	De rechten die worden verkregen door octrooien welke zijn verleend voor uitvindingen binnen de werkingssfeer van deze richtlijn, laten handelingen onverlet die zijn toegestaan op grond van Richt- lijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name op grond van de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit.
art7	nl	De Commissie volgt welke invloed de octrooibescherming van in computers toegepaste uitvin- dingen heeft op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, met inbegrip van de elektronische handel.
art8	nl	De Commissie brengt bij het Europees Parlement en de Raad tegen uiterlijk [DATUM (drie jaar vanaf de in artikel 9, lid 1, vermelde datum)] verslag uit over
art9	nl	1. De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om v��r [DATUM (laatste dag van een maand)] aan deze richtlijn te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis. Wanneer de lidstaten die bepalingen aannemen, wordt in de bepalingen zelf of bij de offici�le bekendmaking daarvan naar deze richtlijn verwezen. De regels voor de verwijzing worden vast- gesteld door de lidstaten. 2. De lidstaten delen de Commissie de tekst van de bepalingen van nationaal recht mede die zij op het onder deze richtlijn vallende gebied vaststellen.
art10	nl	
art11	nl	
rec1	pt	A realiza��o do mercado interno implica a elimina��o das restri��es � liberdade de circula��o e �s distor��es da concorr�ncia, criando um ambiente que seja favor�vel � inova��o e ao investimento. Neste contexto, a protec��o dos inventos por meio de patentes � um elemento essencial para o �xito do mercado interno. A protec��o eficaz e harmonizada dos inventos implementados atrav�s de computador, em todos os Estados- -Membros, � essencial para manter e incentivar o investimento neste dom�nio;
rec2	pt	H� diferen�as na protec��o dos inventos implementados atrav�s de computador proporcionada pelas pr�ticas administrativas e pela jurisprud�ncia dos diversos Estados- -Membros. Essas diferen�as podem criar barreiras ao com�rcio, dificultando, assim, o bom funcionamento do mercado interno;
rec3	pt	
rec4	pt	A constante expans�o da distribui��o e utiliza��o de programas de computador em todos os ramos tecnol�gicos e da sua distribui��o mundial via Internet � um factor fundamental da inova��o tecnol�gica. Por isso, � necess�rio garantir na Comunidade um ambiente �ptimo para os criadores e utilizadores de programas de computador;
rec5	pt	Consequentemente, as normas jur�dicas, conforme interpretadas pelos tribunais dos Estados-Membros, devem ser harmonizadas e a lei que rege a patenteabilidade dos inventos implementados atrav�s de computador deve tornar-se transparente. A certeza jur�dica da� resultante deve permitir �s empresas tirarem o m�ximo partido das patentes dos inventos implementados atrav�s de um computador e dar um incentivo ao investimento e � inova��o;
rec6	pt	A Comunidade e os seus Estados-Membros est�o vinculados pelo Acordo sobre os Aspectos dos Direitos de Propriedade Intelectual Relacionados com o Com�rcio (TRIPS), aprovado pela Decis�o 94/800/CE do Conselho, de 22 de Dezembro de 1994, relativa � celebra��o, em nome da Comunidade Europeia e em rela��o �s mat�rias da sua compet�ncia, dos acordos resultantes das negocia��es multilaterais do Uruguay Round (1986/1994) . O artigo 27.� do Acordo TRIPS prev�, no seu n.� 1, que dever�o existir patentes para quaisquer inventos, quer se trate de produtos ou processos, em todos os dom�nios da tecnologia, desde que esses inventos sejam novos, impliquem uma actividade inventiva e sejam suscept�veis de aplica��o industrial. Al�m disso, segundo o Acordo TRIPS, deve haver direitos de patentes que possam ser usufru�dos sem discrimina��o quanto ao dom�nio da tecnologia. Tais princ�pios devem, nesse sentido, aplicar-se a todos os inventos implementados atrav�s de um computador;
rec7	pt	Segundo a Conven��o sobre a Concess�o de Patentes Europeias, assinada em Munique em 5 de Outubro de 1973, e a legisla��o em mat�ria de patentes dos Estados-Membros, considera-se expressamente que os programas de computador, em conjunto com as descobertas, as teorias cient�ficas, os m�todos matem�ticos, as cria��es est�ticas, os esquemas, as regras e m�todos para execu��o de actividades intelectuais, jogos ou actividades comerciais, assim como as exposi��es de informa��o, n�o s�o inventos, pelo que est�o exclu�dos da patenteabilidade. Esta excep��o, por�m, aplica-se e justifica-se apenas na medida em que um pedido de patente ou uma patente se relaciona com esses temas ou actividades em si, porque esses temas e actividades, enquanto tais, n�o pertencem a um dom�nio da tecnologia;
rec8	pt	
rec9	pt	De acordo com a Directiva 91/250/CEE do Conselho, de 14 de Maio de 1991, relativa � protec��o jur�dica dos programas de computador , a express�o sob qualquer forma de um programa de computador original � protegida por direitos de autor como uma obra liter�ria. Contudo, os princ�pios e ideias subjacentes a qualquer elemento de um programa de computador n�o est�o protegidos pelo direito de autor;
rec10	pt	Para que qualquer invento seja considerado patente�vel, deve ter um car�cter t�cnico e, consequentemente, pertencer a um dom�nio da tecnologia;
rec11	pt	� condi��o geral de um invento que, por forma a implicar uma actividade inventiva, ele tenha que dar um contributo t�cnico para o estado da arte.
rec12	pt	Por conseguinte, embora um invento implementado atrav�s de computador perten�a a um dom�nio da tecnologia, quando n�o der um contributo t�cnico para o estado da arte, como acontecer�, por exemplo, se o seu contributo espec�fico estiver desprovido de car�cter t�cnico, faltar-lhe-� actividade inventiva e n�o ser� por isso patente�vel.7
rec13	pt	Um determinado processo ou sequ�ncia de ac��es, quando executados com recurso a um equipamento como, por exemplo, um computador, podem dar um contributo t�cnico para o estado da arte, constituindo por isso invento patente�vel.
rec14	pt	Para a protec��o jur�dica dos inventos implementados atrav�s de computador n�o � necess�rio criar um organismo de aplica��o das normas em vigor da legisla��o nacional em mat�ria de patentes. As regras da legisla��o nacional em mat�ria de patentes devem permanecer a base essencial para a protec��o jur�dica dos inventos implementados atrav�s de um computador, adaptadas ou acrescentadas em certas circunst�ncias espec�ficas, conforme se indica na presente directiva;
rec15	pt	A presente directiva deve limitar-se a fixar certos princ�pios na medida em que se aplicam � patenteabilidade desses inventos, destinando-se esse princ�pios, em particular, a garantir que os inventos pertencentes a um dom�nio da tecnologia e que apresentem um contributo t�cnico sejam suscept�veis de protec��o e, por oposi��o, a garantir que os inventos sem contributo t�cnico o n�o sejam;
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhoraria se fossem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos implementados atrav�s de computador e se a situa��o jur�dica fosse transparente;
rec17	pt	
rec18	pt	
rec19	pt	Dado que os objectivos da ac��o proposta, nomeadamente para harmonizar as regras nacionais respeitantes aos inventos implementados atrav�s de computador n�o podem ser suficientemente alcan�ados pelos Estados-Membros, podendo, por isso, devido � sua escala ou aos seus efeitos, ser alcan�ados de melhor forma a n�vel comunit�rio, a Comunidade poder� adoptar medidas, de acordo com o princ�pio da subsidiariedade, conforme estabelecido no artigo 5.� do Tratado. De acordo com o princ�pio da proporcionalidade, conforme determinado nesse artigo, a presente directiva n�o vai mais longe que o necess�rio para alcan�ar esses objectivos,
art1	pt	A presente directiva estabelece regras para a patenteabilidade dos inventos implementados atrav�s de computador.
art2	pt	Para efeitos da presente directiva, entende-se por
art3	pt	� Suprimido � 8
art4	pt	N�o se considera que um invento implementado atrav�s de computador d� um contributo t�cnico meramente por utilizar computadores ou outro equipamento. Da mesma forma, n�o s�o patente�veis os inventos que envolvam programas de computador que implementam um procedimento comercial ou matem�tico ou outros m�todos, sem outros efeitos de car�cter t�cnico al�m das habituais interac��es f�sicas entre um programa e o computador, a rede ou outro equipamento em que seja utilizado.
art5	pt	1. Os Estados-Membros garantir�o que um invento implementado atrav�s de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro equipamento programado, ou ainda como processo executado por esse computador, rede inform�tica ou equipamento, atrav�s da execu��o de um software. 2. Uma reivindica��o relativa a um programa aut�nomo ou numa portadora s� ser� permitida se este programa, ao ser carregado e corrido num computador, rede inform�tica programada ou outro equipamento program�vel, executar um produto ou processo reivindicado no mesmo pedido de patente a que se refere o n.� 1.
art6	pt	Os direitos conferidos por patentes concedidas aos inventos no �mbito da aplica��o da presente directiva n�o ser�o extensivos aos actos permitidos ao abrigo da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular ao abrigo das suas disposi��es respeitantes � descompila��o e � interoperabilidade.
art7	pt	A Comiss�o acompanhar� o impacto, sobre a inova��o e a concorr�ncia, da protec��o conferida por patentes aos inventos implementados atrav�s de computador, tanto na Europa como a n�vel internacional, bem como nas empresas europeias, inclusive no com�rcio electr�nico.
art8	pt	A Comiss�o apresentar� ao Parlamento Europeu e ao Conselho, at� [DATA (no prazo de tr�s anos a contar da data especificada no n.� 1 do artigo 9.�)] um relat�rio sobre a) O impacto das patentes dos inventos implementados atrav�s de computador nos factores mencionados no artigo 7.�; b) Se as normas que regem a determina��o dos requisitos de patenteabilidade e, mais especificamente, de novidade, de actividade inventiva e do �mbito apropriado das reivindica��es, s�o adequadas; c) Se houve dificuldades relativamente aos Estados-Membros onde os requisitos de novidade e de actividade inventiva n�o s�o examinados antes da emiss�o de uma patente e, nesse caso, se ser� desej�vel tomar medidas para solucionar essas dificuldades; e d) Se foram sentidas dificuldades no que respeita � rela��o entre a protec��o conferida por patentes aos inventos implementados atrav�s de computador e a protec��o jur�dica de programas de computador prevista na Directiva 91/250/CE.
art9	pt	
art10	pt	
art11	pt	
rec1	sv	F�r att genomf�ra den inre marknaden m�ste man dels undanr�ja hinder f�r fri r�rlighet och motverka snedvridna konkurrensf�rh�llanden, dels skapa f�rh�llanden som gynnar innovation och investeringar. I detta sammanhang �r skydd f�r uppfinningar genom patent en v�sentlig f�ruts�ttning f�r att den inre marknaden skall bli en framg�ng. Ett verksamt och harmoniserat skydd av datorrelaterade uppfinningar i alla medlemsstater �r viktigt f�r att trygga och stimulera investeringsviljan p� detta omr�de.
rec2	sv	Det r�ttsskydd f�r datorrelaterade uppfinningar som meddelas i olika medlemsstater uppvisar skillnader p� grund av f�rvaltningspraxis och r�ttspraxis. S�dana skillnader kan skapa handelshinder och l�gga hinder i v�gen f�r en v�l fungerande inre marknad.
rec3	sv	Skillnaderna har vuxit fram och kan �ka genom att medlemsstaterna till�mpar ny och egen f�rvaltningspraxis, eller genom att tolkningen av g�llande lag utvecklas p� olika s�tt i nationell r�ttspraxis.
rec4	sv	Den stadiga �kningen n�r det g�ller distribution och anv�ndning av datorprogram p� alla teknikomr�den samt spridningen av programmen globalt via Internet �r en kritisk faktor f�r teknisk innovation. Det �r d�rf�r n�dv�ndigt att s�rja f�r optimala f�rh�llanden f�r programanv�ndare och programutvecklare i gemenskapen.
rec5	sv	D�rf�r b�r de lagregler som medlemsstaternas domstolar tolkar harmoniseras och den lag som reglerar patenterbarhet f�r datorrelaterade uppfinningar g�ras klar och tydlig. Ty �tf�ljande r�ttss�kerhet b�r ge f�retagen m�jlighet att utnyttja patent p� datorrelaterade uppfinningar optimalt samt stimulera investeringar och innovation.
rec6	sv	Gemenskapen och dess medlemsstater �r bundna av avtalet om handelsrelaterade immateriella r�ttigheter (TRIPS-avtalet), som godk�ndes genom r�dets beslut 94/800/EG av den 22 december 1994 om att i Europeiska gemenskapens namn, i fr�gor inom dess beh�righet, sluta avtal enligt de multilaterala f�rhandlingarna inom Uruguayrundan (1986-1994)5. Artikel 27.1 i TRIPS-avtalet stadgar att patent skall meddelas f�r alla uppfinningar, s�v�l anordningar som f�rfaranden, p� alla teknikomr�den, f�rutsatt att de �r nya, har uppfinningsh�jd och kan tillgodog�ras industriellt. Vidare skall det, enligt TRIPS-avtalet, g� att erh�lla och utnyttja patentskydd p� alla teknikomr�den utan diskriminering. Dessa principer b�r g�lla f�ljdenligt f�r datorrelaterade uppfinningar.
rec7	sv	Enligt Konventionen om meddelande av europeiska patent, som undertecknades i M�nchen den 5 oktober 1973, och patentlagarna i medlemsstaterna kan datorprogram liksom uppt�ckter, vetenskapliga teorier, matematiska metoder, konstn�rliga skapelser, planer, regler eller metoder f�r intellektuell verksamhet, f�r spel eller f�r aff�rsverksamhet samt framl�gganden av information uttryckligen inte betraktas som uppfinningar och �r d�rf�r undantagna fr�n det patenterbara omr�det. Dessa undantag �r emellertid endast till�mpliga och ber�ttigade i den m�n en patentans�kan eller ett patent h�nf�r sig till s�dana f�rem�l eller verksamheter i sig, eftersom de n�mnda f�rem�len och verksamheterna i sig inte tillh�r n�got teknikomr�de.
rec7a	sv	S�ledes kan inte ett datorprogram i sig, s�rskilt inte ett datorprogram i k�llkod eller objektskod eller i n�gon annan form utg�ra en patenterbar uppfinning.
rec8	sv	
rec9	sv	Enligt r�dets direktiv 91/250/EEG av den 14 maj 1991 om r�ttsligt skydd f�r datorprogram6 skall ett originellt datorprogram i alla dess uttrycksformer �tnjuta upphovsr�ttsligt skydd som litter�rt verk. Men id�er och principer som ligger bakom de olika detaljerna i ett datorprogram �r inte upphovsr�ttsligt skyddade.
rec10	sv	
rec11	sv	Det �r ett villkor f�r uppfinningar i allm�nhet att de, f�r att tillerk�nnas uppfinningsh�jd, b�r utg�ra ett tekniskt bidrag till teknikens st�ndpunkt.
rec12	sv	Fast�n en datorrelaterad uppfinning s�ledes tillh�r ett teknikomr�de d�r den inte utg�r ett bidrag till teknikens st�ndpunkt, vilket t.ex. �r fallet om bidraget saknar teknisk karakt�r, skall den anses sakna uppfinningsh�jd och d�rmed inte vara patenterbar7.
rec13	sv	En specificerad �tg�rdssekvens som utf�rs i f�rbindelse med en anordning s�som en dator kan utg�ra ett tekniskt bidrag till teknikens st�ndpunkt och d�rigenom vara en patenterbar uppfinning.
rec13a	sv	Emellertid �r enbart till�mpningen som s�dan av en annars icke patenterbar metod p� en apparat s�som en dator inte tillr�cklig f�r att s�kerst�lla att ett tekniskt bidrag f�religger. S�ledes kan en datortill�mpad aff�rsmetod eller annan metod d�r det enda bidraget till teknikens st�ndpunkt �r icke-teknisk inte utg�ra en patenterbar uppfinning.
rec13b	sv	Vidare kan det, om bidraget till teknikens st�ndpunkt enbart r�r icke patenterbara fr�gor, inte finnas n�gon patenterbar uppfinning oavsett hur den fr�gan l�ggs fram i kraven.
rec13c	sv	Vidare �r en algoritm som utformas utan h�nvisning till n�gon fysisk omgivning till sin natur icke-teknisk och kan d�rf�r inte vara en patenterbar uppfinning.
rec14	sv	
rec15	sv	Direktivets inneh�ll b�r begr�nsas till vissa principer som b�r reglera uppfinningarnas patenterbarhet, varvid dessa principer s�rskilt b�r garantera att uppfinningar som tillh�r ett teknikomr�de och utg�r ett tekniskt bidrag g�r att patentskydda, samt omv�nt att uppfinningar som inte utg�r n�got tekniskt bidrag inte g�r att patentskydda.
rec16	sv	
rec17	sv	
rec18	sv	
rec19	sv	Eftersom m�len f�r f�rslaget, n�mligen att harmonisera nationella regler om datorrelaterade uppfinningar, inte i tillr�cklig utstr�ckning kan uppn�s av medlemsstaterna och d�rf�r, p� grund av den planerade �tg�rdens omfattning eller verkningar, b�ttre kan uppn�s p� gemenskapsniv�, f�r gemenskapen anta best�mmelser i kraft av subsidiaritetsprincipen i artikel 5 i f�rdraget. I �verensst�mmelse med proportionalitetsprincipen, som fastst�lls i den artikeln, g�r direktivet inte l�ngre �n vad som �r n�dv�ndigt f�r att uppn� dessa m�l.
art1	sv	
art2	sv	I detta direktiv avses med a) datorrelaterad uppfinning
art3	sv	� Utg�r �8
art4	sv	F�r att vara patenterbar skall en datorrelaterad uppfinning vara ny, ha uppfinningsh�jd och kunna tillgodog�ras industriellt. En datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd endast p� villkor att den utg�r ett tekniskt bidrag.
art4a	sv	En datorrelaterad uppfinning skall inte anses utg�ra ett tekniskt bidrag enbart d�rf�r att den f�ruts�tter anv�ndning av en dator eller annan anordning. I enlighet d�rmed skall uppfinningar som r�r datorprogram som till�mpar aff�rsmetoder, matematiska eller andra metoder och som inte har n�gra tekniska verkningar ut�ver de normala fysiska v�xelverkningarna mellan ett program och datorn, datorn�t eller anordningen d�r de k�rs, inte vara patenterbara.
art5	sv	1. Medlemsstaterna skall se till att en datorrelaterad uppfinning kan patentskyddas s�som anordning, dvs. en programmerad dator, ett programmerat n�tverk eller n�gon annan programmerad anordning, eller s�som f�rfarande utf�rt av ovann�mnda dator, datorn�t eller anordning genom att en mjukvara k�rs p� anordningen. 2. Ett krav p� ett datorprogram, antingen som sj�lvst�ndig produkt eller p� en b�rare, skall inte vara till�tet om det programmet inte, n�r det laddas och exekveras i en dator, programmerat datorn�t eller annan programmerbar anordning, s�tter i kraft en produkt eller en process som ing�r i samma patentans�kan, i enlighet med punkt 1.
art6	sv	De r�ttigheter som ges genom patentskydd f�r uppfinningar som omfattas av detta direktiv, skall inte p�verka handlingar som �r till�tna enligt direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt enligt best�mmelserna i det direktivet avseende dekompilering och samverkansf�rm�ga.
art7	sv	Kommissionen skall f�lja hur patentskydd av datorrelaterade uppfinningar p�verkar innovation och konkurrens, b�de i Europa och internationellt, samt europeiskt n�ringsliv, �ven elektronisk handel.
art8	sv	Senast f�re [DATUM (tre �r fr�n det datum som anges i artikel 9.1)] skall kommissionen rapportera till Europaparlamentet och r�det om a) hur datorrelaterade uppfinningar p�verkar de faktorer som n�mns i artikel 7, b) huruvida reglerna f�r bed�mning av patenterbarhet, n�rmare best�mt nyhet, uppfinningsh�jd och l�mpligt skyddsomf�ng f�r kraven, �r �ndam�lsenliga, c) huruvida de medlemsstater som inte granskar nyhet och uppfinningsh�jd innan patent utf�rdas har haft sv�righeter och, om s� �r fallet, huruvida det kr�vs �tg�rder f�r att angripa dessa sv�righeter, och d) huruvida det har uppst�tt sv�righeter betr�ffande f�rh�llandet mellan patentskyddet f�r datorrelaterade uppfinningar och upphovsr�ttsskyddet f�r datorprogram enligt direktiv 91/250/EG.
art9	sv	1. Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast den [DATUM (sista dagen i m�naden)] och skall genast underr�tta kommissionen om detta. N�r en medlemsstat antar dessa best�mmelser skall de inneh�lla en h�nvisning till detta direktiv eller �tf�ljas av en s�dan h�nvisning n�r de offentligg�rs. N�rmare f�reskrifter om hur h�nvisningen skall g�ras skall varje medlemsstat sj�lv utf�rda. 2. Medlemsstaterna skall till kommissionen �verl�mna texterna till de best�mmelser i nationell lagstiftning som de antar inom det omr�de som omfattas av detta direktiv.
art10	sv	
art11	sv	
\.


--
-- Data for TOC entry 62 (OID 23696)
-- Name: juri; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY juri (id, lang, juri) FROM stdin;
rec1	da	Gennemf�relsen af det indre marked indeb�rer, at hindringerne for den frie bev�gelighed og konkurrencefordrejningerne fjernes, og at der samtidig skabes et gunstigt klima for innovation og investeringer. I den forbindelse er beskyttelsen af opfindelser ved hj�lp af patenter en v�sentlig betingelse for et vellykket indre marked. Det er vigtigt med en effektiv, gennemskuelig og harmoniseret beskyttelse af computer-implementerede opfindelser i alle medlemsstater for at opretholde og fremme investeringerne p� dette omr�de.
rec5	da	Lovbestemmelserne vedr�rende computer-implementerede opfindelsers patenterbarhed b�r derfor harmoniseres med henblik p� at sikre, at den herved opn�ede retssikkerhed og omfanget af de betingelser, der stilles for patenterbarhed, g�r det muligt for virksomhederne at f� det fulde udbytte af deres opfindelsesproces og udg�r et incitament til investering og innovation. <P>Retssikkerheden vil ogs� i tilf�lde af tvivl om fortolkningen af dette direktiv blive sikret ved, at nationale domstole kan, og nationale sidsteinstansdomstole skal, indbringe sagen for Domstolen.
rec7a	da	Form�let med dette direktiv er ikke at �ndre Den Europ�iske Patentkonvention, men at forhindre divergerende fortolkninger af konventionens bestemmelser.
rec11	da	For at v�re patenterbare skal opfindelser i almindelighed og computer-implementerede opfindelser i s�rdeleshed kunne anvendes industrielt, v�re nye og have opfindelsesh�jde. For at have opfindelsesh�jde b�r computer-implementerede opfindelser yde et bidrag til det aktuelle tekniske niveau.
rec12	da	Selv om en computer-implementeret opfindelse p� grund af selve sin karakter henh�rer under et teknologisk omr�de, er det derfor vigtigt at g�re det klart, at hvis en opfindelse ikke yder et bidrag til det aktuelle tekniske niveau, hvilket er tilf�ldet, hvis det specifikke bidrag ikke er af teknisk karakter, har opfindelsen ikke opfindelsesh�jde og er derfor ikke patenterbar. <P>N�r det skal vurderes, om der er tale om opfindelsesh�jde, anvendes s�dvanligvis "problem og l�sning"-indfaldsvinklen med henblik p� at afg�re, om der foreligger et teknisk problem, som skal l�ses. Hvis der ikke foreligger noget teknisk problem, kan opfindelsen ikke anses for at yde et bidrag til det aktuelle tekniske niveau.
rec13a	da	Det blotte forhold, at en ellers ikke-patenterbar metode implementeres p� en maskine s�som en computer er ikke i sig selv nok til at konkludere, at der foreligger et teknisk bidrag. En computer-implementeret forretningsmetode eller en anden metode, hvor det eneste bidrag til det aktuelle tekniske niveau er ikke-teknisk, kan derfor ikke udg�re en patenterbar opfindelse.
rec13b	da	Hvis bidraget til det aktuelle tekniske niveau udelukkende vedr�rer en ikke-patenterbar genstand, kan der ikke v�re tale om nogen patenterbar opfindelse, uanset hvorledes genstanden pr�senteres i patentkravet. F.eks. kan kravet om teknisk bidrag ikke omg�s blot ved at anf�re anvendelse af tekniske midler i patentkravet.
rec13c	da	En algoritme er grundl�ggende ikke-teknisk og kan derfor ikke udg�re nogen teknisk opfindelse. En metode, som indeb�rer anvendelse af en algoritme, kan dog v�re patenterbar, forudsat at metoden anvendes til at l�se et teknisk problem. Ethvert patent, som meddeles for en s�dan metode, giver dog ikke eneret p� algoritmen som s�dan eller p� dens anvendelse i sammenh�nge, som ikke er beskrevet i patentet.
rec13d	da	Omfanget af de eksklusive rettigheder, der er knyttet til et hvilket som helst patent, er fastsat i patentkravene. Ans�gninger om patenter p� computer-implementerede opfindelser skal ske med henvisning til enten et produkt, f.eks. en programmeret maskine, eller en proces, der udf�res af en s�dan maskine. Anvendelse af individuelle elementer af software i en sammenh�ng, der ikke indeb�rer frembringelse af et produkt eller en proces, for hvilke der lovligt er ans�gt om et patent, vil derfor ikke blive betragtet som en patentkr�nkelse.
rec14	da	Retlig beskyttelse af computer-implementerede opfindelser kr�ver ikke indf�relse af en s�rlig lovgivning til erstatning af medlemsstaternes patentlovgivning. Det prim�re grundlag for retlig beskyttelse af computer-implementerede opfindelser er fortsat medlemsstaternes patentlovgivning. Dette direktiv afklarer blot den nuv�rende retlige stilling under hensyn til Den Europ�iske Patentmyndigheds praksis med det form�l at skabe retssikkerhed, gennemsigtighed og klarhed i lovgivningen og undg� enhver tendens i retning af patenterbarhed af ikke-patenterbare metoder, f.eks. forretningsmetoder.
rec16	da	EU's erhvervslivs konkurrenceevne i forhold til EU's vigtigste handelspartnere vil blive forbedret, hvis de nuv�rende forskelle i den retlige beskyttelse af computer-implementerede opfindelser fjernes og retstilstanden bliver gennemsigtig. Med den nuv�rende tendens inden for den traditionelle forarbejdningsindustri i retning af at flytte arbejdsprocesser til �konomier med lave omkostninger uden for Den Europ�iske Union er betydningen af beskyttelse af intellektuel ejendomsret, is�r patentbeskyttelse, umiddelbart indlysende.
rec17	da	Dette direktiv b�r ikke ber�re anvendelsen af konkurrencereglerne, herunder is�r traktatens artikel 81 og 82.
rec18	da	De rettigheder, der indr�mmes i patenter udstedt for opfindelser inden for dette direktivs anvendelsesomr�de, ber�rer ikke handlinger, der er tilladt i henhold til artikel 5 og 6 i direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r i henhold til bestemmelserne om dekompilering og interoperabilitet. Navnlig kr�ver handlinger, som i henhold til artikel 5 og 6 i direktiv 91/250/E�F ikke kr�ver tilladelse fra rettighedshaveren med hensyn til rettighedshaverens ejendomsret til eller vedr�rende et edb-program, og som, havde det ikke v�ret for artikel 5 eller 6 i direktiv 91/250/E�F, ville have kr�vet en s�dan tilladelse, ikke tilladelse fra rettighedshaveren for s� vidt ang�r rettighedshaverens patentrettigheder til eller vedr�rende edb-programmet.
art2a	da	a)ved en "computer-implementeret opfindelse" forst�s enhver opfindelse, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art3	da	udg�r <P>
art4	da	For at v�re patenterbar skal en computer-implementeret opfindelse kunne anvendes industrielt, v�re ny og have opfindelsesh�jde. For at have opfindelsesh�jde skal en computer-implementeret opfindelse yde et teknisk bidrag. <P>Medlemsstaterne sikrer, at det er en n�dvendig foruds�tning for opfindelsesh�jde, at en computer-implementeret opfindelse yder et teknisk bidrag. <P>Det tekniske bidrag vurderes ud fra det aktuelle tekniske niveau og patentkravets genstand som helhed, som skal omfatte tekniske karakteristika, uanset om disse karakteristika ledsages af ikke-tekniske karakteristika.
art4a	da	Artikel 4a <P>Udelukkelse fra patenterbarhed <P>En computer-implementeret opfindelse anses ikke for at yde et teknisk bidrag, blot fordi den indeb�rer anvendelse af en computer, et netv�rk eller andre programmerbare maskiner. Der kan derfor ikke udtages patent p� opfindelser, der omfatter edb-programmer, som implementerer forretningsmetoder, matematiske metoder eller andre metoder, og som ikke har nogen tekniske virkninger ud over det normale fysiske samspil mellem et program og den computer, det netv�rk eller de andre programmerbare maskiner, som det k�res p�.
art5	da	1.Medlemsstaterne sikrer, at der kan ans�ges om patent p� en computer-implementeret opfindelse som et produkt, dvs. som en programmeret computer, et programmeret computernet eller en anden programmeret maskine, eller som en proces, der udf�res af computeren, computernettet eller maskinen ved k�rsel af software. <P>2.En ans�gning om patent p� et edb-program, enten alene, p� et b�remedium eller i form af et signal, skal kun v�re tilladt, hvis dette program, n�r det indl�ses i eller k�res p� en computer, et computernetv�rk eller en anden programmerbar maskine, frembringer et produkt eller udf�rer en proces, som er patenterbar i henhold til artikel 4 og 4a.
art6	da	De rettigheder, der indr�mmes i patenter udstedt for opfindelser inden for dette direktivs anvendelsesomr�de, ber�rer ikke handlinger, der er tilladt i henhold til artikel 5 og 6 i direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r i henhold til bestemmelserne om dekompilering og interoperabilitet.
art6a	da	Artikel 6a<P>Medlemsstaterne sikrer, at n�r anvendelsen af en patenteret teknik er p�kr�vet alene med henblik p� konvertering af de konventioner, der anvendes i to forskellige computersystemer eller -netv�rk for at muligg�re kommunikation og dataudveksling mellem dem, betragtes en s�dan anvendelse ikke som en kr�nkelse af patentet.
art7	da	Kommissionen overv�ger, hvordan patentbeskyttelse af computer-implementerede opfindelser indvirker p� innovation og konkurrence, b�de i Europa og internationalt, og p� EU's erhvervsliv, is�r sm� og mellemstore virksomheder og elektronisk handel.
art8a	da	Artikel 8a <P>P� baggrund af den i artikel 7 omhandlede overv�gning og den i artikel 8 omhandlede rapport vurderer Kommissionen indvirkningen af dette direktiv og forel�gger i givet fald Europa-Parlamentet og R�det forslag til en �ndringsretsakt.
art8c	da	(c)hvorvidt der har v�ret vanskeligheder med medlemsstater, hvor kravene om nyhedskarakter og opfindelsesh�jde ikke unders�ges, inden der udstedes patenter, og i bekr�ftende fald, om der b�r tr�ffes foranstaltninger til l�sning af disse vanskeligheder, og
art8ca	da	ca)hvorvidt der har v�ret vanskeligheder med hensyn til forholdet mellem beskyttelsen af patenter p� computer-implementerede opfindelser og beskyttelsen af ophavsret til edb-programmer i henhold til direktiv 91/250/E�F, og hvorvidt der er sket misbrug af patentsystemet i forbindelse med computer-implementerede opfindelser
art8cb	da	cb)hvorvidt det ville v�re �nskeligt og retligt muligt under hensyn til F�llesskabets internationale forpligtelser at indf�re en "henstandsperiode" for s�danne elementer i en patentans�gning for enhver form for opfindelse, som afsl�res forud for datoen for ans�gningens indgivelse
art8cc	da	cc)i hvilke henseender det kan blive n�dvendigt at g�re forberedelser til en diplomatisk konference om �ndring af den europ�iske patentkonvention, bl.a. i lyset af det kommende EF-patent
art8cd	da	cd)hvordan der er taget hensyn til kravene i dette direktiv i Det Europ�iske Patentkontors praksis og i dets retningslinjer for behandling.
art9_1	da	1.Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at efterkomme dette direktiv senest atten m�neder efter dets ikrafttr�den. De underretter straks Kommissionen herom.
rec1	de	Damit der Binnenmarkt verwirklicht wird, m�ssen Beschr�nkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen beg�nstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium f�r den Erfolg des Binnenmarkts. Es ist unerl�sslich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam, transparent und einheitlich gesch�tzt sind, wenn Investitionen auf diesem Gebiet gesichert und gef�rdert werden sollen.
rec5	de	Aus diesen Gr�nden sollten die f�r die Patentierbarkeit computerimplementierter Erfindungen ma�geblichen Rechtsvorschriften vereinheitlicht werden, um sicherzustellen, dass die dadurch gew�hrte Rechtssicherheit und das Anforderungsniveau f�r die Patentierbarkeit dazu f�hren, dass innovative Unternehmen den gr��tm�glichen Nutzen aus ihrem Erfindungsprozess ziehen und Anreize f�r Investitionen und Innovationen geschaffen werden. <P>Einzelstaatliche Gerichte k�nnen und einzelstaatliche letztinstanzliche Gerichte m�ssen bei Zweifeln �ber die Auslegung dieser Richtlinie den Gerichtshof anrufen, wodurch ebenfalls Rechtssicherheit gew�hrleistet wird.
rec11	de	Um patentierbar zu sein, m�ssen Erfindungen im Allgemeinen und computerimplementierte Erfindungen im Besonderen neu sein, auf einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen T�tigkeit zu erf�llen, sollten computerimplementierte Erfindungen einen technischen Beitrag zum Stand der Technik leisten.
rec12	de	Wenn auch eine computerimplementierte Erfindung von Natur aus zu einem Gebiet der Technik geh�rt, ist es doch wichtig klarzustellen, dass folglich eine Erfindung, die keinen technischen Beitrag zum Stand der Technik leistet, z. B. weil dem besonderen Beitrag die Technizit�t fehlt, nicht das Kriterium der erfinderischen T�tigkeit erf�llt und somit nicht patentierbar ist. <P>Bei der Pr�fung, ob das Kriterium der erfinderischen T�tigkeit erf�llt ist, wird �blicherweise der Ansatz Problem/L�sung verfolgt um festzustellen, dass das zu l�sende Problem technischer Art ist. Liegt kein technisches Problem vor, kann nicht davon ausgegangen werden, dass die Erfindung einen technischen Beitrag zum Stand der Technik leistet.
rec13a	de	Allerdings reicht allein die Tatsache, dass eine ansonsten nicht patentierbare Methode in einer Vorrichtung wie einem Computer angewendet wird, nicht aus, um davon auszugehen, dass ein technischer Beitrag geleistet wird.<P>Folglich kann eine computerimplemen-tierte Gesch�fts- oder andere Methode, bei der der einzige Beitrag zum Stand der Technik nichttechnischen Charakter hat, keine patentierbare Erfindung darstellen.
rec13b	de	Bezieht sich der Beitrag zum Stand der Technik ausschlie�lich auf einen nichtpatentierbaren Gegenstand, kann es sich nicht um eine patentierbare Erfindung handeln, unabh�ngig davon, wie der Gegenstand in den Anspr�chen dargestellt wird. So kann beispielsweise das Erfordernis eines technischen Beitrags nicht einfach dadurch umgangen werden, dass in den Patentanspr�chen technische Hilfsmittel spezifiziert werden.
rec13c	de	Au�erdem ist ein Algorithmus von Natur aus nichttechnischer Art und kann deshalb keine technische Erfindung darstellen. Allerdings kann eine Methode, die die Benutzung eines Algorithmus umfasst, unter der Voraussetzung patentierbar sein, dass die Methode zur L�sung eines technischen Problems angewandt wird. Allerdings w�rde ein f�r eine derartige Methode gew�hrtes Patent kein Monopol auf den Algorithmus selbst oder seine Anwendung in einem von dem Patent nicht betroffenen Kontext verleihen.
rec13d	de	Der Anwendungsbereich der ausschlie�lichen Rechte, die durch ein Patent �bertragen werden, wird durch die Anspr�che definiert. Anspr�che auf computerimplementierte Erfindungen m�ssen unter Bezugnahme entweder auf ein Erzeugnis wie beispielsweise eine programmierte Vorrichtung oder ein Verfahren, dass in einer solchen Vorrichtung verwirklicht wird, angemeldet werden. Werden einzelne Software-Elemente in einem Kontext benutzt, bei dem es nicht um die Verwirklichung eines rechtm��ig beanspruchten Erzeugnisses oder Verfahrens geht, stellt eine solche Verwendung keine Patentverletzung dar.
rec14	de	Um computerimplementierte Erfindungen rechtlich zu sch�tzen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage f�r den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage mit Blick auf die Praxis des Europ�ischen Patentamts klargestellt, um Rechtssicherheit, Transparenz und Klarheit bei der Rechtslage zu gew�hrleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Gesch�ftsmethoden, als patentf�hig zu erachten.
rec16	de	Die Wettbewerbsposition der europ�ischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern wird sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausger�umt sind und die Rechtslage transparenter ist. Beim derzeitigen Trend der klassischen verarbeitenden Industrie zur Verlagerung ihrer Betriebe in Niedrigkostenl�nder au�erhalb der Europ�ischen Union liegt die Bedeutung des Urheberrechtsschutzes und insbesondere des Patentschutzes auf der Hand.
rec17	de	
rec18	de	Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere gem�� den Vorschriften in Bezug auf die Dekompilierung und die Interoperabilit�t. Insbesondere erfordern Handlungen, die gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG nicht die Genehmigung des Rechtsinhabers in Bezug auf die Urheberrechte des Rechtsinhabers an dem oder in Zusammenhang mit dem Computerprogramm erfordern, und f�r die ohne Artikel 5 oder 6 der Richtlinie 91/250/EWG eine solche Genehmigung erforderlich w�re, keine Genehmigung des Rechtsinhabers in Bezug auf die Patentrechte des Rechtsinhabers an dem oder in Zusammenhang mit dem Computerprogramm.
art2a	de	(a)"Computerimplementierte Erfindung" ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art3	de	entf�llt
art4	de	Um patentierbar zu sein, m�ssen computerimplementierte Erfindungen neu sein, auf einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um Das Kriterium der erfinderischen T�tigkeit zu erf�llen, m�ssen computerimplementierte Erfindungen einen technischen Beitrag leisten. <P>2.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung, die einen technischen Beitrag leistet, eine notwendige Voraussetzung einer erfinderischen T�tigkeit ist. <P>Bei der Ermittlung des technischen Beitrags wird der Stand der Technik und der Gegenstand des Patentanspruchs in seiner Gesamtheit, der technische Merkmale umfassen muss, beurteilt, unabh�ngig davon, ob neben diesen Merkmalen nichttechnische Merkmale gegeben sind.
art4a	de	Artikel 4a <P>Ausnahmen von der Patentierbarkeit <P>Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausf�hrung ein Computerprogramm eingesetzt wird und durch die Gesch�ftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentf�hig, wenn sie �ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.
art5	de	1.Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf�hrung von Software verwirklicht wird. <P>2.Ein Patentanspruch auf ein Computerprogramm f�r sich allein, auf einem Datentr�ger oder als ein Signal ist nur zul�ssig, wenn dieses Programm, nachdem es auf einem Computer, einem Computernetz oder einer sonstigen programmierbaren Vorrichtung geladen ist oder abl�uft, zu einem Erzeugnis f�hrt oder ein Verfahren ausf�hrt, das nach Artikeln 4 und 4a patentierbar ist.
art6	de	Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere gem�� den Vorschriften in Bezug auf die Dekompilierung und die Interoperabilit�t.
art6a	de	Artikel 6a<P>Die Mitgliedstaaten stellen sicher, dass in allen F�llen, in denen der Einsatz einer patentierten Technik nur zum Zweck der Konvertierung der in zwei verschiedenen Computersystemen oder "netzen verwendeten Konventionen ben�tigt wird, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu erm�glichen, diese Verwendung nicht als Patentverletzung gilt.
art7	de	7.Die Kommission beobachtet, wie sich der Patentschutz hinsichtlich computerimplementierter Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen, insbesondere kleine und mittlere Unternehmen, und den elektronischen Gesch�ftsverkehr auswirkt.
art8a	de	Artikel 8a<P>Anhand der Beobachtung gem�� Artikel 7 und dem gem�� Artikel 8 zu erstellenden Bericht �berpr�ft die Kommission die Auswirkungen dieser Richtlinie und unterbreitet dem Europ�ischen Parlament und dem Rat erforderlichenfalls Vorschl�ge zur �nderung der Rechtsvorschriften.
rec1	en	The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. <I>Effective</I>, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.
rec5	en	Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation.<P>Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.
rec7a	en	The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.
rec11	en	In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.
rec12	en	Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. <P>When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.
rec13a	en	However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.
rec13b	en	If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.
rec13c	en	Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.
rec13d	en	The scope of the exclusive rights conferred by any patent are defined by the<BR>claims. Computer-implemented inventions must be claimed with reference to<BR>either a product such as a programmed apparatus, or to a process carried out<BR>in such an apparatus. Accordingly, where individual elements of software are<BR>used in contexts which do not involve the realisation of any validly claimed<BR>product or process, such use will not constitute patent infringement.
rec14	en	The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.
rec16	en	The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.
rec17	en	This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.
rec18	en	The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.
art2a	en	(a)"computer-implemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;
art3	en	(deleted) <P>
art4	en	In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. <P>Member States shall ensure that a computer-implemented invention making a technical contribution constitutes a necessary condition of involving an inventive step. <P>The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.
art4a	en	Article 4a <P>Exclusions from patentability
art5	en	1.Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software. <P>2.A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.
art6	en	The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.
art6a	en	Article 6a <P>Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
art7	en	7.The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and electronic commerce.
art8a	en	Article 8a <P>In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall review the impact of this Directive and, where necessary, submit proposals for amending legislation to the European Parliament and the Council.
art9_1	en	1.Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than eighteen months after its entry into force. They shall forthwith inform the Commission thereof.
rec1	es	La realizaci�n del mercado interior implica la eliminaci�n de las restricciones a la libre circulaci�n y de las distorsiones de la competencia, as� como la creaci�n de un entorno que sea favorable a la innovaci�n y a las inversiones. En este contexto, la protecci�n de las invenciones mediante patentes constituye un elemento esencial para el �xito del mercado interior. Una protecci�n efectiva, transparente y armonizada de las invenciones implementadas en ordenador en todos los Estados miembros es esencial para mantener y fomentar las inversiones en este �mbito.
rec5	es	Por consiguiente, las normas jur�dicas relativas a la patentabilidad de las invenciones implementadas en ordenador deber�an armonizarse, de modo que se garantizara que la seguridad jur�dica resultante y el nivel de los requisitos exigidos para la patentabilidad permitieran a las empresas innovadoras obtener el m�ximo beneficio de su proceso de invenciones e impulsaran la inversi�n y la innovaci�n. <P>La seguridad jur�dica estar� garantizada asimismo por el hecho de que, en caso de que se planteen dudas en cuanto a la interpretaci�n de la presente Directiva, los tribunales nacionales podr�n remitir el asunto al Tribunal de Justicia para que resuelva, y los tribunales nacionales de �ltima instancia deber�n hacerlo.
rec7a	es	Con la presente Directiva no se pretende modificar el Convenio de la Patente Europea, sino evitar que puedan existir interpretaciones divergentes de su texto.
rec11	es	Para ser patentables, las invenciones en general y las invenciones implementadas en ordenador en particular deber�n ser susceptibles de aplicaci�n industrial, nuevas y suponer una actividad inventiva. Para que entra�en una actividad inventiva, las invenciones implementadas en ordenador deber�n aportar una contribuci�n t�cnica al estado de la t�cnica.
rec12	es	En consecuencia, aunque una invenci�n implementada en ordenador pertenece por su propia naturaleza al �mbito de la tecnolog�a, es importante dejar claro que si una invenci�n no aporta una contribuci�n t�cnica al estado de la t�cnica, como ser�a el caso, por ejemplo, si su contribuci�n espec�fica careciera de car�cter t�cnico, la invenci�n no implicar� actividad inventiva y no podr� ser patentable. <P>Para evaluar si ha habido o no actividad inventiva, lo habitual es aplicar el enfoque de problema y soluci�n para determinar si existe un problema t�cnico por resolver. Si no hay ning�n problema t�cnico, no puede considerarse que la invenci�n aporte una contribuci�n t�cnica al estado de la t�cnica.
rec13a	es	No obstante, la mera implementaci�n en un aparato, como un ordenador, de un m�todo considerado por otras razones no patentable no es suficiente por s� misma para justificar la conclusi�n de que existe una contribuci�n t�cnica. Por consiguiente, un m�todo comercial u otro m�todo cuya �nica contribuci�n al estado de la t�cnica sea de car�cter no t�cnico no podr� constituir una invenci�n patentable.
rec13b	es	Si la contribuci�n al estado de la t�cnica reside exclusivamente en elementos no patentables, no puede considerarse que la invenci�n sea patentable, independientemente de c�mo se presente el elemento en las reivindicaciones. Por ejemplo, el requisito de la contribuci�n t�cnica no podr� eludirse con una mera especificaci�n de medios t�cnicos en las reivindicaciones de patente.
rec13c	es	Adem�s, un algoritmo es esencialmente no t�cnico, por lo que no puede constituir una invenci�n t�cnica. No obstante, un m�todo que comprenda el uso de un algoritmo podr� ser patentable siempre que dicho m�todo se utilice para resolver un problema t�cnico. Sin embargo, una patente concedida por un m�todo de estas caracter�sticas no permitir� que se monopolice el propio algoritmo o su uso en contextos no previstos en la patente.
rec13d	es	El �mbito de los derechos exclusivos conferidos por una patente se define en las reivindicaciones de patentes. Las invenciones implementadas en ordenador deben reivindicarse haciendo referencia a un producto como, por ejemplo, un aparato programado, o a un procedimiento realizado en un aparato de esas caracter�sticas. En este contexto, en aquellos casos en que se utilicen distintos elementos de programas inform�ticos en contextos que no impliquen la realizaci�n de un producto o de un procedimiento reivindicado de forma v�lida, esta utilizaci�n no se considerar� una vulneraci�n de patente.
rec14	es	La protecci�n jur�dica de las invenciones implementadas en ordenador no precisa la creaci�n de una disposici�n jur�dica independiente que sustituya a las normas de la legislaci�n nacional sobre patentes. Las normas de la legislaci�n nacional sobre patentes siguen siendo la referencia b�sica para la protecci�n jur�dica de las invenciones implementadas en ordenador. La presente Directiva simplemente clarifica la posici�n jur�dica vigente teniendo en cuenta las pr�cticas de la Oficina Europea de Patentes con miras a garantizar la seguridad jur�dica, la transparencia y la claridad de la ley y a evitar toda desviaci�n hacia la patentabilidad de m�todos no patentables, como los m�todos comerciales.
rec16	es	La posici�n competitiva de la industria europea respecto a sus principales socios comerciales mejorar� si se eliminan las diferencias existentes en la protecci�n jur�dica de las invenciones implementadas en ordenador y se garantiza la transparencia de la situaci�n jur�dica. Con la tendencia actual de la industria manufacturera tradicional a trasladar sus operaciones a econom�as de bajo coste fuera de la Uni�n Europea, resulta evidente por s� misma la importancia de la protecci�n de la propiedad intelectual y, en particular, de la protecci�n mediante patentes.
rec17	es	La presente Directiva deber�a entenderse sin perjuicio de la aplicaci�n de las normas sobre competencia, en particular los art�culos 81 y 82 del Tratado.
rec18	es	Los derechos que confieren las patentes concedidas para invenciones en el �mbito de la presente Directiva no afectar�n a actos permitidos en el marco de los art�culos 5 y 6 de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular en el marco de sus preceptos por lo que se refiere a la descompilaci�n y la interoperabilidad. En particular, los actos que, en virtud de los art�culos 5 y 6 de la Directiva 91/250/CEE, no necesiten autorizaci�n del titular del derecho respecto de los derechos de autor del titular en un programa inform�tico o en relaci�n con el mismo, y para los que, fuera del �mbito de los art�culos 5 o 6 de la Directiva 91/250/CEE se exigir�a esta autorizaci�n, no precisar�n la autorizaci�n del titular del derecho respecto de sus derechos de patente en el programa inform�tico o en relaci�n con el mismo.
art2a	es	(a)«invenci�n implementada en ordenador», toda invenci�n para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas que se realicen total o parcialmente mediante un programa o programas de ordenador;
art3	es	suprimido <P>
art4	es	Para ser patentable, una invenci�n implementada en ordenador deber� ser susceptible de aplicaci�n industrial, nueva y suponer una actividad inventiva. Para entra�ar una actividad inventiva, la invenci�n implementada en ordenador deber� aportar una contribuci�n t�cnica. <P>Los Estados miembros garantizar�n que constituya una condici�n necesaria de la actividad inventiva el hecho de que una invenci�n implementada en ordenador aporte una contribuci�n t�cnica. <P>La contribuci�n t�cnica se evaluar� considerando el estado de la t�cnica y el �mbito de la reivindicaci�n de la patente considerada en su conjunto, que deber� incluir caracter�sticas t�cnicas, con independencia de que �stas est�n acompa�adas por caracter�sticas no t�cnicas.
art4a	es	Art�culo 4 bis <P>Causas de exclusi�n de la patentabilidad <P>No se considerar� que una invenci�n implementada en ordenador aporta una contribuci�n t�cnica meramente porque implique el uso de un ordenador, red u otro aparato programable. En consecuencia, no ser�n patentables las invenciones que utilizan programas inform�ticos que aplican m�todos comerciales, matem�ticos o de otro tipo y no producen efectos t�cnicos, aparte de la normal interacci�n f�sica entre un programa y el ordenador, red o aparato programable de otro tipo en que se ejecute.
art5	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red inform�tica programada u otro aparato programado, o como procedimiento realizado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa. <P>2.Una reivindicaci�n de patente para un programa de ordenador, almacenado o no en un soporte f�sico, o distribuido en forma de se�al, ser� admisible si dicho programa, una vez instalado o ejecutado en un ordenador, una red programada de ordenadores o cualquier aparato programable, da lugar a un producto o proceso patentable de conformidad con los art�culos 4 y 4 bis.
art6	es	Los derechos derivados de patentes otorgadas a invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva no afectar�n a los actos permitidos en virtud de los art�culos 5 y 6 de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular en aplicaci�n de sus preceptos sobre la descompilaci�n y la interoperabilidad.
art6a	es	Art�culo 6 bis <P>Los Estados miembros garantizar�n que, siempre que sea necesaria la utilizaci�n de una t�cnica patentada con el �nico objetivo de asegurar la conversi�n de las convenciones utilizadas en dos sistemas de ordenadores o redes diferentes a fin de hacer posible la comunicaci�n y el intercambio rec�proco del contenido de datos, esta utilizaci�n no se considere una violaci�n de la patente.
art7	es	La Comisi�n seguir� de cerca el impacto de la protecci�n otorgada por las patentes respecto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, en particular las PYME y el comercio electr�nico.
art8a	es	Art�culo 8 bis <P>Teniendo en cuenta el seguimiento realizado de conformidad con el art�culo 7 y el informe elaborado de conformidad con el art�culo 8, la Comisi�n evaluar� el impacto de la presente Directiva y, si procede, presentar� al Parlamento Europeo y al Consejo propuestas para modificar la legislaci�n.
art9_1	es	1.Los Estados miembros adoptar�n, a m�s tardar dieciocho meses despu�s de su entrada en vigor, las disposiciones legislativas, reglamentarias y administrativas necesarias para dar cumplimiento a lo dispuesto en la presente Directiva. Informar�n de ello inmediatamente a la Comisi�n.
rec1	fi	Sis�markkinoiden toteutumisen my�t� vapaan liikkuvuuden esteet ja kilpailua v��rist�v�t tekij�t poistuvat ja luodaan innovaatiotoiminnalle ja investoinneille suotuisa toimintaymp�rist�. T�ss� yhteydess� keksint�jen suojaaminen patentein on sis�markkinoiden onnistumista edist�v� keskeinen tekij�. Tietokoneella toteutettujen keksint�jen tehokas, avoin ja yhdenmukainen suoja kaikissa j�senvaltioissa on olennaisen t�rke�� alan investointien jatkumiseksi ja kannustamiseksi.
rec5	fi	N�in ollen tietokoneella toteutettujen keksint�jen patentoitavuutta s��ntelev�t oikeudelliset s��nn�t olisi yhdenmukaistettava, jotta varmistetaan, ett� tuloksena saavutettavan oikeusvarmuuden ja patentoitavuudelle asetettavan vaatimustason ansiosta yritykset voivat saada mahdollisimman paljon etua innovointiprosesseistaan, ja edistet��n investointeja ja innovaatiotoimintaa. <P>Oikeusvarmuus taataan my�s sill�, ett� kun t�m�n direktiivin soveltamiseen liittyy ep�ilyksi�, kansalliset tuomioistuimet voivat pyyt�� ja ylimm�n oikeusasteen kansallisten tuomioistuinten on pyydett�v� ratkaisua yhteis�jen tuomioistuimelta.
rec7a	fi	T�ll� direktiivill� ei pyrit� muuttamaan Euroopan patenttiyleissopimusta, vaan est�m��n, ett� sen teksti� voidaan tulkita eri tavoin.
rec11	fi	Kaikki keksinn�t ja erityisesti tietokoneella toteutetut keksinn�t ovat patentoitavissa edellytt�en, ett� ne ovat teollisesti k�ytt�kelpoisia, uusia ja keksinn�llisi�. Jotta niit� voidaan pit�� keksinn�llisin�, niiden on tuotava lis�ys vallitsevaan tekniikan tasoon.
rec12	fi	N�in ollen vaikka tietokoneella toteutettu keksint� kuuluu jo luonteensa puolesta tekniikan alaan, on t�rke�� tehd� selv�ksi, ett� jos keksint� ei tuo lis�yst� vallitsevaan tekniikan tasoon, esimerkiksi silloin, kun lis�ys vallitsevaan tasoon ei ole luonteeltaan tekninen, keksint� ei ole keksinn�llinen, joten se ei ole patentoitavissa. <P>Arvioitaessa keksinn�n keksinn�llisyytt� k�ytet��n tavallisesti ongelma ja ratkaisu -l�hestymistapaa sen m��rittelemiseksi, onko kyseess� jokin tekninen ongelma, johon pyrit��n l�yt�m��n ratkaisu. Jos mit��n teknist� ongelmaa ei ole, keksinn�n ei voida katsoa tuovan mit��n teknist� lis�yst� vallitsevaan tekniikan tasoon.
rec13a	fi	Pelk�st��n se seikka, ett� menetelm�, joka ei muutoin ole patentoitavissa, on toteutettu sellaisella v�lineell� kuin tietokone, ei sin�ll��n ole riitt�v� osoitus lis�yksest� tekniikan tasoon. N�in ollen mit��n sellaista tietokoneella toteutettua liiketoiminnan menetelm�� tai jotakin muuta menetelm��, jossa ainoa lis�ys vallitsevaan tekniikan tasoon ei ole ominaisuuksiltaan tekninen, ei voida pit�� patentoitavissa olevana keksint�n�.
rec13b	fi	Jos lis�ys vallitsevaan tekniikan tasoon koskee ainoastaan sellaista keksinn�n osaa, joka ei ole patentoitavissa, keksint�� ei voida patentoida riippumatta siit�, kuinka asia on esitetty patenttivaatimuksessa. Esimerkiksi vaatimusta lis�yksest� tekniikan tasoon ei voida kiert�� pelk�st��n esitt�m�ll� patenttivaatimuksessa, mit� teknisi� v�lineit� on k�ytetty.
rec13c	fi	Algoritmi ei ole sin�ll��n luonteeltaan tekninen ja siksi sit� ei voida pit�� teknisen� keksint�n�. Menetelm�, johon sis�ltyy algoritmi, voisi kuitenkin olla patentoitavissa edellytt�en, ett� menetelm�� k�ytet��n teknisen ongelman ratkaisuun. T�llaiselle menetelm�lle my�nnetyll� patentilla ei kuitenkaan olisi yksinoikeutta itse algoritmiin tai sen k�ytt��n muussa kuin patentin m��r��m�ss� tarkoituksessa.
rec13d	fi	Patentin antamien yksinoikeuksien laajuus m��ritell��n patenttivaatimuksissa. Tietokoneella toteutetuista keksinn�ist� on teht�v� vaatimus, joka koskee joko ohjelmoidun laitteen kaltaista tuotetta tai t�llaisessa laitteessa toteutettua prosessia. Samoin kun ohjelmien yksitt�isi� osia k�ytet��n yhteyksiss�, joihin ei liity mink��n voimassa olevan patenttivaatimuksen alaisen tuotteen tai prosessin toteutumista, t�llainen k�ytt� ei merkitse patentin loukkausta.
rec14	fi	Tietokoneella toteutettujen keksint�jen oikeudellinen suoja ei edellyt� erillist� s��ntely�, joka korvaisi kansallisten patenttis��d�sten s��nn�t. Tietokoneella toteutettujen keksint�jen oikeudellisen suojan on edelleen perustuttava p��osin kansallisten patenttis��d�sten s��nt�ihin sellaisina kuin niit� on t�m�n direktiivin mukaisesti tietyilt� osin mukautettu tai t�ydennetty. T�ll� direktiivill� pyrit��n ainoastaan selvent�m��n keksint�jen nykyist� oikeudellista asemaa ottaen huomioon Euroopan patenttiviraston k�yt�nt� lain oikeusvarmuuden, avoimuuden ja selkeyden varmistamisessa ja v�ltt�m��n sellaisten menetelmien patentoimista, jotka eiv�t ole patentoitavissa, mist� esimerkkin� voidaan mainita liiketoimintaan liittyv�t menetelm�t.
rec16	fi	Euroopan teollisuuden kilpailuasema suhteessa t�rkeimpiin kauppakumppaneihin paranee, jos tietokoneella toteutettujen keksint�jen oikeudellisen suojan nykyiset erot poistetaan ja oikeudellisen tilanteen avoimuutta lis�t��n. Ottaen huomioon, ett� perinteinen valmistusteollisuus pyrkii nyky��n siirt�m��n tuotantoaan matalapalkkamaihin Euroopan unionin ulkopuolelle, teollis- ja tekij�noikeuksien suojelun ja etenkin patenttisuojan merkitys on ilmeisen t�rke�.
rec17	fi	<I>T�ll� direktiivill�</I> ei saisi rajoittaa kilpailus��nt�jen eik� etenk��n perustamissopimuksen 81 ja 82 artiklan soveltamista.
rec18	fi	T�m�n direktiivin soveltamisalaan kuuluville keksinn�ille my�nnettyjen patenttien suomat oikeudet eiv�t vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY 5 ja 6artiklan mukaisesti sallittuihin toimiin eiv�tk� analysointiin ja yhteentoimivuuteen. Erityisesti toimissa, joissa direktiivin 91/250/ETY 5 ja 6 artiklan mukaisesti ei tarvita oikeudenomistajan lupaa liittyen oikeudenomistajalle kuuluviin tietokoneohjelman tekij�noikeuksiin ja joihin t�llainen lupa tarvittaisiin ilman direktiivin 91/250/ETY 5 ja 6artiklan m��r�yksi�, ei tarvita oikeudenomistajan lupaa liittyen oikeudenomistajan tietokoneohjelmia koskeviin patenttioikeuksiin.
art2a	fi	a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art3	fi	Poistetaan.
art4	fi	Jotta tietokoneella toteutettu keksint� olisi patentoitavissa, sen on oltava uusi, keksinn�llinen ja teollisesti k�ytt�kelpoinen. Tietokoneella toteutettu keksint� on keksinn�llinen vain, kun se tuo lis�yksen tekniikan tasoon. <P>J�senvaltioiden on varmistettava, ett� tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon on v�ltt�m�t�n keksinn�llisyyden edellytys. <P>Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon vallitseva tekniikan taso ja kokonaisuutena tarkasteltava patenttivaatimus, johon t�ytyy sis�lty� teknisi� piirteit� riippumatta siit�, liittyyk� siihen muita kuin teknisi� piirteit�.
art4a	fi	4 a artikla <P>Poikkeukset patentoitavuudesta <P>Tietokoneella toteutettua keksint�� ei katsota lis�ykseksi tekniikan tasoon pelk�st��n sen perusteella, ett� se edellytt�� tietokoneen tai muun laitteiston k�ytt��. Patentoitavissa eiv�t siis ole keksinn�t, joissa suoritetaan tietokoneohjelmien avulla sellaisia liiketoiminnallisia, matemaattisia tai muita toimia, joilla ei saada aikaan muuta teknist� vaikutusta kuin normaali fyysinen vuorovaikutus ohjelman ja tietokoneen, verkon tai muun sellaisen ohjelmoitavan laitteen v�lill�, jossa ohjelma toimii.
art5	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutetulle keksinn�lle voidaan hakea patenttisuojaa tuotteena, toisin sanoen ohjelmoituna tietokoneena, ohjelmoituna tietokoneiden verkkona tai muuna ohjelmoituna laitteena, tai kyseisen tietokoneen, tietokoneiden verkon tai laitteen suorittamana prosessina, joka pannaan t�yt�nt��n ohjelmiston avulla. <P>2.Tietokoneohjelmaa sellaisenaan tai tietov�lineelle tallennettua tai signaalin v�lityksell� toimitettavaa tietokoneohjelmaa koskeva patenttivaatimus voidaan hyv�ksy�, jos ohjelma tietokoneeseen, ohjelmoituun tietokoneverkkoon tai muuhun ohjelmoitavaan laitteeseen asennettuna tai sill� suoritettuna antaa tulokseksi tuotteen tai prosessin, joka voidaan patentoida 4 ja 4 a artiklan mukaisesti.
art6	fi	T�m�n direktiivin soveltamisalaan kuuluville keksinn�ille my�nnettyihin patentteihin liittyv�t oikeudet eiv�t vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY 5ja 6artiklan mukaisesti sallittuihin toimiin ottaen huomioon erityisesti n�iden artiklojen sis�lt�m�t analysointia ja yhteentoimivuutta koskevat s��nn�kset.
art6a	fi	6 a artikla <P>J�senvaltioiden on varmistettava, ett� aina kun on k�ytett�v� patentoitua tekniikkaa siihen yksinomaiseen tarkoitukseen, ett� varmistetaan kahden eri tietokonej�rjestelm�n tai verkon k�yt�nt�jen muuntaminen niiden v�lisen yhteyden ja tietojenvaihdon mahdollistamiseksi, t�llaista k�ytt�� ei katsota patentinloukkaukseksi.
art7	fi	Komissio seuraa tietokoneella toteutettujen keksint�jen patenttisuojan vaikutuksia innovaatiotoimintaan ja kilpailuun Euroopan laajuisesti ja kansainv�lisesti sek� niiden vaikutuksia eurooppalaisiin yrityksiin, etenkin pieniin ja keskisuuriin yrityksiin, ja s�hk�iseen kaupank�yntiin.
art8	fi	(b)siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; <P>(c)siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi; ja
art8a	fi	8 a artikla <P>Komission on arvioitava t�m�n direktiivin vaikutusta 7 artiklan mukaisesti suoritetun valvonnan ja 8 artiklan mukaisesti laaditun kertomuksen perusteella ja tarvittaessa teht�v� Euroopan parlamentille ja neuvostolle ehdotuksia lains��d�nn�n muuttamiseksi.
art8ca	fi	c a) mahdollisista vaikeuksista, joita on aiheutunut tietokoneella toteutettujen keksint�jen patenttisuojan ja direktiivin 91/250/ETY mukaisen tietokoneohjelmien tekij�noikeussuojan v�lisest� suhteesta, ja mahdollisista tietokoneella toteutettuihin keksint�ihin liittyvist� patenttij�rjestelm�n v��rink�yt�ksist�.
art8cb	fi	(c b) siit�, olisiko suotavaa ja laillisesti mahdollista ottaen huomioon yhteis�n kansainv�liset velvoitteet ottaa k�ytt��n ns. "armonaika" kaikenlaisten keksint�jen patenttihakemukseen sis�ltyville osille, jotka julkistetaan ennen patenttihakemuksen tekemisp�iv��.
art9_1	fi	1.J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n 18 kuukauden kuluttua direktiivin voimaantulosta. Niiden on ilmoitettava t�st� komissiolle viipym�tt�.
rec1	fr	La r�alisation du march� int�rieur implique que l'on �limine les restrictions � la libre circulation et les distorsions � la concurrence, tout en cr�ant un environnement favorable � l'innovation et � l'investissement. Dans ce contexte, la protection des inventions par brevet est un �l�ment essentiel du succ�s du march� int�rieur. Une protection effective, transparente et harmonis�e des inventions mises en oeuvre par ordinateur dans tous les �tats membres est essentielle pour maintenir et encourager les investissements dans ce domaine.
rec5	fr	En cons�quence, les r�gles de droit r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre harmonis�es de fa�on � assurer que la s�curit� juridique qui en r�sulte et le niveau des crit�res de brevetabilit� permettent aux entreprises innovatrices de tirer le meilleur parti de leur processus inventif et stimulent l'investissement et l'innovation. <P>La s�curit� juridique sera �galement assur�e par le fait que, en cas de doute quant � l'interpr�tation de la pr�sente directive, les juridictions nationales ont la possibilit�, et les juridictions nationales de derni�re instance l'obligation, de demander un jugement de la Cour de justice.
rec7a	fr	La pr�sente directive ne vise pas � modifier la Convention sur le brevet europ�en, mais � �viter des interpr�tations divergentes du texte de ladite Convention.
rec11	fr	Pour �tre brevetables, les inventions en g�n�ral, et les inventions mises en oeuvre par ordinateur en particulier, doivent �tre susceptibles d'application industrielle, �tre nouvelles et impliquer une activit� inventive. Pour r�pondre au crit�re de l'activit� inventive, les inventions mises en oeuvre par ordinateur devraient apporter une contribution technique � l'�tat de la technique.
rec12	fr	En cons�quence, bien qu'une invention mise en oeuvre par ordinateur appartienne, par nature, � un domaine technique, il importe de pr�ciser que, lorsqu'une invention n'apporte pas de contribution technique � l'�tat de la technique, parce que, par exemple, sa contribution sp�cifique ne rev�t pas un caract�re technique, elle ne r�pond pas au crit�re de l'activit� inventive et ne peut donc faire l'objet d'un brevet. <P>Pour �valuer le crit�re de l'activit� inventive, il est habituel d'appliquer l'approche probl�me-solution, afin d'�tablir qu'il existe un probl�me technique � r�soudre. S'il n'existe pas de probl�me technique, l'invention ne peut �tre consid�r�e comme apportant une contribution technique � l'�tat de la technique.
rec13a	fr	Toutefois, la simple mise en oeuvre d'une m�thode, par ailleurs non brevetable, sur un appareil tel qu'un ordinateur ne suffit pas, en soi, � prouver l'existence d'une contribution technique. En cons�quence, une m�thode destin�e � l'exercice d'activit�s �conomiques, ou une autre m�thode, mise en oeuvre par ordinateur, dont la seule contribution � l'�tat de la technique n'est pas de nature technique ne peut constituer une invention brevetable.
rec13b	fr	L'invention n'est en aucun cas brevetable si la contribution � l'�tat de la technique se rapporte uniquement � des �l�ments non brevetables, quelle que soit la fa�on dont l'objet du brevet est pr�sent� dans la revendication. Ainsi, l'exigence d'une contribution technique ne peut �tre contourn�e uniquement en sp�cifiant des moyens techniques dans la revendication de brevet.
rec13c	fr	En outre, un algorithme est, par nature, non technique et ne peut donc constituer une invention technique. Une m�thode recourant � un algorithme pourrait n�anmoins �tre brevetable, dans la mesure o� elle est utilis�e pour r�soudre un probl�me technique. Toutefois, tout brevet accord� pour cette m�thode n'�tablirait pas un monopole sur l'algorithme lui-m�me ou sur son utilisation dans des contextes non pr�vus dans le brevet.
rec13d	fr	Le champ d'application des droits exclusifs conf�r�s par tout brevet est d�fini par les revendications. Les inventions mises en oeuvre par ordinateur doivent �tre revendiqu�es en faisant r�f�rence � un produit, tel qu'un appareil programm�, ou � un proc�d� r�alis� sur un tel appareil. En cons�quence, lorsque des �l�ments individuels de logiciel sont utilis�s dans des contextes qui ne comportent pas la r�alisation d'un produit ou d'un proc�d� faisant l'objet d'une revendication valable, cette utilisation ne constitue pas une contrefa�on de brevet.
rec14	fr	La protection juridique des inventions mises en oeuvre par ordinateur ne n�cessite pas l'�tablissement d'une l�gislation distincte en lieu et place des dispositions du droit national des brevets. Les r�gles du droit national des brevets continuent de former la base de r�f�rence de la protection juridique des inventions mises en oeuvre par ordinateur. La pr�sente directive clarifie simplement la situation juridique actuelle en tenant compte des pratiques de l'Office europ�en des brevets, en vue d'assurer la s�curit� juridique, la transparence et la clart� de la l�gislation et d'�viter toute d�rive vers la brevetabilit� de m�thodes non brevetables, telles que les m�thodes destin�es � l'exercice d'activit�s �conomiques.
rec16	fr	La position concurrentielle de l'industrie europ�enne vis-�-vis de ses principaux partenaires commerciaux sera am�lior�e si les diff�rences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur sont �limin�es et si la situation juridique est transparente. �tant donn� la tendance actuelle, qui voit l'industrie manufacturi�re traditionnelle d�placer son activit� vers des �conomies o� les co�ts sont faibles, l'importance de la protection de la propri�t� intellectuelle, et en particulier de la protection assur�e par le brevet, est �vidente.
rec17	fr	La pr�sente directive ne devrait pas pr�juger de l'application des r�gles de concurrence, en particulier des articles81 et82 du trait�.
rec18	fr	Les droits conf�r�s par les brevets d'invention d�livr�s dans le cadre de la pr�sente directive ne portent pas atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit�. En particulier, les actes qui, en vertu des articles 5 et 6 de la directive 91/250/CEE, ne n�cessitent pas l'autorisation du titulaire du droit, au regard des droits d'auteur de ce titulaire aff�rents ou attach�s � un programme d'ordinateur, et qui, en l'absence des articles 5 et 6 de la directive 91/250/CEE, n�cessiteraient cette autorisation, ne n�cessitent pas l'autorisation du titulaire du droit, au regard des droits de brevet de ce titulaire aff�rents ou attach�s au programme d'ordinateur.
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute invention dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art3	fr	supprim�
art4	fr	Pour �tre brevetable, une invention mise en oeuvre par ordinateur doit �tre susceptible d'application industrielle, �tre nouvelle et impliquer une activit� inventive. Pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur doit apporter une contribution technique. <P>Les �tats membres veillent � ce que le fait qu'une invention mise en oeuvre par ordinateur apporte une contribution technique constitue une condition n�cessaire � l'existence d'une activit� inventive. <P>La contribution technique est �valu�e en prenant en consid�ration l'�tat de la technique et l'objet de la revendication de brevet consid�r� dans son ensemble, qui doit comprendre des caract�ristiques techniques, accompagn�es ou non de caract�ristiques non techniques.
art4a	fr	Article 4 bis <P>Exclusions de la brevetabilit� <P>Une invention mise en oeuvre par ordinateur n'est pas consid�r�e comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, d'un r�seau ou d'un autre appareil programmable. En cons�quence, ne sont pas brevetables les inventions impliquant des programmes d'ordinateurs, qui mettent en oeuvre des m�thodes destin�es � l'exercice d'activit�s �conomiques, des m�thodes math�matiques ou d'autres m�thodes, si ces inventions ne produisent pas d'effets techniques en dehors des interactions physiques normales entre un programme et l'ordinateur, le r�seau ou un autre appareil programmable sur lequel il est ex�cut�.
art5	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme. <P>2.Une revendication portant sur un programme d'ordinateur, en tant que tel, enregistr� sur un support ou livr� par un signal, n'est autoris�e que si ce programme, une fois charg� ou ex�cut� sur un ordinateur, un r�seau informatique ou un autre appareil programmable, assure la mise en oeuvre d'un produit ou r�alise un proc�d� brevetable en vertu des articles 4 et 4 bis.
art6	fr	Les droits conf�r�s par les brevets d'invention d�livr�s dans le cadre de la pr�sente directive ne portent pas atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit�.
art6a	fr	Article 6 bis <P>Les �tats membres veillent � ce que, lorsque le recours � une technique brevet�e est n�cessaire � la seule fin d'assurer la conversion des conventions utilis�es dans deux syst�mes ou r�seaux informatiques diff�rents, de fa�on � permettre entre eux la communication et l'�change de donn�es, ce recours ne soit pas consid�r� comme une contrefa�on de brevet.
art7	fr	7.La Commission surveille l'incidence de la protection par brevet des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes, en particulier les petites et moyennes entreprises, et le commerce �lectronique.
art8a	fr	Article 8 bis<P>La Commission �value l'impact de la pr�sente directive � la lumi�re du suivi r�alis� conform�ment � l'article7 et du rapport � r�diger conform�ment � l'article8 et pr�sente, si n�cessaire, des propositions en vue de modifier la l�gislation au Parlement europ�en et au Conseil.
art9_1	fr	1.Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard dix-huit mois apr�s son entr�e en vigueur et en informent imm�diatement la Commission.
rec1	it	La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza nonch� la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto la protezione delle invenzioni mediante i brevetti � un elemento essenziale per il successo del mercato interno. Una protezione efficace, trasparente ed armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri � indispensabile per mantenere e stimolare gli investimenti in questo campo.
rec5	it	� pertanto necessario armonizzare le disposizioni di legge che disciplinano la brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici, onde garantire che la certezza giuridica che ne risulter� e il livello dei requisiti richiesti per la brevettabilit� permettano alle imprese innovative di ricavare il massimo vantaggio dal loro processo inventivo e stimolare gli investimenti e l'innovazione. <P>La certezza giuridica viene altres� garantita dal fatto che, in caso di dubbio sull'interpretazione della presente direttiva, i tribunali nazionali possono e i tribunali nazionali di ultima istanza devono chiedere una sentenza alla Corte di giustizia.
rec7a	it	Con la presente direttiva non si intende modificare la Convenzione sul brevetto europeo, ma bens� evitare che possano esistere interpretazioni divergenti del relativo testo.
rec11	it	Per poter essere brevettabili, le invenzioni in generale e le invenzioni attuate per mezzo di elaboratori elettronici in particolare devono essere suscettibili di applicazione industriale, presentare un carattere di novit� e implicare un'attivit� inventiva. Le invenzioni attuate per mezzo di elaboratori elettronici devono costituire un contributo tecnico allo stato dell'arte per poter essere considerate implicanti un'attivit� inventiva.
rec12	it	Di conseguenza, anche se una invenzione attuata per mezzo di elaboratori elettronici appartiene per sua stessa natura al settore della tecnologia � importante stabilire chiaramente che, se un'invenzione non costituisce un contributo tecnico allo stato dell'arte, come nel caso in cui, ad esempio, il suo contributo specifico non presenta un carattere tecnico, non pu� essere considerata implicante un'attivit� inventiva e quindi non � brevettabile. <P>Qualora si valuti se sia presente un'attivit� inventiva, si � soliti applicare l'approccio "problema-soluzione" onde stabilire se vi sia un problema tecnico da risolvere. Se non viene riscontrato alcun problema tecnico, in tal caso l'invenzione non pu� essere considerata contributo tecnico allo stato dell'arte.
rec13a	it	Tuttavia, la semplice attuazione di un metodo altrimenti non brevettabile su di un apparecchio come un elaboratore non � di per s� sufficiente per giustificare la conclusione che un contributo tecnico sia presente. Di conseguenza, un metodo per attivit� commerciali o di altro tipo che venga attuato per mezzo di elaboratori elettronici, in cui l'unico contributo allo stato dell'arte non sia tecnico, non pu� costituire un'invenzione brevettabile.
rec13b	it	Se il contributo allo stato dell'arte � relativo unicamente a materiali non brevettabili, non vi pu� essere un'invenzione brevettabile, indipendentemente dal modo in cui vengano presentati nelle rivendicazioni. Per esempio, il requisito di contributo tecnico non pu� essere eluso semplicemente specificando mezzi tecnici nelle rivendicazioni di brevetto.
rec13c	it	Inoltre, un algoritmo � intrinsecamente non tecnico e, pertanto, non pu� costituire un'invenzione tecnica. Tuttavia, un metodo che comporti l'utilizzazione di un algoritmo pu� essere brevettabile purch� venga usato per risolvere un problema tecnico. Tuttavia, un brevetto concesso per tale metodo non monopolizza lo stesso algoritmo o la sua utilizzazione in contesti non previsti nel brevetto.
rec13d	it	La portata dei diritti esclusivi conferiti da un qualsiasi brevetto sono definiti dalle relative rivendicazioni. Le invenzioni attuate per mezzo di elaboratori elettronici vanno rivendicate con riferimento a un prodotto, quale un apparecchio programmato, o a un processo svolto da tale apparecchio. Di conseguenza, l'utilizzo di singoli elementi di un software in contesti che non comportano la realizzazione di un prodotto o un processo regolarmente rivendicato non costituisce una violazione di brevetto.
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non richiede una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici. La presente direttiva semplicemente chiarisce l'attuale posizione giuridica tenendo presenti le pratiche dell'Ufficio europeo dei brevetti in vista di garantire la certezza giuridica, la trasparenza e la chiarezza della legislazione ed evitare qualsiasi transizione verso la brevettabilit� di metodi non brevettabili, come i metodi per attivit� commerciali.
rec16	it	La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali sar� rafforzata dall'eliminazione delle differenze attuali nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici e dalla trasparenza della situazione giuridica. Data l'attuale tendenza dell'industria manifatturiera tradizionale di dislocare le proprie attivit� verso economie a basso costo al di fuori dell'Unione europea, l'importanza della tutela della propriet� intellettuale e, in particolare, della tutela del brevetto � di per s� evidente.
rec17	it	La presente direttiva lascerebbe impregiudicata l'applicazione delle norme in materia di concorrenza, in particolare gli articoli 81 e 82 del trattato.
rec18	it	I diritti conferiti dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lasciano impregiudicate le facolt� riconosciute ai sensi degli articoli 5 e 6 della direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare sulla base delle disposizioni relative alla decompilazione e all'interoperabilit�. Nello specifico, gli atti che, a norma degli articoli 5 e 6 della direttiva 91/250/CEE, non sono soggetti all'autorizzazione del titolare del diritto per quel che concerne i diritti d'autore del titolare attinenti ad un programma per elaboratore o in esso contenuti e che, fatto salvo per gli articoli 5 e 6 della direttiva 91/250/CEE, necessitano di tale autorizzazione, non devono essere soggetti all'autorizzazione del titolare del diritto per quel che concerne i diritti del brevettatore attinenti al programma per elaboratore o in esso contenuti.
art2a	it	(a)"invenzione attuata per mezzo di elaboratori elettronici", un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta una o pi� caratteristiche che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art3	it	soppresso
art4	it	Onde poter essere brevettabile, un'invenzione attuata per mezzo di elaboratori elettronici deve essere atta ad un'applicazione industriale, presentare un carattere di novit� ed implicare un'attivit� inventiva. Per implicare un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici deve arrecare un contributo tecnico. <P>Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici, che apporti un contributo tecnico, costituisca una condizione necessaria ad un'attivit� inventiva. <P>Il contributo tecnico � valutato considerando lo stato dell'arte e l'oggetto della rivendicazione di brevetto nel suo insieme, che deve comprendere le caratteristiche tecniche, indipendentemente dal fatto che tali caratteristiche siano o non siano accompagnate da caratteristiche non tecniche.
art4a	it	Articolo 4 bis <P>Esclusioni dalla brevettabilit�
art5	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata come prodotto, ossia come elaboratore programmato, rete di elaboratori programmati o altro apparecchio programmato, o come processo realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software. <P>2.Una rivendicazione di un programma per elaboratori elettronici relativa al programma in s� per s�, collocato su un vettore oppure come segnale, � ammissibile solo se tale programma, una volta installato o in funzione su un elaboratore elettronico, una rete di elaboratori o altro apparecchio programmabile, dia origine ad un prodotto o realizzi un processo brevettabile ai sensi degli articoli 4 e 4-bis.
art6	it	I diritti conferiti dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lasciano impregiudicate le facolt� riconosciute ai sensi degli articoli 5 e 6 della direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare sulla base delle disposizioni relative alla decompilazione e all'interoperabilit�.
art6a	it	Articolo 6 bis <P>Gli Stati membri assicurano che, nel caso in cui l'uso di una tecnica brevettata sia necessario al solo fine di garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, cos� da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto.
art7	it	La Commissione osserva gli effetti della protezione conferita dal brevetto per quanto riguarda le invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, in particolare le piccole e medie imprese, e il commercio elettronico.
art8a	it	Articolo 8 bis <P>In virt� del monitoraggio effettuato ai sensi dell'articolo 7 e della relazione da elaborare ai sensi dell'articolo 8, la Commissione procede a un riesame della presente direttiva e, ove necessario, presenta proposte di modifica al Parlamento europeo e al Consiglio.
art8c	it	c)il verificarsi di difficolt� negli Stati membri nel caso in cui i criteri della novit� e dell'attivit� inventiva non siano esaminati prima del rilascio di un brevetto e le eventuali misure da adottare per risolvere tali difficolt�, e
art9_1	it	l.Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro diciotto mesi dalla sua entrata in vigore. Essi ne informano immediatamente la Commissione.
rec1	nl	Voor de totstandbrenging van de interne markt is het nodig beperkingen voor het vrije verkeer en concurrentieverstoringen op te heffen, en tegelijkertijd een gunstig klimaat te scheppen voor innovatie en investeringen. In deze context is de bescherming van uitvindingen een essentieel element voor het succes van de interne markt. Doeltreffende, transparante en geharmoniseerde bescherming van in computers ge�mplementeerde uitvindingen in alle lidstaten is van essentieel belang om de investeringen op dit gebied in stand te houden en aan te moedigen.
rec5	nl	Om die reden moeten de rechtsregels betreffende de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen worden geharmoniseerd, om ervoor te zorgen dat de daaruit voortvloeiende rechtszekerheid en het peil van de voorwaarden waaraan voor octrooieerbaarheid moet worden voldaan, de ondernemingen in staat stellen optimaal profijt te trekken van hun uitvindingsproces en een stimulans zijn voor investeringen en innovatie. <P>De rechtszekerheid wordt tevens gewaarborgd door het feit dat ingeval van twijfel over de interpretatie van deze richtlijn nationale rechtscolleges kunnen en nationale rechtscolleges in laatste ressort moeten verzoeken om een uitspraak van het Hof van Justitie.
rec7a	nl	De onderhavige richtlijn heeft niet tot doel het Europees Octrooiverdrag te wijzigen, maar moet voorkomen dat de bepalingen ervan verschillend worden ge�nterpreteerd.
rec11	nl	Om octrooieerbaar te zijn moeten uitvindingen in het algemeen en in computers ge�mplementeerde uitvindingen in het bijzonder geschikt zijn voor industri�le toepassing, nieuw zijn en op uitvinderswerkzaamheid berusten. Om op uitvinderswerkzaamheid te berusten moeten in computers ge�mplementeerde uitvindingen een technische bijdrage tot de stand van de techniek leveren.
rec12	nl	Dienovereenkomstig is het, ook al hoort een in computers ge�mplementeerde uitvinding uit de aard der zaak thuis op het terrein van de technologie, van belang dat duidelijk vooropgesteld wordt dat, ingeval een uitvinding geen technische bijdrage tot de stand van de techniek levert, zoals bijvoorbeeld het geval zou zijn wanneer de specifieke bijdrage ervan geen technisch karakter heeft, niet op uitvinderswerkzaamheid <I>berust</I> en bijgevolg niet octrooieerbaar <I>is</I>. <P>Bij de beoordeling of er sprake is van uitvinderswerkzaamheid is het gebruikelijk de probleem- en oplossingbenadering toe te passen om vast te stellen of er een technisch probleem moet worden opgelost. Indien er geen technisch probleem bestaat kan de uitvinding niet geacht worden een technische bijdrage aan de stand van de techniek te leveren.
rec13a	nl	Echter louter de implementatie van een anders niet octrooieerbare methode op een apparaat zoals een computer, garandeert op zich niet afdoende dat er van een technische bijdrage sprake is. Bijgevolg kan een in computers ge�mplementeerde bedrijfsmethode of een andere methode waarbij de enige bijdrage aan de stand van de techniek van niet-technische aard is, geen octrooieerbare uitvinding vormen.
rec13b	nl	Indien de bijdrage aan de stand van de techniek slechts verband houdt met niet-octrooieerbare zaken kan er geen sprake zijn van een octrooieerbare uitvinding, ongeacht hoe die zaak in de aanvraag gepresenteerd wordt. Bijvoorbeeld het vereiste van een technische bijdrage kan niet omzeild worden door louter specificatie van technische middelen in de octrooiaanvraag.
rec13c	nl	Bovendien is een algoritme inherent van niet-technische aard en kan derhalve geen technische uitvinding vormen. Desalniettemin zou een methode waarbij een algoritme gebruikt wordt, octrooieerbaar kunnen zijn op voorwaarde dat de methode gebruikt wordt om een technisch probleem op te lossen. Echter elk octrooi dat verleend wordt voor een dergelijke methode zou niet de algoritme zelf of het gebruik ervan in een context die niet in het octrooi voorzien is, monopoliseren.
rec13d	nl	De omvang van de exclusieve rechten die een octrooi verleent, worden bepaald door de conclusies. Voor in computers ge�mplementeerde uitvindingen moet het octrooi worden opge�ist voor een product, zoals een geprogrammeerd apparaat, of voor een werkwijze die door een dergelijk apparaat wordt toegepast. Bijgevolg vormt het gebruik van individuele software-elementen in een omgeving die niet de realisatie inhoudt van een op geldige wijze opge�ist product of werkwijze, geen octrooi-inbreuk.
rec14	nl	De rechtsbescherming van in computers ge�mplementeerde uitvindingen vereist niet dat wordt voorzien in een afzonderlijk rechtsinstrument ter vervanging van de regels van het nationale octrooirecht. De regels van het nationale octrooirecht blijven de essenti�le basis voor de rechtsbescherming van in computers ge�mplementeerde uitvindingen. Deze richtlijn verduidelijkt de huidige juridische situatie, mede gelet op de praktijken van het Europees Octrooibureau ter waarborging van de rechtszekerheid, doorzichtigheid en duidelijkheid in de wetgeving en ter vermijding van een eventueel afglijden naar de octrooieerbaarheid van niet-octrooieerbare methoden, zoals bedrijfsmethoden.
rec16	nl	De concurrentiepositie van het Europese bedrijfsleven ten opzichte van zijn voornaamste handelspartners zal worden verbeterd indien de bestaande verschillen in de rechtsbescherming van in computers ge�mplementeerde uitvindingen worden opgeheven en de rechtssituatie transparant is. Gelet op de huidige tendens voor de traditionele verwerkende industrie om haar werkzaamheden te verplaatsen naar lageloneneconomie�n buiten de Europese Unie, is het belang van de bescherming van intellectuele eigendom en met name van octrooien vanzelfsprekend.
rec17	nl	Deze richtlijn mag geen afbreuk doen aan de toepassing van de mededingingsregels, inzonderheid de artikelen 81 en 82 van het Verdrag.
rec18	nl	De rechten die zijn toegekend met octrooien die binnen de werkingssfeer van deze richtlijn voor uitvindingen zijn verleend, laten handelingen onverlet die zijn toegestaan op grond van de artikelen 5 en 6 van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit. In het bijzonder is voor handelingen waarvoor op grond van de artikelen 5 en 6 van Richtlijn 91/250/EEG geen toestemming van de rechthebbende is vereist in verband met diens auteursrechten in of ten aanzien van een computerprogramma en waarvoor dergelijke toestemming zonder de artikelen 5 en 6 van Richtlijn 91/250/EEG wel zou zijn vereist, geen toestemming van de rechthebbende vereist in verband met de octrooirechten die deze in of ten aanzien van het computerprogramma heeft.
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art3	nl	Schrappen
art4	nl	Om octrooieerbaar te zijn moet een in computers ge�mplementeerde uitvinding geschikt zijn voor industri�le toepassing, nieuw zijn en op uitvinderswerkzaamheid te berusten. Om op uitvinderswerkzaamheid te berusten moet een in computers ge�mplementeerde uitvinding een technische bijdrage leveren. <P>De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding die een technische bijdrage levert, een noodzakelijke voorwaarde voor het bestaan van uitvinderswerkzaamheid is. <P>De technische bijdrage wordt beoordeeld aan de hand van de stand van de techniek en het toepassingsgebied van de in zijn geheel beschouwde octrooiaanvraag, die technische kenmerken moet omvatten, ongeacht of deze kenmerken gepaard gaan met niet-technische kenmerken.
art4a	nl	Artikel 4 bis<P>Uitzonderingen op de octrooieerbaarheid:</P><P ALIGN="JUSTIFY">Een in computers ge�mplementeerde uitvinding wordt niet geacht een technische bijdrage te leveren louter op grond van het feit dat daarbij gebruikgemaakt wordt van een computer, een netwerk of andere programmeerbare apparatuur. Bijgevolg zijn uitvindingen waarbij gebruikgemaakt wordt van computerprogramma's met toepassing van bedrijfs-, mathematisch of andere methoden en die geen technische resultaten produceren buiten de normale fysieke interactie tussen een programma en de computer, een netwerk of andere programmeerbare apparatuur waarop dit ten uitvoer wordt gebracht, niet octrooieerbaar.
art5	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding kan worden geclaimd als product, dat wil zeggen als een geprogrammeerde computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat, of als een werkwijze die door zo een computer, computernetwerk of apparaat middels het uitvoeren van software wordt toegepast. <P>2.Een octrooiaanspraak op een computerprogramma, als zodanig, opgeslagen op een drager of verspreid in de vorm van een signaal, is toegestaan, als het programma, zodra het op een computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat ge�nstalleerd en uitgevoerd wordt, een product implementeert of een werkwijze uitvoert die octrooieerbaar zijn overeenkomstig de artikelen 4 en 4 bis.
art6	nl	De rechten die zijn toegekend met octrooien die binnen de werkingssfeer van deze richtlijn voor uitvindingen zijn verleend, laten handelingen onverlet die zijn toegestaan op grond van de artikelen 5 en 6 van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit.
art6a	nl	Artikel 6 bis<P>De lidstaten zorgen ervoor dat gebruik van een geoctrooieerde techniek met als enig doel de omzetting van de conventies die in twee verschillende computersystemen of netwerken worden gebruikt om communicatie en gegevensuitwisseling tussen beide mogelijk te maken, niet wordt beschouwd als een inbreuk op het octrooi.
art7	nl	7.De Commissie <I>controleert </I>welke invloed de octrooibescherming voor in computers ge�mplementeerde uitvindingen heeft op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, met name op kleine en middelgrote ondernemingen en op de elektronische handel.
art8a	nl	Artikel 8 bis<P>In het kader van het toezicht overeenkomstig artikel 7 en het verslag dat moet worden opgesteld overeenkomstig artikel 8 evalueert de Commissie de gevolgen van deze richtlijn en dient zij, voorzover noodzakelijk, bij het Europees Parlement en de Raad voorstellen in tot wijziging van de wetgeving.
art9_1	nl	1.De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om uiterlijk 18 maanden na de inwerkingtreding van deze richtlijn eraan te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis.
rec1	pt	A realiza��o do mercado interno implica a elimina��o das restri��es � liberdade de circula��o e �s distor��es da concorr�ncia, criando um ambiente que seja favor�vel � inova��o e ao investimento. Neste contexto, a protec��o dos inventos por meio de patentes � um elemento essencial para o �xito do mercado interno. A protec��o eficaz, transparente e harmonizada dos inventos que implicam programas de computador, em todos os Estados-Membros, � essencial para manter e incentivar o investimento neste dom�nio;
rec5	pt	Consequentemente, as normas jur�dicas que regem a patenteabilidade dos inventos que implicam programas de computador devem ser harmonizadas por forma a assegurar que a certeza jur�dica da� resultante e o n�vel dos requisitos exigidos para a patenteabilidade permitam �s empresas inovadoras tirarem o m�ximo partido do seu processo inventivo e dar um incentivo ao investimento e � inova��o. <P>O princ�pio da certeza jur�dica ficar� tamb�m garantido pelo facto de, em caso de d�vida quanto � interpreta��o da presente Directiva, os tribunais nacionais e as inst�ncias nacionais de �ltimo recurso poderem tentar obter um ac�rd�o do Tribunal de Justi�a das Comunidades Europeias.
rec7a	pt	Com a presente Directiva n�o se pretende alterar a Conven��o sobre a Patente Europeia, mas evitar que possam existir interpreta��es divergentes do seu texto.
rec11	pt	Os inventos, em geral (e os inventos que implicam programas de computador, em particular), para serem patente�veis, dever�o ser suscept�veis de aplica��o industrial, constituir uma novidade e implicar uma actividade inventiva. Para implicarem uma actividade inventiva, os inventos que implicam programas de computador devem dar um contributo t�cnico para o progresso tecnol�gico.
rec12	pt	Deste modo, embora os inventos que implicam programas de computador perten�am, pela sua pr�pria natureza, a um determinado dom�nio da tecnologia, � importante deixar claro que,<P>se um invento n�o der um contributo t�cnico para o progresso tecnol�gico, como aconteceria, por exemplo, se o seu contributo espec�fico n�o tivesse car�cter t�cnico, o invento n�o apresentar� uma actividade inventiva, pelo que n�o ser� patente�vel. <P>Quando se pretende saber se se trata de uma actividade inventiva ou n�o, � h�bito recorrer"se ao chamado m�todo da coloca��o do problema e da busca de uma solu��o, a fim de verificar se h� um problema t�cnico por resolver. Se n�o h� qualquer problema dessa �ndole, ent�o, o invento n�o poder� ser considerado como um contributo t�cnico para os mais recentes avan�os do progresso tecnol�gico.
rec13a	pt	Por�m, a mera aplica��o de um m�todo que n�o seja patente�vel, por outros motivos, num aparelho como, por exemplo, um computador, n�o basta, por si s�, para atestar a ideia de que se est� perante um contributo t�cnico. Em conformidade, um plano de neg�cios que implique a utiliza��o de programas de computador ou qualquer outro m�todo, no qual o �nico contributo para o progresso tecnol�gico n�o se revista de car�cter t�cnico, n�o pode configurar um invento patente�vel.
rec13b	pt	Caso o contributo para o progresso tecnol�gico se relacione apenas com mat�ria n�o patente�vel, n�o se poder� tratar de um invento patente�vel, independentemente da forma como essa mat�ria seja apresentada nas reivindica��es de patente. Por exemplo, a exig�ncia do contributo t�cnico n�o pode ser contornada atrav�s da mera especifica��o dos meios t�cnicos nas reivindica��es de patente.
rec13c	pt	Para al�m disso, um algoritmo, em si mesmo, n�o se reveste de �ndole t�cnica, pelo que n�o pode constituir um invento de car�cter t�cnico. N�o obstante, um m�todo que envolva o recurso a um algoritmo poder� ser patente�vel, na condi��o de esse m�todo ser utilizado para solucionar um problema t�cnico. Uma patente concedida a um m�todo desse tipo n�o poder�, todavia, monopolizar o pr�prio algoritmo ou o respectivo uso em contextos que n�o estejam previstos na patente.
rec13d	pt	O �mbito dos direitos de exclusividade conferidos por uma patente s�o definidos no �mbito das reivindica��es. Os inventos que implicam programas de computador t�m imperativamente de ser reivindicados tomando como refer�ncia, seja um produto (como, por exemplo, um equipamento programado), seja um processo levado a cabo por esse equipamento. Em consequ�ncia, nos casos em que sejam usados elementos individuais de um programa em contextos que n�o envolvam a elabora��o de qualquer produto ou processo reivindicado de forma v�lida, tal utiliza��o n�o constituir� uma infrac��o � patente.
rec14	pt	Para a protec��o jur�dica dos inventos que implicam programas de computador n�o � necess�rio criar um organismo de aplica��o das normas em vigor da legisla��o nacional em mat�ria de patentes. As regras da legisla��o nacional em mat�ria de patentes permanecer�o a base essencial para a protec��o jur�dica dos inventos que implicam programas de computador. A presente Directiva limita"se a clarificar o quadro jur�dico actualmente existente, tendo em conta as pr�ticas seguidas pelo Instituto Europeu de Patentes com vista a assegurar a certeza jur�dica, a transpar�ncia e a clareza das leis, bem como a evitar qualquer tend�ncia para a patenteabilidade de m�todos n�o patente�veis, tais como os procedimentos comerciais.
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhorar�, se forem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos que implicam programas de computador e se a situa��o jur�dica for transparente. Dada a actual tend�ncia manifestada pelas tradicionais ind�strias produtoras no sentido de deslocarem as suas actividades para economias com baixos custos de produ��o fora da Uni�o Europeia, a import�ncia da protec��o da propriedade intelectual e, em particular, da protec��o por patente, � evidente.
rec17	pt	A presente directiva dever� ser aplicada sem preju�zo das regras de concorr�ncia, em particular dos artigos 81.º e 82.º do Tratado
rec18	pt	Os direitos conferidos pelas patentes concedidas a inventos dentro do �mbito de aplica��o da presente directiva n�o afectam os actos permitidos ao abrigo dos artigos 5º e 6º da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular ao abrigo das suas disposi��es sobre a descompila��o e a interoperabilidade. Em especial, os actos que, ao abrigo dos artigos 5º e 6º da Directiva91/250/CEE, n�o carecem da autoriza��o do titular de direitos na parte relativa aos seus direitos de autor num programa inform�tico ou referentes a um programa inform�tico, e que, � excep��o do previsto nos artigos 5º ou 6º da Directiva91/250/CEE, necessitariam dessa autoriza��o, n�o carecem de autoriza��o do titular de direitos na parte relativa aos seus direitos de patente no programa inform�tico ou referentes ao programa inform�tico;
art2a	pt	a)"invento que implica programas de computador" significa qualquer invento cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art3	pt	Suprimido <P>
art4	pt	Para ser patente�vel, um invento que implique programas de computador tem de ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva. Para implicar uma actividade inventiva, um invento que implique programas de computador tem de constituir um contributo de car�cter t�cnico. <P>2.Os Estados"Membros garantir�o que um invento que implique programas de computador e que preste um contributo t�cnico seja condi��o necess�ria para a declara��o de exist�ncia de uma actividade inventiva. <P>O contributo t�cnico ser� avaliado mediante a pondera��o dos �ltimos avan�os do progresso tecnol�gico e o �mbito da reivindica��o de patente visto na sua globalidade, independentemente de essas caracter�sticas serem ou n�o acompanhadas de especificidades de car�cter n�o t�cnico.
art4a	pt	Artigo 4º bis <P>Exclus�es de patenteabilidade <P>Um invento que implique programas de computador n�o ser� encarado como sendo um contributo t�cnico apenas e s� por envolver a utiliza��o de um computador, de uma rede, ou de qualquer outro aparelho suscept�vel de programa��o. Da mesma forma, os inventos que impliquem programas de computador destinados � execu��o de procedimentos de natureza comercial, matem�tica ou outra, e que n�o produzam quaisquer efeitos t�cnicos, para al�m das normais interac��es f�sicas entre o programa e a m�quina, a rede ou qualquer outro aparelho program�vel em que possa correr, n�o ser� patente�vel.
art5	pt	1.Os Estados"Membros garantir�o que um invento que implica programas de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda como processo executado por esse computador, rede inform�tica ou aparelho, pela execu��o do software. <P>2.Qualquer reivindica��o de um programa de computador, isoladamente ou num suporte, deve ser autorizada, se o programa, depois de carregado e executado num computador, rede inform�tica programada ou outro aparelho program�vel, permitir a aplica��o de um produto ou a execu��o de um processo, patente�veis ao abrigo dos artigos 4º e 4 bis.
art6	pt	Os direitos conferidos pelas patentes concedidas a inventos dentro do �mbito de aplica��o da presente directiva n�o afectam os actos permitidos ao abrigo dos artigos 5º e 6º da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular ao abrigo das suas disposi��es sobre a descompila��o e a interoperabilidade.
art6a	pt	Artigo 6 º bis <P>Os Estados-Membros garantir�o que, sempre que seja necess�ria a utiliza��o de uma t�cnica patenteada com o �nico fito de assegurar a convers�o das conven��es utilizadas em dois sistemas ou redes inform�ticos diferentes, por forma a permitir a comunica��o e a troca de dados entre eles, tal utiliza��o n�o seja considerada uma viola��o de patente.
art7	pt	A Comiss�o acompanhar� o impacto, na inova��o e na concorr�ncia, do direito das patentes em rela��o aos inventos que implicam programas de computador, tanto na Europa como a n�vel internacional, bem como nas empresas europeias, em particular, nas pequenas e m�dias empresas e no com�rcio electr�nico. inclusive no com�rcio electr�nico.
art8a	pt	Artigo 8º bis <P>A Comiss�o proceder� � an�lise do impacto da presente Directiva em fun��o das ac��es de fiscaliza��o levadas a cabo nos termos do artigo 7.º, bem como do relat�rio que ser� elaborado ao abrigo do disposto no artigo 8.º e, se necess�rio, apresentar� ao Parlamento Europeu e ao Conselho propostas de altera��o da legisla��o em vigor.
art9_1	pt	1.Os Estados"Membros por�o em vigor as disposi��es legislativas, regulamentares e administrativas necess�rias para darem cumprimento � presente directiva, o mais tardar, dezoito meses ap�s a respectiva entrada em vigor. Desse facto informar�o imediatamente a Comiss�o.
rec1	sv	F�r att genomf�ra den inre marknaden m�ste man dels undanr�ja hinder f�r fri r�rlighet och motverka snedvridna konkurrensf�rh�llanden, dels skapa f�rh�llanden som gynnar innovation och investeringar. I detta sammanhang �r skydd f�r uppfinningar genom patent en v�sentlig f�ruts�ttning f�r att den inre marknaden skall bli en framg�ng. Ett verksamt, �ppet och harmoniserat skydd av datorrelaterade uppfinningar i alla medlemsstater �r viktigt f�r att trygga och stimulera investeringsviljan.
rec5	sv	D�rf�r b�r de lagregler som reglerar patenterbarhet f�r datorrelaterade uppfinningar harmoniseras, s� att man ser till att den �tf�ljande r�ttss�kerheten och niv�n p� de krav som uppst�lls f�r patenterbarhet ger de nyskapande f�retagen m�jlighet att utnyttja sin uppfinningsprocess optimalt samt stimulerar investeringar och innovation. <P>R�ttss�kerheten kommer ocks� att garanteras genom att nationella domstolar f�r och nationella domstolar i sista instans m�ste be domstolen om ett f�rhandsavg�rande d� det �r oklart hur direktivet skall tolkas.
rec7a	sv	Syftet med detta direktiv �r inte att �ndra Europeiska patentkonventionen utan att f�rhindra skiljaktiga tolkningar av dess best�mmelser.
rec11	sv	F�r att kunna patenteras m�ste uppfinningar i allm�nhet och datorrelaterade uppfinningar i synnerhet kunna tillgodog�ras industriellt, vara nya och ha uppfinningsh�jd. F�r att kunna tillerk�nnas uppfinningsh�jd m�ste datorrelaterade uppfinningar utg�ra ett tekniskt bidrag till teknikens st�ndpunkt.
rec12	sv	Det �r s�ledes viktigt att klarg�ra att om en uppfinning inte utg�r ett tekniskt bidrag till teknikens st�ndpunkt, vilket t.ex. �r fallet om bidraget saknar teknisk karakt�r, �ven om en datorrelaterad uppfinning till sin natur �r teknisk, skall den anses sakna uppfinningsh�jd och d�rmed inte vara patenterbar. <P>F�r att bed�ma om en uppfinning har uppfinningsh�jd till�mpas vanligtvis en problem- och l�sningsmetod f�r att fastst�lla om d�r finns ett tekniskt problem att l�sa. Om det inte finns n�gra tekniska problem kan uppfinningen inte anses utg�ra ett tekniskt bidrag till teknikens st�ndpunkt.
rec13a	sv	Om en metod som annars inte g�r att patentera till�mpas med hj�lp av en anordning, exempelvis en dator, �r det emellertid inte i sig tillr�ckligt f�r att konstatera att det �r fr�ga om ett tekniskt bidrag. En datorrelaterad aff�rsmetod eller en annan metod d�r det enda bidraget till teknikens st�ndpunkt inte �r tekniskt kan d�rf�r inte betraktas som en patenterbar uppfinning.
rec13b	sv	Om bidraget till teknikens st�ndpunkt enbart r�r n�got som inte �r patenterbart kan uppfinningen inte patenteras, oberoende av hur fr�gan l�ggs fram i ans�kan. Till exempel kan kravet p� tekniskt bidrag inte kringg�s enbart genom att tekniska medel specificeras i patentkravet.
rec13c	sv	En algoritm �r dessutom inte till sin natur teknisk och kan d�rf�r inte utg�ra en teknisk uppfinning. En metod som omfattar anv�ndning av en algoritm kan trots detta vara patenterbar om metoden anv�nds f�r att l�sa ett tekniskt problem. Alla patent som beviljas f�r s�dana metoder har emellertid inte ensamr�tt till sj�lva algoritmen eller dess anv�ndning i sammanhang som inte fastst�lls i patentet.
rec13d	sv	Omfattningen av den ensamr�tt som vilket patent som helst tillhandah�ller anges i patentkraven. Ans�kningar om datorrelaterade uppfinningar m�ste g�lla antingen en produkt, s�som en programmerad anordning, eller en process utf�rd av en s�dan anordning. Av detta f�ljer att anv�ndning av enskilda detaljer i mjukvara i sammanhang som inte inbegriper framst�llning av en patentskyddad produkt eller process inte kommer att betraktas som patentintr�ng.
rec14	sv	F�r att inf�ra patentskydd f�r datorrelaterade uppfinningar kr�vs det inte n�got nytt regelverk som ers�ttning f�r de nationella patentlagarna. De nationella patentlagarna bibeh�lls som huvudsaklig grund f�r patentskydd n�r det g�ller datorrelaterade uppfinningar. Genom direktivet klarg�rs helt enkelt den nu g�llande r�ttsliga st�llningen n�r det g�ller Europeiska patentverkets praxis i syfte att garantera r�ttss�kerhet, insyn och r�ttslig klarhet och f�rhindra en utveckling som inneb�r att metoder som inte �r patenterbara, s�som aff�rsmetoder, blir patenterbara.
rec16	sv	Den europeiska industrins konkurrensl�ge gentemot dess viktigaste handelspartner kommer att st�rkas om r�dande olikheter i det r�ttsskydd som finns f�r datorrelaterade uppfinningar undanr�js och r�ttsl�get klarg�rs. Med tanke p� att de traditionella tillverkningsindustrierna tenderar att flytta sin verksamhet till l�gkostnadsl�nder utanf�r EU �r det uppenbart att det �r viktigt att skydda immateriella r�ttigheter och i synnerhet patentskyddet.
rec17	sv	Detta direktiv b�r inte inverka p� till�mpningen av konkurrensreglerna, s�rskilt <I>artiklarna 81 och 82</I> i f�rdraget.
rec18	sv	De r�ttigheter som �r f�rbundna med patent f�r uppfinningar inom ramen f�r detta direktiv f�r inte p�verka handlingar som �r till�tna enligt artiklarna 5 och 6 i direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt enligt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga. Handlingar som enligt artiklarna 5 och 6 i direktiv 91/250/EEG inte kr�ver r�ttsinnehavarens tillst�nd n�r det g�ller r�ttsinnehavarens upphovsr�tt till eller i samband med ett datorprogram, och som " om det inte vore f�r best�mmelserna i artiklarna 5 och 6 i direktiv 91/250/EEG " skulle kr�va ett s�dant tillst�nd, f�r inte kr�va r�ttsinnehavarens tillst�nd n�r det g�ller r�ttsinnehavarens patentr�ttigheter till eller i samband med datorprogrammet.
art2a	sv	a)"datorrelaterad uppfinning"
art3	sv	utg�r
art4	sv	F�r att kunna patenteras m�ste en datorrelaterad uppfinning kunna tillgodog�ras industriellt, vara ny och ha uppfinningsh�jd. F�r att en datorrelaterad uppfinning skall kunna tillerk�nnas uppfinningsh�jd m�ste den utg�ra ett tekniskt bidrag. <P>Medlemsstaterna skall se till att en datorrelaterad uppfinning som utg�r ett tekniskt bidrag utg�r en n�dv�ndig f�ruts�ttning f�r uppfinningsh�jd. <P>Det tekniska bidraget skall bed�mas med hj�lp av teknikens st�ndpunkt och patentkravens samlade skyddsomf�ng som skall omfatta tekniska k�nnetecken, oberoende av om dessa k�nnetecken kombineras med icke-tekniska k�nnetecken.
art4a	sv	Artikel 4a <P>Undantag fr�n patenterbarhet <P>En datorrelaterad uppfinning skall inte anses ge ett tekniskt bidrag bara f�r att den inbegriper anv�ndning av dator, n�tverk eller andra programmerbara anordningar. F�ljaktligen skall patent inte kunna utf�rdas f�r en uppfinning som till�mpar aff�rsmetoder, matematiska eller andra metoder med hj�lp av datorprogram och som inte medf�r n�gra tekniska effekter som �verstiger den vanliga fysiska v�xelverkan mellan programmet och datorn, n�tverket eller n�gon annan programmerbar anordning d�r den anv�nds.
art5	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning kan patentskyddas s�som anordning, dvs. en programmerad dator, ett programmerat n�tverk eller n�gon annan programmerad anordning, eller s�som f�rfarande utf�rt av ovann�mnda dator, n�tverk eller anordning genom att en mjukvara k�rs p� anordningen. <P>2.Patentkrav kan medges f�r ett datorprogram i sig eller f�r ett datorprogram som �r lagrat p� en b�rare eller som en signal, f�rutsatt att ett s�dant program till�mpar en produkt eller utf�r en process som �r patenterbar enligt artiklarna 4 och 4a n�r det laddats eller k�rs p� en dator, ett datoriserat n�tverk eller annan programmerbar anordning.
art6	sv	De r�ttigheter som �r f�rbundna med patent f�r uppfinningar inom ramen f�r detta direktiv skall inte p�verka de handlingar som �r till�tna enligt artiklarna5 och 6 i direktiv91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt enligt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga.
art6a	sv	Artikel 6a <P>Om anv�ndning av en patenterad teknik endast beh�vs f�r att garantera omvandling av de konventioner som anv�nds i tv� olika datorsystem eller n�tverk f�r att m�jligg�ra kommunikation och �msesidigt utbyte av uppgifter, skall medlemsstaterna se till att denna anv�ndning inte betraktas som patentintr�ng.
art7	sv	Kommissionen skall f�lja hur patentskyddet f�r datorrelaterade uppfinningar p�verkar innovation och konkurrens, b�de i Europa och internationellt, samt europeiskt n�ringsliv, i synnerhet sm� och medelstora f�retag och n�thandel.
art8a	sv	Artikel 8a <P>P� grundval av uppf�ljningen enligt artikel7 och rapporten enligt artikel8 skall kommissionen se �ver f�ljderna av detta direktiv och, vid behov, f�rel�gga Europaparlamentet och r�det f�rslag om �ndring av g�llande lagstiftning.
art8ca	sv	ca)huruvida sv�righeter har uppst�tt i f�rh�llandet mellan patentskydd f�r datorrelaterade uppfinningar och det upphovsr�ttsliga skyddet f�r datorprogram i enlighet med direktiv91/250/EG, och huruvida patentsystemet har missbrukats i fr�ga om datorrelaterade uppfinningar,
art8cb	sv	cb)huruvida det skulle vara �nskv�rt och r�ttsligt m�jligt med tanke p� gemenskapens internationella �taganden att inf�ra en "skonfrist" f�r k�nnetecken som blir k�nda f�re ans�kningsdatumet och som ing�r i en patentans�kan, oberoende av vilket slags uppfinning det �r fr�ga om,
art8cc	sv	cc)huruvida det kan bli n�dv�ndigt att anordna en diplomatkonferens f�r att se �ver europeiska patentkonventionen, inte minst mot bakgrund av inf�randet av gemenskapspatentet,
art8cd	sv	cd)i vilken m�n kraven i detta direktiv beaktas i Europeiska patentverkets praxis och i verkets bed�mningsriktlinjer.
art9_1	sv	1.Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast arton m�nader efter det att direktivet tr�tt i kraft och skall genast underr�tta kommissionen om detta.
\.


--
-- Data for TOC entry 63 (OID 23704)
-- Name: parl; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY parl (id, lang, parl) FROM stdin;
rec1	da	Gennemf�relsen af det indre marked indeb�rer, at hindringerne for den frie bev�gelighed og konkurrencefordrejningerne fjernes, og at der samtidig skabes et gunstigt klima for innovation og investeringer. I den forbindelse er beskyttelsen af opfindelser ved hj�lp af patenter en v�sentlig betingelse for et vellykket indre marked. Det er vigtigt med en effektiv, gennemsigtig og harmoniseret beskyttelse af computer-implementerede opfindelser i alle medlemsstater for at opretholde og fremme investeringerne p� dette omr�de.
rec5	da	Lovbestemmelserne vedr�rende computer-implementerede opfindelsers patenterbarhed b�r derfor harmoniseres med henblik p� at sikre, at den herved opn�ede retssikkerhed og omfanget af de betingelser, der stilles for patenterbarhed, g�r det muligt for virksomhederne at f� det fulde udbytte af deres opfindelsesproces og udg�r et incitament til investering og innovation. Retssikkerheden garanteres ogs� ved, at nationale domstole kan og, n�r der er tale om nationale domstole, der afg�r sagen i sidste instans, skal indbringe denne for De Europ�iske F�llesskabers Domstol i tilf�lde af tvivl om fortolkningen af dette direktiv.
rec5a	da	Reglerne i artikel 52 i konventionen om meddelelse af europ�iske patenter om gr�nser for patenterbarhed b�r bekr�ftes og pr�ciseres. Den derved opst�ede retssikkerhed ville bidrage til et investerings- og innovationsgunstigt klima p� softwareomr�det.
rec6	da	udg�r
rec7	da	I henhold til konventionen om meddelelse af europ�iske patenter, der blev undertegnet i M�nchen den 5. oktober 1973, og medlemsstaternes patentlove er edb-programmer s�vel som opdagelser, videnskabelige teorier, matematiske metoder, �stetiske frembringelser, planer, regler og metoder for intellektuel virksomhed, for spil eller for erhvervsvirksomhed (forretningskoncepter) og freml�ggelse af information udtrykkeligt ikke betragtet som opfindelser og kan derfor ikke patenteres. Disse undtagelser g�lder, fordi disse genstande eller aktiviteter som s�danne ikke henh�rer under et teknologisk omr�de.
rec7a	da	Form�let med dette direktiv er ikke at �ndre den n�vnte konvention, men at forhindre divergerende fortolkninger af dens bestemmelser.
rec7b	da	Europa-Parlamentet har gentagne gange kr�vet, at Den Europ�iske Patentmyndighed reviderer sit reglement og underl�gges offentlig kontrol i forbindelse med ud�velsen af sine funktioner. Det ville i den forbindelse is�r v�re hensigtsm�ssigt, om man tog Patentmyndighedens praksis med at tage sig betalt for de patenter, den udsteder, op til fornyet overvejelse, for s� vidt som denne praksis skader institutionens offentlige karakter. Europa-Parlamentet kr�vede i sin beslutning af 30. marts 2000 om Den Europ�iske Patentmyndigheds afg�relse vedr�rende patent EP 695 351, udstedt den 8. december 1999<SUP>1</SUP>, kr�vede Europa-Parlamentet en revision af Den Europ�iske Patentmyndigheds reglement for at sikre, at den underl�gges offentlig kontrol ved ud�velsen af sine funktioner. <P><SUP>1</SUP> EFT C 378 af 29.12.2000, s. 95.
rec11	da	For at v�re patenterbare skal opfindelser i almindelighed og computer-implementerede opfindelser i s�rdeleshed v�re nye, have opfindelsesh�jde og kunne anvendes industrielt. For at have opfindelsesh�jde skal computer-implementerede opfindelser desuden yde et nyt bidrag til det aktuelle tekniske niveau for at afgr�nse dem i forhold til ren software.
rec12	da	Derfor er en innovation, der ikke yder et bidrag til det aktuelle tekniske niveau, ikke nogen opfindelse i patentretlig forstand.
rec13	da	udg�r
rec13a	da	Det forhold, at en ellers ikke-patenterbar metode implementeres p� en maskine som f.eks. en computer er ikke i sig selv tilstr�kkeligt til at konkludere, at der ydes et teknisk bidrag. En computer-implementeret forretningsmetode, databehandlingsmetode eller en anden metode, hvor det eneste bidrag til det aktuelle teknologiske niveau er ikke-teknologisk, kan derfor ikke udg�re en patenterbar opfindelse.
rec13b	da	Hvis bidraget til det aktuelle tekniske niveau udelukkende vedr�rer en ikke-patenterbar genstand, kan der ikke v�re tale om nogen patenterbar opfindelse, uanset hvorledes genstanden pr�senteres i patentkravet. F.eks. kan kravet om teknisk bidrag ikke omg�s blot ved at anf�re anvendelse af tekniske midler i patentkravet.
rec13c	da	En algoritme er principielt ikke-teknisk og kan derfor ikke udg�re nogen teknisk opfindelse. En metode, som indeb�rer anvendelse af en algoritme, kan dog v�re patenterbar, forudsat at metoden anvendes til at l�se et teknisk problem. Ethvert patent, som meddeles for en s�dan metode, b�r dog ikke give eneret p� algoritmen som s�dan eller p� dens anvendelse i sammenh�nge, som ikke er beskrevet i patentet.
rec13d	da	Omfanget af de eksklusive rettigheder, der er knyttet til et patent, er fastsat i patentkravene. Ans�gninger om patenter p� computer-implementerede opfindelser b�r ske med henvisning til enten et produkt, f.eks. en programmeret maskine, eller en proces, der udf�res af en s�dan maskine. Anvendelse af individuelle elementer af software i en sammenh�ng, der ikke indeb�rer frembringelse af et produkt eller en proces, for hvilke der lovligt er ans�gt om et patent, b�r derfor ikke betragtes som en patentkr�nkelse.
rec14	da	Retlig beskyttelse af computer-implementerede opfindelser kr�ver ikke indf�relse af en s�rlig lovgivning til erstatning af medlemsstaternes patentlovgivning. Det prim�re grundlag for retlig beskyttelse af computer-implementerede opfindelser er fortsat medlemsstaternes patentlovgivning. Dette direktiv klarl�gger blot den nuv�rende retspraksis med henblik p� at skabe retssikkerhed, gennemsigtighed og klarhed i lovgivningen og undg� enhver tendens i retning af patenterbarhed for ikke-patenterbare metoder, f.eks. standardprocesser og forretningsmetoder.
rec16	da	EU's erhvervslivs konkurrenceevne i forhold til EU's vigtigste handelspartnere vil blive forbedret, hvis de nuv�rende forskelle i den retlige beskyttelse af computer-implementerede opfindelser fjernes og retstilstanden bliver gennemsigtig. Med den nuv�rende tendens inden for den traditionelle forarbejdningsindustri i retning af at flytte arbejdsprocesser til �konomier med lave omkostninger uden for Den Europ�iske Union er det �benbart, at der er et behov for beskyttelse af intellektuel ejendomsret, is�r patentbeskyttelse.
rec17	da	Dette direktiv b�r ikke ber�re anvendelsen af konkurrencereglerne, herunder is�r traktatens artikel 81 og 82.
rec18	da	De rettigheder, der indr�mmes i patenter udstedt for opfindelser inden for dette direktivs anvendelsesomr�de, b�r ikke ber�re handlinger, der er tilladt i henhold til artikel 5 og 6 i direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r i henhold til bestemmelserne om dekompilering og interoperabilitet. Navnlig b�r handlinger, som i henhold til artikel 5 og 6 i n�vnte direktiv ikke kr�ver tilladelse fra rettighedshaveren med hensyn til rettighedshaverens ophavsret til eller vedr�rende et edb-program, og som ville have kr�vet en s�dan tilladelse, hvis artikel 5 eller 6 i direktiv 91/250/E�F ikke havde undtaget fra dette krav, ikke kr�ve tilladelse fra rettighedshaveren for s� vidt ang�r rettighedshaverens patentrettigheder til eller vedr�rende edb-programmet.
rec18a	da	Under alle omst�ndigheder skal medlemsstaternes lovgivning sikre, at patenterne har nyhedskarakter og opfindelsesh�jde for at undg� tilegnelse af opfindelser, der allerede er offentlig tilg�ngelige, blot ved at de inkorporeres i et computerprogram.
art2a	da	a) ved en 'computer-implementeret opfindelse' forst�s enhver opfindelse i den i den europ�iske patentkonvention omhandlede forstand, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som i sine implementationer omfatter et eller flere ikke-tekniske karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer, ud over de tekniske karakteristika, som enhver opfindelse m� bidrage med
art2b	da	b) ved et 'teknisk bidrag', ogs� kaldet en 'opfindelse' forst�s et bidrag til det aktuelle tekniske niveau. At bidraget er af teknisk karakter, er en af de fire foruds�tninger for patenterbarhed. For at komme i betragtning til et patent skal det tekniske bidrag derudover v�re nyt, ikke-indlysende og kunne anvendes industrielt. Anvendelse af naturkr�fter til at kontrollere fysiske virkninger ud over numerisk repr�sentation af information henh�rer under et teknologisk omr�de. Behandling, h�ndtering og freml�ggelse af information henh�rer ikke under et teknologisk omr�de, selv om der anvendes teknologiske indretninger til dette form�l
art2ba	da	ba) ved 'teknologisk omr�de' forst�s et industrielt anvendelsesomr�de, som n�dvendigg�r kontrolleret anvendelse af naturkr�fter til at opn� forudsigelige resultater. 'Teknologisk' vil sige 'der henh�rer under et teknologisk omr�de'
art2bb	da	bb) ved 'industri' forst�s i patentretlig forstand 'automatiseret produktion af materielle goder'.
art3	da	udg�r
art3a	da	Artikel 3a <P>Medlemsstaterne sikrer, at databehandling ikke betragtes som et teknologisk omr�de i patentretlig forstand, og at innovationer inden for databehandling ikke betragtes som opfindelser i patentretlig forstand.
art4	da	For at v�re patenterbar skal en computer-implementeret opfindelse kunne anvendes industrielt, v�re ny og have opfindelsesh�jde. For at have opfindelsesh�jde skal en computer-implementeret opfindelse yde et teknisk bidrag. <P>Medlemsstaterne sikrer, at det er en n�dvendig foruds�tning for opfindelsesh�jde, at en computer-implementeret opfindelse yder et teknisk bidrag. <P>Det v�sentlige omfang af et teknisk bidrag vurderes som forskellen mellem de tekniske karakteristika, der indg�r i patentkravets genstand som helhed, og det aktuelle tekniske niveau, uanset om disse karakteristika ledsages af ikke-tekniske karakteristika. <P>3a. Ved afg�relsen af, om en given computer-implementeret opfindelse yder et teknisk bidrag, tages hensyn til, om den udg�r en ny l�re om forholdet mellem �rsag og virkning ved kontrolleret anvendelse af naturkr�fter, og om den kan anvendes industrielt i ordets sn�vre betydning, b�de med hensyn til metode og resultat.
art4a	da	Artikel 4a <P>Udelukkelse fra patenterbarhed <P>En computer-implementeret opfindelse anses ikke for at yde et teknisk bidrag, alene fordi den indeb�rer anvendelse af en computer, et netv�rk eller andre programmerbare maskiner. Der kan derfor ikke udtages patent p� opfindelser, der omfatter edb-programmer, som implementerer forretningsmetoder, matematiske metoder eller andre metoder, og som ikke har nogen tekniske virkninger ud over det normale fysiske samspil mellem et program og den computer, det netv�rk eller de andre programmerbare maskiner, som det k�res p�.
art4b	da	Artikel 4b <P>Medlemsstaterne skal sikre, at computer-implementerede l�sninger p� tekniske problemer ikke betragtes som patenterbare opfindelser, alene fordi de f�rer til en mere effektiv brug af et databehandlingssystems ressourcer.
art5_1	da	Medlemsstaterne sikrer, at der kun kan ans�ges om patent p� en computer-implementeret opfindelse som et produkt, dvs. som en programmeret anordning eller som en teknisk produktionsproces.
art5_1a	da	1a. Medlemsstaterne sikrer, at meddelelsen af patenter p� computerimplementerede opfindelser kun omfatter de tekniske bidrag, som danner grundlag for patentkravet. Der kan ikke meddeles patent p� et edb-program, hverken p� et program alene eller et program, der befinder sig p� et b�remedium.
art5_1b	da	1b. Medlemsstaterne sikrer, at produktion, h�ndtering, behandling, distribution og offentligg�relse af enhver form for information aldrig kan udg�re en direkte eller indirekte patentkr�nkelse, heller ikke n�r en teknisk maskine anvendes til dette form�l.
art5_1c	da	1c. Medlemsstaterne sikrer, at anvendelse af edb-programmer til form�l, der ikke er omfattet af patentets genstand, ikke kan udg�re en direkte eller indirekte patentkr�nkelse. <P>1d. Medlemsstaterne sikrer, at der, n�r et patentkrav omtaler karakteristika, som indeb�rer anvendelse af et edb-program, som en del af beskrivelsen offentligg�res en velfungerende og veldokumenteret reference-implementering af et s�dant program uden nogen begr�nsninger i form af licensbetingelser.
art6	da	Rettigheder, der indr�mmes i patenter udstedt for opfindelser inden for dette direktivs anvendelsesomr�de, ber�rer ikke handlinger, der er tilladt i henhold til artikel 5 og 6 i direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, herunder is�r i henhold til bestemmelserne om dekompilering og interoperabilitet.
art6a	da	Artikel 6a <P>Anvendelse af patenterede teknikker <P>Medlemsstaterne sikrer, at n�r anvendelsen af en patenteret teknik er p�kr�vet til et v�sentligt form�l som f.eks. konvertering af de konventioner, der anvendes i to forskellige computersystemer eller -netv�rk, for at muligg�re kommunikation og dataudveksling mellem dem, betragtes en s�dan anvendelse ikke som en patentkr�nkelse.
art7	da	Kommissionen overv�ger computer-implementerede opfindelsers indvirkning p� innovation og konkurrence, b�de i Europa og internationalt, og p� EU's erhvervsliv, s�rlig sm� og mellemstore virksomheder og Open Sourcesamfundet, samt elektronisk handel.
art8a	da	Artikel 8a <P>Vurdering af direktivets virkninger <P>P� baggrund af den i artikel 7 omhandlede overv�gning og den i artikel 8 omhandlede rapport vurderer Kommissionen dette direktivs virkninger og forel�gger i givet fald Europa-Parlamentet og R�det forslag om en �ndring af direktivet.
art8b	da	b) hvorvidt reglerne for fastl�ggelse af patentets gyldighed, kriterierne for patenterbarhed, herunder is�r nyhedskarakter, opfindelsesh�jde og patentkravets genstand, er fyldestg�rende, og
art8ca	da	ca) hvorvidt der har v�ret vanskeligheder med hensyn til forholdet mellem beskyttelsen af patenter p� computer-implementerede opfindelser og beskyttelsen af ophavsret til edb-programmer i henhold til direktiv 91/250/E�F, og hvorvidt der er sket misbrug af patentsystemet i forbindelse med computer-implementerede opfindelser
art8cb	da	cb) hvorvidt det ville v�re �nskeligt og retligt muligt under hensyn til F�llesskabets internationale forpligtelser at indf�re en 'nyhedssk�nefrist' for s�danne elementer i en patentans�gning for enhver form for opfindelse, som afsl�res forud for datoen for ans�gningens indgivelse
art8cc	da	cc) i hvilke henseender det kan blive n�dvendigt at g�re forberedelser til en diplomatisk konference om �ndring af konventionen om meddelelse af europ�iske patenter, bl.a. i lyset af det kommende EF-patent
art8cd	da	cd) p� hvilken m�de der er taget hensyn til kravene i dette direktiv i Det Europ�iske Patentmyndigheds praksis og i dets retningslinjer for behandling
art8ce	da	ce) hvorvidt de til Den Europ�iske Patentmyndighed uddelegerede bef�jelser er forenelige med kravene om harmonisering af EU-lovgivningen og med principperne om gennemsigtighed og kontrollerbarhed
art8cf	da	cf) virkningerne af konvertering af de konventioner, der anvendes i to forskellige computersystemer eller -netv�rk, for at muligg�re kommunikation og dataudveksling mellem dem
art8cg	da	cg) om den mulighed, der omhandles i direktivet om brug af patenterede opfindelser til udelukkende at sikre interoperabilitet mellem to systemer, er tilstr�kkelig.
art8_1a	da	I denne rapport begrunder Kommissionen i givet fald, hvorfor den anser en �ndring af direktivet for n�dvendig, og opregner de punkter, hvortil den vil frems�tte forslag om �ndring.
art9_1	da	Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at efterkomme dette direktiv senest den ...*. De underretter straks Kommissionen herom. <P>* 18 m�neder efter dette direktivs ikrafttr�den
rec1	de	Damit der Binnenmarkt verwirklicht wird, m�ssen Beschr�nkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen beg�nstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium f�r den Erfolg des Binnenmarkts. Es ist unerl�sslich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam, transparent und einheitlich gesch�tzt sind, wenn Investitionen auf diesem Gebiet gesichert und gef�rdert werden sollen.
rec5	de	Aus diesen Gr�nden sollten die f�r die Patentierbarkeit computerimplementierter Erfindungen ma�geblichen Rechtsvorschriften vereinheitlicht werden, um sicherzustellen, dass die <I>daraus folgende</I> Rechtssicherheit und das Anforderungsniveau f�r die Patentierbarkeit dazu f�hren, dass innovative Unternehmen den gr��tm�glichen Nutzen aus ihrem Erfindungsprozess ziehen und Anreize f�r Investitionen und Innovationen geschaffen werden. Die Rechtssicherheit wird auch dadurch sichergestellt, dass bei Zweifeln �ber die Auslegung dieser Richtlinie die Gerichte der Mitgliedstaaten den Gerichtshof der Europ�ischen Gemeinschaften anrufen k�nnen und die letztinstanzlichen Gerichte der Mitgliedstaaten hierzu verpflichtet sind.
rec5a	de	Die Regeln gem�� Artikel 52 des Europ�ischen Patent�bereinkommens �ber die Grenzen der Patentierbarkeit sollen best�tigt und pr�zisiert werden. Die dadurch entstehende Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen.
rec6	de	entf�llt
rec7	de	Nach dem �bereinkommen �ber die Erteilung europ�ischer Patente (Europ�isches Patent�bereinkommen) vom 5. Oktober 1973 (EP�) und den Patentgesetzen der Mitgliedstaaten gelten Programme f�r Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, �sthetische Formsch�pfungen, Pl�ne, Regeln und Verfahren f�r gedankliche T�tigkeiten, f�r Spiele oder f�r gesch�ftliche T�tigkeiten sowie die Wiedergabe von Informationen ausdr�cklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt, da die besagten Gegenst�nde und T�tigkeiten keinem Gebiet der Technik zugeh�ren.
rec7a	de	Durch diese Richtlinie soll jenes �bereinkommen nicht ge�ndert, sondern die unterschiedliche Auslegung seiner Bestimmungen vermieden werden.
rec7b	de	</SUP> forderte das Europ�ische Parlament 'eine �berpr�fung der T�tigkeiten des EPA, um zu gew�hrleisten, dass es einer �ffentlichen Rechenschaftspflicht unterliegt'. <P>_________<P>ABl. C 378 vom 29.12.2000, S. 95.
rec11	de	Um patentierbar zu sein, m�ssen Erfindungen im Allgemeinen und computerimplementierte Erfindungen im Besonderen neu sein, auf einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen T�tigkeit zu erf�llen, m�ssen computerimplementierte Erfindungen zus�tzlich einen neuen technischen Beitrag zum Stand der Technik leisten, um sie von reiner Software abzugrenzen.
rec12	de	Folglich ist eine Innovation, die keinen technischen Beitrag zum Stand der Technik leistet, keine Erfindung im Sinne des Patentrechts.
rec13	de	entf�llt
rec13a	de	Allerdings reicht allein die Tatsache, dass eine ansonsten nicht patentierbare Methode in einer Vorrichtung wie einem Computer angewendet wird, nicht aus, um davon auszugehen, dass ein technischer Beitrag geleistet wird. Folglich kann eine computerimplementierte Gesch�fts-, Datenverarbeitungs- oder andere Methode, bei der der einzige Beitrag zum Stand der Technik nichttechnischen Charakter hat, keine patentierbare Erfindung darstellen.
rec13b	de	Bezieht sich der Beitrag zum Stand der Technik ausschlie�lich auf einen nichtpatentierbaren Gegenstand, kann es sich nicht um eine patentierbare Erfindung handeln, unabh�ngig davon, wie der Gegenstand in den Patentanspr�chen dargestellt wird. So kann beispielsweise das Erfordernis eines technischen Beitrags nicht einfach dadurch umgangen werden, dass in den Patentanspr�chen technische Hilfsmittel spezifiziert werden.
rec13c	de	Au�erdem ist ein Algorithmus von Natur aus nichttechnischer Art und kann deshalb keine technische Erfindung darstellen. Allerdings kann eine Methode, die die Benutzung eines Algorithmus umfasst, unter der Voraussetzung patentierbar sein, dass die Methode zur L�sung eines technischen Problems angewandt wird. Allerdings w�rde ein f�r eine derartige Methode gew�hrtes Patent kein Monopol auf den Algorithmus selbst oder seine Anwendung in einem von dem Patent nicht betroffenen Kontext verleihen.
rec13d	de	Der Anwendungsbereich der ausschlie�lichen Rechte, die durch ein Patent gew�hrt werden, wird durch die Patentanspr�che definiert. Patentanspr�che auf computerimplementierte Erfindungen m�ssen unter Bezugnahme entweder auf ein Erzeugnis wie beispielsweise eine programmierte Vorrichtung oder ein Verfahren, dass in einer solchen Vorrichtung verwirklicht wird, angemeldet werden. Werden demnach einzelne Software-Elemente in einem Kontext benutzt, bei dem es nicht um die Verwirklichung eines rechtm��ig beanspruchten Erzeugnisses oder Verfahrens handelt, stellt eine solche Verwendung keine Patentverletzung dar.
rec14	de	Um computerimplementierte Erfindungen rechtlich zu sch�tzen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage f�r den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarkeit zu gew�hrleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Trivialvorg�nge und Gesch�ftsmethoden, als patentf�hig zu erachten.
rec16	de	Die Wettbewerbsposition der europ�ischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern wird sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausger�umt sind und die Rechtslage transparenter ist. Beim derzeitigen Trend der klassischen verarbeitenden Industrie zur Verlagerung ihrer Betriebe in Niedriglohnl�nder au�erhalb der Europ�ischen Union liegt die Bedeutung des Urheberrechtsschutzes und insbesondere des Patentschutzes auf der Hand.
rec17	de	Die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 <I>des Vertrags</I> sollen durch diese Richtlinie unber�hrt bleiben.
rec18	de	Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere gem�� den Vorschriften in Bezug auf die Dekompilierung und die Interoperabilit�t. Insbesondere erfordern Handlungen, die gem�� Artikel 5 und 6 jener Richtlinie keine Genehmigung des Rechtsinhabers in Bezug auf dessen Urheberrechte an dem oder in Zusammenhang mit dem Computerprogramm erfordern, f�r die aber ohne Artikel 5 oder 6 jener Richtlinie eine solche Genehmigung erforderlich w�re, keine Genehmigung des Rechtsinhabers in Bezug auf die Patentrechte des Rechtsinhabers an dem oder in Zusammenhang mit dem Computerprogramm.
rec18a	de	In jedem Fall muss durch die Rechtsvorschriften der Mitgliedstaaten sichergestellt werden, dass die Patente Neuheiten und einen erfinderischen Beitrag beinhalten, um zu verhindern, dass eine Aneignung von bereits allgemein bekannten Erfindungen allein auf Grund ihrer Aufnahme in ein Computerprogramm erfolgt.
art2a	de	a) 'Computerimplementierte Erfindung' ist jede Erfindung im Sinne des Europ�ischen Patent�bereinkommens, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die in ihren Implementationen - au�er den technischen Merkmalen, die jede Erfindung beisteuern muss - mindestens ein nichttechnisches Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art2b	de	b) 'Technischer Beitrag', auch 'Erfindung' genannt, ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der f�r eine fachkundige Person nicht nahe liegend ist. Die Technizit�t des Beitrags ist eine von vier Voraussetzungen f�r die Patentierbarkeit. Zus�tzlich muss der technische Beitrag neu, nicht naheliegend und gewerblich anwendbar sein, damit ein Patent erteilt werden kann. Die Nutzung der Kr�fte der Natur zur Beherrschung der physikalischen Wirkungen �ber die numerische Darstellung der Informationen hinaus geh�rt zu einem Gebiet der Technik. Die Verarbeitung, die Bearbeitung und die Darstellungen von Informationen geh�ren nicht zu einem Gebiet der Technik, selbst wenn daf�r technische Vorrichtungen verwendet werden.
art2ba	de	ba) 'Gebiet der Technik' ist ein gewerbliches Anwendungsgebiet, das zur Erreichung vorhersehbarer Ergebnisse der Nutzung kontrollierbarer Kr�fte der Natur bedarf. 'Technisch' bedeutet 'einem Gebiet der Technik zugeh�rig'.
art2bb	de	bb) 'Industrie' im Sinne des Patentrechts ist die automatisierte Herstellung materieller G�ter;
art3	de	entf�llt
art3a	de	Artikel 3a<P>Die Mitgliedstaaten stellen sicher, dass die Datenverarbeitung nicht als Gebiet der Technik im Sinne des Patentrechts betrachtet wird und dass Innovationen im Bereich der Datenverarbeitung nicht als Erfindungen im Sinne des Patentrechts betrachtet werden.
art4	de	Um patentierbar zu sein, m�ssen computerimplementierte Erfindungen neu sein, auf einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen T�tigkeit zu erf�llen, m�ssen computerimplementierte Erfindungen einen technischen Beitrag leisten. <P>Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung, die einen technischen Beitrag leistet, eine notwendige Voraussetzung einer erfinderischen T�tigkeit ist. <P>Bei der Ermittlung des signifikanten Ausma�es des technischen Beitrags wird beurteilt, inwieweit sich alle technischen Merkmale, die der Gegenstand des Patentanspruchs in seiner Gesamtheit aufweist, vom Stand der Technik abheben, unabh�ngig davon, ob neben diesen Merkmalen nichttechnische Merkmale gegeben sind. <P>(3a) Bei der Feststellung, ob eine gegebene computerimplementierte Erfindung einen technischen Beitrag leistet, wird gepr�ft, ob sie eine neue Lehre �ber die Beziehung zwischen Ursache und Wirkung in der Nutzung kontrollierbarer Kr�fte der Natur darstellt, und ob sie sowohl im Hinblick auf die Methode als auch auf das Ergebnis eine industrielle Anwendung im engen Sinne dieses Ausdrucks hat.
art4a	de	Artikel 4a <P>Ausschluss von der Patentierbarkeit <P>Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausf�hrung ein Computerprogramm eingesetzt wird und durch die Gesch�ftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentf�hig, wenn sie �ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.
art4b	de	Artikel 4b <P>Die Mitgliedsstaaten sorgen daf�r, dass computerimplementierte L�sungen technischer Probleme nicht allein deshalb als patentf�hige Erfindungen angesehen werden, weil sie Einsparungen von Ressourcen innerhalb eines Datenverarbeitungssystems erm�glichen.
art5_1	de	Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung nur entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um eine programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein technisches Produktionsverfahren handelt.
art5_1a	de	Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentanspr�che nur den technischen Beitrag umfassen, der den Patentanspruch begr�ndet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datentr�ger vorliegendes Programm, ist unzul�ssig.
art5_1b	de	Die Mitgliedstaaten stellen sicher, dass die Erstellung, die Bearbeitung, die Verarbeitung, die Verbreitung und die Ver�ffentlichung von Informationen in jedweder Form niemals eine direkte oder indirekte Patentverletzung darstellen k�nnen, selbst wenn daf�r technische Vorrichtungen verwendet werden.
art5_1c	de	Die Mitgliedstaaten stellen sicher, dass in allen F�llen, in denen in einem Patentanspruch Merkmale genannt sind, die die Verwendung eines Computerprogramms erfordern, eine gut funktionierende und gut dokumentierte Referenzimplementierung eines solchen Programms als Teil der Beschreibung ohne einschr�nkende Lizenzbedingungen ver�ffentlicht wird.
art6	de	Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere hinsichtlich der Bestimmungen �ber die Dekompilierung und die Interoperabilit�t.
art6a	de	Artikel 6a<P>Einsatz patentierter Techniken</P><P>Die Mitgliedstaaten stellen sicher, dass in allen F�llen, in denen der Einsatz einer patentierten Technik f�r einen bedeutsamen Zweck wie die Konvertierung der in zwei verschiedenen Computersystemen oder -netzen verwendeten Konventionen ben�tigt wird, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu erm�glichen, diese Verwendung nicht als Patentverletzung gilt.
art7	de	Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen, insbesondere auf die kleinen und mittleren Unternehmen und die Open-Source-Bewegung, und den elektronischen Gesch�ftsverkehr auswirken.
art8a	de	Artikel 8a<P>Pr�fung der Auswirkungen der Richtlinie</P><P>Anhand der Beobachtung gem�� Artikel 7 und dem gem�� Artikel 8 zu erstellenden Bericht �berpr�ft die Kommission die Auswirkungen dieser Richtlinie und unterbreitet dem Europ�ischen Parlament und dem Rat erforderlichenfalls Vorschl�ge zur �nderung der Rechtsvorschriften.
art8b	de	b) die Angemessenheit der Regeln f�r die Laufzeit des Patents und die Festlegung der Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die Neuheit, die erfinderische T�tigkeit und den eigentlichen Patentanspruch, und
art8ca	de	ca) etwaige Schwierigkeiten, die im Hinblick auf das Verh�ltnis zwischen dem Schutz durch Patente auf computerimplementierte Erfindungen und dem Schutz von Computerprogrammen durch das Urheberrecht, wie es die Richtlinie 91/250/EWG vorsieht, aufgetreten sind, sowie etwaige Missbr�uche im Patentsystem in Verbindung mit computerimplementierten Erfindungen;
art8cb	de	cb) ihre Einsch�tzung, ob es unter Ber�cksichtigung der internationalen Verpflichtungen der Gemeinschaft w�nschenswert und rechtlich m�glich w�re, eine 'Gnadenfrist' hinsichtlich der Merkmale eines Patentantrags f�r jede Art von Erfindungen einzuf�hren, die vor dem Zeitpunkt des Antrags offenbart werden;
art8cc	de	cc) die Aspekte, unter denen es unter Umst�nden notwendig ist, eine diplomatische Konferenz zur �berarbeitung des Europ�ischen Patent�bereinkommens auch mit Blick auf das kommende Gemeinschaftspatent vorzubereiten;
art8cd	de	cd) die Art und Weise, in der die Anforderungen dieser Richtlinie in der Praxis des Europ�ischen Patentamts und in seinen Pr�fungsrichtlinien ber�cksichtigt worden sind;
art8ce	de	(ce) die Vereinbarkeit der dem EPA �bertragenen Befugnisse mit den Erfordernissen der Harmonisierung des EU-Rechts sowie mit den Grunds�tzen der Transparenz und Rechenschaftspflicht;
art8cf	de	cf) die Auswirkungen der Konvertierung der in zwei verschiedenen Computersystemen verwendeten Konventionen, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu erm�glichen;
art8cg	de	(cg) die Frage, ob die nach der Richtlinie zul�ssige Option hinsichtlich des Einsatzes einer patentierten Erfindung zum alleinigen Zweck der Sicherstellung von Interoperabilit�t zwischen zwei Systemen sachgerecht ist.
art8_1a	de	In diesem Bericht erl�utert die Kommission, warum sie eine �nderung der fraglichen Richtlinie f�r notwendig bzw. nicht f�r notwendig h�lt, und z�hlt erforderlichenfalls die Punkte auf, zu denen sie �nderungen vorzuschlagen gedenkt.
rec1	en	The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. <I>Effective</I>, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.
rec5	en	Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation. Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice of the European Communities.
rec5a	en	The rules pursuant to Article 52 of the Convention on the Grant of European Patents concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.
rec6	en	Deleted
rec7	en	Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.
rec7a	en	The aim of this Directive is not to amend the aforementioned Convention, but to prevent different interpretations of its provisions.
rec7b	en	The European Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice whereby the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution. In its Resolution of 30 March 2000 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999<SUP>1</SUP>, Parliament requested a review of the Office's operating rules to ensure that it was publicly accountable in the exercise of its functions.<P>__________</P><P><SUP>1</SUP> OJ C 378, 29.12.2000, p. 95.
rec11	en	In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.
rec12	en	Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention within the meaning of patent law.
rec13	en	Deleted.
rec13a	en	However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method, data processing method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.
rec13b	en	If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.
rec13c	en	Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method should not monopolise the algorithm itself or its use in contexts not foreseen in the patent.
rec13d	en	The scope of the exclusive rights conferred by any patent are defined by the<P>claims. Computer-implemented inventions must be claimed with reference to</P><P>either a product such as a programmed apparatus, or to a process carried out</P><P>in such an apparatus. Accordingly, where individual elements of software are</P><P>used in contexts which do not involve the realisation of any validly claimed</P><P>product or process, such use will not constitute patent infringement.
rec14	en	The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.
rec16	en	The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.
rec17	en	This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.
rec18	en	The rights conferred by patents granted for inventions within the scope of this Directive should not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of that Directive, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for those Articles, would require such authorisation, should not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.
rec18a	en	At all events, the legislation of the Member States must ensure that patents contain innovations and involve an inventive step, so as to prevent inventions already in the public domain from being appropriated simply by being incorporated into a computer program.
art2a	en	(a) 'computer-implemented invention' means any invention within the meaning of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;
art2b	en	(b) 'technical contribution', also called 'invention', means a contribution to the state of the art in a technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.
art2ba	en	(ba) 'technical field' means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. 'Technical' means 'belonging to a technical field'.
art2bb	en	(bb) 'industry' within the meaning of patent law means the automated production of material goods;
art3	en	Deleted.
art3a	en	Article 3a<P>Member States shall ensure that data processing is not considered to be a field of technology within the meaning of patent law, and that innovations in the field of data processing are not considered to be inventions within the meaning of patent law.
art4	en	In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. <P>Member States shall ensure that a computer-implemented invention making a technical contribution constitutes a necessary condition of involving an inventive step. <P>The significant extent of the technical contribution shall be assessed by consideration of the difference between all of the technical features included in the scope of the patent claim considered as a whole and the state of the art, irrespective of whether or not such features are accompanied by non-technical features. <P>3a. In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used
art4a	en	Article 4a <P>Exclusions from patentability <P>A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.
art4b	en	Article 4b <P>Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.
art5_1	en	Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.
art5_1a	en	1a. Member States shall ensure that patent claims granted in respect of computer-implemented inventions include only the technical contribution which justifies the patent claim. A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.
art5_1b	en	1b. Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.
art5_1c	en	1c. Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.<P>1d. Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program shall be published as a part of description without any restricting licensing terms.
art6	en	The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.
art6a	en	Article 6a<P>Use of patented techniques <P>Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
art7	en	The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises and the open source community, and electronic commerce.
art8a	en	Article 8a<P>Impact assessment <P>In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall assess the impact of this Directive and, where necessary, submit proposals for amending legislation to the European Parliament and the Council.
art8b	en	(b) whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and
art8ca	en	(ca) whether difficulties have been experienced in respect of the relationship between the protection by patent of computer-implemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EEC and whether any abuse of the patent system has occurred in relation to computer-implemented inventions;
art8cb	en	(cb) whether it would be desirable and legally possible having regard to the Community's international obligations to introduce a 'grace period' in respect of elements of a patent application for any type of invention disclosed prior to the date of the application;
art8cc	en	(cc) the aspects in respect of which it may be necessary to prepare for a diplomatic conference to revise the Convention on the Grant of European Patents, also in the light of the advent of the Community patent;
art8cd	en	(cd) how the requirements of this Directive have been taken into account in the practice of the European Patent Office and in its examination guidelines.
art8ce	en	(ce) whether the powers delegated to the EPO are compatible with the need to harmonise Community legislation, and with the principles of transparency and accountability.
art8cf	en	(cf) the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data;
art8cg	en	(cg) whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;
art8_1a	en	In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.
art9_1	en	Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than ...*. They shall forthwith inform the Commission thereof. <P>_______________</P><P>* Eighteen months after the entry into force of the Directive.
rec1	es	La realizaci�n del mercado interior implica la eliminaci�n de las restricciones a la libre circulaci�n y de las distorsiones de la competencia, as� como la creaci�n de un entorno que sea favorable a la innovaci�n y a las inversiones. En este contexto, la protecci�n de las invenciones mediante patentes constituye un elemento esencial para el �xito del mercado interior. Una protecci�n efectiva, transparente y armonizada de las invenciones implementadas en ordenador en todos los Estados miembros es esencial para mantener y fomentar las inversiones en este �mbito.
rec5	es	Por consiguiente, las normas jur�dicas relativas a la patentabilidad de las invenciones implementadas en ordenador deber�an armonizarse, de modo que se garantizara que la seguridad jur�dica resultante y el nivel de los requisitos exigidos para la patentabilidad permitieran a las empresas innovadoras obtener el m�ximo beneficio de su proceso de invenciones e impulsaran la inversi�n y la innovaci�n. La seguridad jur�dica est� garantizada asimismo por el hecho de que, en caso de que se planteen dudas en cuanto a la interpretaci�n de la presente Directiva, los tribunales nacionales pueden remitir el asunto al Tribunal de Justicia de las Comunidades Europeas para que resuelva, y los tribunales nacionales de �ltima instancia deben hacerlo.
rec5a	es	Deben confirmarse y precisarse las reglas de conformidad con el art�culo 52 del Convenio sobre la concesi�n de patentes europeas sobre los l�mites de la patentabilidad. La seguridad jur�dica resultante de ello deber�a contribuir a crear un clima favorable a la inversi�n y la innovaci�n en el �mbito de los programas inform�ticos.
rec6	es	suprimido
rec7	es	Con arreglo al Convenio sobre la concesi�n de patentes europeas, firmado en Munich el 5 de octubre de 1973, y las legislaciones sobre patentes de los Estados miembros, no se consideran invenciones, y quedan por tanto excluidos de la patentabilidad, los programas de ordenadores, as� como los descubrimientos, las teor�as cient�ficas, los m�todos matem�ticos, las creaciones est�ticas, los planes, principios y m�todos para el ejercicio de actividades intelectuales, para juegos o para actividades econ�micas, y las formas de presentar informaciones. Esta excepci�n se aplica porque dichos elementos y actividades no pertenecen al campo de la tecnolog�a.
rec7a	es	Con la presente Directiva no se pretende modificar dicho Convenio, sino evitar que puedan existir interpretaciones divergentes de su texto.
rec7b	es	El Parlamento Europeo ha solicitado varias veces que la Oficina Europea de Patentes revise sus normas de funcionamiento y que este organismo sea sometido a control p�blico en el ejercicio de sus funciones. A este respecto, ser�a especialmente oportuno reexaminar la pr�ctica de la Oficina Europea de Patentes de imponer un canon sobre las patentes que concede, ya que dicha pr�ctica vulnera el car�cter p�blico de la Instituci�n. <P>En su Resoluci�n, de 30 de marzo de 2000, sobre la decisi�n de la Oficina Europea de Patentes con respecto a la patente EP 695 351, expedida el 8 de diciembre de 1999<SUP>1</SUP>, el Parlamento Europeo solicit� la revisi�n de las normas de funcionamiento de la Oficina de forma que se garantizara que este organismo pueda rendir cuentas p�blicamente sobre el ejercicio de sus funciones. <P>__________<P><SUP>1</SUP> DO C 378 de 29.12.2000, p. 95.
rec11	es	Para ser patentables, las invenciones en general y las invenciones implementadas en ordenador en particular deben ser susceptibles de aplicaci�n industrial, nuevas y suponer una actividad inventiva. Para que entra�en una actividad inventiva, las invenciones implementadas en ordenador <I>deben</I> aportar adicionalmente una nueva contribuci�n t�cnica al estado de la t�cnica para diferenciarlas de los puros programas inform�ticos.
rec12	es	En consecuencia, una innovaci�n que no aporta una contribuci�n t�cnica al estado de la t�cnica, no constituye una invenci�n en el sentido del Derecho de patentes.
rec13	es	suprimido
rec13a	es	No obstante, la mera implementaci�n en un aparato, como un ordenador, de un m�todo considerado por otras razones no patentable no es suficiente por s� misma para justificar la conclusi�n de que existe una contribuci�n t�cnica. Por consiguiente, un m�todo comercial o de tratamiento de datos u otro m�todo cuya �nica contribuci�n al estado de la t�cnica sea de car�cter no t�cnico no puede constituir una invenci�n patentable.
rec13b	es	Si la contribuci�n al estado de la t�cnica reside exclusivamente en elementos no patentables, no puede considerarse que la invenci�n sea patentable, independientemente de c�mo se presente el elemento en las reivindicaciones. Por ejemplo, el requisito de la contribuci�n t�cnica no podr� eludirse con una mera especificaci�n de medios t�cnicos en las reivindicaciones de patente.
rec13c	es	Adem�s, un algoritmo es esencialmente no t�cnico, por lo que no puede constituir una invenci�n t�cnica. No obstante, un m�todo que comprenda el uso de un algoritmo puede ser patentable siempre que dicho m�todo se utilice para resolver un problema t�cnico. Sin embargo, una patente concedida por un m�todo de estas caracter�sticas no debe permitir que se monopolice el propio algoritmo o su uso en contextos no previstos en la patente.
rec13d	es	El �mbito de los derechos exclusivos conferidos por una patente se define en las reivindicaciones de patentes. Las invenciones implementadas en ordenador deben reivindicarse haciendo referencia a un producto como, por ejemplo, un aparato programado, o a un procedimiento realizado en un aparato de esas caracter�sticas. En este contexto, en aquellos casos en que se utilicen distintos elementos de programas inform�ticos en contextos que no impliquen la realizaci�n de un producto o de un procedimiento reivindicado de forma v�lida, esta utilizaci�n no debe considerarse una vulneraci�n de patente.
rec14	es	La protecci�n jur�dica de las invenciones implementadas en ordenador no precisa la creaci�n de una disposici�n jur�dica independiente que sustituya a las normas de la legislaci�n nacional sobre patentes. Las normas de la legislaci�n nacional sobre patentes siguen siendo la referencia b�sica para la protecci�n jur�dica de las invenciones implementadas en ordenador. La presente Directiva simplemente clarifica la posici�n jur�dica vigente con miras a garantizar la seguridad jur�dica, la transparencia y la claridad de la ley y a evitar toda desviaci�n hacia la patentabilidad de m�todos no patentables, como los procedimientos triviales y los m�todos comerciales.
rec16	es	La posici�n competitiva de la industria europea respecto a sus principales socios comerciales mejorar� si se eliminan las diferencias existentes en la protecci�n jur�dica de las invenciones implementadas en ordenador y se garantiza la transparencia de la situaci�n jur�dica. Dada la tendencia actual de la industria manufacturera tradicional a trasladar sus operaciones a econom�as de bajo coste fuera de la Uni�n Europea, resulta evidente por s� misma la importancia de la protecci�n de la propiedad intelectual y, en particular, de la protecci�n mediante patentes.
rec17	es	La presente Directiva deber�a entenderse sin perjuicio de la aplicaci�n de las normas sobre competencia, en particular los art�culos 81 y 82 del Tratado.
rec18	es	Los derechos que confieren las patentes concedidas para invenciones en el �mbito de la presente Directiva no deben afectar a actos permitidos en el marco de los art�culos 5 y 6 de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular en el marco de sus preceptos por lo que se refiere a la descompilaci�n y la interoperabilidad. En particular, los actos que, en virtud de los art�culos 5 y 6 de dicha Directiva, no necesiten autorizaci�n del titular del derecho respecto de los derechos de autor del titular en un programa inform�tico o en relaci�n con el mismo, y para los que, fuera del �mbito de los art�culos 5 o 6 de la Directiva 91/250/CEE se exigir�a esta autorizaci�n, no deben precisar la autorizaci�n del titular del derecho respecto de sus derechos de patente en el programa inform�tico o en relaci�n con el mismo.
rec18a	es	En todo caso, la legislaci�n de los Estados miembros debe asegurarse de que las patentes contengan novedades y supongan una actividad inventiva para impedir que invenciones que son ya del dominio p�blico sean apropiadas por el mero hecho de incorporarse a un programa de ordenador.
art2a	es	(a) 'invenci�n implementada en ordenador', toda invenci�n en el sentido del Convenio sobre la Patente Europea para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga en sus aplicaciones una o m�s caracter�sticas no t�cnicas que se realicen total o parcialmente mediante un programa o programas de ordenador, sin perjuicio de las caracter�sticas t�cnicas que debe presentar toda invenci�n;
art2b	es	(b) 'contribuci�n t�cnica', tambi�n llamada 'invenci�n', una contribuci�n al estado de la t�cnica en un campo tecnol�gico. El car�cter t�cnico de la contribuci�n constituye uno de los cuatro requisitos de la patentabilidad. Adem�s, para merecer una patente, la contribuci�n t�cnica debe ser nueva, no evidente y susceptible de aplicaci�n industrial. La utilizaci�n de las fuerzas de la naturaleza para controlar los efectos f�sicos, m�s all� de la representaci�n digital de las informaciones, pertenece a un campo tecnol�gico. El tratamiento, la manipulaci�n y la presentaci�n de informaciones no pertenecen a un campo tecnol�gico, aunque se utilicen instrumentos t�cnicos para estas operaciones.
art2ba	es	b bis) 'campo tecnol�gico' designa un campo industrial de aplicaci�n que requiere la utilizaci�n de fuerzas controlables de la naturaleza para obtener resultados previsibles. 'T�cnico' significa que pertenece a un campo tecnol�gico.
art2bb	es	b ter) 'industria' en el sentido del Derecho de patentes, 'producci�n autom�tica de bienes materiales';
art3	es	suprimido
art3a	es	Art�culo 3 bis <P>Los Estados miembros garantizar�n que el tratamiento de datos no se considere un campo tecnol�gico en el sentido del Derecho de patentes, y que las innovaciones en el campo del tratamiento de datos no se consideren innovaciones en el sentido del Derecho de patentes.
art4	es	Para ser patentable, una invenci�n implementada en ordenador deber� ser susceptible de aplicaci�n industrial, nueva y suponer una actividad inventiva. Para entra�ar una actividad inventiva, la invenci�n implementada en ordenador deber� aportar una contribuci�n t�cnica. <P>Los Estados miembros garantizar�n que constituya una condici�n necesaria de la actividad inventiva el hecho de que una invenci�n implementada en ordenador aporte una contribuci�n t�cnica. <P>El alcance significativo de la contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y todas las caracter�sticas t�cnicas de la reivindicaci�n de la patente, con independencia de que �stas est�n acompa�adas por caracter�sticas no t�cnicas. <P>3 bis. Al determinar si una invenci�n implementada en ordenador aporta una contribuci�n t�cnica, se deber� verificar si constituye una nueva ense�anza sobre las relaciones causa-efecto en el uso de las fuerzas controlables de la naturaleza y si tiene una aplicaci�n industrial en el sentido estricto de la expresi�n, tanto en t�rminos de m�todo como de resultado.
art4a	es	Art�culo 4 bis <P>Causas de exclusi�n de la patentabilidad <P>No se considerar� que una invenci�n implementada en ordenador aporta una contribuci�n t�cnica meramente porque implique el uso de un ordenador, red u otro aparato programable. En consecuencia, no ser�n patentables las invenciones que utilizan programas inform�ticos que aplican m�todos comerciales, matem�ticos o de otro tipo y no producen efectos t�cnicos, aparte de la normal interacci�n f�sica entre un programa y el ordenador, red o aparato programable de otro tipo en que se ejecute.
art4b	es	Art�culo 4 ter <P>Los Estados miembros garantizar�n que las soluciones a problemas t�cnicos implementadas en ordenador no se consideren como invenciones patentables simplemente por el hecho de que mejoran la eficiencia en la utilizaci�n de los recursos dentro del sistema de tratamiento de datos.
art5_1	es	Los Estados miembros garantizar�n que las invenciones implementadas en ordenador s�lo puedan reivindicarse como producto, es decir, como aparato programado, o como procedimiento t�cnico de producci�n.
art5_1a	es	1 bis. Los Estados miembros garantizar�n que las patentes concedidas a invenciones implementadas en ordenador s�lo comprendan la contribuci�n t�cnica en la que se fundamente la reivindicaci�n. Las reivindicaciones de patente referidas a un programa de ordenador, almacenado o no en soporte f�sico, no son admisibles.
art5_1b	es	1 ter. Los Estados miembros velar�n por que la producci�n, la manipulaci�n, el tratamiento, la distribuci�n y la publicaci�n de informaci�n, en cualquier forma, no pueda constituir en ning�n caso, directa o indirectamente, una violaci�n de las patentes, incluso cuando se utilicen dispositivos t�cnicos con este fin.
art5_1c	es	1 qu�ter. Los Estados miembros garantizar�n que la utilizaci�n de un programa de ordenador para fines no incluidos en el �mbito de la patente no pueda constituir una violaci�n directa o indirecta de la patente. <P>1 quinquies. Los Estados miembros garantizar�n que, siempre que una reivindicaci�n de patente mencione caracter�sticas que impliquen la utilizaci�n de un programa de ordenador, se publique una implementaci�n de referencia de tal programa, de funcionamiento correcto y bien documentada, como parte de la descripci�n y sin ning�n tipo de condiciones de autorizaci�n restrictivas.
art6	es	Los derechos derivados de patentes otorgadas a invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva no afectar�n a los actos permitidos en virtud de los art�culos 5 y 6 de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular en aplicaci�n de sus preceptos sobre la descompilaci�n y la interoperabilidad.
art6a	es	Art�culo 6 bis<P>Utilizaci�n de t�cnicas patentadas</P><P>Los Estados miembros garantizar�n que, en cualquier caso en que sea necesaria la utilizaci�n de una t�cnica patentada con un prop�sito significativo, como es asegurar la conversi�n de las convenciones utilizadas en dos sistemas de ordenadores o redes diferentes a fin de hacer posible la comunicaci�n y el intercambio rec�proco de datos, esta utilizaci�n no se considere una violaci�n de la patente.
art7	es	La Comisi�n seguir� de cerca el impacto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, en particular las peque�as y medianas empresas y la comunidad que defiende el software libre, y el comercio electr�nico.
art8a	es	Art�culo 8 bis <P>Evaluaci�n del impacto <P>Teniendo en cuenta el seguimiento realizado de conformidad con el art�culo 7 y el informe elaborado de conformidad con el art�culo 8, la Comisi�n evaluar� el impacto de la presente Directiva y, si procede, presentar� al Parlamento Europeo y al Consejo propuestas para modificar la legislaci�n.
art8b	es	b) si las normas que rigen la duraci�n de la patente y la determinaci�n de los requisitos de patentabilidad, y m�s concretamente la novedad, la actividad inventiva y el �mbito concreto de las reivindicaciones, son adecuadas; y
art8ca	es	c bis) si se han encontrado dificultades respecto de la relaci�n entre la protecci�n mediante patente de las invenciones implementadas en ordenador y la protecci�n mediante derechos de autor de los programas inform�ticos contemplada en la Directiva 91/250/CE y si se ha producido alg�n abuso del sistema de patentes en relaci�n con las invenciones implementadas en ordenador;
art8cb	es	c ter) si resultar�a conveniente y posible jur�dicamente, en vista de las obligaciones internacionales de la Comunidad, introducir un 'periodo de gracia' respecto de los elementos de una solicitud de patente para cualquier tipo de invenci�n divulgada antes de la fecha de la solicitud;
art8cc	es	c qu�ter) los aspectos en que puede ser necesario preparar una Conferencia Diplom�tica para revisar el Convenio sobre la concesi�n de patentes europeas, tambi�n a la luz de la llegada de la patente comunitaria;
art8cd	es	c quinquies) la manera en que las disposiciones de la presente Directiva se han tomado en consideraci�n en la pr�ctica de la Oficina Europea de Patentes y en sus orientaciones de examen.
art8ce	es	c sexies) si los poderes delegados a la Oficina Europea de Patentes son compatibles con las exigencias de armonizaci�n de la legislaci�n de la Uni�n Europea, adem�s de con los principios de transparencia y responsabilidad;
art8cf	es	c septies) el impacto en la conversi�n de las convenciones utilizadas en dos sistemas de ordenadores diferentes a fin de hacer posible la comunicaci�n y el intercambio rec�proco de datos;
art8cg	es	c octies) si la opci�n se�alada en la Directiva sobre el uso de una invenci�n patentada con el �nico prop�sito de garantizar la interoperabilidad entre dos sistemas es adecuada;
art8_1a	es	En este informe, la Comisi�n justificar� el motivo por el que considera o no necesaria una modificaci�n de la Directiva en cuesti�n y, si es preciso, enumerar� los puntos en los que tiene previsto proponer modificaciones.
art9_1	es	Los Estados miembros adoptar�n, a m�s tardar �* las disposiciones legislativas, reglamentarias y administrativas necesarias para dar cumplimiento a lo dispuesto en la presente Directiva. Informar�n de ello inmediatamente a la Comisi�n. <P>___________<P>* Dieciocho meses despu�s de su entrada en vigor.
rec1	fi	Sis�markkinoiden toteutumisen my�t� vapaan liikkuvuuden esteet ja kilpailua v��rist�v�t tekij�t poistuvat ja luodaan innovaatiotoiminnalle ja investoinneille suotuisa toimintaymp�rist�. T�ss� yhteydess� keksint�jen suojaaminen patentein on sis�markkinoiden onnistumista edist�v� keskeinen tekij�. Tietokoneella toteutettujen keksint�jen tehokas, avoin ja yhdenmukainen suoja kaikissa j�senvaltioissa on olennaisen t�rke�� alan investointien jatkumiseksi ja kannustamiseksi.
rec5	fi	N�in ollen tietokoneella toteutettujen keksint�jen patentoitavuutta s��ntelev�t oikeudelliset s��nn�t olisi yhdenmukaistettava, jotta varmistetaan, ett� tuloksena saavutettavan oikeusvarmuuden ja patentoitavuudelle asetettavan vaatimustason ansiosta yritykset voivat saada mahdollisimman paljon etua innovointiprosesseistaan, ja edistet��n investointeja ja innovaatiotoimintaa. Oikeusvarmuus taataan my�s sill�, ett� kun t�m�n direktiivin tulkintaan liittyy ep�ilyksi�, kansalliset tuomioistuimet voivat pyyt�� ja ylimm�n oikeusasteen kansallisten tuomioistuinten on pyydett�v� ratkaisua Euroopan yhteis�jen tuomioistuimelta.
rec5a	fi	On syyt� vahvistaa ja t�sment�� Euroopan patenttisopimuksen 52 artiklan mukaiset s��nn�kset patentoitavuuden rajoista. Siit� johtuva oikeusvarmuus edist�� ohjelmistoalan investointeja ja innovaatioita.
rec6	fi	Poistetaan.
rec7	fi	Eurooppapatenttien my�nt�misest� M�ncheniss� 5 p�iv�n� lokakuuta 1973 allekirjoitetun yleissopimuksen ja j�senvaltioiden patenttis��d�sten mukaan keksint�in� ei nimenomaisesti pidet� tietokoneohjelmia eik� l�yt�j�, tieteellisi� teorioita, matemaattisia menetelmi�, taiteellisia luomuksia eik� �lyllist� toimintaa, pelej� tai liiketoimintaa varten tarkoitettuja suunnitelmia, s��nt�j� tai menetelmi� eik� tietojen esitt�mist�, joten n�m� kohteet eiv�t ole patentoitavissa. T�t� poikkeusta sovelletaan, koska mainitut kohteet tai toiminnot eiv�t kuulu mihink��n tekniikanalaan.
rec7a	fi	T�ll� direktiivill� ei pyrit� muuttamaan kyseist� yleissopimusta, vaan est�m��n, ett� sen teksti� voidaan tulkita eri tavoin.
rec7b	fi	Euroopan parlamentti on useaan otteeseen vaatinut, ett� Euroopan patenttivirasto tarkistaisi toimintas��nt�j��n ja ett� sen teht�vien hoitamista valvottaisiin julkisesti. Olisi erityisen suotavaa kyseenalaistaa Euroopan patenttiviraston k�yt�nt� peri� maksu my�nt�mist��n patenteista, sill� se on haitaksi virastolle julkisena laitoksena. <P>Euroopan patenttiviraston 8 p�iv�n� joulukuuta 1999 tekem�st� patenttia EP 695 351 koskevasta p��t�ksest� 30 p�iv�n� maaliskuuta 2000 antamassaan p��t�slauselmassa<SUP>1</SUP> Euroopan parlamentti vaati tarkistamaan Euroopan patenttiviraston toimintas��nt�j� sen varmistamiseksi, ett� siit� tulee teht�viens� hoitamisesta julkisesti vastuussa oleva elin. <P>______<P>1 EYVL C 378, 29.12.2000, s. 95.
rec11	fi	Kaikkien keksint�jen ja erityisesti tietokoneella toteutettujen keksint�jen patentoitavuus edellytt��, ett� ne ovat uusia, keksinn�llisi� ja teollisesti k�ytt�kelpoisia. Jotta tietokoneella toteutettuja keksint�j� voidaan pit�� keksinn�llisin�, niiden on tuotava lis�ys vallitsevaan tekniikan tasoon, jotta ne voidaan erottaa pelkist� ohjelmistoista.
rec12	fi	N�in ollen keksint�, joka ei tuo lis�yst� vallitsevaan tekniikan tasoon, ei ole keksint� patenttilains��d�nn�n kannalta.
rec13	fi	Poistetaan.
rec13a	fi	Pelk�st��n se seikka, ett� menetelm�, joka ei muutoin ole patentoitavissa, on toteutettu sellaisella v�lineell� kuin tietokone, ei sin�ll��n ole riitt�v� osoitus lis�yksest� tekniikan tasoon. N�in ollen mit��n sellaista tietokoneella toteutettua liiketoiminnan menetelm��, tietojenk�sittelymenetelm�� tai jotakin muuta menetelm��, jossa ainoa lis�ys vallitsevaan tekniikan tasoon ei ole ominaisuuksiltaan tekninen, ei voida pit�� patentoitavissa olevana keksint�n�.
rec13b	fi	Jos lis�ys vallitsevaan tekniikan tasoon koskee ainoastaan sellaista keksinn�n osaa, joka ei ole patentoitavissa, keksint�� ei voida patentoida riippumatta siit�, kuinka asia on esitetty patenttivaatimuksessa. Esimerkiksi vaatimusta lis�yksest� tekniikan tasoon ei voida kiert�� pelk�st��n esitt�m�ll� patenttivaatimuksessa, mit� teknisi� v�lineit� on k�ytetty.
rec13c	fi	Algoritmi ei ole sin�ll��n luonteeltaan tekninen ja siksi sit� ei voida pit�� teknisen� keksint�n�. Menetelm�n, johon sis�ltyy algoritmi, olisi voitava kuitenkin olla patentoitavissa edellytt�en, ett� menetelm�� k�ytet��n teknisen ongelman ratkaisuun. T�llaiselle menetelm�lle my�nnetyll� patentilla ei kuitenkaan tulisi olla yksinoikeutta itse algoritmiin tai sen k�ytt��n muussa kuin patentin m��r��m�ss� tarkoituksessa.
rec13d	fi	Patentin antamien yksinoikeuksien laajuus m��ritell��n patenttivaatimuksissa. Tietokoneella toteutetuista keksinn�ist� on teht�v� vaatimus, joka koskee joko ohjelmoidun laitteen kaltaista tuotetta tai t�llaisessa laitteessa toteutettua prosessia. Samoin kun ohjelmien yksitt�isi� osia k�ytet��n yhteyksiss�, joihin ei liity mink��n voimassa olevan patenttivaatimuksen alaisen tuotteen tai prosessin toteutumista, t�llainen k�ytt� ei merkitse patentin loukkausta.
rec14	fi	Tietokoneella toteutettujen keksint�jen oikeudellinen suoja ei edellyt� erillist� s��ntely�, joka korvaisi kansallisten patenttis��d�sten s��nn�t. Tietokoneella toteutettujen keksint�jen oikeudellisen suojan on edelleen perustuttava p��osin kansallisten patenttis��d�sten s��nt�ihin sellaisina kuin niit� on t�m�n direktiivin mukaisesti tietyilt� osin mukautettu tai t�ydennetty. T�ll� direktiivill� pyrit��n ainoastaan selvent�m��n nykyist� oikeusasemaa, varmistamaan oikeusvarmuus, avoimuus ja lains��d�nn�n selkeys ja v�ltt�m��n patentoitaviksi kelpaamattomien menetelmien kuten tavanomaisten tapahtumien ja liiketoiminnan menetelmien patentoimista.
rec16	fi	Euroopan teollisuuden kilpailuasema suhteessa t�rkeimpiin kauppakumppaneihin paranee, jos tietokoneella toteutettujen keksint�jen oikeudellisen suojan nykyiset erot poistetaan ja oikeudellisen tilanteen avoimuutta lis�t��n. Ottaen huomioon, ett� perinteinen valmistusteollisuus pyrkii nyky��n siirt�m��n tuotantoaan matalapalkkamaihin Euroopan unionin ulkopuolelle, teollis- ja tekij�noikeuksien suojelun ja etenkin patenttisuojan merkitys on ilmeisen t�rke�.
rec17	fi	T�ll� direktiivill� ei saisi rajoittaa kilpailus��nt�jen eik� etenk��n perustamissopimuksen 81 ja 82 artiklan soveltamista.
rec18	fi	T�m�n direktiivin soveltamisalaan kuuluville keksinn�ille my�nnettyihin patentteihin liittyv�t oikeudet eiv�t saisi vaikuttaa tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY 5 ja 6 artiklan mukaisesti sallittuihin toimiin ottaen huomioon erityisesti n�iden artiklojen sis�lt�m�t analysointia ja yhteentoimivuutta koskevat s��nn�kset. Erityisesti toimissa, joissa 5 ja 6 artiklan mukaisesti ei tarvita oikeudenomistajan lupaa liittyen oikeudenomistajalle kuuluviin tietokoneohjelman tekij�noikeuksiin ja joihin t�llainen lupa tarvittaisiin kyseisten artiklojen m��r�yksi�, ei tarvita oikeudenomistajan lupaa liittyen oikeudenomistajan tietokoneohjelmia koskeviin patenttioikeuksiin.
rec18a	fi	J�senvaltioiden lains��d�nn�ss� on taattava, ett� patentteihin sis�ltyy uutuuksia ja keksinn�llinen osuus, jotta v�ltett�isiin, ett� jo yleisesti saatavilla olevat keksinn�t ovat soveliaita patentoitaviksi yksinomaan siit� syyst�, ett� ne on sis�llytetty tietokoneohjelmaan.
art2a	fi	a) 'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan Euroopan patenttisopimuksen mukaisesti keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja jonka toteuttamiseen liittyy sellaisten teknisten piirteiden ohella, joita edellytet��n kaikilta keksinn�ilt�, yksi tai useampi sellainen ei-tekninen piirre, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art2b	fi	b) 'Lis�yksell� tekniikan tasoon', jota kutsutaan my�s 'keksinn�ksi', tarkoitetaan lis�yst� tekniikanalalla vallitsevaan tasoon. Lis�yksen tekninen luonne on yksi patentoitavuuden nelj�st� vaatimuksesta. Lis�ksi patentin ansaitakseen lis�yksen tekniikan tasoon on oltava uusi mutta ei itsest��n selv� ja sovellettavissa teolliseen k�ytt��n. Tekniikanalaan kuuluu luonnonvoimien k�ytt�minen fyysisten vaikutusten kontrolloimiseksi tietojen digitaalisen esitt�misen lis�ksi. Tietojen k�sittely, manipulointi ja esittely eiv�t kuulu tekniikanalaan, vaikka n�iss� k�ytett�isiin teknisi� laitteita.
art2ba	fi	b a) 'Teknisell� alalla' tarkoitetaan teollisen sovelluksen alaa, jolla vaaditaan hallittavissa olevien luonnonvoimien k�ytt�� ennakoitujen tulosten saavuttamiseksi; 'Teknisell�' tarkoitetaan 'tekniseen alaan kuuluvaa';
art2bb	fi	b b)'teollisuudella' tarkoitetaan patenttilains��d�nn�ss� 'materiaalisten hy�dykkeiden automatisoitua tuotantoa'.
art3	fi	<P>Poistetaan.
art3a	fi	3 a artikla <P>J�senvaltioiden on varmistettava, ett� tietojenk�sittely� ei pidet� patenttilains��d�nn�n mukaisena teknologian alana ja ett� tietojenk�sittelyn alan innovaatioita ei pidet� patenttilains��d�nn�n tarkoittamina keksint�in�.
art4	fi	Jotta tietokoneella toteutettu keksint� olisi patentoitavissa, sen on oltava teollisesti k�ytt�kelpoinen, uusi ja keksinn�llinen. Tietokoneella toteutettu keksint� on keksinn�llinen vain, kun se tuo lis�yksen tekniikan tasoon. <P>J�senvaltioiden on varmistettava, ett� tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon on v�ltt�m�t�n keksinn�llisyyden edellytys. <P>Merkitt�v�� lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on patenttivaatimuksen kaikkien teknisten piirteiden ja vallitsevan <I>tekniikan </I>tason v�lill�, riippumatta siit�, liittyyk� n�ihin piirteisiin muitakin kuin teknisi� piirteit�. <P>3 a. M��ritelt�ess�, antaako tietty tietokoneella toteutettu keksint� lis�yst� tekniikan tasoon, tutkitaan, antaako keksint� luonnonvoimien hallitun k�yt�n syysuhdetta koskevaa uutta tietoa ja onko se ilmauksen tiukassa mieless� teollisesti k�ytt�kelpoinen sek� menetelm�n ett� tuloksen kannalta.
art4a	fi	4 a artikla <P>Poikkeukset patentoitavuudesta <P>Tietokoneella toteutettua keksint�� ei katsota lis�ykseksi tekniikan tasoon pelk�st��n sen perusteella, ett� se edellytt�� tietokoneen, verkon tai muun laitteiston k�ytt��. Patentoitavissa eiv�t siis ole keksinn�t, joissa suoritetaan tietokoneohjelmien avulla sellaisia liiketoiminnan, matemaattisia tai muita menetelmi�, joilla ei saada aikaan muuta teknist� vaikutusta kuin normaali fyysinen vuorovaikutus ohjelman ja tietokoneen, verkon tai muun sellaisen ohjelmoitavan laitteen v�lill�, jossa ohjelma toimii.
art4b	fi	4 b artikla <P>J�senvaltioiden on varmistettava, ett� tietokoneella teknisiin ongelmiin toteutettuja ratkaisuja ei pidet� patentoitavina vain siksi, ett� ne parantavat resurssien k�yt�n tehokkuutta tietojenk�sittelyj�rjestelm�ss�.
art5	fi	1 c) J�senvaltioiden on varmistettava, ett� tietokoneohjelman k�ytt� muihin kuin patentin suojapiiriin kuuluviin tarkoituksiin ei voi muodostaa suoraa tai v�lillist� patentinloukkausta. <P>1 d) J�senvaltioiden on varmistettava, ett� aina kun patenttivaatimuksessa mainitaan piirteit�, jotka viittaavat tietokoneohjelman k�ytt��n, kuvauksen osana on julkaistava toimiva ja hyvin dokumentoitu ohjelman viitetoteutus ilman rajoittavia lisenssiehtoja.
art5_1a	fi	1 a. J�senvaltioiden on varmistettava, ett� tietokoneella toteutettuja keksint�j� koskevat hyv�ksytyt patenttivaatimukset sis�lt�v�t vain patenttivaatimuksessa perustellun lis�yksen tekniikan tasoon. Tietokoneohjelmaa sellaisenaan tai tietov�lineell� olevaa ohjelmaa koskeva patenttivaatimus ei ole hyv�ksytt�viss�.
art5_1b	fi	1 b) J�senvaltioiden on varmistettava, ett� tiedon tuottaminen, muokkaus, k�sittely, levitt�minen ja julkistaminen miss� muodossa tahansa ei voi koskaan olla suora tai v�lillinen patentinloukkaus, vaikka niiss� k�ytett�isiin teknisi� apuv�lineit�.
art6	fi	T�m�n direktiivin soveltamisalaan kuuluville keksinn�ille my�nnettyihin patentteihin liittyv�t oikeudet eiv�t vaikuta tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY 5 ja 6 artiklan mukaisesti sallittuihin toimiin ottaen huomioon erityisesti n�iden artiklojen sis�lt�m�t analysointia ja yhteentoimivuutta koskevat s��nn�kset.
art6a	fi	6 a artikla <P>Patentoitujen tekniikoiden k�ytt� <P>J�senvaltioiden on varmistettava, ett� aina kun patentoitua tekniikkaa tarvitsee k�ytt�� t�rke��n tarkoitukseen, kuten varmistamaan kahden eri tietokonej�rjestelm�n tai verkon konventioiden konversio niiden v�lisen yhteyden ja tietojenvaihdon mahdollistamiseksi, t�llaista k�ytt�� ei katsota patentinloukkaukseksi.
art7	fi	Komissio seuraa tietokoneella toteutettujen keksint�jen vaikutuksia innovaatiotoimintaan ja kilpailuun Euroopan laajuisesti ja kansainv�lisesti sek� niiden vaikutuksia eurooppalaisiin yrityksiin, erityisesti pieniin ja keskikokoisiin yrityksiin ja avoimen l�hdekoodin kannattajiin, ja s�hk�iseen kaupank�yntiin.
art8a	fi	8 a artikla <P>Vaikutuksen arviointi <P>Komissio arvioi t�m�n direktiivin vaikutusta 7 artiklan mukaisesti suoritetun seurannan ja 8 artiklan mukaisesti laaditun kertomuksen perusteella ja esitt�� tarvittaessa Euroopan parlamentille ja neuvostolle ehdotuksia lains��d�nn�n muuttamiseksi.
art8b	fi	b) siit�, ovatko patentin voimassaoloajalle ja patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; ja
art8ca	fi	c a) mahdollisista vaikeuksista, joita on aiheutunut tietokoneella toteutettujen keksint�jen patenttisuojan ja direktiivin 91/250/ETY mukaisen tietokoneohjelmien tekij�noikeussuojan v�lisest� suhteesta, ja mahdollisista tietokoneella toteutettuihin keksint�ihin liittyvist� patenttij�rjestelm�n v��rink�yt�ksist�;
art8cb	fi	c b) siit�, olisiko suotavaa ja laillisesti mahdollista ottaen huomioon yhteis�n kansainv�liset velvoitteet ottaa k�ytt��n ns. 'armonaika' kaikenlaisten keksint�jen patenttihakemukseen sis�ltyville osille, jotka julkistetaan ennen patenttihakemuksen tekemisp�iv��;
art8cc	fi	c c) siit�, miss� suhteessa saattaa olla tarpeen valmistella diplomaattikokousta Eurooppapatenttien my�nt�mist� koskevan yleissopimuksen tarkistamiseksi etenkin tulevaan yhteis�patenttiin liittyen;
art8cd	fi	c d) siit�, miten t�m�n direktiivin vaatimukset on otettu k�yt�nn�ss� huomioon Euroopan patenttivirastossa ja sen tutkintaohjeissa;
art8ce	fi	(c e) siit�, ovatko Euroopan patenttivirastolle siirretyt valtuudet yhdenmukaisia unionin lains��d�nn�n harmonisointipyrkimysten sek� avoimuuden ja vastuullisuuden periaatteiden kanssa;
art8cf	fi	c f) vaikutuksesta, joka kahden eri tietokonej�rjestelm�n konventioiden konversiolla niiden v�lisen yhteyden ja tietojenvaihdon mahdollistamiseksi on;
art8cg	fi	c g) siit�, onko direktiiviss� esitetty vaihtoehto, joka koskee patentoidun keksinn�n k�ytt�� ainoastaan kahden j�rjestelm�n yhteensopivuuden varmistamista varten, sovelias;
art8_1a	fi	Komissio perustelee t�ss� kertomuksessa, miksi se pit�� direktiivin muuttamista tarpeellisena tai tarpeettomana, ja esitt�� vaadittaessa luettelon kohdista, joihin se aikoo esitt�� tarkistuksen.
art9_1	fi	J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n ...*. Niiden on ilmoitettava t�st� komissiolle viipym�tt�. <P>__________<P>* 18 kuukauden kuluttua direktiivin voimaantulosta.
rec1	fr	La r�alisation du march� int�rieur implique que l'on �limine les restrictions � la libre circulation et les distorsions � la concurrence, tout en cr�ant un environnement favorable � l'innovation et � l'investissement. Dans ce contexte, la protection des inventions par brevet est un �l�ment essentiel du succ�s du march� int�rieur. Une protection effective, transparente et harmonis�e des inventions mises en oeuvre par ordinateur dans tous les �tats membres est essentielle pour maintenir et encourager les investissements dans ce domaine.
rec5	fr	En cons�quence, les r�gles de droit r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre harmonis�es de fa�on � assurer que la s�curit� juridique qui en r�sulte et le niveau des crit�res de brevetabilit� permettent aux entreprises innovatrices de tirer le meilleur parti de leur processus inventif et stimulent l'investissement et l'innovation. La s�curit� juridique est �galement assur�e par le fait que, en cas de doute quant � l'interpr�tation de la pr�sente directive, les juridictions nationales ont la possibilit�, et les juridictions nationales de derni�re instance l'obligation, de demander � la Cour de justice des Communaut�s europ�ennes de statuer.
rec5a	fr	Les dispositions vis�es � l'article 52 de la Convention sur la d�livrance de brevets europ�ens relatifs aux limites de la brevetabilit� devraient �tre renforc�es et pr�cis�es. La s�curit� juridique qui en d�coule contribue � l'instauration d'un climat favorable aux investissements et � l'innovation dans le domaine du software.
rec6	fr	supprim�
rec7	fr	En vertu de la Convention sur la d�livrance de brevets europ�ens sign�e � Munich, le 5 octobre 1973, et du droit des brevets des �tats membres, les programmes d'ordinateurs ainsi que les d�couvertes, th�ories scientifiques, m�thodes math�matiques, cr�ations esth�tiques, plans, principes et m�thodes dans l'exercice d'activit�s intellectuelles, en mati�re de jeu ou dans le domaine des activit�s �conomiques et les pr�sentations d'informations, ne sont pas consid�r�s comme des inventions et sont donc exclus de la brevetabilit�. Cette exception s'applique parce que lesdits objets et activit�s n'appartiennent � aucun domaine technique.
rec7a	fr	La pr�sente directive ne vise pas � modifier ladite Convention mais � �viter des interpr�tations divergentes de son texte.
rec7b	fr	Le Parlement europ�en a, � plusieurs reprises, demand� que l'Office europ�en des brevets r�vise ses r�gles de fonctionnement et que cet organisme soit contr�l� publiquement dans l'exercice de ses fonctions. � cet �gard, il serait particuli�rement opportun de remettre en cause la pratique qui am�ne l'Office europ�en des brevets � se r�tribuer sur les brevets qu'il d�livre, dans la mesure o� cette pratique nuit au caract�re public de l'institution. Dans sa r�solution du 30 mars 2000 sur la d�cision de l'Office europ�en des brevets en ce qui concerne le brevet n� EP 695 351 d�livr� le 8 d�cembre 1999<SUP>1</SUP>, le Parlement europ�en a demand� une r�vision des r�gles de fonctionnement de l'Office afin d'assurer un contr�le public de l'exercice de ses fonctions.<P>__________</P><P><SUP>1 </SUP>JO C 378 du 29.12.2000, p. 95.
rec11	fr	Pour �tre brevetables, les inventions en g�n�ral et les inventions mises en oeuvre par ordinateur en particulier doivent �tre nouvelles, impliquer une activit� inventive, et �tre susceptibles d'application industrielle. Pour impliquer une activit� inventive, les inventions mises en oeuvre par ordinateur devraient, de plus, apporter une contribution technique � l'�tat de la technique, afin de les diff�rencier du simple software.
rec12	fr	En cons�quence, une innovation qui n'apporte pas de contribution technique � l'�tat de la technique n'est pas une invention au sens du droit des brevets.
rec13	fr	supprim�
rec13a	fr	Toutefois, la simple mise en oeuvre d'une m�thode, par ailleurs non brevetable, sur un appareil tel qu'un ordinateur ne suffit pas, en soi, � justifier l'existence d'une contribution technique. En cons�quence, une m�thode de traitement des donn�es, une m�thode destin�e � l'exercice d'activit�s �conomiques, ou une autre m�thode, mise en oeuvre par ordinateur, dont la seule contribution � l'�tat de la technique n'est pas de nature technique ne peut constituer une invention brevetable.
rec13b	fr	L'invention n'est en aucun cas brevetable si la contribution � l'�tat de la technique se rapporte uniquement � des �l�ments non brevetables, quelle que soit la fa�on dont l'objet du brevet est pr�sent� dans la revendication. Ainsi, l'exigence d'une contribution technique ne peut �tre contourn�e uniquement en sp�cifiant des moyens techniques dans la revendication de brevet.
rec13c	fr	En outre, un algorithme est, par nature, non technique et ne peut donc constituer une invention technique. Une m�thode recourant � un algorithme peut n�anmoins �tre brevetable, dans la mesure o� elle est utilis�e pour r�soudre un probl�me technique. Toutefois, tout brevet accord� pour cette m�thode ne doit pas �tablir un monopole sur l'algorithme lui-m�me ou sur son utilisation dans des contextes non pr�vus par le brevet.
rec13d	fr	Le champ d'application des droits exclusifs conf�r�s par tout brevet est d�fini par les revendications. Les inventions mises en oeuvre par ordinateur doivent �tre revendiqu�es en faisant r�f�rence � un produit, tel qu'un appareil programm�, ou � un proc�d� r�alis� sur un tel appareil. En cons�quence, lorsque des �l�ments individuels de logiciel sont utilis�s dans des contextes qui ne comportent pas la r�alisation d'un produit ou d'un proc�d� faisant l'objet d'une revendication valable, cette utilisation ne doit pas constituer une contrefa�on de brevet.
rec14	fr	La protection juridique des inventions mises en oeuvre par ordinateur ne n�cessite pas l'�tablissement d'une l�gislation distincte en lieu et place des dispositions du droit national des brevets. Les r�gles du droit national des brevets continuent de former la base de r�f�rence de la protection juridique des inventions mises en oeuvre par ordinateur. La pr�sente directive clarifie simplement la situation juridique actuelle, en vue d'assurer la s�curit� juridique, la transparence et la clart� de la l�gislation et d'�viter toute d�rive vers la brevetabilit� de m�thodes non brevetables, telles que des proc�dures triviales et des m�thodes destin�es � l'exercice d'activit�s �conomiques.
rec16	fr	La position concurrentielle de l'industrie europ�enne vis-�-vis de ses principaux partenaires commerciaux sera am�lior�e si les diff�rences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur sont �limin�es et si la situation juridique est transparente. �tant donn� la tendance actuelle, qui voit l'industrie manufacturi�re traditionnelle d�placer son activit� vers des �conomies o� les co�ts sont faibles � l'ext�rieur de l'Union europ�enne, l'importance de la protection de la propri�t� intellectuelle, et en particulier de la protection assur�e par le brevet, est �vidente.
rec17	fr	La pr�sente directive devrait s'appliquer sans pr�judice des r�gles de concurrence, en particulier des articles 81 et 82 du trait�.
rec18	fr	Les droits conf�r�s par les brevets d'invention d�livr�s dans le cadre de la pr�sente directive ne doivent pas porter atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit�. En particulier, les actes qui, en vertu des articles 5 et 6 de ladite directive, ne n�cessitent pas l'autorisation du titulaire du droit, au regard des droits d'auteur de ce titulaire aff�rents ou attach�s � un programme d'ordinateur, et qui, en l'absence desdits articles, n�cessiteraient cette autorisation, ne doivent pas n�cessiter l'autorisation du titulaire du droit, au regard des droits de brevet de ce titulaire aff�rents ou attach�s au programme d'ordinateur.
rec18a	fr	En toute hypoth�se, la l�gislation des �tats membres doit garantir que les brevets contiennent des �l�ments nouveaux et impliquent une activit� inventive, afin d'emp�cher que des inventions tomb�es dans le domaine public ne fassent l'objet d'une appropriation, simplement parce qu'elles font partie int�grante d'un programme informatique.
art2a	fr	<P>a) 'invention mise en oeuvre par ordinateur' d�signe toute invention au sens de la Convention sur le brevet europ�en dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou <I>d'un autre</I> appareil programmable et pr�sentant dans sa mise en oeuvre une ou plusieurs caract�ristiques non techniques qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs, en plus des caract�ristiques techniques que toute invention doit poss�der;</P>
art2b	fr	<P>b) 'contribution technique' , �galement appel�e 'invention', d�signe une contribution � l'�tat de la technique dans un domaine technique. Le caract�re technique de la contribution est une des quatre conditions de la brevetabilit�. En outre, pour m�riter un brevet, la contribution technique doit �tre nouvelle, non �vidente et susceptible d'application industrielle. L'utilisation des forces de la nature afin de contr�ler des effets physiques au del� de la repr�sentation num�rique des informations appartient � un domaine technique. Le traitement, la manipulation et les pr�sentations d'informations n'appartiennent pas � un domaine technique, m�me si des appareils techniques sont utilis�s pour les effectuer.</P>
art2ba	fr	b bis) 'domaine technique' d�signe un domaine industriel d'application n�cessitant l'utilisation de forces contr�lables de la nature pour obtenir des r�sultats pr�visibles. 'Technique' signifie 'appartenant � un domaine technique'.
art2bb	fr	b ter) 'industrie', au sens du droit des brevets, signifie 'production automatis�e de biens mat�riels';
art3	fr	supprim�
art3a	fr	Article 3 bis<P>Les �tats membres veillent � ce que le traitement des donn�es ne soit pas consid�r� comme un domaine technique au sens du droit des brevets et � ce que les innovations en mati�re de traitement des donn�es ne constituent pas des inventions au sens du droit des brevets.
art4	fr	Pour �tre brevetable, une invention mise en oeuvre par ordinateur doit �tre susceptible d'application industrielle, �tre nouvelle et impliquer une activit� inventive. Pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur doit apporter une contribution technique. <P>Les �tats membres veillent � ce que le fait qu'une invention mise en oeuvre par ordinateur qui apporte une contribution technique constitue une condition n�cessaire � l'existence d'une activit� inventive. <P>Le caract�re notable de la contribution technique est �valu� en prenant en consid�ration la diff�rence entre l'ensemble des caract�ristiques techniques de la revendication de brevet et l'�tat de la technique, ind�pendamment du fait que ces caract�ristiques soient accompagn�es ou non de caract�ristiques non techniques.<P>3 bis. Pour d�terminer si une invention mise en oeuvre par ordinateur apporte une contribution technique, il y a lieu d'�tablir si elle apporte une connaissance nouvelle sur les relations de causalit� en ce qui concerne l'utilisation des forces contr�lables de la nature et si elle a une application industrielle au sens strict de l'expression, tant sous l'angle de la m�thode que sous celui du r�sultat.
art4a	fr	Article 4 bis <P>Exclusions de la brevetabilit� <P>Une invention mise en oeuvre par ordinateur n'est pas consid�r�e comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, d'un r�seau ou d'un autre appareil programmable. En cons�quence, ne sont pas brevetables les inventions impliquant des programmes d'ordinateurs, qui mettent en oeuvre des m�thodes commerciales, des m�thodes math�matiques ou d'autres m�thodes, si ces inventions ne produisent pas d'effets techniques en dehors des interactions physiques normales entre un programme et l'ordinateur, le r�seau ou un autre appareil programmable sur lequel il est ex�cut�.
art4b	fr	Article 4 ter <P>Les �tats membres veillent � ce que les solutions, mises en oeuvre par ordinateur, � des probl�mes techniques ne soient pas consid�r�es comme des inventions brevetables au seul motif qu'elles am�liorent l'efficacit� de l'utilisation des ressources dans le syst�me de traitement des donn�es.
art5_1	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur ne puisse �tre revendiqu�e qu'en tant que produit, c'est-�-dire en tant qu'appareil programm�, ou en tant que proc�d� technique de production.
art5_1a	fr	1 bis. Les �tats membres veillent � ce que les revendications de brevet reconnues sur des inventions mises en oeuvre par ordinateur couvrent uniquement la contribution technique qui fonde une revendication. Une revendication de brevet sur un programme d'ordinateur, que ce soit sur le seul programme ou sur un programme enregistr� sur un support de donn�es, est irrecevable.
art5_1b	fr	1 ter. Les �tats membres veillent � ce que la production, la manipulation, le traitement, la distribution et la publication de l'information, sous quelque forme que ce soit, ne puisse jamais constituer une contrefa�on de brevet, directe ou indirecte, m�me lorsqu'un dispositif technique est utilis� dans ce but.
art5_1c	fr	1 quater. Les �tats membres veillent � ce que l'utilisation d'un programme d'ordinateur � des fins qui ne rel�vent pas de l'objet du brevet ne puisse constituer une contrefa�on de brevet, directe ou indirecte. <P>1 quinquies. Les �tats membres veillent � ce que, lorsqu'une revendication de brevet mentionne des caract�ristiques impliquant l'utilisation d'un programme d'ordinateur, une mise en oeuvre de r�f�rence, op�rationnelle et bien document�e, de ce programme soit publi�e en tant que partie de la description, sans conditions de licence restrictives.
art6	fr	Les droits conf�r�s par les brevets d'invention d�livr�s dans le cadre de la pr�sente directive ne portent pas atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit�.
art6a	fr	Article 6 bis <P>Utilisation de techniques brevet�es <P>Les �tats membres veillent � ce que, lorsque le recours � une technique brevet�e est n�cessaire � une fin significative, par exemple pour assurer la conversion des conventions utilis�es dans deux syst�mes ou r�seaux informatiques diff�rents, de fa�on � permettre entre eux la communication et l'�change de donn�es, ce recours ne soit pas consid�r� comme une contrefa�on de brevet.
art7	fr	La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes, en particulier les petites et moyennes entreprises et la communaut� des logiciels libres, de m�me que le commerce �lectronique.
art8a	fr	Article 8 bis<P>�valuation de l'impact</P><P>La Commission �value l'impact de la pr�sente directive � la lumi�re du suivi r�alis� conform�ment � l'article 7 et du rapport � r�diger conform�ment � l'article 8 et pr�sente, si n�cessaire, au Parlement europ�en et au Conseil, des propositions en vue de modifier la l�gislation.
art8b	fr	<P>b) si les r�gles r�gissant la dur�e de validit� du brevet et de la d�termination des crit�res de brevetabilit� en ce qui concerne plus pr�cis�ment la nouveaut�, l'activit� inventive et la port�e des revendication sont ad�quates
art8ca	fr	c bis) si des difficult�s sont apparues dans la relation entre la protection par brevet des inventions mises en oeuvre par ordinateur et la protection des programmes d'ordinateur par le droit d'auteur, pr�vue par la directive 91/250/CEE, et si des abus du syst�me de brevet se sont produits en rapport avec les inventions mises en oeuvre par ordinateur;
art8cb	fr	c ter) s'il serait souhaitable, et juridiquement r�alisable, compte tenu des obligations internationales de la Communaut�, d'instaurer une 'p�riode de gr�ce' pour les �l�ments d'une demande de brevet, relative � tout type d'invention, qui auraient �t� divulgu�s avant la date de la demande;
art8cc	fr	c quater) � quels �gards il pourrait �tre n�cessaire de pr�parer une conf�rence diplomatique afin de r�viser la Convention sur la d�livrance de brevets europ�ens, � la lumi�re �galement de l'introduction du brevet communautaire;
art8cd	fr	c quinquies) comment les exigences de la pr�sente directive ont �t� prises en compte dans la pratique de l'Office europ�en des brevets et dans ses lignes directrices en mati�re d'examen.
art8ce	fr	c sexies) si les pouvoirs d�l�gu�s � l'Office europ�en des brevets sont compatibles avec les exigences li�es � l'harmonisation de la l�gislation de l'Union europ�enne, ainsi qu'avec les principes de transparence et de responsabilit�;
art8cf	fr	c septies) l'impact sur la conversion des conventions utilis�es dans deux syst�mes informatiques diff�rents, de fa�on � permettre entre eux la communication et l'�change de donn�es;
art8cg	fr	c octies) si l'option d�crite dans la directive concernant l'utilisation des inventions brevet�es dans le seul objectif d'assurer l'int�rop�rabilit� entre deux syst�mes est ad�quate.
art8_1a	fr	Dans ce rapport, la Commission donnera les raisons pour lesquelles elle estime qu'un amendement � la directive en question est n�cessaire ou pas et, si n�cessaire, indiquera les points auxquels elle a l'intention de proposer un amendement.
art9_1	fr	Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard le ...* et en informent imm�diatement la Commission.<P>_____________</P><P>* dix-huit mois apr�s l'entr�e en vigueur de la pr�sente directive.
rec1	it	La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza nonch� la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto la protezione delle invenzioni mediante i brevetti � un elemento essenziale per il successo del mercato interno. Una protezione efficace, trasparente ed armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri � indispensabile per mantenere e stimolare gli investimenti in questo campo.
rec5	it	Occorre confermare e precisare le norme riguardanti i limiti della brevettabilit�, ai sensi dell'articolo 52 della Convenzione sulla concessione dei brevetti europei. La certezza giuridica che ne deriva dovrebbe contribuire ad un clima pi� aperto agli investimenti e alle innovazioni nel settore dei programmi per elaboratori.
rec6	it	soppresso
rec7	it	Secondo la convenzione sul rilascio dei brevetti europei, firmata a Monaco di Baviera il 5 ottobre 1973, e secondo le legislazioni degli Stati membri in materia di brevetti, i programmi per elaboratore, nonch� le scoperte, le teorie scientifiche, i metodi matematici, le creazioni estetiche, i piani, i principi e i metodi per attivit� intellettuali, giochi o attivit� commerciali e le presentazioni di informazioni sono espressamente non considerati invenzioni e sono quindi esclusi dalla brevettabilit�. Questa eccezione si applica perch� tali materie o attivit� non appartengono ad un settore della tecnologia.
rec7a	it	Con la presente direttiva non si intende modificare la succitata Convenzione, bens� evitare che possano esistere interpretazioni divergenti del relativo testo.
rec7b	it	Il Parlamento europeo ha chiesto a pi� riprese che l'Ufficio europeo dei brevetti rivedesse le sue norme di funzionamento e che fosse soggetto a controllo pubblico nell'esercizio delle sue funzioni. In proposito, sarebbe particolarmente opportuno rimettere in discussione la prassi in base alla quale l'Ufficio europeo dei brevetti percepisce introiti per i brevetti che rilascia, in quanto tale prassi nuoce al carattere pubblico di tale organismo. <P>Nella sua risoluzione del 30 marzo 2000, sulla decisione dell'Ufficio europeo dei brevetti concernente il brevetto n. EP 695 351 rilasciato l'8 dicembre 1999<SUP>1</SUP> il Parlamento europeo ha chiesto la revisione delle norme di funzionamento dell'Ufficio in questione, onde garantire che esso 'sia soggetto a un obbligo di pubblicit� nell'esercizio delle sue funzioni'<P>_________</P><P><SUP>1</SUP> GU C 378 del 29.12.2000, pag. 95.
rec11	it	Per poter essere brevettabili, le invenzioni in generale e le invenzioni attuate per mezzo di elaboratori elettronici in particolare devono essere suscettibili di applicazione industriale, presentare un carattere di novit� e implicare un'attivit� inventiva. Le invenzioni attuate per mezzo di elaboratori elettronici devono costituire un contributo tecnico nuovo allo stato dell'arte per poter essere distinte dai semplici programmi per elaboratori e considerate implicanti un'attivit� inventiva.
rec12	it	Di conseguenza, un'innovazione che non costituisca un contributo tecnico allo stato dell'arte, non � considerata un'invenzione ai sensi della normativa in materia di brevetti.
rec13	it	soppresso
rec13a	it	Tuttavia, la semplice attuazione di un metodo altrimenti non brevettabile su di un apparecchio come un elaboratore non � di per s� sufficiente per concludere che sia presente un contributo tecnico. Di conseguenza, un metodo per attivit� commerciali, di elaborazione di dati o di altro tipo che venga attuato per mezzo di elaboratori elettronici, in cui l'unico contributo allo stato dell'arte non sia tecnico, non pu� costituire un'invenzione brevettabile.
rec13b	it	Se il contributo allo stato dell'arte � relativo unicamente a elementi non brevettabili, non vi pu� essere un'invenzione brevettabile, indipendentemente dal modo in cui tali elementi vengano presentati nelle rivendicazioni. Per esempio, il requisito di contributo tecnico non pu� essere eluso semplicemente specificando mezzi tecnici nelle rivendicazioni di brevetto.
rec13c	it	Inoltre, un algoritmo � intrinsecamente non tecnico e, pertanto, non pu� costituire un'invenzione tecnica. Tuttavia, un metodo che comporti l'utilizzazione di un algoritmo pu� essere brevettabile purch� venga usato per risolvere un problema tecnico. Ciononostante, un brevetto concesso per tale metodo non deve permettere che si monopolizzi lo stesso algoritmo o la sua utilizzazione in contesti non previsti nel brevetto.
rec13d	it	La portata dei diritti esclusivi conferiti da un brevetto � definita nelle relative rivendicazioni. Le invenzioni attuate per mezzo di elaboratori elettronici vanno rivendicate con riferimento a un prodotto, quale un apparecchio programmato, o a un processo svolto da tale apparecchio. Di conseguenza, l'utilizzo di singoli elementi di un programma per elaboratore in contesti che non comportano la realizzazione di un prodotto o un processo regolarmente rivendicato non deve costituire una violazione di brevetto.
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non richiede una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici. La presente direttiva si limita a chiarire l'attuale situazione giuridica per garantire la certezza giuridica, la trasparenza e la chiarezza della legislazione e contrastare la tendenza a sancire la brevettabilit� di metodi non brevettabili, come quelli ovvi e quelli per attivit� commerciali.
rec16	it	La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali sar� rafforzata dall'eliminazione delle differenze attuali nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici e dalla trasparenza della situazione giuridica. Data l'attuale tendenza dell'industria manifatturiera tradizionale di dislocare le proprie attivit� verso economie a basso costo al di fuori dell'Unione europea, l'importanza della tutela della propriet� intellettuale e, in particolare, della tutela del brevetto � di per s� evidente.
rec17	it	La presente direttiva dovrebbe lasciare impregiudicata l'applicazione delle norme in materia di concorrenza, in particolare gli articoli 81 e 82 del trattato.
rec18	it	I diritti conferiti dai brevetti rilasciati per le invenzioni che rientrano nel campo d'applicazione della presente direttiva devono lasciare impregiudicate le facolt� riconosciute ai sensi degli articoli 5 e 6 della direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare nel quadro delle disposizioni per quanto riguarda la decompilazione e l'interoperabilit�. In particolare, gli atti che, a norma degli articoli 5 e 6 di detta direttiva, non sono soggetti all'autorizzazione del titolare del diritto per quel che concerne i diritti d'autore del titolare attinenti ad un programma per elaboratore o in esso contenuti e che, fatto salvo per i suddetti articoli, necessitano di tale autorizzazione, non devono essere soggetti all'autorizzazione del titolare del diritto per quel che concerne i diritti del brevettatore attinenti al programma per elaboratore o in esso contenuti.
rec18a	it	In ogni caso, la legislazione degli Stati membri deve assicurare che i brevetti presentino un carattere di novit� e implichino un'attivit� inventiva per impedire che ci si appropri di invenzioni gi� di dominio pubblico inserendole semplicemente in un programma per elaboratori elettronici.
art2a	it	(a) 'invenzione attuata per mezzo di elaboratori elettronici', un'invenzione ai sensi della Convenzione sulla concessione dei brevetti europei la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta nelle sue applicazioni una o pi� caratteristiche non tecniche che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore, oltre al contributo tecnico che ogni invenzione deve apportare;
art2b	it	b) 'contributo tecnico', altrimenti chiamato 'invenzione', un contributo allo stato dell'arte in un settore tecnico. La natura tecnica del contributo costituisce uno dei quattro requisiti della brevettabilit�. In aggiunta, per poter ricevere un brevetto, il contributo tecnico deve presentare un carattere di novit�, essere non ovvio ed atto ad un'applicazione industriale. L'impiego delle forze della natura per controllare gli effetti fisici al di l� della rappresentazione digitale delle informazioni rientra in un settore tecnico. Il trattamento, la manipolazione e le presentazioni di informazioni non rientrano in un settore tecnico, anche se sono utilizzati apparecchi tecnici per effettuarli.
art2ba	it	b bis) 'settore tecnico', un settore industriale d'applicazione che presuppone l'utilizzo di forze naturali controllabili per ottenere risultati prevedibili. 'Tecnico' significa 'appartenente a un settore della tecnologia'.
art2bb	it	b) ter 'industria', ai sensi della normativa sui brevetti, 'una produzione automatizzata di beni materiali'.
art3	it	soppresso
art3a	it	Articolo 3 bis <P> Gli Stati membri assicurano che l'elaborazione dei dati non venga considerata un settore della tecnologia ai sensi della normativa in materia di brevetti e che le innovazioni nel settore dell'elaborazione di dati non vengano considerate invenzioni ai sensi di tale normativa.
art4	it	Per poter essere brevettabile, un'invenzione attuata per mezzo di elaboratori elettronici deve essere suscettibile di applicazione industriale, presentare un carattere di novit� e implicare un'attivit� inventiva. Per implicare un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici deve apportare un contributo tecnico. <P>Gli Stati membri assicurano che costituisca una condizione necessaria dell'attivit� inventiva il fatto che un'invenzione attuata per mezzo di elaboratori elettronici apporti un contributo tecnico. <P> La portata significativa del contributo tecnico � valutata considerando la differenza tra l'insieme delle caratteristiche tecniche della rivendicazione di brevetto e lo stato dell'arte, a prescindere dal fatto che tali caratteristiche siano accompagnate da caratteristiche non tecniche. <P>3 bis. Per determinare se un'invenzione attuata per mezzo di elaboratore elettronico apporta un contributo tecnico, si valuta se essa costituisce un nuovo insegnamento sulle relazioni di causa-effetto nell'impiego delle forze controllabili della natura e se ha un'applicazione industriale nel senso stretto dell'espressione, in termini sia di metodo che di risultato.
art4a	it	Articolo 4 bis <P>Cause di esclusione dalla brevettabilit� <P>Un'invenzione attuata per mezzo di elaboratori elettronici non � considerata arrecante un contributo tecnico semplicemente perch� implica l'uso di un elaboratore, di una rete o di un altro apparecchio programmabile. Pertanto, non sono brevettabili le invenzioni implicanti programmi per elaboratori che applicano metodi per attivit� commerciali, metodi matematici o di altro tipo e non producono alcun effetto tecnico oltre a quello delle normali interazioni fisiche tra un programma e l'elaboratore, la rete o un altro apparecchio programmabile in cui viene eseguito.
art4b	it	Articolo 4 ter <P>Gli Stati membri assicurano che le soluzioni attuate mediante elaboratore elettronico rispetto a problemi tecnici non siano considerate invenzioni brevettabili solo perch� migliorano l'efficacia nell'impiego delle risorse del sistema di trattamento dei dati.
art5_1	it	Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata solamente come prodotto, ossia come dispositivo programmato o come processo tecnico di produzione.
art5_1a	it	1 bis. Gli Stati membri assicurano che le rivendicazioni di brevetto accolte per le invenzioni attuate per mezzo di elaboratori elettronici comprendano solo il contributo tecnico che motiva la rivendicazione di brevetto. Una rivendicazione di brevetto per un programma per elaboratore, relativa al solo programma o ad un programma esistente su un supporto dati, non � ricevibile.
art5_1b	it	1 ter. Gli Stati membri assicurano che la produzione, la manipolazione, il trattamento, la distribuzione e la pubblicazione di informazioni, in qualsiasi forma, non possano mai costituire una violazione di brevetto, diretta o indiretta, anche se a tale fine sono stati utilizzati dispositivi tecnici.
art5_1c	it	1 quater. Gli Stati membri assicurano che l'uso di un programma per elaboratore per scopi che non riguardano l'oggetto del brevetto non possa costituire una violazione di brevetto diretta o indiretta. <P>1 quinquies. Gli Stati membri assicurano che, qualora una rivendicazione di brevetto menzioni caratteristiche che implicano l'uso di un programma per elaboratore, un'applicazione di riferimento, ben funzionante e ben documentata, di tale programma sia pubblicata come parte della descrizione senza condizioni di licenza restrittive.
art6	it	I diritti conferiti dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lasciano impregiudicate le facolt� riconosciute ai sensi degli articoli 5 e 6 della direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare sulla base delle disposizioni relative alla decompilazione e all'interoperabilit�.
art6a	it	Articolo 6 bis<P>Uso di tecniche brevettate <P>Gli Stati membri assicurano che, in ogni caso in cui l'uso di una tecnica brevettata sia necessario per un fine importante quale ad esempio garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, cos� da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto.
art7	it	La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, in particolare le piccole e medie imprese e la comunit� di produttori di programmi per! elaboratori liberi, nonch� il commercio elettronico.
art8a	it	Articolo 8 bis <P>Valutazione dell'impatto <P>In virt� del monitoraggio effettuato ai sensi dell'articolo 7 e della relazione da elaborare ai sensi dell'articolo 8, la Commissione procede a un riesame della presente direttiva e, ove necessario, presenta proposte di modifica della legislazione al Parlamento europeo e al Consiglio.
art8b	it	(b) l'adeguatezza delle norme che determinano la durata di validit� del brevetto e i criteri di brevettabilit�, in particolare la novit�, l'attivit� inventiva e l'oggetto delle rivendicazioni
art8ca	it	c bis) eventuali difficolt� affrontate riguardo alla relazione tra la tutela brevettuale di invenzioni attuate per mezzo di elaboratori elettronici e la tutela dei programmi per elaboratore a norma dei diritti d'autore, in conformit� della direttiva 91/250/CE nonch� eventuali abusi del sistema di brevetto verificatisi in relazione a invenzioni attuate per mezzo di elaboratori elettronici;
art8cb	it	c ter) l'opportunit� e la possibilit� giuridica, tenendo conto degli obblighi internazionali della Comunit�, di introdurre un 'periodo di dilazione' rispetto agli elementi della domanda di brevetto per qualsiasi tipo di invenzione divulgata anteriormente alla data di domanda;
art8cc	it	c quater) gli aspetti che possono richiedere la preparazione di una conferenza diplomatica per rivedere la Convenzione europea sui brevetti, anche alla luce dell'ingresso del brevetto comunitario;
art8cd	it	c quinquies) il modo in cui i requisiti della presente direttiva sono stati tenuti in considerazione nella prassi dell'Ufficio europeo dei brevetti e nei suoi orientamenti di esame.
art8ce	it	c sexies) la compatibilit� dei poteri delegati all'Ufficio europeo dei brevetti con le esigenze dell'armonizzazione della legislazione dell'Unione europea, nonch� con i principi di trasparenza e di responsabilit�;
art8cf	it	c septies) gli effetti sulla conversione delle convenzioni utilizzate in due diversi sistemi di elaboratori elettronici, cos� da consentire la comunicazione e lo scambio dei dati;
art8cg	it	c octies) l'adeguatezza o meno dell'opzione descritta nella direttiva sull'utilizzo delle invenzioni brevettate per il solo fine di assicurare l'interoperabilit� fra due sistemi;
art8_1a	it	Nella relazione la Commissione espone le ragioni per cui reputa che sia necessario o meno modificare la direttiva in questione e, all'occorrenza, elenca i punti che intende modificare.
art9_1	it	Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro �*. Essi ne informano immediatamente la Commissione. <P>_________________<P>* Diciotto mesi dalla sua entrata in vigore.
rec1	nl	Voor de totstandbrenging van de interne markt is het nodig beperkingen voor het vrije verkeer en concurrentieverstoringen op te heffen, en tegelijkertijd een gunstig klimaat te scheppen voor innovatie en investeringen. In deze context is de bescherming van uitvindingen een essentieel element voor het succes van de interne markt. Doeltreffende, transparante en geharmoniseerde bescherming van in computers ge�mplementeerde uitvindingen in alle lidstaten is van essentieel belang om de investeringen op dit gebied in stand te houden en aan te moedigen.
rec5	nl	Om die reden moeten de rechtsregels betreffende de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen worden geharmoniseerd, om ervoor te zorgen dat de daaruit voortvloeiende rechtszekerheid en het peil van de voorwaarden waaraan voor octrooieerbaarheid moet worden voldaan, de innoverende ondernemingen in staat stellen optimaal profijt te trekken van hun uitvindingsproces en een stimulans zijn voor investeringen en innovatie. De rechtszekerheid wordt tevens gewaarborgd door het feit dat in geval van twijfel over de interpretatie van deze richtlijn nationale rechtscolleges kunnen en nationale rechtscolleges in laatste ressort moeten verzoeken om een uitspraak van het Hof van Justitie van de Europese Gemeenschappen.
rec5a	nl	De voorschriften overeenkomstig artikel 52 van het Verdrag inzake de verlening van Europese octrooien betreffende de grenzen van octrooieerbaarheid dienen te worden bevestigd en nauwkeuriger omschreven. De rechtszekerheid die daaruit voortkomt, moet bijdragen tot een op investering en vernieuwing gericht klimaat in de sector software.
rec6	nl	Schrappen
rec7	nl	Overeenkomstig het Verdrag inzake de verlening van Europese octrooien, ondertekend in M�nchen op 5 oktober 1973, en de octrooiwetten van de lidstaten worden computerprogramma's alsmede ontdekkingen, natuurwetenschappelijke theorie�n, wiskundige methoden, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten. Deze uitzondering is van toepassing omdat de genoemde onderwerpen en werkzaamheden niet tot een gebied van de technologie behoren.
rec7a	nl	Het Europees Parlement heeft er diverse malen op aangedrongen dat het Europees Octrooibureau zijn interne regels herziet en bij de uitoefening van zijn functies aan openbare controle wordt onderworpen. In dit verband zouden er met name vraagtekens geplaatst dienen te worden bij de praktijk van het Europees Octrooibureau om zich te laten betalen voor de octrooien die het afgeeft, aangezien dit schadelijk is voor het openbaar karakter van deze instelling. Het Europees Parlement heeft in zijn resolutie van 30 maart 2000 over het besluit van het Europees Octrooibureau met betrekking tot het op 8 december 1999 verleende octrooi EP 695 351 <SUP>1</SUP> verlangd dat de interne regels van het Europees Octrooibureau zodanig worden herzien dat gewaarborgd is dat het bij de uitoefening van zijn functies openbare verantwoording kan afleggen.<P>__________________</P><P><SUP>1</SUP> PB C 378 van 29.12.2000, blz. 95.
rec7b	nl	Deze richtlijn heeft niet tot doel dit verdrag te wijzigen, maar moet voorkomen dat de bepalingen ervan verschillend worden ge�nterpreteerd.
rec11	nl	Om octrooieerbaar te zijn moeten uitvindingen in het algemeen en in computers ge�mplementeerde uitvindingen in het bijzonder nieuw zijn, op uitvinderswerkzaamheid berusten, en geschikt zijn voor industri�le toepassing. Om op uitvinderswerkzaamheid te berusten moeten in computers ge�mplementeerde uitvindingen bovendien een nieuwe technische bijdrage tot de stand van de techniek leveren, ten einde onderscheid te kunnen aanbrengen met zuivere software.
rec12	nl	Dienovereenkomstig is een innovatie die geen technische bijdrage tot de stand van de techniek levert geen uitvinding in de zin van de octrooiwetgeving.
rec13	nl	Schrappen
rec13a	nl	Louter de implementatie van een anders niet octrooieerbare methode op een apparaat zoals een computer, garandeert niettemin als zodanig niet afdoende dat er van een technische bijdrage sprake is. Bijgevolg kan een in computers ge�mplementeerde bedrijfs-, gegevensverwerkings- of andere methode waarbij de enige bijdrage aan de stand van de techniek van niet-technische aard is, geen octrooieerbare uitvinding vormen.
rec13b	nl	Indien de bijdrage aan de stand van de techniek slechts verband houdt met niet-octrooieerbare onderwerpen kan er geen sprake zijn van een octrooieerbare uitvinding, ongeacht hoe het onderwerp in de aanvraag gepresenteerd wordt. Zo kan bijvoorbeeld het vereiste van een technische bijdrage niet omzeild worden door in de octrooiaanvraag louter de technische middelen te specificeren.
rec13c	nl	Bovendien is een algoritme inherent van niet-technische aard en kan derhalve geen technische uitvinding vormen. Desalniettemin zou een methode waarbij een algoritme gebruikt wordt, octrooieerbaar kunnen zijn op voorwaarde dat de methode gebruikt wordt om een technisch probleem op te lossen. Elk octrooi dat verleend wordt voor een dergelijke methode zou echter niet de algoritme zelf of het gebruik ervan in een context die niet in het octrooi voorzien is, monopoliseren.
rec13d	nl	De omvang van de exclusieve rechten die een octrooi verleent, wordt bepaald door de octrooiconclusies. Voor in computers ge�mplementeerde uitvindingen moet het octrooi worden geclaimd hetzij voor een product, zoals een geprogrammeerd apparaat, hetzij voor een werkwijze die door een dergelijk apparaat wordt uitgevoerd. Bijgevolg vormt het gebruik van individuele software-elementen in een omgeving die niet de realisatie inhoudt van een op geldige wijze geclaimd product of werkwijze, geen inbreuk op een octrooi.
rec14	nl	De rechtsbescherming van in computers ge�mplementeerde uitvindingen vereist niet dat wordt voorzien in een afzonderlijk rechtsinstrument ter vervanging van de regels van het nationale octrooirecht. De regels van het nationale octrooirecht blijven de essenti�le basis voor de rechtsbescherming van in computers ge�mplementeerde uitvindingen. Deze richtlijn verduidelijkt slechts de huidige juridische situatie, ter waarborging van de rechtszekerheid, doorzichtigheid en duidelijkheid in de wetgeving en als tegenwicht tegen de ontwikkeling naar octrooieerbaarheid van niet-octrooieerbare methoden, zoals alledaagse procedures en bedrijfsmethoden.
rec16	nl	De concurrentiepositie van het Europese bedrijfsleven ten opzichte van zijn voornaamste handelspartners zal worden verbeterd indien de bestaande verschillen in de rechtsbescherming van in computers ge�mplementeerde uitvindingen worden opgeheven en de rechtssituatie transparant is. Gelet op de huidige tendens voor de traditionele verwerkende industrie om haar werkzaamheden te verplaatsen naar lageloneneconomie�n buiten de Europese Unie, is het belang van de bescherming van intellectuele eigendom en met name van octrooien vanzelfsprekend.
rec17	nl	Deze richtlijn mag geen afbreuk doen aan de toepassing van de mededingingsregels, inzonderheid de artikelen 81 en 82 van het Verdrag.
rec18	nl	De rechten die zijn toegekend middels octrooien die binnen de werkingssfeer van deze richtlijn voor uitvindingen zijn verleend, laten handelingen onverlet die zijn toegestaan op grond van de artikelen 5 en 6 van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit. In het bijzonder is voor handelingen waarvoor op grond van de artikelen 5 en 6 van die richtlijn geen toestemming van de rechthebbende is vereist in verband met diens auteursrechten in of ten aanzien van een computerprogramma en waarvoor dergelijke toestemming zonder de artikelen 5 en 6 van die richtlijn wel zou zijn vereist, geen toestemming van de rechthebbende vereist in verband met de octrooirechten die deze in of ten aanzien van het computerprogramma bezit.
rec18a	nl	Hoe dan ook moet in de wetgeving van de lidstaten worden verzekerd dat de octrooien noviteiten bevatten en op een uitvinderswerkzaamheid berusten, om te voorkomen dat uitvindingen die reeds publiek bezit zijn, worden toege�igend, enkel en alleen omdat ze deel uitmaken van een computerprogramma.
art2a	nl	a) 'in computers ge�mplementeerde uitvinding'
art2b	nl	b) 'technische bijdrage' ook wel 'uitvinding' genoemd
art2ba	nl	b bis) 'technisch gebied'
art2bb	nl	b ter) 'industrie'
art3	nl	Schrappen
art3a	nl	Artikel 3 bis<P>De lidstaten zorgen ervoor dat gegevensverwerking niet wordt beschouwd als een gebied van de technologie in octrooirechtelijke zin en dat innovaties op het gebied van gegevensverwerking geen uitvindingen zijn in octrooirechtelijke zin.
art4	nl	Om octrooieerbaar te zijn moet een in computers ge�mplementeerde uitvinding geschikt zijn voor industri�le toepassing, nieuw zijn en op uitvinderswerkzaamheid te berusten. Om op uitvinderswerkzaamheid te berusten moet een in computers ge�mplementeerde uitvinding een technische bijdrage leveren. <P>De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding die een technische bijdrage levert, een noodzakelijke voorwaarde voor het bestaan van uitvinderswerkzaamheid is. <P>Of de technische bijdrage significante omvang heeft, wordt beoordeeld door het bepalen van het verschil tussen de technische kenmerken in de omvang van de octrooiconclusie in hun geheel beschouwd, en de stand van de techniek, ongeacht of deze kenmerken gepaard gaan met niet-technische kenmerken.<P>3 bis. Of een in computers ge�mplementeerde uitvinding een technische bijdrage levert tot de stand van de techniek, wordt bepaald aan de hand van de vraag of zij nieuw inzicht verschaft in het oorzakelijk verband bij het gebruik van beheersbare natuurkrachten en of zij industrieel toepasbaar in de enge zin, zowel wat de methode als wat het resultaat betreft
art4a	nl	Artikel 4 bis<P>Uitzonderingen op de octrooieerbaarheid:</P><P>Een in computers ge�mplementeerde uitvinding wordt niet geacht een technische bijdrage te leveren louter op grond van het feit dat daarbij gebruik wordt gemaakt van een computer, een netwerk of andere programmeerbare apparatuur. Bijgevolg zijn uitvindingen waarbij gebruik wordt gemaakt van computerprogramma's met toepassing van bedrijfsmethoden, mathematische of andere methoden en die geen technische resultaten produceren buiten de normale fysieke interactie tussen een programma en de computer, een netwerk of andere programmeerbare apparatuur waarop dit ten uitvoer wordt gebracht, niet octrooieerbaar.
art4b	nl	Artikel 4 ter<P>De lidstaten zorgen ervoor dat in computers ge�mplementeerde oplossingen voor technische problemen niet worden beschouwd als octrooieerbare uitvindingen enkel en alleen omdat zij de doeltreffendheid bij het gebruik van middelen binnen het gegevensverwerkingssysteem verhogen.
art5_1	nl	De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding alleen kan worden geclaimd als product, dat wil zeggen als een geprogrammeerd instrument of als een technisch productieproc�d�.
art5_1a	nl	1 bis. De lidstaten zorgen ervoor dat voor in computers ge�mplementeerde uitvindingen verleende octrooiaanspraken alleen betrekking hebben op de technische bijdrage die de basis van de octrooiaanvraag vormt. Een octrooiaanspraak op een computerprogramma, het weze een computerprogramma als zodanig dan wel een op een drager opgeslagen computerprogramma, is niet toelaatbaar.
art5_1c	nl	1 quater. De lidstaten zorgen ervoor dat het gebruik van een computerprogramma voor doeleinden die niet onder het toepassingsgebied van het octrooi vallen, geen directe of indirecte inbreuk op het octrooi kan zijn. <P>1 quinquies. De lidstaten zorgen ervoor dat als in een octrooiconclusie kenmerken worden genoemd die het gebruik van een computerprogramma impliceren, een goed werkende en goed gedocumenteerde referentie-implementering van een dergelijk programma wordt gepubliceerd als onderdeel van de beschrijving, zonder beperkende licentievoorwaarden.
art6	nl	De rechten die zijn toegekend met octrooien die binnen de werkingssfeer van deze richtlijn voor uitvindingen zijn verleend, laten handelingen onverlet die zijn toegestaan op grond van de artikelen 5 en 6 van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit.
art6a	nl	Artikel 6 bis<P>Gebruik van een geoctrooieerde techniek</P><P>De lidstaten zorgen ervoor dat gebruik van een geoctrooieerde techniek met een belangrijk doel zoals de omzetting van de conventies die in twee verschillende computersystemen of netwerken worden gebruikt om communicatie en gegevensuitwisseling tussen beide mogelijk te maken, niet wordt beschouwd als een inbreuk op het octrooi.
art7	nl	De Commissie volgt welke invloed in computers ge�mplementeerde uitvindingen hebben op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, in het bijzonder kleine en middelgrote ondernemingen en de opensourcegemeenschap, en de elektronische handel.
art8a	nl	Artikel 8 bis<P>Evaluatie van de gevolgen</P><P>In het kader van het toezicht overeenkomstig artikel 7 en het verslag dat moet worden opgesteld overeenkomstig artikel 8 evalueert de Commissie de gevolgen van deze richtlijn en dient zij, voorzover noodzakelijk, bij het Europees Parlement en de Raad voorstellen in tot wijziging van de wetgeving.
art8b	nl	b) de vraag of de regels betreffende de octrooitermijn en het bepalen van de octrooieerbaarheidseisen, meer bepaald nieuwheid, uitvinderswerkzaamheid en de passende omvang van de conclusies, adequaat zijn; en
art8ca	nl	(c bis) de vraag of de in deze richtlijn genomen optie met betrekking tot het gebruik van een geoctrooieerde uitvinding met als enig doel interoperabiliteit tussen twee systemen mogelijk te maken, adequaat is;
art8cb	nl	c ter) de vraag of het wenselijk en juridisch mogelijk zou zijn om, gelet op de internationale verplichtingen van de Gemeenschap, een 'gratieperiode' voor elementen van een octrooiaanvraag voor elk type uitvinding die voor de datum van aanvraag worden onthuld, in te voeren.
art8cc	nl	c quater) de eventuele noodzaak een diplomatieke conferentie voor te bereiden ter herziening van het Europees Octrooiverdrag, mede in het licht van de totstandkoming van het Gemeenschapsoctrooi;
art8cd	nl	c quinquies) de manier waarop met de voorschriften van deze richtlijn rekening is gehouden in de praktijk van het Europees Octrooibureau en zijn onderzoeksrichtsnoeren.
art8ce	nl	c sexies) de vraag of de aan het Europees Octrooibureau verleende bevoegdheden stroken met de vereisten voor de harmonisering van de EU-wetgeving, alsmede met de beginselen van transparantie en verantwoordingsplicht.
art8cf	nl	c septies) de gevolgen voor de omzetting van de conventies in twee verschillende computersystemen om communicatie en gegevensuitwisseling mogelijk te maken,
art8_1a	nl	In dit verslag motiveert de Commissie waarom ze van oordeel is dat er al dan niet een wijziging van onderhavige richtlijn nodig is en somt ze desgevallend de punten op waarop ze zich voorneemt een voorstel tot wijziging te formuleren.
art9_1	nl	De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om uiterlijk op ...* aan deze richtlijn te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis.<P>* 18 maanden na de inwerkingtreding van de richtlijn
rec1	pt	A realiza��o do mercado interno implica a elimina��o das restri��es � liberdade de circula��o e �s distor��es da concorr�ncia, criando um ambiente que seja favor�vel � inova��o e ao investimento. Neste contexto, a protec��o dos inventos por meio de patentes � um elemento essencial para o �xito do mercado interno. A protec��o eficaz, transparente e harmonizada dos inventos que implicam programas de computador, em todos os Estados-Membros, � essencial para manter e incentivar o investimento neste dom�nio;
rec5	pt	Consequentemente, as normas jur�dicas que regem a patenteabilidade dos inventos que implicam programas de computador devem ser harmonizadas por forma a assegurar que a certeza jur�dica da� resultante e o n�vel dos requisitos exigidos para a patenteabilidade permitam �s empresas inovadoras tirarem o m�ximo partido do seu processo inventivo e dar um incentivo ao investimento e � inova��o. A certeza jur�dica ficar� tamb�m garantida pelo facto de, em caso de d�vida quanto � interpreta��o da Directiva 91/250/CEE, os tribunais nacionais e as inst�ncias nacionais de �ltimo recurso poderem tentar obter um ac�rd�o do Tribunal de Justi�a das Comunidades Europeias.
rec5a	pt	As normas visadas no artigo 52� da Conven��o sobre a Concess�o de Patentes Europeias relativamente aos limites da patenteabilidade devem ser refor�adas e especificadas. A seguran�a jur�dica da� decorrente deveria contribuir para um clima prop�cio ao investimento e � inova��o no dom�nio do software.
rec6	pt	Suprimido
rec7	pt	Segundo a Conven��o sobre a Concess�o de Patentes Europeias, assinada em Munique em 5 de Outubro de 1973, e a legisla��o em mat�ria de patentes dos Estados-Membros, os programas de computador, em conjunto com as descobertas, teorias cient�ficas, m�todos matem�ticos, cria��es est�ticas, esquemas, regras e m�todos para execu��o de actividades intelectuais, jogos ou actividades comerciais, assim como exposi��es de informa��o, s�o expressamente n�o considerados inventos, sendo, por isso, exclu�dos da patenteabilidade. Esta excep��o aplica-se porque esses temas e actividades n�o pertencem a um dom�nio da tecnologia;
rec7a	pt	Com a presente directiva n�o se pretende alterar a dita conven��o, mas evitar que possam existir interpreta��es divergentes do seu texto.
rec7b	pt	O Parlamento j� solicitou, em diversas ocasi�es, que o Instituto Europeu de Patentes reveja as suas regras de funcionamento e que este organismo seja controlado publicamente no exerc�cio das suas fun��es. Nesta perspectiva, seria particularmente oportuno reconsiderar a pr�tica segundo a qual o Instituto Europeu de Patentes recebe pagamento pelas patentes que emite, na medida em que esta pr�tica prejudica o car�cter p�blico da institui��o. Na sua resolu��o de 30 de Mar�o de 2000 sobre a decis�o do Instituto Europeu de Patentes relativa � patente n� EP 695 351, concedida em 8 de Dezembro de 1999<SUP>1</SUP>, o Parlamento solicitou a revis�o das regras de funcionamento do Instituto, por forma a garantir que este organismo possa prestar contas publicamente no exerc�cio das suas fun��es. <P>____________<P><SUP>1</SUP>�����JO C 378 de 29.12.2000, p. 95.
rec11	pt	Para serem patente�veis, os inventos, em geral, e os inventos que implicam programas de computador, em particular, dever�o constituir uma novidade, implicar uma actividade inventiva e ser suscept�veis de utiliza��o industrial. Para implicarem uma actividade inventiva, os inventos que implicam programas de computador devem dar, adicionalmente, um novo contributo t�cnico para o progresso tecnol�gico, a fim de os diferenciar do mero software;
rec12	pt	Deste modo, uma inova��o que n�o d� um contributo t�cnico para o progresso tecnol�gico n�o constitui um invento na acep��o da lei das patentes.
rec13	pt	Suprimido
rec13a	pt	Por�m, a mera aplica��o de um m�todo que n�o seja patente�vel, por outros motivos, num aparelho como, por exemplo, um computador, n�o basta, por si s�, para comprovar que se est� perante um contributo t�cnico. Por conseguinte, um m�todo destinado ao exerc�cio de actividades econ�micas e ao tratamento de dados ou qualquer outro m�todo que implique a utiliza��o de programas de computador, no qual o �nico contributo para o progresso tecnol�gico n�o se revista de car�cter t�cnico, n�o pode configurar um invento patente�vel;
rec13b	pt	Caso o contributo para o progresso tecnol�gico se relacione apenas com mat�ria n�o patente�vel, n�o se poder� considerar o invento como patente�vel, independentemente da forma como essa mat�ria seja apresentada na reivindica��o de patente. Por exemplo, a exig�ncia do contributo t�cnico n�o pode ser contornada atrav�s da mera especifica��o dos meios t�cnicos na reivindica��o de patente.
rec13c	pt	Para al�m disso, um algoritmo, em si mesmo, n�o se reveste de �ndole t�cnica, pelo que n�o pode constituir um invento de car�cter t�cnico. N�o obstante, um m�todo que envolva o recurso a um algoritmo poder� ser patente�vel, na condi��o de esse m�todo ser utilizado para solucionar um problema t�cnico. Uma patente concedida a um m�todo desse tipo n�o deve, todavia, permitir a monopoliza��o do pr�prio algoritmo ou do respectivo uso em contextos n�o previstos na patente.
rec13d	pt	, seja um processo levado a cabo por esse equipamento. Em consequ�ncia, nos casos em que sejam usados elementos individuais de um programa em contextos que n�o envolvam a elabora��o de qualquer produto ou processo reivindicado de forma v�lida, tal utiliza��o n�o deve considerar-se como uma infrac��o � patente.
rec14	pt	Para a protec��o jur�dica dos inventos que implicam programas de computador n�o � necess�rio criar um organismo de aplica��o das normas em vigor da legisla��o nacional em mat�ria de patentes. As regras da legisla��o nacional em mat�ria de patentes permanecer�o a base essencial para a protec��o jur�dica dos inventos que implicam programas de computador. A presente directiva limita-se a clarificar o quadro jur�dico actualmente existente, com vista a assegurar a certeza jur�dica, a transpar�ncia e a clareza da legisla��o, bem como a evitar qualquer tend�ncia para a patenteabilidade de m�todos n�o patente�veis, como sejam os procedimentos triviais e os m�todos de exerc�cio de actividades econ�micas;
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhorar�, se forem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos que implicam programas de computador e se a situa��o jur�dica for transparente. Dada a actual tend�ncia manifestada pelas tradicionais ind�strias produtoras no sentido de deslocarem as suas actividades para economias com baixos custos de produ��o fora da Uni�o Europeia, a import�ncia da protec��o da propriedade intelectual e, em particular, da protec��o por patente � evidente.
rec17	pt	A presente directiva dever� ser aplicada sem preju�zo das regras de concorr�ncia, em particular dos artigos 81� e 82� do Tratado
rec18	pt	Os direitos conferidos pelas patentes concedidas a inventos abrangidos pela presente directiva n�o devem afectar os actos permitidos ao abrigo dos artigos 5� e 6� da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, e, em particular, das suas disposi��es espec�ficas sobre a descompila��o e a interoperabilidade. Em especial, os actos que, nos termos dos artigos 5� e 6� da referida directiva, n�o carecem da autoriza��o do titular de direitos relativamente aos seus direitos de autor num programa inform�tico ou referentes a um programa inform�tico, e que, � excep��o do previsto nos artigos 5� ou 6� da dita directiva, necessitariam dessa autoriza��o, n�o carecem de autoriza��o do titular de direitos relativamente aos seus direitos de patente no programa inform�tico ou referentes ao programa inform�tico.
rec18a	pt	Em todo o caso, a legisla��o dos Estados-Membros deve garantir que as patentes contenham uma novidade e impliquem uma actividade inventiva, a fim de impedir que inventos que j� sejam do dom�nio p�blico sejam apropriados pelo simples facto de serem incorporados num programa de computador.
art2a	pt	a) 'invento que implica programas de computador' significa qualquer invento na acep��o da Conven��o sobre a Patente Europeia cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha nas suas implementa��es uma ou mais caracter�sticas n�o t�cnicas, que sejam realizadas, no todo ou em parte, por um ou mais programas de computador, para al�m das caracter�sticas t�cnicas que todos os inventos devem comportar;
art2b	pt	b) 'contributo t�cnico', igualmente designado 'inven��o', significa um contributo para o progresso tecnol�gico num dom�nio t�cnico. O car�cter t�cnico do contributo � um dos quatro requisitos da patenteabilidade. Para al�m dele, o contributo t�cnico ter� de ser novo, n�o �bvio e suscept�vel de aplica��o industrial, para poder merecer a atribui��o de uma patente. A utiliza��o das for�as da natureza para controlar efeitos f�sicos para l� da representa��o digital das informa��es pertence a um dom�nio t�cnico. O tratamento, a manipula��o e a apresenta��o da informa��o n�o pertencem a um dom�nio t�cnico, mesmo que, para os realizar, sejam utilizados aparelhos t�cnicos.
art2ba	pt	b bis) 'dom�nio t�cnico' significa um dom�nio de aplica��o industrial que exige a utiliza��o das for�as control�veis da natureza para obter resultados previs�veis. 'T�cnico' significa 'que pertence a um dom�nio t�cnico'.
art2bb	pt	b ter) 'ind�stria', na acep��o do direito das patentes, significa 'produ��o automatizada de bens materiais';
art3	pt	Suprimido
art3a	pt	Artigo 3� bis <P>Os Estados-Membros assegurar�o que o processamento de dados n�o seja considerado um dom�nio t�cnico na acep��o do direito das patentes e que as inova��es no dom�nio do processamento de dados n�o sejam inventos na acep��o do direito das patentes.
art4	pt	Para ser patente�vel, um invento que implique programas de computador tem de ser suscept�vel de aplica��o industrial, ser novo e implicar uma actividade inventiva. Para implicar uma actividade inventiva, um invento que implique programas de computador tem de constituir um contributo de car�cter t�cnico. <P>Os Estados-Membros garantir�o que o facto de um invento que implica programas de computador prestar um contributo t�cnico seja condi��o necess�ria da actividade inventiva. <P>O alcance significativo do contributo t�cnico ser� avaliado considerando a diferen�a entre o progresso tecnol�gico e todas as caracter�sticas t�cnicas da reivindica��o de patente, independentemente do facto de estarem acompanhadas de caracter�sticas n�o t�cnicas. <P>3 bis. Para determinar se um invento que implica programas de computador presta um contributo t�cnico, h� que verificar se representa um novo conhecimento sobre as rela��es de causalidade no referente � utiliza��o das for�as control�veis da natureza e se tem uma aplica��o industrial stricto sensu, quer em termos de m�todo, quer em termos de resultado.
art4a	pt	Artigo 4� bis <P>Causas de exclus�o da patenteabilidade <P>Um invento que implique programas de computador n�o ser� considerado como contributo t�cnico meramente por envolver a utiliza��o de um computador, de uma rede, ou de qualquer outro aparelho suscept�vel de programa��o. Da mesma forma, n�o ser�o patente�veis os inventos que impliquem programas de computador destinados � execu��o de m�todos de natureza comercial, matem�tica ou outra, e que n�o produzam quaisquer efeitos t�cnicos para al�m das normais interac��es f�sicas entre o programa e a m�quina, a rede ou qualquer outro aparelho program�vel em que possa correr.
art4b	pt	Artigo 4� ter <P>Os Estados-Membros garantir�o que as solu��es implementadas por computador dadas a problemas t�cnicos n�o sejam consideradas como inventos patente�veis simplesmente porque melhoram a efic�cia na utiliza��o de recursos no interior de um sistema de tratamento de dados.
art5_1	pt	Os Estados-Membros garantir�o que um invento que implica programas de computador possa ser reivindicado apenas como um produto, ou seja, como aparelho programado, ou como processo t�cnico de produ��o.
art5_1a	pt	1 bis. Os Estados-Membros garantir�o que as reivindica��es de patente reconhecidas, no que se refere a inventos que impliquem programas de computador, abranjam apenas o contributo t�cnico que fundamenta a reivindica��o de patente. Uma reivindica��o de patente para um programa de computador, quer se trate apenas do programa ou de um programa existente numa portadora, n�o � admiss�vel.
art5_1b	pt	1 ter. Os Estados-Membros certificar-se-�o de que a produ��o, o manuseamento, o processamento, a distribui��o e a publica��o de informa��o, sob qualquer forma, jamais poder� constituir, directa ou indirectamente, uma viola��o de patente, mesmo nos casos em que, para esse fim, seja usado um dispositivo de car�cter t�cnico.
art5_1c	pt	1 quater. Os Estados-Membros garantir�o que a utiliza��o de um programa inform�tico para fins que excedam o �mbito da patente n�o possam constituir uma viola��o directa ou indirecta de patente. <P>1 quinquies. Os Estados-Membros garantir�o que, sempre que uma reivindica��o de patente mencione caracter�sticas que impliquem a utiliza��o de um programa inform�tico, seja publicada uma aplica��o de refer�ncia bem documentada e que funcione como parte de uma descri��o sem condi��es de licen�a restritivas.
art6	pt	Os direitos conferidos pelas patentes concedidas a inventos abrangidos pelo �mbito de aplica��o da presente directiva n�o afectam os actos permitidos ao abrigo dos artigos 5� e 6� da Directiva 91/250/CEE, relativa � protec��o jur�dica dos programas de computador, e, em particular, das suas disposi��es sobre a descompila��o e a interoperabilidade.
art6a	pt	Artigo 6� bis <P>Uso de t�cnicas patenteadas <P>Os Estados-Membros garantir�o que, sempre que seja necess�ria a utiliza��o de uma t�cnica patenteada com um fim importante, como seja assegurar a convers�o das conven��es utilizadas em dois sistemas ou redes inform�ticos diferentes, por forma a permitir a comunica��o e a troca de dados entre eles, tal utiliza��o n�o seja considerada uma viola��o de patente.
art7	pt	A Comiss�o acompanhar� o impacto, na inova��o e na concorr�ncia, dos inventos que implicam programas de computador, tanto na Europa como a n�vel internacional, bem como nas empresas europeias, sobretudo nas pequenas e m�dias empresas e na comunidade de utilizadores de software de fonte aberta, e no com�rcio electr�nico.
art8a	pt	Artigo 8� bis<P>An�lise de impacto <P>A Comiss�o proceder� � an�lise do impacto da presente directiva em fun��o das ac��es de fiscaliza��o levadas a cabo nos termos do artigo 7�, bem como do relat�rio que ser� elaborado ao abrigo do disposto no artigo 8� e, se necess�rio, apresentar� ao Parlamento Europeu e ao Conselho propostas de altera��o da legisla��o em vigor.
art8b	pt	b) se as normas que regem o prazo de validade da patente e a determina��o dos requisitos de patenteabilidade e, mais especificamente, de novidade, de actividade inventiva e do �mbito apropriado das reivindica��es, s�o adequadas;
art8ca	pt	c bis) as dificuldades encontradas no tocante � rela��o entre a protec��o por patente dos inventos que implicam programas de computador e a protec��o jur�dica por direitos de autor dos referidos programas, tal como consta da Directiva 91/250/CE, de 14 de Maio de 1991, bem como sobre os abusos ocorridos em rela��o ao sistema de protec��o por patente dos inventos que implicam programas de computador;
art8cb	pt	c ter) a quest�o de saber se seria desej�vel, ou legalmente poss�vel, tendo em conta as obriga��es internacionais da Comunidade, introduzir um per�odo 'de gra�a' relativamente aos elementos de uma reivindica��o de patente para qualquer tipo de invento dado a conhecer antes da data da reivindica��o;
art8cc	pt	c quater) os aspectos que ser� necess�rio debater no quadro de uma confer�ncia para a revis�o da Conven��o sobre a Patente Europeia, tamb�m na perspectiva da institui��o da patente comunit�ria;
art8cd	pt	c quinquies) o modo como as disposi��es da presente directiva foram tidas em considera��o na pr�tica do Instituto Europeu de Patentes e nas respectivas normas de an�lise.
art8ce	pt	c sexies) se as compet�ncias delegadas no Instituto Europeu de Patentes s�o compat�veis com as exig�ncias ligadas � harmoniza��o da legisla��o da Uni�o Europeia, assim como com os princ�pios da transpar�ncia e da responsabilidade;
art8cf	pt	c bis) o impacto na convers�o das conven��es utilizadas em dois sistemas inform�ticos diferentes, por forma a permitir a comunica��o e o interc�mbio de dados entre eles;
art8cg	pt	c octies) se a op��o prevista na directiva relativamente � utiliza��o de um invento patenteado com o �nico objectivo de assegurar a interoperabilidade entre dois sistemas � adequada.
art8_1a	pt	Nesse relat�rio, a Comiss�o justificar� por que raz�o considera ser ou n�o necess�ria uma altera��o � directiva em quest�o e, se necess�rio, indicar� os pontos relativamente aos quais tenciona apresentar uma altera��o.
art9_1	pt	Os Estados-Membros por�o em vigor as disposi��es <I>legais</I>, regulamentares e administrativas necess�rias para darem cumprimento � presente directiva at� ...<SUP>*</SUP>. Desse facto informar�o imediatamente a Comiss�o. <P>____________<P><SUP>*</SUP>�����Dezoito meses ap�s a respectiva entrada em vigor.
rec1	sv	F�r att genomf�ra den inre marknaden m�ste man dels undanr�ja hinder f�r fri r�rlighet och motverka snedvridna konkurrensf�rh�llanden, dels skapa f�rh�llanden som gynnar innovation och investeringar. I detta sammanhang �r skydd f�r uppfinningar genom patent en v�sentlig f�ruts�ttning f�r att den inre marknaden skall bli en framg�ng. Ett verksamt, �ppet och harmoniserat skydd av datorrelaterade uppfinningar i alla medlemsstater �r viktigt f�r att trygga och stimulera investeringsviljan.
rec5	sv	D�rf�r b�r de lagregler som reglerar patenterbarhet f�r datorrelaterade uppfinningar harmoniseras, s� att man ser till att den �tf�ljande r�ttss�kerheten och niv�n p� de krav som uppst�lls f�r patenterbarhet ger de nyskapande f�retagen m�jlighet att utnyttja sin uppfinningsprocess optimalt samt stimulerar investeringar och innovation. R�ttss�kerheten kommer ocks� att garanteras genom att nationella domstolar f�r och nationella domstolar i sista instans m�ste be Europeiska gemenskapernas domstol om ett f�rhandsavg�rande d� det �r oklart hur direktivet skall tolkas.
rec5a	sv	De regler som avses i artikel 52 i europeiska patentkonventionen, om gr�nser f�r vad som �r patenterbart, b�r bekr�ftas och preciseras. Den r�ttss�kerhet som d�rmed skulle uppst� skulle bidra till ett gott investerings- och innovationsklimat inom mjukvarusektorn.
rec6	sv	utg�r
rec7	sv	Enligt Konventionen om meddelande av europeiska patent (EPC), som undertecknades i M�nchen den 5 oktober 1973, och patentlagarna i medlemsstaterna kan datorprogram liksom uppt�ckter, vetenskapliga teorier, matematiska metoder, konstn�rliga skapelser, planer, regler eller metoder f�r intellektuell verksamhet, f�r spel eller f�r aff�rsverksamhet samt framl�gganden av information uttryckligen inte betraktas som uppfinningar och �r d�rf�r undantagna fr�n det patenterbara omr�det. Dessa undantag �r till�mpliga eftersom de n�mnda f�rem�len och verksamheterna inte tillh�r n�got teknikomr�de.
rec7a	sv	Syftet med detta direktiv �r inte att �ndra ovann�mnda konvention utan att f�rhindra skiljaktiga tolkningar av dess best�mmelser.
rec7b	sv	Europaparlamentet har vid flera tillf�llen beg�rt att Europeiska patentverket skall se �ver sina verksamhetsregler och att verket skall kontrolleras offentligt i sitt arbete. Den praxis som inneb�r att Europeiska patentverket avl�nar sig sj�lv f�r de patent det utf�rdar b�r s�rskilt �ndras, eftersom denna praxis skadar institutionens offentliga karakt�r. I Europaparlamentets resolution av den 30 mars 2000 om Europeiska patentverkets beslut om utf�rdande av patent nr EP 695 351 den 8 december 1999<SUP>1</SUP>, beg�rde parlamentet att Europeiska patentverkets verksamhetsbest�mmelser skulle ses �ver, s� att verket kan st�llas till svars offentligt d� den ut�var sitt uppdrag. <P><SUP>1</SUP> EGT C 378, 29.12.2000, s. 95.
rec11	sv	F�r att kunna patenteras m�ste uppfinningar i allm�nhet och datorrelaterade uppfinningar i synnerhet vara nya, ha uppfinningsh�jd och kunna tillgodog�ras industriellt. F�r att de skall tillerk�nnas uppfinningsh�jd m�ste datorrelaterade uppfinningar dessutom utg�ra ett nytt tekniskt bidrag till teknikens st�ndpunkt f�r att s�rskilja dem fr�n ren mjukvara.
rec12	sv	En innovation som inte utg�r ett tekniskt bidrag till teknikens st�ndpunkt �r inte n�gon uppfinning i patentr�ttsligt avseende.
rec13	sv	utg�r
rec13a	sv	Om en metod som annars inte g�r att patentera till�mpas med hj�lp av en anordning, exempelvis en dator, �r det inte i sig tillr�ckligt f�r att konstatera att det �r fr�ga om ett tekniskt bidrag. En datorrelaterad aff�rs- eller databearbetningsmetod, eller en annan metod d�r det enda bidraget till teknikens st�ndpunkt inte �r tekniskt, kan d�rf�r inte betraktas som en patenterbara uppfinning.
rec13b	sv	Om bidraget till teknikens st�ndpunkt enbart r�r n�got som inte �r patenterbart kan uppfinningen inte patenteras, oberoende av hur fr�gan l�ggs fram i ans�kan. Till exempel kan kravet p� tekniskt bidrag inte kringg�s enbart genom att tekniska medel specificeras i patentkravet.
rec13c	sv	En algoritm �r dessutom inte till sin natur teknisk och kan d�rf�r inte utg�ra en teknisk uppfinning. En metod som omfattar anv�ndning av en algoritm kan trots detta vara patenterbar om metoden anv�nds f�r att l�sa ett tekniskt problem. Alla patent som beviljas f�r s�dana metoder inneb�r emellertid inte ensamr�tt till sj�lva algoritmen eller dess anv�ndning i sammanhang som inte fastst�lls i patentet.
rec13d	sv	Omfattningen av den ensamr�tt som ett patent inneb�r anges i patentkraven. Ans�kningar om datorrelaterade uppfinningar m�ste g�lla antingen en produkt, s�som en programmerad anordning, eller en process utf�rd av en s�dan anordning. Av detta f�ljer att anv�ndning av enskilda detaljer i mjukvara i sammanhang som inte inbegriper framst�llning av en patentskyddad produkt eller process inte kommer att betraktas som patentintr�ng.
rec14	sv	F�r att inf�ra patentskydd f�r datorrelaterade uppfinningar kr�vs det inte n�got nytt regelverk som ers�ttning f�r de nationella patentlagarna. De nationella patentlagarna bibeh�lls som huvudsaklig grund f�r patentskydd n�r det g�ller datorrelaterade uppfinningar. Direktivet klarg�r helt enkelt den nu g�llande r�ttsliga st�llningen i syfte att garantera r�ttss�kerhet, insyn och r�ttslig klarhet och f�rhindra en utveckling som inneb�r att metoder som inte �r patenterbara, s�som vardagliga processer och aff�rsmetoder, blir patenterbara.
rec16	sv	Den europeiska industrins konkurrensl�ge gentemot dess viktigaste handelspartner kommer att st�rkas om r�dande olikheter i det r�ttsskydd som finns f�r datorrelaterade uppfinningar undanr�js och r�ttsl�get klarg�rs. Med tanke p� att de traditionella tillverkningsindustrierna tenderar att flytta sin verksamhet till l�gkostnadsl�nder utanf�r EU �r det uppenbart att det �r viktigt att skydda immateriella r�ttigheter och i synnerhet patentskyddet.
rec17	sv	Detta direktiv b�r inte inverka p� till�mpningen av konkurrensreglerna, s�rskilt <I>artiklarna 81 och 82</I> i f�rdraget.
rec18	sv	De r�ttigheter som �r f�rbundna med patent f�r uppfinningar inom ramen f�r detta direktiv f�r inte p�verka handlingar som �r till�tna enligt artiklarna 5 och 6 i direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt enligt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga. Handlingar som enligt artiklarna 5 och 6 i det direktivet inte kr�ver r�ttsinnehavarens tillst�nd n�r det g�ller r�ttsinnehavarens upphovsr�tt till eller i samband med ett datorprogram, och som - om det inte vore f�r best�mmelserna i dessa artiklar - skulle kr�va ett s�dant tillst�nd, f�r inte kr�va r�ttsinnehavarens tillst�nd n�r det g�ller r�ttsinnehavarens patentr�ttigheter till eller i samband med datorprogrammet.
rec18a	sv	Under alla omst�ndigheter b�r medlemsstaterna i sin lagstiftning se till att patenten inneh�ller nyskapande ting med uppfinningsh�jd, s� att man undviker m�jligheten att n�gon l�gger beslag p� uppfinningar som redan �r offentligt tillg�ngliga endast genom att l�ta dem ing� i ett datorprogram.
art2a	sv	a) 'datorrelaterad uppfinning'
art2b	sv	b) 'tekniskt bidrag', �ven kallat uppfinning
art2ba	sv	ba) 'teknikomr�de'
art2bb	sv	bb) 'industri' i patentr�ttslig mening
art3	sv	utg�r
art3a	sv	Artikel 3a <P>Medlemsstaterna skall se till att databehandling inte uppfattas som tillh�rande ett teknikomr�de i patentr�ttslig mening och att innovationer p� databehandlingsomr�det inte uppfattas som uppfinningar i patentr�ttslig mening.
art4	sv	F�r att kunna patenteras m�ste en datorrelaterad uppfinning kunna tillgodog�ras industriellt, vara ny och ha uppfinningsh�jd. F�r att en datorrelaterad uppfinning skall kunna tillerk�nnas uppfinningsh�jd m�ste den utg�ra ett tekniskt bidrag. <P>Medlemsstaterna skall se till att en datorrelaterad uppfinning, som utg�r ett tekniskt bidrag, utg�r en n�dv�ndig f�ruts�ttning f�r uppfinningsh�jd. <P>Omfattningen av det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samtliga tekniska k�nnetecken och teknikens st�ndpunkt oberoende av om dessa k�nnetecken �tf�ljs av icke-tekniska k�nnetecken. <P>3a. F�r att avg�ra huruvida en datorrelaterad uppfinning utg�r ett tekniskt bidrag skall det testas om uppfinningen ger ny kunskap om sambandet mellan orsak och verkan i anv�ndningen av kontrollerbara naturkrafter och har industriell till�mpning i begreppets strikta bem�rkelse, vad g�ller s�v�l metoder som resultat.
art4a	sv	Artikel 4a <P>Undantag fr�n patenterbarhet <P>En datorrelaterad uppfinning skall inte anses ge ett tekniskt bidrag bara f�r att den inbegriper anv�ndning av dator, n�tverk eller andra programmerbara anordningar. F�ljaktligen skall patent inte kunna utf�rdas f�r en uppfinning som till�mpar aff�rsmetoder, matematiska metoder eller andra metoder med hj�lp av datorprogram och som inte medf�r n�gra tekniska effekter som �verstiger den vanliga fysiska v�xelverkan mellan programmet och datorn, n�tverket eller n�gon annan programmerbar anordning d�r den anv�nds.
art4b	sv	Artikel 4b <P>Medlemsstaterna skall se till att datorrelaterade l�sningar p� tekniska problem inte anses som patenterbara uppfinningar endast d�rf�r att de �kar effektiviteten i anv�ndningen av resurser inom databehandlingssystem.
art5_1	sv	Medlemsstaterna skall se till att en datorrelaterad uppfinning enbart kan patentskyddas s�som anordning, dvs. en programmerad komponent eller en teknisk produktionsprocess.
art5_1a	sv	1a. Medlemsstaterna garanterar att patentkrav som medgivits f�r datorrelaterade uppfinningar enbart omfattar det tekniska bidraget som ligger till grund f�r patentkravet. Patentkrav f�r enskilda datorprogram eller f�r program installerade p� datab�rare kan ej medges.
art5_1b	sv	1b. Medlemsstaterna skall se till att framtagning, hantering, bearbetning, spridning och offentligg�rande av information i n�gon form aldrig kan utg�ra ett direkt eller indirekt patentintr�ng, �ven d� en teknisk anordning anv�nds i detta syfte.
art5_1c	sv	1c. Medlemsstaterna skall se till att anv�ndning av datorprogram i syften som inte faller inom patentets till�mpningsomr�de inte kan anses utg�ra ett direkt eller indirekt patentintr�ng. <P>1d. Medlemsstaterna skall, om ett patentkrav inneh�ller k�nnetecken som inneb�r anv�ndning av ett datorprogram, se till att ett v�lfungerande och v�ldokumenterat referensgenomf�rande av detta program offentligg�rs som en beskrivande del utan n�gra restriktiva licensieringsvillkor.
art6	sv	De r�ttigheter som �r f�rbundna med patent f�r uppfinningar inom ramen f�r detta direktiv skall inte p�verka de handlingar som �r till�tna enligt artiklarna 5 och 6 i direktiv 91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt enligt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga.
art6a	sv	Artikel 6a <P>Anv�ndning av patenterad teknik <P>Medlemsstaterna skall, om anv�ndning av en patenterad teknik beh�vs f�r ett betydelsefullt syfte s�som att garantera omvandling av de konventioner som anv�nds i tv� olika datorsystem eller n�tverk f�r att m�jligg�ra kommunikation och utbyte av uppgifter mellan systemen, se till att denna anv�ndning inte betraktas som patentintr�ng.
art7	sv	Kommissionen skall f�lja hur datorrelaterade uppfinningar p�verkar innovation och konkurrens, b�de i Europa och internationellt, samt europeiskt n�ringsliv, s�rskilt de sm� och medelstora f�retagen och anv�ndningen av �ppen k�llkod, och n�thandel.
art8a	sv	Artikel 8a <P>�versyn av f�ljderna <P>P� grundval av uppf�ljningen enligt artikel 7 och rapporten enligt artikel 8 skall kommissionen se �ver f�ljderna av detta direktiv och, vid behov, f�rel�gga Europaparlamentet och r�det f�rslag om �ndring av g�llande lagstiftning.
art8b	sv	b) huruvida reglerna f�r patentets giltighetstid och f�r bed�mning av patenterbarhet, n�rmare best�mt nyhet, uppfinningsh�jd och l�mpligt skyddsomf�ng f�r kraven, �r �ndam�lsenliga, och
art8ca	sv	ca) huruvida sv�righeter har uppst�tt i f�rh�llandet mellan patentskydd f�r datorrelaterade uppfinningar och det upphovsr�ttsliga skyddet f�r datorprogram i enlighet med direktiv 91/250/EEG, och huruvida patentsystemet har missbrukats i fr�ga om datorrelaterade uppfinningar,
art8cb	sv	cb) huruvida det skulle vara �nskv�rt och r�ttsligt m�jligt med tanke p� gemenskapens internationella �taganden att inf�ra en 'skonfrist' f�r k�nnetecken som blir k�nda f�re ans�kningsdatumet och som ing�r i en patentans�kan, oberoende av vilket slags uppfinning det �r fr�ga om,
art8cc	sv	cc) huruvida det kan bli n�dv�ndigt att anordna en diplomatkonferens f�r att se �ver konventionen om meddelande av europeiska patent, inte minst mot bakgrund av inf�randet av gemenskapspatentet,
art8cd	sv	cd) i vilken m�n kraven i detta direktiv beaktas i Europeiska patentverkets praxis och i verkets bed�mningsriktlinjer.
art8ce	sv	ce) huruvida de befogenheter som delegeras till Europeiska patentverket �r f�renliga med kraven p� harmonisering av EU-lagstiftningen och med kraven p� principerna om �ppenhet och ansvarstagande.
art8cf	sv	cf) hur det p�verkar omvandlingen av de konventioner som anv�nds i tv� olika datorsystem, i syfte att m�jligg�ra kommunikation och utbyte av uppgifter,
art8cg	sv	cg) huruvida den i direktivet angivna m�jligheten att anv�nda en patenterad uppfinning endast f�r att garantera samverkansf�rm�ga mellan tv� system �r tillr�cklig,
art8_1a	sv	I denna rapport skall kommissionen motivera en eventuell �ndring av detta direktiv, och i f�rekommande fall ange de punkter till vilka den avser inge �ndringsf�rslag.
art9_1	sv	Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast den ...* och skall genast underr�tta kommissionen om detta. <P>* arton m�nader efter det att direktivet tr�tt i kraft
\.


--
-- Data for TOC entry 64 (OID 23712)
-- Name: acpt; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY acpt (id, lang, acpt) FROM stdin;
rec1	da	YES
rec5	da	YES
rec5a	da	COMP
rec6	da	COMP
rec7	da	NO
rec7a	da	YES
rec7b	da	NO ?
rec11	da	NO
rec12	da	NO
rec13	da	COMP
rec13a	da	YES
rec13b	da	YES
rec13c	da	YES
rec13d	da	YES
rec14	da	COMP
rec16	da	YES
rec17	da	YES
rec18	da	YES
rec18a	da	NO ?
art2a	da	NO
art2b	da	NO
art2ba	da	COMP
art2bb	da	NO
art3	da	YES
art3a	da	NO
art4_1	da	YES
art4_2	da	YES
art4_3	da	MIXED
art4_3a	da	NO
art4a	da	YES
art4b	da	NO
art5_1	da	NO
art5_1a	da	NO
art5_1b	da	NO
art5_1c	da	COMP
art5_1d	da	COMP
art6	da	YES
art6a	da	NO
art7	da	YES
art8a	da	YES
art8b	da	REWRITE
art8ca	da	YES
art8cb	da	COMP
art8cc	da	REWRITE
art8cd	da	YES
art8ce	da	COMP
art8cf	da	REWRITE
art8cg	da	COMP
art8_1a	da	NO
art9_1	da	YES
rec1	de	YES
rec5	de	YES
rec5a	de	COMP
rec6	de	COMP
rec7	de	NO
rec7a	de	YES
rec7b	de	NO ?
rec11	de	NO
rec12	de	NO
rec13	de	COMP
rec13a	de	YES
rec13b	de	YES
rec13c	de	YES
rec13d	de	YES
rec14	de	COMP
rec16	de	YES
rec17	de	YES
rec18	de	YES
rec18a	de	NO ?
art2a	de	NO
art2b	de	NO
art2ba	de	COMP
art2bb	de	NO
art3	de	YES
art3a	de	NO
art4_1	de	YES
art4_2	de	YES
art4_3	de	MIXED
art4_3a	de	NO
art4a	de	YES
art4b	de	NO
art5_1	de	NO
art5_1a	de	NO
art5_1b	de	NO
art5_1c	de	COMP
art5_1d	de	COMP
art6	de	YES
art6a	de	NO
art7	de	YES
art8a	de	YES
art8b	de	REWRITE
art8ca	de	YES
art8cb	de	COMP
art8cc	de	REWRITE
art8cd	de	YES
art8ce	de	COMP
art8cf	de	REWRITE
art8cg	de	COMP
art8_1a	de	NO
art9_1	de	YES
rec1	en	YES
rec5	en	YES
rec5a	en	COMP
rec6	en	COMP
rec7	en	NO
rec7a	en	YES
rec7b	en	NO ?
rec11	en	NO
rec12	en	NO
rec13	en	COMP
rec13a	en	YES
rec13b	en	YES
rec13c	en	YES
rec13d	en	YES
rec14	en	COMP
rec16	en	YES
rec17	en	YES
rec18	en	YES
rec18a	en	NO ?
art2a	en	NO
art2b	en	NO
art2ba	en	COMP
art2bb	en	NO
art3	en	YES
art3a	en	NO
art4_1	en	YES
art4_2	en	YES
art4_3	en	MIXED
art4_3a	en	NO
art4a	en	YES
art4b	en	NO
art5_1	en	NO
art5_1a	en	NO
art5_1b	en	NO
art5_1c	en	COMP
art5_1d	en	COMP
art6	en	YES
art6a	en	NO
art7	en	YES
art8a	en	YES
art8b	en	REWRITE
art8ca	en	YES
art8cb	en	COMP
art8cc	en	REWRITE
art8cd	en	YES
art8ce	en	COMP
art8cf	en	REWRITE
art8cg	en	COMP
art8_1a	en	NO
art9_1	en	YES
rec1	es	YES
rec5	es	YES
rec5a	es	COMP
rec6	es	COMP
rec7	es	NO
rec7a	es	YES
rec7b	es	NO ?
rec11	es	NO
rec12	es	NO
rec13	es	COMP
rec13a	es	YES
rec13b	es	YES
rec13c	es	YES
rec13d	es	YES
rec14	es	COMP
rec16	es	YES
rec17	es	YES
rec18	es	YES
rec18a	es	NO ?
art2a	es	NO
art2b	es	NO
art2ba	es	COMP
art2bb	es	NO
art3	es	YES
art3a	es	NO
art4_1	es	YES
art4_2	es	YES
art4_3	es	MIXED
art4_3a	es	NO
art4a	es	YES
art4b	es	NO
art5_1	es	NO
art5_1a	es	NO
art5_1b	es	NO
art5_1c	es	COMP
art5_1d	es	COMP
art6	es	YES
art6a	es	NO
art7	es	YES
art8a	es	YES
art8b	es	REWRITE
art8ca	es	YES
art8cb	es	COMP
art8cc	es	REWRITE
art8cd	es	YES
art8ce	es	COMP
art8cf	es	REWRITE
art8cg	es	COMP
art8_1a	es	NO
art9_1	es	YES
rec1	fi	YES
rec5	fi	YES
rec5a	fi	COMP
rec6	fi	COMP
rec7	fi	NO
rec7a	fi	YES
rec7b	fi	NO ?
rec11	fi	NO
rec12	fi	NO
rec13	fi	COMP
rec13a	fi	YES
rec13b	fi	YES
rec13c	fi	YES
rec13d	fi	YES
rec14	fi	COMP
rec16	fi	YES
rec17	fi	YES
rec18	fi	YES
rec18a	fi	NO ?
art2a	fi	NO
art2b	fi	NO
art2ba	fi	COMP
art2bb	fi	NO
art3	fi	YES
art3a	fi	NO
art4_1	fi	YES
art4_2	fi	YES
art4_3	fi	MIXED
art4_3a	fi	NO
art4a	fi	YES
art4b	fi	NO
art5_1	fi	NO
art5_1a	fi	NO
art5_1b	fi	NO
art5_1c	fi	COMP
art5_1d	fi	COMP
art6	fi	YES
art6a	fi	NO
art7	fi	YES
art8a	fi	YES
art8b	fi	REWRITE
art8ca	fi	YES
art8cb	fi	COMP
art8cc	fi	REWRITE
art8cd	fi	YES
art8ce	fi	COMP
art8cf	fi	REWRITE
art8cg	fi	COMP
art8_1a	fi	NO
art9_1	fi	YES
rec1	fr	YES
rec5	fr	YES
rec5a	fr	COMP
rec6	fr	COMP
rec7	fr	NO
rec7a	fr	YES
rec7b	fr	NO ?
rec11	fr	NO
rec12	fr	NO
rec13	fr	COMP
rec13a	fr	YES
rec13b	fr	YES
rec13c	fr	YES
rec13d	fr	YES
rec14	fr	COMP
rec16	fr	YES
rec17	fr	YES
rec18	fr	YES
rec18a	fr	NO ?
art2a	fr	NO
art2b	fr	NO
art2ba	fr	COMP
art2bb	fr	NO
art3	fr	YES
art3a	fr	NO
art4_1	fr	YES
art4_2	fr	YES
art4_3	fr	MIXED
art4_3a	fr	NO
art4a	fr	YES
art4b	fr	NO
art5_1	fr	NO
art5_1a	fr	NO
art5_1b	fr	NO
art5_1c	fr	COMP
art5_1d	fr	COMP
art6	fr	YES
art6a	fr	NO
art7	fr	YES
art8a	fr	YES
art8b	fr	REWRITE
art8ca	fr	YES
art8cb	fr	COMP
art8cc	fr	REWRITE
art8cd	fr	YES
art8ce	fr	COMP
art8cf	fr	REWRITE
art8cg	fr	COMP
art8_1a	fr	NO
art9_1	fr	YES
rec1	it	YES
rec5	it	YES
rec5a	it	COMP
rec6	it	COMP
rec7	it	NO
rec7a	it	YES
rec7b	it	NO ?
rec11	it	NO
rec12	it	NO
rec13	it	COMP
rec13a	it	YES
rec13b	it	YES
rec13c	it	YES
rec13d	it	YES
rec14	it	COMP
rec16	it	YES
rec17	it	YES
rec18	it	YES
rec18a	it	NO ?
art2a	it	NO
art2b	it	NO
art2ba	it	COMP
art2bb	it	NO
art3	it	YES
art3a	it	NO
art4_1	it	YES
art4_2	it	YES
art4_3	it	MIXED
art4_3a	it	NO
art4a	it	YES
art4b	it	NO
art5_1	it	NO
art5_1a	it	NO
art5_1b	it	NO
art5_1c	it	COMP
art5_1d	it	COMP
art6	it	YES
art6a	it	NO
art7	it	YES
art8a	it	YES
art8b	it	REWRITE
art8ca	it	YES
art8cb	it	COMP
art8cc	it	REWRITE
art8cd	it	YES
art8ce	it	COMP
art8cf	it	REWRITE
art8cg	it	COMP
art8_1a	it	NO
art9_1	it	YES
rec1	nl	YES
rec5	nl	YES
rec5a	nl	COMP
rec6	nl	COMP
rec7	nl	NO
rec7a	nl	YES
rec7b	nl	NO ?
rec11	nl	NO
rec12	nl	NO
rec13	nl	COMP
rec13a	nl	YES
rec13b	nl	YES
rec13c	nl	YES
rec13d	nl	YES
rec14	nl	COMP
rec16	nl	YES
rec17	nl	YES
rec18	nl	YES
rec18a	nl	NO ?
art2a	nl	NO
art2b	nl	NO
art2ba	nl	COMP
art2bb	nl	NO
art3	nl	YES
art3a	nl	NO
art4_1	nl	YES
art4_2	nl	YES
art4_3	nl	MIXED
art4_3a	nl	NO
art4a	nl	YES
art4b	nl	NO
art5_1	nl	NO
art5_1a	nl	NO
art5_1b	nl	NO
art5_1c	nl	COMP
art5_1d	nl	COMP
art6	nl	YES
art6a	nl	NO
art7	nl	YES
art8a	nl	YES
art8b	nl	REWRITE
art8ca	nl	YES
art8cb	nl	COMP
art8cc	nl	REWRITE
art8cd	nl	YES
art8ce	nl	COMP
art8cf	nl	REWRITE
art8cg	nl	COMP
art8_1a	nl	NO
art9_1	nl	YES
rec1	pt	YES
rec5	pt	YES
rec5a	pt	COMP
rec6	pt	COMP
rec7	pt	NO
rec7a	pt	YES
rec7b	pt	NO ?
rec11	pt	NO
rec12	pt	NO
rec13	pt	COMP
rec13a	pt	YES
rec13b	pt	YES
rec13c	pt	YES
rec13d	pt	YES
rec14	pt	COMP
rec16	pt	YES
rec17	pt	YES
rec18	pt	YES
rec18a	pt	NO ?
art2a	pt	NO
art2b	pt	NO
art2ba	pt	COMP
art2bb	pt	NO
art3	pt	YES
art3a	pt	NO
art4_1	pt	YES
art4_2	pt	YES
art4_3	pt	MIXED
art4_3a	pt	NO
art4a	pt	YES
art4b	pt	NO
art5_1	pt	NO
art5_1a	pt	NO
art5_1b	pt	NO
art5_1c	pt	COMP
art5_1d	pt	COMP
art6	pt	YES
art6a	pt	NO
art7	pt	YES
art8a	pt	YES
art8b	pt	REWRITE
art8ca	pt	YES
art8cb	pt	COMP
art8cc	pt	REWRITE
art8cd	pt	YES
art8ce	pt	COMP
art8cf	pt	REWRITE
art8cg	pt	COMP
art8_1a	pt	NO
art9_1	pt	YES
rec1	sv	YES
rec5	sv	YES
rec5a	sv	COMP
rec6	sv	COMP
rec7	sv	NO
rec7a	sv	YES
rec7b	sv	NO ?
rec11	sv	NO
rec12	sv	NO
rec13	sv	COMP
rec13a	sv	YES
rec13b	sv	YES
rec13c	sv	YES
rec13d	sv	YES
rec14	sv	COMP
rec16	sv	YES
rec17	sv	YES
rec18	sv	YES
rec18a	sv	NO ?
art2a	sv	NO
art2b	sv	NO
art2ba	sv	COMP
art2bb	sv	NO
art3	sv	YES
art3a	sv	NO
art4_1	sv	YES
art4_2	sv	YES
art4_3	sv	MIXED
art4_3a	sv	NO
art4a	sv	YES
art4b	sv	NO
art5_1	sv	NO
art5_1a	sv	NO
art5_1b	sv	NO
art5_1c	sv	COMP
art5_1d	sv	COMP
art6	sv	YES
art6a	sv	NO
art7	sv	YES
art8a	sv	YES
art8b	sv	REWRITE
art8ca	sv	YES
art8cb	sv	COMP
art8cc	sv	REWRITE
art8cd	sv	YES
art8ce	sv	COMP
art8cf	sv	REWRITE
art8cg	sv	COMP
art8_1a	sv	NO
art9_1	sv	YES
\.


--
-- Data for TOC entry 65 (OID 23720)
-- Name: ffii; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY ffii (id, lang, ffii) FROM stdin;
rec1	da	
rec5	da	-
rec5a	da	+
rec6	da	+
rec7	da	+
rec7a	da	o
rec7b	da	+
rec11	da	-
rec12	da	++
rec13	da	+
rec13a	da	+
rec13b	da	+
rec13c	da	
rec13d	da	+
rec14	da	
rec16	da	-
rec17	da	
rec18	da	o
rec18a	da	-
art2a	da	+++
art2b	da	+++
art2ba	da	++
art2bb	da	++
art3	da	+
art3a	da	+++
art4_1	da	-
art4_2	da	-
art4_3	da	
art4_3a	da	+
art4a	da	
art4b	da	++
art5_1	da	+
art5_1a	da	o
art5_1b	da	++
art5_1c	da	o
art5_1d	da	+
art6	da	o
art6a	da	++
art7	da	+
art8a	da	
art8b	da	+
art8ca	da	
art8cb	da	-
art8cc	da	
art8cd	da	
art8ce	da	+
art8cf	da	+
art8cg	da	+
art8_1a	da	+
art9_1	da	+
rec1	de	
rec5	de	-
rec5a	de	+
rec6	de	+
rec7	de	+
rec7a	de	o
rec7b	de	+
rec11	de	-
rec12	de	++
rec13	de	+
rec13a	de	+
rec13b	de	+
rec13c	de	
rec13d	de	+
rec14	de	
rec16	de	-
rec17	de	
rec18	de	o
rec18a	de	-
art2a	de	+++
art2b	de	+++
art2ba	de	++
art2bb	de	++
art3	de	+
art3a	de	+++
art4_1	de	-
art4_2	de	-
art4_3	de	
art4_3a	de	+
art4a	de	
art4b	de	++
art5_1	de	+
art5_1a	de	o
art5_1b	de	++
art5_1c	de	o
art5_1d	de	+
art6	de	o
art6a	de	++
art7	de	+
art8a	de	
art8b	de	+
art8ca	de	
art8cb	de	-
art8cc	de	
art8cd	de	
art8ce	de	+
art8cf	de	+
art8cg	de	+
art8_1a	de	+
art9_1	de	+
rec1	en	
rec5	en	-
rec5a	en	+
rec6	en	+
rec7	en	+
rec7a	en	o
rec7b	en	+
rec11	en	-
rec12	en	++
rec13	en	+
rec13a	en	+
rec13b	en	+
rec13c	en	
rec13d	en	+
rec14	en	
rec16	en	-
rec17	en	
rec18	en	o
rec18a	en	-
art2a	en	+++
art2b	en	+++
art2ba	en	++
art2bb	en	++
art3	en	+
art3a	en	+++
art4_1	en	-
art4_2	en	-
art4_3	en	
art4_3a	en	+
art4a	en	
art4b	en	++
art5_1	en	+
art5_1a	en	o
art5_1b	en	++
art5_1c	en	o
art5_1d	en	+
art6	en	o
art6a	en	++
art7	en	+
art8a	en	
art8b	en	+
art8ca	en	
art8cb	en	-
art8cc	en	
art8cd	en	
art8ce	en	+
art8cf	en	+
art8cg	en	+
art8_1a	en	+
art9_1	en	+
rec1	es	
rec5	es	-
rec5a	es	+
rec6	es	+
rec7	es	+
rec7a	es	o
rec7b	es	+
rec11	es	-
rec12	es	++
rec13	es	+
rec13a	es	+
rec13b	es	+
rec13c	es	
rec13d	es	+
rec14	es	
rec16	es	-
rec17	es	
rec18	es	o
rec18a	es	-
art2a	es	+++
art2b	es	+++
art2ba	es	++
art2bb	es	++
art3	es	+
art3a	es	+++
art4_1	es	-
art4_2	es	-
art4_3	es	
art4_3a	es	+
art4a	es	
art4b	es	++
art5_1	es	+
art5_1a	es	o
art5_1b	es	++
art5_1c	es	o
art5_1d	es	+
art6	es	o
art6a	es	++
art7	es	+
art8a	es	
art8b	es	+
art8ca	es	
art8cb	es	-
art8cc	es	
art8cd	es	
art8ce	es	+
art8cf	es	+
art8cg	es	+
art8_1a	es	+
art9_1	es	+
rec1	fi	
rec5	fi	-
rec5a	fi	+
rec6	fi	+
rec7	fi	+
rec7a	fi	o
rec7b	fi	+
rec11	fi	-
rec12	fi	++
rec13	fi	+
rec13a	fi	+
rec13b	fi	+
rec13c	fi	
rec13d	fi	+
rec14	fi	
rec16	fi	-
rec17	fi	
rec18	fi	o
rec18a	fi	-
art2a	fi	+++
art2b	fi	+++
art2ba	fi	++
art2bb	fi	++
art3	fi	+
art3a	fi	+++
art4_1	fi	-
art4_2	fi	-
art4_3	fi	
art4_3a	fi	+
art4a	fi	
art4b	fi	++
art5_1	fi	+
art5_1a	fi	o
art5_1b	fi	++
art5_1c	fi	o
art5_1d	fi	+
art6	fi	o
art6a	fi	++
art7	fi	+
art8a	fi	
art8b	fi	+
art8ca	fi	
art8cb	fi	-
art8cc	fi	
art8cd	fi	
art8ce	fi	+
art8cf	fi	+
art8cg	fi	+
art8_1a	fi	+
art9_1	fi	+
rec1	fr	
rec5	fr	-
rec5a	fr	+
rec6	fr	+
rec7	fr	+
rec7a	fr	o
rec7b	fr	+
rec11	fr	-
rec12	fr	++
rec13	fr	+
rec13a	fr	+
rec13b	fr	+
rec13c	fr	
rec13d	fr	+
rec14	fr	
rec16	fr	-
rec17	fr	
rec18	fr	o
rec18a	fr	-
art2a	fr	+++
art2b	fr	+++
art2ba	fr	++
art2bb	fr	++
art3	fr	+
art3a	fr	+++
art4_1	fr	-
art4_2	fr	-
art4_3	fr	
art4_3a	fr	+
art4a	fr	
art4b	fr	++
art5_1	fr	+
art5_1a	fr	o
art5_1b	fr	++
art5_1c	fr	o
art5_1d	fr	+
art6	fr	o
art6a	fr	++
art7	fr	+
art8a	fr	
art8b	fr	+
art8ca	fr	
art8cb	fr	-
art8cc	fr	
art8cd	fr	
art8ce	fr	+
art8cf	fr	+
art8cg	fr	+
art8_1a	fr	+
art9_1	fr	+
rec1	it	
rec5	it	-
rec5a	it	+
rec6	it	+
rec7	it	+
rec7a	it	o
rec7b	it	+
rec11	it	-
rec12	it	++
rec13	it	+
rec13a	it	+
rec13b	it	+
rec13c	it	
rec13d	it	+
rec14	it	
rec16	it	-
rec17	it	
rec18	it	o
rec18a	it	-
art2a	it	+++
art2b	it	+++
art2ba	it	++
art2bb	it	++
art3	it	+
art3a	it	+++
art4_1	it	-
art4_2	it	-
art4_3	it	
art4_3a	it	+
art4a	it	
art4b	it	++
art5_1	it	+
art5_1a	it	o
art5_1b	it	++
art5_1c	it	o
art5_1d	it	+
art6	it	o
art6a	it	++
art7	it	+
art8a	it	
art8b	it	+
art8ca	it	
art8cb	it	-
art8cc	it	
art8cd	it	
art8ce	it	+
art8cf	it	+
art8cg	it	+
art8_1a	it	+
art9_1	it	+
rec1	nl	
rec5	nl	-
rec5a	nl	+
rec6	nl	+
rec7	nl	+
rec7a	nl	o
rec7b	nl	+
rec11	nl	-
rec12	nl	++
rec13	nl	+
rec13a	nl	+
rec13b	nl	+
rec13c	nl	
rec13d	nl	+
rec14	nl	
rec16	nl	-
rec17	nl	
rec18	nl	o
rec18a	nl	-
art2a	nl	+++
art2b	nl	+++
art2ba	nl	++
art2bb	nl	++
art3	nl	+
art3a	nl	+++
art4_1	nl	-
art4_2	nl	-
art4_3	nl	
art4_3a	nl	+
art4a	nl	
art4b	nl	++
art5_1	nl	+
art5_1a	nl	o
art5_1b	nl	++
art5_1c	nl	o
art5_1d	nl	+
art6	nl	o
art6a	nl	++
art7	nl	+
art8a	nl	
art8b	nl	+
art8ca	nl	
art8cb	nl	-
art8cc	nl	
art8cd	nl	
art8ce	nl	+
art8cf	nl	+
art8cg	nl	+
art8_1a	nl	+
art9_1	nl	+
rec1	pt	
rec5	pt	-
rec5a	pt	+
rec6	pt	+
rec7	pt	+
rec7a	pt	o
rec7b	pt	+
rec11	pt	-
rec12	pt	++
rec13	pt	+
rec13a	pt	+
rec13b	pt	+
rec13c	pt	
rec13d	pt	+
rec14	pt	
rec16	pt	-
rec17	pt	
rec18	pt	o
rec18a	pt	-
art2a	pt	+++
art2b	pt	+++
art2ba	pt	++
art2bb	pt	++
art3	pt	+
art3a	pt	+++
art4_1	pt	-
art4_2	pt	-
art4_3	pt	
art4_3a	pt	+
art4a	pt	
art4b	pt	++
art5_1	pt	+
art5_1a	pt	o
art5_1b	pt	++
art5_1c	pt	o
art5_1d	pt	+
art6	pt	o
art6a	pt	++
art7	pt	+
art8a	pt	
art8b	pt	+
art8ca	pt	
art8cb	pt	-
art8cc	pt	
art8cd	pt	
art8ce	pt	+
art8cf	pt	+
art8cg	pt	+
art8_1a	pt	+
art9_1	pt	+
rec1	sv	
rec5	sv	-
rec5a	sv	+
rec6	sv	+
rec7	sv	+
rec7a	sv	o
rec7b	sv	+
rec11	sv	-
rec12	sv	++
rec13	sv	+
rec13a	sv	+
rec13b	sv	+
rec13c	sv	
rec13d	sv	+
rec14	sv	
rec16	sv	-
rec17	sv	
rec18	sv	o
rec18a	sv	-
art2a	sv	+++
art2b	sv	+++
art2ba	sv	++
art2bb	sv	++
art3	sv	+
art3a	sv	+++
art4_1	sv	-
art4_2	sv	-
art4_3	sv	
art4_3a	sv	+
art4a	sv	
art4b	sv	++
art5_1	sv	+
art5_1a	sv	o
art5_1b	sv	++
art5_1c	sv	o
art5_1d	sv	+
art6	sv	o
art6a	sv	++
art7	sv	+
art8a	sv	
art8b	sv	+
art8ca	sv	
art8cb	sv	-
art8cc	sv	
art8cd	sv	
art8ce	sv	+
art8cf	sv	+
art8cg	sv	+
art8_1a	sv	+
art9_1	sv	+
\.


--
-- Data for TOC entry 66 (OID 23728)
-- Name: just; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY just (id, lang, just) FROM stdin;
rec1	da	Investeringer afh�nger ikke kun af en effektiv og harmoniseret beskyttelse, men ogs� af gennemskuelighed. (JURI).
rec5	da	Form�let med enhver lov vedr�rende udtagning af patent er ikke at sikre indehaverne af patenter en fordel
rec7a	da	Den Europ�iske Patentkonvention er et internationalt instrument, der kun kan �ndres ved hj�lp af de mekanismer, der er fastsat i konventionen selv. (JURI).
rec7b	da	Kr�ver ingen n�rmere forklaring. (ITRE).
rec11	da	
rec13a	da	
rec13b	da	Denne betragtning tager sigte p� at sikre, at kravet om opfindelsesh�jde og hermed om et teknisk bidrag ikke kan omg�s ved opfindsom udarbejdelse af patentkravet. (JURI).
rec13c	da	I henhold til artikel 52, stk. 2, litra a) og c), i Den Europ�iske Patentkonvention kan "matematiske metoder" og "planer, regler og metoder for intellektuel virksomhed, for spil eller for erhvervsvirksomhed samt edb-programmer" ikke patenteres. Eftersom en algoritme kunne v�re et edb-program eller et element i et s�dant program uafh�ngigt af k�rselsmilj�et eller en matematisk formel eller metode, er den som s�dan ikke patenterbar. Den blotte anvendelse af en algoritme udelukker imidlertid ikke patenterbarhed. (JURI).
rec13d	da	
rec14	da	
rec16	da	Den �konomiske betydning af dette direktiv b�r ikke undervurderes. Desuden har unders�gelser vist, at der er en sammenh�ng mellem F&amp;U-udgifter, patentans�gninger og produktivitet. Beskyttelse af intellektuel ejendomsret skaber og sikrer job i Europa og bringer indt�gter. (JURI).
rec17	da	Betragtninger m� ikke affattes som normative bestemmelser. (JURI).
rec18	da	Ubegr�nset patentbeskyttelse for software kunne i henhold til patentretten g�re det ulovligt at udf�re "reverse engineering", som anvendes af edb-udviklere til at opn� interoperabilitet s�ledes som tilladt i �jeblikket i henhold til undtagelserne i direktivet om edb-programmer. Derfor skal fremtidig EU-lovgivning vedr�rende edb-patenter omfatte en eksplicit undtagelse fra patentrettighederne for at sikre, at edb-udviklere fortsat kan f�lge samme praksis for at opn� interoperabilitet i henhold til patentretten, som de inden for gr�nserne af bestemmelserne om ophavsrettigheder har lov til at f�lge i dag.<P>R�dets f�lles holdning af 8. november 2002 st�ttes og afklares gennem en henvisning til artikel 5 og 6 i direktiv 91/250/E�F. (JURI).
art3	da	Ordlyden i forslaget g�r det ganske enkelt umuligt at diskutere den tekniske karakter af en p�st�et opfindelse. Opfyldelsen af denne betingelse skal bevises, ikke tages for givet. (ITRE).
art4_1	da	(JURI).
art4_2	da	(JURI).
art4_3	da	
art4_3a	da	
art4a	da	Sammen med den tilsvarende betragtning g�r �ndringsforslaget det klart, at anf�relse af anvendelse af tekniske midler ikke i sig selv er nok til at g�re en opfindelse patenterbar. Der skal v�re tale om et teknisk bidrag. Det g�res ogs� klart, at computer-implementering af en forretningsmetode ikke er en patenterbar opfindelse. (JURI).
art5_1	da	(ITRE).
art5_1b	da	(ITRE).
art5_1c	da	(ITRE).
art5_1d	da	(ITRE).
art6	da	Ubegr�nset patentbeskyttelse for software kunne i henhold til patentretten g�re det ulovligt at udf�re "reverse engineering", som anvendes af edb-udviklere til at opn� interoperabilitet s�ledes som tilladt i �jeblikket i henhold til undtagelserne i direktivet om edb-programmer. Derfor skal fremtidig EU-lovgivning vedr�rende edb-patenter omfatte en eksplicit undtagelse fra patentrettighederne for at sikre, at edb-udviklere fortsat kan f�lge samme praksis for at opn� interoperabilitet i henhold til patentretten, som de inden for gr�nserne af bestemmelserne om ophavsrettigheder har lov til at f�lge i dag.<P>R�dets f�lles holdning af 8. november 2002 st�ttes og afklares gennem en henvisning til artikel 5 og 6 i direktiv 91/250/E�F. (JURI).
art6a	da	Muligheden for at forbinde udstyr, s�ledes at det bliver interoperabelt, er en m�de, hvorp� man kan sikre �bne net og forhindre misbrug af en dominerende stilling. Dette har is�r EF-Domstolen givet specifikt udtryk for i sin domspraksis. Patentretten b�r ikke g�re det muligt at tilsides�tte dette princip p� den frie konkurrences og brugernes bekostning. (ITRE).
art8a	da	
art8b	da	
art8ca	da	Der er udtrykt bekymring med hensyn til direktivets indvirkning p� beskyttelsen af ophavsret til software og undtagelserne fra interoperabilitet i henhold til direktiv 91/250/E�F. Indf�jelsen af denne bestemmelse ville ogs� s�tte Kommissionen i stand til at overv�ge misbrug af patentsystemet p� dette omr�de. (JURI).
art8cb	da	Man har kraftigt argumenteret for, at det er n�dvendigt med en henstandsperiode (grace period) for at forhindre, at en opfinder mister retten til sin opfindelse, n�r den er bragt til offentlighedens kendskab, inden der ans�ges om patent, f.eks. for at afpr�ve, om der er markedsinteresse herfor. Det fremf�res, at dette ville v�re s�rlig nyttigt for innovative SMV'er og for samarbejdet mellem universiteter og industrien. En s�dan nyskabelse kan imidlertid ikke indf�res udelukkende for patenter p� computer-implementerede opfindelser, uden at der er foretaget en forudg�ende unders�gelse af den indvirkning og forenelighed med F�llesskabets internationale forpligtelser i henhold til f.eks. TRIPS. (JURI).
art8cc	da	
art8cd	da	
art8cf	da	
art8cg	da	
art8_1a	da	
art9_1	da	Det er n�dvendigt at anf�re tidspunktet for direktivets ikrafttr�den. (JURI).
rec1	de	F�r Investitionen bedarf es nicht nur eines wirksamen und einheitlichen Schutzes, sondern auch der Transparenz. (JURI).
rec12	da	Betragtninger m� ikke affattes som normative bestemmelser.
rec5	de	Es ist nicht das Ziel eines Gesetzes �ber die Patentierung sicherzustellen, dass die Patentinhaber Nutzen daraus ziehen
rec7a	de	(JURI).
rec7b	de	Bedarf keiner Erl�uterung. (ITRE).
rec11	de	
rec13a	de	
rec13b	de	Durch diese Erw�gung soll sichergestellt werden, dass das Erfordernis einer erfinderischen T�tigkeit und somit eines technischen Beitrags nicht durch eine geschickte Formulierung der Patentanspr�che umgangen werden kann. (JURI).
rec13c	de	Nach Artikel 52 Absatz 2 Buchstaben a und c des Europ�ischen Patent�bereinkommens sind von der Patentierbarkeit ausgeschlossen
rec13d	de	
rec14	de	
rec16	de	Die wirtschaftliche Bedeutung dieser Richtlinie sollte nicht untersch�tzt werden. Au�erdem haben Untersuchungen ergeben, dass ein Zusammenhang zwischen Ausgaben f�r Forschung und Entwicklung, Patentanmeldungen und Produktivit�t besteht. Urheberrechtsschutz schafft und sichert Arbeitspl�tze in Europa und f�hrt zu Ertr�gen. (JURI).
rec17	de	(JURI).
rec18	de	Unbeschr�nkter Patentschutz f�r Software k�nnte dazu f�hren, dass es gem�� dem Patentrecht unrechtm��ig ist, "reverse engineering"-Methoden anzuwenden, die von Softwareentwicklern eingesetzt werden, um Interoperabilit�t zu erzielen, wie dies derzeit im Rahmen der Ausnahmen der Richtlinie �ber Software-Urheberrecht gestattet ist. Daher m�ssen k�nftige EU-Rechtsvorschriften �ber Software-Patente eine ausdr�ckliche Ausnahme f�r Patentrechte beinhalten, um zu gew�hrleisten, dass die Entwickler von Software im Rahmen des Patentrechts weiterhin die gleichen Handlungen zur Verwirklichung der Interoperabilit�t durchf�hren k�nnen, wie ihnen dies heute im Rahmen des Urheberrechts gestattet ist.<P>Der gemeinsame Ansatz des Rates vom 8. November 2002 wird unter Bezugnahme auf Artikel 5 und 6 der Richtlinie 91/250/EWG unterst�tzt und klargestellt. (JURI).
art3	de	Die Formulierung des Vorschlags macht es einfach unm�glich, �ber den technischen Charakter einer beanspruchten Erfindung zu diskutieren. Diese Voraussetzung muss nachgewiesen werden und darf nicht als selbstverst�ndlich betrachtet werden. (ITRE).
art4_1	de	(JURI).
art4_2	de	(JURI).
art4_3	de	
art4_3a	de	
art4a	de	Hierdurch wird zusammen mit der entsprechenden Erw�gung klargestellt, dass die Spezifizierung technischer Hilfsmittel nicht allein f�r die Patentierbarkeit ausreicht. Au�erdem wird klargestellt, dass die Implementierung einer Gesch�ftsmethode in einem Computer allein keine patentf�hige Erfindung ist. (JURI).
art5_1	de	(ITRE).
art5_1b	de	(ITRE).
art5_1c	de	(ITRE).
art5_1d	de	(ITRE).
art6	de	Unbeschr�nkter Patentschutz f�r Software k�nnte dazu f�hren, dass es gem�� dem Patentrecht unrechtm��ig ist, "reverse engineering"-Methoden anzuwenden, die von Softwareentwicklern eingesetzt werden, um Interoperabilit�t zu erzielen, wie dies derzeit im Rahmen der Ausnahmen der Richtlinie �ber Software-Urheberrecht gestattet ist. Daher m�ssen k�nftige EU-Rechtsvorschriften �ber Software-Patente eine ausdr�ckliche Ausnahme f�r Patentrechte beinhalten, um zu gew�hrleisten, dass die Entwickler von Software im Rahmen des Patentrechts weiterhin die gleichen Handlungen zur Verwirklichung der Interoperabilit�t durchf�hren k�nnen, wie ihnen dies heute im Rahmen des Urheberrechts gestattet ist.<P>Der gemeinsame Ansatz des Rates vom 8. November 2002 wird unter Bezugnahme auf Artikel 5 und 6 der Richtlinie 91/250/EWG unterst�tzt und klargestellt. (JURI).
art6a	de	Die M�glichkeit, Ger�te miteinander zu verbinden, um sie interoperabel zu machen, ist eine Methode, um f�r offene Netze zu sorgen und den Missbrauch marktbeherrschender Stellungen zu vermeiden. Diese spezifische Entscheidung findet sich insbesondere in der Rechtsprechung des Gerichtshofs der Europ�ischen Gemeinschaften. Das Patentrecht sollte nicht die M�glichkeit schaffen, diesen Grundsatz auf Kosten des freien Wettbewerbs und der Nutzer umzusto�en. (ITRE).
art8a	de	
art8b	de	
art8ca	de	(JURI).
art8cb	de	(JURI).
art8cc	de	
art8cd	de	
art8cf	de	
art8cg	de	
art8_1a	de	
art9_1	de	(JURI).
rec1	en	Investment depends not only on effective and harmonised protection, but also on transparency. (JURI).
rec5	en	The object of any law relating to patenting is not to ensure that patent-holders enjoy an advantage
rec7a	en	The European Patent Convention is an international instrument which can be amended only by the mechanisms provided for in the convention itself. (JURI).
rec7b	en	Self-explanatory. (ITRE).
rec13b	en	This recital is designed to ensure that the requirement for inventive step and hence for a technical contribution cannot be circumvented through ingenious drafting of the patent claims. (JURI).
rec6	de	Das Europ�ische Patent�bereinkommen ist ein internationales Rechtsinstrument, das nur noch durch die dort vorgesehenen Mechanismen ge�ndert werden kann.
art2a	de	Durch diese Erw�gung wird die Rechtslage wiedergegeben, wie sie in Artikel 52 Absatz 1 des Europ�ischen Patent�bereinkommens ihren Ausdruck findet.
rec13c	en	Article 52(2)(a) and (c) of the European Patent Convention precludes the patentability of "mathematical methods" and "schemes, rules and methods for performing mental acts, playing games and doing business, and programs for computers". Since an algorithm could be a computer program or an element of such a program in isolation from its execution environment or a mathematical formula or method, it is, as such, precluded from patentablity. However, the mere use of an algorithm does not preclude patentability. (JURI).
rec13d	en	
rec16	en	The economic importance of this Directive should not be underestimated. Moreover, studies have shown a link between R&amp;D spending, patent applications and productivity. Intellectual property protection creates and secures jobs in Europe and brings in revenue. (JURI).
rec17	en	It is bad draftsmanship to couch recitals as normative provisions. (JURI).
rec18	en	Unlimited patent protection for software could make it illegal under patent law to engage in reverse engineering practices employed by software developers to achieve interoperability as currently permitted under the exceptions in the Software Copyright Directive. Therefore future EU legislation related to software patents must include an explicit exception to patent rights in order to ensure that developers of software can continue to engage in the same acts to achieve interoperability under patent law as they are allowed to today within the limits of copyright law.<P>The Council's common approach of 8 November 2002 is supported and clarified by a reference to Articles 5 and 6 of Directive 91/250/EEC. (JURI).
art3	en	The wording of the proposal makes it simply impossible to discuss the technical nature of a claimed invention. This condition has to be proved, and not taken for granted. (ITRE).
art4_1	en	(JURI).
art4_2	en	(JURI).
art4_3	en	
art4_3a	en	
art4a	en	This, in conjunction with the corresponding recital, provides clarification that simply specifying technical means is not enough for patentability. There must be a technical contribution. It also makes it clear that the computer implementation of a business method simpliciter is not a patentable invention. (JURI).
art5_1	en	(ITRE).
art5_1b	en	(ITRE).
art5_1c	en	(ITRE).
art5_1d	en	(ITRE).
art6	en	Unlimited patent protection for software could make it illegal under patent law to engage in reverse engineering practices employed by software developers to achieve interoperability as currently permitted under the exceptions in the Software Copyright Directive. Therefore future EU-legislation related to software patents must include an explicit exception to patent rights in order to ensure that developers of software can continue to engage in the same acts to achieve interoperability under patent law as they are allowed to today within the limits of copyright law.<P>The Council's common approach of 8 November 2002 is supported and clarified by a reference to Articles 5 and 6 of Directive 91/250/EEC. (JURI).
art6a	en	The possibility of connecting equipments so as to make them interoperable is a way of ensuring open networks and avoiding abuse of dominant positions. This has been specifically ruled in the case law of the Court of Justice of the European Communities in particular. Patent law should not make it possible to override this principle at the expense of free competition and users. (ITRE).
art8a	en	
art8b	en	
art8ca	en	(JURI).
art8cb	en	(JURI).
art8cc	en	
art8cd	en	
art8cf	en	
art8cg	en	
art8_1a	en	
art9_1	en	It is necessary to specify the date by which the Directive should enter into force. (JURI).
rec1	es	Las inversiones est�n basadas no �nicamente en una protecci�n eficaz y armonizada, sino tambi�n en la transparencia. (JURI).
rec5	es	El objeto de cualquier ley relativa a la patente no es garantizar que los titulares de la patente tengan una ventaja
rec7a	es	El Convenio de la Patente Europea es un instrumento internacional que no puede ser modificado m�s que por los mecanismos previstos en �l. (JURI).
rec7b	es	Esta enmienda se explica por s� sola. (ITRE).
rec11	es	
rec13a	es	
rec13b	es	Este considerando tiene el prop�sito de garantizar que el requisito de la actividad inventiva y, por tanto, de la </I><B>contribu</B><I>ci�n t�cnica, no puede eludirse con una redacci�n ingeniosa de las reivindicaciones de patente. (JURI).
rec13c	es	Las letras a) y c) del art�culo 52 del Convenio sobre la Patente Europea excluyen la patentabilidad de "m�todos matem�ticos" y "planes, principios y m�todos para el ejercicio de actividades intelectuales, para juegos o para actividades econ�micas, as� como los programas de ordenadores". Dado que un algoritmo puede ser un programa inform�tico o un elemento de un programa aislado de su entorno de ejecuci�n o una f�rmula o m�todo matem�tico, est� excluido en cuanto tal de la patentabilidad. Sin embargo, el mero uso de un algoritmo no excluye la patentabilidad. (JURI).
rec13d	es	
rec14	es	
rec16	es	No deber�a subestimarse la importancia econ�mica de la presente Directiva. Adem�s, hay estudios que muestran la relaci�n entre el gasto en I+D, las solicitudes de patente y la productividad. La protecci�n de la propiedad intelectual crea y asegura puestos de trabajo en Europa y produce ingresos. (JURI).
rec17	es	Denota mala t�cnica redactar los considerandos como disposiciones normativas. (JURI).
art7	en	Self-explanatory.
rec18	es	Una protecci�n ilimitada mediante patente para los programas inform�ticos har�a ilegal, de conformidad con el Derecho de patentes, recurrir a las pr�cticas de descompilaci�n empleadas por los creadores de programas inform�ticos para lograr la interoperabilidad, tal como se permite normalmente en virtud de las excepciones de la Directiva relativa a los derechos de autor de programas inform�ticos. Por consiguiente, la legislaci�n futura de la UE en el �mbito de las patentes de programas inform�ticos debe incluir una excepci�n expl�cita a los derechos de patente, a fin de garantizar que los creadores de programas inform�ticos puedan seguir dedic�ndose a las mismas actividades para lograr la interoperabilidad de conformidad con el Derecho de patentes que les est�n permitidas por el momento en los l�mites de la legislaci�n relativa a los derechos de autor.<P>La referencia a los art�culos 5 y 6 de la Directiva 91/250/CEE apoya y clarifica el planteamiento com�n del Consejo de 8 de noviembre de 2002. (JURI).
art3	es	La redacci�n de la propuesta hace sencillamente imposible discutir el car�cter t�cnico de una supuesta invenci�n. Hay que demostrar esta condici�n, y no darla por supuesta. (ITRE).
art4_1	es	(JURI).
art4_2	es	(JURI).
art4_3	es	
art4_3a	es	
art4a	es	Esta adici�n, junto con el considerando correspondiente, aporta la clarificaci�n de que para la patentabilidad no basta con la mera especificaci�n de medios t�cnicos, debe existir asimismo una contribuci�n t�cnica. Tambi�n aclara que simplemente no es patentable la implementaci�n de un m�todo comercial en ordenador. (JURI).
art5_1	es	(ITRE).
art5_1b	es	(ITRE).
art5_1c	es	(ITRE).
art5_1d	es	(ITRE).
art6	es	Una protecci�n ilimitada mediante patente para los programas inform�ticos har�a ilegal, de conformidad con el Derecho de patentes, recurrir a las pr�cticas de descompilaci�n empleadas por los creadores de programas inform�ticos para lograr la interoperabilidad, tal como se permite normalmente en virtud de las excepciones de la Directiva relativa a los derechos de autor de programas inform�ticos. Por consiguiente, la legislaci�n futura de la UE en el �mbito de las patentes de programas inform�ticos debe incluir una excepci�n expl�cita a los derechos de patente, a fin de garantizar que los creadores de programas inform�ticos puedan seguir dedic�ndose a las mismas actividades para lograr la interoperabilidad de conformidad con el Derecho de patentes que les est�n permitidas por el momento en los l�mites de la legislaci�n relativa a los derechos de autor.<P>La referencia a los art�culos 5 y 6 de la Directiva 91/250/CEE apoya y clarifica el planteamiento com�n del Consejo de 8 de noviembre de 2002. (JURI).
art6a	es	La posibilidad de conectar equipos de modo que sean interoperables es una manera de garantizar redes abiertas y de evitar el abuso de posiciones dominantes. Esta cuesti�n la ha regulado espec�ficamente la jurisprudencia del Tribunal de Justicia de las Comunidades Europeas. La legislaci�n sobre patentes no deber�a ofrecer la posibilidad de conculcar este principio a expensas de la libre competencia y de los usuarios. (ITRE).
art8a	es	
art8b	es	
art8ca	es	(JURI).
art8cb	es	(JURI).
art8cc	es	
art8cd	es	
art8cf	es	
art8cg	es	
art8_1a	es	
art9_1	es	Es necesario especificar una fecha de entrada en vigor de la Directiva. (JURI).
rec1	fi	(JURI).
rec5	fi	Patenttilakien tarkoituksena ei ole varmistaa, ett� patentinhaltijat saavat jotain etua, vaan patentinhaltijalle annettu etu on vain keino kannustaa innovointiprosessia koko yhteiskunnan hyv�ksi. Patentinhaltijalle annettu etu ei saa toimia t�t� patenttiperiaatteen perimm�ist� tavoitetta vastaan.<P>On my�s t�rke�� korostaa, ett� direktiivin perimm�isen� tarkoituksena on taata oikeusvarmuus ja lain yhdenmukainen tulkitseminen ja soveltaminen kansallisissa tuomioistuimissa. Se, ett� EY:n perustamissopimuksen 220 artiklan nojalla yhteis�n ensimm�isen oikeusasteen tuomioistuimen yhteyteen voidaan asettaa erityinen laink�ytt�lautakunta, on my�s mielenkiintoinen mahdollisuus t�ss� yhteydess� (JURI).
rec7a	fi	Euroopan patenttiyleissopimus on kansainv�linen v�line, jota ei voida muuttaa muuten kuin siin� m��r�tyill� mekanismeilla. (JURI).
rec7b	fi	Tarkistus ei kaipaa perusteluja. (ITRE).
rec11	fi	
rec13a	fi	
rec13b	fi	T�ll� johdanto-osan kappaleella pyrit��n varmistamaan, ettei vaatimusta keksinn�llisyydest� ja lis�yksest� tekniikan tasoon pystyt� kiert�m��n patenttihakemuksen taidokkaan muotoilun avulla. (JURI).
rec13c	fi	(JURI).
rec13d	fi	
rec14	fi	
rec16	fi	Direktiivin taloudellista merkityst� ei saa aliarvioida. Sit� paitsi tutkimukset ovat osoittaneet T&amp;K-menojen, patenttihakemusten ja tuottavuuden v�lisen yhteyden. Teollis- ja tekij�noikeuksien suojelun avulla luodaan ty�paikkoja ja varmistetaan niiden s�ilyminen Euroopassa sek� saadaan lis�� tuloja. (JURI).
rec17	fi	(JURI).
art2a	es	Este considerando restablece la ley tal y como est� recogida en el apartado 1 del art�culo 52 del Convenio sobre la Patente Europea.
art2b	es	Este considerando deja claro que no basta con especificar que debe usarse un ordenador (u otros medios t�cnicos) para que una invenci�n implementada en ordenador sea patentable. La invenci�n en su conjunto debe hacer una aportaci�n t�cnica, no basta con el mero tratamiento de datos.
art2bb	es	
art3a	es	
art4b	es	
art7	es	
rec18	fi	Rajoittamaton ohjelmien patenttisuoja voisi tehd� patenttioikeuden mukaan laittomaksi ohjelmien kehitt�jien soveltaman k��nteisen suunnittelutavan noudattamisen yhteentoimivuuden saavuttamiseksi, mik� on nyky��n sallittua ohjelmien tekij�noikeuksista annetun direktiivin poikkeusten mukaan. Siksi tulevaan ohjelmien patentteja koskevaan EU:n lains��d�nt��n olisi sis�llytett�v� nimenomainen poikkeus patenttioikeuksista, jotta varmistettaisiin se, ett� ohjelmien kehitt�j�t voivat patenttioikeuden mukaisesti toteuttaa edelleen samoja toimia yhteentoimivuuden saavuttamiseksi kuin tekij�noikeuslain rajoitukset sallivat nyky��n.<P>Neuvoston 8. marraskuuta 2002 esitt�m�� yhteist� menettely� tuetaan ja selvennet��n viittaamalla direktiivin 91/250/ETY 5 ja 6 artiklaan. (JURI).
art3	fi	Ehdotuksen sanamuoto ei mitenk��n mahdollista keskustelua v�itetyn keksinn�n teknisest� luonteesta. Asia on n�ytett�v� toteen, eik� sit� voida pit�� itsest��n selv�n�. (ITRE).
art4_1	fi	(JURI).
art4_2	fi	(JURI).
art4_3	fi	
art4_3a	fi	
art4a	fi	T�ss� artiklassa samoin kuin siihen liittyv�ss� johdanto-osan kappaleessa selvennet��n, ett� keksint�� ei voida patentoida pelk�st��n siit� syyst�, ett� sen tekemiseen on k�ytetty teknist� v�linett�. Sen t�ytyy tuoda lis�ys tekniikan tasoon. Artiklassa tehd��n my�s selv�ksi, ett� liiketoiminnassa hy�dynnett�v� tietokonesovellus ei sellaisenaan ole patentoitava keksint�. (JURI).
art5_1	fi	(ITRE).
art5_1b	fi	(ITRE).
art5_1c	fi	(ITRE).
art5_1d	fi	(ITRE).
art6	fi	Rajoittamaton ohjelmien patenttisuoja voisi tehd� patenttioikeuden mukaan laittomaksi ohjelmien kehitt�jien soveltaman k��nteisen suunnittelutavan noudattamisen yhteentoimivuuden saavuttamiseksi, mik� on nyky��n sallittua ohjelmien tekij�noikeuksista annetun direktiivin poikkeusten mukaan. Siksi tulevaan ohjelmien patentteja koskevaan EU:n lains��d�nt��n olisi sis�llytett�v� nimenomainen poikkeus patenttioikeuksista, jotta varmistettaisiin se, ett� ohjelmien kehitt�j�t voivat patenttioikeuden mukaisesti toteuttaa edelleen samoja toimia yhteentoimivuuden saavuttamiseksi kuin tekij�noikeuslain rajoitukset sallivat nyky��n.<P>Neuvoston 8. marraskuuta 2002 esitt�m�� yhteist� menettely� tuetaan ja selvennet��n viittaamalla direktiivin 91/250/ETY 5 ja 6 artiklaan. (JURI).
art6a	fi	Mahdollisuus kytke� laitteistoja toisiinsa niiden yhteentoimivuuden aikaansaamiseksi on keino varmistaa verkkojen avoimuus ja v�ltt�� m��r��v�n markkina-aseman v��rink�ytt�. T�st� on esimerkkej� Euroopan yhteis�jen tuomioistuimen oikeusk�yt�nn�ss�. Patenttilakien ei pid� antaa mahdollisuutta t�m�n periaatteen rikkomiseen vapaan kilpailun ja k�ytt�jien kustannuksella. (ITRE).
art8a	fi	
art8b	fi	
art8ca	fi	On oltu huolissaan t�m�n direktiivin vaikutuksesta direktiiviss� 91/250/ETY s��dettyyn tietokoneohjelmien oikeudelliseen suojaan ja yhteentoimivuutta koskevien poikkeuss��nn�sten soveltamiseen. T�m�n s��nn�ksen nojalla komissio voi seurata t�t� alaa koskevia mahdollisia patenttij�rjestelm�n v��rink�yt�ksi�. (JURI).
art8cb	fi	(JURI).
art8cc	fi	
art8cd	fi	
art8cf	fi	
art8cg	fi	
art8_1a	fi	
art9_1	fi	On v�ltt�m�t�nt� ilmoittaa t�sm�llisesti, mihin menness� direktiivin edellytt�mien lakien, asetusten ja hallinnollisten m��r�ysten on tultava voimaan. (JURI).
rec1	fr	Les investissements ne se fondent pas seulement sur une protection effective et harmonis�e, mais aussi sur la transparence. (JURI).
rec5	fr	L'objet de toute l�gislation sur la brevetabilit� n'est pas d'assurer un avantage aux titulaires de brevets
rec7a	fr	La Convention sur le brevet europ�en est un instrument international qui ne peut �tre modifi� que par les m�canismes pr�vus dans ladite Convention. (JURI).
rec7b	fr	Se justifie de lui-m�me. (ITRE).
rec13b	fr	Ce consid�rant est destin� � garantir que l'exigence d'une activit� inventive, et donc d'une contribution technique, ne puisse �tre contourn�e par une formulation ing�nieuse de la revendication de brevet. (JURI).
rec13c	fr	L'article 52, paragraphe 2, points a) et c), de la Convention sur le brevet europ�en exclut de la brevetabilit� les "m�thodes math�matiques" et les "plans, principes et m�thodes dans l'exercice d'activit�s intellectuelles, en mati�re de jeu ou dans le domaine des activit�s �conomiques, ainsi que les programmes d'ordinateurs". �tant donn� qu'un algorithme pourrait �tre un programme d'ordinateur ou un �l�ment d'un tel programme isol�ment de son environnement d'ex�cution, ou une formule ou m�thode math�matique, il est exclu de la brevetabilit� en tant que tel. Toutefois, la simple utilisation d'un algorithme n'emp�che pas la brevetabilit�. (JURI).
rec13d	fr	
rec16	fr	Il convient de ne pas sous-estimer l'importance �conomique de la pr�sente directive. En outre, des �tudes ont fait appara�tre un lien entre les d�penses en recherche et d�veloppement, les demandes de brevet et la productivit�. La protection de la propri�t� intellectuelle cr�e et assure des emplois en Europe et g�n�re des revenus. (JURI).
rec17	fr	Les consid�rants ne doivent pas �tre r�dig�s comme des dispositions normatives. (JURI).
art2a	fi	Johdanto-osan kappale on saatettu Euroopan patenttisopimuksen 52 artiklan 1 kohdan mukaiseksi.
art2b	fi	Johdanto-osan kappaleessa selvennet��n, ett� pelk�st��n tietokoneen (ts. teknisen v�lineen) k�ytt� ei tee tietokoneella toteutettua keksint�� patentoitavaksi. Keksinn�n on oltava kokonaisuudessaan lis�ys tekniikan tasoon. Tavallinen tietojenk�sittely ei ole siit� riitt�v� osoitus.
rec18	fr	Une protection par brevet illimit�e pour les logiciels pourrait rendre ill�gales en vertu du droit des brevets les pratiques d'ing�nierie inverse utilis�es par les concepteurs de logiciels pour obtenir l'interop�rabilit�, qui sont actuellement autoris�es dans le cadre des exceptions pr�vues par la directive sur le droit d'auteur des logiciels. Par cons�quent, la future l�gislation de l'UE relative aux brevets de logiciels doit comporter une exception explicite aux droits de brevet, afin de garantir que les concepteurs de logiciels puissent effectuer, dans le cadre du droit des brevets, les m�mes actes qui leur sont autoris�s aujourd'hui dans les limites de la l�gislation sur le droit d'auteur.<P>L'approche commune du Conseil du 8 novembre 2002 est soutenue et clarifi�e par une r�f�rence aux articles 5 et 6 de la directive 91/250/CEE. (JURI).
art3	fr	La formulation de la proposition ne permet pas d'examiner le caract�re technique d'une invention revendiqu�e. Cette condition doit �tre prouv�e, et non consid�r�e comme remplie d'office. (ITRE).
art4_1	fr	(JURI).
art4_2	fr	(JURI).
art4_3	fr	
art4_3a	fr	
art4a	fr	Cette disposition, conjugu�e avec le consid�rant qui lui correspond, pr�cise clairement que la simple sp�cification de moyens techniques ne suffit pas � assurer la brevetabilit�. Une contribution technique doit �tre apport�e. Elle pr�cise aussi que la mise en oeuvre par ordinateur d'une m�thode destin�e � l'exercice d'activit�s �conomiques ne constitue pas en soi une invention brevetable. (JURI).
art5_1	fr	(ITRE).
art5_1b	fr	(ITRE).
art5_1c	fr	(ITRE).
art5_1d	fr	(ITRE).
art6	fr	Une protection par brevet illimit�e pour les logiciels pourrait rendre ill�gales en vertu du droit des brevets les pratiques d'ing�nierie inverse utilis�es par les concepteurs de logiciels pour obtenir l'interop�rabilit�, qui sont actuellement autoris�es dans le cadre des exceptions pr�vues par la directive sur le droit d'auteur des logiciels. Par cons�quent, la future l�gislation de l'UE relative aux brevets de logiciels doit comporter une exception explicite aux droits de brevet, afin de garantir que les concepteurs de logiciels puissent effectuer, dans le cadre du droit des brevets, les m�mes actes qui leur sont autoris�s aujourd'hui dans les limites de la l�gislation sur le droit d'auteur.<P>L'approche commune du Conseil du 8 novembre 2002 est soutenue et clarifi�e par une r�f�rence aux articles 5 et 6 de la directive 91/250/CEE. (JURI).
art6a	fr	La possibilit� de connecter des �quipements pour les rendre interop�rables est une fa�on de garantir des r�seaux ouverts et d'�viter les abus de position dominante. Ceci a �t� sp�cifi� en particulier dans la jurisprudence de la Cour de justice des Communaut�s europ�ennes. Le droit des brevets ne devrait pas permettre de violer ce principe, au pr�judice de la libre concurrence et des utilisateurs. (ITRE).
art8a	fr	
art8b	fr	
art8ca	fr	(JURI).
art8cb	fr	(JURI).
art8cc	fr	
art8cd	fr	
art8cf	fr	
art8cg	fr	
art8_1a	fr	
art9_1	fr	Il est n�cessaire de sp�cifier la date � laquelle la directive doit entrer en vigueur. (JURI).
rec1	it	Gli investimenti non si basano soltanto su una protezione efficace ed armonizzata, bens� anche sulla trasparenza. (JURI).
rec5	it	L'obiettivo di qualsiasi normativa in materia di brevetti non � di garantire che il titolare del brevetto benefici di un vantaggio; il vantaggio che gli � concesso � solo un modo per incoraggiare il processo inventivo a beneficio della societ� nel suo complesso. I vantaggi concessi al titolare di un brevetto non devono andare contro questo obiettivo ultimo del principio dei brevetti.<P>� anche importante sottolineare che l'obiettivo soggiacente a tale direttiva � quello di garantire la certezza giuridica e l'interpretazione ed applicazione uniformi della legislazione da parte dei tribunali nazionali. La possibilit� di affiancare una camera giurisdizionale specializzata al tribunale di primo grado a norma dell'articolo 220 del trattato CE, quale emendato dal trattato di Nizza, � altres� rilevante a tal riguardo. (JURI).
rec7a	it	La Convenzione sul brevetto europeo � uno strumento internazionale che non pu� essere modificato se non mediante i meccanismi previsti nel medesimo. (JURI).
rec7b	it	L'emendamento si spiega da s�. (ITRE).
rec11	it	
rec13a	it	
rec13b	it	Tale considerando � volto a garantire che il requisito di attivit� inventiva e, pertanto, di contributo tecnico non possa essere eluso elaborando ingegnose rivendicazioni di brevetto. (JURI).
rec13c	it	L'articolo 52, paragrafo 2, lettere a) e c) della Convenzione sul brevetto europeo preclude la brevettabilit� di "metodi matematici" e "schemi, regole e metodi per eseguire atti mentali, giocare e svolgere attivit� commerciali nonch� programmi per elaboratore". Dato che un algoritmo potrebbe essere un programma per elaboratore o un elemento di tale programma, isolato dal suo ambiente di esecuzione o un metodo o formula matematica, � pertanto precluso dalla brevettabilit�. Tuttavia, la semplice utilizzazione di un algoritmo non preclude la brevettabilit�. (JURI).
rec13d	it	
rec14	it	
rec16	it	L'importanza economica di tale direttiva non deve essere sottovalutata. Inoltre, alcuni studi hanno mostrato la correlazione esistente tra le spese R&amp;S, le domande di brevetto e la produttivit�. La tutela della propriet� intellettuale crea e garantisce posti di lavoro in Europa ed � produttrice di entrate. (JURI).
rec17	it	La redazione di considerando come se fossero disposizioni normative costituisce un esempio di cattiva elaborazione. (JURI).
rec18	it	La tutela senza limiti dei brevetti per software potrebbe rivelarsi contraria alle norme nazionali in materia di brevetti; ci� potrebbe causare un turbamento delle pratiche di ingegneristica utilizzate dai creatori di software per conseguire l'interoperabilit� attualmente prevista dalle eccezioni presenti nella direttiva sui diritti d'autore del software. Le future norme comunitarie concernenti i brevetti sul software dovranno pertanto includere un'esplicita eccezione per i diritti sui brevetti al fine di garantire ai creatori di software la possibilit� di continuare ad utilizzare le pratiche attuali per conseguire l'interoperabilit� ai sensi delle norme in materia di brevetti, naturalmente entro i limiti previsti dalla normativa sui diritti d'autore.<P>Attraverso un riferimento agli articoli 5 e 6 della direttiva 91/250/CEE viene cos� appoggiata e chiarita la posizione comune del Consiglio dell'8 novembre 2002. (JURI).
art3	it	La formulazione della proposta rende semplicemente impossibile discutere il carattere tecnico di un'invenzione rivendicata. Tale condizione deve essere provata, e non data per scontata. (ITRE).
art4_1	it	(JURI).
art4_2	it	(JURI).
art4_3	it	
art4_3a	it	
art4a	it	Tale articolo, con il corrispondente considerando, chiarisce che specificare semplicemente i mezzi tecnici non � sufficiente per garantire la brevettabilit�. Vi deve essere un contributo tecnico. Si chiarisce altres� che l'applicazione elettronica di un metodo per attivit� commerciali non � di per s� un'invenzione brevettabile. (JURI).
art5_1	it	(ITRE).
art5_1b	it	(ITRE).
art5_1c	it	(ITRE).
art5_1d	it	(ITRE).
art6	it	La tutela senza limiti dei brevetti per software potrebbe rivelarsi contraria alle norme nazionali in materia di brevetti; ci� potrebbe causare un turbamento delle pratiche di ingegneristica utilizzate dai creatori di software per conseguire l'interoperabilit� attualmente prevista dalle eccezioni presenti nella direttiva sui diritti d'autore del software. Le future norme comunitarie concernenti i brevetti sul software dovranno pertanto includere un'esplicita eccezione per i diritti sui brevetti al fine di garantire ai creatori di software la possibilit� di continuare ad utilizzare le pratiche attuali per conseguire l'interoperabilit� ai sensi delle norme in materia di brevetti, naturalmente entro i limiti previsti dalla normativa sui diritti d'autore.<P>Attraverso un riferimento agli articoli 5 e 6 della direttiva 91/250/CEE viene cos� appoggiata e chiarita la posizione comune del Consiglio dell'8 novembre 2002. (JURI).
art6a	it	La possibilit� di collegare impianti cos� da renderli interoperabili � un modo per garantire reti aperte ed evitare abusi di posizione dominante. Ci� � stato espressamente riconosciuto in particolare dalla giurisprudenza della Corte di giustizia delle Comunit� europee. Il diritto dei brevetti non dovrebbe consentire che tale principio sia calpestato a scapito della libera concorrenza e degli utenti. (ITRE).
art8a	it	
art8b	it	
art8ca	it	(JURI).
art8cb	it	(JURI).
art8cc	it	
art8cd	it	
art8cf	it	
art8cg	it	
art8_1a	it	
art9_1	it	� necessario specificare la data limite per l'entrata in vigore della presente direttiva. (JURI).
rec1	nl	Investeringen berusten niet alleen op doeltreffende en geharmoniseerde bescherming maar ook op transparantie. (JURI).
rec5	nl	Het doel van een octrooiwet is niet ervoor te zorgen dat de octrooihouders een voordeel genieten
rec7a	nl	Het Europees Octrooiverdrag is een internationaal instrument dat alleen kan worden gewijzigd via de daarin opgenomen mechanismen. (JURI).
rec7b	nl	Spreekt voor zich. (ITRE).
rec11	nl	
rec13a	nl	
rec13b	nl	Deze overweging moet waarborgen dat het vereiste van uitvinderswerkzaamheid en derhalve van een technische bijdrage niet kan worden omzeild door een ingenieuze formulering van de octrooiaanvraag. (JURI).
rec13c	nl	Artikel 52, lid 2, onder a) en c) van het Europees Octrooiverdrag sluit de octrooieerbaarheid van "wiskundige methoden" en "stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, alsmede computerprogramma's" uit. Daar een algoritme een computerprogramma of een uit zijn normale gebruikssituatie ge�soleerd element van zo'n programma of een mathematische formule of methode kan zijn, is deze als zodanig uitgesloten van octrooieerbaarheid. Echter louter het gebruik van een algoritme sluit octrooieerbaarheid niet uit. (JURI).
rec13d	nl	
rec14	nl	
rec16	nl	Het economisch belang van deze richtlijn moet niet worden onderschat. Bovendien heeft onderzoek aangetoond dat er een verband bestaat tussen O&amp;O-uitgaven, octrooiaanvragen en productiviteit. Bescherming van intellectuele eigendom schept en waarborgt werkgelegenheid in Europa en levert inkomsten op. (JURI).
rec17	nl	Het is verwerpelijk om overwegingen als normatieve bepalingen te redigeren. (JURI).
art2a	it	Tale considerando reitera le norme in materia sancite all'articolo 52, paragrafo 1, della Convenzione sul brevetto europeo.
art2b	it	Tale considerando indica chiaramente che � insufficiente specificare l'utilizzazione di un elaboratore (ossia di mezzi tecnici) per rendere brevettabile un'invenzione attuata per mezzo di elaboratori elettronici. La semplice elaborazione nel suo complesso deve costituire un contributo tecnico. L'abituale elaborazione di dati non � sufficiente.
art3a	it	
rec18	nl	Onbeperkte octrooirechtelijke bescherming van software kan ervoor zorgen dat het op grond van het octrooirecht illegaal wordt zich bezig te houden met ontsleuteling ("reverse engineering"), wat softwareontwikkelaars doen om interoperabiliteit te garanderen en op dit ogenblik bij wijze van uitzondering in de richtlijn betreffende de auteursrechtelijke bescherming van software is toegestaan. Om deze reden moet toekomstige EU-regelgeving inzake softwareoctrooien een expliciete uitzondering bevatten op de octrooirechten, om te garanderen dat softwareontwikkelaars hun activiteit kunnen voortzetten, om in het kader van het octrooirecht interoperabiliteit te waarborgen, zoals zij daar vandaag het recht toe hebben in het kader van het auteursrecht.<P>Het standpunt van de Raad van 8 november 2002 wordt met de verwijzing naar de artikelen 5 en 6 van Richtlijn 91/250/EEG gesteund en verduidelijkt. (JURI).
art3	nl	De formulering van het voorstel maakt het simpelweg onmogelijk het technische karakter van een gemelde uitvinding te bespreken. Dat aan deze voorwaarde is voldaan, moet worden bewezen en kan niet zomaar worden aangenomen. (ITRE).
art4_1	nl	(JURI).
art4_2	nl	(JURI).
art4_3	nl	
art4_3a	nl	
art4a	nl	Dit, in samenhang met de corresponderende overweging, verduidelijkt dat het eenvoudigweg specificeren van technische middelen niet volstaat voor octrooieerbaarheid. Er moet sprake zijn van een technische bijdrage. Tevens wordt duidelijk geformuleerd dat de computerimplementatie van een bedrijfsmethode niet eenvoudigweg een octrooieerbare uitvinding is. (JURI).
art5_1	nl	(ITRE).
art5_1b	nl	(ITRE).
art5_1c	nl	(ITRE).
art5_1d	nl	(ITRE).
art6	nl	Onbeperkte octrooirechtelijke bescherming van software kan ervoor zorgen dat het op grond van het octrooirecht illegaal wordt zich bezig te houden met ontsleuteling ("reverse engineering"), wat softwareontwikkelaars doen om interoperabiliteit te garanderen en op dit ogenblik bij wijze van uitzondering in de richtlijn betreffende de auteursrechtelijke bescherming van software is toegestaan. Om deze reden moet toekomstige EU-regelgeving inzake softwareoctrooien een expliciete uitzondering bevatten op de octrooirechten, om te garanderen dat softwareontwikkelaars hun activiteit kunnen voortzetten, om in het kader van het octrooirecht interoperabiliteit te waarborgen, zoals zij daar vandaag het recht toe hebben in het kader van het auteursrecht.<P>Het standpunt van de Raad van 8 november 2002 wordt met de verwijzing naar de artikelen 5 en 6 van Richtlijn 91/250/EEG gesteund en verduidelijkt. (JURI).
art6a	nl	De mogelijkheid apparatuur te koppelen om interoperabiliteit tot stand te brengen, is een manier om tot open netwerken te komen en misbruik van dominante posities te voorkomen. Dit is specifiek gesteld in rechterlijke uitspraken, met name van het Hof van Justitie van de Europese Gemeenschappen. Het octrooirecht mag het niet mogelijk maken dat dit beginsel terzijde wordt geschoven ten koste van de vrije mededinging en van de gebruikers. (ITRE).
art8a	nl	
art8b	nl	
art8ca	nl	(JURI).
art8cb	nl	(JURI).
art8cc	nl	
art8cd	nl	
art8cf	nl	
art8cg	nl	
art8_1a	nl	
art9_1	nl	Het is noodzakelijk dat de datum waarop de richtlijn in werking dient te treden, wordt gespecificeerd. (JURI).
rec1	pt	Os investimentos n�o se baseiam unicamente numa protec��o eficaz e harmonizada, mas tamb�m na transpar�ncia. (JURI).
rec5	pt	O objectivo de qualquer lei relativa ao registo de patentes n�o � assegurar que os titulares de patentes beneficiem de uma vantagem
rec7a	pt	A Conven��o sobre a Patente Europeia � um instrumento internacional que n�o pode ser alterado sen�o pelos mecanismos nele previstos. (JURI).
rec7b	pt	O texto da altera��o �, por si s�, elucidativo. (ITRE).
rec13b	pt	Este considerando destina"se a garantir que a obrigatoriedade da exist�ncia de actividade inventiva e, como tal, de um contributo t�cnico, n�o possa ser contornada atrav�s da redac��o engenhosa das reivindica��es de patente. (JURI).
rec13c	pt	As al�neas a) e c) do n.º 2 do artigo 52° da Conven��o sobre a Patente Europeia excluem a patenteabilidade de "m�todos matem�ticos" e de "esquemas, normas e m�todos para executar actos mentais, realizar jogos e fazer neg�cios, bem como programas inform�ticos". Uma vez que um algoritmo pode ser um programa inform�tico, ou um elemento de um programa inform�tico, independentemente do ambiente em que corre ou de qualquer f�rmula ou m�todo matem�tico, ele est�, enquanto tal, exclu�do da patenteabilidade. Contudo, a mera utiliza��o de um algoritmo n�o exclui a patenteabilidade. (JURI).
rec13d	pt	
rec16	pt	A import�ncia econ�mica da presente directiva n�o deve ser subestimada. Para al�m disso, h� estudos que demonstram a exist�ncia de uma liga��o entre os investimentos em I &amp; D, as reivindica��es de patentes e a produtividade. A protec��o da propriedade intelectual cria e assegura empregos na Europa, trazendo novas receitas. (JURI).
rec17	pt	A elabora��o de considerandos, como se estiv�ssemos perante disposi��es de car�cter normativo, constitui um exemplo de m� redac��o. (JURI).
art2a	nl	Deze overweging herformuleert de bepaling die vervat is in artikel 52, lid 1, van het Europees Octrooiverdrag.
art2b	nl	Deze overweging maakt duidelijk dat specificatie van het gebruik van een computer (m.a.w. een technisch middel) niet volstaat om een in computers ge�mplementeerde uitvinding octrooieerbaar te maken. De uitvinding als zodanig moet een technische bijdrage leveren. Gewone dataverwerking is niet voldoende.
rec18	pt	A protec��o ilimitada dos programas de computador pelo direito de patente poderia tornar ilegais as pr�ticas de descompila��o usadas pelos profissionais de desenvolvimento de programas inform�ticos para conseguir a interoperabilidade, pr�ticas essas actualmente autorizadas ao abrigo das excep��es previstas na directiva relativa � protec��o jur�dica dos programas de computador. Assim, a futura legisla��o da UE sobre as patentes de programas inform�ticos deve conter uma excep��o expl�cita para autorizar em sede de direitos de patente os profissionais de desenvolvimento de programas inform�ticos a praticar os mesmos actos para conseguir a interoperabilidade que est�o autorizados a praticar actualmente em sede de direitos de autor.<P>A abordagem comum do Conselho de 8 de Novembro de 2002 � aqui apoiada e clarificada pela remiss�o para os artigos 5º e 6º da Directiva 91/250/CEE. (JURI).
art3	pt	A redac��o da proposta torna completamente imposs�vel discutir a natureza t�cnica de um alegado invento. Esta condi��o tem de ser provada, e n�o tomada como certa. (ITRE).
art4_1	pt	(JURI).
art4_2	pt	(JURI).
art4_3	pt	
art4_3a	pt	
art4a	pt	Este artigo, conjuntamente com o considerando que lhe corresponde, esclarece que a simples especifica��o de meios t�cnicos n�o � suficiente para garantir a patenteabilidade. Tem obrigatoriamente de haver uma contribui��o t�cnica. Ele deixa tamb�m claro que a utiliza��o num computador de um procedimento de natureza comercial n�o constitui, por si s�, um invento patente�vel. (JURI).
art5_1	pt	(ITRE).
art5_1b	pt	(ITRE).
art5_1c	pt	(ITRE).
art5_1d	pt	(ITRE).
art6	pt	A protec��o ilimitada dos programas de computador pelo direito de patente poderia tornar ilegais as pr�ticas de descompila��o usadas pelos profissionais de desenvolvimento de programas inform�ticos para conseguir a interoperabilidade, pr�ticas essas actualmente autorizadas ao abrigo das excep��es previstas na directiva relativa � protec��o jur�dica dos programas de computador. Assim, a futura legisla��o da UE sobre as patentes de programas inform�ticos deve conter uma excep��o expl�cita para autorizar em sede de direitos de patente os profissionais de desenvolvimento de programas inform�ticos a praticar os mesmos actos para conseguir a interoperabilidade que est�o autorizados a praticar actualmente em sede de direitos de autor.<P>A abordagem comum do Conselho de 8 de Novembro de 2002 � aqui apoiada e clarificada pela remiss�o para os artigos 5º e 6º da Directiva 91/250/CEE. (JURI).
art6a	pt	A possibilidade de ligar equipamentos com vista a torn�-los interoper�veis � uma forma de assegurar as redes abertas e de evitar o abuso de posi��es dominantes. Esta possibilidade foi prevista, nomeadamente, na jurisprud�ncia do Tribunal de Justi�a das Comunidades Europeias. A legisla��o em mat�ria de patentes n�o deve permitir que este princ�pio seja violado em detrimento da livre concorr�ncia e dos utilizadores. (ITRE).
art8a	pt	
art8b	pt	
art8ca	pt	(JURI).
art8cb	pt	(JURI).
art8cc	pt	
art8cd	pt	
art8cf	pt	
art8cg	pt	
art8_1a	pt	
art9_1	pt	� indispens�vel que se especifique a data"limite para a entrada em vigor da presente Directiva. (JURI).
rec1	sv	Investeringar �r inte bara beroende av ett verksamt och harmoniserat skydd, utan ocks� av �ppenhet. (JURI).
rec5	sv	Syftet med patentr�tten �r inte att ge patentinnehavaren en f�rdel
rec7a	sv	Europeiska patentkonventionen �r ett internationellt instrument som endast f�r �ndras genom de mekanismer som fastst�lls i densamma. (JURI).
rec7b	sv	Ingen motivering beh�vs. (ITRE).
rec11	sv	
rec13a	sv	
rec13b	sv	Avsikten med sk�let �r att se till att kravet p� uppfinningsh�jd och d�rmed tekniskt bidrag inte kan kringg�s med hj�lp av ett v�lformulerat patentkrav. (JURI).
rec13c	sv	I artikel 52.2 a och c i Europeiska patentkonventionen utesluts patentering av "matematiska metoder" och "planer, regler eller metoder f�r intellektuell verksamhet, f�r spel eller f�r aff�rsverksamhet samt datorprogram". Eftersom en algoritm skulle kunna vara ett datorprogram eller en del av ett s�dant program isolerat fr�n den milj� d�r det genomf�rs eller en matematisk formel eller metod kan den i sig inte patenteras. Enbart det faktum att man anv�nder sig av en algoritm utesluter emellertid inte patenterbarhet. (JURI).
rec13d	sv	
rec14	sv	
rec16	sv	Direktivets ekonomiska betydelse b�r inte underskattas. Unders�kningar har visat att det finns ett samband mellan utgifterna f�r forskning och utveckling, patentans�kningar och produktivitet. Skydd av immateriella r�ttigheter skapar och garanterar arbetstillf�llen i Europa och ger inkomster. (JURI).
rec17	sv	Sk�l skall inte formuleras som normativa best�mmelser. (JURI).
rec18	sv	Om det finns ett obegr�nsat patentskydd f�r mjukvaror kan det bli olagligt enligt patentr�tten att �gna sig �t bakl�ngeskonstruktioner ("reverse engineering"). Dessa konstruktioner, som mjukvaruutvecklarna anv�nder f�r att uppn� samverkansf�rm�ga, �r f�r n�rvarande till�tna enligt undantagen i direktivet om upphovsr�tt till mjukvara. D�rf�r �r det n�dv�ndigt att framtida EU-lagstiftning om mjukvarupatent inneh�ller ett uttryckligt undantag fr�n patentr�ttigheterna, s� att man ser till att mjukvaruutvecklare kan forts�tta att �gna sig �t samma handlingar f�r att uppn� samverkansf�rm�ga enligt patentr�tten, vilket de idag har r�tt att g�ra inom ramen f�r lagstiftningen om upphovsr�tt.<P>R�dets gemensamma strategi av den 8 november 2002 st�ds och tydligg�rs genom en h�nvisning till artiklarna 5 och 6 i direktiv 91/250/EEG. (JURI).
art3	sv	Enligt det nuvarande f�rslaget �r det inte m�jligt att diskutera en uppfinnings tekniska k�nnetecken. Dessa k�nnetecken m�ste bevisas och kan inte tas f�r givet. (ITRE).
art4_1	sv	(JURI).
art4_2	sv	(JURI).
art4_3	sv	
art4_3a	sv	
art4a	sv	Den h�r artikeln och det d�rtill h�rande sk�let klarg�r att en produkt inte kan patenteras bara f�r att man specificerar de tekniska medlen. Det m�ste ocks� finnas ett tekniskt bidrag. Artikeln klarg�r ocks� att en datatill�mpning av en aff�rsmetod helt enkelt inte �r en patenterbar uppfinning. (JURI).
art5_1	sv	(ITRE).
art5_1b	sv	(ITRE).
art5_1c	sv	(ITRE).
art5_1d	sv	(ITRE).
art6	sv	Om det finns ett obegr�nsat patentskydd f�r mjukvaror kan det bli olagligt enligt patentr�tten att �gna sig �t bakl�ngeskonstruktioner ("reverse engineering"). Dessa konstruktioner, som mjukvaruutvecklarna anv�nder f�r att uppn� samverkansf�rm�ga, �r f�r n�rvarande till�tna enligt undantagen i direktivet om upphovsr�tt till mjukvara. D�rf�r �r det n�dv�ndigt att framtida EU-lagstiftning om mjukvarupatent inneh�ller ett uttryckligt undantag fr�n patentr�ttigheterna, s� att man ser till att mjukvaruutvecklare inom ramen f�r patentr�tten kan forts�tta att �gna sig �t de handlingar f�r att uppn� samverkansf�rm�ga som idag �r till�tna enligt lagstiftningen om upphovsr�tt.<P>R�dets gemensamma strategi av den 8 november 2002 st�ds och tydligg�rs genom en h�nvisning till artiklarna 5 och 6 i direktiv 91/250/EEG. (JURI).
art6a	sv	M�jligheten att sammankoppla anordningar f�r att g�ra dem kompatibla �r ett s�tt att garantera �ppna n�tverk och f�rebygga missbruk av dominerande st�llning. Detta har uttryckligen fastslagits i EG-domstolens r�ttspraxis. Patentr�tten b�r inte g�ra det m�jligt att �sidos�tta denna princip p� den fria konkurrensens och anv�ndarnas bekostnad. (ITRE).
art8a	sv	
art8b	sv	
art8ca	sv	Man �r orolig �ver hur direktivet inverkar p� det upphovsr�ttsliga skyddet f�r mjukvara och undantagen r�rande samverkansf�rm�ga i direktiv 91/250/EG. Den h�r best�mmelsen skulle ocks� g�ra det m�jligt f�r kommissionen att �vervaka missbruk av patentsystemet p� det h�r omr�det. (JURI).
art8cb	sv	Det har framf�rts kraftiga argument f�r att det beh�vs en "skonfrist" (grace period) f�r att undvika att en uppfinnare fr�ntas sin uppfinning om han/hon offentliggjort den innan han/hon ans�kt om patent, n�got som uppfinnare ofta g�r f�r att testa marknadens intresse f�r uppfinningen. Det b�r understrykas att detta skulle vara till s�rskild nytta f�r innovativa sm� och medelstora f�retag och f�r samarbetet mellan universitet och n�ringsliv. Men en s�dan nyhet kan inte inf�ras enbart f�r datorrelaterade uppfinningar innan man studerat effekten av den samt dess f�renlighet med gemenskapens internationella �taganden, exempelvis i samband med TRIPS-avtalet. (JURI).
art8cc	sv	
art8cd	sv	
art8cf	sv	
art8cg	sv	
art8_1a	sv	
art9_1	sv	Det �r n�dv�ndigt att specificera n�r direktivet b�r tr�da i kraft. (JURI).
rec6	da	Den Europ�iske Patentkonvention er et internationalt instrument, der kun kan �ndres ved hj�lp af de mekanismer, der er fastsat i konventionen selv.
art2a	da	Denne betragtning repeterer g�ldende ret, s�ledes som denne fremg�r af artikel 52, stk. 1, i Den Europ�iske Patentkonvention.
art2ba	da	Det er vigtigt at g�re det klart, at ikke alle computer- implementerede opfindelser n�dvendigvis er patenterbare. Computer-implementerede opfindelser b�r imidlertid ikke udelukkes fra patenterbarhed med den ene Begrundelse, at de vedr�rer anvendelsen af et edb-program. Ved at understrege, at en patenterbar computer-implementeret opfindelse, selv om den henh�rer under et teknologisk omr�de, skal yde et bidrag til det aktuelle tekniske niveau, og ved at henlede opm�rksomheden p� den "problem og l�sning"-metode, som anvendes af patentbehandlerne ved Det Europ�iske Patentmyndighed, n�r de skal vurdere, om der er tale om opfindelsesh�jde, tages der sigte p� at undg�, at opfindsomme, men ikke-tekniske metoder (herunder forretningsmetoder) anses for at udg�re et bidrag af teknisk karakter og hermed for at v�re patenterbare, blot fordi de implementeres p� en computer.
art2b	da	Denne betragtning g�r det klart, at det ikke er tilstr�kkeligt at anf�re anvendelse af en computer (dvs. af tekniske midler) for at g�re en computer-implementeret opfindelse patenterbar. Opfindelsen som helhed skal udg�re et teknisk bidrag. Almindelig edb-behandling er ikke tilstr�kkelig.
rec5a	da	I henhold til artikel 52, stk. 2, litra a) og c), i Den Europ�iske Patentkonvention kan "matematiske metoder" og "planer, regler og metoder for intellektuel virksomhed, for spil eller for erhvervsvirksomhed samt edb-programmer" ikke patenteres. Eftersom en algoritme kunne v�re et edb-program eller et element i et s�dant program uafh�ngigt af k�rselsmilj�et eller en matematisk formel eller metode, er den som s�dan ikke patenterbar. Den blotte anvendelse af en algoritme udelukker imidlertid ikke patenterbarhed.
rec7	da	Den �konomiske betydning af dette direktiv b�r ikke undervurderes. Desuden har unders�gelser vist, at der er en sammenh�ng mellem F&U-udgifter, patentans�gninger og produktivitet. Beskyttelse af intellektuel ejendomsret skaber og sikrer job i Europa og bringer indt�gter.
rec13	da	Det er absurd og strider mod m�let om tydeligg�relse at fors�ge at foretage en kunstig sondren mellem matematiske algoritmer og ikke-matematiske algoritmer. For en computerekspert giver dette ikke nogen mening, eftersom enhver algoritme er s� matematisk, som noget kan v�re. En algoritme er et abstrakt koncept, som ikke har noget at g�re med universets fysiske love.
art2bb	da	Ordet "industri" benyttes i dag ofte i udvidede betydninger, som ikke er hensigtsm�ssige i patentlovsm�ssig sammenh�ng.
art3a	da	Medlemsstaterne skal sikre, at databehandling ikke betragtes som et   teknologisk   omr�de  i  patentretlig   forstand,   og   at innovationer   inden  for  databehandling  ikke  betragtes   som opfindelser i patentretlig forstand.
art2a	sv	Detta sk�l �terspeglar g�llande r�tt, s�som den framg�r av artikel 52.1 i Europeiska patentkonventionen
art7	sv	
art4b	da	Dette �ndringsforslag afspejler den nuv�rende retspraksis i Tyskland. If�lge den tyske patentdomstol (BPatG, dom af 26. marts 2002, 17 W (pat) 69/98) henviser sags�ger til, at den afg�rende indikation p� metodens teknicitet er, at den er baseret p� et teknisk problem. Da den foresl�ede metode ikke kr�ver et leksikon, kan hukommelsen hertil lagres. [...] For s� vidt ang�r det tekniske problem kan dette kun betragtes som en indikation, men ikke som et bevis p� processens teknicitet. Hvis computerimplementeringer af ikke-tekniske processer fik tildelt en teknisk dimension, blot fordi de udviser forskellige specifikke karakteristika, eksempelvis mindre bearbejdningstid eller mindre hukommelse, ville dette medf�re, at enhver computerimplementering skulle betragtes som v�rende af teknisk karakter. �rsagen hertil er, at enhver separat proces vil resultere i separate implementeringskarakteristika, som muligg�r en besparelse med hensyn til enten bearbejdningstid eller brug af hukommelse. Disse egenskaber er, i det mindste hvad ang�r denne sag, ikke baseret p� en teknisk ydelse, men er et resultat af den valgte ikke-tekniske metode. Hvis det faktum, at et s�dant problem l�ses, kunne udg�re en tilstr�kkelig begrundelse for at tildele computerimplementeringen en teknisk dimension, skulle enhver implementering af en ikke-teknisk metode patenteres, men dette ville imidlertid v�re i modstrid med patentdomstolens konklusioner om, at en juridisk afg�relse om, at edb-programmer ikke skal patenteres, ikke g�r det muligt for os at vedtage en fremgangsm�de, der ville kr�ve patentering af enhver form for undervisning inden for rammerne af computerorienterede instruktioner.
art7	da	Kr�ver ingen n�rmere forklaring.
art5_1a	da	Patentkrav p� et edb-program som s�dant, som anvendes i forbindelse med  patentretligt beskyttede computerimplementerede opfindelser, eller p� et s�dant program p� et b�remedium kan ikke antages. Et s�kaldt "tekstkrav" p�  computerprogrammer i kilde- eller bin�rkode �ger retsusikkerheden, da alene ophavsretten til et udtryk eller et b�remedium med et beskyttet program ville betyde en kr�nkelse af patentretten. Eksistensen af  edb- programmer i tekstform kan betragtes som bidrag til oplysningspligten for edb-programmers funktionaliteter, for hvilke der er meddelt patent.
rec18a	da	Computer-implementerede opfindelsers patenterbarhed skal respektere patentrettens almindelige principper.
art8ce	da	Da patenterbarhedskriterier kan have en enorm indvirkning p� hele erhvervssektorer og derfor p� �konomien og samfundet som helhed, er det yderst vigtigt at f� dem fastlagt under parlamentarisk kontrol.<P>�ndringsforslaget er en sammenfatning af Kulturudvalgets �ndringsforslag 35 og Industriudvalgets �ndringsforslag 44.
art2ba	de	Es ist wichtig klarzustellen, dass nicht alle computerimplementierten Erfindungen automatisch patentierbar sind. Allerdings sollten computerimplementierte Erfindungen auch nicht von der Patentierbarkeit ausgeschlossen werden, nur weil sie die Nutzung eines Computerprogramms erfordern. Durch die Hervorhebung der Tatsache, dass auch eine patentierbare computerimplementierte Erfindung einen technischen Beitrag zum Stand der Technik leisten muss, obwohl sie zu einem Gebiet der Technik geh�rt, und durch den Hinweis auf den Ansatz Problem/L�sung, der von den Patentpr�fern des Europ�ischen Patentamts bei der Pr�fung der erfinderischen T�tigkeit verfolgt wird, soll vermieden werden, dass erfinderische, aber nichttechnische Verfahren (einschlie�lich Gesch�ftsmethoden) als technischer Beitrag gewertet und somit als patentierbar angesehen werden, nur weil sie in einem Computer implementiert werden.
art2b	de	Durch diese Erw�gung wird klargestellt, dass es nicht ausreicht, dass die Benutzung eines Computers (d.h. eines technischen Hilfsmittels) erforderlich ist, um eine computerimplementierte Erfindung patentierbar zu machen. Die Erfindung insgesamt muss einen technischen Beitrag leisten. Normale Datenverarbeitung gen�gt nicht.
rec5a	de	Nach Artikel 52 Absatz 2 Buchstaben a und c des Europ�ischen Patent�bereinkommens sind von der Patentierbarkeit ausgeschlossen: "Mathematische Methoden" und "Pl�ne, Regeln und Verfahren f�r gedankliche T�tigkeiten f�r Spiele oder f�r gesch�ftliche T�tigkeiten sowie Programme f�r Datenverarbeitungsanlagen". Da ein Algorithmus ein Computerprogramm oder ein Teil eines solchen Programms, unabh�ngig davon, in welchem Umfeld es angewendet wird, oder eine mathematische Formel oder Methode sein kann, ist er als solcher von der Patentierbarkeit ausgeschlossen. Allerdings schlie�t allein die Verwendung eines Algorithmus die Patentierbarkeit nicht aus.
rec7	de	Die wirtschaftliche Bedeutung dieser Richtlinie sollte nicht untersch�tzt werden. Au�erdem haben Untersuchungen ergeben, dass ein Zusammenhang zwischen Ausgaben f�r Forschung und Entwicklung, Patentanmeldungen und Produktivit�t besteht. Urheberrechtsschutz schafft und sichert Arbeitspl�tze in Europa und f�hrt zu Ertr�gen.
art3a	de	
art4b	de	
art7	de	
art5_1a	de	Ein Patentanspruch auf ein im Rahmen von patentrechtlich gesch�tzten computerimplementierten Erfindungen verwendetes Computerprogramm als solches oder auf seine Festlegung auf einem Datentr�ger nicht zul�ssig. Ein sogenannter "Textanspruch" auf Computerprogramme im Quell- oder Bin�rcode verst�rkt die Rechtsunsicherheit, da schon der Besitz eines Ausdruckes oder eines Datentr�gers mit einem gesch�tzten Programm eine Patentrechtsverletzung bedeuten w�rde. Das Vorhandensein eines textuell gefassten Programms kann als Beitrag zur Offenbarungspflicht f�r Funktionalit�ten von Computerprogrammen gesehen werden, f�r die Patentanspr�che erteilt worden sind.
rec18a	de	
art8ce	de	
rec12	de	
rec13	de	
art2bb	de	
rec6	en	The European Patent Convention is an international instrument which can be amended only by the mechanisms provided for in the convention itself.
art2a	en	This recital restates the law, as enshrined in Article 52(1) of the European Patent Convention.
art2ba	en	It is important to clarify that not all computer-implemented inventions are necessarily patentable.  However, computer- implemented inventions should not be excluded from patentability on the sole ground that they specify the use of a computer program. By stressing the fact that a patentable computer- implemented invention, albeit belonging to a field of technology, must make a technical contribution to the state of the art and by drawing attention to the problem and solution approach used by the patent examiners at the European Patent Office in assessing inventive step, it is intended to avoid allowing inventive but non-technical methods (including business methods) to be regarded as making a technical contribution and hence as patentable merely because they are implemented on a computer.
art2b	en	This recital makes it clear that it is not enough to specify the use of a computer (i.e. of technical means) to make a computer- implemented invention patentable.  The invention as a whole must make a technical contribution.  Ordinary data processing is not enough.
rec5a	en	Article 52(2)(a) and (c)  of the European Patent Convention precludes the patentability of "mathematical methods" and "schemes, rules and methods for performing mental acts, playing games and doing business, and programs for computers". Since an algorithm could be a computer program or an element of such a program in isolation from its execution environment or a mathematical formula or method, it is, as such,  precluded from patentablity.  However, the mere use of an algorithm does not preclude patentability.
rec7	en	The economic importance of this Directive should not be underestimated.  Moreover, studies have shown a link between R&D spending, patent applications and productivity. Intellectual property protection creates and secures jobs in Europe and brings in revenue.
rec12	en	It is bad draftsmanship to couch recitals as normative provisions.
rec13	en	It is absurd and contrary to the goal of clarification to try to make an artificial distinction between mathematical algorithms and nonmathematical algorithms. To a computer scientist, this makes no sense, because every algorithm is as mathematical as anything could be. An algorithm is an abstract concept unrelated to physical laws of the universe.
art2bb	en	The word "industry" is nowadays often used in extended meanings which are not appropriate in the context of patent law.
art3a	en	Member   states  shall  ensure  that  data  processing  is   not considered  to be a field of technology in the sense  of  patent law,  and  that innovations in the field of data processing  are not considered to be inventions in the sense of patent law.
art4b	en	This amendment reflects current caselaw in Germany. In the words of the justices of the German Federal Patent Court (BPatG, decision of 26. March 2002, 17 W (pat) 69/98,The applicant sees as a decisive indication of technicity of the method that it is based on a technical problem. Because the proposed method does not need a dictionary, the memory space for this can be saved. [...] As far as the technical problem is concerned, this can only be considered as an indication but not as a proof of technicity of the process. If computer implementations of non- technical processes were attributed a technical character merely because they display different specific characteristics, such as needing less computing time or less storage space, the consequence of this would be that any computer implementation would have to be deemed to be of technical character. This is because any distinct process will have distinct implementation characteristics, that allow it to either save computing time or save storage space. These properties are, at least in the present case, not based on a technical achievement but result from the chosen non-technical method. If the fact that such a problem is solved could be a sufficient reason for attributing a technical character to a computer implementation, then every implementation of a non-technical method would have to be patentable; this however would run against the conclusion of the Federal Court of Justice that the legal exclusion of computer programs from patentability does not allow us to adopt an approach which would make any teaching that is framed in computer-oriented instructions patentable.
art5_1a	en	
rec18a	en	
art8ce	en	
rec11	en	
rec13a	en	
rec14	en	
rec6	es	El Convenio de la Patente Europea es un instrumento internacional que no puede ser modificado m�s que por los mecanismos previstos en �l.
art2ba	es	Es importante aclarar que no todas las invenciones implementadas en ordenador son necesariamente patentables. No obstante, las invenciones implementadas en ordenador no deben excluirse de la patentabilidad por la mera raz�n de que especifiquen el uso de un programa inform�tico. Insistiendo en el hecho de que, aunque s� pertenece a un �mbito de la tecnolog�a, una invenci�n patentable implementada en ordenador debe aportar una contribuci�n t�cnica al estado de la t�cnica y llamando la atenci�n sobre el enfoque de problema aplicado por los examinadores de la Oficina Europea de Patentes para evaluar la actividad inventiva, se pretende evitar que pueda considerarse que, simplemente por implementarse en ordenador, m�todos inventivos, pero no t�cnicos (incluyendo m�todos comerciales), hacen una aportaci�n t�cnica y son, por consiguiente, patentables.
rec5a	es	Las letras a) y c) del art�culo 52 del Convenio sobre la Patente Europea excluyen la patentabilidad de "m�todos matem�ticos" y "planes, principios y m�todos para el ejercicio de actividades intelectuales, para juegos o para actividades econ�micas, as� como los programas de ordenadores". Dado que un algoritmo puede ser un programa inform�tico o un elemento de un programa aislado de su entorno de ejecuci�n o una f�rmula o m�todo matem�tico, est� excluido en cuanto tal de la patentabilidad. Sin embargo, el mero uso de un algoritmo no excluye la patentabilidad.
rec7	es	No deber�a subestimarse la importancia econ�mica de la presente Directiva. Adem�s, hay estudios que muestran la relaci�n entre el gasto en I+D, las solicitudes de patente y la productividad. La protecci�n de la propiedad intelectual crea y asegura puestos de trabajo en Europa y produce ingresos.
rec12	es	Denota mala t�cnica redactar los considerandos como disposiciones normativas.
rec13	es	
art5_1a	es	
rec18a	es	La patentabilidad de las invenciones implementadas por ordenador debe respetar los principios que son propios del Derecho de patentes.
art8ce	es	
rec6	fi	Euroopan patenttiyleissopimus on kansainv�linen v�line, jota ei voida muuttaa muuten kuin siin� m��r�tyill� mekanismeilla.
art2ba	fi	On t�rke�� selvent��, ett� kaikki tietokoneella toteutetut keksinn�t eiv�t v�ltt�m�tt� ole patentoitavissa. Tietokoneella toteutetuilta keksinn�ilt� ei saa kuitenkaan ev�t� patenttia vain sill� perusteella, ett� niiss� mainitaan jonkin tietokoneohjelman k�ytt�. Korostamalla sit�, ett� vaikka patentoitava tietokoneella toteutettu keksint� kuuluu tekniikan alaan, sen on tuotava tekninen lis�ys vallitsevan tekniikan tasoon, ja kiinnitt�m�ll� huomiota ongelma ja ratkaisu - l�hestymistapaan, jota Euroopan patenttiviraston tarkastajat k�ytt�v�t arvioidessaan keksinn�llisyytt�, pyrit��n v�ltt�m��n sit�, ett� keksinn�llisi� ei-teknisi� menetelmi� (mukaan lukien liiketoimintaa varten kehitetyt menetelm�t) pidett�isiin tekniikan tasoon lis�yst� tuovina ja siten patentoitavina ainoastaan siksi, ett� ne on toteutettu tietokoneella.
rec5a	fi	Euroopan patenttisopimuksen 52 artiklan 2 kohdan a ja c alakohdassa m��r�t��n, ettei "matemaattisia menetelmi�", "suunnitelmia, s��nt�j� tai menetelmi� �lyllist� toimintaa, pelej� tai liiketoimintaa varten" eik� "tietokoneohjelmia" pidet� patentoitavina keksint�in�. Koska algoritmi voi olla tietokoneohjelma tai sen yksi osa, joka on erill��n ohjelman k�ytt�ymp�rist�st� tai matemaattisesta kaavasta tai menetelm�st�, sit� ei voida sellaisenaan patentoida. Pelk�st��n se seikka, ett� keksint��n sis�ltyy algoritmi, ei kuitenkaan ole esteen� keksinn�n patentoimiselle.
rec7	fi	Direktiivin taloudellista merkityst� ei saa aliarvioida. Sit� paitsi tutkimukset ovat osoittaneet T&K-menojen, patenttihakemusten ja tuottavuuden v�lisen yhteyden. Teollis- ja tekij�noikeuksien suojelun avulla luodaan ty�paikkoja ja varmistetaan niiden s�ilyminen Euroopassa sek� saadaan lis�� tuloja.
rec12	fi	Johdantokappaleita ei pit�isi muotoilla samalla tavoin kuin s��nn�ksi�.
art3a	fi	
art4b	fi	
art7	fi	
art5_1a	fi	
rec18a	fi	
art8ce	fi	
rec13	fi	
art2bb	fi	
rec6	fr	La Convention sur le brevet europ�en est un instrument international qui ne peut �tre modifi� que par les m�canismes pr�vus dans ladite Convention.
art2a	fr	Ce consid�rant rappelle les r�gles en la mati�re, consacr�es par l'article 52, paragraphe 1, de la Convention sur le brevet europ�en.
art2ba	fr	Il importe de pr�ciser que toutes les inventions mises en oeuvre par ordinateur ne sont pas n�cessairement brevetables. Toutefois, les inventions mises en oeuvre par ordinateur ne devraient pas �tre exclues de la brevetabilit� pour la seule raison qu'elles pr�voient l'utilisation d'un programme d'ordinateur. En soulignant le fait que, pour �tre brevetable, une invention mise en oeuvre par ordinateur, bien qu'appartenant d�j� � un domaine technique, doit en outre apporter une contribution technique � l'�tat de la technique, et en attirant l'attention sur l'approche probl�me-solution utilis�e par les examinateurs de l'Office europ�en des brevets pour �valuer l'activit� inventive, le but poursuivi est d'�viter que des m�thodes relevant d'une activit� inventive mais non techniques (telles que des m�thodes destin�es � l'exercice d'activit�s �conomiques) puissent �tre consid�r�es comme apportant une contribution technique et donc comme brevetables, uniquement parce qu'elles sont mises en oeuvre sur un ordinateur.
art2b	fr	Ce consid�rant indique clairement qu'il ne suffit pas de sp�cifier l'utilisation d'un ordinateur (c'est-�-dire de moyens techniques) pour rendre brevetable une invention mise en oeuvre par ordinateur. L'invention dans son ensemble doit apporter une contribution technique. Un simple traitement informatique n'est pas suffisant.
rec5a	fr	L'article 52, paragraphe 2, points a) et c), de la Convention sur le brevet europ�en exclut de la brevetabilit� les "m�thodes math�matiques" et les "plans, principes et m�thodes dans l'exercice d'activit�s intellectuelles, en mati�re de jeu ou dans le domaine des activit�s �conomiques, ainsi que les programmes d'ordinateurs". �tant donn� qu'un algorithme pourrait �tre un programme d'ordinateur ou un �l�ment d'un tel programme isol�ment de son environnement d'ex�cution, ou une formule ou m�thode math�matique, il est exclu de la brevetabilit� en tant que tel. Toutefois, la simple utilisation d'un algorithme n'emp�che pas la brevetabilit�.
rec7	fr	Il convient de ne pas sous-estimer l'importance �conomique de la pr�sente directive. En outre, des �tudes ont fait appara�tre un lien entre les d�penses en recherche et d�veloppement, les demandes de brevet et la productivit�. La protection de la propri�t� intellectuelle cr�e et assure des emplois en Europe et g�n�re des revenus.
rec12	fr	Les consid�rants ne doivent pas �tre r�dig�s comme des dispositions normatives.
art3a	fr	
art4b	fr	L'amendement refl�te la jurisprudence qui pr�vaut actuellement en Allemagne. Les juges de la Cour f�d�rale des brevets (arr�t du 26 mars 2002, 17 W (brevet) 69/98), ont estim� que le requ�rant voyait une indication d�terminante du caract�re technique de la m�thode dans le fait qu'elle reposait sur un probl�me technique. �tant donn� que la m�thode propos�e ne requiert pas un dictionnaire, l'espace m�moire n�cessaire � celui-ci peut �tre �conomis�. S'agissant du probl�me technique, celui-ci ne peut �tre consid�r� que comme une indication mais non comme une preuve du caract�re technique du proc�d�. Si la mise en oeuvre par ordinateur de proc�d�s non techniques �tait consid�r�e comme pr�sentant un caract�re technique pour la seule raison qu'elle pr�sente des caract�ristiques sp�cifiques diff�rentes, par exemple accroissement de la vitesse ou r�duction de l'espace de stockage, cela aurait pour cons�quence que toute mise en oeuvre par ordinateur serait r�put�e pr�senter un caract�re technique. Cela parce que tel proc�d� pr�sente des caract�ristiques de mise en oeuvre distinctes qui lui permettent soit d'ex�cuter plus vite soit d'�conomiser de l'espace de stockage. Dans le cas d'esp�ce, ces propri�t�s ne reposent pas sur une performance technique mais d�coulent de la m�thode non technique retenue. Si le fait de r�soudre un tel probl�me constituait une raison suffisante pour attribuer un caract�re technique � une mise en oeuvre informatis�e, toute mise en oeuvre d'une m�thode non technique devrait �tre brevetable. Cela serait contraire � la conclusion de la Cour f�d�rale selon laquelle l'exclusion des programmes d'ordinateur de la brevetabilit� ne permettent pas au juge d'adopter une approche qui rendrait brevetable une instruction contenue dans des instructions orient�es sur l'ordinateur.
art7	fr	
art5_1a	fr	
rec18a	fr	
art8ce	fr	�tant donn� que les crit�res de brevetabilit� peuvent exercer un �norme impact sur des pans entiers d'activit�s et donc sur l'�conomie et la soci�t� dans son ensemble, il est de la plus haute importance de les d�finir sous contr�le parlementaire. Le pr�sent amendement fait la synth�se des amendements 35 CULT et 44 ITRE.
rec11	fr	
rec13a	fr	
rec14	fr	
rec13	fr	
art2bb	fr	
rec6	it	La Convenzione sul brevetto europeo � uno strumento internazionale che non pu� essere modificato se non mediante i meccanismi previsti nel medesimo.
art2ba	it	� importante chiarire che non tutte le invenzioni attuate per mezzo di elaboratori elettronici sono necessariamente brevettabili. Tuttavia, le invenzioni attuate per mezzo di elaboratori elettronici non dovrebbero essere escluse dalla brevettabilit� unicamente per il motivo che esse specificano l'uso di un programma per elaboratore. Sottolineando il fatto che un'invenzione brevettabile attuata per mezzo di elaboratori elettronici, bench� appartenenti a un determinato settore tecnologico, deve apportare un contributo tecnico allo stato dell'arte e attirando l'attenzione sull'approccio "problema- soluzione" utilizzato dagli esaminatori di brevetti all'Ufficio europeo dei brevetti nel valutare invenzioni implicanti un'attivit� inventiva, si intende evitare che i metodi inventivi ma non tecnici (inclusi quelli per attivit� commerciali) vengano considerati contributi tecnici e, di conseguenza, brevettabili semplicemente perch� essi vengono attuati per mezzo di un elaboratore.
rec5a	it	L'articolo 52, paragrafo 2, lettere a) e c) della Convenzione sul brevetto europeo preclude la brevettabilit� di "metodi matematici" e "schemi, regole e metodi per eseguire atti mentali, giocare e svolgere attivit� commerciali nonch� programmi per elaboratore". Dato che un algoritmo potrebbe essere un programma per elaboratore o un elemento di tale programma, isolato dal suo ambiente di esecuzione o un metodo o formula matematica, � pertanto precluso dalla brevettabilit�. Tuttavia, la semplice utilizzazione di un algoritmo non preclude la brevettabilit�.
rec7	it	L'importanza economica di tale direttiva non deve essere sottovalutata. Inoltre, alcuni studi hanno mostrato la correlazione esistente tra le spese R&S, le domande di brevetto e la produttivit�. La tutela della propriet� intellettuale crea e garantisce posti di lavoro in Europa ed � produttrice di entrate.
rec12	it	La  redazione di considerando come se fossero disposizioni normative costituisce un esempio di cattiva elaborazione.
art4b	it	
art7	it	
art5_1a	it	
rec18a	it	
art8ce	it	
rec13	it	
art2bb	it	
rec6	nl	Het Europees Octrooiverdrag is een internationaal instrument dat alleen kan worden gewijzigd via de daarin opgenomen mechanismen.
art2ba	nl	Het is van belang dat duidelijk vooropgesteld wordt dat niet alle in computers ge�mplementeerde uitvindingen noodzakelijkerwijs octrooieerbaar zijn. In computers ge�mplementeerde uitvindingen moeten echter niet van octrooieerbaarheid worden uitgesloten louter op grond van het feit dat daarbij het gebruik van een computerprogramma wordt gespecificeerd. Door uitdrukkelijk te wijzen op het feit dat een octrooieerbare in computers ge�mplementeerde uitvinding, ook al hoort deze thuis op een terrein van technologie, een technische bijdrage moet leveren aan de stand van de techniek en door de aandacht te vestigen op de probleem- en oplossingbenadering, zoals deze door de octrooionderzoekers van het Europees Octrooibureau bij de beoordeling van uitvinderswerkzaamheid wordt toegepast, wordt beoogd te voorkomen dat uitvindersmethoden van niet-technische aard (met inbegrip van bedrijfsmethoden) geacht kunnen worden een technische bijdrage te leveren en bijgevolg octrooieerbaar zijn, louter omdat deze op een computer ge�mplementeerd worden.
rec5a	nl	Artikel 52, lid 2, onder a) en c) van het Europees Octrooiverdrag sluit de octrooieerbaarheid van "wiskundige methoden" en "stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, alsmede computerprogramma's" uit. Daar een algoritme een computerprogramma of een uit zijn normale gebruikssituatie ge�soleerd element van zo'n programma of een mathematische formule of methode kan zijn, is deze als zodanig uitgesloten van octrooieerbaarheid. Echter louter het gebruik van een algoritme sluit octrooieerbaarheid niet uit.
rec7	nl	Het economisch belang van deze richtlijn moet niet worden onderschat. Bovendien heeft onderzoek aangetoond dat er een verband bestaat tussen O&O-uitgaven, octrooiaanvragen en productiviteit. Bescherming van intellectuele eigendom schept en waarborgt werkgelegenheid in Europa en levert inkomsten op.
rec12	nl	Het is verwerpelijk om overwegingen als normatieve bepalingen te redigeren.
art3a	nl	
art4b	nl	
art7	nl	
art5_1a	nl	
rec18a	nl	Bij de octrooieerbaarheid van software moeten de beginselen van het octrooirecht worden gerespecteerd.</Amend>
art8ce	nl	
rec13	nl	
art2bb	nl	
rec6	pt	A Conven��o sobre a Patente Europeia � um instrumento internacional que n�o pode ser alterado sen�o pelos mecanismos nele previstos.
art2a	pt	Este considerando reitera os termos da lei, tal como se encontra consagrada no n� 1 do artigo 52� da Conven��o sobre a Patente Europeia.
art2ba	pt	� importante esclarecer que nem todos inventos que implicam programas de computador s�o necessariamente patente�veis. Contudo, os inventos que implicam programas de computador n�o deveriam ser exclu�dos da patenteabilidade pela �nica e simples raz�o de que especificam a necessidade da utiliza��o de um programa inform�tico. Ao sublinhar o facto de que um invento patente�vel que implique programas de computador se bem que pertencendo a um determinado dom�nio tecnol�gico tem obrigatoriamente de dar um contributo t�cnico para o progresso tecnol�gico, e ao chamar a aten��o para o m�todo da coloca��o do problema e da busca de uma solu��o utilizado pelos analistas de patentes no Instituto Europeu de Patentes, sempre que avaliam a presen�a de actividade inventiva, pretende-se evitar que m�todos inventivos, embora desprovidos de car�cter t�cnico (incluindo os procedimentos comerciais) possam ser considerados como contributos t�cnicos e, por essa via, como material patente�vel, apenas e s� porque pressup�em a sua utiliza��o num computador.
art2b	pt	Este considerando deixa claro que n�o basta especificar o recurso a um computador (ou seja, a meios de car�cter t�cnico) para fazer com que um invento que implique programas de computador seja patente�vel. � o invento no seu todo que tem de constituir um contributo de car�cter t�cnico. O vulgar processamento de dados n�o basta.
art3a	pt	
art4b	pt	
art7	pt	
art5_1a	pt	
rec18a	pt	
art8ce	pt	
rec5a	pt	As al�neas a) e c) do n� 2 do artigo 52� da Conven��o sobre a Patente Europeia excluem a patenteabilidade de "m�todos matem�ticos" e de "esquemas, normas e m�todos para executar actos mentais, realizar jogos e fazer neg�cios, bem como programas inform�ticos". Uma vez que um algoritmo pode ser um programa inform�tico, ou um elemento de um programa inform�tico, independentemente do ambiente em que corre ou de qualquer f�rmula ou m�todo matem�tico, ele est�, enquanto tal, exclu�do da patenteabilidade. Contudo, a mera utiliza��o de um algoritmo n�o exclui a patenteabilidade.
rec7	pt	A import�ncia econ�mica da presente directiva n�o deve ser subestimada. Para al�m disso, h� estudos que demonstram a exist�ncia de uma liga��o entre os investimentos em I & D, as reivindica��es de patentes e a produtividade. A protec��o da propriedade intelectual cria e assegura empregos na Europa, trazendo novas receitas.
rec12	pt	A elabora��o de considerandos, como se estiv�ssemos perante disposi��es de car�cter normativo, constitui um exemplo de m� redac��o.
rec11	pt	
rec13a	pt	
rec14	pt	
rec13	pt	
art2bb	pt	
rec6	sv	Europeiska patentkonventionen �r ett internationellt instrument som endast f�r �ndras genom de mekanismer som fastst�lls i densamma.
art2ba	sv	Det �r viktigt att klarg�ra att alla datorrelaterade uppfinningar inte n�dv�ndigtvis �r patenterbara. Det g�r dock inte att f�rbjuda patentering av datorrelaterade uppfinningar bara f�r att de specificerar hur ett datorprogram skall anv�ndas. Genom att understryka att en patenterbar datorrelaterad uppfinning, �ven om den tillh�r teknikens omr�de, m�ste utg�ra ett tekniskt bidrag till teknikens st�ndpunkt, och genom att f�sta uppm�rksamheten vid den problem- och l�sningsmetod som anv�nds av patentgranskarna vid Europeiska patentverket (EPO) d� de utv�rderar uppfinningsh�jden, �r avsikten att man inte skall till�ta att uppfinningsrika, men icke-tekniska, metoder (inklusive aff�rsmetoder) betraktas som tekniska bidrag och s�lunda �r patenterbara bara f�r att de anv�nds p� en dator.
art2b	sv	I sk�let klarg�rs det att det inte �r tillr�ckligt att specificera att en dator (med andra ord ett tekniskt medel) skall anv�ndas f�r att en datorrelaterad uppfinning skall bli patenterbar. Uppfinningen i sig m�ste utg�ra ett tekniskt bidrag. Det �r inte tillr�ckligt med vanlig databehandling.
rec5a	sv	I artikel 52.2 a och c i Europeiska patentkonventionen utesluts patentering av "matematiska metoder" och "planer, regler eller metoder f�r intellektuell verksamhet, f�r spel eller f�r aff�rsverksamhet samt datorprogram". Eftersom en algoritm skulle kunna vara ett datorprogram eller en del av ett s�dant program isolerat fr�n den milj� d�r det genomf�rs eller en matematisk formel eller metod kan den i sig inte patenteras. Enbart det faktum att man anv�nder sig av en algoritm utesluter emellertid inte patenterbarhet.
rec7	sv	Direktivets ekonomiska betydelse b�r inte underskattas. Unders�kningar har visat att det finns ett samband mellan utgifterna f�r forskning och utveckling, patentans�kningar och produktivitet. Skydd av immateriella r�ttigheter skapar och garanterar arbetstillf�llen i Europa och ger inkomster.
rec12	sv	Sk�l skall inte formuleras som normativa best�mmelser.
art3a	sv	
art4b	sv	
art5_1a	sv	
rec18a	sv	
art8ce	sv	
rec13	sv	
art2bb	sv	
\.


--
-- Data for TOC entry 67 (OID 23751)
-- Name: itre; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY itre (id, lang, itre) FROM stdin;
rec5	da	Lovbestemmelserne vedr�rende computer-implementerede opfindelsers patenterbarhed b�r derfor harmoniseres med henblik p� at sikre, at den herved opn�ede retssikkerhed og omfanget af de betingelser, der stilles for patenterbarhed, g�r det muligt for virksomhederne at f� det fulde udbytte af deres opfindelsesproces og udg�r et incitament til investering og innovation.
rec7a	da	Europa-Parlamentet har gentagne gange kr�vet, at Den Europ�iske Patentmyndighed reviderer sit reglement og underl�gges offentlig kontrol i forbindelse med ud�velsen af sine funktioner. Det ville i den forbindelse is�r v�re hensigtsm�ssigt, om man tog Patentmyndighedens praksis med at tage sig betalt for de patenter, den udsteder, op til fornyet overvejelse, for s� vidt som denne praksis skader institutionens offentlige karakter. <P>I sin beslutning<SUP>1</SUP> om Den Europ�iske Patentmyndigheds afg�relse vedr�rende patent EP 695 351, udstedt den 8. december 1999, kr�vede Europa-Parlamentet en revision af Den Europ�iske Patentmyndigheds reglement for at sikre, at den underl�gges offentlig kontrol ved ud�velsen af sine funktioner.<P>____________</P><P ALIGN="JUSTIFY"><SUP>1 </SUP>EFT C 378 af 29.12.2000, s. 95.
rec7b	da	Software spiller en vigtig rolle inden for en lang r�kke erhverv, men repr�senterer samtidig en grundl�ggende form for kreativitet og selvudfoldelse. Software er herudover et specialiseret teknisk felt og en grundl�ggende menneskelig aktivitet, der besk�ftiger over 10 millioner professionelle programudviklere p� verdensplan, hvortil kommer de mange millioner andre, der skaber software til et eller andet form�l. Uafh�ngige programudviklere og sm� virksomheder er af afg�rende betydning for innovationen p� dette omr�de. Heraf f�lger, at de metoder, der anvendes til at fremme investeringerne i prim�rt softwarebaserede erhverv, ikke m� bringe alles mulighed for at blive aktive skabere og innovative brugere af software i fare, og navnlig at patenter ikke m� kunne bruges til at monopolisere de v�rkt�jer, der benyttes til selvudfoldelse, kreativitet samt udbredelse og udveksling af information og viden.
rec11	da	udg�r
art2a	da	a)ved en "computer-implementeret opfindelse" forst�s enhver opfindelse, der kan anvendes industrielt, og hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere nye karakteristika, der udg�r et teknisk bidrag, samt andre nye eller ikke-nye karakteristika, og som helt eller delvist skal implementeres ved hj�lp af et eller flere edb-programmer
art2b	da	b)ved et "teknisk bidrag" forst�s et bidrag til et teknisk omr�de, der har opfindelsesh�jde og l�ser et eksisterende teknisk problem eller udvider det aktuelle tekniske niveau for en fagmand.
art3	da	udg�r
art4_1	da	1.Medlemsstaterne sikrer, at en computer-implementeret opfindelse kun kan patenteres, hvis den yder et teknisk bidrag som defineret i artikel 2, litra b.
art4_2	da	udg�r
art4_3	da	3.Det v�sentlige omfang af et teknisk bidrag vurderes som forskellen mellem de tekniske elementer, der indg�r i patentkravets genstand som helhed, og det aktuelle tekniske niveau. Elementer, som en patentans�ger har bragt til offentlighedens kundskab inden for en periode p� seks m�neder forud for ans�gningsdatoen, betragtes ikke som en del af det aktuelle tekniske niveau ved vurderingen af det p�g�ldende patentkrav.
art4_3a	da	3a.Udelukkelse fra patenterbarhed <P>En computer-implementeret opfindelse vurderes ikke at yde et teknisk bidrag, blot fordi det indeb�rer brug af en computer eller andre maskiner. Tilsvarende kan der ikke udtages patent p� opfindelser, der omfatter edb-programmer, som implementerer forretningsmetoder, matematiske metoder og andre metoder, og som ikke giver nogen tekniske virkninger ud over h�ndteringen og repr�sentationen af information inden for computersystemet eller -netv�rket.
art6a	da	Artikel 6a<P>Medlemsstaterne sikrer, at n�r anvendelsen af en patenteret teknik er p�kr�vet alene med henblik p� konvertering af de konventioner, der anvendes i to forskellige computersystemer eller -netv�rk, for at muligg�re kommunikation og dataudveksling mellem dem, betragtes en s�dan anvendelse ikke som en kr�nkelse af patentet.
art7	da	Kommissionen overv�ger, hvordan patentbeskyttelse af computer-implementerede opfindelser indvirker p� innovation og konkurrence, b�de i Europa og internationalt, og p� EU's erhvervsliv, herunder elektronisk handel.
art8ca	da	(ca)hvorvidt der eventuelt er opst�et problemer med hensyn til forholdet mellem patentbeskyttelse af computer-implementerede opfindelser og beskyttelse af computerprogrammer via ophavsret som fastsat i direktiv 91/250/EF.
rec5	de	Aus diesen Gr�nden sollten die f�r die Patentierbarkeit computerimplementierter Erfindungen ma�geblichen Rechtsvorschriften vereinheitlicht werden, um sicherzustellen, dass die dadurch gew�hrte Rechtssicherheit und das Anforderungsniveau f�r die Patentierbarkeit dazu f�hren, dass innovative Unternehmen den gr��tm�glichen Nutzen aus ihrem Erfindungsprozess ziehen und Anreize f�r Investitionen und Innovationen geschaffen werden.
rec7a	de	Das Europ�ische Parlament hat wiederholt eine �berarbeitung der Bestimmungen f�r die T�tigkeit des Europ�ischen Patentamts und eine �ffentliche Rechenschaftspflicht dieses Amtes gefordert. In dieser Hinsicht sollte insbesondere die Praxis in Frage gestellt werden, der zufolge es das Patentamt f�r angemessen h�lt, f�r die Patente, die es erteilt, bezahlt zu werden, da diese Praxis der �ffentlichkeit des Patentamtes schadet. <P>In seiner (im ABl. C378 vom 29.12.2000, S.95, ver�ffentlichten) Entschlie�ung zu dem Beschluss des Europ�ischen Patentamts bez�glich des am 8.Dezember 1999 erteilten Patents Nr.EP 695351 forderte das Europ�ische Parlament "eine �berpr�fung der T�tigkeiten des EPA, um zu gew�hrleisten, dass es einer �ffentlichen Rechenschaftspflicht unterliegt".
rec7b	de	Software spielt in vielen Industriezweigen eine wichtige Rolle und ist zugleich eine grundlegende Form von Werksch�pfung und Ausdruck. Software ist ein spezialisierter Technikbereich und zugleich eine wichtige menschliche T�tigkeit, mit weltweit �ber 10Millionen professionellen Entwicklern und zahllosen Personen, die Software f�r verschiedene Zwecke entwickeln. Unabh�ngige Entwickler und kleine Unternehmen spielen bei der Innovation in diesem Bereich eine wichtige Rolle. Daraus folgt, dass die Mittel, die eingesetzt werden, um Investitionen in softwareintensive Industriezweige anzuregen, nicht die F�higkeit aller beeintr�chtigen d�rfen, aktiv Software zu entwickeln und sie innovativ zu nutzen. Patente d�rfen nicht dazu f�hren, dass Instrumente f�r Ausdruck, Sch�pfung, Verbreitung und Austausch von Daten und Kenntnissen monopolisiert werden.
rec11	de	Entf�llt
art2a	de	(a)"Computerimplementierte Erfindung" ist jede gewerblich anwendbare Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein neuartiges Merkmal, das einen technischen Beitrag darstellt, sowie weitere, neuartige oder bekannte, Merkmale aufweist, die ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert werden m�ssen.
art2b	de	(b)"Technischer Beitrag" ist ein auf einer erfinderischen T�tigkeit beruhender Beitrag zu einem Gebiet der Technik, der ein bestehendes technisches Problem l�st oder f�r eine fachkundige Person den Stand der Technik signifikant bereichert.
art3	de	Entf�llt
art4_1	de	1.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung nur patentierbar ist, wenn sie einen technischen Beitrag im Sinne des Artikels2 Buchstabeb leistet.
art4_2	de	Entf�llt
art4_3	de	3.Bei der Ermittlung des signifikanten Ausma�es des technischen Beitrags wird beurteilt, inwieweit sich die technischen Merkmale, die der Gegenstand des Patentanspruchs in seiner Gesamtheit aufweist, vom Stand der Technik abheben. Merkmale, die der Patentanmelder im Laufe eines Zeitraums von sechs Monaten vor dem Zeitpunkt der Anmeldung offenbart, gelten bei der Beurteilung des diesbez�glichen Anspruchs nicht als zum Stand der Technik geh�rig.
art4_3a	de	3a.Ausschluss von der Patentierbarkeit <P>Eine computerimplementierte Erfindung gilt nicht lediglich deshalb als Erfindung, die einen technischen Beitrag leistet, weil dabei ein Computer oder eine sonstige Vorrichtung verwendet wird. Nicht patentierbar sind folglich Erfindungen, die Computerprogramme zur Implementierung von Gesch�ftsmethoden, mathematischen oder sonstigen Methoden beinhalten und die �ber die Informationsbearbeitung und "darstellung innerhalb des Computersystems oder "netzes hinaus keine technischen Wirkungen hervorrufen.
art6a	de	Artikel 6a<P>Die Mitgliedstaaten stellen sicher, dass in allen F�llen, in denen der Einsatz einer patentierten Technik nur zum Zweck der Konvertierung der in zwei verschiedenen Computersystemen oder "netzen verwendeten Konventionen ben�tigt wird, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu erm�glichen, diese Verwendung nicht als Patentverletzung gilt.
art7	de	Die Kommission beobachtet, wie sich der Patentschutz hinsichtlich computerimplementierter Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen und den elektronischen Gesch�ftsverkehr auswirkt.
art8ca	de	(ca)etwaige Schwierigkeiten, die im Hinblick auf das Verh�ltnis zwischen dem Schutz durch Patente auf computerimplementierte Erfindungen und dem Schutz von Computerprogrammen durch das Urheberrecht, wie es die Richtlinie 91/250/EG vorsieht, aufgetreten sind.
rec5	en	Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation.
rec7a	en	Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution. <P>In its resolution<SUP>1</SUP> on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8December 1999, Parliament requested a review of the Office's operating rules to ensure that it was publicly accountable in the exercise of its functions. <P><SUP>1</SUP>OJ C 378, 29.12.2000, p. 95.
rec7b	en	While software plays an important role in a number of industries it is also a basic form of creativity and self-expression. Software is, in addition, a field of specialised engineering and a basic human activity, with more than 10 million professional developers throughout the world and tens of millions of people creating software for one purpose or another. Independent developers and small businesses play a fundamental role in innovation in this area. It follows that the means employed to boost investment in largely software-based industries should not lead to jeopardising the capacity of all concerned to become active creators and innovative users of software, and in particular that patents should not permit the monopolisation of tools for self-expression, creativity, and the dissemination and exchange of information and knowledge.
rec11	en	deleted
art2a	en	(a)"computer-implemented invention" means any invention susceptible of industrial application the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more novel features which constitute a technical contribution, and other features whether novel or not, and have to be realised wholly or partly by means of a computer program or computer programs;
art2b	en	(b)"technical contribution" means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.
art3	en	Deleted
art4_1	en	1.Member States shall ensure that a computer-implemented invention is patentable only on the condition that it makes a technical contribution as defined in Article 2(b).
art4_2	en	Deleted
art4_3	en	3.The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.
art4_3a	en	3a.Exclusions from patentability <P>A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, or other apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods, which inventions do not produce any technical effects beyond the manipulation and representation of information within computer-system or network, shall not be patentable.
art6a	en	Article 6a <P>Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
art7	en	The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.
art8ca	en	(ca)any difficulties that have arisen with the relationship between protection by means of patents on computer-implemented inventions and the protection of computer programs by means of copyright law, as laid down in Directive 91/250/EEC.
rec5	es	Por consiguiente, las normas jur�dicas relativas a la patentabilidad de las invenciones implementadas en ordenador deber�an armonizarse, de modo que se garantizara que la seguridad jur�dica resultante y el nivel de los requisitos exigidos para la patentabilidad permitieran a las empresas innovadoras obtener el m�ximo beneficio de su proceso de invenciones e impulsaran la inversi�n y la innovaci�n.
rec7a	es	El Parlamento Europeo ha solicitado varias veces que la Oficina Europea de Patentes revise sus normas de funcionamiento y que este organismo sea sometido a control p�blico en el ejercicio de sus funciones. A este respecto, ser�a especialmente oportuno reexaminar la pr�ctica de la Oficina Europea de Patentes de imponer un canon sobre las patentes que expide, ya que dicha pr�ctica vulnera el car�cter p�blico de la Instituci�n. <P>En su Resoluci�n<SUP>(1)</SUP> sobre la decisi�n de la Oficina Europea de Patentes con respecto a la patente EP 695 351, expedida el 8 de diciembre de 1999, el Parlamento Europeo solicit� la revisi�n de las normas de funcionamiento de la Oficina de forma que se garantizara que este organismo pueda rendir cuentas p�blicamente sobre el ejercicio de sus funciones. <P><SUP>(1)</SUP>DO C 378 de 29.12.2000, p. 95.
rec7b	es	El software desempe�a, por una parte, un importante papel en numerosos sectores y representa, por otra, una forma b�sica de expresi�n y creaci�n. Asimismo, el software constituye un �rea especializada de la ingenier�a y una actividad humana fundamental a la que contribuyen m�s de diez millones de creadores profesionales en todo el mundo y varias decenas de millones de personas que crean software de una u otra forma. Los desarrolladores aut�nomos y las peque�as sociedades desempe�an un papel fundamental en la innovaci�n en este sector. De ello se desprende que los medios empleados para estimular las inversiones en las �reas con gran densidad de software no deben poner en peligro la capacidad general para convertirse en creador activo y en usuario innovador de software y, especialmente, que ha de evitarse que las patentes constituyan un medio de monopolizar las herramientas de expresi�n, creaci�n, difusi�n e intercambio del conocimiento y la informaci�n.
rec11	es	suprimido
art2a	es	a)«invenci�n implementada en ordenador», toda invenci�n susceptible de aplicaci�n industrial para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas que constituyan una contribuci�n t�cnica, as� como otras caracter�sticas, nuevas o no, y que deban realizarse total o parcialmente mediante un programa o programas de ordenador;
art2b	es	b)«contribuci�n t�cnica», una contribuci�n a un campo tecnol�gico que implique una medida inventiva que solucione un problema t�cnico existente o que ampl�e el estado de la t�cnica de un modo significativo para un experto en la materia.
art3	es	suprimido <P>
art4_1	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables �nicamente a condici�n de que constituyan una contribuci�n t�cnica con arreglo a la definici�n de la letra b) del art�culo 2.
art4_2	es	suprimido
art4_3	es	3.El alcance significativo de la contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y los elementos t�cnicos incluidos en el �mbito de la reivindicaci�n de la patente considerada en su conjunto. Para la evaluaci�n de esa particular reivindicaci�n no se considerar� que formen parte del estado de la t�cnica los elementos que el solicitante de la patente haya revelado durante los seis meses anteriores a la fecha de la solicitud.
art4_3a	es	3 bis. Exclusiones de la patentabilidad <P>No se considerar� que una invenci�n implementada en ordenador constituya una contribuci�n t�cnica simplemente porque implique la utilizaci�n de un ordenador u otro aparato. En consecuencia, no ser�n patentables las invenciones que supongan programas de ordenador que implementen m�todos comerciales, matem�ticos o de otro tipo, cuyas invenciones no den lugar a efectos t�cnicos distintos de la manipulaci�n y representaci�n de informaci�n dentro del sistema o de la red de ordenadores.
art6a	es	Art�culo 6 bis <P>Los Estados miembros garantizar�n que, en cualquier caso en que sea necesaria la utilizaci�n de una t�cnica patentada con el �nico objetivo de asegurar la conversi�n de las convenciones utilizadas en dos sistemas de ordenadores o redes diferentes a fin de hacer posible la comunicaci�n y el intercambio rec�proco del contenido de datos, esta utilizaci�n no se considere una violaci�n de la patente.
art7	es	La Comisi�n seguir� de cerca el impacto de la protecci�n otorgada por las patentes respecto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, incluido el comercio electr�nico.
art8ca	es	(c bis) si han surgido dificultades en la relaci�n entre la protecci�n otorgada por patentes de invenciones implementadas en ordenador y la protecci�n de programas de ordenador derivada de derechos de autor con arreglo a lo dispuesto en la Directiva 91/250/CEE.
rec5	fi	N�in ollen tietokoneella toteutettujen keksint�jen patentoitavuutta s��ntelev�t oikeudelliset s��nn�t olisi yhdenmukaistettava, jotta varmistetaan, ett� tuloksena saavutettavan oikeusvarmuuden ja patentoitavuudelle asetettavan vaatimustason ansiosta yritykset voivat saada mahdollisimman paljon etua innovointiprosesseistaan, ja edistet��n investointeja ja innovaatiotoimintaa.
rec7a	fi	Euroopan parlamentti on useaan otteeseen vaatinut, ett� Euroopan patenttivirasto tarkistaisi toimintas��nt�j��n ja ett� sen teht�vien hoitamista valvottaisiin julkisesti. Olisi erityisen suotavaa kyseenalaistaa Euroopan patenttiviraston k�yt�nt� peri� maksu my�nt�mist��n patenteista, sill� se on haitaksi virastolle julkisena laitoksena. <P>P��t�slauselmassaan<SUP>1</SUP> Euroopan patenttiviraston 8.joulukuuta 1999 tekem�st� patenttia EP695351 koskevasta p��t�ksest� Euroopan parlamentti vaati tarkistamaan Euroopan patenttiviraston toimintas��nt�j� sen varmistamiseksi, ett� siit� tulee teht�viens� hoitamisesta julkisesti vastuussa oleva elin. <P><SUP>1</SUP> EYVL C 378, 29.12.2000, s. 95.
rec7b	fi	Ohjelmistoilla on t�rke� sija monilla teollisuudenaloilla, ja ne ovat perustavanlaatuinen luomisen ja ilmaisun muoto. Ohjelmistot ovat samalla erikoisteknologian ala ja perustavaa inhimillist� toimintaa
rec11	fi	Poistetaan.
art2a	fi	(a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan teollisesti k�ytt�kelpoista keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman uuden piirteen, joka tuo lis�yksen tekniikan tasoon, sek� muita uusia tai tunnettuja piirteit�, jotka toteutetaan kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art2b	fi	(b)'Lis�yksell� tekniikan tasoon' tarkoitetaan sellaista keksinn�llist� toimintaa edellytt�v�� lis�yst� tekniikanalaan, jolla ratkaistaan tekninen ongelma tai edistet��n tekniikan tasoa alan ammattimiehelle merkitt�v�ll� tavalla.
art3	fi	Poistetaan.
art4_1	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa vain sill� edellytyksell�, ett� se on 2artiklan balakohdassa tarkoitettu lis�ys tekniikan tasoon.
art4_2	fi	Poistetaan.
art4_3	fi	3.Merkitt�v�� lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen suojapiiriin sis�ltyvien teknisten seikkojen ja vallitsevan tason v�lill�. Patentinhakijan kuuden kuukauden aikana ennen hakemuksen j�tt�p�iv�� julkistamien seikkojen ei katsota kuuluvan tekniikan tasoon kyseist� patenttivaatimusta arvioitaessa.
art4_3a	fi	3 a. Poikkeukset patentoitavuudesta <P>Tietokoneella toteutettua keksint�� ei katsota lis�ykseksi tekniikan tasoon pelk�st��n sen perusteella, ett� se edellytt�� tietokoneen tai muun laitteiston k�ytt��. Patentoitavia eiv�t siis ole keksinn�t, joissa k�ytet��n tietokoneohjelmia, joilla suoritetaan liiketoiminnallisia, matemaattisia tai muita menetelmi�, joilla ei saada aikaan muuta teknist� vaikutusta kuin tiedon k�sittely ja esitt�minen tietokonej�rjestelm�ss� tai verkossa.
art5	fi	(c)J�senvaltioiden on varmistettava, ett� tietokoneohjelman k�ytt� muihin kuin patentin suojapiiriin kuuluviin tarkoituksiin ei voi muodostaa suoraa tai ep�suoraa patentinloukkausta. <P>(d)J�senvaltioiden on varmistettava, ett� aina kun patenttivaatimuksessa mainitaan piirteit�, jotka viittaavat tietokoneohjelman k�ytt��n, kuvauksen osana on julkaistava toimiva ja hyvin dokumentoitu ohjelman viitetoteutus ilman rajoittavia lisenssiehtoja.
art6a	fi	6 a artikla <P>J�senvaltioiden on varmistettava, ett� aina kun tarvitsee k�ytt�� patentoitua tekniikkaa siihen yksinomaiseen tarkoitukseen, ett� varmistetaan kahden eri tietokonej�rjestelm�n tai verkon konventioiden konversio niiden v�lisen yhteyden ja tietojenvaihdon mahdollistamiseksi, t�llaista k�ytt�� ei katsota patentinloukkaukseksi.
art7	fi	Komissio seuraa tietokoneella toteutettujen keksint�jen patenttisuojan vaikutuksia innovaatiotoimintaan ja kilpailuun Euroopan laajuisesti ja kansainv�lisesti sek� niiden vaikutuksia eurooppalaisiin yrityksiin, mukaan luettuna s�hk�inen kaupank�ynti.
art8	fi	(b)siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; <P>(c)siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi; ja
art8ca	fi	(c a) mahdollisista vaikeuksista, joita on aiheutunut tietokoneella toteutettujen keksint�jen patenttisuojan ja direktiivin 91/250/ETY mukaisen tietokoneohjelmien tekij�noikeussuojan v�lisest� suhteesta.
rec5	fr	En cons�quence, les r�gles de droit r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre harmonis�es de fa�on � assurer que la s�curit� juridique qui en r�sulte et le niveau des crit�res de brevetabilit� permettent aux entreprises innovatrices de tirer le meilleur parti de leur processus inventif et stimulent l'investissement et l'innovation.
rec7a	fr	Le Parlement europ�en a, � plusieurs reprises, demand� que l'Office europ�en des brevets r�vise ses r�gles de fonctionnement et que cet organisme soit contr�l� publiquement dans l'exercice de ses fonctions. A cet �gard, il serait particuli�rement opportun de remettre en cause la pratique qui am�ne l'Office europ�en des brevets � se r�tribuer sur les brevets qu'il d�livre, dans la mesure o� cette pratique nuit au caract�re public de l'institution. <P>Dans sa r�solution<SUP>1</SUP> sur une d�cision de l'Office europ�en des brevets concernant la d�livrance du brevet n<SUP>o</SUP> EP 695 351, le 8d�cembre 1999, le Parlement europ�en a demand� la r�vision des r�gles de fonctionnement de l'Office de fa�on � garantir que cet organisme puisse publiquement rendre des comptes dans l'exercice de ses fonctions. <P><SUP>1</SUP>JO C 378 du 29.12.2000, p. 95.
rec7b	fr	Les logiciels, d'une part, jouent un r�le important dans de nombreuses industries et constituent d'autre part une forme fondamentale de cr�ation et d'expression. Les logiciels constituent aussi un domaine d'ing�nierie sp�cialis�e et une activit� humaine fondamentale, avec plus de 10 millions de d�veloppeurs professionnels dans le monde et des dizaines de millions de personnes qui cr�ent des logiciels � un titre ou � un autre. Les d�veloppeurs ind�pendants et les petites soci�t�s jouent un r�le fondamental dans l'innovation en cette mati�re. Il s'ensuit que les moyens utilis�s pour stimuler l'investissement dans les industries � forte intensit� logicielle ne doivent pas conduire � mettre en danger la capacit� de tous � devenir des cr�ateurs actifs et des usagers innovants de logiciels et qu'en particulier les brevets ne doivent pas permettre la monopolisation des outils d'expression, de cr�ation de diffusion et d'�change des informations et des connaissances.
rec11	fr	supprim�
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute invention susceptible d'application industrielle dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques nouvelles qui constituent une contribution technique, ainsi que d'autres caract�ristiques nouvelles ou non, et doivent �tre r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art2b	fr	(b)"contribution technique" d�signe une contribution, impliquant une activit� inventive, � un domaine technique, qui r�sout un probl�me technique existant ou �tend notablement l'�tat de la technique pour une personne du m�tier.
art3	fr	supprim�
art4_1	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur ne soit brevetable qu'� la condition qu'elle apporte une contribution technique d�finie � l'article 2, point b).
art4_2	fr	supprim�
art4_3	fr	3.Le caract�re notable de la contribution technique est �valu� en prenant en consid�ration la diff�rence entre les �l�ments techniques inclus dans l'objet de la revendication de brevet consid�r� dans son ensemble et l'�tat de la technique. Les �l�ments r�v�l�s par le demandeur d'un brevet pendant une p�riode de six mois pr�c�dant la date du d�p�t de la demande ne sont pas consid�r�s comme faisant partie de l'�tat de la technique, pour l'�valuation de cette revendication particuli�re.
art4_3a	fr	3 bis. Exclusions de la brevetabilit� <P>Une invention mise en oeuvre par ordinateur n'est pas consid�r�e comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, ou d'autres appareils. En cons�quence, ne sont pas brevetables les inventions impliquant des programmes d'ordinateurs, qui mettent en oeuvre des m�thodes pour l'exercice d'activit�s �conomiques, des m�thodes math�matiques ou d'autres m�thodes, si ces inventions ne produisent pas d'effets techniques en dehors de la manipulation et de la repr�sentation de l'information dans un syst�me ou un r�seau informatique.
art6a	fr	Article 6 bis <P>Les �tats membres veillent � ce que, lorsque le recours � une technique brevet�e est n�cessaire � la seule fin d'assurer la conversion des conventions utilis�es dans deux syst�mes ou r�seaux informatiques diff�rents, de fa�on � permettre entre eux la communication et l'�change de donn�es, ce recours ne soit pas consid�r� comme une contrefa�on de brevet.
art7	fr	La Commission surveille l'incidence de la protection par brevet des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes y compris le commerce �lectronique.
art8ca	fr	(c bis) si des difficult�s sont apparues dans la relation entre la protection par brevet des inventions mises en oeuvre par ordinateur et la protection des programmes d'ordinateur par le droit d'auteur, pr�vue par la directive 91/250/CEE.
rec5	it	� pertanto necessario armonizzare le disposizioni di legge che disciplinano la brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici, onde garantire che la certezza giuridica che ne risulter� e il livello dei requisiti richiesti per la brevettabilit� permettano alle imprese innovative di ricavare il massimo vantaggio dal loro processo inventivo e stimolare gli investimenti e l'innovazione.
rec7a	it	Il Parlamento europeo ha chiesto a pi� riprese che l'Ufficio europeo dei brevetti rivedesse le sue norme di funzionamento e che fosse soggetto a controllo pubblico nell'esercizio delle sue funzioni. In proposito, sarebbe particolarmente opportuno rimettere in discussione la prassi in base alla quale l'Ufficio percepisce introiti per i brevetti che rilascia, in quanto essa nuoce al carattere pubblico di tale organismo. <P>Nella sua risoluzione<SUP>1</SUP> sulla decisione dell'Ufficio europeo dei brevetti concernente il brevetto n. EP 695351 rilasciato l'8 dicembre 1999 il Parlamento europeo ha chiesto la revisione delle norme di funzionamento dell'Ufficio in questione, onde garantire che esso "sia soggetto a un obbligo di pubblicit� nell'esercizio delle sue funzioni". <P>______________________<P><SUP>1</SUP> GU C 378 del 29.12.2000, pag. 95.
rec7b	it	Il software da un lato svolge un ruolo importante in numerose industrie e dall'altro costituisce una forma fondamentale di creazione e di espressione. Il software � anche un settore ingegneristico specializzato nonch� un'attivit� umana fondamentale, con pi� di dieci milioni di progettisti professionisti nel mondo e decine di milioni di persone che sviluppano software, con uno scopo o un altro. I progettisti di software indipendenti e le piccole societ� svolgono un ruolo fondamentale ai fini dell'innovazione in questo settore. Ne consegue che i mezzi utilizzati per promuovere gli investimenti nelle industrie a forte intensit� di software non devono compromettere la capacit� di ciascuno di divenire creatore attivo e utente innovatore di software, e che, in particolare, i brevetti non devono consentire la monopolizzazione degli strumenti di espressione, creazione, diffusione e scambio di informazioni e conoscenze.
rec11	it	soppresso
art2a	it	a)"invenzione attuata per mezzo di elaboratori elettronici", un'invenzione atta ad un'applicazione industriale la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta una o pi� caratteristiche di novit� che costituiscono un contributo tecnico, nonch� altre caratteristiche di novit� o meno, e che devono essere realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art2b	it	b)"contributo tecnico", un contributo, implicante un'attivit� inventiva, ad un settore tecnico, che risolva un problema tecnico esistente o estenda lo stato dell'arte in misura significativa per una persona competente nella materia.
art3	it	soppresso
art4_1	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, solo a condizione che apporti un contributo tecnico quale definito nell'articolo 2, lettera b).
art4_2	it	soppresso
art4_3	it	3.La portata significativa del contributo tecnico � valutata considerando la differenza tra gli elementi tecnici inclusi nell'oggetto della rivendicazione di brevetto nel suo insieme e lo stato dell'arte. Gli elementi divulgati dal richiedente il brevetto nei sei mesi che precedono la data di deposito della domanda non sono considerati parte dello stato dell'arte al momento della valutazione della rivendicazione di cui trattasi.
art4_3a	it	Articolo 3 bis <P>Esclusione dalla brevettabilit� <P>Un'invenzione attuata per mezzo di un elaboratore elettronico non � considerata apportare un contributo tecnico per il solo fatto che implica l'uso di un elaboratore o di un altro apparecchio. Di conseguenza, non sono brevettabili le invenzioni che riguardano programmi per elaboratore che applicano metodi commerciali, matematici o di altro tipo senza produrre alcun effetto tecnico all'infuori della manipolazione e della rappresentazione dell'informazione nell'ambito di un sistema o di una rete di elaboratori.
art5	it	a)Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata solo come prodotto, ossia come dispositivo programmato o come processo tecnico di produzione.
art6a	it	Articolo 6 bis <P>Gli Stati membri assicurano che, nel caso in cui l'uso di una tecnica brevettata sia necessario al solo fine di garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, cos� da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto.
art7	it	La Commissione osserva gli effetti della protezione conferita dal brevetto per quanto riguarda le invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, compreso il commercio elettronico.
art8c	it	c)il verificarsi di difficolt� negli Stati membri nel caso in cui i criteri della novit� e dell'attivit� inventiva non siano esaminati prima del rilascio di un brevetto e le eventuali misure da adottare per risolvere tali difficolt�, e
art8ca	it	c bis) eventuali difficolt� insorte per quanto riguarda il rapporto tra la protezione brevettuale delle invenzioni attuate per mezzo di elaboratori elettronici e la protezione dei programmi per elaboratore mediante il diritto d'autore, ai sensi della direttiva 91/250/CEE.
rec5	nl	Om die reden moeten de rechtsregels betreffende de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen worden geharmoniseerd, om ervoor te zorgen dat de daaruit voortvloeiende rechtszekerheid en het peil van de voorwaarden waaraan voor octrooieerbaarheid moet worden voldaan, de ondernemingen in staat stellen optimaal profijt te trekken van hun uitvindingsproces en een stimulans zijn voor investeringen en innovatie.
rec7a	nl	Het Europees Parlement heeft er diverse malen op aangedrongen dat het Europees Octrooibureau zijn interne regels herziet en bij de uitoefening van zijn functies aan openbare controle wordt onderworpen. In dit verband zouden er met name vraagtekens geplaatst dienen te worden bij de praktijk van het Europees Octrooibureau om zich te laten betalen voor de octrooien die het afgeeft, aangezien dit schadelijk is voor het openbaar karakter van deze instelling. <P>Het Europees Parlement heeft in zijn resolutie<SUP>1</SUP> over het besluit van het Europees Octrooibureau met betrekking tot het op 8 december 1999 verleende octrooi EP 695 351 verlangd dat de interne regels van het Europees Octrooibureau zodanig worden herzien dat gewaarborgd is dat het bij de uitoefening van zijn functies openbare verantwoording kan afleggen.<P>___________</P><P ALIGN="JUSTIFY"><SUP>1</SUP> PB C 378 van 29.12.2000, blz. 95
rec7b	nl	Software speelt enerzijds een belangrijke rol in tal van bedrijfstakken en biedt anderzijds een fundamentele mogelijkheid tot creatief handelen en expressie. Software vormt ook een technisch vakgebied en is een terrein voor fundamentele menselijke activiteit, met meer dan 10 miljoen professionele softwareontwikkelaars in de wereld en tientallen miljoenen mensen die in een of andere hoedanigheid software cre�ren. Onafhankelijke ontwikkelaars en kleine bedrijven spelen een fundamentele rol bij de innovatie op dit gebied. Hieruit volgt dat de maatregelen ter bevordering van investeringen in sterk op software gerichte bedrijfstakken niemand de mogelijkheid mogen ontnemen om actief software te cre�ren en innovatief te gebruiken, en met name dat de octrooien er niet toe mogen leiden dat de instrumenten voor creatief handelen en expressie alsmede voor de verspreiding en uitwisseling van informatie en kennis worden gemonopoliseerd.
rec11	nl	Schrappen
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art2b	nl	b)"technische bijdrage"
art3	nl	Schrappen.
art4_1	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding alleen octrooieerbaar is op voorwaarde dat ze een technische bijdrage levert in de zin van artikel 2 onder b).
art4_2	nl	Schrappen.
art4_3	nl	3.Of de technische bijdrage significante omvang heeft, wordt beoordeeld door het bepalen van het verschil tussen de technische elementen omvat in de omvang van de in haar geheel beschouwde octrooiconclusie en de stand van de techniek. De elementen die door de octrooiaanvrager worden bekendgemaakt gedurende de periode van zes maanden voorafgaand aan de datum van de aanvraag zullen bij de beoordeling van de octrooiconclusie niet worden beschouwd als medebepalend voor de stand van de techniek
art4_3a	nl	3 bis. Uitsluiting van octrooieerbaarheid<P>Een in een computer ge�mplementeerde uitvinding wordt niet beschouwd als een technische bijdrage louter omdat zij het gebruik van een computer of een ander apparaat impliceert. Evenmin zijn uitvindingen octrooieerbaar waarbij computerprogramma's worden gebruikt die bedrijfsmethoden of wiskundige of andere methoden implementeren, zonder dat deze uitvindingen andere technische gevolgen teweegbrengen dan de behandeling en weergave van informatie binnen een computersysteem of een netwerk.
art6a	nl	Artikel 6 bis<P>De lidstaten zorgen ervoor dat gebruik van een geoctrooieerde techniek met als enig doel de omzetting van de conventies die in twee verschillende computersystemen of netwerken worden gebruikt om communicatie en gegevensuitwisseling tussen beide mogelijk te maken, niet wordt beschouwd als een inbreuk op het octrooi.
art7	nl	De Commissie volgt welke invloed de octrooibescherming voor in computers ge�mplementeerde uitvindingen heeft op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, met inbegrip van de elektronische handel.
art8ca	nl	c bis) eventuele moeilijkheden in de verhouding tussen de octrooibescherming voor in computers ge�mplementeerde uitvindingen en de bescherming van computerprogramma's uit hoofde van het auteursrecht, zoals omschreven in richtlijn 91/250/EG.
rec5	pt	Consequentemente, as normas jur�dicas que regem a patenteabilidade dos inventos que implicam programas de computador devem ser harmonizadas por forma a assegurar que a certeza jur�dica da� resultante e o n�vel dos requisitos exigidos para a patenteabilidade permitam �s empresas inovadoras tirarem o m�ximo partido do seu processo inventivo e dar um incentivo ao investimento e � inova��o;
rec7a	pt	O Parlamento j� solicitou, em diversas ocasi�es, que o Instituto Europeu de Patentes reveja as suas regras de funcionamento e que este organismo seja controlado publicamente no exerc�cio das suas fun��es. Nesta perspectiva, seria particularmente oportuno reconsiderar a pr�tica segundo a qual o Instituto Europeu de Patentes recebe pagamento pelas patentes que emite, na medida em que esta pr�tica prejudica o car�cter p�blico da institui��o.<P>Na sua resolu��o<SUP>1</SUP> sobre a decis�o do Instituto Europeu de Patentes relativa � patente nº EP 695 351, concedida em 8 de Dezembro de 1999, o Parlamento solicitou a revis�o das regras de funcionamento do Instituto por forma a garantir que este organismo possa prestar contas publicamente no exerc�cio das suas fun��es.</P><P ALIGN="JUSTIFY"><SUP>1</SUP>JO C 378 de 29.12.2000, p. 95.
rec7b	pt	Os programas inform�ticos, al�m de desempenharem um papel importante em in�meros sectores, constituem uma forma fundamental de criatividade e de express�o individual. Os programas inform�ticos constituem igualmente um dom�nio de engenharia especializada e uma actividade humana fundamental, contando com mais de 10 milh�es de programadores profissionais no mundo e dezenas de milh�es de pessoas que criam programas inform�ticos a um ou outro t�tulo. Os programadores independentes e as pequenas empresas desempenham um papel fundamental na inova��o nesta mat�ria. Por conseguinte, os meios utilizados para estimular o investimento nos sectores fortemente informatizados n�o devem dar origem a que seja colocada em risco a capacidade de todos os interessados poderem ser criadores activos e utilizadores inovadores de programas inform�ticos e, em particular, as patentes n�o devem permitir a monopoliza��o dos instrumentos de express�o individual, de criatividade, de difus�o e de troca de informa��es e de conhecimentos.
rec11	pt	Suprimido
art2a	pt	a)"invento que implica programas de computador" significa qualquer invento pass�vel de aplica��o industrial cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas que constituam uma contribui��o t�cnica, bem como outras caracter�sticas novas ou n�o, e tenham de ser realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art2b	pt	b)"contributo t�cnico" significa um contributo, envolvendo uma etapa inventiva, para um dom�nio t�cnico que solucione um problema t�cnico existente ou aumente o progresso tecnol�gico de modo significativo para uma pessoa competente na tecnologia.
art3	pt	Suprimido
art4_1	pt	1.Os Estados"Membros garantir�o a patenteabilidade de um invento que implique programas de computador, unicamente na condi��o de ele apresentar um contributo t�cnico, nos termos da al�nea b) do artigo 2º.
art4_2	pt	Suprimido
art4_3	pt	3.O alcance significativo do contributo t�cnico ser� avaliado considerando a diferen�a entre os elementos t�cnicos inclu�dos no �mbito da reivindica��o de patente considerada no seu conjunto e o progresso tecnol�gico. Os elementos divulgados pelo requerente da patente num per�odo de seis meses antes da data de apresenta��o do pedido n�o ser�o considerados parte do progresso tecnol�gico aquando da avalia��o da reivindica��o em causa.
art4_3a	pt	3 bis. Exclus�es da patenteabilidade<P>N�o deve considerar-se que um invento que implica programas de computador apresenta um contributo t�cnico apenas porque envolve a utiliza��o de um computador ou de outro aparelho. Por conseguinte, os inventos que implicam programas de computador e que implementam procedimentos comerciais, matem�ticos ou outros, n�o produzindo efeitos t�cnicos para al�m da manipula��o e representa��o de informa��es no sistema ou rede inform�ticos, n�o ser�o patente�veis.
art6a	pt	Artigo 6º bis<P>Os Estados-Membros garantir�o que, sempre que seja necess�ria a utiliza��o de uma t�cnica patenteada com o �nico fim de assegurar a convers�o das conven��es utilizadas em dois sistemas ou redes inform�ticos diferentes, por forma a permitir a comunica��o e a troca de dados entre eles, tal utiliza��o n�o seja considerada uma viola��o de patente.
art7	pt	A Comiss�o acompanhar� o impacto, na inova��o e na concorr�ncia, do direito das patentes em rela��o aos inventos que implicam programas de computador, tanto na Europa como a n�vel internacional, bem como nas empresas europeias, inclusive no com�rcio electr�nico.
art8ca	pt	(c bis) quaisquer dificuldades verificadas no que se refere � rela��o entre a protec��o conferida pelas patentes relativas a inventos que implicam programas de computador e a protec��o de programas de computador atrav�s dos direitos de autor, nos termos do disposto na Directiva 91/250/CEE.
rec5	sv	D�rf�r b�r de lagregler som reglerar patenterbarhet f�r datorrelaterade uppfinningar harmoniseras, s� att man ser till att den �tf�ljande r�ttss�kerheten och niv�n p� de krav som uppst�lls f�r patenterbarhet ger de nyskapande f�retagen m�jlighet att utnyttja sin innovativa process optimalt samt stimulerar investeringar och innovation.
rec7a	sv	Europaparlamentet har vid flera tillf�llen beg�rt att Europeiska patentverket skall se �ver sina verksamhetsregler och att verket skall kontrolleras offentligt i sitt arbete. Den praxis som inneb�r att Europeiska patentverket avl�nar sig sj�lv f�r de patent den utf�rdar kan s�rskilt ifr�gas�ttas, eftersom denna praxis skadar institutionens offentliga karakt�r. <P>I Europaparlamentets resolution om Europeiska patentverkets beslut om utf�rdande av patent nr EP 695351 den 8december 1999<SUP>1</SUP>, beg�rde parlamentet att Europeiska patentverkets verksamhetsbest�mmelser skulle ses �ver, s� att verket kan st�llas till svars offentligt d� den ut�var sitt uppdrag. <P><SUP>1</SUP> EGT C 378, 29.12.2000, s. 95.
rec7b	sv	Mjukvaror spelar en viktig roll i flera industrier, och de �r en grundl�ggande form av skapande och personligt uttryck. Mjukvarorna utg�r ocks� ett specialiserat omr�de f�r ingenj�rsvetenskapen och �r ett mycket viktigt verksamhetsomr�de, med mer �n 10 miljoner professionella utvecklare i v�rlden och tiotals miljoner m�nniskor som av olika anledningar skapar programvaror. De oberoende utvecklarna och sm�f�retagen spelar en grundl�ggande roll f�r innovation p� omr�det. Av det f�ljer att de medel som anv�nds f�r att fr�mja investeringar i industrier med h�gintensiv mjukvaruanv�ndning, inte f�r leda till att varje m�nniskas m�jlighet att bli en aktiv skapare och innovativ anv�ndare av mjukvaror �sidos�tts, och framf�r allt f�r patenten inte inneb�ra att man monopoliserar redskapen f�r personligt uttryck, skapande, spridning och utbyte av information och kunskaper.
rec11	sv	utg�r
art2a	sv	a)"datorrelaterad uppfinning"
art2b	sv	b)"tekniskt bidrag"
art3	sv	utg�r
art4_1	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning endast �r patenterbar p� villkor att den utg�r ett tekniskt bidrag i enlighet med artikel 2b.
art4_2	sv	utg�r
art4_3	sv	3.Omfattningen av det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan de tekniska k�nnetecken som ing�r i patentkravens samlade skyddsomf�ng och teknikens st�ndpunkt. De k�nnetecken som den patents�kande avsl�jar under de sex m�nader som f�reg�r ans�kningsdatumet skall inte anses ing� i teknikens st�ndpunkt n�r den aktuella patentans�kningen bed�ms.
art4_3a	sv	3a.Undantag fr�n patenterbarhet <P>En datorrelaterad uppfinning skall inte anses ge ett tekniskt bidrag bara f�r att den inbegriper anv�ndning av dator eller andra anordningar. F�ljaktligen skall patent inte kunna utf�rdas f�r uppfinningar som g�ller datorprogram som genomf�r aff�rsmetoder, matematiska metoder eller andra metoder, men inte ger n�gra tekniska effekter ut�ver att information bearbetas och �terges inom datorsystem eller n�tverk.
art5	sv	a)Medlemsstaterna skall se till att en datorrelaterad uppfinning enbart kan patentskyddas s�som anordning, dvs. en programmerad komponent eller en teknisk produktionsprocess.
art6a	sv	Artikel 6a <P>Medlemsstaterna skall, om anv�ndning av en patenterad teknik endast beh�vs f�r att garantera omvandling av de konventioner som anv�nds i tv� olika datorsystem eller n�tverk f�r att m�jligg�ra kommunikation och �msesidigt utbyte av uppgifter, se till att denna anv�ndning inte betraktas som patentintr�ng.
art7	sv	Kommissionen skall f�lja hur patentskyddet f�r datorrelaterade uppfinningar p�verkar innovation och konkurrens, b�de i Europa och internationellt, samt europeiskt n�ringsliv, �ven n�thandel.
art8ca	sv	ca)huruvida det eventuellt uppst�tt problem i f�rh�llandet mellan skyddet f�r patent p� datorrelaterade uppfinningar och det upphovsr�ttsliga skyddet f�r datorprogram i enlighet med direktiv91/250/EEG.
\.


--
-- Data for TOC entry 68 (OID 23763)
-- Name: itre_just; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY itre_just (id, lang, itre_just) FROM stdin;
rec5	da	Form�let med enhver lov vedr�rende udtagning af patent er ikke at sikre indehaverne af patenter en fordel
rec7a	da	Europa-Parlamentet har gentagne gange i forskellige beslutninger understreget, at Den Europ�iske Patentmyndigheds metoder b�r revideres. Den Europ�iske Patentmyndighed er ikke en EU-institution, og Europa-Parlamentet har tidligere rejst sp�rgsm�let om muligheden for at stille den til ansvar.
rec7b	da	Kr�ver ingen n�rmere forklaring.
rec11	da	Dette �ndringsforslag frems�ttes i �nsket om sammenh�ng med den r�dgivende ordf�rers �ndringsforslag 9. Den tekniske karakter af computer-implementerede opfindelser skal bevises og ikke tages for givet.
art2a	da	Den oprindelige definition af begrebet patenterbarhed er for bred. En computer-implementeret opfindelse b�r ikke betragtes som v�rende patenterbar udelukkende p� grund af den omst�ndighed, at der anvendes en computer, eller at det program, som gennemf�res ved hj�lp af en ikke-ny programmerbar maskine, er nyt. Der kr�ves et teknisk bidrag. Det er det tekniske aspekt, som kendetegner en opfindelse, til forskel fra en id�. Denne sondring er af st�rste betydning, ikke blot ud fra en teoretisk juridisk synsvinkel, men f�rst og fremmest med henblik p� at sikre, at konkurrencen inden for en �konomisk sektor ikke hindres af, at en enkelt operat�rs forretningsmetode eller praktiske viden opn�r en monopolstilling p� et givet marked.
art2b	da	Kravene om opfindelsesh�jde og teknisk fremskridt er en afg�rende foruds�tning, hvis trivielle "opfindelser" skal undg�s.
art3	da	Ordlyden i forslaget g�r det ganske enkelt umuligt at diskutere den tekniske karakter af en p�st�et opfindelse. Opfyldelsen af denne betingelse skal bevises, ikke tages for givet.
art4_1	da	Denne affattelse bringer artiklen i overensstemmelse med de foreg�ende �ndringsforslag.
art4_2	da	Dette stykke udg�r som konsekvens af de foreg�ende �ndringsforslag.
art4_3	da	P� omr�der som softwareudvikling og hermed relaterede aktiviteter, hvor udviklingen g�r hurtigt, og de fleste opfindelser kommer fra SMV'er, der undertiden er meget sm� og meget unge og i h�jere grad benytter sig af krydsbefrugtning end af advokatr�dgivning, er der brug for en "henstandsperiode", der kan sikre, at en opfinder ikke mister retten til sin opfindelse, hvis den p�g�ldende bringer den til offentlighedens kundskab nogle f� uger f�r indgivelsen af patentans�gningen, ofte for at teste, om der er interesse for opfindelsen p� markedet. Omtalen af en henstandsperiode er i tr�d med den verserende debat inden for generel patentret, eftersom der findes et lignende begreb i visse retssystemer (navnlig det amerikanske), men ikke i EU-lovgivningen og heller ikke i EPO's regler. Hvis man g�r software-opfindelser patenterbare i Europa og samtidig n�gter opfinderne den fleksibilitet, der ligger i muligheden for en tidlig offentligg�relse, vil man skabe un�dvendige flaskehalse p� bekostning af de innovative SMV'er og af samarbejdet mellem universiteter og virksomheder.
art4_3a	da	Reglen om, at en opfindelse, uanset dens genstand, i en patentretlig sammenh�ng kun kan betragtes som en s�dan, n�r den har reel indvirkning p� den virkelige verden, er et grundprincip i patentretten, hvilket i flere �rtier konstant er blevet bekr�ftet i s�vel lovgivning som domstolsafg�relser.
art6a	da	Muligheden for at forbinde udstyr, s�ledes at det bliver interoperabelt, er en m�de, hvorp� man kan sikre �bne net og forhindre misbrug af en dominerende stilling. Dette har is�r EF-Domstolen givet specifikt udtryk for i sin domspraksis. Patentretten b�r ikke g�re det muligt at tilsides�tte dette princip p� den frie konkurrences og brugernes bekostning.
art7	da	Ikke tildelingen af patentet, men overv�gningen af patentbeskyttelsen fra patentindehavers side vil vise, hvilken indvirkning patenter p� computer-implementerede opfindelser har for innovation og konkurrence.
art8ca	da	Kommissionen b�r i sin rapport komme ind p� eventuelle problemer, der er opst�et med hensyn til forholdet mellem patentbeskyttelse af computer-imlementerede opfindelser og beskyttelse af computerprogrammer via ophavsret som fastsat i direktiv 91/250/EF.
rec5	de	Es ist nicht das Ziel eines Gesetzes �ber die Patentierung, sicherzustellen, dass die Patentinhaber Nutzen daraus ziehen
rec7a	de	Das Europ�ische Parlament hat in verschiedenen Entschlie�ungen wiederholt hervorgehoben, dass die Praktiken des Europ�ischen Patentamts �berarbeitet werden sollten. Das Europ�ische Patentamt ist kein EU-Organ, und hinsichtlich seiner Rechenschaftspflicht hat das Europ�ische Parlament bereits zuvor Besorgnis ge�u�ert.
rec7b	de	Bedarf keiner Erl�uterung.
rec11	de	Dieser �nderungsantrag wird im Hinblick auf die logische �bereinstimmung mit �nderungsantrag9 der Verfasserin der Stellungnahme eingereicht. Der technische Charakter der computerimplementierten Erfindungen muss nachgewiesen werden und darf nicht als selbstverst�ndlich betrachtet werden.
art2a	de	Die urspr�ngliche Definition von Patentierbarkeit ist zu umfassend. Eine computerimplementierbare Erfindung sollte nicht blo� deshalb als patentierbar betrachtet werden, weil ein Computer verwendet wird oder das Programm, das in einer programmierbaren Vorrichtung, die nicht neu ist, abl�uft, neuartig ist. Ein technischer Beitrag ist erforderlich. Es ist der technische Aspekt, der f�r eine Erfindung im Unterschied zu einer Idee kennzeichnend ist. Diese Unterscheidung ist von gr��ter Bedeutung, nicht nur unter rechtstheoretischen Gesichtspunkten, sondern vor allem um zu gew�hrleisten, dass der Wettbewerb in einem Wirtschaftszweig nicht dadurch behindert wird, dass ein einziger Akteur auf einem bestimmten Markt eine bestimmte Gesch�ftsmethode oder praktische Kenntnis monopolisiert.
art2b	de	Die Voraussetzungen der erfinderischen T�tigkeit und der F�rderung des Stands der Technik sind von grundlegender Bedeutung, um die Patentierung von Bagatellerfindungen zu vermeiden.
art3	de	Die Formulierung des Vorschlags macht es einfach unm�glich, �ber den technischen Charakter einer beanspruchten Erfindung zu diskutieren. Diese Voraussetzung muss nachgewiesen werden und darf nicht als selbstverst�ndlich betrachtet werden.
art4_1	de	Diese Formulierung bringt den Artikel in Einklang mit den vorhergehenden �nderungsantr�gen.
art4_2	de	Dieser Absatz wird durch die vorhergehenden �nderungsantr�ge �berfl�ssig.
art4_3	de	In einem sich so rasant entwickelnden Bereich wie dem der Softwareindustrie und der softwarebezogenen Branchen, in denen die meisten Erfindungen von manchmal sehr kleinen und jungen KMU kommen, die sich mehr auf gegenseitige Befruchtung als auf den Rat von Anwaltskanzleien verlassen, ist eine sogenannte "Gnadenfrist" notwendig, um zu vermeiden, dass ein Erfinder seiner Erfindung verlustig geht, wenn er sie ein paar Wochen vor der Patentanmeldung publik macht, f�r gew�hnlich um die Attraktivit�t der Erfindung f�r den Markt zu testen. Der Hinweis auf eine Gnadenfrist �berschneidet sich mit einer laufenden Debatte �ber das allgemeine Patentrecht, da es in einigen Rechtsordnungen ein �hnliches Konzept gibt (insbesondere in den Vereinigten Staaten), nicht aber im EU-Recht und auch nicht in den Vorschriften des Europ�ischen Patentamts. W�rde in Europa die Patentierbarkeit von Softwareerfindungen eingef�hrt, w�hrend den Erfindern gleichzeitig die Flexibilit�t der fr�hzeitigen Bekanntgabe genommen wird, so entst�nde ein unn�tiger Engpass auf Kosten der innovativen KMU und der Zusammenarbeit zwischen Hochschulen und Unternehmen.
art4_3a	de	Die Regel, dass eine Erfindung ungeachtet ihres Gegenstands nur dann als Erfindung im Sinne des Patentrechts gilt, wenn sie reale Wirkungen auf die reale Welt hervorruft, ist ein Grundprinzip des Patentrechts, das �ber Jahrzehnte hinweg sowohl durch den Gesetzgeber als auch durch die Gerichte immer wieder bekr�ftigt worden ist.
art6a	de	Die M�glichkeit, Ger�te miteinander zu verbinden, um sie interoperabel zu machen, ist eine Methode, um f�r offene Netze zu sorgen und den Missbrauch marktbeherrschender Stellungen zu vermeiden. Diese spezifische Entscheidung findet sich insbesondere in der Rechtsprechung des Gerichtshofs der Europ�ischen Gemeinschaften. Das Patentrecht sollte nicht die M�glichkeit schaffen, diesen Grundsatz auf Kosten des freien Wettbewerbs und der Nutzer umzusto�en.
art7	de	Nicht die Erteilung des Patents an sich, sondern die Verfolgung des Patentschutzes seitens der Patentinhaber wird zeigen, welche Auswirkungen Patente computerimplementierter Erfindungen auf die Innovationst�tigkeit und den Wettbewerb haben.
art8ca	de	Der Bericht der Kommission sollte auch auf etwaige Schwierigkeiten eingehen, die im Hinblick auf das Verh�ltnis zwischen dem Patentschutz f�r computerimplementierte Erfindungen und dem Schutz von Computerprogrammen durch das Urheberrecht, wie es die Richtlinie 91/250/EWG vom 14. Mai 1991 �ber den Rechtsschutz von Computerprogrammen vorsieht, aufgetreten sind.
rec5	en	The object of any law relating to patenting is not to ensure that patent-holders enjoy an advantage
rec7a	en	Parliament has repeatedly said, in a number of resolutions, that the European Patent Office's practices need reforming. The European Patent Office is not a European Union institution. Parliament has raised the question of its accountability in the past.
rec7b	en	Self-explanatory.
rec11	en	Consistency with Amendment 9 by the draftswoman. The technical nature of computer-implemented inventions must be proved and not taken for granted.
art2a	en	The initial definition of patentability is too broad. A computer-implemented invention should not be considered patentable simply because a computer is used or because the program, performed on a programmable apparatus that is not novel itself, is novel. A technical contribution is required. It is the technical aspect which characterises an invention as opposed to an idea. This distinction is of the utmost importance, not only from a theoretical legal point of view, but above all to guarantee that competition in an economic sector is not hindered by the monopolisation of a given business method or practical knowledge by one operator only on a given market.
art2b	en	The conditions of inventive activity and advancement of the art are fundamental in order to avoid the patenting of trivial "inventions".
art3	en	The wording of the proposal makes it simply impossible to discuss the technical nature of a claimed invention. This condition has to be proved, and not taken for granted.
art4_1	en	This wording makes the article consistent with the previous amendments.
art4_2	en	This wording becomes redundant as a result of the previous amendments.
art4_3	en	In a rapidly moving field such as that of the software and software-related industries, where most inventions come from SMEs, sometimes very small and young which rely more on cross-fertilisation than on law firms' advice, a so-called "grace period" is necessary to avoid that an inventor is deprived of his/her invention when s/he has made it public a few weeks before applying for a patent, usually so as to test the invention's attractiveness to the market. The reference to a grace period overlaps with an on-going debate in general patenting law, as a similar concept exists in some legal systems (in particular the US), but not in the European Union legislation nor in the rules of the European Patent Office. Introducing patentability of software inventions in Europe, while depriving the inventors of the flexibility of early communication would create an unnecessary bottleneck at the expense of innovative SMEs and of university-enterprise co-operation.
art4_3a	en	The rule that an invention, whatever its scope, is only regarded as being an invention for the purposes of patent law when it has real effects on the real world, is a fundamental principle of patent law, as constantly confirmed over decades both in legislation and judicial decisions.
art6a	en	The possibility of connecting equipments so as to make them interoperable is a way of ensuring open networks and avoiding abuse of dominant positions. This has been specifically ruled in the case law of the Court of Justice of the European Communities in particular. Patent law should not make it possible to override this principle at the expense of free competition and users.
art7	en	What impact patents for computer-implemented inventions will have on innovation and competition will depend not on the granting of patents as such, but on how patent-holders enforce their patent protection.
art8ca	en	The Commission report should discuss any difficulties that have arisen with the relationship between patent protection by means of computer-implemented inventions and the protection of computer programs by means of copyright law, as laid down in Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs.
rec5	es	El objeto de cualquier ley relativa a la patente no es garantizar que los titulares de la patente tengan una ventaja
rec7a	es	El Parlamento Europeo ha se�alado varias veces y en diversas resoluciones la necesidad de revisar las pr�cticas de la Oficina Europea de Patentes. La Oficina Europea de Patentes no es una instituci�n de la Uni�n Europea y el Parlamento Europeo ya ha suscitado en el pasado la cuesti�n de su responsabilidad.
rec7b	es	Esta enmienda se explica por s� sola.
rec11	es	Esta enmienda se presenta por razones de coherencia con la enmienda 9 de la ponente. Debe probarse, y no darse por supuesto, el car�cter t�cnico de las invenciones implementadas en ordenador.
art2a	es	La definici�n inicial de patentabilidad es demasiado amplia. Una invenci�n implementable en ordenador no deber�a considerarse patentable por el solo hecho de que se utilice un ordenador, o de que sea nuevo el programa que se ejecuta en un aparato programable que no tenga el car�cter de novedad. Para ello es necesario aportar una contribuci�n t�cnica. Es el aspecto t�cnico el que caracteriza una invenci�n como algo opuesto a una idea. Esta distinci�n es de la m�xima importancia, no s�lo desde un punto de vista jur�dico te�rico sino, sobre todo, para garantizar que la competencia en un sector econ�mico no se ve menoscabada por la monopolizaci�n de un determinado m�todo comercial o industrial o de unos determinados conocimientos pr�cticos por parte de un solo operador en un mercado determinado.
art2b	es	Las condiciones de la actividad inventiva y el avance de la t�cnica son fundamentales para evitar las patentes de "invenciones" triviales.
art3	es	La redacci�n de la propuesta hace sencillamente imposible discutir el car�cter t�cnico de una supuesta invenci�n. Hay que demostrar esta condici�n, y no darla por supuesta.
art4_1	es	Esta redacci�n hace que el art�culo sea coherente con las enmiendas anteriores.
art4_2	es	Las enmiendas anteriores hacen que este apartado sea redundante.
art4_3	es	En un sector de gran dinamismo como el de las industrias de programas inform�ticos y las relacionadas con ellos, en el que la mayor�a de los inventos procede de PYME, a veces muy peque�as y j�venes, que conf�an m�s en la fertilizaci�n cruzada que en el asesoramiento de empresas jur�dicas, es necesario un llamado "per�odo de gracia" para evitar que un inventor se vea despose�do de su invenci�n cuando la haya hecho p�blica pocas semanas antes de solicitar una patente, normalmente para poner a prueba la atracci�n que la invenci�n puede tener en el mercado. La referencia a un per�odo de gracia se solapa con un debate en curso en el Derecho general sobre patentes, dado que existe un concepto similar en algunos sistemas jur�dicos (en particular en los Estados Unidos) pero no en la legislaci�n de la Uni�n Europea ni en las normas de la Oficina Europea de Patentes. La introducci�n de la patentabilidad de invenciones de programas inform�ticos en Europa, al tiempo que privar�a a los inventores de la flexibilidad de la comunicaci�n en una fase temprana, crear�a un innecesario embotellamiento a expensas de las PYME innovadoras y de la cooperaci�n universidad-empresa.
art4_3a	es	La norma de que una invenci�n, sea cual fuere su alcance, s�lo se contempla como tal a efectos de la legislaci�n sobre patentes cuando tiene consecuencias reales para el mundo real es un principio fundamental de la legislaci�n sobre patentes, tal como han confirmado constantemente a lo largo de d�cadas tanto la legislaci�n como las decisiones judiciales.
art6a	es	La posibilidad de conectar equipos de modo que sean interoperables es una manera de garantizar redes abiertas y de evitar el abuso de posiciones dominantes. Esta cuesti�n la ha regulado espec�ficamente la jurisprudencia del Tribunal de Justicia de las Comunidades Europeas. La legislaci�n sobre patentes no deber�a ofrecer la posibilidad de conculcar este principio a expensas de la libre competencia y de los usuarios.
art7	es	No ser� la concesi�n de la patente en s� misma, sino el recurso a la protecci�n de la patente por el titular de la misma, lo que mostrar� los efectos de las patentes de invenciones implementadas en ordenador sobre la actividad de innovaci�n y la competencia.
art8ca	es	El informe de la Comisi�n deber�a examinar tambi�n las eventuales dificultades surgidas en la relaci�n entre la protecci�n otorgada por patentes de invenciones implementadas en ordenador y la protecci�n de programas de ordenador derivada de derechos de autor con arreglo a lo dispuesto en la Directiva 91/250/CEE del Consejo, de 14 de mayo de 1991, sobre la protecci�n jur�dica de programas de ordenador.
rec5	fi	Patenttilakien tarkoituksena ei ole varmistaa, ett� patentinhaltijat saavat jotain etua, vaan patentinhaltijalle annettu etu on vain keino kannustaa innovointiprosessia koko yhteiskunnan hyv�ksi. Patentinhaltijalle annettu etu ei saa toimia t�t� patenttiperiaatteen perimm�ist� tavoitetta vastaan.
rec7a	fi	Euroopan parlamentti on useaan otteeseen eri p��t�slauselmissa korostanut, ett� Euroopan patenttiviraston k�yt�nt�j� olisi uudistettava. Euroopan patenttivirasto ei ole Euroopan unionin toimielin, ja Euroopan parlamentti on jo aiemmin ottanut esiin kysymyksen sen vastuusta.
rec7b	fi	Tarkistus ei kaipaa perusteluja.
rec11	fi	Tarkistus on looginen jatko valmistelijan tarkistukseen9. Tietokoneella toteutettujen keksint�jen tekninen luonne on n�ytett�v� toteen, eik� sit� voida pit�� itsest��n selv�n�.
art2a	fi	Patentoitavuuden alkuper�inen m��ritelm� on liian laaja. Tietokoneella toteutettavissa olevaa keksint�� ei tulisi pit�� patentoitavana vain siksi, ett� on k�ytetty tietokonetta, tai siksi, ett� tunnetulla ohjelmoitavalla laitteella suoritettava ohjelma on uusi. Keksinn�n on tuotava lis�ys tekniikan tasoon. Tekninen n�k�kohta erottaa keksinn�n ideasta. T�m�n eron tekeminen on eritt�in t�rke�� sek� teoreettiselta oikeudelliselta kannalta ett� ennen kaikkea sen takaamiseksi, ett� talousel�m�n kilpailua ei estet� antamalla monopoliasemaa jollekin tietylle liiketoiminnan menetelm�lle tai vain tietyill� markkinoilla toimivan yhden ainoan toimijan k�yt�nn�n tiedoille.
art2b	fi	Keksinn�llinen toiminta ja tekniikan tason edist�minen ovat t�rkeit� edellytyksi�, jotta v�ltet��n triviaalien "keksint�jen" patentoiminen.
art3	fi	Ehdotuksen sanamuoto ei mitenk��n mahdollista keskustelua v�itetyn keksinn�n teknisest� luonteesta. Asia on n�ytett�v� toteen, eik� sit� voida pit�� itsest��n selv�n�.
art4_1	fi	Sanamuoto tekee artiklasta johdonmukaisen edelt�viin tarkistuksiin verrattuna.
art4_2	fi	Sanamuoto tekee artiklasta johdonmukaisen edelt�viin tarkistuksiin verrattuna.
art4_3	fi	Nopeasti muuttuvalla alalla, kuten ohjelmistoteollisuudessa ja siihen liittyvill� aloilla, joilla suurin osa keksinn�ist� tulee joskus hyvinkin pienist� ja nuorista pk-yrityksist�, jotka luottavat enemm�n vuorovaikutukseen kuin lakifirmojen neuvoihin, tarvitaan ns. uutuuden suoja-aika (grace period), jotta keksij� ei menet� keksint���n, jos h�n julkistaa sen muutamaa viikkoa ennen patentin hakemista testatakseen keksinn�n markkinakelpoisuutta. Viittaus uutuuden suoja-aikaan liittyy meneill��n olevaan keskusteluun yleisest� patentointilaista, koska joissakin oikeusj�rjestyksiss� (varsinkin USA:ssa) on vastaava k�site, mutta ei EU:n lains��d�nn�ss� eik� Euroopan patenttiviraston s��nn�iss�. Jos Euroopassa otetaan k�ytt��n ohjelmistojen patentoitavuus, mutta ei anneta keksij�ille mahdollisuutta kertoa keksinn�st��n ajoissa, luodaan tarpeeton pullonkaula, joka haittaa innovatiivisia pk-yrityksi� ja yritysten kansainv�list� yhteisty�t�.
art4_3a	fi	S��nt�, jonka mukaan keksint� katsotaan suojapiirist� riippumatta patenttilain tarkoittamaksi keksinn�ksi, jos sill� on todellisia vaikutuksia todellisessa el�m�ss�, on patenttilain perusperiaate, joka on vahvistettu sek� lains��d�nn�ss� ett� oikeusk�yt�nn�ss�.
art6a	fi	Mahdollisuus kytke� laitteistoja toisiinsa niiden yhteentoimivuuden aikaansaamiseksi on keino varmistaa verkkojen avoimuus ja v�ltt�� m��r��v�n markkina-aseman v��rink�ytt�. T�st� on esimerkkej� Euroopan yhteis�jen tuomioistuimen oikeusk�yt�nn�ss�. Patenttilakien ei pid� antaa mahdollisuutta t�m�n periaatteen rikkomiseen vapaan kilpailun ja k�ytt�jien kustannuksella.
art7	fi	Ei niink��n patentin my�nt�minen, vaan se, miten patentinhaltija k�ytt�� patenttisuojaa, tulee osoittamaan, mit� vaikutuksia tietokoneella toteutettujen keksint�jen patenttisuojalla on innovaatiokykyyn ja kilpailuun.
art8ca	fi	Komission kertomuksessa olisi k�sitelt�v� my�s mahdollisia vaikeuksia, joita on aiheutunut tietokoneella toteutettujen keksint�jen patenttisuojan ja 14 p�iv�n� toukokuuta 1991 annetun direktiivin 91/250/ETY mukaisen tietokoneohjelmien tekij�noikeussuojan v�lisest� suhteesta.
rec5	fr	L'objet de toute l�gislation sur la brevetabilit� n'est pas d'assurer un avantage aux titulaires de brevets
rec7a	fr	Le Parlement europ�en a, � plusieurs reprises, dans diverses r�solutions, soulign� que les pratiques de l'Office europ�en des brevets devaient �tre r�form�es. L'Office europ�en des brevets n'est pas une institution de l'Union europ�enne, et la question de sa responsabilit� a �t� soulev�e par le pass� par le Parlement europ�en.
rec7b	fr	Se justifie de lui-m�me.
rec11	fr	Cet amendement est pr�sent� dans un souci de logique avec l'amendement 9 de la rapporteure. La nature technique des inventions mises en oeuvre par ordinateur doit �tre prouv�e, et non tenue pour acquise.
art2a	fr	La d�finition initiale de la brevetabilit� est trop large. Une invention mise en oeuvre par ordinateur ne devrait pas �tre consid�r�e comme brevetable du seul fait qu'un ordinateur est utilis�, ou que le programme qui s'ex�cute sur un appareil programmable non nouveau est nouveau. Une contribution technique est requise. C'est l'aspect technique qui caract�rise une invention, par opposition � une id�e. Cette distinction est de la plus haute importance, non seulement du point de vue de la th�orie du droit, mais avant tout pour garantir que la concurrence dans un secteur �conomique ne soit pas entrav�e par la monopolisation d'une m�thode relative � l'exercice d'une activit� �conomique ou de connaissances pratiques par un seul op�rateur sur un march� donn�.
art2b	fr	Les conditions d'activit� inventive et d'avancement de la technique sont fondamentales pour �viter de breveter des "inventions" triviales.
art3	fr	La formulation de la proposition ne permet pas d'examiner le caract�re technique d'une invention revendiqu�e. Cette condition doit �tre prouv�e, et non consid�r�e comme remplie d'office.
art4_1	fr	Cette formulation rend l'article coh�rent avec les amendements pr�c�dents.
art4_2	fr	Les amendements pr�c�dents rendent ce paragraphe superflu.
art4_3	fr	Dans un domaine en �volution rapide comme le logiciel et les industries associ�es au logiciel, o� la plupart des inventions proviennent de PME, parfois tr�s petites et tr�s jeunes, qui ont davantage recours � la fertilisation crois�e qu'aux conseils de cabinets juridiques, une "p�riode de gr�ce" est n�cessaire pour �viter qu'un inventeur soit priv� de son invention lorsqu'il/elle l'a rendue publique quelques semaines avant de d�poser une demande de brevet, g�n�ralement dans le but de v�rifier l'int�r�t du march� pour cette invention. La mention d'une p�riode de gr�ce correspond � un d�bat en cours dans le droit g�n�ral des brevets, �tant donn� qu'un concept similaire existe dans certains syst�mes juridiques (en particulier aux �tats-Unis), mais non dans la l�gislation de l'Union europ�enne, ni dans les r�gles de l'Office europ�en des brevets. Introduire la brevetabilit� des inventions en mati�re de logiciels en Europe, tout en privant les inventeurs de la souplesse que conf�re une communication anticip�e, cr�erait un goulet d'�tranglement inutile, aux d�pens des PME innovatrices et de la coop�ration entre universit� et entreprise.
art4_3a	fr	La r�gle selon laquelle une invention, quelle que soit sa port�e, n'est consid�r�e comme une invention aux fins du droit des brevets que lorsqu'elle a des effets concrets sur le monde r�el constitue un principe fondamental du droit des brevets, constamment confirm�, depuis des d�cennies, dans la l�gislation et les d�cisions judiciaires.
art6a	fr	La possibilit� de connecter des �quipements pour les rendre interop�rables est une fa�on de garantir des r�seaux ouverts et d'�viter les abus de position dominante. Ceci a �t� sp�cifi� en particulier dans la jurisprudence de la Cour de justice des Communaut�s europ�ennes. Le droit des brevets ne devrait pas permettre de violer ce principe, au pr�judice de la libre concurrence et des utilisateurs.
art7	fr	Ce n'est pas l'octroi du brevet en soi mais l'utilisation par son titulaire de la protection qu'il lui conf�re qui d�montrera les effets que les brevets accord�s aux inventions mises en oeuvre par ordinateur exercent sur l'innovation et la concurrence.
art8ca	fr	Le rapport de la Commission devrait �galement se pencher sur les difficult�s �ventuelles survenues dans la relation entre la protection par brevet des inventions mises en oeuvre par ordinateur et la protection des programmes d'ordinateur par le droit d'auteur, pr�vue par la directive 91/250/CEE du Conseil du 14 mai 1991 concernant la protection juridique des programmes d'ordinateur.
rec5	it	L'obiettivo di qualsiasi normativa in materia di brevetti non � di garantire che il titolare del brevetto benefici di un vantaggio; il vantaggio che gli � concesso � solo un modo per incoraggiare il processo inventivo a beneficio della societ� nel suo complesso. I vantaggi concessi al titolare di un brevetto non devono andare contro questo obiettivo ultimo del principio dei brevetti.
rec7a	it	Il Parlamento europeo ha sottolineato a pi� riprese, in varie risoluzioni, la necessit� di una riforma delle prassi seguite dall'Ufficio europeo dei brevetti. Detto Ufficio non � un'istituzione dell'Unione europea e in passato il Parlamento europeo aveva gi� sollevato la questione della sua responsabilit�.
rec7b	it	L'emendamento si spiega da s�.
rec11	it	L'emendamento � dettato da un'esigenza di coerenza con l'emendamento 9 della relatrice per parere. Il carattere tecnico delle invenzioni attuate per mezzo di elaboratori elettronici deve essere provato e non dato per scontato.
art2a	it	La definizione originaria di brevettabilit� � troppo ampia. In particolare, un'invenzione attuabile per mezzo di un elaboratore elettronico non dovrebbe essere considerata brevettabile per il semplice fatto che viene utilizzato un elaboratore o che su un apparecchio programmabile privo di caratteristiche di novit� viene eseguito un programma che presenta invece tali caratteristiche. � necessario che vi sia un contributo tecnico. � l'aspetto tecnico che caratterizza un'invenzione e la distingue da un'idea. Tale distinzione � della massima importanza, non solo dal punto di vista della teoria giuridica, ma soprattutto per garantire che la concorrenza in un settore economico non sia ostacolata dalla monopolizzazione di un determinato metodo commerciale o di determinate conoscenze pratiche da parte di un operatore solo in un certo mercato.
art2b	it	I requisiti dell'attivit� inventiva e dell'avanzamento dell'arte sono fondamentali per evitare il rilascio di brevetti per "invenzioni" insignificanti.
art3	it	La formulazione della proposta rende semplicemente impossibile discutere il carattere tecnico di un'invenzione rivendicata. Tale condizione deve essere provata, e non data per scontata.
art4_1	it	La formulazione proposta rende la disposizione coerente con gli emendamenti che precedono.
art4_2	it	Alla luce degli emendamenti che precedono il paragrafo risulta superfluo.
art4_3	it	In un settore in rapido movimento come quello del software e delle industrie correlate, in cui la maggior parte delle invenzioni provengono da PMI talvolta molto piccole e di recente costituzione che fanno affidamento pi� sull'"impollinazione incrociata" che sui consigli degli studi legali, un "periodo di grazia" � necessario per evitare che un inventore sia privato della sua invenzione nel caso in cui l'abbia resa pubblica alcune settimane prima di depositare la sua domanda di brevetto, di solito con lo scopo di testare l'attrattiva dell'invenzione sul mercato. Il riferimento ad un "periodo di grazia" coincide con un dibattito in corso nell'ambito del diritto generale dei brevetti, dal momento che un concetto simile esiste in alcuni sistemi giuridici (in particolare negli Stati Uniti), ma non nella legislazione dell'Unione europea n� fra le regole dell'Ufficio europeo dei brevetti. Introdurre la brevettabilit� delle invenzioni in materia di software privando gli inventori della flessibilit� di una comunicazione anticipata creerebbe un'inutile strozzatura a scapito delle PMI innovatrici e della cooperazione fra universit� e impresa.
art4_3a	it	La regola secondo cui un'invenzione, di qualunque portata, � considerata tale ai fini del diritto dei brevetti solo quando produce effetti concreti sul mondo reale � un principio fondamentale del diritto dei brevetti, costantemente confermato, da decenni, a livello sia della legislazione che delle decisioni giudiziarie.
art6a	it	La possibilit� di collegare impianti cos� da renderli interoperabili � un modo per garantire reti aperte ed evitare abusi di posizione dominante. Ci� � stato espressamente riconosciuto in particolare dalla giurisprudenza della Corte di giustizia delle Comunit� europee. Il diritto dei brevetti non dovrebbe consentire che tale principio sia calpestato a scapito della libera concorrenza e degli utenti.
art7	it	Non sar� il rilascio del brevetto in s� e per s�, bens� il ricorso alla protezione conferita dal brevetto da parte del titolare a mostrare quali effetti avr� sull'innovazione e la concorrenza il brevetto di invenzioni attuate per mezzo di elaboratori elettronici.
art8ca	it	La relazione della Commissione dovrebbe analizzare anche le eventuali difficolt� insorte per quanto riguarda il rapporto tra la protezione brevettuale delle invenzioni attuate per mezzo di elaboratori elettronici e la protezione dei programmi per elaboratore mediante il diritto d'autore, ai sensi della direttiva 91/250/CEE del Consiglio, del 14 maggio 1991, relativa alla tutela giuridica dei programmi per elaboratore.
rec5	nl	Het doel van een octrooiwet is niet ervoor te zorgen dat de octrooihouders een voordeel genieten
rec7a	nl	Het Europees Parlement heeft er in verschillende resoluties al enkele malen op gewezen dat de praktijken van het Europees Octrooibureau hervorming behoeven. Het Europees Octrooibureau is geen instelling van de EU en de verantwoordelijkheid van dit orgaan is in het verleden aan de orde gesteld door het Europees Parlement.
rec7b	nl	Spreekt voor zich.
rec11	nl	Dit amendement is een logische aanvulling op amendement 9 van de rapporteur. De technische aard van in computers ge�mplementeerde uitvindingen moet worden bewezen en kan niet zomaar worden aangenomen.
art2a	nl	De oorspronkelijke definitie van octrooieerbaarheid is te ruim. Een in een computer ge�mplementeerde uitvinding mag niet als octrooieerbaar worden beschouwd alleen omdat gebruik is gemaakt van een computer of het programma dat op een niet-nieuw programmeerbaar apparaat tot stand is gekomen, nieuw is. Er is ook een technische bijdrage vereist. Het technische aspect is kenmerkend voor een uitvinding, in tegenstelling tot een idee. Dit onderscheid is van erg groot belang, niet alleen vanuit theoretisch juridisch oogpunt, maar vooral om te garanderen dat de mededinging in een economische sector niet wordt belemmerd door de monopolisering van een bepaalde bedrijfsmethode of praktische kennis door een enkele speler op een bepaalde markt.
art2b	nl	De voorwaarden van uitvindingsactiviteit en een verbetering van de stand van de techniek zijn fundamenteel als men octrooien voor triviale "uitvindingen" wil voorkomen.
art3	nl	De formulering van het voorstel maakt het simpelweg onmogelijk het technische karakter van een gemelde uitvinding te bespreken. Dat aan deze voorwaarde is voldaan, moet worden bewezen en kan niet zomaar worden aangenomen.
art4_1	nl	Om de formulering van het artikel te laten aansluiten bij die van de voorgaande amendementen.
art4_2	nl	Deze formulering wordt als gevolg van de voorgaande amendementen overbodig.
art4_3	nl	Op een snel evoluerend terrein als dat van software en de met software verband houdende industrie, waar de meeste uitvindingen afkomstig zijn van soms erg kleine en jonge bedrijven, die veeleer afhankelijk van kruisbestuiving dan van adviezen van advocatenkantoren, is een soort van "vrije periode" nodig om te voorkomen dat een uitvinder zijn/haar uitvinding afhandig wordt gemaakt, wanneer hij/zij deze een paar weken voor de octrooiaanvraag bekendmaakt, hetgeen vaak gebeurt om de marktinteresse te peilen. Deze vrije periode past in het kader van een debat dat op dit ogenblik op het gebied van het octrooirecht aan de gang is, aangezien een dergelijk concept in bepaalde rechtsstelsels bestaat (met name in de Verenigde Staten), maar niet in de EU-regelgeving, noch in de regels van het Europees Octrooibureau. Als de octrooieerbaarheid van software-uitvindingen in Europa wordt ingevoerd en de uitvinders een soepele regeling met een mogelijkheid van vroege bekendmaking wordt ontzegd, ontstaat er een onnodig knelpunt die nadelig is voor innovatieve kleine en middelgrote bedrijven en voor de samenwerking tussen universiteit en bedrijfsleven.
art4_3a	nl	Dat een uitvinding ongeacht haar omvang alleen als uitvinding wordt beschouwd indien zij re�le gevolgen heeft voor de werkelijkheid is een fundamenteel beginsel in het octrooirecht, dat al decennialang permanent wordt bevestigd, zowel in de regelgeving als in de rechtspraak.
art6a	nl	De mogelijkheid apparatuur te koppelen om interoperabiliteit tot stand te brengen, is een manier om tot open netwerken te komen en misbruik van dominante posities te voorkomen. Dit is specifiek gesteld in rechterlijke uitspraken, met name van het Hof van Justitie van de Europese Gemeenschappen. Het octrooirecht mag het niet mogelijk maken dat dit beginsel terzijde wordt geschoven ten koste van de vrije mededinging en van de gebruikers.
art7	nl	Niet de toekenning van het octrooi als zodanig, maar de uitoefening van de octrooibescherming door de octrooihouders zal duidelijk maken welke gevolgen de octrooien voor in computers ge�mplementeerde uitvindingen hebben voor innovatie en mededinging.
art8ca	nl	De Commissie dient in haar verslag ook in te gaan op de moeilijkheden die zich eventueel hebben voorgedaan in de verhouding tussen de octrooibescherming voor in computers ge�mplementeerde uitvindingen en de bescherming van computerprogramma's uit hoofde van het auteursrecht, zoals omschreven in richtlijn 91/250/EG.
rec5	pt	O objectivo de qualquer lei relativa ao registo de patentes n�o � assegurar que os titulares de patentes beneficiem de uma vantagem
rec7a	pt	O Parlamento j� afirmou, em in�meras resolu��es, que as pr�ticas do Instituto Europeu de Patentes devem ser reformadas. O Instituto Europeu de Patentes n�o � uma institui��o da Uni�o Europeia. A quest�o da sua responsabilidade j� foi levantada no passado pelo Parlamento.
rec7b	pt	O texto da altera��o �, por si s�, elucidativo.
rec11	pt	Esta altera��o � apresentada por raz�es de coer�ncia com a altera��o 9 da relatora de parecer. A natureza t�cnica dos inventos que implicam programas de computador deve ser provada, e n�o considerada um dado adquirido.
art2a	pt	A defini��o inicial de patenteabilidade � demasiado vasta. Um invento que implica programas de computador n�o deveria ser considerado patente�vel pelo simples facto de ser utilizado um computador ou de ser novo o programa executado num aparelho program�vel n�o novo. � necess�ria uma contribui��o t�cnica. � o aspecto t�cnico que caracteriza um invento, e n�o uma ideia. Esta distin��o � de extrema import�ncia, n�o s� do ponto de vista te�rico e jur�dico, mas, acima de tudo, para garantir que a concorr�ncia num sector econ�mico n�o seja prejudicada pela monopoliza��o, por parte de um operador, de um determinado procedimento comercial ou conhecimento pr�tico num �nico mercado.
art2b	pt	As condi��es da actividade inventiva e da promo��o da tecnologia s�o fundamentais com vista a evitar a concess�o de patentes a "inventos" triviais.
art3	pt	A redac��o da proposta torna completamente imposs�vel discutir a natureza t�cnica de um alegado invento. Esta condi��o tem de ser provada, e n�o tomada como certa.
art4_1	pt	Esta redac��o torna o artigo consistente com as altera��es anteriores.
art4_2	pt	Este texto torna-se redundante, em consequ�ncia das altera��es anteriores.
art4_3	pt	Num dom�nio em r�pida evolu��o como o das ind�strias de software e conexas, em que a maior parte dos inventos prov�m de pequenas e m�dias empresas, por vezes muito pequenas e jovens, que contam mais com a "poliniza��o cruzada" do que com os conselhos de gabinetes jur�dicos, � necess�rio um "per�odo de gra�a" para evitar que um inventor seja privado do seu invento quando o tiver divulgado poucas semanas antes de reivindicar uma patente, geralmente com vista a testar a aceita��o do invento no mercado. A refer�ncia a um per�odo de gra�a coincide com um debate em curso sobre a regulamenta��o geral das patentes, j� que existe um conceito semelhante em alguns sistemas jur�dicos (especialmente nos EUA), mas n�o na legisla��o da Uni�o Europeia nem nas normas do Instituto Europeu de Patentes. A introdu��o da patenteabilidade dos inventos de software na Europa, embora privasse os inventores da flexibilidade de uma comunica��o atempada, criaria um bloqueio desnecess�rio, em detrimento das pequenas e m�dias empresas inovadoras e da coopera��o entre universidades e empresas.
art4_3a	pt	A norma segundo a qual um invento, qualquer que seja o seu alcance, s� deve ser considerado um invento para efeitos da regulamenta��o sobre as patentes quando produz efeitos reais sobre o mundo real � um princ�pio fundamental da regulamenta��o em mat�ria de patentes, constantemente confirmado ao longo de d�cadas, tanto na legisla��o como nas decis�es judiciais.
art6a	pt	A possibilidade de ligar equipamentos com vista a torn�-los interoper�veis � uma forma de assegurar as redes abertas e de evitar o abuso de posi��es dominantes. Esta possibilidade foi prevista, nomeadamente, na jurisprud�ncia do Tribunal de Justi�a das Comunidades Europeias. A legisla��o em mat�ria de patentes n�o deve permitir que este princ�pio seja violado em detrimento da livre concorr�ncia e dos utilizadores.
art7	pt	O impacto das patentes relativas aos inventos que implicam programas de computador na inova��o e na concorr�ncia n�o depende da concess�o da patente em si, mas sim do modo como o titular da patente aplica a protec��o da mesma.
art8ca	pt	O relat�rio da Comiss�o deveria referir quaisquer dificuldades verificadas no que se refere � rela��o entre a protec��o conferida pelas patentes relativas a inventos que implicam programas de computador e a protec��o de programas de computador atrav�s dos direitos de autor, nos termos do disposto na Directiva 91/250/CEE, de 14 de Maio de 1991, relativa � protec��o jur�dica dos programas de computador.
rec5	sv	Syftet med patentr�tten �r inte alltid att ge patentinnehavaren en f�rdel
rec7a	sv	Europaparlamentet har vid flera tillf�llen i olika resolutioner understrukit att Europeiska patentverkets praxis borde f�r�ndras. Europeiska patentverket �r inte en EU-institution, och fr�gan om dess ansvar har tidigare tagits upp av Europaparlamentet.
rec7b	sv	Ingen motivering beh�vs.
rec11	sv	Detta �ndringsf�rslag g�rs f�r att skapa logik med �ndringsf�rslag 9 fr�n f�redraganden. En datorrelaterad uppfinnings tekniska karakt�r m�ste bevisas, inte tas f�r given.
art2a	sv	
art2b	sv	Kraven p� innovativ verksamhet och bidrag till teknikens st�ndpunkt �r viktiga f�r att undvika att triviala "uppfinningar" patenteras.
art3	sv	Enligt det nuvarande f�rslaget �r det inte m�jligt att diskutera en uppfinnings tekniska k�nnetecken. Dessa k�nnetecken m�ste bevisas och kan inte tas f�r givet.
art4_1	sv	Denna ordalydelse g�r att artikeln st�mmer �verens med tidigare �ndringsf�rslag.
art4_2	sv	De tidigare �ndringsf�rslagen g�r denna punkt �verfl�dig.
art4_3	sv	Utvecklingen sker snabbt inom mjukvarubranschen och inom relaterade industrier. De flesta uppfinningarna kommer fr�n sm� och medelstora f�retag. Dessa f�retag �r ibland rentav mycket sm� och nystartade och f�rlitar sig mer p� korsbefruktning �n p� advokatbyr�ers r�dgivning. P� detta omr�de beh�vs d�rf�r en s.k. "skonfrist" (grace period) f�r att undvika att en uppfinnare fr�ntas sin uppfinning om han/hon offentliggjort den n�gra veckor innan han/hon ans�ker om patent, n�got som uppfinnare ofta g�r f�r att testa marknadens intresse f�r uppfinningen. H�nvisningen till en skonfrist sammanfaller med en p�g�ende debatt n�r det g�ller allm�n patentr�tt, eftersom ett liknande begrepp finns i vissa r�ttssystem (s�rskilt i USA) men inte i vare sig EU:s lagstiftning eller Europeiska patentbyr�ns best�mmelser. Om man inf�r patenterbarhet f�r uppfinningar p� mjukvaruomr�det i Europa och samtidigt fr�ntar uppfinnarna flexibiliteten i systemet med tidiga meddelanden, skulle det uppst� on�diga flaskhalsar. Detta skulle inverka menligt p� innovativa sm� och medelstora f�retag och p� samarbetet mellan universitet och n�ringslivet.
art4_3a	sv	En grundregel inom patentr�tten �r att en uppfinning, oavsett till�mpningsomr�de, endast betraktas som en uppfinning i patentr�ttslig mening om den har konkreta effekter i praktiken. Denna grundregel har under flera �rtionden bekr�ftats i lagstiftningen och i olika r�ttsliga beslut.
art6a	sv	M�jligheten att sammankoppla anordningar f�r att g�ra dem kompatibla �r ett s�tt att garantera �ppna n�tverk och f�rebygga missbruk av dominerande st�llning. Detta har uttryckligen fastslagits i EG-domstolens r�ttspraxis. Patentr�tten b�r inte g�ra det m�jligt att �sidos�tta denna princip p� den fria konkurrensens och anv�ndarnas bekostnad.
art7	sv	Det �r inte utf�rdandet av patent i sig, utan uppf�ljningen av patentskyddet fr�n patentinnehavarnas sida som kommer att visa hur patent p� datorrelaterade uppfinningar p�verkar innovation och konkurrens.
art8ca	sv	Kommissionens rapport b�r �ven ta upp eventuella problem som uppst�tt i f�rh�llandet mellan skyddet f�r patent p� datorrelaterade uppfinningar och det upphovsr�ttsliga skyddet f�r datorprogram i enlighet med direktiv91/250/EEG av den 14 maj 1991 om r�ttsligt skydd f�r datorprogram.
\.


--
-- Data for TOC entry 69 (OID 23783)
-- Name: parl_cec; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY parl_cec (id, lang, parl_cec) FROM stdin;
rec6	da	F�llesskabet og dets medlemsstater er bundet af aftalen om handelsrelaterede intellektuelle ejendomsrettigheder (TRIPS), der blev godkendt ved R�dets afg�relse 94/800/EF af 22. december 1994 om indg�else p� Det Europ�iske F�llesskabs vegne af de aftaler, der er resultatet af de multilaterale forhandlinger i Uruguay-rundens regi (1986-1994), for s� vidt ang�r de omr�der, der h�rer under F�llesskabets kompetence. I henhold til artikel 27, stk. 1, i TRIPS-aftalen skal alle opfindelser p� alle teknologiske omr�der, b�de produkter og processer, kunne patenteres, forudsat at de er nye, har opfindelsesh�jde og kan anvendes industrielt. I henhold til TRIPS-aftalen skal det desuden v�re muligt at opn� patentrettigheder og anvende disse uden forskelsbehandling p� grund af teknologisk omr�de. Disse principper b�r f�lgelig finde anvendelse p� computer-implementerede opfindelser.
art2a	da	a) ved en 'computer-implementeret opfindelse' forst�s enhver opfindelse, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere prima facie nye karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art2b	da	b) ved et 'teknisk bidrag' forst�s et bidrag til det aktuelle tekniske niveau, som ikke er indlysende for en fagmand.
art3	da	Artikel 3 <P>Computer-implementerede opfindelser som teknologisk omr�de <P>Medlemsstaterne sikrer, at en computer-implementeret opfindelse betragtes som henh�rende under et teknologisk omr�de.
art4	da	Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny og har opfindelsesh�jde. <P>Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag. <P>Det tekniske bidrag vurderes som forskellen mellem patentkravets genstand som helhed, hvoraf nogle dele kan omfatte b�de tekniske og ikke-tekniske karakteristika, og det aktuelle tekniske niveau.
art5_1	da	Medlemsstaterne sikrer, at der kan ans�ges om patent p� en computer-implementeret opfindelse som et produkt, dvs. som en programmeret computer, et programmeret computernet eller en anden programmeret maskine, eller som en proces, der udf�res af computeren, computernettet eller maskinen ved k�rsel af software.
art8b	da	b) hvorvidt reglerne for fastl�ggelse af kriterierne for patenterbarhed, herunder is�r nyhedskarakter, opfindelsesh�jde og patentkravets genstand, er fyldestg�rende, og
art9_1	da	Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at efterkomme dette direktiv senest den [DATO (sidste dag i en m�ned)]. De underretter straks Kommissionen herom.
rec5	de	Aus diesen Gr�nden sollten die Rechtsvorschriften, so wie sie von den Gerichten in den Mitgliedstaaten ausgelegten werden, vereinheitlicht und die Vorschriften �ber die Patentierbarkeit computerimplementierter Erfindungen transparent gemacht werden. Die <I>dadurch gew�hrte</I> Rechtssicherheit sollte dazu f�hren, dass Unternehmen den gr��tm�glichen Nutzen aus Patenten f�r computerimplementierte Erfindungen ziehen, und sie sollte Anreize f�r Investitionen und Innovationen schaffen.
rec6	de	Die Gemeinschaft und ihre Mitgliedstaaten sind auf das �bereinkommen �ber handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-�bereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 �ber den Abschluss der �bereink�nfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europ�ischen Gemeinschaft in Bezug auf die in ihre Zust�ndigkeiten fallenden Bereiche . Nach Artikel 27 Absatz 1 des TRIPS-�bereinkommens sollen Patente f�r Erfindungen auf allen Gebieten der Technik erh�ltlich sein, sowohl f�r Erzeugnisse als auch f�r Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen T�tigkeit und sind gewerblich anwendbar. Gem�� dem TRIPS-�bereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erh�ltlich sein und Patentrechte ausge�bt werden k�nnen. Diese Grunds�tze sollten demgem�� auch f�r computerimplementierte Erfindungen gelten.
rec11	de	Zwar werden computerimplementierte Erfindungen einem Gebiet der Technik zugerechnet, aber <I>um</I> das Kriterium der erfinderischen T�tigkeit zu erf�llen, sollten sie wie alle Erfindungen einen technischen Beitrag zum Stand der Technik leisten.
rec17	de	Diese Richtlinie ber�hrt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 <I>EG-Vertrag</I>.
art2a	de	a) 'Computerimplementierte Erfindung' ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art2b	de	b) 'Technischer Beitrag' ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der f�r eine fachkundige Person nicht nahe liegend ist.
art3	de	Artikel 3<P>Gebiet der Technik</P><P>Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugeh�rig gilt.
art4	de	Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen T�tigkeit beruht. <P>Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet. <P>Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmale umfassen kann, vom Stand der Technik abhebt.
art5_1	de	Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf�hrung von Software verwirklicht wird.
art8b	de	b) die Angemessenheit der Regeln f�r die Festlegung der Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die Neuheit, die erfinderische T�tigkeit und den eigentlichen Patentanspruch, und
rec1	en	The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. <I>effective </I>and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.
rec6	en	The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.
art2a	en	(a) 'computer-implemented invention' means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;
art2b	en	(b) 'technical contribution' means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.
art3	en	Article 3 <P>Computer-implemented inventions as a field of technology <P>Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.
art4	en	Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step. <P>Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution. <P>The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.
art5_1	en	Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.
art6	en	Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.
art8b	en	(b) whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and
art9_1	en	Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.
rec6	es	La Comunidad y sus Estados miembros son signatarios del Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual relacionados con el Comercio (ADPIC), aprobado mediante la Decisi�n 94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la celebraci�n en nombre de la Comunidad Europea, por lo que respecta a los temas de su competencia, de los acuerdos resultantes de las negociaciones multilaterales de la Ronda Uruguay (1986-1994)<A NAME="FROMEXPL_040203_1_3" HREF="#EXPL_040203_1_3" TITLE=" DO L 336 de 23.12.1994, p. 1."><SUP>(1)</SUP></A>. El apartado 1 del art�culo 27 del acuerdo ADPIC establece que las patentes podr�n obtenerse por todas las invenciones, sean de productos o de procedimientos, en todos los campos de la tecnolog�a, siempre que sean nuevas, entra�en una actividad inventiva y sean susceptibles de aplicaci�n industrial. Adem�s, seg�n este mismo Acuerdo, las patentes se podr�n obtener y los derechos de patente se podr�n gozar sin discriminaci�n por el campo de la tecnolog�a. En consecuencia, estos principios deben aplicarse a las invenciones implementadas en ordenador.
rec11	es	Aunque se considera que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a, para que entra�en una actividad inventiva, como las invenciones en general, <I>deber�n</I> aportar una contribuci�n t�cnica al estado de la t�cnica.
art2a	es	(a) 'invenci�n implementada en ordenador', toda invenci�n para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas prima facie que se realicen total o parcialmente mediante un programa o programas de ordenador;
art2b	es	(b) 'contribuci�n t�cnica', una contribuci�n al estado de la t�cnica en un campo tecnol�gico que no sea evidente para un experto en la materia.
art3	es	Art�culo 3 <P>Invenciones implementadas en ordenador como campo de la tecnolog�a <P>Los Estados miembros garantizar�n que se considere que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a.
art4	es	Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables a condici�n de que sean nuevas, supongan una actividad inventiva y sean susceptibles de aplicaci�n industrial. <P>Los Estados miembros garantizar�n que, como condici�n para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deban aportar una contribuci�n t�cnica. <P>La contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y el �mbito de la reivindicaci�n de la patente considerada en su conjunto, que puede incluir tanto caracter�sticas t�cnicas como no t�cnicas.
art5_1	es	Los Estados miembros garantizar�n que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red inform�tica programada u otro aparato programado, o como procedimiento realizado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa.
art6	es	Los actos permitidos en el marco de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular sus preceptos relativos a la descompilaci�n y la interoperabilidad, o las disposiciones relativas a la topograf�a de los productos semiconductores o las marcas comerciales, no se ver�n afectados por la protecci�n que las patentes otorgan a las invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva.
art7	es	La Comisi�n seguir� de cerca el impacto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, incluido el comercio electr�nico.
art8b	es	b) si las normas que rigen la determinaci�n de los requisitos de patentabilidad, y m�s concretamente la novedad, la actividad inventiva y el �mbito concreto de las reivindicaciones, son adecuadas; y
art9_1	es	Los Estados miembros adoptar�n, a m�s tardar el [FECHA (�ltimo d�a de un mes)], las disposiciones legislativas, reglamentarias y administrativas necesarias para dar cumplimiento a lo dispuesto en la presente Directiva. Informar�n de ello inmediatamente a la Comisi�n.
rec6	fi	Euroopan yhteis� ja sen j�senvaltiot ovat sitoutuneet noudattamaan sopimusta teollis- ja tekij�noikeuksien kauppaan liittyvist� n�k�kohdista (j�ljemp�n� 'TRIPS-sopimus'), joka on hyv�ksytty 22 p�iv�n� joulukuuta 1994 tehdyll� neuvoston p��t�ksell� 94/800/EY Uruguayn kierroksen monenv�lisiss� kauppaneuvotteluissa (1986-1994) laadittujen sopimusten tekemisest� Euroopan yhteis�n puolesta yhteis�n toimivaltaan kuuluvissa asioissa. TRIPS-sopimuksen 27 artiklan 1 kohdan mukaan milt� tekniikan alalta tahansa olevaa tuotetta tai menetelm�� koskevan keksinn�n tulee olla patentoitavissa edellytt�en, ett� se on uusi ja keksinn�llinen ja ett� sit� voidaan k�ytt�� teollisesti hyv�ksi. Lis�ksi TRIPS-sopimuksen mukaan patenttien saaminen ja patenttioikeuksista nauttiminen eiv�t saa olla riippuvaisia siit�, mik� tekniikanala on kyseess�. N�it� periaatteita olisi sovellettava my�s tietokoneella toteutettuihin keksint�ihin.
art2a	fi	a) 'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman ensi n�kem�lt� uuden piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art2b	fi	b) 'Lis�yksell� tekniikan tasoon' tarkoitetaan sellaista lis�yst� tekniikanalalla vallitsevaan tasoon, joka ei ole ilmeinen alan ammattimiehelle.
art3	fi	3 artikla <P>Tietokoneella toteutetut keksinn�t osana tekniikanalaa <P>J�senvaltioiden on varmistettava, ett� tietokoneella toteutettua keksint�� pidet��n johonkin tekniikanalaan kuuluvana.
art4	fi	J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi ja keksinn�llinen. <P>J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon. <P>Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen, jonka osat voivat k�sitt�� sek� teknisi� ett� muita kuin teknisi� piirteit�, ja vallitsevan tason v�lill�.
art5	fi	
art8b	fi	b) siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; ja
art9_1	fi	J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n [ p�iv�n� kuuta (kuukauden viimeinen p�iv�)]. Niiden on ilmoitettava t�st� komissiolle viipym�tt�.
rec6	fr	La Communaut� et ses �tats membres sont li�s par l'accord relatif aux aspects des droits de propri�t� intellectuelle qui touchent au commerce (ADPIC), approuv� par la d�cision 94/800/CE du Conseil, du 22 d�cembre 1994, relative � la conclusion au nom de la Communaut� europ�enne, pour ce qui concerne les mati�res relevant de ses comp�tences, des accords des n�gociations multilat�rales du cycle de l'Uruguay (1986-1994). L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra �tre obtenu pour toute invention, de produit ou de proc�d�, dans tous les domaines techniques, � condition qu'elle soit nouvelle, qu'elle implique une activit� inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent �tre obtenus et des droits de brevets exerc�s sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur.
rec18	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
art2a	fr	<P>a) 'invention mise en oeuvre par ordinateur' d�signe toute invention dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou <I>d'autre</I> appareil programmable et pr�sentant une ou plusieurs caract�ristiques � premi�re vue nouvelles qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;</P>
art2b	fr	<P>b) 'contribution technique' d�signe une contribution � l'�tat de la technique dans un domaine technique, qui n'est pas �vidente pour une personne du m�tier.</P>
art3	fr	Article 3<P>Domaine technique <P>Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit consid�r�e comme appartenant � un domaine technique.
art4	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activit� inventive. <P>Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique. <P>La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet de la revendication de brevet consid�r� dans son ensemble, dont les �l�ments peuvent comprendre des caract�ristiques techniques et non techniques, et l'�tat de la technique.
art5_1	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme.
art6	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
art8b	fr	<P>b) si les r�gles r�gissant la d�termination des crit�res de brevetabilit� en ce qui concerne plus pr�cis�ment la nouveaut�, l'activit� inventive et la port�e des revendication sont ad�quates
art9_1	fr	Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent imm�diatement la Commission.
rec5	it	
rec6	it	La Comunit� e i suoi Stati membri sono parti dell'accordo sugli aspetti dei diritti di propriet� intellettuali attinenti al commercio, approvato con la decisione del Consiglio 94/800/CE, del 22 dicembre 1994, relativa alla conclusione a nome della Comunit� europea, per le materie di sua competenza, degli accordi dei negoziati multilaterali dell'Uruguay Round (1986-1994). L'articolo 27, paragrafo 1 di detto accordo dispone che possono costituire oggetto di brevetto tutte le invenzioni, di prodotti o di processi, in tutti i campi della tecnologia, che presentino carattere di novit�, implichino un'attivit� inventiva e siano atte ad un'applicazione industriale. Inoltre, in base all'accordo, i brevetti possono essere ottenuti e i relativi diritti possono essere esercitati senza discriminazioni quanto al settore della tecnologia. Questi principi valgono di conseguenza per le invenzioni attuate per mezzo di elaboratori elettronici.
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non deve richiedere una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici, con le modifiche o le integrazioni relative a specifici aspetti richieste dalla presente direttiva.
art2a	it	(a) 'invenzione attuata per mezzo di elaboratori elettronici', un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o pi� caratteristiche di novit� che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art2b	it	b) 'contributo tecnico', un contributo allo stato dell'arte in un settore tecnico, giudicato non ovvio da una persona competente nella materia.
art3	it	Articolo 3 <P>Appartenenza ad un settore della tecnologia <P>Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.
art4	it	Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e implichi un'attivit� inventiva. <P>Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico. <P>Il contributo tecnico � valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.
art5_1	it	Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata come prodotto, ossia come elaboratore programmato, rete di elaboratori programmati o altro apparecchio programmato, o come processo realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software.
art8b	it	(b) l'adeguatezza delle norme che determinano i criteri di brevettabilit�, in particolare la novit�, l'attivit� inventiva e l'oggetto delle rivendicazioni
art9_1	it	Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro il [DATA (ultimo giorno di un mese)]. Essi ne informano immediatamente la Commissione.
rec6	nl	De Gemeenschap en haar lidstaten zijn gebonden door de Overeenkomst inzake de handelsaspecten van de intellectuele eigendom (TRIPS-Overeenkomst), goedgekeurd bij Besluit 94/800/EG van de Raad van 22 december 1994 betreffende de sluiting, namens de Europese Gemeenschap voor wat betreft de onder haar bevoegdheid vallende aangelegenheden, van de uit de multilaterale handelsbesprekingen in het kader van de Uruguay-Ronde (1986-1994) voortvloeiende overeenkomsten<SUP>1</SUP>. Artikel 27, lid 1, van de TRIPS-Overeenkomst bepaalt dat octrooi kan worden verleend voor uitvindingen, producten dan wel werkwijzen, op alle gebieden van de technologie, mits zij nieuw zijn, op uitvinderswerkzaamheid berusten en vatbaar zijn voor industri�le toepassing. Bovendien kan volgens de TRIPS-Overeenkomst octrooi worden verleend en kunnen octrooirechten worden genoten zonder onderscheid op grond van het gebied van de technologie. Deze beginselen moeten dienovereenkomstig gelden voor in computers ge�mplementeerde uitvindingen.
rec7	nl	Overeenkomstig het Verdrag inzake de verlening van Europese octrooien, ondertekend in M�nchen op 5 oktober 1973, en de octrooiwetten van de lidstaten worden computerprogramma's alsmede ontdekkingen, natuurwetenschappelijke theorie�n, wiskundige methoden, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten. Deze uitzondering is echter alleen van toepassing en gerechtvaardigd voorzover een octrooiaanvraag of octrooi betrekking heeft op deze onderwerpen of werkzaamheden als zodanig, omdat de genoemde onderwerpen en werkzaamheden als zodanig niet tot een gebied van de technologie behoren.
art2a	nl	a) 'in computers ge�mplementeerde uitvinding'
art2b	nl	b) 'technische bijdrage'
art3	nl	Artikel 3<P>In computers ge�mplementeerde uitvindingen als een gebied van de technologie</P><P>De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding wordt beschouwd als behorende tot een gebied van de technologie.
art4	nl	De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is en op uitvinderswerkzaamheid berust. <P>De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert. <P>De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.
art5_1	nl	De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding kan worden geclaimd als product, dat wil zeggen als een geprogrammeerde computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat, of als een werkwijze die door zo een computer, computernetwerk of apparaat middels het uitvoeren van software wordt toegepast.
art8b	nl	(b) de vraag of de regels betreffende het bepalen van de octrooieerbaarheidseisen, meer bepaald nieuwheid, uitvinderswerkzaamheid en de passende omvang van de conclusies, adequaat zijn; en
art9_1	nl	De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om uiterlijk op [DATUM (laatste dag van een maand)] aan deze richtlijn te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis.
rec5	pt	Consequentemente, as normas jur�dicas, conforme interpretadas pelos tribunais dos Estados-Membros, devem ser harmonizadas e a lei que rege a patenteabilidade dos inventos que implicam programas de computador deve tornar-se transparente. A certeza jur�dica da� resultante deve permitir �s empresas tirarem o m�ximo partido das patentes dos inventos que implicam programas de computador e dar um incentivo ao investimento e � inova��o.
rec6	pt	A Comunidade e os seus Estados-Membros est�o vinculados pelo Acordo sobre os Aspectos dos Direitos de Propriedade Intelectual Relacionados com o Com�rcio (TRIPS), aprovado pela Decis�o 94/800/CE do Conselho, de 22 de Dezembro de 1994, relativa � celebra��o, em nome da Comunidade Europeia e em rela��o �s mat�rias da sua compet�ncia, dos acordos resultantes das negocia��es multilaterais do Uruguay Round (1986/1994)<SUP>1</SUP>. O artigo 27� do Acordo TRIPS prev�, no seu n� 1, que dever�o existir patentes para quaisquer inventos, quer se trate de produtos ou processos, em todos os dom�nios da tecnologia, desde que esses inventos sejam novos, impliquem uma actividade inventiva e sejam suscept�veis de aplica��o industrial. Al�m disso, segundo o Acordo TRIPS, deve haver direitos de patentes que possam ser usufru�dos sem discrimina��o quanto ao dom�nio da tecnologia. Tais princ�pios devem, nesse sentido, aplicar-se a todos os inventos que implicam programas de computador; <P>____________<P><SUP>1</SUP>�����JO L 336 de 23.12.1994, p. 1.
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhoraria se fossem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos que implicam programas de computador e se a situa��o jur�dica fosse transparente.
rec17	pt	A presente directiva aplicar-se-� sem preju�zo das regras de concorr�ncia, em particular dos artigos 81� e 82� do Tratado.
rec18	pt	Os actos permitidos ao abrigo da Directiva 91/250/CEE relativa � protec��o jur�dica dos programas de computador, em particular das suas disposi��es relacionadas com a descompila��o e a interoperabilidade ou das disposi��es relativas a topografias de semicondutores ou a marcas comerciais, n�o ser�o afectados pela protec��o concedida pelas patentes aos inventos no �mbito de aplica��o da presente directiva.
art2a	pt	a) 'invento que implica programas de computador' significa qualquer invento cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas, � primeira vista, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art2b	pt	b) 'contributo t�cnico' significa um contributo para o progresso tecnol�gico num dom�nio t�cnico que n�o seja �bvio para uma pessoa competente na tecnologia.
art3	pt	Artigo 3� <P>Inventos que implicam programas de computador enquanto dom�nio da tecnologia <P>Os Estados-Membros assegurar�o que um invento que implica programas de computador seja considerado como pertencendo a um dom�nio da tecnologia.
art4	pt	Os Estados-Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva. <P>Os Estados-Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva. <P>O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito da reivindica��o de patente considerada no seu conjunto, cujos elementos possam incluir caracter�sticas t�cnicas e n�o t�cnicas, e o progresso tecnol�gico.
art5_1	pt	Os Estados-Membros garantir�o que um invento que implica programas de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda como processo executado por esse computador, rede inform�tica ou aparelho, pela execu��o do software.
art8b	pt	b) se as normas que regem a determina��o dos requisitos de patenteabilidade e, mais especificamente, de novidade, de actividade inventiva e do �mbito apropriado das reivindica��es, s�o adequadas;
art9_1	pt	Os Estados-Membros por�o em vigor as disposi��es <I>legislativas</I>, regulamentares e administrativas necess�rias para darem cumprimento � presente directiva at� [DATA (�ltimo dia de um determinado m�s)]. Desse facto informar�o imediatamente a Comiss�o.
rec6	sv	Gemenskapen och dess medlemsstater �r bundna av avtalet om handelsrelaterade aspekter av immaterialr�tter (TRIPS), som godk�ndes genom r�dets beslut av den 22 december 1994 om att i Europeiska gemenskapens namn, i fr�gor inom dess beh�righet, sluta avtal enligt de multilaterala f�rhandlingarna inom Uruguayrundan (1986-1994)<SUP>1</SUP>. Artikel 27.1 i TRIPS stadgar att patent skall meddelas f�r alla uppfinningar, s�v�l anordningar som f�rfaranden, p� alla teknikomr�den, f�rutsatt att de �r nya, har uppfinningsh�jd och kan tillgodog�ras industriellt. Vidare skall det, enligt TRIPS, g� att erh�lla och utnyttja patentskydd p� alla teknikomr�den utan diskriminering. Dessa principer b�r g�lla f�ljdenligt f�r datorrelaterade uppfinningar. <P><SUP>1</SUP> EGT L 336, 23.12.1994, s. 1.
rec17	sv	Detta direktiv skall inte inverka p� till�mpningen av konkurrensreglerna, s�rskilt <I>artikel 81 och 82</I> i f�rdraget.
art2a	sv	a) 'datorrelaterad uppfinning'
art2b	sv	b) 'tekniskt bidrag'
art3	sv	Artikel 3 <P>Datorrelaterade uppfinningar som teknikomr�de <P>Medlemsstaterna skall se till att en datorrelaterad uppfinning uppfattas som tillh�rande ett teknikomr�de.
art4	sv	Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny och har uppfinningsh�jd. <P>Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag. <P>Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samlade skyddsomf�ng, vari s�v�l tekniska som icke-tekniska k�nnetecken kan ing�, och teknikens st�ndpunkt.
art5_1	sv	Medlemsstaterna skall se till att en datorrelaterad uppfinning kan patentskyddas s�som anordning, dvs. en programmerad dator, ett programmerat n�tverk eller n�gon annan programmerad anordning, eller s�som f�rfarande utf�rt av ovann�mnda dator, n�tverk eller anordning genom att en mjukvara k�rs p� anordningen.
art8b	sv	b) huruvida reglerna f�r bed�mning av patenterbarhet, n�rmare best�mt nyhet, uppfinningsh�jd och l�mpligt skyddsomf�ng f�r kraven, �r �ndam�lsenliga, och
art9_1	sv	Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast den och skall genast underr�tta kommissionen om detta.
\.


--
-- Data for TOC entry 70 (OID 23814)
-- Name: cult; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cult (id, lang, cult) FROM stdin;
rec7a	da	Edb-programmer spiller p� den ene side en vigtig rolle inden for en lang r�kke erhvervsgrene og udg�r p� den anden side en v�sentlig frembringelses- og udtryksform.
rec7b	da	I sin beslutning (offentliggjort i EFT C 378 af 29.12.2000, s. 95) om Den Europ�iske Patentmyndigheds afg�relse vedr�rende patent EP 695 351, udstedt den 8. december 1999, kr�vede Europa-Parlamentet en revision af Den Europ�iske Patentmyndigheds aktiviteter for at sikre, at den bliver offentligt ansvarlig ved ud�velsen af sine funktioner.
art2a	da	(a)ved en "computer-implementeret opfindelse" forst�s enhver teknisk l�sning, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere <I>prima facie </I>nye karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art2b	da	(b)ved et "teknisk bidrag" eller en "teknisk opfindelse" eller "opfindelse" forst�s en l�re om forholdet mellem �rsag og virkning ved anvendelsen af kontrollerbare naturkr�fter. Anvendelse af naturkr�fter til at kontrollere fysiske virkninger ud over numerisk repr�sentation af information betragtes som henh�rende under et teknologisk omr�de. Behandling, h�ndtering og freml�ggelse af information betragtes ikke, selv om der benyttes teknisk udstyr hertil, som henh�rende under et teknologisk omr�de.
art3	da	udg�r
art4_1	da	1.Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny¸ ikke indlysende, har opfindelsesh�jde og henh�rer under et teknologisk omr�de.
art4_2	da	2.Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag, dvs. at opfindelsen udg�r en ny l�re om forholdet mellem �rsag og virkning ved kontrolleret anvendelse af naturkr�fter.
art4_3	da	3.Det tekniske bidrag vurderes som forskellen mellem patentkravets genstands tekniske karakteristika som helhed og det aktuelle tekniske niveau.
art5	da	Medlemsstaterne sikrer, at der kun kan ans�ges om patent p� en computer-implementeret opfindelse som et produkt, dvs. som en programmeret computer, et programmeret computernet eller en anden programmeret maskine, eller som en teknisk produktionsproces, der kontrolleres af computeren, computernettet eller maskinen ved k�rsel af software.
art6	da	udg�r
art6a	da	Artikel 6a<P>De rettigheder, der tildeles ved hj�lp af patenter p� opfindelser, som falder inden for dette direktivs anvendelsesomr�de, ber�rer ikke handlinger, der er tilladt som undtagelser i henhold til direktiv 91/250/E�F om retlig beskyttelse af edb-programmer ved ophavsret, navnlig de handlinger, der er beskrevet og udt�mmende opregnet i artikel 5, stk. 2, artikel 5, stk. 3, og artikel 6 i direktiv 91/250/E�F.
art8ca	da	(ca)hvorvidt de til Den Europ�iske Patentmyndighed uddelegerede bef�jelser er forenelige med kravene om harmonisering af EU-lovgivningen og med principperne om gennemsigtighed og ansvarlighed.
rec7a	de	Software spielt in vielen Industriezweigen eine wichtige Rolle und ist zugleich eine grundlegende Form von Werksch�pfung und Ausdruck.
rec7b	de	In seiner (im ABl. C378 vom 29.12.2000, S.95, ver�ffentlichten) Entschlie�ung zu dem Beschluss des Europ�ischen Patentamts bez�glich des am 8. Dezember 1999 erteilten Patents Nr. EP 695351 forderte das Europ�ische Parlament "eine �berpr�fung der T�tigkeiten des EPA, um zu gew�hrleisten, dass es einer �ffentlichen Rechenschaftspflicht unterliegt".
art2a	de	(a)"Computerimplementierte Erfindung" ist jede technische L�sung, zu deren Implementierung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art2b	de	(b)"Technischer Beitrag" oder "technische Erfindung" oder "Erfindung" ist eine Lehre �ber die Beziehung zwischen Ursache und Wirkung in der Nutzung kontrollierbarer Kr�fte der Natur. Die Nutzung der Kr�fte der Natur zur Beherrschung der physikalischen Wirkungen �ber die numerische Darstellung der Informationen hinaus geh�rt zu einem Gebiet der Technik. Die Verarbeitung, die Bearbeitung und die Darstellungen von Informationen geh�ren nicht zu einem Gebiet der Technik, selbst wenn daf�r technische Vorrichtungen verwendet werden.
art3	de	Entf�llt.
art4_1	de	1.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar, neu und nicht nahe liegend ist, auf einer erfinderischen T�tigkeit beruht und einem Gebiet der Technik zuzuordnen ist.
art4_2	de	2.Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet, das hei�t, wenn sie eine neue Lehre �ber die Beziehungen zwischen Ursache und Wirkung in der kontrollierten Nutzung der Kr�fte der Natur beinhaltet.
art4_3	de	Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand der technischen Merkmale des Patentanspruchs in seiner Gesamtheit vom Stand der Technik abhebt.
art5	de	Die Mitgliedstaaten stellen sicher, dass Anspr�che auf eine computerimplementierte Erfindung nur als Erzeugnisanspr�che erhoben werden k�nnen, das hei�t auf einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung, oder aber als Verfahrensanspruch auf ein technisches Produktionsverfahren, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf�hrung von Software kontrolliert wird.
art6	de	Entf�llt
art6a	de	Artikel 6 a <P>Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, k�nnen die im Sinne der Richtlinie 91/250 �ber den Rechtsschutz von Computerprogrammen durch das Urheberrecht als Ausnahme gestatteten Handlungen, insbesondere die in Artikel 5 Absatz 2, Artikel 5 Absatz 3 und Artikel 6 dieser Richtlinie 91/250 beschriebenen Handlungen, nicht ber�hren.
art8ca	de	(ca) die Vereinbarkeit der dem EPA �bertragenen Befugnisse mit den Erfordernissen der Harmonisierung des EU-Rechts in Verbindung mit den Grunds�tzen der Transparenz und Rechenschaftspflicht.
rec7a	en	Software plays a key role in many industries and, moreover, is a fundamental means of creation and expression.
rec7b	en	In its resolution (published in OJ C378, 29.12.2000, p. 95) on a decision by the EPO with regard to patent No EP695351 granted on 8 December 1999, the European Parliament demanded a review of the EPO to ensure that it becomes publicly accountable in the exercise of its functions.
art2a	en	(a)"computer-implemented invention" means any technical solution the implementation of which involves the use of a computer, computer network or other programmable apparatus and having one or more <I>prima facie</I> novel features which are realised wholly or partly by means of a computer program or computer programs;
art2b	en	(b)"technical contribution" means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.
art3	en	deleted
art4_1	en	1.Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, non-obvious, involves an inventive step<B>, <I>and belongs to a technical field.
art4_2	en	2.Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution, that is to say, it must impart a new lesson in the relationships of cause and effect involved in the controlled use of natural forces.
art4_3	en	3.The technical contribution shall be assessed by consideration of the difference between the scope of the technical features of the patent claim considered as a whole and the state of the art.
art5	en	Member States shall ensure that the forms of claims in respect of a computer-implemented invention may be made only to the effect that the invention is a product, that is a programmed computer, a programmed computer network or other programmed apparatus, or a technical production process controlled by such a computer, computer network or apparatus through the execution of software.
art6	en	deleted
art6a	en	Article 6a <P>The rights conferred by patents granted for inventions within the scope of this Directive shall be without prejudice to acts permitted by way of exception under Directive91/250/EEC on the legal protection of computer programs by copyright, in particular the acts specified and described in the closed list set out in Articles 5(2) and (3) and 6 of Directive91/250/EEC.
art8ca	en	(ca)whether the powers delegated to the EPO are compatible with requirements for harmonisation of the EU legislation, together with the principles of transparency and accountability.
rec7a	es	Los programas inform�ticos desempe�an un importante papel en numerosas industrias y, por otra parte, constituyen una forma fundamental de creaci�n y expresi�n.
rec7b	es	En su Resoluci�n (publicada en el DO C 378 el 29.12.2000, p. 95) relativa a una decisi�n de la OEP sobre la patente n�mero EP 695 315, concedida el 8 de diciembre de 1999, el Parlamento Europeo solicit� que se reexamine la OEP para garantizar que sea p�blicamente responsable en el ejercicio de sus funciones.
art2a	es	(a)«invenci�n implementada en ordenador», toda soluci�n t�cnica para cuya implementaci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas <I>prima facie </I>que se realicen total o parcialmente mediante un programa o programas de ordenador;
art2b	es	(b)«contribuci�n t�cnica», "invenci�n t�cnica" o "invenci�n", un conocimiento sobre la relaci�n de causa a efecto en la utilizaci�n de las fuerzas de la naturaleza controlables. La utilizaci�n de las fuerzas de la naturaleza para controlar los efectos f�sicos, m�s all� de la representaci�n digital de las informaciones, pertenece a un campo tecnol�gico. El tratamiento, la manipulaci�n y la presentaci�n de informaciones no pertenecen a un campo tecnol�gico, aunque se utilicen instrumentos t�cnicos para estas operaciones.
art3	es	suprimido
art4_1	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables a condici�n de que sean nuevas, no obvias, supongan una actividad inventiva, pertenezcan a un campo tecnol�gico y sean susceptibles de aplicaci�n industrial.
art4_2	es	2.Los Estados miembros garantizar�n que, como condici�n para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deban aportar una contribuci�n t�cnica, es decir, deben aportar un conocimiento nuevo sobre las relaciones de causa a efecto en la utilizaci�n controlada de las fuerzas de la naturaleza.
art4_3	es	3.La contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y el �mbito de las caracter�sticas t�cnicas de la reivindicaci�n de la patente considerada en su conjunto.
art5	es	Los Estados miembros garantizar�n que las formas posibles de reivindicaci�n de invenciones implementadas en ordenador se refieran solamente a un producto, es decir, a un ordenador programado, red inform�tica programada u otro aparato programado, o un procedimiento t�cnico de producci�n controlado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa.
art6	es	suprimido
art6a	es	Art�culo 6 bis <P>Los derechos conferidos por las patentes concedidas a invenciones que entren en el �mbito de aplicaci�n de la presente Directiva no podr�n afectar a los actos permitidos como excepciones con arreglo a la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular los actos descritos y enumerados restrictivamente en los apartados 2 y 3 del art�culo 5 y en el art�culo 6 de la Directiva 91/250/CEE.
art8ca	es	c bis) si las competencias delegadas en la OEP son compatibles con los requisitos para la armonizaci�n de la legislaci�n de la UE y con los principios de transparencia y responsabilidad.
rec7a	fi	Tietokoneohjelmilla on t�rke� sija useilla teollisuuden aloilla, ja ne ovat my�s perusluonteinen luomisen ja ilmaisun muoto.
rec7b	fi	Euroopan parlamentti antoi p��t�slauselman (EYVLC378, 29.12.2000) Euroopan patenttiviraston p��t�ksest� patentista nroEP695351, joka my�nnettiin 8.joulukuuta 1999, ja vaati tarkistamaan Euroopan patenttiviraston toimintaa koskevia m��r�yksi�, jotta varmistetaan, ett� patenttivirastosta tulee teht�viens� hoitamisesta julkisesti vastuussa oleva elin.
art2a	fi	a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan teknist� ratkaisua, jonka toteuttamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman ensi n�kem�lt� uuden piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art2b	fi	b)'Lis�yksell� tekniikan tasoon' tai 'teknisell� keksinn�ll�' tai 'keksinn�ll�' tarkoitetaan tietoa syyn ja seurauksen suhteesta kontrolloitavien luonnonvoimien k�yt�ss�. Luonnonvoimien k�ytt�minen fyysisten vaikutusten kontrolloimiseksi tietojen digitaalisen esitt�misen lis�ksi kuuluu tekniikanalaan. Tietojen k�sittely, manipulointi ja esittely eiv�t kuulu tekniikanalaan, vaikka n�iss� k�ytett�isiin teknisi� laitteita<I>.</I>
art3	fi	Poistetaan.
art4_1	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi, ei-ilmeinen, keksinn�llinen ja kuuluu tiettyyn tekniikanalaan.
art4_2	fi	2.J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon, toisin sanoen ett� se tuo uutta tietoa syiden ja seurausten suhteista luonnonvoimien kontrolloidussa k�yt�ss�.
art4_3	fi	3.Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen teknisten piirteiden ja vallitsevan tason v�lill�.
art5	fi	J�senvaltioiden on varmistettava, ett� tietokoneella toteutetulle keksinn�lle haettavan patenttisuojan mahdolliset muodot voidaan hyv�ksy� vain, jos keksint�� pidet��n tuotteena, toisin sanoen ohjelmoituna tietokoneena, ohjelmoituna tietokoneiden verkkona tai muuna ohjelmoituna laitteena, tai kyseisen tietokoneen, tietokoneiden verkon tai laitteen suorittamana teknisen� tuotantoprosessina, joka pannaan t�yt�nt��n ohjelmiston avulla.
art6	fi	Poistetaan.
art6a	fi	6 a artikla <P>Sellaisille keksinn�ille my�nnetyt patenttisuojaoikeudet, jotka kuuluvat t�m�n direktiivin soveltamisalaan, eiv�t voi vaikuttaa toimiin, jotka on poikkeuksellisesti sallittu tietokoneohjelmien oikeudellisesta suojasta annetun direktiivin 91/250/ETY nojalla, etenkin sen 5artiklan 2kohdassa, 5artiklan 3kohdassa ja 6artiklassa kuvattuihin ja rajoitetusti lueteltuihin toimiin.
art8ca	fi	c a) siit�, onko Euroopan patenttivirastolle annettu valta yhdenmukaista EU:n lains��d�nn�n yhdenmukaistamisen vaatimusten kanssa sek� avoimuuden ja vastuullisuuden periaatteiden kanssa.
rec7a	fr	Les logiciels jouent, d'une part, un r�le important dans de nombreuses industries et constituent, d'autre part, une forme fondamentale de cr�ation et d'expression.
rec7b	fr	Dans sa r�solution (publi�e au JOC378 du 29.12.2000, p.95) sur une d�cision de l'Office europ�en des brevets concernant la d�livrance du brevetn<SUP>o</SUP>EP695351, le8d�cembre1999, le Parlement europ�en a demand� la r�vision des r�gles de fonctionnement de l'Office de fa�on � garantir que cet organisme puisse publiquement rendre des comptes dans l'exercice de ses fonctions.
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute solution technique dont la mise en oeuvre implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques � premi�re vue nouvelles qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art2b	fr	(b)"contribution technique" d�signe une contribution � l'�tat de la technique dans un domaine technique, qui n'est pas �vidente pour une personne du m�tier. L'utilisation des forces de la nature afin de contr�ler des effets physiques au del� de la repr�sentation num�rique des informations appartient � un domaine technique. Le traitement, la manipulation et les pr�sentations d'informations n'appartiennent pas � un domaine technique, m�me si des appareils techniques sont utilis�s pour les effectuer.
art3	fr	Supprim�
art4_1	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle, non �vidente, qu'elle implique une activit� inventive et qu'elle appartienne � un domaine technique.
art4_2	fr	2.Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique, c'est-�-dire qu'elle apporte un enseignement nouveau sur les relations de cause � effet dans l'utilisation contr�l�e des forces de la nature.
art4_3	fr	La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet des caract�ristiques techniques de la revendication de brevet consid�r� dans son ensemble et l'�tat de la technique..
art5	fr	Les �tats membres veillent � ce que les formes de revendications possibles pour une invention mise en oeuvre par ordinateur soient seulement un produit, c'est-�-dire un ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou un proc�d� technique de production contr�l� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme.
art6	fr	Supprim�
art6a	fr	Article 6 bis <P>Les droits conf�r�s par les brevets octroy�s pour des inventions tombant dans le champ de la pr�sente directive ne peuvent affecter les actes permis au titre d'exceptions sous la directive 91/250 sur la protection juridique des programmes d'ordinateur par un droit d'auteur, en particulier les actes d�crits et limitativement �num�r�s � l'article5, paragraphes2 et 3, et � l'article6 de cette directive 91/250.
art8ca	fr	(c bis) si les pouvoirs d�l�gu�s � l'Office europ�en des brevets sont compatibles avec les exigences li�es � l'harmonisation de la l�gislation de l'Union europ�enne, ainsi qu'avec les principes de transparence et de responsabilit�.
rec7a	it	Il software da un lato svolge un ruolo importante in numerose industrie e, dall'altro, costituisce una forma fondamentale di creazione e di espressione.
rec7b	it	Nella sua risoluzione (GU C 378 del 29.12.2000, pag. 95) sulla decisione dell'Ufficio europeo dei brevetti concernente il brevetto n. EP 695 351 rilasciato l'8 dicembre 1999, il Parlamento europeo chiedeva la revisione delle attivit� dell'UEB per garantire che tale organo sia soggetto a un obbligo di pubblicit� nell'esercizio delle sue funzioni.
art2a	it	(a)"invenzione attuata per mezzo di elaboratori elettronici", una soluzione tecnica la cui attuazione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o pi� caratteristiche di novit� che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art2b	it	(b)"contributo tecnico" o "invenzione tecnica" o "invenzione", un insegnamento sulla relazione di causa-effetto nell'impiego delle forze della natura controllabili. L'impiego delle forze della natura per controllare gli effetti fisici al di l� della rappresentazione digitale delle informazioni rientra in un settore tecnico. Il trattamento, la manipolazione e le presentazioni di informazioni non rientrano in un settore tecnico, anche se sono utilizzati apparecchi tecnici per effettuarli.
art3	it	soppresso
art4_1	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e di non ovviet�, implichi un'attivit� inventiva e appartenga ad un settore tecnico.
art4_2	it	2.Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico, vale a dire arrechi un insegnamento nuovo sulle relazioni di causa-effetto nell'impiego controllato delle forze della natura.
art4_3	it	3.Il contributo tecnico � valutato considerando la differenza tra l'oggetto delle caratteristiche tecniche della rivendicazione di brevetto nel suo insieme e lo stato dell'arte.
art5	it	Gli Stati membri assicurano che le forme di rivendicazione possibili per un'invenzione attuata per mezzo di elaboratori elettronici siano solamente un prodotto, ossia un elaboratore programmato, una rete di elaboratori programmati o un altro apparecchio programmato, o un processo tecnico di produzione controllato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software.
art6	it	soppresso
art6a	it	Articolo 6 bis <P>I diritti conferiti dai brevetti rilasciati per invenzioni che rientrano nel campo d'applicazione della presente direttiva non riguardano gli atti autorizzati a titolo di deroga dalla direttiva 91/250 relativa alla tutela giuridica dei programmi per elaboratore mediante diritto d'autore, in particolare gli atti descritti ed enumerati all'articolo 5, paragrafi 2 e 3, e all'articolo 6 di detta direttiva.
art8ca	it	c bis) la compatibilit� dei poteri delegati all'Ufficio europeo dei brevetti con le esigenze connesse all'armonizzazione della legislazione dell'Unione europea, nonch� con i principi di trasparenza e di responsabilit�.
rec7a	nl	Software speelt enerzijds een belangrijke rol voor een groot aantal industrie�n, en is anderzijds een essenti�le vorm van creatie en expressie.
rec7b	nl	Het Europees Parlement heeft in zijn resolutie over het besluit van het Europees Octrooibureau met betrekking tot het op 8 december 1999 verleende octrooi EP 695 351 (gepubliceerd in PB C 378/95 van 29.12.2000, blz. 95) een hervorming verlangd van het Europees Octrooibureau zodat het bij de uitoefening van zijn functies openbare verantwoording dient af te leggen.
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art2b	nl	b)"technische bijdrage" of "technische uitvinding" of "uitvinding"
art3	nl	Schrappen.
art4_1	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is, niet voor de hand ligt, op uitvinderswerkzaamheid berust, en tot een technisch gebied behoort.
art4_2	nl	2.De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert, d.w.z. dat deze een nieuw inzicht verschaft in het oorzakelijk verband bij de beheerste gebruikmaking van de krachten van de natuur.
art4_3	nl	3.De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de technische kenmerken van de in haar geheel beschouwde octrooiconclusie en de stand van de techniek.
art5	nl	De lidstaten zorgen ervoor dat de mogelijke vormen van aanspraken op een in computers ge�mplementeerde uitvinding alleen betrekking mogen hebben op een product, dat wil zeggen een geprogrammeerde computer, een geprogrammeerd computernetwerk of een ander geprogrammeerd apparaat, of een technisch productieproces dat door zo een computer, computernetwerk of apparaat middels het uitvoeren van software wordt bestuurd.
art6	nl	Schrappen
art6a	nl	Artikel 6 bis<P>De rechten uit hoofde van de octrooien voor uitvindingen die onder de werkingssfeer van onderhavige richtlijn vallen, laten de handelingen onverlet die krachtens richtlijn 91/250 EEG betreffende de auteursrechtelijke bescherming van computerprogramma's bij uitzondering zijn toegestaan, met name de handelingen die beschreven en opgesomd zijn in artikel 5, leden 2 en 3, en artikel 6 van richtlijn 91/250.
art8ca	nl	c bis) de vraag of de aan het Europees Octrooibureau verleende bevoegdheden stroken met de vereisten voor de harmonisering van de EU-wetgeving, alsmede met de beginselen van transparantie en verantwoording.
rec7a	pt	Considerando que o software, por um lado, desempenha um papel importante num grande n�mero de ind�strias e, por outro, constitui uma forma fundamental de cria��o e de express�o,
rec7b	pt	Na sua resolu��o (publicada no JO C 378 de 29.12.2000, p. 95) relativa a uma decis�o do Instituto Europeu de Patentes sobre a patente n�mero EP 695 351, concedida em 08.12.1999, o Parlamento Europeu solicitou a revis�o das regras de funcionamento do IEP para garantir que este organismo seja publicamente respons�vel no exerc�cio das suas fun��es.
art2a	pt	(a)"invento que implica programas de computador" significa qualquer solu��o t�cnica cuja aplica��o implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas, � primeira vista, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art2b	pt	(b)"contributo t�cnico" significa um contributo para o progresso tecnol�gico num dom�nio t�cnico que n�o seja �bvio para uma pessoa competente na tecnologia. A utiliza��o das for�as da natureza para controlar efeitos f�sicos para l� da representa��o digital das informa��es pertence a um dom�nio t�cnico. O tratamento, a manipula��o e a apresenta��o da informa��o n�o pertencem a um dom�nio t�cnico, mesmo que, para os realizar, sejam utilizados aparelhos t�cnicos.
art3	pt	Suprimido
art4_1	pt	1.Os Estados"Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo, n�o �bvio, de implicar uma actividade inventiva e de pertencer a um dom�nio t�cnico.
art4_2	pt	2.Os Estados"Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva, ou seja, que implique um ensinamento novo sobre as rela��es causa-efeito na utiliza��o controlada das for�as da natureza.
art4_3	pt	3.O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito das caracter�sticas t�cnicas da reivindica��o de patente considerada no seu conjunto e o progresso tecnol�gico.
art5	pt	Os Estados"Membros garantir�o que as formas de reivindica��o poss�veis para um invento que implica programas de computador sejam apresentadas apenas com a finalidade de que o invento seja um produto, ou seja, um computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda um processo de produ��o t�cnico controlado por esse computador, rede inform�tica ou aparelho, pela execu��o do software.
art6	pt	Suprimido
art6a	pt	Artigo 6º bis <P>Os direitos conferidos pelas patentes concedidas a inventos que se inscrevem no �mbito de aplica��o da presente directiva n�o podem afectar os actos autorizados a t�tulo de excep��o pela directiva 91/250 relativa � protec��o jur�dica dos programas de computador por um direito de autor, em particular os actos descritos e enumerados nos nºs 2 e 3 do artigo 5º e no artigo 6º da Directiva 91/250</I><I>.
art8ca	pt	c bis) Se as compet�ncias delegadas no IEP s�o compat�veis com os requisitos de harmoniza��o da legisla��o da UE, bem como com os princ�pios de transpar�ncia e responsabilidade.
rec7a	sv	Mjukvara �r � ena sidan en mycket viktig produkt f�r m�nga branscher och utg�r � andra sidan en grundl�ggande skapande- och uttrycksform.
rec7b	sv	I Europaparlamentets resolution (offentliggjord i EGT C 378, 29.12.2000, s.95) om Europeiska patentbyr�ns beslut betr�ffande patent nr EP 695 351 som beviljades den 8 december 1999, kr�vde parlamentet en �versyn av reglerna f�r Europeiska patentbyr�n, f�r att se till att den g�rs offentligt ansvarig f�r ut�vandet av sin verksamhet.
art2a	sv	a)"datorrelaterad uppfinning"
art2b	sv	b)"tekniskt bidrag" eller "teknisk uppfinning" eller "uppfinning"
art3	sv	utg�r
art4_1	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny, icke uppenbar, har uppfinningsh�jd och ing�r i ett teknikomr�de.
art4_2	sv	2.Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag, vilket inneb�r ny kunskap om f�rh�llandet mellan orsak och verkan vid en kontrollerad anv�ndning av naturkrafter.
art4_3	sv	3.Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan de tekniska k�nnetecknen i patentkravens samlade skyddsomf�ng och teknikens st�ndpunkt.
art5	sv	Medlemsstaterna skall se till att de typer av krav som �r m�jliga f�r en datorrelaterad uppfinning endast g�ller en anordning, dvs. en programmerad dator, ett programmerat n�tverk eller n�gon annan programmerad anordning, eller ett tekniskt produktionsf�rfarande styrt av ovann�mnda dator, n�tverk eller anordning genom att en mjukvara k�rs p� anordningen.
art6	sv	utg�r
art6a	sv	Artikel 6a <P>R�ttigheter till f�ljd av patent f�r uppfinningar som omfattas av detta direktiv f�r inte p�verka handlingar som �r till�tna enligt undantagsbest�mmelserna i direktiv91/250/EEG om r�ttsligt skydd f�r datorprogram genom upphovsr�tt, s�rskilt inte handlingar som anges och avgr�nsas i artikel 5.2 och 5.3 samt i artikel 6 i direktiv91/250/EEG.
art8ca	sv	ca)huruvida de befogenheter som delegerats till Europeiska patentbyr�n �r f�renliga med kraven p� en harmonisering av EU:s lagstiftning och med principerna om �ppenhet och ansvarighet.
\.


--
-- Data for TOC entry 71 (OID 23827)
-- Name: cult_just; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cult_just (id, lang, cult_just) FROM stdin;
rec7a	da	-
rec7b	da	Den Europ�iske Patentmyndighed er ikke en EU-institution, og der er tidligere givet udtryk for bekymring med hensyn til dens ansvarlighed.
art2a	da	Definitionen af en "computer-implementeret opfindelse" har en helt central placering i dette direktiv. I sin nuv�rende udformning g�r direktivet alle edb-programmer patenterbare, n�r blot patentans�gningen formuleres med omhu. Det er helt afg�rende, at patenterbarheden indskr�nkes til at omfatte det fysiske, materielle omr�de. Alt, hvad der henh�rer under det immaterielle omr�de (oplysninger, viden), b�r ikke kunne patenteres.
art2b	da	
art3	da	Der er enighed om, at denne artikel er overfl�dig og kunne give det forkerte indtryk, at alle opfindelser i tilknytning til edb-programmer er patenterbare.
art4_1	da	Det er vigtigt at skelne mellem tekniske opfindelser, som h�rer til i den materielle verden og er patenterbare, og edb-programmer, der som s�danne er ophavsretsbeskyttede ligesom matematik, id�er, information " (Den Europ�iske Patentkonvention, 1972).
art4_2	da	�ndringerne har til form�l at sikre, at patenterbarhed udelukkende vedr�rer de teknologiske omr�der, og skal ses i sammenh�ng med �ndringen af artikel 2.
art4_3	da	Direktivforslagets ordlyd �bner mulighed for patentering af opfindelser af teknisk karakter, hvis innovative karakter ikke desto mindre kun vedr�rer ikke-tekniske aspekter. Dette m� helt klart afvises.
art6	da	Se begrundelse til artikel 6 a (ny).
art6a	da	Direktiv 91/250/E�F, der omhandler retlig beskyttelse af edb-programmer ved ophavsret, g�r det muligt for retm�ssige erhververe at foretage visse handlinger, som ellers falder ind under ophavsretten, navnlig reproduktion og overs�ttelse, som "er en foruds�tning for at skaffe de oplysninger, der er n�dvendige for at tilvejebringe interoperabilitet mellem et selvst�ndigt udviklet edb-program og andre edb-programmer", forudsat at en r�kke pr�cise betingelser er opfyldt (jf. artikel 6 i direktiv 91/250/E�F). Direktiv 91/250/E�F har etableret en skr�belig ligev�gtstilstand mellem rettighedshaverens interesser og de interesser, der kendetegner de parter, som s�ger at udarbejde interoperable programmer. Direktivforslaget om computer-implementerede opfindelsers patenterbarhed m� ikke s�tte sp�rgsm�lstegn ved denne ligev�gtstilstand. Det �ndringsforslag, der er fremsat til artikel 6 a, har den fordel, at det er kendetegnet ved st�rre klarhed end den mere generelle formulering i Kommissionens tekst, navnlig fordi der konkret henvises til de relevante bestemmelser i direktiv 91/250/E�F.
art8ca	da	-
rec7a	de	Entf�llt.
rec7b	de	Das EPA ist kein EU-Organ, und hinsichtlich seiner Rechenschaftspflicht wurden zuvor bereits Besorgnisse ge�u�ert.
art2a	de	Die Definition des Begriffs "computerimplementierte Erfindung" ist das Schl�sselelement dieser Richtlinie. In ihrer derzeitigen Form w�rde die Richtlinie die Patentierbarkeit s�mtlicher Computerprogramme gestatten, sofern nur der Antrag auf Patentierbarkeit sorgf�ltig formuliert ist. Die Patentierbarkeit muss unbedingt auf den physischen, materiellen Bereich beschr�nkt werden. Alles, was zum immateriellen Bereich geh�rt (Information, Wissen), darf nicht patentierbar sein.
art2b	de	Es herrscht Einhelligkeit dar�ber, dass die patentierbaren computerimplementierten Erfindungen von denen abgegrenzt werden m�ssen, die nicht patentierbar sind, weil sie nicht zu einem Gebiet der Technik geh�ren. Der Bezug auf die Kr�fte der Natur ist an sich nicht ausreichend
art3	de	Dieser Artikel ist anerkannterma�en �berfl�ssig, da man damit irrt�mlicherweise annehmen k�nnte, dass alle computerimplementierten Erfindungen patentierbar sind.
art4_1	de	Zwischen technischen Erfindungen, die zur materiellen Welt geh�ren und patentierbar sind, und Computerprogrammen als solchen, die wie Mathematik, Ideen, Informationen usw. urheberrechtlich gesch�tzt sind, muss eine Grenze gezogen werden (Europ�isches Patent�bereinkommen, 1972).
art4_2	de	Mit den �nderungen soll sichergestellt werden, dass die Patentierbarkeit ausschlie�lich Gebiete der Technik betrifft und mit der �nderung von Artikel 2 im Einklang steht.
art4_3	de	Die Formulierung des Richtlinienvorschlags erlaubt die Patentierbarkeit von Erfindungen mit bestimmten technischen Merkmalen, wobei deren Innovation allerdings nur die nichttechnischen Aspekte betrifft. Dies ist eindeutig abzulehnen.
art6	de	Siehe Begr�ndung zum neuen Artikel 6 a.
art6a	de	Die Richtlinie 91/250 �ber den Rechtsschutz von Computerprogrammen gestattet es den rechtm��igen Erwerbern, gewisse Handlungen vorzunehmen, die ansonsten unter das Urheberrecht fallen w�rden, insbesondere die Vervielf�ltigung und �bersetzung, die "unerl�sslich [sind], um die erforderlichen Informationen zur Herstellung der Interoperabilit�t eines unabh�ngig geschaffenen Computerprogramms mit anderen Programmen zu erhalten", wenn genau festgelegte Bedingungen erf�llt sind (siehe Richtlinie 91/250, Artikel 6). Die Richtlinie 91/250 hat ein sensibles Gleichgewicht zwischen den Interessen des Inhabers des Urheberrechts und denen derjenigen geschaffen, die sich bem�hen, interoperable Programme zu entwickeln. Der Richtlinienvorschlag zur Patentierbarkeit computerimplementierter Erfindungen darf dieses Gleichgewicht nicht in Frage stellen. Die vorgeschlagene �nderung zu Artikel 6 hat den Vorteil, klarer zu sein als die im Kommissionstext enthaltene allgemeinere Formulierung, insbesondere dadurch, dass darin die einschl�gigen Bestimmungen der Richtlinie 91/250 genau angegeben sind.
art8ca	de	Keine
rec7a	en	None.
rec7b	en	The EPO is not an EU institution and concerns have previously been raised about its accountability.
art2a	en	The definition of a "computer-implemented invention' is the key point of the directive. All computer programs could be considered patentable under the directive as it now stands, provided that the patentability claims were carefully worded. It is vital to confine patentability to the physical and material sphere. Nothing belonging to the non-material sphere (information, knowledge) should be patentable.
art2b	en	There is general agreement on the need to distinguish computer-implemented inventions that can be patented from those which cannot, because they do not belong to a technical field. The reference to natural forces is not sufficient in itself; the crucial issue is the nature of the effects for which those natural forces are used. The use of physical effects in computers to manipulate information must not serve to justify the patentability of algorithms or interfaces.
art3	en	It is generally agreed that this article is unnecessary and could give the mistaken impression that all software-related inventions can be patented.
art4_1	en	It is important to draw a border between technical inventions, which belong to the material world and are patentable, while computer programs as such are protected by copyright like mathematics, ideas, information ... (European Patent Convention 1972).
art4_2	en	The changes are intended to ensure that patentability applies only to technical fields and are in line with the amendment to Article 2.
art4_3	en	The wording in the proposal for a directive paves the way for patentability of inventions of a technical nature whose innovative features do not, however, extend beyond non-technical aspects. This is clearly unacceptable.
art6	en	See justification for the new Article 6a.
art6a	en	Under Directive 91/250/EEC on the legal protection of computer programs by copyright, persons who have legitimately acquired such programs may perform certain acts that would otherwise be covered by copyright, in particular the acts of reproduction and translation, which are "indispensable to obtain the information necessary to achieve the interoperability of an independently created computer program with other programs' (see Article 6). The directive has established a delicate balance between the interests of rightholders and those of parties seeking to develop interoperable programs. The proposal for a directive on the patentability of computer-implemented inventions must not call that balance into question. The amendment proposed to Article 6 has the advantage of being clearer than the more general wording of the Commission text, not least because it specifies the relevant provisions of Directive 91/250/EEC.
art8ca	en	None.
rec7a	es	
rec7b	es	La OEP no es una instituci�n de la UE y en ocasiones anteriores se han expresado dudas sobre su responsabilidad.
art2a	es	La clave de esta Directiva es la definici�n de lo que deba entenderse por "invenci�n implementada en ordenador". En su forma actual, la Directiva permite la patentabilidad de todos los programas de ordenador si la solicitud se presenta con ciertas precauciones. Es esencial limitar la patentabilidad al �mbito f�sico, material. No debe ser patentable nada que pertenezca al �mbito inmaterial (la informaci�n, el conocimiento).
art2b	es	Existe un consenso en torno a la necesidad de delimitar las invenciones implementadas en ordenador que son patentables con respecto a las que no lo son por no pertenecer a un �mbito t�cnico. La referencia a las fuerzas de la naturaleza no es suficiente por s� misma
art3	es	
art4_1	es	Es importante establecer una frontera entre las invenciones t�cnicas, que pertenecen al �mbito material y son patentables, y los programas de ordenador como tales, que gozan de la protecci�n de los derechos de autor, como las matem�ticas, las ideas, la informaci�n" (Convenio sobre la Patente Europea, 1972).
art4_2	es	Estas modificaciones tienen por objeto asegurar que la patentabilidad se refiera exclusivamente a los campos tecnol�gicos, y son coherentes con la modificaci�n del art�culo 2.
art4_3	es	La formulaci�n de la propuesta de directiva permite la patentabilidad de invenciones que tienen un car�cter t�cnico, pero que s�lo innovan aspectos no t�cnicos, por lo que debe claramente rechazarse.
art6	es	V�ase la justificaci�n del art�culo 6 bis nuevo.
art6a	es	La Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador autoriza a los poseedores leg�timos a efectuar ciertos actos que, de otra manera, entrar�an en el �mbito de los derechos de autor, en particular los actos de reproducci�n y de traducci�n que son indispensables para obtener las informaciones necesarias para la interoperatividad del programa de ordenador creado de forma independiente con otros programas, si se cumplen las condiciones requeridas (v�ase el art�culo 6 de la Directiva 91/250/CEE). La Directiva 91/250/CEE ha establecido un delicado equilibrio entre los intereses del titular de los derechos de autor y los de las otras partes que tratan de desarrollar programas interoperativos. La propuesta de Directiva sobre la patentabilidad de las invenciones implementadas en ordenador no debe poner en peligro dicho equilibrio. La enmienda propuesta para el art�culo 6 bis tiene la ventaja de ofrecer m�s claridad que la formulaci�n, m�s general, contenida en el texto de la Comisi�n, en particular porque especifica las disposiciones pertinentes de la Directiva 91/250/CEE.
art8ca	es	
rec7a	fi	Tarkistus ei kaipaa perusteluja.
rec7b	fi	Euroopan patenttivirasto ei ole EU:n toimielin, ja sen vastuukysymyksest� on aiemmin ollut keskustelua.
art2a	fi	"Tietokoneella toteutetun keksinn�n" m��ritelm� on t�m�n direktiivin avainkohta. Nykyisess� muodossaan direktiivi sallisi kaikkien tietokoneohjelmien patentoitavuuden, kunhan patenttihakemus muotoillaan huolellisesti. On ehdottomasti rajoitettava patentoitavuus koskemaan fyysist�, materiaalista alaa. Kaiken immateriaaliseen alaan kuuluvan (tieto, osaaminen) on oltava ei-patentoitavaa.
art2b	fi	Yksimielisyys vallitsee siit�, ett� on tarpeen erottaa patentoitavat tietokoneella toteutetut keksinn�t sellaisista keksinn�ist�, joita ei voida patentoida, koska ne eiv�t kuulu mihink��n tekniikanalaan. Viittaus luonnonvoimiin ei itsess��n riit�, vaan olennaista on niiden vaikutusten luonne, joiden aikaansaamiseen luonnonvoimia k�ytet��n. Fyysisten vaikutusten k�ytt� tietokoneissa tietojen manipuloimiseksi ei saa olla perusteena algoritmien tai rajapintojen patentoitavuudelle.
art3	fi	Yksimielisyytt� vallitsee siit�, ett� t�m� artikla ei ole tarpeellinen ja se saattaisi johtaa siihen virheelliseen k�sitykseen, ett� kaikki tietokoneohjelmiin liittyv�t keksinn�t voidaan patentoida.
art4_1	fi	On t�rke�� vet�� raja teknisten keksint�jen v�lille, jotka kuuluvat materiaaliseen maailmaan ja ovat patentoitavissa, kun taas tietokoneohjelmia sellaisenaan suojellaan tekij�noikeuksin, kuten matematiikkaa, ideoita, tietoa jne. (Euroopan patenttisopimus 1972).
art4_2	fi	Tarkistuksella pyrit��n varmistamaan, ett� patentoitavuus koskee yksinomaan tekniikanaloja, ja se on johdonmukainen 2 artiklaan tehdyn tarkistuksen kanssa.
art4_3	fi	Direktiiviehdotuksen sanamuoto antaa mahdollisuuden sellaisten keksint�jen patentoimiselle, jotka ovat piirteilt��n teknisi� mutta joissa keksint� koskee vain muita kuin teknisi� piirteit�. T�llainen mahdollisuus on selv�sti hyl�tt�v�.
art6	fi	Ks.</I><I>uuden 6 a artiklan perustelu.
art6a	fi	Tietokoneohjelmien oikeudellisesta suojasta annetussa direktiiviss� 91/250/ETY annetaan tietokoneohjelman laillisesti hankkineille henkil�ille lupa suorittaa tiettyj� toimia, jotka muutoin kuuluisivat tekij�noikeuden piiriin; n�it� toimia ovat erityisesti koodin toisintaminen ja sen muodon k��nt�minen, mik� on "v�ltt�m�t�nt� sellaisten tietojen hankkimiseksi, joiden avulla voidaan saavuttaa yhteentoimivuus itsen�isesti luodun tietokoneohjelman ja muiden ohjelmien v�lill�", jos tietyt edellytykset t�ytet��n (ks. direktiivin 91/250/ETY 6artikla). Direktiivill� 91/250/ETY on luotu hienovarainen tasapaino tekij�noikeuksien haltijan etujen ja niiden osapuolten etujen v�lill�, jotka haluavat kehitt�� yhteentoimivia ohjelmia. Tietokoneella toteutettujen keksint�jen patentoitavuutta koskevalla direktiiviehdotuksella ei saa vaarantaa t�t� tasapainoa. Tarkistuksella ehdotettavalla 6 a artiklalla pyrit��n siihen, ett� selvennett�isiin komission tekstiin sis�ltyv�� yleisemp�� muotoilua, etenkin koska sill� t�smennet��n direktiivin 91/250/ETY olennaisia s��nn�ksi�.
art8ca	fi	Tarkistus ei kaipaa perusteluja.
rec7a	fr	N�ant
rec7b	fr	L'Office europ�en des brevets n'est pas une institution de l'Union europ�enne et la question de sa responsabilit� a �t� soulev�e par le pass�.
art2a	fr	La d�finition de ce qu'est une "invention mise en oeuvre par ordinateur" est le point cl� de cette directive. Dans sa forme actuelle, la directive autoriserait la brevetabilit� de tous les programmes d'ordinateur pour peu que la demande de brevetabilit� soit formul�e avec pr�caution. Il est capital de limiter la brevetabilit� au domaine physique, mat�riel. Tout ce qui rel�ve du domaine immat�riel (l'information, le savoir) ne doit pas �tre brevetable.
art2b	fr	Il y a consensus sur le besoin de d�limiter les inventions mises en oeuvre par ordinateur qui sont brevetables de celles qui ne le sont pas parce qu'elles n'appartiennent pas � un domaine technique. La r�f�rence aux forces de la nature n'est pas suffisante en elle-m�me
art3	fr	Il y a consensus sur le fait que cet article n'est pas n�cessaire et pourrait conduire � l'id�e erron�e que toutes les inventions li�es aux logiciels sont brevetables.
art4_1	fr	Il est important d'�tablir une distinction entre les inventions techniques, qui appartiennent au monde physique et sont brevetables, et les programmes d'ordinateurs en tant que tels, qui sont prot�g�s par le droit d'auteur, comme les math�matiques, les id�es, les informations... (Convention sur le brevet europ�en de1972).
art4_2	fr	Les modifications visent � s'assurer que la brevetabilit� concerne exclusivement les domaines techniques et est en coh�rence avec la modification de l'article2.
art4_3	fr	La formulation de la proposition de directive ouvre la porte � la brevetabilit� d'inventions ayant un caract�re technique mais dont l'innovation ne concerne que les aspects non-techniques, ce qui est clairement � rejeter.
art6	fr	Voir la justification de l'article6bis (nouveau).
art6a	fr	La directive 91/250 concernant la protection juridique des programmes d'ordinateur par un droit d'auteur autorise les acqu�reurs l�gitimes � accomplir certains actes qui autrement tomberaient dans le champ du droit d'auteur, en particulier les actes de reproduction et de traduction qui sont "indispensables pour obtenir les informations n�cessaires � l'interop�rabilit� d'un programme d'ordinateur cr�� de mani�re ind�pendante avec d'autres programmes", si des conditions pr�cises sont remplies (voir article 6 de la directive 91/250). La directive 91/250 a �tabli un d�licat �quilibre entre les int�r�ts du titulaire du droit d'auteur et ceux des parties qui cherchent � d�velopper des programmes interop�rables. La proposition de directive concernant la brevetabilit� des inventions mises en oeuvre par ordinateur ne doit pas remettre en cause cet �quilibre. L'amendement propos� pour l'article6 a l'avantage d'offrir davantage de clart� que la formulation plus g�n�rale contenue dans le texte de la Commission, notamment par le fait qu'elle sp�cifie les dispositions pertinentes de la directive 91/250.
art8ca	fr	N�ant.
rec7a	it	
rec7b	it	L'Ufficio europei dei brevetti non � un'istituzione dell'Unione europea e la questione della sua responsabilit� � gi� stata sollevata in passato.
art2a	it	
art2b	it	Esiste un consenso sulla necessit� di distinguere le invenzioni attuate mediante elaboratore elettronico che sono brevettabili da quelle che non lo sono perch� non rientrano in un settore tecnico. Il riferimento alle forze della natura non � sufficiente in se stesso
art3	it	Esiste un consenso sul fatto che l'articolo non � necessario e potrebbe indurre a pensare, erroneamente, che tutte le invenzioni legate al software sono brevettabili.
art4_1	it	E' importante distinguere tra le invenzioni tecniche, che rientrano nel settore materiale e sono brevettabili, ed i programmi per elaboratori, in quanto tali, che sono protetti dalle legge sui diritti d'autore, come la matematica, le idee, l'informazione... (Convenzione sul brevetto europeo, 1972).
art4_2	it	Le modifiche intendono garantire che la brevettabilit� riguardi esclusivamente i settori tecnici e sia coerente con la modifica dell'articolo 2.
art4_3	it	La formulazione della proposta di direttiva apre la porta alla brevettabilit� di invenzioni che presentano un carattere tecnico ma in cui l'innovazione concerne solo gli aspetti non tecnici, cosa che va chiaramente respinta.
art6	it	Cfr.</I><I>motivazione dell'articolo 6 bis.
art6a	it	La direttiva 91/250 relativa alla tutela giuridica dei programmi per elaboratore mediante diritto d'autore autorizza gli acquisitori legittimi a compiere determinati atti che altrimenti rientrerebbero nel campo d'applicazione del diritto d'autore e, in particolare, gli atti di riproduzione e di traduzione che sono "indispensabili per ottenere le informazioni necessarie per conseguire l'interoperabilit� con altri programmi di un programma per elaboratore creato autonomamente" purch� sussistano determinate condizioni (cfr. articolo 6 della direttiva 91/250). Le direttiva 91/250 ha creato un delicato equilibrio tra gli interessi del titolare del diritto d'autore e quelli delle parti che cercano di sviluppare programmi interoperabili. La proposta di direttiva concernente la brevettabilit� delle invenzioni attuate mediante elaboratore non deve rimettere in questione tale equilibrio. L'emendamento proposto per l'articolo 6 ha il vantaggio di offrire maggiore chiarezza rispetto alla formulazione pi� generale contenuta nel testo della Commissione, segnatamente specificando le disposizioni pertinenti della direttiva 91/250.
art8ca	it	
rec7a	nl	
rec7b	nl	Het Europees Octrooibureau is geen instelling van de EU en er is in het verleden reeds bezorgdheid uitgesproken over de verantwoordelijkheid van dit orgaan.
art2a	nl	De definitie van "in computers ge�mplementeerde uitvinding" is de hoeksteen van deze richtlijn. In haar huidige vorm zou de richtlijn de octrooieerbaarheid toestaan van alle computerprogramma's voor zover de octrooiaanvraag zorgvuldig is geformuleerd. Het is van fundamenteel belang de octrooieerbaarheid te beperken tot het fysieke, materi�le gebied. Alles wat onder het immateri�le gebied valt (informatie, kennis) mag niet octrooieerbaar zijn.
art2b	nl	Er bestaat consensus over de noodzaak een onderscheid te maken tussen in computers ge�mplementeerde uitvindingen die octrooieerbaar zijn, en in computers ge�mplementeerde uitvindingen die niet octrooieerbaar zijn omdat zij niet tot een gebied van de technologie behoren. Een verwijzing naar de krachten van de natuur is op zichzelf onvoldoende
art3	nl	Er bestaat consensus over de overbodigheid van dit artikel, dat kan leiden tot de verkeerde opvatting dat alle met software verbonden uitvindingen octrooieerbaar zijn.
art4_1	nl	Het is belangrijk een grens te trekken tussen technische uitvindingen, die tot de materi�le wereld behoren en octrooieerbaar zijn, terwijl computerprogramma's als zodanig beschermd zijn door het auteursrecht, zoals wiskunde, idee�n, informatie"(Europees Octrooiverdrag van 1972).
art4_2	nl	Dit amendement heeft ten doel te waarborgen dat de octrooieerbaarheid alleen geldt voor de technische gebieden en dat er coherentie bestaat met de wijziging van artikel 2.
art4_3	nl	De in het voorstel voor een richtlijn gebruikte formulering zet de deur open voor de octrooieerbaarheid van uitvindingen met een technisch karakter die slechts innoverend zijn wat de niet-technische aspecten betreft, hetgeen duidelijk dient te worden verworpen.
art6	nl	Zie motivering van artikel 6 bis (nieuw).
art6a	nl	Richtlijn 91/250 EEG betreffende de auteursrechtelijke bescherming van computerprogramma's staat rechtmatige verkrijgers toe bepaalde handelingen te verrichten die normaliter onder het auteursrecht zouden vallen, met name de handelingen reproductie en vertaling, die "onmisbaar zijn om de informatie te verkrijgen die nodig is om de compatibiliteit van een onafhankelijk gecre�erd computerprogramma met andere programma's tot stand te brengen", indien aan welbepaalde voorwaarden wordt voldaan (zie artikel 6 van richtlijn 91/250). Richtlijn 91/250 heeft een delicaat evenwicht tot stand gebracht tussen de belangen van de houder van het auteursrecht en de belangen van diegenen die compatibele programma's proberen te ontwikkelen. Het voorstel voor een richtlijn betreffende de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen mag dit evenwicht niet aan het wankelen brengen. Dit op artikel 6 voorgestelde amendement heeft het voordeel meer duidelijkheid te bieden dan de meer algemene formulering van de tekst van de Commissie, vooral omdat erin wordt verwezen naar de relevante bepalingen van richtlijn 91/250.
art8ca	nl	
rec7a	pt	Nada
rec7b	pt	O Instituto Europeu de Patentes n�o � uma institui��o da UE e j� foram levantadas d�vidas acerca da sua responsabilidade.
art2a	pt	A defini��o do que � um "invento que implica programas de computador" � a quest�o-chave desta directiva. Na sua forma actual, a directiva autorizaria a patenteabilidade de todos os programas de computador, se a reivindica��o for formulada com cuidado. � fundamental limitar a patenteabilidade ao dom�nio f�sico, material. Tudo o que se relaciona com o dom�nio imaterial (a informa��o, o conhecimento) n�o deve ser patente�vel.
art2b	pt	Existe consenso quanto � necessidade de diferenciar as inven��es realizadas por computador que s�o patente�veis das que o n�o s�o por n�o pertencerem a um dom�nio t�cnico. A refer�ncia �s for�as da natureza n�o �, por si s�, suficiente
art3	pt	H� um consenso sobre o facto de este artigo n�o ser necess�rio e poder suscitar a ideia err�nea de que todas as inven��es ligadas ao software s�o patente�veis.
art4_1	pt	
art4_2	pt	A altera��o visa garantir que a patenteabilidade incida apenas sobre os dom�nios t�cnicos e vem na sequ�ncia da altera��o ao artigo 2.
art4_3	pt	A formula��o da proposta de directiva abre o caminho � patenteabilidade de inven��es com determinadas caracter�sticas t�cnicas mas cuja inova��o n�o diz unicamente respeito aos aspectos n�o t�cnicos, o que � de rejeitar.
art6	pt	Veja-se justifica��o relativa ao artigo 6 bis (novo).
art6a	pt	A directiva 91/250 relativa � protec��o jur�dica dos programa de computador por um direito de autor autoriza os adquirentes leg�timos a realizar certos actos que, de outro modo, estariam sujeitos ao direito de autor, em particular os actos de reprodu��o e de tradu��o que s�o "indispens�veis para obter as informa��es necess�rias � interoperabilidade de um programa de computador criado independentemente, com outros programas", se estiverem preenchidas certas condi��es (veja-se art. 6 da Directiva 91/250). A directiva 91/250 estabeleceu um delicado equil�brio entre os interesses do titular do direito de autor e os das partes que procuram desenvolver programas interoper�veis. A proposta de directiva relativa � patenteabilidade dos inventos que implicam programas de computador n�o deve p�r em causa este equil�brio. A altera��o proposta ao artigo 6 tem a vantagem de ser mais clara do que a formula��o mais geral contida no texto da Comiss�o, nomeadamente por especificar as disposi��es pertinentes da directiva 91/250.
art8ca	pt	Nada.
rec7a	sv	Motivering saknas.
rec7b	sv	Europeiska patentbyr�n �r ingen EU-institution, och dess ansvarighet har tidigare ifr�gasatts.
art2a	sv	Direktivets k�rnfr�ga �r vad som �r en datorrelaterad uppfinning. Den nuvarande formuleringen inneb�r att alla dataprogram blir patenterbara, bara ans�kan om patent formuleras noggrant. Det �r av avg�rande betydelse att patenterbarheten begr�nsas till det fysiska, materiella omr�det. Det f�r inte vara m�jligt att ta patent p� n�got som faller inom det immateriella omr�det (t.ex. information och kunskap).
art2b	sv	Det r�der enighet om att patenterbara datorrelaterade uppfinningar m�ste skiljas fr�n datorrelaterade uppfinningar som inte �r patenterbara f�r att de inte tillh�r ett teknikomr�de. Att n�mna naturkrafterna �r inte tillr�ckligt; det viktiga �r vilka effekter man vill uppn� med hj�lp av naturkrafterna. Att fysikaliska effekter utnyttjas i datorer f�r att hantera information �r inget sk�l att g�ra algoritmer eller gr�nssnitt patenterbara.
art3	sv	Det r�der enighet om att artikeln inte beh�vs och att den skulle kunna leda till missuppfattningen att alla datorrelaterade uppfinningar �r patenterbara.
art4_1	sv	Det �r viktigt att avgr�nsa tekniska uppfinningar, som h�r till den materiella v�rlden och �r patenterbara. Datorprogram som s�dana skyddas av upphovsr�tt, p� samma s�tt som exempelvis matematik, id�er och information (europeiska patentkonventionen fr�n 1972).
art4_2	sv	�ndringsf�rslaget syftar till att garantera att patenterbarheten uteslutande r�r teknikomr�den och h�r ihop med �ndringsf�rslaget till artikel 2.
art4_3	sv	Formuleringen i f�rslaget till direktiv �ppnar d�rren f�r m�jligheten att patentera uppfinningar med </I><U><I>tekniska</I></U><I> k�nnetecken men d�r den innovativa aspekten endast r�r icke"tekniska k�nnetecken vilket naturligtvis inte �r ber�ttigat.
art6	sv	Se motiveringen till artikel 6a (ny).
art6a	sv	I direktiv 91/250 om r�ttsligt skydd f�r datorprogram genom upphovsr�tt till�ts den som p� laglig v�g f�rv�rvat ett program att utf�ra vissa handlingar som annars skulle omfattas av upphovsr�tten, i f�rsta hand �tergivning och �vers�ttning som kan vara n�dv�ndig "f�r att erh�lla den information som beh�vs f�r att ett sj�lvst�ndigt skapat program skall uppn� samverkansf�rm�ga med andra program", om vissa villkor �r uppfyllda (se artikel 6 i direktiv 91/250). I direktiv 91/250 fastst�lls en k�nslig j�mvikt mellan en upphovsr�ttsinnehavares intressen och intressena hos de parter som f�rs�ker utveckla program med samverkansf�rm�ga. F�rslaget till direktiv om patenterbarhet f�r datorrelaterade uppfinningar f�r inte rubba denna j�mvikt. Genom den f�reslagna artikeln 6a skapas st�rre klarhet j�mf�rt med den mer allm�nna formuleringen i kommissionens text, framf�r allt i och med att de relevanta best�mmelserna i direktiv 91/250 uttryckligen anges.
art8ca	sv	Motivering saknas.
\.


--
-- Data for TOC entry 72 (OID 23867)
-- Name: juri_just; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY juri_just (id, lang, juri_just) FROM stdin;
rec11	da	Denne betragtning repeterer g�ldende ret, s�ledes som denne fremg�r af artikel 52, stk. 1, i Den Europ�iske Patentkonvention.
rec12	da	Det er vigtigt at g�re det klart, at ikke alle computer-implementerede opfindelser n�dvendigvis er patenterbare. Computer-implementerede opfindelser b�r imidlertid ikke udelukkes fra patenterbarhed med den ene begrundelse, at de vedr�rer anvendelsen af et edb-program. Ved at understrege, at en patenterbar computer-implementeret opfindelse, selv om den henh�rer under et teknologisk omr�de, skal yde et bidrag til det aktuelle tekniske niveau, og ved at henlede opm�rksomheden p� den "problem og l�sning"-metode, som anvendes af patentbehandlerne ved Det Europ�iske Patentmyndighed, n�r de skal vurdere, om der er tale om opfindelsesh�jde, tages der sigte p� at undg�, at opfindsomme, men ikke-tekniske metoder (herunder forretningsmetoder) anses for at udg�re et bidrag af teknisk karakter og hermed for at v�re patenterbare, blot fordi de implementeres p� en computer.
rec13a	da	Denne betragtning g�r det klart, at det ikke er tilstr�kkeligt at anf�re anvendelse af en computer (dvs. af tekniske midler) for at g�re en computer-implementeret opfindelse patenterbar. Opfindelsen som helhed skal udg�re et teknisk bidrag. Almindelig edb-behandling er ikke tilstr�kkelig.
rec13d	da	
rec14	da	Det er vigtigt at g�re det klart, at dette direktiv ikke er revolutionerende og ikke vil �ndre status quo med hensyn til patenterbarhed af computer-implementerede opfindelser. Det vil imidlertid skabe retssikkerhed og s�tte klare gr�nser for, hvad der er patenterbart p� dette omr�de.
art2a	da	Udtrykket "prima facie nye" er uklart og kunne f�re til et indledende yderligere krav om vurdering af nyhedskarakteren ved behandlingsprocedurens begyndelse.
art3	da	Denne artikel er un�dvendig og dens r�kkevidde uklar. Den ville v�re vanskelig at gennemf�re og kunne f�re til uforudsigelige resultater. Den kunne fortolkes som en udvidelse af r�kkevidden af patentbeskyttelse.
art6a	da	Muligheden for at forbinde udstyr, s�ledes at det bliver interoperabelt, er en m�de, hvorp� man kan sikre �bne net og forhindre misbrug af en dominerende stilling. Dette har is�r EF-Domstolen givet specifikt udtryk for i sin domspraksis. Patentretten b�r ikke g�re det muligt at tilsides�tte dette princip p� den frie konkurrences og brugernes bekostning.
art7	da	Det er vigtigt at overv�ge, hvordan patenterbarhed af computer-implementerede opfindelser indvirker p� sm� og mellemstore virksomheder.
art8a	da	
art8cc	da	
art8cd	da	
rec11	de	Durch diese Erw�gung wird die Rechtslage wiedergegeben, wie sie in Artikel 52 Absatz 1 des Europ�ischen Patent�bereinkommens ihren Ausdruck findet.
rec12	de	Es ist wichtig klarzustellen, dass nicht alle computerimplementierten Erfindungen automatisch patentierbar sind. Allerdings sollten computerimplementierte Erfindungen auch nicht von der Patentierbarkeit ausgeschlossen werden, nur weil sie die Nutzung eines Computerprogramms erfordern. Durch die Hervorhebung der Tatsache, dass auch eine patentierbare computerimplementierte Erfindung einen technischen Beitrag zum Stand der Technik leisten muss, obwohl sie zu einem Gebiet der Technik geh�rt, und durch den Hinweis auf den Ansatz Problem/L�sung, der von den Patentpr�fern des Europ�ischen Patentamts bei der Pr�fung der erfinderischen T�tigkeit verfolgt wird, soll vermieden werden, dass erfinderische, aber nichttechnische Verfahren (einschlie�lich Gesch�ftsmethoden) als technischer Beitrag gewertet und somit als patentierbar angesehen werden, nur weil sie in einem Computer implementiert werden.
rec13a	de	Durch diese Erw�gung wird klargestellt, dass es nicht ausreicht, dass die Benutzung eines Computers (d.h. eines technischen Hilfsmittels) erforderlich ist, um eine computerimplementierte Erfindung patentierbar zu machen. Die Erfindung insgesamt muss einen technischen Beitrag leisten. Normale Datenverarbeitung gen�gt nicht.
rec13d	de	
rec14	de	Es muss unbedingt klargestellt werden, dass diese Richtlinie keine grunds�tzliche �nderung bedeutet und den Status quo bei der Patentierbarkeit von computerimplementierten Erfindungen nicht ver�ndert. Sie sorgt allerdings f�r Rechtssicherheit und enth�lt klare Grenzen hinsichtlich dessen, was in diesem Bereich patentf�hig ist.
art2a	de	Die Formulierung "auf den ersten Blick " neuartiges" ist unklar und k�nnte dazu f�hren, dass zu Beginn ein zus�tzliches Erfordernis insofern hinzugef�gt wird, als die Neuartigkeit am Anfang des Pr�fungsverfahrens gepr�ft werden muss.
art3	de	Dieser Artikel ist unn�tig, und sein Geltungsbereich ist unklar. Seine Umsetzung w�re schwierig und k�nnte zu unvorhersehbaren Ergebnissen f�hren. Er k�nnte herangezogen werden, um den Anwendungsbereich des Patentschutzes auszuweiten.
art6a	de	Die M�glichkeit, Ger�te miteinander zu verbinden, um sie interoperabel zu machen, ist eine Methode, um f�r offene Netze zu sorgen und den Missbrauch marktbeherrschender Stellungen zu vermeiden. Diese spezifische Entscheidung findet sich insbesondere in der Rechtsprechung des Gerichtshofs der Europ�ischen Gemeinschaften. Das Patentrecht sollte nicht die M�glichkeit schaffen, diesen Grundsatz auf Kosten des freien Wettbewerbs und der Nutzer umzusto�en.
art7	de	Es ist unbedingt erforderlich, die Auswirkungen der Patentierbarkeit von computerimplementierten Erfindungen auf kleine und mittlere Unternehmen zu beobachten.
art8a	de	
rec11	en	This recital restates the law, as enshrined in Article 52(1) of the European Patent Convention.
rec12	en	It is important to clarify that not all computer-implemented inventions are necessarily patentable. However, computer-implemented inventions should not be excluded from patentability on the sole ground that they specify the use of a computer program. By stressing the fact that a patentable computer-implemented invention, albeit belonging to a field of technology, must make a technical contribution to the state of the art and by drawing attention to the problem and solution approach used by the patent examiners at the European Patent Office in assessing inventive step, it is intended to avoid allowing inventive but non-technical methods (including business methods) to be regarded as making a technical contribution and hence as patentable merely because they are implemented on a computer.
rec13a	en	This recital makes it clear that it is not enough to specify the use of a computer (i.e. of technical means) to make a computer-implemented invention patentable. The invention as a whole must make a technical contribution. Ordinary data processing is not enough.
rec13d	en	
rec14	en	It is essential to make it clear that this Directive is not revolutionary and will not change the status quo as regards the patentability of computer-implemented inventions. It will, however, make for legal certainty and set clear limits as to what is patentable in this area.
art2a	en	The expression "prima facie novel" is unclear and could add an initial additional requirement to assess novelty at the commencement of the examination procedure.
art3	en	This article is unnecessary and unclear in scope. It would be difficult to put into effect, and might lead to unpredictable results. It might be construed as extending the scope of patent protection.
art6a	en	The possibility of connecting equipments so as to make them interoperable is a way of ensuring open networks and avoiding abuse of dominant positions. This has been specifically ruled in the case law of the Court of Justice of the European Communities in particular. Patent law should not make it possible to override this principle at the expense of free competition and users.
art7	en	It is essential to monitor the impact of the patentability of computer-implemented inventions on small and medium-sized undertakings.
art8a	en	
rec11	es	Este considerando restablece la ley tal y como est� recogida en el apartado 1 del art�culo 52 del Convenio sobre la Patente Europea.
rec12	es	Es importante aclarar que no todas las invenciones implementadas en ordenador son necesariamente patentables. No obstante, las invenciones implementadas en ordenador no deben excluirse de la patentabilidad por la mera raz�n de que especifiquen el uso de un programa inform�tico. Insistiendo en el hecho de que, aunque s� pertenece a un �mbito de la tecnolog�a, una invenci�n patentable implementada en ordenador debe aportar una contribuci�n t�cnica al estado de la t�cnica y llamando la atenci�n sobre el enfoque de problema aplicado por los examinadores de la Oficina Europea de Patentes para evaluar la actividad inventiva, se pretende evitar que pueda considerarse que, simplemente por implementarse en ordenador, m�todos inventivos, pero no t�cnicos (incluyendo m�todos comerciales), hacen una aportaci�n t�cnica y son, por consiguiente, patentables.
rec13a	es	Este considerando deja claro que no basta con especificar que debe usarse un ordenador (u otros medios t�cnicos) para que una invenci�n implementada en ordenador sea patentable. La invenci�n en su conjunto debe hacer una aportaci�n t�cnica, no basta con el mero tratamiento de datos.
rec13d	es	
rec14	es	Es esencial dejar claro que la presente Directiva no es revolucionaria y no cambiar� el status quo en lo que se refiere a la patentabilidad de las invenciones implementadas en ordenador. En cambio, contribuye a la seguridad jur�dica y delimita con claridad lo que es patentable en este sector.
art2a	es	La expresi�n "nuevas prima facie" no es clara y podr�a a�adir un requisito m�s para evaluar la novedad al comienzo del proceso de examen.
art3	es	Este art�culo es innecesario y su alcance, poco claro. Resultar�a dif�cil de aplicar y podr�a llevar a resultados impredecibles. Podr�a interpretarse que ampl�a el �mbito de protecci�n de la patente.
art6a	es	La posibilidad de conectar equipos de modo que sean interoperables es una manera de garantizar redes abiertas y de evitar el abuso de posiciones dominantes. Esta cuesti�n la ha regulado espec�ficamente la jurisprudencia del Tribunal de Justicia de las Comunidades Europeas. La legislaci�n sobre patentes no deber�a ofrecer la posibilidad de conculcar este principio a expensas de la libre competencia y de los usuarios.
art7	es	Es esencial observar el impacto de la patentabilidad de las invenciones implementadas en ordenador en las PYME.
art8a	es	
rec11	fi	
rec12	fi	
rec13a	fi	Johdanto-osan kappaleessa selvennet��n, ett� pelk�st��n tietokoneen (ts. teknisen v�lineen) k�ytt� ei tee tietokoneella toteutettua keksint�� patentoitavaksi. Keksinn�n on oltava kokonaisuudessaan lis�ys tekniikan tasoon. Tavallinen tietojenk�sittely ei ole siit� riitt�v� osoitus.
rec13d	fi	
rec14	fi	On olennaista selvent��, ett� t�m� direktiivi ei ole vallankumouksellinen eik� muuta tietokoneella toteutettujen keksint�jen patentoitavuuden nykytilannetta. Sill� pyrit��n kuitenkin oikeusvarmuuteen ja selkeiden rajojen asettamiseen sille, mik� on patentoitavissa t�ll� alalla.
art2a	fi	
art3	fi	Artikla on tarpeeton ja sen soveltamisala on ep�selv�. Sit� olisi vaikea panna t�yt�nt��n ja sen t�yt�nt��npano saattaisi johtaa ennalta arvaamattomiin seurauksiin. Sit� voitaisiin pit�� patenttisuojan laajentamisena.
art6a	fi	Mahdollisuus kytke� laitteistoja toisiinsa niiden yhteentoimivuuden aikaansaamiseksi on keino varmistaa verkkojen avoimuus ja v�ltt�� m��r��v�n markkina-aseman v��rink�ytt�. T�m� ilmenee nimenomaisesti etenkin Euroopan yhteis�jen tuomioistuimen oikeusk�yt�nn�st�. Patenttioikeuden ei pid� antaa mahdollisuutta t�m�n periaatteen rikkomiseen vapaan kilpailun ja k�ytt�jien kustannuksella.
art7	fi	On t�rke�� seurata tietokoneella toteutettujen keksint�jen patentoitavuuden vaikutuksia pieniin ja keskisuuriin yrityksiin.
art8a	fi	
rec11	fr	Ce consid�rant rappelle les r�gles en la mati�re, consacr�es par l'article 52, paragraphe 1, de la Convention sur le brevet europ�en.
rec12	fr	Il importe de pr�ciser que toutes les inventions mises en oeuvre par ordinateur ne sont pas n�cessairement brevetables. Toutefois, les inventions mises en oeuvre par ordinateur ne devraient pas �tre exclues de la brevetabilit� pour la seule raison qu'elles pr�voient l'utilisation d'un programme d'ordinateur. En soulignant le fait que, pour �tre brevetable, une invention mise en oeuvre par ordinateur, bien qu'appartenant d�j� � un domaine technique, doit en outre apporter une contribution technique � l'�tat de la technique, et en attirant l'attention sur l'approche probl�me-solution utilis�e par les examinateurs de l'Office europ�en des brevets pour �valuer l'activit� inventive, le but poursuivi est d'�viter que des m�thodes relevant d'une activit� inventive mais non techniques (telles que des m�thodes destin�es � l'exercice d'activit�s �conomiques) puissent �tre consid�r�es comme apportant une contribution technique et donc comme brevetables, uniquement parce qu'elles sont mises en oeuvre sur un ordinateur.
rec13a	fr	Ce consid�rant indique clairement qu'il ne suffit pas de sp�cifier l'utilisation d'un ordinateur (c'est-�-dire de moyens techniques) pour rendre brevetable une invention mise en oeuvre par ordinateur. L'invention dans son ensemble doit apporter une contribution technique. Un simple traitement informatique n'est pas suffisant.
rec13d	fr	
rec14	fr	Il est essentiel d'indiquer clairement que la pr�sente directive n'est pas r�volutionnaire et ne changera pas le statu quo en ce qui concerne la brevetabilit� des inventions mises en oeuvre par ordinateur. Elle assurera toutefois la s�curit� juridique et fixera des limites claires � la brevetabilit� dans ce domaine.
art2a	fr	L'expression "� premi�re vue nouvelles" n'est pas claire et pourrait ajouter une exigence suppl�mentaire visant � �valuer le caract�re de nouveaut� au d�but de la proc�dure d'examen.
art3	fr	Cet article n'est pas n�cessaire et sa port�e n'est pas claire. Il serait difficile � appliquer et pourrait entra�ner des r�sultats impr�visibles. Il pourrait �tre interpr�t� comme une extension du champ d'application de la protection par brevet.
art6a	fr	La possibilit� de connecter des �quipements pour les rendre interop�rables est une fa�on de garantir des r�seaux ouverts et d'�viter les abus de position dominante. Ceci a �t� sp�cifi� en particulier dans la jurisprudence de la Cour de justice des Communaut�s europ�ennes. Le droit des brevets ne devrait pas permettre de violer ce principe, au pr�judice de la libre concurrence et des utilisateurs.
art7	fr	Il est essentiel de surveiller l'impact de la brevetabilit� des inventions mises en oeuvre par ordinateur sur les petites et moyennes entreprises.
art8a	fr	
rec11	it	Tale considerando reitera le norme in materia sancite all'articolo 52, paragrafo 1, della Convenzione sul brevetto europeo.
rec12	it	� importante chiarire che non tutte le invenzioni attuate per mezzo di elaboratori elettronici sono necessariamente brevettabili. Tuttavia, le invenzioni attuate per mezzo di elaboratori elettronici non dovrebbero essere escluse dalla brevettabilit� unicamente per il motivo che esse specificano l'uso di un programma per elaboratore. Sottolineando il fatto che un'invenzione brevettabile attuata per mezzo di elaboratori elettronici, bench� appartenenti a un determinato settore tecnologico, deve apportare un contributo tecnico allo stato dell'arte e attirando l'attenzione sull'approccio "problema-soluzione" utilizzato dagli esaminatori di brevetti all'Ufficio europeo dei brevetti nel valutare invenzioni implicanti un'attivit� inventiva, si intende evitare che i metodi inventivi ma non tecnici (inclusi quelli per attivit� commerciali) vengano considerati contributi tecnici e, di conseguenza, brevettabili semplicemente perch� essi vengono attuati per mezzo di un elaboratore.
rec13a	it	Tale considerando indica chiaramente che � insufficiente specificare l'utilizzazione di un elaboratore (ossia di mezzi tecnici) per rendere brevettabile un'invenzione attuata per mezzo di elaboratori elettronici. La semplice elaborazione nel suo complesso deve costituire un contributo tecnico. L'abituale elaborazione di dati non � sufficiente.
rec13d	it	
rec14	it	� essenziale chiarire che la presente direttiva non � rivoluzionaria e non modificher� lo status quo relativamente alla brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici. Tuttavia essa contribuir� alla certezza giuridica e imporr� limiti chiari per quanto riguarda la brevettabilit� in tale campo.
art2a	it	L'espressione "a prima vista" e "" di novit�" non � chiara e potrebbe aggiungere un'esigenza supplementare per la valutazione della novit� all'inizio della procedura di esame.
art3	it	Tale articolo non � necessario ed � poco chiaro quanto al campo di applicazione. Sarebbe difficile metterlo in pratica e potrebbe portare a risultati imprevedibili. Potrebbe essere interpretato come atto ad estendere il campo di applicazione della tutela del brevetto.
art6a	it	La possibilit� di collegare impianti cos� da renderli interoperabili � un modo per garantire reti aperte ed evitare abusi di posizione dominante. Ci� � stato espressamente riconosciuto in particolare dalla giurisprudenza della Corte di giustizia delle Comunit� europee. Il diritto dei brevetti non dovrebbe consentire che tale principio sia calpestato a scapito della libera concorrenza e degli utenti.
art7	it	� essenziale vigilare sull'impatto della brevettabilit� delle invenzioni attuate per mezzo di elaboratori elettronici sulle piccole e medie imprese.
art8a	it	
rec11	nl	Deze overweging herformuleert de bepaling die vervat is in artikel 52, lid 1, van het Europees Octrooiverdrag.
rec12	nl	Het is van belang dat duidelijk vooropgesteld wordt dat niet alle in computers ge�mplementeerde uitvindingen noodzakelijkerwijs octrooieerbaar zijn. In computers ge�mplementeerde uitvindingen moeten echter niet van octrooieerbaarheid worden uitgesloten louter op grond van het feit dat daarbij het gebruik van een computerprogramma wordt gespecificeerd. Door uitdrukkelijk te wijzen op het feit dat een octrooieerbare in computers ge�mplementeerde uitvinding, ook al hoort deze thuis op een terrein van technologie, een technische bijdrage moet leveren aan de stand van de techniek en door de aandacht te vestigen op de probleem- en oplossingbenadering, zoals deze door de octrooionderzoekers van het Europees Octrooibureau bij de beoordeling van uitvinderswerkzaamheid wordt toegepast, wordt beoogd te voorkomen dat uitvindersmethoden van niet-technische aard (met inbegrip van bedrijfsmethoden) geacht kunnen worden een technische bijdrage te leveren en bijgevolg octrooieerbaar zijn, louter omdat deze op een computer ge�mplementeerd worden.
rec13a	nl	Deze overweging maakt duidelijk dat specificatie van het gebruik van een computer (m.a.w. een technisch middel) niet volstaat om een in computers ge�mplementeerde uitvinding octrooieerbaar te maken. De uitvinding als zodanig moet een technische bijdrage leveren. Gewone dataverwerking is niet voldoende.
rec13d	nl	
rec14	nl	Het is van essentieel belang dat duidelijk vooropgesteld wordt dat deze richtlijn niet revolutionair is en de status quo inzake de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen niet wijzigt. W�l wordt er gestreefd naar rechtszekerheid en de vaststelling van duidelijke grenzen voor hetgeen op dit terrein octrooieerbaar is.
art2a	nl	De woorden "op het eerste gezicht nieuwe" zijn onduidelijk en zouden aanleiding kunnen zijn tot een initieel extra vereiste ter beoordeling van nieuwe producten bij de aanvang van de onderzoekprocedure.
art3	nl	Dit artikel is overbodig en onduidelijk qua reikwijdte. Het zou moeilijk toepasbaar zijn en zou tot onvoorspelbare resultaten kunnen leiden. Het zou ge�nterpreteerd kunnen worden als een uitbreiding van het toepassingsgebied van de octrooibescherming.
art6a	nl	De mogelijkheid apparatuur te koppelen om interoperabiliteit tot stand te brengen, is een manier om tot open netwerken te komen en misbruik van dominante posities te voorkomen. Dit is specifiek gesteld in rechterlijke uitspraken, met name van het Hof van Justitie van de Europese Gemeenschappen. Het octrooirecht mag het niet mogelijk maken dat dit beginsel terzijde wordt geschoven ten koste van de vrije mededinging en van de gebruikers.
art7	nl	Het is van essentieel belang dat toezicht wordt uitgeoefend op de gevolgen van de octrooieerbaarheid van in computers ge�mplementeerde uitvindingen voor kleine en middelgrote ondernemingen.
art8a	nl	
rec11	pt	
rec12	pt	� importante esclarecer que nem todos inventos que implicam programas de computador s�o necessariamente patente�veis. Contudo, os inventos que implicam programas de computador n�o deveriam ser exclu�dos da patenteabilidade pela �nica e simples raz�o de que especificam a necessidade da utiliza��o de um programa inform�tico. Ao sublinhar o facto de que um invento patente�vel que implique programas de computador " se bem que pertencendo a um determinado dom�nio tecnol�gico " tem obrigatoriamente de dar um contributo t�cnico para o progresso tecnol�gico, e ao chamar a aten��o para o m�todo da coloca��o do problema e da busca de uma solu��o " utilizado pelos analistas de patentes no Instituto Europeu de Patentes, sempre que avaliam a presen�a de actividade inventiva ", pretende"se evitar que m�todos inventivos, embora desprovidos de car�cter t�cnico (incluindo os procedimentos comerciais) possam ser considerados como contributos t�cnicos e, por essa via, como material patente�vel, apenas e s� porque pressup�em a sua utiliza��o num computador.
rec13a	pt	Este considerando deixa claro que n�o basta especificar o recurso a um computador (ou seja, a meios de car�cter t�cnico) para fazer com que um invento que implique programas de computador seja patente�vel. � o invento no seu todo que tem de constituir um contributo de car�cter t�cnico. O vulgar processamento de dados n�o basta.
rec13d	pt	
rec14	pt	� essencial tornar claro que a presente directiva n�o vem revolucionar o que quer que seja e n�o modificar� o "statu quo" no que diz respeito aos inventos que implicam programas de computador. Contudo, ela proporcionar� certeza jur�dica e impor� limites claros relativamente ao que � patente�vel neste dom�nio.
art2a	pt	A express�o "novas, � primeira vista" � inintelig�vel e poderia acrescentar, � partida, uma exig�ncia adicional no quadro da avalia��o da novidade no in�cio dos procedimentos de exame.
art3	pt	Este artigo � sup�rfluo e pouco claro quanto aos objectivos. Seria dif�cil lev�"lo � pr�tica, podendo conduzir a resultados imprevis�veis. Poderia ser interpretado como uma extens�o do �mbito de aplica��o da protec��o das patentes.
art6a	pt	A possibilidade de ligar equipamentos com vista a torn�-los interoper�veis � uma forma de assegurar as redes abertas e de evitar o abuso de posi��es dominantes. Esta possibilidade foi prevista, nomeadamente, na jurisprud�ncia do Tribunal de Justi�a das Comunidades Europeias. A legisla��o em mat�ria de patentes n�o deve permitir que este princ�pio seja violado em detrimento da livre concorr�ncia e dos utilizadores.
art7	pt	� essencial controlar o impacto da patenteabilidade de inventos que implicam programas de computador nas PME.
art8a	pt	
rec11	sv	Detta sk�l �terspeglar g�llande r�tt, s�som den framg�r av artikel 52.1 i Europeiska patentkonventionen
rec12	sv	Det �r viktigt att klarg�ra att alla datorrelaterade uppfinningar inte n�dv�ndigtvis �r patenterbara. Det g�r dock inte att f�rbjuda patentering av datorrelaterade uppfinningar bara f�r att de specificerar hur ett datorprogram skall anv�ndas. Genom att understryka att en patenterbar datorrelaterad uppfinning, �ven om den tillh�r teknikens omr�de, m�ste utg�ra ett tekniskt bidrag till teknikens st�ndpunkt, och genom att f�sta uppm�rksamheten vid den problem- och l�sningsmetod som anv�nds av patentgranskarna vid Europeiska patentverket (EPO) d� de utv�rderar uppfinningsh�jden, �r avsikten att man inte skall till�ta att uppfinningsrika, men icke-tekniska, metoder (inklusive aff�rsmetoder) betraktas som tekniska bidrag och s�lunda �r patenterbara bara f�r att de anv�nds p� en dator.
rec13a	sv	I sk�let klarg�rs det att det inte �r tillr�ckligt att specificera att en dator (med andra ord ett tekniskt medel) skall anv�ndas f�r att en datorrelaterad uppfinning skall bli patenterbar. Uppfinningen i sig m�ste utg�ra ett tekniskt bidrag. Det �r inte tillr�ckligt med vanlig databehandling.
rec13d	sv	
rec14	sv	Det b�r klarg�ras att detta direktiv inte �r revolutionerande och att det inte kommer att �ndra status quo i fr�ga om datorrelaterade uppfinningars patenterbarhet. Direktivet kommer emellertid att garantera r�ttss�kerhet och s�tta klara gr�nser f�r vad som kan patenteras inom detta omr�de.
art2a	sv	Uttrycket "sannolikt nya" �r oklart och skulle kunna leda till att det st�lls ytterligare krav p� att det i b�rjan av ett granskningsf�rfarande skall utv�rderas om n�got �r nytt eller inte.
art3	sv	Artikeln �r on�dig, och dess r�ckvidd �r oklar. Det skulle vara sv�rt att till�mpa artikeln, och den kan leda till of�rutsebara resultat. Det kan tolkas som om den utvidgar till�mpningsomr�det f�r patentskydd.
art6a	sv	M�jligheten att sammankoppla anordningar f�r att g�ra dem kompatibla �r ett s�tt att garantera �ppna n�tverk och f�rebygga missbruk av dominerande st�llning. Detta har uttryckligen fastslagits i EG-domstolens r�ttspraxis. Patentr�tten b�r inte g�ra det m�jligt att �sidos�tta denna princip p� den fria konkurrensens och anv�ndarnas bekostnad.
art7	sv	Det �r viktigt att �vervaka effekten p� sm� och medelstora f�retag av att datorrelaterade uppfinningar �r patenterbara.
art8a	sv	
art8cc	sv	
art8cd	sv	
\.


--
-- Data for TOC entry 73 (OID 23880)
-- Name: juri_cec; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY juri_cec (id, lang, juri_cec) FROM stdin;
rec12	da	Hvis en opfindelse <I>derfor</I> ikke yder et bidrag til det aktuelle tekniske niveau, hvilket er tilf�ldet, hvis det specifikke bidrag ikke er af teknisk karakter, har opfindelsen ikke opfindelsesh�jde og er derfor ikke patenterbar.
art2a	da	a)ved en "computer-implementeret opfindelse" forst�s enhver opfindelse, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere prima facie nye karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art3	da	Artikel 3 <P>Computer-implementerede opfindelser som teknologisk omr�de <P>Medlemsstaterne sikrer, at en computer-implementeret opfindelse betragtes som henh�rende under et teknologisk omr�de.
art4	da	1.Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny og har opfindelsesh�jde. <P>2.Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag. <P>3.Det tekniske bidrag vurderes som</I><I> forskellen mellem patentkravets genstand som helhed, hvoraf nogle dele kan omfatte b�de tekniske og ikke-tekniske karakteristika, og det aktuelle tekniske niveau.
art8c	da	(c)hvorvidt der har v�ret vanskeligheder med medlemsstater, hvor kravene om nyhedskarakter og opfindelsesh�jde ikke unders�ges, inden der udstedes patenter, og i bekr�ftende fald, om der b�r tr�ffes foranstaltninger til l�sning af disse vanskeligheder.
art9_1	da	1.Medlemsstaterne s�tter de n�dvendige love og administrative bestemmelser i kraft for at efterkomme dette direktiv senest den [DATO (sidste dag i en m�ned)]. De underretter straks Kommissionen herom.
rec17	de	
art2a	de	(a)"Computerimplementierte Erfindung" ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art3	de	Artikel 3<P>Gebiet der Technik</P><P ALIGN="JUSTIFY">Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugeh�rig gilt.
art4	de	1.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen T�tigkeit beruht. <P>2.Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet. <P>3.Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmale umfassen kann, vom Stand der Technik abhebt.
art6	de	"Zul�ssige Handlungen im Sinne der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften �ber die Dekompilierung und die Interoperabilit�t, oder im Sinne der Vorschriften �ber Marken oder Halbleitertopografien bleiben vom Patentschutz f�r Erfindungen aufgrund dieser Richtlinie unber�hrt."
art7	de	7.Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen und den elektronischen Gesch�ftsverkehr auswirken.
rec1	en	The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. <I>effective </I>and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.
art2a	en	(a)"computer-implemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;
art3	en	Article 3 <P>Computer-implemented inventions as a field of technology <P>Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.
art4	en	1.Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step. <P>2.Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution. <P>3.The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.
art6	en	Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.
art7	en	7.The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.
art9_1	en	1.Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.
art2a	es	(a)«invenci�n implementada en ordenador», toda invenci�n para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas prima facie que se realicen total o parcialmente mediante un programa o programas de ordenador;
art3	es	Art�culo 3 <P>Invenciones implementadas en ordenador como campo de la tecnolog�a <P>Los Estados miembros garantizar�n que se considere que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a.
art4	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables a condici�n de que sean nuevas, supongan una actividad inventiva y sean susceptibles de aplicaci�n industrial. <P>2.Los Estados miembros garantizar�n que, como condici�n para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deban aportar una contribuci�n t�cnica. <P>3.La contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y el �mbito de la reivindicaci�n de la patente considerada en su conjunto, que puede incluir tanto caracter�sticas t�cnicas como no t�cnicas.
art5	es	Los Estados miembros garantizar�n que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red inform�tica programada u otro aparato programado, o como procedimiento realizado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa.
art6	es	"Los actos permitidos en el marco de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular sus preceptos relativos a la descompilaci�n y la interoperabilidad, o las disposiciones relativas a la topograf�a de los productos semiconductores o las marcas comerciales, no se ver�n afectados por la protecci�n que las patentes otorgan a las invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva."
art7	es	La Comisi�n seguir� de cerca el impacto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, incluido el comercio electr�nico.
art9_1	es	1.Los Estados miembros adoptar�n, a m�s tardar el [FECHA (�ltimo d�a de un mes)], las disposiciones legislativas, reglamentarias y administrativas necesarias para dar cumplimiento a lo dispuesto en la presente Directiva. Informar�n de ello inmediatamente a la Comisi�n.
rec17	fi	<I>T�m� direktiivi</I> ei rajoita kilpailus��nt�jen eik� etenk��n perustamissopimuksen 81 ja 82 artiklan soveltamista.
art2a	fi	a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman ensi n�kem�lt� uuden piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art3	fi	Tietokoneella toteutetut keksinn�t osana tekniikanalaa <P>J�senvaltioiden on varmistettava, ett� tietokoneella toteutettua keksint�� pidet��n johonkin tekniikanalaan kuuluvana.
art4	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi ja keksinn�llinen. <P>2.J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon. <P>3.Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen, jonka osat voivat k�sitt�� sek� teknisi� ett� muita kuin teknisi� piirteit�, ja vallitsevan tason v�lill�.
art8	fi	(b)siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; ja <P>(c)siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi.
art9_1	fi	1.J�senvaltioiden on saatettava t�m�n direktiivin noudattamisen edellytt�m�t lait, asetukset ja hallinnolliset m��r�ykset voimaan viimeist��n [ p�iv�n� kuuta (kuukauden viimeinen p�iv�)]. Niiden on ilmoitettava t�st� komissiolle viipym�tt�.
rec1	fr	La r�alisation du march� int�rieur implique que l'on �limine les restrictions � la libre circulation et les distorsions � la concurrence, tout en cr�ant un environnement favorable � l'innovation et � l'investissement. Dans ce contexte, la protection des inventions par brevet est un �l�ment essentiel du succ�s du march� int�rieur. Une protection effective et harmonis�e des inventions mises en oeuvre par ordinateur dans tous les �tats membres est essentielle pour maintenir et encourager les investissements dans ce domaine.
rec5	fr	En cons�quence, les r�gles de droit telles qu'interpr�t�es par les tribunaux des �tats membres doivent �tre harmonis�es et les dispositions r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre rendues transparentes. La s�curit� juridique qui en r�sulte devrait permettre aux entreprises de tirer le meilleur parti des brevets pour les inventions mises en oeuvre par ordinateur et stimuler l'investissement et l'innovation.
rec11	fr	Bien que les inventions mises en oeuvre par ordinateur soient consid�r�es comme appartenant � un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique � l'�tat de la technique pour r�pondre au crit�re de l'activit� inventive.
rec12	fr	En cons�quence, lorsqu'une invention n'apporte pas de contribution technique � l'�tat de la technique, parce que, par exemple, sa contribution sp�cifique ne rev�t pas un caract�re technique, elle ne r�pond pas au crit�re de l'activit� inventive et ne peut donc faire l'objet d'un brevet.
rec14	fr	La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas n�cessiter l'�tablissement d'une l�gislation distincte en lieu et place des dispositions du droit national des brevets. Les r�gles du droit national des brevets doivent continuer de former la base de r�f�rence de la protection juridique des inventions mises en oeuvre par ordinateur, m�me si elles doivent �tre adapt�es ou ajout�es en fonction de certaines contraintes sp�cifiques d�finies dans la directive.
rec16	fr	La position concurrentielle de l'industrie europ�enne vis-�-vis de ses principaux partenaires commerciaux serait am�lior�e si les diff�rences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur �taient �limin�es et si la situation juridique �tait transparente.
rec17	fr	La pr�sente directive ne pr�juge pas de l'application des r�gles de concurrence, en particulier des articles81 et82 du trait�.
rec18	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute invention dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques � premi�re vue nouvelles qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art3	fr	Domaine technique <P>Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit consid�r�e comme appartenant � un domaine technique.
art4	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activit� inventive. <P>2.Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique. <P>3.La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet de la revendication de brevet consid�r� dans son ensemble, dont les �l�ments peuvent comprendre des caract�ristiques techniques et non techniques, et l'�tat de la technique.
art5	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme.
art6	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
art7	fr	7.La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes y compris le commerce �lectronique.
art9_1	fr	1.Les �tats membres mettent en vigueur les dispositions l�gislatives, r�glementaires et administratives n�cessaires pour se conformer � la pr�sente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent imm�diatement la Commission.
rec14	it	La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non deve richiedere una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici, con le modifiche o le integrazioni relative a specifici aspetti richieste dalla presente direttiva.
art2a	it	(a)"invenzione attuata per mezzo di elaboratori elettronici", un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o pi� caratteristiche di novit� che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art3	it	Appartenenza ad un settore della tecnologia <P>Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.
art4	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e implichi un'attivit� inventiva. <P>2.Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico. <P>3.Il contributo tecnico � valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.
art8c	it	c)il verificarsi di difficolt� negli Stati membri nel caso in cui i criteri della novit� e dell'attivit� inventiva non siano esaminati prima del rilascio di un brevetto e le eventuali misure da adottare per risolvere tali difficolt�.
art9_1	it	1.Gli Stati membri mettono in vigore le disposizioni legislative, regolamentari e amministrative necessarie per conformarsi alla presente direttiva entro il [DATA (ultimo giorno di un mese)]. Essi ne informano immediatamente la Commissione.
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art3	nl	In computers ge�mplementeerde uitvindingen als een gebied van de technologie<P>De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding wordt beschouwd als behorende tot een gebied van de technologie<I>.</I>
art4	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is en op uitvinderswerkzaamheid berust. <P>2.De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert. <P>3.De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.
art7	nl	7.De Commissie <I>volgt</I> welke invloed in computers ge�mplementeerde uitvindingen hebben op innovatie en mededinging, zowel in Europa als internationaal, en op het Europese bedrijfsleven, met inbegrip van de elektronische handel.
art9_1	nl	1.De lidstaten doen de nodige wettelijke en bestuursrechtelijke bepalingen in werking treden om uiterlijk op [DATUM (laatste dag van een maand)] aan deze richtlijn te voldoen. Zij stellen de Commissie daarvan onverwijld in kennis.
rec5	pt	Consequentemente, as normas jur�dicas, conforme interpretadas pelos tribunais dos Estados"Membros, devem ser harmonizadas e a lei que rege a patenteabilidade dos inventos que implicam programas de computador deve tornar"se transparente. A certeza jur�dica da� resultante deve permitir �s empresas tirarem o m�ximo partido das patentes dos inventos que implicam programas de computador e dar um incentivo ao investimento e � inova��o.
rec11	pt	Embora se considere que os inventos que implicam programas de computador pertencem a um dom�nio da tecnologia, para implicarem uma actividade inventiva, em comum com os inventos em geral, devem dar um contributo t�cnico para o progresso tecnol�gico.
rec12	pt	Deste modo, se um invento n�o der um contributo t�cnico para o progresso tecnol�gico, como aconteceria, por exemplo, se o seu contributo espec�fico n�o tivesse car�cter t�cnico, o invento n�o apresentar� uma actividade inventiva, pelo que n�o ser� patente�vel.
rec14	pt	Para a protec��o jur�dica dos inventos que implicam programas de computador n�o � necess�rio criar um organismo de aplica��o das normas em vigor da legisla��o nacional em mat�ria de patentes. As regras da legisla��o nacional em mat�ria de patentes devem permanecer a base essencial para a protec��o jur�dica dos inventos que implicam programas de computador, adaptadas ou acrescentadas em certas circunst�ncias espec�ficas, conforme se indica na presente directiva.
rec16	pt	A posi��o concorrencial da ind�stria europeia em rela��o aos seus principais parceiros comerciais melhoraria se fossem eliminadas as actuais diferen�as em termos de protec��o jur�dica dos inventos que implicam programas de computador e se a situa��o jur�dica fosse transparente.
rec17	pt	A presente directiva aplicar-se-� sem preju�zo das regras de concorr�ncia, em particular dos artigos 81.º e 82.º do Tratado.
art2a	pt	a)"invento que implica programas de computador" significa qualquer invento cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas, � primeira vista, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art3	pt	Artigo 3 <P>Inventos que implicam programas de computador enquanto dom�nio da tecnologia <P>Os Estados"Membros assegurar�o que um invento que implica programas de computador seja considerado como pertencendo a um dom�nio da tecnologia.
art4	pt	1.Os Estados"Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva. <P>2.Os Estados"Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva. <P>3.O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito da reivindica��o de patente considerada no seu conjunto, cujos elementos possam incluir caracter�sticas t�cnicas e n�o t�cnicas, e o progresso tecnol�gico.
art5	pt	Os Estados"Membros garantir�o que um invento que implica programas de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda como processo executado por esse computador, rede inform�tica ou aparelho, pela execu��o do software.
art9_1	pt	1.Os Estados"Membros por�o em vigor as disposi��es legislativas, regulamentares e administrativas necess�rias para darem cumprimento � presente directiva at� [DATA (�ltimo dia de um determinado m�s)]. Desse facto informar�o imediatamente a Comiss�o.
rec17	sv	Detta direktiv skall inte inverka p� till�mpningen av konkurrensreglerna, s�rskilt <I>artikel 81 och 82</I> i f�rdraget.
art2a	sv	a)"datorrelaterad uppfinning"
art4	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny och har uppfinningsh�jd. <P>2.Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag. <P>3.Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samlade skyddsomf�ng, vari s�v�l tekniska som icke-tekniska k�nnetecken kan ing�, och teknikens st�ndpunkt.
art6	sv	Handlingar som �r till�tna enligt direktiv91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga eller dem om kretsm�nster p� halvledare eller varum�rken, skall inte p�verkas av det patentskydd f�r uppfinningar som g�ller inom detta direktivs r�ckvidd.
art9_1	sv	1.Medlemsstaterna skall s�tta i kraft de lagar och andra f�rfattningar som �r n�dv�ndiga f�r att f�lja detta direktiv senast den och skall genast underr�tta kommissionen om detta.
\.


--
-- Data for TOC entry 74 (OID 24012)
-- Name: cult_cec; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cult_cec (id, lang, cult_cec) FROM stdin;
art2a	da	(a)ved en "computer-implementeret opfindelse" forst�s enhver opfindelse, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere <I>prima facie </I>nye karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art2b	da	(b)ved et "teknisk bidrag" forst�s et bidrag til det aktuelle tekniske niveau, som ikke er indlysende for en fagmand.
art3	da	Computer-implementerede opfindelser som teknologisk omr�de <P>Medlemsstaterne sikrer, at en computer-implementeret opfindelse betragtes som henh�rende under et teknologisk omr�de.
art4_1	da	1.Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny og har opfindelsesh�jde.
art4_2	da	2.Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag.
art4_3	da	3.Det tekniske bidrag vurderes som forskellen mellem patentkravets genstand som helhed, hvoraf nogle dele kan omfatte b�de tekniske og ikke-tekniske karakteristika, og det aktuelle tekniske niveau.
art2a	de	(a)"Computerimplementierte Erfindung" ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art2b	de	(b)"Technischer Beitrag" ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der f�r eine fachkundige Person nicht nahe liegend ist.
art3	de	Gebiet der Technik <P>Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugeh�rig gilt.
art4_1	de	1.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen T�tigkeit beruht.
art4_2	de	2.Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet.
art4_3	de	Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmalen umfassen kann, vom Stand der Technik abhebt.
art2a	en	(a)"computer-implemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more <I>prima facie</I> novel features which are realised wholly or partly by means of a computer program or computer programs;
art2b	en	(b)"technical contribution" means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.
art3	en	Computer-implemented inventions as a field of technology <P>Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.
art4_1	en	1.Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.
art4_2	en	2.Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.
art4_3	en	3.The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.
art2a	es	a)«invenci�n implementada en ordenador», toda invenci�n para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas <I>prima facie </I>que se realicen total o parcialmente mediante un programa o programas de ordenador;
art2b	es	(b)«contribuci�n t�cnica», una contribuci�n al estado de la t�cnica en un campo tecnol�gico que no sea evidente para un experto en la materia.
art3	es	Campo de la tecnolog�a <P>Los Estados miembros garantizar�n que se considere que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a.
art4_1	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables a condici�n de que sean nuevas, supongan una actividad inventiva y sean susceptibles de aplicaci�n industrial.
art4_2	es	2.Los Estados miembros garantizar�n que, como condici�n para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deban aportar una contribuci�n t�cnica.
art4_3	es	3.La contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y el �mbito de la reivindicaci�n de la patente considerada en su conjunto, que puede incluir tanto caracter�sticas t�cnicas como no t�cnicas.
art5	es	Los Estados miembros garantizar�n que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red inform�tica programada u otro aparato programado, o como procedimiento realizado por un ordenador, red inform�tica o aparato mediante la ejecuci�n de un programa.
art6	es	Los actos permitidos en el marco de la Directiva 91/250/CEE sobre la protecci�n jur�dica de programas de ordenador mediante derechos de autor, y en particular sus preceptos relativos a la descompilaci�n y la interoperabilidad, o las disposiciones relativas a la topograf�a de los productos semiconductores o las marcas comerciales, no se ver�n afectados por la protecci�n que las patentes otorgan a las invenciones pertenecientes al �mbito de aplicaci�n de la presente Directiva.
art2a	fi	a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman ensi n�kem�lt� uuden piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai "ohjelmien avulla.
art2b	fi	b)'Lis�yksell� tekniikan tasoon' tarkoitetaan sellaista lis�yst� tekniikanalalla vallitsevaan tasoon, joka ei ole ilmeinen alan ammattimiehelle.
art3	fi	Tietokoneella toteutetut keksinn�t osana tekniikanalaa <P>J�senvaltioiden on varmistettava, ett� tietokoneella toteutettua keksint�� pidet��n johonkin tekniikanalaan kuuluvana.
art4_1	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi ja keksinn�llinen.
art4_2	fi	2.J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon.
art4_3	fi	3.Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen, jonka osat voivat k�sitt�� sek� teknisi� ett� muita kuin teknisi� piirteit�, ja vallitsevan tason v�lill�.
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute invention dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques � premi�re vue nouvelles qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art2b	fr	(b)"contribution technique" d�signe une contribution � l'�tat de la technique dans un domaine technique, qui n'est pas �vidente pour une personne du m�tier.
art3	fr	Domaine technique <P>Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit consid�r�e comme appartenant � un domaine technique.
art4_1	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activit� inventive.
art4_2	fr	2.Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique.
art4_3	fr	La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet de la revendication de brevet consid�r� dans son ensemble, dont les �l�ments peuvent comprendre des caract�ristiques techniques et non techniques, et l'�tat de la technique.
art5	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur puisse �tre revendiqu�e en tant que produit, c'est-�-dire en tant qu'ordinateur programm�, r�seau informatique programm� ou autre appareil programm� ou en tant que proc�d�, r�alis� par un tel ordinateur, r�seau d'ordinateur ou autre appareil � travers l'ex�cution d'un programme.
art6	fr	Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particuli�res relatives � la d�compilation et � l'interop�rabilit� ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affect�s par la protection octroy�e par les brevets d'invention dans le cadre de la pr�sente directive.
art2a	it	(a)"invenzione attuata per mezzo di elaboratori elettronici", un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o pi� caratteristiche di novit� che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art2b	it	(b)"contributo tecnico"<I>, </I>un contributo allo stato dell'arte in un settore tecnico, giudicato non ovvio da una persona competente nella materia.
art3	it	Appartenenza ad un settore della tecnologia <P>Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.
art4_1	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e implichi un'attivit� inventiva.
art4_2	it	2.Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico.
art4_3	it	3.Il contributo tecnico � valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art2b	nl	b)"technische bijdrage"
art3	nl	In computers ge�mplementeerde uitvindingen als een gebied van de technologie<P>De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding wordt beschouwd als behorende tot een gebied van de technologie.
art4_1	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is en op uitvinderswerkzaamheid berust.
art4_2	nl	2.De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert.
art4_3	nl	3.De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.
art2a	pt	(a)"invento que implica programas de computador" significa qualquer invento cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas, � primeira vista, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art2b	pt	(b)"contributo t�cnico" significa um contributo para o progresso tecnol�gico num dom�nio t�cnico que n�o seja �bvio para uma pessoa competente na tecnologia.
art3	pt	Inventos que implicam programas de computador enquanto dom�nio da tecnologia. <P>Os Estados"Membros assegurar�o que um invento que implica programas de computador seja considerado como pertencendo a um dom�nio da tecnologia
art4_1	pt	1.Os Estados"Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva.
art4_2	pt	2.Os Estados"Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva.
art4_3	pt	3.O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito da reivindica��o de patente considerada no seu conjunto, cujos elementos possam incluir caracter�sticas t�cnicas e n�o t�cnicas, e o progresso tecnol�gico.
art5	pt	Os Estados"Membros garantir�o que um invento que implica programas de computador possa ser reivindicado como um produto, ou seja, como computador programado, rede inform�tica programada ou outro aparelho programado, ou ainda como processo executado por esse computador, rede inform�tica ou aparelho, pela execu��o do software.
art2a	sv	a)"datorrelaterad uppfinning"
art2b	sv	b)"tekniskt bidrag"
art3	sv	Datorrelaterade uppfinningar som teknikomr�de <P>Medlemsstaterna skall se till att en datorrelaterad uppfinning uppfattas som tillh�rande ett teknikomr�de.
art4_1	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny och har uppfinningsh�jd.
art4_2	sv	2.Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag.
art4_3	sv	3.Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samlade skyddsomf�ng, vari s�v�l tekniska som icke-tekniska k�nnetecken kan ing�, och teknikens st�ndpunkt.
art6	sv	Handlingar som �r till�tna enligt direktiv91/250/EEG om r�ttsligt skydd f�r datorprogram, och s�rskilt best�mmelserna i det direktivet om dekompilering och samverkansf�rm�ga eller dem om kretsm�nster p� halvledare eller varum�rken, skall inte p�verkas av det patentskydd f�r uppfinningar som g�ller inom detta direktivs r�ckvidd.
\.


--
-- Data for TOC entry 75 (OID 24020)
-- Name: itre_cec; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY itre_cec (id, lang, itre_cec) FROM stdin;
art2a	da	a)ved en "computer-implementeret opfindelse" forst�s enhver opfindelse, hvis implementering indeb�rer anvendelse af en computer, et computernet eller en anden programmerbar maskine, og som omfatter et eller flere prima facie nye karakteristika, som helt eller delvist implementeres ved hj�lp af et eller flere edb-programmer
art2b	da	b)ved et "teknisk bidrag" forst�s et bidrag til det aktuelle tekniske niveau, som ikke er indlysende for en fagmand.
art4_1	da	1.Medlemsstaterne sikrer, at en computer-implementeret opfindelse kan patenteres, forudsat at den kan anvendes industrielt, er ny og har opfindelsesh�jde.
art4_2	da	2.Medlemsstaterne sikrer, at en computer-implementeret opfindelse som en foruds�tning for at have opfindelsesh�jde yder et teknisk bidrag.
art4_3	da	3.Det tekniske bidrag vurderes som forskellen mellem patentkravets genstand som helhed, hvoraf nogle dele kan omfatte b�de tekniske og ikke-tekniske karakteristika, og det aktuelle tekniske niveau.
art2a	de	(a)"Computerimplementierte Erfindung" ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.
art2b	de	(b)"Technischer Beitrag" ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der f�r eine fachkundige Person nicht nahe liegend ist.
art4_1	de	1.Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen T�tigkeit beruht.
art4_2	de	2.Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen T�tigkeit nur erf�llt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet.
art4_3	de	3.Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmalen umfassen kann, vom Stand der Technik abhebt.
art2a	en	(a)"computer-implemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;
art2b	en	(b)"technical contribution" means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.
art4_1	en	1.Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.
art4_2	en	2.Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.
art4_3	en	3.The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.
art2a	es	a)«invenci�n implementada en ordenador», toda invenci�n para cuya ejecuci�n se requiera la utilizaci�n de un ordenador, una red inform�tica u otro aparato programable y que tenga una o m�s caracter�sticas nuevas prima facie que se realicen total o parcialmente mediante un programa o programas de ordenador;
art2b	es	b)«contribuci�n t�cnica», una contribuci�n al estado de la t�cnica en un campo tecnol�gico que no sea evidente para un experto en la materia.
art3	es	Art�culo 3 <P>Invenciones implementadas en ordenador como campo de la tecnolog�a <P>Los Estados miembros garantizar�n que se considere que las invenciones implementadas en ordenador pertenecen a un campo de la tecnolog�a.
art4_1	es	1.Los Estados miembros garantizar�n que las invenciones implementadas en ordenador sean patentables a condici�n de que sean nuevas, supongan una actividad inventiva y sean susceptibles de aplicaci�n industrial.
art4_2	es	2.Los Estados miembros garantizar�n que, como condici�n para que impliquen una actividad inventiva, las invenciones implementadas en ordenador deban aportar una contribuci�n t�cnica.
art4_3	es	3.La contribuci�n t�cnica deber� evaluarse considerando la diferencia entre el estado de la t�cnica y el �mbito de la reivindicaci�n de la patente considerada en su conjunto, que puede incluir tanto caracter�sticas t�cnicas como no t�cnicas.
art7	es	La Comisi�n seguir� de cerca el impacto de las invenciones implementadas en ordenador sobre la innovaci�n y la competencia, tanto a escala europea como internacional, y sobre las empresas europeas, incluido el comercio electr�nico.
art2a	fi	(a)'Tietokoneella toteutetulla keksinn�ll�' tarkoitetaan keksint��, jonka suorittamiseen k�ytet��n tietokonetta, tietokoneiden verkkoa tai muuta ohjelmoitavaa laitetta ja joka sis�lt�� yhden tai useamman ensi n�kem�lt� uuden piirteen, joka toteutuu kokonaan tai osittain tietokoneohjelman tai -ohjelmien avulla.
art2b	fi	(b)'Lis�yksell� tekniikan tasoon' tarkoitetaan sellaista lis�yst� tekniikanalalla vallitsevaan tasoon, joka ei ole ilmeinen alan ammattimiehelle.
art4_1	fi	1.J�senvaltioiden on varmistettava, ett� tietokoneella toteutettu keksint� on patentoitavissa sill� edellytyksell�, ett� sit� voidaan k�ytt�� teollisesti ja ett� se on uusi ja keksinn�llinen.
art4_2	fi	2.J�senvaltioiden on varmistettava, ett� keksinn�llisyyden edellytykseksi asetetaan tietokoneella toteutetun keksinn�n tuoma lis�ys tekniikan tasoon.
art4_3	fi	3.Lis�yst� tekniikan tasoon on arvioitava ottamalla huomioon ero, joka on kokonaisuutena tarkasteltavan patenttivaatimuksen, jonka osat voivat k�sitt�� sek� teknisi� ett� muita kuin teknisi� piirteit�, ja vallitsevan tason v�lill�.
art5	fi	
art8	fi	(b)siit�, ovatko patentoitavuudelle asetettujen vaatimusten ja etenkin uutuuden, keksinn�llisyyden ja patenttivaatimuksen laajuuden m��rittelyst� annetut s��nn�t asianmukaisia; ja <P>(c)siit�, onko havaittu ongelmia niiss� j�senvaltioissa, joissa uutuus- ja keksinn�llisyystutkimuksia ei suoriteta ennen patentin my�nt�mist�, ja jos n�in on, olisiko suotavaa toteuttaa toimia kyseisten ongelmien poistamiseksi.
rec5	fr	En cons�quence, les r�gles de droit telles qu'interpr�t�es par les tribunaux des �tats membres doivent �tre harmonis�es et les dispositions r�gissant la brevetabilit� des inventions mises en oeuvre par ordinateur doivent �tre rendues transparentes. La s�curit� juridique qui en r�sulte devrait permettre aux entreprises de tirer le meilleur parti des brevets pour les inventions mises en oeuvre par ordinateur et stimuler l'investissement et l'innovation.
rec11	fr	Bien que les inventions mises en oeuvre par ordinateur soient consid�r�es comme appartenant � un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique � l'�tat de la technique pour r�pondre au crit�re de l'activit� inventive.
art2a	fr	(a)"invention mise en oeuvre par ordinateur" d�signe toute invention dont l'ex�cution implique l'utilisation d'un ordinateur, d'un r�seau informatique ou d'autre appareil programmable et pr�sentant une ou plusieurs caract�ristiques � premi�re vue nouvelles qui sont r�alis�es totalement ou en partie par un ou plusieurs programmes d'ordinateurs;
art2b	fr	(b)"contribution technique" d�signe une contribution � l'�tat de la technique dans un domaine technique, qui n'est pas �vidente pour une personne du m�tier.
art3	fr	Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit consid�r�e comme appartenant � un domaine technique.
art4_1	fr	1.Les �tats membres veillent � ce qu'une invention mise en oeuvre par ordinateur soit brevetable � la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activit� inventive.
art4_2	fr	2.Les �tats membres veillent � ce que pour impliquer une activit� inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique.
art4_3	fr	3.La contribution technique est �valu�e en prenant en consid�ration la diff�rence entre l'objet de la revendication de brevet consid�r� dans son ensemble, dont les �l�ments peuvent comprendre des caract�ristiques techniques et non techniques, et l'�tat de la technique.
art7	fr	La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises europ�ennes y compris le commerce �lectronique.
art2a	it	a)"invenzione attuata per mezzo di elaboratori elettronici", un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o pi� caratteristiche di novit� che sono realizzate in tutto o in parte per mezzo di uno o pi� programmi per elaboratore;
art2b	it	b)"contributo tecnico", un contributo allo stato dell'arte in un settore tecnico, giudicato non ovvio da una persona competente nella materia.
art3	it	Appartenenza ad un settore della tecnologia <P>Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.
art4_1	it	1.Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novit� e implichi un'attivit� inventiva.
art4_2	it	2.Gli Stati membri assicurano che, affinch� sia considerata implicante un'attivit� inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico.
art4_3	it	3.Il contributo tecnico � valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.
art8c	it	c)il verificarsi di difficolt� negli Stati membri nel caso in cui i criteri della novit� e dell'attivit� inventiva non siano esaminati prima del rilascio di un brevetto e le eventuali misure da adottare per risolvere tali difficolt�.
art2a	nl	a)"in computers ge�mplementeerde uitvinding"
art2b	nl	b)"technische bijdrage"
art4_1	nl	1.De lidstaten zorgen ervoor dat een in computers ge�mplementeerde uitvinding octrooieerbaar is op voorwaarde dat ze vatbaar is voor toepassing op het gebied van de nijverheid, nieuw is en op uitvinderswerkzaamheid berust.
art4_2	nl	2.De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers ge�mplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert.
art4_3	nl	3.De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.
rec5	pt	Consequentemente, as normas jur�dicas, conforme interpretadas pelos tribunais dos Estados"Membros, devem ser harmonizadas e a lei que rege a patenteabilidade dos inventos que implicam programas de computador deve tornar"se transparente. A certeza jur�dica da� resultante deve permitir �s empresas tirarem o m�ximo partido das patentes dos inventos que implicam programas de computador e dar um incentivo ao investimento e � inova��o;
art2a	pt	a)"invento que implica programas de computador" significa qualquer invento cujo desempenho implique o uso de um computador, de uma rede inform�tica ou de outro aparelho program�vel e que tenha uma ou mais caracter�sticas novas, � primeira vista, que sejam realizadas, no todo ou em parte, atrav�s de um ou mais programas de computador;
art2b	pt	b)"contributo t�cnico" significa um contributo para o progresso tecnol�gico num dom�nio t�cnico que n�o seja �bvio para uma pessoa competente na tecnologia.
art3	pt	Os Estados"Membros assegurar�o que um invento que implica programas de computador seja considerado como pertencendo a um dom�nio da tecnologia.
art4_1	pt	1.Os Estados"Membros garantir�o a patenteabilidade de um invento que implique programas de computador, na condi��o de ele ser suscept�vel de aplica��o industrial, de ser novo e de implicar uma actividade inventiva.
art4_2	pt	2.Os Estados"Membros garantir�o que o facto de um invento apresentar um contributo t�cnico seja condi��o para implicar uma actividade inventiva.
art4_3	pt	3.O contributo t�cnico ser� avaliado considerando a diferen�a entre o �mbito da reivindica��o de patente considerada no seu conjunto, cujos elementos possam incluir caracter�sticas t�cnicas e n�o t�cnicas, e o progresso tecnol�gico.
art2a	sv	a)"datorrelaterad uppfinning"
art2b	sv	b)"tekniskt bidrag"
art4_1	sv	1.Medlemsstaterna skall se till att en datorrelaterad uppfinning �r patenterbar p� villkor att den kan tillgodog�ras industriellt, �r ny och har uppfinningsh�jd.
art4_2	sv	2.Medlemsstaterna skall se till att en datorrelaterad uppfinning f�r tillerk�nnas uppfinningsh�jd, endast p� villkor att den utg�r ett tekniskt bidrag.
art4_3	sv	3.Det tekniska bidraget skall bed�mas med h�nsyn till skillnaden mellan patentkravens samlade skyddsomf�ng, vari s�v�l tekniska som icke-tekniska k�nnetecken kan ing�, och teknikens st�ndpunkt.
\.


--
-- Data for TOC entry 76 (OID 24107)
-- Name: cult_item; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cult_item (id, lang, cult_item) FROM stdin;
art4_1	da	Artikel 4, stk. 1
art4_2	da	Artikel 4, stk. 2
art4_3	da	Artikel 4, stk. 3
rec7a	de	Erw�gung 7 a (neu)
rec7b	de	Erw�gung 7 b (neu)
art4_1	de	Artikel 4 Absatz 1
art4_2	de	Artikel 4 Absatz 2
art4_3	de	Artikel 4 Absatz 3
rec7a	en	Recital 7 a (new)
rec7b	en	Recital 7 b (new)
art4_1	en	Article 4, paragraph 1
art4_2	en	Article 4, paragraph 2
art4_3	en	Article 4, paragraph 3
art6a	en	Article 6 a (new)
art8ca	en	Article 8, point (c a) (new)
art2a	es	Art�culo 2, punto a)
art2b	es	Art�culo 2, punto b)
art4_1	es	Art�culo 4, apartado 1
art4_2	es	Art�culo 4, apartado 2
art4_3	es	Art�culo 4, apartado 3
art5	es	Art�culo 5
art8ca	es	Art�culo 8, letra c) bis (nueva)
art4_1	fi	4 artiklan 1 kohta
art4_2	fi	4 artiklan 2 kohta
art4_3	fi	4 artiklan 3 kohta
art5	fi	5 artikla
rec7a	fr	Consid�rant 7 bis (nouveau)
art4_1	fr	Article 4, paragraphe 1
art4_2	fr	Article 4, paragraphe 2
art4_3	fr	Article 4, paragraphe 3
art4_1	it	Articolo 4, paragrafo 1
art4_2	it	Articolo 4, paragrafo 2
art4_3	it	Articolo 4, paragrafo 3
art4_1	nl	Artikel 4, lid 1
art4_2	nl	Artikel 4, lid 2
art4_3	nl	Artikel 4, lid 3
art8ca	nl	Artikel 8, letter c bis) (nieuw)
art4_1	pt	Artigo 4, nº 1
art4_2	pt	Artigo 4, nº 2
art4_3	pt	Artigo 4, nº 3
art5	pt	Artigo 5
art8ca	pt	Artigo 8, al�nea c) bis (nova)
art4_1	sv	Artikel 4, punkt 1
art4_2	sv	Artikel 4, punkt 2
art4_3	sv	Artikel 4, punkt 3
\.


--
-- Data for TOC entry 77 (OID 24115)
-- Name: itre_item; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY itre_item (id, lang, itre_item) FROM stdin;
art4_1	da	Artikel 4, stk. 1
art4_2	da	Artikel 4, stk. 2
art4_3	da	Artikel 4, stk. 3
art4_3a	da	Artikel 4, stk. 3 a (nyt)
rec7a	de	Erw�gung 7 a (neu)
rec7b	de	Erw�gung 7 b (neu)
art4_1	de	Artikel 4 Absatz 1
art4_2	de	Artikel 4 Absatz 2
art4_3	de	Artikel 4 Absatz 3
art4_3a	de	Artikel 4 Absatz 3a (neu)
art8ca	de	Artikel 8 Buchstabe c a (neu)
rec7a	en	Recital 7 a (new)
rec7b	en	Recital 7 b (new)
art2a	en	Article 2, letter (a)
art2b	en	Article 2, letter (b)
art4_1	en	Article 4, paragraph 1
art4_2	en	Article 4, paragraph 2
art4_3	en	Article 4, paragraph 3
art4_3a	en	Article 4, paragraph 3a (new)
art6a	en	Article 6 a (new)
art8ca	en	Article 8, letter (c a) (new)
art2a	es	Art�culo 2, letra a)
art4_1	es	Art�culo 4, apartado 1
art4_2	es	Art�culo 4, apartado 2
art4_3	es	Art�culo 4, apartado 3
art4_3a	es	Art�culo 4, apartado 3 bis (nuevo)
art4_1	fi	4 artiklan 1 kohta
art4_2	fi	4 artiklan 2 kohta
art4_3	fi	4 artiklan 3 kohta
art4_3a	fi	4 artiklan 3 a kohta (uusi)
art5	fi	5 artiklan c ja d alakohta (uusi)
art8	fi	8 artiklan b ja c alakohta
rec7a	fr	Consid�rant 7 bis (nouveau)
art2a	fr	Article 2, point (a)
art2b	fr	Article 2, point (b)
art4_1	fr	Article 4, paragraphe 1
art4_2	fr	Article 4, paragraphe 2
art4_3	fr	Article 4, paragraphe 3
art4_3a	fr	Article 4 paragraphe 3 bis (nouveau)
rec5	it	Considerando 5
art4_1	it	Articolo 4, paragrafo 1
art4_2	it	Articolo 4, paragrafo 2
art4_3	it	Articolo 4, paragrafo 3
art4_3a	it	Articolo 4, paragrafo 3 bis (nuovo)
art8c	it	Articolo 8, lettera c)
art4_1	nl	Artikel 4, lid 1
art4_2	nl	Artikel 4, lid 2
art4_3	nl	Artikel 4, lid 3
art4_3a	nl	Artikel 4, lid 3 bis (nieuw)
art8ca	nl	Artikel 8, letter c bis) (nieuw)
art4_1	pt	Artigo 4, nº 1
art4_2	pt	Artigo 4, nº 2
art4_3	pt	Artigo 4, nº 3
art4_3a	pt	Artigo 4, nº 3 bis (novo)
art4_1	sv	Artikel 4, punkt 1
art4_2	sv	Artikel 4, punkt 2
art4_3	sv	Artikel 4, punkt 3
art4_3a	sv	Artikel 4, punkt 3a (ny)
\.


--
-- Data for TOC entry 78 (OID 24309)
-- Name: juri_item; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY juri_item (id, lang, juri_item) FROM stdin;
art8c	da	Artikel 8, litra c
rec13a	de	Erw�gung 13 a (neu)
rec13b	de	Erw�gung 13 b (neu)
rec13c	de	Erw�gung 13 c (neu)
rec13d	de	Erw�gung 13 d (neu)
art4a	de	Artikel 4 a (neu)
art6a	de	Artikel 6 a (neu)
art8a	de	Artikel 8 a (neu)
rec7a	en	Recital 7 a (new)
art6a	en	Article 6 a (new)
art9_1	en	Article 9, paragraph 1, first subparagraph
art2a	es	Art�culo 2, letra a)
art5	es	Art�culo 5
art5	fi	5 artikla
art8	fi	8 artiklan b ja c alakohta
art2a	fr	Article 2, point (a)
rec5	it	Considerando 5
art8c	it	Articolo 8, lettera c)
rec13d	nl	Overweging 13 quinquies
art5	pt	Artigo 5
art9_1	pt	Artigo 9, n° 1, par�grafo 1
\.


--
-- Data for TOC entry 79 (OID 26321)
-- Name: cec_item; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY cec_item (id, lang, cec_item) FROM stdin;
rec1	fi	Recital 1
rec5	fi	Recital 5
rec6	fi	Recital 6
rec7	fi	Recital 7
rec11	fi	Recital 11
rec12	fi	Recital 12
rec13	fi	Recital 13
rec14	fi	Recital 14
rec16	fi	Recital 16
rec17	fi	Recital 17
rec18	fi	Recital 18
art5	fi	5 artikla
rec5	it	Considerando 5
art3	pt	Artigo 3.�
art4	pt	Artigo 4.�
art6	pt	Artigo 6.�
art7	pt	Artigo 7.�
\.


--
-- Data for TOC entry 80 (OID 26975)
-- Name: caps; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY caps (id, lang, caps) FROM stdin;
rec1	fr	Rien de mieux que dans l'original.
rec5	fr	Position dogmatique affirmant que les brevets par d�finitioon stimulent le d�veloppement.
rec5a	fr	R�affirme l'article 52 de la CBE.
rec6	fr	Mieux vaut une suppression, interpr�tation do�teuse des ADPIC.
rec7	fr	R�affirme l'article 52 de la CBE.
rec7b	fr	Plus grande responsabilit� de l'OEB.
rec11	fr	Confond les crit�res de brevetabilt�s.
rec12	fr	D�finit justement une "invention technique".
rec13	fr	Supprime l'ambigu�t� au sujet de la brvetabilit� des algorithme.
rec13a	fr	�nonc� pauvre mais OK.
rec13c	fr	Dit que les algorithme ne sont pas techniques (+); mais peuvent �tre brevetables (-).
rec13d	fr	D�finition du terme "revendications" (+); Sugg�re que les logiciels sont brevetables en utilisant les bonnes formulations (-).
rec16	fr	L'unification de la jurisprendence ne garantit pas la concurrence.
rec17	fr	Les brevets logiciels soul�vent leurs propres probl�mes de concurrence.
rec18a	fr	Implique que les programmes d'ordinateur pourraient �tre brevetables.
art2a	fr	Confirme clairement l'article 52 de la CBE.
art2b	fr	Correspond sans ambigu�t� � l'article 52 de la CBE.
art2ba	fr	D�finit justement le terme "domaine technique"
art2bb	fr	D�finit justement le terme "industrie"
art3	fr	Supprime la cons�quence faisant que les logiciels soient brevetables.
art3a	fr	D�finit justement le terme "technique"
art4_1	fr	Implique que les programmes sont brevetables, en contradication avec l'article 52 de la CBE.
art4_2	fr	Implique que les programmes sont brevetables, en contradication avec l'article 52 de la CBE.
art4_3a	fr	�claircit le terme "invention technique".
art4a	fr	Utilise des termes ind�finis, cr�ant des �chappatoires.
art4b	fr	Emp�che clairement les brevets sur les m�thodes pour l'exercice d'activit�s �conomiques.
art6a	fr	Assure que les brevets ne doivent pas emp�cher l'interop�rabilit�.
art7	fr	Impacts d'un suivi.
art8b	fr	Contr�le parlementaire.
art8cb	fr	P�riode de gr�ce = ins�curit� suppl�mentaire.
art8ce	fr	Contr�le parlementaire.
art8cf	fr	Interop�rabilt� - obscure.
art8cg	fr	Interop�rabilt� - meilleur.
art8_1a	fr	Clarifie le but du rapport.
\.


--
-- Data for TOC entry 81 (OID 26984)
-- Name: comm; Type: TABLE DATA; Schema: public; Owner: gibus
--

COPY comm (id, lang, comm) FROM stdin;
rec1	fr	La Commission affirme p�remptoirement, contre toute <a href="../../../archive/mirror/impact/index.en.html">�vidence �conomique</a>, y compris une <a href="http://www.researchoninnovation.org/swpat.pdf">enqu�te tr�s fouill�e de Bessen & Hunt en 2003</a>, De plus, le terme &quot;invention mise en oeuvre par ordinateur&quot; pr�te � confusion. Il est pr�f�rable de n'avoir aucune d�claration d'intention plut�t qu'une fausse.
rec5	fr	La Commission europ�enne (CCE) et l'amendement 2 (JURI) affirment dogmatiquement que les brevets logiciels stimulent l'innovation. Il serait plus appropri� de d�clarer un but politique, � l'aune duquel le r�gime de brevet propos� peut �tre mesur�. <p> L'amendement 2 semble pr�tendre que le seul fait d'�crire une loi de l'UE, quel qu'en soit le contenu, engendre une &quot;certitude juridique&quot;, simplement parce que la cour du Luxembourg peut interpr�ter cette loi. Cette nouvelle d�claration d'intention n'est pas seulement discutable, mais aussi compl�tement inutile, parce que <ol><li>le Brevet de la Communaut� est en cours de toute fa�on</li> <li>il y a des moyens plus faciles et plus syt�matiques de d�l�guer � la Cour du Luxembourg que de faire passer une directive interpr�tative concernant l'un des aspect de la Convention europ�enne sur le brevet</li></ol>
rec6	fr	Nous devons pr�ciser qu'il y a des limites quant � ce qui peut �tre consid�r� comme &quot;champs d'application de la technologie&quot; selon l'Art 27 des ADPIC et que cet article n'est pas con�u pour permettre une brevetabilit� illimit�e mais plut�t pour �viter des frictions dans le libre-�change, qui peuvent �tre caus�es par des exceptions indues mais aussi par des extensions indues de la brevetabilit�. Cette interpr�tation des ADPIC est indirectement confirm�e par le r�cent lobbying du gouvernement am�ricain contre l'Art 27 des ADPIC sur la base qu'il peut exclure (ou peut �tre interpr�t� de mani�re � exclure) la brevetabilit� des logiciels et des m�thodes destin�es � l'exercice d'activit�s �conomiques, sur lesquels le gouvernement am�ricain d�sire que l'�bauche du nouveau Substantive Patent Law Treaty fasse autorit�, voir l'article de Brian Kahin dans First Monday. L'amendement 31 supprime l'article factuellement incorrect. <p> La suppression est une bonne chose, mais la clart� aurait �t� pr�f�rable.
rec7	fr	L'article 52 de la CBE dit que les programmes d'ordinateur ne sont pas <em>des inventions</em> au sens du droits sur les brevet, c'est-�-dire qu'un syst�me qui consiste en un mat�riel g�n�raique de calcul et des combinaisons de r�gles de calculs agissant sur sur celui-ci ne peuvent faire l'objet d'un brevet. Il ne dit pas que de tels syst�mes ne peuvent pas �tre brevet�s en d�clarant qu'ils &quot;ne sont pas en tant que tels&quot; ou &quot;de caract�re technique&quot;. L'amendement 5 corrige cette erreur en r�affiramnt l'article 52 de la CBE. Il est � noter que l'exclusion des programmes d'ordinateur n'est pas une exception, elle fait partie de la r�gle pour d�finir ce qui est &quot;une invention&quot;.
rec7b	fr	Nouvelle r�solution appelant � une plus grande responsabilit� de la CBE. L'effet serait plus fort si c'�tait un article, mais c'est quand m�me une bonne position de principe. Il est important d'�tablir toutes les sources de conflits pour faciliter la d�termination des solutions.
rec11	fr	Les quatre tests de brevetabilit� indiqu�s dans l'Art 52 de la CBE d�clarent qu'il doit y avoir une invention (technique) et qu'additionnellement, cette invention doit �tre susceptible d'une application industrielle, �tre nouvelle, et comporter une activit� inventive. La condition d'&quot;activit� inventive&quot; est d�finie � l'Art 52 de la CBE comme requ�rant que l'invention ne soit pas une combinaison �vidente de techniques connues d'une personne experte dans le domaine. Additionnellement, &quot;invention&quot; et &quot;contribution technique&quot; sont synonymes
rec12	fr	L'amendement 5 de la JURI pose plusieurs probl�mes
rec13	fr	Ce consid�rant pr�tend exclure les algorithmes de la brevetabilit�, mais en fait, ils les rend brevetables, car un algorithme qui est d�fini en r�f�rence � l'environnement d'un �quipement g�n�rique de traitement de donn�es (= &quot;un environnement physique&quot;) est excatement identique � un algorithme per se.
rec13a	fr	L'ambigu�t� syntaxique de cet amendement ne clarifie pas si les &quot;m�thodes destin�es � l'exercice d'activit�s �conomiques&quot; en g�n�ral sont consid�r�es comme des &quot;m�thodes dans lesquelles la seule contribution � l'�tat de l'art est non-technique&quot; ou si certaines m�thodes destin�es � l'exercice d'activit�s �conomiques pourraient contenir des &quot;contributions techniques&quot;. Ce qui est plus important, cet amendement n'exclura rien, parce que les avocats en brevets seront prompts � revendiquer que l'&quot;invention&quot; est caract�ris�es par quelque propri�t�s informatiques, laquelle invention doit �tre &quot;consid�r�e comme un tout&quot;, comme il en est question ailleurs dans la proposition de directive. Pour que ceci soit d'une utilit� quelconque, l'ambigu�t� syntaxique entourant &quot;les autres m�thodes&quot; devrait �tre �limin�e et la cat�gorie des &quot;m�thodes non techniques&quot; devrait �tre expliqu�e par des d�finitions et/ou des exemples.
rec13b	fr	Ceci exprime un point de vue de bon sens qui a souvent �t� �cart� pour pouvoir �tendre le domaine de la brevetabilit�. Seul, il n'accomplit pas grand chose et la d�claration dans la loi que la loi &quot;ne doit pas pouvoir �tre contourn�e&quot; est un voeu pieux. Mais c'est toujours mieux que rien.
rec13c	fr	Dans le cadre de la pratique de l'examen des brevets, cette d�claration n'est pas ce qu'elle semble �tre. Elle �largit le domaine de brevetabilit� m�me en comparaison avec les pratiques de la CBE, en permettant la brevetabilit� de solutions non techniques de probl�mes techniques. De plus, elle rend les algorithmes brevetables dans toutes les situtations, parce qu'un &quot;probl�me technique&quot; peut toujours �tre d�fini en termes de mat�riel informatique g�n�rique, ce qui fournit en m�me temps un moyen de revendiquer des algorithmes sous leur forme la plus abstraite.
rec13d	fr	L'amendement ne fait que rappeler la d�finition de la revendication de brevet
rec14	fr	Le consid�rant de la CCE n'est pas trop mauvais (bien qu'il utilise le terme d'&quot;inventions mises en oeuvre sur ordinateur&quot;, voir l'article 1). Il dit simplement que la loi sur les brevets ne devrait pas �tre remplac�e. L'amendement 86 ajoute que l'on doit �viter la brevetabilit� de &quot;proc�dures triviales et de m�thodes destin�es � l'exercice d'activit�s �conomiques&quot;. Non seulement les m�thodes triviales destin�es � l'exercice d'activit�s �conomiques doivent rester non brevetables, mais toutes les m�thodes destin�es � l'exercice d'activit�s �conomiques doivent le rester (autrement nous nous �cartons de l'Art 52 de la CBE).
rec16	fr	L'unification de la jurisprudence n'est pas en soi une garantie de l'am�lioration de la situation de l'industrie europ�enne. Cette directive ne devrait pas utiliser le pr�texte de l'&quot;harmonisation&quot; pour changer les r�glements de l'Art 52 de la CBE, qui sont d�j� d'application dans tous les pays. Que les brevets logiciels soient ou non favorables au d�veloppement des soci�t�s �ditrices de logiciel est ind�pendant du fait que l'industrie des manufactures traditionnelles se d�place vers des �conomies � bas co�ts, hors de l'UE. L'amendement 11 et le texte de la CCE supposent que l'extension de la protection par le brevet � l'industrie du d�veloppement logiciel am�liorera la comp�titivit� des soci�t�s de l'UE, malgr� que 75% des 30000 brevets logiciels d�j� accord�s (que la CCE/JURI nomme brevets sur des &quot;inventions mises en oeuvre sur ordinateur&quot;) sont entre les mains des soci�t�s am�ricaines et japonaises. De plus, si l'Europe n'entre pas dans le syst�me des brevets logiciels, les soci�t�s europ�ennes peuvent toujours parfaitement acqu�rir des brevets aux �tats-Unis et ailleurs, et les rendre ex�cutoires l�-bas, alors que les soci�t�s �trang�res ne pourront faire de m�me ici. En ce sens, ne pas participer au syst�me des brevets logiciels est un avantage comp�titif.
rec17	fr	Les brevets logiciels apportent leurs propres probl�mes pour la concurrence. Il aurait �t� appropri� de les r�soudre dans la directive et de r�guler la mani�re dont les articles 30-31 des ADPICS s'appliquent, par exemple par un consid�rant soutenant l'article Art 6a. Il ne suffit pas de se reposer sur le droit existant sur la concurrence. Ainsi, il aurait �t� plus appropri� de supprimer ce consid�rant ou de le remplacer par quelque chose se chargeant des probl�mes auxquels le l�gislateur est confront�. Si cet amendement est soutenu, il pourrait �tre interpr�t� comme un signe que le Parlement europ�en reconnait l'�chec de la Commission europ�enne � s'occuper des probl�mes de concurrence dans cette directive.
rec18	fr	La proposition de la CCE fait seulement semblant de prot�ger l'interop�rabilit�, mais en r�alit� fait en sorte que les droits du d�tenteur de brevet ne puissent �tre limit�s en aucune mani�re par des consid�rations d'interop�rabilit�. Voir � ce propos l'explication de l'Art 6. L'amendement 13 ne fait que restreindre encore plus les dispositions pouvant �tre utilis�es pour passer outre aux droits du brevet.
rec18a	fr	Ce consid�rant sugg�re �galement que les programmes d'ordinateurs peuvent �tre des inventions. Il est �galement sans effet, car il ne fait que r�affirmer des doctrines sans contreverse sur les brevets.
art2a	fr	L'article 52 de la CBE statue clairement qu'un programme d'ordinateur (un objet revendiqu� consistant en un �quipement de traitement de donn�es et les r�gles pour qu'il op�re) ne peut pas constituer une invention brevetable.<p> Les amendements 36, 117 et 42 pr�cisent qu'une innovation est brevetable seulement si elle se conforme � l'article 52 de la CBE, sans se soucier qu'un programme d'ordinateur fasse ou non partie de sa mise en oeuvre.</dd></dl>
art2b	fr	Une directive qui ferait d�pendre la brevetabilit� du terme &quot;contribution technique&quot; doit expliquer ce terme clairement. <p> L'amendement 69 va dans le bon sens en d�clarant que &quot;le traitement, la manipulation et la pr�sentation d'informations n'appartiennent pas � un domaine technique&quot;. Il risque de m�langer la condition de &quot;contribution technique&quot; (= invention) aux conditions de non �vidence, similairement � l'amendement 96. Cependant m�me si les formulations sont redondantes, il n'y a aucun mal � dire que la contribution (invention) doit �tre &quot;non �vidente&quot;. Ceci peut m�me aider � corriger les mauvaise interpr�tations de l'OEB, d'apr�s qui &quot;la non �vidence doit contenir une contribution technique&quot;. �tant donn� que cet amendement d�finit �galement ce que sont des &quot;domaines techniques&quot; (article 27 des ADPIC), il est extr�mement utile. <p> Enfin, l'amendement 107 a tout juste
art2ba	fr	Puisque la tentative du rapporteur de &quot;rendre clair ce qui est brevetable de ce qu'il ne l'est pas&quot; repose enti�rement sur le mot &quot;technique&quot;, ce terme doit �tre d�fini clairement et de fa�on restrictive. La seule d�finition qui remplisse ces conditions est celle des lignes des amendements 55 et 97 (l'amendement 37, qui est bon aussi, sera retir� pour �viter un conflit avec celui-ci), qui est trouv� dans des d�cisions de justice de diff�rents pays et dans quelques lois sur les brevets (par exemple le Trait� sur les Brevets Nordique et les lois sur les brevets de la Pologne, du Japon, de Ta�wan et autres). Cette d�finition est fond�e sur des concepts valides de science et d'�pist�mologie et a prouv� avoir une signification claire dans la pratique. Elle assure que les droits d'exclusion, �tendus, chers et plein d'ins�curit� tels que les brevets, sont utilis�s dans des domaines o� il y a un raisonnement �conomique pour eux, et que l'innovation abstraite-logique est du domaine des droits de copyright ou de droits du m�me genre seulement. Le mot &quot;num�rique&quot; dans l'amendement 55 est apparemment le r�sultat d'une mauvaise traduction � partir du fran�ais. La d�finition brute donn�e ici correspond � l'acception commune du terme de par le monde, implicite dans le cas juridique en cours et dans les autres d�clarations du rapport de la JURI sur ce qui doit �tre brevetable (par exemple &quot;pas le logiciel en tant que tel, mais les inventions relatives aux t�l�phones mobiles, aux appareils de contr�le de moteur, aux appareils domotiques, ...&quot; ). Cette acception doit �tre rendue explicite � des fins de clarification.</dd></dl>
art2bb	fr	Nous ne voulons pas d'innovations dans l'&quot;industrie musicale&quot; ou l'&quot;industrie des services juridiques&quot; pour �tre en accord avec la condition d'&quot;application industrielle&quot; des ADPIC. Le mot &quot;industrie&quot; est de nos jours souvent utilis� dans un sens �tendu qui n'est pas appropri� dans le contexte de la loi sur les brevets.</dd></dl>
art3	fr	Le texte de la Commission Europ�enne dit que toutes les id�es &quot;mises en oeuvre par ordinateur&quot; sont des inventions brevetables selon l'article 27 des ADPIC. Son rejet serait une bonne chose, une clarification de la mani�re dont s'applique l'article 27 des ADPI serait encore mieux.</dd></dl>
art3a	fr	Un des objectifs majeurs de ce projet de directive a �t� de clarifier l'interpr�tation de l'article 52 de la CBE � la lumi�re de l'article 27 des ADPIC, qui dit que les &quot;inventions de tous les domaines de technologie&quot; doivent �tre brevetables. Nous proposons de traduire l'article 52(2) de la CBE dans le langage de l'article 27 des ADPIC, dans l'amendement CULT-16. <p> Si le traitement de donn�es est &quot;technique&quot;, alors tout est &quot;technique&quot;. <p> Le traitement de donn�es est la base commune � tous les domaines technologiques et non technologiques. Avec l'av�nement de l'ordinateur (universel) dans les ann�es 1950, le traitement de donn�es automatis� s'est r�pandu dans la soci�t� et l'industrie. <p> Comme Gert Kolle, le th�oricien � l'origine des d�cisions des ann�es 1970 d'exclure le logiciel de la brevetabilit�, l'�crit en 1977 (voir Gert Kolle 1977
art4_3a	fr	Comme mentionn� dans l'analyse de l'article 1, &quot;invention mise en oeuvre par ordinateur&quot; est un terme contradictoire. Comme mentionn� dans l'analyse du consid�rant 11, &quot;invention&quot; et &quot;contribution technique&quot; sont synonymes
art4a	fr	L'amendement 17 pr�sume que &quot;l'interaction physique normale entre un programme et l'ordinateur&quot; signifie quelque chose. Ce n'est pas le cas. On aurait pu trouver un sens, par exemple en sp�cifiant, � l'instar de la Cour f�d�rale des brevets allemande dans une r�cente <a href="../../bpatg17-suche02/index.de.html">d�cision sur la Recherche d'erreurs (Suche Ferhlerhafter Zeichenketten)</a>, que l'&quot;augmentation de l'efficacit� de calcul&quot;, &quot;les �conomies dans l'utilisation de la m�moire&quot;, etc. &quot;ne constituent pas une contribution technique&quot;, ni ne &quot;sont dans la port�e de l'interaction physique normale entre un programme et un ordinateur&quot;. En l'absence d'une telle sp�cification, l'amendement 17 ne fait qu'obscurcir au lieu de clarifier.</dd></dl>
art4b	fr	La directive propos�e par la CEC et la JURI, bas�e sur la doctrine du&quot;Syst�me de pension b�n�ficiaire&quot; de l'OEB et sur le Standard trilat�ral de 200 des offices de brevets US, japonais et europ�en, consid�re que tous les programmes d'ordinateur sont des inventions brevetables. Ceci est manifeste dans le terme &quot;invention mise en oeuvre par ordinateur&quot;. Il n'existe plus de test pour savoir s'il existe ou non un caract�re inventif, mais seulement un test sur la &quot;contribution technique dans l'activit� inventive&quot;, c'est-�-dire que l'activit� entre &quot;l'�tat de l'art le plus proche&quot; et &quot;l'invention&quot; doit �tre circonscrite en terme de &quot;probl�me technique&quot;, par exemple la question de l'am�lioration la vitesse ou l'efficacit� dans l'utilisation de la m�moire d'un ordinateur. En appliquant cette doctrine de l'OEB, tout algorithme ou toute m�thode destin�e � l'exercice d'activit�es �conomiques devient effectivement brevetable, comme cela a �t� soulign� par la Cour f�d�rale des brevets allemande, qui a refus� de suivre cette doctrine de l'OEB dans une <a href="../../bpatg17-suche02/index.de.html">d�cision r�cente</a>. <p> Voir <a href="../tech/index.en.html">Pourquoi Le One-Click Shopping de Amazon est Brevetable selon la Directive Propos�e</a> <p> L'amendement 60 dit clairement � l'OEB de suivre l'approche de la Cour f�d�rale des brevets allemande en refusant de consid�rer que l'am�lioration de l'efficacit� du traitement de donn�e soit soint une &quot;contribution technique&quot;.</dd></dl>
art6	fr	La proposition de la Commission Europ�enne ne prot�ge pas l'interop�rabilit� mais, au contraire, assure qu'un logiciel interop�rable ne peut pas �tre publi� quand son interface est brevet�e. Le seul comportement que la Commission Europ�enne veut permettre dans ce cas est la d�compilation, qui ne constituerait pas une contrefa�on de brevet de toute fa�on (car les brevets impliquent l'usage de ce qui est d�crit dans les revendications, et non l'�tude dudit objet). Les amendements 66 et 67, de plus, sp�cifient les lois sur lesquelles les privil�ges d'interop�rabilit� peuvent �tre fond�s, les rendant par cons�quent encore plus s�rs, de sorte que la r�serve ne peut plus avoir d'effets.</dd></dl>
art7	fr	Cet article n'a aucun effet r�gulatoire. Tel que formul� actuellement, il donne plut�t � l'Unit� charg�e de la propri�t� industrielle au March� int�rieur de la Commission europ�enne davantage de fonds et d'opportunit�s pour produire des &quot;�tudes&quot; de propagande qui <a href="http://www.researchineurope.org/policy/critique.htm">ne b�n�ficie d'aucune reconnaissance de la part de la communaut� scientifique</a>. Bien que les efforts pour sousmettre et commenter les amendements sur cet article soient presque vains, cela ne fait aucun do�te que le th�me propos� dans l'amendement 91 par Marianne Thysse est plus digne d'�tre �tudi� que ce qui a �t� propos� jusqu'ici par la CEC et la JURI.</dd></dl>
\.


--
-- TOC entry 28 (OID 23651)
-- Name: item_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY item
    ADD CONSTRAINT item_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 29 (OID 23659)
-- Name: name_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY name
    ADD CONSTRAINT name_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 30 (OID 23667)
-- Name: cec_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cec
    ADD CONSTRAINT cec_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 31 (OID 23676)
-- Name: am_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY am
    ADD CONSTRAINT am_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 32 (OID 23684)
-- Name: auth_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY auth
    ADD CONSTRAINT auth_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 33 (OID 23693)
-- Name: cm_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cm
    ADD CONSTRAINT cm_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 34 (OID 23701)
-- Name: juri_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY juri
    ADD CONSTRAINT juri_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 35 (OID 23709)
-- Name: parl_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY parl
    ADD CONSTRAINT parl_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 36 (OID 23717)
-- Name: acpt_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY acpt
    ADD CONSTRAINT acpt_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 37 (OID 23725)
-- Name: ffii_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY ffii
    ADD CONSTRAINT ffii_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 38 (OID 23733)
-- Name: just_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY just
    ADD CONSTRAINT just_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 39 (OID 23756)
-- Name: itre_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY itre
    ADD CONSTRAINT itre_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 40 (OID 23768)
-- Name: itre_just_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY itre_just
    ADD CONSTRAINT itre_just_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 41 (OID 23788)
-- Name: parl_cec_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY parl_cec
    ADD CONSTRAINT parl_cec_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 42 (OID 23819)
-- Name: cult_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cult
    ADD CONSTRAINT cult_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 43 (OID 23832)
-- Name: cult_just_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cult_just
    ADD CONSTRAINT cult_just_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 44 (OID 23872)
-- Name: juri_just_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY juri_just
    ADD CONSTRAINT juri_just_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 45 (OID 23885)
-- Name: juri_cec_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY juri_cec
    ADD CONSTRAINT juri_cec_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 46 (OID 24017)
-- Name: cult_cec_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cult_cec
    ADD CONSTRAINT cult_cec_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 47 (OID 24025)
-- Name: itre_cec_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY itre_cec
    ADD CONSTRAINT itre_cec_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 48 (OID 24112)
-- Name: cult_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cult_item
    ADD CONSTRAINT cult_item_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 49 (OID 24120)
-- Name: itre_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY itre_item
    ADD CONSTRAINT itre_item_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 50 (OID 24314)
-- Name: juri_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY juri_item
    ADD CONSTRAINT juri_item_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 51 (OID 26326)
-- Name: cec_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY cec_item
    ADD CONSTRAINT cec_item_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 52 (OID 26980)
-- Name: caps_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY caps
    ADD CONSTRAINT caps_pkey PRIMARY KEY (id, lang);


--
-- TOC entry 53 (OID 26989)
-- Name: comm_pkey; Type: CONSTRAINT; Schema: public; Owner: gibus
--

ALTER TABLE ONLY comm
    ADD CONSTRAINT comm_pkey PRIMARY KEY (id, lang);


