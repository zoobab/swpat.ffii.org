 28 August 2003                                                                               A5-0238/ 29

 AMENDMENT 29
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                        A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                          Amendment 29
                                                Title

 Proposal for a directive of the European                Proposal for a directive of the European
 Parliament and of the Council on the                    Parliament and of the Council on the limits
 patentability of computer-implemented                   of patentability with respect to automated
 inventions                                              data processing and its fields of
                                                         application

                                             Justification

 The term "computer-implemented invention" is not used by computer professionals. It is in fact
 not in wide use at all. It was introduced by EPO in May 2000 to legitimate business method
 patents, so as to bring EPO practise in line with the USA and Japan. The term "computer-
 implemented invention" is a programmatic statement. It implies that calculation rules framed in
 the terms of the general-purpose computer are patentable inventions. This implication is in
 contradiction with Art 52 EPC, according to which algorithms, business methods and programs
 for computers are not inventions in the sense of patent law. It can not be the aim of the current
 directive to declare all kinds of "computer-implemented" ideas to be patentable inventions.
 Rather the aim is to clarify the limits of patentability with regard to automatic data processing
 and its various (technical and non-technical) fields of application, and this must be expressed
 in the title in plain and unambiguous wording.   


                                                                                                  Or. en





                                                                                       PE 333.851/ 29

EN                                                                                                          EN


 28 August 2003                                                                         A5-0238/ 30

 AMENDMENT 30
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                       A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


                Commission proposal                                Amendments by Parliament

                                            Amendment 30
                                               Recital 1

 (1) The realisation of the internal market             Deleted
 implies the elimination of restrictions to
 free circulation and of distortions in
 competition, while creating an
 environment which is favourable to
 innovation and investment. In this context
 the protection of inventions by means of
 patents is an essential element for the
 success of the internal market. effective
 and harmonised protection of computer-
 implemented inventions throughout the
 Member States is essential in order to
 maintain and encourage investment in
 this field.


                                              Justification

 There is no evidence to support the Commission's suggestion that patents promote innovation in
 the field of software. Various economic studies suggest the contrary.


                                                                                               Or. en





                                                                                     PE 333.851/ 30

EN                                                                                                       EN


 28 August 2003                                                                         A5-0238/ 31

 AMENDMENT 31
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                      A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


             Commission proposal                                   Amendments by Parliament

                                            Amendment 31
                                               Recital 6

 (6) The Community and its Member                       Deleted
 States are bound by the Agreement on
 trade-related aspects of intellectual
 property rights (TRIPS), approved by
 Council Decision 94/800/EC of 22
 December 1994 concerning the
 conclusion on behalf of the European
 Community, as regards matters within its
 competence, of the agreements reached in
 the Uruguay Round multilateral
 negotiations (1986-1994). Article 27(1) of
 TRIPS provides that patents shall be
 available for any inventions, whether
 products or processes, in all fields of
 technology, provided that they are new,
 involve an inventive step and are capable
 of industrial application. Moreover,
 according to TRIPS, patent rights should
 be available and patent rights enjoyable
 without discrimination as to the field of
 technology. These principles should
 accordingly apply to computer-
 implemented inventions.


                                              Justification

 According to Art 27 TRIPS there are limits to what can be subsumed under "fields of
 technology". This article is not designed to mandate unlimited patentability but rather to avoid
 frictions in free trade.
                                                                                               Or. en



                                                                                     PE 333.851/ 31

EN                                                                                                       EN


 28 August 2003                                                                            A5-0238/ 32

 AMENDMENT 32
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                          A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


           Commission proposal                                     Amendments by Parliament

                                            Amendment 32
                                                Recital 7

 (7) Under the Convention on the Grant of               (7) Under the Convention on the Grant of
 European Patents signed in Munich on                   European Patents signed in Munich on
 5 October 1973 and the patent laws of the              5 October 1973 and the patent laws of the
 Member States, programs for computers                  Member States, programs for computers
 together with discoveries, scientific                  together with discoveries, scientific
 theories, mathematical methods, aesthetic              theories, mathematical methods, aesthetic
 creations, schemes, rules and methods for              creations, schemes, rules and methods for
 performing mental acts, playing games or               performing mental acts, playing games or
 doing business, and presentations of                   doing business, and presentations of
 information are expressly not regarded as              information are expressly not regarded as
 inventions and are therefore excluded from             inventions and are therefore excluded from
 patentability. This exception, however,                patentability. This exception applies
 applies and is justified only to the extent            because the said subject-matter and
 that a patent application or patent relates            activities do not belong to a field of
 to such subject-matter or activities as                technology.
 such, because the said subject-matter and
 activities as such do not belong to a field
 of technology.


                                             Justification

 A clarification in accordance with the EPC.


                                                                                                  Or. en





                                                                                        PE 333.851/ 32

EN                                                                                                          EN


 28 August 2003                                                                           A5-0238/ 33

 AMENDMENT 33
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                      A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                   Amendments by Parliament

                                         Amendment 33
                                              Recital 11

 (11) Although computer-implemented                    Deleted
 inventions are considered to belong to a
 field of technology, in order to involve an
 inventive step, in common with inventions
 in general, they should make a technical
 contribution to the state of the art


                                             Justification

 Computer programs are abstract and do not belong to any particular field. The Commission
 text removes the independent requirement of invention ("technical contribution") and merges it
 into the requirement of non-obviousness ("inventive step"). This leads to theoretical
 inconsistency and undesirable practical consequences.
                                                                                              Or. en





                                                                                    PE 333.851/ 33

EN                                                                                                      EN


 28 August 2003                                                                           A5-0238/ 34

 AMENDMENT 34
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                        A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                    Amendments by Parliament

                                           Amendment 34
                                              Recital 13

 (13) A defined procedure or sequence of               Deleted
 actions when performed in the context of
 an apparatus such as a computer may
 make a technical contribution to the state
 of the art and thereby constitute a
 patentable invention. However, an
 algorithm which is defined without
 reference to a physical environment is
 inherently non-technical and cannot
 therefore constitute a patentable
 invention.

                                             Justification

 It is absurd and contrary to the goal of clarification to try to make an artificial distinction
 between mathematical algorithms and nonmathematical algorithms. To a computer scientist,
 this makes no sense, because every algorithm is as mathematical as anything could be. An
 algorithm is an abstract concept unrelated to physical laws of the universe.


                                                                                               Or. en





                                                                                       PE 333.851/ 34

EN                                                                                                       EN


 28 August 2003                                                                             A5-0238/ 35

 AMENDMENT 35
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                          A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                              Amendment 35
                                                Recital 16

 (16) The competitive position of European               (16) The competitive position of European
 industry in relation to its major trading               industry in relation to its major trading
 partners would be improved if the current               partners could be improved if the current
 differences in the legal protection of                  schism in judicial practice concerning the
 computer-implemented inventions were                    limits of patentability with regard to
 eliminated and the legal situation was                  computer programs was eliminated.
 transparent.

                                               Justification

 Unification of caselaw in itself is not a guarantee of improvement of the situation of European
 industry. This directive should not use a pretext of "harmonisation" for changing the rules of
 Art 52 EPC, which are already in force in all countries.


                                                                                                   Or. en





                                                                                         PE 333.851/ 35

EN                                                                                                           EN


 28 August 2003                                                                      A5-0238/ 36

 AMENDMENT 36
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                    A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


           Commission proposal                                 Amendments by Parliament

                                            Amendment 36
                                       Article 2, point (a)

 (a) "computer-implemented invention"               (a) "computer-implemented invention"
 means any invention the performance of             means any invention in the sense of the
 which involves the use of a computer,              European Patent Convention the
 computer network or other programmable             performance of which involves the use of a
 apparatus and having one or more prima             computer, computer network or other
 facie novel features which are realised            programmable apparatus and having in its
 wholly or partly by means of a computer            implementations one or more non-
 program or computer programs;                      technical features which are realised
                                                    wholly or partly by a computer program or
                                                    computer programs, besides the technical
                                                    features that any invention must
                                                    contribute;




                                                                                             Or. en





                                                                                  PE 333.851/ 36

EN                                                                                                     EN


 28 August 2003                                                                           A5-0238/ 37

 AMENDMENT 37
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                        A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                      Amendments by Parliament

                                           Amendment 37
                                      Article 2, point (ba) (new)

                                                       (ba) "technology" in the sense of patent
                                                       law means "applied natural science";
                                                       "technical" in the sense of patent law
                                                       means "concrete and physical".


                                             Justification

 TRIPs obliges us to define "technology" and related terms and to rely on them for delimiting
 what is patentable. The above definition is explicitely or implicitely used by all patent
 jurisdictions, including the EPO.


                                                                                                 Or. en





                                                                                       PE 333.851/ 37

EN                                                                                                         EN


 28 August 2003                                                                        A5-0238/ 38

 AMENDMENT 38
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                     A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                   Amendments by Parliament

                                        Amendment 38
                                   Article 2, point (bb) (new)

                                                    (bb) "industry" in the sense of patent law
                                                    means "automated production of material
                                                    goods";

                                          Justification

 The word "industry" is nowadays often used in extended meanings which are not appropriate in
 the context of patent law.


                                                                                              Or. en





                                                                                    PE 333.851/ 38

EN                                                                                                      EN


 28 August 2003                                                                          A5-0238/ 39

 AMENDMENT 39
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

       REPORT by Arlene McCarthy                                                       A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                          Amendment 39
                                     Article 2, point (bc) (new)

                                                      (bc) "invention" in the sense of patent
                                                      law means "solution of a problem by use
                                                      of controllable forces of nature".

                                            Justification

 This is a standard patent doctrine in most jurisdictions. The EPO says that inventions are
 "technical solutions of technical problems" and understands "technical" as "concrete and
 physical". The term "controllable forces of nature" clarifies this further.


                                                                                                Or. en





                                                                                      PE 333.851/ 39

EN                                                                                                        EN


 28 August 2003                                                                           A5-0238/ 40

 AMENDMENT 40
 tabled by Bent Hindrup Andersen, on behalf of the EDD Group

      REPORT by Arlene McCarthy                                                         A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


           Commission proposal                                      Amendments by Parliament

                                         Amendment 40
                                      Article 4, paragraph 2

 2. Member States shall ensure that it is a              2. Member States shall ensure that patents
 condition of involving an inventive step                on computerised innovations are upheld
 that a computer-implemented invention                   and enforced only if they were granted
 must make a technical contribution.                     according to the rules of Art 52 of the
                                                         European Patent Convention of 1973, as
                                                         explained in the European Patent Office's
                                                         Examination Guidelines of 1978.

                                               Justification

 This amendment avoids deviation from the European Patent Convention and therefore provides
 increased coherence and clarity.


                                                                                                Or. en





                                                                                       PE 333.851/ 40

EN                                                                                                        EN


