 18 septembre 2003                                                                            A5-0238/ 54

 AMENDEMENT 54
 d�pos� par Armando Cossutta, Francis Wurtz, Pernille Frahm  et  Ilda Figueiredo au nom du
 groupe GUE/NGL, Daniel Cohn-Bendit au nom du groupe Verts/ALE et Marco Cappato

       RAPPORT Arlene McCarthy                                                               A5-0238/2003
       Brevetabilit� des inventions mises en oeuvre par ordinateur

       Proposition de directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


 Rejette la proposition de directive.

                                             Justification

 En proposant la brevetabilit� des inventions mises en oeuvre par ordinateur, la Commission
 ouvre la voie � la brevetabilit� du savoir humain. De plus, cette directive ne r�pond pas aux
 enjeux �conomiques, scientifiques et culturels du secteur du logiciel ainsi qu'� la n�cessit� de
 promouvoir l'innovation. Pour toutes ces raisons et pour r�pondre � la fort opposition de
 scientifiques et d'�diteurs de logiciels, la proposition de directive doit �tre rejet�e.

                                                                                                   Or. fr





                                                                                        PE 333.851/ 54

FR                                                                                                           FR


