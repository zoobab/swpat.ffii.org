<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl 09/2003 Amendements sur la Directive des brevets logiciels: vraies contre fausses limites

#descr: Le Parlement Européen a prévu de de donner sa décision au sujet de la Directive sur les Brevets Logiciels le 24 septembre.  La directive telle qu'elle est proposée par la Commission Européenne démolit les fondements de la structure de la loi actuelle (article 52 of de la Convention sur les Brevets Européenne) et la remplace par le Standard Trilatéral developpé par les Offices de Brevets américain, européen et japonais en 2000, selon lequel toutes les solutions aux problèmes %(q:mises en oeuvre sur ordinateur) sont des inventions brevetables.  Quelques membres du Parlement ont proposé des amendements dans le but de soutenir le plus strict des concepts d'invention au sens de la Convention des Brevets Européenne, alors que d'autres ont poussé à la brevetabilité illimitée au sens du Standard Trilatéral, quoique dans un habillement rhétorique restrictif.  Nous avons essayé de faire une analyse comparative de tous les amendements proposés, de façon à aider les preneurs de décisions à reconnaître s'ils votent pour de réelles ou de fausses limites de brevetabilité.

#Lgn: Légende et exemple commenté

#ril: Articles et Motions

#eia: Rejet/Acceptation, Titre et Considérants

#enm: Numéro d'amendement (pour des raisons de référencement dans ce document, les vrais numéros d'amendements proposés seront probablement différents)

#jac: pour, contre, indécis

#tuW: 410 ont voté pour, 178 ont voté contre

#TitTit: Titre

#TitCec: Proposition de directive du Parlement européen et du Conseil concernant la brevetabilité des inventions mises en oeuvre par ordinateur

#trW: texte extrait %(op:de la proposition originale de la Commission Européenne)

#TitSyn: Le but de la directive en question ne peut être de déclarer que toutes les idées %(q:mises en oeuvre par ordinateur) sont des inventions brevetables. Le but est plutôt de clarifier les limites de la brevetabilité en ce qui concerne le traitement automatiques des données et ses divers champs (techniques et non techniques) d'application; ceci doit être exprimé dans le titre par une formulation nette et non ambiguë.

#ung: Exemple de justification/commentaires

#enm2: Numéro d'amendement

#uia: Soumetteurs

#amd: Recommandations de vote

#ogs: résultat du vote

#Am29: Proposition de directive du Parlement européen et du Conseil concernant les limites de la brevetabilité pour le traitement automatisé des données et ses domaines d'application

#eet: texte de l'amendement

#tIa: Extensions de l'assurance sur les brevets

#Art1: La présente directive établit des règles concernant la brevetabilité des inventions mises en oeuvre par ordinateur.

#nWw: Des idées mises en oeuvre dans un ordinateur générique ne sont pas des inventions au sens de l'article 52(2) de la CBE. L'expression %(q:invention mise en oeuvre par ordinateur) n'est pas connu de l'homme qualifiée dans ce domaine, et puisque les seules choses qui peuvent être mise en oeuvre par ordinateur sont des programmes d'ordinateur (une machine à laver ou un téléphone mobile ne peuvent pas être mis en oeuvre sur un ordinateur), cela suggère que les programmes d'ordinateurs peuvent être des inventions. Cela a été introduit par l'OEB en 2000 dans la méconnue %(e6:Annexe 6 du document du Site Web Trilatéral) afin d'harmoniser la loi sur les brevets européenne avec celles des États-Unis et du Japon et pour autoriser les brevets sur les %(q:procédés mis en oeuvre par ordinateur), en anticipation de la suppression de l'article 52(2) de la Convention sur le Brevet Européen.

#Am116: La présente directive établit les règles concernant les limites de la brevetabilité et des possibilités de mise en oeuvre des brevets en ce qui concerne les programmes d'ordinateurs.

#Art2a: %(q:invention mise en oeuvre par ordinateur) désigne toute invention dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'autre appareil programmable et présentant une ou plusieurs caractéristiques à première vue nouvelles qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs ;

#apt: L'article 52 de la CBE statue clairement qu'un programme d'ordinateur seul (ou un %(q:programme d'ordinateur en tant que tel)) ne peut pas constituer une invention brevetable.

#fet: Les amendements 36, 117 et 42 précisent qu'une innovation et brevetable seulement si elle se conforme à l'article 52 de la CBE, sans se soucier qu'un programme d'ordinateur fasse ou non partie de sa mise en oeuvre. Dans le cas de l'amendement 14, ce qui suit se conforme à la définition donnée: %(q:une invention dont l'exécution implique l'utilisation d'un ordinateur et dont tout ou partie de ses caractéristiques est réalisée au moyen d'un programme d'ordinateur). Ainsi, cela implique que les programmes d'ordinateurs peuvent être des inventions, même si cela contredit l'article 52 de la CBE.

#Am36: %(q:invention mise en oeuvre par ordinateur) désigne toute invention au sens de la Convention sur le brevet européen dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'un autre appareil programmable et présentant dans sa mise en oeuvre une ou plusieurs caractéristiques non techniques qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs, en plus des caractéristiques techniques que toute invention doit posséder;

#Am14: %(q:invention mise en oeuvre par ordinateur) désigne toute invention dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'autre appareil programmable et présentant une ou plusieurs caractéristiques qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs;

#Art2b: %(q:contribution technique) désigne une contribution à l'état de la technique dans un domaine technique, qui n'est pas évidente pour une personne du métier.

#Art2bSyn1: Une directive qui ferait dépendre la brevetabilité du terme %(q:contribution technique) doit expliquer ce terme clairement.

#Art2bSyn96: L'amendement 96 semble impliquer que seul le problème et non la solution nécessite d'être technique. Le mot %(q:notablement) n'a pas de signification légale et donc conduit à la confusion plutôt qu'à la clarification. Évidemment %(q:technique) nécessite d'être clairement défini si cela doit avoir un effet utile. Requérir qu'un problème technique soit résolu est aussi mauvais que la proposition de la CEC, voire pire.  Nous ne devons pas nous focaliser sur l'application (le problème résolu), mais sur la solution (le résultat d'une recherche empirique qui peut mériter un brevet).  Tout logiciel peut prétendre résoudre un problème technique, mais si la solution est seulement innovante dans le logiciel, elle ne peut pas prétendre être une solution technique (tout au moins pas une nouvelle solution technique). Si vous prenez des moyens physiques connus et que vous les utilisez en tant qu'équipements périphériques d'un ordinateur, vous pouvez résoudre toute sorte de problèmes techniques par programmation. Cela ne qualifiera pas ces solutions en inventions quoi qu'il en soit, ni tout enseignement qu'elle apporte à la contribution technique (les deux dernières phrases sont synonymes, invention = contribution technique). Inversement, une invention technique et brevetable peut parfaitement résoudre des problèmes non techniques (par exemple une puce d'ordinateur ne peut qu'éxécuter des calculs simples très rapidement, ce qui n'est pas un problème technique), aussi, cet amendement peut même exclure des inventions brevetables autrement, en fonction de l'interprétation ou de la définition  de technique.

#Art2bSyn69: L'amendement 69 va dans le bon sens en déclarant que %(q:le traitement, la manipulation et la présentation d'informations n'appartiennent pas à un domaine technique).  Il risque de mélanger la condition de %(q:contribution technique) (= invention) aux conditions de non évidence, similairement à l'amendement 96.  Cependant même si les formulations sont redondantes, il n'y a aucun mal à dire que la contribution (invention) doit être %(q:non évidente).  Ceci peut même aider à corriger les mauvaise interprétations de l'OEB,  d'après qui%(q:la non évidence doit contenir une contribution technique).  Étant donné que cet amendement définit également ce que sont des %(q:domaines techniques) (article 27 des ADPIC), il est extrêmement utile.

#Art2bSyn107: Enfin, l'amendement 107 a tout juste : il statue clairement que la %(q:contribution technique) est un synonyme d'invention et répète les conditions de brevetabilité de l'article 52 de la CBE. Cet amendement ne limite la brevetabilité en aucune façon, il confirme simplement l'article 52 de la CBE et supprime toute confusion qui peut survenir en mêlant les tests de brevetabilité, comme le fait le texte original de la CEC et les autres amendements.

#Am69: %(q:contribution technique) désigne une contribution à l'état de la technique dans un domaine technique, qui n'est pas évidente pour une personne du métier. L'utilisation des forces de la nature afin de contrôler des effets physiques au delà de la représentation numérique des informations appartient à un domaine technique. Le traitement, la manipulation et les présentations d'informations n'appartiennent pas à un domaine technique, même si des appareils techniques sont utilisés pour les effectuer.

#Am107: %(q:contribution technique), également appelée %(q:invention), désigne une contribution à l'état de la technique dans un domaine technique. Le caractère technique de la contribution est une des quatre conditions de la brevetabilité. En outre, pour mériter un brevet, la contribution technique doit être nouvelle, non évidente et susceptible d'application industrielle.

#Am96: %(q:contribution technique) désigne une contribution, impliquant une activité inventive, à un domaine technique, qui résout un problème technique existant ou étend notablement l'état de la technique pour une personne du métier.

#Art2baSyn: Puisque la tentative du rapporteur de %(q:rendre clair ce qui est brevetable de ce qu'il ne l'est pas) repose entièrement sur le mot %(q:technique), ce terme doit être défini clairement et de façon restrictive. La seule définition qui remplisse ces conditions est celle des lignes des amendements 55 et 97 (l'amendement 37, qui est bon aussi, sera retiré pour éviter un conflit avec celui-ci), qui est trouvé dans des décisions de justice de différents pays et dans quelques lois sur les brevets (par exemple le Traité sur les Brevets Nordique et les lois sur les brevets de la Pologne, du Japon, de Taïwan et autres). Cette définition est fondée sur des concepts valides de science et d'épistémologie et a prouvé avoir une signification claire dans la pratique. Elle assure que les droits d'exclusion, étendus, chers et plein d'insécurité tels que les brevets, sont utilisés dans des domaines où il y a un raisonnement économique pour eux, et que l'innovation abstraite-logique est du domaine des droits de copyright ou de droits du même genre seulement.  Le mot %(q:numérique) dans l'amendement 55 est apparemment le résultat d'une mauvaise traduction à partir du français. La définition brute donnée ici correspond à l'acception commune du terme de par le monde, implicite dans le cas juridique en cours et dans les autres déclarations du rapport de la JURI sur ce qui doit être brevetable (par exemple %(q:pas le logiciel en tant que tel, mais les inventions relatives aux téléphones mobiles, aux appareils de contrôle de moteur, aux appareils domotiques, ...) ).  Cette acception doit être rendue explicite à des fins de clarification.

#Am97: %(q:domaine technique) désigne un domaine industriel d'application nécessitant l'utilisation de forces contrôlables de la nature pour obtenir des résultats prévisibles. %(q:Technique) signifie %(q:appartenant à un domaine technique). L'utilisation de forces de la nature pour contrôler des effets physiques au delà de la représentation numérique de l'information appartient à un domaine technique. La production, la manipulation, le traitement, la distribution et la présentation de l'information n'appartiennent pas à un domaine technique, même si des dispositifs techniques sont utilisés dans ce but.

#Am37: %(q:Technologie) signifie %(q:science naturelle appliquée).  %(q:Technique) signifie %(q:concret et physique).

#Am39Syn: Ceci est la doctrine de brevets standard dans la plupart des juridictions.  Les %(q:quatre forces de la nature) sont un concept admis de l'épistémologie (théorie de la science). Alors que les mathématiques sont abstraites et ne relèvent pas des lois de la nature, quelques méthodes destinées à l'exercice d'activités économiques peuvent très bien dépendre de la chimie des cellules du cerveau du client, qui est quoi qu'il en soit non contrôlable, c'est-à-dire non déterministe, sujet à une volonté propre. En conséquence, les termes %(q:forces contrôlables de la nature) excluent clairement ce qui doit être exclu et cependant fournissent suffisamment de flexibilité pour y inclure les futurs domaines possibles de la science naturelle appliquée au-delà des %(q:quatres forces de la nature) actuellement admises. Notez que cet amendement ne rendra pas les inventions dans les appareils comme les téléphones mobiles ou les machines à laver non brevetables, il assure seulement qu'en ajoutant simplement un nouveau programme d'ordinateur à un appareil (technique) connu cela ne rendra pas le programme d'ordinateur brevetable. Inversement, ajouter un programme d'ordinateur à une invention brevetable autrement, ne rendra pas cette invention non brevetable (car l'invention résoudra toujours le problème en utilisant une des forces contrôlables de la nature, sans se soucier de la présence d'un programme d'ordinateur).

#Am39: %(q:invention), au sens du droit des brevets, signifie %(q:solution d'un problème par l'utilisation de forces contrôlables de la nature).

#Am38Syn: Nous ne voulons pas d'innovations dans l'%(q:industrie musicale) ou l'%(q:industrie des services juridiques) pour être en accord avec la condition d'%(q:application industrielle) des ADPIC. Le mot %(q:industrie) est de nos jours souvent utilisé dans un sens étendu qui n'est pas approprié dans le contexte de la loi sur les brevets.

#Am38: %(q:industrie) au sens du droit des brevets signifie %(q:production automatisée de biens matériels);

#Art3: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit considérée comme appartenant à un domaine technique.

#Art3Syn: Le texte de la Commission Européenne dit que toutes les idées %(q:mises en oeuvre par ordinateur) sont des inventions brevetables selon l'article 27 des ADPIC.  Son rejet serait une bonne chose, une clarification de la manière dont s'applique l'article 27 des ADPI serait encore mieux.

#Art3aSyn1: Un des objectifs majeurs de ce projet de directive a été de clarifier l'interprétation de l'article 52 de la CBE à la lumière de l'article 27 des ADPIC, qui dit que les %(q:inventions de tous les domaines de technologie) doivent être brevetables. Nous proposons de traduire l'article 52(2) de la CBE dans le langage de l'article 27 des ADPIC, dans l'amendement CULT-16.

#Art3aSyn2: Si le traitement de données est %(q:technique), alors tout est %(q:technique).

#Art3aSyn3: Le traitement de données est la base commune à tous les domaines technologiques et non technologiques. Avec l'avènement de l'ordinateur (universel) dans les années 1950, le traitement de données automatisé s'est répandu dans la société et l'industrie.

#Art3aSyn4: Comme Gert Kolle, le théoricien à l'origine des décisions des années 1970 d'exclure le logiciel de la brevetabilité, l'écrit en 1977 (voir Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: Le traitement de données automatisé est devenu aujourd'hui l'outil auxilliaire indispensable dans tous les secteurs de la société et le restera dans le futur. Il est omniprésent. ... Son côté contributif et sa fonction d'auxilliaire distingue le traitement de données automatisé des ... domaines individuels de technologie et l'assimile à des domaines tels que l'administration d'entreprise, dont les procédés et résultats ... sont nécessaires dans toutes les entreprises, et par conséquent, à première vue, le besoin d'assurer sa libre disponibilité est indiqué.

#Art3aSynCult: CULT a été bien avisé de voter pour clarifier l'exclusion du traitement de données du domaine de la %(q:technologie).

#Art3aSynStud: De nombreuses études, quelques unes d'entre elles conduites par les institutions de l'Union Européenne, aussi bien que les avis du Comité Economique et Social Européen et le Comité des Régions Européen, expliquent en détails pourquoi l'économie de l'Europe souffrirait, si les innovations dans le traitement de données n'étaient pas clairement exclues de la brevetabilité.

#Am45: Les États membres veillent à ce que le traitement des données ne soit pas considéré comme un domaine technique au sens du droit des brevets et à ce que les innovations en matière de traitement des données ne constituent pas des inventions au sens du droit des brevets.

#Art4N1: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit brevetable à la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activité inventive.

#Art4N2: Les États membres veillent à ce que pour impliquer une activité inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique.

#Art4N3: La contribution technique est évaluée en prenant en considération la différence entre l'objet de la revendication de brevet considéré dans son ensemble, dont les éléments peuvent comprendre des caractéristiques techniques et non techniques, et l'état de la technique.

#Art4Syn: Cet article mélange le test de non évidence avec le test d'inventivité, affaiblissant ainsi les deux tests et rentrant en conflit avec l'article 52 de la CEB.

#cpW: La JURI a décomposé cet article en 3 parties.  Ceci est dangereux car certains amendements, pouvant s'appliquer à l'article en entier, sont ainsi empêchés d'être mis au vote.

#iaa: Divisé en 3 parties.

#Art4N1Syn: %(q:Les inventions mises en oeuvre par ordinateur) (c'est-à-dire les idées agencées en termes d'équipement de traitement de données à usage général = programmes pour ordinateurs) ne sont pas des inventions selon l'article 52 de la CBE. Voir aussi l'analyse pour l'article 1. Les amendements 56=98=109 corrigent le bogue et réaffirment l'article 52 de la CBE.  Aucun des amendements ne limite la brevetabilité, mais cet amendement, au moins, obtient des choses formellement correctes.

#Am16N1: Pour être brevetable, une invention mise en oeuvre par ordinateur doit être susceptible d'application industrielle, être nouvelle et impliquer une activité inventive. Pour impliquer une activité inventive, une invention mise en oeuvre par ordinateur doit apporter une contribution technique.

#Am98: Les États membres veillent à ce que les brevets ne soient délivrés qu'à des inventions techniques qui sont nouvelles, non évidentes et susceptibles d'application industrielle.

#Art41aSyn: La doctrine de la Cour fédérale des brevets allemande, dans sa décision de recherche d'erreur de 2002, nie une doctrine de l'OEB qui rend brevetables la plupart des méthodes informatisées destinées à l'exercice d'activités économiques.

#Am47: Les États membres veillent à ce que les solutions, mises en oeuvre par ordinateur, à des problèmes techniques ne soient pas considérées comme des inventions brevetables au seul motif qu'elles améliorent l'efficacité de l'utilisation des ressources dans le système de traitement des données.

#Art41bSyn: Une vraie réaffirmation de l'article 52 de la CBE. Comme le but de cette directive est d'harmoniser et de clarifier, il est d'une importance primordiale d'insister sur l'interprétation correcte de la CBE, car c'est le fondement du droit sur les brevets en Europe.

#Am48: Les États membres veillent à ce qu'il soit précisé qu'une innovation ne peut constituer une invention au sens du droit des brevets que si elle présente un caractère technique, indépendamment de la question de savoir si elle implique ou non l'utilisation d'un ordinateur.

#Art4N2Syn: Le texte de la Commission dévie de l'article 52 de la CBE. Comme il est expliqué dans l'analyse du considérant 11, on doit d'abord déterminer s'il y a une invention/contribution technique (en utilisant soit la définition négative de l'article 52 de la CBE : un programme d'ordinateur n'est pas une invention, ou en utilisant la définition positive qui ressort de la jurisprudence, fondé sur les forces contrôlables de la nature). Ensuite, on doit vérifier si l'invention passe les trois autres tests de l'article 52, dont l'un est l'activité inventive (= non évidence).  Le texte de la Commission affirme que le test de l'activité inventive est une condition pour que quelque chose soit une invention. Puisqu'un programme d'ordinateur peut être non évident, il pourrait être une invention selon cette définition (contrairement à ce que dit l'article 52 de la CBE).

#Art4N2SynP2: De plus, le texte de la Commission implique que des idées formulées en termes d'ordinateur générique (programmes pour ordinateurs) sont des inventions brevetables. La suppression, comme le propose l'amendement 82, pourrait être favorable, parce que le seul effet de ce paragraphe est de rendre confus les tests de brevetabilité, et par là, de mettre les offices de brevets nationaux dans l'impossibilité de rejeter des revendications non statutaires sans examen substantiel. L'amendement 16 fait la même erreur que le texte de la Commission, il ne fait mettre un peu d'ordre dans le langage utilisé. L'amendement 40 sera retiré, il a maintenant été redéposé en tant qu'insertion (83).

#Am16N2: Les États membres veillent à ce que le fait qu'une invention mise en oeuvre par ordinateur qui apporte une contribution technique constitue une condition nécessaire à l'existence d'une activité inventive.

#Art4N3Syn1: Quand vous brevetez une invention, vous revendiquez une application particulière de cette invention. Cette application est ce qui est décrit dans les revendications. Comme telles, les revendications (comme un tout) contiennent à la fois l'invention elle-même d'une certaine façon, et un nombre de caractéristiques non brevetables (comme par exemple un programme d'ordinateur) qui sont requis pour ladite application de l'invention. Cela signifie que même si vous exigez que l'invention soit technique, les revendications peuvent contenir des caractéristiques non techniques (ce à quoi nous ne nous opposons pas du tout, c'est la façon dont les brevets ont toujours fonctionné).

#Art4N3Syn2: Quoiqu'il en soit, la proposition de la Commission Européenne vide de son sens son propre concept de %(q:contribution technique) en autorisant la contribution à n'être composée que de caractéristiques non techniques. La version de la JURI affirme seulement que les revendications doivent contenir des caractéristiques techniques, mais ce n'est pas une vraie limite. Un exemple peut clarifier ceci. Supposez que nous voulions breveter un programme d'ordianteur, alors nous disons que c'est une %(q:contribution technique). Dans les revendications, nous décrivons l'application de son programme, aussi nous disons que nous brevetons l'exécution de ce programme sur ordinateur. Maintenant, les revendications dans leur ensemble contiennent des caractéristiques techniques (l'ordinateur), et la différence entre les revendications dans leur ensemble (soit l'ordinateur+le nouveau programme) et l'état de la technique (soit l'ordinateur) est un programme d'ordinateur. Alors, un programme d'ordinateur peut être une contribution technique, selon cette condition, ce qui est complètement contradictoire (car un programme d'ordinateur ne peut pas être technique). Il est clair qu'une correction comme celle-là dans l'amendement 57=99=110 est nécessaire, si le concept de %(q:contribution technique) ne doit pas être utilisé du tout.

#lWo: La période de grâce de nouveauté proposée par l'amendement 100 est une question orthogonale.  Comme il est montré dans une %(ng:récente consultation conduite par l'Office des Brevets du Royaume-Uni), elle est très controversée même parmi ceux qui sont supposés en bénéficier.  Il n'est pas sûr qu'une période de grâce de nouveauté, serait bénéfique pour les PME, comme le suggère le rapport de l'ITRE.  Dans le cas du développement open source, il pourrait même causer une insécurité supplémentaire comme de savoir si les idées publiées sont libres de droits.

#Am16N3: La contribution technique est évaluée en prenant en considération l'état de la technique et l'objet de la revendication de brevet considéré dans son ensemble, qui doit comprendre des caractéristiques techniques, accompagnées ou non de caractéristiques non techniques.

#Am99: La contribution technique est évaluée en prenant en considération la différence entre l'ensemble des caractéristiques techniques de la revendication de brevet et l'état de la technique.

#Am100P1: Le caractère notable de la contribution technique est évalué en prenant en considération la différence entre les éléments techniques inclus dans l'objet de la revendication de brevet considéré dans son ensemble et l'état de la technique. Les éléments révélés par le demandeur d'un brevet pendant une période de six mois précédant la date du dépôt de la demande ne sont pas considérés comme faisant partie de l'état de la technique, pour l'évaluation de cette revendication particulière.

#Art43aSyn: Comme mentionné dans l'analyse de l'article 1, %(q:invention mise en oeuvre par ordinateur) est un terme contradictoire. Comme mentionné dans l'analyse du considérant 11, %(q:invention) et %(q:contribution technique) sont synonymes: ce que vous avez inventé est votre contribution (technique) à l'état de la technique.  Dans le jargon utilisée par la Directive, quoi qu'il en soit, %(q:contribution technique) ne signifie que %(q:solution à un problème technique par rapport à l'état antérieur de la technique et l'invention revendiquée).  Dans ce contexte, il n'est pas clair de voir quel effet un test, aussi strict soit-il, pourrait avoir, car il n'est pas clair de savoir à quoi il s'applique.

#Art43aSynTest: Aussi, en disant que ce test %(q:s'appliquera), la disposition ne requiert pas clairement que ce test doit effectivement %(e:être vérifié), ni ce que l'on entend par %(q:contribution technique).

#Art43aSynInd: Finalement, il n'y a pas de définition légale d'%(q:application industrielle au sens strict, en terme de méthode et de résultat).

#Art43aSynVal: En dépit de ces déficiences, nous recommandons d'appuyer l'amendement 70. Au moins, il codifie des aspects centraux du concept d'%(q:invention technique). Donc, en combinaison avec d'autres amendements, il pourrait finelement contribuer à tracer une limite claire de la brevetabilité.

#Am70: Pour déterminer si une invention mise en oeuvre par ordinateur apporte une contribution technique, il y a lieu d'établir si elle apporte une connaissance nouvelle sur les relations de causalité en ce qui concerne l'utilisation des forces contrôlables de la nature et si elle a une application industrielle au sens strict de l'expression, tant sous l'angle de la méthode que sous celui du résultat.

#Art4aSyn: L'amendement 17 présume que %(q:l'interaction physique normale entre un programme et l'ordinateur) signifie quelque chose. Ce n'est pas le cas.  On aurait pu trouver un sens, par exemple en spécifiant, à l'instar de la Cour fédérale des brevets allemande dans une récente %(es:décision sur la %(tp|Recherche d'erreurs|Suche Ferhlerhafter Zeichenketten)), que l'%(q:augmentation de l'efficacité de calcul), %(q:les économies dans l'utilisation de la mémoire), etc. %(q:ne constituent pas une contribution technique), ni ne %(q:sont dans la portée de l'interaction physique normale entre un programme et un ordinateur).  En l'absence d'une telle spécification, l'amendement 17 ne fait qu'obscurcir au lieu de clarifier.

#Am17: %(al|Exclusions de la brevetabilité|Une invention mise en oeuvre par ordinateur n'est pas considérée comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, d'un réseau ou d'un autre appareil programmable. En conséquence, ne sont pas brevetables les inventions impliquant des programmes d'ordinateurs, qui mettent en oeuvre des méthodes commerciales, des méthodes mathématiques ou d'autres méthodes, si ces inventions ne produisent pas d'effets techniques en dehors des interactions physiques normales entre un programme et l'ordinateur, le réseau ou un autre appareil programmable sur lequel il est exécuté.)

#Art4bSyn: Mêmes commentaires que pour l'amendement ci-dessus.

#Am87: Exclusions de la brevetabilité Une invention mise en oeuvre par ordinateur n'est pas considérée comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, d'un réseau ou d'un autre appareil programmable. En conséquence, ne sont pas brevetables les inventions impliquant uniquement des programmes d'ordinateurs (traitement des données), qui mettent en oeuvre des méthodes destinées à l'exercice d'activités économiques, des méthodes mathématiques ou d'autres méthodes, si ces inventions ne produisent pas d'effets techniques en dehors des interactions physiques normales entre un programme et l'ordinateur, le réseau ou un autre appareil programmable sur lequel il est exécuté.

#Art4cSyn1: La directive proposée par la CEC et la JURI, basée sur la doctrine du%(q:Système de pension bénéficiaire) de l'OEB et sur le Standard trilatéral de 200 des offices de brevets US, japonais et européen, considère que tous les programmes d'ordinateur sont des inventions brevetables.  Ceci est manifeste dans le terme %(q:invention mise en oeuvre par ordinateur).  Il n'existe plus de test pour savoir s'il existe ou non un caractère inventif, mais seulement un test sur la %(q:contribution technique dans l'activité inventive), c'est-à-dire que l'activité entre %(q:l'état de l'art le plus proche) et %(q:l'invention) doit être circonscrite en terme de %(q:problème technique), part explemple la question de l'amélioration la vitesse ou l'efficacité dans l'utilisation de la mémoire d'un ordinateur.  Ainsi tout algorithme ou toute méthode destinée à l'exercice d'activitées économiques devient effectivement brevetable, comme cela a été souligné par la Cour fédérale des brevets allemande, qui a refusé de suivre cette doctrine de l'OEB dans une %(es:décision récente).

#Art4cSyn2: Les amendements 47 et 60 disent clairement à l'OEB de suivre l'approche de la Cour fédérale des brevets allemande en refusant de considérer que l'amélioration de l'efficacité du traitement de donnée soit soint une %(q:contribution technique).

#Am60: Les États membres veillent à ce que les solutions, mises en oeuvre par ordinateur, à des problèmes techniques ne soient pas considérées comme des inventions brevetables au seul motif qu'elles améliorent l'efficacité de l'utilisation des ressources dans le système de traitement des données.

#Art4dSyn1: Cet amendement pourrait à lui seul lever la plupart des objections à la directive. Les lignes directrices de 1978 fournissent une interprétation claire de l'article 52. Ceci a été modifié %(q:en réponse aux demandes de l'industrie) dans les versions suivantes des lignes directrices, rendant les règles contradictoires et introduisant d'un schisme. Les objectifs de cette directive devraient être de résoudre ce schisme et de concrétiser l'application du traité des ADPIC de 1994. Le premier objectif pourrait être atteint par cet amendement à lui seul. Il faut remarquer que cet amendement ne dit pas que les lignes directrices de l'OEB de 1978 doivent devenir la loi. Il dit plutôt qu'elles donnent une interprétation appropriée de l'article 52 de la CBE.

#Am46: Les États membres veillent à ce que les brevets relatifs à des innovations informatisées ne soient reconnus et protégés que s'ils ont été accordés conformément aux règles de l'article 52 de la Convention sur le brevet européen de 1973, ainsi qu'il est expliqué dans les lignes directrices en matière d'examen de l'Office européen des brevets de 1978.

#Art5: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur puisse être revendiquée en tant que produit, c'est-à-dire en tant qu'ordinateur programmé, réseau informatique programmé ou autre appareil programmé ou en tant que procédé, réalisé par un tel ordinateur, réseau d'ordinateur ou autre appareil à travers l'exécution d'un programme.

#Art5Syn18: L'amendement 18 propose d'introduire une revendication pour un programme, c-à-d une revendication sous la forme d'%(bc:un programme, caractérisé par le fait qu'au chargement dans la mémoire d'un ordinateur [ un processus quelconque] est exécuté.) Cela rendrait toute publication de logiciel susceptible d'infraction potentielle de brevet et mettrait de ce fait en danger les programmeurs, chercheurs et distributeurs de services Internet. Sans la possibilité des revendications de logiciel, les distributeurs ne seraient qu'indirectement légalement responsables, c-à-d à travers les garanties qu'ils donnent à leur clients. En l'état, la question de la revendiction de programme semble être plus symbolique que substantielle: le lobby des brevets veut clairement exposer qu'un logiciel est brevetable en tant que tel. La CCE s'est refusée à cette dernère conséquence.

#Art5Syn58: Cependant, la formulation de la CCE suggère toujours que du matériel informatique commun et les calculs exécutés sur celui-ci (= les programmes d'ordinateurs) sont des produits et processus brevetables. L'amendement 101/58 essaie de pallier à ce problème. Ce pourrait être une bonne idée de remplacer %(q:procédé de production technique exploité par un tel ordinateur) par %(q:les processus innovateurs s'exécutant sur cet équipement) si c'est encore possible, sous la forme d'un %(q:amendement de compromis). L'amendement 102 définit %(q:invention mise en oeuvre sur ordinateur) comme étant uniquement un produit ou un processus de production technique. Du moment que %(q:technique) est défini, cela pourrait être utile. Néanmoins, un ordinateur exécutant un programme pourrait aussi être interprété comme un %(q:appareil programmé), et breveter l'utilisation d'un logiciel lorsqu'il est exécuté sur un ordinateur revient au même que breveter le logiciel lui-même: il n'y a pas moyen d'utiliser un logiciel autrement qu'en l'exécutant sur un ordinateur.

#Am18: Une revendication portant sur un programme d'ordinateur, en tant que tel, enregistré sur un support ou livré par un signal, n'est autorisée que si ce programme, une fois chargé ou exécuté sur un ordinateur, un réseau informatique ou un autre appareil programmable, assure la mise en oeuvre d'un produit ou réalise un procédé brevetable en vertu des articles 4 et 4 bis.

#Am101: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur ne puisse être revendiquée qu'en tant que produit, c'est-à-dire un ensemble d'équipements comprenant à la fois des appareils et des dispositifs programmables qui utilisent les forces de la nature d'une façon inventive, ou en tant que procédé de production technique exploité par un tel ordinateur, réseau d'ordinateur ou autre appareil à travers l'exécution d'un programme.

#Am102: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur ne puisse être revendiquée qu'en tant que produit, c'est-à-dire en tant qu'appareil programmé, ou en tant que procédé technique de production.

#Art5aSyn: Le logiciel libre apporte d'importantes contributions à l'innovation et à la diffusion du savoir sans bénéficier du système des brevets. Il faudrait en tenir compte dans la politique d'innovation et dans la loi, sans pour cela en conclure que les logiciels propriétaires devraient être brevetables. Malheureusement, cet amendement fait usage de l'expression %(q:inventions mise en oeuvre sur ordinateur), qui a été introduites par la CBE en 2000 dans une tentative de faire en sorte que les opérations génériques de traitement de données (logiciel) soient considérées comme brevetables. Une seconde lecture devrait corriger cela.

#Am62: %(al|Limitation des effets des brevets octroyés aux inventions mises en oeuvre par ordinateur|%(linol|Les États membres veillent à ce que les droits conférés par le brevet ne s'étendent pas aux activités effectuées pour exécuter, copier, distribuer, étudier, modifier ou améliorer un programme d'ordinateur distribué sous le couvert d'une licence prévoyant:|la liberté d'exécuter le programme;| la liberté d'étudier comment le programme fonctionne et de l'adapter aux besoins de l'utilisateur;|la liberté de redistribuer des copies aux mêmes conditions que celles prévues par la licence;|la liberté d'améliorer le programme et de diffuser les améliorations dans le public aux mêmes conditions que celles prévues par la licence;|le libre accès au code-source du programme.))

#Art51a: Cet amendement tente apparemment d'exclure les revendications sur des moyens d'opérer du traitement de données (= programmes d'ordinateur), mais propose des moyens de régulation inapproprié.  Les revendications de brevet comportent toujours plus que l'invention (= contribution technique).  S'il existe une invention technique (ex. un nouveau procédé chimique), alors il n'y pas de raison pour que la revendication de brevet ne comporte pas cette invention, accompagnée d'élements non inventifs, comme un programme d'ordinateur contrôlant la réaction chimique. Une revendication ne décrit pas une invention mais le cadre du droit d'exclusion qui est légitimé par une invention.

#Am72: Les États membres veillent à ce que les revendications de brevet reconnues sur des inventions mises en oeuvre par ordinateur couvrent uniquement la contribution technique qui fonde une revendication. Une revendication de brevet sur un programme d'ordinateur, que ce soit sur le seul programme ou sur un programme enregistré sur un support de données, est irrecevable.

#Am103Syn: Sans cet amendement, les détenteurs de brevets logiciels pourront toujours envoyer des lettres d'intimidation aux programmeurs ou distributeurs de logiciels, les accusant d'infraction contributive. Cet article assure que le droit de publication, garanti par l'Art 10 de la Convention Européenne sur les Droits Humains (CEDR), prend le pas sur les brevets. D'un autre côté, cet amendement n'empêche pas la détention de brevets logiciels ou leur application à l'encontre des utilisateurs de logiciels. Les vendeurs de logiciels propriétaires et les distributeurs de Linux commerciaux pourront toujours être mis sous pression par leurs clients, qui leur imposeront généralement une obligation contractuelle de protection contre les brevets.

#Am103: Les États membres veillent à ce que la production, la manipulation, le traitement, la distribution et la publication de l'information, sous quelque forme que ce soit, ne puisse jamais constituer une contrefaçon de brevet, directe ou indirecte, même lorsqu'un dispositif technique est utilisé dans ce but.

#Am104N1Syn: L'étendue d'application du brevet est normalement définie dans la revendication, et si quelque chose sort du domaine d'application, elle ne constitue pas une infraction. L'amendement semble donc, à première vue, tautologique. L'intention de cet amendement est peut-être de permettre la simulation de processus brevetables sur un système de traitement de données standard. Dans ce cas, il faudrait le dire clairement, comme dans certains autres amendements

#Am104N1: Les États membres veillent à ce que l'utilisation d'un programme d'ordinateur à des fins qui ne relèvent pas de l'objet du brevet ne puisse constituer une contrefaçon de brevet, directe ou indirecte.

#Art5dSyn: Ces amendements, contrairement à ce que l'on pourrait penser, ne servent pas à promouvoir la protection de du logiciel libre ou open source, mais plutôt pour s'assurer que l'obligation de divulgation qui est inhérente au système de brevet soit prise au sérieux et que le logiciel est, comme tout autre objet d'information, dans la partie divulgation du brevet plutôt que dans sa partie exclusion/monopole. Ceci rend les choses plus difficiles pour empêcher les gens de faire ce que vous n'avez même pas fait, mais qui est évidemment possible puisque le modèle de calcul est parfaitement défini et que vous savez toujours ce que vous pouvez faire avec un ordinateur. Quand on publie le code source du travail, on offre au moins une réelle connaissance sur la façon de résoudre le problème, à la différence de %(q:les ressources processeur couplées aux ressources d'entrée-sorties de sorte qu'il réalise une fonction telle que le résultat de ladite fonction au moyen des entrées-sorties résolvent le problème que voulait résoudre l'utilisateur).

#Am104N2: Les États membres veillent à ce que, lorsqu'une revendication de brevet mentionne des caractéristiques impliquant l'utilisation d'un programme d'ordinateur, une mise en oeuvre de référence, opérationnelle et bien documentée, de ce programme soit publiée en tant que partie de la description, sans conditions de licence restrictives.

#Art6: Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particulières relatives à la décompilation et à l'interopérabilité ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affectés par la protection octroyée par les brevets d'invention dans le cadre de la présente directive.

#Art6Syn: La proposition de la Commission Européenne ne protège pas l'interopérabilité mais, au contraire, assure qu'un logiciel interopérable ne peut pas être publié quand son interface est brevetée. Le seul comportement que la Commission Européenne veut permettre dans ce cas est la décompilation, qui ne constituerait pas une contrefaçon de brevet de toute façon (car les brevets impliquent l'usage de ce qui est décrit dans les revendications, et non l'étude dudit objet). Les amendements 66 et 67, de plus, spécifient les lois sur lesquelles les privilèges d'interopérabilité peuvent être fondés, les rendant par conséquent encore plus sûrs, de sorte que la réserve ne peut plus avoir d'effets.

#Am19: Les droits conférés par les brevets d'invention délivrés dans le cadre de la présente directive ne portent pas atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particulières relatives à la décompilation et à l'interopérabilité.

#Art6aSyn: Les amendements 20 et 50 sont les seuls à formuler un réel privilège d'interopérabilité.  La formulation %(q:systèmes d'ordinateur et reseaux) devrait être, pour raisons de clarté, corrigé en %(q:systèmes de traitement de données) si possible. L'amendement 50 de l'UEN le fait.  L'amendement 20 était déjà accepté par le CULT, l'ITRE et la JURI.

#Am76Syn: L'amendement 76 supprime toute clarté quant à savoir quand l'utilisation de techniques brevetées peuvent être utilisées ou non dans un but d'interopérabilité.  Il va à l'encontre du propos principal de la directive, qui est de concrétiser certaines règles annexes abstraites, y compris celles de l'article 30 TRIP, et ainsi arriver à la clarté et à une sécurité juridique.

#Am105Syn: Il est plaisant de voir que l'amendement 105 est d'accord avec nous sur le fait qu'%(q:une invention mise en oeuvre par ordinateur) ne peut signifier autre chose qu'un logiciel exécuté sur un ordinateur. Quoi qu'il en soit, les exceptions qu'il mentionne sont très étranges. Dans le premier cas, si l'invention est une machine indépendante, il n'y a pas de condition d'interopérabilité. S'il n'y a pas de condition d'interopérabilité, l'amendement 20 ne s'appliquera pas non plus. Inversement, si une telle condition existe, alors l'invention brevetée ne fonctionnera pas en tant que machine indépendante ou invention technique. La seconde clause est vraiment dangereuse, car elle laisse tout au soin des décisions de tribunaux, et donc échoue à satisfaire le but de clarté de la directive. Comme le montre les affaires anti-trust contre Microsoft aux États-Unis et en Europe, il est extrêmement long et difficile de déterminer si un comportement enfreint le droit existant sur la concurrence.  Cela rendrait la clause d'interopérabilité inefficace et mettrait les PME en situation encore plus désavantageuse, car les grandes sociétés ont beaucoup plus d'argent à dépenser en justice.

#Am50: Les États membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire à la seule fin d'assurer la conversion des conventions utilisées dans deux systèmes ou réseaux informatiques différents, de façon à permettre entre eux la communication et l'échange de données, ce recours ne soit pas considéré comme une contrefaçon de brevet.

#Am20: Utilisation de techniques brevetées Les États membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire à la seule fin d'assurer la conversion des conventions utilisées dans deux systèmes ou réseaux informatiques différents, de façon à permettre entre eux la communication et l'échange de données, ce recours ne soit pas considéré comme une contrefaçon de brevet.

#Am76: Les États membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire à une fin significative, par exemple pour assurer la conversion des conventions utilisées dans deux systèmes ou réseaux informatiques différents, de façon à permettre entre eux la communication et l'échange de données, ce recours ne soit pas considéré comme une contrefaçon de brevet, à condition qu'il ne soit pas nettement incompatible avec une exploitation normale du brevet et ne porte pas notablement atteinte aux intérêts légitimes du titulaire du brevet, non sans tenir compte des intérêts légitimes des tiers.

#Am105: %(al|Utilisation des techniques brevetées|Les États membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire à la seule fin d'assurer la conversion des conventions utilisées dans deux systèmes ou réseaux informatiques différents, de façon à permettre entre eux la communication et l'échange de données, ce recours ne soit pas considéré comme une contrefaçon de brevet à condition que::|%(ol|l'invention constituant le logiciel utilisé dans le système informatique ne fonctionne pas en tant que machine ou invention technique indépendante, et |le brevet ne soit pas contraire à la législation européenne relative à la concurrence.))

#Art6b: Cet amendement semble créer un nouveau droit de propriété en dehors du système de brevets.  Il établit une catégorie appelée %(q:inventions mises en oeuvre par ordinateur), qui n'appartiennent à aucun %(q:domaine de la technologie).  C'est une approche intéressante, bien qu'une période de 7 ans soit certainement encore trop longue et que d'autres questions plus approfondies aient besoin d'être clarifiées pour que cela fonctionne.

#Am106: %(al|Durée de validité du brevet|La durée de validité d'un brevet délivré pour des inventions mises en oeuvre par ordinateur conformément à la présente directive sera de sept ans à compter de la date du dépôt de la demande.)

#Artt7: La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes y compris le commerce électronique.

#Art7Syn: Cet article n'a aucun effet régulatoire. Tel que formulé actuellement, il donne plutôt à l'Unité chargée de la propriété industrielle au Marché intérieur de la Commission européenne davantage de fonds et d'opportunités pour produire des %(q:études) de propagande qui %(le:ne bénéficie d'aucune reconnaissance de la part de la communauté scientifique).  Bien que les efforts pour sousmettre et commenter les amendements sur cet article soient presque vains, cela ne fait aucun doûte que le thème proposé dans l'amendement 91 par Marianne Thysse est plus digne d'être étudié que ce qui a été proposé jusqu'ici par la CEC et la JURI.

#Am21: La Commission surveille l'incidence de la protection par brevet des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes, en particulier les petites et moyennes entreprises, et le commerce électronique.

#Am91: La Commission surveille l'incidence de la protection des brevets des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes. Une attention particulière sera accordée à la position des petites et moyennes entreprises ainsi qu'à celle du commerce électronique et à l'impact des positions dominantes sur le fonctionnement du marché.

#Am71Syn: L'amendement 71 appelle à la création d'un système d'assurance pour l'obtention de brevets et pour les litiges, bien qu'il n'existe pas d'exemple pratique d'un tel système en état de fonctionner. La possibilité théorique, telle que suggérée par un raport de consultants à la Direction générale MARKT, est aussi fondée sur de fausses assomptions, comme l'explique la %(le:lettre ouverte au Parlement européen), rédigée par nombre d'économistes réputés.

#Am90Syn: L'amendement 90 est comme le 71, mais moins explicite.

#Art7PI: Les deux amendements 71 et 90 demandent une asssurance en faveur non de la défense des entreprises de logiciel contre l'agression des brevets mais au contraire, de l'utilisation aggressive des brevets par des PME spécialisées dans les litiges sur les brevets, comme Eolas et Allvoice, contre les entreprises de logiciel.  L'archetype du concept de %(q:Défense de brevet) est la %(q:Patent Defence Union), fondée par le PDG de %(AV).  Allvoice est %(q:l'entreprise de dix personnes située dans une région du sud-est de l'Angleterre marquée par le chômage) dont Arlene McCarthy fait l'éloge dans son projet de rapport pour JURI.  Allvoice a du mal à produire un quelconque logiciel elle-même, certainement pas de logiciel de reconnaissance vocale, mais elle a utilisé deux brevets triviaux et étendus sur les interfaces utilisateur dans le but d'extorquer de l'argent aux véritable entreprises de logiciel de reconnaissance vocale.  En favorisant le schémas de défense de brevet tel que présenté dans cet amendement, les parlementaires européens devraient noter qu'il favoriserait les litiges au lieu de l'innovation.

#weW: Plusieurs études de l'UE sur les PME et les brevets logiciels ont trouvé qu'il existait des raisons systématiques pour lesquelles les PME n'utilisaient pas le système de brevets.  Celles-ci sont malheureusement destinées à être éclipsées par le système de proselytisme, peu importe le montant d'argent publique qui y sera gaspillé.

#Am90: La Commission examine la question des moyens de rendre la protection par les brevets plus lisiblement accessible aux petites et moyennes entreprises et des les assister sur les coûts pour obtenir et fraire respecter les brevets.

#Am71P1: La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes, en particulier les petites et moyennes entreprises et la communauté des logiciels libres, de même que le commerce électronique.

#Am71P2: La Commission examine les manières de faciliter l'accès des petites et moyennes entreprises à la protection offerte par le brevet ainsi que de contribuer aux frais d'obtention et de protection des brevets, notamment par la création d'un fonds de protection et l'introduction de dispositions spéciales relatives aux frais de justice.

#Am71P3: Elle communique les résultats de ses travaux et présente des propositions législatives appropriées, sans délai, au Parlement européen et au Conseil.

#rbr: La Commission soumet au Parlement européen et au Conseil, pour le [DATE (trois ans à compter de la date spécifiée à l'article 9 (1))] au plus tard, un rapport indiquant :

#lrl: Ces exceptions n'ont aucun effet normatif et ont une importance mineure pour la plupart. Les amendements du PPE-DE (par M. Thyssen) apportent quelques améliorations.  Le privilège d'interopérabilité formulé par l'article 6a est une nouvelle approche normative, qui nécessite d'être examinée et raportée, en se conformant également avec la manière dont l'article 30 des ADPIC doit être transposé dans la loi européenne. Ces discussions n'ont pas eu lieu jusqu'ici. Dans cet objectif, l'amendement 93 est mieux formulé que le 89. L'amendement 94 clarifie d'une certaine manière l'objectif de la rédaction d'un rapport, rendant ainsi l'article plus clair.

#cia: l'impact sur la conversion des conventions utilisées dans deux systèmes informatiques différents, de façon à permettre entre eux la communication et l'échange de données;

#Am92: si les règles régissant la durée de validité du brevet et de la détermination des critères de brevetabilité en ce qui concerne plus précisément la nouveauté, l'activité inventive et la portée des revendication sont adéquates : et

#nvy: si l'option décrite dans la directive concernant l'utilisation des inventions brevetées dans le seul objectif d'assurer l'intéropérabilité entre deux systèmes est adéquate.

#neh: Dans ce rapport, la Commission donnera les raisons pour lesquelles elle estime qu'un amendement à la directive en question est nécessaire ou pas et, si nécessaire, indiquera les points auxquels elle a l'intention de proposer un amendement.

#Rej: Rejet/Acceptation

#app: approuve la directive proposée

#dWt: Il est possible de corriger la directive en utilisant les amendements déposés.  Si les bons amendements sont adoptés, la directive pourrait être bénéfique pour l'Union Européenne. Une directive corrigée complètement de la sorte serait mieux qu'un rejet. D'un autre côté, amender une directive d'une manière si profonde est une tâche complexe, spécialement en séance plénière, et le Parlement Européen pourrait ne pas réussir à corriger l'abondance d'assomptions et de conceptions erronées introduites par la Commission. La directive a examiné une procédure qui n'a pas pris en compte l'avis des parties les plus concernées, les études universitaires ou les évidences. Rejeter le texte et demander à la Commission de recommencer depuis le début avec une méthode adéquate serait aussi justifiée. En fait, le nombre d'erreurs nécessitant une correction est si important, que compter sur les résultats du vote de la séance plénière serait très risqué. Étant donné le risque pour le progrès, la liberté et l'aptitude des sociétés européennes à être compétitives si le processus d'amendements échouait partiellement, le rejet est plus souhaitable.  Il s'agit d'une législation pour les vingt prochaines années.  Bien que cela ait déjà pris un certain temps, nous sommes dans un processus encore jeune de compréhension et pas dans l'urgence.

#ute: rejette la directive proposée

#Rec1Cec: La réalisation du marché intérieur implique que l'on élimine les restrictions à la libre circulation et les distorsions à la concurrence, tout en créant un environnement favorable à l'innovation et à l'investissement. Dans ce contexte la protection des inventions par brevet est un élément essentiel pour le succès du succès du marché intérieur. Une protection effective et harmonisée des inventions mises en oeuvre par ordinateur dans tous les États membres est essentielle pour maintenir et encourager l'investissement dans ce domaine.

#Rec1Syn: La Commission affirme péremptoirement, contre toute %(ss:évidence économique), y compris une %(bh:enquête très fouillée de Bessen & Hunt en 2003), que les brevets encouragent les investissements dans le développement informatique. De plus, le terme %(q:invention mise en oeuvre par ordinateur) prête à confusion. Il est préférable de n'avoir aucune déclaration d'intention plutôt qu'une fausse.

#Am1: La réalisation du marché intérieur implique que l'on élimine les restrictions à la libre circulation et les distorsions à la concurrence, tout en créant un environnement favorable à l'innovation et à l'investissement. Dans ce contexte, la protection des inventions par brevet est un élément essentiel du succès du marché intérieur. Une protection effective, transparente et harmonisée des inventions mises en oeuvre par ordinateur dans tous les États membres est essentielle pour maintenir et encourager les investissements dans ce domaine.

#Rec5Cec: En conséquence, les règles de droit telles qu'interprétées par les tribunaux des États membres doivent être harmonisées et les dispositions régissant la brevetabilité des inventions mises en oeuvre par ordinateur doivent être rendues transparentes. La sécurité juridique qui en résulte devrait permettre aux entreprises de tirer le meilleur parti des brevets pour les inventions mises en oeuvre par ordinateur et stimuler l'investissement et l'innovation.

#Rec5Syn1: La Commission européenne (CCE) et l'amendement 2 (JURI) affirment dogmatiquement que les brevets logiciels stimulent l'innovation. Il serait plus approprié de déclarer un but politique, à l'aune duquel le régime de brevet proposé peut être mesuré.

#Rec5Syn2: L'amendement 2 semble prétendre que le seul fait d'écrire une loi de l'UE, quel qu'en soit le contenu, engendre une %(q:certitude juridique), simplement parce que la cour du Luxembourg peut interpréter cette loi. Cette nouvelle déclaration d'intention n'est pas seulement discutable, mais aussi complétement inutile, parce que %(ol|le Brevet de la Communauté est en cours de toute façon|il y a des moyens plus faciles et plus sytématiques de déléguer à la Cour du Luxembourg que de faire passer une directive interprétative concernant l'un des aspect de la Convention européenne sur le brevet)

#Am2: En conséquence, les règles de droit régissant la brevetabilité des inventions mises en oeuvre par ordinateur doivent être harmonisées de façon à assurer que la sécurité juridique qui en résulte et le niveau des critères de brevetabilité permettent aux entreprises innovatrices de tirer le meilleur parti de leur processus inventif et stimulent l'investissement et l'innovation.

#css: La sécurité juridique est également assurée par le fait que, en cas de doute quant à l'interprétation de la présente directive, les juridictions nationales ont la possibilité, et les juridictions nationales de dernière instance l'obligation, de demander à la Cour de justice des Communautés européennes de statuer

#Rec5aSyn: Nous sommes d'accord que les réglements établis à l'article 52 de la CBE ont été rendus méconnaissables par la CBE et que l'intention originelle, décrite dans les directives d'examen de la CBE de 1978, doivent être confirmées à nouveau.

#Am88: Les dispositions visées à l'article 52 de la Convention sur la délivrance de brevets européens relatifs aux limites de la brevetabilité devraient être renforcées et précisées. La sécurité juridique qui en découle contribue à l'instauration d'un climat favorable aux investissements et à l'innovation dans le domaine du software.

#Rec6Cec: La Communauté et ses États membres sont liés par l'accord relatif aux aspects des droits de propriété intellectuelle qui touchent au commerce (ADPIC), approuvé par la décision 94/800/CE du Conseil, du 22 décembre 1994, relative à la conclusion au nom de la Communauté européenne, pour ce qui concerne les matières relevant de ses compétences, des accords des négociations multilatérales du cycle de l'Uruguay (1986-1994). L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra être obtenu pour toute invention, de produit ou de procédé, dans tous les domaines techniques, à condition qu'elle soit nouvelle, qu'elle implique une activité inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent être obtenus et des droits de brevets exercés sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur.

#Rec6Syn: Nous devons préciser qu'il y a des limites quant à ce qui peut être considéré comme %(q:champs d'application de la technologie) selon l'Art 27 des ADPIC et que cet article n'est pas conçu pour permettre une brevetabilité illimitée mais plutôt pour éviter des frictions dans le libre-échange, qui peuvent être causées par des exceptions indues mais aussi par des extensions indues de la brevetabilité. Cette interprétation des ADPIC est indirectement confirmée par le récent lobbying du gouvernement américain contre l'Art 27 des ADPIC sur la base qu'il peut exclure (ou peut être interprété de manière à exclure) la brevetabilité des logiciels et des méthodes destinées à l'exercice d'activités économiques, sur lesquels le gouvernement américain désire que l'ébauche du nouveau Substantive Patent Law Treaty fasse autorité, voir %(bh:l'article de Brian Kahin dans First Monday). L'amendement 31 supprime l'article factuellement incorrect.

#Am31: La suppression est bien, mais la clarté aurait été préférable.

#Rec7Cec: En vertu de la Convention sur la délivrance de brevets européens signée à Munich, le 5 octobre 1973, et du droit des brevets des États membres, les programmes d'ordinateurs ainsi que les découvertes, théories scientifiques, méthodes mathématiques, créations esthétiques, plans, principes et méthodes dans l'exercice d'activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques et les présentations d'informations, ne sont pas considérés comme des inventions et sont donc exclus de la brevetabilité. Cette exception ne s'applique cependant et n'est justifiée que dans la mesure où la demande de brevet ou le brevet concerne ces objets ou ces activités en tant que tels parce que lesdits objets et activités en tant que tels n'appartiennent à aucun domaine technique.

#Rec7Syn: La signification de cette disposition semble peu claire. Le but d'une directive est habituellement de définir des règles, pas d'amender des textes. Même si les états membres de la CBE décidaient qu'il est approprié de corriger l'Art 52 de la CBE (plutôt que simplement que passer outre par d'autres moyens légaux), ils pourraient toujours amender la CBE et dire que, ce faisant, ils servent l'objet de la directive.

#Am32: En vertu de la Convention sur la délivrance de brevets européens signée à Munich, le 5 octobre 1973, et du droit des brevets des États membres, les programmes d'ordinateurs ainsi que les découvertes, théories scientifiques, méthodes mathématiques, créations esthétiques, plans, principes et méthodes dans l'exercice d'activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques et les présentations d'informations, ne sont pas considérés comme des inventions et sont donc exclus de la brevetabilité. Cette exception s'applique parce que lesdits objets et activités n'appartiennent à aucun domaine technique.

#Am3: La présente directive ne vise pas à modifier ladite Convention mais à éviter des interprétations divergentes de son texte.

#Rec7bSyn: Nouvelle résolution appelant à une plus grande responsabilité de la CBE. L'effet serait plus fort si c'était un article, mais c'est quand même une bonne position de principe. Il est important d'établir toutes les sources de conflits pour faciliter la détermination des solutions.

#Am95: Le Parlement européen a, à plusieurs reprises, demandé que l'Office européen des brevets révise ses règles de fonctionnement et que cet organisme soit contrôlé publiquement dans l'exercice de ses fonctions. A cet égard, il serait particulièrement opportun de remettre en cause la pratique qui amène l'Office européen des brevets à se rétribuer sur les brevets qu'il délivre, dans la mesure où cette pratique nuit au caractère public de l'institution. Dans sa résolution du 30 mars 2000 sur la décision de l'Office européen des brevets en ce qui concerne le brevet n° EP 695 351 délivré le 8 décembre 1999, le Parlement européen a demandé une révision des règles de fonctionnement de l'Office afin d'assurer un contrôle public de l'exercice de ses fonctions.

#cdn: Les brevets logiciels seraient néfastes pour tous les développeurs de logiciel qui n'ont pas derrière eux une grosse entreprise avec beaucoup d'argent et d'avocats, et ceci comprend évidemment nombre de développeurs de logiciels %(e:libres.

#Am61: Les logiciels libres représentent une manière précieuse et socialement utile d'assurer l'innovation en commun et en partage ainsi que la diffusion de la connaissance.

#Rec11: Bien que les inventions mises en oeuvre par ordinateur soient considérées comme appartenant à un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique à l'état de la technique pour répondre au critère de l'activité inventive.

#nry: Les quatre tests de brevetabilité indiqués dans l'Art 52 de la CBE déclarent qu'il doit y avoir une invention (technique) et qu'additionnellement, cette invention doit être susceptible d'une application industrielle, être nouvelle, et comporter une activité inventive. La condition d'%(q:activité inventive) est définie à l'Art 52 de la CBE comme requérant que l'invention ne soit pas une combinaison évidente de techniques connues d'une personne experte dans le domaine. Additionnellement, %(q:invention) et %(q:contribution technique) sont synonymes: vous ne pouvez inventer que des choses nouvelles, et cette invention est votre contribution (technique) à l'état de l'art. Ainsi, l'original de la CCE et les amendements 4 et 84 sont en contradiction directe avec la CBE. De plus, une telle logique garantit que la brevetabilité est simplement une question de formulation de la demande de brevet, voir la section %(q:Quatre tests séparés pour un objet Unique.) Afin d'éviter toute confusion et tout conflit avec les articles 52 et 56 de la CBE, le considérant doit être reformulé ou supprimé.

#Am51: Les programmes informatiques sont abstraits et n'appartiennent à aucun domaine particulier. Ils sont utilisés pour décrire et contrôler des procédés dans tous les domaines des sciences naturelles et sociales appliquées.

#Am4: Pour être brevetables, les inventions en général, et les inventions mises en oeuvre par ordinateur en particulier, doivent être susceptibles d'application industrielle, être nouvelles et impliquer une activité inventive. Pour répondre au critère de l'activité inventive, les inventions mises en oeuvre par ordinateur devraient apporter une contribution technique à l'état de la technique.

#Am84: Pour être brevetables, les inventions en général et les inventions mises en oeuvre par ordinateur en particulier doivent être nouvelles, impliquer une activité inventive, et être susceptibles d'application industrielle. Pour impliquer une activité inventive, les inventions mises en oeuvre par ordinateur devraient, de plus, apporter une contribution technique à l'état de la technique, afin de les différencier du simple software.

#Rec11Syn: Le texte de la Commission déclare que les programmes d'ordinateurs sont des inventions techniques. Il supprime la condition indépendante d'invention (= %(q:contribution technique)) et l'incorpore dans la condition de non-trivialité (= %(q:marque d'inventivité), voir commentaires du Considérant 11). Ceci mène à une inconsistance théorique et à des conséquences pratiques indésirables.

#Rec11bSyn: L'amendement 73 serait un exposé parfait de l'Art 52 de la CBE s'il avait dit %(q:inventions) au lieu d'%(q:inventions mises en oeuvre par ordinateur). En l'état actuel, il a l'air d'être une règle spéciale pour des inventions faisant appel à des ordinateurs. Il est cependant utile en ce qu'il renforce la notion qu'une même invention doit réussir les quatre tests. Voir l'article 1 pour savoir en quoi le terme %(q:invention mises en oeuvre par ordinateur) est trompeur.

#Am73: Les inventions mises en oeuvre par ordinateur ne sont brevetables que si elles peuvent être rapportées à un domaine technique et, de plus, si elles sont nouvelles, impliquent une activité inventive et sont susceptibles d'application industrielle.

#Rec12: En conséquence, lorsqu'une invention n'apporte pas de contribution technique à l'état de la technique, parce que, par exemple, sa contribution spécifique ne revêt pas un caractère technique, elle ne répond pas au critère de l'activité inventive et ne peut donc faire l'objet d'un brevet.

#Rec12Syn: Le texte de la Commission européenne mélange le test d'%(q:invention technique) avec le test de %(q:marque d'inventivité), et ainsi affaiblit les deux test tout en ouvrant un espace infini d'interprétation. Cela contredit l'Art 52 de la CBE, est théoriquement inconsistant, et mène à des conséquences pratiques indésirables, telles que rendre l'examen impossible pour certains offices de brevets nationaux. A nouveau, voir l'explication du Considérant 11 pour plus d'informations. L'amendement 5 de la JURI rend les choses encore plus graves: d'abord il réintroduit littérallement l'article 3 (qu'il avait supprimé), ensuite il prétend que la %(q:marque d'inventivité) est une condition complètement différente de ce que dit l'Art 56 de la CBE, troisièmement, il parle d'un %(q:problème technique) qui devrait être résolu (alors que la brevetabilité n'a rien à voir avec le genre de problème à résoudre, mais au contraire avec la solution employée) et finalement il réitère les erreurs de l'original de la CCE.

#Am5: En conséquence, bien qu'une invention mise en oeuvre par ordinateur appartienne, par nature, à un domaine technique, il importe de préciser que, lorsqu'une invention n'apporte pas de contribution technique à l'état de la technique, parce que, par exemple, sa contribution spécifique ne revêt pas un caractère technique, elle ne répond pas au critère de l'activité inventive et ne peut donc faire l'objet d'un brevet. Pour évaluer le critère de l'activité inventive, il est habituel d'appliquer l'approche problème-solution, afin d'établir qu'il existe un problème technique à résoudre. S'il n'existe pas de problème technique, l'invention ne peut être considérée comme apportant une contribution technique à l'état de la technique.

#Am114: En conséquence, une innovation qui n?apporte pas de contribution technique à l'état de la technique n'est pas une invention au sens du droit des brevets.

#Rec13: Une procédure définie ou une séquence d'actions exécutées sur un appareil tel qu'un ordinateur, peut apporter une contribution technique à l'état de la technique et constituer ainsi une invention brevetable. Par contre, un algorithme défini sans référence à un environnement physique ne présente pas un caractère technique et ne peut donc constituer une invention brevetable.

#Rec13Syn: Dans le cadre de la pratique de l'examen des brevets, cette déclaration n'est pas ce qu'elle semble être. Elle élargit le domaine de brevetabilité même en comparaison avec les pratiques de l'OEB, en permettant la brevetabilité de solutions non techniques de problèmes techniques. De plus, elle rend les algorithmes brevetables dans toutes les situtations, parce qu'un %(q:problème technique) peut toujours être défini en termes de matériel informatique générique, ce qui fournit en même temps un moyen de revendiquer des algorithmes sous leur forme la plus abstraite.

#Rec13aSyn: L'ambiguïté syntaxique de cet amendement ne clarifie pas si les %(q:méthodes destinées à l'exercice d'activités économiques) en général sont considérées comme des %(q:méthodes dans lesquelles la seule contribution à l'état de l'art est non-technique) ou si certaines méthodes destinées à l'exercice d'activités économiques pourraient contenir des %(q:contributions techniques). Ce qui est plus important, cet amendement n'exclura rien, parce que les avocats en brevets seront prompts à revendiquer que l'%(q:invention) est caractérisées par quelque propriétés informatiques, laquelle invention doit être %(q:considérée comme un tout), comme il en est question ailleurs dans la proposition de directive. Pour que ceci soit d'une utilité quelconque, l'ambiguïté syntaxique entourant %(q:les autres méthodes) devrait être éliminée et la catégorie des %(q:méthodes non techniques) devrait être expliquée par des définitions et/ou des exemples.

#Am6: Toutefois, la simple mise en oeuvre d'une méthode, par ailleurs non brevetable, sur un appareil tel qu'un ordinateur ne suffit pas, en soi, à justifier l'existence d'une contribution technique. En conséquence, une méthode de traitement des données, une méthode destinée à l'exercice d'activités économiques, ou une autre méthode, mise en oeuvre par ordinateur, dont la seule contribution à l'état de la technique n'est pas de nature technique ne peut constituer une invention brevetable.

#Rec13bSyn: Ceci exprime un point de vue de bon sens qui a souvent été écarté pour pouvoir étendre le domaine de la brevetabilité. Seul, il n'accomplit pas grand chose et la déclaration dans la loi que la loi %(q:ne doit pas pouvoir être contournée) est un voeu pieux. Mais c'est toujours mieux que rien.

#Am7: L'invention n'est en aucun cas brevetable si la contribution à l'état de la technique se rapporte uniquement à des éléments non brevetables, quelle que soit la façon dont l'objet du brevet est présenté dans la revendication. Ainsi, l'exigence d'une contribution technique ne peut être contournée uniquement en spécifiant des moyens techniques dans la revendication de brevet.

#Am8: En outre, un algorithme est, par nature, non technique et ne peut donc constituer une invention technique. Une méthode recourant à un algorithme peut néanmoins être brevetable, dans la mesure où elle est utilisée pour résoudre un problème technique. Toutefois, tout brevet accordé pour cette méthode ne doit pas établir un monopole sur l'algorithme lui-même ou sur son utilisation dans des contextes non prévus par le brevet.

#Rec13dSyn1: L'amendement ne fait que rappeler la définition de la revendication de brevet: la demande définit le droit d'exclusion accordé par le brevet. Il permet la demande de brevet sur des logiciels en se référant simplement à l'équipement d'un ordinateur générique ou à des processus informatiques, quel que soit l'objet de l'innovation. C'est comme dire: vous pouvez revendiquer tout ce que vous voulez du moment que vous utilisez des mots tels que mémoire, processeur, ou appareil, quelque part dans la demande, mais soyez attentif à ce que votre revendication soit la plus large possible, parce que vous ne monopoliserez pas ce que vous n'avez pas demandé.

#Rec13dSyn2: L'amendement peut avoir un bénéfice indirect; il semble contredire l'amendement 18 de l'article 5 en disant que seuls les appareils et les processus peuvent être revendiqués.

#Am9: Le champ d'application des droits exclusifs conférés par tout brevet est défini par les revendications. Les inventions mises en oeuvre par ordinateur doivent être revendiquées en faisant référence à un produit, tel qu'un appareil programmé, ou à un procédé réalisé sur un tel appareil. En conséquence, lorsque des éléments individuels de logiciel sont utilisés dans des contextes qui ne comportent pas la réalisation d'un produit ou d'un procédé faisant l'objet d'une revendication valable, cette utilisation ne doit pas constituer une contrefaçon de brevet.

#Rec14: La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas nécessiter l'établissement d'une législation distincte en lieu et place des dispositions du droit national des brevets. Les règles du droit national des brevets doivent continuer de former la base de référence de la protection juridique des inventions mises en oeuvre par ordinateur, même si elles doivent être adaptées ou ajoutées en fonction de certaines contraintes spécifiques définies dans la directive.

#Rec14Syn1: Le considérant de la CCE n'est pas trop mauvais (bien qu'il utilise le terme d'%(q:inventions mises en oeuvre sur ordinateur), voir l'article 1). Il dit simplement que la loi sur les brevets ne devrait pas être remplacée. L'amendement 10 est moins clair. D'une part il dit que la brevetabilité des méthodes destinées à l'exercice d'activités économiques doit être évitée. Cependant, la CBE lui-même a accordé de nombreux brevets pour des méthodes destinées à l'exercice d'activités économiques et a fourni un %(a6:raisonnement légal pour appuyer le principe de la brevetabilité des méthodes destinées à l'exercice d'activités économiques). Le seul effet possible de l'amendement, si tant est qu'il en ait un, serait de remettre la responsabilité législative entre les mains de la CBE, dont les pratiques serviraient à définir la brevetabilité au lieu qu'elles soient soumises aux lois votées par le Parlement européen et les autres corps démocratiques représentatifs.

#Am86Syn: L'amendement 86 ne demande pas de codification de la pratique actuelle de la CBE, mais au contraire parle de %(q:méthodes non brevetables telles que les procédés et méthodes destinées à l'exercice d'activités économiques). Non seulement les méthodes destinées à l'exercice d'activités économiques triviales doivent rester non brevetables, mais toutes les méthodes destinées à l'exercice d'activités économiques doivent le rester (autrement nous nous écartons de l'Art 52 de la CBE, ce qui devrait être évité comme le suggère l'amendement 88).

#Am10: La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas nécessiter l'établissement d'une législation distincte en lieu et place des dispositions du droit national des brevets.  Les règles du droit national des brevets doivent continuer de former la base de référence de la protection juridique des inventions mises en oeuvre par ordinateur, même si elles doivent être adaptées ou ajoutées en fonction de certaines contraintes spécifiques définies dans la directive.

#Am86: La protection juridique des inventions mises en oeuvre par ordinateur ne nécessite pas l?établissement d?une législation distincte en lieu et place des dispositions du droit national des brevets. Les règles du droit national des brevets continuent de former la base de référence de la protection juridique des inventions mises en oeuvre par ordinateur. La présente directive clarifie simplement la situation juridique actuelle en tenant compte des pratiques de l'Office européen des brevets, en vue d'assurer la sécurité juridique, la transparence et la clarté de la législation et d'éviter toute dérive vers la brevetabilité de méthodes non brevetables, telles que les méthodes destinées à l'exercice d'activités économiques.

#Rec16: La position concurrentielle de l'industrie européenne vis-à-vis de ses principaux partenaires commerciaux serait améliorée si les différences actuelles dans la protection juridique des inventions mises en oeuvre sur ordinateur étaient éliminées et si la situation juridique était claire.

#Rec16Syn: L'unification de la jurisprudence n'est pas en soi une garantie de l'amélioration de la situation de l'industrie européenne. Cette directive ne devrait pas utiliser le prétexte de l'%(q:harmonisation) pour changer les règlements de l'Art 52 de la CBE, qui sont déjà d'application dans tous les pays. Que les brevets logiciels soient ou non favorables au développement des sociétés éditrices de logiciel est indépendant du fait que l'industrie des manufactures traditionnelles se déplace vers des économies à bas coûts, hors de l'UE. L'amendement 11 et le texte de la CCE supposent que l'extension de la protection par le brevet à l'industrie du développement logiciel améliorera la compétitivité des sociétés de l'UE, malgré que 75% des 30000 brevets logiciels déjà accordés (que la CCE/JURI nomme brevets sur des %(q:inventions mises en oeuvre sur ordinateur)) sont entre les mains des sociétés américaines et japonaises. De plus, si l'Europe n'entre pas dans le système des brevets logiciels, les sociétés européennes peuvent toujours parfaitement acquérir des brevets aux États-Unis et ailleurs, et les rendre exécutoires là-bas, alors que les sociétés étrangères ne pourront faire de même ici. En ce sens, ne pas participer au système des brevets logiciels est un avantage compétitif.

#Am11: La position concurrentielle de l'industrie européenne vis-à-vis de ses principaux partenaires commerciaux sera améliorée si les différences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur sont éliminées et si la situation juridique est transparente. Etant donné la tendance actuelle, qui voit l'industrie manufacturière traditionnelle déplacer son activité vers des économies où les coûts sont faibles à l'extérieur de l'Union européenne, l'importance de la protection de la propriété intellectuelle, et en particulier de la protection assurée par le brevet, est évidente.

#Am35: The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: La présente directive devrait s'appliquer sans préjudice des règles de concurrence, en particulier des articles 81 et 82 du traité.

#Rec18: Les actes permis par la Directive 91/250/EEC sur la protection juridique de programme d'ordinateurs par copyright, en particulier les réserves relatives à la décompilation et l'interopérabilité, ou les réserves concernant les plans des semi-conducteurs ou des marques déposées, ne seront pas affectés par la protection accordée par brevets aux inventions dans l'étendue couverte par cette Directive.

#Rec18Syn: La proposition de la CCE fait seulement semblant de protéger l'interopérabilité, mais en réalité fait en sorte que les droits du détenteur de brevet ne puissent être limités en aucune manière par des considérations d'interopérabilité. Voir à ce propos l'explication de l'Art 6. L'amendement 13 ne fait que restreindre encore plus les dispositions pouvant être utilisées pour passer outre aux droits du brevet.

#Am13: Les droits conférés par les brevets d'invention délivrés dans le cadre de la présente directive ne doivent pas porter atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particulières relatives à la décompilation et à l'interopérabilité. En particulier, les actes qui, en vertu des articles 5 et 6 de ladite directive, ne nécessitent pas l'autorisation du titulaire du droit, au regard des droits d'auteur de ce titulaire afférents ou attachés à un programme d'ordinateur, et qui, en l'absence desdits articles,  nécessiteraient cette autorisation, ne doivent pas nécessiter l'autorisation du titulaire du droit, au regard des droits de brevet de ce titulaire afférents ou attachés au programme d'ordinateur.

#Rec18bSyn: Ce considérant suggère que les programmes d'ordinateurs peuvent être une invention (puisqu'il n'y a pas d'autres innovations qui possède un code source ou que l'on peut décompiler), ce qui contredit l'article 52 de l'EPC, étant donné que celui-ci exclut explicitement les programmes d'ordinateur de la brevetabilité. Un logiciel ne peut jamais faire partie d'une invention. Il peut être mentionné dans la demande de brevet, mais dans ce cas il sert simplement à clarifier l'application de l'invention réelle et à limiter l'étendue du monopole que le brevet garantit à son détenteur (c-à-d si quelqu'un s'arrange pour utiliser l'invention sans le programme, il n'enfreindra pas un brevet qui décrit seulement l'utilisation de ladite invention en combinaison avec un programme d'ordinateur).

#Am74: L'application de la présente directive ne peut impliquer aucun manquement aux principes traditionnels du droit des brevets, ce qui signifie que toute demande de brevet doit inclure la description de tous les éléments de l'invention, y compris le code source; toute recherche sur ce code doit être possible, de même, donc, que sa décompilation. Ce mécanisme est nécessaire aux licences obligatoires, par exemple lorsque l'obligation d'approvisionner le marché n'est pas respectée.

#Rec18tSyn: Ce considérant suggère également que les programmes d'ordinateurs peuvent être des inventions.

#Am75: En toute hypothèse, la législation des Etats membres doit garantir que les brevets contiennent des éléments nouveaux et présentent une dimension novatrice, afin d'empêcher que des inventions tombées dans le domaine public ne fassent l'objet d'une appropriation, simplement parce qu'elles font partie intégrante d'un programme informatique.

#pmd: Europarl 09/2003: Amendements

#eus: Vous trouverez ici les amendements pour la séance plénière sur la directive des brevets logiciels publiés dans différentes langues.

#ata: Beaucoup de participants, y compris ceux qui sont supposés bénéficier de la période de grâce de nouveauté, expriment leurs doutes sur ce concept à double tranchants.

#enW: Une autre version des amendements publiables, quelquefois légèrement en avance sur cette page.

#soW: Results of the Vote

#Wan: Documents Publics au sujet du Vote en Séance Plénière

#aaW: Amendements déposés par différents groupes et leurs analyses, as far as they can be published.

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Aidez à rendre cette page multilingue

#uhn: Il est très apprécié des Membres du Parlement Européen de pouvoir cette analyse dans leur langue.

#diW: Cette page a été créée en utilisant un système de gestion multilingue, ce qui vous permet plus facilement de contribuer, si vous observez quelques règles.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

