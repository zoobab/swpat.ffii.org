 18. September 2003                                                                       A5-0238/ 84

 ÄNDERUNGSANTRAG 84
 eingereicht von Joachim Wuermeling im Namen der PPE-DE-Fraktion

          BERICHT Arlene McCarthy                                                       A5-0238/2003
          Patentierbarkeit computerimplementierter Erfindungen

          Vorschlag für eine Richtlinie
          (KOM(2002) 92 ­ C5-0082/2002 ­ 2002/0047(COD))


              Vorschlag der Kommission                            Änderungsanträge des Parlaments



                                            Änderungsantrag 84
                                                Erwägung 11

      (11) Zwar werden computerimplementierte           (11) Um patentierbar zu sein, müssen
      Erfindungen einem Gebiet der Technik              Erfindungen im Allgemeinen und
      zugerechnet, aber um das Kriterium der            computerimplementierte Erfindungen im
      erfinderischen Tätigkeit zu erfüllen, sollten     Besonderen neu sein, auf einer
      sie wie alle Erfindungen einen technischen        erfinderischen Tätigkeit beruhen und
      Beitrag zum Stand der Technik leisten.            gewerblich anwendbar sein. Um das
                                                        Kriterium der erfinderischen Tätigkeit zu
                                                        erfüllen, müssen computerimplementierte
                                                        Erfindungen zusätzlich einen neuen
                                                        technischen Beitrag zum Stand der Technik
                                                        leisten, um sie von reiner Software
                                                        abzugrenzen.

                                                                                                     Or. de





                                                                                       PE 333.851/ 84


DE                                                                                                       DE


 18. September 2003                                                                  A5-0238/ 85

 ÄNDERUNGSANTRAG 85
 eingereicht von Joachim Wuermeling im Namen der PPE-DE-Fraktion

      BERICHT Arlene McCarthy                                                      A5-0238/2003
      Patentierbarkeit computerimplementierter Erfindungen

      Vorschlag für eine Richtlinie
      (KOM(2002) 92 ­ C5-0082/2002 ­ 2002/0047(COD))


         Vorschlag der Kommission                            Änderungsanträge des Parlaments

                                       Änderungsantrag 85
                                       Erwägung 13a (neu)

                                                   (13a) Allerdings reicht allein die
                                                   Tatsache, dass eine ansonsten nicht
                                                   patentierbare Methode in einer
                                                   Vorrichtung wie einem Computer
                                                   angewendet wird, nicht aus, um davon
                                                   auszugehen, dass ein technischer Beitrag
                                                   geleistet wird. Folglich kann eine
                                                   computerimplementierte Geschäfts-,
                                                   Datenverarbeitungs- oder andere
                                                   Methode, bei der der einzige Beitrag zum
                                                   Stand der Technik nichttechnischen
                                                   Charakter hat, keine patentierbare
                                                   Erfindung darstellen.




                                                                                                Or. de





                                                                                  PE 333.851/ 85


DE                                                                                                  DE


 18. September 2003                                                                         A5-0238/ 86

 ÄNDERUNGSANTRAG 86
 eingereicht von Joachim Wuermeling im Namen der PPE-DE-Fraktion

          BERICHT Arlene McCarthy                                                         A5-0238/2003
          Patentierbarkeit computerimplementierter Erfindungen

          Vorschlag für eine Richtlinie
          (KOM(2002) 92 ­ C5-0082/2002 ­ 2002/0047(COD))


              Vorschlag der Kommission                              Änderungsanträge des Parlaments

                                              Änderungsantrag 86
                                                 Erwägung 14

      (14) Um computerimplementierte                      (14) Um computerimplementierte
      Erfindungen rechtlich zu schützen, sollten          Erfindungen rechtlich zu schützen, sind
      keine getrennten Rechtsvorschriften                 keine getrennten Rechtsvorschriften
      erforderlich sein, die das nationale                erforderlich, die das nationale Patentrecht
      Patentrecht ersetzen. Die Vorschriften des          ersetzen. Die Vorschriften des nationalen
      nationalen Patentrechts sollten auch                Patentrechts sind auch weiterhin die
      weiterhin die Hauptgrundlage für den                Hauptgrundlage für den Rechtschutz
      Rechtschutz computerimplementierter                 computerimplementierter Erfindungen.
      Erfindungen liefern, und lediglich in               Durch diese Richtlinie wird lediglich die
      bestimmten Punkten, die in dieser                   derzeitige Rechtslage klargestellt, um
      Richtlinie dargelegt sind, angepasst oder           Rechtssicherheit, Transparenz und
      ergänzt werden.                                     Rechtsklarheit zu gewährleisten und
                                                          Tendenzen entgegenzuwirken, nicht
                                                          patentierbare Methoden, wie
                                                          Trivialvorgänge und Geschäftsmethoden,
                                                          als patentfähig zu erachten.




                                                                                                       Or. de





                                                                                          PE 333.851/ 86


DE                                                                                                         DE


 18. September 2003                                                                  A5-0238/ 87

 ÄNDERUNGSANTRAG 87
 eingereicht von Joachim Wuermeling im Namen der PPE-DE-Fraktion

      BERICHT Arlene McCarthy                                                      A5-0238/2003
      Patentierbarkeit computerimplementierter Erfindungen

      Vorschlag für eine Richtlinie
      (KOM(2002) 92 ­ C5-0082/2002 ­ 2002/0047(COD))


         Vorschlag der Kommission                            Änderungsanträge des Parlaments

                                       Änderungsantrag 87
                                        Artikel 4a (neu)

                                                                     Artikel 4a
                                                      Ausschluss von der Patentierbarkeit
                                                   Bei computerimplementierten
                                                   Erfindungen wird nicht schon deshalb
                                                   von einem technischen Beitrag
                                                   ausgegangen, weil zu ihrer Ausführung
                                                   ein Computer, ein Computernetz oder
                                                   eine sonstige programmierbare
                                                   Vorrichtung eingesetzt wird. Folglich sind
                                                   Erfindungen, zu deren Ausführung
                                                   lediglich ein Computerprogramm
                                                   eingesetzt wird (Datenverarbeitung) und
                                                   durch die Geschäftsmethoden,
                                                   mathematische oder andere Methoden
                                                   angewendet werden, nicht patentfähig,
                                                   wenn sie über die normalen
                                                   physikalischen Interaktionen zwischen
                                                   einem Programm und dem Computer,
                                                   Computernetzwerk oder einer sonstigen
                                                   programmierbaren Vorrichtung, in der es
                                                   abgespielt wird, keine technischen
                                                   Wirkungen erzeugen.




                                                                                                Or. de





                                                                                   PE 333.851/ 87


DE                                                                                                  DE


 18. September 2003                                                                  A5-0238/ 88

 ÄNDERUNGSANTRAG 88
 eingereicht von Joachim Wuermeling im Namen der PPE-DE-Fraktion

      BERICHT Arlene McCarthy                                                      A5-0238/2003
      Patentierbarkeit computerimplementierter Erfindungen

      Vorschlag für eine Richtlinie
      (KOM(2002) 92 ­ C5-0082/2002 ­ 2002/0047(COD))


         Vorschlag der Kommission                            Änderungsanträge des Parlaments

                                       Änderungsantrag 88
                                       Erwägung 5a (neu)

                                                   (5a) Die Regeln gemäß Artikel 52 des
                                                   Europäischen Patentübereinkommens
                                                   über die Grenzen der Patentierbarkeit
                                                   sollen bestätigt und präzisiert werden. Die
                                                   dadurch entstehende Rechtssicherheit
                                                   sollte zu einem investitions- und
                                                   innovationsfreudigen Klima im Bereich
                                                   der Software beitragen.




                                                                                                Or. de





                                                                                  PE 333.851/ 88


DE                                                                                                  DE


