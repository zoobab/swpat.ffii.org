<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europaparlamentet 2003/09 Ändringsförslag till patentdirektivet: Verkliga eller falska begränsningar.

#descr: Den 24. september skal Europaparlamentet tage en beslutning om Kommissionens direktiv vedrörende patenter på computerprogrammer. I nuvärende form ödelägger direktivet den idag gäldende lovgivnings grundstruktur (EPC 52) og erstatter den med en ny norm (the Trilateral Standard) faststillet af de amerikanske, europäiske og japanske patentmyndigheder. I fölge denne norm betragtes alle %(q:computerimplementerede) problemlösninger som patenterbare opfindelser. Visse parlamentsmedlemmer har fremsagt ändringsforslage til direktivet, med formål at opretholde den europäiske patentkonventions striksere holdning overfor opfindelsesbegrebet. Andre argumenterer, i fölge den nye norm, for ubegränset patenterbarhed, om så i restriktiv retorisk forklädning. Vi tilbyder her en sammenlignende analyse af alle ändringsforslage, for at hjälpe

#Lgn: Legend and Commented Example

#ril: Articles and Motions

#eia: Rejection/Acceptance, Title and Recitals

#enm: Amendment number (for reference purposes in this document, real tabled amendment numbers will likely be different)

#jac: pro, contra, undecided

#tuW: 410 voted in favour, 178 voted against

#TitTit: Title

#TitCec: Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions

#trW: text of the %(op:original proposal of European Commission)

#TitSyn: Det kan ikke väre nuvärende direktivs formål at erkläre alle mulige slags %(q:computerimplementerede) ideer for patenterbare opfindelser.  Snarere er målet tydeliggörelse af patenterbarhedens begränsninger med henblik på automatisk databehandling og dens forskellige (tekniske og ikke-tekniske) användelsesområder, og dette må udtrykkes i titlen i klare og utvetydige formuleringer.

#ung: Sample justification/comments

#enm2: Amendment numbers

#uia: Submittants

#amd: voting recommendation

#ogs: voting result

#Am29: Proposal for a Directive of the European Parliament and of the Council on the limits of patentability with respect to automated data processing and its fields of application

#eet: amendment text

#tIa: Patent Insurance Extensions

#Art1: This Directive lays down rules for the patentability of computer-implemented inventions.

#nWw: Ideas implemented in generic computer hardware are not inventions in the sense of Art 52(2) EPC. The term %(q:computer-implemented invention) is not known to the man skilled in the art, and since the only things that can be implemented in a computer are computer programs (a washing machine or mobile phone cannot be implemented in a computer), it suggest that computer programs can be inventions. It was introduced by the EPO in 2000 in the infamous %(e6:Appendix 6 of the Trilateral Website document) in order to bring European patent law in line with US and JP and to allow patents for %(q:computer-implemented business methods), in anticipation of a deletion of Art 52(2) EPC.

#Am116: This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs.

#Art2a: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: Art 52 of the EPC clearly states that a computer program (a claimed object consisting of data processing equipment and rules for its operation) cannot constitute a patentable invention.

#fet: Amendments 36=42=117 clarify that an innovation is only patentable if it conforms to Art 52 of the EPC, regardless of whether or not a computer program is part of its implementation.  Amendment 14 would allow a pure computer program to be claimed as %(q:an invention the performance of which involves the use of a computer and having all of its features realised by means of a computer program), thus contradicting Art 52 EPC.

#Am36: %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

#Am14: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;

#Art2b: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Art2bSyn1: Et direktiv der lader patenterbarheden stå og falde med ordet %(q:teknisk bidrag) er nödt til at tydeligt forklare denne term.

#Art2bSyn96: Ändringsforslag 96  ser ud til at forudsätte at, kun problemet men ikke dets lösning er nödt til at väre teknisk. Ordet %(q:signifikant) ejer ingen lovlig indebyrd og medförer derfor snarere forvirrelse end tydeliggörelse. Åbenbarligen skal %(q:teknisk) defineres tydeligt for at dette skal have nogen som helst nyttig effekt. Kravet på lösning af "et teknisk problem" er lige så slemt som CEC eller värre.  Vi burde ikke fokusera på tillempelsen (det löste problem), men på lösningen (resultatet af empirisk forskning der kan meritere for et patent).  Et hvilketsomhelst computerprogram kan siges löse et teknisk problem, men hvis lösningen kun er innovativ indenfor software, kan den ikke siges at väre en teknisk lösning (i det mindste ikke en ny teknisk lösning). Hvis kendte fysiske midler bruges som perifert udstyr til en computer, kan alle mulige tekniske problemer löses via programmering. Det vil ikke sige at, disse lösninger derved er opfindelser, eller at en hvilkensomhelst kunskabsformidling de bidrager til er et teknisk bidrag (de sidste 2 fraser er synonymer, opfindelse = teknisk bidrag). Tvärtimod, en teknisk og patenterbar opfindelse kan udmärket löse ikke-tekniske problemer (f.eks. et computer chip kan kun udföre enkle udregninger meget hurtigt, hvad ikke er et teknisk problem), så dette ändringsforslag kan tilmed ekskludere ellers patenterbare opfindelser, afhängig af fortolkningen eller definitionen af ordet teknisk.

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Endelig, ändringsforslag 107 har det hele: det slår tydelig fast at, %(q:teknisk bidrag) er en synonym for opfindelse og gentager patenterbarhedskravene fra Art 52 EPC. Dette ändringsforslag begränser ikke patenterbarheden på nogen måde, det bekräfter kun Art 52 EPC og fjerner enhver forvirring der måtte opstå ved sammenblandingen af patenterbarhedspröverne, såsom i den oprindelige CEC tekst og övrige ändringsforslag.

#Am69: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#Am107: %(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.

#Am96: %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#Art2baSyn: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendments 55=97=108, which is found in various national case laws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and epistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 55 is apparently the result of mistranslation from French, should read %(q:digital). The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day case law and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such, but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification.

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#Am37: %(q:Technology) means %(q:applied natural science).  %(q:Technical) means %(q:concrete and physical).

#Am39Syn: Denne patentdoktrine er standard under de fleste jurisdiktioner.  De %(q:fire naturkräfter) er et anerkendt epistemologisk (videnskapsteoretisk) koncept. Imens matematik er abstrakt og ikke relaterer til naturkräfterne, kan nogle erhvervsmetoder meget vel väre afhängig kemien i brugerens hjerneceller, hvilken dog ikke er kontrollerbar, d.v.s. ikke-deterministisk, understillet den fri vilje. Således ekskluderer termen %(q:kontrollerbare naturkräfter) tydeligt hvad der må ekskluderes og befordrer samtidlig tilsträkkelig fleksibilitet for inkluderende af mulige fremtidige områder af tillempet naturvidenskab udenfor de hidtil anerkendte %(q:4 naturkräfter). Notér at dette ändringsforslag ikke medförer at opfindelser i apparater som mobiltelefoner og vaskemaskiner bliver umulige at patentere, det sikrer kun, at et enkelt tilläg af et nyt computerprogram i en tidligere kendt (teknisk) opfindelse ikke medförer, at dette program bliver patenterbart. Tvärtimod, tilläg af computerprogrammer til en i övrigt patenterbar opfindelse, medförer ikke at denne opfindelse bliver umulig at patentere (siden opfindelsen stadig vil löse et problem ved anvendelse af kontrollerbare naturkräfter, uanset närvär af computerprogram).

#Am39: %(q:invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature) ;

#Am38Syn: Vi önsker ikke at opfindelser indenfor %(q:musikindustrien) eller %(q:det retslige erhvervsområde, eng. legal service industry) skal möde TRIPs krav til %(q:industriel tillempelse). Ordet %(q:industri) bliver idag tit brugt i en udvidet betydelse der ikke er passende indenfor patentlovgivningens kontekst.

#Am38: %(q:industry) in the sense of patent law means %(q:automated production of material goods);

#Art3: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Art3Syn: Denne artikel omhandler fortolkningen af Art 27 TRIPs. Det er den allervigtigste princip der kräver tydeliggörelse. Artikel 3 burde således ikke slettes, men rettes, forsåvidt dette direktiv overhoved kan tydeliggöre noget.  Arlene McCarthy kritiserer helt rigtigt denne artikel og foreslår dens slettelse, bare for senere at indföre det samme kritiserede indhold i recital 12 ved ändringsforslag 5. Den Europäiske Kommissions tekst siger at alle ideer, inklusiv %(q:computer-implementerede erhvervsmetoder) mv., er patenterbare opfindelser.

#Art3aSyn1: Een af hovedindvendelserne i direktivprojektet har väret at tydeliggöre fortolkningen af Art 52 EPC i lyset af Art 27 TRIPs, hvilket siger at %(q:opfindelser indenfor alle tekniske områder) skal väre patenterbare. Vi foreslår oversättelse af Art 52(2) EPC til Art 27 TRIPs:s sprog, ifölge Ändringsforslag CULT-16.

#Art3aSyn2: Hvis databehandling er %(q:teknisk), så er hvadsomhelst %(q:teknisk).

#Art3aSyn3: Databehandling er fälles for alle områder af teknik og ikke-teknik. Ved födelsen af (universal-)computeren i 50erne, blev elektronisk databehandling (EDB) gennemgribende i samfundet og industrien.

#Art3aSyn4: Som Gert Kolle, förende teoretiker bag beslutningerne i 70erne om at ekskludere software fra patenterbarheden, skriver i 1977 (se Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: EDB er idag blevet et uundvärligt hjälpeverktöj på alle områder af det menneskelige samfund og vil fortsat väre det i fremtiden. Det er  närvärende alle steder. ... Dets instrumentelle mening, dets hjälpende og underordnede funktion adskiller EDB fra de ... individuelle felter af teknikken og sidestiller det med områder som erhvervsadministration, hvilkes arbejdsresultater og metoder ... er nödvendige for alle foretagende og for hvilke derfor prima facie påviser et behov for at sikre fri tilgängelighed.

#Art3aSynCult: Velovervejet stemte CULT for en tydelig ekskludering af databehandling fra feltet %(q:teknik).

#Art3aSynStud: Adskillige studier, nogle udförte af EU-institutter, så vel som udtalelser fra European Economic and Social Committee og European Comittee of Regions forklarer i detalje hvorfor Europas ökonomi vil lide skade, hvis databehandlingsopfindelser ikke tydeligt ekskluderes fra patenterbarheden.

#Am45: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#Art4N1: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Art4N2: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#Art4N3: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#Art4Syn: This article mixes the non-obviousness test with the invention test, thereby weakening both and conflicting with Art 52 EPC.

#cpW: JURI has split this article into parts 1, 2 and 3.  This is dangerous because it prevents certain amendments, which may apply to the whole article, from being put to vote.

#iaa: Split into parts 1 2 and 3.

#Art4N1Syn: %(q:Computer-implementerede opfindelser) (dvs. Ideer indenfor alment anvendeligt EDB-udstyr = computerprogrammer) er ikke-opfindelser ifölge Art 52 EPC. Se videre analyse for artikel 1. Ändringsforslag 58=98=109 retter problemet og genformulerer Art 52 EPC.  Ingen af ändringsforslagene begränser patenterbarheden, men dette ändringsforslag udtrykker i det mindste tingene formelt rigtigt.

#Am16N1: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.

#Am98: Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: En ren genformulering af Art 52 EPC. Da målet for dette direktiv er harmonisering og tydeliggörelse, er det af allerhöjeste vägt at understrege den rigtige fortolkning af EPC, siden den er basis for patentlovgivningen i Europa.

#Am48: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4N2Syn: Kommissionens tekst afviger fra Art 52 EPC. Som vist i analysen af Recital 11, skal man först afgöre om der foreligger opfindelse/teknisk bidrag (enten ved brug af den negative definition i Art 52 EPC: et computerprogram er ikke en opfindelse, eller ved brug af den positive definition der opstod fra präjudikater baserede på opfindelsesbegrebet de kontrollerbare naturkräfter). Efter det er det nödvendigt at kontrollere, hvis denne opfindelse klarer de tre fölgende tester fra Art 52, af hvilke eet er det nyskabende led (ikke-indlysende). Kommissionens tekst hävder, at pröven af det nyskabende led er nödvendigt for at noget skal betragtes som en opfindelse. Siden et computerprogram kan väre ikke-indlysende, kan det väre en opfindelse ifölge denne definition (modsat hvad EPC Art 52 siger). Videre implicerer Kommissionens tekst at ideer indenfor alment anvendeligt EDB-udstyr (computerprogrammer) er patenterbare opfindelser.  Slettelse, som foreslået i ändringsforslag 82, kunne väre en hjälp, siden den eneste effekt af dette stykke er forstyrrelse af patenterbarhedstesterne, således at det vil blive umuligt for de nationale patentstyrelser at afvise patentansögninger udenfor det patenterbare område lovhjemmel non-statutory patent applications without substantive examination.  Amendment 16 makes the same mistake as the commission text, it just tidies up the language a bit. Amendment 40 will be retracted, it has been retabled as an insertion now (83).

#Art4N2SynP2: Moreover, the Commission text implies that ideas framed in terms of the general purpose computer (programs for computers) are patentable inventions.  Deletion, as proposed by amendment 82, could be helpful, because the only effect of this paragraph is to confuse patentability tests and thereby make it impossible for national patent offices to reject non-statutory patent applications without substantive examination.  Amendment 16 makes the same mistake as the commission text, it just tidies up the language a bit. Amendment 40 will be retracted, it has been retabled as an insertion now (83).

#Am16N2: Amendment 14 is analysed as parts 14(1), 14(2) and 14(3) below.

#Art4N3Syn1: When you patent an invention, you claim a particular application of this invention. This application is what is described in the claims. As such, the claims (as a whole) contain both the invention itself in some way, as well as a number of non-patentable features (such as e.g. a computer program) which are required for said application of the invention. This means that even if you require the the invention to be technical, the claims can contain non-technical features (which we don't oppose at all, that's how patents have always worked).

#Art4N3Syn2: However, the European Commission's proposal voids its own concept of %(q:technical contribution) by allowing the contribution to consist of non-technical features. The JURI version only states that the claims must contain technical features, but this is no real limit. An example may clarify this. Suppose we want to patent a computer program, so we say that it is the %(q:technical contribution). In the claims, we describe the application of this program, so we say that we patent the execution of this program on a computer. Now the patent claims as a whole contain technical features (the computer), and the difference between the claims as a whole (known computer+new program) and the state of the art (known computer) is the computer program. So a computer program can be a technical contribution, according to this condition, which is completely contradictory (as a computer program cannot be technical). It is clear that a correction such as that in amendment 99=57=110 is necessary, if the concept of %(q:technical contribution) is to be used at all.

#lWo: The novelty grace period proposed by amendment 100 is an orthogonal question.  As shown by a %(ng:recent consultation conducted by the UK Patent Office), it is very controversial even among those who are supposed to benefit from it.  It is not clear whether a novelty grace period would in fact, as suggested by the ITRE report, benefit SMEs.  In the case of open source development, it could even cause additional insecurity as to whether published ideas are free from patents.

#Am16N3: Amendment 14 is analysed as parts 14(1), 14(2) and 14(3) below.

#Am99: The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art.

#Am100P1: The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.

#Art43aSyn: As mentioned in the analysis of article 1, %(q:computer-implemented invention) is contradictory term. As mentioned in the analysis of Recital 11, %(q:invention) and %(q:technical contribution) are synonyms: what you invented, is your (technical) contribution to the state of the art.  In the jargon used by the Directive, however, %(q:technical contribution) only means %(q:solution to a technical problem within the step from the closest prior art document to the claimed invention).  In this context, it is unclear which effect any test, however strict, might have, because it is uncertain to what it applies.

#Art43aSynTest: Also, by saying that this test %(q:shall apply), the provision does not clearly require that this test actually %(e:must be passed), nor that it defines what is meant by %(q:technical contribution).

#Art43aSynInd: Finally, there is no legal definition of %(q:industrial application in the strict sense of the expression, in terms of both method and result).

#Art43aSynVal: In spite of these shortcomings, we recommend to support Amendment 70.  At least it codifies central aspects of the concept of %(q:technical invention).  Thereby, in combination with other amendments, it could eventually contribute to drawing a clear limit of patentability.

#Am70: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4aSyn: Amendment 17 assumes that %(q:normal physical interaction between a program and the computer) means something.  It does not.  A meaning could have been assigned, e.g. by specifying, in borrowing from the German Federal Patent Court's recent %(es:%(tp|Error Search|Suche Fehlerhafter Zeichenketten) decision), that %(q:increase of computation efficiency), %(q:savings in memory usage) etc %(q:do not constitute a technical contribution), or %(q:are within the scope of the normal physical interaction between a program and the computer).  Without such a specification, Amendment 17 only adds obufscation instead of clarification.

#Am17: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)

#Art4bSyn: Same comments as for the amendment above.

#Am87: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions merely involving computer programs (data processing) which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)

#Art4cSyn1: The directive as proposed by CEC and JURI, is based on the Trilateral Standard of the US, Japanese and European Patent Office of 2000, which considers all computer programs to be patentable inventions.  This is expressed in the word %(q:computer-implemented invention), which was introduced by the EPO in order to support this standard.  There is no longer a test of whether an invention is present, but only a test of %(q:technical contribution in the inventive step), i.e the step between the %(q:closest prior art) and the %(q:invention) must be framed in terms of a %(q:technical problem), e.g. a problem of improving speed or efficiency in the use of computer memory.  By using this EPO doctrine, in effect any algorithm or business method becomes patentable, as has been pointed out by the German Federal Patent Court in a %(es:recent decision).

#Art4cSyn2: Amendments 47 and 60 clearly tell the EPO to follow the German Federal Patent Court in refusing to consider improvement of data processing efficiency a %(q:technical contribution).

#Am60: Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.

#Art4dSyn1: This amendment alone could replace most of the provisions of the directive.  The EPO Guidelines of 1978 provide a clear interpretation of Art 52.  This was changed %(q:in response to demands from the industry) by later versions of the Guidelines, at the price of making the rules contradictory and introducing a schism.  The objectives of this directive should be to resolve the schism and to concretise the application of the TRIPs treaty of 1994.  The first objective can be achieved by this amendment alone.  Note that the amendment does not say that the EPO Guidlines of 1978 should become law.  It merely says that they provide a proper interpretation of Art 52 EPC.

#Am46: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Article 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#Art5: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Art5Syn18: Amendment 18 proposes to introduce program claims, i.e. claims of the form %(bc:a program, characterised by that upon loading in computer memory [ some process ] is executed.) This would make all publication of software a potential direct patent infringement and thus put programmers, researchers and Internet service distributors at risk. Without program claims, distributors could only be liable indirectly, e.g. through the guarantees which they give to their customer.  As such, the program claim question seems to be more of symbolic than of material nature: the patent lobby wants to document unmistakeably that software as such is patentable.  CEC has refrained from this last consequence.

#Art5Syn58: Yet, the CEC wording still suggests that known general purpose computing hardware and calculation rules executed thereon (= computer programs) are patentable products and processes. Amendment 101/58 tries to address this problem.  It might be a good idea to replace %(q:technical production processes operated by such a computer) by %(q:the inventive processes running on such a set of equipment) if that is still possible in the form of a %(q:compromise amendment). Amendment 102 defines %(q:computer-implemented invention) as being only a product or a technical production process. As long as %(q:technical) is defined, this might be useful. Nevertheless, a computer running a program could also be interpreted as a %(q:programmed device), and patenting the use of a computer program when executed on a computer has the same effect as patenting the computer program itself: there is no other way to use a computer program than by executing it on a computer.

#Am18: A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

#Am101: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#Am102: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#Art5aSyn: Free software makes important contributions to innovation and knowledge diffusion without benefitting from the patent system.  This should be reflected in innovation policy and in law, without implying that proprietary software should be patentable.  Unfortunately this amendment uses the term %(q:computer-implemented inventions), which was introduced by the EPO in 2000 in an attempt to imply patentability of general purpose data processing operations (software).  This should be corrected in a second reading.

#Am62: %(al|Limitation of the effects of patents granted to computer-implemented inventions|%(linol|Member States shall ensure that the rights conferred by the patent shall not extend to the acts done to run, copy, distribute, study, change or improve a computer program which is distributed under a licence that provides for:|the freedom to run the program;|the freedom to study how the program works, and adapt it to the user's needs;|the freedom to redistribute copies under the same licence conditions;|the freedom to improve the program, and release improvements to the public under the same licence conditions;|free access to the source code of the program.))

#Art51a: This amendment apparently intends to exclude claims to ways of operating data processing hardware (= computer programs), but proposes inappropriate regulatory means.  Patent claims always comprise more than the invention (= technical contribution).  If there is a technical invention (e.g. a new chemical process), then there is no reason why the patent claim should not comprise this invention together with non-invention elements, such as a computer program that controls the reaction.  A claim does not describe an invention but the scope of an exclusion right which is legitimated by an invention.

#Am72: Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentansprüche nur den technischen Beitrag umfassen, der den Patentanspruch begründet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datenträger vorliegendes Programm, ist unzulässig.

#Am103Syn: Without this amendment, software patent owners could still send cease-and-desist letters to programmers or software distributors accusing them of contributory infringement.  This article makes sure that the right of publication as guaranteed by Art 10 ECHR takes precedence over patents.  On the other hand, this amendment does not prevent the granting of software patents or their enforcement against users of software.  Vendors of proprietary software and commercial Linux distributors could still be under pressure from their customers, who would usually impose a contractual obligation of patent clearance on them.

#Am103: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#Am104N1Syn: The scope of the patent is normally defined by the claims, and if something falls outside the claim scope, it does not constitute an infringment.  Thus the amendment, at first sight, seems tautological.  The intention of this amendment may be to allow simulation of patentable processes on a standard data processing system.  If so, it should be said clearly, as done by some of the other amendments.

#Am104N1: Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#Art5dSyn: These amendments do, unlike some may think, not serve to promote protect free/opensource software, but rather to ensure that the obligation of disclosure which is inherent in the patent system is taken seriously and that software is, like any other information object, on the disclosure side of the patent rather than on its exclusion/monopolisation side. This makes it a little more difficult to block people from doing things you  even haven't done yourself, but which are obviously possible since the computing model is perfectly defined and you always know what you can do with a computer. When you publish working source code you at least offer some real knowledge on how to solve the problem, unlike when you say that %(q:processor means coupled to input output means so that they compute a function such that the result of said function when output through said output means solves the problem the user wanted to solve).

#Am104N2: Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#Art6: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Art6Syn: The European Commission's proposal does not protect interoperability but, on the contrary, makes sure that interoperable software can not be published or used when an interface is patented. The only behavior which the European Commission wants to permit in this case is decompilation, which would not infringe on patents anyway (as patents involve the use of what is described in the claims, not the studying of said object). Amendments 66 and 67 moroever specify the laws on which interoperability privileges can be based, thus making it even more certain that the provision can not have any effect.

#Am19: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

#Art6aSyn: Amendments 20 and 50 are the only one that formulate a real interoperability privilege.  However, the word %(q:computer systems and network) should, for greater clarity, be changed to %(q:data processing systems), as proposed by amendment 50.  Amendment 20 was already accepted in CULT, ITRE and JURI.

#Am76Syn: Amendment 76 removes all clarity about when use of patented techniques for interoperation is allowed and when not.  It runs counter to the main purpose of the directive, which is to concretise certain abstract meta-rules, including those of Art 30 TRIPs, and thereby achieve clarity and legal security.

#Am105Syn: It is nice that amendment 105 agrees with us that a %(q:computer-implemented invention) cannot mean anything else but software running on a computer. However, the exceptions it mentions are very strange. In the first case, if the invention is an independent machine, there is no interoperability requirement. If there is no interoperability requirement, amendment 20 also won't apply. Conversely, if there is such a requirement, then the patented invention will not be working as an independent machine or technical invention. The second clause is quite dangerous, as it leaves everything for the courts to decide, thus failing to achieve the directive's aim of clarification. As the anti-trust cases against Microsoft in both the USA and in Europe shows, it is extremely difficult and time-consuming to determine whether any behavior violates existing competition law.  This would render the interoperability clause toothless and put SME's even more at a disadvantage, as large companies have much more money to spend on court cases.

#Am50: Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement..

#Am20: Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#Am76: Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that it does not unreasonably conflict with a normal exploitation of the patent and does not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#Am105: %(al|Use of patented technologies|Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that:|%(ol|the invention embodying the computer-implemented software is not working as an independent machine or technical invention, and|the patent is not in conflict with European competition law.))

#Art6b: This amendment seems to create a new property right outside of the patent system.  It establishes a category called %(q:computer-implemented inventions), which belong to no %(q:field of technology).  This is an interesting approach, although 7 years is probably still too long and some further questions need to be clarified in order for this to be workable.

#Am106: %(al|Term of patent|The term of a patent granted for computer-implemented inventions pursuant to this Directive shall be 7 years as from the date of filing.)

#Artt7: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Art7Syn: It should be made sure that the investigations are not performed by the patent lawyers from DG Internal Market, but rather by software and economics experts. Amendment 71 talks about creating an insurance system for patent acquisition and litigation, although there is no practical example of such a working system. The theoretical possibility as suggested by a consultants report to DG MARKT is also based on wrong assumptions, as explained in %(le:open letter to the European Parliament) written by a number of distinguished economists. Amendment 91 is slightly better than the rest, but if SMEs are specially mentioned, then at least free/opensource software and standardisation should also be mentioned. It also does not guarantee that the study will be performed by an independent team of software economics experts.

#Am21: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and electronic commerce.

#Am91: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses. Special attention will be paid to the position of small and medium-sized enterprises and of electronic commerce and to the impact of dominant positions on the functioning of the market.

#Am71Syn: Amendment 71 calls for creation of an insurance system for patent acquisition and litigation, although there is no practical example of such a working system. The theoretical possibility as suggested by a consultants report to DG MARKT is also based on wrong assumptions, as explained in %(le:open letter to the European Parliament) written by a number of distinguished economists.

#Am90Syn: Amendment 90 is like 71, but less explicit.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises and the open source community, and electronic commerce.

#Am71P2: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents, in particular through the creation of a defence fund and the introduction of special rules on legal costs.

#Am71P3: It shall report on its findings to the European Parliament and the Council and present appropriate proposals for legislation without delay.

#rbr: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Rejection/Acceptance

#app: approves the proposed directive

#dWt: It is possible to correct the directive using the tabled amendments.  If the right amendments are adopted, the directive could be beneficial for the EU. So adopting a thoroughly corrected directive would be better than rejection. On the other hand, amending a directive this thoroughly is a complex task, especially in plenary, and the European Parliament may not succeed in fixing the abundant assumptions and misconceptions introduced by the Commission. The directive went through a procedure that hasn't taken into account most interested parties, academic studies or evidences. Rejecting the text and asking the Commission to start over with due process would also be justified. In fact, the number of mistakes needing correction is so large, that relying on the results of plenary vote for a good directive is very risky. Given the risk to the progress, freedom and the EU-companies' ability to compete if the amending partially fails, rejection is more desirable.  This is legislation for the next 20 years.  Although it has already taken some time, we are still in an early process of understanding and not in a hurry.

#ute: reject the proposed directive

#Rec1Cec: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec1Syn: The Commission boldly postulates, against all %(ss:economic evidence), including %(bh:very detailed empirical research by Bessen & Hunt from 2003), that patents encourage investments in software develoment.  Moreover, the term %(q:computer-implemented invention) is confusing.  It is better to have no target statement than a wrong one.

#Am1: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec5Cec: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Rec5Syn1: Both the European Commission (CEC) and Amendment 2 (JURI) say dogmatically that software patents stimulate innovation.  It would be more appropriate to state a policy goal, by which the proposed patentability regime can be measured.

#Rec5Syn2: Amendment 2 seems to claim that the mere fact of writing EU law, no matter what the contents, already %(q:results in legal certainty), simply because the Luxemburg court can interpret this law.  This new goal statement is not only questionable but also pointless, because %(ol|the Community Patent is under way anyway|there are easier and more systematic ways to put the Luxemburg court in charge than by passing an interpretative directive for one aspect of the European Patent Convention)

#Am2: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#css: Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

#Rec5aSyn: We agree that the rules laid down in Article 52 EPC have been stretched beyond recognition by the EPO and that the original meaning, as described in the 1978 EPO examination guidelines, should be reconfirmed.

#Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

#Rec6Cec: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Rec6Syn: We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it can excludes (or can be construed to exclude) software and business method patents, which the US government wants to mandate by the new Substantive Patent Law Treaty draft, see %(bh:Brian Kahin article in First Monday). Amendment 31 deletes the factually incorrect article.

#Am31: Deletion is good, but clarification would have been better.

#Rec7Cec: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Rec7Syn: Art 52 EPC says that programs for computers etc are not %(e:inventions) in the sense of patent law, i.e. that a system consisting of generic computing hardware and some combination of calculation rules operating on it can not form the object of a patent. It does not say that such systems can be patented by declaring them to be %(q:not as such) or %(q:technical). Amendment 5 fixes this bug by reconfirming Art 52 EPC (as suggested in Amendment 88 to Recital 5 (a) (new) above). Note that the exclusion of programs for computers is not an exception, it is part of the rule for defining what an %(q:invention) is.

#Am32: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.

#Am3: The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.

#Rec7bSyn: New resolution calling for greater accountability of EPO. The effect would be stronger if it was an article, but it is still a good principle to have. It is important to state all sources of conflicts in order to ease eventual solutions.

#Am95: Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeÕs operating rules to ensure that it was publicly accountable in the exercise of its functions.

#cdn: Software patents would be bad for all software developers who aren't backed by a big company with a lot of money and lawyers, and this obviously includes a number of Free Software developers.

#Am61: Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: The four patentibility tests laid out in Art 52 EPC state that there must be a (technical) invention and that additionally, this invention must be susceptible of industrial application, new and involve an inventive step. The %(q:inventive step) conditions is defined in Art 56 EPC as requiring the invention not to be an obvious combination of known techniques to a person skilled in the art. Additionally, %(q:invention) and %(q:technical contribution) are synonyms: you can only invent new things, and this invention is your (technical) contribution to the state of the art.  Therefore, the CEC original and amendments 4 and 84 are in direct contradiction with the EPC. Additionally, such a logic assures that patentability is simply a question of claim wording, see %(se:Four Separate Tests on One Unified Object).  In order to avoid confusion and conflicts with EPC Art 52 and 56, the recital needs to be rewritten or deleted.

#Am51: While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science.

#Am4: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.

#Am84: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.

#Rec11Syn: The Commission text declares computer programs to be technical inventions. It removes the independent requirement of invention (= %(q:technical contribution)) and merges it into the requirement of non-obviousness (= %(q:inventive step), see comments for Recital 11). This leads to theoretical inconsistency and undesirable practical consequences.

#Rec11bSyn: Amendment 73 would be a perfect restatement of Art 52 EPC if it had said %(q:inventions) instead of %(q:computer-implemented inventions). As it is now, it looks as if it was a special rule for inventions involving computers.   Yet it is useful in that reinforces the notion that the same invention must pass the four tests.  See Article 1 for why the term %(q:computer-implemented invention) is misleading.

#Am73: Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application..

#Rec12: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Rec12Syn: The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This contradicts Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible. Again, see the explanation in Recital 11 for more information. Amendment 5 by JURI makes matters even worse: first of all it literally reintroduces article 3 (which it deleted), secondly it claims the %(q:inventive step) requirement is something completely different from what Art 56 EPC says, thirdly it starts talking about a %(q:technical problem) which should be solved (while patentability has nothing to do with the kind of problem that is solved, but instead on the solution employed) and finally it also repeats the mistakes of the CEC original.

#Am5: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Am114: Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law.

#Rec13: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Rec13Syn: The CEC recital pretends to exclude algorithms but in fact makes algorithms patentable, because an algorithm which is defined with reference to the environment of generic data processing equipment (= %(q:a physical environment)) is exactly the same as the algorithm per se. It is absurd and contrary to the goal of clarification to try to make an artificial distinction between mathematical algorithms and nonmathematical algorithms. To a computer scientist, this makes no sense, because every algorithm is as mathematical as anything could be. An algorithm is an abstract concept unrelated to physical laws of the universe.

#Rec13aSyn: Syntactical ambiguity in this amendment leaves it unclear whether %(q:business method) in general are considered to be a %(q:methods in which the only contribution to the state of the art is non-technical) or whether certain business methods could contain %(q:technical contributions).  More importantly, this amendment will not really exclude anything, because patent lawyers will be quick to claim that some computer-related features are characteristic for the %(q:invention), which, as stated elsewhere in this directive proposal, must be %(q:considered as a whole).  For this to be of any use, the syntactical ambiguity around %(q:other method) would have to be removed and the category of %(q:non-technical methods) would have to be explained by definitions and/or examples.

#Am6: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#Rec13bSyn: This states a common sense position which has often been disregarded in order to extend patentability.  This alone does not achieve much and the statement in the law that the law %(q:can not be circumvented) is a pious wish.  Yet, even a wish is better than nothing.

#Am7: If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

#Am8: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#Rec13dSyn1: The amendment simply restates the definition of claims: claims define the exclusion right granted by the patent. It allows claims to software by merely refering to generic computer equipment or computer processes, regardless of where the innovation lies. It's similar to saying: you can claim what you please as long as you use vocabulary like storage, processor, or apparatus somewhere in the claims, but be careful to make your claim as broad as you want it, because you won't monopolise what you didn't claim.

#Rec13dSyn2: The amendment may have an indirect benefit: it seems to contradict amendment 18 to article 5 by saying that only programmed apparatuses and processes can be claimed.

#Am9: The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.

#Rec14: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#Rec14Syn1: The CEC recital is not too bad (although it uses the %(q:computer-implemented inventions) term, see Article 1). It simply says patent law should not be replaced.  Amendment 10 is less clear.  On the one hand it equates %(q:present legal position) (?) with %(q:practices of the European Patent Office). On the other it says that the patentability of business methods is to be avoided. However the EPO itself has been granting many patents for business method and has provided %(a6:legal reasoning to support business method patents).  The only possible effect of the amendment, if any, would be to surrender legislative responsability to the EPO, whose practices would seem to define patentability instead of abiding to the laws voted by the European Parliament and other democraticaly representative bodies.

#Am86Syn: Amendment 86 does not ask for a codification of the current EPO practice, but on the other hand talks about %(q:unpatentable methods such as trivial procedures and business methods). Not just trivial business methods must remain unpatentable, but all business methods must remain so (otherwise we are deviating from Art 52 EPC, which should be avoided as suggested in amendment 88).

#Am10: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.

#Am86: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec16: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#Rec16Syn: Unification of case law in itself is not a guarantee of improvement of the situation of European industry. This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries. Whether or not software patents are good for European software development companies is independent of the fact that the traditional manufacturing industry is moving to low-cost economies outside the EU. Amendment 11 and the CEC text assume that extending patent protection to the software development industry will improve the competitiveness of EU companies, even though 75% of the 30,000 already granted software patents (which CEC/JURI call patents on %(q:computer-implemented inventions)) are in hands of US and Japanese companies. Additionally, if Europe does not have software patents, European companies still can perfectly acquire them in the US and elsewhere and enforce them there, while the affected foreign companies cannot do the same here. In that sense, not having software patents in Europe is a competitive advantage.

#Am11: The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.

#Am35: The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Rec18: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Rec18Syn: The CEC proposal only pretends to protect interoperability, but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendment 13 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.

#Am13: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

#Rec18bSyn: This recital suggests that computer programs can be an invention (since there are no other innovations which have source code or which you can decompile), which contradicts article 52 EPC, since that one explicitly excludes computer programs from patentability. Software can never be part of an invention. It can be mentioned in the claims of the patent, but in that case it merely serves to clarify the application of the actual invention and to limit the scope of the monopoly that the patent grants to its holder (i.e., if someone manages to make use of the invention without using a computer program, he will not infringe on a patent that only describes the use of said invention in combination with a computer program).

#Am74: The application of this directive must not deviate from the original foundations of patent law, which means that the applicant of the patent must provide a description of all elements of the invention, including the source code, and that research into it, and as such decompilation, must be made possible. This way of working is indispensable to make compulsory licensing possible, for example if the obligation to supply the market is not fulfilled.

#Rec18tSyn: This recital also suggests that computer programs can be inventions.  Also, it has no effect, because it only restates undisputed patent doctrines.

#Am75: In any case, the law of all member states must ensure that the patents contain novelties and contain an inventive step, to prevent inventions which are already public from being appropriated, simply because they belong to a computer program.

#pmd: Europarl 2003/09: Amendments

#eus: Here the amendments for the plenary decision on the software patent directive are published in various languages.

#ata: Many of the participants, including those who are supposed to benefit from the novelty grace period, express doubts about this double-edged concept.

#enW: Another version of the publishable amendments, sometimes slightly ahead of this page.

#soW: Results of the Vote

#Wan: Public Documents Regarding the Plenary Vote

#aaW: deleted

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Help make this page multilingual

#uhn: It is very much valued by many MEPs if they can read this analysis in their language.  This page can also be useful as a reference for the Council and for the second reading.

#diW: This page is created using a multilinguality managment system, which makes it convenient for you to contribute, if you observe a few rules.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: da ;
# multlin: t ;
# End: ;

