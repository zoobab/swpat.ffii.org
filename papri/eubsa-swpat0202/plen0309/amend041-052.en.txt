 18 September 2003                                                                                A5-0238/ 41

 AMENDMENT 41
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                             A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


             Commission proposal                                        Amendments by Parliament

                                              Amendment 41
                                                    Title

  Proposal for a directive of the European                   Proposal for a directive of the European
  Parliament and of the Council on the                       Parliament and of the Council on the limits
  patentability of computer-implemented                      of patentability with respect to automated
  inventions                                                 data processing and its fields of
                                                             application

                                                Justification

 The term "computer-implemented invention" is not used by computer professionals. It is in fact
 not in wide use at all. It was introduced by the European Patent Office (EPO) in May 2000 in
 Appendix 6 of the Trilateral Conference, where it served to legitimate business method patents,
 so  as  to  bring  EPO  practise  in  line  with  the  USA  and  Japan.  Much  of  the  European
 Commission's  directive  proposal  is  based  on  wordings  from  this  "Appendix  6"  .  The  term
 "computer-implemented  invention"  is  a  programmatic  statement.  It  implies  that  calculation
 rules  framed  in  the  terms  of  the  general-purpose  computer  are  patentable inventions. This
 implication  is  in  contradiction  with  Art  52  EPC,  according  to  which  algorithms,  business
 methods and programs for computers are not inventions in the sense of patent law. It can not be
 the aim of the current directive to declare all kinds of "computer-implemented" ideas to be
 patentable  inventions.  Rather  the  aim  is  to  clarify  the  limits  of  patentability  with  regard  to
 automatic data processing and its various (technical and non-technical) fields of application,
 and this must be expressed in the title in plain and unambiguous wording.


                                                                                                      Or. en





                                                                                            PE 333.851/ 41

EN                                                                                                               EN


 18 September 2003                                                                        A5-0238/ 42

 AMENDMENT 42
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

      REPORT by Arlene McCarthy                                                         A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                            Amendment 42
                                       Article 2, point (a)

 (a) "computer-implemented invention"                    (a) "computer-implemented invention"
 means any invention the performance of                  means any invention in the sense of the
 which involves the use of a computer,                   European Patent Convention the
 computer network or other programmable                  performance of which involves the use of a
 apparatus and having one or more prima                  computer, computer network or other
 facie novel features which are realised                 programmable apparatus and having in its
 wholly or partly by means of a computer                 implementations one or more non-
 program or computer programs;                           technical features which are realised
                                                         wholly or partly by a computer program or
                                                         computer programs, besides the technical
                                                         features that any invention must
                                                         contribute;

                                              Justification

 Art 52 of the EPC clearly states that a stand-alone computer program (or a "computer
 program as such") cannot constitute a patentable invention. This amendment clarifies that an
 innovation is only patentable if conforms to Art 52 of the EPC, regardless of whether or not a
 computer program is part of its implementation.

                                                                                                  Or. en





                                                                                       PE 333.851/ 42

EN                                                                                                          EN


 18 September 2003                                                                         A5-0238/ 43

 AMENDMENT 43
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                         A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                          Amendment 43
                                     Article 2, point (ba) (new)

                                                      (ba) "invention" in the sense of patent law
                                                      means "solution of a problem by use of
                                                      controllable forces of nature".


                                            Justification

 This is a standard patent doctrine in most jurisdictions. The EPO says that inventions are
 "technical solutions of technical problems" and understands "technical" as "concrete and
 physical". The term "controllable forces of nature" clarifies this further. The "four forces of
 nature" are an acknowledged concept of epistemology (theory of science). While mathematics is
 abstract and unrelated related to forces of nature, some business methods may well depend on
 the chemistry of the customer's brain cells, which is however not controllable, i.e.
 non-deterministic, subject to free will. Thus the term "controllable forces of nature" clearly
 excludes what needs to be excluded and yet provides enough flexibility for inclusion of possible
 future fields of applied natural science beyond the currently acknowledged "4 forces of nature".
 This concept has been formulated in most jurisdictions and even written into the law in some
 countries such as Japan and Poland. Even the CEC proposal say that "algorithms and business
 methods are inherently non technical". The classical justification for the "technical character"
 of "computer-implemented inventions" is not that the meaning of "technical" has changed but
 that the computer indeed consumes energy in a controlled way, and that the "invention" must be
 "considered as a whole". The critics of this view, e.g. the German Federal Patent Court, argue
 that "the solution is completed by abstract calculation before, during its non-inventive
 implementation on a conventional data processing system, forces of nature come into play".

                                                                                                Or. en





                                                                                         PE 333.851/ 43

EN                                                                                                        EN


 18 September 2003                                                                     A5-0238/ 44

 AMENDMENT 44
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

      REPORT by Arlene McCarthy                                                      A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                   Amendments by Parliament

                                         Amendment 44
                                   Article 2, point (bb) (new)

                                                    (bb) "industry" in the sense of patent law
                                                    means "automated production of material
                                                    goods".

                                          Justification

 We do not want innovations in the "music industry" or "legal services industry" to meet the
 TRIPS requirement of "industrial applicability". The word "industry" is nowadays often used in
 extended meanings which are not appropriate in the context of patent law.

                                                                                              Or. en





                                                                                    PE 333.851/ 44

EN                                                                                                      EN


 18 September 2003                                                                     A5-0238/ 45

 AMENDMENT 45
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                     A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                  Amendments by Parliament

                                          Amendment 45
                                          Article 3a (new)

                                                                      Article 3a
                                                      Member States shall ensure that data
                                                      processing is not considered to be a field
                                                      of technology in the sense of patent law,
                                                      and that innovations in the field of data
                                                      processing are not considered to be
                                                      inventions in the sense of patent law.


                                            Justification

 Member states shall ensure that data processing is not considered to be a field of technology in
 the sense of patent law, and that innovations in the field of data processing are not considered
 to be inventions in the sense of patent law.


                                                                                                Or. en





                                                                                    PE 333.851/ 45

EN                                                                                                        EN


 18 September 2003                                                                   A5-0238/ 46

 AMENDMENT 46
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

      REPORT by Arlene McCarthy                                                    A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


           Commission proposal                                  Amendments by Parliament

                                        Amendment 46
                                  Article 4, paragraph -1 (new)

                                                    -1. Member States shall ensure that
                                                    patents on computerised innovations are
                                                    upheld and enforced only if they were
                                                    granted according to the rules of Article
                                                    52 of the European Patent Convention of
                                                    1973, as explained in the European
                                                    Patent Office's Examination Guidelines
                                                    of 1978.


                                          Justification

 This amendment achieves full clarification and ensures that the European Court of Justice can
 harmonise caselaw if needed.


                                                                                            Or. en





                                                                                  PE 333.851/ 46

EN                                                                                                    EN


 18 September 2003                                                                  A5-0238/ 47

 AMENDMENT 47
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

      REPORT by Arlene McCarthy                                                   A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


           Commission proposal                                Amendments by Parliament

                                        Amendment 47
                                  Article 4, paragraph 1a (new)

                                                   1a. Member States shall ensure that
                                                   computer implemented solutions to
                                                   technical problems are not considered to
                                                   be patentable inventions when they only
                                                   improve efficiency in the use of resources
                                                   within the data processing system.




                                                                                          Or. en





                                                                                  PE 333.851/ 47

EN                                                                                                  EN


 18 September 2003                                                                       A5-0238/ 48

 AMENDMENT 48
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                       A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                     Amendments by Parliament

                                          Amendment 48
                                   Article 4, paragraph 1b (new)

                                                      1b. Member States shall ensure that it is a
                                                      condition of constituting an invention in
                                                      the sense of patent law that an innovation,
                                                      regardless of whether it involves the use
                                                      of a computer or not, must be of technical
                                                      character.


                                            Justification

 Non-obviousness (_ "inventive step") and the presence of a technical invention (= "technical
 contribution") are two separate requirements. Merging them into one is counter-intuitive and
 leads to practical problems, among others that the invention needn't be new and that patent
 offices are no longer entitled to reject patents on non-inventions without first conducting a
 wasteful prior art search.

                                                                                                Or. en





                                                                                      PE 333.851/ 48

EN                                                                                                        EN


 18 September 2003                                                                     A5-0238/ 49

 AMENDMENT 49
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

      REPORT by Arlene McCarthy                                                      A5-0238/2003
      Patentability of computer-implemented inventions

      Proposal for a directive
      (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                 Amendments by Parliament

                                         Amendment 49
                                         Article 5a (new)

                                                                      Article 5a
                                                     Member States shall ensure that a
                                                     computerised invention may be claimed as
                                                     a product, that is a set of devices
                                                     connected to a data processing system, or
                                                     as a process carried out by such devices.

                                           Justification

 This article explains the meaning of the terms "product" and "process" in the context of
 computerised inventions. The original version interprets both terms correctly but has an
 undesirable side-effect: it suggests that algorithms framed in terms of generic computing
 equipment (programs for computers as such) are or can be "inventions". The amendment
 corrects the error. The inventive products and processes are characterised not by the data
 processing system but by the peripheral devices, which could e.g. be an automobile brake, a
 rubber-curing furnace or a washing machine.

                                                                                              Or. en





                                                                                    PE 333.851/ 49

EN                                                                                                      EN


 18 September 2003                                                                     A5-0238/ 50

 AMENDMENT 50
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                     A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


            Commission proposal                                  Amendments by Parliament

                                          Amendment 50
                                         Article 6a (new)

                                                                       Article 6a
                                                      Member States shall ensure that,
                                                      wherever the use of a patented technique
                                                      is needed for the sole purpose of ensuring
                                                      conversion between the conventions used
                                                      in two different data processing systems
                                                      so as to allow communication and
                                                      exchange of data content between them,
                                                      such use is not considered to be a patent
                                                      infringement.

                                            Justification

 The possibility of connecting equipment so as to make them interoperable is a way of ensuring
 open networks and avoiding abuse of dominant positions. This has been specifically ruled in the
 case law of the Court of .Justice of the European Communities in particular. Patent law should
 not make it possible to override this principle at the expense of free competition and users.

                                                                                             Or. en





                                                                                     PE 333.851/ 50

EN                                                                                                     EN


 18 September 2003                                                                   A5-0238/ 51

 AMENDMENT 51
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                   A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))


             Commission proposal                               Amendments by Parliament

                                         Amendment 51
                                        Recital 11a (new)

                                                    (11a) While computer programs are
                                                    abstract and do not belong to any
                                                    particular field, they are used to describe
                                                    and control processes in all fields of
                                                    applied natural and social science.

                                          Justification

 The Commission text declares computer programs to be technical inventions. It removes the
 independent requirement of invention ("technical contribution") and merges it into the
 requirement of non-obviousness ("inventive step"). This leads to theoretical inconsistency and
 undesirable practical consequences, as explained in detail in the justification of our amendment
 to 4(2).

                                                                                              Or. en





                                                                                   PE 333.851/ 51

EN                                                                                                      EN


 18 September 2003                                                                  A5-0238/ 52

 AMENDMENT 52
 tabled by Brian Crowley and Jos� Ribeiro e Castro, on behalf of the UEN Group

       REPORT by Arlene McCarthy                                                  A5-0238/2003
       Patentability of computer-implemented inventions

       Proposal for a directive
       (COM(2002) 92 � C5-0082/2002 � 2002/0047(COD))

 _____________________________________________________________________________

 Draft legislative resolution

 1.     Rejects the Commission proposal (deletion);

                                                                                         Or. en





                                                                                  PE 333.851/ 52

EN                                                                                                 EN


