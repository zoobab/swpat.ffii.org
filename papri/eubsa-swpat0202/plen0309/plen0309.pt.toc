\select@language {portuguese}
\contentsline {chapter}{\numberline {1}Legenda e Exemplo Comentado}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}T\'{\i }tulo}{4}{section.1.1}
\contentsline {chapter}{\numberline {2}Artigos e Mo\c {c}\~{o}es}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Article 1}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Article 2 (a)}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Article 2 (b)}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Article 2 (ba) (new)}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Article 2 (bb) (new)}{8}{section.2.5}
\contentsline {section}{\numberline {2.6}Article 2 (bc) (new)}{9}{section.2.6}
\contentsline {section}{\numberline {2.7}Article 3}{9}{section.2.7}
\contentsline {section}{\numberline {2.8}Article 3 (a) (new)}{9}{section.2.8}
\contentsline {section}{\numberline {2.9}Article 4}{10}{section.2.9}
\contentsline {section}{\numberline {2.10}Article 4.1}{11}{section.2.10}
\contentsline {section}{\numberline {2.11}}{11}{section.2.11}
\contentsline {section}{\numberline {2.12}Article 4.1 (b) (new)}{12}{section.2.12}
\contentsline {section}{\numberline {2.13}Article 4.2}{12}{section.2.13}
\contentsline {section}{\numberline {2.14}Article 4.3}{12}{section.2.14}
\contentsline {section}{\numberline {2.15}Article 4.3 (a) (new)}{14}{section.2.15}
\contentsline {section}{\numberline {2.16}Article 4 (a) (new)}{14}{section.2.16}
\contentsline {section}{\numberline {2.17}Article 4.4 (b) (new)}{15}{section.2.17}
\contentsline {section}{\numberline {2.18}Article 4 (c) (new)}{15}{section.2.18}
\contentsline {section}{\numberline {2.19}Article 4.4 (d) (new)}{16}{section.2.19}
\contentsline {section}{\numberline {2.20}Article 5}{16}{section.2.20}
\contentsline {section}{\numberline {2.21}Article 5 (a) (new)}{17}{section.2.21}
\contentsline {section}{\numberline {2.22}Article 5.1a}{18}{section.2.22}
\contentsline {section}{\numberline {2.23}Article 5 (b) (new)}{19}{section.2.23}
\contentsline {section}{\numberline {2.24}Article 5 (c)}{19}{section.2.24}
\contentsline {section}{\numberline {2.25}Article 5 (d)}{19}{section.2.25}
\contentsline {section}{\numberline {2.26}Article 6}{20}{section.2.26}
\contentsline {section}{\numberline {2.27}Article 6 (a)}{20}{section.2.27}
\contentsline {section}{\numberline {2.28}Article 6 (b)}{22}{section.2.28}
\contentsline {section}{\numberline {2.29}Article 7}{22}{section.2.29}
\contentsline {section}{\numberline {2.30}Article 7 (Patent Insurance Extensions)}{23}{section.2.30}
\contentsline {section}{\numberline {2.31}Article 8}{24}{section.2.31}
\contentsline {chapter}{\numberline {3}Rejei\c {c}\~{a}o/Aceita\c {c}\~{a}o, T\'{\i }tulo e Considerandos}{26}{chapter.3}
\contentsline {section}{\numberline {3.1}Rejection/Acceptance}{26}{section.3.1}
\contentsline {section}{\numberline {3.2}T\'{\i }tulo}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Recital 1}{27}{section.3.3}
\contentsline {section}{\numberline {3.4}Recital 5}{27}{section.3.4}
\contentsline {section}{\numberline {3.5}Recital 5 (a) (new)}{28}{section.3.5}
\contentsline {section}{\numberline {3.6}Recital 6}{28}{section.3.6}
\contentsline {section}{\numberline {3.7}Recital 7}{29}{section.3.7}
\contentsline {section}{\numberline {3.8}Recital 7 (a) (new)}{30}{section.3.8}
\contentsline {section}{\numberline {3.9}Recital 7 (b) (new)}{30}{section.3.9}
\contentsline {section}{\numberline {3.10}Recital 9 (a) (new)}{31}{section.3.10}
\contentsline {section}{\numberline {3.11}Recital 11}{31}{section.3.11}
\contentsline {section}{\numberline {3.12}Recital 11 (a) (new)}{32}{section.3.12}
\contentsline {section}{\numberline {3.13}Recital 11 (b) (new)}{32}{section.3.13}
\contentsline {section}{\numberline {3.14}Recital 12}{32}{section.3.14}
\contentsline {section}{\numberline {3.15}Recital 13}{33}{section.3.15}
\contentsline {section}{\numberline {3.16}Recital 13a}{34}{section.3.16}
\contentsline {section}{\numberline {3.17}Recital 13b}{34}{section.3.17}
\contentsline {section}{\numberline {3.18}}{34}{section.3.18}
\contentsline {section}{\numberline {3.19}Recital 13d}{35}{section.3.19}
\contentsline {section}{\numberline {3.20}Recital 14}{35}{section.3.20}
\contentsline {section}{\numberline {3.21}Recital 16}{36}{section.3.21}
\contentsline {section}{\numberline {3.22}Recital 17}{37}{section.3.22}
\contentsline {section}{\numberline {3.23}Recital 18}{38}{section.3.23}
\contentsline {section}{\numberline {3.24}Recital 18 bis (new)}{38}{section.3.24}
\contentsline {section}{\numberline {3.25}Recital 18 ter (new)}{39}{section.3.25}
\contentsline {chapter}{\numberline {4}Links Anotados}{40}{chapter.4}
\contentsline {chapter}{\numberline {5}Perguntas, Coisas a Fazer, Como pode Ajudar}{42}{chapter.5}
