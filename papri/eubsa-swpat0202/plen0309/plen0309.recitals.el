# -*- coding: utf-8 -*-

(mlhtdoc 'plen0309
(ML title "Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits" (de "Europarl 2003/09 Softwarepatent-Änderungsanträge: Echte und Scheinbare Begrenzungen"))
(ML descr "The European Parliament is scheduled to decide about the Software Patent Directive on September 24th.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all %(q:computer-implemented) problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.")
nil
(brivla (
  (ML delet "deleted")
)
)

(sects
(tab (ML Ces "Tabular Comparison of Amendment Proposals, Voting Recommendations and Results")

(sects
(legen (ML Lgn "Legend and Commented Example")

(dl
(l "nr" (ML enm "Amendment number (for reference purposes in this document, real tabled amendment numbers will likely be different)"))
(l "+, -, o" (ML jac "pro, contra, undecided"))
(l "21:9" (ML tuW "21 voted in favour, 9 voted against"))
)

(flet
((xpl (a b) (al a (sqbrace (italic b)))))
(amends-articles
(exem
 (ML Tit "Title")
 (xpl
  (ML ehr "Sample Commission text: Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions")
  (fil ((op ah 'eubsa-swpat0202)) (ML trW "text of the %(op:original proposal of European Commission)")) )
 (xpl
  (fil ((dg ah 'dgiv0206)) (ML ung "Sample justification/comments: The criticism of Manders and Doorn is justified. It resonates with the view of the %(dg:study of Bakels and Hugenholtz), commissioned by DG IV, as well as that of many leading law experts, that this directive creates a legal mess. Yet a regulation is not necessarily better."))
  (ML mFo "Comment of FFII/Eurolinux") )
 (l (xpl "13" (ML enm2 "Amendment number"))
    (xpl "tman" (ML tva "Amendment tabled by Toine Manders MEP"))
    (xpl "-" (ML amd "unfavorable recommendation by FFII/Eurolinux"))
    (xpl "214:412" (ML tva "214 voted in favour, 412 against"))
    (xpl (ML ute "Sample Amendment: Proposal for a Regulation of the European Parliament and of the Council on the patentability of computer-implemented inventions") (ML xae "text of sample amendment"))
    (xpl (ML pts "Regulation inappropriate where fundamental rights and major economic policy decisions are affected.")
	 (ML lsm "Simple analysis of the amendment")
	 )
    )
)
) )
)

(recit (ML eia "Rejection/Acceptance, Title and Recitals") 

(ML Wsl "In the following we comment on each amendment under discussion at the parliament we know about, and for which we have permission to disclose them.")


(amends-articles
(tit
 (ML Rej "Rejection/Acceptance")
 (ML app "approves the proposed directive")
 (ML dWt "There will be enough tabled amendments to correct the directive.  If the right amendments are adopted, the directive could be beneficial for the EU. So adopting a thoroughly corrected directive would be better than rejection. On the other hand, amending so completely a directive is a complex task, specially in plenary, and the European Parliament may fail in fixing the abundant assumptions and misconceptions introduced by the Commission after an obscure procedure that hasn't taken into account most interested parties, academic studies or evidences. So rejecting the text and asking the Commission to start over with due process would also be justified. In fact the number of mistakes needing correction is too large to rely on the results of plenary vote for a good directive, so rejection is probably wiser, given the risk to EU competitivity, progress and freedom if the amending partially fails. This recommendation is subject to change according to perceived balance of forces in the EP. Please stay tuned (refresh this page shortly before the vote).")
 (l "68, 54, 52" "EDD, Greens+GUE+..., UEN" "o" "0:0" (ML ute "rejects the proposed directive") (ML aes "Probably better at this stage"))
)

(tit
 (ML Tit "Title")
 (ML ehr "Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions")
 (ML ung " It can not be the aim of the current directive to declare all kinds of %(q:computer-implemented) ideas to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to automatic data processing and its various (technical and non-technical) fields of application, and this must be expressed in the title in plain and unambiguous wording.")
 (l "29,59,41" "EDD,Greens+GUE+...,UEN" "++" "0:0" (ML ute "Proposal for a Directive of the European Parliament and of the Council on the limits of patentability with respect to automated data processing and its fields of application"))
)

(rec1
 (bridi 'RecX 1)
 (ML too "The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field. ")
 (fil ((ss ah 'swpatsisku) (bh ah " http://www.researchoninnovation.org/swpat.pdf")) (ML Wnl "The Commission boldly postulates, against all %(ss:economic evidence), including %(bh:very detailed empirical research by Bessen & Hunt from 2003), that patents encourage investments in software develoment.  Moreover, the term %(q:computer-implemented invention) is confusing.  It is better to have no target statement than a wrong one."))
 (l "2"  "EDD" "+" "0:0" (pe (bridi 'delet)))
 (l "1" "JURI" "-" "0:0" (ML oet "The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field. "))
)

(rec5
 (bridi 'RecX 5)
 (ML tpm "Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.")
 (ML Wrs "Both the European Commission (CEC) and Amendment 2 (JURI) say dogmatically that software patents stimulate innovation, while the policy goal should to stimulate innovation and patentability should be measured by that goal. Amendment 2 seems to claim that the mere fact of writing EU law, no matter what the contents, already %(q:secures legal certainty), simply because the Luxemburg court can interpret this law.  This new goal statement is not only questionable but also pointless, because %(ol|the Community Patent is under way anyway|there are easier and more systematic ways to put the Luxemburg court in charge than by passing an interpretative directive for one aspect of the European Patent Convention)")
 (l "2" "JURI" "-" "0:0" (lin (ML anl "Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.") (ML css "Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.") ) )
)

(rec5a
 (bridi 'RecX "5 (a) (new)")
 nil
(ML Wrs "We agree that the rules laid down in Article 52 EPC have been stretched beyond recognition by the EPO and that the original meaning, as described in the 1978 EPO examination guidelines, should be reconfirmed.")
 (l "88" "PPE-DE" "+" "0:0" (lin (ML anl "The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be
confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.") ) )
)


(rec6
 (bridi 'RecX 6)
 (ML tpm "The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.")
 (fil (bh ah "http://www.firstmonday.dk/issues/issue8_3/kahin/index.html") (ML Wrs "We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it excludes business method patents, which the US government wants to mandate by the new Substantive Patent Law Treaty draft, see %(bh:Brian Kahin article in First Monday). Amendment 31 deletes the factually incorrect article."))
 (l "31" "EDD" "+" "0:0" (pe (bridi 'delet)) (ML oaW "Deletion is good, but clarification would have been better."))
# comments in case it is tabled after all: , while 4 amends it to clarify the relation between TRIPs and this directive. Given that the TRIPs agreement is often postulated as one of the reasons why software should be patentable, we prefer amendment 4.
# Problem: NGL didn't table it??? (l "4" "?" "+" "0:0" (ML esW "The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are susceptible to industrial application. Moreover, according to TRIPS, patent rights should be available without discrimination as to the field of technology. This means that patentability must be effectively limited in terms of general concepts such as %(q:invention), %(q:technology) and %(q:industry), so as to avoid both unsystematic exceptions and unoverseeable extensions, both of which would act as barriers to free trade. Thus inventions in all fields of applied natural science are patentable, whereas innovations in fields such as mathematics, data processing and organisational logic are not patentable, regardless of whether a computer is used for their implementation or not.") (ML tat "Interpreting Art 27(1) TRIPs is one of the main clarificatory tasks for which a directive might be useful."))
)

(rec7
 (bridi 'RecX 7)
 (ML nph "Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology. ")
 (ML eug "Art 52 EPC says that programs for computers etc are not %(e:inventions) in the sense of patent law, i.e. that a system consisting of generic computing hardware and some combination of calculation rules operating on it can not form the object of a patent. It does not say that such systems can be patented by declaring them to be %(q:not as such) or %(q:technical). Amendment 5 fixes this bug by reconfirming Art 52 EPC (as suggested in Amendment 88 to Recital 5 (a) (new) above). Note that the exclusion of programs for computers is not an exception, it is part of the rule for defining what an %(q:invention) is.")

 (l "32,112" "EDD,Kauppi" "+" "0:0" (ML WaW "Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology."))
)


(rec7a
 (bridi 'RecX "7 (a) (new)")
 nil
 (ML aod "This amendment tries to redress some potential uncertainity in vague wording in the CEC proposal by recognizing the validity of the EPC in case any provision in the directive would contradict it, thus making the coexistence of both legislations less uncertain.") 
 (l "3" "JURI" "+" "0:0" (ML ste "The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions."))
)

(rec7b
 (bridi 'RecX "7 (b) (new)")
 nil
 (ML aoe "New resolution calling for greater accountability of EPO. The effect would be stronger if it was an article, but it is still a good principle to have. It is important to state all sources of conflicts in order to ease eventual solutions.")
 (l "95" "ELDR" "+" "0:0" (ML Phr "Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeÕs operating rules to ensure that it was publicly accountable in the exercise of its functions. "))
)

# not tabled? (NGL/GUE)
#(rec8
# (bridi 'RecX 8)
# (ML ane "Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.")
# (ML tpe "Dogmatic statements about the beneficiality of patents need to be replaced by a statement which takes the various possible effects of patents into account.  The Commission's statement implies that the rules for granting and enforcing patents need not be designed to take anti-competitive effects into consideration at all.  Rather, patent owners %(q:should) behave well, and if they fail to heed this empty exhortation, patent law may not be blamed.  Such irresponsible patent owner thinking must not be written into a law.")
# (l "6" "edd?" "+" "0:0" (ML WiW "Patents are temporary exclusion rights granted by the state to inventors in order to stimulate technical progress. In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed. In particular, inevitable corollaries of the patent system such as restriction of creative freedom, legal insecurity and anti-competitive effects must be kept within reasonable limits.") (ML shr "This sets things straight."))
#)

#(rec9
# (bridi 'RecX "9")
# (ML iaw "In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright. ")
# (ML cdn "Copyright does not only apply to literary works, but also to textbooks, operation manuals, computer programs and all kinds of information structures. Copyright is the system of %(q:intellectual property) for computer programs, not only a system for a %(q:literary) side aspect of computer programs. If copyright does not cover the %(q:underlying idea) of a book or a program then that is not an indication of an insufficiency of copyright but rather an indication of the need to keep %(q:underlying ideas) (general concepts) free, so that many different creators have a chance to obtain property in individual works based on these general concepts.")
# (l "7" "?" "+" "0:0" (ML WiW "In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, property in computer programs is acquired by copyright. General ideas and principles which underlie a computer program must stay freely usable, so that many different creators may simultaneously obtain property in individual creations based thereon."))
#)

(rec9a
 (bridi 'RecX "9 (a) (new)")
 nil
 (ML cdn "Software patents would be bad for all software developers who aren't backed by a big company with a lot of money and lawyers, and this obviously includes a number of Free Software developers.")
 (l "61" "GUE" "+" "0:0" (ML WiW "Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion."))
)


# not tabled? (NGL)
#(rec10
# (bridi 'RecX 10)
# (ML iof "In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology. ")
# (ML ioa "The Commission text is not in line with Art 52 EPC. Art 52(2) EPC lists examples of non-inventions. It is not permissible to subsume these under %(q:inventions) and then test their technical character. Moreover, while it can not be inferred from Art 52 EPC that all technical innovations are inventions, it can, based on a unanimous tradition of patent law, be assumed that all inventions have technical character.")
# (l "8" "?" "+" "0:0" (ML WiW "In order for any innovation to be considered a patentable invention it should have a technical character, and thus belong to a field of technology."))
#)


(rec11
 (bridj 'RecX 11)
 (ML eWe "Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art. ")
 (fil ((se ahi 'separ)) (ML nry "The four patentibility tests laid out in Art 52 EPC state that there must be a (technical) invention and that additionally, this invention must be usceptible of industrial application, new and involve an inventive step. The %(q:inventive step) conditions is defined in Art 56 EPC as requiring the invention not to be an obvious combination of known techniques to a person skilled in the art. Additionally, %(q:invention) and %(q:technical contribution) are synonyms: you can only invent new things, and this invention is your (technical) contribution to the state of the art. As such, the CEC originl and amendments 4 and 84 are in direct contradiction with the EPC. Additionally, such a logic assures that patentability is simply a question of claim wording, see the section on %(se:Four Separate Tests on One Unified Object).  In order to avoid confusion and conflicts with EPC Art 52 and 56, the recital needs to be rewritten or deleted.")) 
 (l "4" "JURI" "-" "0:0"  (ML ews "In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art."))
 (l "33" "EDD" "++" "0:0" (pe (bridi 'delet)))
 (l "84" "PPE-DE" "-" "0:0" (ML Wba "In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software."))
)


(rec11a
 (bridk 'RecX "11 (a)(new)")
 nil
 (ML tjW "The Commission text declares computer programs to be technical inventions. It removes the independent requirement of invention (= %(q:technical contribution)) and merges it into the requirement of non-obviousness (= %q(inventive step), see comments for Recital 11). This leads to theoretical inconsistency and undesirable practical consequences.")
 (l "51" "UEN" "+" "0:0" (ML toc "While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science. "))
)


(rec11b
 (bridl 'RecX "11 (b)(new)")
 nil
 (ML tjW "Amendment 28 would be a perfect restatement of Art 52 EPC if it had said %(q:inventions) instead of %(q:computer-implemented inventions). As it is now, it looks as if it was a special rule for inventions involving computers.   Yet it is useful in that reinforces the notion that the same invention must pass the four tests.  See Article 1 for why the term %(q:computer-implemented invention) is misleading.")
 (l "73" "PSE" "o" "0:0" (ML toc "Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application.. "))
)


(rec12
 (bridi 'RecX 12)
 (ML ola "Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. ")
 (ML tjW "The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This contradicts Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible. Again, see the explanation in Recital 11 for more information. Amendment 5 by JURI makes matters even worse: first of all it literally reintroduces article 3 (which it deleted), secondly it claims the %(q:inventive step) requirement is something completely different from what Art 56 EPC says, thirdly it starts talking about a %(q:technical problem) which should be solved (while patentability has nothing to do with the kind of problem that is solved, but instead on the solution employed) and finally it also repeats the mistakes of the CEC original. ")
 (l "5" "JURI" "--" "0:0"  (ML Wio "Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art. "))
 (l "114" "Kauppi" "++" "0:0" (ML toc "Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law."))
)

(rec13
 (bridi 'RecX 13)
 (ML mnt "A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.")
 (ML gin "The CEC recital pretends to exclude algorithms but in fact makes algorithms patentable, because an algorithm which is defined with reference to the environment of generic data processing equipment (= %(q:a physical environment)) is exactly the same as the algorithm per se. It is absurd and contrary to the goal of clarification to try to make an artificial distinction between mathematical algorithms and nonmathematical algorithms. To a computer scientist, this makes no sense, because every algorithm is as mathematical as anything could be. An algorithm is an abstract concept unrelated to physical laws of the universe.")
 (l "34,114" "EDD,Kauppi" "+" "0:0" (pe (bridi 'delet)))
)


(rec13aa
 (ML ea32 "recital 13 a")
 nil
 (ML dad "Syntactical ambiguity in this amendment leaves it unclear whether %(q:business method) in general are considered to be a %(q:methods in which the only contribution to the state of the art is non-technical) or whether certain business methods could contain %(q:technical contributions).  More importantly, this amendment will not really exclude anything, because patent lawyers will be quick to claim that some computer-related features are characteristic for the %(q:invention), which, as stated elsewhere in this directive proposal, must be %(q:considered as a whole).  For this to be of any use, the syntactical ambiguity around %(q:other method) would have to be removed and the category of %(q:non-technical methods) would have to be explained by definitions and/or examples.")
 (l "6" "JURI" "o" "0:0"  (ML tta "However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention. "))
)


(rec13b
 (ML ea33 "recital 13 b")
 nil
 (ML hcp "This states a common sense position which has often been disregarded in order to extend patentability.  This alone does not achieve much and the statement in the law that the law %(q:can not be circumvented) is a pious wish.  Yet, even a wish is better than nothing.")
 (l "7" "JURI" "+" "0:0"  (ML eWe2 "If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims."))
)

(rec13c
 (ML ea34 "recital 13 c") 
 nil
 (ML toW "Placed in the context of patent examination practise, this statement is not what it seems.  It widens patentability even compared to EPO practise by allowing patentability of non-technical solutions to technical problems.  Moreover, it makes all algorithms patentable in all situations, because a %(q:technical problem) can always be defined in terms of generic computing hardware, which at the same time provides a way of claiming algorithms in their most abstract form.")
 (l "8" "JURI" "-" "0:0"  (ML nWe "Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent."))
)


(recital13d
 (ML ea35 "recital 13 d")
 nil
 (al
  (ML Waf "The amendment simply restates the definition of claims: claims define the exclusion right granted by the patent. It allows claims to software by merely refering to generic computer equipment or computer processes, regardless of where the innovation lies. It's similar to saying: you can claim what you please as long as you use vocabulary like storage, processor, or apparatus somewhere in the claims, but be careful to make your claim as broad as you want it, because you won't monopolise what you didn't claim.")
  (ML Wns "The amendment may have an indirect benefit: it seems to contradict amendment 18 to article 5 by saying that only programmed apparatuses and processes can be claimed.") )
 (l "9" "JURI" "o" "0:0" (ML fsn "The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement."))


(rec14
 (bridi 'RecX 14) 
 (ML nnW "The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive. ")
 (fil ((a6 ah 'epo-tws-app6))
   (al
     (ML tdw "The CEC recital is not too bad (although it uses the %(q:computer-implemented inventions) term, see Article 1). It simply says patent law should not be replaced.  Amendment 10 is less clear.  On the one hand it equates %(q:present legal position) (?) with %(q:practices of the European Patent Office). On the other it says that the patentability of business methods is to be avoided. However the EPO itself has been granting many patents for business method and has provided %(a6:legal reasoning to support business method patents).  The only possible effect of the amendment, if any, would be to surrender legislative responsability to the EPO, whose practices would seem to define patentability instead of abiding to the laws voted by the European Parliament and other democraticaly representative bodies.")
     (l "Amendment 84 does not ask for a codification of the current EPO practice, but on the other hand talks about %q(unpatentable methods such as trivial procedures and business methods). Not just trivial business methods must remain unpatentable, but all business methods must remain so (otherwise we are deviating from Art 52 EPC, which should be avoided as suggested in amendment 88).")
     )
    )
 (l "10" "JURI" "--" "0:0"  (ML tnt "The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods."))
 (l "84" "PPE-DE" "-" "0:0"  (ML tnt "The legal protection of computerimplemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods."))
)



(rec16
 (bridi 'RecX 16)
 (ML nni "The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent. ")
  (ML sed "Unification of caselaw in itself is not a guarantee of improvement of the situation of European industry. This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries. Whether or not software patents are good for European software development companies is independent of the fact that the traditional manufacturing industry is moving to low-cost economies outside the EU. Amendment 11 and the CEC text assume that extending patent protection to the software development industry will improve the competitiveness of EU companies, even though 75% of the 30,000 already granted software patents (which CEC/JURI call patents on %(q:computer-implemented inventions)) are in hands of US and Japanese companies. Additionally, if Europe does no have software patents, European companies still can perfectly acquire them in the US and elsewhere and enforce them there, while the affected foreign companies cannot do the same here. In that sense, not having software patents in Europe is a competitive advantage")
 (l "11" "JURI" "-" "0:0" (ML rst "The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident."))
 (l "16" "EDD" "+" "0:0" (ML jue "The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated."))
)

(rec16
 (bridi 'RecX 16)
 (ML nni "The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent. ")
  (ML sed "Unification of caselaw in itself is not a guarantee of improvement of the situation of European industry. This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries. Whether or not software patents are good for European software development companies is independent of the fact that the traditional manufacturing industry is moving to low-cost economies outside the EU. Amendment 11 and the CEC text assume that extending patent protection to the software development industry will improve the competitiveness of EU companies, even though 75% of the 30,000 already granted software patents (which CEC/JURI call patents on %(q:computer-implemented inventions)) are in hands of US and Japanese companies. Additionally, if Europe does no have software patents, European companies still can perfectly acquire them in the US and elsewhere and enforce them there, while the affected foreign companies cannot do the same here. In that sense, not having software patents in Europe is a competitive advantage")
 (l "11" "JURI" "-" "0:0" (ML rst "The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident."))
 (l "16" "EDD" "+" "0:0" (ML jue "The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated."))
)

(rec17
 (bridi 'RecX 17)
 nil
 nil
 (l "12" "amccarthy" "o" "+" "+" "22:6"  (ML lWs "This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty."))
)



(rec18
 (bridi 'RecX 18)
 (ML aiW2 "Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive. ")
 (ML eWa "The CEC proposal only pretends to protect interoperability, but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendment 13 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.")
 (l "13" "JURI" "o" "0:0" (ML t5t "The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program."))
)

(rec18b
 (bridi 'RecX "18 bis (new)")
 nil
 (ML eWa "This recital suggests that computer programs can be an invention (since there are no other innovations which have source code or which you can decompile), which contradicts article 52 EPC, since that one explicitly excludes computer programs from patentability. Software can never be part of an invention. It can be mentioned in the claims of the patent, but in that case it merely serves to clarify the application of the actual invention and to limit the scope of the monopoly that the patent grants to its holder (i.e., if someone manages to make use of the invention without using a computer program, he will not infringe on a patent that only describes the use of said invention in combination with a computer program).")
 (l "74" "PSE" "-" "0:0" (ML t5t "(Warning: unofficial translation from Dutch version, English version not yet available at publication time) The application of this directive must not deviate from the original foundations of patent law, which means that the applicant of the patent must provide a description of all elements of the invention, including the source code, and that research into it, and as such decompilation, must be made possible. This way of working is indispensable to make compulsory licensing possible, for example if the obligation to supply the market is not fulfilled."))
)


(rec18t
 (bridi 'RecX "18 ter (new)")
 nil
 (ML eWa "This recital also suggests that computer programs can be inventions.")
 (l "75" "PSE" "-" "0:0" (ML t5t "(Warning: unofficial translation from Dutch version, English version not yet available at publication time) In any case, the law of all member states must ensure that the patents contain novelties and contain an inventive step, to prevent inventions which are already public from being appropriated, simply because they belong to a computer program."))
)


)
)

(artik (ML ril "Articles and Motions")

(amends-articles
(art1
 (bridi 'ArtX "1")
 (ML vtm "This Directive lays down rules for the patentability of computer-implemented inventions.")
 (fil ((e6 ah 'epo-tws-app6)) (ML nWw "Ideas implemented in generic computer hardware are not inventions in the sense of Art 52(2) EPC. The term %(q:computer-implemented invention) is not known to the man skilled in the art. It was introduced by the EPO in 2000 in the infamous %(e6:Appendix 6 of the Trilateral Website document) in order to bring European patent law in line with US and JP and to allow patents for %(q:computer-implemented business methods), in anticipation of a deletion of Art 52(2) EPC."))
 (l "13" "?" "++" "0:0" (ML ste2 "This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs."))
)

(art2a
 (bridi 'ArtX "2 (a)")
 (ML aga "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;")
 (ML apt "Art 52 of the EPC clearly states that a stand-alone computer program (or a %(q:computer program as such) cannot constitute a patentable invention. This amendment clarifies that an innovation is only patentable if conforms to Art 52 of the EPC, regardless of whether or not a computer program is part of its implementation.")
 (l "14" "?" "+++" "0:0" (bold (ML Wtr "(a) %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;")) )
)

(art2b (bridi 'ArtX "2 (b)")
 (ML bWo "%(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.")
 (al (ML ytW "A directive which makes patentability hinge on the word %(q:technical contribution) must explain this term clearly. Unfortunately none of the amendments fixes the notional chaos which is introduced by mixing the %(q:technical contribution) (= invention) requirement with the novelty and non-obviousness requirements. All thereby deviate from Art 52 EPC.")
     (ML Wni "ITRE-6 seems to imply that only the problem but not the solution needs to be technical. The word %(q:significant) is merely another indeterminate pious wish and therefore conductive to confusion rather than clarification. Obviously %(q:technical) needs to be clearly defined if this is to have any useful effect. Requiring a technical problem to be solved is as bad as CEC or worse.  We should not focus on the application (the problem solved) but the solution (the result of empirical research that may merit a patent).  Any software can be said to solve a technical problem, but if the solution only innovates in software, it cannot be said to be a  technical solution (at least not a new technical solution). If you take known physical means and use them as peripheral equipment to a computer, you can solve all sorts of technical problems by programming, and that does not make those solutions inventions, or any teaching they contribute a technical contribution (the last 2 phrases are synonyms, invention = technical contribution).") )
 (l "ITRE-6" "ITRE" "-" "0:0" (ML eoi "(b) %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art. "))
)

(art2ba
 (bridi 'ArtX "2 (ba) (new)")
 nil
 (ML mca "Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendment 15 and JURI-45, which is found in various national caselaws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and empistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 45 is apparently the result of mistranslation from French.  It should be %(q:digital). Unfortunately, am 15 (below) and JURI-45 conflict. A split vote on JURI-45 should be avoided, as the %(q:<<technical>> means belonging to a technical field) part does not change anything on its own.")
 (l "JURI-45" "JURI" "++" "0:0" (bold (ML Wct "(ba) %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the numerical representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.")))
)


(art2bb
 (bridi 'ArtX "2 (bb) (new)")
 nil
 (ML mcb "The Commission text shift the question of what is a patentable invention to the question of what is %(q:technical) and then leaves that question unanswered.  The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day caselaw and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification. The definition given here is founded in generally acknowledged concepts of the philosophy of science (empirically observable %(q:4 forces of nature) versus information gained by abstraction therefrom, %(q:controllable) meaning that deterministic control must be possible, which excludes social and aesthetical phenomena).  It has been in wide use throughout Europe, East Asia and other continents.  In the wake of recent extensions of patentability the definition has been cited less often, but it still constitutes the implicit or explicit understanding of what %(q:technology) means, even in a large part of the EPO caselaw.")
 (l "15" "edd?" "++" "0:0" (ML Wct "(bb) %(q:technology) in the sense of patent law means %(q:applied natural science); %(q:technical) in the sense of patent law means %(q:concrete and physical);"))
)


(art2bc
 (bridi 'ArtX "2 (bc) (new)")
 nil
 (ML mcd "We do not want innovations in the %(q:music industry) or %(q:legal services industry) to meet the TRIPs requirement of %(q:industrial applicability). The word %(q:industry) is nowadays often used in extended meanings which are not appropriate in the context of patent law.")
 (l "16" "?" "++" "0:0" (bold (l "(bc) %(q:industry) in the sense of patent law means %(q:automated production of material goods);")))
)


(art2bd
 (bridi 'ArtX "2 (bd) (new)")
 nil
 (ML mcc "This is a common EPC doctrine which has been regularly used by most patent courts, including the Technical Chambers of Appeal of the EPO.")
 (l "18" "?" "++" "0:0"
    (al
     (bold (l "(bd) A %(q:calculation rule), also called %(q:algorithm), is a teaching about relations within constructions of the human mind, such as models and axiomatic systems, including formal models of data processing equipment such as the Turing Machine or the Von Neumann Machine."))
     (bold (l "An %(q:organization) rule is a teaching about relations between material phenomena that are not determined by controllable forces of nature. Methods of business and social engineering are organisation rules. Most organisation rules are direct applications of calculation rules to known models of social relations. %(q:Rules of organisation and calculation) are teachings about cause-effect relations that are not determined by controllable forces of nature."))) )
 )

(art3
 (bridi 'ArtX "3")
 (ML aWa "Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.") 
 (ML nas "This article is about the interpretation of Art 27 TRIPs.  It is the single most important principle that needs clarification.  Article 3 should therefore not be deleted but corrected, if this directive is to clarify anything.  Arlene McCarthy correctly criticises this article and proposes its deletion, only to reinsert the same criticised content into recital 12 by amendment JURI-3. The European Commission text says that all ideas, including %(q:computer-implemented business methods) etc, are patentable inventions. ")
 (l "19" "?" "++" "0:0" (bold (ML ieW "Member States shall ensure that an innovation is not considered to belong to a field of technology merely because its implementation involves the use of a computer.")))
)


(art3a
 (bridi 'ArtX "3 (a) (new)")
 nil
 (al
  (ML nbs "One of the chief objectives of the directive project has been to clarify the interpretation of Art 52 EPC in the light of Art 27 TRIPs, which says that %(q:inventions all fields of technology) must be patentable. We propose to translate of Art 52(2) EPC into the language of Art 27 TRIPs, along the lines of Amendment CULT-16.")
  (ML oaf "If data processing is %(q:technical), then anything is %(q:technical).")
  (ML oWW "Data processing is a common denominator of all fields of technology and non-technology. With the advent of the universal computer in the 1950s, automated data processing (ADP) became pervasive in society and industry.")
  (ML bsi "As Gert Kolle, a leading theoretician behind the decisions of the 1970s to exclude software from patentability, writes in 1977 (see Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):")
  (bc (ML WWr "ADP has today become an indispensable auxiliary tool in all domains of human society and will remain so in the future. It is ubiquitous. ... Its instrumental meaning, its auxiliary and ancillary function distinguish ADP from the ... individual fields of technology and liken it to such areas as enterprise administration, whose work results and methods ... are needed by all enterprises and for which therefore prima facie a need to assure free availability is indicated."))
  (ML dnW "CULT was well-advised to vote for a clear exclusion of data processing from the scope of %(q:technology).")
  (ML yeg "Numerous studies, some of them conducted by EU instiutions as well as the opinions of the European Economic and Social Committee and the European Comittee of Regions explain in detail why Europe's economy will suffer damage, if data processing innovations are not clearly excluded from patentability.") )
 (l "19" "?" "++" "0:0" (bold (ML ieW "Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.")))
)


(art41
 (bridi 'ArtX "4.1")
 (ML WoW2 "Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.")
 (ML aWp "%(q:Computer-implemented inventions) (i.e. ideas framed in terms of general-purpose data processing equipment = programs for computers) are non-inventions according to Art 52 EPC. Amendment JURI-48 fixes the bug and restates Art 52 EPC.  None of the amendments limit patentability, but nr. JURI-48 at least gets things formally correct. ITRE-8 seems to remove the requirements of novelty and industrial application for computer-implemented inventions, and can only be useful if Art 2(b) is properly amended. ")
 (l "JURI-48" "?" "++" "0:0" (ML nib "Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application."))
 (l "ITRE-8" "?" "-" "0:0" (ML nWt "1. Member States shall ensure that a computer-implemented invention is patentable only on the condition that it makes a technical contribution as defined in Article 2(b). "))
)

(art42
 (bridi 'ArtX "4.2")
 (ML svm2 "Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.")
 (ML u5e "The Commission text deviates from Art 52 EPC by merging the independent requirement of %(q:invention) and %(q:technical character) into a single requirement of %(q:technical contribution) which again is not an independent requirement but somehow subordinate to the requirement of %(q:inventive step) (non-obviousness).  Moreover the Commission text implies that ideas framed in terms of the general purpose computer (programs for computers) are patentable inventions.  Deletion, as proposed by ITRE-9, could be helpful, because the only effect of this paragraph is to confuse patentability tests and thereby make it impossible for national patent offices to reject non-statutory patent applications without substantive examination. Amendment 20 is also good, as it reaffirms the applicability of EPC Art 52.")
 (l "20" "?" "++" "0:0" (ML Wrc "Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Art 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978."))
 (l "ITRE-9" "?" "+" "0:0" (spaced (bridi 'delet) (sqbrace (ML ecn "included in 4.1"))))
)

(art43
 (bridi 'ArtX "4.3")
 (ML eak2 "The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.")
 (al 
  (ML nrh "The European Commission's proposal voids its own concept of %(q:technical contribution) by allowing the contribution to consist of non-technical features, as has been widely criticised in the patent law literature. It is clear that a correction such as that in amendment JURI-52 is necessary, if the concept of %(q:technical contribution) is to be used at all.")
  (fil ((ng ah "http://www.patent.gov.uk/about/consultations/responses/grace/grace.pdf"))
       (ML lWo "The novelty grace period proposed by ITRE is an orthogonal question.  As shown by a %(ng:recent consultation conducted by the UK Patent Office), it is very controversial even among those who are supposed to benefit from it.  It is not clear whether a novelty grace period would in fact, as suggested by the ITRE report, benefit SMEs.  In the case of open source development, it could even cause additional insecurity as to whether published ideas are free from patents.")
  ) )
 (l "JURI-52" "?" "++" "0:0" (ML ien "The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art."))
 (l "ITRE-10" "?"  "-" (ML btW "3. The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim. "))
)

(art5
 (bridi 'ArtX "5")
 (ML ime "Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.")
 (ML xdf "The CEC wording suggests that known general purpose computing hardware and calculation rules executed thereon (= computer programs) are patentable products and processes. Amendment 59 tries to address this problem.  It might be a good idea to replace %(q:technical production processes operated by such a computer) by %(q:the inventive processes running on such a set of equipment) if that is still possible in the form of a %(q:compromise amendment). ITRE-12 defines %(q:computer-implemented invention) as being only a product or a technical production process. As long as %(q:technical) is defined, this is useful .")
 (l "JURI-59" "?" "++" "0:0" (ML nWt2 "Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.") )
 (l "ITRE" "ITRE" "-" "0:0" (ML eea "(a) Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process."))
)

(art5b
 (bridi 'ArtX "5 (b)")
 nil
 (ML Web "Without this amendment, software patent owners could still send cease-and-desist letters to programmers or software distributors accusing them of contributory infringement.  This article makes sure that the right of publication as guaranteed by Art 10 ECHR takes precedence over patents.  On the other hand, this amendment does not prevent the granting of software patents or their enforcement against users of software.  Vendors of proprietary software and commercial Linux distributors could still be under pressure from their customers, who would usually impose a contractual obligation of patent clearance on them.")
 (l "ITRE-13" "ITRE" "++" "0:0" (ML pte "Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.") (ML vto "Not put to vote in JURI because made dependent on adoption of ITRE-12."))
)

(art5c
 (bridi 'ArtX "5 (c)")
 nil
 (ML dWw "The suggested provisions are likely to be redundant, but stating them won't do any harm.  They suggest that only programs related to inventive physical processes are affected, and that these may be run for simulation or other purposes in a generic computing environment where the physical process is not executed.")
 (l "ITRE-14(1)" "?" "+" "0:0" (ML sne "(c) Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.") (ML oWo "Not put to vote in JURI, because made it dependent on non-adoption of COMP-1"))
)

(art5d
 (bridi 'ArtX "5 (d)")
 nil
 (ML reW "These amendments do, unlike some may think, not serve to promote protect free/opensource software, but rather to ensure that the obligation of disclosure which is inherent in the patent system is taken seriously and that software is, like any other information object, on the disclosure side of the patent rather than on its exclusion/monopolisation side. This makes it a little more difficult to block people from doing things you  even haven't done yourself, but which are obviously possible since the computing model is perfectly defined and you always know what you can do with a computer. If you give the source you at least offer some real knowledge on how to solve the problem, unlike when you say that %(q:processor means coupled to input output means so that they compute a function such that the result of said function when output through said output means solves the problem the user wanted to solve).")
 (l "ITRE-14(2)" "?" "+" "0:0" (ML eWe3 "(d) Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.") (ML oWo2 "Not put to vote, because Arlene McCarthy made it dependent on non-adoption of COMP-1."))
)
;amends-articles
)
;artik
)
(links (bridi 'links)
(progn
(putref 'caliu-plen0309
 "http://patents.caliu.info/plen0309.html"
 (xq (ml "Caliu 2003/09: Plenary Amendments"))
 (xq (ML enW "Another version of the publishable amendments, sometimes slightly ahead of this page."))
)
nil
)
(mlht-links
(ah "amend001-028.de.pdf")
(ah "amend029-040.en.pdf")
(ah "amend041-052.en.pdf")
(ah "amend054-054r1.en.pdf")
(ah "amend055-058.en.pdf")
(ah "amend059-062.en.pdf")
(ah "amend068-068.fr.pdf")
(ah "amend069-071.en.pdf")
(ah "amend072-073.en.pdf")
(ah "amend074-075.en.pdf")
(ah "amend076-076.en.pdf")
(ah "amend081-081.fr.pdf")
(ah "amend082-083.en.pdf")
(ah "amend084-088.en.pdf")
(ah "amend089-106.en.pdf")
(l (ah "public.html" (ML Wan "Public Documents Regarding the Plenary Vote"))
   (ML aaW "Amendments tabled by various groups and analyses thereof, as far as they can be published."))
'caliu-plen0309
'eubsa-prop
'eubsa-propmini
'juri0304
'swpat
)
)
)
)
)
)

(mlhtdoc 'plenkond0309
(ML title "Conditions for Rejection")
(ML descr "If these conditions are not fulfilled, the directive should be rejected (i.e. sent back to the Commission or to the Parliament for reworking).")
nil
(linul
(ML dmj "The directive must be rejected")
(tepe (ML eli "if freedom of publication is impaired") (fil ((pc ah 'eubsa-prog) (i3 ah '(anch juri0304 art5))) (ML anW "e.g. by allowing %(pc:program claims) or failing to state clearly that publication of text cannot constitute a patent infringement, as done e.g. in %(i3:ITRE amendment 13)")))
(fil ((a6 ah 'eubsa-itop)) (ML htp "if the right to interoperate (i.e. to convert data between different representation conventions) is impaired, e.g. if %(a6:Art 6a) is weakened."))
(ML dWe "if the directive stops short of excluding algorithms, business methods and programs for computers (= %(q:computer-implemented inventions) = patent claim objects consisting of general purpose data processing equipment and rules for operating it) from patentability.")
(ML Wci "if the directive makes patentability hinge on abstract terms which are open to interpretation by caselaw.  In particular, the word %(q:technical), if used, must be defined by reference to %(q:controllable forces of nature), and the word %(q:industrial), if used, by %(q:production of material goods).")
(tepe (ML deW "if the word %(q:computer-implemented invention) is used in the directive.") (ML weW "This term means nothing but %(q:program for computers in the context of a patent claim).  It was introduced by European Patent Office (EPO) in 2000 in attempt to legitimate %(q:computer-implemented business methods) and bring Europe in line with the %(q:Trilateral Standard) of the US, Japanese and European Patent Office.  Please read the definitions of the European Commission (CEC)'s Article 2 and the EPO' %(tw:Trilateral Document Appendix 6). Don't be misled by secondary information!  A computer-controlled washing machine is %(q:not) a %(q:computer-implemented invention) in the strict sense of the term."))
(ML iep "If the directive text is contradictory, e.g. if the aims laid out in the recitals do not support the means stipulated in the articles.")
)
)


