\contentsline {chapter}{\numberline {1}Legende und Kommentiertes Beispiel}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Titel}{4}{section.1.1}
\contentsline {chapter}{\numberline {2}Artikel und Beschlussantr\"{a}ge}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Artikel 1}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Artikel 2 (a)}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Artikel 2 (b)}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Artikel 2 (ba) (neu)}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Artikel 2 (bb) (neu)}{8}{section.2.5}
\contentsline {section}{\numberline {2.6}Artikel 2 (bc) (neu)}{9}{section.2.6}
\contentsline {section}{\numberline {2.7}Artikel 3}{9}{section.2.7}
\contentsline {section}{\numberline {2.8}Artikel 3 (a) (neu)}{9}{section.2.8}
\contentsline {section}{\numberline {2.9}Artikel 4}{10}{section.2.9}
\contentsline {section}{\numberline {2.10}Artikel 4.1}{11}{section.2.10}
\contentsline {section}{\numberline {2.11}}{11}{section.2.11}
\contentsline {section}{\numberline {2.12}Artikel 4.1 (b) (neu)}{12}{section.2.12}
\contentsline {section}{\numberline {2.13}Artikel 4.2}{12}{section.2.13}
\contentsline {section}{\numberline {2.14}Artikel 4.3}{13}{section.2.14}
\contentsline {section}{\numberline {2.15}Artikel 4.3 (a) (neu)}{14}{section.2.15}
\contentsline {section}{\numberline {2.16}Artikel 4 (a) (neu)}{14}{section.2.16}
\contentsline {section}{\numberline {2.17}Artikel 4.4 (b) (neu)}{15}{section.2.17}
\contentsline {section}{\numberline {2.18}Artikel 4 (c) (neu)}{16}{section.2.18}
\contentsline {section}{\numberline {2.19}Artikel 4.4 (d) (neu)}{16}{section.2.19}
\contentsline {section}{\numberline {2.20}Artikel 5}{17}{section.2.20}
\contentsline {section}{\numberline {2.21}Artikel 5 (a) (neu)}{18}{section.2.21}
\contentsline {section}{\numberline {2.22}Artikel 5.1a}{18}{section.2.22}
\contentsline {section}{\numberline {2.23}Artikel 5 (b) (neu)}{19}{section.2.23}
\contentsline {section}{\numberline {2.24}Artikel 5 (c)}{19}{section.2.24}
\contentsline {section}{\numberline {2.25}Artikel 5 (d)}{20}{section.2.25}
\contentsline {section}{\numberline {2.26}Artikel 6}{20}{section.2.26}
\contentsline {section}{\numberline {2.27}Artikel 6 (a)}{21}{section.2.27}
\contentsline {section}{\numberline {2.28}Artikel 6 (b)}{23}{section.2.28}
\contentsline {section}{\numberline {2.29}Artikel 7}{23}{section.2.29}
\contentsline {section}{\numberline {2.30}Artikel 7 (Patent Insurance Extensions)}{24}{section.2.30}
\contentsline {section}{\numberline {2.31}Artikel 8}{25}{section.2.31}
\contentsline {chapter}{\numberline {3}Ablehnung/Annahme, Titel und Erw\"{a}gungsgr\"{u}nde}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Ablehnung/Annahme}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}Titel}{27}{section.3.2}
\contentsline {section}{\numberline {3.3}Erw\"{a}gung 1}{28}{section.3.3}
\contentsline {section}{\numberline {3.4}Erw\"{a}gung 5}{28}{section.3.4}
\contentsline {section}{\numberline {3.5}Erw\"{a}gung 5 (a) (neu)}{29}{section.3.5}
\contentsline {section}{\numberline {3.6}Erw\"{a}gung 6}{29}{section.3.6}
\contentsline {section}{\numberline {3.7}Erw\"{a}gung 7}{30}{section.3.7}
\contentsline {section}{\numberline {3.8}Erw\"{a}gung 7 (a) (neu)}{31}{section.3.8}
\contentsline {section}{\numberline {3.9}Erw\"{a}gung 7 (b) (neu)}{31}{section.3.9}
\contentsline {section}{\numberline {3.10}Erw\"{a}gung 9 (a) (neu)}{32}{section.3.10}
\contentsline {section}{\numberline {3.11}Erw\"{a}gung 11}{32}{section.3.11}
\contentsline {section}{\numberline {3.12}Erw\"{a}gung 11 (a) (neu)}{33}{section.3.12}
\contentsline {section}{\numberline {3.13}Erw\"{a}gung 11 (b) (neu)}{33}{section.3.13}
\contentsline {section}{\numberline {3.14}Erw\"{a}gung 12}{33}{section.3.14}
\contentsline {section}{\numberline {3.15}Erw\"{a}gung 13}{34}{section.3.15}
\contentsline {section}{\numberline {3.16}Erw\"{a}gung 13a}{35}{section.3.16}
\contentsline {section}{\numberline {3.17}Erw\"{a}gung 13b}{35}{section.3.17}
\contentsline {section}{\numberline {3.18}}{35}{section.3.18}
\contentsline {section}{\numberline {3.19}Erw\"{a}gung 13d}{36}{section.3.19}
\contentsline {section}{\numberline {3.20}Erw\"{a}gung 14}{37}{section.3.20}
\contentsline {section}{\numberline {3.21}Erw\"{a}gung 16}{38}{section.3.21}
\contentsline {section}{\numberline {3.22}Erw\"{a}gung 17}{38}{section.3.22}
\contentsline {section}{\numberline {3.23}Erw\"{a}gung 18}{39}{section.3.23}
\contentsline {section}{\numberline {3.24}Erw\"{a}gung 18 bis (neu)}{39}{section.3.24}
\contentsline {section}{\numberline {3.25}Erw\"{a}gung 18 ter (neu)}{40}{section.3.25}
\contentsline {chapter}{\numberline {4}Kommentierte Verweise}{41}{chapter.4}
\contentsline {chapter}{\numberline {5}Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}{43}{chapter.5}
