<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl 2003/09 Softwarepatent-Änderungsanträge: Echte und Scheinbare Begrenzungen

#descr: The European Parliament is scheduled to decide about the Software Patent Directive on September 24th.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all %(q:computer-implemented) problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.

#Lgn: Legende und Kommentiertes Beispiel

#ril: Artikel und Beschlussanträge

#eia: Ablehnung/Annahme, Titel und Erwägungsgründe

#enm: Antragsnummer

#jac: pro, kontra, unentschieden

#tuW: 410 dafür, 178 dagegen

#TitTit: Titel

#TitCec: Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions

#trW: Text des %(op:Originalvorschlags der Europäischen Kommission)

#TitSyn: It can not be the aim of the directive to declare all kinds of %(q:computer-implemented) ideas to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to automatic data processing and its various (technical and non-technical) fields of application, and this must be expressed in the title in plain and unambiguous wording.

#ung: Begründungen/Kommentare

#enm2: Antragsnummern

#uia: Antragsteller

#amd: Abstimmempfehlung

#ogs: Abstimmergebnis

#Am29: Vorschlag für eine Richtlinie des Europäischen Parlaments und des Rates über die Grenzen der Patentierbarkeit im Hinblick auf die automatische Datenverarbeitung und ihre Anwendungsbereiche

#eet: Text des Änderungsvorschlags

#tIa: Patent Insurance Extensions

#Art1: This Directive lays down rules for the patentability of computer-implemented inventions.

#nWw: Ideen die in allgemeiner Computerhardware umgesetzt werden sind keine Erfindungen im Sinne von Art 52(2) EPÜ. Der Begriff %(q:Computer-implementierte Erfindung) is nicht dem Fachmann auf diesem Gebiet bekannt, und da die einzigen Dinge, die man auf einem Computer implementieren kann, Computerprogramme sind (eine Waschmaschine oder ein Mobiltelefon können nicht in einem Computer implementiert werden), suggeriert er dass Computerprogramme Erfindungen sein können. Er wurde vom EPA im Jahr 2000 in dem berüchtigten %(e6:Anhang 6 des Trilateralen Website-Dokuments) eingeführt um das Europäische Patentrecht mit den USA und Japan auf eine Linie zu bringen und um Patente für %(q:computer-implementierte Geschäftsmethoden) zu erlauben, in der Erwartung, dass der Artikel 52(2) EPÜ gelöscht werde.

#Am116: This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs.

#Art2a: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: Art 52 des EPÜ sagt klar, dass ein Computerprogramm (ein Anspruch auf eine Datenverarbeitungsanlage und Regeln für ihre Verwendung) keine patentierbare Erfindung darstellen kann.

#fet: Die Änderungsanträge 36, 117 und 42 stellen klar, dass eine Erfindung nur dann patentierbar ist, wenn sie mit Art 52 EPÜ übereinstimmt, egal ein Computerprogramm Teil der Verwirklichung ist oder nicht. Der Änderungsantrag 14 würde es zulassen, dass ein reines Computerprogramm als %(q:Erfindung, zu deren Verwendung ein Computer notwendig ist und die alle ihre Eigenschaften durch ein Computerprogramm verwirklicht) beansprucht werden kann, so dass ein Widerspruch zu Art 52 EPÜ entsteht.

#Am36: %(q:Computerimplementierte Erfindung) ist jede Erfindung im Sinne des Europäischen Patentübereinkommens, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die in ihren Implementationen ­ außer den technischen Merkmalen, die jede Erfindung beisteuern muss  mindestens ein nichttechnisches Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#Am14: %(q:Computerimplementierte Erfindung) ist jede Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#Art2b: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Art2bSyn1: Eine Richtline welche den Begriff %(q:technischer Beitrag) zum Dreh- und Angelpunkt der Patentierbarkeit macht, muss diesen Begriff eindeutig definieren.

#Art2bSyn96: Änderungsantrag 96 scheint zu implizieren dass nur das Problem, nicht jedoch die Lösung technisch sein muss. Das Wort %(q:signifikant) hat keine rechtliche Bedeutung, und schafft damit eher Verwirrung denn Klarheit. Es ist offensichtlich, dass %(q:technisch) klar definiert werden muss damit dies hier eine nützliche Wirkung zeigt. Zu erfordern dass ein technisches Problem gelöst wird, ist genauso schlimm wie CEC oder sogar noch schlimmer.  Wir sollten uns nicht auf die Anwendung (das gelöste Problem) konzentrieren, sondern auf die Lösung (das Resultat empirischer Forschung die ein Patent rechtfertigen könnte). Man kann über jede Software sagen dass sie ein technisches Problem löst, aber wenn die Lösung einzig in Software-Teil innovativ ist, kann sie nicht als eine technische Lösung bezeichnet werden (zumindest keine neuartige technische Lösung).

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Änderungsantrag 107 macht es schließlich richtig: Es wird festgelegt dass %(q:technischer Beitrag) ein Synonym für Erfindung ist, und die Patentierbarkeits-Erfordernisse aus Art 52 EPC wiedergegeben. Dieser Änderungsantrag beschränkt die Patentierbarkeit in keiner Weise, sondern bekräftigt einfach Art 52 EPC und vermeidet Verwirrung die auftauchen kann wenn die Patentierbarkeits-Kriterien vermengt werden, wie es der Originaltext von CEC und die anderen Änderungsanträge tun.

#Am69: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#Am107: %(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.

#Am96: %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#Art2baSyn: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendments 55,97,108, which is found in various national case laws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and empistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 55 is apparently the result of mistranslation from French, should read %(q:digital). The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day case law and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such, but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification.

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#Am37: %(q:Technology) means %(q:applied natural science).  %(q:Technical) means %(q:concrete and physical).

#Am39Syn: Hierbei handelt sich um eine Grundlehre des Patentwesens in den meistens Rechtssprechungen.  Die %(q:vier Kräfte der Natur) sind ein anerkanntes Konzept der Epistemologie. Während Mathematik abstrakt ist und keinen Bezug zu Kräften der Natur hat, könnten einige Geschäftsmethoden von der Chemie des Hirnzellen eines Kunden abhängen, was jedoch nicht kontrollierbar ist, dass heißt nicht-deterministisch, dem freien Willen unterworfen. Somit schließt der Begriff %(q:kontrollierbare Kräfte der Natur) eindeutig aus was ausgeschlossen werden muss, und bietet gleichzeitig genügend Flexibilität um eventuelle zukünftige Felder der angewandten Naturwissenschaften außerhalb der derzeit annerkannten %(q:vier Kräfte der Natur) einzuschließen. Man muss hierbei beachten dass dieser Änderungsantrag Erfindungen im Bereich von Geräten wie Mobiltelefonen oder Waschmaschienen nicht unpatentierbar macht; es wird nur sichergestellt dass Computerprogramme nicht einfach patentierbar werden, indem sie bereits bekannten (technischen) Geräten beigefügt werden. Umgekehrt macht das beifügen eines Programms zu einer an sonsten patentierbaren Erfindung diese nicht unpatentierbar (da die Erfindung dann immer noch ein Problem durch Anwendung kontrollierbarer Naturkräfte löst, unabhängig von dem Vorhandensein des Computerprogramms.)

#Am39: %(Erfindung) im Sinne des Patentrechts ist die %(q:Lösung eines Problems durch Nutzung kontrollierbarer Naturkräfte).

#Am38Syn: Innovationen in der %(q:Musikindustrie) oder der %(q:Rechtsdienste-Industrie) dürfen nicht das Erfordernis der %(q:industriellen Anwendbarkeit) des TRIPs erfüllen. Der Begriff %(q:Industrie) wird heutzutage oft in erweiterten Bedeutungen benutzt, die im Zusammenhang des Patentrechts nicht angemessen sind.

#Am38: %(q:Industrie) im Sinne des Patentrechts ist die %(q:automatisierte Herstellung materieller Güter);

#Art3: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Art3Syn: Der Text der Europäischen Kommission besagt dass alle %(q:computerimplementierten) Ideen patentierbare Erfindungen nach Art 27 TRIPs sind. Verwerfen ist gut; Klärung auf welche Weise Art 27 TRIPs Anwendung findet wäre besser gewesen.

#Art3aSyn1: Einer der Hauptgründe für die Einführung der Richtline war es die Auslegung von Art 52 EPC angesichts Art 27 TRIPs klarzustellen, welcher besagt dass %(q:Erfindungen auf allen technologischen Gebieten) Patentierbar sein müssen. Wir beantragen dass Art 52(2) EPC in die Sprache von Art 27 TRIPs gefasst wird, in der Art von CULT-16.

#Art3aSyn2: Wenn Datenverarbeitung %(q:technisch) ist, dann ist alles %(q:technisch).

#Art3aSyn3: Datenverarbeitung ist eine gemeinsame Grundlage aller technologischen und nicht-technologischen Gebiete. Mit dem Aufkommen (universeller) Computer in den 50er Jahren hat elektronische Datenverarbeitung (EDV) Industrie und Gesellschaft durchdrungen.

#Art3aSyn4: Wie Gert Kolle, ein führender Theoretiker bei der Entscheidung in den 70er Jahren, Software von der Patentierbarkeit auszuschließen, im Jahre 1977 schreibt (siehe Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: Die Automatische Datenverarbeitung (ADV) ist heute zu einem unentbehrlichen Hilfsmittel in allen Bereichen der menschlichen Gesellschaft geworden und wird dies auch in Zukunft bleiben.  Sie ist ubiquitär. ... Ihre instrumendale Bedeutung, ihre Hilfs- und Dienstleistungsfunktion unterscheidet die ADV von den enger oder weiter begrenzten Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden -- beispielsweise auf den Gebieten des Managements, der Organisation, des Rechnungswesens, der Werbung und des Marketings -- von allen Wirtschaftsunternehmen benötigt werden und für die daher prima facie ein Freihaltungsbedürfnis indiziert ist.

#Art3aSynCult: CULT war gut damit beraten für einen klaren Ausschluss von Datenverarbeitung aus dem Bereich der %(q:Technologie) zu stimmen.

#Art3aSynStud: Zahlreiche Studien, einige davon durchgeführt von EU-Institutionen, sowie die geäußerten Ansichten des Europäischen Wirtschafts- und Sozialausschusses und des (((European Comittee of Regions))) erklären detailiert, warum die Europas Wirtschaft Schaden erleidet wenn Innovationen in der Datenverarbeitung nicht klar von der Patentierbarkeit ausgeschlossen werden.

#Am45: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#Art4N1: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Art4N2: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#Art4N3: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#Art4Syn: This article mixes the non-obviousness test with the invention test, thereby weakening both and conflicting with Art 52 EPC.

#cpW: JURI hat diesen Artikel in Teile 1, 2 und 3 aufgeteilt. Dies ist gefährlich, da es verhindert, dass einige Änderungsanträge, die für den Artikel in seiner Gesamtheit anwendbar sind, überhaupt zur Abstimmung gebracht werden.

#iaa: Aufgeteilt in Teile 1, 2 und 3.

#Art4N1Syn: %(q:Computerimplementierte Erfindungen) (sprich Ideen die begrifflich für allgemeinen Datenverarbeitungs-Anlagen gefasst sind = Computerprogramme), sind keine Erfindungen laut Art 52 EPC. Siehe auch die Analyse von Artikel 1. Änderungsvorschlag 56=98=109 behebt diesen Fehler und bestätigt Art 52 EPC. Keiner der Änderungsvorschläge beschränkt Patentierbarkeit, aber dieser bringt die Dinge zumindest formal in Ordnung.

#Am16N1: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.

#Am98: Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: Eine angemessene Wiedergabe von Art 52 EPC. Da es Ziel der Richtline ist zu harmonisieren und klarzustellen, ist es von höchster Bedeutung die korrekte Auslegung des EPC hervorzuheben, da diese die Grundlage des Patentrechts in Europa darstellt.

#Am48: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4N2Syn: Der Text der Kommision weit von Art 52 EPC ab. Wie in der Analyse von Erwägung 11 erklärt, muss erst festgestellt werden ob eine Erfindung/ein technischer Beitrag vorliegt (entweder anhand der negativen Definition in Art 52 EPC: ein Computerprogramm ist keine Erfindung, oder anhand der positiven Definition aus der Rechtspraxis, basierend auf kontrollierbaren Kräften der Natur). Danach muss geprüft werden ob diese Erfindung den drei anderen Kriterien aus Art 52 standhält, von denen der erfinderische Schritt (= Erfindungshöhe) eins ist. Der Text der Kommission macht den erfinderischen Schritt zum Kriterium, ob etwas eine Erfindung darstellt. Da eine Computerprogramm nicht immer naheliegend sein muss, könnte es nach dieser Definition eine Erfindung darstellen (entgegen dem was EPC Art 52 besagt).

#Art4N2SynP2: Des weiteren impliziert der Text der Kommission dass Ideen, die für allgemeine Computer abgefasst sind (also Computerprogramme), patentierbare Erfinungen sind.  Entfernen dieses Absatzes, wie in Änderungsantrag 82 gefordert, könnte hilfreich sein, da er nur eine Verwirrung der Patentierbarkeits-Kriterien bewirkt, und es so den nationalen Patentämtern unmöglich macht gesetzesfremde Patentanmeldungen ohne substanzielle Ergründung abzulehnen. Änderungsantrag 16 macht den gleichen Fehler wie der Text der Kommission; er verbessert nur ein wenig die sprachliche Formulierung. Änderungsantrag 40 wird zurückgezogen; er wurde jetzt als (((insertion))) neu eingebracht (83).

#Am16N2: In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

#Art4N3Syn1: Eine Erfindung zum Patent anzumelden bedeutet, eine bestimmte Anwendung der Erfindung für sich zu beanspruchen. Diese Anwendung ist das was in den Ansprüchen beschrieben wird. Somit beinhalten die Ansprüche in ihrer Gesamtheit sowohl die Erfindung an sich in irgendeiner Form, als auch eine Reihe nicht-patentierbarer Merkmale (wie zum Beispiel ein Computerprogramm), welche nötig sind für die jeweilige Anwendung der Erfindung. Das heißt selbst wenn gefordert wird dass die Erfindung technischer Natur sein muss, können die Ansprüche nicht-technische Merkmale enthalten (dem wir auch überhaupt nicht widersprechen; so haben Patente schon immer funktioniert).

#Art4N3Syn2: Der Entwurf der Europäischen Kommission entwertet jedoch sein eigenes Konzept des %(q:technischen Beitrags), indem der Beitrag selbst aus nicht-technischen merkmalen bestehen kann. Die Version von JURI legt nur fest dass die Ansprüche technische Merkmale enthalten müssen, was keine wirkliche Einschränkung ist. Ein Beispiel könnte dies veranschaulichen. Angenommen wir wollen ein Computerprogramm patentieren; also sagen wir dass dies den %(q:technischen Beitrag) darstellt. In den Ansprüchen beschreiben wir die Anwendung dieses Programms, also sagen wir dass wir das ausführen dieses Programms auf einem Computer patentieren. Jetzt enthalten die Patentansprüche in ihrer Gesamtheit technische Merkmale (den Computer), und der Unterschied zwischen dem Anspruch als Ganzes (bekannter Computer+neues Programm) und dem Stand der Technik (bekannter Computer) besteht in dem Programm. Somit kann ein Computerprogramm laut dieser Bedingung einen technischen Beitrag darstellen, was einen totaler Widerspruch ist (ein Computerprogramm kann nicht technisch sein). Es wird klar dass eine Korrektur wie die im Änderungsantrag 57=99=110 notwendig ist, wenn das Konzept des  %(q:technischen Beitrags) überhaupt benutzt werden soll.

#lWo: Die Neuheitsschonfrist, die von Änderungsantrag 100 vorgeschlagen wird, ist eine unabhängige Fragestellung. Wie in sich kürzlich in einer %(ng:Beratung des europäischen Patentamts) gezeigt hat, ist sie sogar unter denjenigen, die eigentlich davon profitieren sollten, sehr umstritten. Es ist nicht klar, ob eine Neuheitsschonfrist in der Tat, wie von dem Bericht von ITRE behauptet, KMUs zugute kommen. Im Fall von quellenoffener Entwicklung kann sogar zusätzliche Unsicherheit entstehen, ob veröffentlichte Ideen patentfrei sind.

#Am16N3: The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#Am99: The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art.

#Am100P1: The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.

#Art43aSyn: Wie in der Analyse von Artikel 1 erläutert, ist %(q:computerimplementierte Erfindung) ein wiedersprüchlicher Begriff. Wie in der Analyse von Erwägung 11 erwähnt, sind %(q:Erfindung) und %(q:technischer Beitrag) synonym: Was man erfunden hat, ist der eigene (technische) Beitrag zum Stand der Technik.  In dem Jargon der in der Richtline verwendet wird, bedeutet %(q:technischer Beitrag) jedoch nur %(q:(((solution to a technical problem within the step from the closest prior art document to the claimed invention))) ).  In diesem Zusammenhang ist es unklar welche Wirkung ein Kriterium, egal wie strikt, haben wird, da es nicht klar ist worauf es sich bezieht.

#Art43aSynTest: Mit der Formulierung dass dieses Kriterium %(q:(((shall apply))) ), wird nicht klar gefordert dass dieses Kriterium tatsächlich %(e: erfüllt sein muss), noch dass es definiert was mit %(q:technischem Beitrag) gemeint ist.

#Art43aSynInd: Schlussendlich gibt es keine rechtlich verbindliche Definition von %(q:industrial application in the strict sense of the expression, in terms of both method and result)

#Art43aSynVal: Trotz dieser Mängel empfehlen wir Änderungsantrag 70 zu unterstützen. Wenigstens schreibt er zentrale Aspekte des Konzepts der technischen Erfindung fest. Damit könnte er, in Verbindung mit anderen Änderungsanträgen, am Ende dazu beitragen eine klare Grenze der Patentierbarkeit zu zeichnen.

#Am70: In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of natures and has an industrial application in the strict sense of the expression, in terms of both method and result.

#Art4aSyn: Änderungsantrag 17 geht davon aus dass %(q:normale physikalische Interaktionen zwischen einem Programm und dem Computer) etwas bedeutet.  Das tut es jedoch nicht.  Eine Bedeutung hätte zugewiesen werden können, zum Beispiel indem man festlegt, in Anleihe an die %(es:%(tp|Error Search|Suche Fehlerhafter Zeichenketten) decision) des deutschen Bundespatentgerichts, dass %(q:Erhöhung der Recheneffizienz), %(q:Einsparungen im Speicherverbrauch) etc. %(q:keinen technischen Beitrag darstellen) noch %(q:anzusiedeln sind im Bereich der normalen physikalischen Interaktion zwischen einem Programm und dem Komputer).  Ohne solch eine Präzisierung, bringt Änderungsantrag 17 nur Verwirrung statt einer Klärung.

#Am17: %(al|Ausschluss von der Patentierbarkeit|Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausführung ein Computerprogramm eingesetzt wird und durch die Geschäftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentfähig, wenn sie über die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.)

#Art4bSyn: Hier gilt das Gleiche wie für den darüberstehenden Änderungsantrag.

#Am87: %(al:Ausschluss von der Patentierbarkeit Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausführung lediglich ein Computerprogramm eingesetzt wird (Datenverarbeitung) und durch die Geschäftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentfähig, wenn sie über die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.)

#Art4cSyn1: Die Richtline wie sie von CEC und JURI vorgegeben wird, basiert auf der %(q:(((Pension Benefit System))) ) Lehre des EPO und der trilateralen Übereinkunft der USA, Japans und des Europäischen Patentamts aus dem Jahre 2000, welche alle Computerprogramme als patentierbare Erfindungen betrachten.  Dies wird durch den Begriff %(q:computerimplementierte Erfindung) zum Ausdruck gebracht, der vom EPO eingeführt wurde um die Übereinkunft durchzubringen.  Es gibt keinen Test mehr ob eine Erfindung vorliegt, sondern nur noch nach einem %(q:technischen Beitrag im erfinderischen Schritt), das heißt der Schritt zwischen dem %(q:Stand der Technik) und der %(q:Erfindung) muss abgefasste werden als %(q:technisches Problem), zum Beispeiel ein Problem der Erhöhung der Geschwindigkeit oder der effizienteren Ausnutzung von Computerspeicher.  Durch Nutzung dieser EPO-Doktrin wird im Endeffekt jeder Algorithmus und jede Geschäftsmethode patentierbar, wie von dem deutschen Bundespatentgericht in einer %(es:recent decision) aufgezeigt wurde.

#Art4cSyn2: Änderungsanträge 47 und 60 sagen dem EPO deutlich, es möge dem Ansatz des deutschen Bundespatentgerichts folgen, und es ablehnen Verbesserungen in der Effizienz von Datenverarbeitung als %(q:technischen Beitrag) zu werten.

#Am60: Mitgliedsstaaten sollen sicherstellen, dass computerimplementierte Lösungen zu einem technischen Problem nicht nur deshalb als patentierbare Erfindungen angesehen werden weil sie die Effizienz beim Verbrauch von Resourcen innerhalb des Datenverarbeitunsgssystems verbessern.

#Art4dSyn1: Dieser Änderungsantrag alleine könnte die meisten Vorkehrungen der Richtline ersetzen.  Der Leitsatz der EPO von 1978 liefert eine klare Auslegung von Art 52 EPC.  Dies wurde %(q:als Antwort auf die Erfordernisse der Industrie) in späteren Versionen des Leitsatzs geändert, in Kauf nehmend dass die Regeln widersprüchlich wurden und ein Schisma eingeführt wurde.  Ziel dieser Richtlinie sollte sein dieses Schisma aufzulösen und die Anwendung des TRIPs-Vertrages aus dem Jahre 1994 zu konkretisieren.  Das erste Ziel kann durch diesen Änderungsantrag allein erreicht werden.  Dabei ist zu beachten dass der Änderungsantrag nicht verlangt dass der Leitsatz des EPO von 1978 zum Gesetz werden sollte.  Er sagt nur aus dass dieser eine angemessene Auslegung von Art 52 EPC liefert.

#Am46: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Article 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#Art5: Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausführung von Software verwirklicht wird.

#Art5Syn18: Änderungsantrag 18 beantragt die Einführung von Programmansprüchen, das heißt Ansprüchen der Art %(bc:ein Programm, gekennzeichnet dadurch, dass es nachdem es auf einem Computer geladen ist, ein Verfahren ausführt.) Dies würde jede Publikation von Software zu einer möglichen direkten Patentverletzung machen, und würde somit Programmierer, Forscher und Internet-Provider einem Risiko aussetzen. Ohne Programmansprüche können Lieferanten nur indirekt zur Verantwortung gezogen werden, z.B. durch Garantien, die sie ihren Kunden gewähren. Insofern scheint die Frage nach Programmansprüchen eher symbolischer als materieller Natur zu sein: Die Patentlobby will unmißverständlich dokumentieren, daß Software als solche patentierbar ist. CEC hatte von dieser letzten Konsequenz Abstand genommen.

#Art5Syn58: Die Wortwahl der EUK legt nach wie vor nahe, dass bekannte universelle Computerhardware und darauf ausgeführte Rechenregeln (= Computerprogramme) patentierbare Produkte und Prozesse sind. Änderungsantrag 101/58 versucht dieses Problem anzugehen. Es könnte eine gute Idee sein, %(q:(((technical production processes operated by such a computer))) ) durch %(q:die erfinderischen Verfahren, die von einer solchen Apparatur ausgeführt werden) zu ersetzen, wenn dies noch in Form eines %(q:Kompromiss-Änderungsantrags) möglich ist. Änderungsantrag 102 definiert eine %(q:computerimplementierte Erfindung) entweder als ein Produkt oder einen technischen Produktionsprozess. Dies mag nützlich sein, sofern %(q:technisch) definiert ist. Nichtdestoweniger kann ein Computer, der ein Programm ausführt, ebenfalls als ein %(q:programmiertes Gerät) aufgefaßt werden, und den Gebrauchs eines Computersprogramms zu patentieren, wenn dieses auf einem Computer ausgeführt wird, hat denselben Effekt wie das Programm an sich zu patentieren: Es gibt keine andere Weise, ein Computerprogramm zu benutzen, außer es auf einem Computer auszuführen.

#Am18: Ein Patentanspruch auf ein Computerprogramm für sich allein, auf einem Datenträger oder als ein Signal ist nur zulässig, wenn dieses Programm, nachdem es auf einem Computer, einem Computernetz oder einer sonstigen programmierbaren Vorrichtung geladen ist oder abläuft, zu einem Erzeugnis führt oder ein Verfahren ausführt, das gemäß den Artikeln 4 und 4a patentierbar ist.

#Am101: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#Am102: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#Art5aSyn: Freie Software leistet einen wichtigen Beitrag zur Innovationskraft und Verbreitung von Wissen, ohne vom Patentsystem zu profitieren. Dies sollte sich in der Innovationsförderung und im Gesetz wiederspiegeln, ohne von vornherein davon auszugehen, das proprietäre Software patentierbar sein sollte. Leider verwendet dieser Änderungsantrag den Begriff %(q:computerimplementierte Erfindungen), der vom EPA im Jahr 2000 mit der Absicht eingeführt wurde, Patentierbarkeit von Datenverarbeitungsvorgängen zu ermöglichen. Dies sollte in einer zweiten Lesung korrigiert werden.

#Am62: %(al|Limitation of the effects of patents granted to computer-implemented inventions|%(linol|Member States shall ensure that the rights conferred by the patent shall not extend to the acts done to run, copy, distribute, study, change or improve a computer program which is distributed under a licence that provides for:|the freedom to run the program;|the freedom to study how the program works, and adapt it to the user's needs;|the freedom to redistribute copies under the same licence conditions;|the freedom to improve the program, and release improvements to the public under the same licence conditions;|free access to the source code of the program.))

#Art51a: This amendment apparently intends to exclude claims to ways of operating data processing hardware (= computer programs), but proposes inappropriate regulatory means.  Patent claims always comprise more than the invention (= technical contribution).  If there is a technical invention (e.g. a new chemical process), then there is no reason why the patent claim should not comprise this invention together with non-invention elements, such as a computer program that controls the reaction.  A claim does not describe an invention but the scope of an exclusion right which is legitimated by an invention.

#Am72: Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentansprüche nur den technischen Beitrag umfassen, der den Patentanspruch begründet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datenträger vorliegendes Programm, ist unzulässig.

#Am103Syn: Ohne diesen Änderungsantrag könnten Inhaber von Softwarepatenten immer noch Programmierer oder Softwarevertreiber abmahnen, und sie der Beihilfe zu einer Patentverletztung beschuldigen. Dieser Artikel stellt sicher, dass das Recht zu Publizieren, wie es in Art 10 ECHR festgeschrieben ist, Vorrang über Patentanprüche hat. Andererseits verhindert dieser Änderungsantrag nicht das Erteilen von Softwarepatenten oder ihre Durchsetzung gegenüber Anwendern von Software. Hersteller proprietärer Software und kommerzielle Linux-Distributoren könnten immer noch unter dem Druck ihrer Kunden stehen, welche gewöhnlich eine vertragliche Verpflichtung zur Abdeckung jeglicher Patentforderung durch den Hersteller erwarten.

#Am103: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#Am104N1Syn: The scope of the patent is normally defined by the claims, and if something falls outside the claim scope, it does not constitute an infringment.  Thus the amendment, at first sight, seems tautological.  The intention of this amendment may be to allow simulation of patentable processes on a standard data processing system.  If so, it should be said clearly, as done by some of the other amendments.

#Am104N1: Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#Art5dSyn: Diese Änderungsanträge sollen, anders als manche denken könnten, nicht dazu dienen, freie/Open Source Software zu schützen, sondern vielmehr sicherstellen, dass die Verpflichtung zur Veröffentlichung, welche dem Patentsystem innewohnt, ernst genommen wird, und dass sich Software -- wie jedes andere Informationsobjekt -- im Patent auf der Seite der Öffentlichmachung befindet, nicht auf der Seite des Ausschlusses/der Monopolisierung. Dies macht es ein wenig schwieriger, Menschen davon abzuhalten, Dinge zu tun, die man selbst nicht getan hat, die aber offensichtlich möglich sind, da das Rechenmodell perfekt definiert ist, und man immer genau weiß, was man mit einem Computer tun kann. Wer funktionsfähigen Programmquelltext anbietet, bietet zumindest konkretes Wissen darüber wie man das Problem lösen kann -- anders als bei Aussagen wie %(q:eine Recheneinheit gekoppelt an eine Ein-/Ausgabeeinheit, so dass eine Funktion berechnet wird, in einer solchen Weise, dass das Ergebnis besagter Funktion, ausgegeben über besagte Ausgabeeinheit, das Problem löst, welches der Kunde lösen wollte).

#Am104N2: Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#Art6: Zulässige Handlungen im Sinne der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften über die Dekompilierung und die Interoperabilität, oder im Sinne der Vorschriften über Marken oder Halbleitertopografien bleiben vom Patentschutz für Erfindungen aufgrund dieser Richtlinie unberührt.

#Art6Syn: Der Entwurf der Europäischen Kommission sichert nicht die Interoperabilität; er stellt ganz im Gegenteil sicher, dass kompatible Software nicht veröffentlicht und benutzt werden darf, wenn die Schnittstelle patentiert wurde. Das einzige was die Kommission in diesem Fall erlauben möchte ist das Dekompilieren, welches aber Patente sowieso nicht verletzen würde (da Patente sich auf die Benutzung dessen was in den Ansprüchen beschrieben ist beziehen, nicht aber das Studieren des gleichen). Änderungsanträge 66 und 67 legen darüber hinaus fest auf Basis welcher Gesetze Interoperabilitäts-Privilegien beansprucht werden können, und bewirken so erst recht dass die Vorkehrung keinerlei Wirkung haben kann.

#Am19: Rechte, die aus Patenten erwachsen, die für Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unberührt von urheberrechtlich zulässigen Handlungen gemäß Artikel 5 und 6 der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen, insbesondere hinsichtlich der Bestimmungen über die Dekompilierung und die Interoperabilität.

#Art6aSyn: Änderungsanträge 20 und 50 sind die einzigen die ein tatsächliches Interoperabilitätsprivileg vorsehen.  Die Wortgruppe %(q:Computersystem und Netzwerk) sollte jedoch um der Klarheit willen zu %(q:Datenverarbeitungssysteme) korrigiert werden. Änderungsanträgen 50 tut dies.  Änderungsantrag 20 wurde bereits von CULT, ITRE und JURI akzeptiert.

#Am76Syn: Amendment 76 beseitigt alle Klarheit darüber, wann die Nutzung von patentierten Techniken zum Zweck der Interoperabilität erlaubt ist und wann nicht. Damit läuft es dem Hauptanliegen der Direktive zuwider, gewisse abstrakte Meta-Regeln zu konkretisieren, darunter die des Art 30 TRIPs, und dadurch Klarheit und Rechtssicherheit zu schaffen.

#Am105Syn: Es ist nett dass Änderungsantrag 105 unsere Ansicht bestätigt, dass eine %(q:computerimplementierte Erfindung) nichts anderes bedeuten kann, als Software die auf einem Computer abläuft. Die Ausnahmen, die es nennt, sind jedoch äußerst merkwürdig. Im ersten Fall, wenn die Erfindung eine eigentständige Maschine ist, keine Notwendigkeit für Interoperabilität. Wenn es keine Notwendigkeit für Interoperabilität gibt, dann ist Amendment 20 auch nicht anwendbar. Wenn dagegen eine solche Notwendigkeit vorliegt, dann funktioniert die patentierte Erfindung nicht als eigenständige Maschine oder technische Erfindung. Der zweite Abschnitt ist  ziemlich gefährlich, da er alle Entscheidungsmacht den Gerichten überlässt, und damit das Ziel der Direktive verfehlt, Klarheit zu schaffen. Wie der Antitrust-Klagen gegen Microsoft sowohl in den USA als auch in Europa zeigen, ist es extrem schwierig und zeitraubend, abzuschätzen ob ein bestimmtes Verhalten gegen Wettbewerbsrecht verstößt. Damit würde die Interoperabilitätsklausel zahnlos werden und KMUs noch mehr benachteiligen, da große Konzerne sehr viel mehr Geld für Gerichtsverfahren ausgeben können.

#Am50: Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement..

#Am20: Die Mitgliedstaaten stellen sicher, dass in allen Fällen, in denen der Einsatz einer patentierten Technik nur zum Zweck der Konvertierung der in zwei verschiedenen Computersystemen oder netzen verwendeten Konventionen benötigt wird, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu ermöglichen, diese Verwendung nicht als Patentverletzung gilt.

#Am76: Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that it does not unreasonably conflict with a normal exploitation of the patent and does not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#Am105: %(al|Use of patented technologies|Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that:|%(ol|the invention embodying the computer-implemented software is not working as an independent machine or technical invention, and|the patent is not in conflict with European competition law.))

#Art6b: Dieses Amendment scheint ein neues Eigentumsrecht außerhalb des Patentsystems zu schaffen. Es etabliert eine Kategorie %(q:computerimplementierte Erfindung), die zu keinem %(q:Gebiet der Technik) gehört. Dies ist ein interessanter Ansatz, obwohl 7 Jahre wahrscheinlich immer noch zu lang sind und einige weitergehende Fragen erst geklärt werden müssen, um dies durchführbar zu machen.

#Am106: %(al|Term of patent|The term of a patent granted for computer-implemented inventions pursuant to this Directive shall be 7 years as from the date of filing.)

#Artt7: Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit auswirken; ferner auf die europäischen Unternehmen und den elektronischen Geschäftsverkehr.

#Art7Syn: Es sollte sichergestellt werden, dass die Erhebung nicht von den Patentanwälten der GD Binnenmarkt durchgeführt werden, sondern stattdessen von Software und Wirtschaftsexperten. Änderungsvorschlag 71 spricht über die Schaffung eines Versicherungssystems für den Patentzukauf und Patentsrechtsstreite, obwohl kein praktisches Beispiel für ein derartiges System das funktioniert existiert. Die theoretische Möglichkeit wie sie von einem Bericht von Beratern an die GD Binnenmarkt vorgeschlagen wird, basiert auch auf den falschen Annahmen, wie schon im %(le:open letter to the European Parliament) erklärt wird, welcher von einer Reihe angesehener Ökonomen geschrieben wurde. Änderungsantrag 91 ist etwas besser als die anderen, aber wenn KMUs explizit aufgeführt sind, dann sollten zumindest auch freie/Open Source Software und Standardisierung erwähnt werden. Es garantiert auch nicht, dass die Prüfung von einem unabhängigen Team von Experten für Softwareökonomie durchgeführt wird.

#Am21: Die Kommission beobachtet, wie sich der Patentschutz hinsichtlich computerimplementierter Erfindungen auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen, insbesondere auf kleine und mittlere Unternehmen, und den elektronischen Geschäftsverkehr auswirkt.

#Am91: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses. Special attention will be paid to the position of small and medium-sized enterprises and of electronic commerce and to the impact of dominant positions on the functioning of the market.

#Am71Syn: Änderungsvorschlag 71 spricht über die Schaffung eines Versicherungssystems für den Patentzukauf und Patentsrechtsstreite, obwohl kein praktisches Beispiel für ein derartiges System das funktioniert existiert. Die theoretische Möglichkeit wie sie von einem Bericht von Beratern an die GD Binnenmarkt vorgeschlagen wird, basiert auch auf den falschen Annahmen, wie schon im %(le:Offenen Brief an das Europäische Parlament) erklärt wird, welcher von einer Reihe angesehener Ökonomen geschrieben wurde.

#Am90Syn: Amendment 90 is like 71, but less explicit.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: Die Kommission beobachtet, wie sich der Patentschutz hinsichtlich computerimplementierter Erfindungen auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen, insbesondere auf kleine und mittlere Unternehmen und die Open Source Gemeinschaft, und den elektronischen Geschäftsverkehr auswirkt.

#Am71P2: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents, in particular through the creation of a defence fund and the introduction of special rules on legal costs.

#Am71P3: It shall report on its findings to the European Parliament and the Council and present appropriate proposals for legislation without delay.

#rbr: Die Kommission legt dem Europäischen Parlament und dem Rat spätestens am [DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor über:

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Ablehnung/Annahme

#app: lehnt die vorgeschlagene Richtlinie ab

#dWt: Es ist möglich durch die vorgelegten Änderungsanträge die Richtlinie zu korrigieren. Wenn die richtigen Änderungsanträge angenommen werden, kann die Richtlinie nützlich für die EU sein. Die Annahme einer grundlegend berichtigten Richtlinie wäre also besser als eine Ablehnung. Auf der anderen Seite ist es eine komplexe Aufgabe eine Richtlinie derartig grundlegend zu verändern, insbesondere im Plenum; und es kann passieren, dass es dem Europäischen Parlament nicht gelingt die reichlich vorhandenen Annahmen und Missverständnisse zu berichtigen. Bei der Erstellung der Richtlinie wurden die meisten interessierten Parteien, wissenschaftlichen Studien und Tatsachen nicht angemessen berücksichtigt. Den Text zurückzuweisen und die Kommission aufzufordern das Verfahren neu zu starten wäre auch gerechtfertigt. In der Tat, die Zahl der Fehler ist so groß, dass es sehr riskant ist, sich auf die Ergebnisse des Plenarvotums für eine gute Direktive zu verlassen. In Anbetracht der Gefahren gegenüber Fortschritt, Freiheit und der Wettbewerbsfähigkeit von Unternehmen der EU, ist eine Zurückweisung wünschenswerter. Es handelt sich um Rechtsprechung für die nächsten 20 Jahre. Obwohl schon einige Zeit investiert wurde, sind wir noch einer frühen Phase der Meinungsbildung und nicht in Eile.

#ute: Richtlinie ablehnen

#Rec1Cec: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec1Syn: Die Kommission behauptet verwegen, entgegen jeden %(ss:ökonomischen Belegen), einschließlich %(bh:sehr detaillierter empirischer wirtschaftswissenschaftlicher Untersuchungen wie Bessen & Hunt von 2003), dass Patente Investitionen in Softwareentwicklung fördern. Darüberhinaus ist der Begriff %(q:der computer-implementierten Erfindung) verwirrend.  Es ist besser keine Zielaussage zu haben als eine falsche.

#Am1: Damit der Binnenmarkt verwirklicht wird, müssen Beschränkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen begünstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium für den Erfolg des Binnenmarkts. Es ist unerlässlich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam, transparent und einheitlich geschützt sind, wenn Investitionen auf diesem Gebiet gesichert und gefördert werden sollen.

#Rec5Cec: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Rec5Syn1: Sowohl die Europäische Kommission und der Änderungsantrag 2 (JURI) sagen dogmatisch dass Softwarepatente Innovation fördern. Es wäre angemessener ein Strategiezahl anzugeben, an dem das vorgeschlagene Patentierbarkeitsregime gemessen werden kann.

#Rec5Syn2: Änderungsantrag 2 scheint zu behaupten, dass die alleinige Tatsache, europäisches Recht zu schreiben, völlig ungeachtet von dessen Inhalt, bereits %(q:Rechtssicherheit bewirkt), nur weil der Luxemburger Gerichtshof dieses Recht auslegen kann. Diese neue Ziel ist nicht nur fraglich sondern auch überflüssig, da %(ol|das Gemeinschaftspatent sowieso auf dem Weg ist|es einfachere und sytematischere Wege gibt den Luxemburger Gerichtshof verantwortlich zu machen als durch eine auslegbare Richtlinie für einen Aspekt des Europäischen Patentübereinkommens)

#Am2: Aus diesen Gründen sollten die für die Patentierbarkeit computerimplementierter Erfindungen maßgeblichen Rechtsvorschriften vereinheitlicht werden, um sicherzustellen, dass die daraus folgende Rechtssicherheit und das Anforderungsniveau für die Patentierbarkeit dazu führen, dass innovative Unternehmen den größtmöglichen Nutzen aus ihrem Erfindungsprozess ziehen und Anreize für Investitionen und Innovationen geschaffen werden.

#css: Die Rechtssicherheit wird auch dadurch sichergestellt, dass bei Zweifeln über die Auslegung dieser Richtlinie die Gerichte der Mitgliedstaaten den Gerichtshof der Europäischen Gemeinschaften anrufen können und die letztinstanzlichen Gerichte der Mitgliedstaaten hierzu verpflichtet sind.

#Rec5aSyn: Wir stimmen zu, dass die Regeln, wie sie in Artikel 52 EPÜ niedergelegt wurden durch das EPA bis zur Unkenntlichkeit verzerrt wurden und dass die ursprüngliche Bedeutung, wie in den Prüfungsreichtlinien von 1978 des Europäischen Patentamts spezifiziert, wiederhergestellt werden sollten.

#Am88: Die Regeln gemäß Artikel 52 des Europäischen Patentübereinkommens über die Grenzen der Patentierbarkeit sollen bestätigt und präzisiert werden. Die dadurch entstehende Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen.

#Rec6Cec: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Rec6Syn: Wir müssen klarstellen, dass es Grenzen für das, was unter den %(q:Gebieten der Technik) nach Art 27 TRIPS zusammengefaßt wird, gibt, und dass dieser Artikel nicht entworfen wurde, um unbegrenzte Patentierbarket zu fordern, sondern um Reibungen im Freihandel zu vermeiden, die durch unangemessene Ausnahmen wie auch unangemessene Ausweitungen der Patentbarkeit verursacht werden können. Diese Interpretation von TRIPS ist indirekt durch jüngstes Lobbying der US-Regierung gegen Art 27 TRIPS bestätigt, da argumentiert wurde, dass er die Patentierbarkeit von Software und Geschäftsmethoden ausschließt oder zumindest so interpretiert werden kann; die US-Regierung will diese deshalb durch den neuen Entwurf des Vertrags über Materielles Patentrecht gewährleisten, siehe %(bh:Artikel von Brian Kahin in First Monday). Der Änderungsantrag 31 löscht diesen faktisch falschen Artikel.

#Am31: Loeschen ist gut, aber eine Klarstellung wäre besser gewesen.

#Rec7Cec: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Rec7Syn: Die Bedeutung dieser Bestimmung ist unklar. Das Ziel einer Richtlinie ist es gewöhnlich, Regeln zu definieren, nicht Texte zu korrigieren. Selbst wenn die Mitgliedsstaaten entscheiden würden, dass es angemessen wäre, den Art 52 zu ändern (anstelle ihn einfach nur durch andere rechtliche Begriffe außer Kraft zu setzen), könnten sie dennoch explizit das EPÜ ändern (anstelle es einfach außer Kraft zu setzen), indem sie behaupten damit dem Zweck der Richtlinie zu dienen.

#Am32: Nach dem Übereinkommen über die Erteilung europäischer Patente (Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den Patentgesetzen der Mitgliedstaaten gelten Programme für Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt, da die besagten Gegenstände und Tätigkeiten keinem Gebiet der Technik zugehören.

#Am3: Durch diese Richtlinie soll jenes Übereinkommen nicht geändert, sondern die unterschiedliche Auslegung seiner Bestimmungen vermieden werden.

#Rec7bSyn: Ein neuer Entschluss, der größere Verantwortlichkeit des EPA fordert. Dieser Effekt wäre stärker, wenn es in einem Artikel gewesen wäre, aber ist nichtsdestoweniger ein gutes Prinzip. Es wichtig alle Konfliktquellen zu nennen, um dementsprechende Lösungen einfacher zu finden.

#Am95: Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeÃ?s operating rules to ensure that it was publicly accountable in the exercise of its functions.

#cdn: Softwarepatente wären schlecht für alle Softwareentwickler, die nicht durch eine große Firma mit viel Geld und Anwälten gedeckt werden, und das schließt offensichtlich eine Anzahl von Entwicklern Freier Software ein.

#Am61: Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: Die vier Patentierbarkeitstests in Art 52 EPÜ stellen fest, dass es eine (technische) Erfindung gibt und dass zusätzlich die Erfindung der gewerblichen Anwendung zugänglich sein muss, neu sein muss und einen erfinderischen Schritt beinhalten muss. Die Bedingung des %(q:erfinderischen Schritts) ist definiert nach Art 56 EPÜ als die Anforderung, dass die Erfindung nicht eine offensichtliche Kombination von bekannten Techniken für einen Fachmann auf dem Gebiet ist. Zusätzlich sind, %(q:Erfindung) und %(q:technischer Beitrag) synonym: man kann nur neue Dinge erfinden, und diese Erfindung ist dann der (technische) Beitrag zum Stand der Technik. Deshalb sind das Original der Kommission und die Änderungsanträge 4 und 84 in direktem Widerspruch mit dem EPÜ. Zusätzlich würde solch eine Argumentation darauf hinauslaufen, dass Patentierbarkeit nur eine Frage der Formulierung der Ansprüche ist, siehe %(se:Vier verschiedene Tests von einem Einheitlichen Objekt) Um Verwirrung und Konflikte mit EPÜ Art 52 und 56 zu vermeiden muss diese Präambelklause neuformuliert oder gelöscht werden.

#Am51: While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science.

#Am4: Um patentierbar zu sein, müssen Erfindungen im Allgemeinen und computerimplementierte Erfindungen im Besonderen neu sein, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen Tätigkeit zu erfüllen, sollten computerimplementierte Erfindungen einen technischen Beitrag zum Stand der Technik leisten.

#Am84: The legal protection of computerimplemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec11Syn: The Commission text declares computer programs to be technical inventions. It removes the independent requirement of invention (= %(q:technical contribution)) and merges it into the requirement of non-obviousness (= %(q:inventive step), see comments for Recital 11). This leads to theoretical inconsistency and undesirable practical consequences.

#Rec11bSyn: Der Änderungsantrag 73 wäre eine perfekte Wiedergabe von Art 52 EPÜ, wenn er %(q:Erfindung) statt %(q:computer-implementierter Erfindung) verwenden würde. Im seinem jetzigen Zustand sieht er wie eine Sonderregel für Erfindungen, die Computer betreffen aus. Er ist dennoch nützlich, in dem er klärt, dass auch eine solche Erfindung die vier Tests bestehen muss. Vgl Artikel 1 warum der Begriff %(q:computer-implementierte Erfindung) irreführend ist.

#Am73: Computerimplementierte Erfindungen sind nur dann patentierbar, wenn sie einem Gebiet der Technik zugeordnet werden können und zudem neu, auf einer erfinderischen Tätigkeit beruhend und gewerblich anwendbar sind.

#Rec12: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Rec12Syn: The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This contradicts Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible. Again, see the explanation in Recital 11 for more information. Amendment 5 by JURI makes matters even worse: first of all it literally reintroduces article 3 (which it deleted), secondly it claims the %(q:inventive step) requirement is something completely different from what Art 56 EPC says, thirdly it starts talking about a %(q:technical problem) which should be solved (while patentability has nothing to do with the kind of problem that is solved, but instead on the solution employed) and finally it also repeats the mistakes of the CEC original.

#Am5: Wenn demzufolge auch eine computerimplementierte Erfindung von Natur aus einem Gebiet der Technik zuzurechnen ist, ist es doch wichtig klarzustellen, dass eine Erfindung, die keinen technischen Beitrag zum Stand der Technik leistet, z. B. weil dem besonderen Beitrag die Technizität fehlt, nicht das Kriterium der erfinderischen Tätigkeit erfüllt und somit nicht patentierbar ist. Bei der Prüfung, ob das Kriterium der erfinderischen Tätigkeit erfüllt ist, wird üblicherweise der Ansatz Problem/Lösung verfolgt um festzustellen, dass das zu lösende Problem technischer Art ist. Liegt kein technisches Problem vor, kann nicht davon ausgegangen werden, dass die Erfindung einen technischen Beitrag zum Stand der Technik leistet.

#Am114: Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law.

#Rec13: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Rec13Syn: Im Zusammenhang mit der Patentprüfungspraxis ist diese Aussage nicht das was sie auf den ersten Blick zu sein scheint. Sei erweitert die Patentierbarkeit sogar im Vergleich zur Praxis des EPA, indem sie die Patentierbarkeit von nicht-technischen Lösungen zu techischen Problemen zuläßt. Darüberhinaus macht sie Algorithmen in allen Situationen patentierbar, da ein %(q:technisches Problem) immer in Form von allgemeiner Rechnerhardware beschrieben werden kann, womit zugleich Algorithmen auch in ihrer abstraktesten Form zuläßt.

#Rec13aSyn: Die syntaktische Zweideutigkeit dieser Erwägung läßt es unklar, ob Geschäftsmethoden allgemein eine %(q:Methode sein sollen, in der der einzige Beitrag zum Stand der Technik nicht-technisch ist) oder ob bestimmte Geschäftsmethoden %(q:technische Beiträge) enthalten könnten. Wichtiger, dieser Änderungsantrag schließt nicht wirklich etwas aus, da Patentanwälte darauf hinweisen können, dass einige computerbezogene Eigenschaften charakeristisch für die %(q:Erfindung) sind, welche, wie an anderer Stelle in diesem Entwurf festgestellt, %(q:als Ganze betrachtet) werden muss. Damit die Definition von irgendeinem Nutzen ist, muss die syntaktische Zweideutigkeit um die %(q:andere Methode) entfernt werden, die Kategorie der %(q:nicht-technischen Methoden) müsste durch Defitionen und/oder Beispiele erläutert werden.

#Am6: Allerdings reicht allein die Tatsache, dass eine ansonsten nicht patentierbare Methode in einer Vorrichtung wie einem Computer angewendet wird, nicht aus, um davon auszugehen, dass ein technischer Beitrag geleistet wird. Folglich kann eine computerimplementierte Geschäftsmethode oder andere Methode, bei der der einzige Beitrag zum Stand der Technik nichttechnischen Charakter hat, keine patentierbare Erfindung darstellen.

#Rec13bSyn: Dies erklärt eine Position des gesunden Menschenverstandes, die oft übergangen wurde, um Patentierbarkeit zu erweitern. Hierdurch alleine wird nicht viel erreicht, und die Behauptung, dass das Gesetz %(q:nicht hintergangen) werden kann, ist ein frommer Wunsch. Jedoch, auch ein Wunsch ist besser als garnichts.

#Am7: Bezieht sich der Beitrag zum Stand der Technik ausschließlich auf einen nichtpatentierbaren Gegenstand, kann es sich nicht um eine patentierbare Erfindung handeln, unabhängig davon, wie der Gegenstand in den Patentansprüchen dargestellt wird. So kann beispielsweise das Erfordernis eines technischen Beitrags nicht einfach dadurch umgangen werden, dass in den Patentansprüchen technische Hilfsmittel spezifiziert werden.

#Am8: Außerdem ist ein Algorithmus von Natur aus nichttechnischer Art und kann deshalb keine technische Erfindung darstellen. Allerdings kann eine Methode, die die Benutzung eines Algorithmus umfasst, unter der Voraussetzung patentierbar sein, dass die Methode zur Lösung eines technischen Problems angewandt wird. Allerdings würde ein für eine derartige Methode gewährtes Patent kein Monopol auf den Algorithmus selbst oder seine Anwendung in einem von dem Patent nicht betroffenen Kontext verleihen.

#Rec13dSyn1: Dieser Änderungsantrag wiederholt einfach die Definition von Patentansprüchen: Ansprüche definieren das Ausschlussrecht, das durch das Patent gewährt wird. Es erlaubt Ansprüche auf Software, indem es nur auf allgemeine Computerausrüstung oder Computerprozesse verweist, egal wo die Innovation liegt. Genauso gut kann man sagen: beanspruchen Sie, was Sie wollen, solange Sie ein Vokabular wie Speicher, Prozessor, oder Apparat irgendwo in den Ansprüchen definieren, und achten Sie darauf, Ihren Anspruch so breit wie möglich zu formulieren, denn für das was nicht beansprucht wurde, erhalten Sie kein Monopolrecht.

#Rec13dSyn2: Dieser Änderungesantrag könnte einen indirekten Nutzen haben: er scheint dem Änderungsantrag 18 an Artikel 5 zu widersprechen, da er sagt dass nur programmierte Vorrichtungen und Prozesse beansprucht werden können.

#Am9: Der Anwendungsbereich der ausschließlichen Rechte, die durch ein Patent gewährt werden, wird durch die Patentansprüche definiert. Patentansprüche auf computerimplementierte Erfindungen müssen unter Bezugnahme entweder auf ein Erzeugnis wie beispielsweise eine programmierte Vorrichtung oder ein Verfahren, dass in einer solchen Vorrichtung verwirklicht wird, angemeldet werden. Werden demnach einzelne Software-Elemente in einem Kontext benutzt, bei dem es nicht um die Verwirklichung eines rechtmäßig beanspruchten Erzeugnisses oder Verfahrens handelt, stellt eine solche Verwendung keine Patentverletzung dar.

#Rec14: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#Rec14Syn1: Diese Erwägung der Kommission ist nicht ganz schlecht (obwohl die Kommissionserwägung den Begriff der %(q:computer-implementierten Erfindung verwendet), siehe Artikel 1). Sie sagt einfach, dass das Patentrecht nicht ersetzt werden soll. Der Änderungsantrag 10 ist weniger klar. Auf der einen Seite setzt es die %(q:derzeitige Rechtslage) mit den %(q:Praktiken des Europäischen Patentamts gleich). On the other it says that the patentability of business methods is to be avoided. However the EPO itself has been granting many patents for business method and has provided %(a6:legal reasoning to support business method patents).  The only possible effect of the amendment, if any, would be to surrender legislative responsability to the EPO, whose practices would seem to define patentability instead of abiding to the laws voted by the European Parliament and other democraticaly representative bodies.

#Am86Syn: Amendment 86 does not ask for a codification of the current EPO practice, but on the other hand talks about %(q:unpatentable methods such as trivial procedures and business methods). Not just trivial business methods must remain unpatentable, but all business methods must remain so (otherwise we are deviating from Art 52 EPC, which should be avoided as suggested in amendment 88).

#Am10: Um computerimplementierte Erfindungen rechtlich zu schützen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage mit Blick auf die Praxis des Europäischen Patentamts klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarkeit zu gewährleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Geschäftsmethoden, als patentfähig zu erachten.

#Am86: Um computerimplementierte Erfindungen rechtlich zu schützen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarheit zu gewährleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Trivialvorgänge und Geschäftsmethoden, als patentfähig zu erachten.

#Rec16: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#Rec16Syn: Unification of case law in itself is not a guarantee of improvement of the situation of European industry. This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries. Whether or not software patents are good for European software development companies is independent of the fact that the traditional manufacturing industry is moving to low-cost economies outside the EU. Amendment 11 and the CEC text assume that extending patent protection to the software development industry will improve the competitiveness of EU companies, even though 75% of the 30,000 already granted software patents (which CEC/JURI call patents on %(q:computer-implemented inventions)) are in hands of US and Japanese companies. Additionally, if Europe does not have software patents, European companies still can perfectly acquire them in the US and elsewhere and enforce them there, while the affected foreign companies cannot do the same here. In that sense, not having software patents in Europe is a competitive advantage.

#Am11: Die Wettbewerbsposition der europäischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern wird sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausgeräumt sind und die Rechtslage transparenter ist. Beim derzeitigen Trend der klassischen verarbeitenden Industrie zur Verlagerung ihrer Betriebe in Niedriglohnländer außerhalb der Europäischen Union liegt die Bedeutung des Urheberrechtsschutzes und insbesondere des Patentschutzes auf der Hand.

#Am35: The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: Die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 des Vertrags sollen durch diese Richtlinie unberührt bleiben.

#Rec18: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Rec18Syn: The CEC proposal only pretends to protect interoperability, but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendment 13 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.

#Am13: Rechte, die aus Patenten erwachsen, die für Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unberührt von urheberrechtlich zulässigen Handlungen gemäß Artikel 5 und 6 der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen, insbesondere gemäß den Vorschriften in Bezug auf die Dekompilierung und die Interoperabilität. Insbesondere erfordern Handlungen, die gemäß Artikel 5 und 6 jener Richtlinie keine Genehmigung des Rechtsinhabers in Bezug auf dessen Urheberrechte an dem oder in Zusammenhang mit dem Computerprogramm erfordern, für die aber ohne Artikel 5 oder 6 jener Richtlinie eine solche Genehmigung erforderlich wäre, keine Genehmigung des Rechtsinhabers in Bezug auf die Patentrechte des Rechtsinhabers an dem oder in Zusammenhang mit dem Computerprogramm.

#Rec18bSyn: Diese Erwägung legt nahe dass ein Computerprogramm eine Erfindung sein kann (da es keine anderen Innovationen gibt welche Quellcode haben oder die man dekompilieren kann), was Artikel 52 EPÜ wiederspricht, da dieser Computerprogramme explizit von der Patentierbarkeit ausnimmt. Software kann niemals Teil einer Erfindung sein. Sie kann in den Ansprüchen des Patentes erwähnt werden, aber in diesem Fall dient das lediglich dazu die Anwendung der jeweiligen Erfindung klarzustellen, und den Bereich des Monopols, welches es Patenthalter gewährt, in seinem Umfang zu beschränken (wenn es z.B. jmd. schafft die Erfindung ohne ein Computerprogramm zu benutzen, dann wird er nicht das Patent verletzen welches nur die Benutzung von besagter Erfindung in Kombination mit einem Computerprogramm beschreibt).

#Am74: Die Anwendung dieser Richtlinie darf nicht dazu führen, dass die herkömmlichen Grundsätze des Patentrechts umgangen werden, was bedeutet, dass der Patentanmelder die Beschreibung aller Elemente seiner Erfindung beifügen muss, einschließlich des Quellcodes, und dass dessen Untersuchung und somit dessen Dekompilierung gestattet sein muss. Dieses Verfahren ist notwendig, um obligatorische Lizenzen zu ermöglichen, wenn z.B. die Verpflichtung nicht eingehalten wird, die Versorgung des Marktes zu gewährleisten.

#Rec18tSyn: Diese Erwägung legt ebenfalls nahe dass auch Computerprogramme Erfindungen sein können.

#Am75: In jedem Fall muss durch die Rechtsvorschriften der Mitgliedstaaten sichergestellt werden, dass die Patente Neuheiten und einen erfinderischen Beitrag beinhalten, um zu verhindern, dass eine Aneignung von bereits allgemein bekannten Erfindungen allein auf Grund ihrer Aufnahme in ein Computerprogramm erfolgt.

#pmd: Europarl 2003/09: Änderungsanträge

#eus: Hier sind die Anträge in verschiednen Sprachen veröffentlicht.

#ata: Viele der Teilnehmer, einschließlich derer, die von einem solchen Neuheitsschonfrist eigentlich profitieren sollten, äußern Zweifel über dieses zweischneideige Konzept.

#enW: Eine andere Version der publizierbaren Änderungsanträge, manchmal etwas aktueller als dieser Seite.

#soW: Results of the Vote

#Wan: Öffentliche Dokumente über die Plenarabstimmung

#aaW: Von verschiedenen Gruppen eingereichte Anträge und Analysen davon, soweit veröffentlichbar

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Wie Sie beim Übersetzen helfen können

#uhn: Viele MdEP legen darauf wert, diese Anlayse in ihrer Sprache zu bekommen

#diW: Diese Seite wird mit einem Mehrsprachigkeitsverwaltungssytem betrieben, welches es Ihnen leicht macht, beizutragen, sofern sie bestimmte Regeln beachten.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

