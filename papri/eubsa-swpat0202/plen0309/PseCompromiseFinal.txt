Preamble

In addition to supporting the amendments below, do not support the following JURI
amendments: Am4 to recital 11, am5 to recital 12, Am8 to recital13c, Am11 to recital 16
and Am13 to recital 18 (please vote for amendments that solve their problems in a
thorough way).

The big problem with this directive and the tabled JURI amendments, is that there is no
"quick fix" possible, where you would change one or two recitals and one or two articles
and then can say that the directive is "fixed". What we propose here, is really a
minimalist set of amendments that is required to prevent pure software patents from
being granted and at the same time keep the directive more or less consistent.

This compromise contains most of the amendments signed by Arlene McCarthy and
Manuel Medina Ortega, although some have been slightly modified (such modifications
are underlined and explained).





                                           1/19                                             2


    Commission proposal                                          Amendments

                                     Recital

                                                La aplicaci�n de esta directiva no puede
                                                suponer una quiebra de los principios
                                                tradicionales del Derecho de patentes, lo
                                                cual significa que el solicitante de la
                                                patente debe incluir la descripci�n de
                                                todos los elementos de su invenci�n
                                                incluyendo el c�digo fuente y se deber�
                                                permitir la investigaci�n sobre el mismo y
                                                por lo tanto, su descompilaci�n.  Este
                                                mecanismo resulta necesario para
                                                permitir las licencias obligatotias por
                                                ejemplo cuando se incumpla con la
                                                obligaci�n de tener abastecido el
                                                mercado.


                                   Justification

La patentabilidad de las invenciones implementadas por ordenador debe respetar los
                principios que son propios del Derecho de patentes





                                       2/19                                                 2


    Commission proposal                                          Amendments

                                      Recital

                                                 En todo caso la legislaci�n de los Estados
                                                 Miembros deber� asegurarse que las
                                                 patentes contengan novedades y una
                                                 aportaci�n inventiva para impedir que
                                                 invenciones que ya se encuentran en el
                                                 dominio p�blico sean apropiadas por el
                                                 mero hecho de incorporarse a un
                                                 programa de ordenador.


                                   Justification

La patentabilidad de las invenciones implementadas por ordenador debe respetar los
                principios que son propios del Derecho de patentes.





                                       3/19                                                2


         Commission proposal                                               Amendments

                                               Recital 7

(7) Under the Convention on the Grant of                 (7) Under the Convention on the Grant of
European Patents signed in Munich on                     European Patents signed in Munich on
5 October 1973 and the patent laws of the                5 October 1973 and the patent laws of the
Member States, programs for computers                    Member States, programs for computers
together with discoveries, scientific                    together with discoveries, scientific
theories, mathematical methods, aesthetic                theories, mathematical methods, aesthetic
creations, schemes, rules and methods for                creations, schemes, rules and methods for
performing mental acts, playing games or                 performing mental acts, playing games or
doing business, and presentations of                     doing business, and presentations of
information are expressly not regarded as                information are expressly not regarded as
inventions and are therefore excluded from               inventions and are therefore excluded from
patentability. This exception, however,                  patentability. This exception applies
applies and is justified only to the extent              because the said subject-matter and
that a patent application or patent relates              activities do not belong to a field of
to such subject-matter or activities as                  technology.
such, because the said subject-matter and
activities as such do not belong to a field
of technology.


                                           Justification

  A clarification in accordance with article 52 of the EPC. The fact that computer
  programs as such are excluded from patentability, means that adding a computer
  program to an otherwise patentable invention does not render said invention
  unpatentable. It does not mean that adding a computer program to anything else
  (technical or not) makes the computer program patentable, as this recital seems to
  suggest.





                                                 4/19                                               2


          Commission proposal                                       Amendments by Parliament


                                              Recital 11

(11) Although computer-implemented                       While computer programs are abstract
inventions are considered to belong to a                 and do not belong to any particular field,
field of technology, in order to involve an              they are used to describe and control
inventive step, in common with inventions                processes in all fields of applied natural
in general, they should make a technical                 and social science.
contribution to the state of the art


                                             Justification

  The Commission text declares computer programs to be technical inventions.  It removes
  the independent requirement of invention ("technical contribution") and merges it into
  the requirement of non-obviousness ("inventive step").  This leads to theoretical
  inconsistency and undesirable practical consequences, as explained in detail in the
  justification of our amendment to 4(2).





                                                 5/19                                              2


       Commission proposal                                           Amendments

                                    Recital 11 (a) (new)

                                                    11 (a) Innovations are only patentable if
                                                    they may be considered to belong to a
                                                    field of technology and, in addition, are
                                                    new, involve an inventive step and are
                                                    susceptible of industrial application.


                                        Justification

In order to create legal certainty, the patent law principles of novelty, inventive step and
industrial application (Articles 54, 56 and 57 of the EPC) should be incorporated in the
recitals.

                                           Notes

The reason for changing "inventions" to "innovations", is that an invention per
definition belongs to a field of technology (since invention is a synonym for "technical
contribution" in patent law). The general all-encompassing term is "innovation".
Another possibility is to change "Inventions are only patentable if" to "Innovations are
only patentable inventions if".





                                            6/19                                               2


         Commission proposal                                               Amendments

                                              Recital 12

Accordingly, where an invention does not                 Accordingly, an innovation that does not
make a technical contribution to the state               make a technical contribution to the state
of the art, as would be the case, for                    of the art is not an invention in the sense
example, where its specific contribution                 of patent law.
lacks a technical character, the invention
will lack an inventive step and thus will
not be patentable.


                                             Justification

  The European Commission text merges the "technical invention" test into the "inventive
  step" test, thereby weakening both tests and opening an infinite space of interpretation.
  This deviates from Art 52 EPC. It is theoretically inconsistent, and leads to undesirable
  practical consequences, such as making examination at some national patent offices
  infeasible. See also the discussion for recital 11 (a) (new) and the "What's in a name"
  document.





                                                 7/19                                             2


         Commission proposal                                           Amendments

                                            Recital 13

(13) A defined procedure or sequence of                Deleted
actions when performed in the context of
an apparatus such as a computer may
make a technical contribution to the state
of the art and thereby constitute a
patentable invention. However, an
algorithm which is defined without
reference to a physical environment is
inherently non-technical and cannot
therefore constitute a patentable
invention.


                                           Justification

  It is contrary to the goal of clarification to try to make an artificial distinction between
  mathematical algorithms and nonmathematical algorithms. To a computer scientist, this
  makes no sense, because every algorithm is as mathematical as anything could be. An
  algorithm is an abstract concept unrelated to physical laws of the universe.





                                               8/19                                                2


         Commission proposal                                           Amendments

                                      Article 2, point (a)

(a) "computer-implemented invention"                   (a) "computer-implemented invention"
means any invention the performance of                 means any invention in the sense of the
which involves the use of a computer,                  European Patent Convention the
computer network or other programmable                 performance of which involves the use of a
apparatus and having one or more prima                 computer, computer network or other
facie novel features which are realised                programmable apparatus and having in its
wholly or partly by means of a computer                implementations one or more non-
program or computer programs;                          technical features which are realised
                                                       wholly or partly by a computer program or
                                                       computer programs, besides the technical
                                                       features that any invention must
                                                       contribute;


                                           Justification

  Art 52 of the EPC clearly states that a stand-alone computer program (or a "computer
  program as such") cannot constitute a patentable invention. This amendment clarifies
  that an innovation is only patentable if conforms to Art 52 of the EPC, regardless of
  whether or not a computer program is part of its implementation.





                                               9/19                                              2


         Commission proposal                                               Amendments

                                             Article 2 (b)

"technical contribution" means a                         "technical contribution", also called
contribution to the state of the art in a                "invention", means a contribution to the
technical field which is not obvious to a                state of the art in a technical field. The
person skilled in the art.                               technical character of the contribution is
                                                         one of the four requirements for
                                                         patentability. Additionally, to deserve a
                                                         patent the technical contribution has to be
                                                         new, non-obvious, and susceptible of
                                                         industrial application.


                                             Justification

     The Commission text merges the invention (technical contribution) test with the non-
     obviousness (inventive step) test, thereby weakening both tests, deviating from Art 52
                              EPC, and creating practical problems.





                                                10/19                                                   2


      Commission proposal                                           Amendments

                               Article 2, point (ba) (new)

                                                    (ba) "technical field" means an industrial
                                                    application domain requiring the use of
                                                    controllable forces of nature to achieve
                                                    predictable results. "Technical" means
                                                    "belonging to a technical field". The use
                                                    of forces of nature to control physical
                                                    effects beyond the numerical
                                                    representation of information belongs to a
                                                    technical domain. The production,
                                                    handling, processing, distribution and
                                                    presentation of information do not belong
                                                    to a technical field, even when technical
                                                    devices are employed for such purposes.


                                      Justification

  The fact that a programmable apparatus, such as a generic computer, makes use of
   physical effects in order to process information should not be used to allow patent
               protection to the program running on such an apparatus.

This amendment synthesises Am. 16 Cult, Am. 19 Cult, Am. 23 Itre, Am. 24 Itre. and Am.
                                         25 Itre





                                         11/19                                                  2


      Commission proposal                                             Amendments

                                Article 2, point (bb) (new)

                                                      (bb) "industry" in the sense of patent law
                                                      means "automated production of material
                                                      goods";


                                       Justification

The  word  "industry"  is  nowadays  often  used  in  extended  meanings  which  are  not
appropriate in the context of patent law.





                                             12/19                                           2


         Commission proposal                                              Amendments

                                       Article 4, paragraph 1

1. Member States shall ensure that a                     1. Member States shall ensure that patents
computer-implemented invention is                        are granted only for technical inventions
patentable on the condition that it is                   which are new, non-obvious and
susceptible of industrial application, is                susceptible of industrial application
new and involves an inventive step.


                                             Justification

  Article 4(1) should be coherent with the amended version of Article 2. There must not be
       distinctions between patentable and non-patentable inventions. This amendment
              synthesises Am. 11 Cult, Am. 20 Cult, Am. 28 Itre, and Am. 29 Itre.





                                                13/19                                              2


          Commission proposal                                              Amendments


                                     Article 4, paragraph 2

2. Member States shall ensure that it is a                2. Member States shall ensure that patents
condition of involving an inventive step                  on computerised innovations are upheld
that a computer-implemented invention                     and enforced only if they were granted
must make a technical contribution.                       according to the rules of Art 52 of the
                                                          European Patent Convention of 1973, as
                                                          explained in the European Patent Office's
                                                          Examination Guidelines of 1978.


                                              Justification

  This amendment avoids deviation from the European Patent Convention and therefore
  provides increased coherence and clarity.





                                                 14/19                                                2


         Commission proposal                                               Amendments

                                      Article 4, paragraph 3

3. The technical contribution shall be                    3. The technical contribution shall be
assessed by consideration of the difference               assessed by consideration of the difference
between the scope of the patent claim                     between all of the technical features of the
considered as a whole, elements of which                  patent claim and the state of the art.
may comprise both technical and non-
technical features, and the state of the art.


                                           Justification

     The wording of this article is self-contradictory, as it seems to state that a technical
  contribution may consist of non-technical features. One should ensure that the conditions
     of novelty and inventive step regard the technical contribution, otherwise any novel
             software running on a non-novel technical device could be patentable

           This amendment synthesises Am. 32 Itre and Am. 33 Itre. (was: JURI 52)





                                                 15/19                                               2


      Commission proposal                                           Amendments

                               Article 4, paragraph 4 (new)

                                                   In determining whether a given computer-
                                                   implemented invention makes a technical
                                                   contribution the following test shall be
                                                   used:

                                                   whether it constitutes a new teaching on
                                                   cause-effect relations in the use of
                                                   controllable forces of nature and has an
                                                   industrial application in the strict sense of
                                                   the expression, in terms of both method
                                                   and result.



                                       Justification


                                          Notes

We still think this amendment leaves loopholes (see analysis, the most glaring defect  can
be fixed by changing "shall be used" to "must be passed"), but those are closed in this
compromise proposal by the other amendments tabled for articles 2 and 4.





                                          16/19                                              2


         Commission proposal                                            Amendments

                                             Article 5

Member States shall ensure that a                      Member States shall ensure that a
computer-implemented invention may be                  computer-implemented invention may be
claimed as a product, that is as a                     claimed only as a product, that is a set of
programmed computer, a programmed                      equipment comprising both
computer network or other programmed                   programmable apparatus and devices
apparatus, or as a process carried out by              which use forces of nature in an inventive
such a computer, computer network or                   way, or as a technical production process
apparatus through the execution of                     operated by such a computer, computer
software.                                              network or apparatus through the execution
                                                       of software

                                         Justification

    The original wording of this article is confusing, since allowing to patent programmed
  generic computers would be equivalent to allowing to patent their software as such. Also,
      one must make sure that the production of information cannot be considered as an
                                 industrial production process.

     This amendment synthesises Am. 24 Cult, Am. 25 Cult, Am. 37 Itre and Am. 38 Itre.

                                              Notes

    We suggest this instead of (or at least complementary to) AM65 JURI, as AM65 states
      that computer programs cannot appear in patent claims, even though according to
  traditional patent law they can be used (just like any other non-patentable subject matter)
   to more clearly define the monopoly granted by the patent and as such to limit the scope
                                         of the claims.





                                              17/19                                              2


        Commission proposal                                         Amendments


                               Article 5 paragraph 2 (new)

                                                   Member States shall ensure that patent
                                                   claims granted in respect of computer-
                                                   implemented inventions include only the
                                                   technical contribution which justifies the
                                                   patent claim. A patent claim to a
                                                   computer program, either on its own or
                                                   on a carrier, shall not be allowed.

                                       Justification

A patent claim to a computer program as such used in connection with patent-protected
computer-implemented inventions, or to its recording on a carrier, is not permissible. A
`text claim' to computer programs in source or binary code would increase legal
uncertainty, as simply possessing an expression or a carrier with a protected program
would mean infringing patent law. The availability of a program in text form may be seen
as a contribution to the disclosure requirement in respect of functionalities of computer
programs for which patent claims are granted.

                                          Notes

                                See previous amendment.





                                          18/19                                               2


        Commission proposal                                              Amendments

                                             Article 7

7. The Commission shall monitor the                     7. The Commission shall monitor the
impact of computer-implemented                          impact of computer-implemented
inventions on innovation and competition,               inventions on innovation and competition,
both within Europe and internationally, and             both within Europe and internationally, and
on European businesses, including                       on European businesses, especially small
electronic commerce.                                    and medium-sized enterprises and the
                                                        open source community, and electronic
                                                        commerce.

                                                        The Commission shall examine the
                                                        question of  how to make patent
                                                        protection more readily accessible to small
                                                        and medium-sized enterprises and ways of
                                                        assisting them with the costs of obtaining
                                                        and enforcing patents in particular
                                                        through the creation of a defence fund
                                                        and the introduction of special rules on
                                                        legal costs. It shall report on its findings
                                                        to the European Parliament and the
                                                        Council and present appropriate
                                                        proposals for legislation without delay.

                                         Justification




                                               Notes

  We still don't think this is a good idea (since there are no working examples of such
  system, and since the theoretical possibility as suggested by a consultants report to DG
  MARKT is also based on wrong assumptions), but we are prepared to compromise on
  this amendment.





                                               19/19                                               2


