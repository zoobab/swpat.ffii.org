<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: If you want to keep the sphere of logics free from patents while allowing patentability of new washing machines and mobile phones, regardless of whether a computer program is involved in them, you'd better vote according to these recommendations.
#title: Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs
#VotlstX: %1 Voting Recommendations
#oth: Who voted how?
#hii: Analysis of the voting behavior, sorted by parties and closeness to FFII recommendations.  Green means in favor of freedom and innovation, red in favor of red tape and litigation.
#lfo: Analysis of MEP voting
#rFs: Report for all MEPs from Belgium. If they completely followed the %(vl:FFII voting list), they get 100%.  A similar analysis is under way for all MEPs.  First results show that many EPP deputies followed Kauppi (against software patents) rather than Wuermeling (pro software patents).
#tav: voting data csv file
#uor: raw input for all kinds of analyses and presentations, with some scripts by Tapani Tarvainen.
#hoW: Who voted how
#sro: Tabular listing based on %(mw:MSWord original from the Europarl website).
#lWd: detailed analysis of the amendments.
#hsn: get hold of missing voting lists
#leU: We still don't have those of Wuermeling, UEN and EDD.
#cot: produce HTML versions of the voting lists
#bWW: This can be done with OpenOffice on the basis of the MSWord versions.
#pWc: Contact plen0309 at ffii org if you intend to do this work.
#evh: Produce more databases and reports to visualise the behavior of the groups and MEPs
#tdl: In particular we need two tabular listings:
#mdn: amendment
#aga: language
#tex: text
#mdn2: amendment
#vot: vote
#rye: Prepare for next year's EP elections
#rae: We should bring the subject of IT infrastructure policy into the election campaign.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/plen0309.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: plenvote0309 ;
# txtlang: en ;
# End: ;

