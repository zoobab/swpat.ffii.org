<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl 2003/09 Τροποποιήσεις Σύστασης Διπλωμάτων Ευρεσιτεχνίας Λογισμικού: Πραγματικά έναντι Πλαστών Ορίων

#descr: Το Ευρωπαϊκό Κοινοβούλιο προγραμματίζει να αποφασίσει για την Οδηγία για τα Διπλώματα Ευρεσιτεχνίας λογισμικού στις 24 Σεπτεμβρίου. Η οδηγία όπως προτείνεται από την Ευρωπαική Επιτροπή κατεδαφίζει τη βασική δομή του υπάρχοντος νόμου (’ρθρο 52 της Ευρωπαϊκής Συμφωνίας για τα Διπλωμάτα Ευρεσιτεχνίας) (EPC) και την αντικαθιστά με το Τριμερές Πρότυπο που προέκυψε με τη συνεργασία των Γραφείων Διπλωμάτων Ευρεσιτεχνίας των ΗΠΑ, της Ευρώπης και της Ιαπωνίας το 2000, σύμφωνα με το οποίο όλες οι λύσεις προβλημάτων %(q:υλοποιημένες με υπολογιστή) είναι εφευρέσεις που μπορούν να πάρουν δίπλωμα ευρεσιτεχνίας.  Κάποια μέλη του Κοινοβουλίου έχουν προτείνει τροποποιήσεις που στοχεύουν στην υποστήριξη της ακριβούς έννοιας της εφεύρεσης σύμφωνα με την Ευρωπαϊκή Συμφωνία για τα Διπλωμάτα Ευρεσιτεχνίας, ενώ άλλα πιέζουν για απεριόριστη δυνατότητα διπλωμάτων ευρεσιτεχνίας σύμφωνα με το Τριμερές Πρότυπο, ενδεδυμένη όμως με μια ρητορεία για περιορισμούς.  Προσπαθούμε να δώσουμε μια συγκριτική ανάλυση όλων των προτεινόμενων τροποποιήσεων, ώστε να βοηθήσουμε τους ιθύνοντες να αναγνωρίσουν αν ψηφίζουν για πραγματικά ή πλαστά όρια στα δικαιώματα κατοχύρωσης ευρεσιτεχνίας.

#Lgn: Υπόμνημα και Σχολιασμένο Παράδειγμα

#ril: ’ρθρα και Προτάσεις

#eia: Απόρριψη/Αποδοχή, Τίτλος και Απαγγελίες

#enm: Αριθμός τροποποίησης (για σκοπούς αναφοράς σε αυτό το κείμενο, οι πραγματικοί αριθμοί τροποποιήσεων στους πίνακες θα είναι πιθανώς διαφορετικοί)

#jac: υπέρ, εναντίον, αναποφάσιστοι

#tuW: 410 ψήφισαν υπέρ, 178 ψήφισαν εναντίον

#TitTit: Τίτλος

#TitCec: Πρόταση για μια Οδηγία του Ευρωπαϊκού Κοινοβουλίου και της Επιτροπής για τη δυνατότητα κατοχύρωσης ευρεσιτεχνίας για εφευρέσεις υλοποιημένες σε υπολογιστή

#trW: κείμενο της %(op:αρχικής πρότασης της Ευρωπαϊκής Επιτροπής)

#TitSyn: Δεν μπορεί να είναι σκοπός της τρέχουσας οδηγίας να ανακυρήξει όλων των ειδών τις ιδέες %(q:υλοποιημένες με υπολογιστή) εφευρέσεις με δικαίωμα ευρεσιτεχνίας. Σκοπός είναι μάλλον να ξεκαθαρίσει τα όρια του δικαιώματος ευρεσιτεχνίας σχετικά με την αυτόματη επεξεργασία δεδομένων και τα διάφορα (τεχνικά και μη) πεδία εφαρμογής της, και αυτό πρέπει να εκφράζεται στον τίτλο με απλή και μη διφορούμενη διατύπωση.

#ung: Δείγμα αιτιολόγησης/σχόλια

#enm2: Αριθμός τροποποίησης

#uia: Καταθέτης

#amd: Πρόταση ψήφου

#ogs: αποτέλεσμα ψηφοφορίας

#Am29: Proposal for a Directive of the European Parliament and of the Council on the limits of patentability with respect to automated data processing and its fields of application

#eet: τροποποιημένο κείμενο

#tIa: Patent Insurance Extensions

#Art1: Αυτή η Οδηγία θέτει κανόνες για τα δικαιώματα ευρεσιτεχνίας εφευρέσεων υλοποιημένων σε υπολογιστή.

#nWw: Ιδέες υλοποιημένες σε γενικό υλικό υπολογιστή δεν είναι εφευρέσεις κατά την έννοια του ’ρθρου 52(2) της EPC. Ο όρος %(q:εφεύρεση υλοποιημένη σε υπολογιστή) δεν είναι γνωστός σε ανθρώπους έμπειρους στο αντικείμενο, και καθώς τα μόνα πράγματα που μπορούν να υλοποιηθούν σε έναν υπολογιστή είναι προγράμματα υπολογιστή (ένα πλυντήριο ή ένα κινητό τηλέφωνο δεν μπορούν να υλοποιηθούν σε έναν υπολογιστή), υπαινίσσεται ότι τα προγράμματα υπολογιστών μπορεί να είναι εφευρέσεις. Εισήχθη από τον EPO το 2000 στο διαβόητο %(e6:Παράρτημα 6 του εγγράφου στον Τριμερή Δικτυακό Τόπο) προκειμένου να ευθυγραμμίσει τον Ευρωπαϊκό νόμο διπλωμάτων ευρεσιτεχνίας με αυτούς των ΗΠΑ και Ιαπωνίας και να επιτρέψει διπλώματα ευρεσιτεχνίας για %(q:επιχειρηματικές μεθόδους υλοποιημένες σε υπολογιστή), σε αναμονή της διαγραφής του ’ρθρου 52(2) EPC.

#Am116: This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs.

#Art2a: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: Το αρθρο 52 της EPC δηλώνει καθαρά ότι ένα μεμονωμένο πρόγραμμα υπολογιστή δεν μπορεί να αποτελεί μια εφεύρεση με δικαίωμα κατοχύρωσης ευρεσιτεχνίας.

#fet: Οι τροποποιήσεις 36=42=117 διευκρινίζουν ότι μια καινοτομία δικαιούται κατοχύρωση ευρεσιτεχνίας αν είναι σύμφωνη με το ’ρθρο 52 της EPC, άσχετα με το άν ένα πρόγραμμα υπολογιστή είναι μέρος της υλοποίησής της. Στην περίπτωση της τροποποίησης 14, το εξής συμφωνεί με τον ορισμό που δίνεται: %(q:μια εφεύρεση η λειτουργία της οποίας απαιτεί τη χρήση υπολογιστή και όλα της τα χαρακτηριστικά πραγματοποιούνται μέσω ενός προγράμματος υπολογιστή). Ως έχει, υπονοεί ότι τα προγράμματα υπολογιστή μπορεί να είναι εφευρέσεις, παρόλο που αυτό έρχεται σε αντίθεση με το άρθρο 52 EPC.

#Am36: %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

#Am14: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;

#Art2b: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Art2bSyn1: Μια οδηγία που κάνει το δικαίωμα κατοχύρωσης ευρεσιτεχνίας να εξαρτάται από τη λέξη %(q:τεχνική συνεισφορά) πρέπει να εξηγεί καθαρά αυτόν τον όρο.

#Art2bSyn96: Η τροποποίηση 96 φαίνεται να υποδηλώνει ότι μόνο το πρόβλημα αλλά όχι η λύση χρειάζεται να είναι τεχνική. Η λέξη %(q:σημαντικό) δεν έχει νομική έννοια και οδηγεί μάλλον σε σύγχιση παρά σε διευκρίνιση. Προφανώς το %(q:τεχνικό) πρέπει να οριστεί ξεκάθαρα αν είναι να έχει κάποιο χρήσιμο αποτέλεσμα. Το να απαιτείται η επίλυση ενός τεχνικού προβλήματος είναι εξίσου κακό ή χειρότερο με την CEC. Δεν πρέπει να εστιάζουμε την προσοχή στην εφαρμογή (το πρόβλημα που επιλύεται), αλλά στη λύση (το αποτέλεσμα της εμπειρικής έρευνας που μπορεί να δικαιούται προστασία ευρεσιτεχνίας). Κάθε λογισμικό μπορεί να λέγεται ότι λύνει κάποιο πρόβλημα, αλλά αν η λύση καινοτομεί μόνο στο λογισμικό, δεν μπορεί να λεχθεί ότι είναι μια τεχνική λύση (τουλάχιστον μια νέα τεχνική λύση).

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Τέλος, η τροποποίηση 107 τα θέτει σωστά: δηλώνει καθαρά ότι η %(q:τχνολογική συνεισφορά) είναι συνώνυμο της εφεύρεσης και επαναλαμβάνει τις απαιτήσεις για δυνατότητα κατοχύρωσης του ’ρθρου 52 EPC. Αυτή η τροποποίηση δεν περιορίζει το δικαίωμα κατοχύρωσης ευρεσιτεχνίας με κανέναν τρόπο, απλως επιβεβαιώνει το ’ρθρο 52 EPC και αφαιρεί οποιαδήποτε σύγχιση μπορεί να προέκυπτε από το ανακάτεμα των ελέγχων δυνατότητας κατοχύρωσης, όπως κάνει το αρχικό κείμενο της CEC και οι άλλες τροποποιήσεις.

#Am69: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#Am107: %(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.

#Am96: %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#Art2baSyn: Καθώς η προσπάθεια του εισηγητή να %(q:ξεκαθαρίσει τι δικαιούται κατοχύρωση ευρεσιτεχνίας και τι όχι) εξαρτάται πλήρως από τη λέξη %(q:τεχνικό), αυτός ο όρος πρέπει να οριστεί καθαρά και περιοριστικά. Ο μόνος ορισμός που το καταφέρνει αυτό είναι αυτός κατά το πνεύμα των τροποποιήσεων 55 και 97 (η τροποποίηση 37, που είναι επίσης καλή, θα ανακληθεί για να αποφευχθεί σύγκρουση με αυτές), που βρίσκονται σε διάφορες εθνικές νομολογίες και σε μερικές νομοθεσίες διπλωμάτων ευρεσιτεχνίας (π.χ. Σκανδιναβική Συνθήκη Διπλωμάτων Ευρεσιτεχνίας, και οι νόμοι διπλωμάτων ευρεσιτεχνίας της Πολωνίας, Ιαπωνίας, Ταϊβάν και λοιποί). Αυτός ο ορισμός βασίζεται σε έγκυρες έννοιες της επιστήμης και της επιστημολογίας και έχει αποδειχτεί ότι έχει ξεκάθαρο νόημα στην πράξη. Εξασφαλίζει ότι ευρέα, ακριβά και κατάφορτα με ανασφάλεια δικαιώματα αποκλεισμού όπως τα διπλώματα ευρεσιτεχνίας χρησιμοποιούνται μόνο σε περιοχές που υπάρχει οικονομική δικαιολογία για αυτά, και οτι η καινοτομία στην αφηρημένη σκέψη ανήκει μόνο στο copyright και στα sui generis δικαιώματα όμοια με το copyright. Η λέξη %(q:αριθμητικό) στην τροποποίηση 55 είναι όπως φαίνεται αποτέλεσμα λάθος μετάφρασης από τα Γαλλικά. Ο ασαφής ορισμός που δίνεται εδώ αντιστοιχεί στην παγκόσμια κοινώς αποδεκτή έννοια του όρου, εγγενή στη σημερινή νομολογία και στις άλλες προτάσεις της αναφοράς JURI για το τι πρέπει να δικαιούται κατοχύρωση ευρεσιτεχνίας (π.χ. %(q:όχι το λογισμικό καθεαυτό, αλλά εφευρέσεις σχετικές με κινητά τηλέφωνα, συσκευές ελέγχου κινητήρων, οικιακές συσκευές, ...) ).  Η κατανόηση αυτού πρέπει να γίνει ρητά για λόγους διασάφησης.

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#Am37: %(q:Technology) means %(q:applied natural science).  %(q:Technical) means %(q:concrete and physical).

#Am39Syn: Αυτό είναι ένα κλασικό δόγμα πατεντών στις περισσότερες αρμοδιότητες.  Οι %(q:τέσσερις δυνάμεις της φύσης) είναι μια αναγνωρισμένη Αρχή της επιστημολογίας (θεωρία της επιστήμης). Ενώ τα μαθηματικά είναι θεωρητικά και άσχετα με δυνάμεις της φύσης, κάποιες επιχειρηματικές μέθοδοι μπορεί να εξαρτώνται από τη χημεία των εγκεφαλικών κυττάρων του πελάτη, το οποίο όμως δεν μπορεί να ελεγχτεί, δηλαδή μη-ντετερμινιστικά, υπόκεινται στην ελεύθερη βούληση. Έτσι ο όρος %(q:ελεγχόμενες δυνάμεις της φύσης) αποκλείει ξεκάθαρα το τι πρέπει να αποκλειστεί αλλά και παρέχει αρκετή ευελιξία για εισαγωγή μελλοντικών πεδίων εφαρμοσμένης φυσικής επιστήμης πέρα από τις σήμερα αναγνωρισμένες %(q:δυνάμεις της φύσης). Προσέξτε πως αυτή η τροποποίηση δεν καθιστά εφευρέσεις σ συσκευές όπως κινητά τηλέφωνα και πλυντήρια ανίκανες να κατοχυρωθούν κάτω από κάποιο δίπλωμα ευρεσιτεχνίας. Αντίθετα, προσθέτοντας ένα πρόγραμμα σε μια αλλιώτικα ικανή για πατεντάρισμα εφεύρεση, δεν καθιστά αυτή την εφεύρεση ανίκανη για πατεντάρισμα (μια και η εφεύρεση θα λύνει ακόμη κάποιο πρόβλημα χρησιμοποιώντας ελεγχόμενες δυνάμεις της φύσης, ανεξάρτητα από την παρουσία του προγράμματος του υπολογιστή).

#Am39: %(q:invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature) ;

#Am38Syn: Δεν θέλουμε οι καινοτομίες στη %(q:μουσική βιομηχανία) ή στη %(q:βιομηχανία νομικών υπηρεσιών) να πληρούν τις απαιτήσεις TRIPs της %(q:βιομηχανικής εφαρμογής). Η λέξη %(q:βιομηχανία) σήμερα χρησιμοποιείται συχνά με εκτεταμένες έννοιες που δεν είναι κατάλληλες στο πεδίο της νομοθεσίας διπλωμάτων ευρεσιτεχνίας.

#Am38: %(q:industry) in the sense of patent law means %(q:automated production of material goods);

#Art3: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Art3Syn: Το κείμενο της Ευρωπαϊκής Επιτροπής λέει ότι όλες οι %(q:υλοποιημένες σε υπολογιστή) ιδέες, είναι εφευρέσεις που μπορούν να κατοχυρωθούν κάτω από το Art 27 TRIPs.  Η απόρριψη είναι καλή, η διασαφήνιση του πώς εφαρμόζεται το Art 27 TRIPs θα ήταν καλύτερη.

#Art3aSyn1: Ένας από τους κύριους σκοπούς της δουλειάς με αυτή την οδηγία είναι να διευκρινιστεί η ερμηνεία του άρθρου 52 της EPC κάτω από το πρίσμα του άρθρου 27 του TRIPs, που λέει ότι οι %(q:εφευρέσεις σε όλα τα τεχνολογικά πεδία) πρέπει να επιδέχονται κατοχύρωση ευρεσιτεχνίας. Προτείνουμε μεταγραφή του άρθρου 52(2) EPC στη γλώσα του άρθρου 27 TRIPs, ανάλογα με τις γραμμές της Τροποποίησης CULT-16.

#Art3aSyn2: Αν η επεξεργιασία δεδομένων είναι %(q:τεχνική), τότε τα πάντα είναι %(q:τεχνικά).

#Art3aSyn3: Η επεξεργασία δεδομένων είναι κοινή βάση όλων των πεδίων της τεχνολογίας και εκτός της τεχνολογίας. Με την έλευση του υπολογιστή (γενικής χρήσεως) τη δεκαετία του 1950, η αυτόματη επεξεργασία δεδομένων (ADP) γενικεύτηκε στην κοινωνία και τη βιομηχανία.

#Art3aSyn4: Όπως γράφει το 1977 ο Gert Kolle, ένας κρυφαίος θεωρητικός πίσω από τις αποφάσεις της δεκαετίας του 1970 να μη δικαιούται το λογισμικό κατοχύρωση ευρεσιτεχνίας, (βλέπε Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: Η ADP έχει γίνει σήμερα και θα παραμείνει στο μέλλον ένα αναντικατάστατο βοηθητικό εργαλείο σε όλους τους χώρους της ανθρώπινης κοινωνίας. Είναι πανταχού παρούσα. ... Η καθοριστική της σημασία, η βοηθητική και εξαρτημένη της λειτουργία, διαχωρίζουν την ADP από τα ... διάφορα μεμονωμένα πεδία της τεχνολογίας και την κάνουν να προσομοιάζει σε περιοχές όπως η διοίκηση επιχειρήσεων, της οποίας τα αποτελέσματα και οι μέδοθοι ... απαιτούνται σε όλες τις επιχειρήσεις και για την οποία εκ πρώτης όψεως ενδείκνυται ελεύθερη διαθεσιμότητα.

#Art3aSynCult: Το CULT έκανε καλά να ψηφίσει για ξεκάθαρη εξαίρεση της επεξεργασίας δεδομένων από το χώρο που καλύπτει η %(q:τεχνολογία).

#Art3aSynStud: Πολυάριθμες μελέτες, ορισμένες από τις οποίες διεξάχθηκαν από όργανα της ΕΕ, καθώς και οι γνώμες της Ευρωπαϊκής Οικονομικής και Κοινωνικής Επιτροής και της Ευρωπαϊκής Επιτροπής Περιοχών εξηγούν με λεπτομέρειες γιατί η οικονομία της Ευρώπης θα υποστεί ζημιές αν οι καινοτομίες στην επεξεργασία δεδομένων δεν εξαιρεθούν καθαρά από την κατοχύρωση ευρεσιτεχνιών.

#Am45: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#Art4N1: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Art4N2: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#Art4N3: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#Art4Syn: Αυτό το άρθρο μπερδεύει τη μη-ολοφάνερη δοκιμασία με τη δοκιμασία εφευρετικότητας, και έτσι εξασθενίζει και τα δύο και έρχεται σε σύγκρουση με το Art 52 EPC.

#cpW: JURI has split this article into parts 1, 2 and 3.  This is dangerous because it prevents certain amendments, which may apply to the whole article, from being put to vote.

#iaa: Split into parts 1 2 and 3.

#Art4N1Syn: Οι %(q:εφευρέσεις υλοποιημένες σε υπολογιστή) (δηλ. ιδέες που αποδίδονται σε όρους εξοπλισμού επεξεργασίας δεδομένων γενικής χρήσεως = προγράμματα υπολογιστών) δεν είναι εφευρέσεις σύμφωνα με το ’ρθρο 52 EPC. Δείτε επίσης την ανάλυση για το άρθρο 1. Η τροποποίηση 56=98=109 διορθώνει το σφάλμα και ξαναεκθέτει το ’ρθρο 52 EPC. Καμιά από τις τροποποιήσεις δεν περιορίζει το δικαίωμα κατοχύρωσης ευρεσιτεχνίας, αλλά αυτή η τροποποίηση τουλάχιστον θέτει τα πράγματα τυπικά σωστά.

#Am16N1: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.

#Am98: Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: A proper restatement of Art 52 EPC. As the goal of this directive is to harmonise and clarify, it is of paramount importance to stress the correct interpretation of the EPC, as it is the basis of patent law in Europe.

#Am48: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4N2Syn: Το κείμενο της Επιτροπής παρεκλίνει από το ’ρθρο 52 της EPC. Όπως εξηγείται στην ανάλυση της Αγόρευσης 11, πρέπει κανείς πρώτα να διαπιστώσει αν υπάρχει κάποια εφεύρεση/τεχνική συνεισφορά (είτε χρησιμοποιώντας τον αρνητικό ορισμό του ’ρθρου 52 EPC: ένα πρόγραμμα υπολογιστή δεν είναι εφεύρεση, είτε χρησιμοποιώντας τον θετικό ορισμό που προέξυψε από τη νομολογία, βασισμένο στις ελεγχόμενες δυνάμεις της φύσης). Εν συνεχεία, πρέπει κανείς να ελέγξει εαν η εφεύρεση αυτή περνά τις 3 άλλες δοκιμασίες του ’ρθρου 52, από τις οποίες μια είναι το εφευρετικό βήμα (= το μη προφανές). Το κείμενο της Επιτροπής υποστηρίζει ότι το εφευρετικό βήμα είναι μια απαίτηση για να είναι κάτι εφεύρεση. Καθώς ένα πρόγραμμα υπολογιστή μπορεί να είναι μη προφανές, θα μπορούσει να είναι εφεύρεση (αντίθετα με ό,τι λέει το ’ρθρο 52 EPC).

#Art4N2SynP2: Μάλιστα, το κείμενο της Επιτροπής υπονοεί οτι ιδέες πλαισιωμένες σε όρους υπολογιστή γενικής χρήσεως (προγράμματα υπολογιστών) είναι εφευρέσεις που δικαιούνται κατοχύρωση ευρεσιτεχνίας. Η διαγραφή, όπως προτείνεται από την τροποποίηση 82, θα μπορούσε να βοηθήσει, διότι τα μόνα αποτελέσματα αυτής της παραγράφου είναι να συγχέουν τις δοκιμασίες δικαιώματος κατοχύρωσης και έτσι να κάνουν αδύνατο για τα εθνικά γραφεία διπλωμάτων ευρεσιτεχνίας να απορρίψουν μη νόμιμες αιτήσεις διπλώματος ευρεσιτεχνίας χωρίς σημαντική μελέτη. Η Τροποποίηση 16 κάνει το ίδιο σφάλμα με το κείμενο της επιτροπής, απλώς τακτοποιεί λίγο τη διατύπωση. Η Τροποποίηση 40 θα ανακληθεί, τώρα έχει τεθεί προς συζήτηση ως εισαγωγή (83).

#Am16N2: The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#Art4N3Syn1: Όταν κατοχυρώνετε μια εφεύρεση, ζητάτε δικαιώματα πάνω σε μια συγκεριμένη εφαρμογή αυτής της εφεύρεσης. Αυτή η εφαρμογή είναι που περιγράφεται στα αιτήματα. Ως έχουν, τα αιτήματα (σαν σύνολο) περιλαμβάνουν και την ίδια την εφεύρεση με κάποιον τρόπο, καθώς και έναν αριθμό από χαρακτηριστικά που δεν δικαιούνται κατοχύρωση ευρεσιτεχνίας (όπως, π.χ., κάποιο πρόγραμμα υπολογιστή) τα οποία απαιτούνται για την εν λόγω εφαρμογή της εφεύρεσης. Αυτό σημαίνει ότι ακόμα και αν απαιτείται η εφεύρεση να είναι τεχνική, τα αιτήματα μπορεί να περιέχουν μη τεχνικά χαρακτηριστικά (πράγμα στο οποίο δεν είμαστε αντίθετοι, έτσι λειτουργούσαν πάντα τα διπλώματα ευρεσιτεχνίας).

#Art4N3Syn2: Όμως, η πρόταση της Ευρωπαϊκής Επιτροπής ακυρώνει την ίδια της την έννοια της %(q:τεχνικής συνεισφοράς) επιτρέποντας την εφεύρεση να αποτελείται από μη τεχνικά χαρακτηριστικά. Η εκδοχή της JURI δηλώνει μόνο ότι τα αιτήματα πρέπει να περιέχουν τεχνικά χαρακτηριστικά, αλλά αυτό δεν είναι πραγματικό όριο. Ένα παράδειγμα μπορεί να το κάνει αυτό σαφές. Υποθέστε ότι θέλουμε να κατοχυρώσουμε ευρεσιτεχνία για ένα πρόγραμμα υπολογιστή, και έτσι λέμε ότι αυτό είναι η %(q:τεχνική συνεισφορά). Στα αιτήματα, περιγράφουμε μια εφαρμογή αυτού του προγράμματος, οπότε λέμε ότι κατοχυρώνουμε την εκτέλεση αυτού του προγράμματος σε υπολογιστή. Τώρα τα αιτήματα του διπλώματος ευρεσιτεχνίας περιέχουν τεχνικά χαρακτηριστικά (τον υπολογιστή), και η διαφορά ανάμεσα στα αιτήματα της ευρεσιτεχνίας (γνωστός υπολογιστής + νέο πρόγραμμα) και το επίπεδο της τεχνολογίας (γνωστός υπολογιστής) είναι το πρόγραμμα υπολογιστή. Έτσι, ένα πρόγραμμα υπολογιστή μπορεί να είναι μια τεχνική συνεισφορά, σύμφωνα με αυτόν τον όρο, πράγμα τελείως αντιφατικό (καθώς ένα πρόγραμμα υπολογιστή δεν μπορεί να είναι τεχνικό). Είναι σαφές ότι απαιτείται μια διόρθωση σαν αυτή στην τροποποίηση 57=99=110, αν πρόκειται να χρησιμοποιηθεί η έννοια της %(q:τεχνικής συνεισφοράς).

#lWo: Η περίοδος χάριτος νεωτερισμού που προτείνεται από την τροποποίηση 100 είναι ένα ανεξάρτητο ερώτημα. Όπως φαίνεται από ένα %(ng:πρόσφατο συμβούλιο που διεξήχθη από το Γραφείο Διπλωμάτων Ευρεσιτεχνίας του Ηνωμένου Βασιλείου), είναι εξαιρετικά αμφιλεγόμενη ακόμα και ανάμεσα σε αυτούς που υποτίθεται ότι θα ωφεληθούν από αυτή. Δεν είναι σαφές αν μια περίοδος χάριτος νεωτερισμού θα ωφελήσει πραγματικά τους SME, όπως προτείνει η αναφορά της ITRE. Στην περίπτωση της ανάπτυξης λογισμικού ανοικτού κώδικα, θα μπορούσε να προξενήσει ακόμα και πρόσθετη ανασφάλεια σχετικά με το αν οι ιδέες που δημοσιεύονται δε δεσμεύονται από διπλώματα ευρεσιτεχνίας.

#Am16N3: The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#Am99: The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art.

#Am100P1: The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.

#Art43aSyn: Όπως αναφέρθηκε στην ανάλυση του άρθρου 1, το %(q:εφεύρεση υλοποιημένη σε υπολογιστή) είναι αντιφατικός όρος. Όπως αναφέρθηκε στην ανάλυση της Αγόρευσης 11, η %(q:εφεύρεση) και η %(q:τεχνική συνεισφορά) είναι συνώνυμα: αυτό που εφηύρατε είναι η (τεχνική) σας συνεισφορά στο επίπεδο της προόδου. Στη διάλεκτο της Οδηγίας, όμως, %(q:τεχνική συνεισφορά) σημαίνει μόνο %(q:λύση σε ένα τεχνικό πρόβλημα ένα βήμα μετά την πιο κοντινή προηγούμενη γνώση στην διεκδικούμενη εφεύρεση). Σε αυτό το πλαίσιο, δεν είναι σαφές τι αποτέλεσμα θα έχει μια δοκιμασία, οσοδήποτε αυστηρή, γιατί δεν είναι σαφές σε τι αναφέρεται.

#Art43aSynTest: Επίσης, λέγοντας ότι αυτή η δοκιμασία %(q:θα εφαρμόζεται), ο όρος δεν απαιτεί ξεκάθαρα πως αυτή η δοκιμασία στην πραγματικότητα %(e:πρέπει να περαστεί), ούτε ξεκαθαρίζει το τι εννοείται από τον όρο %(q:τεχνική συνεισφορά).

#Art43aSynInd: Τέλος, δεν υπάρχει νομικός ορισμός της %(q:βιομηχανικής εφαρμογής κατά την αυστηρή έννοια της έκφρασης, σε όρους και της μεθόδου και του αποτελέσματος).

#Art43aSynVal: Παρόλ' αυτά, προτείνουμε την υποστήριξη της τροποποίησης 70.  Τουλάχιστον συστηματοποιεί σημαντικοά σημεία της έννοιας της %(q:τεχνικής εφεύρεσης). Έτσι, σε συυνδυασμό με άλλες τροποποιήσεις, μπορεί τελικά να βοηθήσει στην χάραξη ενός καθαρού ορίου για την ικανότητα κάλυψης από τα διπλώματα ευρεσιτεχνίας.

#Am70: In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of natures and has an industrial application in the strict sense of the expression, in terms of both method and result.

#Art4aSyn: Η τροποποίηση 17 υποθέτει πως %(q:κανονική φυσική αλληλεπίδραση μεταξύ ενός προγράμματος και ενός υπολογιστή) σημαίνει κάτι.  Δεν σημαίνει.  Κάποιο νόημα θα μπορούσε να καθοριστεί, για παράδειγμα προσδιορίζοντας, δανειζόμενοι από την πρόσφατη %(es:%(tp|Error Search|Suche Fehlerhafter Zeichenketten) απόφαση) του German Federal Patent Court, πως η %(q:αύξηση της υπολογιστικής αποδοτικότητας), %(q:εξοικονόμιση χρήσης της μνήμης) κλπ %(q:δεν αποτελούν τεχνική συνεισφορά), ή %(q:είναι μέσα στο αντικείμενο της φυσικής αλληλεπίδρασης μεταξύ ενός προγράμματος και του υπολογιστή). Χωρίς αυτό τον ορισμό, η τροποποίηση 17 προσθέτει μόνο σύγχυση αντί διασαφήνιση.

#Am17: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)

#Art4bSyn: Τα ίδια σχόλια όπως και για τις παραπάνω τροποποιήσεις.

#Am87: Exclusions from patentability A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions merely involving computer programs (data processing) which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

#Art4cSyn1: The directive as proposed by CEC and JURI, is based on the Trilateral Standard of the US, Japanese and European Patent Office of 2000, which considers all computer programs to be patentable inventions.  This is expressed in the word %(q:computer-implemented invention), which was introduced by the EPO in order to support this standard.  There is no longer a test of whether an invention is present, but only a test of %(q:technical contribution in the inventive step), i.e the step between the %(q:closest prior art) and the %(q:invention) must be framed in terms of a %(q:technical problem), e.g. a problem of improving speed or efficiency in the use of computer memory.  By using this EPO doctrine, in effect any algorithm or business method becomes patentable, as has been pointed out by the German Federal Patent Court in a %(es:recent decision).

#Art4cSyn2: Οι Τροποποιήσεις 47 και 60 σαφώς λένε στο EPO να ακολουθήσει την προσέγγιση του Γερμανικού Ομοσπονδιακού Δικαστηρίου Διπλωμάτων Ευρεσιτεχνίας στο να αρνηθεί το ότι η βελτίωσης της απόδοσης επεξεργασίας δεδομένων είναι μια %(q:τεχνική συνεισφορά).

#Am60: Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.

#Art4dSyn1: This amendment alone could replace most of the provisions of the directive.  The EPO Guidelines of 1978 provide a clear interpretation of Art 52.  This was changed %(q:in response to demands from the industry) by later versions of the Guidelines, at the price of making the rules contradictory and introducing a schism.  The objectives of this directive should be to resolve the schism and to concretise the application of the TRIPs treaty of 1994.  The first objective can be achieved by this amendment alone.  Note that the amendment does not say that the EPO Guidlines of 1978 should become law.  It merely says that they provide a proper interpretation of Art 52 EPC.

#Am46: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Article 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#Art5: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Art5Syn18: Η Τροποποίηση 18 προτείνει την εισαγωγή αιτημάτων προγραμμάτων, δηλ. αιτήματα της μορφής %(bc:ενός προγράμματος, που χαρακτηρίζεται από το ότι κατά την φόρτωσή του στη μνήμη ενός υπολογιστή επιτελείται [ κάποια διεργασία ] .) Αυτό θα μετέτρεπε κάθε δημοσίευση λογισμικού μια πιθανή άμεση παραβίαση κατοχυρωμένης ευρεσιτεχνίας και έτσι θα έθετε σε κίνδυνο προγραμματιστές, ερευνητές και διανομείς υπηρεσιών στο Διαδίκτυο. Χωρίς αιτήματα προγράμματων, οι διανομείς θα μπορούσαν να είναι παραβάτες μόνο έμμεσα, π.χ. μέσω των εγγυήσεων που παρέχουν στον πελάτη τους. Το ερώτημα για αιτήματα προγραμμάτων καθεαυτό φαίνεται να είναι μάλλον συμβολικής παρά ουσιαστικής φύσης: το λόμπι των διπλωμάτων ευρεσιτεχνίας θέλει να καταγραφεί αναμφίβολα ότι το λογισμικό καθεαυτό επιδέχεται κατοχύρωση ευρεσιτεχνίας. Η CEC έχει αποφύγει αυτή την τελευταία συνέπεια.

#Art5Syn58: Όμως, η διατύπωση της CEC ακόμα προτείνει ότι το γνωστό υλικό υπολογιστή γενικής χρήσεως και οι κανόνες υπολογισμών που εκτελούνται σε αυτό (= προγράμματα υπολογιστή) είναι προϊόντα και διεργασίες που επιδέχονται κατοχύρωση ευρεσιτεχνίας. Η τροποποίηση 101/58 προσπαθεί να αντιμετωπίσει το πρόβλημα αυτό. Μπορεί να είναι καλή ιδέα να αντικατασταθεί το %(q:τεχνικές διαδικασίες παραγωγής που λειτουργούν με έναν τέτοιο υπολογιστή) με το %(q:οι εφευρετικές διαδικασίες που τρέχουν σε ένα τέτοιο σύνολο εξοπλισμού) αν αυτό είναι ακόμη δυνατό με τη μορφή μιας %(q:συμβιβαστικής τροποποίησης). Η Τροποποίηση 102 ορίζει την %(q:εφεύρεση υλοποιημένη σε υπολογιστή) ως μόνο κάποιο προϊόν ή τεχνική διεργασία παραγωγής. Εφόσον οριστεί το %(q:τεχνική), αυτό μπορεί να είναι χρήσιμο. Παρόλα αυτά, ένας υπολογιστής που τρέχει κάποιο πρόγραμμα μπορεί να ερμηνευτεί επίσης και ως %(q:προγραμματιζόμενη συσκευή), και η κατοχύρωση ευρεσιτεχνίας για τη χρήση ενός προγράμματος υπολογιστή όταν εκτελείται σε έναν υπολογιστή έχει το ίδιο αποτέλεσμα με την κατοχύρωση του ίδιου του προγράμματος: δεν υπάρχει άλλος τρόπος να χρησιμοποιηθεί ένα πρόγραμμα εκτός από το να εκτελεστεί σε κάποιον υπολογιστή.

#Am18: A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

#Am101: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#Am102: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#Art5aSyn: Το ελεύθερο λογισμικό συνεισφέρει σημαντικά στην καινοτομία και τη διάδοση των γνώσεων, γεγονός που πρέπει να αντικατοπτρίζεται στην πολιτική για την καινοτομία και στη νομοθεσία, άσχετα με το το αν η επεξεργασία δεδομένων γενικά επιδέχεται κατοχύρωση ευρεσιτεχνίας ή όχι. Δυστυχώς η τροποποίηση αυτή χρησιμοποιεί τον όρο %(q:εφευρέσεις υλοποιημένες σε υπολογιστή), ο οποίος εισήχθη από το EPO το 2000 σε μια προσπάθεια να δηλώσει τη δυνατότητα κατοχύρωσης ευρεσιτεχνίας λειτουργιών επεξεργασίας δεδομένων γενικής χρήσεως (λογισμικού). Αυτό πρέπει να διορθωθεί σε μια δεύτερη ανάγνωση.

#Am62: %(al|Limitation of the effects of patents granted to computer-implemented inventions|%(linol|Member States shall ensure that the rights conferred by the patent shall not extend to the acts done to run, copy, distribute, study, change or improve a computer program which is distributed under a licence that provides for:|the freedom to run the program;|the freedom to study how the program works, and adapt it to the user's needs;|the freedom to redistribute copies under the same licence conditions;|the freedom to improve the program, and release improvements to the public under the same licence conditions;|free access to the source code of the program.))

#Art51a: This amendment apparently intends to exclude claims to ways of operating data processing hardware (= computer programs), but proposes inappropriate regulatory means.  Patent claims always comprise more than the invention (= technical contribution).  If there is a technical invention (e.g. a new chemical process), then there is no reason why the patent claim should not comprise this invention together with non-invention elements, such as a computer program that controls the reaction.  A claim does not describe an invention but the scope of an exclusion right which is legitimated by an invention.

#Am72: Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentansprüche nur den technischen Beitrag umfassen, der den Patentanspruch begründet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datenträger vorliegendes Programm, ist unzulässig.

#Am103Syn: Χωρίς αυτη την τροποποίηση, οι κάτοχοι διπλώματος ευρεσιτεχνίας λογισμικού θα μπορούν ακόμα να στείλουν ακόμα γράμματα σε προγραμματιστές και διανομείς λογισμικού για να σταματήσουν (cease-and-desist) κατηγορώντας τους για συνεισφορά σε παραβάσεις. Αυτό το άρθρο διασφαλίζει ότι το δικαίωμα της δημοσίευσης όπως το εγγυάται το άρθρο 10 της ECHR έχει προτεραιότητα ως προς τα διπλώματα ευρεσιτεχνίας. Από την άλλη πλευρά, αυτή η τροποποίηση δεν εμποδίζει την κατοχύρωση ευρεσιτεχνιών λογισμικού ή την επιβολή τους έναντι των χρηστών λογισμικού. Οι πωλητές ιδιόκτητου λογισμικού και οι εμπορικοί διανομείς του Linux μπορούν ακόμα να δεχτούν πίεση από τους πελάτες τους, που συνήθως θα τους επέβαλαν με συμβόλαιο την υποχρέωση εκκαθάρισης από ενδεχόμενες κατοχυρωμένες ευρεσιτεχνίες.

#Am103: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#Am104N1Syn: Το πεδίο που καλύπτει το δίπλωμα ευρεσιτεχνίας κανονικά ορίζεται από τα αιτήματα, και άν κάτι βρίσκεται εκτός των αιτημάτων, δεν αποτελεί παράβαση. Έτσι, η τροποποίηση σε πρώτη ματιά φαίνεται να είναι ταυτολογική. Η πρόθεση αυτής της τροποποίησης είναι να επιτρέψει την εξομείωση κατοχυρωμένων με ευρεσιτεχνία διεργασιών σε συνήθη εξοπλισμό επεξεργασίας δεδομένων. Αν είναι έτσι, αυτό πρέπει να δηλωθεί σαφώς, όπως γίνεται από μερικές από τις άλλες τροποποιήσεις.

#Am104N1: Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#Art5dSyn: Αυτές οι τροποποιήσεις, αντίθετα με ότι πιστεύουν κάποιοι, δεν εξυπηρετούν την προώθηση του ελεύθερου λογισμικού/λογισμικού ανοικτού κώδικα, αλλά μάλλον διασφαλίζουν ότι η υποχρέωση αποκάλυψης, που είναι εγγενής στο σύστημα των διπλωμάτων ευρεσιτεχνίας, λαμβάνεται σοβαρά και ότι το λογισμικό βρίσκεται, όπως κάθε άλλο αντικείμενο πληροφορίας, στην πλευρά της αποκάλυψης του διπλώματος ευρεσιτεχνίας και όχι στην πλευρά του αποκλεισμού/της μονοπώλησης. Αυτό κάνει πιο δύσκολο να εμποδίσεις τους άλλους να κάνουν πράγματα που δεν έχεις κάνει ο ίδιος, αλλά τα οποία είναι προφανώς δυνατά αφού το υπολογιστικό μοντέλο είναι ορισμένο τέλεια και πάντα ξέρεις τι μπορείς να κάνεις με έναν υπολογιστή. Όταν δημοσιεύεις λειτουργικό πηγαίο κώδικα προσφέρεις τουλάχιστον κάποιες πραγματικές γνώσεις για τι πως μπορεί να λυθεί το πρόβλημα, αντίθετα με όταν μιλάς για %(q:μέσα επεξεργασίας συνδυασμένα με μέσα εισόδου/εξόδου ώστε να υπολογίσουν μια συνάρτηση ώστε το αποτέλεσμα αυτής όταν εξαχθεί μέσω της εν λόγω εξόδου επιλύει το πρόβλημα που ήθελε να λύσει ο χρήστης).

#Am104N2: Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#Art6: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Art6Syn: The European Commission's proposal does not protect interoperability but, on the contrary, makes sure that interoperable software can not be published or used when an interface is patented. The only behavior which the European Commission wants to permit in this case is decompilation, which would not infringe on patents anyway (as patents involve the use of what is described in the claims, not the studying of said object). Amendments 66 and 67 moroever specify the laws on which interoperability privileges can be based, thus making it even more certain that the provision can not have any effect.

#Am19: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

#Art6aSyn: Οι Τροποποιήσεις 20 και 50 είναι οι μόνες που διατυπώνουν ένα πραγματικό δικαίωμα διαλειτουργικότητας. Όμως, έχουν %(rm:εκφραστεί) φόβοι ότι η διατύπωση  %(q:συστήματα υπολογιστή) μπορεί να ερμηνευτεί στενά ως %(q:αρχιτεκτονικές υλικού). Θα πρέπει να διορθωθεί σε %(q:συστήματα επεξεργασίας δεδομένων) αν είναι δυνατόν. Η τροποποίηση 50 του UEN το κάνει αυτό.  20 έχει ήδη γίνει δεκτή από τα CULT, ITRE και JURI.

#Am76Syn: Η τροποποίηση 76 αφαιρεί κάθε σαφήνεια σχετικά με το πότε επιτρέπεται η χρήση τεχνικών κατοχυρωμένων ως ευρεσιτεχνία για διαλειτουργία και πότε όχι. Είναι αντίθετη στο βασικό σκοπό της οδηγίας, ο οποίος είναι να συγκεκριμενοποιήσει κάποιους αφηρημένουνς μετά-κανόνες, συμπεριλαμβανομένων αυτών του ’ρθρου 30 του TRIPs, και έτσι να πετύχει σαφήνεια και νομική ασφάλεια.

#Am105Syn: Είναι καλό ότι η τροποποίηση 105 συμφωνεί με εμάς ότι μια %(q:εφεύρεση υλοποιημένη σε υπολογιστή) δεν μπορεί να σημαίνει οτιδήποτε άλλο εκτός από λογισμικό που τρέχει στον υπολογιστή. Όμως, οι εξαιρέσεις που αναφέρει είναι πολυ παράξενες. Στην πρώτη περίπτωση, αν η εφεύρεση είναι μια αυτόνομη μηχανή, δεν υπάρχει απαίτηση διαλειτουργικότητας. Αν δεν υπάρχει απαίτηση διαλειτουργικότητας, ούτε η τροποποίηση 20 δεν εφαρμόζεται. Αν αντίθετα υπάρχει τέτοια απαίτηση, τότε η κατοχυρωμένη ευρεσιτεχνία δε θα λειτουργεί ως ανεξάρτητη μηχανή ή τεχνική εφεύρεση. Ο δεύτερος όρος είναι πολύ επικίνδυνος, καθώς αφήνει τα πάντα να αποφασιστούν από τα δικαστήρια, αποτυγχάνοντας έτσι στο σκοπό της οδηγίας για σαφήνεια. Όπως δείχνουν οι αντιμονοπωλιακές υποθέσεις εναντίον της Microsoft και στις ΗΠΑ και στην Ευρώπη, είναι εξαιρετικά δύσκολο και χρονοβόρο να διαπιστωθεί εάν κάποια συμπεριφορά παραβιάζει την υπάρχουσα νομοθεσία περί αναταγωνισμού. Αυτό θα έκανε τον όρο περί διαλειτουργικότητας ανίσχυρο και θα έθετε τους SME σε ακόμα μειονεκτικότερη θέση, καθώς οι μεγάλες εταιρίες μπορούν να ξοδέψουν πολύ περισσότερα χρήματα σε δικαστικές υποθέσεις.

#Am50: Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement..

#Am20: Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#Am76: Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that it does not unreasonably conflict with a normal exploitation of the patent and does not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#Am105: %(al|Use of patented technologies|Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that:|%(ol|the invention embodying the computer-implemented software is not working as an independent machine or technical invention, and|the patent is not in conflict with European competition law.))

#Art6b: This amendment seems to create a new property right outside of the patent system.  It establishes a category called %(q:computer-implemented inventions), which belong to no %(q:field of technology).  This is an interesting approach, although 7 years is probably still too long and some further questions need to be clarified in order for this to be workable.

#Am106: %(al|Term of patent|The term of a patent granted for computer-implemented inventions pursuant to this Directive shall be 7 years as from the date of filing.)

#Artt7: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Art7Syn: Πρέπει να εξασφαλιστεί ότι οι έρευνες δεν γίνονται από δικηγόρους διπλωμάτων ευρεσιτεχνίας από την Εσωτερική Αγορά DG, αλλά μάλλον από ειδικούς στο λογισμικό και τα οικονομικά.  Η τροποποίηση 91 ειναι λίγο καλύτερη από τις άλλες, αν αναφέρονται ειδικά οι SME τότε πρέπει τουλάχιστον να γίνει αναφορά και στο ελεύθερο λογισμικό/λογισμικό ανοικτού κώδικα και την προτυποποίηση. Δεν υπάρχει επίσης εγγύηση ότι η μελέτη θα πραγματοποιηθεί από ανεξάρτητη ομάδα ειδικών στα οικονομικά του λογισμικού.

#Am21: Ακατάστατη διατύπωση, χρησιμοποιεί τον προπαγανδιστικό όρο %(q:ε. υ. σε υπ.).

#Am91: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses. Special attention will be paid to the position of small and medium-sized enterprises and of electronic commerce and to the impact of dominant positions on the functioning of the market.

#Am71Syn: Η τροποποίηση 71 μιλά για τη δημιουργία ενός ταμείου απόκτησης, υπεράσπισης και δικαστικής διεκδίκησης διπλωμάτων ευρεσιτεχνίας, παρόλο που δεν υπάρχει παράδειγμα στην πράξη ένος τέτοιου συστήματος που λειτουργεί. Η θεωρητική του δυνατότητα, όπως προτείνεται από μια συμβουλευτική αναφορά στην DG MARKT βασίζεται επίσης σε εσφαλμένες παραδοχές, όπως εξηγείται στην %(le:ανοικτή επιστολή προς το Ευρωπαϊκό Κοινοβούλιο) που γράφτηκε από έναν αριθμό διακεκριμένων οικονομολόγων.

#Am90Syn: Amendment 90 is like 71, but less explicit.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises and the open source community, and electronic commerce.

#Am71P2: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents, in particular through the creation of a defence fund and the introduction of special rules on legal costs.

#Am71P3: It shall report on its findings to the European Parliament and the Council and present appropriate proposals for legislation without delay.

#rbr: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Απόρριψη/Αποδοχή

#app: εγκρίνει την προτεινόμενη Οδηγία

#dWt: Είναι δυνατόν η οδηγία να διορθωθεί χρησιμοποιώντας τις τροποποιήσεις που έχουν παρουσιαστεί. Αν γίνουν αποδεκτές οι σωστές τροποποιήσεις, η οδηγία θα μπορούσε να είναι ευεργετική για την ΕΕ. Έτσι, η αποδοχή μιας πλήρως διορθωμένης οδηγίας μπορεί να είναι καλύτερη από την απόρριψη. Από την άλλη πλευρά, η τροποποίηση μιας οδηγίας τόσο λεπτομερώς είναι ένα σύνθετο έργο, ειδικά στην ολομέλεια, και το Ευρωπαϊκό Κοινοβούλιο μπορεί να μην καταφέρει να διορθώσει τις άφθονες παρανοήσεις και παραδοχές που ειισήχθησαν από την Επιτροπή. Η οδηγία πέρασε από μια διαδικασία που δεν έλαβε υπόψην της τα περισσότερα ενδιαφερόμενα μέρη, τις ακαδημαϊκές έρευνες ή τα διαθέσιμα στοιχεία. Έτσι, θα ήταν επίσης δικαιολογημένο να απορριφθεί το κείμενο και να ζητηθεί από την Επιτροπή να ξαναξεκινήσει τη διαδικασία. Μάλλιστα, ο αριθμός των σφαλμάτων που χρειάζονται διόρθωση είναι τόσο μεγάλος, που το να βασιστούμε στο αποτέλεσμα της ψήφου της ολομέλειας για μια καλή οδηγία είναι πολύ επικίνδυνο. Δεδομένου του κινδύνου για την πρόοδο, την ελευθερία, και την δυνατότητα συναγωνισμού των εταιριών της ΕΕ αν η τροποποίηση μερικώς αποτύχει, η απόρριψη είναι δυστυχώς το πιθανότερο πιο σόφρων. Αυτή είναι όμως μια απόφαση που μπορεί να παρθεί δύσκολα.

#ute: απορρίπτει την προτεινόμενη οδηγία

#Rec1Cec: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec1Syn: Η Επιτροπή με θάρρος διακυρήσσει, αντίθετα σε όλες τις %(ss:οικονομικές ενδείξεις), συμπεριλαμβανομένης μιας %(bh:πολύ λεπτομερούς εμπειρικής μελέτης από τους Bessen & Hunt το 2003), ότι τα διπλώματα ευρεσιτεχνίας ενθαρρύνουν επενδύσεις στην ανάπτυξη λογισμικού. Επιπλέον, ο όρος %(q:εφεύρεση υλοποιημένη σε υπολογιστή) είναι ασαφής. Είναι καλύτερα να μην υπάρχει καθόλου δήλωση στόχων παρά μια εσφαλμένη δήλωση.

#Am1: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec5Cec: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Rec5Syn1: Both the European Commission (CEC) and Amendment 2 (JURI) say dogmatically that software patents stimulate innovation.  It would be more appropriate to state a policy goal, by which the proposed patentability regime can be measured.

#Rec5Syn2: Amendment 2 seems to claim that the mere fact of writing EU law, no matter what the contents, already %(q:results in legal certainty), simply because the Luxemburg court can interpret this law.  This new goal statement is not only questionable but also pointless, because %(ol|the Community Patent is under way anyway|there are easier and more systematic ways to put the Luxemburg court in charge than by passing an interpretative directive for one aspect of the European Patent Convention)

#Am2: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#css: Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

#Rec5aSyn: We agree that the rules laid down in Article 52 EPC have been stretched beyond recognition by the EPO and that the original meaning, as described in the 1978 EPO examination guidelines, should be reconfirmed.

#Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

#Rec6Cec: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Rec6Syn: Πρέπει να κάνουμε σαφές ότι υπάρχουν όρια στο τι μπορεί να τεθεί κάτω από τον όρο %(q:τεχνολογικά πεδία) σύμφωνα με το ’ρθρο Art 27 TRIPs και ότι αυτό το άρθρο δεν είναι σχεδιασμένο να ορίζει απεριόριστη δυνατότητα κατοχύρωσης ευρεσιτεχνιών αλλά μάλλον να αποφύγει τριβές στο ελεύθερο εμπόριο, που μπορεί να προκληθούν από αδικαιολόγητες εξαιρέσεις όπως επίσης και από αδικαιολόγητες επεκτάσεις της δυνατότητας κατοχύρωσης. Αυτή η ερμηνεία του TRIPs εμμέσως επιβεβαιώνεται έμμεσα από την πρόσφατη πίεση της κυβέρνησης των ΗΠΑ εναντίον του ’ρθρου 27 TRIPS λόγω του ότι εξαιρεί τα διπλώματα ευρεσιτεχνίας επιχειρηματικών μεθόδων, τα οποία η κυβέρνηση των ΗΠΑ θέλει να επιβάλλει με το νέο σχέδιο Συμφωνίας Νομοθεσίας Ουσιαστικών Διπλωμάτων Ευρεσιτεχνίας, βλέπε %(bh:άρθρο του Brian Kahin στη First Monday). Η τροποποίηση 31 διαγράφει το ουσιαστικά ανακριβές άρθρο.

#Am31: Η διαγραφή είναι καλή, αλλά θα ήταν καλύτερη η διασάφηση.

#Rec7Cec: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Rec7Syn: Το νόημα αυτής της παραγράφου δεν είναι ξεκάθαρο. Ο σκοπός μιας οδηγίας συνήθως είναι να θέσει κανόνες, όχι να τροποποιεί κείμενα.  Ακόμη και αν τα EPC member states αποφάσιζαν πως θα ήταν σωστό να τροποποιήσουν το Art 52 EPC (αντί απλά να το παρακάμψουν με άλλα νομικά μέσα), θα μπορούσαν ακόμη να τροποποιήσουν το EPC και να πουν πως με αυτό τον τρόπο εξυπηρετούν τον σκοπό αυτής της οδηγίας.

#Am32: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.

#Am3: The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.

#Rec7bSyn: Νέo ψήφισμα που ζητά μεγαλύτερη υποχρέωση του ΕΠΟ να είναι υπόλογο για τις πράξεις του. Το αποτέλεσμα θα ήταν ισχυρότερο αν επρόκειτο για άρθρο, αλλά ακόμα και έτσι είναι καλό να γίνει αποδεκτή αυτή η αρχή. Είναι σημαντικό να αναφερθούν όλες οι πηγές συγκρούσεων ώστε να διευκολυνθούν οι τελικές λύσεις.

#Am95: Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeO~s operating rules to ensure that it was publicly accountable in the exercise of its functions.

#cdn: Τα διπλώματα ευρεσιτεχνίας λογισμικού θα ήταν άσχημα για όλους τους δημιουργούς λογισμικού που δεν υποστηρίζονται από μεγάλες εταιρείες με πολλά χρήματα και δικηγόρους, και αυτό προφανώς περιλαμβάνει έναν αριθμό από δημιουργούς ελεύθερου λογισμικού.

#Am61: Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: Οι τέσσερις δοκιμασίες για τη δυνατότητα κατοχύρωσης ευρεσιτεχνίας που εκτίθενται στο ’ρθρο 52 της EPC δηλώνουν ότι πρέπει να υφίσταται μια (τεχνική) εφεύρεση και ότι επιπλέον, αυτή η εφεύρεση πρέπει να επιδέχεται βιομηχανική εφαμογή, να είναι νέα και να περιέχει ένα εφευρετικό βήμα. Οι προϋποθέσεις για το %(q:εφευρετικό βήμα) καθορίζονται στο ’ρθρο 56 της EPC και απαιτούν η εφεύρεση να μην είναι ένας προφανής συνδυασμός τεχνικών γνωστών σε ένα άτομο έμπειρο στην τεχνική. Επιπλέον, η %(q:εφεύρεση) και η %(q:τεχνική συνεισφορά) είναι συνώνυμα: μπορείς να εφεύρεις μόνο νέα πράγματα, και αυτή η εφεύρεση είναι η (τεχνική) σου συνεισφορά στο επίπεδο της τεχνικής. Όπως είναι, το πρωτότυπο της CEC και οι τροποποιήσεις 4 και 84 βρίσκονται σε άμεση αντίφαση με την EPC. Επιπρόσθετα, μια τέτοια λογική εξασφαλίζει ότι το δικαίωμα κατοχύρωσης ευρεσιτεχνίας είναι απλώς θέμα της διατύπωσης των αιτημάτων, βλέπε το τμήμα για %(se:Τέσσερις Διαφορετικές Δοκιμασίες για Ένα Ενοποιημένο Αντικείμενο). Προκειμένου να αποφευχθεί η σύγχιση και οι συγκρούσεις με τα άρθρα 52 και 56 της EPC, η αγόρευση πρέπει να ξαναγραφτεί ή να διαγραφεί.

#Am51: While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science.

#Am4: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.

#Am84: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.

#Rec11Syn: The Commission text declares computer programs to be technical inventions. It removes the independent requirement of invention (= %(q:technical contribution)) and merges it into the requirement of non-obviousness (= %(q:inventive step), see comments for Recital 11). This leads to theoretical inconsistency and undesirable practical consequences.

#Rec11bSyn: Amendment 73 would be a perfect restatement of Art 52 EPC if it had said %(q:inventions) instead of %(q:computer-implemented inventions). As it is now, it looks as if it was a special rule for inventions involving computers.   Yet it is useful in that reinforces the notion that the same invention must pass the four tests.  See Article 1 for why the term %(q:computer-implemented invention) is misleading.

#Am73: Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application..

#Rec12: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Rec12Syn: Το κείμενο της Ευρωπαϊκής Επιτροπής συνδυάζει τη δοκιμασία της %(q:τεχνικής εφεύρεσης) στη δοκιμασία του %(q:εφευρετικού βήματος), καθιστώντας έτσι και τις δυο δοκιμασίες πιο αδύναμες και ανοίγοντας ένα απεριόριστο πεδίο ερμηνείας. Αυτό έρχεται σε αντίθεση με το ’ρθρο 52 της EPC, είναι θεωρητικά ασυνεπές, και οδηγεί σε ανεπιθύμητες πρακτικές συνέπειες, όπως το να κάνει την έξέταση σε κάποια εθνικά γραφεία διπλωμάτων ευρεσιτεχνίας ανέφικτη. Ξανά, δείτε την εξήγηση στην Αγόρευση 11 για περισσότερες πληροφορίες. Η τροποποίηση του JURI κάνει τα πράγματα ακόμα χειρότερα: πρώτα από όλα, επανεισαγάγει κυριολεκτικά το άρθρο 3 (το οποίο διέγραψε), κατά δεύτερον υποστηρίζει ότι η απαίτηση για το %(q:εφευρετικό βήμα) είναι κάτι τελείως διαφορετικό requirement is something completely different from what Art 56 EPC says, τρίτον αρχίζει να μιλά για ένα %(q:τεχνικό πρόβλημα) που πρέπει να επιλυθεί (ενώ το δικαίωμα κατοχύρωσης ευρεσιτεχνίας δε σχετίζεται με το είδος του προβλήματος που πρόκειται να λυθεί αλλά με τη λύση που χρησιμοποιείται) και τέλος επαναλαμβάνει τα σφάλματα του πρωτοτύπου της CEC.

#Am5: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Am114: Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law.

#Rec13: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Rec13Syn: Η αγόρευση της CEC recital προφασίζεται ότι εξαιρεί τους αλγόριθμους, αλλά στην πραγματικότητα επιτρέπει την κατοχύρωση αλγορίθμων ως ευρεσιτεχνίες, διότι ένας αλγόριμος που ορίζεται ως προς το περιβάλλον εξοπλισμού επεξεργασίας δεδομένων γενικής φύσεως (= %(q:ένα φυσικό περιβάλλον)) είναι ακριβώς το ίδιο με τον αλγόριθμο καθεαυτό. Είναι παράλογο να αντίθερτο με το σκοπό της διασάφησης να γίνει τεχνητή προσπάθεια διαχωρισμού μεταξύ μαθηματικών αλγορίθμων και μη μαθηματικών αλγορίθμων. Για τους επιστήμονες υπολογιστών, αυτό δεν έχει νόημα γιατί κάθε αλγόριθμος είναι τόσο μαθηματικός όσο γίνεται, δεν υπάρχουν πιο μαθηματικά πράγματα. Οι αλγόριθμοι είναι αφηρημένες έννοιες, άσχετες με τους φυσικούς νόμους του σύμπαντος.

#Rec13aSyn: Η συντακτική ασάφεια αυτής της τροποποίησης αφήνει ασαφές το αν οι %(q:επιχειρηματικές μέθοδοι) γενικά θεωρούνται %(q:μέθοδοι των οποίων η μόνη συνεισφορά στο επίπεδο προόδου είναι μη τεχνική) ή εάν ορισμένες επιχειρηματικές μέθοδοι θα μπορούσαν να περιέχουν %(q:τεχνικές συνεισφορές). Πιο σημαντικά, αυτή η τροποποίηση δε θα αποκλείσει στην πραγματικότητα τίποτα, γιατί οι δικηγόροι διπλωμάτων ευρεσιτεχνίας σύντομα θα ισχυριστούν ότι κάποια χαρακτηριστικά σχετιζόμενα με υπολογιστή είναι χαρακτηριστικά της %(q:εφεύρεσης), η οποία, όπως λέχθηκε αλλού σε αυτή την πρόταση οδηγίας, πρέπει να %(q:λογαριαστεί ως όλον). Για να έχει αυτό κάποια χρησιμότητα, πρέπει η συντακτική ασάφεια γύρω από την %(q:άλλη μέθοδο) να απομακρυνθεί και η κατηγορία των %(q:μη τεχνικών μεθόδων) να εξηγηθεί με ορισμούς και/ή παραδείγματα.

#Am6: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#Rec13bSyn: Εδώ δηλώνεται μια θέση κοινής λογικής που συχνά αγνοήθηκε προκειμένου να επεκταθεί το δικαίωμα κατοχύρωσης ευρεσιτεχνίας. Αυτό από μόνο του δεν επιτυγχάνει πολλά, και η δήλωση μέσα στο νόμο ότι ο νόμος %(q:δεν μπορεί να παρακαμφθεί) είναι μια ευσεβής ευχή. Όμως, ακόμα και μια ευχή είναι καλύτερη από το τίποτα.

#Am7: If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

#Am8: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#Rec13dSyn1: Αυτή η τροποποίηση απλώς ξαναδιατυπώνει τον ορισμό των αιτημάτων: τα αιτήματα ορίζουν τα δικαιώματα αποκλεισμού που παρέχει το δίπλωμα ευρεσιτεχνίας. Επιτρέπει αιτήματα επί λογισμικού κάνοντας απλώς αναφορά σε γενικό υπολογιστικό εξοπλισμό ή υπολογιστικές διεργασίες, ανεξάρτητα από το που βρίσκεται η καινοτομία. Είναι παρόμοιο με το να λεχθεί: μπορείς να κατοχυρώσεις ό,τι επιθυμείς εφόσον χρησιμοποιήσεις λεξιλόγιο όπως αποθήκευση, επεξεργαστής ή διάταξη κάπου στα αιτήματά σου, αλλά πρόσεξε να κάνεις τα αιτήματα όσο ευρέα τα θέλεις, γιατί δεν θα αποκτήσεις μονοπώλειο σε ό,τι δεν έθεσες αξίωση.

#Rec13dSyn2: Η τροποποίηση μπορεί να έχει ένα έμμεσο όφελος: φαίνεται να έρχεται σε σύγκρουση με την τροποποίηση 18 στο άρθρο 5 λέγοντας ότι μόνο προγραμματιζόμενες συσκευές και διαδικασίες μπορούν να κατοχυρωθούν.

#Am9: The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.

#Rec14: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#Rec14Syn1: Η αγόρευση της CEC δεν είναι και τόσο κακή (παρόλο που χρησιμοποιεί τον όρο %(q:εφευρέσεις υλοποιημένες σε υπολογιστή), βλέπε ’ρθρο 1). Λέει απλώς ότι η νομοθεσία διπλωμάτων ευρεσιτεχνίας δε θα πρέπει να αντικατασταθεί. Η τροποποίηση 10 είναι λιγότερο σαφής. Από τη μια πλευρά εξισώνει την %(q:παρούσα νομική κατάσταση) (?) με τις %(q:πρακτικές του Ευρωπαϊκού Γραφείου Διπλωμάτων Ευρεσιτεχνίας). Από την άλλη, λέει ότι η δυνατότητα κατοχύρωσης ευρεσιτεχνίας και επιχειρηματικές μεθόδους πρέπει να αποφευχθεί. Όμως, το ίδιο το EPO έχει χορηγήσει πολλά διπλώματα ευρεσιτεχνίας για επιχειρηματικές μεθόδους και έχει παράσχει %(a6:νομικούς συλλογισμούς για να υποστηρίξει τα διπλώματα ευρεσιτεχνίας επιχειρηματικών μεθόδων). Το μόνο δυνατό αποτέλεσμα αυτής της τροποποίησης, αν υπάρξει κάποιο αποτέλεσμα, είναι να παραδοθεί η νομοθετική ευθύνη στο EPO, του οποίου οι πρακτικές φαίνεται να υπερασπίζονται την κατοχύρωση ευρεσιτεχνιών αντί να τηρούν τους νόμους που ψηφίστηκαν από το Ευρωπαϊκό Κουνοβούλιο και άλλα δημοκρατικά αντιπροσωπευτικά σώματα.

#Am86Syn: Amendment 86 does not ask for a codification of the current EPO practice, but on the other hand talks about %(q:unpatentable methods such as trivial procedures and business methods). Not just trivial business methods must remain unpatentable, but all business methods must remain so (otherwise we are deviating from Art 52 EPC, which should be avoided as suggested in amendment 88).

#Am10: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.

#Am86: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec16: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#Rec16Syn: Η ενοποίηση της νομολογίας από μόνη της δεν αποτελεί εγγύηση για βελτίωση της κατάστασης της Ευρωπαϊκής βιομηχανίας. Αυτή η οδηγία δεν πρέπει να χρησιμοποιήσει το πρόσχημα της %(q:εναρμόνισης) για να αλλάξει τους κανόνες του ’ρθρου 52 της EPC, που είναι ήδη σε ισχύ σε όλες τις χώρες. Το αν τα διπλώματα ευρεσιτεχνίας λογισμικού είναι ωφέλιμα ή όχι για τις Ευρωπαϊκές εταιρίες ανάπτυξης λογισμικού είναι ανεξάρτητο από το γεγονός ότι η παραδοσιακή παραγωγική βιομηχανία μετακινείται σε οικονομίες χαμηλού κόστους έξω από την ΕΕ. Η τροποποίηση 11 και το κείμενο της CEC υποθέτουν ότι η επέκταση της προστασίας των διπλωμάτων ευρεσιτεχνίας στη βιομηχανία ανάπτυξης λογισμικού θα βελτιώσει την ανταγωνιστικότητα των εταιριών της ΕΕ, παρόλο που 75% των 30.000 διπλωμάτων ευρεσιτεχνίας λογισμικού που έχουν ήδη κατοχυρωθεί (τα οποία η CEC/JURI αποκαλεί διπλώματα ευρεσιτεχνίας για %(q:εφευρέσεις υλοποιημένες σε υπολογιστή)) βρίσκονται στα χέρια εταιριών των ΗΠΑ και της Ιαπωνίας. Επιπρόσθετα, αν η Ευρώπη δεν έχει διπλώματα ευρεσιτεχνίας λογισμικού, οι Ευρωπαϊκές εταιρείες μπορούν παρόλα αυτά να τις αποκτήσουν στις ΗΠΑ και αλλού και να τις επιβάλλουν εκεί, ενώ οι επηρεαζόμενες ξένες εταιρίες δεν μπορούν να κάνουν το ίδιο εδώ. Κατά αυτή την έννοια, το να μην υπάρχουν διπλώματα ευρεσιτεχνίας λογισμικού στην Ευρώπη αποτελεί ανταγωνιστικό πλεονέκτημα.

#Am11: The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.

#Am35: The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Rec18: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Rec18Syn: The CEC proposal only pretends to protect interoperability, but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendment 13 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.

#Am13: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

#Rec18bSyn: This recital suggests that computer programs can be an invention (since there are no other innovations which have source code or which you can decompile), which contradicts article 52 EPC, since that one explicitly excludes computer programs from patentability. Software can never be part of an invention. It can be mentioned in the claims of the patent, but in that case it merely serves to clarify the application of the actual invention and to limit the scope of the monopoly that the patent grants to its holder (i.e., if someone manages to make use of the invention without using a computer program, he will not infringe on a patent that only describes the use of said invention in combination with a computer program).

#Am74: The application of this directive must not deviate from the original foundations of patent law, which means that the applicant of the patent must provide a description of all elements of the invention, including the source code, and that research into it, and as such decompilation, must be made possible. This way of working is indispensable to make compulsory licensing possible, for example if the obligation to supply the market is not fulfilled.

#Rec18tSyn: Αυτό εισηγείται επίσης πως προγράμματα υπολογιστών μπορούν να είναι εφευρέσεις.

#Am75: In any case, the law of all member states must ensure that the patents contain novelties and contain an inventive step, to prevent inventions which are already public from being appropriated, simply because they belong to a computer program.

#pmd: Europarl 2003/09: Amendments

#eus: Here the amendments for the plenary decision on the software patent directive are published in various languages.

#ata: Πολλοί από τους συμμετέχοντες, συμπεριλαμβανομένων αυτών που υποτίθεται ότι ωφελούνται από την περίοδο χάριτος νεωτερισμού, εκφράζουν αμφιβολίες για αυτή την έννοια που δεν είναι σίγουρο αν θα ωφελήσει ή θα βλάψει.

#enW: Μια άλλη έκδοση των δημοσιεύσιμων τροποποιήσεων, που μερικές φορές βρίσκεται λίγο πιο μπροστά από αυτή τη σελίδα.

#soW: Results of the Vote

#Wan: Δημόσια Έγγραφα σχετικά με την Ψήφο στην Ολομέλεια

#aaW: Τροποποιήσεις που έχουν παρουσιαστεί από διάφορες ομάδες και αναλύσεις αυτών, εφόσον μπορούν να δημοσιευτούν.

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Βοηθήστε να κάνουμε αυτή τη σελίδα πολυγλωσσική

#uhn: It is very much valued by many MEPs if they can read this analysis in their language.  This page can also be useful as a reference for the Council and for the second reading.

#diW: This page is created using a multilinguality managment system, which makes it convenient for you to contribute, if you observe a few rules.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: el ;
# multlin: t ;
# End: ;

