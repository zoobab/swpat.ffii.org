<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl 2003/09 Softwarepatent Richtlijn Amendementen: Echte vs Valse Limitering

#descr: Het Europees Parlement zal volgens de planning op 24 September over de Softwarepatent-richtlijn stemmen. De richtlijn zoals voorgesteld door de Europese Commissie vernietigt de basisstructuur van de huidige wet (Art. 52 van de Europese Patent Conventie) en vervangt het door de Trilaterale Standaard die uitgewerkt werd door de VS, Europese en Japanse Patentbureaus in 2000, volgens welke alle %(q:computer-geïmplementeerde) probleemoplossingen patenteerbare uitvindingen zijn. Sommige lede van het Parlement hebben amendementen voorgesteld met als doel het striktere uitvindingsconcept van de Europese Patent Conventie te bewaren, terwijl anderen kiezen voor ongelimiteerde patenteerbaarheid volgens de Trilaterale Standaard, maar wel in een rethorische vorm van beperkingen. We proberen een vergelijkende analyse te maken van alle voorgestelde amendementen om beslissingmakers te helpen herkennen of ze stemmen voor echte of valse limitering van patenteerbaarheid.

#Lgn: Legende en Becommentarieerde Voorbeelden

#ril: Artikels en Moties

#eia: Verwerping/Aanvaarding, Titel en Overwegingen

#enm: Amendement nummer

#jac: voor, tegen, onbeslist

#tuW: 410 stemden voor, 178 stemden tegen

#TitTit: Titel

#TitCec: Voorstel voor een richtlijn van het Europees Parlement en van de Raad over de patenteerbaarheid van computer-geïmplementeerde uitvindingen

#trW: tekst van het %(op:origineel voorstel van de Europese Commissie)

#TitSyn: Het kan niet het doel zijn van de huidige richtlijn om alle soorten %(q:computer-geïmplementeerde) ideeën patenteerbare uitvindingen te maken. Het doel is veeleer om de limieten van patenteerbaarheid i.v.m. automatische gegevensverwerking en zijn verschillende (technische en niet-technische) toepassingsgebieden te verduidelijken, en dit moet uitgedrukt worden in de titel in duidelijke en ondubbelzinnige bewoording.

#ung: Voorbeeld rechtvaardiging/commentaar

#enm2: Amendment nummers

#uia: Indieners

#amd: stemadvies

#ogs: resultaat stemming

#Am29: Proposal for a Directive of the European Parliament and of the Council on the limits of patentability with respect to automated data processing and its fields of application

#eet: amendment text

#tIa: Patent Insurance Extensions

#Art1: This Directive lays down rules for the patentability of computer-implemented inventions.

#nWw: Ideas implemented in generic computer hardware are not inventions in the sense of Art 52(2) EPC. The term %(q:computer-implemented invention) is not known to the man skilled in the art, and since the only things that can be implemented in a computer are computer programs (a washing machine or mobile phone cannot be implemented in a computer), it suggest that computer programs can be inventions. It was introduced by the EPO in 2000 in the infamous %(e6:Appendix 6 of the Trilateral Website document) in order to bring European patent law in line with US and JP and to allow patents for %(q:computer-implemented business methods), in anticipation of a deletion of Art 52(2) EPC.

#Am116: This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs.

#Art2a: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: Art 52 of the EPC clearly states that a stand-alone computer program (or a %(q:computer program as such) cannot constitute a patentable invention.

#fet: Amendments 36, 117 and 42 clarify that an innovation is only patentable if conforms to Art 52 of the EPC, regardless of whether or not a computer program is part of its implementation. In case of amendment 14, the following conforms to the given definition: %q(an invention the performance of which involves the use of a computer and having all of its features realised by means of a computer program). As such, it implies that a computer programs can be inventions, even though this contradicts article 52 EPC.

#Am36: %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

#Am14: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;

#Art2b: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Art2bSyn1: Een directieve die patenteerbaarheid laat afhangen van het woord %(q:technische bijdrage) moet deze term duidelijk uitleggen.

#Art2bSyn96: Amendement 96 lijkt erop te wijzen dat alleen het probleem maar niet de oplossing technisch moet zijn. Het woord %(q:significant) heeft geen wettelijke betekenis en is daardoor eerder aanleiding tot verwarring dan verduidelijking. Natuurlijk moet %(q:technisch) duidelijk gedefinieerd worden om een bruikbaar effect te hebben. Eisen dat een technisch probleem opgelost moet worden is zo slecht als CEC of erger. We moeten niet focussen op de toepassing (het opgeloste probleem), maar op de oplossing (het resultaat van empirisch onderzoek dat een patent kan verdienen). Van alle software kan gezegd worden dat het een technisch probleem oplost, maar als de oplossing alleen in de software innoveert, kan er niet gezegd worden dat het een technische oplossing is (tenminste niet een nieuwe technisch oplossing). Als je bekende fysische middelen neemt en ze gebruikt als uitwendige uitrusting voor een computer, kan je alle soorten technische problemen oplossen door programmeren. Dat maakt van deze oplossingen echter geen uitvindingen of leereffect dat ze bijdragen aan een technische bijdrage. (de laatste 2 zinnen zijn synoniem, uitvinding = technische bijdrage) Aan de andere kant kan een technische en patenteerbare uitvinding perfect een niet-technisch probleem oplossen (b.v. een computerchip kan alleen heel snel eenvoudige berekeningen maken, iets wat geen technisch probleem is), dus dit amendement zou zelfs uitvindingen uitsluiten die anders patenteerbaar zouden zijn afhankelijk van de interpretatie of definitie van technisch.

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Uiteindelijk krijgt amendement 107 het goed: dit stelt duidelijk dat %(q:technische bijdrage) een synoniem is voor uitvinding en herhaalt de patenteerbaarheidsvereisten van Art. 52 EPC. Dit amendement limiteert op geen enkele manier patenteerbaarheid, het bevestigt eenvoudigweg Art. 52 EPC en verwijdert alle onduidelijkheid die zou kunnen ontstaan door de patenteerbaarheidstesten te vermengen, zoals de originele CEC-tekst en de andere amendementen doen.

#Am69: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#Am107: %(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.

#Am96: %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#Art2baSyn: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendment 55 and 97 (amendments 37, which is also good, will be retracted to avoid a conflict with this one), which is found in various national case laws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and empistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 45 is apparently the result of mistranslation from French. The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day case law and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such, but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification.

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#Am37: %(q:Technology) means %(q:applied natural science).  %(q:Technical) means %(q:concrete and physical).

#Am39Syn: Dit is een standaarddoctrine in de meeste rechtspraken.  De %(q:vier natuurkrachten) zijn een bekend concept uit de epistemologie (wetenschapstheorie). Terwijl wiskunde abstract is en ongerelateerd met natuurkrachten, hangen sommige zakenmethodes wel af van chemie in de breincellen van de klant, wat echter niet aanstuurbaar is, i.e. niet-deterministisch, onderworpen aan de vrije wil. De term %(q:aanstuurbare natuurkrachten) sluit duidelijk uit wat uitgesloten moet worden en voorziet toch voldoende flexibiliteit voor het insluiten van mogelijke toekomstige velden van de toegepaste wetenschap die verder reiken dan de huidig erkende %(q:4 natuurkrachten). Bemerk dat dit amendement uitvindingen in apparaten als mobiele telefoons of wasmachines niet onpatenteerbaar maakt. Aan de andere kant, een computerprogramma toevoegen aan anders patenteerbare uitvinding, maakt deze uitvinding niet onpatenteerbaar (aangezien de uitvinding nog steeds een probleem oplost door gebruik te maken van van aanstuurbare natuurkrachten, of er nu een computerprogramma aanwezig is of niet).

#Am39: %(q:invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature) ;

#Am38Syn: We willen niet dat innovaties in de %(q:muziekindustrie) of %(q:wettelijke dienstenindustrie) aan de TRIPS-vereisten voldoen van %(q:industriële toepasbaarheid). Het woord %(q:industrie) wordt tegenwoordig vaak gebruikt in uitgebreide betekenis die niet gepast zijn binnen de context van het patentrecht.

#Am38: %(q:industry) in the sense of patent law means %(q:automated production of material goods);

#Art3: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Art3Syn: Dit artikel gaat over de interpretatie van Art 27 TRIPs. Het is het

#Art3aSyn1: Het is één van de belangrijkste doelstellingen van het richtlijnproject geweest om Art 52 EPC te verduidelijken in het licht van Art 27 TRIPs, dat zegt dat %(q:uitvindingen in alle gebieden van de technologie) octrooieerbaar moeten zijn. We stellen voor om Art 52 EPC te vertalen in de taal van Art 27 TRIPs, op de lijn van Amendement CULT-16.

#Art3aSyn2: Als gegevensverwerking %(q:technisch) is, dan is alles %(q:technisch).

#Art3aSyn3: Gegevensverwerking is een gemeenschappelijke basis voor alle technologische en niet-technologische gebieden. Met de komst van de (universele) computer in 1950, is automatische gegevensverwerking (AG) doorgedrongen tot de maatschappij en de industrie.

#Art3aSyn4: Zoals Gert Kolle, een leidinggevende theoreticus achter de beslissing van 1970 om software uit te sluiten van octrooieerbaarheid, schrijft in 1977 (zie Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: AG is vandaag een onmiskenbaar hulpmiddel geworden in alle domeingen van de menselijke maatschappij en zal dat blijven in de toekomst. Het is alomtegenwoordig. ... Zijn instrumentele betekenis, zijn hulp- en ondersteuningsfunctie onderscheiden AG van de ... individuele technologiegebieden en en het is net als bij gebeiden zoals bedrijfsadiminstratie, waarvan de werkresultaten en -methodes ... nodig zijn voor alle bedrijven en waarvoor daarom is er bij voorbaat nood aan de vrije beschikbaarheid ervan.

#Art3aSynCult: CULT heeft goed geadviseerd om te stemmen voor een duidelijke uitsluiting van gegevensverwerking van het gebied van %(q:technologie).

#Art3aSynStud: Talrijke studies, waarvan sommige uitgevoerd door EU-instellingen alsook de opinies van het Europees Economisch en Sociaal Comité en het Europees Comité van de Regio's verklaren in detail waarom Europa's economie schade zal leiden, als gegevensverwerking-innovaties niet duidelijk uitgesloten worden van octrooieerbaarheid.

#Am45: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#Art4N1: De lidstaten zorgen ervoor dat een in computers geïmplementeerde uitvinding patenteerbaar is op voorwaarde dat het een industriële toepassing kan hebben, nieuw is, en op uitvinderswerkzaamheid berust.

#Art4N2: De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers geïmplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert.

#Art4N3: De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.

#Art4Syn: This article mixes the non-obviousness test with the invention test, thereby weakening both and conflicting with Art 52 EPC.

#cpW: JURI has split this article into parts 1, 2 and 3.  This is dangerous because it prevents certain amendments, which may apply to the whole article, from being put to vote.

#iaa: Split into parts 1 2 and 3.

#Art4N1Syn: %(q:In computers geïmplementeerde uitvindingen) (d.w.z. ideeën gesteld in termen van universele gegevensbewerkingsapparatuur = programma's voor computers) zijn geen uitvindingen volgens artikel 52 EPC. Zie ook analyse voor artikel 1. Amendement 58=98=109 repareert de fout en herhaalt de strekking van Artikel 52 EPC. Geen van de amendementen beperken octrooieerbaarheid, maar dit amendement stelt de zaken tenminste formeel correct.

#Am16N1: Om octrooieerbaar te zijn, moet een in computers geïmplementeerde uitvinding vatbaar zijn voor industriële toepassing, nieuw zijn, en op uitvinderswerkzaamheid berusten.

#Am98: De lidstaten zorgen ervoor dat octrooien alleen worden verleend voor technische uitvindingen die nieuw en niet voor de hand liggend zijn, en vatbaar voor industriële toepassing.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: Een gepaste weergave van de strekking van Art. 52 EPC. Omdat het doel van de richtlijn is te harmoniseren en te verklaren, is het van het grootste belang om de correcte interpretatie van de EPC te benadrukken, omdat het de basis is van de patentwetgeving in Europa.

#Am48: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4N2Syn: De tekst van de Commissie wijkt af van Art 52 EPC. Zoals uitgelegd in de analyse van Verklaring 11, moet men eerst beoordelen of er een uitvinding/technische bijdrage is (ofwel met gebruik van de negatieve definitie in artikel 52 EPC: een computerprogramma is geen uitvinding, ofwel met gebruik van de positieve definitie die uitvloeit uit jurisprudentie, gebaseerd op de beheersbare natuurkrachten). Daarna moet er gecontroleerd worden of deze uitvinding aan de 3 andere toetsen van artikel 52 voldoet, waarvan de uitvinderswerkzaamheid (d.w.z. het niet voor de hand liggende) er één is. De commissietekst stelt dat de uitvinderswerkzaamheid een voorwaarde is voor iets om een uitvinding te zijn. Aangezien het mogelijk is dat een computerprogramma niet voor de hand liggend is, zou het volgens deze definitie een uitvinding kunnen zijn (in tegenstelling tot wat EPC Art. 52 zegt). Bovendien impliceert de commissietekst dat ideeën in termen van de universeel bruikbare computer (programma's voor computers) octrooieerbaar zijn. Verwijdering, zoals voorgesteld door amendement 82, zou helpen, omdat het enige effect van deze alinea is octrooieerbaarheidtoetsen te verwarren, en het daardoor onmogelijk te maken voor nationale patentbureaus om ongeldige octrooiaanvragen af te wijzen zonder uitgebreid onderzoek. Amendement 16 maakt dezelfde vergissing als de commissietekst, het poetst alleen de formulering wat op. Amendement 40 wordt teruggetrokken, het wordt nu als een invoeging ter tafel gebracht.

#Art4N2SynP2: Moreover, the Commission text implies that ideas framed in terms of the general purpose computer (programs for computers) are patentable inventions.  Deletion, as proposed by amendment 82, could be helpful, because the only effect of this paragraph is to confuse patentability tests and thereby make it impossible for national patent offices to reject non-statutory patent applications without substantive examination.  Amendment 16 makes the same mistake as the commission text, it just tidies up the language a bit. Amendment 40 will be retracted, it has been retabled as an insertion now (83).

#Am16N2: De concurrentiepositie van de Europese industrie ten opzichte van haar grootste handelspartners zou kunnen worden verbeterd als de huidige kloof in de rechterlijke praktijk met betrekking tot de begrenzing van octrooieerbaarheid van computerprogramma's zou worden geëlimineerd.

#Art4N3Syn1: Wanneer je een uitvinding octrooieert, claim je een bepaalde toepassing van deze uitvinding. Deze aanvraag is wat in de octrooiconclusie wordt beschreven. Als zodanig, bevat de octrooiconclusie (als geheel beschouwd) zowel de uitvinding zelf, als een aantal niet-octrooieerbare kenmerken (zoals bijvoorbeeld een computerprogramma) die benodigd zijn voor de genoemde toepassing van de uitvinding. Dit betekent dat zelfs als wordt geëist dat de uitvinding een technisch karakter heeft, de octrooiconclusie niet-technische kenmerken kan hebben (waar wij helemaal geen bezwaar tegen hebben, zo hebben octrooien altijd gewerkt).

#Art4N3Syn2: Echter, het voorstel van de Europese Commissie ontkracht haar eigen concept van %(q:technische bijdrage) door toe te staan dat de bijdrage bestaat in niet-technische kenmerken. De versie van de JURI stelt alleen dat de octrooiconclusie technische kenmerken moet omvatten, maar dit is geen echte beperking. Een voorbeeld kan dit verduidelijken. Stel dat wij een computerprogramma willen patenteren, dus we zeggen dat het de %(q: technische bijdrage) is. In de octrooiconclusie beschrijven we de toepassing van dit programma, dus ze zeggen dat we de uitvoering van dit programma op een computer octrooieren. Nu claimt het octrooi als geheel technische kenmerken (de computer), en het onderscheid tussen de claims als geheel (bestaande computer+nieuw programma) en de stand van de techniek (bekende computer) is het computerprogramma. Dus een computerprogramma kan volgens deze voorwaarde een technische bijdrage zijn, wat zichzelf volledig tegenspreekt (omdat een computerprogramma niet technisch kan zijn). Het is duidelijk dat een correctie als in amendement 57=99=110 noodzakelijk is, als het concept van %(q:technische bijdrage) al wordt gebruikt.

#lWo: De gratieperiode voor een nieuwheid, voorgesteld door amendement 100, is een orthogonale kwestie. Zoals aangetoond door een %(ng:recente raadpleging gevoerd door het Patentbureau van het Verenigd Koninkrijk) is het heel controversieel, zelfs onder hen die verondersteld worden ervan te profiteren. Het is niet duidelijk of een  gratieperiode voor een nieuwheid in feite, zoals door het ITRE rapport wordt gesuggereerd, MKB's ten goede zou komen. In het geval van open source ontwikkeling zou het zelfs bijkomende onzekerheid veroorzaken over of gepubliceerde ideeën vrij van octrooien zijn.

#Am16N3: De concurrentiepositie van de Europese industrie ten opzichte van haar grootste handelspartners zou kunnen worden verbeterd als de huidige kloof in de rechterlijke praktijk met betrekking tot de begrenzing van octrooieerbaarheid van computerprogramma's zou worden geëlimineerd.

#Am99: De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen alle technische kenmerken van de octrooiconclusie en de stand van de techniek.

#Am100P1: De aanmerkelijke mate van de technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de technische elementen in het bereik van de octrooiconclusie als geheel en de stand der techniek. Elementen die door de aanvrager zijn vrijgegeven voor eem octrooi over een periode van zes maanden voor de datum van de aanvraag zullen niet worden beschouwd als onderdeel van de stand der techniek bij het beoordelen van die octrooiconclusie.

#Art43aSyn: Zoals vermeld in de analyse van artikel 1, is %(q:in computers geïmplementeerde uitvinding) een contradiction in terminis. Zoals genoemd in de analyse van Verklaring 11, zijn %(q:uitvinding) en %(q:technische bijdrage) synoniemen: wat je hebt uitgevonden, is jouw (technische) bijdrage tot de stand der techniek. In het jargon dat wordt gebruikt door de Richtlijn betekent %(q:technische bijdrage) alleen %(q:oplossing voor een technisch probleem binnen de stap van de naaste stand der techniek tot de geclaimde uitvinding). In deze context is het onduidelijk welk effect een toets, hoe strict dan ook, zou kunnen hebben, want het is niet duidelijk waar het betrekking op heeft. Door te zeggen dat deze toets %(q:van toepassing moet zijn), sluit de bepaling niet duidelijk een keuze tussen deze en andere toetsen uit. Het zou zinvoller zijn geweest om te stellen dat een %(q:technische uitvinding) (=technische bijdrage) een %(q:inzicht ...) is. Tenslotte is er geen wettelijke definitie van %q(industriële toepassing in de stricte zin van de uitdrukking, in termen van zowel methode als resultaat), wat betekent dat dit deel van het amendement er niet in slaagt verklaring te bereiken. Toch bevelen we aan dit amendement te steunen, want het legt centrale aspecten van het concept van technische uitvinding vast, en kan daardoor, in combinatie met andere amendementen, uiteindelijk tot verklaring leiden. Art4aSyn: Amendement 17 neemt aan dat %(q:normale fysieke interactie tussen een programma en de computer) iets betekent. Dit is niet het geval. Er zou een betekenis aan toegekend kunnen zijn geweest, bijvoorbeeld door te specificeren, in navolging van het recente "Suche Fehlerhafter Zeichenketten" besluit van het Duitse Federale Octrooihof, dat %(q:vergrootte rekenefficiëntie), %(q:besparing in geheugengebruik) etc., %(q:niet een technische bijdrage inhouden), of %(q:binnen het bereik van normale fysieke interactie tussen een programma en de computer). Zonder zulke specificatie, maakt Amendement 17 de zaken onduidelijker in plaats van ze te verklaren.

#Art43aSynTest: Also, by saying that this test %(q:shall apply), the provision does not clearly require that this test actually %(e:must be passed), nor that it defines what is meant by %(q:technical contribution).

#Art43aSynInd: Finally, there is no legal definition of %(q:industrial application in the strict sense of the expression, in terms of both method and result).

#Art43aSynVal: In spite of these shortcomings, we recommend to support Amendment 70.  At least it codifies central aspects of the concept of %(q:technical invention).  Thereby, in combination with other amendments, it could eventually contribute to drawing a clear limit of patentability.

#Am70: Bij het beoordelen of een gegeven in computers geïmplementeerde uitvinding een technische bijdrage levert, zal de volgende toets worden toegepast: of het een nieuw inzicht in oorzaak-gevolg betrekkingen inhoudt bij het gebruik van beheersbare natuurkrachten en een industriële toepassing heeft in de stricte zin, in termen van zowel methode en resultaat.

#Art4aSyn: Amendement 17 neemt aan dat %(q:normale fysieke interactie tussen een programma en de computer) iets betekent. Dit is niet het geval. Er zou een betekenis aan toegekend kunnen zijn geweest, bijvoorbeeld door te specificeren, in navolging van het recente "Suche Fehlerhafter Zeichenketten" besluit van het Duitse Federale Octrooihof, dat %(q:vergrootte rekenefficiëntie), %(q:besparing in geheugengebruik) etc., %(q:niet een technische bijdrage inhouden), of %(q:binnen het bereik van normale fysieke interactie tussen een programma en de computer). Zonder zulke specificatie, maakt Amendement 17 de zaken onduidelijker in plaats van ze te verklaren.

#Am17: %(al|Uitsluitingen van octrooieerbaarheid|Een in computers geïmplementeerde uitvinding wordt niet beschouwd als een technische bijdrage, alleen omdat het het gebruik van een computer, netwerk of ander programmeerbaar apparaat omvat. Overeenkomstig, uitvindingen die over computerprogramma's gaan die zakelijke, wiskundige of andere methoden implementeren en die geen technische effecten produceren buiten de normale fysieke interactie tussen een programma en de computer, netwerk of ander programmeerbaar apparaat waarop het wordt uitgevoerd, is niet octrooieerbaar.)

#Art4bSyn: Zelfde commentaar als voor het bovenstaande amendement.

#Am87: Uitsluitingen van octrooieerbaarheid Een in computers geïmplementeerde uitvinding wordt niet beschouwd als een technische bijdrage alleen omdat het het gebruik van een computer, netwerk of ander programmeerbaar apparaat meebrengt. Vandaar, zijn uitvindingen die alleen uit computerprogramma's bestaan die zakelijke, wiskundige of andere methoden implementeren en die geen technische effecten produceren buiten de normale fysieke interactie tussen een programma en de computer, netwerk of ander programmeerbaar apparaat waarop het wordt uitgevoerd, niet octrooieerbaar.

#Art4cSyn1: De richtlijn zoals voorgesteld door CEC en JURI, gebaseerd op de "Pension Benefit System" doctrine van de EPO en de Trilaterale Standaard van de VS, Japanse and Europese PatentBureaus van 2000, beschouwen alle computerprogramma's als octrooieerbaare uitvindingen. Dit wordt uitgedrukt in %(q:in een computer geïmplementeerde uitvinding). Er is niet langer een toets of een uitvinding bestaat, maar alleen een test of %(q:technische bijdrage in de uitvinderswerkzaamheid), d.w.z. de stap tussen de %(q:naaste eerdere stand van de techniek) en de %(q:uitvinding) moet gesteld worden in termen van een %(q:technisch probleem). Dit is meestal een probleem van het verbeteren van efficientie in het gebruik van middelen in het gegevensverwerkend systeem. Daardoor wordt in de praktijk ieder algoritme of zakelijke methode octrooieerbaar, zoals is verklaard door het Duitse Federale Octrooihof, dat heeft geweigerd deze praktijk van de EPO te volgen.

#Art4cSyn2: Amendementen 47 en 60 gebieden de EPO duidelijk om de aanpak van het Duitse Federale Patenthof te volgen. Zoals alle juridische tekst, is dit op zich misschien niet genoeg om verder misbruik door de EPO te voorkomen, maar het geeft een duidelijk signaal af.

#Am60: De lidstaten zorgen ervoor dat in computers geïmplementeerde oplossingen voor technische problemen niet als octrooieerbare uitvindingen worden beschouwd alleen omdat zij efficientie in het gebruik van middelen binnen het gegevensverwerkende systeem vergroten.

#Art4dSyn1: Deze amendementen zijn de weinige die de juiste term %(q:gecomputeriseerde uitvindingen) (uitvindingen waarvan bij de toepassing het gebruik van een computer of computerprogramma betrokken is) in plaats van het zichzelf tegensprekende %(q:in computers geïmplementeerde uitvindingen). Zij bevestigen dat artikel 52 ook van toepassing is op deze klasse van uitvindingen. Dit kan niet genoeg gesteld worden.

#Am46: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Article 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#Art5: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Art5Syn18: Amendement 18 stelt voor om programmaclaims te introduceren, i.e. claims van de vorm  %(bc:een programma, gekarakteriseerd door het feit dat bij het inladen in computergeheugen [ één of ander proces ] wordt uitgevoerd.) Dit zou alle publicatie van software een potentiële rechtstreekse octrooi-inbreuk maken en dus programmeurs, onderzoekers en Internet-dienstverleners bedreigen. Zonder programmaclaims kunnen distributeurs alleen onrechtstreeks aansprakelijk gesteld worden, b.v. door de garanties die ze aan hun klant geven. Op die manier lijkt de programmaclaim meer van symbolische dan van materiële aard: de octrooilobby wil onmiskenbaar documenteren dat software op zich octrooieerbaar is. CEC heeft afgezien van deze laatste consequentie.

#Art5Syn58: Yet, the CEC wording still suggests that known general purpose computing hardware and calculation rules executed thereon (= computer programs) are patentable products and processes. Amendment 101/58 tries to address this problem.  It might be a good idea to replace %(q:technical production processes operated by such a computer) by %(q:the inventive processes running on such a set of equipment) if that is still possible in the form of a %(q:compromise amendment). Amendment 102 defines %(q:computer-implemented invention) as being only a product or a technical production process. As long as %(q:technical) is defined, this might be useful. Nevertheless, a computer running a program could also be interpreted as a %(q:programmed device), and patenting the use of a computer program when executed on a computer has the same effect as patenting the computer program itself: there is no other way to use a computer program than by executing it on a computer.

#Am18: A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

#Am101: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#Am102: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#Art5aSyn: Free software makes important contributions to innovation and knowledge diffusion without benefitting from the patent system.  This should be reflected in innovation policy and in law, without implying that proprietary software should be patentable.  Unfortunately this amendment uses the term %(q:computer-implemented inventions), which was introduced by the EPO in 2000 in an attempt to imply patentability of general purpose data processing operations (software).  This should be corrected in a second reading.

#Am62: %(al|Limitation of the effects of patents granted to computer-implemented inventions|%(linol|Member States shall ensure that the rights conferred by the patent shall not extend to the acts done to run, copy, distribute, study, change or improve a computer program which is distributed under a licence that provides for:|the freedom to run the program;|the freedom to study how the program works, and adapt it to the user's needs;|the freedom to redistribute copies under the same licence conditions;|the freedom to improve the program, and release improvements to the public under the same licence conditions;|free access to the source code of the program.))

#Art51a: This amendment apparently intends to exclude claims to ways of operating data processing hardware (= computer programs), but proposes inappropriate regulatory means.  Patent claims always comprise more than the invention (= technical contribution).  If there is a technical invention (e.g. a new chemical process), then there is no reason why the patent claim should not comprise this invention together with non-invention elements, such as a computer program that controls the reaction.  A claim does not describe an invention but the scope of an exclusion right which is legitimated by an invention.

#Am72: Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentansprüche nur den technischen Beitrag umfassen, der den Patentanspruch begründet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datenträger vorliegendes Programm, ist unzulässig.

#Am103Syn: Without this amendment, software patent owners could still send cease-and-desist letters to programmers or software distributors accusing them of contributory infringement.  This article makes sure that the right of publication as guaranteed by Art 10 ECHR takes precedence over patents.  On the other hand, this amendment does not prevent the granting of software patents or their enforcement against users of software.  Vendors of proprietary software and commercial Linux distributors could still be under pressure from their customers, who would usually impose a contractual obligation of patent clearance on them.

#Am103: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#Am104N1Syn: The scope of the patent is normally defined by the claims, and if something falls outside the claim scope, it does not constitute an infringment.  Thus the amendment, at first sight, seems tautological.  The intention of this amendment may be to allow simulation of patentable processes on a standard data processing system.  If so, it should be said clearly, as done by some of the other amendments.

#Am104N1: Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#Art5dSyn: These amendments do, unlike some may think, not serve to promote protect free/opensource software, but rather to ensure that the obligation of disclosure which is inherent in the patent system is taken seriously and that software is, like any other information object, on the disclosure side of the patent rather than on its exclusion/monopolisation side. This makes it a little more difficult to block people from doing things you  even haven't done yourself, but which are obviously possible since the computing model is perfectly defined and you always know what you can do with a computer. When you publish working source code you at least offer some real knowledge on how to solve the problem, unlike when you say that %(q:processor means coupled to input output means so that they compute a function such that the result of said function when output through said output means solves the problem the user wanted to solve).

#Am104N2: Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#Art6: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Art6Syn: The European Commission's proposal does not protect interoperability but, on the contrary, makes sure that interoperable software can not be published or used when an interface is patented. The only behavior which the European Commission wants to permit in this case is decompilation, which would not infringe on patents anyway (as patents involve the use of what is described in the claims, not the studying of said object). Amendments 66 and 67 moroever specify the laws on which interoperability privileges can be based, thus making it even more certain that the provision can not have any effect.

#Am19: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

#Art6aSyn: Amendments 20 and 50 are the only one that formulate a real interoperability privilege.  However, the word %(q:computer systems and network) should, for greater clarity, be changed to %(q:data processing systems), as proposed by amendment 50.  Amendment 20 was already accepted in CULT, ITRE and JURI.

#Am76Syn: Amendment 76 removes all clarity about when use of patented techniques for interoperation is allowed and when not.  It runs counter to the main purpose of the directive, which is to concretise certain abstract meta-rules, including those of Art 30 TRIPs, and thereby achieve clarity and legal security.

#Am105Syn: It is nice that amendment 105 agrees with us that a %(q:computer-implemented invention) cannot mean anything else but software running on a computer. However, the exceptions it mentions are very strange. In the first case, if the invention is an independent machine, there is no interoperability requirement. If there is no interoperability requirement, amendment 20 also won't apply. Conversely, if there is such a requirement, then the patented invention will not be working as an independent machine or technical invention. The second clause is quite dangerous, as it leaves everything for the courts to decide, thus failing to achieve the directive's aim of clarification. As the anti-trust cases against Microsoft in both the USA and in Europe shows, it is extremely difficult and time-consuming to determine whether any behavior violates existing competition law.  This would render the interoperability clause toothless and put SME's even more at a disadvantage, as large companies have much more money to spend on court cases.

#Am50: Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement..

#Am20: Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#Am76: Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that it does not unreasonably conflict with a normal exploitation of the patent and does not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#Am105: %(al|Use of patented technologies|Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that:|%(ol|the invention embodying the computer-implemented software is not working as an independent machine or technical invention, and|the patent is not in conflict with European competition law.))

#Art6b: This amendment seems to create a new property right outside of the patent system.  It establishes a category called %(q:computer-implemented inventions), which belong to no %(q:field of technology).  This is an interesting approach, although 7 years is probably still too long and some further questions need to be clarified in order for this to be workable.

#Am106: %(al|Term of patent|The term of a patent granted for computer-implemented inventions pursuant to this Directive shall be 7 years as from the date of filing.)

#Artt7: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Art7Syn: This article has no regulatory effect.  As designed currently, it moreover gives the Industrial Property Unit of the European Commission's Internal Market further funds and opportunities to produce propagandistic %(q:studies) that %(le:enjoy no respect in the academic community).  Although the effort of submitting and commenting amendments on this article is almost wasted, there is no doubt that the subject matter proposed in amendments 91ff by Marianne Thyssen is more worthy of study than what has been proposed by CEC and JURI so far.

#Am21: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and electronic commerce.

#Am91: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses. Special attention will be paid to the position of small and medium-sized enterprises and of electronic commerce and to the impact of dominant positions on the functioning of the market.

#Am71Syn: Amendment 71 calls for creation of an insurance system for patent acquisition and litigation, although there is no practical example of such a working system. The theoretical possibility as suggested by a consultants report to DG MARKT is also based on wrong assumptions, as explained in %(le:open letter to the European Parliament) written by a number of distinguished economists.

#Am90Syn: Amendment 90 is like 71, but less explicit.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises and the open source community, and electronic commerce.

#Am71P2: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents, in particular through the creation of a defence fund and the introduction of special rules on legal costs.

#Am71P3: It shall report on its findings to the European Parliament and the Council and present appropriate proposals for legislation without delay.

#rbr: De Commissie brengt bij het Europees Parlement en de Raad tegen uiterlijk [DATUM (drie jaar vanaf de in artikel 9, lid 1, vermelde datum)] verslag uit over:

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Rejection/Acceptance

#app: approves the proposed directive

#dWt: It is possible to correct the directive using the tabled amendments.  If the right amendments are adopted, the directive could be beneficial for the EU. So adopting a thoroughly corrected directive would be better than rejection. On the other hand, amending a directive this thoroughly is a complex task, especially in plenary, and the European Parliament may not succeed in fixing the abundant assumptions and misconceptions introduced by the Commission. The directive went through a procedure that hasn't taken into account most interested parties, academic studies or evidences. Rejecting the text and asking the Commission to start over with due process would also be justified. In fact, the number of mistakes needing correction is so large, that relying on the results of plenary vote for a good directive is very risky. Given the risk to the progress, freedom and the EU-companies' ability to compete if the amending partially fails, rejection is more desirable.  This is legislation for the next 20 years.  Although it has already taken some time, we are still in an early process of understanding and not in a hurry.

#ute: reject the proposed directive

#Rec1Cec: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec1Syn: The Commission boldly postulates, against all %(ss:economic evidence), including %(bh:very detailed empirical research by Bessen & Hunt from 2003), that patents encourage investments in software develoment.  Moreover, the term %(q:computer-implemented invention) is confusing.  It is better to have no target statement than a wrong one.

#Am1: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec5Cec: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Rec5Syn1: Both the European Commission (CEC) and Amendment 2 (JURI) say dogmatically that software patents stimulate innovation.  It would be more appropriate to state a policy goal, by which the proposed patentability regime can be measured.

#Rec5Syn2: Amendment 2 seems to claim that the mere fact of writing EU law, no matter what the contents, already %(q:results in legal certainty), simply because the Luxemburg court can interpret this law.  This new goal statement is not only questionable but also pointless, because %(ol|the Community Patent is under way anyway|there are easier and more systematic ways to put the Luxemburg court in charge than by passing an interpretative directive for one aspect of the European Patent Convention)

#Am2: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#css: Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

#Rec5aSyn: We agree that the rules laid down in Article 52 EPC have been stretched beyond recognition by the EPO and that the original meaning, as described in the 1978 EPO examination guidelines, should be reconfirmed.Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

#Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

#Rec6Cec: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Rec6Syn: We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it can excludes (or can be construed to exclude) software and business method patents, which the US government wants to mandate by the new Substantive Patent Law Treaty draft, see %(bh:Brian Kahin article in First Monday). Amendment 31 deletes the factually incorrect article.

#Am31: Deletion is good, but clarification would have been better.

#Rec7Cec: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Rec7Syn: The meaning of this provision seems unclear.  The purpose of a directive is usually to set rules, not to amend texts.  Even if the EPC member states were to decide that it would be appropriate to amend Art 52 EPC (rather than just to overrided by other legal means), they could still amend the EPC and say that thereby they are serving the purpose of the directive.

#Am32: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.

#Am3: The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.

#Rec7bSyn: New resolution calling for greater accountability of EPO. The effect would be stronger if it was an article, but it is still a good principle to have. It is important to state all sources of conflicts in order to ease eventual solutions.

#Am95: Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeÕs operating rules to ensure that it was publicly accountable in the exercise of its functions.

#cdn: Software patents would be bad for all software developers who aren't backed by a big company with a lot of money and lawyers, and this obviously includes a number of Free Software developers.

#Am61: Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: The four patentibility tests laid out in Art 52 EPC state that there must be a (technical) invention and that additionally, this invention must be usceptible of industrial application, new and involve an inventive step. The %(q:inventive step) conditions is defined in Art 56 EPC as requiring the invention not to be an obvious combination of known techniques to a person skilled in the art. Additionally, %(q:invention) and %(q:technical contribution) are synonyms: you can only invent new things, and this invention is your (technical) contribution to the state of the art. As such, the CEC originl and amendments 4 and 84 are in direct contradiction with the EPC. Additionally, such a logic assures that patentability is simply a question of claim wording, see the section on Four Separate Tests on One Unified Object.  In order to avoid confusion and conflicts with EPC Art 52 and 56, the recital needs to be rewritten or deleted.

#Am51: While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science.

#Am4: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.

#Am84: The legal protection of computerimplemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec11Syn: The Commission text declares computer programs to be technical inventions. It removes the independent requirement of invention (= %(q:technical contribution)) and merges it into the requirement of non-obviousness (= %(q:inventive step), see comments for Recital 11). This leads to theoretical inconsistency and undesirable practical consequences.

#Rec11bSyn: Amendment 73 would be a perfect restatement of Art 52 EPC if it had said %(q:inventions) instead of %(q:computer-implemented inventions). As it is now, it looks as if it was a special rule for inventions involving computers.   Yet it is useful in that reinforces the notion that the same invention must pass the four tests.  See Article 1 for why the term %(q:computer-implemented invention) is misleading.

#Am73: Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application..

#Rec12: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Rec12Syn: The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This contradicts Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible. Again, see the explanation in Recital 11 for more information. Amendment 5 by JURI makes matters even worse: first of all it literally reintroduces article 3 (which it deleted), secondly it claims the %(q:inventive step) requirement is something completely different from what Art 56 EPC says, thirdly it starts talking about a %(q:technical problem) which should be solved (while patentability has nothing to do with the kind of problem that is solved, but instead on the solution employed) and finally it also repeats the mistakes of the CEC original.

#Am5: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Am114: Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law.

#Rec13: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Rec13Syn: Placed in the context of patent examination practise, this statement is not what it seems.  It widens patentability even compared to EPO practise by allowing patentability of non-technical solutions to technical problems.  Moreover, it makes all algorithms patentable in all situations, because a %(q:technical problem) can always be defined in terms of generic computing hardware, which at the same time provides a way of claiming algorithms in their most abstract form.

#Rec13aSyn: Syntactical ambiguity in this amendment leaves it unclear whether %(q:business method) in general are considered to be a %(q:methods in which the only contribution to the state of the art is non-technical) or whether certain business methods could contain %(q:technical contributions).  More importantly, this amendment will not really exclude anything, because patent lawyers will be quick to claim that some computer-related features are characteristic for the %(q:invention), which, as stated elsewhere in this directive proposal, must be %(q:considered as a whole).  For this to be of any use, the syntactical ambiguity around %(q:other method) would have to be removed and the category of %(q:non-technical methods) would have to be explained by definitions and/or examples.

#Am6: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#Rec13bSyn: This states a common sense position which has often been disregarded in order to extend patentability.  This alone does not achieve much and the statement in the law that the law %(q:can not be circumvented) is a pious wish.  Yet, even a wish is better than nothing.

#Am7: If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

#Am8: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#Rec13dSyn1: The amendment simply restates the definition of claims: claims define the exclusion right granted by the patent. It allows claims to software by merely refering to generic computer equipment or computer processes, regardless of where the innovation lies. It's similar to saying: you can claim what you please as long as you use vocabulary like storage, processor, or apparatus somewhere in the claims, but be careful to make your claim as broad as you want it, because you won't monopolise what you didn't claim.

#Rec13dSyn2: The amendment may have an indirect benefit: it seems to contradict amendment 18 to article 5 by saying that only programmed apparatuses and processes can be claimed.

#Am9: The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.

#Rec14: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#Rec14Syn1: The CEC recital is not too bad (although it uses the %(q:computer-implemented inventions) term, see Article 1). It simply says patent law should not be replaced.  Amendment 10 is less clear.  On the one hand it equates %(q:present legal position) (?) with %(q:practices of the European Patent Office). On the other it says that the patentability of business methods is to be avoided. However the EPO itself has been granting many patents for business method and has provided %(a6:legal reasoning to support business method patents).  The only possible effect of the amendment, if any, would be to surrender legislative responsability to the EPO, whose practices would seem to define patentability instead of abiding to the laws voted by the European Parliament and other democraticaly representative bodies.

#Am86Syn: Amendment 86 does not ask for a codification of the current EPO practice, but on the other hand talks about %(q:unpatentable methods such as trivial procedures and business methods). Not just trivial business methods must remain unpatentable, but all business methods must remain so (otherwise we are deviating from Art 52 EPC, which should be avoided as suggested in amendment 88).

#Am10: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.

#Am86: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec16: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#Rec16Syn: Unification of case law in itself is not a guarantee of improvement of the situation of European industry. This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries. Whether or not software patents are good for European software development companies is independent of the fact that the traditional manufacturing industry is moving to low-cost economies outside the EU. Amendment 11 and the CEC text assume that extending patent protection to the software development industry will improve the competitiveness of EU companies, even though 75% of the 30,000 already granted software patents (which CEC/JURI call patents on %(q:computer-implemented inventions)) are in hands of US and Japanese companies. Additionally, if Europe does no have software patents, European companies still can perfectly acquire them in the US and elsewhere and enforce them there, while the affected foreign companies cannot do the same here. In that sense, not having software patents in Europe is a competitive advantage

#Am11: The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.

#Am35: De concurrentiepositie van de Europese industrie ten opzichte van haar grootste handelspartners zou kunnen worden verbeterd als de huidige kloof in de rechterlijke praktijk met betrekking tot de begrenzing van octrooieerbaarheid van computerprogramma's zou worden geëlimineerd.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Rec18: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Rec18Syn: The CEC proposal only pretends to protect interoperability, but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendment 13 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.

#Am13: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

#Rec18bSyn: This recital suggests that computer programs can be an invention (since there are no other innovations which have source code or which you can decompile), which contradicts article 52 EPC, since that one explicitly excludes computer programs from patentability. Software can never be part of an invention. It can be mentioned in the claims of the patent, but in that case it merely serves to clarify the application of the actual invention and to limit the scope of the monopoly that the patent grants to its holder (i.e., if someone manages to make use of the invention without using a computer program, he will not infringe on a patent that only describes the use of said invention in combination with a computer program).

#Am74: The application of this directive must not deviate from the original foundations of patent law, which means that the applicant of the patent must provide a description of all elements of the invention, including the source code, and that research into it, and as such decompilation, must be made possible. This way of working is indispensable to make compulsory licensing possible, for example if the obligation to supply the market is not fulfilled.

#Rec18tSyn: This recital also suggests that computer programs can be inventions.  Also, it has no effect, because it only restates undisputed patent doctrines.

#Am75: In any case, the law of all member states must ensure that the patents contain novelties and contain an inventive step, to prevent inventions which are already public from being appropriated, simply because they belong to a computer program.

#pmd: Europarl 2003/09: Amendementen

#eus: Hier zijn de amendementen voor de plenaire beslissing over de softwarepatentrichtlijn gepubliceerd in verschillende talen.

#ata: Many of the participants, including those who are supposed to benefit from the novelty grace period, express doubts about this double-edged concept.

#enW: Another version of the publishable amendments, sometimes slightly ahead of this page.

#soW: Results of the Vote

#Wan: Public Documents Regarding the Plenary Vote

#aaW: Amendments tabled by various groups and analyses thereof, as far as they can be published.

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Help make this page multilingual

#uhn: It is very much valued by many MEPs if they can read this analysis in their language.  This page can also be useful as a reference for the Council and for the second reading.

#diW: This page is created using a multilinguality managment system, which makes it convenient for you to contribute, if you observe a few rules.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: dietvu ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

