<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Conditions for Approval of the Software Patent Directive Proposal

#descr: If these conditions are met, it is assured that only %(q:computerised washing machines) but not algorithms and business methods can be patented.  It may be possible to achieve this on wednesday 2003/09/24, if all good amendments are supported by the majority.  However even then an additional round of cleanup work would be needed in order to obtain a clear and consistent directive.

#bWi: Tabular Listing

#jtW: No reason to accept a poorly written directive

#oio: Condition

#eAm: Needed Amendments

#eli: Freedom of publication is upheld.  The meaning of Art 10 ECHR and Art 30 TRIPs for freedom vs appropriability of functional texts is clarified.

#anW: %(pc:Program Claims) are not allowed.

#nne: Communication Standards are kept free.  The meaning of Art 30 TRIPs for interoperability is clarified.

#Pni: Data processing is not a field of technology in the sense of Art 27 TRIPs. Algorithms, Data Processing, Business methods are explicitely excluded from patentability, without ambiguous conditional sentences (such as %(q:that do not make a technical contribution)).

#frf: If the term %(q:technical) plays a central role, it is defined in terms of forces of nature.

#xti: If the term %(q:industrial) plays a central role, it is defined in terms of %(q:production of material goods).

#ait: If the tests (including subject matter / technical character, novelty, inventive step, industrial applicability) all apply to one object, the invention (= technical contribution), and there is no mingling of different tests into one (such as %(q:technical contribution in the inventive step)) or of the %(e:invention) with the %(e:claim scope).  There is no provision saying or implying that the %(q:technical contribution) may consist solely of non-technical features or that the technical part needn't be new or the new part needn't be technical.

#eys: The word %(q:computer-implemented invention) is eliminated.  Or at least it is redefined so that it really refers to %(q:washing machines and mobile phones), as the proponents claim it does, and not to what Art 2 of the CEC/JURI proposal says it is: rules for operating general-purpose computing equipment (= programs for computers).

#tfW: Apart from these minimal requirements, one should also insist that the directive is clear and free of contradictions.  Even if the directive is well amended by the Parliament this time, these requirements can hardly be met.  Therefore in case of doubt, rejection is probably the right choice.

#eti: It is better to have no directive than a bad directive.  It may well be that the EPO will then continue to grant software and business method patents against the letter and spirit of the European Patent Convention, but these patents will be less enforcable before national courts than ever, and even EPO has shown some responsiveness to the public criticism.  The EPO's Enlarged Board of Appeal (EBO) was not even heard, when the Technical Boards of Appeal, which are not competent in juridical matters, drastically changed their jurisdiction in 1986, 1998 and 2000.  The Technical Board of Appeal can be expected to gradually return to a good practise, unless the EU imposes a bad directive.

#whW: Threats that the Commission and Council will reject the directive if the PSE/Kauppi/Greens/UEN/EDD amendments prevail, as have been uttered recently by Frits Bolkestein and earlier by Arlene McCarthy, once more cast doubt on the proponents' commitment to its stated goals of clarification and %(q:harmonisation).  The study written for the Parliament by Bakels and Hugenholtz explains why the Commission proposal fails on these goals.  The JURI draft is no better.  While Bolkestein is suggesting that the Parliament's amendments may run counter to TRIPs, the Commission's proposal itself, by introducing a (bogus) requirement of %(q:technical contribution in the inventive step) deviates from both TRIPs and the EPC.  It is the European Commission itself which has failed to concretise Art 27ff TRIPs, and many of the amendments, such as 20, 50, 45 and 107, are trying to fix this.  As soon as it is amended enough so as to achieve this aim, the directive deserves to be approved.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plenkond0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

