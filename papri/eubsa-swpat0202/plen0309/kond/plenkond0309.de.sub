\begin{subdocument}{plenkond0309}{Bedingungen f\"{u}r die Billigung der Softwarepatent-\"{A}nderungsantr\"{a}ge}{http://swpat.ffii.org/papiere/eubsa-swpat0202/plen0309/kond/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/09/24 von Yven Leist\footnote{http://lists.ffii.org/mailman/listinfo/traduk/index.html}}{Wenn diese Bedingungen erf\"{u}llt sind, ist sichergestellt das nur ``computergesteuerte Waschmaschinen'' und nicht Algorithmen und Gesch\"{a}ftsmethoden patentiert werden k\"{o}nnen. Wenn alle guten Erg\"{a}nzungen von der Mehrheit unterst\"{u}tzt werden k\"{o}nnte es m\"{o}glich sein dies am Mittwoch 2003/09/24 zu erreichen. Selbst dann w\"{a}re jedoch eine weitere Runde an Ausbesserungsarbeiten n\"{o}tig um eine klare und widerspruchsfreie Richtlinie zu erhalten.}
\begin{sect}{kond}{Tabellarische Auflistung}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
Bedingung & Notwendige \"{A}nderungsantr\"{a}ge\\\hline
Die Publikationsfreiheit bleibt aufrechterhalten. Die Bedeutung des Art 10 ECHR und ART 30 TRIPS bez\"{u}glich Freiheit vs. der M\"{o}glichkeit zur Aneignung funktionaler Texte wird gekl\"{a}rt. & +(113|33)\\\hline
Programmanspr\"{u}che\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prog/index.en.html} sind nicht erlaubt. & {\bf -18}, +(49=58=101)\\\hline
Kommunikations-Standards werden frei gehalten. Die Bedeutung von Art 30 TRIPS f\"{u}r Interoperabilit\"{a}t wird gekl\"{a}rt. & +(50\texmath{>}(20=76.1=105.1)), -76.2, -105.2\\\hline
Datenbearbeitung ist kein Technologiebereich im Sinne des Art 27 TRIPs. Algorithmen, Datenverarbeitung, Gesch\"{a}ftsmethoden werden ausdr\"{u}cklich von der Patentierbarkeit ausgeschlossen, ohne verschwommene Konditionals\"{a}tze (wie z.B. ``welche keinen technischen Beitrag leisten''). & +45, +108, +((51=113)\texmath{>}33)\\\hline
Sobald der Begriff ``technisch'' eine entscheidende Rolle spielt, wird er im Hinblick auf Kr\"{a}fte der Natur definiert. & +(101=58=49|{\bf 97=55=108}|37|72|49=58=101)\\\hline
Sobald der Begriff ``gewerblich'' eine entscheidende Rolle spielt, wird er im Hinblick auf die ``Herstellung materieller G\"{u}ter'' definiert. & +(38=44=118)\\\hline
Wenn die Tests (einschlie{\ss}lich Gegenstand / technischer Charakter, Neuheit, Erfindungsh\"{o}he, gewerbliche Anwendbarkeit) alle auf ein Objekt, die Erfindung(technischer Beitrag), zutreffen, und es es keine Vermischung verschiedener Tests in einen (so wie z.B. ``einem technischen Beitrag in dem Erfindungsschritt'' oder der \emph{Erfindung} mit dem ``Anspruchsumfang'' gibt. Es gibt keine Bestimmung die sagt oder impliziert, dass der ``technische Beitrag'' nur aus nicht technischen Merkmalen bestehen darf oder dass der technische Teil nicht neu sein muss oder der neue Teil nicht technisch sein muss. & +(57=97=110), +(56=98=109), +82, +107, +114, -86, -10.2\\\hline
Der Begriff ``computerimplementierte Erfindung'' wird entfernt. Oder zumindest wird er so neu definiert dass er sich wirklich auf ``Waschmaschinen und Handys'' bezieht, was er laut Behauptungen der Bef\"{u}rworter tut, und nicht auf dass was Art 2 des CEC/JURI Vorschlags sagt was er sei: Rechenregeln f\"{u}r den Betrieb von allgemeinen Datenverarbeitungs-Systemen (= Programme f\"{u}r Computer)\par

siehe auch Was ist eine Computerimplementierte Erfindung?\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/kinv/index.de.html} & +({\bf 36=42=117}), +(29=59=41), +114, +116\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{ratio}{Kein Grund eine schlecht verfasste Richtlinie zu akzeptieren.}
Abgesehen von diesen minimalen Anforderungen, sollte auch darauf bestanden werden dass die Richtlinie klar und widerspruchsfrei ist. Selbst wenn die Richtlinie diesmal in sinnvoller Weise vom Parlament ge\"{a}ndert wird, k\"{o}nnen diese Anforderungen kaum erf\"{u}llt werden. Somit ist im Zweifelsfall eine Ablehnung wohl die richtige Entscheidung.

Es ist besser keine Richtlinie zu haben als eine schlechte Richtlinie. Es k\"{o}nnte durchaus sein dass die EPO dann weiterhin gegen den Wortlaut und die grunds\"{a}tzliche Idee des Europ\"{a}ischen Patentabkommens Patente auf Software- und Gesch\"{a}ftsmethoden gew\"{a}hren wird, aber diese Patente werden dann vor nationalen Gerichten noch weniger einklagbar sein als zuvor, und sogar die EPO hat eine gewisse Reaktion auf den \"{o}ffentlichen Protest hin gezeigt. Die vergr\"{o}{\ss}erte Beschwerdekammer (EBO) der EPO wurde nicht einmal angeh\"{o}rt als die technischen Beschwerdekammern, die in juristischen Angelegenheiten keine Kompetenz besitzen, ihre Rechtsprechung 1986, 1998 und 2000 drastisch \"{a}nderten. Man kann davon ausgehen, dass die technische Beschwerdekammer allm\"{a}hlich wieder zu einem sinnvollen Verfahren zur\"{u}ckkehren wird, sofern die EU keine schlechte Richtlinie durchsetzt.

Drohungen dass die Kommission und der Rat die Richtlinie verwerfen falls sich die Erg\"{a}nzungen von PSE/Kauppi/Greens/UEN/EDD durchsetzen sollten, wie sie vor kurzem von Frits Bolkestein und fr\"{u}her von Arlene McCarthy ge\"{a}u{\ss}ert wurden, lassen das Engagement der Bef\"{u}rworter bez\"{u}glich ihres verk\"{u}ndeten Ziels der Kl\"{a}rung und ``Harmonisierung'' einmal mehr zweifelhaft erscheinen. Die f\"{u}r das Parlament von Bakels und Hugenholtz erstellte Studie erkl\"{a}rt warum der Vorschlag der Kommission bez\"{u}glich dieser Ziele scheitert. Der JURI Entwurf ist keineswegs besser. W\"{a}hrend Bolkestein darauf hinweist, dass die Erg\"{a}nzungen des Parlaments TRIPs zuwiderlaufen k\"{o}nnten, weicht der Vorschlag der Kommission selbst sowohl von TRIPs und der EPC ab, indem er eine (Schein)Forderung nach ``einem technischen Beitrag in dem Erfindungsschritt'' einf\"{u}hrt. Es ist die Europ\"{a}ische Kommission selbst die es nicht geschafft hat Art 27ff TRIPs zu konkretisieren und viele der Erg\"{a}nzungen, wie z.B. 20, 50, 45 und 107 versuchen dies zu beheben. Sobald die Richtlinie weitgehend genug ge\"{a}ndert worden ist um dieses Ziel zu erreichen verdient sie es gebilligt zu werden.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Europarl 2003/09 Softwarepatent-\"{A}nderungsantr\"{a}ge: Echte und Scheinbare Begrenzungen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/plen0309/index.de.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 24th.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Programmanspr\"{u}che: Verbote der Ver\"{o}ffentlichung N\"{u}tzlicher Patentbeschreibungen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prog/index.en.html}}}

\begin{quote}
Patentanspr\"{u}che auf ein ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', werden genannt ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' oder ``information claims''. Patente die diesen Anspruch enthalten werden manchmal auch ``text patents'' oder ``information patents'' genannt. Solche Patente monopolisieren nicht l\"{a}nger ein physikalisches Objekt sondern eine Beschreibung eines solchen Objekts.  Ob dies erlaubt werden sollte ist eine der kontroversen Fragen in der Auseinandersetzung um die vorgeschlagene EU Software Patent Richtlinie. Wir versuchten zu erkl\"{a}ren wie diese Debatte aufkam und was wirklich auf dem Spiel steht.
\end{quote}
\filbreak

\item
{\bf {\bf Warum Amazon One Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
Gem\"{a}ss des Richtlinienvorschlags COM(2002)92 der Europ\"{a}ischen Kommission (CEC) f\"{u}r ``Patentierbarkeit Computer-Implementierter Erfindungen'' und die \"{u}berarbeitete Version genehmigt durch das Komitee f\"{u}r Rechtsangelegenheiten und Binnenmarkt (JURI) des Europa Parlaments, sind Algorithmem und Gesch\"{a}ftmethoden, wie etwa Amazon One Click Shopping, ohne Zweifel als patentierbare Gegenst\"{a}nde zu betrachten. Die ist so weil \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO). The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Interoperabilit\"{a}t und die Softwarepatent-Richtlinie: Welche Schranken sind Erforderlich?\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  These patent lawyers are spreading fear propaganda about the implications of Art 6a and pressing hard to have it removed.  After the summer pause of 2003, Arlene McCarthy proposed an amendment to Art 6a which would render Art 6a meaningless.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf Was ist eine Computerimplementierte Erfindung?\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/kinv/index.de.html}}}

\begin{quote}
Eine Waschmaschine mit eingebettetem Rechner?  Oder ein allgemein einsetzbares Datenverarbeitungsprogramm?  In der \"{o}ffentlichen Debatte sagen die Bef\"{u}rworter des EU Software Patent Richtlinienvorschlags, dass sie keine Patente auf ``reine Software'' wollen, sondern nur auf ``Computerimplementierte Erfindungen'', womit sie, wie sie sagen, ``Waschmaschinen und Mobiltelefone'' meinen.  Art 2 des Vorschlags widerspricht dem jedoch, wie auch die Geschichte und Praxis des EPA, Patente auf ``Computerimplementierte Erfindungen'' zu erteilen.
\end{quote}
\filbreak

\item
{\bf {\bf Wuermeling ver\"{o}ffentlicht neue PE Gegen (F\"{u}r) Softwarepatente\footnote{http://swpat.ffii.org/index.de.html}}}

\begin{quote}
In den letzten Jahren hat das Europ\"{a}ische Patentamt (EPA) gegen den Buchstaben und Geist der geltenden Gesetze \"{u}ber 30.000 Patente auf computer-eingekleidete Organisations- und Rechenregeln (laut Gesetz \emph{Programme f\"{u}r Datenverarbeitungsanlagen}, laut EPA-Neusprech von 2000 ``computer-implementierte Erfindungen'') erteilt.  Nun m\"{o}chte Europas Patentbewegung diese Praxis durch ein neues Gesetz festschreiben.  Europas Programmierer und B\"{u}rger sehen sich betr\"{a}chtlichen Risiken ausgesetzt.  Hier finden Sie die grundlegende Dokumentation zur aktuellen Debatte, ausgehend von einer Kurzeinf\"{u}hrung und \"{U}bersicht \"{u}ber die neuesten Entwicklungen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/plenkond0309.el ;
% mode: latex ;
% End: ;

