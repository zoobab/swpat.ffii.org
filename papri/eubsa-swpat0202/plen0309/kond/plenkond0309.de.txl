<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bedingungen für die Billigung der Softwarepatent-Änderungsanträge

#descr: Wenn diese Bedingungen erfüllt sind, ist sichergestellt das nur %(q:computergesteuerte Waschmaschinen) und nicht Algorithmen und Geschäftsmethoden patentiert werden können. Wenn alle guten Ergänzungen von der Mehrheit unterstützt werden könnte es möglich sein dies am Mittwoch 2003/09/24 zu erreichen. Selbst dann wäre jedoch eine weitere Runde an Ausbesserungsarbeiten nötig um eine klare und widerspruchsfreie Richtlinie zu erhalten.

#bWi: Tabellarische Auflistung

#jtW: Kein Grund eine schlecht verfasste Richtlinie zu akzeptieren.

#oio: Bedingung

#eAm: Notwendige Änderungsanträge

#eli: Die Publikationsfreiheit bleibt aufrechterhalten. Die Bedeutung des Art 10 ECHR und ART 30 TRIPS bezüglich Freiheit vs. der Möglichkeit zur Aneignung funktionaler Texte wird geklärt.

#anW: %(pc:Programmansprüche) sind nicht erlaubt.

#nne: Kommunikations-Standards werden frei gehalten. Die Bedeutung von Art 30 TRIPS für Interoperabilität wird geklärt.

#Pni: Datenbearbeitung ist kein Technologiebereich im Sinne des Art 27 TRIPs. Algorithmen, Datenverarbeitung, Geschäftsmethoden werden ausdrücklich von der Patentierbarkeit ausgeschlossen, ohne verschwommene Konditionalsätze (wie z.B. %(q:welche keinen technischen Beitrag leisten)).

#frf: Sobald der Begriff %(q:technisch) eine entscheidende Rolle spielt, wird er im Hinblick auf Kräfte der Natur definiert.

#xti: Sobald der Begriff %(q:gewerblich) eine entscheidende Rolle spielt, wird er im Hinblick auf die %(q:Herstellung materieller Güter) definiert.

#ait: Wenn die Tests (einschließlich Gegenstand / technischer Charakter, Neuheit, Erfindungshöhe, gewerbliche Anwendbarkeit) alle auf ein Objekt, die Erfindung(technischer Beitrag), zutreffen, und es es keine Vermischung verschiedener Tests in einen (so wie z.B. %(q:einem technischen Beitrag in dem Erfindungsschritt) oder der %(e:Erfindung) mit dem %(q:Anspruchsumfang) gibt. Es gibt keine Bestimmung die sagt oder impliziert, dass der %(q:technische Beitrag) nur aus nicht technischen Merkmalen bestehen darf oder dass der technische Teil nicht neu sein muss oder der neue Teil nicht technisch sein muss.

#eys: Der Begriff %(q::computerimplementierte Erfindung) wird entfernt. Oder zumindest wird er so neu definiert dass er sich wirklich auf %(q:Waschmaschinen und Handys) bezieht, was er laut Behauptungen der Befürworter tut, und nicht auf dass was Art 2 des CEC/JURI Vorschlags sagt was er sei: Rechenregeln für den Betrieb von allgemeinen Datenverarbeitungs-Systemen (= Programme für Computer)

#tfW: Abgesehen von diesen minimalen Anforderungen, sollte auch darauf bestanden werden dass die Richtlinie klar und widerspruchsfrei ist. Selbst wenn die Richtlinie diesmal in sinnvoller Weise vom Parlament geändert wird, können diese Anforderungen kaum erfüllt werden. Somit ist im Zweifelsfall eine Ablehnung wohl die richtige Entscheidung.

#eti: Es ist besser keine Richtlinie zu haben als eine schlechte Richtlinie. Es könnte durchaus sein dass die EPO dann weiterhin gegen den Wortlaut und die grundsätzliche Idee des Europäischen Patentabkommens Patente auf Software- und Geschäftsmethoden gewähren wird, aber diese Patente werden dann vor nationalen Gerichten noch weniger einklagbar sein als zuvor, und sogar die EPO hat eine gewisse Reaktion auf den öffentlichen Protest hin gezeigt. Die vergrößerte Beschwerdekammer (EBO) der EPO wurde nicht einmal angehört als die technischen Beschwerdekammern, die in juristischen Angelegenheiten keine Kompetenz besitzen, ihre Rechtsprechung 1986, 1998 und 2000 drastisch änderten. Man kann davon ausgehen, dass die technische Beschwerdekammer allmählich wieder zu einem sinnvollen Verfahren zurückkehren wird, sofern die EU keine schlechte Richtlinie durchsetzt.

#whW: Drohungen dass die Kommission und der Rat die Richtlinie verwerfen falls sich die Ergänzungen von PSE/Kauppi/Greens/UEN/EDD durchsetzen sollten, wie sie vor kurzem von Frits Bolkestein und früher von Arlene McCarthy geäußert wurden, lassen das Engagement der Befürworter bezüglich ihres verkündeten Ziels der Klärung und %(q:Harmonisierung) einmal mehr zweifelhaft erscheinen. Die für das Parlament von Bakels und Hugenholtz erstellte Studie erklärt warum der Vorschlag der Kommission bezüglich dieser Ziele scheitert. Der JURI Entwurf ist keineswegs besser. Während Bolkestein darauf hinweist, dass die Ergänzungen des Parlaments TRIPs zuwiderlaufen könnten, weicht der Vorschlag der Kommission selbst sowohl von TRIPs und der EPC ab, indem er eine (Schein)Forderung nach %(q:einem technischen Beitrag in dem Erfindungsschritt) einführt. Es ist die Europäische Kommission selbst die es nicht geschafft hat Art 27ff TRIPs zu konkretisieren und viele der Ergänzungen, wie z.B. 20, 50, 45 und 107 versuchen dies zu beheben. Sobald die Richtlinie weitgehend genug geändert worden ist um dieses Ziel zu erreichen verdient sie es gebilligt zu werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: yleist ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plenkond0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

