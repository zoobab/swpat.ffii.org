\begin{subdocument}{plenkond0309}{Conditions for Approval of the Software Patent Directive Proposal}{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/kond/index.en.html}{Workgroup\\swpatag@ffii.org}{If these conditions are met, it is assured that only ``computerised washing machines'' and not algorithms and business methods can be patented.  It may be possible to achieve this on wednesday 2003/09/24, if all good amendments are supported by the majority.  However even then an additional round of cleanup work would be needed in order to obtain a clear and consistent directive.}
\begin{sect}{kond}{Tabular Listing}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
Condition & Needed Amendments\\\hline
Freedom of publication is upheld.  The meaning of Art 10 ECHR and Art 30 TRIPs for freedom vs appropriability of functional texts is clarified. & +(113|33)\\\hline
Program Claims\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prog/index.en.html} are not allowed. & {\bf -18}, +(49=58=101)\\\hline
Communication Standards are kept free.  The meaning of Art 30 TRIPs for interoperability is clarified. & +(50\texmath{>}(20=76.1=105.1)), -76.2, -105.2\\\hline
Data processing is not a field of technology in the sense of Art 27 TRIPs. Algorithms, Data Processing, Business methods are explicitely excluded from patentability, without ambiguous conditional sentences (such as ``that do not make a technical contribution''). & +45, +108, +((51=113)\texmath{>}33)\\\hline
If the term ``technical'' plays a central role, it is defined in terms of forces of nature. & +(101=58=49|{\bf 97=55=108}|37|72|49=58=101)\\\hline
If the term ``industrial'' plays a central role, it is defined in terms of ``production of material goods''. & +(38=44=118)\\\hline
If the tests (including subject matter / technical character, novelty, inventive step, industrial applicability) all apply to one object, the invention (= technical contribution), and there is no mingling of different tests into one (such as ``technical contribution in the inventive step'') or of the \emph{invention} with the \emph{claim scope}.  There is no provision saying or implying that the ``technical contribution'' may consist solely of non-technical features or that the technical part needn't be new or the new part needn't be technical. & +(57=97=110), +(56=98=109), +82, +107, +114, -86, -10.2\\\hline
The word ``computer-implemented invention'' is eliminated.  Or at least it is redefined so that it really refers to ``washing machines and mobile phones'', as the proponents claim it does, and not to what Art 2 of the CEC/JURI proposal says it is: rules for operating general-purpose computing equipment (= programs for computers).\par

see also What is a Computer-Implemented Invention?\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/kinv/index.en.html} & +({\bf 36=42=117}), +(29=59=41), +114, +116\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{ratio}{No reason to accept a poorly written directive}
Apart from these minimal requirements, one should also insist that the directive is clear and free of contradictions.  Even if the directive is well amended by the Parliament this time, these requirements can hardly be met.  Therefore in case of doubt, rejection is probably the right choice.

It is better to have no directive than a bad directive.  It may well be that the EPO will then continue to grant software and business method patents against the letter and spirit of the European Patent Convention, but these patents will be less enforcable before national courts than ever, and even EPO has shown some responsiveness to the public criticism.  The EPO's Enlarged Board of Appeal (EBO) was not even heard, when the Technical Boards of Appeal, which are not competent in juridical matters, drastically changed their jurisdiction in 1986, 1998 and 2000.  The Technical Board of Appeal can be expected to gradually return to a good practise, unless the EU imposes a bad directive.

Threats that the Commission and Council will reject the directive if the PSE/Kauppi/Greens/UEN/EDD amendments prevail, as have been uttered recently by Frits Bolkestein and earlier by Arlene McCarthy, once more cast doubt on the proponents' commitment to its stated goals of clarification and ``harmonisation''.  The study written for the Parliament by Bakels and Hugenholtz explains why the Commission proposal fails on these goals.  The JURI draft is no better.  While Bolkestein is suggesting that the Parliament's amendments may run counter to TRIPs, the Commission's proposal itself, by introducing a (bogus) requirement of ``technical contribution in the inventive step'' deviates from both TRIPs and the EPC.  It is the European Commission itself which has failed to concretise Art 27ff TRIPs, and many of the amendments, such as 20, 50, 45 and 107, are trying to fix this.  As soon as it is amended enough so as to achieve this aim, the directive deserves to be approved.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Program Claims: Bans on Publication of Useful Patent Descriptions\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prog/index.en.html}}}

\begin{quote}
Patent Claims to ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', are called ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' or ``information claims''.  Patents which contain these claims are sometimes called ``text patents'' or ``information patents''.  Such patents no longer monopolise a physical object but a description of such an object.  Whether this should be allowed is one of the controversial questions in the struggle about the proposed EU Software Patent Directive.  We try to explain how this debate emerged and what is really at stake.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Interoperability and the Software Patents Directive: What Degree of Exemption is Needed\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  These patent lawyers are spreading fear propaganda about the implications of Art 6a and pressing hard to have it removed.  After the summer pause of 2003, Arlene McCarthy proposed an amendment to Art 6a which would render Art 6a meaningless.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf What is a Computer-Implemented Invention?\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/kinv/index.en.html}}}

\begin{quote}
A washing machine with an embedded computer?  Or a general-purpose data processing program?  In their public discourse, the promoters of the EU software patent directive proposal say they do not want patents on ``pure software'' but only on ``computer-implemented inventions'', by which they say they mean ``washing machines and mobile phones''.  Art 2 of the proposal itself however says otherwise, as does the history and practise of granting patents for ``computer-implemented inventions'' at the EPO.
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/plenkond0309.el ;
% mode: latex ;
% End: ;

