<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Condiciones para la Aprobación de la Propuesta de Directiva de Patentes de Software

#descr: Si se cumplen estas condiciones, se asegura que sólo pueden patentarse las %(q:lavadoras computerizadas) y no los algoritmos y métodos de negocio. Podría ser posible conseguir esto el miércoles 2003/09/24, si la mayoría apoya todas las enmiendas apropiadas. No obstante, entonces será necesaria una ronda de trabajo de limpieza para obtener una directiva clara y consistente.

#bWi: Listado Tabular

#jtW: Ninguna razón para aceptar una directiva pobremente redactada

#oio: Condición

#eAm: Enmiendas necesarias

#eli: Se confirma la libertad de publicación. Se aclara el significado del Art 10 de ECHR y del Art 30 del TRIPs para la libertad vs conveniencia de los textos funcionales.

#anW: Las %(pc:Reivindicaciones de Programas) no están permitidas.

#nne: Los Estándares de Comunicación se mantienen libres. Se aclara el significado del Art 30 del TRIPs para la interoperabilidad.

#Pni: El procesamiento de datos no es un campo de la tecnología en el sentido del Art 27 del TRIPs. Los algoritmos, el procesamiento de datos, los métodos de negocio, están explícitamente excluidos de la patentabilidad, sin frases condicionales ambiguas (tales como %(q:que no hacen una contribución técnica)).

#frf: Si el término %(q:técnico) juega un papel central, se define en términos de fuerzas de la naturaleza.

#xti: Si el término %(q:industrial) juega un papel centra, se define en términos de %(q:producción de bienes materiales).

#ait: Si los ensayos (incluyendo la materia que es sujeto / el carácter técnico, la novedad, el paso inventivo, la aplicabilidad industrial) se aplican todos a un objeto, la invención (= contribución técnica), y no hay una mezcla de diferentes ensayos en uno (tal como %(q:contribución técnica en el paso inventivo)) o de la %(e:invención) con el %(e:ámbito de la reivindicación). Noy hay estipulación que diga o implique que la %(q:contribución técnica) podría consistir sólamente en características no técnica, o que la parte técnica no tiene que ser nueva, o que la parte nueva no tiene que ser técnica.

#eys: Se elimina la expresión %(q: invención implementada en ordenador). O al menos se redefine de forma que se refiera realmente a %(q:lavadoras y teléfonos móviles), tal como lo hace la reivindicación de los proponentes, y no a lo que el Art de la propuesta de la CEC/JURI dice que es: reglas para operar equipamiento informático de propósito general (= programas para ordenadores).

#tfW: Apart from these minimal requirements, one should also insist that the directive is clear and free of contradictions.  Even if the directive is well amended by the Parliament this time, these requirements can hardly be met.  Therefore in case of doubt, rejection is probably the right choice.

#eti: Es mejor no tener directiva alguna que tener una mala directiva. Bien pudiera suceder que entonces la EPO continua concediendo patentes de software y métodos de negocio en contra de la letra y del espíritu de la Convención de Patentes Europea, pero estas patentes serán menos exigibles que nunca ante tribunales nacionales, e incluso la EPO ha mostrado cierta respuesta a la crítica pública. El Comité Ampliado de Apelaciones (EBO) de la EPO no fue siquiera consultado cuando los Comités Técnicos de Apelaciones, los cuales no son competentes en materias jurídicas, cambiaron drásticamente su jurisdicción en 1986, 1998 y 2000. Se puede esperar que el Comité Técnico de Apelaciones vuelva gradualmente a una práctica correcta, a menos que la EU imponga una mala directiva.

#whW: La amenazas de que la Comisión y el Consejo rechazarían la directiva si prevalecían las enmiendas del PSE/Kauppi/Greens/UEN/EDD, tal como han sido recientemente proferidas por Frist Bolksetein y anteriomente por Arlene McCarthy, siembran dudas una vez más sobre la disposición de los proponentes sobre los objetivos establecidos de clarificación y (q:armonización). El estudio redactado para el Parlamento por Bakels y Hugenholtz explica porqué la propuesta de la Comisión falla en estos objetivos. El borrador del JURI no es mejor. Mientras que Bolkestein está sugiriendo que las enmiendas del Parlamento podrían ir en contra del TRIPs, la propia propuesta de la Comisión, introduciendo un requisito (fraudulento) de 5(q:contribución técnica en el paso inventivo) se desvía tanto del TRIPs como de la EPC. Es la propia Comisión Europea la que no ha conseguido concretar el Art27ff del TRIPs, y muchas de las enmiendas, tales como la 20, 50, 45 y 107, están intentando corregir esto. Tal pronto como sea suficientemente corregida como para conseguir este objetivo, la directiva merece ser aprobada.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: mcabanas ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plenkond0309 ;
# txtlang: es ;
# multlin: t ;
# End: ;

