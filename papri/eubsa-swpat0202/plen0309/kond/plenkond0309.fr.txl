<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Conditions pour l'approbation de la proposition de directive sur les brevets logiciels

#descr: Si ces conditions sont remplies, on peut être sûr que seules les %(q:machines à laver informatisées) et non le algorithmes et méthodes orgnisationnelles pourront être brevetées. Il est encore possible d'arriver à ce résultat mercreci 24/9/2003, si tous les bons amendements sont appuyés par une majorité. Cependant, même dans ce cas, un nouveau tour de mise au net sera nécessaire pour obtenir une directive claire et cohérente.

#bWi: Liste

#jtW: Pourquoi rejeter une mauvaise directive

#oio: Condition

#eAm: Amendements nécessaires

#eli: La liberté de publication est maintenue. Le sens de l'Art 10 ECHR et de l'Art 30 TRIPs concernant la liberté ou l'appropriation des textes fonctionnels est clarifié.

#anW: %(pc:Les revendications de brevets pour des programmes) ne sont pas admises.

#nne: Les standards de communication restent libres. Le sens de l'Art 30 TRIPs concernant l'interopérabilité est clarifié.

#Pni: Le traitement des données n'est pas un domaine de la technologie au sens de l'Art 27 TRIPs. Les algorithmes, le traitement des données, les méthodes organisationnelles sont explicitement exclues de la brevetabilité, sans phrases conditionnelles ambiguës (du genre %(q:ne fait pas une contribution technique)).

#frf: Si le terme %(q:technique) joue un rôle central, il est défini en termes de %(q:forces de la nature contrôlables)

#xti: Si le terme %(q:industriel) joue un rôle central, il est défini en termes de %(q:production de biens matériels).

#ait: Si les tests (y compris domaine d'intérêt / caractère technique, innovation, activité inventive, applicabilité à l'industrie) s'appliquent tous à un objet unique, l'invention (= contribution technique) et qu'il n'y a pas de refonte des différents tests en un seul (comme dans %(q:contribution technique dans l'activité inventive)) ou de mélange entre %(q:invention) et l'%(q:étendue de la revendication). Il n'y a aucune disposition indiquant ou impliquant que la %(q:contribution technique) peut ne consister qu'en des traits non techniques ou que la composante technique ne doit pas forcément être nouvelle ou encore que la nouvelle composante ne doit pas obligatoirement être technique.

#eys: Le mot %(q:invention mise en oeuvre par ordinateur) est éliminé. Ou au minimum il est redéfini de manière à ce qu'il fasse réellement référence à des %(q:machines à laver ou téléphones mobiles), comme les partisans l'affirment, et non à ce que l'Art 2 de la proposition de la CCE/JURI en dit: règles destinées au fonctionnement un matériel informatique générique (= programmes pour ordinateurs).

#tfW: Outre ces conditions minimalistes, il faut aussi insister sur le fait que la directive soit claire et sans contradictions, et qu'elle fournisse des éclaircissements à propos des conséquences de l'Art 27ff TRIPs sur les logiciels, la liberté de publication, l'interopérabilité, ... en Europe. Même si la directive est, cette fois-ci, favorablement amendée par le Parlement, ces conditions seront difficilement remplies. En conséquence, dans le doute, le rejet est probablement le choix le plus approprié.

#eti: Il est mieux de n'avoir pas de directive du tout plutôt qu'avoir une mauvaise directive. L'OEB pourrait en effet continuer d'accorder des brevets pour des logiciels et des méthodes organisationnelles, en dépit de la lettre et l'esprit de la Convention européenne sur les brevets, mais ces brevets seront beaucoup plus difficilement exécutoires. Les tribunaux en brevets, y compris l'OEB, ont déjà réagi à la critique et on peut s'attendre à ce qu'ils modifient leurs pratiques pour un mieux, sauf si l'UE impose une mauvaise directive.

#whW: Les menaces indiquant que la Commission et le Conseil de l'Europe devront rejeter la directive si les amendements PSE/Kauppi/Greens/UEN/EDD sont retenus, telles que proférées par Frits Bolkestein et auparavant par Arlene McCarthy, font à nouveau planer le doute  quant à la volonté réelle des promoteurs de la directive de faire suivre d'effet leurs déclarations d'intention visant à plus de clarté et d'%(q:harmonisation). Le rapport préliminaire du JURI n'est guère mieux. Alors que Bolkestein suggère que les amendements du Parlement pourraient contrecarrer TRIPs, la proposition de la CCE elle-même contrecarre de fait à la fois TRIPs et EPC en introduisant une %(q:contribution technique dans l'activité inventive). C'est la Commission européenne elle-même qui n'a pas réussi à concrétiser l'Art 27ff TRIPs, et nombre des amendements, tels que 20, 50, 45 et 107, s'attellent à corriger cela. Dès qu'elle sera amendée suffisamment pour atteindre ce but, la directive méritera d'être approuvée.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: gharfang ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plenkond0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

