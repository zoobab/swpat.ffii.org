<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Plenary Debate 03/09/23

#descr: Rough Transcript of the Speeches given in the Plenary Debate of 2003/09/23.

#iai: Transcription of EU Parliament debate on software patentability, 23.9.2003

#ttr: A transcription of the real-time simultaneous translation, so it may not reflect the style and content of original language speeches.

#fPa: Taken from the Real Player stream at %(URL).

#uWe: Click on Tuesday, go to 9am-10am, and click on  09h43'58'' on the left)

#nWc: Careful transcription only up to 10.06 (end of Mrs Fless), very scrappy after that

#tWn: Bolkestein's Responses to particular individuals

#aAW: May I start by thanking Mrs Arlene McCarthy very much indeed.

#Woe: She is the rapporteur on this complex and important file, and on behalf of the Commission I would like to thank her in particular for the excellent work on this dossier.

#Wtf: I would also like to thank in a similar way the rapporteurs of the industry committee and the culture committee who have also made a major contribution to the work of the parliament on this important issue.

#abv: Computers are becoming ever more ubiquitous parts of our everyday lives.

#hje: And quite apart from the box which sits in almost all of our offices, and quite a few of our bedrooms, microchips are commonplace in all sorts of everyday gagets such as electric razors, cars, and microwave ovens.

#WgW: The question of how to define the patentability of computer-implemented inventions is thus becoming steadily more important especially as such inventions are estimated to cover 15% of new patent applications.

#cet: All the more so as the acceptance of the potential patentability of computer-implemented inventions has already developed in the current practice of the European Patent Office.

#dor: The proposed directive does not aim to abolish this practice nor to extend it.

#hte: Neither to abolish it, nor to extend it to cover the patenting of pure computer programs, as many detractors have complained.

#nvm: Not the intention of the directive to cover the patenting of pure computer programs.

#dnU: Indeed many have claimed, and claimed equally falsely, that the directive is introducing the notion of patentability of software inventions into the European Union's patent practice for first time.

#tta: That is not the case.

#afW: And I am therefore all the more pleased that rapporteur has been able to steer past these misconceptions, and has produced a highly constructive report, which seeks to build on the objective of the commission in proposing its original text: namely to clarify the scope of patentability of inventions which incorporate software, and to harmonise across the European Union on the basis of existing practice.

#siW: The proposal seeks to harmonise and to clarify; but nothing will become patentable which is not already patentable now.

#ire: And it is in this spirit that the commission would welcome the amendments proposed in the report by Mrs McCarthy as a further step to clarifying that objective.

#asc: Now opponents of the directive have mounted very vocal, at times even personal campaign, based on half truths and misconceptions, and which have played on the legitimate concerns over competitiveness especially for smaller firms.

#hWo: But the fact is that the proposal does not introduce software patents, and won't have all the terrible effects that the doom-mongers would have you believe.

#Wle: It is a prudent, it is a cautious measure which will clarify and if anything tighten the rules which already exist.

#enW: Now I am aware that the large number of amendments to the McCarthy report have been tabled.

#rch: Many of those try to re-introduce ideas and themes which were already considered and rejected by the committee during the preparation of the report.

#etc: There are some interesting points, but in the main, I am afraid that the majority of those amendments will be unacceptable to the Commission.

#teh: And I must confess, to being very concerned about this situation.

#nsl: Many of these amendments are fundamenal, and there is the very real possibility of the failure of the proposal if the parliament chooses to accept them.

#Irs: If that were to happen, there would I fear be two consequences, neither of which I suspect has been forseen by some mebers of parliament, and neither of which I can only assume would advance the objectives which seem to lie behind a number of amendments.

#eul: Firstly, in the complete absence of harmonisation at the level of the community, the European and various national patent offices would be free to continue their current practice of issuing patents for software-implemented inventions which may blur or even cross the line in undermining the exclusion from patentability of software as such under article 52 of the European Patent Convention.

#una: And the result would be not only continuing legal uncertainty and divergence fpr inventors; but also erode the position which I think almost everyone in this room and above all the Commission itself wants -- namely to maintain the exclusion of pure software from patentability.

#adW: That we do not want.

#toe: That the proposal rejects.

#bsW: And secondly, in the absence of harmonisation at Community level, member states would be very likely to pursue harmonisation at the European level instead.

#ytt: And may I explain what I mean by that remark.

#sWw: Unlike many fields, patents are unusual in that as a result of the existence of the European Patent Convention, and the creation of the European Patent Office, there already exists a supranational patent system, which covers the whole of the European Union, and indeed beyond, and which can act independently of the Community's legislative process.

#oit: Now if we fail in our efforts to achieve a harmonisation of patent law relating to computer-implemented inventions in the European Union, we may well be confronted with a renegotiation of the European Patent Convention.

#jrr: And if I may be blunt, President, the process of renegotiation of the European Patent Convention would not require any contribution from this parliament.

#aac: So the situation is clear: there is a single objective but a choice of means.

#nna: Either we proceed using the community method, or we take a back seat and watch while member states go via the route of an intergovernmental treaty.

#wri: And I think it is clear which route would give European citizens a greater say through this parliament in patent legislation in an area which is so crucial to our economy.

#nWe: Thank you, President.

#tam: I believe that at the end of the debate I may have the floor again for some further remarks.

#nWe2: Thank you, President.

#hko: Thank you.

#eev: The Commission proposal before the Parliament on the patentability of computer-implemented inventions is not as some opponents of the directive have suggested a new phenomenon.

#uef: Neither does it argue for the patenting of software, nor for the extending of the patentability and scope of protection in this area.

#0nW: The fact is that some 30000 patents for computer-implemented inventions have been  already handed out by the European Patent Office, and indeed national patent offices.

#Wbo: Computer-implemented inventions are present, as the Commissioner in everyday household appliances, from safety devices such as airbags in cars to mobile phones, washing machines, the list is endless, and the relevance goes well beyond the traditional computer industries to the heart of our manufacturing sectors.

#ctu: Let us be clear: without this directive, patents will continue to be filed.

#0ji: Out of over 110,000 applications received at the EPO in 2001, 16000 dealt with inventions in computer-implemented technologies.

#gWe: I have to say that in the US and increasingly in Japan, patents have unfortunately been granted for what is essentially pure software; and an EU directive by setting limits in this area could stop the drift towards a US liberal style of patenting software as such and indeed pure business methods.

#oak: One often quoted example of such a bad patent is the Amazon one-click shopping method.

#ino: Clearly this technology is not new, nor is it unique, and I believe that the patenting of software business methods such as this is not good for innovation and competition.

#uWl: But it is unfortunate that the EPO has granted a patent on this and therefore that is an example of bad EPO practice.

#lsW: Without a doubt, computer-implemented inventions -- genuine, not trivial inventions -- which in some cases are the result of ten to fifteen years of R&D investment are important for the European economy.

#ohn: At a time when many of our traditional industries are migrating to China and the far East, we do need to rely on our innovation and inventiveness to earn our living.

#erW: I have seen letters from small companies across Europe who are supportive of this directive.

#nah: A small Belgian company employing some 12 people has written to me saying that they need patents to get a return on their investment, and to grow their business and to be sure that their technology is respected by others.

#WWn: For many European companies, it is important to grow their business through revenue secured by patents and licensing of them.

#ato: This is also the case for a 10 person company in the south-west of England, located in an economic blackspot with high unemployment.

#cte: This company granted a licence to a US multinational for its computer-implemented voice-recognition patents, which shows that in the world of global patents, there can be David beating Goliath examples.

#Wai: Without patent protection the small company could have found itself in a perverse situation where its R&D efforts would have been free for the multinational company, who with its team of patent lawyers would have gained a patent in this area and of course the European company would not have reaped the benefits, or indeed  been in infringement of a patent owned by a multinational.

#oWh: But in my report, Commissioner, I want to stress that I have tabled amendments to underline my opposition to the patenting of software as such, computer methods, algorithms, mathematical models -- this is in a new article 4 -- which we want to specifically exclude from patentability.

#coo: I have attempted to produce balanced legislation, which takes account of the needs of all sectors of the industry, not just of one vocal sector who are against this directive.

#Wea: I would be the first to agree that we need to have more debate on refinement to the proposed legislation, and I do therefore ask the Commissioner to now look at the crux of this debate: how do we get good patent law, which rewards our most innovative companies for their investment in R&D, while at the same time not allowing companies to use patents to abuse a dominant position, lock up technology and stifle innovation and competition.

#oee: I ask you therefore, Commissioner, to look at these amendments tabled to plenary today, they are very clear in suggesting that we need to limit patentability to genuine inventions in article 2 and article 4.

#ees: We want to ensure interoperability, to enable computer programmers to reverse-engineer, decompile programs for experimental purposes without risk of infringement or legal action.

#toW: It is vital that we address the perception that patents are not only for big business.

#ooW: Small companies can and do gain from patent protection.

#afc: But to have a fighting chance in such a competitive business, they need access to affordable patents, and assistance with legal fees to protect their patents and enforce them.

#Wpo: Other amendments seek to ensure that the granting of a patent doesn't allow monopoly or abuse of a dominant position.

#ole: We also want to protect open source community, who are a vital contribution to competitiveness in the area of software development.

#avo: So I would ask the Commissioner to give serious consideration to the amendments.

#teW: Please recognise them in the spirit in which they have been tabled, as a genuine attempt to ensure Europe develops good patent law in the field of computer-implemented inventions.

#aet: This does not mean rubber stamping bad practice in Europe, but developing good practice for the future, good law, good patent examiners, quick death for applications which are clearly not inventions.

#esi: And I think we also want to make sure that there is no dominance for multinationals in our EU markets, and that is why at the end of the day we do need patents, because we need to protect our own companies, and our own investments to be an effective global player in this fast changing and rapid developing software development market.

#hko2: Thank you.

#nMi: Thank you Mr President.

#mra: Commissioner, colleagues:

#inh: First of all I would like to say that in the 9 years that I have been working in the parliament I have never been lobbied in such an improper and agressive way.

#WWa: There seems to be a lot at stake here.

#Wvo: People do seem to be very worried about this.

#jae: Colleagues, the aim of this proposal is to harmonise European legislation.

#ney: At the moment there is legal uncertainty about what is and what is not patentable in software, and the reason for that is that the EPO rules are implemented in different ways in member states, and are not checked.

#gii: And this legal uncertainty has negative consequences for the internal market.

#WWl: So the industry committee welcomes the aim of the proposal, which is to recognise the patentability of computer-implemented inventions, and to allow for more transparent regulation.

#lWW: But it is an illusion, colleagues, to think that no patents are granted for software in Europe at the moment.

#0iu: The EPO has granted over 30,000 such patents, and the proposed directive will not make it possible to patent computer programs as such, and will actually restrict current EPO practice.

#iey: Nothing will be made patentable that was not already patentable.

#sna: The industry committee thinks that the directive should be limited to unequivocal cases, and thinks it is very important that there should be a test of the effectiveness of this, and that the patent should be granted in relation to an invention, and not just an idea; and we don't agree with the US practice of granting patents for business methods.

#eeu: Interoperability between equipment, and establishing interoperability means that we can prevent the abuse of dominant position.

#Wec: And I would ask for support for itre's amendment on article 6a of the report, and other amendments as well, and also our proposal on the grace period which would be allowed for the inventor.

#miW: That is something which is essential if the market is to work properly.

#aus: Thank you, President

#eeW: Commissioner, this directive and this debate are all incredibly complicated -- doubly so they are complex in legal terms and complex in computer termms.

#esW: But what is at stake is very important: tens of billions of Euros, and also in philosophical terms the status of the human intellect.

#WiW: We have proceeded by copying in the past up until now.

#WaW: The products of knowledge, music, mathematics etc, have to be freely available, freely

#beW: in both senses of the word.

#taW: Copyright tries to protect the creator without imperilling this principle.

#aoe: Things are changing, patenting has made it possible for people to gain a lot more money from their inventions by preventing their use without authorisation.

#Wle2: Software is something which often builds on the creation of others in the past.

#sWs: It builds on tens, and often hundreds of previous creations.

#wuW: So human knowledge tomorrow and in the future will increasingly take on the form of

#owe: software.

#oaW: Up until now the patenting of software has been banned under the convention.

#eoc: But now there have been a multiplication of patents in the changes which have occurred.

#nlc: The situation is unclear, and it is highly dangerous for individual creators and for

#SME: SMEs.

#rns: The European Commission have tried to stop this.

#ras: We need a directive, and your proposal is a good start, as you yourself said this morning.

#inf: You have said, rightly, that we are not expanding the scope of patentability.

#asa: You have said this in recent articles.

#mai: So, Commissioner, we accept your motivation.

#phy: But a lot of people here, including my group, think that you haven't gone quite as far as your own logic demands.

#hea: Your last article ends with the tremendous sentence, that the users of existing software will continue to be able to use it, no matter what future patents might be imposed in this area.

#taa: That is what we want.

#WdW: That is tremendous, well done.

#ntW: But the technical provisions of your directive don't guarantee that this will be the case.

#mWf: We have drawn up amendments which make clear the distinction between an invention and a pure product of the human imagination.

#Wni: This isn't accepted by everybody, and that is why we are having this debate.

#gie: But this way of doing it is the only way we can stop infinite extension of companies taking over the creations of the intellects of human beings.

#yiW: We think that you will thank us for having clarified your text at the end of the day, Commissioner.

#hko3: Thank you.

#nWe3: Thank you, President.

#Mda: Mrs McCarthy, dear colleagues:

#leo: First of all thank you for the excellent piece of work done by our rapporteur.

#mmu: You had two major difficulties to overcome, which not many people would have tackled.

#les: It is a very complex field, between legal questions on the one hand, and complicated technical issues on the other hand.

#Wrt: And you had to deal also with very aggressive, but also very irrational lobbying.

#ots: But you also had to be in constant dialogue with them, even though it must have been very difficult, as it was for many of us.

#nWy: No, ladies and gentlemen, we don't want to have any general patentability of any software.

#nnW: We don't want the market powers and monopolies of software giants to be increased.

#aoc: No, we don't wan't small and medium sized software companies to be risked; and we don't want the successful open source movement and linux technology to be damaged in any way.

#aWa: What do we want ?

#iat: We want to have a sensible limitation between computer-guided technical inventions on the one hand, and they must be patentable, and pure software for data processing on the other hand, which can't be patented.

#Wma: It is important.

#eWW: In the past patents have been granted too easily by the EPO; and Mrs McCarthy mentioned the case of Amazon.

#adW2: But, colleagues, this damaging tendency can only be braked by legislation.

#pWt: There is no point to be against this sort of directive, because without this directive

#noc: current practice would only be continued.

#Wse: Therefore I just don't understand why anybody could be against this sort of directive, at least not in the open-source movement, because this would only mean that the current practice would be continued, which is seen so very critically.

#tWW: On the other hand it is also true that an inventor in the classical case deserves the protection in his invention, and it is not acceptable if the state in these cases looks on when other people use this idea for themselves for economic gain.

#ufv: And, I have to say, in this context that there is the question of competitiveness of the European Union which comes in, because if we go so far in our directive that just about any software element means that an invention is no longer patentable, then we will be completely sidelined in the global battle for inventiveness, and that is something that we definitely ought to be aware of.

#aWi: The proposal from the legal affairs committee brings in big improvements.

#xrW: It delimits the tendency towards the extension of patentability better than in the Commission proposal, and in new article 4 there is a waterproof protection against the patentability of software, because we are saying very clearly that pure software can't be patented.

#eWa: Business methods can't be patented.

#hse: Algorithms and data processing also can't be patented.

#aaw: Colleagues, we need a European patent law which is friendly towards invention, with clear laws and with a waterproof delimitation with pure software, and this is what we can achieve with this directive.

#hko4: Thank you.

#onc: We in Socialist group do not believe that the legal affairs committee amendments reflect the present situation on patentability of computer-implemented inventions.

#tic: We need to start from the view that this is not an attempt to protect computer programs.

#mWa: Computer programs, as the rapporteur for Culture, Mr Rocard pointed out, are governed by a community directive.

#bet: We are talking about intellectual creation, which is very different, as far as industial protection is concerned.

#the: As Mr Rocard pointed out, industrial protection deals with industial uses, and the rules there are pretty well laid down.

#Whu: There's a dangerous drift, caused by the USA, which has allowed that purely intellectual issues without any industrial use can be patented.

#itW: An attempt is being made to achieve monopoly over computer programs, and it rightly caused the anger of those who are active in the field in Europe and could find themselves and their economic interests severely damaged.

#Wtj: That is why we, in our desire to create an information society, as laid down in the Lisbon declaration, are against such a view.

#Wgp: And we in the Socialist group believe that this proposal needs to be substantially amended, having very clear limits laid down, so that the nature of the industrial patent should not be radically altered.

#Wce: We don't want to be in a situation, as they are in North America, where certain companies can practically prevent any work going on in certain computer program sectors.

#WeW: As Commissioner Bolkestein said, it is true that the EPO has tried to stand out against this dangerous drift.

#aed: But that is why we need a Community directive.

#vma: We believe that we need to maintain intellectual property.

#vli: We need a Community directive which deals only with industrial applications, not with actual computer programs, and would stop us from going down the drifting road that Japan is travelling down.

#ynt: That is why we need to do something on computer-implemented inventions.

#hko5: Thank you.

#nWe4: Thank you, President.

#nei: Commissioner, colleagues, we are speaking abou a difficult subject.

#Wed: There is a big difference between copyright and patent law.

#lsy: But it is difficult to define that difference, and that was reflected I think in the amount of lobbying we have had on this.

#evt: I would like to thank Mrs McCarthy because she has managed to make that distiction clear, and in her briefing towards some of the aggressive lobbyists here she has made that clear, because I think they have interpreted it in a different way, and I think it is a shame that they have done that.

#fWW: The aim of this, to my mind, is to clear up any legal uncertainty.

#rhe: Mr Medina Ortega gave an example of this already, as other colleagues did also.

#Mod: The European Patent Office in Munich is already using the American method, and is granting patents for computer programs as such already, so it is important that we should get the directive to put a stop to this.

#yit: And the lobbyists that want to reject this directive also are tring to achieve that.

#WWo: They want to reject the directive; but in order to ensure that the US practice can be continued, and so that thousands of different software methods can be patented.

#Wtd: And I think that that is a bad thing.

#tea: President, I have tried to set down a number of amendments.

#WaM: Many of them have been adopted in the legal affairs committee, in Mrs McCarthy's report.

#Wec2: A lot of them seek to protect SMEs.

#fii: Some of them seek to improve the definitions.

#ted: And I think now we have a very balanced report.

#sai: I would like to see some further improvements, for example the grace period which Mrs Plooij already mentioned.

#hih: I think that would be a good thing, to have that in this directive.

#Waa: But I would also like to call on the Commission to take this further, and ensure that for all intellectual property rights should benefit from this kind of grace period, because I think this is a good thing for inventors who are not very powerful because it gives them an opportunity to see whether there is a market for their product.

#dWc: I have tabled another amendment, dealing with the duration when we are talking about software which is computer-implemented inventions which are computer software which is not patentable but there should be a limited period because with your patent there are legal procedures which are connected to this, which will keep other operators out of the market.

#iit: I think that is a bad thing.

#aab: I think that we have to respect the non-discrimination ban in TRIPS, because otherwise we will be excluding one sector.

#noi: Then, interoperability.

#den: President, interoperability is necessary.

#iWW: But we need to make a clear distinction between inventions which work on their own and inventions which are destined to be used in conjunction with other inventions, and I have tabled an amendment on that.

#lea: But I would call on colleagues to support this directive so we can avoid legal uncertainty in the future.

#nWe5: Thank you, President.

#Woo: Thank you to the Commissioner, thank you to the rapporteur for the enormous piece of work.

#mee: It seems that we have the same intentions.

#rWW: I'd like to underline that the intention is to create more development and more investment in SMES in this area.

#hWs: The strange thing is that why aren't we happy about this directive, if that is the intention ?

#hss: Why isn't it the situation in the answers on this directive that the SMEs are praising this directive ?

#tgt: Why isn't it the SMEs who are demanding that we should carry out this directive ?

#ltn: Why isn't it all the innovators in the software sector demanding that we should carry on with this directive ?

#rWW2: Why are they doing the opposite to that ?

#axa: I think that that leads to some doubts as to the extent that we are actually doing what we say we're doing.

#ycu: What we're saying that we want with this directive is to ensure that we can guarantee our rights.

#Who: But the ability to guarantee rights in this area, or to protect oneself against others claiming that we are infringing their rights involves so much costs that SMEs can see that there is no way ahead for them -- cases of this type cost about 1 million Euros in the courts.

#Whk: That is not something that SMEs can fork out for.

#ytt2: We are saying that we don't want to extend the current provisions.

#wnt: I think that is wise, in particular because the European Patent Convention underlines that software can't be patented.

#tet: Let's keep to that.

#hla: We have a legal basis.

#vav: We have the European Patent Convention.

#hWa: The fact is that the European Patent Office has extended its field, gradually, bit by bit.

#hnd: And at the moment we're trying to legalise this development.

#oid: If we do that, we are going in the wrong direction.

#Wlt: We are going in the direction where it will be more general to have patents for software.

#Wrw: But not pure software.

#teW2: So the question is: how pure does the software have to be for it to be pure ?

#nWW: I haven't had a clear answer to that question.

#ooa: It's questions like this which ought to be clarified before we seal this directive.

#tse: I think we should think hard, and we ought to listen to what small and medium sized enterprises in Europe are saying in this area.

#hko6: Thank you.

#hko7: Thank you.

#Wit: Colleagues, I will try to be brief and respect the speaking time, and I will try not to repeat anything that's already been said.

#WtC: So I'd start by saying that I fully support what M. Rocard said for the Culture committee.

#WaW2: The thrust of this directive is something that I also support, as Mrs Framm already mentioned.

#atr: We want to create legal certainty.

#WWt: We agree with that.

#ore: But the commission I think -- there seems to be some sort of suggestion of threat to the opponents of this directive.

#Wps: What we are talking about here are computer-implemented inventions using software,

#WeW2: and you said that those who criticise this directive have used half truths to put forward their arguments against this.

#ono: Now, we have all tried to balance out the aggressive lobbying that we have experienced, and we have tried to use the advice of experts to produce a balanced proposal on this.

#rot: But there are some areas, some questions, which you have not yet responded to.

#moe: If there is a solution to a problem -- and that is what software is made up of, many such solutions -- if one component of that is patented as a computer-implemented invention, then it is patented, and you cannot make any further use of this.

#Wfg: As Mr Manders said, we are treading a very difficult line here between copyright and patent law.

#oas: And I think the proposal made by the legal affairs committee and the industry committee should be the basis for our decision here.

#amu: We're trying to achieve the same objectives as you, Commissioner, but I think that we've thought it through better.

#sre: Legal experts have explained that this directive may not lead to greater legal certainty.

#clW: It may close some of the loopholes but open up others.

#lol: And let's be honest, colleagues.

#kWh: We all know what the real situation is in the market.

#osh: We know that if someone wants to use a patent to abuse a dominant position, to use it against their competitors in the market, they can do that.

#Wwt: So we need to think very carefully about how we can effectively protect investment and invention.

#vtr: We have a copyright directive from 1991.

#Wod: Perhaps we should have thought about revising that and adjusting that ?

#eoh: If we had reached a decision here based on the European Patent Convention, you would have had my support.

#tel: And I do think that some of the amendments would help us to this.

#tWb: But, finally the patents which have been granted or which could be granted in the future patents are first of all the responsibility of the signatory states.

#npn: And I think it's good that Europe wants to take action in this area.

#tet2: But I think the first thing we should have done was to re-think the Patents Convention.

#nWe6: Thank you, President.

#Pta: Mr President, Colleagues:

#Won: The computer is going to be the backbone of the development of any country in the world.

#ewf: It's linked of course with hardware technology and software as well.

#tti: Any innovation at the moment is protected through patentability in hardware.

#ecl: But software can enjoy a copyright which only deals with its intellectual property.

#WWc: In order to improve this, to clarify, let's look at it in connection with music.

#Wri: Someone who makes an instrument can now use something which is controlled by various codes and inputs, and which gives rise to various results.

#onp: What would happen to music if you could suddenly start copyrighting scales, chords etc -- the whole world's symphonic pattern would alter.

#oWu: The same thing would happen to the world of computers, where we would see any tiny command suddenly being patented.

#feW: Sequences of codes, algorithms, and the whole market would become a kind of jungle.

#eed: Any software would be parcelled up and it would be almost impossible to put anything together if all of the programs in existence at the moment all suddenly started enjoying patentability.

#ude: And SMEs would find themselves pushed out of the market by the big companies.

#aWa2: You can't patent a book, or a painting.

#alt: You have to guarantee the author's rights, while circulating it to the broadest possible audience.

#wht: That would allow people to build upon the work of the past; and the same thing applies to computer software.

#lht: A market bubbling over with creativity, such as the European one, does not need more rules.

#Wwo: It does not need to bring about what would be a drawback on European creativity.

#Wrh: So we do not support the McCarthy report.

#elm: We think it provides a danger to creativity, and technological debate, which can only occur if people's minds and spirits are free.

#wht2: I have worked as an architect throughout my life.

#man: The problem we have here is the same as if we had a patent in my area.

#woW: Just imagine that we had a patent on staircases -- so that you would have to pay a royalty to design a house with a staircase.

#Wws: It would be very good in one way, we wouldn't have all these massive high-rises.

#oln: But a big company within my sector would be able to put a brake on any development.

#tew: In the design architecture I have done, of course you have to have the protection of copyright, which prevents work from being plagiarised.

#WeW3: It's the same thing with software really, and any other artistic work, which can be compared with the design of software programs, where the design is protected by copyright.

#bej: The admission of patentability of software in the EU has serious negative consequences for consumers, SMES, the whole open source movement, and all innovation in the sector.

#ewW: We should be naive to think that we would benfit innovation in the EU, if Ameircan companies can patent anything within this sector, or that it would benefit SMEs to invest in a department for patent law before anything could be developed.

#orW: Neither software or pure software ought to be able to be patented.

#aui: Thank you president.

#how: I would like to thank the rapporteur and the commission for the work they have carried out, work which was tough as well.

#ipe: The radical members of the (?)Buanino list will vote favour of amendments which will restrict leeway for software patentability, and will vote against the overall proposal if the substance of these proposal are not reflected in final version.

#aee: This is because we are against European harmonisation, some people might say.

#aeW: But in fact we agree with the commission and the rapporteur.

#rts: We agree with her description of the situation, where the EPO has de facto received thousands of software patents.

#tso: The problem is that faced with such -- I might say -- patent violation of the EPC, that which is, was and will remain essential is to clarify and to confirm the non-patentability of software. Full stop.

#eit: The distinction between software patentability as such and the patentability of software as part of a technological invention is a subtle distinction.

#rsk: So subtle, that it runs the risk of producing confusion; whereas the solution -- as far as I am concerned -- is very straightforward.

#WWW: If software is part and parcel of an invention, the invention is patentable, and should be patented.

#nuo: The invention.  But not the software.

#tae: But this can already be done.

#Wve: Laws on technological invention already permit this.

#lrh: Software is excluded from patentability for a very precise reason -- for the same reason that mathematical formulae are left out, for the same reason that theorems are left out, for the same reason that symphonies or pieces of music are left out.

#gte: They belong to the realm of ideas, the organisation and processing of ideas.

#lWd: We are aware that the level of functionality which software attains can be brought about by different types of programs, with different computer languages.

#ooW2: That is why software patenting is very dangerous, because a patent would last for twenty years.

#cft: That's basically geological time, as far as software development is concerned.

#tWp: We, at the moment, have a situation where independent programmers can use functionalities patented back in 1983.

#arW: That's really the prehistoric period so far as software is concerned.

#ahg: That's the danger.

#bdr: I don't think that it would be or should be necessary for us to move forward in this complicated area of the distinction between software qua software, and software qua part of technological inventions.

#olr: We should simply leave technological inventions as being patentable, and make sure that the rule that leaves out software from patentability is followed; which still allows protection through copyright, don't forget.

#aWf: So we support those amendments on interoperability, and on forces of nature as being necessary for considering of software as being an invention.

#sWr: Also I think we should not adopt this proposal because member states will simply act on their own bats if we don't.

#uha: It's our job to do the best job that we can.

#edt: Don't forget, hundreds of thousands have got in touch with us on this.

#ood: That is a democratic contribution, and not something that should be regarded as an annoyance.

#auW: Thank you very much.

#nWe7: Thank you, President.

#sve: In tackling many issues in the lifetime of this parliament, I have to say that today's issue is the one of the most complicated of all.

#Wha: And I'd like to pay tribute to Mrs McCarthy for the tremendous work that she has put in.

#nns: I don't have any real answers.

#WWt2: I can understand why the commission has acted in the way it has, Mr. Bolkestein has explained it very clearly, why it felt the need to intervene.

#rin: So all I am going to do is to make some general observations, starting with a comment, which is that we are dealing with an issue where the identification of an object in legal terms -- the translation of computer terms into legal terms -- is something which already could make things exceptionally complicated.

#Wgi: Secondly it is absolutely vital that we run up against a very complicated international legal framework.

#pch: If we look at what is happening if we look not just in Europe, I'm thinking in particular of the United States, patenting systems are already showing how very limited they are.

#cea: Particularly limited in practical terms.

#hmh: So I believe that the directive, although it is open to improvement, has tackled a genuine problem, which the Union has to face up to.

#Sem: We need to move away from the US posiition, and we should I think start advocating a new approach to our economic partners, looking at the issue of software in a way different from the way it has been tackled up until now.

#saW: The characteristics on which patents are based are already identified clearly in the TRIPS agreement.

#sad: So we should stick to the concept that software patents will only be granted on a solid basis.

#atu: We have seen a huge number of software patent requests in the US, and I assume in Europe too.

#wnv: This shows us that this is not genuine inventive activity.

#eob: You can't allow any kind of application to be patented.

#ali: Further, if there is an explosion of patents, it would make it virtually impossible to check whether we are really dealing with something new or not.

#oea: There are some aspects which from a legal standpoint could probably be improved.

#Wet: We still have certain differences with directive 91/250.

#llt: There is also the question of Article 52 and the European Patent Convention.

#Wih: But one thing is certain. We are going to have to come back to this, because things change so rapidly in this area, that the Union is probably going to have to revise its position, whatever it is, in the next few years anyway.

#hko8: Thank you.

#aui2: Thank you president.

#aWh: Colleagues, It is rare that our legislative job at an early stage has attracted such great public attention as has Mrs McCarthy's report.

#hhe: That hasn't made her job any easier.

#Wnt: But there has been a flood of information and arguments coming in.

#sud: Many things for the rapporteur and us were very important and informative.

#tot: But these mountains of papers -- many of these mountains of papers were ready for the wastepaper basket.

#ekr: Our work has led us to the compromise and I think it ought to be adopted, and my group agrees with this.

#oWe: It tries to get us out of the trap of articles 2 and 4, where a new definition of %(q:technical applications) threatens to open up the floodgates to patentability.

#eWe: Dear colleagues, now this hole has to some extent been stopped.

#tMd: I think that in particular SMEs would have wanted to see more.

#nng: It is not clear enough to me that software patents should not just be limited, but ought not to be granted at all in future.

#san: [NB Problems with the Real Player stream jumping back to Wuermelung at this point]

#aWW: Software patents make life difficult for SMEs, and we want to grant particular protection and we want to nurture these companies.

#han: Putting it another way, if you patent software, you are playing big capitalism, but it's not very intelligent.

#WWn2: Therefore we want to keep well away from software and patenting.

#dgs: President, colleagues, Commissioner.

#olh: I share the view of Mr Rocard and Mr Cappato and other colleagues, who said that software as such should not be patentable.

#aej: Article 52 of the European Patents Convention excludes software as such from patentability, and says that ideas should not be patentable, but only technical inventions.

#hnw: And that is the crux of the problem here, and also the probelmatic point with the directive.

#ait: The European Patents Office however has reached the conclusion that all computer programs run on an apparatus are by definition technical, and that has led to the patent office granting over 30,000 patents even though some of them would not be dependable legally.

#tsa: Mr Rocard has said that you have to have an invention which is based on the use of natural forces and which is a product of the human mind.

#oWa: I think that we should support some of the amendments that were tabled to the industry committee, and to the culture committe because I think they tighten up the definition and mean that pure software would be able to be excluded.

#mdW: If these amendments are not included I will vote against the directive.

#eEw: Colleagues, there is a huge amount of concern among SMEs and the open source movement about what is happening here.

#fae: Dissemination of human knowledge is a great wealth and it is important that we should retain that in Europe.

#htW: It would also mean that we can be more competitive than the US; and the directive should exclude at all costs the US practice of business methods.

#agw: And I hope that the colleagues will vote along those lines and we will get a workable directive.

#Wne: Mr. President, Colleagues,

#gtg: This morning's debate is coming after a stormy period where the users, inventors and computer innovators have defended tooth and nail their rights to difference, their right to freedom, their right to creativity.

#aaW: At the heart of the debate lied patentability, with its plusses when the patent rightly protects the inventor, and its major drawbacks which are linked to the creation of often conservative monopolies which hamper young creative minds.

#rte: I have and continue to support the movement which goes in the same direction as all of the alternative movements struggling for a different type of society, less rigid and less commercially minded.

#fWp: Since the beginning of this debate, both sides have put forward points with draft amendments which have considerably improved the initial proposal.

#Wms: Recognising that to be patentable and invention which is implemented by computer has to be part of an industrial application is going in the right direction.

#trt: Saying that patents cannot be authorised for mere computer programs, that was vitally important too.

#Wns: But we can't ignore the subtle elements and the still existing vagueness at this stage.

#rWr: And so although there is a risk that our amendments might not be carried, or not picked up, we have to realise that these amendments don't in themselves sort out all of the problems that exist.

#rnp: So I am going to be very careful when the votes comes round, and I am going to be ready to vote against the report if the improvements are only superficial

#Weo: and are simply hiding an attempt by big companies to hold up the creative minds of young Europeans.

#rbi: And wasn't Mr Bolkestein trying a little blackmail this morning with his final sentence ?

#Wtc: But he won't convince me.

#kWs: Thank you very much, President.

#nen: Many of the amendments, and certainly all those to which my group has given its name, are clearly and directly aimed at trying to bolt the door that will stop a leakage out from this directive into the patenting of computer software itself.

#aoi: And that seems to us to be absolutely important.

#cWW: It is quite clear that a body of law which protects computer software by copyright and leaves patents for other purposes is a body of law which works well from the point of view of the software industry, from the point of view of the creative minds which develop computer software.

#trM: I don't think this is a point which Arlene McCarthy disagrees with, nor I think Mr Bolkestein either.

#ttr2: Nobody wants to have a leakage so there is patenting of pure software.

#mle: There is evidently some risk that genuine inventions, where they do involve a software element, wouldn't be adequately protected without a new directive.

#oet: So be it.

#ssI: But I think that then the burden of proof is with the Commission, to show us which of the amendments which the parliament is minded to put forward which amendments would over-protect against the risk that we see, and under-protect against the danger which the Commissioner is anxious to avoid, namely the danger of real inventions, if I may call them that, not being adequately patentable within the European Union.

#tsW: So the strategy of those of us who are amending this directive is, as I say, to prevent any leakage over into software patents as such.

#knh: That we think would be a real disaster, and we have been lobbied very heavily about this.

#Wne2: But we have been lobbied heavily because we have very many able, hard working constituents, who see the threat to their livelihood of the leakage which I have mentioned.

#chh: So let us be absolutely sure that what we send back from this debate and from tomorrow's vote, and Arlene McCarthy has worked very hard on this, really does bolt the door, and lock it with a key, and protect what Mr Bolkestein wants to protect, withput creating the danger which so many of us have been taught to apprehend by our constituents.

#sCo: President, Commissioner:

#aoi2: Our point of departure is that we think patents are not the right way to protect computer-implemented inventions.

#tuu: Copyright has provided adequate protection up until now.

#alW: And we think that allowing patents in this area will probably have a negative impact on invention and on SMEs.

#ogt: But unfortunately this discussion is coming too late in the day, because the European Patent Office has granting such patents for years already, and these have been maintained at the highest levels in the member states.

#ero: So really this is emergency action that we are taking here, in order to put certain restrictions on patenting of computer inventions in Europe.

#aWe: But I wonder is there any point, at this point, because the EPO has already granted over 30,000 patents which go further than the proposals in this directive.

#yWl: So I really think this directive is coming a little bit too late in the day.

#the2: It is true that the directive will mean greater harmonisation and clarity in the legislation of the EU.

#lfa: But nonetheless SMEs are going to suffer, and are not going be able to compete.

#Wce2: That is an inherent characteristic of a patents system.

#dmv: We are worried about the position that the commissioner and the rapporteur have taken on this.

#kir: They have taken a very lax approach to this, and I think it a shame for SMEs in the EU.

#sWg: President, colleagues:

#agm: What are we talking about this morning ?

#gje: Protecting intellectual property, resulting from computer innovation.

#tsa2: There are two different legal ideas here -- patentability and copyright.

#tvr: And I think that most of us, Mr. Commissioner, most of those who have spoken, would think that the normal way of protecting would be copyright.

#sto: Just as a journalist would allow his text to be copyright, but couldn't allow grammar and syntax to be copyrighted - or patented.

#hWt: And the same thing goes for software too.

#thl: The aggressive remarks, threatening I might even say, Mr. Commissioner, about the criticisms which your proposal has given rise to, you have said that we are not really thinking that software is patentable.

#oWW: But you shouldn't take us for fools.

#yiW2: The least one can say is that your directive is ambiguous, because the definition that you yourself give of computer-implemtented inventions -- I am taking about Article 2a -- could apply perfectly well to software.

#yme: Obviously article 4 of your directive does give the impression that you are limiting patentability to those inventions which are susceptible to industrial application.

#upy: But you don't define precisely what you mean.

#thW: We all know that American multinationals have had very trivial uses of software granted.

#fib: The use of a click to close a window on a screen has been patented.

#rWr2: The idea of warning a user by a series of notes that he or she has received an email or underlining those words which are to be corrected in word-processing in a certain colour.

#hWo2: And that is again a part of software.

#Wig: Internet trading:  young creative people have even started using the 35 hour week imposed by a previous government in my country.

#inp: The EPO is financed by the number of patents it receives, so are they partly behind it ?

#esm: A strategic choice needs to be made, Commissioner.

#hai: Either we use the system as used by American multinationals -- and it seems you are moving in that direction;

#efW: Or we should defend the unique characteristics of European Law and refuse any kind of illegal patent.

#aec: You have not gone that way, and I think we have to say that your directive is not in line with the interests of the creative community in Europe.

#hko9: Thank you.

#nMi2: Thank you, Mr. President.

#fjn: In all my time on the legal affairs cttee, this is the first time I can recall that one of our major directives has achieved such prominence in the parliamentary agenda, and I hope that trend will continue.

#rIW: In fact it was called so early that I wasn't here in time, I think for the first time, and I must apologise to both the commissioner and the rapporteur for not being here to hear their speeches.

#ljo: Now, colleagues, I have listened with interest to what people have to say, and I just want I think at this stage in the debate to reflect on what this is really all about.

#eWe2: We have an objective: to create the most dynamic and competitive knowledge-driven economy in the world.

#tdt: Now patents are an indispensible part of that.

#meg: I want to remind all of you here, and everybody else who is listening this debate that

#eWn: there are hundreds of thousands, maybe even millions of people across the European Union, employed working on inventions that have been protected by patents, and that patentability stimulated investment to develop those inventions into products that the world wants to buy.

#nhn: What we are talking about here, colleagues, is a regime that is legitimately going to encourage invention in all fields.

#leo2: And I think that part of the problem we have had in dealing with this is that a lot of the issues that have been raised with us have come from one particular direction of creativity, to do with writing elements of computer programs.

#pWo: But actually patents are about protecting a genuine invention, a new way of doing something.

#hsr: Something which is, as this directive says, susceptible of industrial application.

#dga: And I want to remind colleagues that patents when you apply and get granted a patent it doesn't have to have all the details with them.

#lni: And in today's world almost every technical and industrial contribution requires some form of computer-aided activity of some kind.

#llo: Now why should we deny protection to people who are working on inventions in that field ?

#Wnn: And I think the Commission has made a persuasive case, supported by a lot of research, that we need a consistent framework for dealing with this kind of invention, so that people know that they can get patents for that type of invention, and they will know and be discouraged from applying for trivial business process patents which should not under any circumstances be patented.

#kWa: Now that we know is difficult to do, that is why we have argued a bit about the wording.

#AWW: That is why Arlen McCarthy has done such a good job as rapporteur on this directive.

#tWv: Because she has consistently seen importance of this in the knowledge-driven economy, has directed us in that direction, to look at ways of improving this and making it work better.

#aWh2: She has not allowed herself to be diverted by all the noise around, she has gone for that.

#ljm: And that is why I hope, colleagues, that you will support this directive, you will support the thrust of the legal affairs amendments, there will be some others, but I hope above all that you will entirely resist some of the complicating and abstruse concepts that have come from other people that will make an inventor's life far harder.

#naW: It is invention and creativity we are here to support, and nothing else.

#nWe8: Thank you, President.

#ons: This is a political discussion, and as politicians the first thing we should ask ourselves, and the commisssion should ask themselves too, is: %(q:why has this caused such a fuss, this proposal ?)

#yoi: Now it is not correct to say that anyone against this has been got at by the lobbyists, because those people who got in touch with me, some of them were university computer professors, experts in issues of intellectual property and representatives of SMEs.

#ata: And rather than aggression, I saw concern: concern about the danger in which they saw the situation in Europe moving.

#hcW: Those who are in favour of this are saying that the only thing it is trying to do is unify practice in national patent offices, in an area where there have been contradictory rulings.  That is true.

#sto2: But what is not true, or at least not certain, Mr Commissioner, is that, if we have an intellectual doubt, what is not certain is that this directive will sort out all of the problems that it claims that it will sort out.

#nsa: The EPO is breaking a tradition of European law, and it is adopting a practice very similar to that of the United States.

#Wts: Everybody knows that in European law for something to be patentable, it has to have some industrial application or character; and the method doesn't have to be industrial, but the result of the product has to be industrial too.

#Wgu: Whereas in American law it is enough for it to have a useful application.

#esr: That has led to certain innovations and certain software program patents.

#tei: So let's look carefully at this point.

#fWc: The legal affairs committee and commission proposal have said that they can sort this out.

#Wmd: But if it is to be sorted out, it is through the amendments tabled in the industry and culture committees.

#ndp: So if those amendments are not carried, those from Culture and Industry, it would be very difficult for my group to support this directive.

#hko10: Thank you.

#lWp: Will we be able to use our computers in the future without having to pay patent royalties ?

#iee: That is ehat we are all concerned about.

#oeo: What we don't want to see is a US-type situation here, which would mean that patents would be granted for a simple computer language or software.

#aes: We all agree that we have to have some restrictions.

#gof: And we're lagging behind developments, you just have to look at what the European Patents Office is already doing.

#Wdy: So we can't afford to wait any longer.

#htn: We need to have a clear definition between what is patentable and what is not.

#gca: I think that the right balance has been struck in the industry committee's amendments, and in Mrs Plooij's and Mr Manders' amendments.

#rss: All forms of software should be excluded explicitly from patents, and only where there is a real invention should a patent be granted.

#utW: It must be a thought out technical proceess, with industrial applications, and not just simply an idea or a language.

#Wes: The technology which is used for the new technical process should be protected, regardless of whether a computer is necessary to use it or not.

#nWe9: Thank you, President.

#mra2: Commissioner, colleagues:

#WWe: There are a certain number lots of things I don't understand here.

#etl: I'm not sure whether I've followed what gave rise to this proposal or directive ?

#uer: Is it just the large numer of requests for patents ?

#eln: That seems a rather limited starting point.

#cyW: Nor can I understand why the commission, normally so keen to oppose over-concentration in industry, is now here proposing -- as a certain number of experts have said -- support for industrial concentration.

#tWe: That seems to be contradictory to the goals the Commission stated that it has.

#row: We are also forgetting that we have a directive on patentability of human beings, which is disastrous.

#nhp: No-one knows how to apply it.

#ecr: It's new; but no-one can really understand it.

#alW2: Can you patent the cells in a leaf but not the leaf or the tree ?

#ear: The same thing might go for patents here -- the legal uncertainty which eists on the patentability of human beings is now being repeated in our directive on the patentability of programs, software, technical contributions.

#jir: It's all complicated, because it hasn't really been clearly defined in the directive -- which is what the directive really ought to be all about.

#Wla: In the light of all this uncertainty, it's difficult for me, Commissioner, to understand what is going on.

#nia: But when I listen to economic experts, Mr Rocard mentioned the question of pre-history in all of this, because things change so rapidly as far as software is concerned.

#Wto: Are we in a position to follow this accelerated rate of evolution ?

#nrf: I think it is very, very difficult.

#ijt: So the situation is very bad, and is probably getting worse.

#sae: In the pharmaceutical sphere it is easier to see who is doing what where patentability is concerned; but in the computer sphere it is a lot more complicated.

#Win: Publishers and teachers say the information society is being threatened by this kind of directive, and transparency and distribution are being threatened as well.

#Wai2: And I have no idea what is going on.

#WWW2: There is a lot of heat around this report because there is a lot at stake.

#eWa2: well meant

#eMu: threat to SMEs in Europe.

#Wei: They might lose patent rights.

#ewn: these are small businesses we should cherish and nurture.

#nvi: innovation

#WtW: innovation is what will help us retain our position in the world market.

#egi: over-regulation

#pho: copyright enough

#rlW: We Greens are often criticised -- people say we want to legislate for everything.

#sts: This shows the opposite.

#Uap: Now in the United States, people are talking about watering down patent protection.

#eau: So why are we implementing a law which is already out of date ?

#Wte: First of all I would like to congratulate the rapporteur

#cmr: The protection of inventions implemented by computers nothing new

#ano: Standards to extend patent protection

#xfe: So we are extending the protection offered by patents to computer-run programs.

#iug: Patentability of inventions by computer. So this is something very specific.

#oub: We are trying to encourage interoperability. We do not want programs which are not going to be able to communicate with each other.  Now I think this is something which will have to be revised in the directive in a few years.

#ell: Have to explain to laymen.

#Wpf: We need to ensure that people applying for patents

#vli2: Therefore very important that people describe their inventions properly.

#hat: attackedthis proposal saying that it will create red tape for smes.

#WWa2: patent protection is extended to smes patents have led to increases in research and development market better organised.

#nmd: This means that we can have more patents , more developments

#tWe2: I would also like to thank the rapporteur, because I do think that this subject is a very complicated one and a very sensitive one

#nad: Software plays a fundamental role in development. Software is also a highly specialised engineering sector. More than 10 million software designers exist across the world.

#elk: Software designers who work in small companies and who work independently

#iWW2: Europe is at the cutting edge of computer technology.

#frW: 100s of thousabnds here- only 13% in the sates.

#eao: So we don't want a monopoly

#gan: What we are trying to do here is to ensure that knowldge and innovation are in fact free, following the spirit of Lisbon.

#wbo: knowledge based society

#afn: want to avoid monoply of software production and marketing.

#itW2: Need improvements to commission proposal,

#aWi2: many things

#uue: product not just the method.

#ssm: And that is why commissioner Bolkestein worried that amendments proposed

#WeW4: Worried because if they are not approved, I don;t see that we can support the directive being discussed today.

#WWd: Important not just for software designers in Europe but all people concerned about knowledge.

#gni: human beings can be the possession of large companies, like Microsoft.

#vWa: It's obvious that we must have a free software supply.

#tpu: must develop this furher,

#cgW: Therefore scientists and software designers are very much against this directive.

#WWW3: I hope plenary will send out a very clear signal by rejecting this amendment as my group intends to do.

#iWi: A directive which aims to

#rth: in the interest of the ineternal market in order to avoid so we should welcome it.

#oWe2: don't want to make life more difficult for smes either. We need to create legal certainty.

#eta: need to make

#nac: some concerns lie in misinterpretations. and some views are because of the American realities, and not the contents of this directive.

#ees2: Some arguments I share,

#Orn: EPO infringing,

#niW: technical contribution too vague.

#pem: That is why I support some of the amendmnets which have been tabled compromises between Wuermeling and rapporteur.

#aWe2: I also Echerer

#Wou: I also Kauppi

#0ee: 108 defines

#_112: 112

#_114: 114

#dsd: 117 definitions of what do not

#loc: also con

#_116: 116

#wem: mr wurmeling's compromises.

#WhW: getting rid of the adjudication by the board of appeal of the patent offices.

#teb: And it says that business methods are not patentable, so I think everybody should be a ble to live with that.

#idl: Growing use and serious lack of

#afW2: privatisation of the human being.

#WlW: cannot have privatisation of knowledge. We must ensure there is a clear demarcation

#mee2: common heritage.

#ieo: There are certain interests which have to be protected -- scientific interests, general interests, and not just economic interests.

#lta: protect legitimate interests, but not sure patentability

#rip: compromises socialist acceptable.

#rol: First of all I

#onr: Now do we really need this directive ?

#eWg: Like other colleagues

#xce: take expressions of concern very seriously.

#Wei2: Nobody wants to hinder innovation in Europe

#eht: Tomorrow we will be taking the right decision if we adopt this directive.

#oia: harmonising exisiting practioces

#orr: imposing stricter criteria

#fia: First of all the directive will mean that we won;t

#Wii: won't get an american-style situation

#otW: pure software will not be patentable in europe.

#dmW: The directive makes this clear

#nWe10: a technical innovation of some sort, and I am glad that Mr Bolkestein has made that clear today.

#yta: if anybody has anything to say against

#Wae: some very small inventions or innovations .  trivial software should not be given patent protection either.

#oWp: So I hope

#Wny: we want to bring forward innovation in the European Unionby simplification

#isr: this directive represents a retrograde step

#ipb: maximises patentability

#atc: we say no to this directive.

#kWt: we think informatics is a cutting edge technology

#eae: intellectual property

#egv: independent researchers, and a patent isn't good for them, it's too expensive, too cumbersome

#mws: economic rivalry with united states.

#min: big companies to retain their dominant position

#hak: if it were up to the EPP there would be no software patents, and if it were up to you commisioner I think the same would be true.

#gnW: In our group there are many who support the commission

#Wpf2: hope you can give your support to a number of amendments

#cee: more clearly delineate the whole thing

#iem: stubborn view prevailing which states that this is bad for small businesses

#otg: don't agree

#sat: smaller businesses don't have a particular problem with this directive

#eWo: small businesses have a more general problem with the system of patentability

#hth: when they have patent protection themselves

#soo: and of course also when they are confronted with patent claims from third parties

#eWn2: a more comfortable seat in patent land.

#iep: politically I think it is important enough to make the suggestion here and now very much appreciate an answer now

#rio: If we seek to have open source work in favour of small firms, but then put obstacles in their way by extending patentability, then we are destroying the whole basis of the information society.

#llv: examples such as linux help everybody

#ohW: narrowing of the digital divide.

#orn: copyright of course is already there to protect software and their codes

#enW2: programmer doesn't always know he is breaching patent law.

#Wni2: used to prevent competition.

#pei: We support small enterprises in this sector.

#ops: not present

#lMr: 10.25  Mr Darossa

#etf: This is one of those areas where those of us who are neither legal expert or technically expert address debates like this with our fingers crossed behind our back.

#oni: perhaps the commission is right to some extent, perhaps the parliament is right to some extent.

#gio: doing nothing is not an option.

#oot: hope the commissioner can accept some of the many amendments from arlene.

#eed2: Arlene who has done such tremendous work

#tuu2: important open source is encouraged,

#tWt: not right that we should allow the current legal uncertainty to continue

#aWo: Since there are many amendments, I would not propose to go thorufh them all indicating the commission's response one by one.

#WWu: Will provide a list by number.

#rnW: On this understanding I would like to confine myself to remarks of a general nature.

#scy: -- Commission prepared to accept most of McCarthy amendments

#vow: We are favourably disposed to most of the amendments which are made

#tel2: Interoperability

#ooP: at first sight this looks reasonable; but on closer examination however it might empty some patents of their value; it might even make them totally worthless; and may be contrary to international obligations.  Commission could look favourably on a compromise if these two points were recognised -- and the wording used at the end of amendment 76 (PSE) might form the basis of a compromise becasuse it invokes the TRIPs agreement,

#oma: Program Claims

#aii: Commission was originally against, and this was not an oversight, but something we considered long and hard.  But after reflection, and let me not disguise that a lot of the reflection

#Wdw: due in part to the views expressed by the Parliament, it is now ready to reconsider.  A clause to allow program claims might work if re-worded carefully and cautiously.

#hme: Other Amendments

#nbb: Unfortunately I cannot be so positive about them.

#oed: they introduce a special regime for computer-related technologies

#deh: Patents should be examined in a neutral manner, the type of technology on which they require.

#iii: problem.  Patent examiners  must not have preconceptions about an invention they are considering because of the technology it uses.  TRIPS requires this.

#noo: Amendments on monitoring

#bsW2: Commission has less of a problem with these, but its resources are finite, cannot accept unreasonable demands.  Some of the proposed demands were way too broad, much wider than scope of the directive.

#sWo: Proposal does not guarantee no swpat

#oWW2: It lays down a specific hurdle: non-obvious technical contribution

#alW3: If a small part of a problem was patentable, this would make the whole problem patentable

#hls: Patent has to cover the whole solution to a problem, not a little part.  Patenting the whole solution does not monopolise the use of component parts.

#pWt2: On SMEs: if they are patent users, nothing is made patentable which is not already patentable.  If they are inventors, this will help protect them.

#nsr: Quoting Wuermeling: whoever opposes this directive must know that without it the present practice will continue.

#adh: The McCarthy report would achieve the right balance.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plendeba0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

