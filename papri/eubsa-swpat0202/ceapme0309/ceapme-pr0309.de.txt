                                                                                   Avenue de la Renaissance 1
                                                                                                     B-1000 Br�ssel
                                                                                            Tel   0032/2/739 63
                                                                                            59
                                                                                            Fax 0032/2/736 05 71
                                                                                                  ceapme@skynet.be
                                                                                                   www.ceapme.org





Stellungnahme zum Richtlinienvorschlag der EU-Kommission zu
Software-Patenten



Die CEA-PME/EV-KMU ist eine parteipolitisch neutrale Vereinigung von 22 Unternehmerverb�nden aus
19 europ�ischen L�ndern. Sie vertritt die Interessen kleiner und mittlerer Unternehmen (KMU) aller
Branchen gegen�ber den europ�ischen Institutionen mit dem Ziel, den KMU ein ihrer Bedeutung f�r die
Volkswirtschaft in Europa entsprechendes Gewicht zu verschaffen.


Mario Ohoven, Pr�sident der Conf�d�ration Europ�enne des Associations de Petites et Moyennes
Entreprises (CEA-PME), lehnt den Richtlinienvorschlag der EU-Kommission zu den Software- bzw.
Logikpatenten ab, da dieser Vorschlag den Interessen der Europ�ischen Softwareunternehmen
zuwiderl�uft. Sollte das Europ�ische Parlament diesen Vorschlag ohne gr��ere �nderungen zur
Gesetzesrealit�t werden lassen, droht der Verlust von Arbeitspl�tzen und ein drastischer
Innovationsr�ckgang bis hin zum v�lligen Innovationsstopp bei den KMU�s.


Auf kleine und mittlere IT-Unternehmen, die nicht �ber eine gro�e Rechtsabteilung verf�gen, kommen
enorme Zusatzkosten zu, da sie in Zukunft zu jedem Software-Projekt umfangreiche Patentrecherchen
durchf�hren lassen m�ssen, wenn dieser Richtlinienentwurf so umgesetzt wird. Das gilt insbesondere nicht
nur f�r Software-Entwickler, sondern auch f�r Systemh�user und IT-Abteilungen von Anwenderunter-
nehmen. Hinzu kommen Lizenzgeb�hren f�r die Nutzung fremder Patente, Mehrkosten bei der
Entwicklung und Patentgeb�hren, falls man versucht, sich mit eigenen Patenten vor Angriffen zu sch�tzen.

Laut Monopolkommission haben empirische Studien (z. B. Frauenhofer-Institut) �ber das Verhalten von
kleinen und mittleren Unternehmen im Softwarebereich gezeigt, "dass Patente f�r diese zu den am
wenigsten effizienten Methoden des Investitionsschutzes z�hlen". Um sich gegen m�gliche Angriffe zu
sch�tzen wird von Software-Patent-Bef�rwortern geraten, flei�ig Software-Patente anzumelden und damit
ein eigenes Patentportfolios zu schaffen. Gerade aber KMU�s sind bisher ohne solche Patente bestens
ausgekommen. Hier bew�hrte sich seit Jahren das Urheberrecht.


Im Gegensatz zu anderen komplexen Technologien er�ffnet sich die M�glichkeit zur Entwicklung von
Software ohne weiteres f�r kleine und mittelst�ndische Unternehmen, wenn nicht sogar f�r
Einzelpersonen. Software Patente k�nnen allerdings lediglich von der Gro�industrie bezahlt werden, nicht
hingegen von den KMU�s, weswegen sie langfristig Innovationen untergraben. Sie f�hren zu erh�hten
Kosten und Unsicherheiten bei der Zusammenstellung der Einzelkomponenten von komplexen
Computerprogrammen und schr�nken damit die Schnelligkeit und die Effektivit�t von Innovationen ein.
Die Risiken und Haftungsfragen, die automatisch mit Patentverletzungen einhergehen und oft gnadenlos
von den Gro�industrien forciert werden, belasten besonders schwerwiegend kleine und mittelst�ndische
Unternehmen die ihre Energien und Ressourcen nicht mehr auf die Software-Entwicklung konzentrieren
k�nnen. Gerade aber kleine und mittelst�ndische Unternehmen sind und waren in der Software-Branche
die Wegbereiter und Unterst�tzer der Entwicklung. Der Einf�hrung der Software-Richtlinie wird somit
dazu f�hren, dass im Umgang mit Patenten bessere Strategien eingesetzt werden, nicht aber zu
Innovationen im Software-Bereich.

Urheberrechte und andere Wettbewerbsregelungen erlauben es bereits kleinen und mittelst�ndischen
Software-Unternehmen, trotz immenser Betriebsmittelvorteile gegen�ber gro�en Firmen zu bestehen. Aus
der flexiblen und wachstumsfreudigen Software-Branche k�nnte eine schwerf�llige Industrie werden, weil
ein Einstieg in diese Branche nur noch �ber weitl�ufige Absprachen und Vereinbarungen mit
Gro�unternehmen m�glich sein wird, und weil etliche juristische H�rden genommen werden m�ssen.
Auch wenn es einige kleine und mittelst�ndische Unternehmen in einem derartigen Umfeld schaffen
k�nnten, werden es sehr viele nicht schaffen, weil sich gerade kleine und mittelst�ndische Unternehmen
nicht erlauben k�nnen, seinen riesigen betriebswirtschaftlichen und juristischen Beraterstab zu
besch�ftigen.  Insbesondere werden langfristig best�tigte und gelockerte Standards im Patentbereich die
Aussichten f�r eine freie und leicht zug�ngliche Softwareindustrie innerhalb Europas verd�stern und die
Dominanz gegenw�rtig herrschender Marktf�hrer unterst�tzen. 


Aus Sicht von CEA-PME ist zwar eine Vereinheitlichung der europ�ischen Patentpraxis sinnvoll, jedoch
m�sse dabei ein Weg gew�hlt werden, der sowohl freie KMU�s als auch freie Software/Open-Source-
Software nicht behindere. Die beste L�sung sei auf jeden Fall, von einer Ausdehnung des Patent-Systems
auf das Gebiet der Logik abzusehen und in der Richtlinie deutlich klarzustellen, dass die
Datenverarbeitung selbst nicht technisch im Sinne des Patentwesens und damit nicht patentierbar ist.

CEA-PME vertritt die Ansicht, dass die bereits existierende Gesetzgebung wie sie im Urheberrecht
(91/250/EWG) und in dem Europ�ischen Patent �bereinkommen (EP�), Artikel 52 geregelt ist, ausreicht
und im Rahmen der EU-Richtlinie lediglich best�tigt werden sollte. Eine Auslegung der diesbez�glichen
Regelungen sollte wie bisher der Rechtsprechung �berlassen bleiben.



Br�ssel, den 15.09.2003
Gezeichnet: Walter Grupp


