<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Allianz von 2.000.000 KMU gegen Softwarepatente und EU-Richtlinie

#descr: Der Bundesverband der Mittelständischen Wirtschaft und ähnliche Verbände mehrerer Europäischer Länder haben gemeinsam im Rahmen ihres Dachverbandes CEAPME.org eine Presseerklärung herausgegeben, in der sie vor schädlichen Wirkungen von Softwarepatenten für Kleine und Mittlere Unternehmen (KMU) warnen und die Mitglieder des Europäischen Parlaments zur Ablehnung der geplanten Richtlinie in ihrer derzeitigen Form auffordern.

#eti: Stellungnahme von CEA-PME zum Richtlinienvorschlag der EU-Kommission zu Software-Patenten

#ehA: Statement of the Larger Alliance

#dii: Die CEA-PME/EV-KMU ist eine parteipolitisch neutrale Vereinigung von 22 Unternehmerverbänden aus 19 europäischen Ländern. Sie vertritt die Interessen kleiner und mittlerer Unternehmen (KMU) aller Branchen gegenüber den europäischen Institutionen mit dem Ziel, den KMU ein ihrer Bedeutung für die Volkswirtschaft in Europa entsprechendes Gewicht zu verschaffen.

#oro: Mario Ohoven, Präsident der Confédération Européenne des Associations de Petites et Moyennes Entreprises (CEA-PME), lehnt den Richtlinienvorschlag der EU-Kommission zu den Software- bzw. Logikpatenten ab, da dieser Vorschlag den Interessen der Europäischen Softwareunternehmen zuwiderläuft. Sollte das Europäische Parlament diesen Vorschlag ohne größere Änderungen zur Gesetzesrealität werden lassen, droht der Verlust von Arbeitsplätzen und ein drastischer Innovationsrückgang bis hin zum völligen Innovationsstopp bei den KMUs.

#sfh: Auf kleine und mittlere IT-Unternehmen, die nicht über eine große Rechtsabteilung verfügen, kommen enorme Zusatzkosten zu, da sie in Zukunft zu jedem Software-Projekt umfangreiche Patentrecherchen durchführen lassen müssen, wenn dieser Richtlinienentwurf so umgesetzt wird. Das gilt insbesondere nicht nur für Software-Entwickler, sondern auch für Systemhäuser und IT-Abteilungen von Anwenderunternehmen. Hinzu kommen Lizenzgebühren für die Nutzung fremder Patente, Mehrkosten bei der Entwicklung und Patentgebühren, falls man versucht, sich mit eigenen Patenten vor Angriffen zu schützen.

#ftt: Laut Monopolkommission haben empirische Studien (z. B. Frauenhofer-Institut) über das Verhalten von kleinen und mittleren Unternehmen im Softwarebereich gezeigt, %(q:dass Patente für diese zu den am wenigsten effizienten Methoden des Investitionsschutzes zählen). Um sich gegen mögliche Angriffe zu schützen wird von Software-Patent-Befürwortern geraten, fleißig Software-Patente anzumelden und damit ein eigenes Patentportfolios zu schaffen. Gerade aber KMUŽs sind bisher ohne solche Patente bestens ausgekommen. Hier bewährte sich seit Jahren das Urheberrecht.

#enc: Im Gegensatz zu anderen komplexen Technologien eröffnet sich die Möglichkeit zur Entwicklung von Software ohne weiteres für kleine und mittelständische Unternehmen, wenn nicht sogar für Einzelpersonen. Software Patente können allerdings lediglich von der Großindustrie bezahlt werden, nicht hingegen von den KMUŽs, weswegen sie langfristig Innovationen untergraben. Sie führen zu erhöhten Kosten und Unsicherheiten bei der Zusammenstellung der Einzelkomponenten von komplexen Computerprogrammen und schränken damit die Schnelligkeit und die Effektivität von Innovationen ein.  Die Risiken und Haftungsfragen, die automatisch mit Patentverletzungen einhergehen und oft gnadenlos von den Großindustrien forciert werden, belasten besonders schwerwiegend kleine und mittelständische Unternehmen die ihre Energien und Ressourcen nicht mehr auf die Software-Entwicklung konzentrieren können. Gerade aber kleine und mittelständische Unternehmen sind und waren in der Software-Branche die Wegbereiter und Unterstützer der Entwicklung. Der Einführung der Software-Richtlinie wird somit dazu führen, dass im Umgang mit Patenten bessere Strategien eingesetzt werden, nicht aber zu Innovationen im Software-Bereich.

#reW: Urheberrechte und andere Wettbewerbsregelungen erlauben es bereits kleinen und mittelständischen Software-Unternehmen, trotz immenser Betriebsmittelvorteile gegenüber großen Firmen zu bestehen. Aus der flexiblen und wachstumsfreudigen Software-Branche könnte eine schwerfällige Industrie werden, weil ein Einstieg in diese Branche nur noch über weitläufige Absprachen und Vereinbarungen mit Großunternehmen möglich sein wird, und weil etliche juristische Hürden genommen werden müssen.  Auch wenn es einige kleine und mittelständische Unternehmen in einem derartigen Umfeld schaffen könnten, werden es sehr viele nicht schaffen, weil sich gerade kleine und mittelständische Unternehmen nicht erlauben können, seinen riesigen betriebswirtschaftlichen und juristischen Beraterstab zu beschäftigen.  Insbesondere werden langfristig bestätigte und gelockerte Standards im Patentbereich die Aussichten für eine freie und leicht zugängliche Softwareindustrie innerhalb Europas verdüstern und die Dominanz gegenwärtig herrschender Marktführer unterstützen.

#tsi: Aus Sicht von CEA-PME ist zwar eine Vereinheitlichung der europäischen Patentpraxis sinnvoll, jedoch müsse dabei ein Weg gewählt werden, der sowohl freie KMUs als auch freie Software/Open-Source-Software nicht behindere. Die beste Lösung sei auf jeden Fall, von einer Ausdehnung des Patent-Systems auf das Gebiet der Logik abzusehen und in der Richtlinie deutlich klarzustellen, dass die Datenverarbeitung selbst nicht technisch im Sinne des Patentwesens und damit nicht patentierbar ist.

#eWn: CEA-PME vertritt die Ansicht, dass die bereits existierende Gesetzgebung wie sie im Urheberrecht (91/250/EWG) und in dem Europäischen Patent Übereinkommen (EPÜ), Artikel 52 geregelt ist, ausreicht und im Rahmen der EU-Richtlinie lediglich bestätigt werden sollte. Eine Auslegung der diesbezüglichen Regelungen sollte wie bisher der Rechtsprechung überlassen bleiben.

#ujm: Brüssel, den 16.09.2003

#ein: Gezeichnet

#atl: Die europäischen Verbände CEA-PME (Confédération Européenne des Associations de Petites et Moyennes Entreprises), CEDI (Europaverband der Selbständigen ­ Confédération Européenne Des Indépendants) und ESBA (European Small Business Alliance) vertreten europaweit ca. 2 Millionen kleine und mittelständische Unternehmen (KMU).

#not: Neen tegen Softwarepatenten

#WEW: This website has published some letters written by SME organisations such as UNIZO and UEAPME and consumer organisations (BEUC) in protest against the software patent directive proposal.

#shs: Selbstdarstellung im WWW

#rWe: Pressemitteilung

#lWc: Aktionsbündnis von CEA-PME und anderen Organisationen gegen Softwarepatente und die Richtlinie.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ceapme0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

