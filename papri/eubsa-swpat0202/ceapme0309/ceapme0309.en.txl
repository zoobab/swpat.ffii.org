<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Alliance of 2,000,000 SMEs against Software Patents and EU Directive

#descr: An alliance representing a total of 2,000,000 small and medium-sized businesses in Europe says that software patents are harmful for SMEs and that in particular the software patent directive proposal as amended by the European Parliament's Legal Affairs Commission is a grave risk for innovation, productivity and employment in Europe.

#eti: CEA-PME Statement on the Software-Patent-Directive

#ehA: Statement of the Larger Alliance

#dii: CEA-PME (engl. ECA-SME) is an ideologically neutral and non-party Confederation of 22 member associations from 19 European countries representing in total more than 500,000 enterprises. They represent the interests of SMEs in all sectors to the European institutions with the aim of giving them a level of influence commensurate with their importance within the European economy.

#oro: Mario Ohoven, President of the Confédération Européenne des Associations de Petites et Moyennes Entreprises (CEA-PME), rejects the proposed Directive on the Patentability of Computer-Implemented Inventions, since this proposal strongly runs contrary to the interests of European Software-Enterprises.  Should the European Parliament adapt this proposal without any changes, European economy would be threatened with the loss of thousands of jobs, a dramatic decline in innovation and even the stop of innovation for SMEs.

#sfh: If the Directive is enforced as it is considered, SMEs who do not possess legal advisors will be confronted with enormous additional costs, since in the future they will have to carry out wide inquiries in matters of patents for every software-project. This does not only regard developers of software, but also computer retailer and IT-branches of enterprises of application. They will also be confronted with costs for licensing for the utilisation of  external patents, additional costs for the development and costs for own patents in case enterprises should try to save themselves from the attacks of others.

#ftt: Empiric studies (e.g. Frauenhofer Institute) about the behaviour of SMEs in the Software field have shown, that patents are the least effective method to protect investments. In order to protect themselves from potential attacks, software patent supporters suggest applying for as many patents as possible in order to establish a patent portfolio. However, especially SMEs used to get along perfectly without patents. They are perfectly protected by the copyright law.

#enc: Unlike most complex technologies, the opportunity to develop software is open to small companies, and even to individuals. Software patens damage innovation by raising costs and uncertainties in assembling the many components needed for complex computer programs and constraining the speed and effectiveness of innovation. These risks and liabilities are particularly burdensome for SMEs, which play a central role in software innovation in Europe as well as North America. Moreover, within the ICT sector, expansion of patent protection has been found to lead to an increase in the strategic use of patents, but not to a demonstrable increase in innovation.

#reW: Copyright and other rules of competition permit SMEs to grow despite the overwhelming resource advantages of large companies. The flexible and easy-growing software-industrie could become a clumsy industry, because of the need for access to cross-licensing agreements and the legal protection of large corporations. While some SMEs will be able to prosper in this new environment, many will not. In particular, validating loosened standards on patent ability will cloud the prospects of Europe's ascendant free and open source software industry while preserving the dominance of present market leaders.

#tsi: CEA-PME promotes the harmonisation of the European patent practice. However a way has to be found, that neither constrains SMEs nor the free Software/Open-Source-Software. The best solution would be, to abstain from the enlargement of patent-systems on the field of logic and to clearly declare in the directive, that data processing is not technical and thus not patentable.

#eWn: CEA-PME holds the view that the existing legal legislation ­ as regulated in the copyright law and the European Patent Agreement, Article 52 is sufficient and should only be confirmed within the EC-directive.  The interpretation of the concerning regulation should be left to the jurisprudence.

#ujm: Brussels, 2003/09/16

#ein: Signed by

#atl: The three European federations CEA-PME (Confédération Européenne des Associations de Petites et Moyennes Entreprises), CEDI (European Confederation of Independents ­ Confédération Européenne Des Indépendants) and ESBA (European Small Business Alliance) represent around 2 million small and medium-sized enterprises (SMEs) throughout Europe.

#not: Neen tegen Softwarepatenten

#WEW: This website has published some letters written by SME organisations such as UNIZO and UEAPME and consumer organisations (BEUC) in protest against the software patent directive proposal.

#shs: website of the organisation

#rWe: Press Release

#lWc: Alliance for Action of CEA-PME and others.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ceapme0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

