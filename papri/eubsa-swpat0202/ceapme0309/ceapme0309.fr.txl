<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Alliance de deux millions de PME contre les brevets logiciels  et la directive de l'Union Européenne

#descr: Une alliance de plus de deux millions de petites et moyennes  entreprises européennes déclare que la brevetabilité du logiciel comme  prévue dans la proposition de directive de la Commission Juridique du  Parlement Européen est dangereuse pour ses membres, pour l'innovation,  la productivité et l'emploi en Europe.

#eti: Déclaration au sujet de la Directive sur la Brevetabilité des Logiciels

#ehA: Constitution de la Grande Alliance

#dii: CEA-PME (Confédération Européenne des Associations de Petites et Moyennes Entreprises) est une confédération apolitique de 22 associations membres de 19 pays européens représentant plus de 500 000 entreprises. Elle représente les intérêts des PME dans tous les secteurs des institutions européennes dans le but de leur donner un degré d'influence en rapport avec leur importance au sein de l'économie européenne.

#oro: Mario Ohoven, Président de la Confédération Européenne des Associations de Petites et Moyennes Entreprises (CEA-PME), rejette la proposition de Directive sur la Brevetabilité des Inventions implémentées sur Ordinateur, car cette proposition va profondément à l'encontre des intérêts des SSII européennes. Si le Parlement Européen adoptait cette proposition sans aucun amendement, l'économie européenne serait menacée de perdre des milliers d'emploi, de subir un déclin spectaculaire de ses innovations et cela pourrait aller jusqu'à l'arrêt total des innovations pour les PME.

#sfh: Si la Directive est mise en application comme prévu, les PME qui n'ont pas de juristes d'entreprise seront confrontés à d'énormes coûts supplémentaires, car elles devront, à l'avenir, mener à bien de vastes recherches en matière de brevets pour chaque projet logiciel. Cela ne concerne pas seulement les développeurs de logiciels, mais aussi les revendeurs d'ordinateurs et les branches IT des sociétés d'applications. Elles seront également confrontées à des coûts de licences pour utilisation de brevets externes, des coûts supplémentaires de développement et des coûts pour leurs propres brevets, si elles veulent essayer de se prémunir contre les attaques des autres entreprises.

#ftt: Selon la Commission Anti-trust, des études empiriques (par exemple de l'Institut Frauenhofer) sur le comportement des PME de la branche Logiciel ont montré que les brevets sont le moyen le moins efficace de protection des investissements. Pour se protéger d'attaques éventuelles, les tenants de la brevetabilité des logiciels suggèrent de déposer le plus grand nombre possible de brevets de façon à constituer un portefeuille de brevets. Néanmoins, les PME s'en sont toujours très bien sorties sans brevets. Elles sont parfaitement protégées par les droits d'auteur.

#enc: À la différence de technologies plus complexes, le développement de logiciels est ouvert aux petites sociétés et même aux personnes physiques. Les brevets logiciels nuisent à l'innovation en augmentant les coûts, en générant des incertitudes sur l'assemblage des nombreux composants nécessaires à des programmes complexes sur ordinateur et en limitant la rapidité et l'efficience des innovations. Ces risques et responsabilités sont particulièrement pesants pour les PME, qui jouent un rôle central dans l'innovation logiciel en Europe ainsi qu'en Amérique du Nord. En outre, on a constaté que dans le secteur TCI  l'augmentation de la protection par brevets conduisait à un accroissement de l'utilisation stratégique des brevets, mais pas à un développement sensible de l'innovation.

#reW: Les droits d'auteur et la réglementation en matière de concurrence permettent aux PME de se développer, malgré la multiple d'avantages en ressources dont disposent les grosses sociétés. Le secteur logiciel, à l'heure actuelle flexible et à fort potentiel de développement, pourrait devenir un secteur naufragé, car on ne pourrait plus y accéder qu'après de longues négociations avec les grosses entreprises et tout un parcours du combattant juridique. En supposant que quelques PME puissent prospérer dans ce nouvel environnement, nombre d'entre elles n'y arriveraient pas. La validation de standards vagues en matière de brevetabilité assombrirait les perspectives du secteur des logiciels libres et open source en Europe, tandis qu'elle préserverait la domination des leaders actuels.

#tsi: CEA-PME promeut l'harmonisation des pratiques de brevetabilité européenne. Néanmoins, il faut trouver une méthode qui ne restreigne ni les PME, ni le secteur logiciel libre / logiciel open source. La meilleure solution serait de s'abstenir d'étendre le système des brevets au champ de la logique et de déclarer clairement dans la Directive que le traitement des données ne relève pas du domaine technique et n'est donc pas brevetable.

#eWn: CEA-PME soutient que les lois en vigueur - comme les droits d'auteur et l'Accord sur les Brevets Européens, Article 52 - sont suffisantes et que la Directive doit se contenter de les entériner. L'interprétation de la législation afférente doit être laissée à la jurisprudence.

#ujm: Bruxelles, le 16/09/2003

#ein: Signé

#atl: Les trois confédérations européennes CEA-PME (Confédération Européenne des Associations de Petites et Moyennes Entreprises), CEDI (Confédération Européenne Des Indépendants) et ESBA (European Small Business Alliance - Assocation Européenne des Petites et Moyennes Entreprises) représentent environ 2 millions de petites et moyennes entreprises (PME) situées dans toute l'Europe.

#not: Neen tegen Softwarepatenten

#WEW: This website has published some letters written by SME organisations such as UNIZO and UEAPME and consumer organisations (BEUC) in protest against the software patent directive proposal.

#shs: Site web de l'organisation

#rWe: Dépêche

#lWc: Alliance pour l'Action de CEA-PME avec autres associations de PME

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ceapme0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

