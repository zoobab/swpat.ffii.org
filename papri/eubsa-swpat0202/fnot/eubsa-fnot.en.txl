<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Footnotes

#descr: Footnotes in the BSA version and the European Commission's version of the EU software patentability directive proposal of 2002-02-20

#CfA: Cf. study by Booz Allen & Hamilton for the Dutch Ministry of Economic Affairs, %(TIT), March 2000, at 10.

#Ser: September 2000

#Lno: London

#Fir2: For a definition of the term, see Art. 1

#ces: %(q:The Munich Convention). It entered into force on 7 October 1977. All 15 EC Member States as well as Cyprus, Liechtenstein, Monaco, Switzerland and Turkey are contracting states.

#Ocd: On the divergences in greater detail see below.

#Ppt: Promoting innovation through patents: Green Paper on the Community patent and the patent system in Europe COM(1997) 314 final, 24 June 1997

#PWt: Promoting innovation through patents: The follow-up to the Green Paper on the Community patent and the patent system in Europe COM (1999) 42 final , 5 February 1999

#Tpa: The patentability of computer-implemented inventions: consultation paper by the services of the Directorate-General for the Internal Market (19 October 2000). Paper available for downloading at %(URL).

#CcW: Contract no. %(REF)

#Rei: Report available for downloading at %(URL). A complementary guide on software protection for Small and Medium-sized Enterprises is also available for download from the following link: %(URL2).

#cni: %(TIT) (text available for downloading at %(URL). The study was conducted by the Intellectual Property Institute, London, on behalf of the Commission and finalised in March 2000. Other pertinent economic studies which have been taken into account and which relate to the divergent U.S. situation include %(STUD).

#Ifl: In the wake of the decision of the U.S. Court of Appeals for the Federal Circuit, of 23 July 1998, in State Street State Street Bank & Trust Co. v. Signature Financial Group, Inc., 149 F.3d 1368, patent applications for business methods have soared.

#StW: See study, at 5.

#Ilt: Ibid., at 3.

#IWe: Ibid., at 5 et seq.

#Ilt2: Ibid., at 3.

#Ilt3: Ibid., at 8.

#Ilt4: Ibid., at 36.

#CWC: Computer program product I and II, T1173/97 of 1.7.1998, 1999 OJ EPO [609] and T0935/97 of 4.2.1999, [1999] R.P.C. 861. The holdings of the two cases are largely similar.

#Ci5: Controlling pension benefits system/PBS T-0931/1995 decision dated 8.09.2000

#Srh: Supra. See also case T1002/92 where the EPO Board of Appeal made this criticism for the first time.

#ToW2: The claims have to be interpreted in the light of the description and the drawings relating to the invention. Cf., e.g., Art. 69(1) of the EPC.

#Sed: Such expression alone cannot serve as disclosure of a respective invention; see, e.g., EPO Guidelines for Substantive Examination, C-II, 4.14a.

#TWW: The law relating to copyright, as it applies to computer programs, was harmonised at Community level with the introduction of this Directive, Council Directive of 14 May 1991 on the legal protection of computer programs (91/250/EEC), [17.5.1991] OJ L 122, at 42. See Commission Report on the implementation and effects of Directive 91/250/EEC, COM(2000) 199 final of 10.4.2000.

#icn: insofar confirming %(WL).

#Che: Cf. in this sense %(NA).

#Cef: Case %(REF)

#deG: decision of the German supreme court (Bundesgerichtshof (BGH)) issued on October 17, 2001

#cWl2: %(UKN) of 19.4.1999

#aWe: available on the Patent Office website at %(URL)

#Cef2: Case %(REF)

#sup: supra

#Tyr: The BGH disapproved an earlier judgement of the Federal Patent Court (Bundespatentgericht) in which it was held that a claim to a carrier only with a computer program was not allowable. In doing so, the court seems indirectly to have indicated its approval of the EPO practice of permitting claims to computer programs on their own provided that when associated with computer apparatus, a technical contribution is achieved.

#SEW: See e.g. Directive 89/104/EEC approximating the laws of the Member States relating to trade marks (OJ L 40, 11.2.1989, at 1) ; Directive 91/250/EEC on the legal protection of computer programs (OJ L 122, 17.5.1991, at 42) ; Directive 93/98/EEC harmonising the term of protection of coyright and certain related rights (OJ L 290, 24.11.1993, at 9) ; and Directive 96/9/EC on the legal protection of databases (OJ L 77, 27.3.1996, at 20).

#Sas: See opinion 1/94, Competence of the Community to conclude international agreements concerning services and the protection of intellectual property [15.11.1994] ECR I-5267, and Case C-350/92 Spain v Council [13.7.1995] ECR I-1985. See also Case C-376/98, Federal Republic of Germany v. European Parliament and Council of the European Union [5.10.2000], where this choice was rejected for reasons which do not apply here (certain prohibitions of that Directive were considered as helping in no way to facilitate trade in the products concerned, see para. 99; moreover the Directive was held not to ensure free movement of products which are in conformity with its provisions (para. 101) since Member States retain the right to lay down stricter requirements without the Directive ensuring the free movement of products which conform to its provisions, see paras. 103 et seq.).

#Se1: See opinion 1/94, Competence of the Community to conclude international agreements concerning services and the protection of intellectual property [15.11.1994] ECR I-5267, and Case C-350/92 Spain v Council [13.7.1995] ECR I-1985.

#Cel: C-377/98. Pays-Bas v Parliament and Council. It was concluded (para 18-20): %(q:By requiring the Member States to protect biotechnological inventions by means of their national patent law, the Directive in fact aims to prevent damage to the unity of the internal market which might result from the Member States' deciding unilaterally to grant or refuse such protection.  However, the applicant submits, secondly, that if the application by the Member States of the relevant provisions of international law left a measure of legal uncertainty, it should have been removed not by Community harmonisation but by renegotiation of international legal instruments such as the EPC, in order to clarify their rules. That argument is unfounded. The purpose of harmonisation is to reduce the obstacles, whatever their origin, to the operation of the internal market which differences between the situations in the Member States represent. If divergences are the result of an interpretation which is contrary, or may prove contrary, to the terms of international legal instruments to which the Member States are parties, there is nothing in principle to prevent recourse to adoption of a Directive as a means of ensuring a uniform interpretation of such terms by the Member States.)

#Tl8: T26/86 (21.5.87) [1988] OJEPO 19

#S26: See Vicom Case T208/84 (15.7.1986) [1987] OJEPO 14

#OCj: OJ C, , p.

#OCj2: OJ C, , p.

#OCj3: OJ C, , p.

#Oj1: OJ L 336, 23.12.1994, p. 1

#O7l: OJL 122 , 17/05/1991 p. 0042 - 0046

#Ocm: OJ L 122 , 17.5.1991 p. 42­ Directive amended by Directive 93/98/EEC (OJ L 290, 24.11.1993, p. 9).

#Shy: See study of May 1998 by Price Waterhouse, %(q:The Contribution of the Packaged Software Industry to the European Economies), available at %(URL).

#CWW: COM(1997) 314 final of 24.6.1997. The issue had already been addressed in the Commission %(q:questionnaire on Industrial Property Rights in the Information Society), supra, note 34.

#Ra1: Resolution on the Commission Green Paper, A4-0384/98, Minutes of 19.11. 1998, paragraph 16, [1999] OJ EPO 197.

#Oe9: Opinion of the Economic and Social Committee on the Green Paper, [27.4.1998] OJ C 129, at 8, points 1.14., 6.9.1.1. and 6.9.1.2.

#SlW: See point 11 of the conclusions of this hearing, OJ EPO 1-2/1998, at 82.

#TtW: The programme of the conference as well as transcripts of the speeches given there are accessible on the world-wide web at %(URL).

#SIa: See, e.g., the EICTA position statement at %(URL).

#TWo: The representatives of EuroLinux have published an unofficial, non-authorised report of the meeting on the web site of the EuroLinux Alliance at %(URL).

#cWu: %(co:Opinion of the Committee of the Regions on 'The competitiveness of European enterprises in the face of globalisation - How it can be encouraged'), OJ C 57, 29.2.2000, at 36 et seq., %(cu:points 7.4. and 8.20.)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-fnot ;
# txtlang: en ;
# multlin: t ;
# End: ;

