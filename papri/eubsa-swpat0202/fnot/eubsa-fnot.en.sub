\begin{subdocument}{eubsa-fnot}{Footnotes}{http://swpat.ffii.org/papers/eubsa-swpat0202/fnot/index.en.html}{Workgroup\\swpatag@ffii.org}{Footnotes in the BSA version and the European Commission's version of the EU software patentability directive proposal of 2002-02-20}
\begin{description}
\item[Note 1\label{ftn1}:]\ Cf. study by Booz Allen \& Hamilton for the Dutch Ministry of Economic Affairs, ``The Competitiveness of Europe's ICT Markets'', March 2000, at 10.
\item[Note 2\label{ftn2}:]\ ``Packaged software in Western Europe: The economic impact of the packaged software industry on the combined economies of sixteen European countries'', September 2000, Datamonitor, London
\item[Note 3\label{ftn3}:]\ For a definition of the term, see Art. 1
\item[Note 4\label{ftn4}:]\ ``The Munich Convention''. It entered into force on 7 October 1977. All 15 EC Member States as well as Cyprus, Liechtenstein, Monaco, Switzerland and Turkey are contracting states.
\item[Note 5\label{ftn5}:]\ On the divergences in greater detail see below.
\item[Note 6\label{ftn6}:]\ Promoting innovation through patents: Green Paper on the Community patent and the patent system in Europe COM(1997) 314 final, 24 June 1997\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/paten.pdf}
\item[Note 7\label{ftn7}:]\ Promoting innovation through patents: The follow-up to the Green Paper on the Community patent and the patent system in Europe COM (1999) 42 final , 5 February 1999\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/8682en.pdf}
\item[Note 8\label{ftn8}:]\ The patentability of computer-implemented inventions: consultation paper by the services of the Directorate-General for the Internal Market (19 October 2000). Paper available for downloading at Software Patents - Commission launches Consultations\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softpaten.htm}.
\item[Note 9\label{ftn9}:]\ see Replies to the EC swpat consultation\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softpatanalyse.htm}\footnote{see also European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\footnote{http://swpat.ffii.org/papers/eukonsult00/index.en.html}}
\item[Note 10\label{ftn10}:]\ ``Patent protection of computer programmes'' (Contract no. INNO-99-04).
Report available for downloading at URL. A complementary guide on software protection for Small and Medium-sized Enterprises is also available for download from the following link: ftp://ftp.ipr-helpdesk.org/software.pdf.
\item[Note 11\label{ftn11}:]\ ``The Economic Impact of Patentability of Computer Programs'' (text available for downloading at http://europa.eu.int/comm/internal\_market/en/indprop/studyintro.htm\footnote{see also IPI 2000: The Economic Impact of Patentability of Computer Programs\footnote{http://swpat.ffii.org/papers/indprop-ipi00/index.en.html}}. The study was conducted by the Intellectual Property Institute, London, on behalf of the Commission and finalised in March 2000. Other pertinent economic studies which have been taken into account and which relate to the divergent U.S. situation include Cohen, Wesley M., Nelson, Richard R., and Walsh, John P., Protecting their Intellectual Assets: Appropriability Conditions and why U.S. Manufacturing Firms Patent (or not), Working Paper 7552, National Bureau of Economic Research, February 2000; Bessen, James and Maskin, Eric, Sequential Innovation, Patents, and Imitation, Working Paper, Department of Economics, Massachusetts Institute of Technology, January 2000; Jaffe, Adam B., The U.S. Patent System in Transition: Policy Innovation and the Innovation Process, Working Paper 7280, National Bureau of Economic Research, August 1999.
\item[Note 12\label{ftn12}:]\ In the wake of the decision of the U.S. Court of Appeals for the Federal Circuit, of 23 July 1998, in State Street State Street Bank \& Trust Co. v. Signature Financial Group, Inc., 149 F.3d 1368, patent applications for business methods have soared.
\item[Note 13\label{ftn13}:]\ See study, at 5.
\item[Note 14\label{ftn14}:]\ Ibid., at 3.
\item[Note 15\label{ftn15}:]\ Ibid., at 5 et seq.
\item[Note 16\label{ftn16}:]\ Ibid., at 3.
\item[Note 17\label{ftn17}:]\ Ibid., at 8.
\item[Note 18\label{ftn18}:]\ Ibid., at 36.
\item[Note 19\label{ftn19}:]\ Computer program product I and II, T1173/97 of 1.7.1998, 1999 OJ EPO [609] and T0935/97 of 4.2.1999, [1999] R.P.C. 861. The holdings of the two cases are largely similar.
\item[Note 20\label{ftn20}:]\ Controlling pension benefits system/PBS T-0931/1995 decision dated 8.09.2000
\item[Note 21\label{ftn21}:]\ Supra. See also case T1002/92 where the EPO Board of Appeal made this criticism for the first time.
\item[Note 22\label{ftn22}:]\ The claims have to be interpreted in the light of the description and the drawings relating to the invention. Cf., e.g., Art. 69(1) of the EPC.
\item[Note 23\label{ftn23}:]\ Such expression alone cannot serve as disclosure of a respective invention; see, e.g., EPO Guidelines for Substantive Examination, C-II, 4.14a.
\item[Note 24\label{ftn24}:]\ The law relating to copyright, as it applies to computer programs, was harmonised at Community level with the introduction of this Directive, Council Directive of 14 May 1991 on the legal protection of computer programs (91/250/EEC), [17.5.1991] OJ L 122, at 42. See Commission Report on the implementation and effects of Directive 91/250/EEC, COM(2000) 199 final of 10.4.2000.
\item[Note 25\label{ftn25}:]\ [1989] RPC 569.
\item[Note 26\label{ftn26}:]\ [1993] RPC 427; insofar confirming Wang Laboratories Inc's Application [1991] RPC 463.
\item[Note 27\label{ftn27}:]\ Cf. in this sense Nack, Ralph, Sind jetzt computerimplementierte Gesch\"{a}ftsmethoden patentf\"{a}hig? --- Analyse der Bundesgerichtshof-Entscheidung ``Sprachanalyseeinrichtung''\footnote{http://swpat.ffii.org/papers/grur-nack00/index.de.html}, [2000] GRUR Int. 853.
\item[Note 28\label{ftn28}:]\ [1999] GRUR 1078.
\item[Note 29\label{ftn29}:]\ [2000] GRUR 930
\item[Note 30\label{ftn30}:]\ Case X ZB 16/00\footnote{http://swpat.ffii.org/papers/bgh-suche01/index.de.html} (decision of the German supreme court (Bundesgerichtshof (BGH)) issued on October 17, 2001).\footnote{CEC version only}
\item[Note 31\label{ftn31} (B 30\label{ftnB30}):]\ see U.K. Patent Office practice notice of 19.4.1999 (available on the Patent Office website at http://www.patent.gov.uk/patent/notices/practice/computer.htm).
\item[Note 32\label{ftn32}:]\ Case X ZB 16/00\footnote{http://swpat.ffii.org/papers/bgh-suche01/index.de.html} (supra).
The BGH disapproved an earlier judgement of the Federal Patent Court (Bundespatentgericht) in which it was held that a claim to a carrier only with a computer program was not allowable. In doing so, the court seems indirectly to have indicated its approval of the EPO practice of permitting claims to computer programs on their own provided that when associated with computer apparatus, a technical contribution is achieved.\footnote{CEC version only}
\item[Note 33\label{ftn33} (B 31\label{ftnB31}):]\ See e.g. Directive 89/104/EEC approximating the laws of the Member States relating to trade marks (OJ L 40, 11.2.1989, at 1) ; Directive 91/250/EEC on the legal protection of computer programs (OJ L 122, 17.5.1991, at 42) ; Directive 93/98/EEC harmonising the term of protection of coyright and certain related rights (OJ L 290, 24.11.1993, at 9) ; and Directive 96/9/EC on the legal protection of databases (OJ L 77, 27.3.1996, at 20).
\item[Note 34\label{ftn34} (B 32\label{ftnB32}):]\ BSA: See opinion 1/94, Competence of the Community to conclude international agreements concerning services and the protection of intellectual property [15.11.1994] ECR I-5267, and Case C-350/92 Spain v Council [13.7.1995] ECR I-1985. See also Case C-376/98, Federal Republic of Germany v. European Parliament and Council of the European Union [5.10.2000], where this choice was rejected for reasons which do not apply here (certain prohibitions of that Directive were considered as helping in no way to facilitate trade in the products concerned, see para. 99; moreover the Directive was held not to ensure free movement of products which are in conformity with its provisions (para. 101) since Member States retain the right to lay down stricter requirements without the Directive ensuring the free movement of products which conform to its provisions, see paras. 103 et seq.).

CEC: See opinion 1/94, Competence of the Community to conclude international agreements concerning services and the protection of intellectual property [15.11.1994] ECR I-5267, and Case C-350/92 Spain v Council [13.7.1995] ECR I-1985.
\item[Note 35\label{ftn35}:]\ C-377/98. Pays-Bas v Parliament and Council. It was concluded (para 18-20): ``By requiring the Member States to protect biotechnological inventions by means of their national patent law, the Directive in fact aims to prevent damage to the unity of the internal market which might result from the Member States' deciding unilaterally to grant or refuse such protection.  However, the applicant submits, secondly, that if the application by the Member States of the relevant provisions of international law left a measure of legal uncertainty, it should have been removed not by Community harmonisation but by renegotiation of international legal instruments such as the EPC, in order to clarify their rules. That argument is unfounded. The purpose of harmonisation is to reduce the obstacles, whatever their origin, to the operation of the internal market which differences between the situations in the Member States represent. If divergences are the result of an interpretation which is contrary, or may prove contrary, to the terms of international legal instruments to which the Member States are parties, there is nothing in principle to prevent recourse to adoption of a Directive as a means of ensuring a uniform interpretation of such terms by the Member States.''\footnote{CEC version only}
\item[Note 36\label{ftn36} (B 33\label{ftnB33}):]\ see Note 20
\item[Note 37\label{ftn37} (B 34\label{ftnB34}):]\ T26/86 (21.5.87) [1988] OJEPO 19
\item[Note 38\label{ftn38} (B 35\label{ftnB35}):]\ See Vicom Case T208/84 (15.7.1986) [1987] OJEPO 14
\item[Note 39\label{ftn39} (B 36\label{ftnB36}):]\ OJ C, , p.
\item[Note 40\label{ftn40} (B 37\label{ftnB37}):]\ OJ C, , p.
\item[Note 41\label{ftn41} (B 38\label{ftnB38}):]\ OJ C, , p.
\item[Note 42\label{ftn42}:]\ OJ L 336, 23.12.1994, p. 1\footnote{CEC version only}
\item[Note 43\label{ftn43} (B 39\label{ftnB39}):]\ BSA: OJL 122 , 17/05/1991 p. 0042 - 0046

CEC: OJ L 122 , 17.5.1991 p. 42--- Directive amended by Directive 93/98/EEC (OJ L 290, 24.11.1993, p. 9).
\item[Note B40\label{ftnB40}:]\ See study of May 1998 by Price Waterhouse, ``The Contribution of the Packaged Software Industry to the European Economies'', available at http://www.bsa.org/statistics/index.html.
\item[Note B41\label{ftnB41}:]\ see Note 10
\item[Note B42\label{ftnB42}:]\ see Note 40 (BSA 37)
\item[Note B43\label{ftnB43}:]\ see Note 2
\item[Note B44\label{ftnB44}:]\ COM(1997) 314 final of 24.6.1997. The issue had already been addressed in the Commission ``questionnaire on Industrial Property Rights in the Information Society'', supra, note 34.
\item[Note B45\label{ftnB45}:]\ Resolution on the Commission Green Paper, A4-0384/98, Minutes of 19.11. 1998, paragraph 16, [1999] OJ EPO 197.
\item[Note B46\label{ftnB46}:]\ Opinion of the Economic and Social Committee on the Green Paper, [27.4.1998] OJ C 129, at 8, points 1.14., 6.9.1.1. and 6.9.1.2.
\item[Note B47\label{ftnB47}:]\ See point 11 of the conclusions of this hearing, OJ EPO 1-2/1998, at 82.
\item[Note B48\label{ftnB48}:]\ The programme of the conference as well as transcripts of the speeches given there are accessible on the world-wide web at http://www.patent.gov.uk/softpat/en/frmain.html.
\item[Note B49\label{ftnB49}:]\ COM(1999) 42 final of 5.2.1999.
\item[Note B50\label{ftnB50}:]\ See, e.g., the EICTA position statement at http://www.eicta.org.
\item[Note B51\label{ftnB51}:]\ The representatives of EuroLinux have published an unofficial, non-authorised report of the meeting on the web site of the EuroLinux Alliance at http://eurolinux.ffii.org/news/euipCAen.html.
\item[Note B52\label{ftnB52}:]\ Opinion of the Committee of the Regions on 'The competitiveness of European enterprises in the face of globalisation - How it can be encouraged'\footnote{http://www.cor.eu.int/presentation/down/Comm6/english/134-99.htm}, OJ C 57, 29.2.2000, at 36 et seq., points 7.4. and 8.20.\footnote{http://swpat.ffii.org/archive/quotes/index.en.html\#cor}
\item[Note B53\label{ftnB53}:]\ see Note 11
\item[Note B54\label{ftnB54}:]\ Ibid.
\item[Note B55\label{ftnB55}:]\ see Note 9
\end{description}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

