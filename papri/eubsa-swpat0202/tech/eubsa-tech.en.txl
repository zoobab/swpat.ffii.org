<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Why Amazon One Click Shopping is Patentable under the Proposed EU Directive

#descr: According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for %(q:Patentability of Computer-Implemented Inventions) and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as %(as:Amazon One Click Shopping) are without doubt patentable subject matter. This is because %(ol|Any %(q:computer-implemented) innovation is in principle considered to be a patentable %(q:invention).|The additional requirement of %(q:technical contribution in the inventive step) does not mean what most people think it means.|The directive proposal explicitly aims to codify the practise of the %(tp|European Patent Office|EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.|CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.)

#Uci: The EPO/CEC/JURI doctrine of %(q:technical contribution) in a nutshell

#ohn: Voided Terminology and its Technical Contribution to Political Engineering in the EU

#iWt: Privileged Status of the Amazon One-Click Patent in Brussels

#eWe: How CEC/JURI ensure Unlimited Patentability: Some Sample Provisions from their Directive Proposal

#qiW: According to EPO/CEC/JURI doctrine, the %(q:inventive step) between the %(q:closest prior art) and the %(q:invention) must involve what is called a %(q:technical contribution) or %(q:solution of a technical problem): an improvement of computing efficiency, e.g. a reduction in the number of mouse-clicks needed to perform a business transaction.  Equivalents of the %(q:technical contribution the inventive step) requirement exist in the US and Japan as well.  US patent attorneys therefore generally write business method patents in a way that allows them to pass the EPO's requirements, and thousands have already passed.  An attorney who is unable to patent algorithms and business methods under the EPO/CEC/JURI doctrine would not be worth his lawyer's fee.

#Itm: As if this wasn't enough, EPO/CEC/JURI moreover insist that the %(q:technical contribution) needn't be new and may consist entirely of %(q:non-technical features), and they staunchly refuse to define or explain what %(q:technical) means.  All amendment proposals in the European Parliament which tried to address these defects were %(jr:rejected) by JURI.

#tepra: In the tradition of patent law throughout the world and in particular in countries influenced by German jurisprudence, %(q:technology) has been understood to refer to %(q:applied natural science), and the %(ti:technical invention) has been defined as %(q:solution to a problem by use of controllable forces of nature) or similar.  The European Patent Office has inherited the terminology from the German patent tradition, but it has been reluctant to apply it in a binding way.  In recent years the word %(q:technical) has been used by the EPO with increasing frequency and decreasing clarity.  In %(ex:some decisions) it retains its meaning of %(q:concrete and physical).  In others it seems to mean only %(q:concrete).  One EPO judge %(ms:proposed) to redefine %(q:technical) as %(q:practical).

#Wxi: The EPO prefers to leave the term %(q:technical) undefined.  One EPO official %(ga:explains) why:

#tiW: I'm glad you asked me about that. It's a wonderful word, fuzzy and yet sounds meaningful. We love it. Until 2001, it had no basis whatsoever in the EPC, just a passing mention in a couple of rules. .... I agree that we've never defined 'technical'. It's deliberate and allows us to fine-tune as a consensus develops on what should be patentable. Yes, I do know the correct way to do that is by amending the law, but have you any idea how hard it is to get consensus on amending the EPC?

#aaW: Proponents of the software patentability directive at EPO/CEC/JURI have insisted that the word %(q:technical) %(ol|must be the only criterion for distinguishing patentable from unpatentable|must not be defined.)  MEP Arlene McCarthy relies on the ambiguity of the word %(q:technical) when she %(am:promotes the directive proposal) as follows:

#nid: McCarthy excludes only %(q:non-technical business methods) from patentability, and the European Patent Office (EPO) finds all %(q:computer-implemented business methods) to be %(q:technical).  Let us quote what the EPO and leading patent practitioners have to say about this:

#tlb: It should be emphasised that ... the computer implementation of a ... business method, can involve %(q:technical considerations), and therefore be considered the solution of a technical problem, if implementation features are claimed.

#Wrd: The Portuguese Patent Office explains its support for the European Commission's directive approach:

#tmc: We approve the %(cf:harmonised position of the %(tp|US Patent Office|USPTO), the %(tp|Japanese Patent Office|JPO) and the %(tp|European Patent Office|EPO) concerning the patentability of these inventions), according to which technical features are required in order for a computer-implemented business method to be considered patentable.  According to EPO and JPO these computer-related technical features must be claimed explicitly, wheras according to the USPTO it is sufficient if the claims implicitly refer to such features.

#jun00: June 2000

#Wii: An apparatus constituting a physical entity or concrete product, suitable for performing or supporting an economic activity, is an invention within the meaning of Article 52(1) EPC.

#emn: There is no basis in the EPC for distinguishing between %(q:new features) of an invention and features of that invention which are known from the prior art when examining whether the invention concerned may be considered to be an invention within the meaning of Article 52(1) EPC.

#Wih: In T 769/92, General purpose management system/SOHEI, the method claim as allowed opened with the words %(q:method for operating a general-purpose computer management system), the steps of the method being closely related to functional features defining the computer system operated by this method. The board in that case found that the invention had technical character because it implied a need for technical considerations when carrying out that invention. A technical invention could not lose its technical character, because it was used for a non-technical purpose, like, for instance, financial management. Therefore, the purpose of such a method and of its individual steps remained a technical one, namely operating a technical system, which ensured the technical character of the method itself.

#stt: In T 1002/92, Queueing system/PETTERSSON, a %(q:system for determining the queue sequence for serving customers at a plurality of service points) was decided to be a three-dimensional apparatus and, therefore, clearly technical in nature, which clearly distinguishes the subject-matter of this case from that of the method claims of the present case.

#arc: It may very well be that, as put forward by the appellant, the meaning of the term %(q:technical) or %(q:technical character) is not particularly clear. However, this also applies to the term %(q:invention). In the Board's view the fact that the exact meaning of a term may be disputed does in itself not necessarily constitute a good reason for not using that term as a criterion, certainly not in the absence of a better term: case law may clarify the issue.

#aco: A scheme for learning a language, a method of solving cross-word puzzles, a game (as an abstract entity defined by its rules) or a scheme for organising a commercial operation would not be patentable. However, if the claimed subject-matter specifies an apparatus or technical process for carrying out at least some part of the scheme, that scheme and the apparatus or process have to be examined as a whole. In particular, if the claim specifies computers, computer networks or other conventional programmable apparatus, or a program therefor, for carrying out at least some steps of a scheme, it is to be examined as a %(q:computer-implemented invention) (see below).

#eam: If a computer program is capable of bringing about, when running on a computer, a further technical effect going beyond these normal physical effects, it is not excluded from patentability [...]. This further technical effect may be known in the prior art. A further technical effect which lends technical character to a computer program may be found e.g. in the control of an industrial process or in processing data which represent physical entities or in the internal functioning of the computer itself or its interfaces under the influence of the program and could, for example, affect the efficiency or security of a process, the management of computer resources required or the rate of data transfer in a communication link.

#boe: When considering whether a claimed computer-implemented invention is patentable, the following is to be borne in mind. In the case of a method, specifying technical means for a purely non-technical purpose and/or for processing purely non-technical information does not necessarily confer technical character on any such individual step of use or on the method as a whole. On the other hand a computer system suitably programmed for use in a particular field, even if that is, for example, the field of business and economy, has the character of a concrete apparatus, in the sense of a physical entity or product, and thus is an invention within the meaning of Art. 52(1) (see %(pb:T 931/95), OJ 10/2001, 441).

#gnW: If a claimed invention does not have a prima facie technical character, it should be rejected under Art. 52(2) and (3). In the practice of examining computer-implemented inventions, however, it may be more appropriate for the examiner to proceed directly to the questions of novelty and inventive step, without considering beforehand the question of technical character. In assessing whether there is an inventive step, the examiner must establish an objective technical problem which has been overcome (see IV, 9.5). The solution of that problem constitutes the invention's technical contribution to the art. The presence of such a technical contribution establishes that the claimed subject-matter has a technical character and therefore is indeed an invention within the meaning of Art. 52(1). If no such objective technical problem is found, the claimed subject-matter does not satisfy at least the requirement for an inventive step because there can be no technical contribution to the art, and the claim is to be rejected on this ground.

#goW: When assessing inventive step the examiner normally applies the problem and solution approach.

#ope: In the problem and solution approach there are three main stages:

#rWi: determining the closest prior art,

#sas: establishing the technical problem to be solved, and

#WWW: considering whether or not the claimed invention, starting from the closest prior art and the technical problem, would have been obvious to the skilled person.

#npg: The expression technical problem should be interpreted broadly; it does not necessarily imply that the solution is a technical improvement over the prior art. Thus the problem could be simply to seek an alternative to a known device or process providing the same or similar effects or which is more cost-effective.

#oao2: Of course, the liberal practice in the U.S. requires to file almost any business method for patent. These business methods are always implemented by a computer (I have not seen any other example). This computer implementation in most cases gives the opportunity to look for some sort of a %(q:technical effect) or %(q:technical consideration) or %(q:technical problem) etc. which might help to obtain a European patent, in the long run. When studying a business method computer software program it turns out that in most cases that program includes at least one aspect (feature) which might qualify under the European standards as %(q:technical). It is an advisable measure for any bank these days to try to develop a patent portfolio in order to have at least some arguments in hand when approached by others for patent infringement.

#dob: K. Beresford, World Patent Information 23 (2001) 253-263: European patents for software, E-commerce and business model inventions.

#adt: A british and european patent attorney, known by his manual %(q:Patenting Software Under the European Patent Convention), points out the gap between written law and judicial practise concerning patentability in Europe today.  This article's abstract summarises Beresford's basic message:

#eoh: It is commonly held that software, e-commerce and business models related inventions are inhenrently unpatentable in Europe. In reviewing the situation, this article emphasises the large number of inventions in this field which have in fact been patented through the EPO or through national patent offices in Europe. [...] He shows that, if proper attention is paid to both the substance and the form of claims and description, to direct the reader to the technical problem and its solution, effective protection is in fact very often available.

#dtn: The %(tp|European Commission|CEC) explains how its directive proposal aims to replace the word %(q:patentable) by the word %(q:technical) and then leave it up to the courts to define that word.  CEC explains recent EPO caselaw but adds the word %(q:physical) in a context where it is redundant and contributes only to political engineering:

#tic: It would not be possible for a legal text such as a Directive to attempt to spell out in fine detail what is meant by %(q:technical), because the very nature of the patent system is to protect what is novel, and therefore not previously known. In practice the courts will determine in individual cases what is or is not encompassed within the definition. However, earlier court decisions have indicated that a technical contribution may arise if there has been some improvement in the way that processes are carried out or resources used in a computer (for example an increase in the efficiency of a physical process), or if the exercise of technical skills beyond %(q:mere) programming has been necessary to arrive at the invention.

#Woa: In 2002 the German Federal Patent Court (Error Search decision, 26 March 2002, BPatG 17 W (pat) 69/98) rejects an EPO patent on a non-technical innovation and points out that the EPO's doctrine of %(q:technical contribution) makes all business methods patentable and is therefore in violation of the European Patent Convention:

#sWn: From this it becomes clear that the %(q:technical contribution) means %(e:trivial improvement of computing efficiency), as found in any %(q:computer-implemented business method).  This %(q:contribution) is not %(q:made) by the %(q:inventor) but constructed by the attorney.  A patent attorney who can not construct a %(q:technical problem) from a business method would hardly be worth his fees.  And any solution to a %(q:technical problem) will necessarily entail some kind of improvement in computing efficiency.

#trP: Once software becomes patentable, the word %(q:technical) can no longer mean %(q:concrete and physical) but at best %(q:concrete).  However, since %(ta:all non-trivial advances in software are of abstract nature), %(q:concrete) in this context implies %(q:trivial).  Which of course does not mean that a business method must be trivial in order to qualify for patentability at the EPO.  E.g. Karmarkar's solution to the problem of the travelling salesman was not trivial.  However its ingenuity lies in the field of mathematics, and in order to qualify for an EPO patent, ingenuity in mathematics is said to be irrelevant, whereas novelty (compared to known mathematical methods) and the presence of a %(tp|technical contribution|i.a. a %(e:trivial improvement in efficiency of solving a constructed computing problem)) are necessary conditions.

#epogl01t: EPO Examination Guidelines on Inventions and Art 52 EPC

#epogl01d: The section from the Guidelines for Examination in the European Patent Office of 2001-10-05 that deals with what are Patentable Inventions and how the list of non-inventions in Article 52 of the European Patent Invention (EPC) is to be applied in examination.  Especially in the sections concerning computer programs and business methods, it says exactly the opposite of what the guidelines of 1978 said: today computer-implemented rules of organisation and calculation are in principle to be considered as patentable inventions.

#amph: standard reply which McCarthy has been sending out to inquirers, based on a FAQ of May 2003.  Claims that One Click wouldn't be patentable under her proposal and that she has been doing honest work.

#Wcs: The European Commission (CEC) has never cited any example patents in order to explain what should be patentable and what not, the only exception being %(q:Amazon One Click Shopping).  Yet, although CEC may make the casual reader believe that Amazon One Click Shopping would not be patentable in Europe, the more attentive reader clearly recognises from this statement that in fact it is:

#amazq: Q: Would the Amazon %(q:one-click) shopping cart ordering model be patentable under the Commission proposal?

#amaza: A: The European Patent Office has yet to come to a decision on the related European application, so it would not be appropriate to comment on whether there is any patentable subject-matter in the application as a whole. However, a patent with the breadth of claims which has been granted in the United States would be highly unlikely to be considered to make a %(q:technical contribution) in the EU under the terms of the proposed Directive.

#iWd: In other words: the Amazon 1Click method is in principle patentable by EPO/CEC standards but, depending on what prior art is found, the claims might be narrower than in the USA.  It should be noted that in the USA also the claims encompassed the %(q:ordering model) %(q:only) in a context of improvements in the way a computer is used (reduction of number of needed mouse-clicks = %(q:technical contribution)).  As with most other US business patents on %(q:computer-implemented business methods), the there was no reason to assume that %(q:Amazon One Click Shopping) could fail the EPO's test of %(q:technical contribution in the inventive step).

#noW: Arlene McCarthy explicitly refuses to explain her position in terms of example patents, but, when it comes to %(q:Amazon One Click Shopping), is quick to deviate from this rule: %(orig:While it is not possible to comment on whether any patent application would be excluded from the directive, the directive, as amended, would not permit the patentability of Amazon's 'one-click' method.)  Unfortunately McCarthy does not support this assertion with any reasoning as to what in her proposal, particularly in her amendments, might make Amazon 1Click unpatentable.  Arlene McCarthy has less difficulty than the patent experts of the European Commission in reconciling political expediency with the expert's conscience.  This may in part be due to the fact that, as she %(am:says) herself, she is %(q:not an expert).

#oyn: Almost every provision in the CEC/JURI proposal assures unlimited patentability, often couched in wording which means the opposite of what it may seem to the unexperienced reader.  JURI has added nothing but further extensions of patentability and an extra layer of dissimulation.  Let us cite a few examples.

#prov: Nr.

#text: Text

#expl: Explanation

#rvt: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.  In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

#nnn: %(ei:In recent EPO usage, %(q:susceptible of industrial application) means no more than %(q:potentially of commercial value).)  This voided requirement, together with novelty and %(tp|%(q:inventive step)|%(q:non-obviousness)), is a basic part of all patent law and therefore redundant in this context.  What this provision really does is to abolish the EPC's requirement of technical invention by declaring it to be somehow (contrary to logic and law) implied in the non-obviousness requirement.

#bec: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#iee: When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Btu: This provision, introduced by Arlene McCarthy, clearly states the %(pb:EPO Pension Benefit System doctrine) by which anything running on a computer is a technical invention and the %(q:inventive step) requirement is said to inherit the function of the EPC's %(q:technical invention) requirement.  Moreover, while the %(e:technical invention) was a synonym of %(e:technical solution), from now on the solution no longer needs to be %(q:technical).  It is sufficient if a %(q:technical problem) is identified.

#tdh: McCarthy is using this new recital text to replace CEC Article 3, which said the same more readably: %(bc:Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.)  This sparked irresistible criticism from all sides. McCarthy vaguely echoes this criticism %(bc:This article is unnecessary and unclear in scope.  It would be difficult to put into effect, and might lead to unpredictable results.  It might be construed as extending the scope of patent protection.) and proposes deletion of Article 3, only to reinsert the criticised provisions in less clear wording in recital 12.

#ttt: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present.  Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#sif: The unexperienced reader may see this as a limitation of patentability.  The experienced reader understands: a computer-implemented business method constitutes a patentable invention, provided that it is framed as a solution to a %(q:technical problem).

#nei: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.  Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem.  However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#smW: To the casual reader, this suggests that algorithms are not patentable.  To the attentive reader, it says clearly that algorithms can be patented simply by framing them as solutions to a problem of improving computing efficiency, thus monopolising the algorithm's use on the universal computer in all contexts where it could ever be of practical relevance.

#eak: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#sdf: The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#oao: CEC says directly that the %(q:technical contribution) (i.e. the solution to the %(q:technical problem)) may consist solely of non-technical features.  This sparked widespread criticism.  JURI evades by saying exactly nothing about how the %(q:contribution) is assessed.  Yet, indirectly, the JURI version still allows the conclusion that the solution to the %(q:technical problem) may consist of non-technical features only.

#hWW: A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

#oIW: All software innovations can be claimed in terms of %(q:technical problems) or %(q:technical effects beyond the normal physical interactions between a program and the computer).  This exclusion means exactly nothing.  The %(q:justification) again suggests that business methods are patentable, if framed in terms of a %(q:technical problem): %(bc:It also makes it clear that the computer implementation of a business method simpliciter is not a patentable invention.)  This sentence contradicts the JURI text of recital 12, according to which computer-implementations %(q:by virtue of their very nature) qualify as technical inventions (but, in addition to that need to %(q:make a technical contribution in their inventive step)).

#nWc: A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

#Wra: Program claims were not allowable at all under the CEC proposal.  The adverb %(q:only) is misleading, because %(ol|this amendment does not constitute a limitation but an extension of allowable claim forms|Articles 4 and 4a do not exclude any subject matter from patentability|Nobody ever suggested or dreamt that by claiming a process as a program or vice versa the scope of patentable subject matter could be expanded or reduced.)  Most of the text in this article does not serve to clarify but to dissimulate.

#rti: Fairly accurate report about the patent situation from an Australian perspective.

#one: This article contains a comparison of EPO, JPO and USPTO practise concerning patenting of abstract methods and finds the EPO to be the most generous of all to grant such patents.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-tech ;
# txtlang: en ;
# multlin: t ;
# End: ;

