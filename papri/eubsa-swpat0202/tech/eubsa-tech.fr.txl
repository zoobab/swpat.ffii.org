<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Pourquoi Amazon One Click Shopping est brevetable selon la  proposition de directive de l'UE

#descr: En accord avec la proposition de directive de la Commission  Européenne COM(2002)92  pour la %(q:brevetabilité des inventions mises  en oeuvre par ordinateur) et la version modifiée et approuvée par la  Direction des Affaires juridiques et du Marché intérieur du Parlement  Européen (JURI), les algorithmes et méthodes commerciales tels que le  %(as:Amazon One Click Shopping) sont sans aucun doute des sujets  susceptibles de brevetabilité. Cela parce que %(ol|Toute innovation  %(q:mise en oeuvre par ordinateur) est par principe considéré comme  %(q:invention) brevetable.| L'exigence supplémentaire de  %(q:contribution technique dans l'activité inventive) ne signifie pas ce  que la plupart des gens pense que cela signifie.|La proposition de  directive a pour but explicite de codifier la pratique de l'%(tp|Office  des Brevets Européen|OEB). L'OEB a déjà accordé des milliers de brevets  sur des algorithmes et des méthodes commerciales similaires au ''One  Click Shopping'' d'Amazon.|CEC et JURI ont construit, en plus, des  failles importantes assurant que, même si certaines dispositions sont  amendées par le Parlement Européen, une brevetabilité illimitée demeure  assurée.)

#Uci: La doctrine de l'EOB/CEC/JURI sur la %(q:contribution technique) résumée en deux mots

#ohn: Des Notions Vidées de Sens et leur Contribution Technique a l'Engineering Politique dans l'Union Européenne

#iWt: Statuts Privilégiés du Brevet Amazon One-Click à Bruxelles

#eWe: Comment la CEC/JURI maintient la Brevetabilité Illimitée: Quelques Echantillons de Mesures de leur Proposition de Directive

#qiW: Selon la doctrine de l'EOB/CEC/JURI, l'%(q:avancée inventive) entre l'%(q:art premier le plus proche) et l'%(q:invention) doit impliquer ce qui est appelé une %(q:contribution classique) ou %(q:une solution à un problème technique): une amélioration de l'efficacité de traitement, par exemple une diminution du nombre de clics de souris nécessaire pour accomplir une transaction commerciale.  Les équivalents de la condition requise à la %(q:contribution technique à l'avancée inventive) existent aux Etats-Unis tout comme au Japon.  Les avocats en brevet américains écrivent donc généralement des brevets de méthodes commerciales de sorte qu'ils répondent aux conditions requises par l'EOB, et des milliers les ont déjà passées.  Un avocat qui serait incapable de faire breveter des algorithmes ou des méthodes commerciales selon la doctrine de l'EOB/CEC/JURI ne mériterait pas ses honoraires d'avocat.

#Itm: Comme si ce n'était pas suffisant, l'EOB/CEC/JURI insiste encore plus sur le fait que la %(q:contribution technique) n'a pas besoin d'être nouvelle et peut être entièrement composée de %(q:caractéristiques non techniques), et ils ont résolument refuser de définir ou expliquer ce qui signifie %(q:technique).  Toutes les propositions d'amendements du Parlement Européen qui ont essayé de corriger ces défauts ont été %(jr:rejetées) par la JURI.

#tepra: Dans la tradition de la loi sur les brevets de par le monde et particulièrement dans les pays influencés par la jurisprudence allemande, le terme %(q:technologie) a été admis comme référant à %(q:une science naturelle appliquée), et le terme %(ti:invention technique) a été défini comme %(q:solution à un problème au moyen de forces naturelles contrôlables) ou similaires.  L'Office Européen des Brevets a hérité de cette terminologie de la tradition de brevet allemande, mais a renâclé à l'appliquer de manière obligatoire.  Ces dernières années, le mot %(q:technique) a été utilisé par l'EOB avec une fréquence accrue et une clarté décroissante.  Dans %(ex:quelques décisions) il retient sa significationde %(q:concret et matériel).  Dans d'autres il semble signifier seulement %(q:concret).  Un juge de l'EOB %(ms:a proposé) de redéfinir %(q:technique) en %(q:pratique).

#Wxi: L'EOB préfère laisser le terme %(q:technique) indéfini.  Un officiel de l'EOB %(ga:explique) pourquoi:

#tiW: Je suis heureux que vous me posiez la question à ce sujet. C'est un mot merveilleux, flou et qui cependant a l'air plein de sens. Nous l'adorons. Jusqu'en 2001, il n'avait aucune base que ce soit à la Conventio des Brevets Européenne (EPC), juste une mention en passant dans deux règles. .... J'admet que nous n'avons jamais défini le mot 'technique'. C'est délibéré et cela nous permet des ajustements précis quand un consensus se développe sur ce qui pourrait être brevetable. Oui, je sais pertinemment que la façon correcte pour faire cela passe par un amendement de la loi, mais avez-vous une quelconque idée sur la difficulté qu'il y a à obtenir un consensus sur un amendement de l'EPC?

#aaW: Les laudateurs de la directive de brevetabilité logicielle à l'EOB/CEC/JURI ont insisté sur le fait que le mot %(q:technique) %(ol|doit être le seul critère pour distinguer le brevetable du non brevetable|ne doit pas être défini.)  Le Membre du Parlement Européen Arlene McCarthy s'appuie sur l'ambiguïté du mot %(q:technique) quand elle %(am:soutient la proposition de directive) comme suit:

#nid: McCarthy exclut seulement %(q:les méthodes commerciales non techniques) de la brevetabilité, et l'Office Européen des Brevets (EOB) trouve que toutes %(q:les méthodes commerciales mises en oeuvre sur ordinateur) sont %(q:techniques).  Citons ce que l'EOB et les praticiens en brevets en vue ont à dire à ce sujet:

#tlb: Il devrait être souligné que ... la mise en oeuvre sur ordinateur d'une ... méthode commerciale, peut impliquer %(q:des considerations techniques), et par conséquent peut être considérée comme une solution à un problème technique, si les caractéristiques mises en oeuvre sont revendiquées.

#Wrd: L'Office des Brevets Portugais explique son soutien à l'approche de la directive de la Commission Européenne:

#tmc: Consideramos, também, equilibrada %(cf:a posição harmonizada dos Institutos de patentes dos Estados Unidos, do Japão e do Instituto Europeu de Patentes relativamente aos critérios de apreciação da patenteabilidade dessas invenções), para os quais são necessárias características técnicas para que um método de negócio implementado por um computador possa ser considerado patenteável (estas características técnicas, relacionadas com o computador, têm que ser expressas nas reivindicações, de acordo com os Institutos japonês e europeu, enquanto que para o Instituto americano basta que essas características estejam implicitamente expressas nas reivindicações).

#jun00: juin 2000

#Wii: Un dispositif constituant une entité physique ou un produit concret, appropriée pour réaliser ou soutenir une activité économique, est une invention au sens de l'article 52(1) EPC.

#emn: Il n'y a pas de base dans l'EPC permettant de distinguer %(q:les nouvelles caractéristiques) d'une invention des caractéristiques d'une invention qui sont connues de l'art premier quand on examine si l'invention concernée peut être considérée ou non comme une invention au sens de l'article 52(1) EPC.

#Wih: Dans le T 769/92, Système de gestion de portée générale/SOHEI, la méthode de revendication qui est autorisée commence par ces mots %(q:méthode pour manoeuvrer un système de gestion d'ordinateur de portée générale), les avancées de cette méthode sont fortement liées aux caractéristiques fonctionnelles définissant le système d'ordinateur manoeuvré par cette méthode. Le conseil dans ce cas a trouvé que l'invention avait un caractère technique car elle impliquait un besoin de considérations techniques en sortant cette invention. Une invention technique ne pourrait pas perdre son caractère technique, parce qu'elle serait utilisée à des fins non techniques, comme, par exemple, la gestion financière. Par conséquent, le propos d'un telle méthode et de ses avancées restent techniques, c'est-à-dire manoeuvrant un système technique, ce qui implique un caractère technique de la méthode elle-même.

#stt: Dans le T 1002/92, Système de file d'attente/PETTERSSON, un %(q:système pour déterminer l'ordre de la file d'attente pour desservir les clients de plusieurs points clientèle) a été déclaré comme étant un dispositif tri-dimensionnel, et donc, évidemment technique par nature, ce qui distingue clairement le sujet de ce cas de la revendication de méthode de ce cas.

#arc: Cela pourrait très bien être cela, comme l'a mis en avant l'appelant, la signification du terme %(q:technique) ou %(q:caractère technique) n'est pas particulièrement clair. Quoiqu'il en soit, cela s'applique aussi au terme %(q:invention). Dans la vision du Conseil, le fait que l'exacte signification d'un terme puisse être discuté ne constitue pas nécessairement une bonne raison pour ne pas utiliser ce terme comme critère, certainement pas en l'absence d'un meilleur terme: les tribunaux peuvent clarifier la question.

#aco: Une méthode d'apprentissage de langue, une méthode de résolution de grilles de mots-croisés, un jeu (en tant qu'entité abstraite définie par ses règles) ou une méthode pour organiser une opération commerciale ne serait pas brevetable. Quoi qu'il en soit, si le sujet revendiqué spécifie un dispositif ou un processus technique pour mettre en évidence quelques parties de la méthode, cette méthode et le dispositif ou le processus doivent être examinés comme un tout. En particulier, si la revendication spécifie des ordinateurs, des réseaux d'ordinateurs ou un autre dispositif conventionnel programmable, ou un programme donc, pour mettre en évidence au moins quelques avancées d'une méthode, elle doit être examinée comme une %(q:invention mise en oeuvre sur ordinateur) (voir ci-dessous).

#eam: Si un programme d'ordinateur est capable d'apporter, quand il est exécuté sur un ordinateur, un effet technique supplémentaire allant au-delà de ces effets physiques normaux, il n'est pas exclu de la brevetabilité [...]. Cet effet technique supplémentaire peut être connu dans l'art premier. Un effet technique supplémentaire qui prêterait un caractère technique à un programme d'ordinateur peut être trouvé, par exemple dans le contrôle d'un processus industriel ou dans un traitement de données  qui représente des entités physiques ou le fonctionnement interne de l'ordinateur ou ses interfaces en interaction avec le programme et pourrait, par exemple, affecter l'efficacité ou la sécurité d'un processus, la gestion des ressources requises d'un ordinateur ou le taux de transfert de données sur un lien réseau.

#boe: Quand on se pose la question de savoir si une revendication d'invention mise en oeuvre sur ordinateur est brevetable ou non, ce qui suit doit être gardé présent à l'esprit. Une méthode, spécifiant des moyens techniques pour un sujet purement non technique et/ou pour un traitement purement non technique de l'information, ne confère pas nécessairement un caractère technique sur une quelconque étape d'utilisation ou sur la méthode en son entier. D'un autre côté, un système d'ordinateur programmé de façon appropriée pour une utilisation d'un domaine particulier, même si c'est, par exemple, le domaine de l'économie ou de l'entreprise, a le caractère d'un dispositif concret, au sens de l'entité physique ou du produit physique, et donc est une invention au sens de l'article. 52(1) (voir %(pb:T 931/95), OJ 10/2001, 441).

#gnW: Si une invention revendiquée n'a pas de caractère technique de prime abord, il devrait être rejeté selon les articles 52(2) et (3). Dans la pratique de l'examen des inventions mises en oeuvre sur ordinateur, cependant, il peut être plus approprié pour l'examinateur de procéder directement à l'examen des questions de nouveauté et d'avancée inventive, sans préjuger de la question du caractère technique. En évaluant s'il y a une avancée inventive, l'examinateur doit établir un problème technique objectif  qui a été résolu (voir IV, 9.5). La solution de ce problème constitue la contribution technique de l'invention à l'art. La présence d'une telle revendication technique établit que le sujet revendiqué a un caractère technique et donc est vraiment une invention au sens de l'article 52(1). Si aucun problème technique objectif n'est trouvé, le sujet revendiqué ne satisfait pas au moins la condition requise pour l'avancée inventive car il n'y a pas de contribution technique à l'art, et la revendication doit être rejetée sur cette base.

#goW: Quand il évalue une avancée inventive l'examinateur applique normalement l'approche problème - solution.

#ope: Dans l'approche problème - solution il y a trois étapes principales:

#rWi: déterminer l'art premier le plus proche,

#sas: établir le problème technique à résoudre, et

#WWW: considérer si une invention revendiquée, partant de l'art premier le plus proche d'un problème technique, aurait été évidente pour une personne qualifiée.

#npg: L'expression problème technique devrait être interprêtée au sens large; cela n'implique pas nécessairement que la solution soit une amélioration technique de l'art premier. De cette façon, le problème pourrait simplement être la recherche d'une alternative à un composant connu ou à un processus connu fournissant les mêmes effets ou des effets similaires ou qui serait plus efficace du point de vue du coût.

#oao2: Bien sûr, la pratique libérale aux Etats-Unis requiert de déposer pratiquement toute méthode commerciale en vue d'un brevetage. Ces méthodes commerciales sont toujours mise en oeuvre sur ordinateur (je n'ai jamais vu d'autre exemple). Cette mise en oeuvre sur ordinateur, dans la plupart des cas, donne l'opportunité de chercher un quelconque %(q:effet technique) ou une %(q:considération technique) ou un %(q:problème technique) etc. qui puisse aider à obtenir un brevet européen, à long terme. Quand on étudie un programme logiciel mis en oeuvre sur ordinateur d'une méthode commerciale il en ressort que dans la plupart des cas, le programme comprend au moins un aspect (ou caractéristique) qui pourrait le qualifier au sens des standards européens de %(q:technique). Il est conseillé à toute banque,  de nos jours, d'essayer de développer un portefeuille de brevets afin d'avoir au moins en main quelques arguments quand ils sont inquiétés par d'autres pour violation de brevets.

#dob: K. Beresford, World Patent Information 23 (2001) 253-263: Brevets logiciels européens, e-commerce et inventions de modèle commercial.

#adt: Un avocat en brevet britannique et européen, connu pour son manuel %(q:Brevetage logiciel au sens de la Convention des Brevets Européenne), relève le fossé entre la loi écrite et la pratique judiciaire concernant la brevetabilité en Europe aujourd'hui.  Cet extrait de l'article résume le fond de la pensée de Beresford:

#eoh: Il est communément admis que les inventions en relation avec les logiciels, le e-commerce et les modèles commerciaux sont fondamentalement non brevetables en Europe. En passant en revue la situation, cet article met en valeur le grand nombre d'inventions dans ce domaine qui ont en fait été brevetées par l'EOB ou les offices de brevets européens. [...] Il montre que, si une attention particulière est portée à la fois sur le fond et la forme des revendications et de la description, pour orienter le lecteur vers un problème technique et sa solution, une réelle protection est en fait très souvent obtenue.

#dtn: La %(tp|Commission Européenne|CEC) explique comment sa proposition de directive se propose de remplacer le mot %(q:brevetable) par le mot %(q:technique) et donc laisse le soin aux tribunaux de définir ce mot.  la CEC explique une récente affaire en justice de de l'EOB mais ajoute le mot %(q:matériel) dans un contexte où il est redondant et contribue seulement à l'engineering politique:

#tic: Il ne serait pas possible pour un texte juridique comme une Directive de définir précisément ce que l'on entend par %(q:technique), car la nature profonde du système de brevet est de protéger ce qui est nouveau, et par conséquent ce qui n'est pas encore connu. En pratique les tribunaux détermineront au cas par cas ce qui est ou n'est pas dans le périmètre de la définition. Quoi qu'il en soit, les récentes décisions des tribunaux ont indiqué qu'une contribution technique peut survenir s'il y a eu une amélioration dans la façon dont les processus sont exécutés ou dont les ressources sont utilisées dans un ordinateur (par exemple, une augmentation dans l'efficacité d'un processus matériel), ou si l'application de compétences techniques au-delà de la %(q:simple) programmation a été nécessaire pour arriver à l'invention.

#Woa: En 2002, le Tribunal Fédéral des Brevets Allemand (décision sur la Recherche d'Erreur, 26 mars 2002, BPatG 17 W (pat) 69/98) rejette un brevet de l'EOB sur une innovation non technique et relève qu ela doctrine de l'EOB sur %(q:la contribution technique) rend toutes les méthodes commerciales brevetables et est par conséquent en violation de la Convention des Brevets Européenne:

#sWn: A partir de là, il devient clair que la %(q:contribution technique) signifie %(e:amélioration banale de l'efficacité du traitement), comme on le trouve dans toute %(q:méthode d'entreprise mise en oeuvre sur ordinateur).  Cette %(q:contribution) n'est pas %(q:faite) par l'%(q:inventeur) mais construite par le juriste.  Un juriste en brevet qui ne peut pas construire un %(q:problème technique) à partir d'une méthode d'entreprise mériterait difficilement son salaire.  Et toute solution à un %(q:problème technique) entraînera nécessairement quelque amélioration dans l'efficacité de traitement.

#trP: Une fois le logiciel devenu brevetable, le mot %(q:technique) ne peut plus signifier %(q:concret et matériel) mais au mieux %(q:concret).  Cependant, puisque %(ta:toutes les avancées significatives dans le domaine du logiciel sont de nature abstraites), %(q:concret) dans ce contexte sous-entend %(q:banal).  Ce qui bien sûr ne signifie pas qu'une méthode d'entreprise doit être banale pour prétendre à la brevetabilité à l'EOB.  Par exemple, la solution au problème du représentant de commerce de Karmarkar est banale.  Cependant, son ingéniosité relève du domaine des mathématiques, et afin de prétendre à un brevet de l'EOB, l'ingéniosité en mathématiques est hors de propos, attendu que la nouveauté (comparée aux méthodes mathématiques connues) et la présence d'une %(tp|contribution technique|c'est-à-dire une %(e:amélioration banale dans l'efficacité de résolution d'un problème construit sur ordinateur)) sont des conditions nécessaires.

#epogl01t: Directives d'Examen sur les Avancées Inventives de l'EOB

#epogl01d: La section des Directives d'Examen de l'Office Européen des Brevets du 05-10-2001 qui traite de la façon d'examiner si une invention contient une %(q:avancée inventive).  Selon une récente doctrine de l'EOB qui ne figure pas dans les directives plus anciennes, cette %(q:avancée inventive par rapport à l'art premier le plus proche de l'invention) doit être interprêtée comme une résolution d'un %(q:problème technique).  Les conditions requises indépendantes pour qu'il y ait invention technique, comme enoncé dans l'article 52 EPC, sont donc abandonnées et fusionnées dans les conditions requises pour une avancée inventive (article 57 EPC).

#amph: la réponse standard qu'a envoyée McCarthy à ceux qui posaient des questions, d'après une FAQ de mai 2003.  Elle revendique que One Click n'aurait pas été brevetable avec sa proposition et qu'elle a fait un travail honnête.

#Wcs: La Commission Européenne (CEC) n'a jamais cité d'exemples de brevets afin d'expliquer ce qui serait brevetable et ce qui ne le serait pas, la seule exception étant %(q:Amazon One Click Shopping).  Cependant, bien que la CEC puisse faire croire au lecteur insouciant que Amazon One Click Shopping ne serait pas brevetable en Europe, le lecteur attentif reconnaîtra clairement dans cette affirmation qu'en fait il est brevetable:

#amazq: Q: Est-ce que le modèle de panier électronique Amazon %(q:one-click) serait brevetable avec la proposition de la Commission?

#amaza: A: L'Office Européen des Brevets doit prendre une décision relative à son application européenne, aussi il serait inapproprié de commenter si y il y a quoique ce soit de brevetable dans l'application comme un tout. Cependant, pour un brevet avec l'étendue de revendications qui a été autorisé aux Etats-Unis, il serait hautement improbable qu'il soit considéré comme étant une %(q:contribution technique) dans l'Union Européenne dans les termes proposés par la Directive.

#iWd: En d'autres mots: la méthode Amazon 1Click est un principe brevetable pour les standards de EOB/CEC mais, en fonction de l'art premier trouvé, les revendications seraient encore plus restreintes qu'aux Etats-Unis.  Il est à noter qu'aux Etats-Unis aussi les revendications englobent %(q:le modèle de commande) %(q:seulement) dans un contexte d'amélioration de la manière dont l'ordinateur est utilisé (diminution du nombre de clics de souris nécessaires = %(q:contribution technique)).  Comme avec la plupart des brevets d'entreprises américaines sur %(q:les méthodes d'entreprise mises en oeuvre sur ordinateur), il n'y a pas de raison de présumer que %(q:Amazon One Click Shopping) échouera au test de l'EOB sur la %(q:contribution technique dans les avancées inventives).

#noW: Arlene McCarthy refuse explicitement d'expliquer sa position en termes d'exemples de brevets, mais, quand on en vient à %(q:Amazon One Click Shopping), elle est prompte à dévier ce cette règle: %(orig:Alors qu'il n'est pas possible de dire si une application de brevet serait exclue de la directive, la directive, telle qu'amendée, ne permettrait pas la brevetabilité de la méthode Amazon's 'one-click'.)  Malheureusement McCarthy ne soutient pas cette assertion dans son raisonnement quant à sa proposition, particulièrement dans ses amendements, qui pourrait rendre Amazon 1Click non brevetable.  Arlene McCarthy a moins de difficultés que les experts en brevets de la Commission Européenne pour réconcilier la convenance politique avec la conscience de l'expert.  Cela est peut-être du en partie au fait que, comme elle le %(am:dit) elle-même, elle n'est %(q:pas une experte).

#oyn: Presque chaque mesure de la proposition de la CEC/JURI assure une brevetabilité illimitée, souvent formulée dans des termes qui signifient l'opposé de ce qu'ils semblent dire pour le lecteur inexpérimenté.  La JURI n'a ajouté que davantage d'extensions à la brevetabilité et une couche supplémentaire de dissimulation.  Citons quelques exemples.

#prov: Numéro

#text: Texte

#expl: Explication

#rvt: Afin d'être brevetable, une invention mise en oeuvre sur un ordinateur doit être susceptible d'application industrielle et de nouveauté et d'impliquer une avancée inventive.  Afin d'impliquer une avancée inventive, une invention mise en oeuvre sur un ordinateur doit être une contribution technique.

#nnn: %(ei:Dans l'usage récent de l'OEB, %(q:susceptible d'application industrielle) ne signifie rien de plus que %(q:d'une valeur commerciale potentielle).)  Cette condition creuse, ainsi que la nouveauté et %(tp|%(q:l'avancée inventive)|%(q:non-évidence)), est une partie basique de toute loi sur les brevets et par conséquent est redondante dans ce contexte.  Ce que cette mesure fait réellement c'est d'abolir la condition d'invention technique de la Commission Européenne du Brevet (EPC) en la déclarant d'une façon ou d'une autre (contrairement à la logique et la loi) comme découlant de la condition de non-évidence.

#bec: En conséquence, même si une invention mise en oeuvre sur un ordinateur appartient en vertue de sa vraie nature à un domaine de technologie, il est important de rendre clair que, si une invention ne représente pas une contribution technique à l'état de l'art, comme ce devrait être le cas, par exemple, si une contribution spécifique est dénuée de caractère technique, cette invention sera dénuée d'avancée inventive et donc ne serait pas brevetable.

#iee: Quand on juge si une avancée inventive est impliquée, il est courant d'appliquer l'approche problème - solution afin d'établir qu'il y a un problème technique à résoudre. S'il n'y a pas de problème technique, alors l'invention ne peut être considérée comme étant une contribution technique à l'état de l'art.

#Btu: Cette mesure, introduite par Arlene McCarthy, établit clairement la %(pb:doctrine du Système de Bénéfice de Pension de l'EOB) pour laquelle toute chose s'exécutant sur un ordinateur est une invention technique et la condition de l'%(q:avancée inventive) prétend hériter de la fonction de la condition %(q:d'invention technique) de la Commission Européenne du Brevet.  De plus, alors que l'%(e:invention technique) était synonyme de %(e:solution technique), à partir de maintenant la solution n'a plus besoin d'être %(q:technique).  Il suffit juste qu'un %(q:problème technique) soit identifié.

#tdh: McCarthy utilise ce nouveau postulat pour remplacer l'article 3 de la CEC, qui dit la même chose de façon plus lisible: %(bc:Les Etats Membres devront prendre des mesures pour qu'une invention mise en oeuvre sur ordinateur soit considérée comme appartenant à un domaine de technologie.)  Ceci déclencha des critiques bien prévisibles de toutes parts. McCarthy se fit vaguement l'écho de ces critiques %(bc:Cet article n'est pas nécessaire et n'éclaire son étendue.  Il serait difficile à mettre en oeuvre, et pourrait amener à des résultats imprévisibles.  Il pourrait être interprêté comme étendant la portée de la protection par brevet.) et propose la suppression de l'article 3, seulement pour y insérer les meusres critiquées dans une formaulation moins claire dans le postulat 12.

#ttt: Toutefois, la seule mise en oeuvre d'une méthode non brevetable autrement sur un dispositif tel qu'un ordinateur n'est pas en soi suffisante pour garantir à une découverte qu'une contribution technique est présente.  En conséquence, une méthode d'entreprise mise en oeuvre sur ordinateur ou une autre méthode dans laquelle la seule contribution à l'état de l'art n'est pas technique ne peut pas constituer une invention brevetable.

#sif: Le lecteur inexpérimenté peut voir cela comme une limitation de la brevetabilité.  Le lecteur expérimenté comprend: une méthode d'entreprise mise en oeuvre sur ordinateur constitue une invention brevetable, en ce qu'elle forme une solution à un %(q:problème technique).

#nei: De plus, un algorithme est fondamentalement non technique et donc ne peut constituer une invention technique.  Néanmoins, une méthode impliquant l'utilisation d'un algorithme pourrait être brevetable en ce que la méthode est utilisée pour résoudre un problème technique.  Cependant, tout brevet autorisé pour de telle méthode ne monopoliserait pas l'algorithme lui-même ou son usage dans des contextes non prévus par le brevet.

#smW: Pour le lecteur insouciant, cela suggère que les algorithmes ne sont pas brevetables.  Pour le lecteur attentif, cela dit clairement que les algorithmes peuvent être brevetables simplement en les présentant comme solution à un problème d'amélioration de l'efficacité de traitement, donc cela monopolise l'usage de l'algorithme sur un ordinateur universel dans tous les contextes où il serait d'une justification pratique.

#eak: La contribution technique doit être évaluée en regard de la différence entre l'étendue de la revendication de brevet considérée comme un tout, dont les éléments peuvent comprendre à la fois des caractéristiques techniques et non techniques, et l'état de l'art.

#sdf: La contribution technique devra être évaluée en considérant l'état de l'art et l'étendue de la revendication de brevet considérée comme un tout, ce qui doit inclure les caractéristiques techniques, indépendamment du fait que de telles caractéristiques soient accompagnées ou non de caractéristiques non techniques.

#oao: La CEC dit carrément que la %(q:contribution technique) (c'est-à-dire la solution au %(q:problème technique)) peut se composer seulement de caractéristiques non techniques.  Ceci déclencha une grande vague de critiques.  La JURI l'éluda en ne disant strictement rien sur la manière d'évaluer la %(q:contribution).  Cependant, indirectement, la version de la JURI autorise encore la conclusion que la solution au %(q:problème technique) peut se composer de caractéristiques non techniques seulement.

#hWW: Une invention mise en oeuvre sur ordinateur ne doit pas être vue comme étant une contribution technique simplement parce qu'elle implique l'usage d'un ordinateur, d'un réseau ou d'un autre dispositif programmable.  En conséquence, les inventions impliquant des programmes d'ordinateurs qui mettent en oeuvre des méthodes d'entreprises, mathémathiques ou d'autres méthodes et qui ne produisent aucun effet technique au-delà des interactions physiques normales entre un programme et un ordinateur, un réseau ou un autre dispositif programmable sur lequel il est exécuté ne sera pas brevetable.

#oIW: Toutes les innovations logicielles peuvent être revendiquées en termes de %(q:problèmes techniques) ou d'%(q:effets techniques au-delà des interactions physiques normales entre un programme et un ordinateur).  Cette exclusion ne signifie strictement rien.  La %(q:justification) à nouveau suggère que les méthodes d'entreprise sont brevetables, elle se rédige en termes de %(q:problème technique): %(bc:Cela rend aussi clair que la seule mise en oeuvre sur ordinateur d'une méthode d'entreprise n'est pas une invention brevetable.)  Cette phrase contredit le texte du postulat 12 de la JURI, selon lequel les mises en oeuvre sur ordinateur %(q:en vertue de leur vraie nature) sont qualifiées d'inventions techniques (mais, en plus, elles nécessitent %(q:de faire une contribution technique dans leur avancée inventive)).

#nWc: Une revendication de programme sur ordinateur, sur lui-même, comme porteuse ou comme signal, devra être autorisé seulement si un tel programme, quand il est chargé ou exécuté sur un ordinateur, un réseau d'ordinateur ou un autre dispositif programmable, met en oeuvre un produit ou fait ressortir un processus brevetable comme défini dans les articles 4 et 4a.

#Wra: Les revendications de programmes ne sont pas autorisées du tout dans la proposition de la CEC.  L'adverbe %(q:seulement) est trompeur, car %(ol|cet amendement ne constitue pas une limitation mais une extension des formes de revendications autorisées|Les articles 4 et 4a n'excluent aucun sujet de la brevetabilité|Personne n'a jamais suggéré ou rêvé que le fait de revendiquer un processus en tant que programme ou vice versa étendait ou réduisait la portée des matières sujettes à brevetage.)  La plupart du texte de cet article ne sert pas à clarifier mais à dissimuler.

#rti: Un rapport précis et impartial sur la situation des brevets à partir d'une perspective australienne.

#one: Cet article contient une comparaison entre les pratiques de l'EOB, du JPO et de l'USPTO concernant le brevetage de méthodes abstraites et trouve que l'EOB est le plus généreux de tous pour autoriser de tels brevets.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-tech ;
# txtlang: fr ;
# multlin: t ;
# End: ;

