\begin{subdocument}{eubsa-kinv}{Qu'est-ce qu'une ``Invention mise en oeuvre par ordinateur'' ?}{http://swpat.ffii.org/papiers/eubsa-swpat0202/kinv/index.fr.html}{Groupe de travail\\swpatag@ffii.org\\version fran\c{c}aise 2003/12/18 par Michèle Garoche\footnote{http://micmacfr.homeunix.org}}{Une machine \`{a} laver avec ordinateur embarqu\'{e} ?  Ou un programme  g\'{e}n\'{e}rique de traitement de donn\'{e}es ?  Dans leurs discours publics, les  partisans de la proposition de directive de l'UE sur la brevetabilit\'{e}  des logiciels disent qu'ils ne veulent pas de brevets sur de ``purs  logiciels'' mais seulement sur des ``inventions mises en oeuvre par  ordinateur'', entendant signifier par l\`{a} sur ``des machines \`{a} laver et  des t\'{e}l\'{e}phones mobiles''. L'article 2 de la proposition elle-m\^{e}me dit  tout autre chose, tout comme les annales et les pratiques de l'OEB  accordant des brevets sur les ``inventions mises en oeuvre par  ordinateur''.}
\begin{sect}{intro}{introduction}
\begin{center}
\parbox[c]{.4\textwidth}{\centerline{\mbox{\includegraphics{kinv}}}}
\hfill
\parbox[c]{.5\textwidth}{\begin{center}
{\Large {\bf Est-ce qu'une machine a laver peut \^{e}tre une ``invention mise en oevre par ordinateur''?\\
Seulement les Programmes sont ``mis en oevre par ordinateur''.\\
Selon l'Art 52 CBE\footnote{http://swpat.ffii.org/analyse/epc52/index.en.html} les programmes pour ordinateurs ne sont pas des inventions au sense de la loi des brevets.}}
\end{center}}
\end{center}

The term ``computer-implemented invention'' is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in Appendix 6\footnote{http://swpat.ffii.org/papiers/epo-tws-app6/index.en.html} of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this ``Appendix 6''.  The term ``computer-implemented invention'' is a programmatic statement.  It implies that calculation rules framed in terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.

Programmers speak about ``implementing a specification''.  A patent claim usually specifies a sequence of events (i.e. a program) which could be \emph{implemented} by a programmer or by an engineer.  ``Implementation'' in this context means ``working it out in detail so that it can run without errors''.  The implementation requires human work.  It is never done by a computer, and in fact computers will usually not play any role in it at all, beyond that of an auxiliary tool for checking the validity of logical concepts, similar to pencil and paper.

Moreover, the programmer would not say that she is implementing an ``invention'', no matter how new and admirable the specification is.  Rather she might be implementing an algorithm, an idea or, more often, a complex combination of such abstract elements.  Real advances in the art of programming are generally of abstract nature and therefore would usually not be referred to as ``inventions'' by the person skilled in the art.

A term like ``computer-executed innovations'' or simply ``data processing innovations'' would have been less conducive to misunderstanding.  However misunderstanding was exactly what the proponents wanted.

The proponents of patents on ``computer-implemented inventions'' sometimes say that it refers only to ``washing machines, mobile phones, intelligent household appliances ...'' and ``not computer programs as such''.  However this is untrue.  The term as defined in the EPO's Trilateral Appendix 6 and in the Commission's Directive Proposal refers to nothing but data manipulation processes running on general-purpose computing equipment.  Even if the extremely rare cases where a conventional washing machine is involved in a patent claim which EPO/CEC would subsume under ``computer-implemented inventions'', the gist of the ``invention'' will lie in data processing, not in applying heat and reactants to clothes.  Otherwise this would, according to the EPO/CEC definitions, no longer be a ``computer-implemented invention'' but rather an ordinary technical invention (which does not match the CII definition, because its ``prima facie novel'' aspect is unrelated to computers).

The European Parliament has redefined the term ``computer-implemented invention'' in such a way that general-purpose data processing does not qualify while a washing machine, where data processing has only an auxiliary function and is not constitutive for the invention, would qualify.  This amendment is fiercely opposed by the European Commission and by all those who earlier claimed that they want only washing machines and the like but not programs as such to be patentable.

While the redefinition of the European Parliament constitutes a clever way of correcting a central flaw of the proposed directive, it does not make the text much clearer.  The misleading term ``computer-implemented invention'' continues to be used, and it will continue to mislead all those people who have not carefully read and memorised the EP definition.
\end{sect}

\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf OEB 2000-05-19: Examen des Applications pour M\'{e}thodes d'Affaires\footnote{http://swpat.ffii.org/papiers/epo-tws-app6/index.en.html}}}

\begin{quote}
Document de l'OEB, datant de 2000 et introduisant le terme  d'``invention mise en oeuvre par ordinateur'' pour l\'{e}gitimer la  d\'{e}livrance de brevets sur des programmes d'ordinateur ou sur des  m\'{e}thodes dans le domaine des activit\'{e}s \'{e}conomiques, en compl\`{e}te  contradiction avec la lettre et l'esprit de la loi, alignant ainsi les  pratiques juridiques europ\'{e}ennes sur celles des Offices de Brevets  am\'{e}ricains et japonais.  L'OEB \'{e}crit :

\begin{quote}
{\it L'expression ``invention mise en oeuvre par ordinateur'' est  pr\'{e}vue pour couvrir les revendications d\'{e}signant des ordinateurs, des  r\'{e}seaux d'ordinateurs ou d'autres appareils num\'{e}riques programmables  conventionnels avec lesquels, \`{a} premi\`{e}re vue, l'apport en nouveaut\'{e} de  l'invention revendiqu\'{e}e est r\'{e}alis\'{e} par le biais d'un ou plusieurs  programmes nouveaux.}

{\it [\dots]}

{\it La seule raison apparente de faire une distinction dans la  d\'{e}cision entre des ``effets techniques'' et des ``effets encore plus  techniques'' \'{e}tait la pr\'{e}sence de ``programmes d'ordinateur'' dans la  liste des exclusions de l'Article 52(2) de la CEB. Si, comme c'est \`{a}  pr\'{e}voir, cet \'{e}l\'{e}ment est supprim\'{e} de la liste par la Conf\'{e}rence  Diplomatique, une telle distinction n'aura plus aucun fondement. On  peut en conclure que la Grande Chambre des Recours aurait pr\'{e}f\'{e}r\'{e}  pouvoir dire qu'aucune invention mise en oeuvre par ordinateur n'\'{e}tait  exclue de la brevetabilit\'{e} par les clauses des Articles 52(2) et (3) de  l'OEB.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf CCE \& BSA 2002-02-20: Proposition pour rendre toutes les id\'{e}es utiles brevetables\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
L'Article 2 de la proposition de la Commission d\'{e}finit une  ``invention mise en oeuvre par ordinateur'' conform\'{e}ment aux \'{e}l\'{e}ments  de l'Annexe 6 :

\begin{quote}
{\it Une ``Invention mise en oeuvre par ordinateur'' d\'{e}signe  toute invention dont l'ex\'{e}cution implique l'utilisation d'un  ordinateur, d'un r\'{e}seau d'ordinateurs ou d'un autre appareil  programmable et pr\'{e}sentant une ou plusieurs caract\'{e}ristiques \`{a} premi\`{e}re  vue nouvelles qui sont r\'{e}alis\'{e}es totalement ou en partie au moyen d'un  ou de plusieurs programmes d'ordinateurs.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Amendement pr\'{e}sentant une contre-proposition \`{a} la proposition de  titre de la directive\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/prop/index.fr.html\#tit}}}

\begin{quote}
Explique \'{e}galement ce qui ne convient pas dans l'expression  ``invention mise en oeuvre par ordinateur''.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 1990: T 0022/85\footnote{http://legal.european-patent-office.org/dg3/biblio/t850022ep1.htm}}}

\begin{quote}
Cette d\'{e}cision de l'OEB datant de 1984 montre que  l'interpr\'{e}tation ant\'{e}rieure de l'expression ``programme d'ordinateur'' est exactement celle appliqu\'{e}e en 2000 au nouveau terme ``invention  mise en oeuvre par ordinateur''.
\end{quote}
\filbreak

\item
{\bf {\bf Ce qu'est une ``Invention mise en oeuvre par ordinateur''\footnote{http://elis.ugent.be/~jmaebe/swpat/cii.html}}}

\begin{quote}
Articles p\'{e}dagogiques \'{e}crits par Jonas Maebe en septembre 2003
\end{quote}
\filbreak

\item
{\bf {\bf DE Justice Ministry 03-09-26: Only ABS Inventions, not Software as Such\footnote{http://swpat.ffii.org/papiers/europarl0309/bmj030926/index.de.html}}}

\begin{quote}
In a statement on the European Parliament's Vote, the patent law department of the German Ministry of Justice (BMJ) evades the question of whether the EP drew the right kind of limits.  Instead the BMJ says that the term ``software patents'' is misleading because ``pure source codes are not patentable'' and only ``computer-implemented inventions'' are patentable.  As an example of a ``computer-implemented invention'', the BMJ mentions the ``anti-blocking system''.  The BMJ does not answer the question of whether a system and method involving only general-purpose computing equipment is a ``computer-implemented invention''.  But it becomes clear from the text that they want nothing to be excluded from patentability and they imply that the EPC should be interpreted in this way.  However they fail to clearly say that, thereby misleading less attentive readers.  The BMJ's only contribution to the debate has so far consisted in attempts to impose misleading terminology, so as to be able to justify whatever the patent judiciary may chose to do.

voir aussi PE 2003-09-24: Directive Brevets Logiciels Amend\'{e}es\footnote{http://swpat.ffii.org/papiers/europarl0309/index.fr.html},Minist\`{e}re F\'{e}d\'{e}ral de Justice et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/bmj/index.de.html} et Conseil de l'Union Europ\'{e}enne et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/consilium/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Amendment 36=42=117\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/index.fr.html\#Am36}}}

\begin{quote}
The European Parliament voted for a redefinition of the ``computer-implemented invention''.  Thereby it removed the damaging effects of this term within the directive.  However the term ``computer-implemented invention'' is still in the title of the directive and will therefore continue to mislead all those people who have not taken the time to carefully read the redefinition.

\begin{quote}
{\it ``invention mise en oeuvre par ordinateur'' d\'{e}signe toute invention au sens de la Convention sur le brevet europ\'{e}en dont l'ex\'{e}cution implique l'utilisation d'un ordinateur, d'un r\'{e}seau informatique ou d'un autre appareil programmable et pr\'{e}sentant dans sa mise en oeuvre une ou plusieurs caract\'{e}ristiques non techniques qui sont r\'{e}alis\'{e}es totalement ou en partie par un ou plusieurs programmes d'ordinateurs, en plus des caract\'{e}ristiques techniques que toute invention doit apporter;}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Autodesk Testimony against Software Patents\footnote{http://www.base.com/software-patents/statements/autodesk.html}}}

\begin{quote}
In his testimony at the USPTO 1994 hearings, Jim Warren, board member of the US software giant Autodesk Inc, objected against the patent lawyer newspeak term ``software related inventions'', which is still not quite as biased as ``computer-implemented inventions'':

\begin{quote}
{\it C'est pourquoi, je proteste respectueusement contre le titre donn\'{e}  \`{a} ces auditions -- ``Inventions Relatives aux Logiciels'' -- puisque  vous n'\^{e}tes pas essentiellement concern\'{e}s par des gadgets contr\^{o}l\'{e}s par  logiciel. Le titre illustre un parti pris inappropri\'{e} et pr\^{e}tant  s\'{e}rieusement \`{a} confusion. En fait, en plus d'un quart de si\`{e}cle en tant  que professionnel des ordinateurs et observateur et \'{e}crivain dans cette  industrie, je ne me souviens pas avoir jamais entendu ou lu une telle  phrase -- except\'{e} dans le contexte des revendications juridiques pour le  monopole, o\`{u} les demandeurs essayaient de d\'{e}naturer la tradition de  brevetage des \'{e}quipements afin de monopoliser l'ex\'{e}cution des processus  intellectuels.}
\end{quote}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Questions, choses \`{a} faire, comment vous pouvez aider}
\begin{itemize}
\item
{\bf {\bf Comment vous pouvez nous aider a mettre fin au cauchemare des brevets logiciels\footnote{http://swpat.ffii.org/groupe/gunka/index.fr.html}}}
\filbreak

\item
{\bf {\bf Nominate the ``computer-impemented invention'' for linguistic (un)beauty contests}}

\begin{quote}
Various language associations regularly call for public bidding for ``UnWord of the Year'' or similar.  Is there any other high-level political term which is so misleading and so clearly implies a break of law as the term ``computer-implemented invention''?  Is there any other political term which is closely connected to attempts at deceiving the legislature, negating fundamental rights and sidestepping democratic processes in Europe\footnote{http://swpat.ffii.org/papiers/europarl0309/reag/index.fr.html}?  If not, why isn't ``Computer-Implemented Invention'' the top candidate of all those linguistic beauty contests?

voir Unwort des Jahres: ``Computer-Implementierte Erfindung''\footnote{http://swpat.ffii.org/analyse/unwort/index.de.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

