\begin{subdocument}{eubsa-kinv}{What is a Computer-Implemented Invention?}{http://swpat.ffii.org/papers/eubsa-swpat0202/kinv/index.en.html}{Workgroup\\swpatag@ffii.org\\english version 2003/12/18 by PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{A washing machine with an embedded computer?  Or a general-purpose data processing program?  In their public discourse, the promoters of the EU software patent directive proposal say they do not want patents on ``pure software'' but only on ``computer-implemented inventions'', by which they say they mean ``washing machines and mobile phones''.  Art 2 of the proposal itself however says otherwise, as does the history and practise of granting patents for ``computer-implemented inventions'' at the EPO.}
\begin{sect}{intro}{introduction}
\begin{center}
\parbox[c]{.4\textwidth}{\centerline{\mbox{\includegraphics{kinv}}}}
\hfill
\parbox[c]{.5\textwidth}{\begin{center}
{\Large {\bf Can a Washing Machine be a ``Computer-Implemented Invention''?\\
Only Computer Programs are ``Computer-Implemented''.\\
According to Art 52 EPC\footnote{http://swpat.ffii.org/analysis/epc52/index.en.html}, programs for computers are not inventions in the sense of patent law.}}
\end{center}}
\end{center}

The term ``computer-implemented invention'' is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in Appendix 6\footnote{http://swpat.ffii.org/papers/epo-tws-app6/index.en.html} of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission (CEC) directive proposal is based on wordings from this ``Appendix 6''.  The term ``computer-implemented invention'' is a programmatic statement.  It implies that calculation rules framed in terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.

Programmers speak about ``implementing a specification''.  A patent claim usually specifies a sequence of events (i.e. a program) which could be \emph{implemented} by a programmer or by an engineer.  ``Implementation'' in this context means ``working it out in detail so that it can run without errors''.  The implementation requires human work.  It is never done by a computer, and in fact computers will usually not come into play during the implementation phase.  Rather, they will be used for executing the final result, and this result will in principle be independent of the computer architecture.

Moreover, the programmer would not say that she is implementing an ``invention'', no matter how new and admirable the specification is.  Rather she might be implementing an algorithm, an idea or, more often, a complex combination of such abstract elements.  Real advances in the art of programming are generally of abstract nature and therefore would usually not be referred to as ``inventions'' by the person skilled in the art.

A term like ``computer-executed schemes'' or ``software innovations'' would have been less conducive to misunderstanding.  Thereby it would however not have served the aims of the proponents.

The proponents of patents on ``computer-implemented inventions'' regularly claimed that their term refers ``only to washing machines, mobile phones, intelligent household appliances ...'' and ``not to software as such''.  Unfortunately this claim is at odds with the definition wording used by the proponents themselves.  The term ``computer-implemented invention'' as defined in the EPO's Trilateral Appendix 6 and in the Commission's Directive Proposal refers to nothing but data processing programs running on general-purpose computing equipment.  Even if the extremely rare cases where some additional equipment is involved in an EPO-granted patent on what they would call a ``computer-implemented inventions'', the gist of the ``invention'' will lie in data processing, not in applying heat and reactants to clothes.  Otherwise this would, according to the EPO/CEC definitions, no longer be a ``computer-implemented invention'' but rather an ordinary technical invention.

The European Parliament has redefined the term ``computer-implemented invention'' in such a way that general-purpose data processing does not qualify while a washing machine, where data processing has only an auxiliary function and is not constitutive for the invention, would qualify.  This amendment is fiercely opposed by the European Commission and by all those who earlier claimed that they want only washing machines and the like but ``not software as such'' to be patentable.

While the redefinition of the European Parliament constitutes a clever way of correcting a central flaw of the proposed directive, it does not make the text much clearer.  The misleading term ``computer-implemented invention'' continues to be used, and it will continue to mislead all those people who have not carefully read and internalised the definition.  Thereby it would also embody the supremacy of a small group of newspeak-creating lawyers over the basic rules by which programmers and engineers have to abid.  Although the Parliament could be credited with ending the invasion of the patent lawyers into the domain of the software engineers, it would leave a symbol of that invasion intact.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf EPO 2000/05/19: Examination of ``business method'' applications\footnote{http://swpat.ffii.org/papers/epo-tws-app6/index.en.html}}}

\begin{quote}
The EPO document of 2000 which introduced the term ``computer-implemented invention'', in order to legitimate the granting of patents on programs for computers and methods for doing business, in contradiction of the letter and spirit of the written law, so as to bring European juridical practise in line with the practise of the patent offices of the USA and Japan.  The EPO writes:

\begin{quote}
{\it The expression ``computer-implemented inventions'' is intended to cover claims which specify computers, computer networks or other conventional programmable digital apparatus whereby prima facie the novel features of the claimed invention are realised by means of a new program or programs.}

{\it [\dots]}

{\it The only apparent reason for distinguishing ``technical effect'' from ``further technical effect'' in the decision was because of the presence of ``programs for computers'' in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
Article 2 of the Commission proposal defines ``computer-impemented invention'' along the lines of Appendix 6:

\begin{quote}
{\it ``computer-implemented invention'' means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Counterproposal title amendment\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html\#tit}}}

\begin{quote}
Also explains what is wrong with the word C.I.I.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 1990: T 0022/85\footnote{http://legal.european-patent-office.org/dg3/biblio/t850022ep1.htm}}}

\begin{quote}
This EPO decision of 1984 shows that previously ``program for computers'' was interpreted to mean exactly what in 2000 the new term ``computer-implemented invention'' was defined to mean.
\end{quote}
\filbreak

\item
{\bf {\bf What is a ``Computer-Implemented Invention''\footnote{http://elis.ugent.be/~jmaebe/swpat/cii.html}}}

\begin{quote}
Educative material written by Jonas Maebe in September 2003
\end{quote}
\filbreak

\item
{\bf {\bf DE Justice Ministry 03-09-26: Only ABS Inventions, not Software as Such\footnote{http://swpat.ffii.org/papers/europarl0309/bmj030926/index.de.html}}}

\begin{quote}
In a statement on the European Parliament's Vote, the patent law department of the German Ministry of Justice (BMJ) evades the question of whether the EP drew the right kind of limits.  Instead the BMJ says that the term ``software patents'' is misleading because ``pure source codes are not patentable'' and only ``computer-implemented inventions'' are patentable.  As an example of a ``computer-implemented invention'', the BMJ mentions the ``anti-blocking system''.  The BMJ does not answer the question of whether a system and method involving only general-purpose computing equipment is a ``computer-implemented invention''.  But it becomes clear from the text that they want nothing to be excluded from patentability and they imply that the EPC should be interpreted in this way.  However they fail to clearly say that, thereby misleading less attentive readers.  The BMJ's only contribution to the debate has so far consisted in attempts to impose misleading terminology, so as to be able to justify whatever the patent judiciary may chose to do.

see also Europarl 2003-09-24: Amended Software Patent Directive\footnote{http://swpat.ffii.org/papers/europarl0309/index.en.html}, Minist\`{e}re F\'{e}d\'{e}ral de Justice et Brevets Logiciels\footnote{http://swpat.ffii.org/players/bmj/index.de.html} and Council of the European Union and Software Patents\footnote{http://swpat.ffii.org/players/consilium/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Amendment 36=42=117\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am36}}}

\begin{quote}
The European Parliament voted for a redefinition of the ``computer-implemented invention''.  Thereby it removed the damaging effects of this term within the directive.  However the term ``computer-implemented invention'' is still in the title of the directive and will therefore continue to mislead all those people who have not taken the time to carefully read the redefinition.

\begin{quote}
{\it ``computer-implemented invention'' means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Autodesk Testimony against Software Patents\footnote{http://www.base.com/software-patents/statements/autodesk.html}}}

\begin{quote}
In his testimony at the USPTO 1994 hearings, Jim Warren, board member of the US software giant Autodesk Inc, objected against the patent lawyer newspeak term ``software related inventions'', which is still not quite as biased as ``computer-implemented inventions'':

\begin{quote}
{\it Thus, I respectfully object to the title for these hearings -- ``Software-Related Inventions'' -- since you are not primarily concerned with gadgets that are controlled by software. The title illustrates an inappropriate and seriously-misleading bias. In fact, in more than a quarter-century as a computer professional and observer and writer in this industry, I don't recall ever hearing or reading such a phrase -- except in the context of legalistic claims for monopoly, where the claimants were trying to twist the tradition of patenting devices in order to monopolize the execution of intellectual processes.}
\end{quote}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf How you can help us end the software patent nightmare\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}
\filbreak

\item
{\bf {\bf Nominate the ``computer-impemented invention'' for linguistic (un)beauty contests}}

\begin{quote}
Various language associations regularly call for public bidding for ``UnWord of the Year'' or similar.  Is there any other high-level political term which is so misleading and so clearly implies a break of law as the term ``computer-implemented invention''?  Is there any other political term which is closely connected to attempts at deceiving the legislature, negating fundamental rights and sidestepping democratic processes in Europe\footnote{http://swpat.ffii.org/papers/europarl0309/reag/index.en.html}?  If not, why isn't ``Computer-Implemented Invention'' the top candidate of all those linguistic beauty contests?

see Unwort des Jahres: ``Computer-Implementierte Erfindung''\footnote{http://swpat.ffii.org/analysis/unwort/index.de.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

