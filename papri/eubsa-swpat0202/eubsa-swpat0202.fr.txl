<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CCE & BSA 2002-02-20: Proposition pour rendre toutes les idées utiles
brevetables

#descr: La Commission Européenne (CCE) propose d'entériner la pratique de
l'Office Européen des Brevets (OEB), lequel a illégalement accordé
plus que 30000 brevets sur des règles d'organisation et de calcul
(programmes d'ordinateur) en tant que tels depuis 1986.  Si le
parlement européen adopte cette proposition de loi, il deviendra
désormais impossible pour les Cours européennes de contester la
légalité de tels brevets, et la brevetabilité laxiste à l'américaine
s'imposera définitivement en Europe.  %(q:Mais attendez une minute, la
CCE ne dit pas cela dans son communiqué de presse!) vous voulez dire,
n'est-ce pas?  Vous avez raison!  Pour savoir ce que la CCE dit en
réalité, il vous faut lire la proposition elle-même.  Mais faites
attention: cette proposition est écrite dans un langage ésotérique de
l'Office Européen des Brevets (OEB), dans lequel les mots ordinaires
signifient souvent le contraire de ce que vous attendriez.  Aussi vous
devez sauter toute suite à la fin du texte.  La partie pertinente du
propos se trouve dans les 3 dernières  pages, qui sont précédées par
une longue et confuse introduction qui mélange le langage de l'OEB
avec des confessions de foi sur l'importance des brevets et du
logiciel propriétaire, en insinuant une corrélation entre les deux.  
Ce texte ignore les opinions d'à peu près tous les developpeurs de
logiciel respectés, en citant comme sa seule source d'information sur
la réalité du logiciel deux études non-publiées de BSA et amis
(alliance pour l'application du droit d'auteur dominée par Microsoft
et autres grandes entreprises américaines) sur l'importance du
logiciel propriétaire.  Ces études ne traîtent même pas du thème
brevet!  L'introduction et la proposition elle-même a apparemment été
écrite pour la CCE par un employé de BSA.  Ci-dessous nous citons le
propos complet, en ajoutant des preuves sur le rôle de BSA aussi
qu'une analyse du contenu, basée sur une comparaison en tableau des
versions de BSA et de la CCE avec une version décryptée, basée sur la
Convention sur le Brevet Européen et des doctrines y étant liées qui
se trouvent dans les règles d'examen de l'OEB de 1978 aussi que dans
la juridiction de l'époque.  Cette version OEB vous aidera à apprécier
la clarté et la sagesse des règles de brevetabilité de la loi
actuelle, qui a fait l'objet d'intenses efforts de détournement par
l'OEB et le cercle d'amis des juristes du brevet qui dominent le
dossier au sein la Commission Européenne.

#TCp: Tableau comparatif des Versions CCE et BSA avec Contre-Proposition
dans l'Esprit de la CBE

#HCd: Nous présentons ici une édition critique du texte, avec des
annotations et une comparaison en tableau du papier initial et final
de la CCE/BSA et une version FFII ajoutée qui met en lumière ce qui
est mauvais avec la version CCE/BSA et comment elle pourrait être
réécrite d'une façon cohérente et adéquate.  Nous avons souligné en
gras quelques différences.

#Pad: Proposal to make all useful ideas patentable, based on a draft from
BSA, released by the European Commission on 2002-02-20

#Uma: Upon Release of its Software Patentability Directive Proposal (based
on a draft by BSA), the European Commission lies to the press and to
the world about the contents of this Directive Proposal, trying to
create the impression that this directive proposal excludes patents on
business methods and software as such.

#Aey: An text by the European Commission's (CEC) software patentability law
drafters, designed to (dis)inform journalists and the general public
about what is at stake and to soothe widespread fears that the CEC
might be legalising unwanted software and business method patents. 
The text tries to achieve this by using EPO/UKPO Patent Newspeak, in
which normal phrases may have two radically different meanings, one
for consupmtion by politicians, journalists and citizens, i.e. the
readers of this FAQ, and another as understood by patent
professionals.

#Jro: JURI working documents

#WBt: Working documents of the Legal Affairs Commission of the European
Parliament about the CEC/BSA software patentability proposal of
2002-02-20, published 2002-06-19, inititially consisting of a short
report by MEP Arlene McCarthy and a study ordered by the European
Commission.

#Pi1: Lenz 2002-03-01: Sinking the Software Patent Proposal

#lpa: Karl-Friedrich Lenz, professor of European Law, lists some legal and
constitutional arguments to explain why the CEC/BSA proposal is a
legal and political scandal, starting from the fact that the European
Commission is using %(q:harmonisation) and %(q:clarification) merely
as pretexts to declare itself competent for promoting an unspeakable
political agenda which does not fall in the Commission's competentce.

#DxW: Datamonitor 2000-09: Packaged Software Industry in Europe

#ArW2: A rarely cited and largely unknown study by a company called
Datamonitor about the importance of proprietary software as a creator
of jobs in Europe.  The study claims that proprietary software will
create 1/2 million new jobs in the next few years.  Praises Ireland as
a tiger state which is profiting from this job miracle thanks to its
low tax rates.  The original of this study seems to be inaccessible on
the Net.  Various summaries and references have been published on
Microsoft's website in the context of Microsoft's lobbying work.  In
2002 this study found its way into the advocacy preface of the
European Commission's software patentability proposal.  This was
apparently due to the influence of BSA in the drafting work.  The
study does not deal with the subject of software patents.  It
correctly states that the biggest asset of software companies is their
manpower, i.e. their ability to manage complex copyrighted works and
quickly turn out nifty software rather than in their patents or their
ability to invent a mousetrap.

#MWw: Microsoft: The growth of the packaged software industry in Norway

#AWi2: An example of Microsoft's use of the Datamonitor Study for political
persuasion efforts in Norway.

#Aoh: A controversial debate between two people about the directive proposal
and the FFII criticism.  The %(q:anonymous coward) is Erik Josefsson
from SSLUG, his opponent is a patent lawyer from Philips who maintains
a %(im:website which argues in favor of the EPO's recent
interpretation of the EPC) (and thus basically also of the directive
proposal).

#pnc: press release of Linux-Verband, an association of Linux companies and
users in Germany

#Mmn: Mingorance denies our allegations that he wrote the proposal, arguing
that he would have written the %(q:form of claims) section
differently.  We think that the decision to not follow the EPO on the
claim form question was indeed taken by the CEC.  We do not doubt that
the CEC played an important role in shaping the proposal.  But so did
Mingorance.

#AWg: An journalist reports that he just phonecalled the European Commission
to find out whether the BSA version that Eurolinux proposed was really
the version that the CEC was going to publish that day.  He was told
by the Commission's press speaker that the version which Eurolinux
published was not theirs but one %(q:from the industry).  Several
French journalists who called on the same morning received the same
answer.  One even received the answer %(q:no, that draft is not from
us but from BSA).

#AvA: A fairly comprehensive account of the BSA/CEC proposal scandal

#CcB: CEC Press Release inconsistent with CEC/BSA directive content

#Eui: Eurolinux Warning to journalists issued on 2002-02-20 shortly after
publication of CEC/BSA directive proposal

#Rsr: Robin Webb (UK PTO & Gov't) 2002-02-20: proposes to remove all limits
to patentability and to rewrite Art 52 EPC so as to reflect EPO
practise

#Tla: The UK PTO conducted its own consultation, which showed an
overwhelming wish of software professionals to be free of software
patents.  But the UK PTO, speaking in the name of the UK government,
reinterprets this as a legitimation to remove all limits on
patentability by modifying Art 52 EPC at the Diplomatic Conference in
June 2002.  The %(pr:proposal) has one argument going for it:  while
rendering Art 52 meaningless, it at least brings clarity.  It
describes in fairly straightforward (and yet still sufficiently
deceptive) terms what the EPO is doing and what the patent movement
wants.  This paper was presented by the UKTPO on the same day as the
EU-BSA Directive Proposal.  The UKPTO had held a parallel consultation
exercise in Great Britain.  Its exercise ignored public opinion using
the same double-talk about %(q:technical contribution) and by
selective choice of examples made sure that this talk is misunderstood
by the public.  Yet the UKPTO has shown the Commission that even
massive fraud and %(e:theft of intellectual property with with a
further legal effect), jointly committed by the EPO, the CEC and the
UKPTO against the european software industry and the european public,
can be carried out in a graceful and mannered fashion.

#Ftr: article français qui explique que ce propos de directive n'apporte pas
de clarté

#Jti: Les associations européennes du partagiciel dénoncent le propos de
directive

#Gyu: députée allemande dans l'EuroParl dénonce ce projét de directive et
quelques autres initiatives similaires de la CCE.

#Prc: références nombreuses a des articles de presse notamment française

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-swpat0202 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

