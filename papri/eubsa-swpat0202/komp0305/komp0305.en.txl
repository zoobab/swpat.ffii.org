<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#rtP: 30 Computer Scientists 2003/05: Petition against Software Patent
Directive

#Wac: explain why this directive is economically unjustifiable and ethically
scandalous

#iEr: Petition to the European Parliament

#ift: From 1997, the European Patent Office has initiated and generalised
the granting of patents for algorithms, software ideas, data
structures and information processing methods. In a directive proposal
on 20 February 2002, the European Commission proposed to officialise
this abuse, presenting it as a status quo. In fact, this is a
considerable extension of scope of patentability, in breach of the
spirit of the European Patent Convention that excludes from
patentability mathematical methods, computer programs and
presentations of information.

#WWa: The signatories are scientists and software innovators, each of which
has contributed at his level to the extraordinary development of
information technology. We draw the attention of the Members of the
European Parliament to the danger that would arise from accepting the
text proposed by the Commission as it stands. Acceptance of
patentability of algorithms, of principles of software, of information
processing methods or of data structures is scandalous from the view
point of ethics, economically unjustified and harmful, would impact
adversely scientific and technical innovation, and puts democracy at
danger.

#wdd: It is ethically scandalous, because in today's world, knowledge,
information and ideas can not be separated from their technical
representations and the software that manipulate them. It would allow
patent offices to further develop the giant auctioning of the domain
of ideas and knowledge, when this domain was always considered as a
precious common good, that can not be turned in anyone's property.

#laW: It is economically unjustified, because the very arguments that have
been used to justify patents for mechanical and chemical industries,
or more generally manufacturing, do not apply in anyway to software.
No need for software of those monopolies without which one could
hesitate to build a production plant. Manufacturing can very well
continue to patent their technical devices, whether or not they
include software components, as have done for decades. But this
protection must not be extended to software. Copyright protection for
software has allowed the development of huge industries, without any
need for patents. They would be not only useless, but also extremely
harmful, because they would cast in concrete the so powerful
oligopolies that naturally emerge in information-based industries,
when we need on the contrary new instruments to create more
competition.

#cja: In the field of software and information, scientific and technical
innovation needs the open exchange of ideas and knowledge more than
anything, in contrast to the land grab of ideas. Patents would
institute a giant tax on innovation, feeding a system out of control,
servant of established positions.

#hcf: It puts democracy at danger, since the tools of public expression, of
debate, of media, of public consultation are critically dependent on
software. How can one imagine to create private monopoly statute for
this essential basis of tomorrow's democracy ?

#ipo: Patent offices and some technocrats of intellectual property have
demonstrated an imagination without limits in order to justify the
auctioning of what belongs to the public against the spirit of their
charter. We urge the Members of the European Parliament, whatever
their party affiliation, to adopt a text that will make impossible,
clearly, for today and tomorrow, any patenting of the underlying ideas
of software (or algorithms), of information processing methods, of
representations of information and data, and of software interaction
between human beings and computers.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: komp0305 ;
# txtlang: en ;
# multlin: t ;
# End: ;

