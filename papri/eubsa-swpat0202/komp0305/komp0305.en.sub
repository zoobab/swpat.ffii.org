\begin{subdocument}{komp0305}{30 Scientists 2003/05: Petition against Software Patent Directive}{http://swpat.ffii.org/papers/eubsa-swpat0202/komp0305/index.en.html}{Workgroup\\swpatag@ffii.org}{30 famous computer scientists sharply criticise the European Commission's proposal to legalise software patents in Europe.}
\begin{sect}{text}{Petition to the European Parliament}
From 1997, the European Patent Office has initiated and generalised the granting of patents for algorithms, software ideas, data structures and information processing methods. In a directive proposal on 20 February 2002, the European Commission proposed to officialise this abuse, presenting it as a status quo. In fact, this is a considerable extension of scope of patentability, in breach of the spirit of the European Patent Convention that excludes from patentability mathematical methods, computer programs and presentations of information.

The signatories are scientists and software innovators, each of which has contributed at his level to the extraordinary development of information technology. We draw the attention of the Members of the European Parliament to the danger that would arise from accepting the text proposed by the Commission as it stands. Acceptance of patentability of algorithms, of principles of software, of information processing methods or of data structures is scandalous from the view point of ethics, economically unjustified and harmful, would impact adversely scientific and technical innovation, and puts democracy at danger.

It is ethically scandalous, because in today's world, knowledge, information and ideas can not be separated from their technical representations and the software that manipulate them. It would allow patent offices to further develop the giant auctioning of the domain of ideas and knowledge, when this domain was always considered as a precious common good, that can not be turned in anyone's property.

It is economically unjustified, because the very arguments that have been used to justify patents for mechanical and chemical industries, or more generally manufacturing, do not apply in anyway to software. No need for software of those monopolies without which one could hesitate to build a production plant. Manufacturing can very well continue to patent their technical devices, whether or not they include software components, as have done for decades. But this protection must not be extended to software. Copyright protection for software has allowed the development of huge industries, without any need for patents. They would be not only useless, but also extremely harmful, because they would cast in concrete the so powerful oligopolies that naturally emerge in information-based industries, when we need on the contrary new instruments to create more competition.

In the field of software and information, scientific and technical innovation needs the open exchange of ideas and knowledge more than anything, in contrast to the land grab of ideas. Patents would institute a giant tax on innovation, feeding a system out of control, servant of established positions.

It puts democracy at danger, since the tools of public expression, of debate, of media, of public consultation are critically dependent on software. How can one imagine to create private monopoly statute for this essential basis of tomorrow's democracy ?

Patent offices and some technocrats of intellectual property have demonstrated an imagination without limits in order to justify the auctioning of what belongs to the public against the spirit of their charter. We urge the Members of the European Parliament, whatever their party affiliation, to adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the underlying ideas of software (or algorithms), of information processing methods, of representations of information and data, and of software interaction between human beings and computers.
\end{sect}

\begin{sect}{sign}{Signatories}
Andr\'{e} Arnold, Professeur d'Informatique, LaBRI, Domaine Universitaire, 351, cours de la Lib\'{e}ration, F-33405 Talence Cedex, France, arnold@labri.fr\\
Henk Barendregt, Prof.dr, Foundations of Mathematics and Computer Science, Faculty of Science, Toernooiveld 1, NL-6525 ED Nijmegen, The Netherlands, henk@cs.kun.nl\\
Jan A. Bergstra, Instituut Informatica, University of Amsterdam, Informatics Institute, Kruislaan 403, NL-1098 SJ Amsterdam, The Netherlands, janb@science.uva.nl\\
Mark van den Brand, General Secretary of the European Association for Programming Languages and Systems, Centrum voor Wiskunde en Informatica, Department of Software Engineering, P.O. Box 94079, NL-1090 GB Amsterdam,The Netherlands, Mark.van.den.Brand@cwi.nl \\
Maurice Bruynooghe, Editor in Chief of Theory and Practice of Logic Programming, Professor, Katholieke Universiteit Leuven, Departement Computerwetenschappen Celestijnenlaan 200 A, B-3001 Heverlee, Belgi\"{e}, Maurice.Bruynooghe@cs.kuleuven.ac.be\\
Luigia Carlucci Aiello, AAAI and ECCAI Fellow, Full Professor of Computer Science, Dipartimento di Informatica e Sistemistica, Via Salaria 113, I-00198 Roma, Italy, aiello@dis.uniroma1.it\\
Bruno Courcelle, Professeur d'Informatique, Vice-Pr\'{e}sident de l'Universit\'{e} Bordeaux I, LaBRI, Domaine Universitaire, 351, cours de la Lib\'{e}ration, F-33405 Talence Cedex, France, courcell@labri.fr\\
Pierre-Louis Curien, Directeur de Recherches au CNRS, Directeur de l'UMR Preuves, Programmes et Syst\`{e}mes,  Universit\'{e} Denis Diderot Case 7014, 2 Place Jussieu, F-75251 PARIS Cedex 05, France, Pierre-Louis.Curien@pps.jussieu.fr\\
Philippe Flajolet, Prize Award 1986, Corresponding Member of the French Academy of Sciences, Member Academia Europaea, and Dr Honoris Causa, Brussels, Directeur de recherches, INRIA, Domaine de Voluceau, Rocquencourt, B.P.105, F-78153 Le Chesnay Cedex, France, Philippe.Flajolet@inria.fr\\
Maurizio Gabrielli, Professor of Computer Science, Dipartimento di Scienze dell'Informazione, Universit\`{a} di Bologna,  Mura Anteo Zamboni 7, I-40127 Bologna, Italy, gabbri@cs.unibo.it\\
Manuel Hermenegildo, Full Professor, Departamento de Inteligencia Artificial, Facultad de Informatica, Universidas Politecnica de Madrid, E-28660 Boadilla del Monte, Spain, herme@fi.upm.es\\
G\'{e}rard Huet, Member of the French Academy of Sciences, Member Academia Europaea, Herbrand Award  1997, Directeur de recherches, INRIA, Domaine de Voluceau, Rocquencourt, B.P.105, F-78153 Le Chesnay Cedex, France, Gerard.Huet@inria.fr\\
Neil Jones, ACM Fellow, Full professor of Computer Science, University of Copenhagen,Bukkeballevej 88, DK-2960 Rungsted Kyst, DENMARK, neil@diku.dk\\
Paul Klint, Prof. Dr., President of the Board of the European Association for Programming Languages and Systems, Head of Department of Software Engineering, Centrum voor Wiskunde en Informatica ,  P.O. Box 94079, NL-1090 GB Amsterdam,The Netherlands, Paul.Klint@cwi.nl\\
Herbert Kuchen, Prof. Dr., Westf\"{a}liche Wilhems-Universit\"{a}t M\"{u}nster, Institut f\"{u}r Wirtschaftsinformatik, Leonardo Campus 3, D-48149 M\"{u}nster, Germany, kuchen@uni-muenster.de\\
Markus Kuhn, Lecturer, University of Cambridge, Computer Laboratory, William Gates Building, 15 JJ Thomson Avenue, Cambridge CB3 0FD, UK, mgk25@cl.cam.ac.uk\\
Jean-Jacques L\'{e}vy, Directeur de Recherches, INRIA, Domaine de Voluceau-Rocquencourt B.P. 105, F-78153 Le Chesnay, France, Jean-Jacques.Levy@inria.fr . Also Professeur \`{a} L'Ecole Polytechnique.\\
Ramon Lopez de Mantaras, ECCAI Fellow, European AI Award, Full Research Professor, IIIA - Artificial Intelligence Research Institute, CSIC - Spanish Scientific Research Council, Campus Universitat Autonoma de Barcelona, 08193 Bellaterra, Catalonia, Spain, mantaras@iiia.csic.es\\
Alan Mycroft, Reader, University of Cambridge, Computer Laboratory, William Gates Building, 15 JJ Thomson Avenue, Cambridge CB3 0FD, UK, am@cl.cam.ac.uk\\
Robin Milner, Turing Award 1991, Fellow of the Royal Society of London, Fellow of the Royal Society of Edinburgh, Founding Member of Academia Europaea, Holder of six honorary doctorates from five countries, Winner of Italgas Award 1991, Ex-head of the Computer Laboratory, Cambridge University, University of Cambridge, Computer Laboratory, William Gates Building, 15 JJ Thomson Avenue, Cambridge CB3 0FD, UK, Robin.Milner@cl.cam.ac.uk\\
Ugo Montanari, Professor, Dipartimento di Informatica, Universit\`{a} di Pisa, Corso Italia 40, I-56125 Pisa, Italy, ugo@di.unipi.it\\
Maurice Nivat, EACTS Award 2002\footnote{http://www.eatcs.org/Activities/Awards/eatcs\_award2002.html}, 10 av Chardonnerets, F-95570 Attainville, France,  mnivat@wanadoo.fr\\
Bengt Nordstrom, Professor, Department of Computing Science, Chalmers University of Technology, S-412 96 G\"{o}teborg, Sweden\\
Brian Randell, Emeritus Professor, and Senior Research Investigator, School of Computing Science, University of Newcastle upon Tyne, Newcastle upon Tyne, NE1 7RU, UK, brian.randell@ncl.ac.uk\\
Willem-Paul de Roever, Prof. Dr., chair of Software Technology, Institut f\"{u}r Informatik und Prakt. Mathematik, Christian-Albrechts-Universit\"{a}t zu Kiel, Preusserstrasse 1-9, D-24098 Kiel, Germany, wpr@informatik.uni-kiel.de\\
Lorenza Saitta, ECCAI Fellow, Full Professor of Computer Science, Dipartimento of Informatica, Universit\`{a} Amedeo Avogadro, Spalto Marengo, 33, I-15100 Alessandria, Italy, saitta@mfn.unipmn.it\\
G\'{e}raud S\'{e}nizergues, Godel Prize 2002, Professeur d'Informatique, LaBRI, Domaine Universitaire, 351, cours de la Lib\'{e}ration, F-33405 Talence Cedex, France, ges@labri.fr\\
Carsten Svaneborg, Max Planck Institute for Polymer Research, Theory Group, PO. Box 3148, D-55021 Mainz, Germany, svanebor@mpip-mainz.mpg.de\\
Andrew S. Tanenbaum, Professor of Computer Science, Division of Mathematics and Computer Science, Faculty of Sciences, Vrije Universiteit, De Boelelaan 1081A, NL-1081 HV Amsterdam The Netherlands, ast@cs.vu.nl\\
Wolfgang Thomas, Full Professor of Computer Science, Lehrstuhl Informatik VII, RWTH Aachen, 52056 Aachen, Germany, thomas@informatik.rwth-aachen.de\\
Jerzy Tiuryn, Full Professor, Institute of Informatics, Warsaw University, Banacha 2, 02-097 Warsaw, Poland, tiuryn@minuw.edu.pl
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf 30 Computer Scientists 2003/05: Petition against Software Patent Directive\footnote{http://www.cs.chalmers.se/~bengt/petition.pdf}}}

\begin{quote}
explain why this directive is economically unjustifiable and ethically scandalous
\end{quote}
\filbreak

\item
{\bf {\bf CEPIS 2003/07: Leading Computer scientists reject software patent directive proposal\footnote{http://www.upgrade-cepis.org/issues/2003/3/upgrade-vIV-3.html}}}

\begin{quote}
The third edition of the renowned computing quarterly CEPIS Upgrade contains several articles and the text of the ``Petition of 30 Scientists''.  The table of contents says:\begin{quote}
{\it Why software should be within the scope of copyright law, and not patent law.  Pierre Haren opens this section with some brief notes on his opinion on software patents, ``A Note on Software Patents''.}

{\it Alberto Bercovitz Rodr\'{\i}guez-Cano offers a transcript of his ``On the Patentability of Inventions Involving Computer Programmes'', which he delivered at a hearing held at the European Parliament.}

{\it Roberto Di Cosmo ``Legal Tools to Protect SoftwareChoosing the Right One'', an article studying the different legal tools to deal with software.}

{\it Closing this section, we include ``Petition to the European Parliament'', written by several well-known European computer scientists and engineers, related to the proposed Directive on software patents currently being discussed at the European Parliament.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Call for Action\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players.  Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf Appeal to the German Government\footnote{http://swpat.ffii.org/letters/bund028/index.de.html}}}

\begin{quote}
The Federal Government is pushing Brussels for legalisation of patents which it is at the same time infringing.  It has so far not published a position concerning the software patentability directive proposal of the European Commission.  While the parliament and the ministeries are on the whole rather critical, the lawyer-diplomats from the ministry of justice are pushing the European Council toward even more radically pro-software-patent counter-proposal.  The signatories direct four demands to the German Government.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf Eurolinux Petition for a Software Patent Free Europe\footnote{http://petition.eurolinux.org/}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

