                                                   
International Chamber of Commerce
The world business organization





                                                                             





   




                                                                                 22 APRIL 2003

       PROPOSAL FOR A DIRECTIVE ON THE PATENTABILITY OF COMPUTER-
                                  IMPLEMENTED INVENTIONS 

 JOINT STATEMENT OF THE INDUSTRY  ON THE DRAFT REPORT OF THE EUROPEAN PARLIAMENT'S LEGAL
                                              AFFAIRS COMMITTEE



Dear Member of the European Parliament,


A very broad platform of trade associations and industry groups, including ICC
(International Chamber of Commerce), UNICE, EICTA, Growth Plus, ICRT, Open
Forum Europe, BITKOM, AGORIA, INTELLECT, SEDISI and the Confederation of
Swedish Enterprise welcomes the Draft Report of the Committee of Legal Affairs and the
Internal Market, that stresses the need for a directive and for patent protection of
computer-implemented inventions, and tables amendments that will help to clarify the
legal situation in Europe. We highly appreciate the work done by the Rapporteur and


want to encourage all the members of the Legal Affairs Committee to adopt a clear
framework that could contribute to the fulfilment of the promises of the Lisbon summit
on the competitiveness of Europe. We believe the amendments proposed in the Draft
Report are beneficial to the European industry, including the small and medium
enterprises (see points 1 to 3), but we have a great concern with the issue of product
claims (point 4). 


1. The Directive should confirm the current scope of patentability and ensure that
the European practice which has served Europe well is not disrupted.                    Legal
certainty in patent protection is a precondition for the industry to invest in software
development. Such certainty needs to build on the existing interpretations of the legal
framework. By integrating the long-standing approach of the European Patent Office, the
proposed amendments 2, 3, 4, 5, 6, 7, 12, 14, 15 will codify existing rules and preclude
the patentability of "pure" business methods. The use of the precise definitions and
conditions developed by the jurisprudence is the only way to prevent an evolution
towards an overly liberal treatment, such as the one in effect in the United States.     The
EPO's rigorous examination practices will be maintained by this directive and should
prevent many of the problems seen in other parts of the world with so-called "trivial"
patents. 


2. The Directive should      safeguard the possibility for      software developers to
develop interoperable systems. Amendment 10 and 16 of the Draft Report seek to
maintain the existing possibilities of software developers to engage in studying and
reverse engineering of computer programs by clarifying that acts falling within the
relevant exceptions to the copyright protection of programs, are not affected by patent
protection.  


3. The Directive should provide for a mechanism that ensures that open source
software development will not be negatively affected. Amendment 17 empowers the
European Commission to monitor the impact of the Directive on innovation and
competition, and in particular on small and medium businesses. This mechanism will
guarantee against any adverse effect of the Directive on the community of independent
developers, in particular on those that are contributing to the development of open source
software products.


4. The Directive should permit to apply for product claims as it is now allowed by
the case law. Article 5 should be amended to allow program product claims in line with
the existing practice of the European Patent Office and of Member States' national
courts. Otherwise the Directive will hinder the enforcement of patent protection for
computer- implemented inventions. The Commission's proposal has indeed the effect
that a product claim can only be enforced when a user implements the program with
some hardware or apparatus. It is contradictory to provide for a right that is not easily
enforceable against the suppliers (or distributors), which cause (or participate in) the
infringement. Curiously, a patent owner would be able to stop the supplier of a software
product if the supplier is in the same Member State where the software is used but not if
the software product is exported for use in a different Member State. The Commission
proposal, if not amended, would have the serious drawback of introducing a cross-border


anomaly and distortion in the internal market. It is also not consistent with the stated
objective in that the Directive clearly diverges from the existing practice on this
important issue. 

The associations below representing a broad cross section of Europe's most innovative
industries seek Parliament's approval of the Directive on patentability of computer-
implemented inventions with amendments outlined above.





Oliver Blank,                                             Philippe de Buck,
EICTA Director General                                    UNICE Secretary General





 
Urho Ilmonen,                                                    John Stephens, 
Chair ICC Commission on                                   ICRT Chairman
Intellectual Property 





Graham Taylor,                                            Christian Hunt,          
Open Forum Europe Programme Director                      President GrowthPlus
"Initiative to accelerate the market take up              "Europe's 500 fastest growing
of Open Source Software (OSS)"                            companies"




 




Dr. Bernhard Rohleder,                                    Christian Vanhuffel,
Hauptgesch�ftsf�hrer BITKOM                               Director AGORIA


Anthony Parish,            G�ran Tunhammar, 
INTELLECT rep. on the      Director General, 
EICTA Board                Confederation of Swedish Enterprise





Joaquin Oliveras
SEDISI Director General


