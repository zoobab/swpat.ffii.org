<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Unice/Eicta/ICC etc 2003/05/22: %(q:Joint Statement of the Industry) for Software Patents

#descr: In a %(q:Joint Statement of the Industry), directed to the Members of the European Parliament (MEPs), the presidents of various industry associations, including Graham Taylor from the %(q:Open Forum Europe) as a representative of the Linux/Opensource world, asks the legislators to ensure %(ol|that computer programs are treated as patentable inventions and|programs are directly claimable, so that programmers can be sued for publishing a program.)  Moreover, as means of %(q:protecting opensource software), the signatories ask the European Parliament to ensure that %(ol|whenever an interface is patented, interoperable software may not be published or used without a license|business methods are patentable %(q:only) to the extent that a computer or some other device is involved|the European Commission shall publish more %(tp|papers|obituaries?) about the effects of patents on SMEs.) This statement was sent to many MEPs, together with accompanying letters from the patent arms of national industry associations.   Traditionally industry associations have left patent politics completely to corporate patent lawyers, who typically form each association's %(q:industrial property committee).  This committee's papers are usually signed by the president of the association without further consultation of other committees.  Gentle doublespeak with an ungentle insider meaning, as explained above, is also part of the game.  What is unusual about the current letter is that an apparent %(q:opensource community leader) was enlisted for the maneuver.

#fcE: PDF version of the original MSWord file which was sent to Members of the European Parliament

#iai: Limiting patentability

#tel: Interoperability

#oma: Program Claims

#dna: Wider considerations

#mht: Graham Taylor's patent lobbying may be seen as symptomatic of the relationship between patent lobbyists and the associations which they claim to represent.  Taylor %(gt:defends himself) against %(bp:critisism) by implying that support for MEP Arlene McCarthy's proposals was vital to save open source interoperability; and that a position which denied software patents was %(q:unlikely to succeed).

#sft: This is complete misinformation, on both counts.

#Wdn: The changes proposed by Arlene McCarthy to the draft directive are largely cosmetic, and if anything tend to further blur the limits of patentability rather than to clarify them. Like the %(cd:Commission's draft), her %(am:proposal) says that anything %(q:technical) must be patentable but steadfastly refuses to take a stand on what is %(q:technical) and what not.  The thrust of McCarthy's efforts is to ensure that the software and business method patents, which the European Patent Office has granted against the letter and spirit of the written law in recent years, can neither be revoked nor become unenforcable.

#ttr: The real reason that Mr Taylor's organisation was urged to begin lobbying MEPs so hard in January 2003 for the McCarthy proposals has more to do with December 2002, when both the %(cu:Culture committee) and the %(it:Industry committee) of the EU parliament set out (and later voted) to recommended much clearer statutory definitions and significantly stronger restrictions on software patenting.

#xtC: Thus for example the Culture committee's amendment Am 11 (Cult) insists:

#tno: The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field.  The processing, handling and presentation of information do not belong to a technical field.

#rW1: Similarly the Industry committee's Am 13 (Itre):

#paw: Member states shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when technical apparatus is used for that purpose.

#hi1: and the same committee's Am 11 (Itre)

#pph: ... Accordingly, inventions involving computer programs which implement business, mathematical or other methods, which inventions do not produce any technical effects beyond the manipulation and representation of information within the computer-system or network, shall not be patentable.

#pak: It is these amendments, which have already won the support of a majority of MEPs on two committees, that are the real challenge to McCarthy's draft at the crucial vote of the legal affairs committee on Wednesday 21st May; and these amendments (certainly not the original Commission text) which Mr Taylor's co-lobbyists have been desperately trying to marshall their defences against.

#oou: Mr Taylor's other great claim for the %(am:McCarthy draft) is that the draft would make it impossible for software patents to be used %(q:to prevent the development of Open Source alternatives to proprietary products).

#Wsf: Again the truth is rather different.

#nam: The relevant passage is Article 6, as amended by McCarthy (Am 16 McCarthy):

#nnd: Acts permitted as exceptions under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#cts: McCarthy's change (the italics above) is trivial: merely adding seven words to name the relevant articles of directive 91/250/EEC, thus narrowing the scope of possible limitations on patent enforcement.

#Xhi: More importantly the directive [2]91/250/EEC, and its UK implementation ([3]CDPA sec 50B), confers only the right to decompile a program to investigate its interfaces, and then only if the information is not otherwise readily available.

#aWW: But this is of little help in achieving interoperability when interfaces are patented: decompilation would be allowed; but actual interoperation would be possible only by securing a valid licence for the patent.

#mmt: This key problem was recognised by the Industry committee, which recommended the additional Am 15 (Itre):

#sWn: Member States shall ensure that whenever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement

#orc: But Mr Taylor is urging MEPs to support the McCarthy draft, which ignores this.

#eWm: Moreover, Mr Taylor and his colleagues urge the European Parliament to allow program claims, i.e. claims to %(bc:a computer program, characterised by upon loading it into computer memory [ some computing process ] is executed.)

#Usr: So far not only CULT and ITRE but even the European Commission and Arlene McCarthy have refrained from allowing such claims.

#set: If program claims are legalised, the publication and/or distribution of computer programs becomes a direct patent infringement and programmers (as well as Internet service providers) can be held liable.

#Wlf: Graham Taylor has so far failed to explain how such additional liabilities can serve make the life of free software programmers less risky.

#WbW: Software patentability is about much more than Open Source.

#spt: The fundamental reason that the software industry is now so successful and employs so many people is that it has seen a constant stream of innovation changing the landscape, endlessly throwing up new possibilities and challenges for transforming and improving existing applications -- new threats that existing market leaders are forced to respond to, or die.

#dvi: But what if those market leaders were protected by patents ? As Alex Hudson's website asks, %(vc:What if Visicalc had been patented?) How much poorer and more limited the whole industry would be now?

#rhi: The software industry is too important to be allowed to be quietly strangled and portioned out by patents. To take up the words of a recent %(pt:petition) signed by 31 distinguished professors of Computer Science from across the European Union, we should urge the Members of the European Parliament to %(bc:adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the underlying ideas of software (or algorithms), of information processing methods, of representations of information and data, and of interaction between human beings and computers.)

#pfm: If Mr Taylor wants to keep any credibility that his organisation really does speak up for %(q:business users of open source) and the %(q:open source supplier community) (rather than just the Intellectual Property and Public Affairs departments of the most powerful IT giants in the industry), the least he could do would be to clarify his organisation's position on the amendments Am 11 (Cult), Am 13 (Itre) and Am 11 (Itre) on patentability; Am 15 (Itre) on interoperation; and the four specific exclusions called for by the distinguished professors.

#itn: As for the rest of us, there are still nine days to go before the JURI vote; the MEPs on the JURI legal affairs committee can be found %(ju:here); and the Eurolinux front page has some good suggestions about how to %(lm:lobby with maximum effect).

#Ift: A very broad platform of trade associations and industry groups, including ICC (International Chamber of Commerce), UNICE, EICTA, Growth Plus, ICRT, Open Forum Europe, BITKOM, AGORIA, INTELLECT, SEDISI and the Confederation of Swedish Enterprise welcomes the Draft Report of the Committee of Legal Affairs and the Internal Market, that stresses the need for a directive and for patent protection of computer-implemented inventions, and tables amendments that will help to clarify the legal situation in Europe.  We highly appreciate the work done by the Rapporteur and want to encourage all the members of the Legal Affairs Committee to adopt a clear framework that could contribute to the fulfilment of the promises of the Lisbon summit on the competitiveness of Europe.  We believe the amendments proposed in the Draft Report are beneficial to the European industry, including the small and medium enterprises (see points 1 to 3), but we have a great concern with the issue of product claims (point 4).

#tjP: The Directive should confirm the current scope of patentability and ensure that the European practice which has served Europe well is not disrupted.  Legal certainty in patent protection is a precondition for the industry to invest in software development. Such certainty needs to build on the existing interpretations of the legal framework.  By integrating the long-standing approach of the European Patent Office, the proposed amendments 2, 3, 4, 5, 6, 7, 12, 14, 15 will codify existing rules and preclude the patentability of %(q:pure) business methods.  The use of the precise definitions and conditions developed by the jurisprudence is the only way to prevent an evolution towards an overly liberal treatment, such as the one in effect in the United States.   The EPO's rigorous examination practices will be maintained by this directive and should prevent many of the problems seen in other parts of the world with so-called %(q:trivial) patents.

#Aan: The Directive should  safeguard the possibility for  software developers to  develop interoperable systems.  Amendment 10 and 16 of the Draft Report seek to   maintain the existing possibilities of software developers to engage in studying and reverse engineering of computer programs by clarifying that acts falling within the relevant exceptions to the copyright protection of programs, are not affected by patent protection.

#enj: The Directive should provide for a mechanism that ensures that open source software development will not be negatively affected.  Amendment 17 empowers the European Commission to monitor the impact of the Directive on innovation and competition, and in particular on small and medium businesses.  This mechanism will guarantee against any adverse effect of the Directive on the community of independent developers, in particular on those that are contributing to the development of open source software products.

#ioi: The Directive should permit to apply for product claims as it is now allowed by the case law.  Article 5 should be amended to allow program product claims in line with the existing practice of the European Patent Office and of Member States' national courts. Otherwise the Directive will hinder the enforcement of patent protection for computer-implemented inventions.  The Commission's proposal has indeed the effect that a product claim can only be enforced when a user implements the program with some hardware or apparatus.  It is contradictory to provide for a right that is not easily enforceable against the suppliers (or distributors), which cause (or participate in) the infringement.  Curiously, a patent owner would be able to stop the supplier of a software product if the supplier is in the same Member State where the software is used but not if the software product is exported for use in a different Member State. The Commission proposal, if not amended, would have the serious drawback of introducing a cross-border anomaly and distortion in the internal market.  It is also not consistent with the stated objective in that the Directive clearly diverges from the existing practice on this important issue.

#iid: The associations below representing a broad cross section of Europe's most innovative industries seek Parliament's approval of the Directive on patentability of computer-implemented inventions with amendments outlined above.

#sai: english language mailing list discussion

#nis: german language mailing list discussion

#ttl: The report which %(q:the industry) so enthusiastically endorses.

#mlx: Contains some proposals for program claims, as requested by %(q:the industry).

#nWr: A %(q:study) commissioned by the European Commission' industrial property unit, full of unsubstantiated statements about the beneficial effects of software patents.  It shows why %(q:the industry) might want the European Commission to publish more studies about the effects of patents on SMEs.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ipat0304 ;
# txtlang: en ;
# multlin: t ;
# End: ;

