<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: La Comissió Europea (CE) proposa de legalitzar la concessió de patents de programes d'ordinador com a tals a Europa i assegurar que ja no hi hagi cap fonament legal per a refusar l'estil americà  de patents de programari i mètodes de negoci a Europa.   %(q:Però espereu un moment, la CE no diu això al comunicat de premsa!) podeu pensar.  És cert!  Per a trobar el que diu realment, heu de llegir la mateixa proposta.  Però amb compte, està escrita en un nou llenguatge esotèric de la Oficina Europea de Patents (OEP), on les paraules normals sovint volen dir el contrari del què s'esperaria.  També t'has de ficar en un prefaci llarg i confús, que barreja argot de l'OEP amb creences sobre la importància de les patents i el programari propietari, suggerint implícitament alguna connexió entre ambdós.  Aquest text oblida les opinions de gairebé tots els respectats desenvolupadors i economistes, citant com a única font d'informació sobre la realitat del programari dos estudis no publicats de la BSA i col·legues (aliança pels drets d'autor dominada per Microsoft i d'altres grans empreses americanes) sobre la importància del programari propietari.  Aquests estudis ni tan sols parlen de patents!  El text de defensa i la proposta mateixa sembla que les va redactar, en nom de la CE, un empleat de la BSA.  A sota, citem la proposta completa, afegint-hi proves del rol de la BSA com també una anàlisi del contingut, basat en una comparació tabular de les versions de la BSA i de la CE amb una versió corregida basada en la Convenció Europea de Patents (CEP) i doctrines relacionades tal com es troben a les línies d'actuació d'examinació de l'OEP del 1978 i la jurisprudència d'aquell temps.  Aquesta versió de CEP ens ajuda a apreciar la claredat i el seny a les lleis de patentabilitat actualment vàlides, les quals els amics dels advocats de patents de la CE han treballat dur aquests últims anys per deformar-les.

#title: CEC & BSA 2002-02-20: proposta per a fer totes les bones idees patentables

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-jinvi ;
# txtlang: ca ;
# multlin: t ;
# End: ;

