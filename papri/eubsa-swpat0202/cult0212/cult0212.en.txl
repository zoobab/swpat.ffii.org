<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rocard/CULT 2002-12-09: Data Processing is Not a Field of Technology

#descr: The chairman of the European Parliament's Cultural Affairs Commission (CULT), Michel Rocard, proposes amendments to the Software Patentability Directive which are intended to exclude patents on program logic and limit patentability to the field of engineering with forces of nature.  Rocard aptly speaks the TRIPs idiom of the European Commission.  Rocard's amendments constitute the first counter-proposal of an official EU institution where the content actually corresponds to the packaging.  It is less ambitious than the FFII counter-proposal and leaves more loopholes.  If enacted, it would preserve the systematics of the current law (Art 52 EPC) and thereby oblige the patent offices to achieve those very aims which hard-pressed patent politicians in Brussels, London, Berlin and elsewhere always say are theirs.

#hdf: DRAFT OPINION of the Committee on Culture, Youth, Education, the Media and Sport for the Committee on Legal Affairs and the Internal Market

#rtt: Alternatives to the Articles

#Teo: CULT amendment proposals

#ovC: Most of these were in favor of further clarifying the limits.  All were voted by the CULT majority, against the EPP (Conservative & Christian Democrat) members of CULT, whose attempt to introduce program claims by amendment 26 was rejected.

#nWo: Finally a politician is making counter-proposals designed to protect the creative freedom of programmers, where the content of the proposals is in line with the stated intentions.  They may not be sufficient in every respect, but they are also neither empty words nor lies.  Remaining problems reside not so much in the amendments themselves but in what has been left unamended.

#ilW: We like in particular the following amendment to the preamble:

#Wra: This is the single most important statement which needs to be made in this directive, and it is embedded in a preamble which also makes the intention quite clear.

#esd: Patent lawyers will try to deconstruct it by saying that computers use forces of nature and therefore computerised data processing is also a technical field.  The answer is of course that the art of data processing is independent of the technical equipment.  As the german courts have traditionally put it:  %(q:the problem solution is already completed as an abstract solution of an abstract problem before, during its implementation on a known data-processing machine the field of technology is set foot upon).  Given the reluctance of many patent law experts to understand this point, it will be necessary to create a list of examples cases which show how the distinction works.

#enb: The following proposal seems correct, although it uses the word %(q:computer-implemented invention), a word which has been introduced recently by the EPO in order to blur borders:

#WoW: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#tii: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, involves an inventive step, and belongs to a technical field.

#rbe: Problems

#evw: This sentence uses the confusing term %(q:computer-implemented invention).  If this term is used, it must be made clear that we are talking about inventions outside the field of data processing, as stated in Rocard's preamble.  Unfortunately Article 1 of the CEC/BSA proposal says otherwise, and Rocard seems to have left it untouched.  It makes that a %(q:computer-implemented invention) is the opposite of what Rocard defines to be an invention: a data-processing idea (prima facie novel features in residing in a computer program) which is dressed up in claim-language like an invention and therefore, by a sloppy language habit, often called %(q:invention).

#Wnm: Our patent attorney would simply point to the unamended article 1 and infer that at least some data-processing ideas must belong to a technical field.  From there he will conclude that all data-processing ideas which are claimed in terms of technical effects must belong to a technical field.

#esl: Suggestet Solutions

#WWu: Making it clear that %(q:invention) is a synonym for %(q:technical contribution), i.e. the problem solution which the patent claim discloses.  This solution = teaching = invention = contribution must be novel, non-obvious and industrially applicable, and of course technical.  As Rocard correctly says it in the European Commission's favorite idiom, the TRIPs language: they must %(q:belong to a technical field).  The solution = teaching = invention = contribution is of course not identical to the claimed object.  A patent claim's main purpose is not to disclose a problem solution but to exclude others from a scope of possible implementations.  This exclusion scope can only be derived from the invention (= solution = teaching = contribution) by combining it with implementation characteristics which are known from prior art and do not belong to the invention.

#Win: Rewrite article 1 of the CEC proposal:  A %(q:computer-implemented invention) is  an invention in which the computer is part of the implementation, while the invention itself lies in a technical field.

#otl: Even the following is not wrong, although it leaves loopholes for patent lawyers to exploit:

#eak: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#sed: The technical contribution shall be assessed by consideration of the difference between the scope of the technical features of the patent claim considered as a whole and the state of the art.

#hsk: The patent attorney will say that his %(q:claim as a whole) contains a %(q:scope of technical features) of which the calculation rules are an inseparable part and which (taken as a whole) constitute something new.

#tWt: The phrase %(q:as a whole) has always been used as a pretext for refusing what Rocard calls the %(q:consideration of the difference).  Patent lawyers have been successfully portraying this common sense as some mysterious and outdated %(q:core theory) (often using the german word %(q:Kerntheorie) in the english context to make it appear even more mysterious), apparently something very mysterious.

#8ao: The EPO Guidelines of 1978 had a nice straightforward way of express the %(q:consideration of the difference) in terms that seem more difficult for our patent attorney to undermine:

#sha: In assessing whether a claim discloses a technical invention, one must disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the claimed subject matter makes to the known art.  If this contribution does not constitute a technical invention, there is no patentable subject matter.  If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.

#efs: The same could be translated into the patent lobby's favorite contemporary idiom, the TRIPs language, as follows:

#rls: In assessing whether a claim discloses a technical invention, one must disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the claimed subject matter makes to the known art.  If this contribution does not constitute a technical invention, there is no patentable subject matter.  If the contribution to the known art is a contribution to the field of data processing by means of general-purpose computers or other known realisations of abstract machines, then the subject matter is not patentable in whatever manner it may be presented in the claims.

#pei: The amendments to Art 5 stuff a possible loophole by inserting %(q:either), so as to make the %(q:or) exclusive.  That's nice.  It would of course be even nicer if we could use the opportunity in order to raise some more problems and build a few more lines of defense, as we have attempted in our Counter-Proposal.

#spd: This analysis is incomplete and it wouldn't be surprising if our patent attorney found further loopholes.  In particular too many texts of the original proposal still seem to have been left unmodified.  As we have shown, some of these directly contradict Rocard's amendments.

#Wki: Moreover Rocard's text does not address all the problems which we would like to see addressed during the parliamentary discussions of this directive project.

#phn: Rocard's proposal is however the first counter-proposal of an official EU institution which attempts to address the problems seriously, honestly and constructively.  The proposal of the European Commission, which lacks all these features, may find it hard to compete with Rocard's counter-proposal, if only the MEPs take the time to read both proposals.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cult0212 ;
# txtlang: en ;
# multlin: t ;
# End: ;

