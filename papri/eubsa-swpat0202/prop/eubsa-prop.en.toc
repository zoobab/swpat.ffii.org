\contentsline {chapter}{\numberline {1}Rationale}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Why a directive?}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Harmonising a unified system?}{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Clarifying the Unspeakable?}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}The political deal between EU and EPO}{5}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Valid Reasons for a Directive}{6}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Core Layer: Fence in the Technical Invention, Reaffirm Art 52 EPC}{6}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Negative Definition of ``Technical Invention'' in Art 52 EPC}{6}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Translating from EPC language to TRIPs language}{7}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Translating from EPC language to the Meta-Language of Claim-Writing}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}A well-fenced gray zone}{7}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}No such thing as a ``Computer-Implemented Invention''}{8}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Extension Layer: Some Clarifications on enforcability of patents in the context of information goods}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Completion Layer: Define the ``Technical Invention'', limit the scope of judiciary rule-setting}{9}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Abstract Definition of Technical Invention}{9}{subsection.1.4.1}
\contentsline {subsubsection}{the invention which a claim discloses vs the scope of implementations which it forbids}{9}{section*.3}
\contentsline {subsubsection}{Mind vs Matter}{9}{section*.4}
\contentsline {subsection}{\numberline {1.4.2}Categorial exclusions}{9}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Application Examples}{10}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Preamble}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}[Recital 1: Internal Market]}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}[Recital 2: Differences]}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}[Recital 3: No Convergence without Directive]}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}[Recital 4: Significance of Computer Programs Today]}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}[Recital 5: Ergo Harmonisation \& Clarification]}{13}{section.2.5}
\contentsline {section}{\numberline {2.6}[Recital 6: TRIPs]}{14}{section.2.6}
\contentsline {section}{\numberline {2.7}[Recital 7: EPC]}{15}{section.2.7}
\contentsline {section}{\numberline {2.8}[Recital 8: Patents and Public Interest]}{17}{section.2.8}
\contentsline {section}{\numberline {2.9}[Recital 9: Patents and Copyright]}{17}{section.2.9}
\contentsline {section}{\numberline {2.10}[Recital 10: Technical Character]}{18}{section.2.10}
\contentsline {section}{\numberline {2.11}[Recital 11: Field of Technology]}{18}{section.2.11}
\contentsline {section}{\numberline {2.12}[Recital 12: Technical Contribution]}{18}{section.2.12}
\contentsline {section}{\numberline {2.13}[Recital 13: Algorithms]}{19}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Recital 13}{19}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Recital 13(a)(bis) []}{20}{subsection.2.13.2}
\contentsline {section}{\numberline {2.14}[Recital 14: EU Law vs National Law]}{20}{section.2.14}
\contentsline {section}{\numberline {2.15}[Recital 15: Scope of Regulation]}{20}{section.2.15}
\contentsline {section}{\numberline {2.16}[Recital 16: Competitivity]}{21}{section.2.16}
\contentsline {subsection}{\numberline {2.16.1}Recital 16}{21}{subsection.2.16.1}
\contentsline {subsection}{\numberline {2.16.2}Recital 16a []}{21}{subsection.2.16.2}
\contentsline {section}{\numberline {2.17}[Recital 17: Competition Rules]}{22}{section.2.17}
\contentsline {section}{\numberline {2.18}[Recital 18: Interoperability]}{22}{section.2.18}
\contentsline {section}{\numberline {2.19}[Recital 19: Subsidiarity]}{23}{section.2.19}
\contentsline {chapter}{\numberline {3}The Articles}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Article 1: Scope}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Article 2: Definitions}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Computer-Implemented Invention}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Technology, Invention, Non-Inventions}{25}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Article 3: Fields of Technology}{28}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Article 3}{28}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Article 3.bis []}{29}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Article 4: Conditions for patentability}{30}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Article 4(1)}{30}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Article 4(2)}{31}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Article 4(3)}{33}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Article 4(3)a}{33}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Article 4(a)bis []}{33}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Article 5: Claims and Rights}{34}{section.3.5}
\contentsline {section}{\numberline {3.6}Article 6: Interoperability}{37}{section.3.6}
\contentsline {section}{\numberline {3.7}Article 7: Monitoring}{38}{section.3.7}
\contentsline {section}{\numberline {3.8}Article 8: Reporting on the effects of the Directive}{39}{section.3.8}
\contentsline {section}{\numberline {3.9}Article 9: Implementation}{42}{section.3.9}
\contentsline {section}{\numberline {3.10}Article 10: Entry into force}{42}{section.3.10}
\contentsline {section}{\numberline {3.11}Article 11: Addressees}{42}{section.3.11}
\contentsline {chapter}{\numberline {4}Annotated Links}{43}{chapter.4}
