<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Änderungsvorschläge zur Softwarepatentrichtlinie der EU

#descr: Am 2002-02-20 schlug die Europäische Kommission vor, Comptuterprogramme als patentierbare Erfindungen zu betrachten und es sehr schwer zu machen, eine Patentanmeldung auf einen Algorithmus oder eine logische Funktionalität, einschließlich Geschäftsmodell, abzulehnen, sofern diese in den typischen Begriffen der Informatik (e.g. Rechner, Ein- und Ausgabe, Speicher, Datenbank etc) beansprucht wird.  Wir haben einen Gegenvorschlag ausgearbeitet, der die Freiheit des rechnergestützten Denkens, Rechnens, Organisierens und Formulierens und das urheberrechtliche Eigentum der Software-Autoren wahrt und zugleich die Patentierbarkeit von technischen Erfindungen (Problemlösung durch Einsatz beherrschbarer Naturkräfte) bekräftigt, wie sie im Europäischen Patentübereinkommen (EPÜ), dem TRIPs-Abkommen und in mustergültigen Kommentaren und Lehrbüchern des Patentrechts zum Ausdruck kommt.  Dieser Gegenvorschlag erfährt die Unterstützung zahlreicher maßgeblicher Akteure in den Bereich Software, Wirtschaftswissenschaften, Politik und Recht.

#eak: Zugrundeliegende Erwägungen

#Pem: Präambel

#TAc: Die Artikel

#yii: Wozu eine Richtlinie?

#Prt: Kernschicht: die Technische Erfindung einzäunen, Art 52 EPÜ Bekräftigen

#IfW: Erweiterungsschicht: einige Klärungen über die Anwendbarkeit von Patenten im Hinblick auf Informationsgüter

#nia: Vollendungsschicht: die %(q:Technische Erfindung) positiv definieren, den Bereich des Richterrechts eingrenzen

#nia2: Ein einheitliches System harmonisieren?

#fey: Das Unaussprechliche Klären?

#olW: Der Kuhhandel zwischen EU und EPA

#dWr: Gültige Gründe für eine Richtlinie

#neW: The European Commission has proposed a directive for the %(q:patentability of computer-implemented inventions), colloquially termed %(q:software patent directive).

#rot2: The stated aim of the directive is to %(q:harmonise) and %(q:clarify) the rules of what is patentable and what not in the field of computer-related products and proceses.

#yee: Europe's patentability rules were unified by the European Patent Convention of 1973.  In cases of distortions of the internal market, the European Court of Justice is entitled to decide, no matter whether the substantial patent law which it cites is EU law or the EPC.  Moreover, preparations are under way for a centralised EU Patent Court in Luxemburg, which again would be empowerd to decide on the basis of the EPC as well as of EU directives.  So what could possibly still be left for a EU directive to %(e:harmonise)?

#Wsc: If there is nothing to %(e:harmonise), couldn't there at least be something to %(e:clarify)?  Sure, there must be.  Laws usually leave some questions open for interpretation.  Patent law in particular has large gray zones in which rules have been set by judges.  Regulating patentability would be a thorny political task.  Some even say that clarification beyond Art 52 EPC is %(dg:infeasible).

#rvn: Legal study from Amsterdam University commissioned by the European Parliament.

#Wce: Rather than narrowing the gray zones, the EU Comission proposes to further enlarge them.   Everything is made dependent on undefined terms such as %(q:technical character), and even on self-contradictory arrangments of this empty word, such as saying that a %(q:technical contribution) may consist solely of untechnical features (article 4), or that it is implied in non-obviousness (articles 1-4).  Between the lines this directive proposal seems to say:

#eWW: Member states shall ensure that national courts may not revoke any patents based on what they consider to be a consistent interpretation of the written law but only based on the authority of European judicial institutions such as the European Patent Office.

#hWt: It would be have been much clearer if it could have been reduced to this one sentence.

#tWW: So, if there is nothing to harmonise and nothing to clarify, why are EU politicians almost unanimously pushing for a directive?

#WUh: One answer could be that EU politicians are naturally in favor of anything that extends EU influence.  But that is not the whole answer.

#Whi: Europe's substantive patent law is currently not regulated by the EU but by the European Patent Organisation (EPO), an intergovernmental organisation which runs the European Patent Office (EPO) and is associated with the EU (e.g. EPO membership is precondition for EU membership) .  There has however been a movement to transfer power from the EPO to the EU.  This power transfer has been based on win-win deals, such as an increased influence of the EPO at the European Commission.  The software patent directive is such a win-win deal:  the EPO receives a license to grant software patents, while the EU is allowed to extend its legislative competence in patent matters.

#WWu: When the EU extends its competence, it usually cites the EC Treaty.  The most convenient pretext is %(q:harmonisation): somewhere in Europe there must be some remaining traces of national diversity which can be said to %(q:impede the proper functioning of the internal market).  However, this %(e:harmonisation trick) does not always work, as can be seen from some recent unsuccessful clashes of the European Commission with national governments (e.g. over the tobacco advertisement directive) before the European Court of Justice.

#Uee: There are good reasons for the EU to legislate on matters of patentability:

#fWl: Restrictions on the use of ideas affect basic liberties and can have a great impact on propsperity.  Unlike many detail questions of patent law, the basic rules concerning what is patentable need to be decided by an elected parliament.

#nWh: With the introduction of the community patent, the patent system has become a domain of EU legislation.  Without clear and strict patentability rules, a more efficient patent system can mean more efficient infliction of harm on European industry and citizens.

#bpe: Rule-setting power on questions of material patent law is currently is already europeanised.  There is a pattern of informal legislation by the European Patent Office and its Administrative Council, both of which are not elected legislative organs.

#tre: The European Patent Office has set new rules of patentability which are at odds with the existing written laws and which are followed by some but not all national courts.  The question of what is patentable is governed by two competing sets of rules and the role of an arbiter between these two is most naturally fulfilled by the European Parliament.

#ant: A few questions concerning enforcability of patents are currently still in the domain of national law and there is indeed some divergence.  Strangely enough, the European Commission has shown little interest in addressing these divergences so far, although the questions are not less capble of %(q:impeding the proper functioning of the internal market).

#arl: With some effort, this reasoning should be able to serve as a valid justification for passing a EU directive.  We have tried to word our preamble amendment proposals accordingly.

#Wei: Negative Definition of %(q:Technical Invention) in Art 52 EPC

#alP: Übersetzung von der Sprache des EPÜ in die Sprache des TRIPs-Vertrages

#nof: Übersetzung von EPÜ-Sprache zur Meta-Sprache der Anspruchsformulierung

#ecy: Eine gut umzäunte Grauzone

#WWt: %(q:Computer-implementierten Erfindungen) kann es nicht geben

#eWc: The basic choice to be made is whether we adopt the rules of Art 52 EPC, as reflected in the %(ep:Examination Guidelines of 1978) and most of the EPO caselaw of the 1970-80s and patent law textbooks such as %(kr:Kraßer 1986) and %(bk:Benkard 1988), or the more %(q:liberal) patentability rules developped by the EPO step by step after the publication of the revised Examination Guidelines of 1985.

#Cra: Since the later EPO caselaw deviates from the EPC in various respects, there is, even from a purely legal point of view, no choice but to opt for the EPC.  Otherwise the directive would in fact change the law, rather than %(q:harmonise the status quo), and it might not stand up to a challenge before the European Court of Justice (ECJ).

#eva: We propose to opt for the original Art 52 EPC (negative definition of %(e:technical invention)) and to restate it som more contexts, so that it is less likely to be misunderstood.  Recent patent language is characterised by new elements such as the TRIPs treaty, which didn't exist in 1973, when the EPC was decided.  Although Art 52 EPC provides a clear and adequate structure for today's patent system and is perfectly compatible with TRIPs, it has been weakened by recent layers of interpretation, many of which aimed at creating a different legal structure.

#WWh: At the same time, we propose to correct some misconceptions of the EPO jurisdiction which are not compatible with Art 52 EPC.  These include

#hie: The term %(q:invention) must be used only in the sense of %(q:patentable invention): a synonym of %(q:technical invention), %(q:technical contribution), %(q:technical solution) or %(q:technical teaching).  It is in particular not permissible to use the term %(q:invention) to refer to non-technical ideas or to features in the claim wording which are not meant to be subjected to the tests of novelty, non-obviousness and industrial applicability.

#vno: The four tests of %(q:technical invention), %(q:novelty), %(q:non-obviousness) and %(q:industrial applicability) must be kept separate and strengthened each on its own terms.  It is in particular not permissible to mingle the %(q:technical invention) test into the %(q:inventive step) (= non-obviousness) test, as the EPO and the CEC proposal have been doing.

#rvo: Wir schlagen vor, Art 52 EPÜ mithilfe der folgenden Aussagen in den Begriffen des Art 27 TRIPs neu zu formulieren:

#datatech: Die Mitgliedsstaaten sorgen dafür, dass die Datenverarbeitung nicht als ein Gebiet der Technik im Sinne des Patentrechts angesehen wird, und dass Neuerungen auf dem Gebiet der Datenverarbeitung nicht als Erfindungen im Sinne des Patentrechts angesehen werden.

#sRh: A similar provision was adopted and voted favorably by the European Parliament's Culture commission (CULT-16).

#Wue: Moreover, in our amendment to Article 5, we propose to translate the rule into the context of patent claim writing:

#produkt: Die Mitgliedsstaaten sorgen dafür, dass eine computerisierte Erfindunge als ein Erzeugnis, d.h. ein Satz von an eine Datenverarbeitungsanlage angeschlossenen Geräten, oder als ein von solchen Geräten ausgeführtes Verfahren beansprucht werden kann.

#fWn: The exclusion of general-purpose data processing leaves room for the courts and patent offices to grant patents on computer-controlled machine tools or %(ab:anti-blocking systems), even when the novel teaching arguably lies in the realm of logics or mathematics.  Just as in Art 52 EPC, we merely exclude certain classes of objects and do not specify that the rest must be patentable.  There is nothing in this rule that could encourage lawcourts to grant patents on applied mathematics.  Our core proposal does not exhaustively define the technical invention.  Rather, it sets a clear and concrete boundary to the extension of the scope of patentability: programs for the general purpose computer are off limits, and so is the application of computers to business models, social processes and other phenomena that are unrelated to controlling forces of nature.

#oWa: Such a regulation thus would clarify some important points, and it corresponds to a %(wc:wide consensus of politicians, economists, engineers, programmers and patent experts) (in particular judges and examiners, less so attorneys).

#WWs: Read e.g. the %(sc:quotations) from all these groups.  Whenever politicians make statements about this question, they in fact plead for the border outlined here, but often their words are interpreted in a very special way by their patent officials.  Compare e.g. the quote from UK e-minister Patricia Hewitt with its interpretation by the UK Patent Office.

#ior: It is clear from the above why the term %(e:computer-implemented invention) is misleading.  An idea that can be implemented merely by means of a general-purpose computer cannot be an invention in the sense of patent law.  Rather, it is an algorithm expressed in terms of a mathematical model called Von Neumann Machine.  In the language of Art 52 EPC, it is a %(q:program for computers) or a %(pd:%(q:program for data processing equipment)) in the narrowest sense, and it will usually also fall into the categories of %(q:mathematical methods) or %(q:plans and rules for performing mental activity), all of which are not considered to be technical inventions.

#ang: The german version has %(e:Programme für Datenverarbeitungsanlagen) (programs for data processing equipment) for where the english says %(e:programs for computers).

#mue: If a term like %(q:computer-implemented invention) really has to be used, we recommend renaming it to %(q:computer-related invention) and incorporating a proper definition in the directive, similar to the one which we propose below.

#bnW: The %(it:ITRE vote) has shown that it is possible to obtain consensus for regulations on a series of questions which have arisen out of recent practise, such as:

#ori: limits of patent enforcability and claim form in view regard to information freedoms such as the freedom of publication guaranteed by Art 10 ECHR or the freedom to use one's copyrighted intellectual property

#fir: limits of patent enforcability in view of interoperability

#fgW: limits of patent enforcability in view of prior use in the case of information objects (i.e. %(q:use) means %(q:dissemination) and can therefore not easily be limited in scope)

#sna: what constitutes published prior art in the case of information objects

#rrW: obligation to disclose a workable program in the description wherever a claim refers to a program

#rWf: measures for regular monitoring of the scope of patentability and control of the patent system

#anh: safeguards to ensure that patents can be revoked when the Parliament later finds that a stricter approach to the technical invention is needed.

#oan: These questions are largely in the sphere of national legislation until today.  They have not been regulated by theprovisions in Art 52-57 EPC (substantive patent law).  Thus EU intervention in this sphere is both feasible and conveniently justifiable.

#tpo: The existence of gray zones may be inevitable.  Yet currently various EU proposals have made ambitious promises of wanting to eliminate gray zones.  Such promises were apparently needed in order to extend the EU's regulative competence into the area of patentability.

#bvt: Yet it may indeed be a good idea to get serious about eliminating gray zones.  That would however involve far more of an effort than what the EU seems to be ready to do at the moment.  It would be an ambitious project, involving a deepened discussion among concerned circles.  It would probably have to rely on three pillars:

#con: Abstrakte Definition der %(q:Technischen Erfindung)

#eWi: Kategorielle Ausschlüsse

#pia: Anwendungsbeispiele

#snc: This is rooted in most european patent traditions and has found one of its most famous and most elaborate expression in the German Federal Court's %(dp:Dispositionsprogramm decision of 1976).  It relies on two essential distinctions:

#rct: the invention which a claim discloses vs the scope of implementations which it forbids

#nst: Mind vs Matter

#dai: The invention must be new and technical, independently of how it is implemented.  When software-patent advocates speak about a %(q:computer-implemented invention), they are usually not referring to inventions but to scopes of forbidden implementations.  By such a shift of underlying meaning, any restriction on patentability can be made meaningless.

#eli: Synonyms for %(e:invention) are %(e:technical contribution), %(e:technical teaching) or %(e:technical solution).  Inventions are implicitely technical.  It is dogmatically wrong to distinguish between %(e:invention) and %(e:technical contribution) as done in the CEC/BSA proposal.  When CEC/BSA speaks of %(q:invention), they mean %(q:idea).

#hre: Inventing in the sense of patent law means harnessing the forces of nature, which do not follow the rules of intellectual creation and must be verified by experimentation (letting nature answer questions) rather than by mathematical proof (letting the human mind answer).  Only in this field can there be a macro-economid justification for granting a 20 year monopoly on a novel teaching.

#uvt: %(tr:Art 27 TRIPs) determines that patentability must be limited by reference to the concepts of %(q:invention), %(q:technical) and %(q:industrial).  Even those who have difficulty with these concepts are forced to use them.  Failing to define and apply them in a meaningful way may be construed as a violation of TRIPs.

#cme: These are more concrete and therefore more mass-communicatable than the above-explained basic distinctions.

#Wla: In EPC language

#atn: Programs for computers are not inventions

#Wsg: In TRIPs language

#rot: Data processing is not a field of technology

#lrW: These first two pillars have in the past been weakened by progressive erosion.  They need further support from a third, even more concrete pillar.

#eaw: The directive should contain an annex with examples of model decisions, comprising both real cases from the earlier national caselaw and constructed cases.

#EWo: The CEC/BSA proposal quotes recent EPO decisions as models.  An amendement proposal should be accompanied by a preamble and an annex which quotes extracts from a few model cases.  These should contain a description of prior art, an alleged invention and examples of possible claims and how they would be judged (refuted or upheld).

#bWu: The annex with examples could be regularly reviewed by the European Parliament.  Thus the Legislative could take charge of an important area of rule-setting competences which has been in the hands of judges for too long.

#twx: This again presupposes that the directive acknowledges the existence of a gray area in which the validity of existing patents is even less to be %(q:taken for granted) than elsewhere.

#titcec: %(nl|Vorschlag für eine|RICHTLINIE DES EUROPÄISCHEN PARLAMENTS UND DES RATES|über die Patentierbarkeit computerimplementierter Erfindungen)

#titamd: %(nl|Vorschlag für eine|RICHTLINIE DES EUROPÄISCHEN PARLAMENTS UND DES RATES|über die Grenzen der Patentierbarkeit|im Hinblick auf die Automatische Datenverarbeitung und ihre Anwendungsgebiete)

#titjust: Der Begriff %(q:computer-implementierte Erfindung) ist dem Computer-Fachmann nicht geläufig.  Er ist in der Tat überhaupt nicht im allgemeinen Gebrauch.  Er wurde vom Europäischen Patentamt (EPA) im Mai 2000 in %(a6:Anhang 6) der Akte zur Trilateralen Konferenz eingeführt, wo er dazu diente, die Patentierbarkeit %(q:computer-implementierter Geschäftsmethoden) zu legitimieren, um die Europäische Praxis in Einklang mit der der USA und Japans zu bringen.  Der Richtlinienvorschlag der Europäischen Kommission lehnt sich eng an diesen %(q:Anhang 6) an.  Der Begriff %(q:computer-implementierte Erfindung) impliziert, dass in Begriffen des Universalrechners formulierte Algorithmen und Geschäftsmethoden patentfähige Erfindungen sind.  Diese Implikation steht im Widerspruch zu Art 52 EPÜ, demzufolge Algorithmen, Geschäftsmethoden und Programme für Datenverarbeitungsanlagen keine Erfindungen im Sinne des Patentrechts sind.  Es kann nicht das Ziel der vorliegenden Richtlinie sein, %(q:computer-implementierte) Innovationen aller Art zu patentfähigen Erfindungen zu erklären.  Vielmehr muss es darum gehen, die Grenzen der Patentierbarkeit im Hinblick auf die Datenverarbeitung und ihre %(tp|diversen|technischen und untechnischen) Anwendungsgebiete klar zu stellen, und dies muss im Titel auf einfache und unmissverständliche Weise formuliert werden.

#TEO: DAS EUROPÄISCHE PARLAMENT UND DER RAT DER EUROPÄISCHEN UNION -

#HiW: gestützt auf den Vertrag zur Gründung der Europäischen Gemeinschaft, insbesondere auf Artikel 95,

#HWo: auf %(fn:39:Vorschlag der Kommission),

#HpW: nach %(fn:40:Stellungnahme des Wirtschafts- und Sozialausschusses),

#AtW2: gemäß dem %(fn:41:Verfahren des Artikels 251 EG-Vertrag),

#Wee: in Erwägung nachstehender Gründe:

#tlk: Internal Market

#ien: Unterschiede.

#euW: Keine Konvergenz ohne Richtlinie

#fti: Heutige Bedeutung der Automatischen Datenverarbeitung

#Wif: Ergo Harmonisierung und Klärung

#ePt: Patente und Öffentliches Interesse

#tdr: Patente und Urheberrecht

#cla: Technischer Character (Technizität)

#Pde: Gebiet der Technik

#hCu: Technischer Beitrag

#lrh: Algorithmen

#LNl: Europäisches und Nationales Recht

#Lmt: Umfang der Regelung

#rCt: Wettbewerbsfähigkeit

#mtR: Wettbewerbsregeln

#mnr: Interoperabilität

#udi: Subsidiarität

#TWE: Damit der Binnenmarkt verwirklicht wird, müssen Beschränkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen begünstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium für den Erfolg des Binnenmarkts. Es ist unerlässlich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam und einheitlich geschützt sind, wenn Investitionen auf diesem Gebiet gesichert und gefördert werden sollen.

#Tse: Die Verwirklichung des Binnenmarktes erfordert, dass Beschränkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden.  Dabei soll ein Umfeld geschaffen werden, welches Innovationen und Investitionen begünstigt.  Ein %(s:verlässlicher und ausgeglichener Leistungsschutz im Bereich der Software-Entwicklung) ist eine wichtige Voraussetzung für den Erfolg des Binnenmarktes.  %(s:Innovative Software-Entwickler sollten weder durch bequeme, kostenscheuende Nachahmung noch durch die Monopolisierung von grundlegenden Ideen und Schnittstellen an der Entwicklung und Verwertung ihrer Werke gehindert werden.  Die Software-Branche sollte ein ebenes Spielfeld sein, auf dem viele kleine Unternehmen florieren und wachsen können, indem sie hochwertige interoperable Software entwickeln.)

#isa: Es gibt keinerlei Belege für die Andeutungen der Kommission, dass Patente der Innovation im Bereich der Software dienen.  Zahlreiche %(es:wirtschaftswissenschaftliche Studien) deuten auf das Gegenteil hin.  Der Kommissionsvorschlag betont die Wichtigkeit einzelner %(q:Erfindungen) und vernachlässigt andere wichtige Faktoren der Software-Ökonomie.

#Dao: Die Patentpraxis und die Rechtsprechung in den einzelnen Mitgliedstaaten hat zu Unterschieden bei der Behandlung von software-bezogenen Patentanmeldungen geführt. Solche Unterschiede könnten den Handel stören und somit verhindern, dass der Binnenmarkt reibungslos funktioniert.

#pit: Wir meiden geladene Begriffe wie %(q:Schutz) und %(q:computer-implementierte Erfindung).  Ob die offenbarten Lehren wirklich %(q:Erfindungen) im Sinne des Patentrechts sind, muss gemäß den Regeln dieser Richtlinie erst noch geprüft werden.  Es geht nicht um Computer sondern um deren Logik (Software), und es ist umstritten, was Patente hierbei %(q:schützen) können und sollen.  Viele Programmierer haben in der Tat den Gesetzgeber gebeten, wie %(e:vor) Patenten zu schützen.

#San: Die Ursachen für die Unterschiede liegen darin begründet, dass die Mitgliedstaaten neue, voneinander abweichende Verwaltungspraktiken eingeführt oder die nationalen Gerichte die geltenden Rechtsvorschriften unterschiedlich ausgelegt haben; diese Unterschiede könnten mit der Zeit noch größer werden.

#Nua: Wegen der nach 1985 schrittweise veränderten EPA-Rechtsprechung sind die Gerichte in Europa jedoch zwischen zwei miteinander konkurrierenden Rechtsnormen hin und her gerissen.  Diese Situation erfordert eine gesetzgeberische Entscheidung.

#ads: Mit zunehmend intensiver Meinungsbildung auf nationaler und europäischer Ebene werden Unterschiede im Richterrecht normalerweise eher abgebaut als vertieft.  In den letzten Jahren hat sich die Patentrechtsprechung in vielen Fragen transnational konvergierend entwickelt.  Gerichte verschiedener Länder haben ihre Regeln aneinander angeglichen, wo dies möglich war.  Da das materielle Patentrecht in Europa bereits vereinheitlicht ist, wäre hier normalerweise kein Harmonisierungsbedarf zu erwarten.

#rec4cec: Die zunehmende Verbreitung und Nutzung von Computerprogrammen auf allen Gebieten der Technik und die weltumspannenden Verbreitungswege durch das Internet sind ein kritischer Faktor für die technologische Innovation. Deshalb sollte sichergestellt sein, dass die Entwickler und Nutzer von Computerprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden.

#rec4amd: Seit der Einführung des Universalrechners in den 50er Jahren sind Computerprogramme ubiquitär verwendet worden, um Vorgänge auf allen Gebieten des menschlichen und gesellschaftlichen Handelns zu beschreiben, zu steuern und zu integrieren.  Daher ist es nötig dafür zu sorgen, dass Entwickler und Anwender von Comuterprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden.

#rec4just: Der Text der Kommission erklärt nicht, in welcher Weise Computerprogramme heutzutage %(q:kritisch) sind, und er deutet eine Beziehung zwischen einer angenommenen %(q:Vermehrung) des Einsatzes von Computerprogrammen und einem Handlungsbedarf des Gesetzgebers an, ohne klar zu sagen, wie diese Beziehung beschaffen ist.  Auf ebenso unerklärte Weise werden Computerprogramme mit %(q:Gebieten der Technik) in Verbindung gebracht.  Dadurch wird der Boden für den allerseits abgelehnten Artikel 3 des Kommissionstextes vorbereitet, wonach Computerprogramme immer gemäß Art 27 TRIPs patentfähig sein müssen, egal ob sie Vorgänge auf dem Gebiet des Rechnens, des Bankgeschäfts oder der Verwaltung betreffen.

#TWv: Aus diesen Gründen sollten die Rechtsvorschriften, so wie sie von den Gerichten in den Mitgliedstaaten ausgelegten werden, vereinheitlicht und die Vorschriften über die Patentierbarkeit computerimplementierter Erfindungen transparent gemacht werden. Die dadurch gewährte Rechtssicherheit sollte dazu führen, dass Unternehmen den größtmöglichen Nutzen aus Patenten für computerimplementierte Erfindungen ziehen, und sie sollte Anreize für Investitionen und Innovationen schaffen.

#TWl: Deshalb sollen die Regeln des Art 52 EPÜ über die Grenzen der Patentierbarkeit bestätigt und präzisiert werden.  Die dadurch entstehende Rechtssicherheit sollte %(s:zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen).

#tdo: Experience and economic studies strongly suggest that patents on computer-implemented rules of organisation and calculation (programs for computers), as have been granted by the European Patent Office (EPO) in contradiction to the letter and spirit of the EPC, are not conducive to innovation in the area of software.  A recent study (http://www.researchoninnovation.org/swpat.pdf) shows that the same patents have substituted innovation and thus led to a reduction in R&D investment and productivity even those companies that patented most.  Reconfirming the invalidity of these patents would reestablish legal security and confidence in investing in software development.

#TcW: Die Gemeinschaft und ihre Mitgliedstaaten sind auf das Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-Übereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 über den Abschluss der Übereinkünfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europäischen Gemeinschaft in Bezug auf die in ihre Zuständigkeiten fallenden %(fn:42:Bereiche). Nach Artikel 27 Absatz 1 des TRIPS-Übereinkommens sollen Patente für Erfindungen auf allen Gebieten der Technik erhältlich sein, sowohl für Erzeugnisse als auch für Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen Tätigkeit und sind gewerblich anwendbar. Gemäß dem TRIPS-Übereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erhältlich sein und Patentrechte ausgeübt werden können. Diese Grundsätze sollten demgemäß auch für computerimplementierte Erfindungen gelten.

#Wjr: Die Gemeinschaft und ihre Mitgliedstaaten sind auf das Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-Übereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 über den Abschluss der Übereinkünfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europäischen Gemeinschaft in Bezug auf die in ihre Zuständigkeiten fallenden Bereiche. Nach Artikel 27 Absatz 1 des TRIPS-Übereinkommens sollen Patente für Erfindungen auf allen Gebieten der Technik erhältlich sein, sowohl für Erzeugnisse als auch für Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen Tätigkeit und sind gewerblich anwendbar. Gemäß dem TRIPS-Übereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erhältlich sein und Patentrechte ausgeübt werden können. %(s:Dies bedeutet, dass die Patentierbarkeit durch allgemeine Begriffe wie %(q:Erfindung), %(q:Technik) und %(q:Industrie) wirksam begrenzt werden muss, damit die Freiheit des Welthandels weder durch unsystematische Ausnahmen noch durch unüberschaubare Ausweitungen beeinträchtigt wird.  Demnach sind Erfindungen auf allen Gebieten der angewandten Naturwissenschaften, nicht jedoch Innovationen im Bereich der Mathematik und Datenverarbeitung, patentierbar, und zwar jeweils unabhängig davon, ob bei der Ausführung der Idee eine Datenverarbeitungsanlage zum Einsatz kommt.)

#tdW: We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it excludes business method patents, which the US government wants to mandeate by the new Substantive Patent Law Treaty draft, see %(URL).

#rec7cec: Nach dem Übereinkommen über die Erteilung europäischer Patente (Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den Patentgesetzen der Mitgliedstaaten gelten Programme für Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt jedoch nur, und hat auch ihre Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht, da die besagten Gegenstände und Tätigkeiten als solche keinem Gebiet der Technik zugehören.

#rec7amd: Nach dem Übereinkommen über die Erteilung europäischer Patente (Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den Patentgesetzen der Mitgliedstaaten gelten Programme für Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Dieser %(s:Ausschluss) gilt jedoch nur, und hat auch seine Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht.  %(s:Sie kommt somit etwa nicht zur Geltung, wenn die offenbarte Erfindung nicht in einem Datenverarbeitungsprogramm als solchem liegt sondern in einer %(tp|technischen Erfindung|z.B. einem neuen chemischen Vorgang), der durch ein Programm gesteuert wird.  In diesem Falle wäre das Programm frei implementierbar und auf einer Datenverarbeitungsanlage (z.B. zur Simulation) frei verwendbar, aber der chemische Vorgang unterfiele dem Patent).

#rec7just: Nach den normalen Regeln der Semantik kann %(q:Programm als solches) nur %(q:Programm als Programm) bedeuten, nicht %(q:Programm insofern nicht technisch).  Gemäß Art 52(2) EPÜ sind Programme für Datenverarbeitungsanlagen (Rechenregeln für Universalrechner) nicht technische Erfindungen.  Art 52 nennt kein Abgrenzungskriterium namens %(q:technisch).  Der Patentrechtsprechung steht es frei, ein solches Kriterium zu definieren und zu verwenden, solange sie die Vorgaben von Art 52 EPÜ einhalten.  Nicht irgend ein Technikbegriff sondern Art 52 EPÜ ist der Maßstab.

#j9e: In seiner (im ABl. C 378 vom 29.12.2000, S. 95, veröffentlichten) Entschließung zu dem Beschluss des Europäischen Patentamts bezüglich des am 8. Dezember 1999 erteilten Patents Nr. EP 695 351 forderte das Europäische Parlament eine Überprüfung der Tätigkeiten des EPA, um zu gewährleisten, dass es einer öffentlichen Rechenschaftspflicht unterliegt.

#sea: Das EPA ist keine Einrichtung der EU und es ist schon früher Sorge über seine Zurechenbarkeit geäußert worden.

#Pha: Der Patentschutz versetzt die Innovatoren in die Lage, Nutzen aus ihrer Kreativität zu ziehen. Patentrechte schützen zwar Innovationen im Interesse der Gesellschaft allgemein; sie sollten aber nicht in wettbewerbswidriger Weise genutzt werden.

#Pri: Patente sind zeitweilige Monopole, welche der Staat Erfindern gewährt, um den technischen Fortschritt zu fördern.  Um sicher zu stellen, dass das System wie vorgesehen funktioniert, müssen die Bedingungen der Erteilung und Durchsetzung von Patenten umsichtig festgelegt werden.  Insbesondere müssen unvermeidbare Begleiterscheinungen des Patentwesens wie die Beschränkung der Schaffensfreiheit, Rechtsunsicherheit und kartellfördernde Wirkungen in vernünftigen Grenzen gehalten werden.

#ist: Innovatoren können auch ohne Patente Nutzen aus ihrer Kreativität ziehen.  Ob Patentrechte die Innovation %(q:schützen) oder hemmen und ob sie im Sinne der Gesellschaft als ganzer wirken, ist eine Frage, die nur durch Erfahrung (empirische Studien) und nicht durch dogmatische Festlegungen im Gesetz beantwortet werden kann.

#I3H: Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz von Computerprogrammen43 sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht geschützt.  Die Ideen und Grundsätze, die einem Element eines Computerprogramms zugrunde liegen, sind dagegen nicht durch das Urheberrecht geschützt.

#InW: Nach der %(fn:43:Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz von Computerprogrammen) sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht als %(s:individuelle Schöpfungen) geschützt.  Die Ideen und Grundsätze, die einem Computerprogramm zugrunde liegen, müssen frei verwendbar bleiben.

#asr: Das Urheberrecht findet nicht nur Anwendung auf literarische Werke sondern auch auf Lehrbücher, Gebrauchsanweisungen, Computerprogramme und Informationsstrukturen aller Art.  Das Urheberrecht ist %(e:das) System des %(q:Geistigen Eigentums) in Computerprogrammen und nicht nur ein System für einen %(q:literarischen) Nebenaspekt von Computerprogrammen.  Wenn das Urheberrecht nicht die %(q:zugrundeliegende Idee) eines Buches oder Programms mit umfasst, dann deutet dies nicht auf eine Unzulänglichkeit des Urheberrechts sondern viel mehr auf ein Freihaltungsbedürfnis hin.  Grundlegende Ideen müssen frei verfügbar bleiben, damit viele unterschiedliche Autoren die Chance haben, darauf aufbauend Eigentum an individuellen Schöpfungen zu erwerben.

#Isc: Damit eine Erfindung als patentierbar gilt, sollte sie technischen Charakter haben und somit einem Gebiet der Technik zuzuordnen sein.

#IWd: Damit eine Idee als patentfähige Erfindung gelten kann, sollte sie technischen Charakter haben und somit zu einem Gebiet der Technik gehören.

#Elu: Der Kommissionstext ist mit Art 52 EPÜ unvereinbar.  Art 52(2) EPÜ listet Beispiele von Nicht-Erfindungen auf.  Es ist nicht statthaft, diese unter %(q:Erfindungen) zu subsumieren und dann auf Technizität zu prüfen.  Aus Art 52 EPÜ kann zwar nicht hergeleitet werden, dass alle technischen Innovationen Erfindungen im Sinne des Patentrechts sind, aber man darf, gestützt auf eine einhellige Tradition des Patentrechts, annehmen, dass alle patentfähigen Erfindungen technischer Natur sind.

#rec11cec: Zwar werden computerimplementierte Erfindungen einem Gebiet der Technik zugerechnet, aber um das Kriterium der erfinderischen Tätigkeit zu erfüllen, sollten sie wie alle Erfindungen einen technischen Beitrag zum Stand der Technik leisten.

#rec11amd: Obwohl Computerprogramme keinem Gebiet der Technik (d.h. der angewandten Naturwissenschaft) angehören, können sie verwendet werden, um technische Erfindungen zu beschreiben und zu steuern.

#rec11just: Der Text der Kommission erklärt Computerprogramme zu technischen Erfindungen.  Er beseitigt das unabhängige Erfordernis, dass eine Erfindung (= ein technischer Beitrag) vorliegen muss, und vermengt es mit dem Erfordernis des Nichtnaheliegens (Erfindungsschrittes).  Dies führt zu Widersprüchen in der Theorie und unerwünschten Folgen in der Praxis.  Mehr dazu findet sich in der Rechtfertigung unseres Änderungsvorschlages zu 4(2).

#Ahi: Folglich erfüllt eine Erfindung, die keinen technischen Beitrag zum Stand der Technik leistet, z. B. weil dem besonderen Beitrag die Technizität fehlt, nicht das Kriterium der erfinderischen Tätigkeit und ist somit nicht patentierbar.

#Aho: Folglich ist eine Lehre, die keinen technischen Beitrag zum Stand der Technik leistet, keine Erfindung im Sinne des Patentrechts.

#pef: Der Kommissionstext mischt das Erfordernis des %(q:technischen Beitrag) in das Erfordernis des Nichtnaheliegens %(q:Erfindungsschrittes) hinein, schwächt so beide Erfordernisse und eröffnet einen unendlichen Raum für willkürliche Auslegungen.  Diese Vermengung ist unverständlich, steht im Widerspruch zu Art 52 EPÜ, und führt zu prkatischen Problemen.  Insbesondere wird die Prüfung der Patentierbarkeit bei den meisten nationalen Patentämtern dadurch unmöglich gemacht.  Mehr dazu in der Begründung des Änderungsvorschlages zu 4(2).

#Aaw: Wenn eine festgelegte Prozedur oder Handlungsfolge in einer Vorrichtung, z. B. einem Computer, abläuft, kann sie einen technischen Beitrag zum Stand der Technik leisten und somit eine patentierbare Erfindung darstellen. Dagegen besitzt ein Algorithmus, der ohne Bezug zu einer physischen Umgebung definiert ist, keinen technischen Charakter; er stellt somit keine patentierbare Erfindung dar.

#Aao: Bei einer festgelegten Prozedur oder Handlungsfolge, die in einer Vorrichtung, z. B. einem Computer, abläuft, kann es sich insoweit um eine technische Erfindung handeln, wie darin bislang unbekannte Wirkungszusammenhänge beherrschbarer Naturkräfte genutzt werden. Dagegen weist ein Algorithmus, gleichgültig ob die symbolischen Elemente, aus denen er besteht, als Bezüge zu einer physischen Umgebung interpretiert werden können, keinen technischen Charakter auf.  Die Kombination eines Algorithmus mit nicht-erfinderischen Realisierungen abstrakter Maschinen ergibt keine patentierbare Erfindung.

#tij: Der Kommissions- und JURI-Entwurf macht ALgorithmen patentierbar, sofern sie unter Bezugnahme auf einen Universalrechner (Allzweck-Datenverarbeitungsanlage) beansprucht werden.  Damit sind Algorithmen in ihrer üblichen Darstellungsform und allen praktisch denkbaren Anwendungen abgedeckt.

#defalgo: Gemäß dem üblichen Verständnis der Informatik ist ein Algorithmus eine %(q:Regel zum Rechnen mit abstrakten Einheiten, derart, dass dass das Ergebnis der Rechnung unabhängig von jeder möglichen physischen Umgebung ist, der diese Einheiten zugeordnet werden könnten.)

#dixitknuth: Die Kommission un JURI versuchen, eine Unterscheidung zwichen physischen und nicht-physischen (%(q:technischen) und %(q:nicht-technischen)) Algorithmen einzuführen.  Damit wiederholen sie einen bekannten Fehler, über den der Informatik-Pionier Professor Donald Knuth 1994 in einem Schreiben an das US-Patentamt spottete:

#aha: Man hat mir gesagt, dass die Gerichte versuchen, zwischen mathematischen und nichtmathematischen Algorithmen zu unterscheiden.  Für einen Informatiker ergibt das keinen Sinn, denn jeder Algorithmus ist so mathematisch wie irgendetwas nur sein könnte.  Ein Algorithmus ist ein von den Naturgesetzen des Universums unabhängiges abstraktes Konzept.

#mca: Die Vorstellung, Gesetze zu verabschieden, die behaupten, bestimmte Algorithmen gehörten zur Mathematik und andere nicht erscheint mir so absurd wie die Versuche der Gesetzgeber des Bundesstaates Indiana im 19. Jahrhundert, per Gesetz zu verfügen, dass das Verhältnis des Umfangs eines Kreises zu seinem Durchmesser genau 3 und nicht etwa annähernd 3,1416 beträgt.  Das ist wie die mittelalterliche Kirche, die entschied, dass die Sonne um die Erde kreist.  Von Menschen geschaffene Gesetze können sehr hilfreich sein, aber nur solange sie nicht grundlegenden Wahrheiten widersprechen.

#WWe: Die Tatsache, dass eine Methode es ermöglichen kann, dass Daten schneller, effizienter oder mit weniger Speicherbedarf verarbeitet werden, genügt nicht, um eine solche Methode patentierbar zu machen, wenn sie es andernfalls nicht wäre.

#Woa: In den Worten des deutschen Bundespatentgerichtes (Suche Fehlerhafter Zeichenketten, 26. März 2002, BPatG 17 W (pat) 69/98):

#osm: Würde Computerimplementierungen von nichttechnischen Verfahren schon deshalb technischer Charakter zugestanden, weil sie jeweils unterschiedliche spezifische Eigenschaften zeigen, etwa weniger Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur Konsequenz dass jeglicher Computerimplementierung technischer Charakter zuzubilligen wäre.

#Tet: Um computerimplementierte Erfindungen rechtlich zu schützen, sollten keine getrennten Rechtsvorschriften erforderlich sein, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sollten auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen liefern, und lediglich in bestimmten Punkten, die in dieser Richtlinie dargelegt sind, angepasst oder ergänzt werden.

#sed: Der Zweck der Richtlinie ist es, die Entschlossenheit des Gesetzgebers zur Begrenzung der Patentierbarkeit im Geiste von Art 52 EPÜ zu bekräftigen.  Der Änderungsantrag zitiert den Titel %(q:Patentfähige Erfindungen) von Art 52 EPÜ.  Die Richtlinie sollte jegliche Behauptung einer Patentfähigkeit von undefinierten oder unzureichend abgegrenzten Kategorien wie %(q:computer-implementierte Erfindungen) vermeiden.

#Tsc: Diese Richtlinie sollte sich auf die Festlegung bestimmter Patentierbarkeitsgrundsätze beschränken; im Wesentlichen sollen diese Grundsätze einerseits die Schutzfähigkeit von Erfindungen sicherstellen, die einem Gebiet der Technik zugehören und einen technischen Beitrag zum Stand der Technik leisten, andererseits Erfindungen vom Schutz ausschließen, die keinen technischen Beitrag zum Stand der Technik leisten.

#Tho: Diese Richtlinie sollte sich darauf beschränken, die Grenzen der Patentierbarkeit im Hinblick auf Datenverarbeitungsprogramme zu erläutern, wobei insbesondere sicher gestellt werden soll, dass %(s:technische Erfindungen patentierbar und Organisations- und Rechenregeln nicht patentierbar) sein sollen.

#eea: Der Begriff %(q:computer-implementierte Erfindungen) ist für seinen Missbrauch geschaffen, wie im Änderungsantrag zum Titel der Ricthlinie erklärt.

#Ttu: Die Wettbewerbsposition der europäischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern würde sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausgeräumt würden und die Rechtslage transparenter wäre.

#End: Es kommt der Rechtssicherheit und den Wettbewerbschancen der europäischen Softwareunternehmer im Vergleich zu denen von Europas wichtigsten Handelspartnern zugute, wenn nunmehr %(s:klar gestellt wird, dass Europa die Patentierbarkeit nicht über den Bereich der technischen Erfindungen hinaus ausdehnt).

#xWa: Unification of caselaw in itself is not a guarantee of improvement of the situation of European industry.  This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries.  Also, as explained above, the term %(q:computer-implemented invention) implies that programs for computer are patentable inventions and thereby calls for violation of Art 52 EPC.

#dte: At the international level, Europe is ahead in the area of open, alternative development and licensing approaches to computer programs, e.g. open source projects under the %(q:GNU General Public License). Particularly in the light of increasing requirements for stability, interoperability and IT security of computer programs, open source computer programs developed on a common, ongoing and transparent basis are gaining in importance.  In order to turn this European lead in terms of development into a real competitive advantage, a reliable legal framework for such alternative development and licensing approaches must be maintained.

#Wsi: This is JURI Amendment 35 as submitted by Evelyn Gebhardt but with a slightly simplified last sentence.  This amendment proposal states a generally acknowledged European policy goal.  It is to be wondered why it didn't find the support of the JURI majority.

#TWn2: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Aod: Urheberrechtlich zulässige Handlungen gemäß der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen, insbesondere deren Vorschriften über die Dekompilierung und die Interoperabilität, oder die Vorschriften über Marken oder Halbleitertopografien sollen unberührt bleiben von dem Patentschutz für Erfindungen aufgrund diese Richtlinie.

#Teh: Ähnlich wie die Richtlinie 9/50/EWG über den Rechtsschutz von Coputerprogrammen durch das Urheberrecht soll auch diese Richtlinie sicherstellen, dass Handlungen, die nur der Herstellung von Interoperabilität mit existierenden Systemen und Normen notwendig dienen, nicht verboten werden können.

#ntl: The European Commission text is misleading.  It pretends to ensure a right to interoperate but in fact does exactly the opposite.  Patents grant much broader exclusivity than copyright.  Therefore the interoperability exemptions for patents also need to be broader than those for copyright.  Limiting them to the scope offered by existing copyright laws is means of assuring that patentee interests override interoperability interests.

#SjW: Gemäß Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiaritätsprinzip tätig werden, da die Ziele der vorgeschlagenen Maßnahme, also die Harmonisierung der nationalen Vorschriften für computerimplementierte Erfindungen, auf Ebene der Mitgliedstaaten nicht ausreichend erreicht werden können und daher wegen ihres Umfangs oder ihrer Wirkung besser auf Gemeinschaftsebene erreicht werden können. Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verhältnismäßigkeit, da sie nicht über das für die Erreichung der Ziele erforderliche Maß hinausgeht -

#yWn: Terms such as %(q:protection) and %(q:computer-implemented invention) are misleading, as explained in the title amendment.

#HTD: HABEN FOLGENDE RICHTLINIE ERLASSEN:

#Sco: Anwendungsbereich

#Dit: Begriffsbestimmungen

#techkamp: Gebiete der Technik

#Cse: Voraussetzungen der Patentierbarkeit

#Cnl: Ansprüche und Rechte

#rIb: Interoperability

#Mir: Beobachtung

#ReW: Berichte über die Auswirkungen der Richtlinie

#Iet: Umsetzung

#EWW2: Inkrafttreten

#Ars: Adressaten

#Tsm: Diese Richtlinie legt Vorschriften für die Patentierbarkeit computerimplementierter Erfindungen fest.

#Tap: Diese Richtlinie erklärt die Regeln über die Grenzen der Patentierbarkeit im Hinblick auf Programme für Datenverarbeitungsanlagen.

#eWo: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the EPO in May 2000 in the Trilateral Website document %(APP), where it served to legitimate patents on %(q:computer-implemented business methods), so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a hidden programmatic statement.  It implies that algorithms, business methods and in fact all ideas are patentable inventions, provided that they are claimed in terms of general-purpose data processing equipment.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.  It can not be the aim of the current directive to declare programs for computers to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to computer programs, and this must be expressed clearly and unambiguously.

#ume: Computer-Implementierte Erfindung

#otv: Technik, Erfindung, Nicht-Erfindungen

#FWW: Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen:

#kompinvcec: %(q:Computerimplementierte Erfindung) ist jede Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#kompinv: %(q:Computerisierte Erfindung), auch %(q:computer-implementierte Erfindung) genannt, bedeutet eine Neuerung, deren Umsetzung den Gebrauch eines Datenverarbeitungssystems in Verbindung mit peripheren Geräten erfordert, und die, aufgrund der Art der Nutzung der peripheren Geräte, eine Erfindung im Sinne des Patentrechts darstellt.

#kompinvjust: Der Begriff %(q:computer-implementierte Erfindung) ist dem Computer-Fachmann nicht geläufig.  Er ist in der Tat überhaupt nicht im allgemeinen Gebrauch.  Er wurde vom Europäischen Patentamt (EPA) im Mai 2000 als Anhang 6 der Dokumentation der Trilateralen Konferenz zwischen dem Amerikanischen, Europäischen und Japanischen Patentamt eingeführt, wo er dazu diente, die Patentierbarkeit %(q:computer-implementierter Geschäftsmethoden) in Europa zu verankern und nachzuweisen, dass das EPA den von US und Japan vorgegebenen %(q:internationalen Standard) trotz ungeliebter heimischer Gesetze weitestgehend umgesetzt hat.  Die Änderung beseitigt die Verwirrung und bringt den Begriff wieder in Einklang Art 52 EPÜ.  Sie stellt klar, dass eine erfinderische Waschmaschine nicht dadurch zur Nicht-Erfindung wird, dass sie von einem eingebetteten Rechner gesteuert wird.  In einer EU-Presseerklärung (%(q:MEPs vote to tighten up rules on patentability of computerised inventions), Date: 2003-06-18, on cordis.lu) über diese Richtlinie, wurde der Begriff %(q:computerisierte Erfindungen) eingeführt, und es wurde erklärt, dass %(q:solche Erfindungen nicht normale Softwareprogramme umfassen, sondern vielmehr Lösungen für Geräte wie Mobiltelefone, intelligente Haushaltsgeräte, Maschinensteuerungen, ...).  In der Tat stellt der Begriff %(q:computerisierte Erfindung) verständlicher als %(q:computer-implementierte Erfindung) dar, was die Gesetzgeber als patentfähgig ansehen wollen: den erfinderischen Gebrauch von Geräten, die der Steuerung durch ein Datenverarbeitungssystem unterworfen wurden.

#cWi: %(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der für eine fachkundige Person nicht nahe liegend ist.

#cua: %(q:Technischer Beitrag), auch %(q:Erfindung) genannt, bedeutet a %(q:Beitrag zum Stand der Kunst auf einem technischen Gebiet).

#Wad: Der Kommissionstext mischt das Erfordernis der Erfindung (des technischen Beitrags) in das Erfordernis des Nicht-Naheliegens (des erfindungerischen Schrittes), schwächt damit beide Erfordernisse, weicht von Art 52 EPÜ ab, und schafft theoretische ebenso wie praktische Probleme (z.B. wird Prüfung der Patentfähigkeit an den meisten nationalen Patentämtern unmöglich gemacht), s. Begründung des Änderungsantrages zu 4(2).

#applsci: %(q:Technik) im Sinne des Patentrechts ist %(angewandte Naturwissenschaft).

#qce: The Commission text shift the question of what is a patentable invention to the question of what is %(q:technical) and then leaves that question unanswered.  The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day caselaw and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such but inventions related to mobile phones, engine control devices, household appliances, ...)) .  This understanding should be made explicit for the purpose of clarification.

#Ain: %(q:Technology) in the sense of patent law is %(q:use of controllable forces of nature).

#cnW: %(q:Technical) in the sense of patent law means %(q:concrete and physical).

#Wxb: Eine %(e:Erfindung) im Sinne des Patntrechts ist eine %(e:technische Lösung eines technischen Problems).

#Coa: This is a common EPC doctrine which has been regularly used by most patent courts, including the Technical Chambers of Appeal of the EPO.

#techinv: %(q:Erfindung) im Sinne des Patentrechts bedeutet %(q:Lösung eines Problems durch Einsatz beherrschbarer Naturkräfte).

#techinvjust: This is a standard patent doctrine in most jurisdictions.  The EPO says that inventions are %(q:technical solutions of technical problems) and understands %(q:technical) as %(q:concrete and physical).  The term %(q:controllable forces of nature) clarifies this further.  The %(q:four forces of nature) are an acknowledged concept of epistemology (theory of science).  While mathematics is abstract and unrelated related to %(e:forces of nature), some business methods may well depend on the chemistry of the customer's brain cells, which is however not %(e:controllable), i.e. non-deterministic, subject to free will.  Thus the term %(q:controllable forces of nature) clearly excludes what needs to be excluded and yet provides enough flexibility for inclusion of possible future fields of applied natural science beyond the currently acknowledged %(q:4 forces of nature).  This concept has been formulated in most jurisdictions and even written into the law in some countries such as Japan and Poland.  Even the CEC and JURI proposals say that %(q:algorithms and business methods are inherently non technical), and the JURI report associates %(q:technical contributions) with %(q:mobile phones, household appliances, engine control devices, ...).  The classical justification for the %(q:technical character) of %(q:computer-implemented inventions) is not that the meaning of %(q:technical) has changed but that the computer indeed consumes energy in a controlled way, and that the %(q:invention) must be %(q:considered as a whole).  The critics of this view, e.g. the German Federal Patent Court, argue that %(q:the solution is completed by abstract calculation before, during its non-inventive implementation on a conventional data processing system, forces of nature come into play).

#wcn: An %(q:invention) in the sense of patent law, also called %(q:technical teaching), %(q:technical solution) or %(q:technical contribution), is a %(e:teaching of plan-conformant use of controllable forces of nature to immediately, without mediation by human reason, achieve a causally overseeable result.)

#nol: This is a frequently used definition, as used by the German Federal Court of Justice in its %(q:Anti-Blocking System) decision of 1980.  It allowed the court to accept a patent for a computer-controlled anti-blocking system while rejecting a patent for more abstract computing problems such as determining optimal lengths of rolling rods or optimal distribution of air-plane fuel based on flying distances.

#Aoj3: Eine %(e:Rechenregel), auch %(e:Algorithmus) genannt, ist eine Lehre über Beziehungen innerhalb von gedanklichen Gebilden wie z.B. Modellen und Axiomsystemen, einschließlich der Turing-Maschine.

#AlB: Eine %(e:Organisationsregel) ist eine Lehre über Beziehungen zwischen dinglichen Erscheinungen (materiellen Phänomenen), die nicht von beherrschbaren Naturkräften bestimmt sind.  Geschäftsmethoden und soziale Techniken sind Organisationsregeln.  Die meisten Organisationsregeln sind Anwendungen von Rechenregeln auf bekannte Modelle gesellschaftlicher Beziehungen.  %(q:Organisations- und Rechenregeln) sind Lehren über Wirkungszusammenhänge, die nicht von beherrschbaren Naturkräften bestimmt sind.

#Wtu: These additional definitions could help to establish terminology which can be used in order to refer to various types of non-inventions.  The generic term %(q:rules of organisation and calculation) is taken from the German patent jurisdiction of %(dp:Dispositionsprogramm) and later.

#CeW: Gebiet der Technik

#iug: The article specifies the relation of the European invention concept with Art 27 TRIPs.  Thus we are talking about fields of technology in the plural, and inventions can only %(q:belong) to such a field.  To which field they may belong is not determined by the means of implementation.  Note that the title in the french and german versions of the CEC proposal differs from that in the english version.

#Mue: Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugehörig gilt.

#uan: Die Mitliedsstaaten sorgen dafür, dass eine Innovation nicht lediglich deshalb als einem Gebiet der Technik zugehörig gilt, weil ihre Umsetzung den Einsatz einer Datenverarbeitungsanlage erfordert.

#tnT: The European Commission text says that all ideas, including %(q:computer-implemented business methods) etc, are patentable inventions.  This has been widely criticised.  See Justification for 3a.

#lrL: One of the chief objectives of the directive project has been to clarify the interpretation of Art 52 EPC in the light of Art 27 TRIPs, which says that %(q:inventions all fields of technology) must be patentable.  We propose to translate of Art 52(2) EPC into the language of Art 27 TRIPs, along the lines of Amendment CULT-16.

#oaf: If data processing is %(q:technical), then anything is %(q:technical).

#otg: Data processing is a common denominator of all fields of technology and non-technology.  With the advent of the universal computer in the 1950s, automated data processing (ADP) became pervasive in society and industry.

#d1w: As Gert Kolle, a leading theoretician behind the decisions of the 1970s to exclude software from patentability, writes in 1977 (see %(URL)):

#Wmr: Die ADV ist heute zu einem unentbehrlichen Hilfsmittel in allen Bereichen der menschlichen Gesellschaft geworden und wird dies auch in Zukunft bleiben.  Sie ist ubiquitär.  ...  Ihre instrumendale Bedeutung, ihre Hilfs- und Dienstleistungsfunktion unterscheidet die ADV von den ... Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden ... von allen Wirtschaftsunternehmen benötigt werden und für die daher prima facie ein Freihaltungsbedürfnis indiziert ist.

#dnW: CULT was well-advised to vote for a clear exclusion of data processing from the scope of %(q:technology).

#yag: Numerous studies, some of them conducted by EU instiutions as well as the opinions of the European Economic and Social Committee and the European Comittee of Regions explain in detail why Europe's economy will suffer damage, if data processing innovations are not clearly excluded from patentability.

#Mii: Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen Tätigkeit beruht.

#MaW: Die Mitgliedsstaaten sorgen dafür, dass Patente nur für Erfindungen (auch %(q:technische Erfindungen), %(q:technische Beiträge), %(q:technische Lehren) oder %(q:technische Lösungen) genannt) erteilt werden, die neu, nicht naheliegend und industriell anwendbar sind.

#IWt: Saying that %(q:inventions are patentable) is tautological in the context of patent law.  The question is: what is an invention?  The Commission text seems to be saying that any solution which is implemented by a computer is an invention, and that there is no invention test but only tests of novelty, non-obviousness and industrial applicability.   Thereby the Commission text drastically changes the rules of Art 52 EPC and allows unlimited patentability.   It would possible to delete the Commission's Article 4 entirely, because, once the errors are corrected, we arrive at little more than a literal restatement of Art 52 EPC.  Yet, in order to correct a recent practise of the EPO, it may be a good idea to restate that the %(q:technical contribution) is a synonym of %(q:invention) and unrelated to the %(q:inventive step) test.  This has been pointed out by many EPO-friendly law scholars, see e.g. the dissertation of Ralph Nack, whom the Commission cites in its explanatory memorandum.

#art42cec: Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen Tätigkeit nur erfüllt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet.

#art42amd: Die Mitgliedsstaaten sorgen dafür, dass eine Erfindung im Sinne des Patentrechtes nur dann vorliegt, wenn die Lösung des Problems, unabhängig davon, ob ihre Umsetzung den Einsatz einer Datenverarbeitungsanlage erfordert oder nicht, technischen Charakter aufweist.

#art42just: Nichtnaheliegen (= %(q:erfinderische Tätigkeit / erfinderischer Schritt)) und das Vorliegen einer Technischen Erfindung (= %(q:eines technischen Beitrages)) sind zwei getrennte Erfodernisse.  Ihre Vermengung in ein einziges Erfordernis ist unverständlich und führt zu praktischen Problemen, u.a. dazu, dass die Erfindung nicht neu sein muss und dass Patentämter Patentanträge auf Nicht-Erfindungen nicht mehr abweisen dürfen, ohne zuvor eine aufwendige Suche nach vorbekannter Technik durchzuführen.

#aea: Der Text der Kommission öffnet mehrere Türen zur grenzenlosen Patentierbarkeit, indem er von Art 52 EPÜ abweicht.  Die %(EP) erklären Art 52 EPÜ folgendermaßen.

#epogl78: EPA-Prüfungsrichtlinien

#Tsf: Die Patentierbarkeit setzt voraus, dass alle folgenden vier grundlegenden Bedingungen erfüllt werden:

#Tbi: Es muss eine %(qc:Erfindung) vorliegen.

#Tcd2: Die Erfindung muss %(qc:gewerblich anwendbar) sein.

#Tie: Die Erfindung muss %(qc:neu) sein.

#Ttx: Die Erfindung muss auf einer %(qc:erfinderischen Tätigkeit) beruhen.

#Ihp: Außer diesen vier grundlegenden Bedingungen hat der Prüfer die beiden folgenden Erfordernisse zu beachten, die implizit im Übereinkommen und in der Ausführungsordnung enthalten sind.

#Taa: Die Erfindung muss so beschaffen sein, dass ein Fachmann sie [...] ausführen kann

#Tce: Die Erfindung muss .. technischen Charakter haben [...].

#Wse: Der Kommissionstext weicht von Art 52 EPÜ dadurch ab, dass er die unabhängigen Erfordernisse %(q:technische Erfindung) (= %(q:technischer Beitrag)) und %(q:erfinderische Tätigkeit) (Nichtnaheliegen) zu einem einzigen Erfordernis vermengt.  Dies ist unverständlich und widersprüchlich, wie selbst die Kommission in ihrem Erläuternden Vorwort indirekt eingesteht:

#htr: Das Vorliegen eines %(q:technischen Beitrages) ist ... im Rahmen der Erfinderischen Tätigkeit zu prüfen.  Die Erfahrung hat gezeigt, dass dieser Ansatz in der Praxis leichter anzuwenden ist.

#Waf: Indeed the approach has no theoretical merits but significant practical effects.  It opens an infinite space of interpretation for those patent offices that conduct a full substantive examination, such as the EPO and the German Patent Office, while preventing other patent offices (such as French, Italian) from rejecting patents on non-inventions.

#nee: Furthermore, treating %(q:computer-implemented) inventions in a special way, as suggested by the European Commission, runs counter to Art 27 TRIPs. And, as has been pointed out above, the term %(q:computer-implemented invention) itself may run counter to Art 52 EPC by implying that algorithms framed in terms of generic computing equipment (programs for computers) are patentable inventions.

#era: The amendment solves all these problems by restating the structure of Art 52 EPC as explained in the EPO's Examination Guidelines.

#art43cec: Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmalen umfassen kann, vom Stand der Technik abhebt.

#art43amd: Der technische Beitrag ist durch Betrachtung des Unterschiedes zwischen dem Umfang der technischen Merkmale des Patentanspruchs als ganzem und dem Stand der Technik zu ermitteln.

#art43just: Die Kommissions- und JURI-Texte bedeuten, dass ein %(q:technischer Beitrag) ausschließlich aus nicht-technischen Merkmalen bestehen kann.  Dies ist selbstwidersprüchlich und führt zu unbegrenzter Patentierbarkeit.  Änderung CULT-15 korrigiert den Fehler, soweit dies im Rahmen der Anspruchsmerkmal-Arithmetik überhaupt möglich ist.

#nno2: Bei der Prüfung der Frage, ob eine technische Erfindung vorliegt, sollte man die Form oder die Art des Patentanspruchs außer acht lassen und sich auf den Inhalt konzentrieren, um festzustellen, welchen neuen Beitrag der beanspruchte Gegenstand zum Stand der Technik leistet.  Stellt dieser Beitrag keine Erfindung dar, so liegt kein patentierbarer Gegenstand vor.

#WoW4: %(q:Anspruchsmerkamle) können kein sinnvoller Gegenstand gesetzlicher Regelung sein.  Die Änderung ist wörtlich den %(EP) entnommen.  Sie fordert den Prüfer dazu auf, die Form des Anspruchs zu ignorieren und allen Versuchen, die Prozedur der Patentprüfung zu formalisieren, zu widerstehen.  Jede Formalisierung macht den Anmelder zum Herrn der Prüfung und fördert eine unkritische Haltung auf der Seite des Prüfers.

#bpatg02: Die Mitgliedsstaaten sorgen dafür, dass computer-implementierte Lösungen technischer Probleme nicht lediglich deshalb als patentfähige Erfindungen angesehen werden, weil sie Einsparungen von Ressourcen innerhalb eines Datenverarbeitungssystems ermöglichen.

#bpatg02just1: Diese Änderung spiegelt derzeitiges deutsches Fallrecht wieder.  In den Worten des 17. Senats des Bundespatentgerichts (BPatG, Entscheidung vom 26. März 2002, 17 W (pat) 69/98, %(URL)):

#bpatg02just2: Ein entscheidendes Indiz für die Technizität des Verfahrens sieht die Anmelderin darin, dass es auf einer technischen Problemstellung beruht. Dadurch, dass das vorgeschlagene Verfahren nicht mit einem Wörterbuch arbeite, könne der Speicherplatz hierfür entfallen. ...  Was die technische Aufgabenstellung anlangt, so kann hierin nur ein Indiz, nicht aber ein Nachweis für die Technizität des Verfahrens gesehen werden.  Würde Computerimplementierungen von nichttechnischen Verfahren schon deshalb technischer Charakter zugestanden, weil sie jeweils unterschiedliche spezifische Eigenschaften zeigen, etwa weniger Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur Konsequenz dass jeglicher Computerimplementierung technischer Charakter zuzubilligen wäre. Denn jedes andersartige Verfahren zeigt bei seiner Implementierung andersartige Eigenschaften, erweist sich entweder als besonders rechenzeitsparend oder als speicherplatzsparend. Diese Eigenschaften beruhen - jedenfalls im vorliegenden Fall - nicht auf einer technischen Leistung, sondern sind durch das gewählte nichttechnische Verfahren vorgegeben. Würde schon das Erfüllen einer solchen Aufgabenstellung den technischen Charakter einer Computerimplementierung begründen, so wäre jeder Implementierung eines nichttechnischen Verfahrens Patentierbarkeit zuzubilligen; dies aber liefe der Folgerung des Bundesgerichtshofs zuwider, dass das gesetzliche Patentierungsverbot für Computerprogramme verbiete, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten.

#FWc: Form des Patentanspruchs

#pte: We are interested in claim forms only to the extent that they imply certain rights and obligations.  The aim of this provision is to explain how patent rights are limited by the freedom of publication (Art 10 ECHR) and other information-related freedom rights.

#art5cec: Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausführung von Software verwirklicht wird.

#produktjust: Dieser Artikel erklärt die Bedeutung der Begriffe %(q:Produkt) und %(q:Prozess) im Kontext von computerisierten Erfindungen.  Die Originalversion interpretiert beide Begriffe korrekt, aber hat eine unerwünschten Nebenwirkung: sie suggeriert, dass in den Begriffen des Universalrechners formulierte Algorithmen (Programme für Datenverarbeitungsanlagen als solche) patentfähige %(q:Erfindungen) sind oder sein können.  Die Änderung korrigiert den Fehler.  Die erfindungsrelevanten Erzeugnisse und Verfahren sind nicht durch das Datenverarbeitungsssytem sondern durch die daran angeschlossenen peripheren Geräte gekennzeichnet.  Bei letzteren könnte es sich z.B. um eine Automobilbremse, einen Gummihärtungsofen oder eine Waschmaschine handeln.

#publig: Die Mitgliedsstaaten sorgen dafür, dass die Veröffentlichung und Verbreitung von Computerprogrammen oder sonstigen Urheberrechtsgegenständen niemals eine direkte oder indirekte Patentverletzung darstellen kann.

#publigjust: Hiermit wird erklärt, wie Patentrechte durch andere Rechtsgüter, vor allem die Veröffentlichungsfreiheit (Art 10 EKMR) und das Eigentum an individuellen Schöpfungen (geistiges Eigentum, im Falle der Software: Urheberrecht), beschränkt werden.  Diese Änderung macht deutlicher, dass sowohl die Veröffentlichungsfreiheit als auch das Recht zur Beschränkung der Verwendung eigenschöpferischer Werke (Urheberrecht) grundlegender als Patentrechte sind und somit eine Schranke des zulässigen Umfangs von Patentrechten darstellen.   Wo das Urheberrecht anwendbar ist, sind Patente nicht anwendbar, und umgekehrt.  Das Prinzip der %(q:maximalen Trennung der Bereiche des Geistigen Eigentums) fand in den Entscheidungen %(LST) des deutschen Bundesgerichtshofes mustergültige Formulierung und unterliegt dem Art 52 des Europäischen Patentübereinkommens.

#kompexe: Die Mitgliedstaaten sorgen dafür, dass die Ausführung eines Programms auf einer Datenverarbeitungsanlage (d.h. Rechner mit Schnittstellen zum Austausch von Daten mit Personen oder Rechnern) keine Patentverletzung darstellen kann, wohingegen eine Kombinationshandlung, wie z.B. der Vertrieb von programmgesteuert hergestellten Chemikalien, sehr wohl eine Patentverletzung darstellen kann.

#kompexejust: This is another concretisation the core principle of this directive in the context of patent enforcability.  It is based on the distinction between data processing equipment (system of processors with devices for storage and manipulaton of data and for exchange of data with humans and computers) and peripheral devices such as automobile brakes or rubber-curing furncases, which maniuplate material phenomena (forces of nature) rather than information.

#scrambled: Die Mitgliedsstaaten stellen sicher, dass digitale Information als öffentliches Wissen gilt, sobald es der Öffentlichkeit faktisch möglich ist, diese Information einem veröffentlichten Werk zu entnehmen, auch dann, wenn die Entnahme eine Dekodierung voraussetzte.

#lli: This is to ensure that information (software, databases etc) that is published in closed-source (binary) form can, just like information that is openly accessible, be cited as prior art against a non-novel patent.  A generous understanding of what can constitute prior art is one of very few possible ways of reducing the number of trivial patents.  If software patents are permissible, this is moreover helps to reduce legal insecurity and possible abuses arising from the fact that many computer programs are published in binary form.

#prekre: Die Mitgliedsstaaten sorgen dafür, dass alle urheberrechtsfähigen Werke, die vor der Veröffentlichung eines Patentes in Gebrauch waren, auch künftig von dem Urheberrechtsinhaber und von allen Personen, denen der Urheberrechtsinhaber dies gestattet, gemäß vom Urheberrechtsinhaber zu bestimmenden Bedingungen weiterentwickelt, weiterverbreitet und zum Betrieb von Datenverarbeitungsanlagen verwendet werden dürfen.

#bWW: This provision is needed only to the extent that information (software) patents are not reliably excluded by the directive.  When the object of a patent is informational, the scope of %(q:prior use rights) becomes unclear:  it becomes either everything or nothing.  %(q:Prior use right) means %(q:the right to continue to commercially exploit the idea oneself within certain limits).   In the case of information products, such limits cannot be defined.  Information does not require distribution efforts but at best more or less futile efforts to restrict distribution.  Valuable software tends to become freely available with source code sooner or later in its lifecycle.  Patents should be an incentive to publication.  When chosing between %(q:everything or nothing), we say %(q:everything).

#progpub: Die Mitgliedsstaaten stellen sicher, dass in allen Fällen, in denen in einem Patentanspruch Merkmale genannt sind, die die Verwendung eines Computerprogramms erfordern, eine gut funktionierende und gut dokumentierte Referenzimplementation eines solchen Programms als Teil der Patentbeschreibung ohne einschränkende Lizenzbedingungen veröffentlicht wird.

#oWc: Patents are two-sided deals: the inventor discloses information in return for a monopoly.  Computer programs are information, whereas the monopoly affects only physical objects.  Nothing describes a computerised process more accurately than a well written program.  This provision is not designed to promote open-source software but rather to make the patent system take its function of knowledge disclosure seriously.  It is similar to ITRE-9, CULT-31 and CULT-32.

#RWi: Konkurrenz zur Richtlinie 91/250/EWG

#rtW: The title should clearly specify the purpose of the provision rather than secondary aspects such as which other laws may at present be involved in achieving this purpose.

#dekompil: Zulässige Handlungen im Sinne der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften über die Dekompilierung und die Interoperabilität, oder im Sinne der Vorschriften über Marken oder Halbleitertopografien bleiben vom Patentschutz für Erfindungen aufgrund dieser Richtlinie unberührt.

#abo: Acts which are permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, such as the reimplementation of a program's interfaces for the sake of interoperability, shall not be forbidden by patents.

#dekompiljust: The Commission's text fails to protect interoperability.  Decompilation is not an issue, because patents do not forbid decompilation anyway.  Patent-specific problems need patent-specific solutions.  The Commission's text offers only copyright-specific solutions to patent-specific problems, therebey effectively giving patentees the right to block the use of an idea for purposes of interoperability.  The amdendment ensures that patents are not used as a means to obtain control rights over software which copyright does not offer.

#interop: Die Mitgliedsstaaten sorgen dafür, dass überall dort, wo ein Gebrauch einer patentierten Technik zur Kommunikation mit dem anderen System (d.h. zur Umwandlung in die und aus den Konventionen dieses Systems) erforderlich ist, ein solcher Gebrauch nicht als Verletzung angesehen wird.

#interopjust: Hierbei handelt es sich um ITRE-15 mit einer kleinen Änderung: statt %(q:Computer-System oder Netzwerk) heißt es %(q:Datenverarbeitungssystem).  Dadurch wird klar gestellt, dass es nicht lediglich um die Interoperabilität zwischen Rechnerarchitekturen (z.B. IBMPC und Mac) sondern zwischen Software-Systemen aller Art geht.   Wie die ITRE-Begründung sagt: %(bq:Die Möglichkeit, Geräte zu verbinden um sie interoperabel zu machen ist eine Art, für offene Netzwerke zu sorgen und den Missbrauch dominanter Stellungen zu verhindern.  Dies ist insbesondere vom Fallrecht des EuGH entschieden worden.  Das Patentrecht sollte es nicht möglich machen, dieses Prinzip auf Kosten des freien Wettbewerbs und der Anwender außer Kraft zu setzen.)

#Tnt: Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen und den elektronischen Geschäftsverkehr auswirken.

#lWs: Das Parlament beobachtet, wie sich Patente auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen und den elektronischen Geschäftsverkehr auswirken.

#swW: Patents are controversial, because they may exclude people from a wide range of activities, thus limiting people's freedom of action and potentially colliding with important civil liberties.   On the other hands, patents can lead to a concentration of pecuniary beneficts in the hands of small and politically active groups.  It is therefore difficult to assure the independence and impartiality of any state-sponsored monitoring activity.  At the European Commission, monitoring and consultation work to date has been concentrated in the hands of a few civil servants from DG Internal Market with close affinity to the patent system.  In recent years a strong case has been made against patents in various fields such as software and genetics, and these have been reflected in consultations conducted by DG Internal Market. DG Internal Market has regularly disregarded the outcome of such consultations and instead followed the opinions of the patent establishment.  Various reports written by DG Internal Market, including a monitoring report on the gene patent directive of 2001, show an dogmatic belief in the beneficiality of patents and an unwillingness to even ask the questions needed to take empirical evidence into account.  It is clear that monitoring must be done by an independent and interdisciplinary group of scientists and not by those people who are responsible for the directive proposal whose effects are to be monitored.

#Tme2: Die Kommission legt dem Europäischen Parlament und dem Rat spätestens am [DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor über:

#WiW: The monitoring is done by a committee set up by the Parliament, see the amendment to 7(1).

#Wet: Reporting is needed not only once but at regular intervals.  In particular it cannot be expected that after 3 years much will be observable, as the EPO usually takes about 5 years to grant a patent.

#rbr: Die Kommission legt dem Europäischen Parlament und dem Rat spätestens am [DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor über:

#har: The Monitoring Committee shall publish a summary report about the insights gained by the scientific communities every two years.  The Parliament shall have the power to demand revisions based on findings of these reports.  If the direcitve is not revised to the satisfaction of the Parliament within two years, the directive shall cease to be in force.

#WWl: Reporting at regular intervals is needed, and it must be part of a real review process.

#teW: die Auswirkungen von Patenten auf computerimplementierte Erfindungen auf die in Artikel 7 genannten Faktoren,

#wtn: die Angemessenheit der Regeln für die Festlegung der Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die Neuheit, die erfinderische Tätigkeit und den eigentlichen Patentanspruch, und

#wra: etwaige Schwierigkeiten, die in Mitgliedstaaten aufgetreten sind, in denen Erfindungen vor Patenterteilung nicht auf Neuheit und Erfindungshöhe geprüft werden, und etwaige Schritte, die unternommen werden sollten, um diese Schwierigkeiten zu beseitigen.

#nno: which computer-related patents have been granted, which refused, which enforced, who the current owners are, and to what extent licenses are available to the public for each granted patent and how likely inadvertent infringement is.

#tta: to what extent the patent examination system is operating as intended, whether there are sufficient incentives to reject unjustified patent claims, what kind of reforms might be needed in order to allow for a quick and reliable separation of wanted from unwanted patents.

#drc: to what extent european professionals in software and other concerned business fields are satisfied with the current scope of exclusivity and where they see a need for a broader or narrower scope.

#tie: The report should lead to real insights about the current functioning and effects of the patent system which should be appreciated and undergo the test of peer review in various scientific communities.  This amendment incorporates CULT amendment 35.

#Mvf: Die Mitgliedstaaten erlassen die erforderlichen Rechts­ und Verwaltungsvorschriften, um dieser Richtlinie spätestens am [DATUM (letzter Tag des betreffenden Monats)] nachzukommen. Sie setzen die Kommission unverzüglich davon in Kenntnis.

#WDi: Bei Erlass dieser Vorschriften nehmen die Mitgliedstaaten in den Vorschriften selbst oder durch einen Hinweis bei der amtlichen Veröffentlichung auf diese Richtlinie Bezug. Die Mitgliedstaaten regeln die Einzelheiten der Bezugnahme.

#Mia: Die Mitgliedstaaten übermitteln der Kommission den Wortlaut der innerstaatlichen Rechtsvorschriften, die sie im Geltungsbereich dieser Richtlinie erlassen.

#TWi2: Diese Richtlinie tritt am zwanzigsten Tag nach ihrer Veröffentlichung im Amtsblatt der Europäischen Gemeinschaften in Kraft.

#Tsh2: Diese Richtlinie ist an die Mitgliedstaaten gerichtet.

#Dau: Geschehen zu Brüssel am

#Fua: Im Namen des Europäischen Parlaments

#Fhu: Im Namen des Rates

#Pres: Der Präsident

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prop ;
# txtlang: de ;
# multlin: t ;
# End: ;

