<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Propos de Directive Brevets Logiciels et Contre-Propositions

#descr: Le 2002-02-20 la Commission Européenne a publié une proposition de directive pour définir les programmes d'ordinateur comme des inventions brevetables et rendre difficile toute tentative de refuser un brevet sur un algorithme ou une fonctionnalité logique, y compris les méthodes d'affaires, qui a été drapée dans le langage de l'informatique (ex. ordinateur, mémoire, réseaux, entrée/sortie, base de données etc).  Nous avons mis au point une contre-proposition qui protège la liberté de pensée, de calcul, d'organisation et de formulation informatique tout en soutenant les principes de brevetabilité de l'invention technique (solution de problème par forces de nature contrôlables), comme expliqué dans la Convention sur le Brevet Européen et dans les commentaires et manuels de loi classiques.  Cette contre-proposition jouit du soutien de nombreux acteurs bien connus dans le domaine du logiciel, de la politique, de l'économie et du droit.

#eak: Considérations Soujacentes

#Pem: Préambule

#TAc: Les Articles

#yii: Pour quoi une Directive?

#Prt: Couche nucléaire:  Encadrer l'Invention Technique, Réaffirmer l'Article 52 CBE

#IfW: Couche d'Extensions: Quelques Clarifications sur l'Assurabilité des Brevets dans le contexte des biens informationels

#nia: Couche de Complétion: Positivement Définir l'%(q:Invention Technique), limiter la gamme du %(q:droit des juges)

#nia2: Harmoniser un Système Unifié?

#fey: Clarifier l'Inprononçable?

#olW: L'échange politique entre UE et OEB

#dWr: Raisons Valides pour une Directive

#neW: The European Commission has proposed a directive for the %(q:patentability of computer-implemented inventions), colloquially termed %(q:software patent directive).

#rot2: The stated aim of the directive is to %(q:harmonise) and %(q:clarify) the rules of what is patentable and what not in the field of computer-related products and proceses.

#yee: Europe's patentability rules were unified by the European Patent Convention of 1973.  In cases of distortions of the internal market, the European Court of Justice is entitled to decide, no matter whether the substantial patent law which it cites is EU law or the EPC.  Moreover, preparations are under way for a centralised EU Patent Court in Luxemburg, which again would be empowerd to decide on the basis of the EPC as well as of EU directives.  So what could possibly still be left for a EU directive to %(e:harmonise)?

#Wsc: If there is nothing to %(e:harmonise), couldn't there at least be something to %(e:clarify)?  Sure, there must be.  Laws usually leave some questions open for interpretation.  Patent law in particular has large gray zones in which rules have been set by judges.  Regulating patentability would be a thorny political task.  Some even say that clarification beyond Art 52 EPC is %(dg:infeasible).

#rvn: Legal study from Amsterdam University commissioned by the European Parliament.

#Wce: Rather than narrowing the gray zones, the EU Comission proposes to further enlarge them.   Everything is made dependent on undefined terms such as %(q:technical character), and even on self-contradictory arrangments of this empty word, such as saying that a %(q:technical contribution) may consist solely of untechnical features (article 4), or that it is implied in non-obviousness (articles 1-4).  Between the lines this directive proposal seems to say:

#eWW: Member states shall ensure that national courts may not revoke any patents based on what they consider to be a consistent interpretation of the written law but only based on the authority of European judicial institutions such as the European Patent Office.

#hWt: It would be have been much clearer if it could have been reduced to this one sentence.

#tWW: So, if there is nothing to harmonise and nothing to clarify, why are EU politicians almost unanimously pushing for a directive?

#WUh: One answer could be that EU politicians are naturally in favor of anything that extends EU influence.  But that is not the whole answer.

#Whi: Europe's substantive patent law is currently not regulated by the EU but by the European Patent Organisation (EPO), an intergovernmental organisation which runs the European Patent Office (EPO) and is associated with the EU (e.g. EPO membership is precondition for EU membership) .  There has however been a movement to transfer power from the EPO to the EU.  This power transfer has been based on win-win deals, such as an increased influence of the EPO at the European Commission.  The software patent directive is such a win-win deal:  the EPO receives a license to grant software patents, while the EU is allowed to extend its legislative competence in patent matters.

#WWu: When the EU extends its competence, it usually cites the EC Treaty.  The most convenient pretext is %(q:harmonisation): somewhere in Europe there must be some remaining traces of national diversity which can be said to %(q:impede the proper functioning of the internal market).  However, this %(e:harmonisation trick) does not always work, as can be seen from some recent unsuccessful clashes of the European Commission with national governments (e.g. over the tobacco advertisement directive) before the European Court of Justice.

#Uee: There are good reasons for the EU to legislate on matters of patentability:

#fWl: Restrictions on the use of ideas affect basic liberties and can have a great impact on propsperity.  Unlike many detail questions of patent law, the basic rules concerning what is patentable need to be decided by an elected parliament.

#nWh: With the introduction of the community patent, the patent system has become a domain of EU legislation.  Without clear and strict patentability rules, a more efficient patent system can mean more efficient infliction of harm on European industry and citizens.

#bpe: Rule-setting power on questions of material patent law is currently is already europeanised.  There is a pattern of informal legislation by the European Patent Office and its Administrative Council, both of which are not elected legislative organs.

#tre: The European Patent Office has set new rules of patentability which are at odds with the existing written laws and which are followed by some but not all national courts.  The question of what is patentable is governed by two competing sets of rules and the role of an arbiter between these two is most naturally fulfilled by the European Parliament.

#ant: A few questions concerning enforcability of patents are currently still in the domain of national law and there is indeed some divergence.  Strangely enough, the European Commission has shown little interest in addressing these divergences so far, although the questions are not less capble of %(q:impeding the proper functioning of the internal market).

#arl: With some effort, this reasoning should be able to serve as a valid justification for passing a EU directive.  We have tried to word our preamble amendment proposals accordingly.

#Wei: Negative Definition of %(q:Technical Invention) in Art 52 EPC

#alP: Translating from EPC language to TRIPs language

#nof: Translating from EPC language to the  Meta-Language of Claim-Writing

#ecy: A well-fenced gray zone

#WWt: Il ne peut pas y avoir des %(q:inventions mises en oevre par ordinateur)

#eWc: The basic choice to be made is whether we adopt the rules of Art 52 EPC, as reflected in the %(ep:Examination Guidelines of 1978) and most of the EPO caselaw of the 1970-80s and patent law textbooks such as %(kr:Kraßer 1986) and %(bk:Benkard 1988), or the more %(q:liberal) patentability rules developped by the EPO step by step after the publication of the revised Examination Guidelines of 1985.

#Cra: Since the later EPO caselaw deviates from the EPC in various respects, there is, even from a purely legal point of view, no choice but to opt for the EPC.  Otherwise the directive would in fact change the law, rather than %(q:harmonise the status quo), and it might not stand up to a challenge before the European Court of Justice (ECJ).

#eva: We propose to opt for the original Art 52 EPC (negative definition of %(e:technical invention)) and to restate it som more contexts, so that it is less likely to be misunderstood.  Recent patent language is characterised by new elements such as the TRIPs treaty, which didn't exist in 1973, when the EPC was decided.  Although Art 52 EPC provides a clear and adequate structure for today's patent system and is perfectly compatible with TRIPs, it has been weakened by recent layers of interpretation, many of which aimed at creating a different legal structure.

#WWh: At the same time, we propose to correct some misconceptions of the EPO jurisdiction which are not compatible with Art 52 EPC.  These include

#hie: The term %(q:invention) must be used only in the sense of %(q:patentable invention): a synonym of %(q:technical invention), %(q:technical contribution), %(q:technical solution) or %(q:technical teaching).  It is in particular not permissible to use the term %(q:invention) to refer to non-technical ideas or to features in the claim wording which are not meant to be subjected to the tests of novelty, non-obviousness and industrial applicability.

#vno: The four tests of %(q:technical invention), %(q:novelty), %(q:non-obviousness) and %(q:industrial applicability) must be kept separate and strengthened each on its own terms.  It is in particular not permissible to mingle the %(q:technical invention) test into the %(q:inventive step) (= non-obviousness) test, as the EPO and the CEC proposal have been doing.

#rvo: We propose to translate Art 52 EPC into the terminology of Art 27 TRIPs by means of the following statements:

#datatech: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#sRh: A similar provision was adopted and voted favorably by the European Parliament's Culture commission (CULT-16).

#Wue: Moreover, in our amendment to Article 5, we propose to translate the rule into the context of patent claim writing:

#produkt: Member States shall ensure that a computerised invention may be claimed as a product, that is a set of devices connected to a data processing system, or as a process carried out by such devices.

#fWn: The exclusion of general-purpose data processing leaves room for the courts and patent offices to grant patents on computer-controlled machine tools or %(ab:anti-blocking systems), even when the novel teaching arguably lies in the realm of logics or mathematics.  Just as in Art 52 EPC, we merely exclude certain classes of objects and do not specify that the rest must be patentable.  There is nothing in this rule that could encourage lawcourts to grant patents on applied mathematics.  Our core proposal does not exhaustively define the technical invention.  Rather, it sets a clear and concrete boundary to the extension of the scope of patentability: programs for the general purpose computer are off limits, and so is the application of computers to business models, social processes and other phenomena that are unrelated to controlling forces of nature.

#oWa: Such a regulation thus would clarify some important points, and it corresponds to a %(wc:wide consensus of politicians, economists, engineers, programmers and patent experts) (in particular judges and examiners, less so attorneys).

#WWs: Read e.g. the %(sc:quotations) from all these groups.  Whenever politicians make statements about this question, they in fact plead for the border outlined here, but often their words are interpreted in a very special way by their patent officials.  Compare e.g. the quote from UK e-minister Patricia Hewitt with its interpretation by the UK Patent Office.

#ior: It is clear from the above why the term %(e:computer-implemented invention) is misleading.  An idea that can be implemented merely by means of a general-purpose computer cannot be an invention in the sense of patent law.  Rather, it is an algorithm expressed in terms of a mathematical model called Von Neumann Machine.  In the language of Art 52 EPC, it is a %(q:program for computers) or a %(pd:%(q:program for data processing equipment)) in the narrowest sense, and it will usually also fall into the categories of %(q:mathematical methods) or %(q:plans and rules for performing mental activity), all of which are not considered to be technical inventions.

#ang: The german version has %(e:Programme für Datenverarbeitungsanlagen) (programs for data processing equipment) for where the english says %(e:programs for computers).

#mue: If a term like %(q:computer-implemented invention) really has to be used, we recommend renaming it to %(q:computer-related invention) and incorporating a proper definition in the directive, similar to the one which we propose below.

#bnW: The %(it:ITRE vote) has shown that it is possible to obtain consensus for regulations on a series of questions which have arisen out of recent practise, such as:

#ori: limits of patent enforcability and claim form in view regard to information freedoms such as the freedom of publication guaranteed by Art 10 ECHR or the freedom to use one's copyrighted intellectual property

#fir: limits of patent enforcability in view of interoperability

#fgW: limits of patent enforcability in view of prior use in the case of information objects (i.e. %(q:use) means %(q:dissemination) and can therefore not easily be limited in scope)

#sna: what constitutes published prior art in the case of information objects

#rrW: obligation to disclose a workable program in the description wherever a claim refers to a program

#rWf: measures for regular monitoring of the scope of patentability and control of the patent system

#anh: safeguards to ensure that patents can be revoked when the Parliament later finds that a stricter approach to the technical invention is needed.

#oan: These questions are largely in the sphere of national legislation until today.  They have not been regulated by theprovisions in Art 52-57 EPC (substantive patent law).  Thus EU intervention in this sphere is both feasible and conveniently justifiable.

#tpo: The existence of gray zones may be inevitable.  Yet currently various EU proposals have made ambitious promises of wanting to eliminate gray zones.  Such promises were apparently needed in order to extend the EU's regulative competence into the area of patentability.

#bvt: Yet it may indeed be a good idea to get serious about eliminating gray zones.  That would however involve far more of an effort than what the EU seems to be ready to do at the moment.  It would be an ambitious project, involving a deepened discussion among concerned circles.  It would probably have to rely on three pillars:

#con: Abstract Definition of Technical Invention

#eWi: Categorial exclusions

#pia: Application Examples

#snc: This is rooted in most european patent traditions and has found one of its most famous and most elaborate expression in the German Federal Court's %(dp:Dispositionsprogramm decision of 1976).  It relies on two essential distinctions:

#rct: the invention which a claim discloses vs the scope of implementations which it forbids

#nst: Mind vs Matter

#dai: The invention must be new and technical, independently of how it is implemented.  When software-patent advocates speak about a %(q:computer-implemented invention), they are usually not referring to inventions but to scopes of forbidden implementations.  By such a shift of underlying meaning, any restriction on patentability can be made meaningless.

#eli: Synonyms for %(e:invention) are %(e:technical contribution), %(e:technical teaching) or %(e:technical solution).  Inventions are implicitely technical.  It is dogmatically wrong to distinguish between %(e:invention) and %(e:technical contribution) as done in the CEC/BSA proposal.  When CEC/BSA speaks of %(q:invention), they mean %(q:idea).

#hre: Inventing in the sense of patent law means harnessing the forces of nature, which do not follow the rules of intellectual creation and must be verified by experimentation (letting nature answer questions) rather than by mathematical proof (letting the human mind answer).  Only in this field can there be a macro-economid justification for granting a 20 year monopoly on a novel teaching.

#uvt: %(tr:Art 27 TRIPs) determines that patentability must be limited by reference to the concepts of %(q:invention), %(q:technical) and %(q:industrial).  Even those who have difficulty with these concepts are forced to use them.  Failing to define and apply them in a meaningful way may be construed as a violation of TRIPs.

#cme: These are more concrete and therefore more mass-communicatable than the above-explained basic distinctions.

#Wla: In EPC language

#atn: Programs for computers are not inventions

#Wsg: In TRIPs language

#rot: Data processing is not a field of technology

#lrW: These first two pillars have in the past been weakened by progressive erosion.  They need further support from a third, even more concrete pillar.

#eaw: The directive should contain an annex with examples of model decisions, comprising both real cases from the earlier national caselaw and constructed cases.

#EWo: The CEC/BSA proposal quotes recent EPO decisions as models.  An amendement proposal should be accompanied by a preamble and an annex which quotes extracts from a few model cases.  These should contain a description of prior art, an alleged invention and examples of possible claims and how they would be judged (refuted or upheld).

#bWu: The annex with examples could be regularly reviewed by the European Parliament.  Thus the Legislative could take charge of an important area of rule-setting competences which has been in the hands of judges for too long.

#twx: This again presupposes that the directive acknowledges the existence of a gray area in which the validity of existing patents is even less to be %(q:taken for granted) than elsewhere.

#titcec: %(nl|Proposition de|DIRECTIVE DU PARLEMENT EUROPÉEN ET DU CONSEIL|concernant la brevetabilité des inventions mises en oeuvre par ordinateur)

#titamd: %(nl|Proposition de|DIRECTIVE DU PARLEMENT EUROPÉEN ET DU CONSEIL|%(s:sur les limites de la brevetabilité|à l'égard du processement de données automatisé et ses champs d'application))

#titjust: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in %(a6:Appendix 6) of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a programmatic statement.  It implies that calculation rules framed in the terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.  It can not be the aim of the current directive to declare all kinds of %(q:computer-implemented) ideas to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to automatic data processing and its %(tp|various|technical and non-technical) fields of application, and this must be expressed in the title in plain and unambiguous wording.

#TEO: LE PARLEMENT EUROPÉEN ET LE CONSEIL DE L'UNION EUROPÉENNE,

#HiW: vu le traité instituant la Communauté européenne et notamment son article 95,

#HWo: vu la %(fn:39:proposition de la Commission),

#HpW: vu l'%(fn:40:avis du Comité économique et social),

#AtW2: Statuant conformément à la %(fn:41:procédure prévue à l'article 251 du traité),

#Wee: Considérant ce qui suit:

#tlk: Internal Market

#ien: Differences

#euW: Pas de Convergence sans une Directive

#fti: Significance of Computer Programs Today

#Wif: Ergo Harmonisation & Clarification

#ePt: Brevets et Intérêt Public

#tdr: Patents and Copyright

#cla: Technical Character

#Pde: Field of Technology

#hCu: Technical Contribution

#lrh: Algorithms

#LNl: EU Law vs National Law

#Lmt: Gamme du Règlement

#rCt: Compétitivité

#mtR: Competition Rules

#mnr: Interopérabilité

#udi: Subsidiarité

#TWE: La réalisation du marché intérieur implique que l'on élimine les restrictions à la libre circulation et les distorsions à la concurrence, tout en créant un environnement favorable à l'innovation et à l'investissement. Dans ce contexte, la protection des inventions par brevet est un élément essentiel du succès du marché intérieur. Une protection effective et harmonisée des inventions mises en oeuvre par ordinateur dans tous les États membres est essentielle pour maintenir et encourager les investissements dans ce domaine.

#Tse: La réalisation du marché intérieur implique l'élimination des restrictions à la libre circulation et aux distortions de la concurrence. Un environnement favorisant l'innovation et l'investissement doit alors être créé. %(s:Une protection sûre et égalitaire dans le domaine du développement de logiciels) est un élément important pour le succès du marché intérieur. %(s:Les créateurs de logiciels innovants ne doivent être gênés ni par %(tp|la copie|le plagiat), ni encore par la monopolisation d'idées et interfaces fondamentales). Le secteur du logiciel doit être un espace de compétition régulier dans lequel de nombreuses jeunes pousses peuvent fleurir et prospérer pour développer des logiciels interopérables de haute valeur.

#isa: There is no evidence to support the Commission's suggestion that patents promote innovation in the field of software.  Various %(es:economic studies) suggest the contrary.  The commission's proposal overstresses the importance of individual ideas and neglects other important factors of the software economy.

#Dao: Des différences existent dans la protection des inventions mises en oeuvre par ordinateur, elles sont offertes par les pratiques administratives et la jurisprudence des différents États Membres. De telles différences pourraient créer des barrières au commerce et ainsi entraver le fonctionnement propre du marché intérieur.

#pit: We avoid biased terms such as %(q:protection) and %(q:computer-implemented invention).  Whether the objects in question here are %(q:inventions) in the sense of the patent system is subject to regulation by this directive.  The dispute is not about computing equipment but about software, and it is unclear in what way patents offer %(q:protection) to software.  Many software developpers are in fact asking the legislator to %(q:protect) them against patents.

#San: De telles différences résultent du fait que les États membres adoptent de nouvelles pratiques administratives qui diffèrent les unes des autres ou que les jurisprudences nationales interprétant la législation actuelle évoluent différemment.

#Nua: À cause des décisions prises par l'OEB depuis 1986, les cours européennes font maintenant face à deux normes du droit en concurrence.  Cette situation exige une décision législative.

#ads: Avec l'accroissement intensif de l'opinion sur les plans nationaux et européens les différences de droit positif sont normalement plutôt atténuées qu'amplifiées. Ces dernières années la jurisprudence des brevets a convergé dans de nombreux cas. Différentes cours nationales ont adopté des règles similaires où leurs lois nationales le permettaient.  Étant donné que le droit positif des brevets de l'Europe est déjà harmonisé, il n'y aurait pas à attendre normalement ici le besoin d'une harmonisation.  Si nous le voyons néanmois, nous devons expliquer pour quoi.  La possibilité abstraite d'une amplification de divergence en tant que telle ne justifie pas une action législative de la part de l'Union Européenne.

#rec4cec: L'accroissement continu dans la distribution et l'usage des programmes d'ordinateur dans tous les domaines de la technologie et dans leur distribution mondiale via l'Internet est un facteur critique dans l'innovation technologique. Il est par conséquent nécessaire d'assurer qu'un environnement optimum existe pour les développeurs et les utilisateurs de programmes d'ordinateur en Europe.

#rec4amd: Ever since the introduction of the universal computer in the 1950s and 60s, computer programs have been ubiquitously used to describe, control and integrate processes in all fields of human and social activity. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.

#rec4just: The Commission text does not explain in what way computer programs are %(q:critical) today, and it suggests a relation between a postulated %(q:increase) in use of computer programs and a need for legislation, without clearly saying in what this relation consists.  Moreover, it suggests that computer programs are somehow linked to %(q:fields of technology), implying the conclusion of the Commission's Article 3, namely that computer programs are patentable inventions according to Art 27 TRIPs, even when they are about mathematics, bank transactions or administrative processes.

#TWv: Par conséquent, les règles légales comme interprétées par les Cours des États Membres devront être harmonisés et la loi gouvernant la brevetabilité des inventions implémentées par ordinateur devra être rendue transparente. La certitude légale résultante devrait permettre aux entreprises de tirer le maximum d'avantage des brevets pour les inventions implémentées par ordinateur et donner une incitation pour l'investissement et l'innovation.

#TWl: C'est pourquoi les règles sur les limites de la brevetabilité comme stipulés dans l'Art 52 CBE, doivent être reconfirmés et codifié en détail. La certitude légale résultante devrait %(s:contribuer à un climat encourageant l'investissement et l'innovation dans le domaine du logiciel).

#tdo: Experience and economic studies strongly suggest that patents on computer-implemented rules of organisation and calculation (programs for computers), as have been granted by the European Patent Office (EPO) in contradiction to the letter and spirit of the EPC, are not conducive to innovation in the area of software.  A recent study (http://www.researchoninnovation.org/swpat.pdf) shows that the same patents have substituted innovation and thus led to a reduction in R&D investment and productivity even those companies that patented most.  Reconfirming the invalidity of these patents would reestablish legal security and confidence in investing in software development.

#TcW: La Communauté et ses États membres sont liés par l'accord relatif aux aspects des droits de propriété intellectuelle qui touchent au commerce (ADPIC), approuvé par la décision 94/800/CE du Conseil, du 22 décembre 1994, relative à la conclusion au nom de la Communauté européenne, pour ce qui concerne les matières relevant de ses compétences, des %(fn:42:accords des négociations multilatérales du cycle de l'Uruguay (1986-1994)). L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra être obtenu pour toute invention, de produit ou de procédé, dans tous les domaines techniques, à condition qu'elle soit nouvelle, qu'elle implique une activité inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent être obtenus et des droits de brevets exercés sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur.

#Wjr: La Communauté et ses États Membres sont liés par l'%(tr:Accord sur les Aspects des Droits de Propriété Intellectuelle qui touchent au Commerce) (ADPIC), approuvé par la Décision du Conseil 94/800/CEC du 22 Décembre 1994 concernant la conclusion au nom de la Communauté Européenne, en regard des matières en sa compétence, des %(fn:42:accords atteints lors des négociations multilatérales de l'Uruguay Round (1986-1994)). Selon l'Article 27(1) de l'ADPIC, les brevets doivent être disponibles pour toute les inventions, de produit ou de procédé, dans tous les domaines techniques, à condition qu'elle soient nouvelles, qu'elle impliquent une activité inventive et qu'elle soient susceptible d'application industrielle. De plus, selon l'ADPIC, les droits des brevets doivent être disponibles sans discrimination pour tous les domaines de la technologie. Conformément à l'ADPIC, des brevets doivent être disponibles en outre sans discrimination quant au domaine de la technique ou les droits sur les brevets peuvent être exercés. %(s:Ceci signifie que les inventions techniques, dont celles dont l'exécution implique l'utilisation d'un ordinateur, doivent être brevetables sans que l'on ait à considérer à quel %(tp|domaine technique|par expl. ingénierie mécanique, électrique, chimique) elles appartiennent.  Le processement de données, soit il conduit par la raison humane ou par un ordinateur, n'est pas un champ de technologie dans le sens du loi de brevets.)

#tdW: We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it excludes business method patents, which the US government wants to mandeate by the new Substantive Patent Law Treaty draft, see %(URL).

#rec7cec: Dans la Convention sur l'Octroi des Brevets Européens signée à Munich le 5 octobre 1973 et les lois sur les brevets des États Membres, les programmes d'ordinateur ainsi que les découvertes, les théories scientifiques, les méthodes mathématiques, les création esthétiques, les plans, les principes et méthodes dans l’exercice d’activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques, et les présentations d'information ne sont expressément pas considérés comme des inventions et sont par conséquent exclus de la brevetabilité. Cette exception, néanmoins, s'applique et n'est justifiée que dans la mesure où une application d'un brevet ou un brevet se rapporte à un tel sujet ou de telles activités en tant que tels, car le sujet et les activités nommés en tant que tels n'appartiennent pas à un domaine de la technologie.

#rec7amd: Sous la Convention sur l'Octroi de Brevets Européens du 5 octrobre 1973 (CEB) et les lois sur les brevets des États Membres, les programmes pour ordinateur ainsi que les découvertes, les théories scientifiques, les méthodes mathématiques, les créations esthétiques, les plans, les principes et méthodes dans l'exercice d'activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques, et les présentations d'information ne sont expressément pas considérés comme des inventions et sont par conséquent exclus de la brevetabilité. Cette %(s:exclusion), bien entendu, s'applique et n'est justifiée que dans la mesure où une application d'un brevet ou un brevet se rapporte à un tel sujet ou de telles activités en tant que tels.  %(s:Ainsi cela peut ne pas s'appliquer si l'invention ne consiste pas en un programme d'ordinateur mais en une %(tp|invention technique|ex. une nouvelle recette chimique) qui peut être mise en oeuvre sous le contrôle d'un programme.  Dans ce cas le programme en tant que tel serait librement implémentable, ex. pour des simulations, mais le procédé chimique tomberait sous le brevet.)

#rec7just: According to the normal rules of semantics, %(q:program as such) can only mean %(q:program as program), not %(q:program as something non-technical).  According to Art 52(2) EPC, programs for computers (calculation rules for general-purpose data processing equipment) are not technical inventions.  Art 52 does not mention a delimiting criterion called %(q:technical).  The patent courts are however free to define their own (secondary) concept of %(q:technical invention) as long as this concept conforms to the specifications of Art 52(2) EPC.  The concept of %(q:technical invention) must conform to Art 52(2), not vice versa.

#j9e: Dans sa résolution (publiée au JO C 378 du 29.12.2000, p. 95) sur une décision de l'Office européen des brevets concernant la délivrance du brevet no EP 695 351, le 8 décembre 1999, le Parlement européen a demandé la révision des règles de fonctionnement de l'Office de façon à garantir que cet organisme puisse publiquement rendre des comptes dans l'exercice de ses fonctions.

#sea: L'Office européen des brevets n'est pas une institution de l'Union européenne et la question de sa responsabilité a été soulevée par le passé.

#Pha: La protection par brevet permet aux innovateurs de tirer profit de leur créativité. Les droits de brevet protègent l'innovation dans l'intérêt de la société dans son ensemble mais ils ne doivent pas être utilisés d'une manière anticoncurrentielle.

#Pri: Les brevets sont des monopoles temporaires accordés par l'État afin de promouvoir le progrès technique.  Afin de s'assurer que le système fonctionne comme prévu, les conditions pour accorder des brevets et les modalités pour les faire valoir doivent être conçues avec attention.  En particulier, des corollaires inévitables du système des brevets comme la restriction de la liberté de créer, l'insécurité légale et les comportements de cartel doivent être contenus dans des limites raisonnables.

#ist: Innovators can benefit from their creativity without patents.  Whether patent rights %(q:protect) or stifle innovation and whether they act in the interests of society as a whole is a question that can only be answered by empirical study, not by dogmatic statements in a law.

#I3H: Conformément à la directive du Conseil 91/250/CEE du 14 mai 1991 concernant la protection juridique des programmes d'ordinateurs43, toute expression d'un programme d'ordinateur original est protégée par un droit d'auteur en tant qu'oeuvre littéraire. Toutefois, les idées et principes qui sont à la base de quelques éléments que ce soit d'un programme d'ordinateur ne sont pas protégés par le droit d'auteur.

#InW: Conformément à la la Directive du Conseil 91/250/CEE du 14 Mai 1991 sur la protection légale des programmes d'ordinateur, l'expression sous toute forme d'un programme d'ordinateur original comme oeuvre littéraire est protégée par le droit d'auteur en tant que %(s:création individuelle).  Les idées et principes qui sont à la base de n'importe quel élément d'un programme d'ordinateur sont %(s:pour de bonnes raisons explicitement exemptés de la protection sous le droit d'auteur).

#asr: Copyright does not only apply to literary works, but also to textbooks, operation manuals, computer programs and all kinds of information structures. Copyright is %(e:the) system of %(q:intellectual property) for computer programs, not only a system for a %(q:literary) side aspect of computer programs.  If copyright does not cover the %(q:underlying idea) of a book or a program then that is not an indication of an insufficiency of copyright but rather an indication of the need to keep %(q:underlying ideas) (general concepts) free, so that many different creators have a chance to obtain property in individual works based on these general concepts.

#Isc: Pour être considérée comme brevetable, une invention doit présenter un caractère technique et donc appartenir à un domaine technique.

#IWd: Afin qu'une idée soit considérée comme une invention dans le sens du système des brevets, il est nécessaire qu'elle présente un caractère technique et donc appartenir a un champ technique.

#Elu: The Commission text is not in line with Art 52 EPC.  Art 52(2) EPC lists examples of non-inventions.  It is not permissible to subsume these under %(q:inventions) and then test their technical character.  Moreover, while it can not be inferred from Art 52 EPC that all technical innovations are inventions, it can, based on a unanimous tradition of patent law, be assumed that all inventions have technical character.

#rec11cec: Bien que les inventions mises en oeuvre par ordinateur soient considérées comme appartenant à un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique à l'état de la technique pour répondre au critère de l'activité inventive.

#rec11amd: Bien que les programmes d'ordinateur n'appartiennent pas à un domaine technique (c.a.d. ingéniérie, sciences de nature appliqués), ils peuvent être utilisé pour décrire et contrôler des inventions techniques.

#rec11just: The Commission text declares computer programs to be technical inventions.  It removes the independent requirement of invention (%(q:technical contribution)) and merges it into the requirement of non-obviousness (%(q:inventive step)).  This leads to theoretical inconsistency and undesirable practical consequences, as explained in detail in the justification of our amendment to 4(2).

#Ahi: En conséquence, lorsqu'une invention n'apporte pas de contribution technique à l'état de la technique, parce que, par exemple, sa contribution spécifique ne revêt pas un caractère technique, elle ne répond pas au critère de l'activité inventive et ne peut donc faire l'objet d'un brevet.

#Aho: En conséquence, un enseignement qui n'apporte pas de contribution technique à l'état de l'art n'est pas une invention au sense du loi des brevets.

#pef: The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This deviates from Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible.  See the Justification of Art 4(2) for detailed explanation.

#Aaw: Une procédure définie ou une séquence d'actions exécutées sur un appareil tel qu'un ordinateur, peut apporter une contribution technique à l'état de la technique et constituer ainsi une invention brevetable. Par contre, un algorithme défini sans référence à un environnement physique ne présente pas un caractère technique et ne peut donc constituer une invention brevetable.

#Aao: Une procédure définie ou une séquence d'actions exécutées dans un dispositif, par expl. un ordinateur, peut constituer une invention technique, %(s:dans la mesure où des effets inconnus en rapport avec les forces de la nature sont dévoilés.  Toutefois, un algorithme, sans se soucier de savoir si les entités symboliques dont il est composé peuvent être interprétées comme se référant à un environnement physique, ne présente aucun caractère technique) et ne peut par conséquent constituer une invention brevetable.  En particulier des références à des réalisations connus d'éléments de l'Ordinateur Universel, de la Machine Turing ou d'autres modèles mathématiques connus ne peut pas donner un caractère technique à un algorithme.  Un programme d'ordinateur n'est qu'un algorithme exprimé dans les termes les plus généraux: ceux de l'Ordinateur Universel.

#tij: The Commission and JURI Proposals make algorithms patentable, if only they are claimed by reference to a universal computer (general-purpose data processing system), which is the common and most general form of presenting and using algorithms.

#defalgo: According to the common understanding of computer science, an algorithm could be defined as a %(q:rule of calculation using abstract entities, such that the result of the calculation is independent of any physical environment to which the entities may be mapped).

#dixitknuth: The Commission and JURI try to impose a distinction between physical and non-physical (%(q:technical) and %(q:non-technical)) algorithms, thereby repeating an error on which computer-science pioneer Professor Donald Knuth commented in 1994:

#aha: I am told that the courts are trying to make a distinction between mathematical algorithms and nonmathematical algorithms.  To a computer scientist, this makes no sense, because every algorithm is as mathematical as anything could be.  An algorithm is an abstract concept unrelated to physical laws of the universe.

#mca: Therefore the idea of passing laws that say some kinds of algorithms belong to mathematics and some do not strikes me as absurd as the 19th century attempts of the Indiana legislature to pass a law that the ratio of a circle's circumference to its diameter is exactly 3, not approximately 3.1416.  It's like the medieval church ruling that the sun revolves about the earth.  Man-made laws can be significantly helpful but not when they contradict fundamental truths.

#WWe: The mere fact that a method may allow data processing to be carried out more quickly, more efficiently or using less memory is not by itself sufficient to cause such a method to be patentable when otherwise it would not be.

#Woa: In the words of the German Federal Patent Court (Error Search decision, 26 March 2002, BPatG 17 W (pat) 69/98):

#osm: If computer implementations of non-technical processes were attributed a technical character merely because they display different specific characteristics, such as needing less computing time or less storage space, the consequence of this would be that any computer implementation would have to be deemed to be of technical character.

#Tet: La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas nécessiter l'établissement d'une législation distincte en lieu et place des dispositions du droit national des brevets. Les règles du droit national des brevets doivent continuer de former la base de référence de la protection juridique des inventions mises en oeuvre par ordinateur, même si elles doivent être adaptées ou ajoutées en fonction de certaines contraintes spécifiques définies dans la directive.

#sed: The aim of the directive is to reaffirm the legislator's commitment to limiting the scope of patentability in the spirit of Art 52 EPC, whose title %(q:patentable inventions) is referenced in our amendment.  This directive should avoid any affirmation of patentability of undelimited categories such as %(q:computer-implemented invention).

#Tsc: La présente directive devrait se borner à fixer certains principes s'appliquant à la brevetabilité de ce type d'inventions, ces principes ayant notamment pour but d'assurer que les inventions appartenant à un domaine technique et apportant une contribution technique peuvent faire l'objet d'une protection et inversement d'assurer que les inventions qui n'apportent pas de contribution technique ne peuvent bénéficier d'une protection.

#Tho: Cette Directive devrait être limitée à établir certains principes %(s:concernant les limites de la brevetabilité vis-à-vis des programmes d'ordinateur), de tels principes étant déterminés en particulier à assurer que les %(s:inventions techniques sont brevetables et que les règles d'organisation et de calcul ne le sont pas).

#eea: The term %(q:computer-implemented inventions) is designed for abuse, as explained in the amendment to the title.

#Ttu: La position concurrentielle de l'industrie européenne vis-à-vis de ses principaux partenaires commerciaux serait améliorée si les différences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur étaient éliminées et si la situation juridique était transparente.

#End: La sécurité légale et les chances de position concurrentielle des entreprises européennes du logiciel, en compétition avec des partenaires commerciaux majeurs de l'Europe, seraient améliorées s'il était %(s:posé de façon claire que l'Europe n'étend pas la brevetabilité au-delà du domaine des inventions techniques).

#xWa: Unification of caselaw in itself is not a guarantee of improvement of the situation of European industry.  This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries.  Also, as explained above, the term %(q:computer-implemented invention) implies that programs for computer are patentable inventions and thereby calls for violation of Art 52 EPC.

#dte: At the international level, Europe is ahead in the area of open, alternative development and licensing approaches to computer programs, e.g. open source projects under the %(q:GNU General Public License). Particularly in the light of increasing requirements for stability, interoperability and IT security of computer programs, open source computer programs developed on a common, ongoing and transparent basis are gaining in importance.  In order to turn this European lead in terms of development into a real competitive advantage, a reliable legal framework for such alternative development and licensing approaches must be maintained.

#Wsi: This is JURI Amendment 35 as submitted by Evelyn Gebhardt but with a slightly simplified last sentence.  This amendment proposal states a generally acknowledged European policy goal.  It is to be wondered why it didn't find the support of the JURI majority.

#TWn2: La présente directive ne préjuge pas de l'application des règles de concurrence, en particulier des articles 81 et 82 du traité.

#Aod: Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particulières relatives à la décompilation et à l'interopérabilité ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affectés par la protection octroyée par les brevets d'invention dans le cadre de la présente directive.

#Teh: Similar to Directive 91/250/EEC on the legal protection of computer programs by copyright, this directive ensures that actions serving to achieve interoperability with existing programs can not be forbidden by any law.

#ntl: The European Commission text is misleading.  It pretends to ensure a right to interoperate but in fact does exactly the opposite.  Patents grant much broader exclusivity than copyright.  Therefore the interoperability exemptions for patents also need to be broader than those for copyright.  Limiting them to the scope offered by existing copyright laws is means of assuring that patentee interests override interoperability interests.

#SjW: Dans la mesure où les objectifs de l'action envisagée ne peuvent pas être réalisés de manière suffisante par les États membres et peuvent donc, en raison des dimensions ou des effets de l'action envisagée, être mieux réalisés au niveau communautaire, la Communauté est en droit d'adopter des mesures conformément au principe de subsidiarité énoncé à l'article 5 du traité. Conformément au principe de proportionnalité, tel qu'énoncé dans cet article, la présente directive ne va pas au-delà de ce. qui est nécessaire pour atteindre les objectifs fixés,

#yWn: Terms such as %(q:protection) and %(q:computer-implemented invention) are misleading, as explained in the title amendment.

#HTD: ONT ARRÊTÉ LA PRÉSENTE DIRECTIVE:

#Sco: Champ d'application

#Dit: Définitions

#techkamp: Champs de Technologie

#Cse: Conditions de brevetabilité

#Cnl: Revendications et Droits

#rIb: Interoperability

#Mir: Suivi

#ReW: Rapports sur les effets de la directive

#Iet: Mise en oeuvre

#EWW2: Entrée en vigueur

#Ars: Destinataires

#Tsm: La présente directive établit des règles concernant la brevetabilité des inventions mises en oeuvre par ordinateur.

#Tap: Cette directive explique les règles sur les limites de la brevetabilité vis-à-vis des programmes pour ordinateurs.

#eWo: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the EPO in May 2000 in the Trilateral Website document %(APP), where it served to legitimate patents on %(q:computer-implemented business methods), so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a hidden programmatic statement.  It implies that algorithms, business methods and in fact all ideas are patentable inventions, provided that they are claimed in terms of general-purpose data processing equipment.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.  It can not be the aim of the current directive to declare programs for computers to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to computer programs, and this must be expressed clearly and unambiguously.

#ume: Computer-Implemented Invention

#otv: Technology, Invention, Non-Inventions

#FWW: Aux fins de la présente directive, les définitions suivantes s'appliquent:

#kompinvcec: %(q:invention mise en oeuvre par ordinateur) désigne toute invention dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'autre appareil programmable et présentant une ou plusieurs caractéristiques à première vue nouvelles qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs ;

#kompinv: %(q:computerised invention), also called %(q:computer-implemented invention), means an innovation the implementation of which involves the use of a data processing system in connection with peripheral devices, and which, due to the way in which the peripheral devices are used, is considered to be an invention in the sense of patent law.

#kompinvjust: The term %(q:computer-implemented invention) is not used by computer professionals.  In fact it is not in wide use at all.  It was introduced by the EPO in 2000 in an attempt to legitimate patents on %(q:computer-implemented business methods).  It suggests that ideas which can be put to work merely by executing a program on generic computing equipment are patentable inventions.  This amendment eliminates the confusion.  It makes it clear that an inventive washing machine does not cease to be an invention just because it is controlled by a computer.  In a EU Press Release (%(q:MEPs vote to tighten up rules on patentability of computerised inventions), Date: 2003-06-18, on cordis.lu) about this directive, the term %(q:computerised inventions) was introduced, and it was explained that %(q:such inventions do not cover ordinary software programs, but rather solutions for devices such as mobile phones, intelligent household appliances, engine control devices, ...).  Indeed the term %(q:computerised invention) depicts more intuitively than %(q:computer-implemented invention) what MEPs meant to be patentable: inventive use of hardware which has been placed under the control of a data processing system.

#cWi: %(q:contribution technique) désigne une contribution à l'état de la technique dans un domaine technique, qui n'est pas évidente pour une personne du métier.

#cua: %(q:technical contribution), also called %(q:invention), means a %(q:contribution to the state of the art in a technical field).

#Wad: The Commission text merges the invention (technical contribution) test with the non-obviousness (inventive step) test, thereby weakening both tests, deviating from Art 52 EPC, and creating practical problems, see Justification in the Amendment of 4(2).

#applsci: %(q:Technology) in the sense of patent law is %(applied natural science).

#qce: The Commission text shift the question of what is a patentable invention to the question of what is %(q:technical) and then leaves that question unanswered.  The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day caselaw and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such but inventions related to mobile phones, engine control devices, household appliances, ...)) .  This understanding should be made explicit for the purpose of clarification.

#Ain: %(q:Technology) in the sense of patent law is %(q:use of controllable forces of nature).

#cnW: %(q:Technical) in the sense of patent law means %(q:concrete and physical).

#Wxb: An %(e:invention) in the sense of patent law is a %(e:technical solution to a technical problem).

#Coa: This is a common EPC doctrine which has been regularly used by most patent courts, including the Technical Chambers of Appeal of the EPO.

#techinv: %(q:Invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature).

#techinvjust: This is a standard patent doctrine in most jurisdictions.  The EPO says that inventions are %(q:technical solutions of technical problems) and understands %(q:technical) as %(q:concrete and physical).  The term %(q:controllable forces of nature) clarifies this further.  The %(q:four forces of nature) are an acknowledged concept of epistemology (theory of science).  While mathematics is abstract and unrelated related to %(e:forces of nature), some business methods may well depend on the chemistry of the customer's brain cells, which is however not %(e:controllable), i.e. non-deterministic, subject to free will.  Thus the term %(q:controllable forces of nature) clearly excludes what needs to be excluded and yet provides enough flexibility for inclusion of possible future fields of applied natural science beyond the currently acknowledged %(q:4 forces of nature).  This concept has been formulated in most jurisdictions and even written into the law in some countries such as Japan and Poland.  Even the CEC and JURI proposals say that %(q:algorithms and business methods are inherently non technical), and the JURI report associates %(q:technical contributions) with %(q:mobile phones, household appliances, engine control devices, ...).  The classical justification for the %(q:technical character) of %(q:computer-implemented inventions) is not that the meaning of %(q:technical) has changed but that the computer indeed consumes energy in a controlled way, and that the %(q:invention) must be %(q:considered as a whole).  The critics of this view, e.g. the German Federal Patent Court, argue that %(q:the solution is completed by abstract calculation before, during its non-inventive implementation on a conventional data processing system, forces of nature come into play).

#wcn: An %(q:invention) in the sense of patent law, also called %(q:technical teaching), %(q:technical solution) or %(q:technical contribution), is a %(e:teaching of plan-conformant use of controllable forces of nature to immediately, without mediation by human reason, achieve a causally overseeable result.)

#nol: This is a frequently used definition, as used by the German Federal Court of Justice in its %(q:Anti-Blocking System) decision of 1980.  It allowed the court to accept a patent for a computer-controlled anti-blocking system while rejecting a patent for more abstract computing problems such as determining optimal lengths of rolling rods or optimal distribution of air-plane fuel based on flying distances.

#Aoj3: Une %(e:règle de calcul), dite aussi %(e:algorithme), est un enseignement sur des relations au sein des constructions de la pensée, comme des modèles et systèmes axiomatiques, y compris tout modèle formel d'une machine de processement de données comme ex. la machine de Turing.

#AlB: Une %(e:règle d'organisation) est un enseignement sur les relations entre des phénomènes réels (phénomènes matériels) qui ne sont pas déterminés par des forces de la nature contrôlables.   Les méthodes d'affaires et les techniques sociales sont des règles d'organisation.  La plupart des règles d'organisation sont des  applications de règles de calcul aux modèles connus de relations sociales.  Les %(e:règles d'organisation et calcul) sont des enseignements sur des relations cause-effet, qui ne sont pas déterminés par des forces de la nature contrôlables.

#Wtu: These additional definitions could help to establish terminology which can be used in order to refer to various types of non-inventions.  The generic term %(q:rules of organisation and calculation) is taken from the German patent jurisdiction of %(dp:Dispositionsprogramm) and later.

#CeW: Domaine technique

#iug: The article specifies the relation of the European invention concept with Art 27 TRIPs.  Thus we are talking about fields of technology in the plural, and inventions can only %(q:belong) to such a field.  To which field they may belong is not determined by the means of implementation.  Note that the title in the french and german versions of the CEC proposal differs from that in the english version.

#Mue: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit considérée comme appartenant à un domaine technique.

#uan: Member States shall ensure that an innovation is not considered to belong to a field of technology merely because its implementation involves the use of a computer.

#tnT: The European Commission text says that all ideas, including %(q:computer-implemented business methods) etc, are patentable inventions.  This has been widely criticised.  See Justification for 3a.

#lrL: One of the chief objectives of the directive project has been to clarify the interpretation of Art 52 EPC in the light of Art 27 TRIPs, which says that %(q:inventions all fields of technology) must be patentable.  We propose to translate of Art 52(2) EPC into the language of Art 27 TRIPs, along the lines of Amendment CULT-16.

#oaf: If data processing is %(q:technical), then anything is %(q:technical).

#otg: Data processing is a common denominator of all fields of technology and non-technology.  With the advent of the universal computer in the 1950s, automated data processing (ADP) became pervasive in society and industry.

#d1w: As Gert Kolle, a leading theoretician behind the decisions of the 1970s to exclude software from patentability, writes in 1977 (see %(URL)):

#Wmr: ADP has today become an indispensable auxiliary tool in all domains of human society and will remain so in the future.  It is ubiquitous.  ... Its instrumental meaning, its auxiliary and ancillary function distinguish ADP from the ... individual fields of technology and liken it to such areas as enterprise administration, whose work results and methods ... are needed by all enterprises and for which therefore prima facie a need to assure free  availability is indicated.

#dnW: CULT was well-advised to vote for a clear exclusion of data processing from the scope of %(q:technology).

#yag: Numerous studies, some of them conducted by EU instiutions as well as the opinions of the European Economic and Social Committee and the European Comittee of Regions explain in detail why Europe's economy will suffer damage, if data processing innovations are not clearly excluded from patentability.

#Mii: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit brevetable à la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activité inventive.

#MaW: Les États membres veillent à ce que les brevets soient accordés seulement pour des invention techniques nouvelles, non-évidentes et destinées pour une application industrielle.

#IWt: Saying that %(q:inventions are patentable) is tautological in the context of patent law.  The question is: what is an invention?  The Commission text seems to be saying that any solution which is implemented by a computer is an invention, and that there is no invention test but only tests of novelty, non-obviousness and industrial applicability.   Thereby the Commission text drastically changes the rules of Art 52 EPC and allows unlimited patentability.   It would possible to delete the Commission's Article 4 entirely, because, once the errors are corrected, we arrive at little more than a literal restatement of Art 52 EPC.  Yet, in order to correct a recent practise of the EPO, it may be a good idea to restate that the %(q:technical contribution) is a synonym of %(q:invention) and unrelated to the %(q:inventive step) test.  This has been pointed out by many EPO-friendly law scholars, see e.g. the dissertation of Ralph Nack, whom the Commission cites in its explanatory memorandum.

#art42cec: Les États membres veillent à ce que pour impliquer une activité inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique.

#art42amd: Member states shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#art42just: Non-obviousness (= %(q:inventive step)) and the presence of a technical invention (= %(q:technical contribution)) are two separate requirements.  Merging them into one is counter-intuitive and leads to practical problems, among others that the invention needn't be new and that patent offices are no longer entitled to reject patents on non-inventions without first conducting a wasteful prior art search.

#aea: The Commission text opens several doors for unlimited patentability by deviating from Article 52 EPC.  The %(EP) explain Art 52 EPC as follows:

#epogl78: EPO Examination Guidelines

#Tsf: There are four basic requirements for patentability:

#Tbi: There must be an %(qc:invention).

#Tcd2: The invention must be %(qc:susceptible of industrial application).

#Tie: The invention must be %(qc:new).

#Ttx: The invention must involve an %(qc:inventive step).

#Ihp: In addition to these four basic requirements, the examiner should be aware of the following two requirements that are implicitely contained in the Convention and in the Regulations:

#Taa: The invention must be such that it can be carried out by a person skilled in the art [...]

#Tce: The invention must be of %(q:technical character) [...]

#Wse: The Commission text deviates from Art 52 EPC by merging the independent requirements %(q:technical invention) (renamed to %(q:technical contribution)) into the requirement of %(q:inventive step).  This is counter-intuitive and theoretically inconsistent, as even the European Commission implicitely admits in its Explanatory Memorandum:

#htr: The presence of a %(q:technical contribution) is to be assessed ... under inventive step.  Experience has shown that this approach is the more straightforward to apply in practice.

#Waf: Indeed the approach has no theoretical merits but significant practical effects.  It opens an infinite space of interpretation for those patent offices that conduct a full substantive examination, such as the EPO and the German Patent Office, while preventing other patent offices (such as French, Italian) from rejecting patents on non-inventions.

#nee: Furthermore, treating %(q:computer-implemented) inventions in a special way, as suggested by the European Commission, runs counter to Art 27 TRIPs. And, as has been pointed out above, the term %(q:computer-implemented invention) itself may run counter to Art 52 EPC by implying that algorithms framed in terms of generic computing equipment (programs for computers) are patentable inventions.

#era: The amendment solves all these problems by restating the structure of Art 52 EPC as explained in the EPO's Examination Guidelines.

#art43cec: La contribution technique est évaluée en prenant en considération la différence entre l'objet de la revendication de brevet considéré dans son ensemble, dont les éléments peuvent comprendre des caractéristiques techniques et non techniques, et l'état de la technique.

#art43amd: The technical contribution shall be assessed by consideration of the difference between the scope of the technical features of the patent claim as a whole and the state of the art.

#art43just: The Commission and JURI versions imply that a %(q:technical contribution) can consist solely of non-technical features.  This is self-contradictory and leads to unlimited patentablity.  Amendment CULT-15 corrects the error, as far as possible.

#nno2: En estimant si une revendication révèle une invention technique, on ne doit tenir aucun compte de la forme ou du type de la revendication mais se concentrer sur le contenu afin d'identifier la contribution nouvelle que l'objet revendiqué fait à la technique connue.  Si cette contribution ne constitue pas une invention, il n'y a pas d'objet brevetable.

#WoW4: %(q:Claim features) can not be a meaningful subject of legal regulation.  This amendment is an excerpt from the %(EP).  It exhorts examiners to disregard the form of the claims and to distrust all attempts at formalising the procedure of patent examination.  Formalisation puts the application drafter in control and promotes an uncritical attitude on the part of the examiner.

#bpatg02: Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of ressources within the data processing system.

#bpatg02just1: This amendment reflects current caselaw in Germany.  In the words of the justices of the German Federal Patent Court (BPatG, decision of 26. March 2002, 17 W (pat) 69/98, %(URL)):

#bpatg02just2: Ein entscheidendes Indiz für die Technizität des Verfahrens sieht die Anmelderin darin, dass es auf einer technischen Problemstellung beruht. Dadurch, dass das vorgeschlagene Verfahren nicht mit einem Wörterbuch arbeite, könne der Speicherplatz hierfür entfallen. ...  Was die technische Aufgabenstellung anlangt, so kann hierin nur ein Indiz, nicht aber ein Nachweis für die Technizität des Verfahrens gesehen werden.  Würde Computerimplementierungen von nichttechnischen Verfahren schon deshalb technischer Charakter zugestanden, weil sie jeweils unterschiedliche spezifische Eigenschaften zeigen, etwa weniger Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur Konsequenz dass jeglicher Computerimplementierung technischer Charakter zuzubilligen wäre. Denn jedes andersartige Verfahren zeigt bei seiner Implementierung andersartige Eigenschaften, erweist sich entweder als besonders rechenzeitsparend oder als speicherplatzsparend. Diese Eigenschaften beruhen - jedenfalls im vorliegenden Fall - nicht auf einer technischen Leistung, sondern sind durch das gewählte nichttechnische Verfahren vorgegeben. Würde schon das Erfüllen einer solchen Aufgabenstellung den technischen Charakter einer Computerimplementierung begründen, so wäre jeder Implementierung eines nichttechnischen Verfahrens Patentierbarkeit zuzubilligen; dies aber liefe der Folgerung des Bundesgerichtshofs zuwider, dass das gesetzliche Patentierungsverbot für Computerprogramme verbiete, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten.

#FWc: Forme de Revendication

#pte: We are interested in claim forms only to the extent that they imply certain rights and obligations.  The aim of this provision is to explain how patent rights are limited by the freedom of publication (Art 10 ECHR) and other information-related freedom rights.

#art5cec: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur puisse être revendiquée en tant que produit, c'est-à-dire en tant qu'ordinateur programmé, réseau informatique programmé ou autre appareil programmé ou en tant que procédé, réalisé par un tel ordinateur, réseau d'ordinateur ou autre appareil à travers l'exécution d'un programme.

#produktjust: This article explains the meaning of the terms %(q:product) and %(q:process) in the context of computerised inventions. The original version interprets both terms correctly but has an undesirable side-effect: it suggests that algorithms framed in terms of generic computing equipment (programs for computers as such) are or can be %(q:inventions).  The amendment corrects the error. The inventive products and processes are characterised not by the data processing system but by the peripheral devices, which could e.g. be an automobile brake, a rubber-curing furnace or a washing machine.

#publig: Les États membres veilent à ce que la publication et diffusion de programmes d'ordinateur ou autre objets informationnels ne puisse jamais être en contrefaçon directe ou indirecte d'un brevet.

#publigjust: This is to explain how the patent rights are limited by other legal values such as freedom of publication (Art 10 ECHR) and property in individual creations (intellectual property, in the case of software: copyright).  This amendment makes it clearer that both freedom of publication and the right to control one's individual creations (copyright) are more fundamental than patent rights and therefore constitute a limit on the allowable scope of patent rights.  Wherever copyright is applicable, patents are not, and vice versa.  The principle of %(q:maximal separation of spheres of intellectual property) is stated in the %(LST) decisions of the German Federal Court of Justice, and it underlies Article 52 of the European Patent Convention.

#kompexe: Les États membres doivent s'assurer que l'exécution d'un programme d'ordinateur (un ordinateur avec des périphériques de sauvegarde, d'entrée et de sortie de données) ne puisse constituer une violation d'un brevet, alors que son usage, comme par expl. la distribution de produits chimiques piloté par un programme d'ordinateur, peut constituer une violation de brevet.

#kompexejust: This is another concretisation the core principle of this directive in the context of patent enforcability.  It is based on the distinction between data processing equipment (system of processors with devices for storage and manipulaton of data and for exchange of data with humans and computers) and peripheral devices such as automobile brakes or rubber-curing furncases, which maniuplate material phenomena (forces of nature) rather than information.

#scrambled: Les États membres veillent à ce que l'information numérique soit réputée comme étant une connaissance publique, dès que il est possible au public d'en prendre connaissance, même si la prise de connaissance ne résulte que d'un procédé laborieux ou illégal de déchiffrage, décompilation ou décryptage.

#lli: This is to ensure that information (software, databases etc) that is published in closed-source (binary) form can, just like information that is openly accessible, be cited as prior art against a non-novel patent.  A generous understanding of what can constitute prior art is one of very few possible ways of reducing the number of trivial patents.  If software patents are permissible, this is moreover helps to reduce legal insecurity and possible abuses arising from the fact that many computer programs are published in binary form.

#prekre: Les États membres veillent à ce que toutes les oeuvres pouvant être protégées par le droit d'auteur, qui étaient en usage avant la publication d'un brevet, doivent pouvoir à l'avenir être continuées à être développées et employées pour l'exploitation des ordinateurs par le détenteur du droit d'auteur et de toutes les personnes qui ont été ou vont être autorisées par lui, selon des modalités précisées par le détenteur du droit d'auteur.

#bWW: This provision is needed only to the extent that information (software) patents are not reliably excluded by the directive.  When the object of a patent is informational, the scope of %(q:prior use rights) becomes unclear:  it becomes either everything or nothing.  %(q:Prior use right) means %(q:the right to continue to commercially exploit the idea oneself within certain limits).   In the case of information products, such limits cannot be defined.  Information does not require distribution efforts but at best more or less futile efforts to restrict distribution.  Valuable software tends to become freely available with source code sooner or later in its lifecycle.  Patents should be an incentive to publication.  When chosing between %(q:everything or nothing), we say %(q:everything).

#progpub: Les États membres veillent à ce que toujours, quand une revendication d'un brevet suppose qu'un programme d'ordinateur est utilisé,  une implémentation fonctionnelle d'un tel programme fera partie de la description du brevet, laquelle sera publié sans conditions de licenses réstrictives.

#oWc: Patents are two-sided deals: the inventor discloses information in return for a monopoly.  Computer programs are information, whereas the monopoly affects only physical objects.  Nothing describes a computerised process more accurately than a well written program.  This provision is not designed to promote open-source software but rather to make the patent system take its function of knowledge disclosure seriously.  It is similar to ITRE-9, CULT-31 and CULT-32.

#RWi: Rapport avec la directive 91/250/CE

#rtW: The title should clearly specify the purpose of the provision rather than secondary aspects such as which other laws may at present be involved in achieving this purpose.

#dekompil: Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particulières relatives à la décompilation et à l'interopérabilité ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affectés par la protection octroyée par les brevets d'invention dans le cadre de la présente directive.

#abo: Acts which are permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, such as the reimplementation of a program's interfaces for the sake of interoperability, shall not be forbidden by patents.

#dekompiljust: The Commission's text fails to protect interoperability.  Decompilation is not an issue, because patents do not forbid decompilation anyway.  Patent-specific problems need patent-specific solutions.  The Commission's text offers only copyright-specific solutions to patent-specific problems, therebey effectively giving patentees the right to block the use of an idea for purposes of interoperability.  The amdendment ensures that patents are not used as a means to obtain control rights over software which copyright does not offer.

#interop: Les États membres veillent à ce qu'en tout lieu où une utilisation d'une technique brevetée est exigée pour la communication avec un autre système (c.-à-d. pour la convertion en import et en export des conventions de ce système), une telle utilisation ne puisse être considérée comme une contrefaçon.

#interopjust: This is ITRE-15 with a slight modification: %(q:computer system or network) was replaced with the %(q:data processing system), so that it is clear that not merely interoperability between computer architectures (e.g. IBMPC and Mac) but between any kind of software systems is protected.   As the ITRE justification says: %(bq:The possibility of connecting equipments so as to make them interoperable is a way of ensuring open networks and avoiding abuse of dominant positions. This has been specifically ruled in the case law of the Court of Justice of the European Communities in particular. Patent law should not make it possible to override this principle at the expense of free competition and users.)

#Tnt: La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes y compris le commerce électronique.

#lWs: Le Parlement Européen doit surveiller l'impact des brevets sur l'innovation et la concurrence, les deux en Europe et internationalement, et sur les compagnies européennes, et sur le commerce électronique.

#swW: Patents are controversial, because they may exclude people from a wide range of activities, thus limiting people's freedom of action and potentially colliding with important civil liberties.   On the other hands, patents can lead to a concentration of pecuniary beneficts in the hands of small and politically active groups.  It is therefore difficult to assure the independence and impartiality of any state-sponsored monitoring activity.  At the European Commission, monitoring and consultation work to date has been concentrated in the hands of a few civil servants from DG Internal Market with close affinity to the patent system.  In recent years a strong case has been made against patents in various fields such as software and genetics, and these have been reflected in consultations conducted by DG Internal Market. DG Internal Market has regularly disregarded the outcome of such consultations and instead followed the opinions of the patent establishment.  Various reports written by DG Internal Market, including a monitoring report on the gene patent directive of 2001, show an dogmatic belief in the beneficiality of patents and an unwillingness to even ask the questions needed to take empirical evidence into account.  It is clear that monitoring must be done by an independent and interdisciplinary group of scientists and not by those people who are responsible for the directive proposal whose effects are to be monitored.

#Tme2: La Commission soumet au Parlement européen et au Conseil, pour le [DATE (trois ans à compter de la date spécifiée à l'article 9 (1))] au plus tard, un rapport indiquant :

#WiW: The monitoring is done by a committee set up by the Parliament, see the amendment to 7(1).

#Wet: Reporting is needed not only once but at regular intervals.  In particular it cannot be expected that after 3 years much will be observable, as the EPO usually takes about 5 years to grant a patent.

#rbr: La Commission soumet au Parlement européen et au Conseil, pour le [DATE (trois ans à compter de la date spécifiée à l'article 9 (1))] au plus tard, un rapport indiquant :

#har: The Monitoring Committee shall publish a summary report about the insights gained by the scientific communities every two years.  The Parliament shall have the power to demand revisions based on findings of these reports.  If the direcitve is not revised to the satisfaction of the Parliament within two years, the directive shall cease to be in force.

#WWl: Reporting at regular intervals is needed, and it must be part of a real review process.

#teW: l'incidence des brevets délivrés pour des inventions mises en oeuvre par ordinateur sur les éléments mentionnés à l'article 7;

#wtn: si les règles régissant la détermination des critères de brevetabilité en ce qui concerne plus précisément la nouveauté, l'activité inventive et la portée des revendication sont adéquates : et

#wra: si des difficultés sont apparues dans les États membres où les aspects de la nouveauté et de l'activité inventive des inventions ne sont pas examinés avant la délivrance d'un brevet et si des mesures doivent être prises, le cas échéant, pour y remédier.

#nno: which computer-related patents have been granted, which refused, which enforced, who the current owners are, and to what extent licenses are available to the public for each granted patent and how likely inadvertent infringement is.

#tta: to what extent the patent examination system is operating as intended, whether there are sufficient incentives to reject unjustified patent claims, what kind of reforms might be needed in order to allow for a quick and reliable separation of wanted from unwanted patents.

#drc: to what extent european professionals in software and other concerned business fields are satisfied with the current scope of exclusivity and where they see a need for a broader or narrower scope.

#tie: The report should lead to real insights about the current functioning and effects of the patent system which should be appreciated and undergo the test of peer review in various scientific communities.  This amendment incorporates CULT amendment 35.

#Mvf: Les États membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent immédiatement la Commission.

#WDi: Lorsque les États membre adoptent ces dispositions, celles-ci contiennent une référence à la présente directive ou sont accompagnées d'une telle référence lors de leur publication officielle. Les États membres déterminent la manière dont cette référence doit être faite.

#Mia: Les États membres communiquent à la Commission le texte des dispositions de droit interne qu'ils adoptent dans le domaine couvert par la présente directive.

#TWi2: La présente directive entre en vigueur le vingtième jour suivant celui de sa publication au Journal officiel des Communautés européennes.

#Tsh2: Les États membres sont destinataires de la présente directive.

#Dau: Fait à Bruxelles,

#Fua: Par le Parlement européen

#Fhu: Par le Conseil

#Pres: Le Président

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prop ;
# txtlang: fr ;
# multlin: t ;
# End: ;

