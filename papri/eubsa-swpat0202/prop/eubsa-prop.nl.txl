<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Software Patent Directive Amendment Proposals

#descr: The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. operation of computer with %(q:storage means), %(q:output means) etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations explained in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.

#eak: Rationale

#Pem: Preamble

#TAc: The Articles

#yii: Why a directive?

#Prt: Core Layer: Reaffirmation of Art 52 EPC

#IfW: Extension Layer: Questions of Patent Enforcability

#nia: Completion Layer: Clarification of Patentability Limits

#nia2: Harmonising a unified system?

#fey: Clarifying the Unspeakable?

#olW: The political deal between EU and EPO

#dWr: Valid Reasons for a Directive

#neW: The European Commission has proposed a directive for the %(q:patentability of computer-implemented inventions), colloquially termed %(q:software patent directive).

#rot2: The stated aim of the directive is to %(q:harmonise) and %(q:clarify) the rules of what is patentable and what not in the field of computer-related products and proceses.

#yee: Europe's patentability rules were unified by the European Patent Convention of 1973, and with the imminent introduction of the Community Patent even the interpretation of the law will be unified by a centralised EU Patent Court.  So what could possibly still be left for a EU directive to %(e:harmonise)?

#Wsc: If there is nothing to %(e:harmonise), couldn't there at least be something to %(e:clarify)?  Sure, there must be.  Laws always leaves some questions open for interpretation.   Often it does this on purpose.  Clear answers may in particular not be politically feasible when the subject matter (a) is difficult to understand (b) is not mass-communicatable (c) excites strong vested interests.

#Wce: Rather than narrowing the gray zones, the EU Comission proposes to further enlarge them.   Everything is made dependent on undefined terms such as %(q:technical character), and even on self-contradictory arrangments of this empty word, such as saying that a %(q:technical contribution) may consist solely of untechnical features (article 4), or that it is implied in non-obviousness (articles 1-4).  Between the lines this directive proposal seems to say:

#eWW: Member states shall ensure that national courts may not revoke any patents based on what they consider to be a consistent interpretation of the written law but only based on the authority of European judicial institutions such as the European Patent Office.

#hWt: It would be have been much clearer if it could have been reduced to this one sentence.

#tWW: So, if there is nothing to harmonise and nothing to clarify, why are EU politicians almost unanimously pushing for a directive?

#WUh: One answer could be that EU politicians are naturally in favor of anything that extends EU influence.  But that is not the whole answer.

#Whi: Europe's substantive patent law is currently not regulated by the EU but by the European Patent Organisation (EPO), an intergovernmental organisation which runs the European Patent Office (EPO).  There has however been a movement to transfer power from the EPO to the EU.  This power transfer has been based on win-win deals, such as an increased influence of the EPO at the European Commission.  The software patent directive is such a win-win deal:  the EPO receives a license to grant software patents, while the EU is allowed to extend its legislative competence in patent matters.

#WWu: When the EU extends its competence, it usually cites the EC Treaty.  The most convenient pretext is %(q:harmonisation): somewhere in Europe there must be some remaining traces of national diversity which can be said to %(q:impede the proper functioning of the internal market).  However, this %(e:harmonisation trick) does not always work, as can be seen from some recent unsuccessful clashes of the European Commission with national governments before the European Court of Justice.

#Uee: We don't mean to say that there is no reason for the EU to legislate on matters of patentability.  Rather, we base our counter-proposal on more tenable aims and justifications:

#fWl: Restrictions on the use of ideas affect basic liberties and can have a great impact on propsperity.  Unlike many detail questions of patent law, the basic rules concerning what is patentable need to be decided by an elected parliament.

#nWh: With the introduction of the community patent, the patent system has become a domain of EU legislation.  Without clear and strict patentability rules, a more efficient patent system can mean more efficient infliction of harm on European industry and citizens.

#bpe: Rule-setting power on questions of material patent law is currently is already europeanised.  There is a pattern of informal legislation by the European Patent Office and its Administrative Council, both of which are not elected legislative organs.

#tre: The European Patent Office has set new rules of patentability which are at odds with the existing written laws and which are followed by some but not all national courts.  The question of what is patentable is governed by two competing sets of rules and the role of an arbiter between these two is most naturally fulfilled by the European Parliament.

#ant: A few questions concerning enforcability of patents are currently still in the domain of national law and there is indeed some divergence.  Strangely enough, the European Commission has shown little interest in addressing these divergences so far, although the questions are not less capble of %(q:impeding the proper functioning of the internal market).

#arl: With some effort, this reasoning should be able to serve as a valid justification for passing a EU directive.  We have tried to word our preamble amendment proposals accordingly.

#Wei: Negative Definition of %(q:Technical Invention) in Art 52 EPC

#alP: Translating from EPC language to TRIPs language

#nof: Translating from EPC language to the  Meta-Language of Claim-Writing

#ecy: A well-fenced gray zone

#WWt: No such thing as a %(q:Computer-Implemented Invention)

#eWc: The basic choice to be made is whether we adopt the rules of Art 52 EPC, as reflected in the EPO's Examination Guidelines of 1978 and most of the caselaw of the 1970-80s, or the rules developped by the European Patent Office since its change of Examination Guidelines in 1985.

#Cra: Since the later EPO caselaw deviates from the EPC in various respects, there is, even from a purely legal point of view, no choice but to opt for the EPC.  Otherwise the directive would in fact change the law, rather than %(q:harmonise the status quo), and it might not stand up to a challenge before the European Court of Justice (ECJ).

#eva: We propose to opt for the original Art 52 EPC and to restate it in more recent patent language, so that it is less likely to be misunderstood.  Recent patent language is characterised by new elements such as the TRIPs treaty, which didn't exist in 1973, when the EPC was decided.  Although Art 52 EPC provides a clear and adequate structure for today's patent system, it has been weakened by recent layers of interpretation, many of which aimed at creating a different legal structure.

#WWh: At the same time, we propose to correct some misconceptions of the EPO jurisdiction which are not compatible with Art 52 EPC.  These include

#hie: The term %(q:invention) must be used only in the sense of %(q:patentable invention): a synonym of %(q:technical invention), %(q:technical contribution), %(q:technical solution) or %(q:technical teaching).  It is in particular not permissible to use the term %(q:invention) to refer to non-technical ideas or to features in the claim wording which are not meant to be subjected to the tests of novelty, non-obviousness and industrial applicability.

#vno: The four tests of %(q:technical invention), %(q:novelty), %(q:non-obviousness) and %(q:industrial applicability) must be kept separate and strengthened each on its own terms.  It is in particular not permissible to mingle the %(q:technical invention) test into the %(q:inventive step) (= non-obviousness) test, as the EPO and the CEC proposal have been doing.

#rvo: We propose to translate Art 52 EPC into recent patent law language by means of the following two statements, which form the core of our directive proposal:

#datatech: De lidstaten zorgen ervoor dat gegevensverwerking niet beschouwd wordt als een gebied van technologie in de zin van de octrooiwetgeving, en dat uitvindingen in het gebied van gegevensverwerking geen uitvindingen zijn in de zin van de octrooiwetgeving.

#sRh: direct translation of Art 52(2) into TRIPs language, adopted by the CULT commission

#Wue: Moreover, in our amendment to Article 5, we propose to translate the rule into the context of patent claim writing:

#produkt: Member States shall ensure that a computerised invention may be claimed as a product, that is a set of devices connected to a data processing system, or as a process carried out by such devices.

#fWn: The exclusion of general-purpose data processing leaves freedom for the courts and patent offices to grant patents on computer-controlled machine tools or anti-blocking systems, even when the novel teaching arguably lies in the realm of logic or mathematics.  Yet, as with Art 52 EPC, there is nothing in our core proposal that could encourage lawcourts to grant patents on industrially applied logic or mathematics.  Our core proposal sets a clear and easy-to-apply boundary to patent inflation: programs for the general purpose computer are off limits, and so is the application of computers to business models, social processes and other phenomena that are unrelated to controlling forces of nature.

#oWa: Such a regulation thus would clarify some important points, and it corresponds to a wide consensus of economists, engineers, programmers and patent experts (in particular judges and examiners, less so attorneys), politicians and the general public.

#ior: It is clear from the above why the term %(q:computer-implemented invention) is misleading.  An idea that can be implemented merely by means of the universal computer cannot be an invention in the sense of patent law.  Rather, it is an algorithm expressed in terms of a mathematical model, the Universal Computer.  In the language of Art 52 EPC, it is a %(q:program for computers) or a %(pd:%(q:program for data processing equipment)) in the narrowest sense, and it will usually also fall into the categories of %(q:mathematical methods) or %(q:plan and rules for performing mental activity), all of which are not considered to be technical inventions.

#ang: Programm für Datenverarbeitungsanlagen

#mue: If a term like %(q:computer-implemented invention) really has to be used, we recommend renaming it to %(q:computer-related invention) and incorporating a proper definition in the directive, similar to the one which we propose below.

#bnW: The %(it:ITRE vote) has shown that it is possible to obtain consensus for regulations on a series of questions which have arisen out of recent practise, such as:

#ori: limits of patent enforcability and claim form in view regard to information freedoms such as the freedom of publication guaranteed by Art 10 ECHR or the freedom to use one's copyrighted intellectual property

#fir: limits of patent enforcability in view of interoperability

#fgW: limits of patent enforcability in view of prior use in the case of information objects (i.e. %(q:use) means %(q:dissemination) and can therefore not easily be limited in scope)

#sna: what constitutes published prior art in the case of information objects

#rrW: obligation to disclose a workable program in the description wherever a claim refers to a program

#rWf: measures for regular monitoring of the scope of patentability and control of the patent system

#anh: safeguards to ensure that patents can be revoked when the Parliament later finds that a stricter approach to the technical invention is needed.

#oan: These questions are largely in the sphere of national legislation until today.  They have not been regulated by theprovisions in Art 52-57 EPC (substantive patent law).  Thus EU intervention in this sphere is both feasible and conveniently justifiable.

#tpo: The existence of gray zones may be inevitable.  Yet currently various EU proposals have made ambitious promises of wanting to eliminate gray zones.  Such promises were apparently needed in order to extend the EU's regulative competence into the area of patentability.

#bvt: Yet it may indeed be a good idea to get serious about eliminating gray zones.  That would however involve far more of an effort than what the EU seems to be ready to do at the moment.  It would be an ambitious project, involving a deepened discussion among concerned circles.  It would probably have to rely on three pillars:

#con: Abstract Definition of Technical Invention

#eWi: Categorial exclusions

#pia: Application Examples

#snc: This is rooted in most european patent traditions and has found one of its most famous and most elaborate expression in the German Federal Court's %(dp:Dispositionsprogramm decision of 1976).  It relies on two essential distinctions:

#rct: Two Sides of Patent Claims: disclosed invention vs forbidden implementations

#nst: Mind vs Matter, Thing vs Description

#dai: The invention must be new and technical, independently of how it is implemented.  When software-patent advocates speak about a %(q:computer-implemented invention), they are usually not referring to inventions but to scopes of forbidden implementations.  By such a shift of underlying meaning, any restriction on patentability can be made meaningless.

#eli: Synonyms for %(e:invention) are %(e:technical contribution), %(e:technical teaching) or %(e:technical solution).  Inventions are implicitely technical.  It is dogmatically wrong to distinguish between %(e:invention) and %(e:technical contribution) as done in the CEC/BSA proposal.  When CEC/BSA speaks of %(q:invention), they mean %(q:idea).

#hre: Inventing in the sense of patent law means harnessing the forces of nature, which do not follow the rules of intellectual creation and must be verified by experimentation (asking questions to nature) rather than by reasoning (asking questions to the human mind).  Patents monopolise a class of things (material objects) and disclose information.  E.g. a patent could monopolise a rubber-curing process and disclose a computer program which describes that process.

#uvt: %(tr:Art 27 TRIPs) determines that patentability must be limited by reference to the concepts of %(q:invention), %(q:technical) and %(q:industrial).  Even those who have difficulty with these concepts are forced to use them.  Failing to define and apply them in a meaningful way may be construed as a violation of TRIPs.

#cme: These are more concrete and therefore more mass-communicatable than the above-explained basic distinctions.

#Wla: In EPC language

#atn: Programs for computers are not inventions

#Wsg: In TRIPs language

#rot: Data processing is not a field of technology

#lrW: These first two pillars have in the past been weakened by progressive erosion.  They need further support from a third, even more concrete pillar.

#eaw: The directive should contain an annex with examples of model decisions, comprising both real cases from the earlier national caselaw and constructed cases.

#EWo: The CEC/BSA proposal quotes recent EPO decisions as models.  An amendement proposal should be accompanied by a preamble and an annex which quotes extracts from a few model cases.  These should contain a description of prior art, an alleged invention and examples of possible claims and how they would be judged (refuted or upheld).

#bWu: The annex with examples could be regularly reviewed by the European Parliament.  Thus the Legislative could take charge of an important area of rule-setting competences which has been in the hands of judges for too long.

#twx: This again presupposes that the directive acknowledges the existence of a gray area in which the validity of existing patents is even less to be %(q:taken for granted) than elsewhere.

#titcec: %(nl|Voorstel voor een|RICHTLIJN VAN HET EUROPEES PARLEMENT EN DE RAAD|%(s:betreffende de octrooieerbaarheid van in computers geïmplementeerde uitvindingen))

#titamd: %(nl|Proposal for a|DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL|on the limits of patentability|with respect to automated data processing and its fields of application)

#titjust: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in %(a6:Appendix 6) of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a programmatic statement.  It implies that calculation rules framed in the terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.  It can not be the aim of the current directive to declare all kinds of %(q:computer-implemented) ideas to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to automatic data processing and its %(tp|various|technical and non-technical) fields of application, and this must be expressed in the title in plain and unambiguous wording.

#TEO: THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,

#HiW: Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,

#HWo: Having regard to the %(fn:39:proposal from the Commission),

#HpW: Having regard to the %(fn:40:opinion of the Economic and Social Committee),

#AtW2: Acting in accordance with the %(fn:41:procedure laid down in Article 251 of the Treaty),

#Wee: Whereas:

#tlk: Interne markt

#ien: Verschillen

#euW: De verschillen zouden groter kunnen worden

#fti: Informatisering

#Wif: Dus harmonisatie en verheldering

#ePt: Octrooien en publiek belang

#tdr: Octrooien en auteursrecht

#cla: Technisch karakter

#Pde: Gebied van de technologie

#hCu: Technische bijdrage

#lrh: Algoritmes

#LNl: Europese wetgeving versus nationale wetgeving

#Lmt: Reikwijdte van de bepalingen

#rCt: Concurrentiepositie verbeteren

#mtR: Mededingingsregels

#mnr: Decompilatie en compatibiliteit

#udi: Subsidiariteit

#TWE: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market.  Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Tse: The realisation of the internal market requires the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. A %(s:reliable and equitable protection of investments in software development) is an important element for the success of the internal market.  %(s:Innovative software developpers should not have to fear that others take away the fruits of their labor either by %(tp|copying|plagiarism) or by excluding them from the use of basic ideas and interfaces.  The software market should be a level playing field where many small companies can flourish and grow by developping high quality interoperable software).

#isa: There is no evidence to support the Commission's suggestion that patents promote innovation in the field of software.  Various %(es:economic studies) suggest the contrary.  The commission's proposal overstresses the importance of individual ideas and neglects other important factors of the software economy.

#Dao: Differences exist in the legal protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market.

#pit: We avoid biased terms such as %(q:protection) and %(q:computer-implemented invention).  Whether the objects in question here are %(q:inventions) in the sense of the patent system is subject to regulation by this directive.  The dispute is not about computing equipment but about software, and it is unclear in what way patents offer %(q:protection) to software.  Many software developpers are in fact asking the legislator to %(q:protect) them against patents.

#San: Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently.

#Nua: Recently caselaw standards have been evolving on a more and more pan-european level.  Different national courts have adopted similar rules where their national laws allowed this.  Given that Europe's susbstantive patent law is already unified, a uniform development of national caselaws could normally be expected to occur.  However, due to the EPO's decisions of 1986 and later, european courts are now faced with two competing sets of rules.  This situation calls for a legislative decision.

#ads: We need to explain why the divergence (schism) can be expected to become wider.  An abstract possibility of caselaw divergence always exists and can therefore not justify legislative action by the EU.  Given that Europe's susbstantive patent law is already unified, a uniform development of national caselaw could normally be expected to occur.  If we do not expect it to occur, we must explain why.

#rec4cec: De gestadige toename van de verspreiding en het gebruik van computerprogramma's op alle gebieden van de technologie en van de wereldwijde verspreiding ervan via internet is een kritieke factor voor de technologische innovatie. Daarom moet worden voorzien in een optimale omgeving voor de ontwikkelaars en de gebruikers van computerprogramma's in de Gemeenschap.

#rec4amd: Ever since the introduction of the universal computer in the 1950s and 60s, computer programs have been ubiquitously used to describe, control and integrate processes in all fields of human and social activity. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.

#rec4just: The Commission text does not explain in what way computer programs are %(q:critical) today, and it suggests a relation between a postulated %(q:increase) in use of computer programs and a need for legislation, without clearly saying in what this relation consists.  Moreover, it suggests that computer programs are somehow linked to %(q:fields of technology), implying the conclusion of the Commission's Article 3, namely that computer programs are patentable inventions according to Art 27 TRIPs, even when they are about mathematics, bank transactions or administrative processes.

#TWv: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#TWl: Therefore, the legal rules on the limits of patentability as interpreted by Member States' courts should once again be brought in line with the written law and codified in a less misunderstandable form.  The resulting legal certainty should be conducive to innovation and investment in the area of software.

#tdo: Experience and economic studies strongly suggest that patents on computer-implemented rules of organisation and calculation (programs for computers), as have been granted by the European Patent Office (EPO) in contradiction to the letter and spirit of the EPC, are not conducive to innovation in the area of software.  A recent study (http://www.researchoninnovation.org/swpat.pdf) shows that the same patents have substituted innovation and thus led to a reduction in R&D investment and productivity even those companies that patented most.  Reconfirming the invalidity of these patents would reestablish legal security and confidence in investing in software development.

#TcW: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the %(fn:42:agreements reached in the Uruguay Round multilateral negotiations (1986-1994)). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Wjr: The Community and its Member States are bound by the %(tr:Agreement on trade-related aspects of intellectual property rights), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the %(fn:42:agreements reached in the Uruguay Round multilateral negotiations (1986-1994)). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available without discrimination as to the field of technology.  %(s:This means that patentability must be effectively limited in terms of general concepts such as %(q:invention), %(q:technology) and %(q:industry), so as to avoid commercial monopolies or ad hoc policies, both of which would act as barriers to free trade.  Thus technical inventions, including those that use a computer, must be patentable no matter %(tp|to which engineering discipline they belong|which forces of nature they help to harness).  Data processing, no matter whether performed by the human mind or by means of a computer, is not a field of technology in the sense of patent law).

#tdW: We need to make clear that there are limits as to what can be subsumed under %(q:fields of technology) according to Art 27 TRIPs and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability.  This interpretation of TRIPs is indirectly confirmed by recent lobbying of the US government against Art 27 TRIPS on the account that it excludes business method patents, which the US government wants to mandeate by the new Substantive Patent Law Treaty draft, see %(URL).

#rec7cec: Overeenkomstig het Verdrag inzake de verlening van Europese octrooien, ondertekend in München op 5 oktober 1973, en de octrooiwetten van de lidstaten worden computerprogramma's alsmede ontdekkingen, natuurwetenschappelijke theorieën, wiskundige methoden, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten. Deze uitzondering is echter alleen van toepassing en gerechtvaardigd voorzover een octrooiaanvrage of octrooi betrekking heeft op deze onderwerpen of werkzaamheden als zodanig, omdat de genoemde onderwerpen en werkzaamheden als zodanig niet tot een gebied van de technologie behoren.

#rec7amd: Overeenkomstig het Europese Octrooiverdrag (EOV) en de octrooiwetten van de lidstaten worden computerprogramma's alsmede ontdekkingen, natuurwetenschappelijke theorieën, wiskundige methodes, esthetische vormgevingen, stelsels, regels en methoden voor het verrichten van geestelijke arbeid, voor het spelen of voor de bedrijfsvoering, en de presentatie van gegevens uitdrukkelijk niet als uitvindingen beschouwd en bijgevolg van octrooieerbaarheid uitgesloten.  Deze %(s:uitsluiting) is van toepassing en gerechtvaardigd voorzover een octrooiaanvraag betrekking heeft op deze onderwerpen of werkzaamheden als zodanig. %(s:Het zou dus kunnen dat ze niet van toepassing is als de geopenbaarde uitvinding zich niet als dusdanig in een computerprogramma bevind, maar in een %(tp|techische uitvinding|bv. nieuw chemisch proces) die uitgevoerd wordt onder controle van een programma.  In dit geval zou het programma zelf vrij implementeerbaar zijn, met als mogelijk doel simulatie, maar het chemische proces zelf zou onder het octrooi vallen).

#rec7just: According to the normal rules of semantics, %(q:program as such) can only mean %(q:program as program), not %(q:program as something non-technical).  According to Art 52(2) EPC, programs for computers (calculation rules for general-purpose data processing equipment) are not technical inventions.  Art 52 does not mention a delimiting criterion called %(q:technical).  The patent courts are however free to define their own (secondary) concept of %(q:technical invention) as long as this concept conforms to the specifications of Art 52(2) EPC.  The concept of %(q:technical invention) must conform to Art 52(2), not vice versa.

#j9e: In its resolution (published in OJ C 378, 29.12.2000, p. 95) on a decision by the EPO with regard to patent No EP 695 351 granted on 8 December 1999, the European Parliament demanded a review of the EPO to ensure that it becomes publicly accountable in the exercise of its functions.

#sea: The EPO is not an EU institution and concerns have previously been raised about its accountability.

#Pha: Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.

#Pri: Patents are temporary monopolies granted by the state to inventors in order to stimulate technical progress.  In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed.  In particular, inevitable corrollaries of the patent system such as restriction of creative freedom, legal insecurity and anti-competitive effects must be kept within reasonable limits.

#ist: Innovators can benefit from their creativity without patents.  Whether patent rights %(q:protect) or stifle innovation and whether they act in the interests of society as a whole is a question that can only be answered by empirical study, not by dogmatic statements in a law.

#I3H: In accordance with %(fn:43:Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs), the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright.

#InW: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as an %(s:individual creation).  General ideas and principles which underlie any element of a computer program are %(s:explicitely exempted from copyright).

#asr: Copyright does not only apply to literary works, but also to textbooks, operation manuals, computer programs and all kinds of information structures. Copyright is %(e:the) system of %(q:intellectual property) for computer programs, not only a system for a %(q:literary) side aspect of computer programs.  If copyright does not cover the %(q:underlying idea) of a book or a program then that is not an indication of an insufficiency of copyright but rather an indication of the need to keep %(q:underlying ideas) (general concepts) free, so that many different creators have a chance to obtain property in individual works based on these general concepts.

#Isc: In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology.

#IWd: In order for any idea to be considered an invention in the sense of the patent system it is necessary that it should have a technical character.  Patents are temporary monopolies granted in return for a disclosure of a technical invention.  %(s:Every technical invention belongs to a field of technology, but not every patent claim to an apparent %(q:product or process in a field of technology) also discloses a technical invention).

#Elu: The Commission text is not in line with Art 52 EPC.  Art 52(2) EPC lists examples of non-inventions.  It is not permissible to subsume these under %(q:inventions) and then test their technical character.  Moreover, while it can not be inferred from Art 52 EPC that all technical innovations are inventions, it can, based on a unanimous tradition of patent law, be assumed that all inventions have technical character.

#rec11cec: Hoewel van in computers geïmplementeerde uitvindingen wordt aangenomen dat ze tot een gebied van de technologie behoren, moeten zij, om op uitvinderswerkzaamheid te berusten zoals uitvindingen in het algemeen, een technische bijdrage tot de stand van de techniek leveren.

#rec11amd: Regels van organisatie en berekening behoren niet tot het gebied van de technologie, maar tot het gebeid van de gegevensverwerking, informatica, wiskunde, logica en tot op zekere hoogte zelfs sociale wetenschappen.  Computerprogramma's bestaan uit entiteiten binnen een abstract model dat de %(q:Turing Machine) heet.  Hoe dan ook worden computerprogramma's algemeen gebruikt voor het beschrijven en implementeren van technische leerstellingen.  Om als een technische uitvinding te worden beschouwd moet een leer een bijdrage leveren tot de stand van de techniek in het bruikbaar maken van beheersbare natuurkrachten.

#rec11just: The Commission text declares computer programs to be technical inventions.  It removes the independent requirement of invention (%(q:technical contribution)) and merges it into the requirement of non-obviousness (%(q:inventive step)).  This leads to theoretical inconsistency and undesirable practical consequences, as explained in detail in the justification of our amendment to 4(2).

#Ahi: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Aho: Accordingly, a teaching that does not make a technical contribution to the state of the art is not a technical invention and thus not patentable.

#pef: The European Commission text merges the %(q:technical invention) test into the %(q:inventive step) test, thereby weakening both tests and opening an infinite space of interpretation.  This deviates from Art 52 EPC, is theoretically inconsistent, and leads to undesirable practial consequences, such as making examination at some national patent offices infeasible.  See the Justification of Art 4(2) for detailed explanation.

#Aaw: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Aao: A defined procedure or sequence of actions when performed with help of an apparatus such as a computer may contribute to our knowledge about the cause-effect relations of controllable forces of nature and thereby constitute a patentable invention.  However, an algorithm, regardless of whether the symbolic entities of which it is composed can be interpreted as referring to a physical environment, is inherently non-technical and cannot therefore constitute a patentable invention.  In particular, references to normal realisations of the Universal Computer, the Turing Machine or other mathematical models, whose physical applicability is beyond any doubt, cannot confer a technical character on an algorithm.  A computer program is no more than an algorithm expressed in the most general terms: those of the universal computer.

#tij: The Commission and JURI Proposals make algorithms patentable, if only they are claimed by reference to a universal computer (general-purpose data processing system), which is the common and most general form of presenting and using algorithms.

#defalgo: According to the common understanding of computer science, an algorithm could be defined as a %(q:rule of calculation using abstract entities, such that the result of the calculation is independent of any physical environment to which the entities may be mapped).

#dixitknuth: The Commission and JURI try to impose a distinction between physical and non-physical (%(q:technical) and %(q:non-technical)) algorithms, thereby repeating an error on which computer-science pioneer Professor Donald Knuth commented in 1994:

#aha: I am told that the courts are trying to make a distinction between mathematical algorithms and nonmathematical algorithms.  To a computer scientist, this makes no sense, because every algorithm is as mathematical as anything could be.  An algorithm is an abstract concept unrelated to physical laws of the universe.

#mca: Therefore the idea of passing laws that say some kinds of algorithms belong to mathematics and some do not strikes me as absurd as the 19th century attempts of the Indiana legislature to pass a law that the ratio of a circle's circumference to its diameter is exactly 3, not approximately 3.1416.  It's like the medieval church ruling that the sun revolves about the earth.  Man-made laws can be significantly helpful but not when they contradict fundamental truths.

#WWe: The mere fact that a method may allow data processing to be carried out more quickly, more efficiently or using less memory is not by itself sufficient to cause such a method to be patentable when otherwise it would not be.

#Woa: In the words of the German Federal Patent Court (Error Search decision, 26 March 2002, BPatG 17 W (pat) 69/98):

#osm: If computer implementations of non-technical processes were attributed a technical character merely because they display different specific characteristics, such as needing less computing time or less storage space, the consequence of this would be that any computer implementation would have to be deemed to be of technical character.

#Tet: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#sed: The aim of the directive is to reaffirm the legislator's commitment to limiting the scope of patentability in the spirit of Art 52 EPC, whose title %(q:patentable inventions) is referenced in our amendment.  This directive should avoid any affirmation of patentability of undelimited categories such as %(q:computer-implemented invention).

#Tsc: This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.

#Tho: This Directive should be limited to laying down certain principles %(s:concerning the limits of patentability with regard to computer programs), such principles being intended in particular to ensure that %(s:technical inventions are susceptible of patent protection and that rules of organisation and calculations are not so susceptible).

#eea: The term %(q:computer-implemented inventions) is designed for abuse, as explained in the amendment to the title.

#Ttu: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#End: The legal security enjoyed by %(s:European citizens) and the competitive position of European %(s:software entrepreneurs) in relation to Europe's major trading partners would be improved if it was %(s:made clear that Europe will not extend patentability beyond the field of technical inventions).

#xWa: Unification of caselaw in itself is not a guarantee of improvement of the situation of European industry.  This directive should not use a pretext of %(q:harmonisation) for changing the rules of Art 52 EPC, which are already in force in all countries.  Also, as explained above, the term %(q:computer-implemented invention) implies that programs for computer are patentable inventions and thereby calls for violation of Art 52 EPC.

#dte: At the international level, Europe is ahead in the area of open, alternative development and licensing approaches to computer programs, e.g. open source projects under the %(q:GNU General Public License). Particularly in the light of increasing requirements for stability, interoperability and IT security of computer programs, open source computer programs developed on a common, ongoing and transparent basis are gaining in importance.  In order to turn this European lead in terms of development into a real competitive advantage, a reliable legal framework for such alternative development and licensing approaches must be maintained.

#Wsi: This is JURI Amendment 35 as submitted by Evelyn Gebhardt but with a slightly simplified last sentence.  This amendment proposal states a generally acknowledged European policy goal.  It is to be wondered why it didn't find the support of the JURI majority.

#TWn2: This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Aod: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Teh: This directive %(s:upholds the %(dp:principle of separation of property systems)).  It ensures that the patent system stays confined to the realm of technical inventions and that %(s:results of abstract reasoning such as algorithms are safeguarded as a public domain and its expression stays within the area of copyright).  If there is any property system that could be allowed to restrict the use of algorithms, then that is copyright.  The public domain of algorithms, executed either in the human mind or on a universal computer, is protected by the freedom of publication and other basic freedoms which form part of the constitutional law of the Community and its member states and are considered to be of particular importance for the development of the information society.

#ntl: Het EG voorstel is bedrieglijk.  Het beweert het recht van het volk op compatibiliteit te bescheremen, maar doet in feite precies het omgekeerde.  Octrooien bieden een veel grotere exclusiviteit dan auteursrecht.  Daarom moet de compatibiliteit-vrijstelling voor octrooien veel breder zijn dan die voor auteursrecht.    Deze beperken tot het gebied van de bestaande auteursrechten is een middel om ervoor te zorgen dat de rechten van de octrooihouder primeren boven deze van compatibiliteit.

#SjW: Since the objectives of the proposed action, namely to harmonise national rules on computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.

#yWn: It is not true that the member states themselves are not able to harmonise patent law.  They have already completely harmonised it by means of the European Patent Organisation, of which they are members.  Thus the only remaining tasks are %(ol|clarification of certain rules where confusion has arisen|establishment of democratic control over the already existing European Patent System in a central question, that of the limits of patentability).  If these aims and the subsidiarity principle is to be taken seriously, it follows that the directive must be strictly target-oriented.  It must limit itself to specifying in the exactest possible terms which kinds of patents should be granted and which not and which rights should be derived therefrom, and leave internal details of legal methodologies by which this is to be achieved to the member states.  The directive must in particular not transfer opaque terminology such as %(q:technical contribution in the inventive step) from the EPO's non-binding internal practise into binding written law.

#HTD: HAVE ADOPTED THIS DIRECTIVE:

#Sco: Scope

#Dit: Definitions

#techkamp: Gebieden van technologie

#Cse: Conditions for patentability

#Cnl: Forbiddable Activities and Forms of Claims

#rIb: Compatibiliteit

#Mir: Monitoring

#ReW: Report on the effects of the Directive

#Iet: Implementation

#EWW2: Entry into force

#Ars: Addressees

#Tsm: This Directive lays down rules for the patentability of computer-implemented inventions.

#Tap: This directive explains the rules concerning the limits of patentability and patent enforcability with respect to computer programs.

#eWo: The term %(q:computer-implemented invention) seems to imply that ideas which can be implemented merely by expressing them in the terms of an abstract model called Universal Computer are patentable inventions.  Such an implication would be in contradiction with  Article 52(2), according to which algorithms and programs for computers are not inventions in the sense of patent law.  It can not be the aim of the current directive to declare algorithms and programs for computers to be patentable inventions.  Rather the aim is to clarify the limits of patentability with regard to computer programs.  This should be unambiguously expressed in the title.

#ume: Computer-Implemented Invention

#otv: Technology, Invention, Non-Inventions

#FWW: For the purposes of this Directive the following definitions shall apply:

#kompinvcec: %(q:in computers geïmplementeerde uitvinding): uitvinding voor de werking waarvan het gebruik van een computer, computernetwerk of een ander programmeerbaar apparaat nodig is en die een of meer op het eerste gezicht nieuwe kenmerken heeft die geheel of gedeeltelijk door middel van een computerprogramma of computerprogramma's worden gerealiseerd;

#kompinv: %(q:computerised invention), also called %(q:computer-implemented invention), means an innovation the implementation of which involves the use of a data processing system in connection with peripheral devices, and which, due to the way in which the peripheral devices are used, is considered to be an invention in the sense of patent law.

#kompinvjust: The term %(q:computer-implemented invention) is not used by computer professionals.  In fact it is not in wide use at all.  It was introduced by the EPO in 2000 in an attempt to legitimate patents on %(q:computer-implemented business methods).  It suggests that ideas which can be put to work merely by executing a program on generic computing equipment are patentable inventions.  This amendment eliminates the confusion.  It makes it clear that an inventive washing machine does not cease to be an invention just because it is controlled by a computer.  In a EU Press Release (%(q:MEPs vote to tighten up rules on patentability of computerised inventions), Date: 2003-06-18, on cordis.lu) about this directive, the term %(q:computerised inventions) was introduced, and it was explained that %(q:such inventions do not cover ordinary software programs, but rather solutions for devices such as mobile phones, intelligent household appliances, engine control devices, ...).  Indeed the term %(q:computerised invention) depicts more intuitively than %(q:computer-implemented invention) what MEPs meant to be patentable: inventive use of hardware which has been placed under the control of a data processing system.

#cWi: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#cua: %(q:technical contribution), also called %(q:invention), means a %(q:contribution to the state of the art in a technical field).

#Wad: The Commission text merges the invention (technical contribution) test with the non-obviousness (inventive step) test, thereby weakening both tests, deviating from Art 52 EPC, and creating practical problems, see Justification in the Amendment of 4(2).

#applsci: %(q:Technology) in the sense of patent law is %(applied natural science).

#qce: The Commission text shift the question of what is a patentable invention to the question of what is %(q:technical) and then leaves that question unanswered.  The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day caselaw and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such but inventions related to mobile phones, engine control devices, household appliances, ...)) .  This understanding should be made explicit for the purpose of clarification.

#Ain: Technology in the sense of patent law is the use of controllable forces of nature to immediately achieve a causally overseeable success.  An %(e:invention) in the sense of patent law, also called %(q:technical invention), %(q:technical teaching), %(q:technical solution) or %(q:technical contribution), is a %(e:problem solution involving controllable forces of nature), or, in other words, a %(e:teaching about cause-effect relations in the use of controllable forces of nature).

#cnW: %(q:Technical) in the sense of patent law means %(q:concrete and physical).

#Wxb: An %(e:invention) in the sense of patent law is a %(e:technical solution to a technical problem).

#Coa: This is a common EPC doctrine which has been regularly used by most patent courts, including the Technical Chambers of Appeal of the EPO.

#techinv: %(q:Invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature).

#techinvjust: This is a standard patent doctrine in most jurisdictions.  The EPO says that inventions are %(q:technical solutions of technical problems) and understands %(q:technical) as %(q:concrete and physical).  The term %(q:controllable forces of nature) clarifies this further.  The %(q:four forces of nature) are an acknowledged concept of epistemology (theory of science).  While mathematics is abstract and unrelated related to %(e:forces of nature), some business methods may well depend on the chemistry of the customer's brain cells, which is however not %(e:controllable), i.e. non-deterministic, subject to free will.  Thus the term %(q:controllable forces of nature) clearly excludes what needs to be excluded and yet provides enough flexibility for inclusion of possible future fields of applied natural science beyond the currently acknowledged %(q:4 forces of nature).  This concept has been formulated in most jurisdictions and even written into the law in some countries such as Japan and Poland.  Even the CEC and JURI proposals say that %(q:algorithms and business methods are inherently non technical), and the JURI report associates %(q:technical contributions) with %(q:mobile phones, household appliances, engine control devices, ...).  The classical justification for the %(q:technical character) of %(q:computer-implemented inventions) is not that the meaning of %(q:technical) has changed but that the computer indeed consumes energy in a controlled way, and that the %(q:invention) must be %(q:considered as a whole).  The critics of this view, e.g. the German Federal Patent Court, argue that %(q:the solution is completed by abstract calculation before, during its non-inventive implementation on a conventional data processing system, forces of nature come into play).

#wcn: An %(q:invention) in the sense of patent law, also called %(q:technical teaching), %(q:technical solution) or %(q:technical contribution), is a %(e:teaching of plan-conformant use of controllable forces of nature to immediately, without mediation by human reason, achieve a causally overseeable result.)

#nol: This is a frequently used definition, as used by the German Federal Court of Justice in its %(q:Anti-Blocking System) decision of 1980.  It allowed the court to accept a patent for a computer-controlled anti-blocking system while rejecting a patent for more abstract computing problems such as determining optimal lengths of rolling rods or optimal distribution of air-plane fuel based on flying distances.

#Aoj3: A %(e:calculation rule), also called %(e:algorithm), is a teaching about relations within constructions of the human mind, such as models and axiomatic systems, including formal models of computers such as the Turing Machine or the Von Neumann Machine.

#AlB: An %(e:organisation rule) is a teaching about relations between material phenomena that are not determined by controllable forces of nature.  Methods of business and social engineering are organisation rules.  Most organisation rules are direct applications of calculation rules to known models of social relations.  %(e:Rules of organisation and calculation) are teachings about cause-effect relations that are not determined by controllable forces of nature.

#Wtu: These additional definitions could help to establish terminology which can be used in order to refer to various types of non-inventions.  The generic term %(q:rules of organisation and calculation) is taken from the German patent jurisdiction of %(dp:Dispositionsprogramm) and later.  It may sound less familiar than %(q:algorithms and business methods).  Its advantage is that it is composed of more elementary words and thereby more conducive to clear understanding.  Another name for the non-inventions could be %(q:immaterial innovations).

#CeW: Computer-implemented inventions as a field of technology

#iug: The article specifies the relation of the European invention concept with Art 27 TRIPs.  Thus we are talking about fields of technology in the plural, and inventions can only %(q:belong) to such a field.  To which field they may belong is not determined by the means of implementation.  Note that the title in the french and german versions of the CEC proposal differs from that in the english version.

#Mue: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#uan: Member States shall ensure that an innovation is not considered to belong to a field of technology merely because its implementation involves the use of a computer.

#tnT: The European Commission text says that all ideas, including %(q:computer-implemented business methods) etc, are patentable inventions.  This has been widely criticised.  See Justification for 3a.

#lrL: This is a translation of Art 52.2c EPC to the language of Art 27 TRIPs.  It answers one of the core controversies on which directive is supposed to give an answers.  The %(cp:CULT proposal) contains a similar provision.

#oaf: If data processing is %(q:technical), then anything is %(q:technical).

#otg: Data processing is a common denominator of all fields of technology and non-technology.  With the advent of the universal computer in the 1950s, automated data processing (ADP) became pervasive in society and industry.

#d1w: As Gert Kolle, a leading theoretician behind the decisions of the 1970s to exclude software from patentability, writes in 1977 (see %(URL)):

#Wmr: ADP has today become an indispensable auxiliary tool in all domains of human society and will remain so in the future.  It is ubiquitous.  ... Its instrumental meaning, its auxiliary and ancillary function distinguish ADP from the ... individual fields of technology and liken it to such areas as enterprise administration, whose work results and methods ... are needed by all enterprises and for which therefore prima facie a need to assure free  availability is indicated.

#dnW: CULT was well-advised to vote for a clear exclusion of data processing from the scope of %(q:technology).

#yag: Numerous studies, some of them conducted by EU instiutions as well as the opinions of the European Economic and Social Committee and the European Comittee of Regions explain in detail why Europe's economy will suffer damage, if data processing innovations are not clearly excluded from patentability.

#Mii: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#MaW: Member states shall ensure that patents are granted only for technical inventions, which are new, non-obvious and industrially applicable.

#IWt: Saying that %(q:inventions are patentable) is tautological in the context of patent law.  The question is: what is an invention?  The Commission text seems to be saying that any solution which is implemented by a computer is an invention, and that there is no invention test but only tests of novelty, non-obviousness and industrial applicability.   Thereby the Commission text drastically changes the rules of Art 52 EPC and allows unlimited patentability.   It would possible to delete the Commission's Article 4 entirely, because, once the errors are corrected, we arrive at little more than a literal restatement of Art 52 EPC.  Yet, in order to correct a recent practise of the EPO, it may be a good idea to restate that the %(q:technical contribution) is a synonym of %(q:invention) and unrelated to the %(q:inventive step) test.  This has been pointed out by many EPO-friendly law scholars, see e.g. the dissertation of Ralph Nack, whom the Commission cites in its explanatory memorandum.

#art42cec: De lidstaten zorgen ervoor dat een voorwaarde opdat een in computers geïmplementeerde uitvinding op uitvinderswerkzaamheid berust, is dat deze een technische bijdrage levert.

#art42amd: Member states shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#art42just: Non-obviousness (= %(q:inventive step)) and the presence of a technical invention (= %(q:technical contribution)) are two separate requirements.  Merging them into one is counter-intuitive and leads to practical problems, among others that the invention needn't be new and that patent offices are no longer entitled to reject patents on non-inventions without first conducting a wasteful prior art search.

#aea: The Commission text opens several doors for unlimited patentability by deviating from Article 52 EPC.  The %(EP) explain Art 52 EPC as follows:

#epogl78: EPO Examination Guidelines

#Tsf: There are four basic requirements for patentability:

#Tbi: There must be an %(qc:invention).

#Tcd2: The invention must be %(qc:susceptible of industrial application).

#Tie: The invention must be %(qc:new).

#Ttx: The invention must involve an %(qc:inventive step).

#Ihp: In addition to these four basic requirements, the examiner should be aware of the following two requirements that are implicitely contained in the Convention and in the Regulations:

#Taa: The invention must be such that it can be carried out by a person skilled in the art [...]

#Tce: The invention must be of %(q:technical character) [...]

#Wse: The Commission text deviates from Art 52 EPC by merging the independent requirements %(q:technical invention) (renamed to %(q:technical contribution)) into the requirement of %(q:inventive step).  This is counter-intuitive and theoretically inconsistent, as even the European Commission implicitely admits in its Explanatory Memorandum:

#htr: The presence of a %(q:technical contribution) is to be assessed ... under inventive step.  Experience has shown that this approach is the more straightforward to apply in practice.

#Waf: Indeed the approach has no theoretical merits but significant practical effects.  It opens an infinite space of interpretation for those patent offices that conduct a full substantive examination, such as the EPO and the German Patent Office, while preventing other patent offices (such as French, Italian) from rejecting patents on non-inventions.

#nee: Furthermore, treating %(q:computer-implemented) inventions in a special way, as suggested by the European Commission, runs counter to Art 27 TRIPs. And, as has been pointed out above, the term %(q:computer-implemented invention) itself may run counter to Art 52 EPC by implying that algorithms framed in terms of generic computing equipment (programs for computers) are patentable inventions.

#era: The amendment solves all these problems by restating the structure of Art 52 EPC as explained in the EPO's Examination Guidelines.

#art43cec: De technische bijdrage wordt beoordeeld door het bepalen van het verschil tussen de omvang van de in haar geheel beschouwde octrooiconclusie, waarvan elementen zowel technische als niet-technische kenmerken kunnen omvatten, en de stand van de techniek.

#art43amd: The technical contribution shall be assessed by consideration of the difference between the scope of the technical features of the patent claim as a whole and the state of the art.

#art43just: The Commission and JURI versions imply that a %(q:technical contribution) can consist solely of non-technical features.  This is self-contradictory and leads to unlimited patentablity.  Amendment CULT-15 corrects the error, as far as possible.

#nno2: Bij het bepalen of een aanvraag een technische uitvinding bevat, moet men de vorm en het soort aanvraag negeren en zich concentreren op de inhoud om te identificeren wat de niet voor de hand liggende bijdrage tot de bekende staat van de techniek is die de aanvraag bevat.  Indien deze bijdrage geen technische uitvinding vormt, is er geen octrooieerbaar onderwerp.

#WoW4: %(q:Claim features) can not be a meaningful subject of legal regulation.  This amendment is an excerpt from the %(EP).  It exhorts examiners to disregard the form of the claims and to distrust all attempts at formalising the procedure of patent examination.  Formalisation puts the application drafter in control and promotes an uncritical attitude on the part of the examiner.

#bpatg02: Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of ressources within the data processing system.

#bpatg02just1: This amendment reflects current caselaw in Germany.  In the words of the justices of the German Federal Patent Court (BPatG, decision of 26. March 2002, 17 W (pat) 69/98, %(URL)):

#bpatg02just2: Ein entscheidendes Indiz für die Technizität des Verfahrens sieht die Anmelderin darin, dass es auf einer technischen Problemstellung beruht. Dadurch, dass das vorgeschlagene Verfahren nicht mit einem Wörterbuch arbeite, könne der Speicherplatz hierfür entfallen. ...  Was die technische Aufgabenstellung anlangt, so kann hierin nur ein Indiz, nicht aber ein Nachweis für die Technizität des Verfahrens gesehen werden.  Würde Computerimplementierungen von nichttechnischen Verfahren schon deshalb technischer Charakter zugestanden, weil sie jeweils unterschiedliche spezifische Eigenschaften zeigen, etwa weniger Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur Konsequenz dass jeglicher Computerimplementierung technischer Charakter zuzubilligen wäre. Denn jedes andersartige Verfahren zeigt bei seiner Implementierung andersartige Eigenschaften, erweist sich entweder als besonders rechenzeitsparend oder als speicherplatzsparend. Diese Eigenschaften beruhen - jedenfalls im vorliegenden Fall - nicht auf einer technischen Leistung, sondern sind durch das gewählte nichttechnische Verfahren vorgegeben. Würde schon das Erfüllen einer solchen Aufgabenstellung den technischen Charakter einer Computerimplementierung begründen, so wäre jeder Implementierung eines nichttechnischen Verfahrens Patentierbarkeit zuzubilligen; dies aber liefe der Folgerung des Bundesgerichtshofs zuwider, dass das gesetzliche Patentierungsverbot für Computerprogramme verbiete, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten.

#FWc: Form of Claims

#pte: EU directives should be target-oriented and interfere in implementation details as little as possible (subsidiarity principle).  When talking about patent claims, we start by talking about the activities from which they may exclude a competitor and then about the legal form by which they may or may not do that.

#art5cec: De lidstaten zorgen ervoor dat een uitvinding kan worden geclaimd als product, bijvoorbeeld zoals een stel geprogrammeerde apparaten, bevattende een computer en een toestel dat natuurkrachten op een nieuwe manier gebruikt, of zoals een proces dat uitgevoerd wordt duur zulke geprogrammeerde apparaten.

#produktjust: This article explains the meaning of the terms %(q:product) and %(q:process) in the context of computerised inventions. The original version interprets both terms correctly but has an undesirable side-effect: it suggests that algorithms framed in terms of generic computing equipment (programs for computers as such) are or can be %(q:inventions).  The amendment corrects the error. The inventive products and processes are characterised not by the data processing system but by the peripheral devices, which could e.g. be an automobile brake, a rubber-curing furnace or a washing machine.

#publig: De lidstaten zorgen ervoor dat de publicatie of verspreiding van informatie, ongeacht in welke vorm, nooit een rechtstreekse of onrechtstreekse inbreuk op een octrooi kunnen vormen. De lidstaten zullen de vrijheid van verbale uitdrukking en informatie uitwisseling garanderen ongeacht de taal en het doel van de teksten in kwestie, en zullen verzekeren dat deze vrijheid enkel kan beperkt worden door auteursrechtelijke bescherming maar nooit door octrooien.

#publigjust: This is to explain how the patent rights are limited by other legal values such as freedom of publication (Art 10 ECHR) and property in individual creations (intellectual property, in the case of software: copyright).  This amendment makes it clearer that both freedom of publication and the right to control one's individual creations (copyright) are more fundamental than patent rights and therefore constitute a limit on the allowable scope of patent rights.  Wherever copyright is applicable, patents are not, and vice versa.  The principle of %(q:maximal separation of spheres of intellectual property) is stated in the %(LST) decisions of the German Federal Court of Justice, and it underlies Article 52 of the European Patent Convention.

#kompexe: De lidstaten zorgen ervoor dat de uitvoering van een computer programma op een gegevensverwerkende machine (met andere woorden een computer met toestellen voor gegevensverwerking en gegevensuitwisseling met mensen of computers) geen rechtstreekse of onrechtstreekse inbreuk op een octrooi kan vormen.

#kompexejust: This is another concretisation the core principle of this directive in the context of patent enforcability.  It is based on the distinction between data processing equipment (system of processors with devices for storage and manipulaton of data and for exchange of data with humans and computers) and peripheral devices such as automobile brakes or rubber-curing furncases, which maniuplate material phenomena (forces of nature) rather than information.

#scrambled: Member states shall ensure that scrambled information is considered to be public knowledge as soon as it is factually possible for the public to obtain this information, even if obtaining it required tedious or illegal procedures of decompilation, descrambling or decryption.

#lli: This is to ensure that information (software, databases etc) that is published in closed-source (binary) form can, just like information that is openly accessible, be cited as prior art against a non-novel patent.  A generous understanding of what can constitute prior art is one of very few possible ways of reducing the number of trivial patents.  If software patents are permissible, this is moreover helps to reduce legal insecurity and possible abuses arising from the fact that many computer programs are published in binary form.

#prekre: Member states shall ensure that any copyrightable work that was in use before the publication date of a patent on which it infringes may be further developped, distributed and used for the operation of data processing equipment by the copyright owner as well as by any persons who have been or will be authorised by the copyright owner at conditions to be determined by the copyright owner.

#bWW: This provision is needed only to the extent that information (software) patents are not reliably excluded by the directive.  When the object of a patent is informational, the scope of %(q:prior use rights) becomes unclear:  it becomes either everything or nothing.  %(q:Prior use right) means %(q:the right to continue to commercially exploit the idea oneself within certain limits).   In the case of information products, such limits cannot be defined.  Information does not require distribution efforts but at best more or less futile efforts to restrict distribution.  Valuable software tends to become freely available with source code sooner or later in its lifecycle.  Patents should be an incentive to publication.  When chosing between %(q:everything or nothing), we say %(q:everything).

#progpub: Member states shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program shall be published as a part of description wihtout any restricting licensing terms.

#oWc: Patents are two-sided deals: the inventor discloses information in return for a monopoly.  Computer programs are information, whereas the monopoly affects only physical objects.  Nothing describes a computerised process more accurately than a well written program.  This provision is not designed to promote open-source software but rather to make the patent system take its function of knowledge disclosure seriously.  It is similar to ITRE-9, CULT-31 and CULT-32.

#RWi: Relationship with Directive 91/250 CEC

#rtW: The title should clearly specify the purpose of the provision rather than secondary aspects such as which other laws may at present be involved in achieving this purpose.

#dekompil: Handelingen die zijn toegestaan op grond van Richtlijn 91/250/EEG betreffende de auteursrechtelijke bescherming van computerprogramma's, met name de daarin opgenomen bepalingen betreffende decompilatie en compatibiliteit, of de bepalingen betreffende topografieën van halfgeleiderproducten of merken, worden onverlet gelaten door de bescherming die binnen de werkingssfeer van deze richtlijn door octrooien voor uitvindingen wordt verleend.

#abo: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#dekompiljust: The Commission's text fails to protect interoperability.  Decompilation is not an issue, because patents do not forbid decompilation anyway.  Patent-specific problems need patent-specific solutions.  The Commission's text offers only copyright-specific solutions to patent-specific problems, therebey effectively giving patentees the right to block the use of an idea for purposes of interoperability.  The amdendment ensures that patents are not used as a means to obtain control rights over software which copyright does not offer.

#interop: Member states shall ensure that whereever a use of a patented technique is needed for communicating with another system (i.e. for converting to and from the conventions of that system), such use is not considered to be a patent infringement.

#interopjust: This is ITRE-15 with a slight modification: %(q:computer system or network) was replaced with the %(q:data processing system), so that it is clear that not merely interoperability between computer architectures (e.g. IBMPC and Mac) but between any kind of software systems is protected.   As the ITRE justification says: %(bq:The possibility of connecting equipments so as to make them interoperable is a way of ensuring open networks and avoiding abuse of dominant positions. This has been specifically ruled in the case law of the Court of Justice of the European Communities in particular. Patent law should not make it possible to override this principle at the expense of free competition and users.)

#Tnt: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#lWs: The European Parliament shall monitor the impact of patents on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#swW: Nobody has a natural right to exclude others from using an idea.  The legislator can however grant exclusion rights for certain types of ideas, if that is found to be in the best interests of society as a whole.  In recent years a strong case has been made against patents in various fields such as software and genes, and these have been reflected in consultations conducted by the European Commission.  The European Commission and national administrative organs have however consistently disregarded the outcome of such consultations and instead closely followed the opinions of the patent community, which has taken a de-facto legislative competence into its own hands and is unwilling to share it in a democractic manner.  It has become clear that a permanent democratic legislative process at the European level is needed in order to fill the voids left in the law and to truly clarify the limits of patentabiity.  A committee of the European Parliament is the appropriate form for this.

#Tme2: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#WiW: The process of clarifying patentability rules must not be prejudiced by an automatism of expansion.  It must be clear that patents are hypothetical property rights which can not only be invalidated by the appearance of prior art but also by changes of interpretation of certain variables such as the concept of %(q:technical invention), as long as these changes are conducted in a systematic manner and not arbitrarily directed against individual patents or individual fields of technology.

#Wet: Reporting is needed not only once but at regular intervals.  In particular it cannot be expected that after 3 years much will be observable, as the EPO usually takes about 5 years to grant a patent.

#rbr: De Commissie brengt bij het Europees Parlement en de Raad tegen uiterlijk [DATUM (drie jaar vanaf de in artikel 9, lid 1, vermelde datum)] verslag uit over:

#har: The Monitoring Committee shall publish a summary report about the insights gained by the scientific communities every two years.  The Parliament shall have the power to demand revisions based on findings of these reports.  If the direcitve is not revised to the satisfaction of the Parliament within two years, the directive shall cease to be in force.

#WWl: Reporting at regular intervals is needed, and it must be part of a real review process.

#teW: the impact of patents for computer-implemented inventions on the factors referred to in Article 7;

#wtn: whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#wra: whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.

#nno: which computer-related patents have been granted, which refused, which enforced, to what extent these patents are valid and enforcable, who the current owners are, and to what extent licenses are available to the public for each granted patent.

#tta: to what extent the patent examination system is operating as intended, whether there are sufficient incentives to reject unjustified patent claims, what kind of reforms might be needed in order to allow for a quick and reliable separation of wanted from unwanted patents.

#drc: to what extent european professionals in software and other concerned business fields are satisfied with the current scope of exclusivity and where they see a need for a broader or narrower scope.

#tie: The report should lead to real insights about the current functioning and effects ofthe patent system and not just provide another opportunity for the patent lawyers DG Internal Market to state their beliefs about the universal beneficiality of patents.  One of the most harmful obstacles to obtaining information about the functioning of the patent system is that the public patent databases do not tell us who owns the patent and under what license they can be used.  By obliging the EU to produce a report, we can induce patent offices to make such information available as far as possible and to provide incentives to companies to make it available.

#Mvf: Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.

#WDi: When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.

#Mia: Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.

#TWi2: This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.

#Tsh2: This Directive is addressed to the Member States.

#Dau: Done at Brussels,

#Fua: For the European Parliament

#Fhu: For the Council

#Pres: The President

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prop ;
# txtlang: nl ;
# multlin: t ;
# End: ;

