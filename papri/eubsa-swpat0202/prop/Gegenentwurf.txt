
Vorschlag f�r eine
RICHTLINIE DES EUROP�ISCHEN PARLAMENTS UND DES RATES
�ber die Grenzen der Patentierbarkeit
im Hinblick auf Computerprogramme


...

auf Vorschlag des/der ...


(1) Die Verwirklichung des Binnenmarktes erfordert, dass Beschr�nkungen
    des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden.
    Dabei soll ein Umfeld geschaffen werden, welches Innovationen und
    Investitionen beg�nstigt. Ein verl�sslicher und ausgewogener
    Leistungsschutz im Bereich der Software-Entwicklung ist eine wichtige
    Voraussetzung f�r den Erfolg des Binnenmarktes. Innovative
    Software-Entwickler sollten weder durch bequeme, kostenscheuende
    Nachahmung noch durch die Monopolisierung von grundlegenden Ideen
    und Schnittstellen an der Entwicklung und Verwertung ihrer Werke
    gehindert werden.


(2) Computer-implementierte Organisations- und Rechenregeln (mathematische
    Methoden, Pl�ne und Regeln f�r geistige und gesch�ftliche T�tigkeiten,
    Programme f�r Datenverarbeitungsanlagen, Wiedergabe von Informationen
    usw.) werden gem�� den Gesetzen der Mitgliedsstaaten nicht als
    patentf�hige Erfindungen angesehen.

(3) Dennoch wurden Patente auf solche Organisations- und Rechenregeln
    erteilt, da in den Patentanmeldungen diese Regeln in einen technischen
    Sachverhalt eingekleidet wurden, der nur den Zweck hatte, eine
    patentf�hige Erfindung vorzut�uschen. Solche Umgehungsversuche wurden
    von einigen Patent�mtern und -gerichten entweder nicht erkannt oder in
    Verkennung der Rechtslage f�r legitim gehalten. Als Folge dieser
    Entwicklung entstand eine uneinheitliche Vergabe- und
    Rechtsprechungspraxis.


(4) Es war eine sehr vern�nftige und weitsichtige gesetzgeberische
    Entscheidung, nichttechnische Innovationen von Patenten frei zu
    halten, da anderenfalls Innovation und Fortschritt in diesen
    Bereichen gef�hrdet w�rden. Die zunehmende Verbreitung und Nutzung
    von Computerprogrammen in allen Lebensbereichen im Zusammenhang mit
    der unheinheitlichen Vergabe- und Rechtsprechungspraxis bei Patenten
    sind ein kritischer Faktor f�r die Innovation sowohl im technischen
    als auch im nichttechnischen Bereich.


(5) Deshalb sollten die unterschiedlich ausgelegten Rechtsvorschriften
    �berarbeitet und pr�zisiert werden. Die dadurch entstehende
    Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen
    Klima im Softwarebereich beitragen.


(6) Die Gemeinschaft und ihre Mitgliedstaaten sind auf das �bereinkommen
    �ber handelsbezogene Aspekte der Rechte des geistigen Eigentums
    verpflichtet (TRIPS-�bereinkommen), und zwar durch den Beschluss des
    Rates 94/800/EG vom 22. Dezember 1994 �ber den Abschluss der
    �bereink�nfte im Rahmen der multilateralen Verhandlungen der
    Uruguay-Runde (1986-1994) im Namen der Europ�ischen Gemeinschaft in Bezug
    auf die in ihre Zust�ndigkeiten fallenden Bereiche. Nach Artikel 27
    Absatz 1 des TRIPS-�bereinkommens sollen Patente f�r Erfindungen auf
    allen Gebieten der Technik erh�ltlich sein, sowohl f�r Erzeugnisse als
    auch f�r Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer
    erfinderischen T�tigkeit und sind gewerblich anwendbar. Gem�� dem
    TRIPS-�bereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet
    der Technik Patente erh�ltlich sein und Patentrechte ausge�bt werden
    k�nnen. Dies bedeutet, dass technische Erfindungen, einschlie�lich derer,
    bei deren Ausf�hrung Computerprogramme zum Einsatz kommen, auf allen
    Gebieten der Technik patentierbar sein m�ssen. Innovationen in
    nichttechnischen Gebieten - bespielsweise der Datenverarbeitung -
    sind jedoch nicht patentierbar, auch wenn zu ihrer Ausf�hrung eine
    (technische) Datenverarbeitungsanlage verwendet wird.


(7) Nach dem �bereinkommen �ber die Erteilung europ�ischer Patente
    (Europ�isches Patent�bereinkommen) vom 5. Oktober 1973 (EP�) und den
    Patentgesetzen der Mitgliedstaaten gelten Programme f�r
    Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien,
    mathematische Methoden, �sthetische Formsch�pfungen, Pl�ne, Regeln und
    Verfahren f�r gedankliche T�tigkeiten, f�r Spiele oder f�r gesch�ftliche
    T�tigkeiten sowie die Wiedergabe von Informationen ausdr�cklich nicht
    als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird.
    Dieser Ausschluss gilt jedoch nur, und hat auch seine Berechtigung nur, 
    sofern sich die Patentanmeldung oder das Patent auf die genannten
    Gegenst�nde oder T�tigkeiten als solche bezieht. Sie kommt somit etwa
    nicht zur Geltung, wenn die offenbarte Erfindung nicht in einem
    Datenverarbeitungsprogramm als solchem liegt sondern in einer
    technischen Erfindung (z.B. einem neuen chemischen Vorgang), der
    durch ein Programm gesteuert wird. In diesem Falle w�re das Programm
    selber f�r Simulationszwecke u.�. frei verwendbar, aber
    der chemische Vorgang w�re patentierbar.


(8) Patente sind zeitweilige Monopole, welche der Staat Erfindern gew�hrt,
    um den technischen Fortschritt zu f�rdern. Um sicher zu stellen, dass
    das System wie vorgesehen funktioniert, m�ssen die Bedingungen der
    Erteilung und Durchsetzung von Patenten umsichtig festgelegt werden.
    Insbesondere m�ssen unvermeidbare Begleiterscheinungen des Patentwesens
    wie die Beschr�nkung der Schaffensfreiheit, Rechtsunsicherheit und
    kartellf�rdernde Wirkungen in vern�nftigen Grenzen gehalten werden.


(9) Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 �ber den
    Rechtsschutz von Computerprogrammen sind alle Ausdrucksformen von
    originalen Computerprogrammen wie literarische Werke durch das
    Urheberrecht als individuelle Sch�pfungen gesch�tzt. Die Ideen und
    Grunds�tze, die einem Element eines Computerprogramms zugrunde
    liegen, sind durch das Urheberrecht aus gutem Grund ausdr�cklich
    vom Schutz ausgenommen.


(10) Damit eine Idee als Erfindung im Sinne des Patentrechts gelten kann,
     muss sie auf einem Gebiet der Technik liegen. Jede technische Erfindung
     ist einem Gebiet der Technik zuzuordnen, aber nicht jeder
     Patentanspruch auf ein Produkt oder Verfahren auf einem Gebiet der
     Technik offenbart auch eine technische Erfindung.


(11) Organisations- und Rechenregeln geh�ren keinem Gebiet der Technik
     sondern m�glicherweise dem Gebiet der Informatik, Mathematik oder
     Logik an. Computerprogramme werden jedoch regelm��ig als Mittel
     zur Umsetzung technischer Erfindungen eingesetzt. Um als technische
     Erfindung angesehen zu werden, muss eine computer-implementierbare
     Lehre einen technischen Beitrag zum Stand der Technik darstellen,
     m.a.W. sie muss der �ffentlichkeit neue Erkenntnisse �ber
     Wirkungszusammenh�nge beherrschbarer Naturkr�fte vermitteln.


(12) Folglich ist eine Lehre, die keinen technischen Beitrag zum Stand
     der Technik leistet, keine technische Erfindung und somit nicht
     patentierbar.


(13) Bei einer festgelegten Prozedur oder Handlungsfolge, die in einer
     Vorrichtung, z. B. einem Computer, abl�uft, kann es sich insoweit
     um eine technische Erfindung handeln, wie darin bislang unbekannte
     Wirkungszusammenh�nge beherrschbarer Naturkr�fte genutzt werden.
     Dagegen weist ein Algorithmus, gleichg�ltig ob die symbolischen
     Elemente, aus denen er besteht, als Bez�ge zu einer physischen
     Umgebung interpretiert werden k�nnen, keinen technischen Charakter
     auf. Insbesondere die Bezugnahme auf physische Umsetzungen des
     Universalrechners, der Turing-Maschine oder sonstiger bekannter
     mathematischer Modelle, deren physische Umsetzbarkeit au�er Frage
     steht, kann einem Algorithmus keinen technischen Charakter
     verleihen. Ein Datenverarbeitungsprogramm ist nichts als ein
     Algorithmus, der in den allgemeinsten m�glichen Begriffen formuliert
     wurde, n�mlich denen des Universalrechners.


(14) Um jeglichen Zweifel daran auszur�umen, dass Organisations-
     und Rechenregeln keine Erfindungen sind, ist es nicht erforderlich,
     an stelle des nationalen Patentrechts neue Rechtsvorschriften zu
     schaffen. In manchen L�ndern kann diese Richtlinie vielleicht ganz
     ohne gesetzgeberische Schritte umgesetzt werden. In anderen L�ndern
     mag es ratsam erscheinen, den Gerichten durch Streichung der
     redundanten Als-Solches-Klausel aus den nationalen Entsprechungen
     von Art 52 EP� einen Hinweis zu geben. Die Mitgliedsstaaten des
     EP� k�nnten auch gemeinsam beschlie�en, im Zuge einer Revision
     des Europ�ischen Patent�bereinkommens (EP�) Art 52(3) zu streichen
     und eine Definition des Begriffs "technische Erfindung" in Art 52(1)
     aufzunehmen, um die Gerichte der nationalen und europ�ischen Ebene
     unmissverst�ndlich auf den Wortlaut und Geist des geschriebenen
     Gesetzes zu verpflichten, wie er in dieser Richtlinie erl�utert wird.


(15) Diese Richtlinie sollte sich darauf beschr�nken, bestimmte
     Grunds�tze betreffend die Grenzen der Patentierbarkeit im Hinblick
     auf Datenverarbeitungsprogramme festzulegen, wobei insbesondere
     sicher gestellt werden soll, dass technische Erfindungen
     patentierbar und Organisations- und Rechenregeln nicht patentierbar
     sein sollen.


(16) Es kommt der Rechtssicherheit und den Wettbewerbschancen der
     europ�ischen Softwareunternehmer im Vergleich zu denen von Europas
     wichtigsten Handelspartnern zugute, wenn nunmehr klar gestellt wird,
     dass Europa die Patentierbarkeit nicht �ber den Bereich der
     technischen Erfindungen hinaus ausdehnt.


(17) Diese Richtlinie ber�hrt nicht die Wettbewerbsvorschriften,
     insbesondere Artikel 81 und 82 EG-Vertrag.


(18) Urheberrechtlich zul�ssige Handlungen gem�� der Richtlinie
     91/250/EWG �ber den Rechtsschutz von Computerprogrammen,
     insbesondere deren Vorschriften �ber die Dekompilierung und die
     Interoperabilit�t, oder die Vorschriften �ber Marken oder
     Halbleitertopografien sollen unber�hrt bleiben von dem Patentschutz
     f�r Erfindungen aufgrund diese Richtlinie.


(19) Gem�� Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem
     Subsidiarit�tsprinzip t�tig werden, da die Ziele der vorgeschlagenen
     Ma�nahme, also die Kl�rung der Grenzen der Patentierbarkeit
     im Hinblick auf Computerprogramme, wegen der grenz�berschreitenden
     Natur von Computerprogrammen und anderen Informationsgegenst�nden
     besser auf Gemeinschaftsebene erreicht werden k�nnen. Diese Richtlinie
     steht auch im Einklang mit dem in diesem Artikel festgeschriebenen
     Grundsatz der Verh�ltnism��igkeit, da sie nicht �ber das f�r die
     Erreichung der Ziele erforderliche Ma� hinausgeht.


Artikel 1

Anwendungsbereich

Diese Richtlinie legt die Grenzen der Patentierbarkeit fest
im Hinblick auf Computerprogramme.


Artikel 2

Begriffsbestimmungen

(a) Eine Erfindung, auch "technische Erfindung", "technische Lehre",
    "technische L�sung" oder "technischer Beitrag" genannt, ist eine
    Lehre �ber Wirkungszusammenh�nge beim Einsatz beherrschbarer
    Naturkr�fte.
(b) Eine Rechenregel, auch Algorithmus genannt, ist eine Lehre �ber
    Beziehungen innerhalb von gedanklichen Gebilden wie z.B. Modellen
    und Axiomsystemen, einschlie�lich der Turing-Maschine.
(c) Eine Organisationsregel ist eine Lehre �ber Beziehungen zwischen
    dinglichen Erscheinungen (materiellen Ph�nomenen), die nicht von
    beherrschbaren Naturkr�ften bestimmt sind. Gesch�ftsmethoden und
    soziale Techniken sind Organisationsregeln. Die meisten
    Organisationsregeln sind Anwendungen von Rechenregeln auf bekannte
    Modelle gesellschaftlicher Beziehungen. "Organisations- und
    Rechenregeln" sind Lehren �ber Wirkungszusammenh�nge, die nicht
    von beherrschbaren Naturkr�ften bestimmt sind.
(d) "Technischer Beitrag" ist ein auf einer erfinderischen T�tigkeit
    beruhender Beitrag zu einem Gebiet der Technik, der ein bestehendes
    technisches Problem l�st und f�r eine fachkundige Person den Stand
    der Technik signifikant bereichert.


Artikel 3

Gebiet der Technik

Datenverarbeitung ist kein Gebiet der Technik.
Innovationen auf diesem Gebiet sind keine Erfindungen.


Artikel 4

Voraussetzungen der Patentierbarkeit

1. Die Mitgliedsstaaten stellen sicher, dass Patente nur f�r
   technische Beitr�ge, die

   (a) neu,
   (b) nicht naheliegend und
   (c) gewerblich anwendbar sind.

   Nicht naheliegend ist ein technischer Beitrag, der f�r eine fachkundige
   Person den Stand der Technik signifikant bereichert
   (Artikel 2 Buchstabe d).

2. Bei der Ermittlung des signifikanten Ausma�es des technischen Beitrags
   wird beurteilt, inwieweit sich die technischen Merkmale, die der
   Gegenstand des Patentanspruchs in seiner Gesamtheit aufweist, vom
   Stand der Technik abheben.


Artikel 4a (neu)

Nicht patentf�hige (untechnische) Innovationen

Nicht patentierbar sind insbesondere

1. Neuerungen, die nur darin bestehen, dass einer existierenden
   Erfindung eine Organisations- oder Rechenregel hinzugef�gt wird,
   oder dass eine in der existierenden Erfindung enthaltene Rechen-
   oder Organisationsregel ge�ndert wird,
2. Neuerungen, die darin bestehen, dass einer existierenden
   Erfindung eine Organisations- oder Rechenregel hinzugef�gt wird,
   oder dass eine in der existierenden Erfindung enthaltene
   Organisations- oder Rechenregel ge�ndert wird, und die dar�ber
   hinaus einen technischen Beitrag enthalten, der nicht den
   Anforderungen des Artikel 2 Buchstabe d gen�gt,
3. Neuerungen, die nur darin bestehen, dass dem Stand der Technik
   eine Organisations- oder Rechenregel hinzugef�gt wird,
4. der Betrieb von Datenverarbeitungsanlagen (Universalrechner
   mit Schnittstellen zur Kommunikation mit anderen Rechnern
   und Nutzern und mit handels�blichen Peripherieger�ten).


Artikel 5

Schrankenbestimmungen und erg�nzende Regelungen

1. Die Mitgliedsstaaten stellen sicher, dass die Ver�ffentlichung oder
   Verbreitung von Informationen in jedweder Form niemals eine direkte
   oder indirekte Patentverletzung darstellen kann.
2. Die Mitgliedsstaaten sorgen daf�r, dass ein Datentr�ger nicht dadurch
   zum patentverletzenden Gegenstand werden kann, dass auf ihm
   Computerprogramme gespeichert werden und dass die Verbreitung eines
   solchen Datentr�gers niemals eine direkte oder indirekte
   Patentverletzung darstellen kann.
3. Die Mitgliedsstaaten stellen sicher, dass in allen F�llen, in denen
   in einem Patentanspruch Merkmale genannt sind, die die Verwendung eines
   Computerprogramms erfordern, eine gut funktionierende und gut
   dokumentierte Referenzimplementation eines solchen Programms als Teil
   der Patentbeschreibung ohne einschr�nkende Lizenzbedingungen
   ver�ffentlicht wird.
4. Die Mitgliedsstaaten stellen sicher, dass alle urheberrechtsf�higen
   Werke, die vor der Ver�ffentlichung eines Patentes in Gebrauch waren,
   auch k�nftig von dem Urheberrechtsinhaber und von allen Personen, denen
   der Urheberrechtsinhaber dies gestattet, gem�� vom Urheberrechtsinhaber
   zu bestimmenden Bedingungen weiterentwickelt, weiterverbreitet und zum
   Betrieb von Datenverarbeitungsanlagen verwendet werden d�rfen.
5. Die Mitgliedsstaaten stellen sicher, dass digitale Information als
   �ffentliches Wissen gilt, sobald es der �ffentlichkeit faktisch m�glich
   ist, diese Information einem ver�ffentlichten Werk zu entnehmen, auch
   dann, wenn die Entnahme eine Dekodierung voraussetzt.


Artikel 6

Konkurrenz zur Richtlinie 91/250/EWG

Zul�ssige Handlungen im Sinne der Richtlinie 91/250/EWG �ber den Rechtsschutz
von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften
�ber die Dekompilierung und die Interoperabilit�t, oder im Sinne der
Vorschriften �ber Marken oder Halbleitertopografien bleiben vom Patentschutz
f�r Erfindungen aufgrund dieser Richtlinie unber�hrt.


Artikel 6a (neu)

Vorrang der Interoperabilit�t

Die Mitgliedstaaten stellen sicher, dass in allen F�llen, in denen der
Einsatz einer patentierten Technik nur zum Zweck der Konvertierung der
in verschiedenen Computersystemen oder -netzen verwendeten Konventionen
ben�tigt wird, um die Kommunikation und den Austausch von Dateninhalten
zwischen ihnen zu erm�glichen, diese Verwendung nicht als
Patentverletzung gilt.


Artikel 7

Beobachtung

Die Kommission beobachtet, wie sich diese Richtlinie auf die
Innovationst�tigkeit und den Wettbewerb in Europa und weltweit sowie
auf die europ�ischen Unternehmen und den elektronischen Gesch�ftsverkehr
auswirkt.


Artikel 8

Bericht �ber die Auswirkungen der Richtlinie

Die Kommission legt dem Europ�ischen Parlament und dem Rat sp�testens am
[DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)]
einen Bericht vor �ber:
(a) die Auswirkungen von Patenten auf Erfindungen, die Computerprogramme
    enthalten, auf die in Artikel 7 genannten Faktoren,
(b) die Angemessenheit der Regeln f�r die Festlegung der
    Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die
    Neuheit, die erfinderische T�tigkeit und den eigentlichen
    Patentanspruch, und
(c) etwaige Schwierigkeiten, die in Mitgliedstaaten aufgetreten sind,
    in denen Erfindungen vor Patenterteilung nicht auf Neuheit und
    Erfindungsh�he gepr�ft werden, und etwaige Schritte, die unternommen
    werden sollten, um diese Schwierigkeiten zu beseitigen.


Artikel 8a (neu)

Untersuchungsausschuss

Das Europ�ische Parlament bildet einen Untersuchungsausschuss �ber die
Grenzen der Patentierbarkeit. Der Untersuchungsausschuss ist mit den
n�tigen Vollmachten und Geldmitteln ausgestattet, um vom Europ�ischen
Patentamt, der Europ�ischen Kommission und von wichtigen Akteuren aus
der Industrie ben�tigte Informationen �ber die Praxis der Verwertung
von Patenten, �ber die Auswirkungen von Patenten auf Geldfl�sse aller
Art, �ber die politischen Willensbildung im Patentwesen zu beschaffen.
Der Untersuchungsausschuss soll nachhaltig wirksame Regeln vorschlagen,
mit denen mehr Transparenz und Steuerbarkeit in das Patentwesen gebracht
wird und mit denen es in vern�nftigen Grenzen gehalten werden kann. Der
Untersuchungsausschuss soll in der �ffentlichsten m�glichen Art die
Forschung in diesem Bereich vorantreiben. Der Untersuchungsausschuss
darf zu nicht mehr als 20% aus Personen bestehen, die im Patentwesen
t�tig waren oder t�tig sind oder eine Ausbildung als Patentanwalt
oder Patentpr�fer haben.


Artikel 9

Umsetzung

1. Die Mitgliedstaaten erlassen die erforderlichen Rechts? und
   Verwaltungsvorschriften, um dieser Richtlinie sp�testens am
   [DATUM (letzter Tag des betreffenden Monats)] nachzukommen. Sie
   setzen die Kommission unverz�glich davon in Kenntnis.
   Bei Erlass dieser Vorschriften nehmen die Mitgliedstaaten in den
   Vorschriften selbst oder durch einen Hinweis bei der amtlichen
   Ver�ffentlichung auf diese Richtlinie Bezug. Die Mitgliedstaaten
   regeln die Einzelheiten der Bezugnahme.
2. Die Mitgliedstaaten �bermitteln der Kommission den Wortlaut der
   innerstaatlichen Rechtsvorschriften, die sie im Geltungsbereich dieser
   Richtlinie erlassen.


Artikel 10

Inkrafttreten

Diese Richtlinie tritt am zwanzigsten Tag nach ihrer Ver�ffentlichung
im Amtsblatt der Europ�ischen Gemeinschaften in Kraft.


Artikel 11

Adressaten

Diese Richtlinie ist an die Mitgliedstaaten gerichtet.

