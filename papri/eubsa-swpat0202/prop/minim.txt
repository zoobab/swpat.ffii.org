Software Patent Directive Core Amendments 
-----------------------------------------

Article 2

                             amendment                              
                                                                    
"computer-implemented                "computerised invention", also 
invention" means any invention       called "computer-implemented   
the performance of which             invention", means an innovation
involves the use of a computer,      the implementation of which    
computer network or other            involves the use of a data     
programmable apparatus and           processing system with         
having one or more prima facie       peripheral devices, and which, 
novel features which are             due to the way in which the    
realised wholly or partly by         peripheral devices are used, is
means of a computer program or       considered to be an invention  
computer programs;                   in the sense of patent law.    
                                                                    
                           justification                            
                                                                    
The term "computer-implemented invention" is not used by computer
professionals. In fact it is not in wide use at all. It was introduced
by the EPO in 2000 in an attempt to legitimate patents on
"computer-implemented business methods". It suggests that ideas which
can be put to work merely by executing a program on generic computing
equipment are patentable inventions. This amendment eliminates the
confusion. It makes it clear that an inventive washing machine does
not cease to be an invention just because it is connected to a
computer. We also introduce the term "computerised invention", which
has been used in an official EP Press Release about this directive as
a synonym of "computer-implemented invention". It depicts more
intuitively what is meant to be patentable: inventive use of devices
which have been placed under the control of a data processing system.

                             amendment                              
                                                                    
"technical contribution" means                                      
a contribution to the state of                                      
the art in a technical field         (deleted)                      
which is not obvious to a                                           
person skilled in the art.                                          
                                                                    
                           justification                            
                                                                    
The Commission text abolishes the invention (technical contribution)
test by mixing it into the %(q:inventive step) (non-obviousness) test.
The statement that the innovation should "not be obvious to a person
skilled in the art" may sound reassuring but in fact only reconfirms
existing EPO formalisms which, as the experience with software and
business method patents at the EPO shows, do not constitute an
obstacle to the granting of trivial patents.


                             amendment                              
                                                                    
                                     Technology in the sense of     
                                     patent law is applied natural  
                                     science. An invention in the   
                                     sense of patent law, also      
"technical contribution" means       called "technical invention",  
a contribution to the state of       "technical teaching",          
the art in a technical field         "technical solution" or        
which is not obvious to a            "technical contribution", is a 
person skilled in the art.           problem solution involving     
                                     controllable forces of nature, 
                                     or, in other words, a teaching 
                                     about cause-effect relations in
                                     the use of controllable forces 
                                     of nature.                     
                                                                    
                           justification                            
                                                                    
The Commission text shifts the question of what is a patentable     
invention to the question of what is a "technical contribution" and 
then leaves that question unanswered. 

There is a vague consensus even at the EPO that "technical" it means
something like "concrete and physical". More precise definitions have
been worked out by various patent jurisdictions, the best known being
those of German caselaw such as Disposition Program of 1976 and Flight
Cost Minimisation of 1986. These definitions are based on unambiguous
concepts which are understood by scientists and founded in
epistemology (philosophy of science) rather than in
caselaw. Experience shows that they are as clear and adequate today as
they were in the 1970s. They allow a previsible judgement of what is
patentable and what not, and lead to the deserved elimination of 5-10%
of all currently granted patents.  

Innovation in abstract-logical ideas requires no laboratory
experiments. The resulting patents are easy to obtain and stand in the
way of complex products, in which each patented idea forms only a
small part. By failing to restrict patentability to the realm of
applied natural science, the EU would stifle innovation and
productivity in Europe. By passing a directive which hinges on an
undefined central term, the EU would abdicate its legislative
competence and create systematic legal insecurity. The term
"technical" must either be defined as above, or it must not be used at
all.

--------------------------------------------------------------------

Article 3

                             amendment                              
                                                                    
                                     Member states shall ensure that
                                     data processing is not         
                                     considered to be a field of    
                                     technology in the sense of     
Member States shall ensure that      patent law, and that           
a computer-implemented               innovations in the field of    
invention is considered to           data processing are not        
belong to a field of                 considered to be inventions in 
technology.                          the sense of patent law,       
                                     regardless of whether they are 
                                     executed in the human mind or  
                                     on electronic data processing  
                                     equipment.                     
                                                                    
                           justification                            
                                                                    
One of the chief objectives of the directive project has been to    
clarify the interpretation of Art 52 EPC in the light of Art 27     
TRIPs, which says that "inventions all fields of technology" must be
patentable. We propose to translate of Art 52(2) EPC into the       
language of Art 27 TRIPs, along the lines of Amendment CULT-16.     
                                                                    
This amendment provides legal certainty, by defining clear limits   
between the domains of patentable inventions and immaterial         
innovations. Electronic data processing has been ubiquitous, ever   
since computers were introduced in the administration and business  
world in the 1950s, als Gert Kolle remarked in an article of 1977,  
which explains why the framers of the current law chose to exclude  
data processing from patentability. Data proceessing is a discipline
of abstraction, closely related to logics and mathematics.          
Innovations in data processing require no laboratory experiments and
yet are of an extremely general nature, i.e. they are usually both  
trivial and broad. This has been pointed out by Institute for       
Information Law of Amsterdam University, and JURI, while voting     
against CULT to allow data processing patents, has failed to come up
with any proposal that even tries to address the resulting problems.
Numerous studies conducted by EU instiutions as well as the opinions
of the European Economic and Social Council and the European        
Comittee of Regions explain in detail why Europe's economy will     
suffer damage, if data processing innovations are not clearly       
excluded from patentability.                                        

--------------------------------------------------------------------
Article 4

                             amendment                              
                                                                    
(2) Member States shall ensure                                      
that it is a condition of                                           
involving an inventive step          (deleted)                      
that a computer-implemented                                         
invention must make a technical                                     
contribution.                                                       
                                                                    
                           justification                            
                                                                    
The Commission abolishes the invention test of Art 52 EPC and states
that somehow this test has become part of the "inventive step"      
(non-obviousness) test. This "non-obviousness implies invention"    
dogma, apart from being counter-intuitive and in breach of the EPC, 
opens an infinite space of interpretation for those patent offices  
that conduct a search and a non-obviousness test, such as the EPO   
and the German Patent Office, while preventing other patent offices 
(such as French, Italian) from rejecting patents on non-inventions. 
Furthermore, treating "computer-implemented" inventions in a special
way, as suggested by the European Commission, runs counter to Art 27
TRIPs.                                                              

                             amendment                              
                                                                    
                                     Member States shall ensure that
(2) Member States shall ensure       computer-implemented solutions to          
that it is a condition of            technical problems are not considered 
involving an inventive step          to be patentable inventions    
that a computer-implemented          merely because they improve    
invention must make a technical      efficiency in the use of       
contribution.                        ressources within the data     
                                     processing system.             
                                                                    
                           justification                            
                                                                    
This amendment reflects current caselaw in Germany. In the words of
the justices of the German Federal Patent Court (BPatG, decision of
26. March 2002, 17 W (pat) 69/98, see
http://swpat.ffii.org/papers/bpatg17-suche02/):
                                                                    
  The applicant sees as a decisive indication of technicity of the
  method that it is based on a technical problem.  Because the
  proposed method does not need a dictionary, the memory space for
  this can be saved.  ... As far as the technical problem is
  concerned, this can only be considered as an indication but not as a
  proof of technicity of the process.  If computer implementation of
  non-technical processes were attributed a technical character merely
  because they display different specific characteristics, such as
  needing less computing time or storage space, the consequence of
  this would be that any computer implementation would have to be
  deemed to be of technical character.  This is because any distinct
  process will have distinct implementation characteristics, that
  allow it to either save computing time or save storage space.  These
  properties are, at least in the present case, not based on a
  technical achievement but result from chosen non-technical method.
  If the fact that such a problem is solved could be a sufficient
  reason for attributing a technical character to a computer
  implementation, then every implementation of a non-technical method
  would have to be patentable; this however would run against the
  conclusion of the Federal Court of Justice that the legal exclusion
  of computer programs from patentability does not allow us to adopt
  an approach which would make any teaching that is framed in
  computer-oriented instructions patentable.

                                                                    
                         amendment CULT-15                               
                                                                    
(3) The technical contribution                                      
shall be assessed by                 The technical contribution     
consideration of the difference      shall be assessed by           
between the scope of the patent      consideration of the difference
claim considered as a whole,         between the scope of the       
elements of which may comprise       technical features of the      
both technical and                   patent claim considered as a   
non-technical features, and the      whole and the state of the art.
state of the art.                                                   
                                                                    
                           justification                            
                                                                    
The Commission and JURI versions imply that a "technical            
contribution" can consist solely of non-technical features. This is 
self-contradictory and leads to unlimited patentablity. Amendment   
CULT-15 corrects the Commission's error, as far as that is possible 
by a simple modification.                                           

                             amendment                              
                                                                    
                                     In assessing whether a claim   
(3) The technical contribution       discloses an invention, one    
shall be assessed by                 must disregard the form or kind
consideration of the difference      of claim and concentrate on the
between the scope of the patent      content in order to identify   
claim considered as a whole,         the novel contribution which   
elements of which may comprise       the claimed subject matter     
both technical and                   makes to the known art. If this
non-technical features, and the      contribution does not          
state of the art.                    constitute an invention, there 
                                     is no patentable subject       
                                     matter.                        
                                                                    
                           justification                            
                                                                    
"Claim features" can not be a meaningful subject of legal
regulation. This amendment is an excerpt from the European Patent
Office Examination Guidelines of 1978
(http://swpat.ffii.org/papers/epo-gl78/). It exhorts examiners to
disregard the form of the claims and to distrust all attempts at
formalising the procedure of patent examination.  Formalisation puts
the application drafter in control and promotes an uncritical attitude
on the part of the examiner.

----------------------------------------------------------------------
Article 5

                             amendment                              
                                                                    
Member States shall ensure that                                     
a computer-implemented                                              
invention may be claimed as a        Member States shall ensure that
product, that is as a                a computerised invention may be
programmed computer, a               claimed as a product, that is a
programmed computer network or       set of devices controlled by a 
other programmed apparatus, or       data processing system, or as a
as a process carried out by          process carried out by such    
such a computer, computer            devices.                       
network or apparatus through                                        
the execution of software.                                          
                                                                    
                           justification                            
                                                                    
This article explains the meaning of the terms "product" and        
"process" in the context of computerised inventions. The original   
version interprets both terms correctly but has an undesirable      
side-effect: it suggests that algorithms framed in terms of generic 
computing equipment (programs for computers as such) are or can be  
"inventions". The amendment corrects the error. The inventive       
products and processes are characterised not by the data processing 
system but by the peripheral devices, which could e.g. be an        
automobile brake, a rubber-curing furnace or a washing machine.

-----------------------------------------------------------------
Article 5.bis: Publication Freedom

                             amendment                              
                                                                    
                                     Member states shall ensure that
                                     the publication or distribution
                                     of copyrightable objects,
                                     including computer programs, can
                                     never constitute a direct or
                                     indirect patent infringement.
                                                                    
                           justification                            
                                                                    
This is to explain how the patent rights are limited by other legal
values such as freedom of publication (Art 10 ECHR) and property in
individual creations (intellectual property, in the case of software:
copyright). This clarifies that both freedom of publication and the
right to control one's individual creations (copyright) are more
fundamental than patent rights and therefore constitute a limit on the
allowable scope of patent rights. Wherever copyright is applicable,
patents are not, and vice versa. The principle of "maximal separation
of spheres of intellectual property" is stated in the
Dispositionsprogramm (1976) and Betriebssystem (1990) decisions of the
German Federal Court of Justice, and it underlies Article 52 of the
European Patent Convention.
