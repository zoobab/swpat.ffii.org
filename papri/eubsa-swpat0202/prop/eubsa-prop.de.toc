\contentsline {chapter}{\numberline {1}Zugrundeliegende Erw\"{a}gungen}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Wozu eine Richtlinie?}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Ein einheitliches System harmonisieren?}{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Das Unaussprechliche Kl\"{a}ren?}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Der Kuhhandel zwischen EU und EPA}{5}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}G\"{u}ltige Gr\"{u}nde f\"{u}r eine Richtlinie}{6}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Kernschicht: die Technische Erfindung einz\"{a}unen, Art 52 EP\"{U} Bekr\"{a}ftigen}{6}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Negative Definition of ``Technical Invention'' in Art 52 EPC}{6}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}\"{U}bersetzung von der Sprache des EP\"{U} in die Sprache des TRIPs-Vertrages}{7}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}\"{U}bersetzung von EP\"{U}-Sprache zur Meta-Sprache der Anspruchsformulierung}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Eine gut umz\"{a}unte Grauzone}{7}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}``Computer-implementierten Erfindungen'' kann es nicht geben}{8}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Erweiterungsschicht: einige Kl\"{a}rungen \"{u}ber die Anwendbarkeit von Patenten im Hinblick auf Informationsg\"{u}ter}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Vollendungsschicht: die ``Technische Erfindung'' positiv definieren, den Bereich des Richterrechts eingrenzen}{9}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Abstrakte Definition der ``Technischen Erfindung''}{9}{subsection.1.4.1}
\contentsline {subsubsection}{the invention which a claim discloses vs the scope of implementations which it forbids}{9}{section*.3}
\contentsline {subsubsection}{Mind vs Matter}{9}{section*.4}
\contentsline {subsection}{\numberline {1.4.2}Kategorielle Ausschl\"{u}sse}{9}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Anwendungsbeispiele}{10}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Pr\"{a}ambel}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}[Erw\"{a}gung 1: Internal Market]}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}[Erw\"{a}gung 2: Unterschiede.]}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}[Erw\"{a}gung 3: Keine Konvergenz ohne Richtlinie]}{13}{section.2.3}
\contentsline {section}{\numberline {2.4}[Erw\"{a}gung 4: Heutige Bedeutung der Automatischen Datenverarbeitung]}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}[Erw\"{a}gung 5: Ergo Harmonisierung und Kl\"{a}rung]}{14}{section.2.5}
\contentsline {section}{\numberline {2.6}[Erw\"{a}gung 6: TRIPs]}{14}{section.2.6}
\contentsline {section}{\numberline {2.7}[Erw\"{a}gung 7: EP\"{U}]}{16}{section.2.7}
\contentsline {section}{\numberline {2.8}[Erw\"{a}gung 8: Patente und \"{O}ffentliches Interesse]}{17}{section.2.8}
\contentsline {section}{\numberline {2.9}[Erw\"{a}gung 9: Patente und Urheberrecht]}{17}{section.2.9}
\contentsline {section}{\numberline {2.10}[Erw\"{a}gung 10: Technischer Character (Technizit\"{a}t)]}{18}{section.2.10}
\contentsline {section}{\numberline {2.11}[Erw\"{a}gung 11: Gebiet der Technik]}{18}{section.2.11}
\contentsline {section}{\numberline {2.12}[Erw\"{a}gung 12: Technischer Beitrag]}{19}{section.2.12}
\contentsline {section}{\numberline {2.13}[Erw\"{a}gung 13: Algorithmen]}{19}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Erw\"{a}gung 13}{19}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Erw\"{a}gung 13(a)(bis) []}{20}{subsection.2.13.2}
\contentsline {section}{\numberline {2.14}[Erw\"{a}gung 14: Europ\"{a}isches und Nationales Recht]}{21}{section.2.14}
\contentsline {section}{\numberline {2.15}[Erw\"{a}gung 15: Umfang der Regelung]}{21}{section.2.15}
\contentsline {section}{\numberline {2.16}[Erw\"{a}gung 16: Wettbewerbsf\"{a}higkeit]}{22}{section.2.16}
\contentsline {subsection}{\numberline {2.16.1}Erw\"{a}gung 16}{22}{subsection.2.16.1}
\contentsline {subsection}{\numberline {2.16.2}Erw\"{a}gung 16a []}{22}{subsection.2.16.2}
\contentsline {section}{\numberline {2.17}[Erw\"{a}gung 17: Wettbewerbsregeln]}{23}{section.2.17}
\contentsline {section}{\numberline {2.18}[Erw\"{a}gung 18: Interoperabilit\"{a}t]}{23}{section.2.18}
\contentsline {section}{\numberline {2.19}[Erw\"{a}gung 19: Subsidiarit\"{a}t]}{24}{section.2.19}
\contentsline {chapter}{\numberline {3}Die Artikel}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Artikel 1: Anwendungsbereich}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Artikel 2: Begriffsbestimmungen}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Computer-Implementierte Erfindung}{25}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Technik, Erfindung, Nicht-Erfindungen}{26}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Artikel 3: Gebiete der Technik}{30}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Artikel 3}{30}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Artikel 3.bis []}{31}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Artikel 4: Voraussetzungen der Patentierbarkeit}{32}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Artikel 4(1)}{32}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Artikel 4(2)}{32}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Artikel 4(3)}{34}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Artikel 4(3)a}{34}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Artikel 4(a)bis []}{34}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Artikel 5: Anspr\"{u}che und Rechte}{35}{section.3.5}
\contentsline {section}{\numberline {3.6}Artikel 6: Interoperability}{39}{section.3.6}
\contentsline {section}{\numberline {3.7}Artikel 7: Beobachtung}{40}{section.3.7}
\contentsline {section}{\numberline {3.8}Artikel 8: Berichte \"{u}ber die Auswirkungen der Richtlinie}{41}{section.3.8}
\contentsline {section}{\numberline {3.9}Artikel 9: Umsetzung}{44}{section.3.9}
\contentsline {section}{\numberline {3.10}Artikel 10: Inkrafttreten}{44}{section.3.10}
\contentsline {section}{\numberline {3.11}Artikel 11: Adressaten}{44}{section.3.11}
\contentsline {chapter}{\numberline {4}Kommentierte Verweise}{45}{chapter.4}
