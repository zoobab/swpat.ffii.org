\contentsline {chapter}{\numberline {1}Titel}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Harmonisierung}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Beziehung zu Art 27 TRIPs: ``Technik'', ``Industrie'' und ``Erfindung''}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Positive Definition der ``Technischen Erfindung'' (Artikel 2)}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Negative Definition der Technischen Erfindung (Artikel 3)}{7}{section.3.2}
\contentsline {chapter}{\numberline {4}Patente, Urheberrecht und Ver\"{o}ffentlichungsfreiheit (Artikel 5.a)}{9}{chapter.4}
\contentsline {chapter}{\numberline {5}Sonstige wichtige \"{A}nderungen}{10}{chapter.5}
\contentsline {section}{\numberline {5.1}Artikel 1: Definition der ``computer-implementierten Erfindung''}{10}{section.5.1}
\contentsline {section}{\numberline {5.2}Artikel 4(a)bis [NEU]: Recheneffizienz nicht Technisch}{11}{section.5.2}
\contentsline {section}{\numberline {5.3}Art 4(3): Technischer Beitrag aus Nicht-Technischen Merkmalen?}{11}{section.5.3}
\contentsline {section}{\numberline {5.4}Art 4(2): Technizit\"{a}t und Nicht-Naheliegen sind Zwei Paar Stiefel!}{12}{section.5.4}
\contentsline {section}{\numberline {5.5}Article 5: Anspruchsform}{12}{section.5.5}
\contentsline {section}{\numberline {5.6}Art 6.a: Freiheit der Interoperation von Datenverarbeitungssystemen}{13}{section.5.6}
\contentsline {section}{\numberline {5.7}Erw\"{a}gung 11: Gebiete der Technik}{13}{section.5.7}
\contentsline {chapter}{\numberline {6}Kommentierte Verweise}{15}{chapter.6}
