\contentsline {chapter}{\numberline {1}Title}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Harmonisation}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Relation to Art 27 TRIPs: ``Technology'', ``Industry'' and ``Invention''}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Positive Definition of Technical Invention (Article 2)}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Negative Definition of Technical Invention (Article 3)}{7}{section.3.2}
\contentsline {chapter}{\numberline {4}Patents, Copyright and Freedom of Publication (Article 5.a)}{9}{chapter.4}
\contentsline {chapter}{\numberline {5}Other important Amendments}{10}{chapter.5}
\contentsline {section}{\numberline {5.1}Article 1: Definition of ``computer-implemented invention''}{10}{section.5.1}
\contentsline {section}{\numberline {5.2}Article 4(a)bis [NEW]: Computing Efficiency not Technical}{10}{section.5.2}
\contentsline {section}{\numberline {5.3}Art 4(3): Technical Contribution consisting of Non-Technical Features?}{11}{section.5.3}
\contentsline {section}{\numberline {5.4}Art 4(2): Technicity and Non-Obviousness are Separate Requirements!}{12}{section.5.4}
\contentsline {section}{\numberline {5.5}Article 5: Forms of Claims}{12}{section.5.5}
\contentsline {section}{\numberline {5.6}Art 6.a: Freedom of Interoperation for Data Processing Systems}{13}{section.5.6}
\contentsline {section}{\numberline {5.7}Recital 11: Fields of Technology}{13}{section.5.7}
\contentsline {chapter}{\numberline {6}Annotated Links}{15}{chapter.6}
