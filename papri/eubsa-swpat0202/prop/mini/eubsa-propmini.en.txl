<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Software Patent Directive Core Amendments

#descr: A few amendment proposals which are absolutely necessary if algorithms and business methods such as Amazon One Click Shopping are not to be considered patentable inventions.

#Tit: Title

#oet: Harmonisation

#aWT: Relation to Art 27 TRIPs

#cln: %(q:Technology), %(q:Industry) and %(q:Invention)

#sdb: Patents, Copyright and Freedom of Publication

#etn: Other important Amendments

#arW: Recitals 1-19 and Articles 1-8 deleted

#ten: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Art 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#euu: This amendment achieves full clarification and ensures that the European Court of Justice can harmonise caselaw if needed.  The directive proposal is full of redundancy and in order to shut the doors to unlimited patentability which it opens, it would be necessary to amend each single amendment and article.  Yet such amendments, as we propose below in case this amendment is not adopted, only fix bugs in a bloated directive draft without providing much clarity beyond what is already found in the European Patent Convention.

#fen: Art 27 of the TRIPs of 1994 has often been cited as a reason to reinterpret Art 52 EPC in a way that widens the scope of patentability.  At a hearing in 1997 in London, Paul Hartnack, comptroller of the UK Patent Office, formulated a question which legislators still need to anwer today:

#Sfl: Some have argued that the TRIPS agreement requires us to grant patents for software because it says %(q:patents shall be available for any inventions in all field of technology, provided they are capable ... of industrial application).

#Hde: However, it depends on how you interpret these words.

#IWe: Is a piece of pure software an invention?

#Eli: European law says it isn't.

#Ioe: Is pure software technology?

#Moa: Many would say no.

#Ifl: Is it capable of %(q:industrial) application?

#Ahy: Again, for much software many would say no.

#Tnc: TRIPS is an argument for wider protection for software.

#BWW: But the decision to do so should be based on sound economic reasons.

#WWn: Would it be in the interests of European industry, and European consumers, to take this step?

#ace: The answer, as shown by various studies conducted by the European Union and others, can only be %(q:No).  And this answer apparently needs to be laid down in a clarificatory law.

#von: Positive Definition of Technical Invention

#von2: Negative Definition of Technical Invention

#ofe: %(q:Technology) in the sense of patent law means %(q:applied natural science).  %(q:Technical) in the sense of patent law means %(q:concrete and physical).

#dic: We do not want %(q:financial technology) or %(q:social engineering) to be patentable.  TRIPs obliges us to define %(q:technology) and related terms and to rely on them for delimiting what is patentable.  The above definition is explicitely or implicitely used by all patent jurisdictions, including the EPO.

#Waf: %(q:Industry) in the sense of patent law means %(q:automated production of material goods).

#eho: We do not want innovations in the %(q:music industry) or %(q:legal services industry) to meet the TRIPs requirement of %(q:industrial applicability).  The word %(q:industry) is nowadays often used in extended meanings which are not appropriate in the context of patent law.

#datatechjust: This is a restatement of Art 52 EPC in the terminology of Art 27 TRIPs.  It corresponds to CULT Amendment 9 and JURI Amendment 46, of which the former received majority support.  Data processing is functional abstraction from applications in technical as well as non-technical fields.  It is as ubiquitous as reading and writing: a universal cultural technique.  Data processing is nowadays automated, but the implementation details of the abstract machines (universal computers) are irrelevant for the art of data processing.  Computers may be built on silicon or neurons or wood, and all run the same programs.  As was pointed out already by the framers of the European Patent Convention in the 1970s: monopolisation of innovations in automated data processing means monpolisation of abstract thought in all its practically relevant applications.  Numerous studies moreover show that data processing patents stifle innovation.  Europe will suffer damage if does not once again explicitely exclude data processing from the realm of patentable fields.

#Wnt: If MEPs still want to focus on a small set of amendments without deleting the body of the directive, we recommend the following in addition to the minimal set outlined above.  But we must warn: as long as not every recital and every article of the CEC/JURI text is deleted or amended, some doors to unlimited patentability will still be open and the unwanted logic patents can be expected to creep in.

#rcW: Article 1: Definition of %(q:computer-implemented invention)

#NEW: NEW

#uie: Computing Efficiency not Technical

#4et: Art 4(3): Technical Contribution consisting of Non-Technical Features?

#WvW: Art 4(2): Technicity and Non-Obviousness are Separate Requirements!

#iFW: Article 5: Forms of Claims

#eWp: Art 6.a: Freedom of Interoperation for Data Processing Systems

#rec11tit: Recital 11: Fields of Technology

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-propmini ;
# txtlang: en ;
# multlin: t ;
# End: ;

