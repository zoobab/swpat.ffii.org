# -*- mode: makefile -*-

eubsa-propmini.en.lstex:
	lstex eubsa-propmini.en | sort -u > eubsa-propmini.en.lstex
eubsa-propmini.en.mk:	eubsa-propmini.en.lstex
	vcat /ul/prg/RC/texmake > eubsa-propmini.en.mk


eubsa-propmini.en.dvi:	eubsa-propmini.en.mk
	rm -f eubsa-propmini.en.lta
	if latex eubsa-propmini.en;then test -f eubsa-propmini.en.lta && latex eubsa-propmini.en;while tail -n 20 eubsa-propmini.en.log | grep -w references && latex eubsa-propmini.en;do eval;done;fi
	if test -r eubsa-propmini.en.idx;then makeindex eubsa-propmini.en && latex eubsa-propmini.en;fi

eubsa-propmini.en.pdf:	eubsa-propmini.en.ps
	if grep -w '\(CJK\|epsfig\)' eubsa-propmini.en.tex;then ps2pdf eubsa-propmini.en.ps;else rm -f eubsa-propmini.en.lta;if pdflatex eubsa-propmini.en;then test -f eubsa-propmini.en.lta && pdflatex eubsa-propmini.en;while tail -n 20 eubsa-propmini.en.log | grep -w references && pdflatex eubsa-propmini.en;do eval;done;fi;fi
	if test -r eubsa-propmini.en.idx;then makeindex eubsa-propmini.en && pdflatex eubsa-propmini.en;fi
eubsa-propmini.en.tty:	eubsa-propmini.en.dvi
	dvi2tty -q eubsa-propmini.en > eubsa-propmini.en.tty
eubsa-propmini.en.ps:	eubsa-propmini.en.dvi	
	dvips  eubsa-propmini.en
eubsa-propmini.en.001:	eubsa-propmini.en.dvi
	rm -f eubsa-propmini.en.[0-9][0-9][0-9]
	dvips -i -S 1  eubsa-propmini.en
eubsa-propmini.en.pbm:	eubsa-propmini.en.ps
	pstopbm eubsa-propmini.en.ps
eubsa-propmini.en.gif:	eubsa-propmini.en.ps
	pstogif eubsa-propmini.en.ps
eubsa-propmini.en.eps:	eubsa-propmini.en.dvi
	dvips -E -f eubsa-propmini.en > eubsa-propmini.en.eps
eubsa-propmini.en-01.g3n:	eubsa-propmini.en.ps
	ps2fax n eubsa-propmini.en.ps
eubsa-propmini.en-01.g3f:	eubsa-propmini.en.ps
	ps2fax f eubsa-propmini.en.ps
eubsa-propmini.en.ps.gz:	eubsa-propmini.en.ps
	gzip < eubsa-propmini.en.ps > eubsa-propmini.en.ps.gz
eubsa-propmini.en.ps.gz.uue:	eubsa-propmini.en.ps.gz
	uuencode eubsa-propmini.en.ps.gz eubsa-propmini.en.ps.gz > eubsa-propmini.en.ps.gz.uue
eubsa-propmini.en.faxsnd:	eubsa-propmini.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eubsa-propmini.en-??.g3n`;source faxsnd main
eubsa-propmini.en_tex.ps:	
	cat eubsa-propmini.en.tex | splitlong | coco | m2ps > eubsa-propmini.en_tex.ps
eubsa-propmini.en_tex.ps.gz:	eubsa-propmini.en_tex.ps
	gzip < eubsa-propmini.en_tex.ps > eubsa-propmini.en_tex.ps.gz
eubsa-propmini.en-01.pgm:
	cat eubsa-propmini.en.ps | gs -q -sDEVICE=pgm -sOutputFile=eubsa-propmini.en-%02d.pgm -
eubsa-propmini.en/eubsa-propmini.en.html:	eubsa-propmini.en.dvi
	rm -fR eubsa-propmini.en;latex2html eubsa-propmini.en.tex
xview:	eubsa-propmini.en.dvi
	xdvi -s 8 eubsa-propmini.en &
tview:	eubsa-propmini.en.tty
	browse eubsa-propmini.en.tty 
gview:	eubsa-propmini.en.ps
	ghostview  eubsa-propmini.en.ps &
print:	eubsa-propmini.en.ps
	lpr -s -h eubsa-propmini.en.ps 
sprint:	eubsa-propmini.en.001
	for F in eubsa-propmini.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eubsa-propmini.en.faxsnd
	faxsndjob view eubsa-propmini.en &
fsend:	eubsa-propmini.en.faxsnd
	faxsndjob jobs eubsa-propmini.en
viewgif:	eubsa-propmini.en.gif
	xv eubsa-propmini.en.gif &
viewpbm:	eubsa-propmini.en.pbm
	xv eubsa-propmini.en-??.pbm &
vieweps:	eubsa-propmini.en.eps
	ghostview eubsa-propmini.en.eps &	
clean:	eubsa-propmini.en.ps
	rm -f  eubsa-propmini.en-*.tex eubsa-propmini.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eubsa-propmini.en-??.* eubsa-propmini.en_tex.* eubsa-propmini.en*~
eubsa-propmini.en.tgz:	clean
	set +f;LSFILES=`cat eubsa-propmini.en.ls???`;FILES=`ls eubsa-propmini.en.* $$LSFILES | sort -u`;tar czvf eubsa-propmini.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
