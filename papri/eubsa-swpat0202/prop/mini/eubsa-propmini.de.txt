<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

Waf: %(q:Industrie) im Sinne des Patentrechts bedeutet %(q:automatisierte Herstellung materieller Güter).
oet: Harmonisierung
arW: Erwägungen 1-19 und Artikel 1-8 gelöscht
ten: Die Mitgliedsstaaten sorgen dafür, dass Patente auf computerisierte Innovationen insoweit aufrecht erhalten und durchgesetzt werden, wie sie gemäß den Regeln von Artikel 52 des Europäischen Patentübereinkommens erteilt wurden, wie sie in den Prüfungsrichtlinien des Europäischen Patentamtes von 1978 erklärt werden.
euu: Diese Änderung erreicht eine vollkommene Klarstellung und stellt sicher, dass der Europäische Gerichtshof divergierende Entwicklungen im Fallrecht harmonisieren kann, sofern dies nötig ist.  Der Richtlinienvorschlag der Kommission ist hingegen unklar und voller Redundanz.  Um die zahlreichen Türen zur unbegrenzten Patentierbarkeit zu schließen, welche er öffnet, müsste man fast jeden Artikel und Erwägungsgrund ändern.  Aber solche Änderungen, von denen wir einige der wichtigsten unten anführen, dienen nur dazu, die Fehler eines aufgeblähten Richtlinienvorschlages zu korrigieren und fügen kaum etwas an Klarheit hinzu, was nicht bereits im Europäischen Patentübereinkommen enthalten wäre.
aWT: Beziehung zu Art 27 TRIPs
cln: %(q:Technik), %(q:Industrie) und %(q:Erfindung)
fen: Art 27 des Vertrages über Handelsbezogene Aspekte des Geistigen Eigentums (TRIPs) von 1994 ist oft als ein Grund für eine Neuauslegung von Art 52 EPÜ zitiert worden, bei der alle Grenzen der Patentierbarkeit verschwinden.  Bei einer Anhörung in London formulierte Paul Hartnack, Comptroller des Britischen Patentamtes, eine Frage an den Gesetzgeber, welche zu beantworten sich auch heute noch lohnen könnte:
Sfl: Manche Leute haben behauptet, der TRIPS-Vertrag verpflichte uns, Patente auf Software zu erteilen, da darin steht %(q:Patente sollen für alle Erfindungen auf allen Gebieten der Technik erhältlich sein, sofern sie ... einer industriellen Anwendung zugänglich sind).
Hde: Es kommt aber darauf an, wie man diese Formulierung auslegt.
IWe: Ist ein bloßes Stück Software eine Erfindung?
Eli: Nach europäischem Recht ist es das nicht.
Ioe: Ist bloße Software Technik?
Moa: Viele würden das verneinen.
Ifl: Ist sie der %(q:industriellen) Anwendung zugänglich?
Ahy: Wiederum würden, was einen Großteil der Software betrifft, viele das verneinen.
Tnc: Der TRIPs-Vertrag ist ein Argument dafür, den Patentschutz im Hinblick auf Software zu erweitern.
BWW: Aber die Entscheidung, dies zu tun, sollte auf soliden wirtschaftspolitischen Überlegungen beruhen.
WWn: Liegt es im Interesse der Europäischen Wirtschaft und der Europäischen Verbraucher, diesen Schritt zu unternehmen?
ace: Die Antwort, die sich aus diversen von der Europäischen Union und anderen durchgeführten Studien ergibt, lautet zweifellos %(q:Nein).  Und diese Antwort ist es wert, in einer klärenden Richtlinie formuliert zu werden.
von: Positive Definition der %(q:Technischen Erfindung)
von2: Negative Definition der Technischen Erfindung
ofe: %(q:Technik) im Sinne des Patentrechtes bedeutet %(q:Angewandte Naturwissenschaft).  %(q:Technisch) im Sinne des Patentrechts bedeutet %(q:konkret and physisch).
dic: %(q:Klaviertechnik), %(q:Finanztechnik), %(q:Sozialtechnik) u.v.m. sollen nicht patentierbar sein.  TRIPs verpflichtet uns, Begriffe wie %(q:Technik) zu definieren und auf ihrer Basis Rechtsklarheit für den Außenhandel zu schaffen.  Die genannte Definition wird von allen Patentsystemen explizit oder implizit verwendet.
eho: Die Innovationen der %(q:Musikindustrie),
datatechjust: Dies ist eine Neuformulierung von Art 52 EPÜ in den Begriffen von Art 27 TRIPs.  Sie entspricht CULT Änderung 9 und JURI Änderung 46, wobei erstere von der Mehrheit gebilligt wurde.  Datenverarbeitung ist funktionelle Abstraktion von Anwendungen auf technischen ebenso wie nicht-technischen Gebieten.  Datenverarbeitung ist so allgegenwärtig (ubiquitär) wie Lesen und Schreiben: es handelt sich um eine grundlegende Kulturtechnik.  Der Universalrechner kann aus Silikon, Lochkarten oder Neuronen gebaut werden und darauf laufen noch immer die selben Programme.  Wie bereits in den 70er Jahren wurde von den Vätern des Europäischen Patentübereinkommens aufgezeigt wurde, bedeutet die Monopolisierung von Innovationen im Bereich der Automatischen Datenverarbeitung, dass abstraktes Denken in all seinen praktisch bedeutsamen Anwendungen monopolisiert wird.  Zahlreiche Studien zeigen ferner, dass Datenverarbeitungspatente die Innovation nicht fördern sondern hemmen.  Europa wird Schaden leiden, wenn das Gebiet der Datenverarbeitung nicht erneut explizit von der Patentierbarkeit ausgeschlossen wird.
sdb: Patente, Urheberrecht und Veröffentlichungsfreiheit
etn: Sonstige wichtige Änderungen
Wnt: Wenn das Parlament sich weiterhin auf einen kleinen Satz von Änderungsanträgen konzentrieren will, ohne die gesamte Richtlinie zusammenzustreichen, empfehlen wir über den oben genannten minimalen Satz hinaus die folgenden Änderungen.  Wir müssen aber warnen: so lange nicht jeder Erwägungsgrund und jeder Artikel des Kommissions- oder JURI-Textes gestrichen oder geändert wird, werden einige Türen zur grenzenlosen Patentierbarkeit offen bleiben, und das Gebäude wird dem Andrang der Logikpatente nicht standhalten.
rcW: Artikel 1: Definition der %(q:computer-implementierten Erfindung)
NEW: NEU
uie: Recheneffizienz nicht Technisch
4et: Art 4(3): Technischer Beitrag aus Nicht-Technischen Merkmalen?
WvW: Art 4(2): Technizität und Nicht-Naheliegen sind Zwei Paar Stiefel!
iFW: Article 5: Anspruchsform
eWp: Art 6.a: Freiheit der Interoperation von Datenverarbeitungssystemen
rec11tit: Erwägung 11: Gebiete der Technik

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: eubsa-propmini ;
# txtlang: de ;
# End: ;

