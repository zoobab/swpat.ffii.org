\contentsline {chapter}{\numberline {1}Rationale}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Why a directive?}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Harmonising a unified system?}{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Clarifying the Unspeakable?}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}The political deal between EU and EPO}{5}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Valid Reasons for a Directive}{6}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Core Layer: Fence in the Technical Invention, Reaffirm Art 52 EPC}{6}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Negative Definition of ``Technical Invention'' in Art 52 EPC}{6}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Translating from EPC language to TRIPs language}{7}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Translating from EPC language to the Meta-Language of Claim-Writing}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}A well-fenced gray zone}{7}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}No such thing as a ``Computer-Implemented Invention''}{8}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Extension Layer: Some Clarifications on enforcability of patents in the context of information goods}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Completion Layer: Define the ``Technical Invention'', limit the scope of judiciary rule-setting}{9}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Abstract Definition of Technical Invention}{9}{subsection.1.4.1}
\contentsline {subsubsection}{the invention which a claim discloses vs the scope of implementations which it forbids}{9}{section*.3}
\contentsline {subsubsection}{Mind vs Matter}{9}{section*.4}
\contentsline {subsection}{\numberline {1.4.2}Categorial exclusions}{9}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Application Examples}{10}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Preamble}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}[Toelichting 1: Interne markt]}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}[Toelichting 2: Verschillen]}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}[Toelichting 3: De verschillen zouden groter kunnen worden]}{13}{section.2.3}
\contentsline {section}{\numberline {2.4}[Toelichting 4: Informatisering]}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}[Toelichting 5: Dus harmonisatie en verheldering]}{14}{section.2.5}
\contentsline {section}{\numberline {2.6}[Toelichting 6: TRIPs]}{15}{section.2.6}
\contentsline {section}{\numberline {2.7}[Toelichting 7: EPC]}{16}{section.2.7}
\contentsline {section}{\numberline {2.8}[Toelichting 8: Octrooien en publiek belang]}{17}{section.2.8}
\contentsline {section}{\numberline {2.9}[Toelichting 9: Octrooien en auteursrecht]}{18}{section.2.9}
\contentsline {section}{\numberline {2.10}[Toelichting 10: Technisch karakter]}{18}{section.2.10}
\contentsline {section}{\numberline {2.11}[Toelichting 11: Gebied van de technologie]}{19}{section.2.11}
\contentsline {section}{\numberline {2.12}[Toelichting 12: Technische bijdrage]}{20}{section.2.12}
\contentsline {section}{\numberline {2.13}[Toelichting 13: Algoritmes]}{20}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Toelichting 13}{20}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Toelichting 13(a)(bis) []}{21}{subsection.2.13.2}
\contentsline {section}{\numberline {2.14}[Toelichting 14: Europese wetgeving versus nationale wetgeving]}{22}{section.2.14}
\contentsline {section}{\numberline {2.15}[Toelichting 15: Reikwijdte van de bepalingen]}{22}{section.2.15}
\contentsline {section}{\numberline {2.16}[Toelichting 16: Concurrentiepositie verbeteren]}{23}{section.2.16}
\contentsline {subsection}{\numberline {2.16.1}Toelichting 16}{23}{subsection.2.16.1}
\contentsline {subsection}{\numberline {2.16.2}Toelichting 16a []}{23}{subsection.2.16.2}
\contentsline {section}{\numberline {2.17}[Toelichting 17: Mededingingsregels]}{24}{section.2.17}
\contentsline {section}{\numberline {2.18}[Toelichting 18: Decompilatie en compatibiliteit]}{24}{section.2.18}
\contentsline {section}{\numberline {2.19}[Toelichting 19: Subsidiariteit]}{25}{section.2.19}
\contentsline {chapter}{\numberline {3}De Artikels}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Artikel 1: Werkingssfeer}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}Artikel 2: Definities}{27}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Computer-Implemented Invention}{27}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Technology, Invention, Non-Inventions}{28}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Artikel 3: Gebieden van technologie}{31}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Artikel 3}{31}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Artikel 3.bis []}{32}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Artikel 4: Voorwaarden voor octrooieerbaarheid}{33}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Artikel 4(1)}{33}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Artikel 4(2)}{34}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Artikel 4(3)}{36}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Artikel 4(3)a}{36}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Artikel 4(a)bis []}{36}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Artikel 5: Draagwijdte van afdwingbaarheid}{37}{section.3.5}
\contentsline {section}{\numberline {3.6}Artikel 6: Compatibiliteit}{40}{section.3.6}
\contentsline {section}{\numberline {3.7}Artikel 7: Monitoring}{42}{section.3.7}
\contentsline {section}{\numberline {3.8}Artikel 8: Verslaggeving over de gevolgen van de richtlijn}{43}{section.3.8}
\contentsline {section}{\numberline {3.9}Artikel 9: Omzetting}{45}{section.3.9}
\contentsline {section}{\numberline {3.10}Artikel 10: Inwerkingtreding}{45}{section.3.10}
\contentsline {section}{\numberline {3.11}Artikel 11: Adressaten}{45}{section.3.11}
\contentsline {chapter}{\numberline {4}koppelingen met voetnoten}{46}{chapter.4}
