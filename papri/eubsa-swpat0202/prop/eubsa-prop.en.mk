# -*- mode: makefile -*-

eubsa-prop.en.lstex:
	lstex eubsa-prop.en | sort -u > eubsa-prop.en.lstex
eubsa-prop.en.mk:	eubsa-prop.en.lstex
	vcat /ul/prg/RC/texmake > eubsa-prop.en.mk


eubsa-prop.en.dvi:	eubsa-prop.en.mk
	rm -f eubsa-prop.en.lta
	if latex eubsa-prop.en;then test -f eubsa-prop.en.lta && latex eubsa-prop.en;while tail -n 20 eubsa-prop.en.log | grep -w references && latex eubsa-prop.en;do eval;done;fi
	if test -r eubsa-prop.en.idx;then makeindex eubsa-prop.en && latex eubsa-prop.en;fi

eubsa-prop.en.pdf:	eubsa-prop.en.ps
	if grep -w '\(CJK\|epsfig\)' eubsa-prop.en.tex;then ps2pdf eubsa-prop.en.ps;else rm -f eubsa-prop.en.lta;if pdflatex eubsa-prop.en;then test -f eubsa-prop.en.lta && pdflatex eubsa-prop.en;while tail -n 20 eubsa-prop.en.log | grep -w references && pdflatex eubsa-prop.en;do eval;done;fi;fi
	if test -r eubsa-prop.en.idx;then makeindex eubsa-prop.en && pdflatex eubsa-prop.en;fi
eubsa-prop.en.tty:	eubsa-prop.en.dvi
	dvi2tty -q eubsa-prop.en > eubsa-prop.en.tty
eubsa-prop.en.ps:	eubsa-prop.en.dvi	
	dvips  eubsa-prop.en
eubsa-prop.en.001:	eubsa-prop.en.dvi
	rm -f eubsa-prop.en.[0-9][0-9][0-9]
	dvips -i -S 1  eubsa-prop.en
eubsa-prop.en.pbm:	eubsa-prop.en.ps
	pstopbm eubsa-prop.en.ps
eubsa-prop.en.gif:	eubsa-prop.en.ps
	pstogif eubsa-prop.en.ps
eubsa-prop.en.eps:	eubsa-prop.en.dvi
	dvips -E -f eubsa-prop.en > eubsa-prop.en.eps
eubsa-prop.en-01.g3n:	eubsa-prop.en.ps
	ps2fax n eubsa-prop.en.ps
eubsa-prop.en-01.g3f:	eubsa-prop.en.ps
	ps2fax f eubsa-prop.en.ps
eubsa-prop.en.ps.gz:	eubsa-prop.en.ps
	gzip < eubsa-prop.en.ps > eubsa-prop.en.ps.gz
eubsa-prop.en.ps.gz.uue:	eubsa-prop.en.ps.gz
	uuencode eubsa-prop.en.ps.gz eubsa-prop.en.ps.gz > eubsa-prop.en.ps.gz.uue
eubsa-prop.en.faxsnd:	eubsa-prop.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eubsa-prop.en-??.g3n`;source faxsnd main
eubsa-prop.en_tex.ps:	
	cat eubsa-prop.en.tex | splitlong | coco | m2ps > eubsa-prop.en_tex.ps
eubsa-prop.en_tex.ps.gz:	eubsa-prop.en_tex.ps
	gzip < eubsa-prop.en_tex.ps > eubsa-prop.en_tex.ps.gz
eubsa-prop.en-01.pgm:
	cat eubsa-prop.en.ps | gs -q -sDEVICE=pgm -sOutputFile=eubsa-prop.en-%02d.pgm -
eubsa-prop.en/eubsa-prop.en.html:	eubsa-prop.en.dvi
	rm -fR eubsa-prop.en;latex2html eubsa-prop.en.tex
xview:	eubsa-prop.en.dvi
	xdvi -s 8 eubsa-prop.en &
tview:	eubsa-prop.en.tty
	browse eubsa-prop.en.tty 
gview:	eubsa-prop.en.ps
	ghostview  eubsa-prop.en.ps &
print:	eubsa-prop.en.ps
	lpr -s -h eubsa-prop.en.ps 
sprint:	eubsa-prop.en.001
	for F in eubsa-prop.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eubsa-prop.en.faxsnd
	faxsndjob view eubsa-prop.en &
fsend:	eubsa-prop.en.faxsnd
	faxsndjob jobs eubsa-prop.en
viewgif:	eubsa-prop.en.gif
	xv eubsa-prop.en.gif &
viewpbm:	eubsa-prop.en.pbm
	xv eubsa-prop.en-??.pbm &
vieweps:	eubsa-prop.en.eps
	ghostview eubsa-prop.en.eps &	
clean:	eubsa-prop.en.ps
	rm -f  eubsa-prop.en-*.tex eubsa-prop.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eubsa-prop.en-??.* eubsa-prop.en_tex.* eubsa-prop.en*~
eubsa-prop.en.tgz:	clean
	set +f;LSFILES=`cat eubsa-prop.en.ls???`;FILES=`ls eubsa-prop.en.* $$LSFILES | sort -u`;tar czvf eubsa-prop.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
