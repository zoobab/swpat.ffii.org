\select@language {french}
\select@language {french}
\contentsline {chapter}{\numberline {1}Consid\'{e}rations Soujacentes}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Pour quoi une Directive\@PI }{4}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Harmoniser un Syst\`{e}me Unifi\'{e}\@PI }{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Clarifier l'Inpronon\c {c}able\@PI }{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}L'\'{e}change politique entre UE et OEB}{4}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Raisons Valides pour une Directive}{5}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Couche nucl\'{e}aire\@DP Encadrer l'Invention Technique, R\'{e}affirmer l'Article 52 CBE}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Negative Definition of ``Technical Invention'' in Art 52 EPC}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Translating from EPC language to TRIPs language}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Translating from EPC language to the Meta-Language of Claim-Writing}{6}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}A well-fenced gray zone}{6}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Il ne peut pas y avoir des ``inventions mises en oevre par ordinateur''}{7}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Couche d'Extensions\@DP Quelques Clarifications sur l'Assurabilit\'{e} des Brevets dans le contexte des biens informationels}{7}{section.1.3}
\contentsline {section}{\numberline {1.4}Couche de Compl\'{e}tion\@DP Positivement D\'{e}finir l'``Invention Technique'', limiter la gamme du ``droit des juges''}{7}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Abstract Definition of Technical Invention}{8}{subsection.1.4.1}
\contentsline {subsubsection}{the invention which a claim discloses vs the scope of implementations which it forbids}{8}{section*.3}
\contentsline {subsubsection}{Mind vs Matter}{8}{section*.4}
\contentsline {subsection}{\numberline {1.4.2}Categorial exclusions}{8}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Application Examples}{8}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Pr\'{e}ambule}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}[Recital 1\@DP Internal Market]}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}[Recital 2\@DP Differences]}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}[Recital 3\@DP Pas de Convergence sans une Directive]}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}[Recital 4\@DP Significance of Computer Programs Today]}{12}{section.2.4}
\contentsline {section}{\numberline {2.5}[Recital 5\@DP Ergo Harmonisation \& Clarification]}{13}{section.2.5}
\contentsline {section}{\numberline {2.6}[Recital 6\@DP TRIPs]}{13}{section.2.6}
\contentsline {section}{\numberline {2.7}[Recital 7\@DP CBE]}{15}{section.2.7}
\contentsline {section}{\numberline {2.8}[Recital 8\@DP Brevets et Int\'{e}r\^{e}t Public]}{16}{section.2.8}
\contentsline {section}{\numberline {2.9}[Recital 9\@DP Patents and Copyright]}{16}{section.2.9}
\contentsline {section}{\numberline {2.10}[Recital 10\@DP Technical Character]}{17}{section.2.10}
\contentsline {section}{\numberline {2.11}[Recital 11\@DP Field of Technology]}{17}{section.2.11}
\contentsline {section}{\numberline {2.12}[Recital 12\@DP Technical Contribution]}{18}{section.2.12}
\contentsline {section}{\numberline {2.13}[Recital 13\@DP Algorithms]}{18}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Recital 13}{18}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Recital 13(a)(bis) []}{19}{subsection.2.13.2}
\contentsline {section}{\numberline {2.14}[Recital 14\@DP EU Law vs National Law]}{20}{section.2.14}
\contentsline {section}{\numberline {2.15}[Recital 15\@DP Gamme du R\`{e}glement]}{20}{section.2.15}
\contentsline {section}{\numberline {2.16}[Recital 16\@DP Comp\'{e}titivit\'{e}]}{21}{section.2.16}
\contentsline {subsection}{\numberline {2.16.1}Recital 16}{21}{subsection.2.16.1}
\contentsline {subsection}{\numberline {2.16.2}Recital 16a []}{21}{subsection.2.16.2}
\contentsline {section}{\numberline {2.17}[Recital 17\@DP Competition Rules]}{22}{section.2.17}
\contentsline {section}{\numberline {2.18}[Recital 18\@DP Interop\'{e}rabilit\'{e}]}{22}{section.2.18}
\contentsline {section}{\numberline {2.19}[Recital 19\@DP Subsidiarit\'{e}]}{23}{section.2.19}
\contentsline {chapter}{\numberline {3}Les Articles}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Article 1\@DP Champ d'application}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Article 2\@DP D\'{e}finitions}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Computer-Implemented Invention}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Technology, Invention, Non-Inventions}{25}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Article 3\@DP Champs de Technologie}{28}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Article 3}{28}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Article 3.bis []}{29}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Article 4\@DP Conditions de brevetabilit\'{e}}{30}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Article 4(1)}{30}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Article 4(2)}{30}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Article 4(3)}{31}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Article 4(3)a}{32}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Article 4(a)bis []}{32}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Article 5\@DP Revendications et Droits}{33}{section.3.5}
\contentsline {section}{\numberline {3.6}Article 6\@DP Interoperability}{36}{section.3.6}
\contentsline {section}{\numberline {3.7}Article 7\@DP Suivi}{38}{section.3.7}
\contentsline {section}{\numberline {3.8}Article 8\@DP Rapports sur les effets de la directive}{38}{section.3.8}
\contentsline {section}{\numberline {3.9}Article 9\@DP Mise en oeuvre}{41}{section.3.9}
\contentsline {section}{\numberline {3.10}Article 10\@DP Entr\'{e}e en vigueur}{41}{section.3.10}
\contentsline {section}{\numberline {3.11}Article 11\@DP Destinataires}{41}{section.3.11}
\contentsline {chapter}{\numberline {4}Liens Annot\'{e}s}{42}{chapter.4}
