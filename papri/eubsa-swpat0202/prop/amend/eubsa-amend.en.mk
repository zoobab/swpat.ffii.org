# -*- mode: makefile -*-

eubsa-amend.en.lstex:
	lstex eubsa-amend.en | sort -u > eubsa-amend.en.lstex
eubsa-amend.en.mk:	eubsa-amend.en.lstex
	vcat /ul/prg/RC/texmake > eubsa-amend.en.mk


eubsa-amend.en.dvi:	eubsa-amend.en.mk
	rm -f eubsa-amend.en.lta
	if latex eubsa-amend.en;then test -f eubsa-amend.en.lta && latex eubsa-amend.en;while tail -n 20 eubsa-amend.en.log | grep -w references && latex eubsa-amend.en;do eval;done;fi
	if test -r eubsa-amend.en.idx;then makeindex eubsa-amend.en && latex eubsa-amend.en;fi

eubsa-amend.en.pdf:	eubsa-amend.en.ps
	if grep -w '\(CJK\|epsfig\)' eubsa-amend.en.tex;then ps2pdf eubsa-amend.en.ps;else rm -f eubsa-amend.en.lta;if pdflatex eubsa-amend.en;then test -f eubsa-amend.en.lta && pdflatex eubsa-amend.en;while tail -n 20 eubsa-amend.en.log | grep -w references && pdflatex eubsa-amend.en;do eval;done;fi;fi
	if test -r eubsa-amend.en.idx;then makeindex eubsa-amend.en && pdflatex eubsa-amend.en;fi
eubsa-amend.en.tty:	eubsa-amend.en.dvi
	dvi2tty -q eubsa-amend.en > eubsa-amend.en.tty
eubsa-amend.en.ps:	eubsa-amend.en.dvi	
	dvips  eubsa-amend.en
eubsa-amend.en.001:	eubsa-amend.en.dvi
	rm -f eubsa-amend.en.[0-9][0-9][0-9]
	dvips -i -S 1  eubsa-amend.en
eubsa-amend.en.pbm:	eubsa-amend.en.ps
	pstopbm eubsa-amend.en.ps
eubsa-amend.en.gif:	eubsa-amend.en.ps
	pstogif eubsa-amend.en.ps
eubsa-amend.en.eps:	eubsa-amend.en.dvi
	dvips -E -f eubsa-amend.en > eubsa-amend.en.eps
eubsa-amend.en-01.g3n:	eubsa-amend.en.ps
	ps2fax n eubsa-amend.en.ps
eubsa-amend.en-01.g3f:	eubsa-amend.en.ps
	ps2fax f eubsa-amend.en.ps
eubsa-amend.en.ps.gz:	eubsa-amend.en.ps
	gzip < eubsa-amend.en.ps > eubsa-amend.en.ps.gz
eubsa-amend.en.ps.gz.uue:	eubsa-amend.en.ps.gz
	uuencode eubsa-amend.en.ps.gz eubsa-amend.en.ps.gz > eubsa-amend.en.ps.gz.uue
eubsa-amend.en.faxsnd:	eubsa-amend.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eubsa-amend.en-??.g3n`;source faxsnd main
eubsa-amend.en_tex.ps:	
	cat eubsa-amend.en.tex | splitlong | coco | m2ps > eubsa-amend.en_tex.ps
eubsa-amend.en_tex.ps.gz:	eubsa-amend.en_tex.ps
	gzip < eubsa-amend.en_tex.ps > eubsa-amend.en_tex.ps.gz
eubsa-amend.en-01.pgm:
	cat eubsa-amend.en.ps | gs -q -sDEVICE=pgm -sOutputFile=eubsa-amend.en-%02d.pgm -
eubsa-amend.en/eubsa-amend.en.html:	eubsa-amend.en.dvi
	rm -fR eubsa-amend.en;latex2html eubsa-amend.en.tex
xview:	eubsa-amend.en.dvi
	xdvi -s 8 eubsa-amend.en &
tview:	eubsa-amend.en.tty
	browse eubsa-amend.en.tty 
gview:	eubsa-amend.en.ps
	ghostview  eubsa-amend.en.ps &
print:	eubsa-amend.en.ps
	lpr -s -h eubsa-amend.en.ps 
sprint:	eubsa-amend.en.001
	for F in eubsa-amend.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eubsa-amend.en.faxsnd
	faxsndjob view eubsa-amend.en &
fsend:	eubsa-amend.en.faxsnd
	faxsndjob jobs eubsa-amend.en
viewgif:	eubsa-amend.en.gif
	xv eubsa-amend.en.gif &
viewpbm:	eubsa-amend.en.pbm
	xv eubsa-amend.en-??.pbm &
vieweps:	eubsa-amend.en.eps
	ghostview eubsa-amend.en.eps &	
clean:	eubsa-amend.en.ps
	rm -f  eubsa-amend.en-*.tex eubsa-amend.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eubsa-amend.en-??.* eubsa-amend.en_tex.* eubsa-amend.en*~
eubsa-amend.en.tgz:	clean
	set +f;LSFILES=`cat eubsa-amend.en.ls???`;FILES=`ls eubsa-amend.en.* $$LSFILES | sort -u`;tar czvf eubsa-amend.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
