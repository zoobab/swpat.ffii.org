\contentsline {chapter}{\numberline {1}Preamble}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}[Recital 1: Internal Market]}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}[Recital 2: Differences]}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}[Recital 3: No Convergence without Directive]}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}[Recital 4: Informatisation]}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}[Recital 5: Ergo Harmonisation \& Clarification]}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}[Recital 6: TRIPs]}{7}{section.1.6}
\contentsline {section}{\numberline {1.7}[Recital 8: Patents and Public Interest]}{7}{section.1.7}
\contentsline {section}{\numberline {1.8}[Recital 9: Patents and Copyright]}{8}{section.1.8}
\contentsline {section}{\numberline {1.9}[Recital 10: Technical Character]}{8}{section.1.9}
\contentsline {section}{\numberline {1.10}[Recital 11: Field of Technology]}{8}{section.1.10}
\contentsline {section}{\numberline {1.11}[Recital 12: Technical Contribution]}{8}{section.1.11}
\contentsline {section}{\numberline {1.12}[Recital 13: Algorithms]}{9}{section.1.12}
\contentsline {section}{\numberline {1.13}[Recital 14: EU Law vs National Law]}{9}{section.1.13}
\contentsline {section}{\numberline {1.14}[Recital 15: Scope of Regulation]}{9}{section.1.14}
\contentsline {section}{\numberline {1.15}[Recital 16: Competitivity]}{10}{section.1.15}
\contentsline {section}{\numberline {1.16}[Recital 17: Competition Rules]}{10}{section.1.16}
\contentsline {section}{\numberline {1.17}[Recital 18: Interoperability, Copyright]}{10}{section.1.17}
\contentsline {section}{\numberline {1.18}[Recital 19: Subsidiarity]}{11}{section.1.18}
\contentsline {chapter}{\numberline {2}The Articles}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Article 1: Scope}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Article 2: Definitions}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}Article 3: Fields of Technology}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}Article 4: Conditions for patentability}{16}{section.2.4}
\contentsline {section}{\numberline {2.5}Article 5: Claims and Rights}{17}{section.2.5}
\contentsline {section}{\numberline {2.6}Article 6: Interoperability}{19}{section.2.6}
\contentsline {section}{\numberline {2.7}Article 7: Monitoring}{19}{section.2.7}
\contentsline {section}{\numberline {2.8}Article 8: Reporting on the effects of the Directive}{20}{section.2.8}
\contentsline {section}{\numberline {2.9}Article 9: Implementation}{21}{section.2.9}
\contentsline {section}{\numberline {2.10}Article 10: Entry into force}{22}{section.2.10}
\contentsline {section}{\numberline {2.11}Article 11: Addressees}{22}{section.2.11}
