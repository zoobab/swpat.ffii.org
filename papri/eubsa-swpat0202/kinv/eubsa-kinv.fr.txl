<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Qu'est-ce qu'une %(q:Invention mise en oeuvre par ordinateur) ?

#descr: Une machine à laver avec ordinateur embarqué ?  Ou un programme  générique de traitement de données ?  Dans leurs discours publics, les  partisans de la proposition de directive de l'UE sur la brevetabilité  des logiciels disent qu'ils ne veulent pas de brevets sur de %(q:purs  logiciels) mais seulement sur des %(q:inventions mises en oeuvre par  ordinateur), entendant signifier par là sur %(q:des machines à laver et  des téléphones mobiles). L'article 2 de la proposition elle-même dit  tout autre chose, tout comme les annales et les pratiques de l'OEB  accordant des brevets sur les %(q:inventions mises en oeuvre par  ordinateur).

#sqe: Est-ce qu'une machine a laver peut être une %(q:invention mise en oevre par ordinateur)?

#PCe: Seulement les Programmes sont %(q:mis en oevre par ordinateur).

#Aen: Selon l'%(ep:Art 52 CBE) les programmes pour ordinateurs ne sont pas des inventions au sense de la loi des brevets.

#titjust0: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in %(a6:Appendix 6) of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a programmatic statement.  It implies that calculation rules framed in terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.

#eWM: Programmers speak about %(q:implementing a specification).  A patent claim usually specifies a sequence of events (i.e. a program) which could be %(e:implemented) by a programmer or by an engineer.  %(q:Implementation) in this context means %(q:working it out in detail so that it can run without errors).  The implementation requires human work.  It is never done by a computer, and in fact computers will usually not play any role in it at all, beyond that of an auxiliary tool for checking the validity of logical concepts, similar to pencil and paper.

#iWo: Moreover, the programmer would not say that she is implementing an %(q:invention), no matter how new and admirable the specification is.  Rather she might be implementing an algorithm, an idea or, more often, a complex combination of such abstract elements.  Real advances in the art of programming are generally of abstract nature and therefore would usually not be referred to as %(q:inventions) by the person skilled in the art.

#teW: A term like %(q:computer-executed innovations) or simply %(q:data processing innovations) would have been less conducive to misunderstanding.  However misunderstanding was exactly what the proponents wanted.

#wan: The proponents of patents on %(q:computer-implemented inventions) sometimes say that it refers only to %(q:washing machines, mobile phones, intelligent household appliances ...) and %(q:not computer programs as such).  However this is untrue.  The term as defined in the EPO's Trilateral Appendix 6 and in the Commission's Directive Proposal refers to nothing but data manipulation processes running on general-purpose computing equipment.  Even if the extremely rare cases where a conventional washing machine is involved in a patent claim which EPO/CEC would subsume under %(q:computer-implemented inventions), the gist of the %(q:invention) will lie in data processing, not in applying heat and reactants to clothes.  Otherwise this would, according to the EPO/CEC definitions, no longer be a %(q:computer-implemented invention) but rather an ordinary technical invention (which does not match the CII definition, because its %(q:prima facie novel) aspect is unrelated to computers).

#tnW: The European Parliament has redefined the term %(q:computer-implemented invention) in such a way that general-purpose data processing does not qualify while a washing machine, where data processing has only an auxiliary function and is not constitutive for the invention, would qualify.  This amendment is fiercely opposed by the European Commission and by all those who earlier claimed that they want only washing machines and the like but not programs as such to be patentable.

#cao: While the redefinition of the European Parliament constitutes a clever way of correcting a central flaw of the proposed directive, it does not make the text much clearer.  The misleading term %(q:computer-implemented invention) continues to be used, and it will continue to mislead all those people who have not carefully read and memorised the EP definition.

#csp: Document de l'OEB, datant de 2000 et introduisant le terme  d'%(q:invention mise en oeuvre par ordinateur) pour légitimer la  délivrance de brevets sur des programmes d'ordinateur ou sur des  méthodes dans le domaine des activités économiques, en complète  contradiction avec la lettre et l'esprit de la loi, alignant ainsi les  pratiques juridiques européennes sur celles des Offices de Brevets  américains et japonais.  L'OEB écrit :

#srs: L'expression %(q:invention mise en oeuvre par ordinateur) est  prévue pour couvrir les revendications désignant des ordinateurs, des  réseaux d'ordinateurs ou d'autres appareils numériques programmables  conventionnels avec lesquels, à première vue, l'apport en nouveauté de  l'invention revendiquée est réalisé par le biais d'un ou plusieurs  programmes nouveaux.

#rdc: La seule raison apparente de faire une distinction dans la  décision entre des %(q:effets techniques) et des %(q:effets encore plus  techniques) était la présence de %(q:programmes d'ordinateur) dans la  liste des exclusions de l'Article 52(2) de la CEB. Si, comme c'est à  prévoir, cet élément est supprimé de la liste par la Conférence  Diplomatique, une telle distinction n'aura plus aucun fondement. On  peut en conclure que la Grande Chambre des Recours aurait préféré  pouvoir dire qu'aucune invention mise en oeuvre par ordinateur n'était  exclue de la brevetabilité par les clauses des Articles 52(2) et (3) de  l'OEB.

#emh: L'Article 2 de la proposition de la Commission définit une  %(q:invention mise en oeuvre par ordinateur) conformément aux éléments  de l'Annexe 6 :

#kompinvcec: Une %(q:Invention mise en oeuvre par ordinateur) désigne  toute invention dont l'exécution implique l'utilisation d'un  ordinateur, d'un réseau d'ordinateurs ou d'un autre appareil  programmable et présentant une ou plusieurs caractéristiques à première  vue nouvelles qui sont réalisées totalement ou en partie au moyen d'un  ou de plusieurs programmes d'ordinateurs.

#tle2: Amendement présentant une contre-proposition à la proposition de  titre de la directive

#xwo: Explique également ce qui ne convient pas dans l'expression  %(q:invention mise en oeuvre par ordinateur).

#hWn: Cette décision de l'OEB datant de 1984 montre que  l'interprétation antérieure de l'expression %(q:programme d'ordinateur) est exactement celle appliquée en 2000 au nouveau terme %(q:invention  mise en oeuvre par ordinateur).

#srn: Ce qu'est une %(q:Invention mise en oeuvre par ordinateur)

#ebe: Articles pédagogiques écrits par Jonas Maebe en septembre 2003

#et2: Amendment 36=42=117

#eWs: The European Parliament voted for a redefinition of the %(q:computer-implemented invention).  Thereby it removed the damaging effects of this term within the directive.  However the term %(q:computer-implemented invention) is still in the title of the directive and will therefore continue to mislead all those people who have not taken the time to carefully read the redefinition.

#Am36: %(q:invention mise en oeuvre par ordinateur) désigne toute invention au sens de la Convention sur le brevet européen dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'un autre appareil programmable et présentant dans sa mise en oeuvre une ou plusieurs caractéristiques non techniques qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs, en plus des caractéristiques techniques que toute invention doit apporter;

#rhW: In his testimony at the USPTO 1994 hearings, Jim Warren, board member of the US software giant Autodesk Inc, objected against the patent lawyer newspeak term %(q:software related inventions), which is still not quite as biased as %(q:computer-implemented inventions):

#TWp: C'est pourquoi, je proteste respectueusement contre le titre donné  à ces auditions -- %(q:Inventions Relatives aux Logiciels) -- puisque  vous n'êtes pas essentiellement concernés par des gadgets contrôlés par  logiciel. Le titre illustre un parti pris inapproprié et prêtant  sérieusement à confusion. En fait, en plus d'un quart de siècle en tant  que professionnel des ordinateurs et observateur et écrivain dans cette  industrie, je ne me souviens pas avoir jamais entendu ou lu une telle  phrase -- excepté dans le contexte des revendications juridiques pour le  monopole, où les demandeurs essayaient de dénaturer la tradition de  brevetage des équipements afin de monopoliser l'exécution des processus  intellectuels.

#enh: Nominate the %(q:computer-impemented invention) for linguistic (un)beauty contests

#WFW: Various language associations regularly call for public bidding for %(q:UnWord of the Year) or similar.  Is there any other high-level political term which is so misleading and so clearly implies a break of law as the term %(q:computer-implemented invention)?  Is there any other political term which is closely connected to attempts at deceiving the legislature, negating fundamental rights and %(da:sidestepping democratic processes in Europe)?  If not, why isn't %(q:Computer-Implemented Invention) the top candidate of all those linguistic beauty contests?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kinv ;
# txtlang: fr ;
# multlin: t ;
# End: ;

