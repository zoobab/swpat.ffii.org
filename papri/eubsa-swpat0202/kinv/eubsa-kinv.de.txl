<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Was ist eine Computerimplementierte Erfindung?

#descr: Eine Waschmaschine mit eingebettetem Rechner?  Oder ein allgemein einsetzbares Datenverarbeitungsprogramm?  In der öffentlichen Debatte sagen die Befürworter des EU Softwarepatent-Richtlinienvorschlags, dass sie keine Patente auf %(q:reine Software) wollen, sondern nur auf %(q:Computerimplementierte Erfindungen), womit sie, wie sie sagen, %(q:Waschmaschinen und Mobiltelefone) meinen.  Art 2 des Vorschlags widerspricht dem jedoch, ebenso wie die Geschichte und die Praxis des EPA, Patente auf %(q:Computerimplementierte Erfindungen) zu erteilen.

#sqe: Kann eine Waschmaschine eine %(q:computer-implementierte Erfindung) sein?

#PCe: Nur Programme sind %(q:computer-implementiert).

#Aen: Gemäß %(ep:Art 52 EPÜ) sind Programme für Datenverarbeitungsanlagen keine Erfindungen im Sinne des Patentrechts.

#titjust0: Der Begriff %(q:computer-implementierte Erfindung) ist dem Computer-Fachmann nicht geläufig.  Er ist in der Tat überhaupt nicht im allgemeinen Gebrauch.  Er wurde vom Europäischen Patentamt (EPA) im Mai 2000 in %(a6:Anhang 6) der Akte zur Trilateralen Konferenz eingeführt, wo er dazu diente, die Patentierbarkeit %(q:computer-implementierter Geschäftsmethoden) zu legitimieren, um die Europäische Praxis in Einklang mit der der USA und Japans zu bringen.  Der Richtlinienvorschlag der Europäischen Kommission lehnt sich eng an diesen %(q:Anhang 6) an.  Der Begriff %(q:computer-implementierte Erfindung) impliziert, dass in Begriffen des Universalrechners formulierte Algorithmen und Geschäftsmethoden patentfähige Erfindungen sind.  Diese Implikation steht im Widerspruch zu Art 52 EPÜ, demzufolge Algorithmen, Geschäftsmethoden und Programme für Datenverarbeitungsanlagen keine Erfindungen im Sinne des Patentrechts sind.

#eWM: Programmers speak about %(q:implementing a specification).  A patent claim usually specifies a sequence of events (i.e. a program) which could be %(e:implemented) by a programmer or by an engineer.  %(q:Implementation) in this context means %(q:working it out in detail so that it can run without errors).  The implementation requires human work.  It is never done by a computer, and in fact computers will usually not play any role in it at all, beyond that of an auxiliary tool for checking the validity of logical concepts, similar to pencil and paper.

#iWo: Moreover, the programmer would not say that she is implementing an %(q:invention), no matter how new and admirable the specification is.  Rather she might be implementing an algorithm, an idea or, more often, a complex combination of such abstract elements.  Real advances in the art of programming are generally of abstract nature and therefore would usually not be referred to as %(q:inventions) by the person skilled in the art.

#teW: A term like %(q:computer-executed innovations) or simply %(q:data processing innovations) would have been less conducive to misunderstanding.  However misunderstanding was exactly what the proponents wanted.

#wan: The proponents of patents on %(q:computer-implemented inventions) sometimes say that it refers only to %(q:washing machines, mobile phones, intelligent household appliances ...) and %(q:not computer programs as such).  However this is untrue.  The term as defined in the EPO's Trilateral Appendix 6 and in the Commission's Directive Proposal refers to nothing but data manipulation processes running on general-purpose computing equipment.  Even if the extremely rare cases where a conventional washing machine is involved in a patent claim which EPO/CEC would subsume under %(q:computer-implemented inventions), the gist of the %(q:invention) will lie in data processing, not in applying heat and reactants to clothes.  Otherwise this would, according to the EPO/CEC definitions, no longer be a %(q:computer-implemented invention) but rather an ordinary technical invention (which does not match the CII definition, because its %(q:prima facie novel) aspect is unrelated to computers).

#tnW: The European Parliament has redefined the term %(q:computer-implemented invention) in such a way that general-purpose data processing does not qualify while a washing machine, where data processing has only an auxiliary function and is not constitutive for the invention, would qualify.  This amendment is fiercely opposed by the European Commission and by all those who earlier claimed that they want only washing machines and the like but not programs as such to be patentable.

#cao: While the redefinition of the European Parliament constitutes a clever way of correcting a central flaw of the proposed directive, it does not make the text much clearer.  The misleading term %(q:computer-implemented invention) continues to be used, and it will continue to mislead all those people who have not carefully read and memorised the EP definition.

#csp: Das EPA Dokument aus dem Jahr 2000, welches den Begriff %(q:Computerimplementierte Erfindung) einführt, um das Erteilen von Patenten auf Programme für Datenverarbeitungsanlagen und Geschäftsmethoden im Widerspruch zum Buchstaben und Geist des Gesetzes zu legitimieren, und so die Europäische Erteilungspraxis der Praxis der Patentämter in den USA und Japan anzugleichen.  Das EPA schreibt:

#srs: Der Ausdruck %(q:Computerimplementierte Erfindung) soll Ansprüche abdecken, welche Computer, Computernetze oder sonstige konventionelle programmierbare digitale Vorrichtungen nennen, bei denen auf den ersten Blick die beanspruchte Erfindung durch ein oder mehrere neue Programme realisiert ist.

#rdc: Der einzige erkennbare Grund, zwischen einem %(q:technischen Effekt) und einem %(q:weiteren technischen Effekt) zu unterscheiden, war die Anwesenheit von %(q:Programme für Datenverarbeitungsanlagen) in der Liste der Patentierbarkeitsausschlüsse in Artikel 52(2) EPÜ.  Wenn, wie es abzusehen ist, dieses Element durch die Diplomatische Konferenz aus der Liste gestrichen wird, wird keine Grundlage mehr für diese Unterscheidung bestehen.  Hieraus ist zu folgern, dass die Beschwerdekammer es vorgezogen hätte, sagen zu können, dass keine computer-implementierte Erfindung durch die Vorschriften der Artikel 52(2) und (3) EPÜ von der Patentfähigkeit ausgenommen ist.

#emh: Artikel 2 des Vorschlags der Kommission definiert %(q:computerimplementierte Erfindung) den Vorgaben des Appendix 6 folgend:

#kompinvcec: %(q:Computerimplementierte Erfindung) ist jede Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#tle2: Änderungsvorschläge zur Softwarepatentrichtlinie der EU

#xwo: Erklärt ebenfalls was an dem Begriff C.I.E. falsch ist.

#hWn: Diese EPA Entscheidung von 1984 zeigt, dass vormals %(q:Programm für Datenverarbeitungsanlagen) so verstanden wurde wie der Begriff %(q:Computerimplementierte Erfindung) im Jahr 2000 definiert wurde.

#srn: What is a %(q:Computer-Implemented Invention)?

#ebe: Lehrreiches Material, geschrieben von Jonas Maebe im September 2003

#et2: Änderungsantrag 36=42=117

#eWs: Das Europäische Parlament hat für eine Umdefinition des Begriffs %(q:computer-implementierte Erfindung) gestimmt.  Dadruch hat es die schädlichen Wirkungen dieses Begriffs innerhalb der Richtlinie beseitigt.  Aber der Begriff %(q:computer-implementierte Erfindung) findet sich nach wie vor im Titel der Richtlinie und wird somit weiterhin all diejenigen Leser irreführen, die die Neudefinition nicht unmittelbar vor Augen haben.

#Am36: %(q:Computerimplementierte Erfindung) ist jede Erfindung im Sinne des Europäischen Patentübereinkommens, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die in ihren Implementationen ­ außer den technischen Merkmalen, die jede Erfindung beisteuern muss  mindestens ein nichttechnisches Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#rhW: In his testimony at the USPTO 1994 hearings, Jim Warren, board member of the US software giant Autodesk Inc, objected against the patent lawyer newspeak term %(q:software related inventions), which is still not quite as biased as %(q:computer-implemented inventions):

#TWp: Thus, I respectfully object to the title for these hearings -- %(q:Software-Related Inventions) -- since you are not primarily concerned with gadgets that are controlled by software. The title illustrates an inappropriate and seriously-misleading bias. In fact, in more than a quarter-century as a computer professional and observer and writer in this industry, I don't recall ever hearing or reading such a phrase -- except in the context of legalistic claims for monopoly, where the claimants were trying to twist the tradition of patenting devices in order to monopolize the execution of intellectual processes.

#enh: %(q:Computer-Implementierte Erfindung) zum %(q:Unwort des Jahres) nominieren

#WFW: Verschiedene Sprachverbände rufen regelmäßig zur Wahl des %(q:Unworts des Jahres) auf.  Gibt es sonstwo auf höchster politischer Ebene etablierte Kampfbegriffe, die so irreführend sind und zugleich das Recht auf den Kopf stellen, wie dies bei der %(q:computer-implementierten Erfindung) der Fall ist?  Gibt es noch einen politischen Begriff, der eng mit intensiven Bemühungen verknüpft ist, den Gesetzgeber zu betrügen, Grundrechte zu negieren und %(da:demokratische Prozesse in der EU auszuschalten)?  Wenn nicht, warum nimmt die %(q:computer-implementierte Erfindung) keinen Spitzenplatz auf den Kandidatenlisten ein?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: rainbow ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kinv ;
# txtlang: de ;
# multlin: t ;
# End: ;

