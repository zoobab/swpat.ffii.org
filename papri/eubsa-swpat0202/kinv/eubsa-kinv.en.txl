<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: What is a Computer-Implemented Invention?

#descr: A washing machine with an embedded computer?  Or a general-purpose data processing program?  In their public discourse, the promoters of the EU software patent directive proposal say they do not want patents on %(q:pure software) but only on %(q:computer-implemented inventions), by which they say they mean %(q:washing machines and mobile phones).  Art 2 of the proposal itself however says otherwise, as does the history and practise of granting patents for %(q:computer-implemented inventions) at the EPO.

#sqe: Can a Washing Machine be a %(q:Computer-Implemented Invention)?

#PCe: Only Computer Programs are %(q:Computer-Implemented).

#Aen: According to %(ep:Art 52 EPC), programs for computers are not inventions in the sense of patent law.

#titjust0: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in %(a6:Appendix 6) of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission (CEC) directive proposal is based on wordings from this %(q:Appendix 6).  The term %(q:computer-implemented invention) is a programmatic statement.  It implies that calculation rules framed in terms of the general-purpose computer are patentable inventions.  This implication is in contradiction with  Art 52 EPC, according to which algorithms, business methods and programs for computers are not inventions in the sense of patent law.

#eWM: Programmers speak about %(q:implementing a specification).  A patent claim usually specifies a sequence of events (i.e. a program) which could be %(e:implemented) by a programmer or by an engineer.  %(q:Implementation) in this context means %(q:working it out in detail so that it can run without errors).  The implementation requires human work.  It is never done by a computer, and in fact computers will usually not come into play during the implementation phase.  Rather, they will be used for executing the final result, and this result will in principle be independent of the computer architecture.

#iWo: Moreover, the programmer would not say that she is implementing an %(q:invention), no matter how new and admirable the specification is.  Rather she might be implementing an algorithm, an idea or, more often, a complex combination of such abstract elements.  Real advances in the art of programming are generally of abstract nature and therefore would usually not be referred to as %(q:inventions) by the person skilled in the art.

#teW: A term like %(q:computer-executed schemes) or %(q:software innovations) would have been less conducive to misunderstanding.  Thereby it would however not have served the aims of the proponents.

#wan: The proponents of patents on %(q:computer-implemented inventions) regularly claimed that their term refers %(q:only to washing machines, mobile phones, intelligent household appliances ...) and %(q:not to software as such).  Unfortunately this claim is at odds with the definition wording used by the proponents themselves.  The term %(q:computer-implemented invention) as defined in the EPO's Trilateral Appendix 6 and in the Commission's Directive Proposal refers to nothing but data processing programs running on general-purpose computing equipment.  Even if the extremely rare cases where some additional equipment is involved in an EPO-granted patent on what they would call a %(q:computer-implemented inventions), the gist of the %(q:invention) will lie in data processing, not in applying heat and reactants to clothes.  Otherwise this would, according to the EPO/CEC definitions, no longer be a %(q:computer-implemented invention) but rather an ordinary technical invention.

#tnW: The European Parliament has redefined the term %(q:computer-implemented invention) in such a way that general-purpose data processing does not qualify while a washing machine, where data processing has only an auxiliary function and is not constitutive for the invention, would qualify.  This amendment is fiercely opposed by the European Commission and by all those who earlier claimed that they want only washing machines and the like but %(q:not software as such) to be patentable.

#cao: While the redefinition of the European Parliament constitutes a clever way of correcting a central flaw of the proposed directive, it does not make the text much clearer.  The misleading term %(q:computer-implemented invention) continues to be used, and it will continue to mislead all those people who have not carefully read and internalised the definition.  Thereby it would also embody the supremacy of a small group of newspeak-creating lawyers over the basic rules by which programmers and engineers have to abid.  Although the Parliament could be credited with ending the invasion of the patent lawyers into the domain of the software engineers, it would leave a symbol of that invasion intact.

#csp: The EPO document of 2000 which introduced the term %(q:computer-implemented invention), in order to legitimate the granting of patents on programs for computers and methods for doing business, in contradiction of the letter and spirit of the written law, so as to bring European juridical practise in line with the practise of the patent offices of the USA and Japan.  The EPO writes:

#srs: The expression %(q:computer-implemented inventions) is intended to cover claims which specify computers, computer networks or other conventional programmable digital apparatus whereby prima facie the novel features of the claimed invention are realised by means of a new program or programs.

#rdc: The only apparent reason for distinguishing %(q:technical effect) from %(q:further technical effect) in the decision was because of the presence of %(q:programs for computers) in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.

#emh: Article 2 of the Commission proposal defines %(q:computer-impemented invention) along the lines of Appendix 6:

#kompinvcec: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#tle2: Counterproposal title amendment

#xwo: Also explains what is wrong with the word C.I.I.

#hWn: This EPO decision of 1984 shows that previously %(q:program for computers) was interpreted to mean exactly what in 2000 the new term %(q:computer-implemented invention) was defined to mean.

#srn: What is a %(q:Computer-Implemented Invention)

#ebe: Educative material written by Jonas Maebe in September 2003

#et2: Amendment 36=42=117

#eWs: The European Parliament voted for a redefinition of the %(q:computer-implemented invention).  Thereby it removed the damaging effects of this term within the directive.  However the term %(q:computer-implemented invention) is still in the title of the directive and will therefore continue to mislead all those people who have not taken the time to carefully read the redefinition.

#Am36: %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

#rhW: In his testimony at the USPTO 1994 hearings, Jim Warren, board member of the US software giant Autodesk Inc, objected against the patent lawyer newspeak term %(q:software related inventions), which is still not quite as biased as %(q:computer-implemented inventions):

#TWp: Thus, I respectfully object to the title for these hearings -- %(q:Software-Related Inventions) -- since you are not primarily concerned with gadgets that are controlled by software. The title illustrates an inappropriate and seriously-misleading bias. In fact, in more than a quarter-century as a computer professional and observer and writer in this industry, I don't recall ever hearing or reading such a phrase -- except in the context of legalistic claims for monopoly, where the claimants were trying to twist the tradition of patenting devices in order to monopolize the execution of intellectual processes.

#enh: Nominate the %(q:computer-impemented invention) for linguistic (un)beauty contests

#WFW: Various language associations regularly call for public bidding for %(q:UnWord of the Year) or similar.  Is there any other high-level political term which is so misleading and so clearly implies a break of law as the term %(q:computer-implemented invention)?  Is there any other political term which is closely connected to attempts at deceiving the legislature, negating fundamental rights and %(da:sidestepping democratic processes in Europe)?  If not, why isn't %(q:Computer-Implemented Invention) the top candidate of all those linguistic beauty contests?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kinv ;
# txtlang: en ;
# multlin: t ;
# End: ;

