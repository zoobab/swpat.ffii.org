<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO 2002-06-21: report to SACEPO on CEC/BSA proposal

#descr: The European Patent Office (EPO) presents a preliminary position on the proposal for a European Directive on the patentability of software innovations and asks its Standing Advisory Committee (SACEPO), a council consisting mainly of corporate patent lawyers, for expression of opinions. The EPO paper refers to the Eurolinux campaign as a %(q:fundamentalist) position promoted by a %(q:strong lobby).  It points out that the CEC/BSA proposal is largely but not completely based on EPO positions and names some points where further clarification would be helpful.  It also explains the state of deliberations in the Council Working Group and the European Parliament and discloses that the EPO has a seat on the %(q:council working party on intellectual property) as a delegate of the European Commission.  The Eurolinux Alliance wonders why it cannot have a seat on this %(q:working party) and on SACEPO.

#ToW: The %(ep:EPO paper) on the CEC proposal, presented to its Standing Advisory Committee (SACEPO), notes

#Ttg2: The EPO statement about the Eurolinux campaign is doubly inaccurate:

#Ecp: Eurolinux does not oppose patentability of inventions related to software, e.g. new chemical reactions conducted under program control.  But Eurolinux does oppose the EPO's practise of patenting of innovations which do not qualify as inventions under Art 52 EPC.

#Tch: The opposition against software patents does not only come from the %(q:open source community) or Linux users, but from a large number of software professionals who write proprietary software.  It has very little to do with open source evangelism, let alone fundamentalism.

#IdW: In its short analysis, the EPO finds that the EC proposal is based very closely on the new EPO guidelines and the EPO's practise.  However, it finds two questions worthy of further clarification:

#moW: meaning of restriction on claim form for enforcement of claims

#hhr: how to distinguish technical contributions from non-technical ones

#Tat: The EPO's concepts seem better than those of CEC/BSA.  In particular they do not award any patent claim the status of an %(q:invention) but, in accordance with Art 52 EPC, acknowledge that there must be a test of whether an invention is present.

#AWt: Also, they suggest at the end to

#MWh: Make clear that mere computer implementation of a per se non-technical method such as business method does not involve a %(q:technical contribution).

#Ttg: This, thought through to the end, would mean that a mere computer implementation of a coding method such as that in the %(jp:JPEG patent) as well as %(bp:numerous other questionable patents granted by the EPO) does not involve a technical contribution and is therefore not an invention.

#Urq: Unfortunately EPO reasoning, like that of the CEC patent lawyers, usually consists of verbal rubble which will have no consequences when it doesn't suit the EPO and the people on SACEPO.

#Asr: At Eurolinux, we wonder why the EPO can't give the Eurolinux Alliance a seat in SACEPO rather than spread misleading assertions about us.  We are, in the EPO's words, a %(q:strong lobby), and we are certainly a part of the public whom the EPO is supposed to serve.  There is also little reason why the %(q:Council Working Party) should be composed only of experts from the patent community and why the European Commission should nominate the EPO as its %(q:expert) delegate.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo020621 ;
# txtlang: en ;
# multlin: t ;
# End: ;

