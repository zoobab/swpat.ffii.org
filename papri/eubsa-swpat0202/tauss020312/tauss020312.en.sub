\begin{subdocument}{tauss020312}{Tauss 2002-03-12: Germany must say No to CEC/BSA Proposal and EPO Practise}{http://swpat.ffii.org/papers/eubsa-swpat0202/tauss020312/index.en.html}{Workgroup\\swpatag@ffii.org}{J\"{o}rg Tauss, president of the New Media Commission of the German Federal Parliament and in charge of Media and Education in the Social Democratic Party (SPD)'s Parliamentary Fraction, has written a letter to his party colleague and minister of justice Prof. Herta D\"{a}ubler-Gmelin.  Tauss asks Ms D\"{a}ubler-Gmelin to opppose this proposal as resolutely as she opposed the planned modification of Art 52 EPC in november 2000.  This proposal, Tauss says, merely enshrines a highly questionable practise of the European Patent Office (EPO).  There is indeed a need for legislating at the EU level on the limits of patentability, Tauss says, but the EPO practise is the problem, not the solution.  Software patents seem harmful for innovation, incompatible with Europe's strategic interests, dangerous for open source software, less than helpful for proprietary software companies including such bing players as SAP, serving mainly the interests of a few american monopolists behind BSA.  The minister should not be fooled by false assertions about the TRIPS treaty.  The TRIPS treaty is a reason to embrace the strict definition of \emph{technical invention} (teaching about physical causalities), according to which software does \emph{not} constitute or belong to a ``field of technology''.  Tauss founds his explanations on an expert hearing which he together with colleagues from all parties in the parliament conducted in June 2001.  It seems that he represents a consensus position shared at least by most of the members of the parliamentary subcommission on the New Media.  This is confirmed by all publicly known statements given by other MPs so far.  The German Government should take a position similar to that of France and of the German Ministry of Economics (BMWi), Tauss says.  The latter has not been published yet, but it is known that there is widespread skepticism about software patents in the BMWi.}
\begin{sect}{text}{The Letter}
J\"{O}RG TAUSS

Member of the German Federal Parliament

President of the Subcommittee for the New Media

J\"{o}rg Tauss, MdB * Unter den Linden 50 * 11011  Berlin

To the\\
Minister of Justice\\
Prof. Dr. Herta D\"{a}ubler-Gmelin\\
Federal Ministery of Justice\\
 - postal exchange -

Berlin 2002-03-12

Software Patents -- Directive Proposal of the European Commission

The European Commission has adopted the long-expected directive proposal concerning patentability of software on 2002-02-20.  As you know, this question has in recent months and years been subject of very controversial discussion.  Thus the Subcommittee for the New Media has, together with the Legal Affairs Committee, conducted a public expert hearing\footnote{http://swpat.ffii.org/events/2001/bundestag/index.en.html} on 2001-06-21, in order to inform itself about the chances and risks of an extended patentability of software. We have summarised the critical results and provided them to the president of the legal commission (see appendix).  Moreover, the Enquete Commission on ``Economic Globalisation - Challenges and Responses'' has concerned itself with the problem of an increasing monopolisation of knowledge and warned explicitely against the negative consequences of an overly rigid interpretation of the TRIPS rules.  The area of software was listed as a negative example in this respect, alongside with the fields of agriculture and food supplies.

In view of these problems, you had at the Conference on the European Patent Convention of November 2000 expressed yourself against a premature modification of Art 52 EPC\footnote{http://swpat.ffii.org/analysis/epc52/index.en.html}.  Now that, after a long internal dissent between the general directorates for Interior Market, Competition and Information Society, he directive proposal (COM 2002 92 final) has come out, this debate is again gaining momentum.  While I will not here go into the details of evaluating this proposal, it seems clear that this proposal leaves a lot of questions open.  It is, to say the least, irritating, that the decided text is, in its crucial points, literally identical to a draft which had been circulating for a while, whose author is believed to be a lawyer from the Business Software Alliance (BSA).  BSA is a lobby group of the big software companies - most importantly Microsoft -, which has so far not been concerned about patent law but about the assertion of specific economic interests.  Why the EU Commission is adopting the proposal of a lobby group may be left to another discussion.  Politically, the following questions seem of paramount interest to me.

\begin{itemize}
\item
An introduction of the american software patent system in Europe must be rejected based on the negative experiences in the USA.  A european path in the politics concerning software patents seems possible and appropriate, and this is apparently also being pursued in Brussels.

\item
The current legally questionable patenting practise of the EPO must be evaluated, and illegally granted patents must be revoked.  To speak in this context of ``creating legal security on the basis of the status quo'', as the directive proposal does, seems somewhat misunderstandable.  To say it clearly:  the EPO has already granted a huge amount of questionable patents, which should not be legalised ex posteriori without thoughrough scrutiny.  Attempting to create legal security on the basis of an aberrance, just in order to put an end to embarassing debates, does not look to me like an appropriate way of proceding.

\item
By allowing the patenting of software, alternative modes of development are put in jeopardy.  Especially open source software, such as the economically successful server software Apache and the operating system Linux, would in its current form no longer be possible (it seems that the route via Brussels is a good way for a monopolist to get rid of its most important competitors).  We cannot on the one hand side promote the use of open source software, which in view of many aspects including security and costs is attractive especially for the public sector, and on the other hand make this chiefly European mode of development impossible and leave the field to american companies by loosening the patentability criteria.
\end{itemize}

Die Patentierbarkeit von Software ist eine Kernfrage der k\"{u}nftigen Entwicklung der Informations- und Wissensgesellschaft, da die Bedeutung sowohl elektronischer Information und Kommunikation, als auch der IT-Infrastruktur weiter zunehmen wird. Lassen wir es weiter zu, dass entscheidende Schnittstellen dieser Infrastruktur zunehmend monopolisiert und der allgemeinen gesellschaftlichen Verf\"{u}gbarkeit entzogen werden, m\"{u}ssen wir uns nicht wundern, wenn in weiten Teilen naturgem\"{a}{\ss} rendite-orientierte Entscheidungen amerikanischer Unternehmen die M\"{o}glichkeits- und Entwicklungsbedingungen auch der europ\"{a}ischen Wissens- und Informationsgesellschaft wie -industrie bestimmen. Erster Hinweise auf die Auswirkungen k\"{o}nnen bereits beobachtet werden, hier m\"{o}chte ich nur drei Beispiele kurz anf\"{u}hren: so ist -- nat\"{u}rlich -- in den USA eine Klage anh\"{a}ngig, in der es um die Verwendung von Hyperlinks geht, dem beinahe wichtigsten Navigationsprinzip im Internet. Dieses verletze Patente, so die Kl\"{a}gerin, die sie an diesem Verfahren halte --- Lizenzgeb\"{u}hren f\"{u}r jeden Mausklick im Netz an ein einziges Unternehmen? In einem zweiten Verfahren werden patentrechtliche Anspr\"{u}che an dem Prinzip erhoben, Kopien digitaler G\"{u}ter aus dem Internet herunterzuladen. Bei jedem dieser Downloads, etwa eines Musikst\"{u}cks oder Programms, w\"{a}ren Geb\"{u}hren an ein einzelnes Unternehmen f\"{a}llig, nur weil es als erstes ein Verfahren patentieren lie{\ss}, welches viele Personen parallel ersonnen haben und das bisher milliardenfach frei eingesetzt wurde. Und drittens schlie{\ss}lich ist erst in diesen Tagen ein Rechtstreit in den USA nach 3 Jahren au{\ss}ergerichtlich beigelegt worden, der 1999 zwischen zwei Unternehmen um das sogenannte One-Click-Patent entbrannte. Hier wurde ein Verfahren patentiert, mit dem Kunden eines Online-Shops ein aktuell angezeigtes Produkt mit nur einem einzigen Mausklick bestellen konnten. Im Umkehrschluss verlangte die Patentinhaberin nunmehr, dass bei der Konkurrenz mindestens zwei Klicks notwendig sein m\"{u}ssen, um den Patentanspruch nicht zu verletzen --- wieder ist der innovative Fortschritt mehr als fragw\"{u}rdig und der Kampf um geringste Wettbewerbsvorteile mehr als offensichtlich. Die Analogien zur Biopatentdebatte sind offenkundig, auch hier ist n\"{a}mlich die Frage zu stellen, welchen au{\ss}erordentlichen gesellschaftlichen Beitrag die Innovationen der Unternehmen geleistet haben, die ein derart folgenreiches und langfristiges Verwertungsmonopol rechtfertigen k\"{o}nnten. Die EU-Kommission dr\"{u}ckt sich um eine Antwort auf diese im Grunde politische Frage: was soll eigentlich patentierbar sein und was geht aus welchen Gr\"{u}nden zu weit? Bis heute gibt es keine Antwort darauf, ob etwa die drei beschriebenen amerikanischen Patente nun auch in Europa m\"{o}glich sein sollen, oder eben nicht. Das EPA, darauf m\"{o}chte ich mit Nachdruck ein weiteres mal hinweisen, hat mittlerweile mehrere Tausend Patente\footnote{http://swpat.ffii.org/patents/index.en.html} erteilt, deren Erfindungsh\"{o}he sich kaum positiv von den zitierten Beispielen abhebt und daher mehr als fraglich ist.

Die EU-Kommission geht der Kernfrage in der Debatte um die Patentierbarkeit von Software gegenw\"{a}rtig aus dem Weg. Sie lautet n\"{a}mlich, ob und inwiefern Software nach den allgemeinen patentrechtlichen Grunds\"{a}tzen tats\"{a}chlich eine Erfindung darstellen kann und auch tats\"{a}chlich dem Bereich dem Technik, auf den das Patentrecht begrenzt ist, zuzuordnen ist. Erfindungen im Sinne einer signifikanten Erweiterung gesellschaftlichen K\"{o}nnens resp. ihrer technischen Probleml\"{o}sungskapazit\"{a}ten sind nicht bereits durch die handwerklich auch noch so gelungene Anwendung bestehenden Wissens und bestehender Verfahren gegeben, sie d\"{u}rfen vielmehr selbst f\"{u}r fachkundige Personen nicht naheliegend sein. Technik im Sinne einer Lehre zum planm\"{a}{\ss}igen Handeln unter Einsatz beherrschbarer Naturkr\"{a}fte zur Erreichung eines kausal \"{u}bersehbaren Erfolges (so der BGH) bedarf offenbar des physikalischen Bezuges. Dieser ist aber in der Dualit\"{a}t von Software/Hardware in der IT-Welt nicht ohne weiteres gegeben, war es doch gerade die Neumann'sche Universalmaschine (die wir heute Computer nennen), die beide Bereiche voneinander unabh\"{a}ngig machte und eigenst\"{a}ndige Entwicklungslinien erm\"{o}glichte. Jedes Programm (als algorithmisierte Logik) l\"{a}uft auf jedem Rechner (als physikalisches Substrat der logischen Manipulationen), gleich welche Programmiersprache verwendet wird oder welcher Prozessor die Befehle abarbeitet --- es ist eben eine logikoffene universale Rechenmaschine. Software ist daher eher einer handwerklichen T\"{a}tigkeit vergleichbar und folgt als textbasiertes Medium eher logischen Regeln als naturgesetzlichen Verfahren. Die Richtlinie verlangt von den Mitgliedstaaten in Artikel 3\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html\#prop} nun aber, so-genannte computerimplementierte Erfindungen als Teil der Technologie auszufassen und spricht sich damit grunds\"{a}tzlich f\"{u}r ihre Patentierbarkeit aus, um die Rechtssicherheit zu erh\"{o}hen - die Beweggr\"{u}nde zu dieser weitreichenden Regelung sind nicht ersichtlich.

Abgesehen davon, dass die rechtliche Frage der Patentierbarkeit von Logiken strittig ist und auch die innovationspolitische Notwendigkeit zumindest f\"{u}r den Bereich der Softwareentwicklung verneint werden kann, sollte auch ein weiteres Ziel nicht aus den Augen verloren werden: wir wollen den IT-Bereich wirklich internationalisieren und eine --- vor allem amerikanisch dominierte --- Monokultur in der Softwarelandschaft aufbrechen. Hier hat Europa eine einmalige Chance, gerade \"{u}ber Open Source-Projekte entscheidende Elemente der k\"{u}nftigen IT-Infrastruktur mitzubestimmen. Diese Chance darf nun nicht durch eine einseitige, an den Interessen (amerikanischer) Gro{\ss}konzerne und deren patentjuristischer Abteilungen orientierte Richtlinie gef\"{a}hrdet werden. Oder, um sinngem\"{a}{\ss} die Worte von Prof. Plattner, dem Vorsitzenden des erfolgreichen deutschen Softwarehauses SAP\footnote{http://swpat.ffii.org/players/sap/index.de.html}, anzuf\"{u}hren: man brauche keine Softwarepatente, um auf den M\"{a}rkten f\"{u}r Software erfolgreich zu sein. Sehr wohl braucht man diese Patente aber, wenn man sich mit den aggressiven amerikanischen Unternehmen vor amerikanischen Gerichten auseinandersetzen will oder gar muss. Auch dort wolle eigentlich keiner Softwarepatente, doch wird die bestehende rechtliche M\"{o}glichkeit zum Rent-Seeking oder Marktabschottung eben inflation\"{a}r genutzt. Dieser Zwang besteht f\"{u}r Europa --- anders als es oft kolportiert wird --- keineswegs, weder verlangt das TRIPS\footnote{http://swpat.ffii.org/analysis/trips/index.en.html} noch der gegenw\"{a}rtig neu verhandelte Patenvertrag der WIPO explizit eine Patentierbarkeit von Software. Niemand beabsichtigt die international anerkannten patentrechtlichen Grunds\"{a}tze generell in Frage zu stellen. Ich bin nur dezidiert der Auffassung, dass Software diese Anforderungen nicht zu erf\"{u}llen vermag und Softwarepatente dar\"{u}ber hinaus nachweislich volks- wie betriebswirtschaftlich negative Effekte produzieren w\"{u}rde.

Die Richtlinie ist daher keineswegs \"{u}berfl\"{u}ssig, sie ist sogar sachlich notwendig und kann ein entscheidendes Signal in die richtige Richtung geben. Dies setzt allerdings voraus, dass es gelingt, erhebliche \"{A}nderungen am gegenw\"{a}rtigen Entwurf durchzusetzen. Hier unterst\"{u}tze ich in jeder Hinsicht die Kritik Frankreichs oder auch aus dem Bundeswirtschaftsministerium zum Richtlinienvorschlag und m\"{o}chte Sie bitten, sich ebenfalls f\"{u}r eine solche Verbesserung und auch Klarstellung einzusetzen. Indem ich auf Ihre weitere Unterst\"{u}tzung in dieser Auseinandersetzung sowie in den bevorstehenden Diskussionen baue, verbleibe ich

sincerely
\end{sect}

\begin{sect}{etc}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf heise 2002-03-14: Streit um Software-Patente geht weiter\footnote{http://www.heise.de/newsticker/data/anw-14.03.02-004/}}}

\begin{quote}
Report about the Tauss letter in Heise Newsticker, the most widely read german IT news source
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament\footnote{http://swpat.ffii.org/events/2001/bundestag/index.en.html}}}

\begin{quote}
Eight experts from the areas of law, informatics and economics will answer questions from MPs, based on written responses to a set of questions.  The interested public is also called to present its answers to any subset of these questions in writing.  We publish here the procedings and submissions.
\end{quote}
\filbreak

\item
{\bf {\bf The TRIPs Treaty and Software Patents\footnote{http://swpat.ffii.org/analysis/trips/index.en.html}}}

\begin{quote}
European patent authorities often cite the TRIPs treaty as a reason for making computer programs patentable.  This reasoning is fallacious and easy to refute.  Here you find everything you need to know.
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak

\item
{\bf {\bf European Software Patent Horror Gallery\footnote{http://swpat.ffii.org/patents/index.en.html}}}

\begin{quote}
A database of the monopolies on programming problems, which the European Patent Office has granted against the letter and spirit of the existing laws, and about which it is unsufficiently informing the public, delivering only chunks of graphical data hidden behind input masks.  The FFII software patent workgroup is trying to single out the software patents, make them better accessible and show their effects on software development.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

