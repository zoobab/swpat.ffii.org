\begin{subdocument}{tauss020312}{Tauss 2002-03-12: Deutschland muss EUK/BSA-Vorschlag und EPA-Praxis zur\"{u}ckweisen}{http://swpat.ffii.org/papiere/eubsa-swpat0202/tauss020312/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Der Vorsitzende des Bundestagsunterausschusses Neue Medien bittet seine Parteikollegin Bundesjustizministerin Prof. Herta D\"{a}ubler-Gmelin, dem Br\"{u}sseler Vorschlag, Programmlogik patentierbar zu machen, eine \"{a}hnlich klare Absage zu erteilen wie Frau D\"{a}ubler-Gmelin es im November 2000 gegen\"{u}ber den Pl\"{a}nen der Patentlobby tat, die ``Programme f\"{u}r Datenverarbeitungsanlagen'' von der Liste der Nicht-Erfindungen im Europ\"{a}ischen Patent\"{u}bereinkommen (EP\"{U}) zu streichen.  Tauss fordert eine Unterst\"{u}tzung der Position Frankreichs und z\"{a}hlt auch das BMWi zum Kreis derer, die dieser Position zuneigen.  Grunds\"{a}tzlich h\"{a}lt Tauss eine Kl\"{a}rung der Grenzen der Patentierbarkeit auf EU-Ebene f\"{u}r sinnvoll.  Der vorliegende Entwurf ziele aber darauf ab, im Sinne des Europ\"{a}ischen Patentamtes (EPA) ``l\"{a}stige Debatten zu beenden'' und so die vom EPA verursachten Probleme weiter zu verschlimmern, statt sie zu l\"{o}sen.}
\begin{sect}{text}{Der Brief}
J\"{O}RG TAUSS

MITGLIED DES DEUTSCHEN BUNDESTAGES

VORSITZENDER UNTERAUSSCHUSS NEUE MEDIEN

J\"{o}rg Tauss, MdB * Unter den Linden 50 * 11011  Berlin

An die\\
Bundesministerin f\"{u}r Justiz\\
Prof. Dr. Herta D\"{a}ubler-Gmelin\\
Bundesministerium f\"{u}r Justiz\\
 - Postaustausch -

Berlin, den 12. M\"{a}rz 2002

Softwarepatente --- Richtlinienentwurf der EU-Kommission

die EU-Kommission hat am 20. Februar 2002 den lange erwarteten Richtlinienentwurf zur Patentierbarkeit von Software beschlossen. Wie Sie wissen, ist diese Frage in den vergangenen Monaten und Jahren sehr kontrovers diskutiert worden. So hat auch der Unterausschuss Neue Medien gemeinsam mit dem Rechtsausschuss des Deutschen Bundestages am 21. Juni 2001 ein \"{o}ffentliches Expertengespr\"{a}ch\footnote{http://swpat.ffii.org/termine/2001/bundestag/index.de.html} durchgef\"{u}hrt, um sich hinsichtlich der Chancen und Risiken einer Erweiterung der Patentierbarkeit von Software zu informieren. Die kritischen Ergebnisse habe ich kursorisch zusammenfassen lassen und dem Vorsitzenden des Rechtsausschusses zur Verf\"{u}gung gestellt (Anlage). Ebenso hat sich die Enquete-Kommission ``Globalisierung der Weltwirtschaft --- Herausforderungen und Antworten'' mit dem Problem der zunehmenden Monopolisierung des Wissens befasst und ausdr\"{u}cklich vor den negativen Auswirklungen --- etwa einer zu engen Auslegung der TRIPS-Bestimmungen --- gewarnt. Hier wurden als negatives Beispiel neben der Gesundheits-, Landwirtschafts- und der Ern\"{a}hrungspolitik insbesondere eben auch der Softwarebereich angef\"{u}hrt.

In dieser Angelegenheit hatten Sie sich auf der Konferenz zum europ\"{a}ischen Patent\"{u}bereinkommen im November 2000 zurecht gegen eine voreilige \"{A}nderung des Art. 52 EP\"{U}\footnote{http://swpat.ffii.org/analyse/epue52/index.de.html} ausgesprochen. Mit dem von der EU-Kommission nach langem internen Streit zwischen den Generaldirektionen Binnenmarkt, Wettbewerb und Informationsgesellschaft beschlossenen Richtlinienvorschlag (KOM (2002) 92end.) gewinnt die Debatte nun wieder an Dynamik. Ohne den Vorschlag an dieser Stelle im Detail bewerten zu wollen, so l\"{a}sst er doch zahlreiche Fragen offen. Zumindest irritierend ist es aber, dass der beschlossene Text in den entscheidenden Punkten wortgleich mit einem bereits l\"{a}nger kursierenden Entwurf ist, als dessen Autor ein Jurist der Business Software Alliance (BSA) gilt. Die BSA wiederum ist ein Interessenverband der gro{\ss}en Softwarehersteller --- allen  voran Microsoft ---, der sich bisher weniger mit Patentrecht, als vielmehr mit der internationalen Durchsetzung eigener wirtschaftlicher Interessen besch\"{a}ftigt hat. Wieso sich die EU-Kommission den Vorschlag eines Interessenverbandes zu eigen macht, sei dahingestellt. Politisch stehen f\"{u}r mich aber weiterhin folgende Fragen im Vordergrund:

\begin{itemize}
\item
Eine \"{U}bernahme des amerikanischen Softwarepatentsystems ist auch aufgrund der bisherigen negativen Erfahrungen in den USA abzulehnen. Ein europ\"{a}ischer Weg in der Softwarepatentpolitik erscheint nicht nur m\"{o}glich, sondern auch angebracht und wird offensichtlich auch in Br\"{u}ssel verfolgt.

\item
Die bisherige, bereits rechtlich strittige Patentierungspolitik des EPA, ist zu evaluieren und unrechtm\"{a}{\ss}ig erteilte Patente sind zu widerrufen. In diesem Zusammenhang von der ``Herstellung der Rechtssicherheit auf Grundlage des  Status Quo'' zu sprechen, wie es der Richtlinienvorschlag tut, erscheint zumindest kl\"{a}rungsbed\"{u}rftig. Um es klar zu sagen: Das EPA hat bereits eine Unmenge fraglicher Patente erteilt, die nicht ohne Pr\"{u}fung mit einem Federstrich im Nachhinein legalisiert werden d\"{u}rfen. Auf Basis einer Fehlentwicklung Rechtssicherheit herstellen zu wollen, nur um l\"{a}stige Debatten zu beenden, erscheint mir nicht als ein angemessenes Vorgehen.

\item
Eine freie Patentierbarkeit von Software entzieht alternativen Entwicklungskonzepten die Grundlage, insbesondere Open Source-Software --- wie die wirtschaftlich erfolgreiche Serversoftware Apache oder das Betriebssystem Linux --- w\"{a}re in der jetzigen Form nicht mehr m\"{o}glich (offenbar ist der Umweg \"{u}ber Br\"{u}ssel ein guter Weg, um sich als weltweiter Monopolist seiner \"{a}rgsten Widersacher zu entledigen). Wir k\"{o}nnen nicht einerseits den Einsatz von Open Source-Software fordern und f\"{o}rdern, die auch hinsichtlich der zunehmend wichtigen Sicherheits- wie Kostenaspekte gerade f\"{u}r den \"{o}ffentlichen Bereich attraktiv sind, andererseits diese insbesondere europ\"{a}ische Entwicklung durch freie Patentierbarkeit unm\"{o}glich machen und amerikanischen Unternehmen das Feld \"{u}berlassen.
\end{itemize}

Die Patentierbarkeit von Software ist eine Kernfrage der k\"{u}nftigen Entwicklung der Informations- und Wissensgesellschaft, da die Bedeutung sowohl elektronischer Information und Kommunikation, als auch der IT-Infrastruktur weiter zunehmen wird. Lassen wir es weiter zu, dass entscheidende Schnittstellen dieser Infrastruktur zunehmend monopolisiert und der allgemeinen gesellschaftlichen Verf\"{u}gbarkeit entzogen werden, m\"{u}ssen wir uns nicht wundern, wenn in weiten Teilen naturgem\"{a}{\ss} rendite-orientierte Entscheidungen amerikanischer Unternehmen die M\"{o}glichkeits- und Entwicklungsbedingungen auch der europ\"{a}ischen Wissens- und Informationsgesellschaft wie -industrie bestimmen. Erster Hinweise auf die Auswirkungen k\"{o}nnen bereits beobachtet werden, hier m\"{o}chte ich nur drei Beispiele kurz anf\"{u}hren: so ist -- nat\"{u}rlich -- in den USA eine Klage anh\"{a}ngig, in der es um die Verwendung von Hyperlinks geht, dem beinahe wichtigsten Navigationsprinzip im Internet. Dieses verletze Patente, so die Kl\"{a}gerin, die sie an diesem Verfahren halte --- Lizenzgeb\"{u}hren f\"{u}r jeden Mausklick im Netz an ein einziges Unternehmen? In einem zweiten Verfahren werden patentrechtliche Anspr\"{u}che an dem Prinzip erhoben, Kopien digitaler G\"{u}ter aus dem Internet herunterzuladen. Bei jedem dieser Downloads, etwa eines Musikst\"{u}cks oder Programms, w\"{a}ren Geb\"{u}hren an ein einzelnes Unternehmen f\"{a}llig, nur weil es als erstes ein Verfahren patentieren lie{\ss}, welches viele Personen parallel ersonnen haben und das bisher milliardenfach frei eingesetzt wurde. Und drittens schlie{\ss}lich ist erst in diesen Tagen ein Rechtstreit in den USA nach 3 Jahren au{\ss}ergerichtlich beigelegt worden, der 1999 zwischen zwei Unternehmen um das sogenannte One-Click-Patent entbrannte. Hier wurde ein Verfahren patentiert, mit dem Kunden eines Online-Shops ein aktuell angezeigtes Produkt mit nur einem einzigen Mausklick bestellen konnten. Im Umkehrschluss verlangte die Patentinhaberin nunmehr, dass bei der Konkurrenz mindestens zwei Klicks notwendig sein m\"{u}ssen, um den Patentanspruch nicht zu verletzen --- wieder ist der innovative Fortschritt mehr als fragw\"{u}rdig und der Kampf um geringste Wettbewerbsvorteile mehr als offensichtlich. Die Analogien zur Biopatentdebatte sind offenkundig, auch hier ist n\"{a}mlich die Frage zu stellen, welchen au{\ss}erordentlichen gesellschaftlichen Beitrag die Innovationen der Unternehmen geleistet haben, die ein derart folgenreiches und langfristiges Verwertungsmonopol rechtfertigen k\"{o}nnten. Die EU-Kommission dr\"{u}ckt sich um eine Antwort auf diese im Grunde politische Frage: was soll eigentlich patentierbar sein und was geht aus welchen Gr\"{u}nden zu weit? Bis heute gibt es keine Antwort darauf, ob etwa die drei beschriebenen amerikanischen Patente nun auch in Europa m\"{o}glich sein sollen, oder eben nicht. Das EPA, darauf m\"{o}chte ich mit Nachdruck ein weiteres mal hinweisen, hat mittlerweile mehrere Tausend Patente\footnote{http://swpat.ffii.org/patente/index.de.html} erteilt, deren Erfindungsh\"{o}he sich kaum positiv von den zitierten Beispielen abhebt und daher mehr als fraglich ist.

Die EU-Kommission geht der Kernfrage in der Debatte um die Patentierbarkeit von Software gegenw\"{a}rtig aus dem Weg. Sie lautet n\"{a}mlich, ob und inwiefern Software nach den allgemeinen patentrechtlichen Grunds\"{a}tzen tats\"{a}chlich eine Erfindung darstellen kann und auch tats\"{a}chlich dem Bereich dem Technik, auf den das Patentrecht begrenzt ist, zuzuordnen ist. Erfindungen im Sinne einer signifikanten Erweiterung gesellschaftlichen K\"{o}nnens resp. ihrer technischen Probleml\"{o}sungskapazit\"{a}ten sind nicht bereits durch die handwerklich auch noch so gelungene Anwendung bestehenden Wissens und bestehender Verfahren gegeben, sie d\"{u}rfen vielmehr selbst f\"{u}r fachkundige Personen nicht naheliegend sein. Technik im Sinne einer Lehre zum planm\"{a}{\ss}igen Handeln unter Einsatz beherrschbarer Naturkr\"{a}fte zur Erreichung eines kausal \"{u}bersehbaren Erfolges (so der BGH) bedarf offenbar des physikalischen Bezuges. Dieser ist aber in der Dualit\"{a}t von Software/Hardware in der IT-Welt nicht ohne weiteres gegeben, war es doch gerade die Neumann'sche Universalmaschine (die wir heute Computer nennen), die beide Bereiche voneinander unabh\"{a}ngig machte und eigenst\"{a}ndige Entwicklungslinien erm\"{o}glichte. Jedes Programm (als algorithmisierte Logik) l\"{a}uft auf jedem Rechner (als physikalisches Substrat der logischen Manipulationen), gleich welche Programmiersprache verwendet wird oder welcher Prozessor die Befehle abarbeitet --- es ist eben eine logikoffene universale Rechenmaschine. Software ist daher eher einer handwerklichen T\"{a}tigkeit vergleichbar und folgt als textbasiertes Medium eher logischen Regeln als naturgesetzlichen Verfahren. Die Richtlinie verlangt von den Mitgliedstaaten in Artikel 3\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html\#prop} nun aber, so-genannte computerimplementierte Erfindungen als Teil der Technologie auszufassen und spricht sich damit grunds\"{a}tzlich f\"{u}r ihre Patentierbarkeit aus, um die Rechtssicherheit zu erh\"{o}hen - die Beweggr\"{u}nde zu dieser weitreichenden Regelung sind nicht ersichtlich.

Abgesehen davon, dass die rechtliche Frage der Patentierbarkeit von Logiken strittig ist und auch die innovationspolitische Notwendigkeit zumindest f\"{u}r den Bereich der Softwareentwicklung verneint werden kann, sollte auch ein weiteres Ziel nicht aus den Augen verloren werden: wir wollen den IT-Bereich wirklich internationalisieren und eine --- vor allem amerikanisch dominierte --- Monokultur in der Softwarelandschaft aufbrechen. Hier hat Europa eine einmalige Chance, gerade \"{u}ber Open Source-Projekte entscheidende Elemente der k\"{u}nftigen IT-Infrastruktur mitzubestimmen. Diese Chance darf nun nicht durch eine einseitige, an den Interessen (amerikanischer) Gro{\ss}konzerne und deren patentjuristischer Abteilungen orientierte Richtlinie gef\"{a}hrdet werden. Oder, um sinngem\"{a}{\ss} die Worte von Prof. Plattner, dem Vorsitzenden des erfolgreichen deutschen Softwarehauses SAP\footnote{http://swpat.ffii.org/akteure/sap/index.de.html}, anzuf\"{u}hren: man brauche keine Softwarepatente, um auf den M\"{a}rkten f\"{u}r Software erfolgreich zu sein. Sehr wohl braucht man diese Patente aber, wenn man sich mit den aggressiven amerikanischen Unternehmen vor amerikanischen Gerichten auseinandersetzen will oder gar muss. Auch dort wolle eigentlich keiner Softwarepatente, doch wird die bestehende rechtliche M\"{o}glichkeit zum Rent-Seeking oder Marktabschottung eben inflation\"{a}r genutzt. Dieser Zwang besteht f\"{u}r Europa --- anders als es oft kolportiert wird --- keineswegs, weder verlangt das TRIPS\footnote{http://swpat.ffii.org/analyse/trips/index.de.html} noch der gegenw\"{a}rtig neu verhandelte Patenvertrag der WIPO explizit eine Patentierbarkeit von Software. Niemand beabsichtigt die international anerkannten patentrechtlichen Grunds\"{a}tze generell in Frage zu stellen. Ich bin nur dezidiert der Auffassung, dass Software diese Anforderungen nicht zu erf\"{u}llen vermag und Softwarepatente dar\"{u}ber hinaus nachweislich volks- wie betriebswirtschaftlich negative Effekte produzieren w\"{u}rde.

Die Richtlinie ist daher keineswegs \"{u}berfl\"{u}ssig, sie ist sogar sachlich notwendig und kann ein entscheidendes Signal in die richtige Richtung geben. Dies setzt allerdings voraus, dass es gelingt, erhebliche \"{A}nderungen am gegenw\"{a}rtigen Entwurf durchzusetzen. Hier unterst\"{u}tze ich in jeder Hinsicht die Kritik Frankreichs oder auch aus dem Bundeswirtschaftsministerium zum Richtlinienvorschlag und m\"{o}chte Sie bitten, sich ebenfalls f\"{u}r eine solche Verbesserung und auch Klarstellung einzusetzen. Indem ich auf Ihre weitere Unterst\"{u}tzung in dieser Auseinandersetzung sowie in den bevorstehenden Diskussionen baue, verbleibe ich

mit freundlichen Gr\"{u}{\ss}en
\end{sect}

\begin{sect}{etc}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf heise 2002-03-14: Streit um Software-Patente geht weiter\footnote{http://www.heise.de/newsticker/data/anw-14.03.02-004/}}}

\begin{quote}
Bericht in Heise Newsticker \"{u}ber den Tauss-Brief
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente\footnote{http://swpat.ffii.org/termine/2001/bundestag/index.de.html}}}

\begin{quote}
Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte \"{O}ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver\"{o}ffentlichen hier das Gespr\"{a}chsprotokoll und die schriftlichen Eingaben.
\end{quote}
\filbreak

\item
{\bf {\bf Der TRIPs-Vertrag und Softwarepatente\footnote{http://swpat.ffii.org/analyse/trips/index.de.html}}}

\begin{quote}
Europas gesetzgebende Patentjuristen zitieren oft den TRIPs-Vertrag als Grund f\"{u}r die angebliche Notwendigkeit, Computerprogramme patentierbar zu machen.  Hier finden Sie alles, was Sie \"{u}ber das ``TRIPs-Scheinargument'' wissen m\"{u}ssen.
\end{quote}
\filbreak

\item
{\bf {\bf EUK \& BSA 2002-02-20: Vorschlag, alle n\"{u}tzlichen Ideen patentierbar zu machen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html}}}

\begin{quote}
Die Europ\"{a}ische Kommission (EUK) schl\"{a}gt vor, die Patentierung von Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren und sicher zu stellen, dass breite und triviale Patente auf Programm- und Gesch\"{a}ftslogik, wie sie derzeit vor allem in den USA von sich reden machen, k\"{u}nftig auch hier in Europa Bestand haben und von keinem Gericht mehr zur\"{u}ckgewiesen werden k\"{o}nnen.  ``Aber hallo, die EUK sagt in ihrer Presseerkl\"{a}rung etwas ganz anderes!'', m\"{o}chten Sie vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK wirklich sagt, m\"{u}ssen Sie n\"{a}mlich nicht die PE sondern den Vorschlag selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom Europ\"{a}ischen Patentamt (EPA) verfasst, in dem gew\"{o}hnliche W\"{o}rter oft das Gegenteil dessen bedeuten, was Sie erwarten w\"{u}rden. Zur Verwirrung tr\"{a}gt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit von Patenten und propriet\"{a}rer Software beschworen wird, wobei dem software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden suggeriert wird.  Dieser Text ignoriert die Meinungen von allen geachteten Programmierern und Wirtschaftswissenschaftlern und st\"{u}tzt seine sp\"{a}rlichen Aussagen \"{u}ber die \"{O}konomie der Software-Entwicklung nur auf zwei unver\"{o}ffentlichte Studien aus dem Umfeld von BSA (von Microsoft und anderen amerikanischen Gro{\ss}unternehmen dominierter Verband zur Durchsetzung des Urheberrechts) \"{u}ber die Wichtigkeit propriet\"{a}rer Software.  Diese Studien haben \"{u}berhaupt nicht Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag selber wurden offensichtlich f\"{u}r die EUK von einem Angestellten von BSA redigiert.  Unten zitieren wir den vollst\"{a}ndigen Vorschlag zusammen mit Belegen f\"{u}r die Rolle von BSA, einer Analyse des Inhalts und einer tabellarischen Gegen\"{u}berstellung der BSA- und EUK-Version sowie einer EP\"{U}-Version, d.h. eines Gegenvorschlages im Geiste des Europ\"{a}ischen Patent\"{u}bereinkommen von 1973 und der aufgekl\"{a}rten Patentliteratur.  Die EP\"{U}-Version sollte Ihnen helfen, die Klarheit und Weisheit der heute g\"{u}ltigen gesetzlichen Regelung verstehen, an deren Aushebelung die Patentanw\"{a}lte der Europ\"{a}ischen Kommission gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet haben.
\end{quote}
\filbreak

\item
{\bf {\bf Gruselkabinett der Europ\"{a}ischen Softwarepatente\footnote{http://swpat.ffii.org/patente/index.de.html}}}

\begin{quote}
Eine Datenbank der Monopole auf Programmieraufgaben, die das Europ\"{a}ischen Patentamt gegen den Buchstaben und Geist der geltenden Gesetze massenweise gew\"{a}hrt hat, und \"{u}ber die es die \"{O}ffentlichkeit nur h\"{a}ppchenweise informiert.  Die Softwarepatent-Arbeitsgruppe des FFII versucht, die Softwarepatente herauszusuchen, besser zug\"{a}nglich zu machen, und ihre Wirkungen auf die Softwareentwicklung zu zeigen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

