<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Essence of the Proposal

#descr: The European Commission presents its proposal %(q:on the patentability of computer-implemented inventions) as a moderate attempt to %(q:harmonise the law) and clarify that recent american software patent excesses are kept out of Europe.  Evidently the European Commission is well aware of what kind of proposal the public might want to see.  Unfortunately it is proposing just the opposite in many ways.  Here is a short summary of what the proposal actually does in the current european context.

#WpW: What the CEC/BSA proposes in terms of legal doctrine

#Wrf: What the CEC/BSA proposes in terms of software economy

#Spt: Some apparent lies

#CWn: Programs as such are patentable inventions

#Aet: Algorithms, business methods etc are patentable

#cte: Inventions need to be neither new nor technical

#Tsi: The EPO's low non-obviousness standards become obligatory

#Tge: The EPO rulings of 1998 are not accepted

#Iin: Patented standards may not be used without license

#Alo: According to this proposal, all computer programs, and thereby all algorithms, including those applicable to social organisation and business, are %(e:patentable inventions).  Art 2 is a clumsy way of saying %(q:computer programs as such are patentable).  Art 3 says that all computerised rules of organisation and calculation are %(q:inventions in a field of technology).  This basically means that algorithms must be ownable for 20 years according to the patenting rules laid down in TRIPs and other international treaties.

#Acd: Any abstract rule that is or can be applied to the material world belongs to a field of technology and is therefore a patentable invention.  It is no longer necessary to assess of what type the achievement is, for which someone wants a patent.  The european traditional distinction between %(tp|patentable vs unpatentable innovations|%(e:technical inventions) vs %(e:rules of organisation and calculation)) is abolished.  Instead all practically applicable ideas are treated as %(e:inventions).  However, these inventions may still be denied a patent, when they are found to be obvious (lack an inventive step) .  The non-obvious aspect, according to the CEC/BSA proposal, must %(e:contain a technical contribution) -- in other words the technical contribution (= invention) does not need to be novel or industrially applicable.   Here the discarded %(e:technical invention) concept seems to come in again through the back door %(q:for practical reasons) -- a nice way of point out %(e:theoretical inconsistency).  It is to be questioned, whether such inconsistency will stand up to WTO challenges based on the TRIPs treaty.

#Ahq: And what is %(q:technical)?  The BSA/CEC answer is circular: %(q:anything that is based on technical considerations).  Moreover, computer programs as such are declared to %(e:belong to a field of technology).  Thus any rule of organisation or calculation (algorithm, business method) is not only a technical invention but also automatically contains a %(q:technical contribution) if it is deemed %(q:new) and %(q:non-obvious).  It must only be presented in the language of computing, i.e. by using terms such as %(q:processor), %(q:memory), %(q:input/output), %(q:network) or even %(q:database) etc.

#CWt: Computing jargon may sound %(q:technical) to the naive mind.  Any computer-literate person however knows that the %(e:universal computer) is our standard %(e:logical device). Computing jargon is merely a metaphorical language for expressing abstract thought.  It is equivalent to mathematics as well as business concepts and many other expression forms of algorithms.  Not only that, it is the standard form in which algorithms come into touch with the material world today.  Thus, under the CEC/BSA proposal, any rule of organisation and calculation, no matter in which field, will receive a patent if it is %(q:novel) and %(q:non-obvious).  Moreover, since algorithms are expressed in pseudo-%(e:technical) clothing rather than in their most naked form, it is ensured that these patents will tend be granted for trivial variations of known algorithms rather than for real mathematical innovations.

#Tfa: This proposal is a 100% transcription of the current doctrine of the European Patent Office (EPO).  An leading EPO judge, in %(ms:explaining) this doctrine, accurately summarised it in the formula %(q:All practical problem solutions are patentable inventions).

#Tsj: The CEC's %(pr:press release) in fact names a few fictive examples of what should be patentable, but it does not name a single example of a new and non-obvious algorithm that would not be patentable.  The CEC's assertion, found elsewhere in the PR, that Amazon One Click might not be patentable under these rules, is a plain lie.  The One-Click idea is, according to the CEC/BSA rules, a %(q:computer-implemented invention) in a %(q:field of technology), which, if found to be novel, also contains a %(q:technical contribution), since methods for improving the efficiency of an automatic process by the clever use of generic computer hardware, according to CEC/BSA, lie in a technical field.

#Erl: Even though the %(q:technical contribution) requirement, as described so far, does not constitute any restriction on patentability, CEC/BSA have added yet another safeguard so as to ensure that it cannot function.  The proposal includes a provision that directly negates the requirement of %(q:technical contribution).

#Ael2: Article 3(2) says that %(q:in determining the technical contribution, the invention must be assessed as a whole).  This means that there must be no assessment of the technical character of the contribution, but only of the claimed object (in EPO/CEC language: %(q:invention)) as a whole.  Thus if the claimed object is a process that runs on a standard computer, the %(q:invention as a whole) can already be considered %(q:technical).

#Tit: To summarise: The requirement of %(q:technical contribution) is a cosmetic one, and several layers of safeguard ensure that it cannot function as a means of restricting patentability:

#Tpn: The %(q:technical contribution) according to CEC/BSA is no longer an independent requirement but somehow mingled into non-obviousness.  This concept is confusing and fragile and says little more than that the technical contribution (i.e. the invention) need not be new.

#Sor: Since there is no longer an independent filter of %(q:technical invention), there is no longer any basis for a test of %(q:technical contribution) within the framework of Art 27 TRIPs.  Any serious attempt to piggyback additional patentability filters onto %(q:non-obviousness) is likely to be seen as a violation of TRIPs, which would lead to WTO disputes and sanctions against EU member states.

#Iuy: Instead of defining what is %(q:technical), the proposal stipulates that computer programs per se are technical and thereby makes it impossible to use this term in any limiting meaning.

#Tia3: The proposal explicitly denies the need for a technical contribution by stipulating that the test of technicity shall not apply to the contribution but to the claimed object as a whole.

#AeC: As a side effect, this directive codifies several detail aspects of the EPO's current formalisms for testing non-obviousness.  These formalisms are theoretically incoherent and practically easy to bypass.  The non-obviousness standards of the EPO are notoriously low.  Not only have they put the German Patent Office under pressure to compete by lowering its standards.  They have even been shown by comparative studies to be lower than those of the patent offices of the USA and Japan.  While in their %(pr:press release) Bolkestein and Liikanen claim that the CEC is doing something to prevent US-style trivial software patents, in reality their proposal does the exact opposite: it makes sure that in Europe any functional idea, however abstract, broad and trivial, will be patentable as long as no bullet-proof prior art document is found.

#Iwa: Instead of creating clear rules, the CEC/BSA tries to push the wild-grown esoteric reasoning habits of a few EPO courts into the law, therebey making this law more proprietary, intransparent and unaccountable than ever.  Evidently the CEC/BSA/EPO want to create a legislative jungle to which they and their friends alone hold the key, perpetually denying everybody else access to the discussion.

#Cxl: Claims to %(q:program on disk), %(q:computer program product), %(q:data structure) etc, which the EPO introduced in 1998, are not allowed, but nevertheless there are %(q:infringing computer programs).

#Tin: The provision of Art 52 EPC that computer programs as such are not patentable inventions is reinterpreted to mean that computer programs (computer-implemented teachings) are as such patentable inventions but they %(tan|may not be claimed as what they are|Indeed all non-trivial software innovations are highly abstract ideas and would, if contradiction was to be consistently avoided, have to be claimed as %(q:a thought process in the human mind, characterised by that ...).  See %(TAM)).

#TmW: The CEC/BSA believes that this dirty construction makes it possible to maintain Art 52 EPC unchanged -- but of course deprived of all practical meaning.

#Aee2: Art 6 pretends to do create an exemption for interoperability purposes, but in fact only ensures that the effect of patents is not limited in practically relevant way even when they are a part of standards.

#Tme: The CEC/BSA ignores all economic studies, even official economic reports published by the governments of France and Germany.  These reports show that the introduction of patents in the software economy tends to create legal risks for innovators, thus stifling innovation and competition.

#T9s: The CEC/BSA also disregards the 90% opposition of software professionals against software patents.

#TWt: The CEC/BSA recognizes that 75% software patents filed in Europe belong to non-EU companies and that European SMEs are not ready to file patents.

#Trr: The CEC/BSA recognizes also that open source software developers may have to get a patent license to keep on developing their project and that nothing garantees that they will receive it.

#TbW: The CEC/BSA believes that there is no possible way to create distincions between SMEs and non SMEs with patent law and that nothing can be done to offset the unbalance in disfavor of open source software (and shareware).

#AWe: Apparently, the European Commission has chosen to back the position of certain lobbying groups (patent attorneys and large IT companies) which may benefit from the extension of the patent system, rather than backing a position based on a neutral assessment of the impact of software patents on innovation, competition, safety and consumers. This would make the current proposed directive illegal with respect to the %(rt:Rome Treaty), which requires that any new CEC Directive must benefit European citizens/consumers in some way and help foster innovation and competition, and possibly also to the European Convention on Human Rights.

#TPt: The CEC/BSA claims that the current european laws on patentability need to be clarified and harmonised.  In reality the related laws are already all the same in all european countries, and they are clear.  The problem is that the EPO is not following them.  Moreover the EPO and its board members (national patent administrators) in a constant struggle with the European Commission over the planned transfer of power from Munich to Brussels.  The EU wants a single European Union Patent, but needs to overcome resistance from the patent establishment in order to achieve this goal.  This is why the EU is doing whatever the EPO wants, as long it can get the Union Patent in return.  Legalisation of software patents is an high-ranking item on the wish list of the EPO.  %(q:The EPO has been flying without a pilot's license for years and now wants to get one from the EU), as one EPO official put it at a %(rm:recent meeting).  The CEC/BSA directive project has nothing whatsoever to do with harmonisation, but is rather part of a whole series of measures designed to buy the cooperation of the patent establishment for the Union Patent project.  The actual Union Patent is expected to take place in 2003 by accession of the EU to the European Patent Convention.  It does not require any new regulations on substantive patent law whatsoever.

#Toi: The CEC/BSA says that %(q:the collective responses on behalf of the regional and sectoral bodies, representing companies of all sizes across the whole of European industry, were unanimous in expressing support for rapid action by the Commission more or less along the lines suggested in the discussion paper).  This is a plain lie, given the large number of organisations, even large ones such as Syntec-Informatique.fr (80000 members), Prosa.dk (15000) members, Bileta, the BBC as well as the 300 organisations in the Eurolinux Alliance epxpress themselves more or less clearly against the discussion paper's approach.

#TuW: The CEC/BSA claims that software ideas are easy to imitate and difficult to develop and that this is %(q:increasingly) so.  The latter statement is unsubstantiated and the former is untrue.  Unlike most ideas in copyrighted works, software ideas are very difficult to imitate, especially when the work is available only in binary form, and fairly easy to develop.  Most software patents represent the intellectual work of a few minutes or hours.

#Twb: The CEC/BSA claims that the European Patent Office does a good job at examining software patents and at making sure that no trivial patents are granted.  It fails to cite any real EPO patents, most of which are trivial.  It is saying this in bad faith, because it knows our %(HG) quite well and has even been orally %(ii:informed) in detail about the situation.   But then this paper is not by the CEC but by BSA.  Yet Francisco Mingorance, as a reader of our mailing list, is also aware of the reality of the EPO's patents, which are, as far as software is concerned, just as bad as those of the USPTO.

#Tlm: The CEC/BSA claims that patents are particularly helpful for small companies.  This is in contradiction to common knowledge and to evidence from all serious studies.

#Tsh: The CEC/BSA claims that opposition procedures at the EPO are very helpful for eliminating %(q:bad patents) and that the US has no system of this kind.  They forget to mention that almost no oppositions have been filed in the software field and that the US institution of %(e:reexamination) may actually be more useful, because there is no time limit of 9 months after publication.

#TWd: The CEC/BSA claims that programming ideas can be distinguished from abstract algorithms and from business methods.  This is not true.  The jargon of %(q:software engineering) is an equivalent of mathematics (and of business methods).  The %(it:problem of abstraction) is well known, and the CEC/BSA tries to fool people into believing that it is addressing it, while it is not, allowing every abstraction to be patented at the most abstract level, which in fact is the level of the universal virtual machine called computer.

#Tge2: The CEC/BSA claims that what the EPO is doing is legal, that the EPC allows the patenting of software and that the proposed regulation changes nothing.  %(sk:This is not true.)  Even the CEC/BSA does mention that very few national courts have followed the EPO on its adventurous course and that the others must be obliged to follow by means of this directive.

#TWs: The CEC/BSA claims that its proposal brings legal security.  In reality, the proposal, besides subjecting software creators to a regime of systematic legal insecurity, creates an ambigous situation for business methods.  It says on the one hand that business methods shouldn't be patented but on the other hand creates rules which inevitably lead to their being patented.  As a result small players in Europe may be deterred from applying for business method patents while large players from the US will easily transfer their business method patents to Europe.  Getting the EPO to patent business methods is a simple art for which american %(ba:instruction manuals) already exist.  Business methods of the %(am:Amazon One Click) type are even directly legalised by this directive proposal.  No special wording skill is needed to get them patented under the proposed CEC/BSA regulation.

#Tls: The CEC/BSA says that the %(rp:regulation proposal) in the %(el:Eurolinux Official Response) agrees to patenting industrial software.  They have evidently not cared to read this proposal.

#Tne2: The authors knowingly reiterate the %(tr:TRIPS Lie).

#Tio: The CEC/BSA proposal includes a financial statement at the end which which claims that the directive will be without budgetary impact and without impact on the number of posts in the Commission.  On the basis of this statement, DG Budget was not consulted in preparation phase.

#TWg: This raises interesting questions:

#Cen: Can one truly envisage a doubling (conservative estimate) of the number of patents granted in Europe without any impact on what it costs to monitor such a situation?

#Aoa: Are no corrective measures envisaged to cope with possible harmful effects, and would these measures come for free?

#Swi: Since the directive plans a review 3 years after delay for tranposition, who will manage this review?

#Tta: The answer might be: we will go on having members of the patent family doing it in the Commission offices, just as %(ps:Paul Schwander) managed for the Commission the study on SME and software patents, just as %(bm:Bernhard Müller) drafted the initial versions in DG Internal Market, before being replaced by %(ah:Anthony Howard).

#Nnr: Note that unfortunately, this is only one aspect: the cost effectiveness part of the financial statement is even more outrageous: Patent expenditures (litigation and associated activities and risks excluded) have been growing 12-15% per year in Europe for the past years. No other tax can afford to grow at such a rate, and even the big industry supporters of patents have started complaining about it.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-esse ;
# txtlang: en ;
# multlin: t ;
# End: ;

