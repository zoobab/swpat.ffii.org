<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Nnr: Notez que malheureusement, ce n'est qu'un aspect: la partie de l'assertion financière sur l'efficacité du coût est encore plus outrageuse: les dépenses en brevets (litiges, activités associées et risques exclus) ont augmenté de 12-15% par an en Europe ces dernières années. Aucun autre impôt ne peut se permettre d'augmenter à un tel rythme, et même les grands supporters industriels des brevets ont commencé à s'en plaindre.

#Tta: La réponse pourrait être: nous allons prendre des membres provenant du système des brevets pour le réaliser dans les bureaux de la Commission, tout comme %(ps:Paul Schwander) dirigea pour la Commission l'étude sur les PMEs et les brevets logiciels, tout comme %(bm:Bernhard Müller) écrit les versions initiales dans le DG Marché Intérieur, avant d'être remplacé par %(ah:Anthony Howard).

#Swi: Comme la directive prévoit un bilan 3 ans après le délai de transposition, qui s'occupera de ce bilan?

#Aoa: Est-ce qu'aucune mesure corrective n'est envisagée pour faire face à des effets néfastes possibles, et ces mesures n'auraient-elles aucun coût?

#Cen: Peut-on vraiment envisager un doublement (estimation conservatrice) du nombre de brevet accordés en Europe sans le moindre impact sur le coût de contrôle d'une telle situation?

#TWg: Cela pose des questions intéressantes:

#Tio: La proposition CCE/BSA inclut une assertion financière qui prétend que la directive n'aura pas d'impact budgétaire ni d'impact sur le nombre de postes dans la Commission. Sur la base d'une telle affirmation, le DG Budget n'a pas été consulté dans la phase de préparation.

#Tne2: Les auteurs repètent consciemment le %(tr:mensonge ADPIC).

#Tls: La CCE/BSA dit que la %(rp:proposition de régulation) dans la %(el:Réponse Officielle d'Eurolinux) accorde le brevetage de logiciels industriels.  Ils n'ont évidemment pas pris la peine de lire cette proposition.

#TWs: La CCE/BSA prétend que sa proposition apporte une sécurité légale.  En réalité, la proposition, en plus de soumettre les créateurs de logiciels à un régime d'insécurité légale systématique, crée une situation ambiguë pour les méthodes d'affaires.  Elle dit d'un côté que les méthodes d'affaires ne doivent pas être brevetées et de l'autre côté elle crée des règles qui mènent inévitablement à leur brevetage.  Pour résultat les petits acteurs en Europe pourront être dissuadées de déposer des brevets sur les méthodes d'affaires tandis que les grandes entreprises américaines vont aisément transférer leurs brevets sur des méthodes d'affaires en Europe.  Faire en sorte que l'OEB brevète des méthodes d'affaires est un art simplissime pour lequel des %(ba:manuels d'instruction) américains existent déjà.  Les méthodes d'affaires du type %(am:One Click d'Amazon) sont même directement légalisées par cette proposition de directive.  Aucun talent de manipulation des mots n'est nécessité pour les voir brevetées sous la régulation proposée par la CCE/BSA.

#Tge2: La CCE/BSA prétend que ce que fait l'OEB est légal, que la CBE permet le brevetage des logiciels et que la régulation proposée ne change rien.  %(sk:Cela n'est pas vrai.)  Même la CCE/BSA mentionne que seule la Cours Fédérale Allemande de Justice (BGH) a suivi l'OEB dans son évolution aventureuse et que les autres doivent être forcés à suivre par le moyen de cette directive.

#TWd: La CCE/BSA prétend que les idées en programmation peuvent être distinguées des algorithmes abstraits et des méthodes d'affaires.  Cela n'est pas vrai.  Le jargon de la %(q:conception de logiciels) est équivalent aux mathématiques (et aux méthodes d'affaires).  Le %(it:problème d'abstraction) est bien connu, et la CCE/BSA essaie de tromper les gens en leur faisant croire qu'elle s'en occupe, alors que ce n'est pas le cas, permettant à chaque abstraction d'être brevetée au niveau le plus abstrait, qui est en fait le niveau de la machine virtuelle universelle appelée ordinateur.

#Tsh: La CCE/BSA prétend que les procédures d'opposition à l'OEB sont très utiles pour éliminer les %(q:mauvais brevets) et que les États-Unis n'ont pas de système de ce type.  Ils oublient de mentionner qu'à peu près aucun opposition n'a été formulée dans le domaine du logiciel et que l'institution américaine de %(e:réexamination) est actuellement plus utile, car il n'y a pas de durée limite de 9 mois après la publication.

#Tlm: La CCE/BSA prétend que les brevets sont particulièrement utiles pour les petites entreprises.  Ceci est en contradiction avec le bon sens et avec les preuves de toutes les études sérieuses.

#Twb: La CCE/BSA prétend que l'Office Européen des Brevets fait un bon travail en examinant les brevets logiciels et en s'assurant qu'aucun brevet trivial n'est accordé.  Elle échoue à citer le moindre brevet de l'OEB, la plupart étant triviaux.  Elle dit ceci de mauvaise foi, car elle connait très bien notre %(HG) et a été %(ii:informée) oralement en détail sur la situation.   Mais alors ce papier ne provient pas de la CCE mais de la BSA.  Francisco Mingorance, étant lecteur de notre liste de diffusion, est aussi au courant de la réalité des brevets de l'OEB, qui sont, en ce qui concerne les logiciels, aussi mauvais que ceux de l'USPTO.

#TuW: La CCE/BSA prétend que les idées d'un logiciel sont aisées à imiter et difficile à développer et que ceci est %(q:de plus en plus) le cas.  La dernière assertion est non corroboré et le précédent est faux.  Contrairement à la plupart des idées contenues dans les oeuvres protégées par le droit d'auteur, les idées d'un logiciel sont très difficiles à imiter, spécialement lorsque l'oeuvre n'est disponible que sous forme binaire, et très aisées à développer.  La plupart des brevets logiciels représentent un travail intellectuel de quelques minutes ou heures.

#Toi: La CCE/BSA dit que %(q:les réponses collectives de la part des corps régionaux et sectoriels, représentant des compagnies de toute taille à travers toute l'industrie européenne, apportaient unanimement un support pour une action rapide de la Commission plus ou moins selon les suggestions du papier de discussion).  C'est un mensonge complet, étant donné le grand nombre d'organisations, même d'importantes comme Syntec-Informatique.fr (80000 membres), Prosa.dk (15000) membres, Bileta, la BBC ainsi que 300 organisations faisant partie de l'Alliance Eurolinux qui se déclarent plus ou moions clairement contre l'approche du papier de discussion.

#TPt: The CEC/BSA claims that the current european laws on patentability need to be clarified and harmonised.  In reality the related laws are already all the same in all european countries, and they are clear.  The problem is that the EPO is not following them.  Moreover the EPO and its board members (national patent administrators) in a constant struggle with the European Commission over the planned transfer of power from Munich to Brussels.  The EU wants a single European Union Patent, but needs to overcome resistance from the patent establishment in order to achieve this goal.  This is why the EU is doing whatever the EPO wants, as long it can get the Union Patent in return.  Legalisation of software patents is an high-ranking item on the wish list of the EPO.  %(q:The EPO has been flying without a pilot's license for years and now wants to get one from the EU), as one EPO official put it at a %(rm:recent meeting).  The CEC/BSA directive project has nothing whatsoever to do with harmonisation, but is rather part of a whole series of measures designed to buy the cooperation of the patent establishment for the Union Patent project.  The actual Union Patent is expected to take place in 2003 by accession of the EU to the European Patent Convention.  It does not require any new regulations on substantive patent law whatsoever.

#AWe: Apparemment, la Commission Européenne a choisi de supporter la position de certains lobbies (les avocats en brevet et les grandes compagnies des TIC) qui peuvent bénéficier de l'extension de la brevetabilité, plutôt que de supporter une position basée sur une estimation neutre de l'impact des brevets logiciels sur l'innovation, la concurrence, la sécurité et les consommateurs. Ceci pourrait rendre la proposition actuelle de Directive illégale selon le %(rt:Traité de Rome) et la Convention Européenne sur les Droits de l'Homme qui requièrent tout deux que chaque nouvelle Directive de la CCE bénéficie en quelque manière aux citoyens/consommateurs européens et favorise l'innovation et la concurrence.

#TbW: La CCE/BSA croit qu'il n'est pas possible de créer des distinctions entre les PMEs et les autres entreprises avec le brevet et que rien ne peut être fait pour compenser l'inégalité de traitement en défaveur des logiciels libres et des sharewares.

#Trr: La CCE/BSA reconnaît aussi que les développeurs de logiciels libres devront obtenir une licence pour continuer à développer leur projet et que rien ne garantit qu'ils recevront cette licence.

#TWt: La CCE/BSA reconnaît que 75% des brevets logiciels déposés en Europe appartiennent à des compagnies hors UE et que les PMEs européennes ne sont pas prêtes à déposer des brevets.

#T9s: La CCE/BSA ne tient aussi aucun compte de l'opposition de 90% des professionnels du logiciel contre les brevets logiciels.

#Tme: La CCE/BSA ignore toutes les études économiques, même les rapports économiques officiels publiés par les gouvernements de la France et de l'Allemagne.  Ces rapports montrent que l'introduction de brevets dans l'économie du logiciel tendent à créer des risques légaux pour les innovateurs, étouffant ainsi l'innovation et la concurrence.

#Aee2: Art 6 pretends to do create an exemption for interoperability purposes, but in fact only ensures that the effect of patents is not limited in practically relevant way even when they are a part of standards.

#TmW: La CCE/BSA croit que cette construction brouillonne rend possible de maintenir l'art. 52 de la CEB inchangée -- mais bien sûr privée de toute signification pratique.

#Tin: La provision de l'article 52 de la CEB (Convention Européenne des Brevets), selon laquelle les programmes d'ordinateur en tant que tels ne sont pas des inventions brevetables, est réinterprétée pour signifier que les programmes (enseignements implémentés par ordinateur) sont des inventions brevetables en tant que tels mais ils %(tan|peuvent ne pas être revendiqués pour ce qu'ils sont|En effet toutes les innovations logicielles non-triviales sont des idées hautement abstraites et pourraient, si la contradiction devait être régulièrement évitée, avoir été revendiquées comme %(q:a processus de pensée dans l'esprit humain, caractérisé par ceci ...).  Voir %(TAM)).

#Cxl: Les revendications pour %(q:un programme sur disque), %(q:un produit logiciel), %(q:une structure de données) etc, que l'OEB a introduit en 1998, ne sont pas autorisées, mais il y a néanmoins des %(q:programmes pour ordinateur enfreignant la loi).

#Iwa: Au lieu de créer des règles claires, la CCE/BSA essaie de mettre dans la loi les habitutes de raisonnement ésotériques et développées de manière sauvage de quelques Cours de l'OEB, rendant cette loi plus propriétaire, opaque et inexplicable que jamais.  Évidemment la CCE/BSA/OEB veut créer une jungle légale dont seuls eux et leurs amis détiennent la clé, déniant perpetuellement toute autre personne d'accéder à la discussion.

#AeC: Pour effet, cette directive codifie de nombreux aspects de détail des formalismes actuels de l'OEB pour le test de non-évidence.  Ces formalismes sont théoriquement incohérents et dans la pratique très faciles à contourner.  Les standards de non-évidence de l'OEB sont notoirement faibles.  Non seulement ils ont mis sous pression l'Office Allemand des Brevets à devenir compétitif en abaissant ses standards.  Ils ont même été montrés comme étant plus faibles que ceux des offices des brevets des États-Unis et du Japon par des études comparatives.  Tandis que dans leur %(pr:communiqué de presse) Bolkestein et Liikanen clament que la CCE fait quelque chose pour préserver l'Europe de brevets logicials trivaux à l'américaine, en réalité leur proposition fait exactement l'opposé: elle permettra qu'en Europe toute idée fonctionnelle, qu'elle soit abstraite, générale ou triviale, sera brevetable tant qu'aucun document d'antécédent imperméable à toute critique ne sera trouvé.

#Tia3: The proposal explicitly denies the need for a technical contribution by stipulating that the test of technicity shall not apply to the contribution but to the claimed object as a whole.

#Iuy: Instead of defining what is %(q:technical), the proposal stipulates that computer programs per se are technical and thereby makes it impossible to use this term in any limiting meaning.

#Sor: Since there is no longer an independent filter of %(q:technical invention), there is no longer any basis for a test of %(q:technical contribution) within the framework of Art 27 TRIPs.  Any serious attempt to piggyback additional patentability filters onto %(q:non-obviousness) is likely to be seen as a violation of TRIPs, which would lead to WTO disputes and sanctions against EU member states.

#Tpn: The %(q:technical contribution) according to CEC/BSA is no longer an independent requirement but somehow mingled into non-obviousness.  This concept is confusing and fragile and says little more than that the technical contribution (i.e. the invention) need not be new.

#Tit: To summarise: The requirement of %(q:technical contribution) is a cosmetic one, and several layers of safeguard ensure that it cannot function as a means of restricting patentability:

#Ael2: Article 3(2) says that %(q:in determining the technical contribution, the invention must be assessed as a whole).  This means that there must be no assessment of the technical character of the contribution, but only of the claimed object (in EPO/CEC language: %(q:invention)) as a whole.  Thus if the claimed object is a process that runs on a standard computer, the %(q:invention as a whole) can already be considered %(q:technical).

#Erl: Even though the %(q:technical contribution) requirement, as described so far, does not constitute any restriction on patentability, CEC/BSA have added yet another safeguard so as to ensure that it cannot function.  The proposal includes a provision that directly negates the requirement of %(q:technical contribution).

#Tsj: Le %(pr:communiqué de presse) de la CCE nomme en fait quelques exemples fictifs de ce qui devrait être brevetable, mais il ne donne pas le moindre exemple d'un algorithme nouveau et non-évident qui ne serait pas brevetable.  L'assertion de la CCE, se trouvant ailleurs dans le communiqué, que le One Click d'Amazon ne serait pas brevetable selon ces règles, est un mensonge total.  L'idée du One-Click est, selon les règles CCE/BSA, une %(q:invention implémentée par ordinateur) dans un %(q:champ de la technologie), laquelle, si elle se trouve nouvelle, contient aussi une %(q:contribution technique), car les méthodes pour améliorer l'efficacité d'un processus automatique par l'usage intelligent d'un ordinateur générique, d'après la CCE/BSA, se trouvent dans un champ de la technique.

#Tfa: Cette proposition est une transcription à 100% de la doctrine actuelle de l'Office Européen de Brevets (OEB).  Un juge OEB en %(ms:expliquant) cette doctrine le résume véritablement dans la formule %(q:Toutes les solutions de problèmes pratiques sont des inventions brevetables).

#CWt: Le jargon informatique peut sembler %(q:technique) au non-initié.  Or ceux qui connaissent un peu la programmation savent que l'%(e:ordinateur universel) est un modèle mathématique qui constitue l'%(e:outil logique standardisé) de la civilisation moderne.  Il est équivalent aux maths ainsi qu'aux concepts de gestion et beaucoup d'autres formes d'expression d'algorithmes.  Plus que cela, il est la forme standardisée dans laquelle les algorithmes opèrent sur le monde matériel.  Ainsi, avec la proposition CCE/BSA toute règle d'organisation et de calcul, dans n'importe quel domaine, est brevetable si elle est %(q:nouvelle) et %(q:non-évidente).  En outre, étant donné que les algorithmes sont exprimés avec un habillement pseudo-technique plutôt que dans la forme la plus abstraite, il est assuré que les brevets vont couvrir plutôt des variations triviales d'algorithmes connus que de vraies innovations mathématiques.

#Ahq: Toute règle abstraite applicable au monde matériel appartient à un champ de technologie et est donc brevetable.  Il n'est plus nécessaire d'analyser le type d'achèvement pour lequel on désire d'obtenir un brevet.  La distinction traditionelle européenne entre les %(e:innovations brevetables et non-brevetables), c.a.d. %(e:inventions techniques) vs %(e:règles d'organisation et calcul) est privée de son fondement légal.  Désormais toute idée pratiquement applicable est traitée comme une %(e:invention technique).  Or le brevet peut néanmoins être refusé pour ces inventions si elles sont considérés comme évidentes, c.a.d. manquant d'activité inventive.  La partie non-évidente, selon la proposition CCE/BSA, doit avoir un %(e:caractère technique).  Ici le concept d' %(q:invention technique) semble rentrer par l'arrière-porte %(q:pour des raisons pratiques) -- une jolie façon d'admettre l'incohérence théorique.  Et qu'est-ce que le %(q:caractère technique)?  La réponse de BSA/CCE est circulaire: %(q:toute idée qui est fondée en considérations techniques).  En outre, les concepts de programmation sont décretés comme %(q:appartenant à un champ de technologie).  Ainsi toute règle d'organisation et de calcul (algorithme, méthode de gestion) n'est pas seulement une invention mais aussi %(q:contient une contribution technique dans son activité inventive) si elle est considérée %(q:non-évidente).  Il suffit de présenter l'idée dans un jargon informatique (utilisant des mots comme %(q:processeur), %(q:mémoire) etc).

#Acd: Any abstract rule that is or can be applied to the material world belongs to a field of technology and is therefore a patentable invention.  It is no longer necessary to assess of what type the achievement is, for which someone wants a patent.  The european traditional distinction between %(tp|patentable vs unpatentable innovations|%(e:technical inventions) vs %(e:rules of organisation and calculation)) is abolished.  Instead all practically applicable ideas are treated as %(e:inventions).  However, these inventions may still be denied a patent, when they are found to be obvious (lack an inventive step) .  The non-obvious aspect, according to the CEC/BSA proposal, must %(e:contain a technical contribution) -- in other words the technical contribution (= invention) does not need to be novel or industrially applicable.   Here the discarded %(e:technical invention) concept seems to come in again through the back door %(q:for practical reasons) -- a nice way of point out %(e:theoretical inconsistency).  It is to be questioned, whether such inconsistency will stand up to WTO challenges based on the TRIPs treaty.

#Alo: Selon ce propos, toute idée %(q:mise en oevre par ordinateur) appartient à un %(q:champ de technologie) et est donc une %(e:invention brevetable).

#Iin: Les Standards Brevetés ne doivent pas être utilisés sans license

#Tge: Les règles de l'OEB de 1998 ne sont pas acceptées

#Tsi: Les standards très faibles de non-évidence de l'OEB sont rendus obligatoires

#cte: Les inventions ne doivent plus être ni nouvelles ni techniques

#Aet: Les algorithmes et méthodes d'affaires etc sont brevetables

#CWn: Les programmes en tant que tels sont des inventions brevetables

#Spt: Quelques mensonges apparents

#Wrf: Ce que la CCE/BSA propose concernant l'économie du logiciel

#WpW: Les propositions CCE/BSA en termes de doctrine légale

#descr: La Commission Européenne présente son Propos %(q:sur la brevetabilités des inventions mises en oevre par ordinateur) comme un projèt modéré, déstiné a %(q:harmoniser le droit) et clarifier que les excès du brevetage de logiciels américains sont tenus hors de l'Europe.  Évidemment la Commission Européenne sait bien quelle sorte de propos le public en Europe voudrait voir.  Malheureusement elle tient l'exacte contraire de ce qu'ell promèt.  Voici une sommaire courte de ce que fait le propos de la Commission Européenne dans le contexte de la pratique de brevets en Europe.

#title: Contenu de la proposition

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-esse ;
# txtlang: fr ;
# multlin: t ;
# End: ;

