\select@language {french}
\select@language {french}
\contentsline {chapter}{\numberline {1}Les propositions CCE/BSA en termes de doctrine l\'{e}gale}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Les programmes en tant que tels sont des inventions brevetables}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Les algorithmes et m\'{e}thodes d'affaires etc sont brevetables}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Les inventions ne doivent plus \^{e}tre ni nouvelles ni techniques}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Les standards tr\`{e}s faibles de non-\'{e}vidence de l'OEB sont rendus obligatoires}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}Les r\`{e}gles de l'OEB de 1998 ne sont pas accept\'{e}es}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}Les Standards Brevet\'{e}s ne doivent pas \^{e}tre utilis\'{e}s sans license}{6}{section.1.6}
\contentsline {chapter}{\numberline {2}Ce que la CCE/BSA propose concernant l'\'{e}conomie du logiciel}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}Quelques mensonges apparents}{8}{chapter.3}
