\select@language {catalan}
\contentsline {chapter}{\numberline {1}What the CEC/BSA proposes in terms of legal doctrine}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Programs as such are patentable inventions}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Algorithms, business methods etc are patentable}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Inventions need to be neither new nor technical}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}The EPO's low non-obviousness standards become obligatory}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}The EPO rulings of 1998 are not accepted}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}Patented standards may not be used without license}{6}{section.1.6}
\contentsline {chapter}{\numberline {2}What the CEC/BSA proposes in terms of software economy}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}Some apparent lies}{8}{chapter.3}
