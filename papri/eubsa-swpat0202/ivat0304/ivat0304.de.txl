<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: IV-net.at 2003/04/24: Schreiben an Österreichische Europa-Parlamentarier

#descr: Stephan Maras, der den Ausschuss für Gewerblichen Rechtschutz bei der Österreichischen Industriellen-Vereinigung leitet, fordert im Namen der Maschinenbauer und Elektronikhersteller, dass nicht nur programmierte Industrieprozesse sondern vor allem reine Datenverarbeitungsprogramme direkt patentierbar und als Texte beanspruchbar sein müssen.  MdEP Malcolm Harbor (brit. Konservative) werde einen Änderungsantrag in diesem Sinne stellen.  Ferner meint Maras, das Urheberrecht sei nur für Gedichte geeignet und sei im Falle von Computerprogrammen sehr leicht zu umgehen, nämlich durch bloße Übersetzung von C nach Perl.  Diese Ausführungen sandte Maras am 24. April 2003 zusammen mit einem ähnlich beschaffenen Schreiben großer Industrieverbände an die österreichischen Abgeordneten im Europäischen Parlament.

#ewn: Ebenfalls auf der Agenda steht der Berichtsentwurf von McCarthy über die Patentierbarkeit von computerimplementierten Erfindungen. Dazu übersende ich Ihnen ein %(q:Joint Statement of Industry) (unterzeichnet zB. von ICC, UNICE, EICTA etc.), in dem der McCarthy-Bericht grundsätzlich unterstützt wird. Nur betreffend Artikel 5 fordern wir noch eine Ergänzung, denn es fehlt die Möglichkeit Patentanträge auch ohne Verbindung mit Hardware einzubringen (siehe Punkt 4 des %(q:Joint Statement)). UNICE erwartet, dass ein solcher %(q:zusätzlicher) Abänderungsantrag noch von einem anderen EP-Parlamentarier (ev. Harbour) eingebracht wird. Sollte dies der Fall sein, bitten wir diesen Antrag zu unterstützen.

#cfh: Ich möchte noch folgende zwei Punkte zum Softwarepatent aus österreichischer Sicht anmerken:

#cbn: Die Patentierung von Software betrifft nicht nur die %(q:klassischen) Softwarehersteller (z.B. Microsoft oder die Linux-Community). Es betrifft auch die Maschinenbau-, Anlagenbau- und Elektro- und Elektronikindustrie.  Denn viele Innovationen spielen sich heute über Verbesserungen der Software ab. Man kann aus einer existierenden Maschine durch neue Software ein ganz anderes Produkt machen - und das ist die eigentliche Innovation. Deshalb ist für diese Industrie die Absicherung der bisherigen Praxis des Europäischen Patentamtes durch die EU-Richtlinie ebenfalls von sehr hoher Bedeutung.  Dennoch besteht die Gefahr, dass diese Anliegen im %(q:Kampf) zwischen großen Softwarehäusern und %(q:unabhängigen) Software-Herstellern (v.a. die LINUX-Community) unterzugehen drohen. Ich kann sagen, dass die Unternehmen, die sich in der IV für dieses Thema engagieren, alle keine %(q:klassischen) Software-Hersteller sind und bitte daher, deren Anliegen um Schutz ihrer Innovationen durch ein Softwarepatent zu berücksichtigen.

#erd: Das österreichische wie europäische Urheberrecht reicht nicht als Schutz für Software-Innovationen. Dies geht aus den Gesprächen zwischen den Patent-Experten der Industrie und dem österreichischen Patentamt klar hervor. Denn das Urheberrecht schützt die Software nur in der originären Textierung. Man kann aber den Effekt und die Idee der Software auch durch eine andere Textierung erreichen. Man muss z.B. nur eine andere Programmsprache wählen (z.B. statt in %(q:C) dann in %(q:Perl) programmieren). So würde der Urheberrechtsschutz umgangen und dennoch die Innovation kopiert.  Zum Vergleich: Das Urheberrecht ist vor allem für Werke der Literatur und Musik geschaffen worden. Ein Gedicht kann ich nur in einer Form schaffen; wenn ich seine Worte ändere, transportiert es nicht mehr die selben Eindrücke.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ivat0304 ;
# txtlang: de ;
# multlin: t ;
# End: ;

