<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: BDI 2002-04-15 zum EUK/BSA-Entwurf: die Industrie will keine Patentierungsmöglichkeiten einbüßen

#descr: Hiermit nimmt der Bundesverband der Deutschen Industrie (BDI) zu dem knapp 2 Monate zuvor von der Europäischen Kommission vorgelegten Entwurf einer Richtlinie über die Patentierbarkeit computer-implementierter Organisations- und Rechenregeln Stellung.  Der BDI sollte satzungsgemäß die Meinung seiner Mitgliedsverbände bündeln.  In der Stellungnahme ist von deren durchaus uneinheitlicher Meinungsbildung jedoch überhaupt nicht die Rede.  Es werden auch nicht Interessen von Unternehmen artikuliert.  Stattdessen finden sich nur Maximalforderungen der Patentbranche wieder, die mit abstrakten Begriffen über abstrakte Begriffe in einem esoterischen Jargon des Europäischen Patentamtes sprechen und dabei häufig noch über die eigenen Abstraktionen stolpern.  So werden z.B. Formulierungen der Kommission, die auf eine möglichst weite Patentierbarkeit zielen, als Einschränkungen der Patentierbarkeit verstanden und bekämpft.  Das einzige Interesse der Industrie besteht laut BDI-Papier darin, keinerlei Möglichkeiten der Patenterlangung und -durchsetzung zu verlieren.  Die Sicht der von Patentdickichten belasteten und bedrohten Unternehmen kommt ebenso wenig zur Sprache wie die (unter Patentjuristen allgemein unbekannte) volkswirtschaftliche Kritik am Patentwesen und seiner Ausdehnung.

#Rmm: Richtlinienvorschlag der EU-Kommission zur Patentierbarkeit computerimplementierter Erfindungen

#DWt: Der BDI unterstützt die EU-Kommission in ihrem Vorhaben, durch eine Richtlinie die Rechtsvorschriften der Mitgliedstaaten zur Patentierbarkeit computerimplementierter Erfindungen zu vereinheitlichen. Die hinsichtlich der Patentierungsvoraussetzungen zugleich zu schaffende Transparenz wird die Rechtssicherheit für Anmelder und ihre Wettbewerber maßgeblich erhöhen.

#Wqe: Wichtigstes Anliegen der Industrie ist, gegenüber dem Status quo keine Möglichkeiten der Patentierbarkeit computerimplementierter Erfindungen einzubüßen. Der Richtlinienvorschlag orientiert sich im Wesentlichen an der bewährten Entscheidungspraxis des EPA und gelangt so grundsätzlich zu einem ausgewogenen Zugang computerimplementierter Erfindungen zum Patentschutz. Es ist richtig, dabei die Patentierbarkeit von Geschäftsmethoden auszuschließen.

#DWi: Dies vorausgeschickt, stellen wir hier drei grundlegende Kritikpunkte heraus, die in der Debatte um den Richtlinienvorschlag von Anfang an berücksichtigt werden sollten.

#ced: %(q:Computerimplementierte Erfindung), Art. 2 (a)

#Dit: Die Definition einer computerimplementierten Erfindung ist unklar: Nach dem Richtlinienvorschlag sind das Erfindungen, die mit einem Computer und einem auf den ersten Blick neuen Programm realisiert werden. Das Merkmal %(q:auf den ersten Blick) schränkt die Patentfähigkeit jedoch zu weit ein. Das zeigt sich beispielsweise, wenn mehrere bekannte Programme in einer Kombination beansprucht werden oder wenn eine Erfindung mit einem bekannten Programm und neuen Hardware-Elementen angemeldet wird.  Das Kriterium %(q:auf den ersten Blick) sollte daher aus Art. 2 (a) gestrichen werden.

#chj: %(q:Technischer Beitrag), Art. 2 (b)

#NWr: Nach Art. 2 (b) des Richtlinienvorschlags darf der Beitrag zum Stand der Technik nicht nahe liegend sein. Dagegen verweist Art. 4 Abs. 3 auf die Prüfung der Erfindung in ihrer Gesamtheit.

#WoW: Würde der Begriff des technischen Beitrags auf die neuen Merkmale einer Erfindung beschränkt, dann würde die Definition des Art. 2 (b) einen Rückschritt gegenüber dem Status quo bedeuten; doch die Richtlinie sollte auch insoweit nicht hinter der EPA-Praxis zurückbleiben. Vielmehr sollte das betreffende Merkmal in Art. 2 (b) gestrichen werden, damit insoweit die Vorschrift des Art. 4 Abs. 3 zum Tragen kommt: Die Erfindung in ihrer Gesamtheit darf nicht nahe liegend sein und muss einen technischen Beitrag liefern, der aber für sich genommen nahe liegend sein kann.

#ZWq: Zudem sollte die Definition des Art. 2 (b) auch nicht hinsichtlich des Technikbezugs der Erfindung zu einer Einengung der Patentfähigkeit führen, wie sie vom EPA entwickelt worden ist: Danach fließen in die Beurteilung der Erfindungshöhe sowohl technische als auch nicht-technische Merkmale ein.

#cWu: %(q:Form des Patentanspruchs), Art. 5

#God: Gemäß Art. 5 des Richtlinienvorschlags müssen Erfindern Erzeugnisansprüche wie auch Verfahrensansprüche zugänglich sein. Nach den Erläuterungen zu dieser Vorschrift folgt der Vorschlag ausdrücklich nicht der Praxis des EPA und schließt Computerprogramm-Produktsansprüche von der Patentierbarkeit aus. Das Argument, dadurch sollten Missverständnisse über die Patentfähigkeit von Computerprogrammen als solche ausgeschlossen werden, überzeugt nicht. Denn die Beschwerdekammer des EPA und der Bundesgerichtshof haben dieses Problem schlicht durch eine entsprechende Erklärung in ihren Entscheidungen gelöst, ohne dass dabei Unklarheiten aufgekommen sind. Dem europäischen Gesetzgeber steht die gleiche Technik offen.

#Its: In der Richtlinie sollten Patente für Computerprogramm-Produktansprüche, die ein wichtiger Wirtschaftsfaktor sind, zugelassen werden, sei es durch eine nicht abschließende Aufzählung der Formen des Anspruchs in Art. 5, sei es durch ihre ausdrückliche Aufnahme in den Anwendungsbereich der Richtlinie.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bdi020415 ;
# txtlang: de ;
# multlin: t ;
# End: ;

