<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: BGH Urheberrechtssenat 1990-10-04: Entscheidung %(q:Betriebssystem)

#descr: Entscheidung des 1. Zivilsenats des BGH zum Urheberrechtsschutz von
Datenverarbeitungsprogrammen und zur Abgrenzung von Urheber- und
Patentrecht: ein Betriebssystem ist kein technisches
Datenverarbeitungsprogramm im Sinne des Patentrechts, da es sich in
Rechenanweisungen zum bestimmungsgemd_en Gebrauch eines Rechners samt
seiner Peripherie erschvpft.  Die Programmierleistung verdient jedoch
urheberrechtlichen Schutz.  Selbst neuere EPA-Entscheidungen seit 1986
sind, zumindest nach Ansicht des EPA, nicht auf Programme als solche
sondern auf darin verkvrperte technische Verfahren gerichtet.  Ein
Betriebssystem ist urheberrechtlich gesch|tzt, weil es eigent|mliche
Leistungen aufweist, die nicht technisch bedingt sind sondern auf
gestalterischen Elementen beruhen.  Gegenstand des Schutzes ist weder
eine (nicht vorhandene) technische Erfindung noch eine (nicht
sch|tzbare) Idee oder Rechenregel, sondern das %(q:Gewebe), d.h. die
von gestalterischer Fdhigkeit und Vorstellungskraft geprdgte Art der
Implementierung und Zuordnung.  Dieses Gewebe weist bei komplexen
Programmen regelmd_ig eine ausreichende Gestaltungshvhe auf.  Die
Darlegungslast fdllt auf denjenigen, welcher die Gestaltungshvhe
abstreiten mvchte.

#Dee2: Die Klägerin stellt Datenverarbeitungsanlagen (Computer, Hardware) her
und vertreibt diese; sie bietet für ihre Anlagen auch die dazugehörige
Systemsoftware an.  Die Beklagte .. handelt mit gebrauchten
Datenverarbeitungsanlagen der Klägerin, die sie von Dritten ankauft
und (einschließlich der Systemsoftware) weiterveräußert und auch beim
Erwerber installiert.  Die Parteien streiten darüber, ob die Beklagte
.. die Systemsoftware der Klägerin i Rahmen der Weiterveräußerung in
unzulässiger Weise benutzt und verwertet.

#Dcr: Die Beklagten .. haben die Urheberrechtsschutzfähigkeit der
Systemsoftware der Klägerin in Abrede gestellt ...

#Dhe: Das Landesgericht hat die Klage abgewiesen. ...

#DsW: Die Berufung ist ohne Erfolg geblieben.  ...

#DWe: Das Berufungsgericht hat sowohl urheber- als auch
wettbewerbsrechtliche Ansprüche verneint ...

#DsW2: Die Revision hat Erfolg.

#Don: Die streitgegenständliche Systemsoftware ist einem Urheberrechtsschutz
für Datenverarbeitungsprogramme grundsätzlich zugänglich.  Die vom
Berufungsgericht angedeuteten Bedenken, es käme möglicherweise nur ein
Patentschutz in Betracht, sind unbegründet.  Zum einen würde ein
solcher Schutz einem Urehberrechtsschutz nicht zwingend
entgegenstehen, zum anderen scheidet er hier aber auch aus.  Nach §1
Abs 2 Nr 3, Abs 3 PatG sind %(q:Programme für
Datenverarbeitungsanlagen) als solche nicht als Erfindungen anzusehen.
 Damit sind alle Computerprogramme nicht technischer Natur vom
Patentschutz ausgenommen.  %(tn:Dies gilt allerdings nicht für
Programme technischer Natur).  Betriebssysteme der vorliegenden Art,
die %(st:lediglich der Steuerung eines Computers und der mit ihm
verbundenen Anschlussgeräte dienen), %(kt:stellen keine technischen
Programme in diesem Sinne dar).  Auch in der vom Berufungsgericht
angeführten %(ep:Entscheidung der technischen Beschwerdekammer des
Europäischen Patentamts), die eine computerbezogene Erfindung zum
Gegenstand hatte, ging es nicht um den Patentschutz für ein dem
Betriebssystem vergleichbares Programm; der auf ein technisches
Verfahren gerichtete Patentanspruch wurde nicht als ein auf eine
Computerprogramm als solches bezogener Schutzanspruch angesehen.

#Uma: Urt. vom 13. Mai 1980

#8uW: 8. Aufl. 1988

#uWl: § 1 Rdn. 104 m.w. Nachw.

#ner: nachflogend unter II.3.b

#sec: siehe auch %s

#Dee: Die Annahme des Berufungsgerichts, die Klägerin habe nicht
substantiierte dargetan, dass die streitgegenständliche Systemsoftware
eine für den Urheberrechtsschutz erforderliche persönliche geistige
Schöpfung im Sinne des §2 Abs 2 UrhG darstelle, hält der rechtlichen
Nachprüfung nicht stand.  Das Berufungsgericht hat zu hohe
Anforderungen an die Darlegungslast der Klägerin gestellt.

#ArW: Abweichend vom Berufungsgericht ist auch davon auszugehen, dass die
Klägerin eine für die Urheberrechtsschutzfähigkeit hinreichende
Gestaltungshöhe ihrer Systemsoftware substantiiert beansprucht hat. 
Das Berufungsgericht hat ... ausgeführt, dass im Klagevorbringen
nirgends deutlich werde, wo in der Auswahl, Sammlung, Anordnung und
Einteilung der Informationen schöpferische Eigenheiten liegen könnten.
 Die von der Klägerin für eigentümlich gehaltenen Leistungen seien nur
kursorisch umschrieben, ohne dass sich daraus entnehmen ließe, ob die
gewählten Strukturen technisch bedingt seien oder auf gestalterischen
Elementen beruhten.  Über %(q:theoretische Umschreibungen) und
%(q:Abstraktionen) hinausgehende sinnlich wahrnehmbare
Formgestaltungen seien nicht erkennbar.

#Dof: Der Senat sieht keine Veranlassung ..., von den Grundsätzen der
Inkasso-Programm-Entscheidung abzuweichen.  In den %(ae:angeführten
Entscheidungen) ist mit Rücksicht auf die Art der in Frage stehenden
Werke als Darstellungen wissenschaftlicher und technischer Art und des
für diese Werke eng begrenzten Schutzumfangs der für die
Urheberrechtsfähigkeit vorausgesetzte schöpferische
Eigentümlichkeitsgrad nicht zu hoch angesetzt worden.  Die
Zweckbestimmung dieser - von %{LEX} gleichwohl als schutzwürdig
angesehenen - Darstellungen, wie Zeichnungen, Pläne, Skizzen, lässt
für eine individuelle Gestaltung wenig Raum, so dass einerseits die
Anforderungen an die Schutzfähigkeit nicht zu hoch gestellt und
andererseits der Schutzumfang entsprechend eng begrenzt werden muss. 
Bei Datenverarbeitungsprogrammen bestehen dagegen -- wie in der
Inkasso-Programm-Entscheidung dargelegt -- vielfältige Möglichkeiten
einer individuellen schöpferischen Gestaltung.  Dementsprechend sind
die Anforderungen bei ihnen nicht zu niedrig anzusetzen; die
Gestaltung muss jedenfalls das handwerkliche Durchschnittskönnen
erheblich überragen.  Diese an der üblichen urheberrechtlichen Diktion
ausgerichtete Formulierung enthält keine gegenüber der allgemeinen
urheberrechtlichen Grundsätzen verschärften Anforderungen für
Datenverarbeitungsprogramme, sondern %(wa:überträgt diese Grundsätze
auf diese besondere Werkart).

#Ume: Urteil vom 20. November 1986

#UvJ: Urteil vom 2. Juli 1987

#acw: aus der Rechtsprechung zum Schriftwerkschutz u.a. %s

#Dtw: Die Klägerin hat hier im einzelnen hinreichend substantiiert dargelegt
und unter Beweis gestellt, dass ihre Systemsoftware die danach
erforderliche Gestaltungshöhe erreicht. ...

#Ugn: Unter diesen Umständen hätte das Berufungsgericht das durch zahlreiche
Meinungsäußerungen gestützte Vorbringen der Klägerin zur
Urheberrechtsschutzfähigkeit von Systemsoftware der
streitgegenständlichen Art nicht als unsubstantiiert behandeln dürfen,
zumal es vorliegend nicht auf eine genaue Festlegung der
eigenschöpferischen Elemente im einzelnen ankommt, sondern die
Feststellung ausreicht, dass die Systemsoftware überhaupt solche
Elemente aufweist.

#Dni: Die Sache ist daher zur weiteren tatrichterlichen Aufklärung an das
Berufungsgericht zurückzuverweisen.  Das Berufungsgericht wird das von
der Klägerin beantragte Sachverständigengutachten einzuholen haben, um
zu klären, ob sich in der Sammlung, Auswahl und Gliederung der Befehle
und einer Vielzahl variierbarer Zweckmäßigkeitserwägungen
eigenschöpferische Züge zeigen.  Es wird dabei vor allem auf die
individuelle (formale Programmstruktur) abzustellen haben mit der
häufigen Benutzung von Grundfunktionen für eine Vielzahl von
unterschiedlichen Anwendungsprogrammen, der Art, wie Unterprogramme,
Arbeitsroutinen, mit Verzweigungsanweisungen verknüpft werden u.ä.;
dazu gehört auch die Verwendung von Algorithmen, die zwar als solche
%(ip:einem Urheberrechtsschutz nicht zugänglich sind), wohl aber
%(iz:in der Art und Weise der Implementierung und Zuordnung zueinander
urheberrechtsschutzfähig sein können).  Nicht die Rechenregel, die
Idee, die mathematische Formel ist hier Gegenstand des Schutzes,
sondern das %(gw:Gewebe).

#dzW: dazu %s

#Nl2: Nichtannahmebeschluss des BGH vom 26. September 1985

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh1-bs90 ;
# txtlang: de ;
# multlin: t ;
# End: ;

