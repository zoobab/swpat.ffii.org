<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Keukenschrijver (BGH) 2000-03-29: Ausweg des BGH aus der Steinzeit

#Ibs: Im Frühjahr 2000 gab der Bundesgerichtshof (BGH) in Deutschland mit
der Entscheidung %(q:Logikverifikation) ein Signal zur grenzenlosen
Patentierbarkeit aller Organisations- und Rechenregeln, die über
%(q:programmtechnische Vorrichtungen) verwirklicht werden.  Mit dieser
Entscheidung folgte der BGH einem sehr laut gewordenen Ruf aus der
Patentanbranche.  Kurz nach Bekanntwerden der Entscheidung
%(q:Logikverifikation) berichtet BGH-Richter Keukenschrijver der
Münchener Patentanwaltskammer von dem neuen Durchbruch. 
Keukenschrijver kommt der Stimmung in seinem Publikum sehr entgegen,
wenn er die frühere ablehndende Haltung des BGH zu Softwarepatenten
als %(q:steinzeitlich) bezeichnet und die neuen Entscheidungen als
einen Meilenstein auf dem Weg in ein besseres Zeitalter feiert. 
Insbesondere entspricht Keukenschrijver dem Geschmack des Publikums,
wenn er erklärt, der BGH habe die geltenden Gesetze nie angewendet uns
sich stattdessen zunächst nach dem %(q:altsteinzeitlichen)
Technikbegriff der Dispositionsprogramm-Entscheidung gerichtet, der
nunmehr einer neusteinzeitlichen Variante gewichen sei.

#Beu: Bewertung

#Wdw: Walzstabteilung und das schlechte Gewissen des BGH heute

#WeW: Im neuen Sprachgebrauch des BGH hat der Technikbegriff jegliche
Abgrenzungsfunktion eingebüßt und erscheint daher unbrauchbar und
altmodisch (%(q:steinzeitlich)).  Es ist jedoch unredlich, die
Richterkollegen früherer Jahrzehnte hierfür verantwortlich machen zu
wollen.  Insbesondere der Beschluss %(q:Walzstabteilung), den
Keukenschrijver nicht zu verstehen vorgibt, war ein Musterbeispiel
scharfen Denkens und ein auch heute zeitgemäßes Vorbild einer klaren
Abgrenzungsregel, an der sich der BGH gerade im Falle
%(q:Logikverifikation) hätte orientieren können.  Im ostentativen
Nichtverstehen zeigt sich das schlechte Gewissen und damit die Macht,
die von Beschlüssen wie %(q:Dispositionsprogramm) und
%(q:Walzstabteilung) heute weiterhin ausgeht.

#ngW: Manche Erkenntnisse erfordern langjährige harte Aufklärungsarbeit.  Im
Falle der Abgrenzungsregeln für die %(q:technische Erfindung) waren
die Widerstände groß und das Wissen wurde niedergejohlt, bevor es sich
nachhaltig verbreiten konnte.   Schade nur, dass Keukenschrijver hier
den Johlern nach dem Mund redet, statt um Verständnis für die
großartigen Begriffsbildungsarbeit des BGH zu werben.

#uzg: Auszüge

#HiW: %(sf:Herr Freischem) hat schon vieles zur bisherigen Rechtsprechung
des BGH gesagt. Es gab bisher in den letzten 24 Jahren, solange ist
das jetzt her, 11 Entscheidungen des X. Zivilsenats zur Frage der
Patentfähigkeit programmbezogener Erfindungen, davon neun
Rechtsbeschwerdesachen, die wichtigsten hat Herr Freischem
angesprochen, vielleicht noch nicht einmal die extrem Gelagerten. Die
%(wt:Walzstabteilung) war nicht dabei, das war für mich immer die, mit
der ich die größten Schwierigkeiten beim Verständnis hatte.

#Wta: Wenn man die Entscheidungsschlagwörter anschaut, bekommt man zunächst
den Eindruck, dass es noch wesentlich mehr Entscheidungen gegeben hat.
 Informationsträger, auch tragbare Informationsträger,
Informationssignal, Textdatenverarbeitung, aber das hat alles mit
unserer Problematik nichts zu tun, das sind größtenteils
Entscheidungen, die zur Teilungsproblematik oder zu ganz anderen
Fragen ergangen sind. Lassen Sie sich von daher nicht täuschen.

#Miu: Man kann wohl drei Perioden in der Rechtsprechung des BGH
unterscheiden mit gewissen Übergangserscheinungen, also eine Art
Paläolithikum, Mesolithikum und Neolithikium, auf Computerprogramme
übertragen.

#Dsi: Die erste Periode im wesentlichen 1976 bis 1980,
%(dp:Dispositionsprogramm) und %(ab:Antiblockiersystem) als
wichtigste, und man wird auch die %(fm:Flugkostenminimierung) noch als
späten Nachläufer mit dazu rechnen müssen. Das war eine recht
restriktive Periode: Nur für technische Programme, was auch immer das
sein sollte, war Patentschutz möglich.

#Dnl: Die zweite Periode war eine deutliche Periode des Umbruchs und des
Wandels, %(q:Seitenpuffer), %(cs:Chinesische Schriftzeichen) ein
speziell gelagerter Fall, und %(tc:Tauchcomputer) waren Fälle, bei
denen die Weichen anders gestellt worden sind und es in eine andere
Richtung gegangen ist, und wir sind jetzt in der dritten Periode, die
vielleicht noch nicht so ganz die klare Konturen hat, aber ich meine,
wenn man einige Monate wartet und sich das rückblickend anschaut, wird
man auch sehen, dass es hier doch ganz erhebliche Umbrüche gegeben hat
und gibt.

#IPn: Ich möchte noch auf eines hinweisen, das ist vielleicht bisher nicht
so deutlich geworden: Alle Entscheidungen des X. Zivilsenats setzen
bei der Technizität an und nicht beim Patentierungsausschluss für
Programme als solche.  Es gibt keine Entscheidung des X. Senats, die
dezidiert Stellung nimmt zu der Frage der Programme als solche. Das
wird nur in einer Entscheidung des I. Zivilsenats, der für
Urheberrecht zuständig ist, angesprochen.  Diese Entscheidung ist ein
bisschen in Vergessenheit und aus dem Blickfeld geraten.

#Eim: Es ist die Entscheidung %(bs:Betriebssystem) aus dem Oktober 1990,
dort ging es um die urheberrechtliche Schutzfähigkeit einer
Systemsoftware und dort wurde auch erörtert (es ist wohl ein obiter
dictum), ob diese Systemsoftware patentrechtlich schutzfähig ist. Der
I. Senat hat dazu folgendes gesagt:

#Nre: Nach § 1 Absatz 2 Nr. 3 und Absatz 3 PatG sind Programme für
Datenverarbeitungsanlagen als solche nicht als Erfindung anzusehen,
damit sind alle Computerprogramme nichttechnischer Natur vom
Patentschutz ausgenommen.

#Der: Dies gilt allerdings nicht für Programme technischer Natur, hierzu
werden %(dp:Dispositionsprogramm) und %(ab:Antiblockiersystem)
zitiert. Betriebssysteme der vorliegenden Art, heißt es weiter, die
lediglich der Steuerung eines Computers und der mit ihm verbundenen
Anschlussgeräte dienen, stellen keine technischen Programme in diesem
Sinne dar. Das wird mit zwei Entscheidungen des Bundespatentgerichts
unterlegt. Das ist eine sehr weitgehende Position, aber sie ist
irgendwie aus dem Blick geraten und auch in der Entscheidung
%(lv:Logikverifikation) nicht angesprochen. Vielleicht ist es ganz
gut, dass sie nicht mehr im Blickfeld ist, aber wir stehen irgendwann
eventuell doch vor dem Problem, müssen wir den I. Senat fragen, ob er
an dieser Rechtsprechung festhält, müssen wir eventuell den Großen
Senat anrufen? Nachdem sich aber unsere jetzigen Fälle im wesentlichen
nicht auf der Schiene des Patentierungsausschlusses für Programme als
solche abspielen, sondern auf der Ebene der Technizität, gehe ich im
Moment noch davon aus, dass diese Frage wohl nicht virulent werden
wird.

#Iju: Ich komme jetzt zur aktuellen Rechtsprechung. Die
%(lv:Logikverifikation) liegt Ihnen vor. Herr Freischem hat dazu auch
schon einiges gesagt, auch den Ausgangsfall dargestellt, der auch
veröffentlicht ist. Ich möchte deshalb nichts zum Sachverhalt sagen,
sondern nur die Entscheidung kurz vorstellen. Ich mache das mit
denselben Worten wie vor 14 Tagen hier auf der GRUR-Veranstaltung, wer
dort war, kann jetzt weghören.

#Dre: Der 17. Senat des Patentgerichts hatte hier die Technizität verneint,
weil der Kern des Verfahrens in einer eine gedanklich-logische
Anweisung darstellenden Regel zum Ordnen von Daten bestehe und das
Verfahren keinen Hinweis auf einen neuen Aufbau oder eine neue Art der
Benutzung der Datenverarbeitungsanlage enthalte. Der BGH hat
demgegenüber den technischen Charakter des Programms bejaht. Er hat
zunächst bestätigt, dass die Patentfähigkeit eines Programms für
Datenverarbeitungsanlagen einen technischen Charakter des
beanspruchten Gegenstands erfordert. Insoweit hat er auf seine
bisherige Rechtsprechung, auf die %(cs:Chinesischen Schriftzeichen),
sowie auf die beiden neuen EPA-Entscheidungen
%(q:Computerprogrammprodukt) und auf %(tr:Art. 27 TRIPS-Übereinkommen)
verwiesen.

#Egg: Er hat weiter ausgeführt, dass die Kriterien der Entscheidungen
%(sp:Seitenpuffer) und %(dp:Dispositionsprogramm) für die Technizität
nicht erfüllt sind, da es Ziel der Anmeldung gerade ist, die
Verifikation mit Hilfe einer keine besonderen Anforderungen stellenden
Datenverarbeitungsanlage zu erledigen. Unter Bezugnahme auf die
%(ab:Antiblockiersystem)- und die %(tc:Tauchcomputer)-Entscheidung hat
er weiter darauf abgestellt, dass mit diesen Möglichkeiten kein
abschließender Katalog vorgegeben ist, sondern die Technizität einer
programmbezogenen Patentanmeldung auf Grund einer Gesamtbetrachtung
des Anmeldungsgegenstands im Einzelfall festzustellen ist, was eine
Bewertung des im Patentanspruch definierten Gegenstands bedeutet und
eine unterschiedliche Gewichtung einzelner Anspruchsmerkmale nicht
ausschließt, wobei diese Bewertung aber von Neuheit und erfinderischer
Tätigkeit nicht abhängt.

#DiW: Da die angemeldete Lehre einen Zwischenschritt in dem Prozess betraf,
der mit der Chipherstellung endete, indem mit ihrer Hilfe dafür
gesorgt wurde, dass die Bauteile aus verifizierten Schaltungen
bestehen, war sie nach ihrer Zweckbestimmung Teil einer, so wörtlich,
%(q:aktuellen Technik). Sie nutzte zwar ein gedankliches Konzept,
beschränkte sich aber nicht darauf.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: keuken00 ;
# txtlang: de ;
# multlin: t ;
# End: ;

