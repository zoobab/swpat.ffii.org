\begin{subdocument}{keuken00}{Keukenschrijver (BGH) 2000-03-29: Ausweg des BGH aus der Steinzeit}{http://swpat.ffii.org/papiere/keuken00/index.de.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\deutsche Version 2005/03/25 von Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Im Fr|hjahr 2000 gab der Bundesgerichtshof (BGH) in Deutschland mit der Entscheidung ``Logikverifikation'' ein Signal zur grenzenlosen Patentierbarkeit aller Organisations- und Rechenregeln, die |ber ``programmtechnische Vorrichtungen'' verwirklicht werden.  Mit dieser Entscheidung folgte der BGH einem sehr laut gewordenen Ruf aus der Patentanbranche.  Kurz nach Bekanntwerden der Entscheidung ``Logikverifikation'' berichtet BGH-Richter Keukenschrijver der M|nchener Patentanwaltskammer von dem neuen Durchbruch.  Keukenschrijver kommt der Stimmung in seinem Publikum sehr entgegen, wenn er die fr|here ablehndende Haltung des BGH zu Softwarepatenten als ``steinzeitlich'' bezeichnet und die neuen Entscheidungen als einen Meilenstein auf dem Weg in ein besseres Zeitalter feiert.  Insbesondere entspricht Keukenschrijver dem Geschmack des Publikums, wenn er erkldrt, der BGH habe die geltenden Gesetze nie angewendet uns sich stattdessen zundchst nach dem ``altsteinzeitlichen'' Technikbegriff der Dispositionsprogramm-Entscheidung gerichtet, der nunmehr einer neusteinzeitlichen Variante gewichen sei.}
\begin{description}
\item[Titel:]\ Keukenschrijver (BGH) 2000-03-29: Ausweg des BGH aus der Steinzeit
\item[Quelle:]\ Vortrag vor Patentjuristen in M\"{u}nchen
\item[Bewertung:]\ siehe Walzstabteilung und das schlechte Gewissen des BGH heute\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Jul/0123.html}}
Im neuen Sprachgebrauch des BGH hat der Technikbegriff jegliche Abgrenzungsfunktion eingeb\"{u}{\ss}t und erscheint daher unbrauchbar und altmodisch (``steinzeitlich'').  Es ist jedoch unredlich, die Richterkollegen fr\"{u}herer Jahrzehnte hierf\"{u}r verantwortlich machen zu wollen.  Insbesondere der Beschluss ``Walzstabteilung'', den Keukenschrijver nicht zu verstehen vorgibt, war ein Musterbeispiel scharfen Denkens und ein auch heute zeitgem\"{a}{\ss}es Vorbild einer klaren Abgrenzungsregel, an der sich der BGH gerade im Falle ``Logikverifikation'' h\"{a}tte orientieren k\"{o}nnen.  Im ostentativen Nichtverstehen zeigt sich das schlechte Gewissen und damit die Macht, die von Beschl\"{u}ssen wie ``Dispositionsprogramm'' und ``Walzstabteilung'' heute weiterhin ausgeht.
Manche Erkenntnisse erfordern langj\"{a}hrige harte Aufkl\"{a}rungsarbeit.  Im Falle der Abgrenzungsregeln f\"{u}r die ``technische Erfindung'' waren die Widerst\"{a}nde gro{\ss} und das Wissen wurde niedergejohlt, bevor es sich nachhaltig verbreiten konnte.   Schade nur, dass Keukenschrijver hier den Johlern nach dem Mund redet, statt um Verst\"{a}ndnis f\"{u}r die gro{\ss}artigen Begriffsbildungsarbeit des BGH zu werben.
\end{description}

\begin{sect}{excerpt}{Ausz\"{u}ge}
\begin{quote}
{\it Herr Freischem\footnote{\url{http://swpat.ffii.org/akteure/freischem/index.en.html}} hat schon vieles zur bisherigen Rechtsprechung des BGH gesagt. Es gab bisher in den letzten 24 Jahren, solange ist das jetzt her, 11 Entscheidungen des X. Zivilsenats zur Frage der Patentf\"{a}higkeit programmbezogener Erfindungen, davon neun Rechtsbeschwerdesachen, die wichtigsten hat Herr Freischem angesprochen, vielleicht noch nicht einmal die extrem Gelagerten. Die Walzstabteilung\footnote{\url{http://swpat.ffii.org/papiere/bgh-walzst80/index.de.html}} war nicht dabei, das war f\"{u}r mich immer die, mit der ich die gr\"{o}{\ss}ten Schwierigkeiten beim Verst\"{a}ndnis hatte.}

{\it Wenn man die Entscheidungsschlagw\"{o}rter anschaut, bekommt man zun\"{a}chst den Eindruck, dass es noch wesentlich mehr Entscheidungen gegeben hat.  Informationstr\"{a}ger, auch tragbare Informationstr\"{a}ger, Informationssignal, Textdatenverarbeitung, aber das hat alles mit unserer Problematik nichts zu tun, das sind gr\"{o}{\ss}tenteils Entscheidungen, die zur Teilungsproblematik oder zu ganz anderen Fragen ergangen sind. Lassen Sie sich von daher nicht t\"{a}uschen.}

{\it Man kann wohl drei Perioden in der Rechtsprechung des BGH unterscheiden mit gewissen \"{U}bergangserscheinungen, also eine Art Pal\"{a}olithikum, Mesolithikum und Neolithikium, auf Computerprogramme \"{u}bertragen.}

{\it Die erste Periode im wesentlichen 1976 bis 1980, Dispositionsprogramm\footnote{\url{http://swpat.ffii.org/papiere/bgh-dispo76/index.de.html}} und Antiblockiersystem\footnote{\url{http://swpat.ffii.org/papiere/bgh-abs80/index.de.html}} als wichtigste, und man wird auch die Flugkostenminimierung\footnote{\url{http://wiki.ffii.org/BghFlug81De}} noch als sp\"{a}ten Nachl\"{a}ufer mit dazu rechnen m\"{u}ssen. Das war eine recht restriktive Periode: Nur f\"{u}r technische Programme, was auch immer das sein sollte, war Patentschutz m\"{o}glich.}

{\it Die zweite Periode war eine deutliche Periode des Umbruchs und des Wandels, ``Seitenpuffer'', Chinesische Schriftzeichen\footnote{\url{http://swpat.ffii.org/papiere/bgh-chinsz92/index.de.html}} ein speziell gelagerter Fall, und Tauchcomputer\footnote{\url{http://swpat.ffii.org/papiere/bgh-tauch93/index.de.html}} waren F\"{a}lle, bei denen die Weichen anders gestellt worden sind und es in eine andere Richtung gegangen ist, und wir sind jetzt in der dritten Periode, die vielleicht noch nicht so ganz die klare Konturen hat, aber ich meine, wenn man einige Monate wartet und sich das r\"{u}ckblickend anschaut, wird man auch sehen, dass es hier doch ganz erhebliche Umbr\"{u}che gegeben hat und gibt.}

{\it Ich m\"{o}chte noch auf eines hinweisen, das ist vielleicht bisher nicht so deutlich geworden: Alle Entscheidungen des X. Zivilsenats setzen bei der Technizit\"{a}t an und nicht beim Patentierungsausschluss f\"{u}r Programme als solche.  Es gibt keine Entscheidung des X. Senats, die dezidiert Stellung nimmt zu der Frage der Programme als solche. Das wird nur in einer Entscheidung des I. Zivilsenats, der f\"{u}r Urheberrecht zust\"{a}ndig ist, angesprochen.  Diese Entscheidung ist ein bisschen in Vergessenheit und aus dem Blickfeld geraten.}

{\it Es ist die Entscheidung Betriebssystem\footnote{\url{http://swpat.ffii.org/papiere/bgh1-bs90/index.de.html}} aus dem Oktober 1990, dort ging es um die urheberrechtliche Schutzf\"{a}higkeit einer Systemsoftware und dort wurde auch er\"{o}rtert (es ist wohl ein obiter dictum), ob diese Systemsoftware patentrechtlich schutzf\"{a}hig ist. Der I. Senat hat dazu folgendes gesagt:}

{\it \begin{quote}
Nach \S{} 1 Absatz 2 Nr. 3 und Absatz 3 PatG sind Programme f\"{u}r Datenverarbeitungsanlagen als solche nicht als Erfindung anzusehen, damit sind alle Computerprogramme nichttechnischer Natur vom Patentschutz ausgenommen.
\end{quote}}

{\it Dies gilt allerdings nicht f\"{u}r Programme technischer Natur, hierzu werden Dispositionsprogramm\footnote{\url{http://swpat.ffii.org/papiere/bgh-dispo76/index.de.html}} und Antiblockiersystem\footnote{\url{http://swpat.ffii.org/papiere/bgh-abs80/index.de.html}} zitiert. Betriebssysteme der vorliegenden Art, hei{\ss}t es weiter, die lediglich der Steuerung eines Computers und der mit ihm verbundenen Anschlussger\"{a}te dienen, stellen keine technischen Programme in diesem Sinne dar. Das wird mit zwei Entscheidungen des Bundespatentgerichts unterlegt. Das ist eine sehr weitgehende Position, aber sie ist irgendwie aus dem Blick geraten und auch in der Entscheidung Logikverifikation\footnote{\url{http://wiki.ffii.org/BghLogik00De}} nicht angesprochen. Vielleicht ist es ganz gut, dass sie nicht mehr im Blickfeld ist, aber wir stehen irgendwann eventuell doch vor dem Problem, m\"{u}ssen wir den I. Senat fragen, ob er an dieser Rechtsprechung festh\"{a}lt, m\"{u}ssen wir eventuell den Gro{\ss}en Senat anrufen? Nachdem sich aber unsere jetzigen F\"{a}lle im wesentlichen nicht auf der Schiene des Patentierungsausschlusses f\"{u}r Programme als solche abspielen, sondern auf der Ebene der Technizit\"{a}t, gehe ich im Moment noch davon aus, dass diese Frage wohl nicht virulent werden wird.}

{\it Ich komme jetzt zur aktuellen Rechtsprechung. Die ``Logikverifikation'' liegt Ihnen vor. Herr Freischem hat dazu auch schon einiges gesagt, auch den Ausgangsfall dargestellt, der auch ver\"{o}ffentlicht ist. Ich m\"{o}chte deshalb nichts zum Sachverhalt sagen, sondern nur die Entscheidung kurz vorstellen. Ich mache das mit denselben Worten wie vor 14 Tagen hier auf der GRUR-Veranstaltung, wer dort war, kann jetzt wegh\"{o}ren.}

{\it Der 17. Senat des Patentgerichts hatte hier die Technizit\"{a}t verneint, weil der Kern des Verfahrens in einer eine gedanklich-logische Anweisung darstellenden Regel zum Ordnen von Daten bestehe und das Verfahren keinen Hinweis auf einen neuen Aufbau oder eine neue Art der Benutzung der Datenverarbeitungsanlage enthalte. Der BGH hat demgegen\"{u}ber den technischen Charakter des Programms bejaht. Er hat zun\"{a}chst best\"{a}tigt, dass die Patentf\"{a}higkeit eines Programms f\"{u}r Datenverarbeitungsanlagen einen technischen Charakter des beanspruchten Gegenstands erfordert. Insoweit hat er auf seine bisherige Rechtsprechung, auf die Chinesischen Schriftzeichen\footnote{\url{http://wiki.ffii.org/BghChinsz91De}}, sowie auf die beiden neuen EPA-Entscheidungen ``Computerprogrammprodukt'' und auf Art. 27 TRIPS-\"{U}bereinkommen\footnote{\url{http://swpat.ffii.org/stidi/trips/index.de.html}} verwiesen.}

{\it Er hat weiter ausgef\"{u}hrt, dass die Kriterien der Entscheidungen Seitenpuffer\footnote{\url{http://wiki.ffii.org/BghSeitenp91De}} und Dispositionsprogramm\footnote{\url{http://swpat.ffii.org/papiere/bgh-dispo76/index.de.html}} f\"{u}r die Technizit\"{a}t nicht erf\"{u}llt sind, da es Ziel der Anmeldung gerade ist, die Verifikation mit Hilfe einer keine besonderen Anforderungen stellenden Datenverarbeitungsanlage zu erledigen. Unter Bezugnahme auf die Antiblockiersystem\footnote{\url{http://swpat.ffii.org/papiere/bgh-abs80/index.de.html}}- und die Tauchcomputer\footnote{\url{http://swpat.ffii.org/papiere/bgh-tauch93/index.de.html}}-Entscheidung hat er weiter darauf abgestellt, dass mit diesen M\"{o}glichkeiten kein abschlie{\ss}ender Katalog vorgegeben ist, sondern die Technizit\"{a}t einer programmbezogenen Patentanmeldung auf Grund einer Gesamtbetrachtung des Anmeldungsgegenstands im Einzelfall festzustellen ist, was eine Bewertung des im Patentanspruch definierten Gegenstands bedeutet und eine unterschiedliche Gewichtung einzelner Anspruchsmerkmale nicht ausschlie{\ss}t, wobei diese Bewertung aber von Neuheit und erfinderischer T\"{a}tigkeit nicht abh\"{a}ngt.}

{\it Da die angemeldete Lehre einen Zwischenschritt in dem Prozess betraf, der mit der Chipherstellung endete, indem mit ihrer Hilfe daf\"{u}r gesorgt wurde, dass die Bauteile aus verifizierten Schaltungen bestehen, war sie nach ihrer Zweckbestimmung Teil einer, so w\"{o}rtlich, ``aktuellen Technik''. Sie nutzte zwar ein gedankliches Konzept, beschr\"{a}nkte sich aber nicht darauf.}
\end{quote}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf BGH 1999 Logikverifikation\footnote{\url{http://swpat.ffii.org/papiere/bgh-logik99/index.de.html}}}}

\begin{quote}
Eine von zwei revolutiondren Entscheidungen, mit denen der BGH-Patentsenat den Begriff der Technischen Erfindung lockert und Weichen f|r eine grenzenlose Patentierbarkeit aller Ideen, die als ``programmtechnische Vorrichtungen'' beschrieben werden kvnnen, stellt.  U.a. hei\_t es: \begin{quote}
{\it Betrifft der Lvsungsvorschlag einen Zwischenschritt im Proze\_, der mit der Herstellung von (Silicium-}
\end{quote}Chips endet, so kann er vom Patentschutz nicht deshalb ausgenommen sein, weil er auf den unmittelbaren Einsatz von beherrschbaren Naturkrdften verzichtet und die Mvglichkeit der Fertigung tauglicher Erzeugnisse anderweitig durch auf technischen erlegungen beruhende Erkenntnisse voranzubringen sucht.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates\footnote{\url{http://swpat.ffii.org/akteure/bgh/index.de.html}}}}

\begin{quote}
Patent Senate of the German Federal Court
\end{quote}
\filbreak

\item
{\bf {\bf PA FREISCHEM\footnote{\url{http://swpat.ffii.org/akteure/freischem/index.en.html}}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
% mode: latex ;
% End: ;

