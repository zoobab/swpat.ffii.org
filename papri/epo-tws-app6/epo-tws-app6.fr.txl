<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: OEB 2000-05-19: Examen des Applications pour Méthodes d'Affaires

#etq: There will undoubtedly continue to be debate as to what constitutes a technical problem and what does not. This is exactly the same debate as we had under the %(q:technical contribution) scheme, we have merely transferred it to a different stage of the examination. The bonus is that we can still use the decisions of the BoA, whether or not they used the contribution approach, as guidance as to what is considered to be technical. It should be emphasised that, according to Sohei, the computer implementation of a, for example, business method, can involve %(q:technical considerations), and therefore be considered the solution of a technical problem, if implementation features are claimed.

#eiW: Examiners should however be familiar with the concept of %(q:further technical effect), since it may be employed by applicants or by parties in an opposition.

#srp: As to the lack of need to consider %(q:further technical effect), this assertion is based on the proposition that according to the scheme put forward no patent would be granted which should have been refused for lack of further technical effect. This is because the existence of an objective technical problem overcome is itself sufficient proof of the requisite further technical effect.  Further, it is to be remarked that this scheme of examination should not lead to refusals where previously a patent would have been granted, since the requirement for an objective technical problem is long-established. The only change is an explicit statement of the already implicit consequences of the lack of such a problem.

#sWW: This scheme makes no mention of the %(q:further technical effect) discussed in T1173/97. There is no need to consider this concept in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing %(q:technical effect) from %(q:further technical effect) in the decision was because of the presence of %(q:programs for computers) in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.

#WWe: The subject-matter of the claim is therefore to be examined for novelty and inventive step.  This is done according to the Guidelines for Examination as currently specified. In particular, in the examination for inventive step the objective technical problem solved by the invention as claimed considered as a whole when compared with the closest prior art is to be determined. If no such objective technical problem can be determined, the claim is to be rejected on the ground that its subject-matter lacks an inventive step.

#hdW: The claimed subject-matter, which by definition includes elements such as a computer or code which is intended to run on a computer, is presumed, prima facie, not to be excluded from patentability by Articles 52(2) and (3) EPC.

#WkW: The scheme for examining computer-implemented inventions is as follows:

#srs: The expression %(q:computer-implemented inventions) is intended to cover claims which specify computers, computer networks or other conventional programmable digital apparatus whereby prima facie the novel features of the claimed invention are realised by means of a new program or programs. Such claims may take the form of a method of operating said conventional apparatus, the apparatus set up to execute the method (loaded with the program), or, following T1173/97, the program itself. Insofar as the scheme for examination is concerned, no distinctions are made on the basis of the overall purpose of the invention, i.e. whether it is intended to fill a business niche, to provide some new entertainment, etc..

#cac: To (3): It is arguable that since the scheme for examining computer-implemented inventions is based on BoA decisions (in particular T1173/97) concerned with that particular field, another approach could or should be used for %(q:non-computer implementations), in particular the traditional approach of rejecting claims evincing no %(q:technical contribution to the art) under Article 52(2) EPC. However, this would lead to confusion and undoubtedly also to accusations of lack of consistency from applicants. A change in approach in the course of the examination of a case should particularly be avoided. As noted above, cases falling in this group are relatively rare, and it would seem unprofitable and inadvisable to introduce a special examination scheme for them.

#w2s: To (2): This is in line with the %(q:Sohei) decision T769/92 (OJ 1995, 525), in which the claim is for a data processing method used in a business context. It also enables us to have a coherent policy which is applicable to all the areas given in the list of Article 52(2) EPC (and the equivalent in PCT). Simply, as soon as a claim is for a computer implementation of an innovation which relates to any of those areas (e.g. games, aesthetic creations, presentations of information), it is to be examined according to the scheme for computer-implemented inventions.

#ssj: To (1): It would be possible to argue by analogy with the discussion of %(q:programs for computers) in T1173/97 (OJ 1999, 609) that a claim directed to an abstract business method itself is not necessarily for a business method %(q:as such). However, the reasoning in that decision was very special, and relied on the intimate relationship between program and an undeniably technical apparatus, the computer. Hence it was possible to argue that programs, even in abstract, can show a %(q:technical effect). No such reasoning would appear to be applicable to abstract business methods.

#Wng: The same approaches should be applied for PCT Chapter II, whereby (1) would lead to non-examination as to novelty, inventive step and industrial applicability according to Article 34(4)(a)(i) and Rule 67 PCT.

#ltr: Claims for other implementations of business methods should be treated using the same scheme for examination as for computer implementations.

#mee: Claims for computer-implemented business methods should be treated in exactly the same way as any other computer-implemented invention (see below).

#seo: Claims to abstract business methods should be rejected on the grounds that they are excluded by Articles 52(2) and (3) EPC, since they are methods of doing business %(q:as such).

#nnW: The following approaches to examination are to be applied in each of these cases:

#pfp: Claims for business methods can be divided into three groups: %(ol|claims for a method of doing business in abstract, i.e. not specifying any apparatus used in carrying out the method;|%(tp|claims which specify computers, computer networks or other conventional programmable digital apparatus for carrying out at least some of the steps of the business method|%(q:computer-implemented business methods));|claims which specify %(tp|other apparatus|perhaps in addition to computers) e.g. mobile telephones.) In the great majority of applications currently pending what is described would fall in the second of these groups. Thus while initial claims may sometimes fall in the first category, the applicant nearly always has the possibility to amend them to specify computer means for carrying out at least part of the method. Claims which fall in the third group are rare but by no means unheard of.

#oyi: Methods of doing business  are, according to Article 52(2) EPC, not to be considered to be inventions. Although not explicitly stated, this exclusion is also considered to apply to a wide range of subject-matters which, while not literally methods of doing business, share the same quality of being concerned more with interpersonal, societal and financial relationships, than with the stuff of engineering - thus for example, valuation of assets, advertising, teaching, choosing among candidates for a job, etc.. The term %(q:business methods) has become a generally used shorthand for all of these areas.

#aei: Examination of computer-implemented inventions

#aip: Examination of %(q:business method) applications

#moW: The EPO document which introduced the term %(q:computer-implemented invention).  This is Appendix 6 of a report in which the EPO explains to the US and Japanese Patent Office to what extent it has made progress in working around the European Patent Convention so as to make business methods patentable in Europe.  This document became the basis of the European Commission's software patentability directive proposal of 2002/02/20.

#0WW: OEB 2000-05-19: Examen des Applications pour Méthodes d'Affaires

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo-tws-app6 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

