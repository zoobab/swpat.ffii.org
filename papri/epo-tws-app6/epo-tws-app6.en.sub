\begin{subdocument}{epo-tws-app6}{EPO 2000/05/19: Examination of %(q:business method) applications}{http://swpat.ffii.org/papri/epo-tws-app6/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{The EPO document which introduced the term ``computer-implemented invention''.  This is Appendix 6 of a report in which the EPO explains to the US and Japanese Patent Office to what extent it has made progress in working around the European Patent Convention so as to make business methods patentable in Europe.  This document became the basis of the European Commission's software patentability directive proposal of 2002/02/20.}
\begin{description}
\item[title:]\ EPO 2000/05/19: Examination of %(q:business method) applications
\item[source:]\ \url{http://www.european-patent-office.org/tws/appendix6.pdf}
\end{description}

\begin{sect}{text}{The Text}
\begin{sect}{exam}{Examination of ``business method'' applications}
\begin{quote}
{\it Methods of doing business  are, according to Article 52(2) EPC, not to be considered to be inventions. Although not explicitly stated, this exclusion is also considered to apply to a wide range of subject-matters which, while not literally methods of doing business, share the same quality of being concerned more with interpersonal, societal and financial relationships, than with the stuff of engineering - thus for example, valuation of assets, advertising, teaching, choosing among candidates for a job, etc.. The term ``business methods'' has become a generally used shorthand for all of these areas.}

{\it Claims for business methods can be divided into three groups: \begin{enumerate}
\item
claims for a method of doing business in abstract, i.e. not specifying any apparatus used in carrying out the method;

\item
claims which specify computers, computer networks or other conventional programmable digital apparatus for carrying out at least some of the steps of the business method (``computer-implemented business methods'');

\item
claims which specify other apparatus (perhaps in addition to computers) e.g. mobile telephones.
\end{enumerate} In the great majority of applications currently pending what is described would fall in the second of these groups. Thus while initial claims may sometimes fall in the first category, the applicant nearly always has the possibility to amend them to specify computer means for carrying out at least part of the method. Claims which fall in the third group are rare but by no means unheard of.}

{\it The following approaches to examination are to be applied in each of these cases:
\begin{enumerate}
\item
Claims to abstract business methods should be rejected on the grounds that they are excluded by Articles 52(2) and (3) EPC, since they are methods of doing business ``as such''.

\item
Claims for computer-implemented business methods should be treated in exactly the same way as any other computer-implemented invention (see below).

\item
Claims for other implementations of business methods should be treated using the same scheme for examination as for computer implementations.
\end{enumerate}}

{\it The same approaches should be applied for PCT Chapter II, whereby (1) would lead to non-examination as to novelty, inventive step and industrial applicability according to Article 34(4)(a)(i) and Rule 67 PCT.}

{\it To (1): It would be possible to argue by analogy with the discussion of ``programs for computers'' in T1173/97 (OJ 1999, 609) that a claim directed to an abstract business method itself is not necessarily for a business method ``as such''. However, the reasoning in that decision was very special, and relied on the intimate relationship between program and an undeniably technical apparatus, the computer. Hence it was possible to argue that programs, even in abstract, can show a ``technical effect''. No such reasoning would appear to be applicable to abstract business methods.}

{\it To (2): This is in line with the ``Sohei'' decision T769/92 (OJ 1995, 525), in which the claim is for a data processing method used in a business context. It also enables us to have a coherent policy which is applicable to all the areas given in the list of Article 52(2) EPC (and the equivalent in PCT). Simply, as soon as a claim is for a computer implementation of an innovation which relates to any of those areas (e.g. games, aesthetic creations, presentations of information), it is to be examined according to the scheme for computer-implemented inventions.}

{\it To (3): It is arguable that since the scheme for examining computer-implemented inventions is based on BoA decisions (in particular T1173/97) concerned with that particular field, another approach could or should be used for ``non-computer implementations'', in particular the traditional approach of rejecting claims evincing no ``technical contribution to the art'' under Article 52(2) EPC. However, this would lead to confusion and undoubtedly also to accusations of lack of consistency from applicants. A change in approach in the course of the examination of a case should particularly be avoided. As noted above, cases falling in this group are relatively rare, and it would seem unprofitable and inadvisable to introduce a special examination scheme for them.}
\end{quote}
\end{sect}

\begin{sect}{kompinv}{Examination of computer-implemented inventions}
\begin{quote}
{\it The expression ``computer-implemented inventions'' is intended to cover claims which specify computers, computer networks or other conventional programmable digital apparatus whereby prima facie the novel features of the claimed invention are realised by means of a new program or programs. Such claims may take the form of a method of operating said conventional apparatus, the apparatus set up to execute the method (loaded with the program), or, following T1173/97, the program itself. Insofar as the scheme for examination is concerned, no distinctions are made on the basis of the overall purpose of the invention, i.e. whether it is intended to fill a business niche, to provide some new entertainment, etc..}

{\it The scheme for examining computer-implemented inventions is as follows:
\begin{enumerate}
\item
The claimed subject-matter, which by definition includes elements such as a computer or code which is intended to run on a computer, is presumed, prima facie, not to be excluded from patentability by Articles 52(2) and (3) EPC.

\item
The subject-matter of the claim is therefore to be examined for novelty and inventive step.  This is done according to the Guidelines for Examination as currently specified. In particular, in the examination for inventive step the objective technical problem solved by the invention as claimed considered as a whole when compared with the closest prior art is to be determined. If no such objective technical problem can be determined, the claim is to be rejected on the ground that its subject-matter lacks an inventive step.
\end{enumerate}}
\end{quote}

\begin{quote}
{\it \begin{enumerate}
\item
This scheme makes no mention of the ``further technical effect'' discussed in T1173/97. There is no need to consider this concept in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing ``technical effect'' from ``further technical effect'' in the decision was because of the presence of ``programs for computers'' in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.

As to the lack of need to consider ``further technical effect'', this assertion is based on the proposition that according to the scheme put forward no patent would be granted which should have been refused for lack of further technical effect. This is because the existence of an objective technical problem overcome is itself sufficient proof of the requisite further technical effect.  Further, it is to be remarked that this scheme of examination should not lead to refusals where previously a patent would have been granted, since the requirement for an objective technical problem is long-established. The only change is an explicit statement of the already implicit consequences of the lack of such a problem.

Examiners should however be familiar with the concept of ``further technical effect'', since it may be employed by applicants or by parties in an opposition.

\item
There will undoubtedly continue to be debate as to what constitutes a technical problem and what does not. This is exactly the same debate as we had under the ``technical contribution'' scheme, we have merely transferred it to a different stage of the examination. The bonus is that we can still use the decisions of the BoA, whether or not they used the contribution approach, as guidance as to what is considered to be technical. It should be emphasised that, according to Sohei, the computer implementation of a, for example, business method, can involve ``technical considerations'', and therefore be considered the solution of a technical problem, if implementation features are claimed.
\end{enumerate}}
\end{quote}
\end{sect}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Wüsthoff 2001: Strategic Advice to Banks on Business Method Patents in Europe\footnote{\url{http://localhost/swpat/papri/wuesthoff-bm01/index.en.html}}}}

\begin{quote}
A prestigious german patent law firm explains that US business method patents can in most cases be europeanised without much difficulty, because the ``technical contribution'' requirement does not present a real obstacle.  They estimate the costs of researching which business method patents a bank may be infringing to around 2 manyears of work by professionals in the fields of patent law and data processing, plus costs for licensing negotiations and buildup of a defensive patent portfolio.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.en.html}}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf CEC & BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{\url{http://localhost/swpat/papri/eubsa-swpat0202/index.en.html}}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{\url{}}}}

\begin{quote}
The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. operation of computer with ``storage means'', ``output means'' etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations explained in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Logic Patents in Europe\footnote{\url{http://localhost/swpat/index.en.html}}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contr= ary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

