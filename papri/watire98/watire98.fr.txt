<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Michael Waterson et Norman Ireland, économistes de l'Université de Warwick, construisent un modèle de jeu paramétrable pour simuler le jeu d'innovation sous un régime de brevets pharmaceutiques, de protection de la diversité des plantes, de brevets logiciels et de copyright logiciel aussi bien que de nombreuses autres situations.  Le modèle contient quelques simplifications qui jouent en faveur des brevets.  Par exemple, il ne considère pas les pertes d'allocations de sécurité sociale basées sur les monopoles, qui sont au centre de nombreuses analyses économiques du système de brevet.  A la place, son système allocations de sécurité sociale est simplement l'aggrégation du potentiel de biens de consommation courantes espérés par les joueurs.  Egalement, il ne considère pas le besoin de modularité et d'interopérabilité dans le monde logiciel.  Cependant, le modèle dépeint nombre de phénomènes observable tout à fait correctement, et il conduit à la conclusion que les brevets logiciels ont un effet négatif sur l'innovation alors que les brevets pharmaceutiques et les copyright logiciel ont un effet positif.

#title: Waterson et Ireland: Un Modèle d'Enchères de la Protection de la Propriété Intellectuelle: Brevet contre Copyright

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: watire98 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

