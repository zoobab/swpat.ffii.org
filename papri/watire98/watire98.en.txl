<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Waterson & Ireland: An Auction Model of Intellectual Property
Protection: Patent vs Copyright

#descr: Michael Waterson and Norman Ireland, economists from the University of
Warwick, constructs a parametrised game model to simulate the
innovation game under a regime of pharma patents, plant variety
protection, software patents and software copyright as well as many
other situations.  The model contains some simplifications that work
in favor of patents.  E.g. it does not consider monopoly-based welfare
losses, which are at the center of many economic analyses of the
patent system.  Instead its social welfare is simply the aggretation
of the potential players expected utilities.  Also it does not
consider the need for modularity and interoperability in the software
world.  Yet, the model depicts many observable phenomena quite well,
and it leads to the conclusion that software patents have a negative
effect on innovation while pharma patents and software copyright has a
positive effect.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: watire98 ;
# txtlang: en ;
# multlin: t ;
# End: ;

