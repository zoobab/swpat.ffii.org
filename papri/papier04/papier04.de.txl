<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hans-Jürgen Papier über die EU-Verfassung

#descr: Der Vorsitzende Richter des deutschen Bundesverfassungsgerichts hat
eine Reihe von Artikeln und Aussagen in Zeitungen veröffentlicht, in
denen er vor schweren Fehlern in der geplanten EU-Verfassung warnt. 
Für besonders bedenklich hält Papier die fehlende Gewaltenteilung
zwischen Legislative und Exekutive, die unkontrollierte Macht des
Rates und der Kommission, die unklaren Begrenzungen der Kompetenz der
EU gegenüber den Nationalstaaten und das Fehlen einer breiten Debatte
über die geplante Verfassung.  Papier kritisiert ferner das deutsche
System der Länderkammer (Bundesrat), in der regionale Exekutivorgane
de facto legislative Gewalt ausüben, was sowohl der Effizienz als auch
der Legitimität der Gesetzgebung schade und ein schlechtes Modell für
die EU sei.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: papier04 ;
# txtlang: de ;
# multlin: t ;
# End: ;

