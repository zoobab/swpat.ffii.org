<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hans-Jürgen Papier on the EU Constitution

#descr: The presiding judge of Germany's Federal Constitutional Court has
published a series of articles and statements in newspapers in which
he warns about serious deficiencies of the planned European
Constitution, especially a lack of a division of executive and
legislative power, the uncontrolled power of the Council and
Commission, the unclear limitations on the competence of the EU vs the
national states and the lack of a broad debate about the EU
constitution.  Papier moreover argues that the German system of a
second legislative chamber which consists of regional governments is a
bad model, which has made Germany's legislative process both
undemocratic and inefficient.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: papier04 ;
# txtlang: en ;
# multlin: t ;
# End: ;

