<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: IPI 2000: The Economic Impact of Patentability of Computer Programs

#descr: A patent advocacy text by the London Intellectual Property Institute,
ordered by the Industrial Property Unit at the European Commission
(CEC Indprop), finished in spring 2000, held back until Oct 2000.  The
name is misleading:  this is not an economic study.  There is only one
chapter which deals with economics but even this chapter only roughly
summarises third parties's works.   Basically this pseudo-study only
restates well-known beliefs of civil servants from the british patent
establishment who at the time were in charge of the European
Commission's Industrial Property Unit at DG Markt (Directorate General
for the Internal Market).  Yet, while main author Robert Hart is a
well known patent lawyer and lobbyist, the economics chapter was
written by an outsider, Peter Holmes.  It provides evidence to show
that software patents have damaging effects on economic development
and tries to balance this by adding some unreasoned statements in
favor of software patents.  Holmes later explained that he had no
other choice in view of the %(q:convictions) of his partners.  Yet the
CEC Indrop people did not like the study: they locked it away for half
a year.  During this time the European patent establishment was
preparing to rewrite Art 52 EPC so as to remove all limits of
patentability.  In October 2000, after the plans been dropped,
CEC/Indprop suddenly published the IPI treatise and used it as a basis
for a %(q:consultation exercise).  From then on, various pro software
patent proposals from Brussels have again and again relied on this
%(q:economic study) for justification.

#csi: Unreasoned Conclusions for Political Use

#nue: Evidence vs Conclusions of the Study

#cei: Economic Literature suggests strong Negative Effects

#oii: History of the Study

#WiW: In her proposal for a somewhat amended Software Patentability
Directive, Arlene McCarthy writes

#rhe: Moreover, a study conducted by the Intellectual Property Institute in
London has found that %(q:the patentability of computer-related
inventions has helped the growth of computer program-related
industries in the US, in particular the growth of small and medium
enterprises and independent software developers into sizeable indeed
major companies).

#rWh: The facts and arguments listed by the %(IPI) do not permit such a
conclusion.  Rather, they could be correctly summarised as follows:

#WWi: Individual (software) comanies can benefit from software patents. 
There is no evidence that this benefit for individual market
participants corresponds to a benefit for the economy or society as a
whole.  Many economists have doubts about such a benefit.  These
doubts are supported by continuing and growing concern about software
patents in the USA.

#sWf: The %(ip:IPI treatise) does contain a statement which resembles that
of McCarthy on page 5 (pg 7 in the pdf file):

#Wzt: Let us first summarize:

#Wdr: McCarthy quotes only the claimed advantage %(q:1.) of software
patentability, whereas the IPI treatise itself lists both an
advantages (%(1.)) and three disadvantages (%(q:2.1-2.3)).

#adi: The IPI treatise fails to cite any evidence in favor of the claimed
advantage (%(q:1)) of software patentability, although it contains 10
pages of bibliography.

#got: It remains unclear in what way the claimed %(q:growth of some SMEs
into large companies) can, even if examples for it are found, be an
argument in favor of software patentability.  Some SMEs always grow,
some always die, no matter what the conditions of the market game are.
 Should the fact that Philips, Ciba-Geigy, SAP or Microsoft grew big
in patent-free spaces be counted as an argument against the patent
system?

#eFe: On page 8 (pg 10 of the PDF file) the IPI treatise then writes

#h3t: This is apparently a reference to page 32 (pg 34 of PDF file) of the
treatise:

#WoW: The economics literature does not show that the balance of positive
and negative effects lies with the negative. All it says is that there
are grounds for supposing that the negative forces are stronger
relative to the positive forces in this area than in some others and
that any move to strengthen IP protection in the software industry
cannot claim to rest on solid economic evidence.

#aWt: It should be noted that the economic literature does not show that the
balance of positive and negative effects lies within the positive,
either for software or for technical inventions in the traditional
sense.  Leading economists suspect that the patent system as a whole
does more to stifle than to stimulate economic and technological
development.  Canadian and Australian governmental reports of the
1970s have proposed to abolish the patent system for that reason or at
least prevent its expansion into further fields such as software,
where the balance is particularly negative.

#nwd: The author of the economic chapter of the treatise, Peter Holmes,
explained at a conference in june 2002 in reply to irritated questions
of fellow economists approximately as follows: %(q:In that study I had
to find some macro-economic arguments in favor of software patents. 
My two co-authors were patent lawyers and it was their conviction that
such arguments exist.  For them it is a question of conviction. 
Without arguments in favor of software patents, we couldn't have
finished the study).  The leading author was in fact %(RH), a deeply
committed advocate of software patentability, whose %(cs:argumentation
tactics) are well documented.  It is also clear that European
Commission's Industrial Property Unit ordered this study from Hart
because its leading british patent lawyers shared Hart's convictions. 
The IndProp patent lawyers kept the IPI treatise unpublished for 6
months, because upon closer look it failed to achieve the aim of
providing an economic foundation for their pro-patent convictions. 
Yet they and McCarthy never grow tired of quoting fragments from it. 
This is because no other of numerous real %(es:economic studies)
provides a iota of evidence to support their pro-patent convictions.

#WWs: The name %(q:economic study) was most likely added in summer 2000 in
reaction to the very popular Eurolinux Petition, in which about 30000
people had signed a statement saying %(q:I am surprised that no
economic report has ever been published by European Authorities to
study the impact of software patents on innovation and competition.). 
Relabelling a legal study was a cheap way of satisfying this popular
demand, so as to press ahead with legalising software patents more
quickly.

#tWi: Insiders say that CEC/IndProp paid 5000 pounds to IPI for this
advocacy text.

#atr: A legal treatise written at the order of the European Commission's
Industrial Property Unit by a british pro patent lobby group, finished
in March 2000, held back until Oct 2000.

#Won: Introductory words about the IPI study from the Industrial Property
Unit which built its %(q:Consultation Exercise) on this study.

#0ze: Lenz 2002-03-01: Grenzen der Patentierbarkeit

#jWe: German book by Dr. Karl-Friedrich Lenz, professor (kyôju) of European
Law, which explains many aspects of the current debate on
patentability, mostly in German, and also contains some english texts,
including a chapter on the CEC/BSA directive proposal of 2002-02-20.

#rWl: The Register 2002-10-29: Patent Lobby get in early for EU consultation
process

#mtI: Graham Lea comments on the IPI treatise, points out that it is not an
economic study but a patent advocacy text.

#aWr2: Thailand to follow EU on software patents

#eWt: This software patent advocacy text from Thai patent lawyer shows
impressively that even when there is no %(q:as such) provision to play
with, patent lawyers find ways to declare the %(q:computer programs)
exclusion in their law meaningless.  The texts quotes the usual
isolated sentences from the IPI %(q:economic study) as well as global
necessities, advantages of patents for SMEs and small programmers etc
as reasons for Thailand to change its patent law and allow software
patents.  However the author argues that Thailand should await the
final european decision before doing so.

#aaW: same author, same argumentation tactics

#ton: quotes the IPI treatise to support its patent advocacy.

#tWr: A Thai patent lawyer cites the usual fragments from the %(q:economic
study) in order to call for a removal of the exclusion of software
from patentability in Thailand.

#aWr: The British patent family was dominant at DG Internal Market when the
IPI treatise was ordered.

#lpa: A chapter in the mainly german book %(q:Grenzen des Patentwesens)
explains why the IPI study does not offers a single arguments in
support of the European Commission's proposal to allow software
patents.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: indprop-ipi00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

