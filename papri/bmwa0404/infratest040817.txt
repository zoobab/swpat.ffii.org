To: sabine.graumann@tns-infratest.com, florian.neinert@tns-infratest.com
Cc: gl@esr-pollmeier.de, nico.erichsen@gmx.de, ccorn@ffii.org
Subject: Softwarepatent-Empfehlung an BMWA in Infratest-"Faktenbericht"
--text follows this line--
Sehr geehrte Frau Graumann,
Sehr geehrter Herr Neinert,

Wie in

    http://swpat.ffii.org/papiere/bmwa0404/index.de.html

dargelegt, geben Sie in einem Bericht an die Bundesregierung vom April 2004
eine politisch kontroverse Empfehlung ab, ohne diese zu begründen und ohne
auf die zu diesem Themenkomplex vorliegende umfangreiche Forschungsliteratur
einzugehen.

Könnten Sie mir bitte erklären, ob ich bei meiner Analyse etwas übersehen
habe?

-- 
Hartmut Pilch, FFII e.V. und Eurolinux-Allianz            +49-89-18979927
350.000 Stimmen 3000 Bosse gegen Logikpatente       http://noepatents.org/
Innovation statt Patentinflation                    http://swpat.ffii.org/
