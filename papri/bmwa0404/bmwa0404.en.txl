<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-04: BMWA-Auftragsstudie fordert Ermöglichung von Softwarepatenten

#descr: Eine vom BMWA an TNG Infratest in Auftrag gegebene Studie empfiehlt
der Bundesregierung, sie solle %(q:Patentierbarkeit
Computerimplementierter Erfindungen ermöglichen).  Bei näherem
Hinsehen zeigt sich, dass die Studie keinerlei Versuch unternimmt,
diese Empfehlung zu begründen.

#ule: Quellen

#pre: Die Empfehlung und ihre (fehlende) Begründung

#Wnu: BMWA-Einführung

#ifS: Bericht auf BMWA-Server

#pbF: Kopie bei FFII

#xii: Textversion bei FFII

#WWM: Es handelt sich um eine Studie im Rahmen einer Serie von
%(q:Faktenberichten), die im April 2004 von der Münchener
Marktforschungsagentur TNS Infratest GmbH & Co KG im Auftrag des BMWA
ausgeführt wurde.

#Wwn: Als Autoren werden genannt:

#IrW: S. XXIV (S. 56 der PDF-Datei) enthält

#7eb: 1.1.7 Politische Handlungsbedarfe

#bmn: Patentierbarkeit computerimplementierter Erfindungen ermöglichen

#esP: Das Wort %(ki:computer-implementierte Erfindungen) wird weithin als
Euphemismus für %(q:Software im Kontext von Patentansprüchen)
verwendet.  Im aktuellen politischen Kontext kommt der Satz einer
Parteinahme für die Politik der Bundesregierung im EU-Rat und gegen
die %(ep:Entscheidung des Europäischen Parlaments vom 24. September
2003) gleich.

#deq: Woraus die Studie diese Empfehlung ableitet, ist nicht zu erkennen. 
Es steht zwar im Vorspann zu den %(q:Empfehlungen) zu lesen:

#eoT: Der Überblick kann die Lektüre der differenzierteren Erörterungen der
Politikbereiche in den Hauptteilen des Fakten- und Trendberichtes
nicht ersetzen.

#toe: Doch in eben jenen Hauptteilen findet sich das Stichwort %(q:Patent)
nirgendwo, und auch im bibliographischen Anhang wird nicht auf
einschlägige Literatur verwiesen.

#tvu: Auch bei dieser Auftragsstudie des selben Ministeriums werden
Empfehlungen für Softwarepatente ausgesprochen, die sich aus den von
der Studie ermittelten Fakten schwer begründen lassen und unzureichend
begründet werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bmwa0404 ;
# txtlang: en ;
# multlin: t ;
# End: ;

