<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/swpat.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2003/11/19">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="title" content="&quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot;">
<meta name="description" content="The Federal Patent Court (FPC/BPatG) had rejected the claims to an anti blocking system, because they pertained to an organisational or computational rule that solved an abstract problem within a known model, without any necessity of recourse to experimenting with natural forces.  The Federal Court of Justice (FCJ/BGH) now overruled this rejection and upheld the patent, saying that it was enough that the claims were directed to a braking process whose causality is not determined by logics but dependent on forces of nature.  Simultaneously the FCJ legalised function claims.  The commentator, a patent attorney, hails this as groundbreaking good news that will send a sigh of relief through the rounds of patent lawyers, who had been struck by sorrow and anguish after reading the earlier FPC verdict, which seemed like a &quot;witch hunt against all things non-technical&quot;.">
<title>BGH 1980 ABS</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<div id="toprel">
<a href="index.de.html" charset=utf-8><img src="/img/flags/de.png" alt="[DE Deutsch]" border="1" height=25></a>
<a href="bgh-abs80.en.txl"><img src="/img/icons.other/txt.png" alt="[translatable text]" border="1" height=25></a>
<a href="/group/todo/index.en.html"><img src="/img/icons.other/questionmark.png" alt="[howto help]" border="1" height=25></a>
<a href="http://kwiki.ffii.org/BghAbs80En"><img src="/img/icons.other/groupedit-en.png" alt="[Readers' Comments]" border="0"></a>
</div>
<div id="toparb">
<table width="100%">
<tr class=doklvl3><th><a href="/papers/bgh-walzst80/index.en.html">BGH 1980 Walzst&auml;be</a></th><td class=eqdok>BGH 1980 ABS</td><td>BGH 1977 Straken</td><td>BGH 1977 Pr&uuml;fverfahren</td><td>BGH 1978 Fehlerortung</td><td>BGH 1984 Acrylfasern</td></tr>
</table>
</div>
<div id="doktit">
<h1><a name="top">&quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot;</a></h1>
</div>
<div id="dokdes">
The Federal Patent Court (FPC/BPatG) had rejected the claims to an anti blocking system, because they pertained to an organisational or computational rule that solved an abstract problem within a known model, without any necessity of recourse to experimenting with natural forces.  The Federal Court of Justice (FCJ/BGH) now overruled this rejection and upheld the patent, saying that it was enough that the claims were directed to a braking process whose causality is not determined by logics but dependent on forces of nature.  Simultaneously the FCJ legalised function claims.  The commentator, a patent attorney, hails this as groundbreaking good news that will send a sigh of relief through the rounds of patent lawyers, who had been struck by sorrow and anguish after reading the earlier FPC verdict, which seemed like a &quot;witch hunt against all things non-technical&quot;.
</div>
</div>
<div id="dokmid">
<div id="papri">
<dl><dt>title:</dt>
<dd>BGH &quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot;</dd>
<dt>source:</dt>
<dd><a href="/papers/index.en.html#GRUR">GRUR</a> GRUR 1980.9</dd></dl>
</div>
<dl><dt>Thema:</dt>
<dd>PatG &sect;1. - &quot;Antiblockiersystem&quot;<p>Beschluss des Bundesgerichtshofs vom 13. Mai 1980</dd>
<dt>Aktz.:  X Z B 19/78 (BPatG):</dt>
<dd>X Z B 19/78 (BPatG)</dd>
<dt>Amtlicher  Leitsatz:</dt>
<dd>On the question of Technical Invention</dd></dl>
<div class="sectmenu">
<ol type="1"><li><a href="#bgh">BGH nimmt Technizit&auml;ts-Grenzfall als Anlass, entgegen der Sichtweise des BPatG Wirkungsanspr&uuml;che anzuerkennen und &quot;technische Programme&quot; f&uuml;r patentierbar zu erkl&auml;ren</a></li>
<li><a href="#eisenf">Aufatmen und Beifall der Patentjuristen</a></li>
<li><a href="#links">Annotated Links</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h2>1. <a name="bgh">BGH nimmt Technizit&auml;ts-Grenzfall als Anlass, entgegen der Sichtweise des BPatG Wirkungsanspr&uuml;che anzuerkennen und &quot;technische Programme&quot; f&uuml;r patentierbar zu erkl&auml;ren</a></h2>
</div>

<div class="sectbody">
<div class="cit">
<div class="sectmenu">
<ol type="1"><li><a href="#anspr">[Anspr&uuml;che]</a></li>
<li><a href="#aufheb">II.  Die Rechtsbeschwerde f&uuml;hrt zur Aufhebung des angefochtenen Beschlusses und zur Zur&uuml;ckweisung der Sache an das BPatG.</a></li>
<li><a href="#nvers">III.  Die vom  BPatG gegebene Begr&uuml;ndung tr&auml;gt die Versagung des Patents deshalb nicht.</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h3>1.1. <a name="anspr">[Anspr&uuml;che]</a></h3>
</div>

<div class="sectbody">
I. Auf die  am 3. August 1967  eingereichte und am 4. M&auml;rz 1971 bekanntgemachte Patentanmeldung f&uuml;r ein Antiblockierregelsystem f&uuml;r druckmittelbet&auml;tigte Fahrzeugbremsen  mit einem  Einlass-  und  einem Auslassventil hat das DPA mit Beschluss der Patentabteilung 21  vom 7. Mai 1975  das Patent 1 655 432 erteilt.  Der Patentanspruch 1 lautet:<p><blockquote>
Antiblockierregelsystem f&uuml;r druckmittelbet&auml;tigte  Fahrzeugbremsen  mit  einer  Drehverz&ouml;gerungs-Schaltvorrichtung,  die in Abh&auml;ngigkeit vom Drehverhalten  des &uuml;ber-wachten  Rades elektrische  Signale  erzeugt  und  ein  Ein- und Auslassventil bet&auml;tigt,  wobei  - wenn  kein  Signalvorhanden ist - das Einlassventil ge&ouml;ffnet und das Auslassventil  geschlossen  ist,  so  dass  der Bremsdruck  ansteigenkann, wobei weiterhin w&auml;hrend des Auftretens eines bestimmten  Verz&ouml;gerungssignals  das Einlassventil  schliesst und das Auslassventil &ouml;ffnet,  so  dass  der Bremsdruck f&auml;llt, und wobei  schliesslich  nach  der Druckabsenkung unterVerwendung einer Drehbeschleunigungs-Schaltvorrichtung ein Signal erzeugt wird, das bei wieder geschlossenem Auslassventil das Eingangsventil geschlossen h&auml;lt, so  dass der Druck bis zum Unterschreiten eines Beschleunigungswertes  im wesentlichen konstant gehalten wird,  dadurch  gekennzeichnet,  dass  in bekannter  Weise eine bistabile Schaltvorrichtung (6, 52, 63) vorgesehen ist und dass diese bistabile Schaltvorrichtung (6,52,63) mit der Drehverz&ouml;gerungs-Schaltvorrichtung (V;-, V'-Geber) und mit der Drehbeschleunigungs-Schaltvorrichtung (B-B*-Geber) derart verbunden ist, dass sie bei Auftreten des Verz&ouml;gerungssignals (V'. V;*) in einen Schaltzustand gelangt, in der sie &uuml;ber die Ventile (E,A) eine Druckabsenkung bewirkt und dass sie bei Auftreten eines Beschleunigungssignals (B,B*) in eine Schaltstellung r&uuml;ckgekippt wird, in der sie keinen Einfluss auf die Ventile (E,A) aus&uuml;bt, so dass nunmehr allein durch das Beschleunigungssignal die Druckkonstanthaltung bewirkt wird.
</blockquote><p>Die Patentanspr&uuml;che 2 bis 8 betreffen besondere Ausgestaltungen der bistabilen Schaltvorrichtung.<p>Auf die Beschwerde der Einsprechenden hat das BPatG den Erteilungsbeschluss aufgehoben und das Patent versagt, weil die Lehre des Patents nicht technischer Natur und daher dem Patentschutz nicht zug&auml;nglich sei (vgl GRUR 1979, 111ff).<p>Hiergegen wendet sich die Anmelderin mit der zugelassenen Rechtsbeschwerde.  Sie beantragt, den angefochtenen Beschluss aufzuheben und die Sache an das BPatG zur&uuml;ckzuverweisen.<p>Die Einsprechende beantragt die Zur&uuml;ckweisung der Rechtsbeschwerde.
</div>

<div class="secthead">
<h3>1.2. <a name="aufheb">II.  Die Rechtsbeschwerde f&uuml;hrt zur Aufhebung des angefochtenen Beschlusses und zur Zur&uuml;ckweisung der Sache an das BPatG.</a></h3>
</div>

<div class="sectbody">
<div class="sectmenu">
<ol type="1"><li><a href="#gegenst">1. Das BPatG hat es vers&auml;umt zuuntersuchen, f&uuml;r welchen im Patentanspruch umschriebenen Gegenstand mit der Anmeldung Schutz beansprucht wird.</a></li>
<li><a href="#technab">2. Dieser Lehre kann der technische Charakter nicht abgesprochen werden.</a></li>
<li><a href="#techlehr">3. Der vorliegende Anmeldungsgegenstand erf&uuml;llt die Anforderungen  einer  Lehre zum technischen Handeln.</a></li>
<li><a href="#techprog">4. Auch wenn  das beanspruchte &quot;Antiblockierregelsystem&quot; als ein Verfahren oder ein Programm charakterisiert wird, ergibt  sich daraus f&uuml;r  die  Frage  nach  dem technischen  Gehalt des Anmeldungsgegenstandes nichts Entscheidendes.</a></li>
<li><a href="#wirkanspr">5. Es  steht  dem  technischen Charakter  des  Anmeldungsge-genstandes auch nicht entgegen,  dass  der Patentanspruch 1 die Schaltvorrichtungen  nicht  n&auml;her  bezeichnet, es vielmehr  dem Fachmann &uuml;berl&auml;sst,  sich f&uuml;r  die  bistabilen, f&uuml;r die  Drehverz&ouml;gerungs- und f&uuml;r die Drehbeschleunigungs-Schaltvorrichtungen &uuml;blicher Schaltungselemente zu bedienen.</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h4>1.2.1. <a name="gegenst">1. Das BPatG hat es vers&auml;umt zuuntersuchen, f&uuml;r welchen im Patentanspruch umschriebenen Gegenstand mit der Anmeldung Schutz beansprucht wird.</a></h4>
</div>

<div class="sectbody">
<ol><li>(a) Der  in  der Anmeldung beanspruchten Erfindung liegt  die &quot;Aufgabe&quot; zugrunde,  bei einem Antiblockierregelsystem,  mit dessen Hilfe  beim  Bremsen  der einzelnen Fahrzeugr&auml;derwegen der bekannten nachteiligen  Folgen  deren  Blockieren (Gleiten) verhindert werden soll,  die  Beendigung  der Druckabsenkung (besser)  den Fahrbahnverh&auml;ltnissen anzupassen  (vgl.  Sp.  2Z. 47-52  der Auslegeschrift).</li>
<li>(b) Zur <strong>L&ouml;sung</strong> dieser Aufgabe schl&auml;gt die Anmelderin ein System  mechanischer, elektrischer  oder elektronischer Art  vor mit bistabilen Schaltvorrichtungen (Sp. 4 Z. 62-68), mit Drehverz&ouml;gerungs- und Drehbeschleunigungs-Schaltvorrichtungen, bei  dem  bestimmte  Signale  gegeben werden und  die einzelnen Elemente  so miteinander verbunden (d.h. geschaltet) sind, dass sie  beim Auftreten eines Verz&ouml;gerungssignals  in einen Schaltzustand geraten, in dem sie - &uuml;ber die Steuerung jeweils eines Einlass- und  eines Auslassventils  - eine Absenkung des  Bremsdrucks bewirken, und erst beim Auftreten eines Beschleunigungssignals eine andere Schaltstellung einnehmen, in der der Bremsdruck konstant gehalten wird.</li>
<li>(c)  <strong>Gegenstand  der  beanspruchten  Erfindung</strong> ist danach ein Antiblockierregelsystem f&uuml;r druckmittelbet&auml;tigte Fahrzeugbremsen mit
<ol><li>Einlass-  und Auslassventilen,</li>
<li>bistabilen Schaltvorrichtungen,</li>
<li>Drehverz&ouml;gerungs-Schaltvorrichtungen und</li>
<li>Drehbeschleunigungs-Schaltvorrichtungen.</li></ol></li>
<li>Die Schaltvorrichtungen (2) bis (4) sind mit den Ventilen (l) derart miteinander verbunden (geschaltet),  dass nach Druckanstieg, d.h. bei ge&ouml;ffneten Einlass- und geschlossenen Auslassventilen (a) bei einem bestimmten Verz&ouml;gerungssignal die Eingangsventile  geschlossen und  die  Ausgangsventile ge&ouml;ffnet  werden,  so  dass  der  Bremsdruck absinkt, und (b) bei einem bestimmten Beschleunigungssignal die  bistabilen Schaltvorrichtungen  in eine Schaltstellung r&uuml;ckgekippt  werden,  in  der  sie keinen Einfluss  auf  die Ventile haben, und dadurch bei geschlossen gehaltenenEinlassventilen  der Bremsdruck konstant gehalten wird.</li></ol>
</div>

<div class="secthead">
<h4>1.2.2. <a name="technab">2. Dieser Lehre kann der technische Charakter nicht abgesprochen werden.</a></h4>
</div>

<div class="sectbody">
Die Abgrenzung patentf&auml;higer Erfindungen von solchen, diedem Patentschutz nicht zug&auml;nglich sind, hat der  Senat  in der Entscheidung &quot;Rote Taube&quot; (BGHZ 52, 74 ff.) r vorgenommen und eine Lehre zum technischen Handeln in einer Anweisung zum planm&auml;ssigen Handeln unter Einsatz beherrschbarer  Na-turkr&auml;fte zur Erreichung  eines kausal &uuml;bersehbaren Erfolges gesehen.  Dies ist in der Entscheidung &quot;Dispositionsprogramm&quot; (BGHZ 67,22 ff.) 2' best&auml;tigt und dahin klargestellt worden, dassdie  mCTischliche Verstandest&auml;tigkeit selbst nicht zu  den beherrschbaren Naturkr&auml;ften geh&ouml;rt, sondern dazu nur  die mitHilfe  der menschlichen Verstandest&auml;tigkeit beherrschbaren Naturkr&auml;fte z&auml;hlen. Schliesslich hat  der  Senat  in der Entschei-dung &quot;Kennungsscheibe&quot; (GRUR  1977,  152,  153) auf ein unmittelbar  in Erscheinung tretendes Ergebnis abgestellt,  das ohne Zwischenschaltung der menschlichen Verstandest&auml;tigkeit erreicht wird.
</div>

<div class="secthead">
<h4>1.2.3. <a name="techlehr">3. Der vorliegende Anmeldungsgegenstand erf&uuml;llt die Anforderungen  einer  Lehre zum technischen Handeln.</a></h4>
</div>

<div class="sectbody">
<ol><li>a)  Dass die  in  der Patentanmeldung enthaltene Lehre eine Anweisung zum planm&auml;ssigen Handeln darstellt, wird zu Recht nicht ernstlich  in Zweifel gezogen.</li>
<li>b)  Dass  dieses Handeln auch unter Einsatz  berechen- undbeherrschbarer Naturkr&auml;fte erfolgt, ergibt  sich daraus, dass  bei dem Anmeldungsgegenstand  ein von der Wiederbeschleunigung des &uuml;berwachten Rades ausgel&ouml;stes Signal dazu benutzt wird,  den &Uuml;bergang  vom Absenken zum Konstanthalten des Bremsdrucks durch Schlie&szlig;en des Auslassventils zu vollziehen.<p>Mit Hilfe aller im Patentanspruch genannten Schaltvorrichtungen wird unter Heranziehung  der Wiederbeschleunigung  des &uuml;berwachten  Rades die Druckabsenkung  beendet und dadurchdas Bremsverhalten beeinflusst.  Dies  geschieht  somit unter planm&auml;ssiger Ausnutzung von auf Naturkr&auml;ften beruhenden Naturerscheinungen.<p>Darin unterscheidet  sich  der vorliegende Sachverhalt von dem der Entscheidung Dispositionsprogramm.  Damals wurde  Schutz beansprucht f&uuml;r eine  Lehre, die das zugrunde liegende Problem  ausschliesslich mit Mitteln  der  Logik  ohne Zuhilfenahme von ausserhalb  der menschlichen Verstandest&auml;tigkeitliegenden Naturkr&auml;ften  l&ouml;ste.  Diese  auf rein  geistigem  Gebiet liegende mathematisch-organisatorische  Lehre war  bereits vollendet,  bevor  der  Bereich des  Technischen erreicht wurde. Sie verkn&uuml;pfte  diese  Lehre  mit technischen Merkmalen nur  nachArt einer nicht erfinderischen Gebrauchsanweisung (BGHZ 67, 22, 3l)2'.c).</li>
<li>(c) Der Zweck des vorliegenden Anmeldungsgegenstandes  ist die unmittelbare Erreichung  einer optimalen Bremswirkung, wobei mit Hilfe  von Naturkr&auml;ften  der  Erfolg  ohne  Zwischenschaltung von Verstandeskr&auml;ften erreicht wird. Sie nutzt die Erscheinung aus, dass die Radumfangsgeschwindigkeit des &uuml;berwachten Rades abh&auml;ngig von der jeweiligen Beschaffenheit der Fahrbahnoberfl&auml;che infolge der Verringerung (Absenkung) des Bremsdrucks in einem allein zeitlich nicht zu bestimmenden Moment wieder einen bestimmten Beschleunigungswert durch Verringerung der Verz&ouml;gerung erreicht, und verwendet  das vom &uuml;berwachten Rad abgegriffene Signal einer bestimmten Beschleunigung zur  Beendigung  der Absenkung des Bremsdrucks durch Schlie&szlig;en des Auslassventils und leitet dadurch die  Phase  der Druckkonstanthaltung ein. Damit soll nach den Angaben in der Auslegeschrift sichergestellt sein, dass die Druckabsenkung auch bei &Uuml;bergang des Rades von trockener Fahrbahn auf Glatteis w&auml;hrend des Bremsens stets so lange andauert, bis das Rad auch bei dem dann vorhandenen &auml;u&szlig;erst geringen Reibbeiwert wieder mitgenommen wird, und es also auch unter derart ung&uuml;nstigen Fahrbahnverh&auml;ltnissen nicht zum Blockieren des Rades kommen kann (Sp. 2 Z. 55-67).</li></ol>
</div>

<div class="secthead">
<h4>1.2.4. <a name="techprog">4. Auch wenn  das beanspruchte &quot;Antiblockierregelsystem&quot; als ein Verfahren oder ein Programm charakterisiert wird, ergibt  sich daraus f&uuml;r  die  Frage  nach  dem technischen  Gehalt des Anmeldungsgegenstandes nichts Entscheidendes.</a></h4>
</div>

<div class="sectbody">
Es gibt sowohl Programme, die technischer Natur sind, als auch Programme, die untechnischer Natur sind. Die vorerw&auml;hnten Entscheidungen des erkennenden Senats sagen nichts dar&uuml;ber aus, dass Programme als solche stets untechnischen Charakter besitzen. Programme und Technik sind keine gegens&auml;tzlichen Begriffe, die einander ausschliessen. Insbesondere bei Anlagen zur Durchf&uuml;hrung von Verfahren und bei Anordnungen im Bereich der Regeltechnik k&ouml;nnen durch eine Aufeinanderfolge von genau bestimmten technischen Einzelma&szlig;nahmen technische Programme verwirklicht sein, weil sie durch den planm&auml;&szlig;igen Einsatz berechen- und beherrschbarer Naturkr&auml;fte unmittelbar ein bestimmtes Ergebnis erreichen. Das BPatG vermisst deshalb zu Unrecht, dass dem der Anmeldung zugrunde liegenden &quot;Programm&quot; ein neuer erfinderischer Aufbau eines Antiblockierregelsystems zu entnehmen ist. Diese Frage stellt sich nur dann, wenn das Programm nicht technischer Natur ist. Nurin einem solchen Fall sind die Ausf&uuml;hrungen des erkennenden Senats in der Dispositionsprogramm-Entscheidung von Bedeutung,  dass die (nicht technische) Lehre, eine Datenverarbeitungsanlage nach einem bestimmten Rechenprogramm zu betreiben, nur patentf&auml;hig sein k&ouml;nne, wenn das (nicht technische) Programm einen neuen, erfinderischen Aufbau einer solchen Anlage erfordere und lehre oder wenn dem (nicht technischen) Programm die Anweisung zu entnehmen sei, die Anlage auf eine neue, bisher nicht &uuml;bliche und auch nicht naheliegende Art und Weise zu benutzen (BGHZ 67, 22, 29)2'.  Das  BPatG wird  dem Anmeldungsgegenstand  nicht gerecht, indem es  in ihm  lediglich eine Bremsregel sieht, die nur &quot;nach Art eines Programms&quot; aufzeige, welche Signale einen bestimmten Bremsvorgang oder -ablauf herbeif&uuml;hren sollen, und allein daraus schon den Schluss zieht, die Anmeldung habe eine untechnische Anweisung zum Gegenstand.  Dabei wird &uuml;bersehen, dass der beanspruchte Gegenstand die Mittel umfasst, die das Beschleunigungssignal  erzeugen und es zur Steuerung einsetzen, um von der Druckabsenkungsphase in die Druckkonstanthaltungsphase &uuml;berzuleiten.
</div>

<div class="secthead">
<h4>1.2.5. <a name="wirkanspr">5. Es  steht  dem  technischen Charakter  des  Anmeldungsge-genstandes auch nicht entgegen,  dass  der Patentanspruch 1 die Schaltvorrichtungen  nicht  n&auml;her  bezeichnet, es vielmehr  dem Fachmann &uuml;berl&auml;sst,  sich f&uuml;r  die  bistabilen, f&uuml;r die  Drehverz&ouml;gerungs- und f&uuml;r die Drehbeschleunigungs-Schaltvorrichtungen &uuml;blicher Schaltungselemente zu bedienen.</a></h4>
</div>

<div class="sectbody">
F&uuml;r den technischen Charakter einer Erfindung ist es gleichg&uuml;ltig, ob die zum Einsatz  gelangenden Vorrichtungen  als solche bekannt sind oder nicht.  Dem  technischen Charakter der beanspruchten Erfindung steht es ferner nicht entgegen, dass die Zuordnung der Schaltvorrichtungen im Patentanspruch nur durch Wirkungsangaben, n&auml;mlich durch das zu erreichende Schaltergebnis, umschrieben ist. Wenn es darum geht, ob diese Wirkungsangaben den Fachmann bei den verschiedenen beanspruchten Schaltvorrichtungen ausreichend erkennen lassen, wie er deren mechanische, elektrische oder elektronische Zuordnung aufgrund seines  Fachwissens gestalten soll, damit er mit dem auftretenden (Wieder-)Beschleunigungssignal den angestrebten Schaltzustand erreicht, stellt sich nicht die Frage, ob der Anmeldungsgegenstand technischen Charakter hat oder nicht, sondern die vom BPatG bisher nicht beantwortete  Frage der ausreichenden Offenbarung der beanspruchten Erfindung.  Bei einer Lehre, die sich mit verschiedenartigen technischen Mitteln verwirklichen l&auml;sst, um damit unmittelbar einen &uuml;bersehbaren Erfolg  zu  erzielen, ist der Anmelder nicht gen&ouml;tigt, in den Patentanspruch die konkreten Mittel aufzunehmen, mit denen die neue, als erfinderisch beanspruchte prinzipielle Lehre verwirklicht werden kann.  Vielmehr gen&uuml;gt es, die Lehre mit dem alle  vorgeschlagenen Mittel kennzeichnenden Prinzip im Patentanspruch zu umschreiben, wenn der Fachmann die Erfindung aufgrund des Gesamtinhalts der Anmeldungsunterlagenanhand seines Fachk&ouml;nnens ohne weiteres verwirklichen kann.
</div>
</div>
</div>

<div class="secthead">
<h3>1.3. <a name="nvers">III.  Die vom  BPatG gegebene Begr&uuml;ndung tr&auml;gt die Versagung des Patents deshalb nicht.</a></h3>
</div>

<div class="sectbody">
Andere Versagungsgr&uuml;nde sind vom  BPatG  nicht  festgestellt.  Der angefochtene  Beschluss  ist deshalb aufzuheben und die Sache zur anderweiten Verhandlung und Entscheidung an das BPatG zur&uuml;ckzuverweisen.
</div>
</div>
</div>
</div>

<div class="secthead">
<h2>2. <a name="eisenf">Aufatmen und Beifall der Patentjuristen</a></h2>
</div>

<div class="sectbody">
<div class="cit">
Anmerkung: Das  Aufatmen  nach  dieser  Entscheidung  d&uuml;rfte  weithin h&ouml;rbar  sein.  Der mit ihr aufgehobene, in GRUR 1979, 1 1 1, ver&ouml;ffentlichte Beschluss des BPatG lie&szlig; in der Tat bef&uuml;rchten, dass die vom Dispositionsprogramm-Beschluss des BGH (GRUR  1977,  96) eingeleitete Kette von Entscheidungen zur Frage der Patentierbarkeit sogenannter Organisations- und Rechenregeln sowie  vergleichbarer  Anweisungen an die menschliche Verstandest&auml;tigkeit eine Art Hexenjagd auf das &quot;Untechnische&quot; eingeleitet haben k&ouml;nnte.  Die  vom  BPatG angewandte  Betrachtungs- und  Beurteilungsweise drohte ganze Bereiche mindestens der  Regelungstechnik in einen  Strudel der Nicht-Patentierbarkeit zu rei&szlig;en, die gerade einer hochentwickelten industriellen Technik ihren Vorsprung verschafft und erh&auml;lt.
Dabei musste  der  BGH  bei seinem  &quot;Bremsman&ouml;ver&quot; (um im Bilde des Entscheidungsgegenstandes zu bleiben) nicht einmal ein  Ausbrechen  aus  der  Spur  seiner  vorerw&auml;hnten  Entscheidungskette oder auch nur eine Lenkkorrektur in Kauf nehmen.
Er  hat  deren  Grunds&auml;tze,  insbesondere die Kriterien zur Abgrenzung des Technischen vom Untechnischen, die von Kolle in seinem ausf&uuml;hrlichen und sehr lesenswerten Aufsatz zur Dispo-sitionsprogramm-Entscheidung (GRUR 1977,  58) sorgf&auml;ltig analysiert  und  in  ihrem  Kern  &uuml;berzeugend  begr&uuml;sst  worden sind,  nicht  klarzustellen oder zu modifizieren brauchen.  Er hat lediglich darauf aufmerksam  machen  m&uuml;ssen,  dass nicht schon die Charakterisierung einer Lehre als  <strong>Programm</strong> ausreicht, sie  als  untechnisch  dastehen  zu  lassen,  und  er  hat  damit eigentlich nur  an seine auch in jener  Entscheidungskette  immer wieder  hervorgehobene  Weisung erinnert,  dass  der  (technische oder  untechnische)  Charakter einer  Anweisung nicht aufgrund mehr  oder  minder  zuf&auml;lliger  Anspruchsfassungen  (bei  Kolle,a.a. 0.,  S.  64  -  Formulierungsk&uuml;nste; vgl.  hierzu auch  BGH  in GRUR 1977,  152 - Kennungsscheibe) zu beurteilen, sondern allein  vom  sachlichen  Gehalt  der  Anweisung  abzuleiten  ist.
Hier d&uuml;rfte in der Tat die Ursache f&uuml;r die Fehlbeurteilung des Anmeldungsgegenstandes  durch  das  BPatG  zu  suchen  sein. Eine  Brems<strong>regelung</strong>  ist nicht eine  Brems<strong>regel</strong> im  Sinne einer  Denkanweisung  oder  Rechenregel,  wenn  sie  bestimmte Bremsfunktionen  (ausschliesslich) an  das  tat&auml;chliche  Auftreten bestimmter  technischer  Zust&auml;nde  (hier:  Wiederbeschleunigung des  &uuml;berwachten  Rades)  kn&uuml;pft,  und  den  Regelablauf eben nicht  durch eine  Kette  logisch  ablaufender  (Gedanken)operationen vorherbestimmt.  Im  letzteren  Falle - aber  auch nur in diesem -  w&auml;re eine &quot;auf rein geistigem  Gebiet liegende mathematisch-organisatorische  Lehre ...  bereits vollendet (gewesen&quot;,bevor  der  Bereich des  Technischen erreicht wurde).
Das  aber  war  hier  - selbst nach den eigenen  Feststellungendes  BPatG  -  offenbar  nicht  der  Fall.  In  der  Begr&uuml;ndung zum  patentgerichtlichen  Beschluss  heisst  es  (a.a.O.,  S.  111,re.  Sp.),  dass  w&auml;hrend  des  Bremsdruck-Absenkens  der  notwendige  Betrag  der  Druckverringerung  unbekannt  und <strong>zudem  variabel</strong> sei,  so dass der Bremsdruck  so  weit verringert  werden  m&uuml;sse,  dass  das  Rad  wieder  beschleunigt werde.  Dies tut die umstrittene  Lehre offenbar dadurch, dass sie die  Beschleunigung  misst  und  in  Abh&auml;ngigkeit  hiervon  bestimmte  Regelfunktionen ablaufen l&auml;sst.  Wenn das  BPatG dann sagt, &quot;Der untere, die  Beendigung der  Druckabsenkung einsteuernde  Schwellenwert ist  <strong>durch  die  &Uuml;berlegung</strong> zu finden,  welche  Kraftschlussgr&ouml;sse  bei  trockener  griffiger Fahrbahn erreicht werden soll,  ohne  dass der  Blockierregler  umgehend wieder in T&auml;tigkeit  tritt&quot;, und darin &quot;allein&quot; (a.a.O.,  S. 112, U. Sp. oben) die Lehre der Anmeldung sehen will,  dann &uuml;bersah es offenbar, dass in Wahrheit  die  Lehre  den  (im  Einzelfall unbekannten und variablen)  Schwellenwert  durcheine  <strong>Feststellung</strong> des jeweiligen tats&auml;chlichen  Zustandes,  der  sich infolge der herrschenden Naturkr&auml;fte einstellt und daher selbst zu den &quot;beherrschbaren Naturkr&auml;ften&quot; geh&ouml;rt, ermittelt. Anmelderin und BPatG haben offensichtlich aneinander vorbeigeredet, wenn diese in ihrem  Patentbegehren &quot;nur eine Regelphilosophie,  d.h.  Bremsregel&quot; sah und jenes  darin eine Best&auml;tigung seiner  Auffassung erblickte, dass der Anmeldungsgegenstand sich in einer untechnischen Rechenregeloder Denkanweisung ersch&ouml;pfe. Hier ist der BGH mit Recht einer vordergr&uuml;ndigen, im Kern schematischen Beurteilung entgegengetreten und hat sich nicht gescheut, von &quot;Programmen technischer Natur&quot; zu sprechen, die sorgf&auml;ltig von inhaltlich untechnischen Programmen zu unterscheiden seien. Der Begriff &quot;Programm&quot; ist durch  die  Auseinandersetzungen  um  die  Patentierbarkeitlogischer  Operationsketten so  sehr  in  den  Ruf des  Untechnischen geraten, dass die Feststellung des BGH wohltut, Programme  und Technik seien keine gegens&auml;tzlichen  Begriffe, die einander  ausschliessen.  Zutreffend  bemerkt  der  BGH, dass gerade im  Bereich der  Regeltechnik auch als &quot;Programme&quot; zu charakterisierende  Verfahren  &uuml;blich  sind,  die  gleichwohl  in einer &quot;Anweisung  zum planm&auml;&szlig;igen Handeln unter Einsatz beherrschbarer  Naturkr&auml;fte zur unmittelbaren  Erreichung eines kausal &uuml;bersehbaren  Erfolges&quot; bestehen.
Beifall f&uuml;r  diese  Entscheidung,  deren  Bedeutung  dadurchnoch gr&ouml;sser wird,  dass sie gegen Ende (11.5.) fast beil&auml;ufig den &quot;circuit-by-function claim&quot; legalisiert.  Zuordnungen  von Schaltvorrichtungen  im  Patentanspruch nur durch Wirkungsangaben, n&auml;mlich durch das zu erreichende  Schaltergebnis, zu umschreiben, ist eine in vielen F&auml;llen nicht nur zweckm&auml;ssige, sondern zur Vermeidung  unangemessener Beschr&auml;nkung des Schutzbereichs sogar notwendige  Massnahme. Die Feststellung des BGH,
<blockquote>
Bei einer  Lehre, die sich mit verschiedenartigen technischen Mitteln verwirklichen l&auml;sst, um damit unmittelbar einen &uuml;bersehbaren Erfolg zu erzielen, ist der Anmelder nicht gen&ouml;tigt, in den Patentanspruch die konkreten Mittel aufzunehmen, mit denen die neue,  als erfinderisch beanspruchte prinzipielle Lehre verwirklicht werden kann.

Vielmehr gen&uuml;gt es, die Lehre mit dem alle vorgeschlagenen  Mittel kennzeichenden  Prinzip im Patentanspruch zu umschreiben, wenn der Fachmann die Erfindung aufgrund des Gesamtinhalts der Anmeldungsunterlagen an Hand seines Fachk&ouml;nnens ohne weiteres verwirklichen kann.
</blockquote>
werden  wir  uns  zusammen mit der oben erw&auml;hnten  Guthei&szlig;ung von Wirkungsangaben und  Schaltergebnissen  zum  t&auml;glichen  Gebrauch auf unseren  Schreibtisch legen.
</div>
</div>

<div class="secthead">
<h2>3. <a name="links">Annotated Links</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/analysis/invention/index.en.html">Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention</a></b></dt>
<dd>So far computer programs and other <em>rules of organisation and calculation</em> are not <em>patentable inventions</em> according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/papers/bgh-dispo76/index.en.html">&quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot;</a></b></dt>
<dd>A landmark decision of the German Federal Court (BGH): 'organisation and calculation programs for computing machines used for disposition tasks, during whose execution a computing machine of known structure is used in the prescribed way, are not patentable.'  This is the first and most often quoted of a series of decisions of the BGH's 10th Civil Senate, which explain why computer-implementable rules of organisation and calculation (programs for computers) are not technical inventions, and elaborates a methodology for analysing whether a patent application pertains to a technical invention or to a computer program.  The Dispositionsprogramm verdict is especially famous for general and almost prophetic terms in which it explains that patent law is a variant of copyright for a specialised context, namely that of solving problems by the use of controllable forces of nature.  Any attempt to &quot;loosen and thereby in fact abolish&quot; the concept of technical invention would lead onto a forbidden path, the judges warn.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/players/bgh/index.de.html">Patent Inflation in Japan</a></b></dt>
<dd>Japan, although a world champion in the number of patents, has been rather passive in the patent inflation movement.  The patent officials in the government bent their patent law in reaction to pressures from the US government and the local patent lawyers.  The subject was hardly discussed anywhere.  Prof. Konno of Tokyo University has tried to prevent the patent inflation movement by publications and even by appealing to the highest courts.  In early 2002 his last appeal was rejected by the supreme court on the grounds that he as an individual was not entitled to sue the Japanese Patent Office (JPO).</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<div align="center">[ <a href="/papers/bgh-walzst80/index.en.html">&quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot;</a> | BGH &quot;Anti Blocking System&quot; decision of 1980 puts brakes on &quot;witch chase against everything untechnical&quot; ]</div>
</div>
<div id="pedbot">
<form action="http://www.google.com/search" method="get">
<input name="q" size=25 type="text" value="">
<input name="btnG" size=25 type="submit" value="Google Search ffii.org">
<input name="q" size=25 type="hidden" value="site:ffii.org">
</form>
</div>
<div id="dokadr">
http://swpat.ffii.org/papers/bgh-abs80/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2003/11/19
<a href="/group/index.en.html">Workgroup</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /ul/prg/src/mlht/sys/mlhtmake.el -->
<!-- mode: html -->
<!-- End: -->

