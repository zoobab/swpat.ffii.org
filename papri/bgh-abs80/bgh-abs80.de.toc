\contentsline {section}{\numberline {1}BGH nimmt Technizit\"{a}ts-Grenzfall als Anlass, entgegen der Sichtweise des BPatG Wirkungsanspr\"{u}che anzuerkennen und ``technische Programme'' f\"{u}r patentierbar zu erkl\"{a}ren}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}[Anspr\"{u}che]}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}II. Die Rechtsbeschwerde f\"{u}hrt zur Aufhebung des angefochtenen Beschlusses und zur Zur\"{u}ckweisung der Sache an das BPatG.}{4}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}1. Das BPatG hat es vers\"{a}umt zuuntersuchen, f\"{u}r welchen im Patentanspruch umschriebenen Gegenstand mit der Anmeldung Schutz beansprucht wird.}{4}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}2. Dieser Lehre kann der technische Charakter nicht abgesprochen werden.}{4}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}3. Der vorliegende Anmeldungsgegenstand erf\"{u}llt die Anforderungen einer Lehre zum technischen Handeln.}{5}{subsubsection.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.4}4. Auch wenn das beanspruchte ``Antiblockierregelsystem'' als ein Verfahren oder ein Programm charakterisiert wird, ergibt sich daraus f\"{u}r die Frage nach dem technischen Gehalt des Anmeldungsgegenstandes nichts Entscheidendes.}{6}{subsubsection.1.2.4}
\contentsline {subsubsection}{\numberline {1.2.5}5. Es steht dem technischen Charakter des Anmeldungsge-genstandes auch nicht entgegen, dass der Patentanspruch 1 die Schaltvorrichtungen nicht n\"{a}her bezeichnet, es vielmehr dem Fachmann \"{u}berl\"{a}sst, sich f\"{u}r die bistabilen, f\"{u}r die Drehverz\"{o}gerungs- und f\"{u}r die Drehbeschleunigungs-Schaltvorrichtungen \"{u}blicher Schaltungselemente zu bedienen.}{6}{subsubsection.1.2.5}
\contentsline {subsection}{\numberline {1.3}III. Die vom BPatG gegebene Begr\"{u}ndung tr\"{a}gt die Versagung des Patents deshalb nicht.}{7}{subsection.1.3}
\contentsline {section}{\numberline {2}Aufatmen und Beifall der Patentjuristen}{7}{section.2}
\contentsline {section}{\numberline {3}Kommentierte Verweise}{9}{section.3}
