\contentsline {section}{\numberline {1}A Programme as Such}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}The Objective Subject of Invention}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Inventions Must be Solutions}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}The Technical Character}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}The Basic Assumption}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}The Reversal Conclusion}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Exclusion also of Technical Theories}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Technicity of Computer Programmes}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}The technicity concept}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}The ``Language Analysis'' and ``Logic Verification'' cases}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The Bell}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Additional Technical Effect}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Programmes for the Description of Hardware}{9}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Borders of the Technicity concept}{9}{subsection.3.5}
\contentsline {section}{\numberline {4}The Objective Subject of Protection}{10}{section.4}
\contentsline {section}{\numberline {5}The Exclusion Catalogue as Legal Disclaimer}{11}{section.5}
\contentsline {section}{\numberline {6}Source Code in the Patent Claim}{12}{section.6}
\contentsline {section}{\numberline {7}Concluding Remark}{13}{section.7}
