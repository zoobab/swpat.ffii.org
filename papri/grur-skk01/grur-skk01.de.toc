\contentsline {section}{\numberline {1}A Program as Such}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}The Objective Subject of Invention}{4}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Inventions Must be Solutions}{4}{subsection.1.2}
\contentsline {section}{\numberline {2}The Technical Character}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}The Basic Assumption}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}The Reversal Conclusion}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Exclusion also of Technical Theories}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Technicity of Computer Programmes}{7}{subsection.2.4}
\contentsline {section}{\numberline {3}The technicity concept}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}The ``Language Analysis'' and ``Logic Verification'' cases}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The Bell}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Additional Technical Effect}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Programmes for the Description of Hardware}{10}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Borders of the Technicity concept}{10}{subsection.3.5}
\contentsline {section}{\numberline {4}The Objective Subject of Protection}{11}{section.4}
\contentsline {section}{\numberline {5}The Exclusion Catalogue as Legal Disclaimer}{12}{section.5}
\contentsline {section}{\numberline {6}Source Code in the Patent Claim}{13}{section.6}
\contentsline {section}{\numberline {7}Concluding Remark}{14}{section.7}
