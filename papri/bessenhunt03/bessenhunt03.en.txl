<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen & Hunt 2003/05: An Empirical Look at Software Patents

#descr: James Bessen (Research on Innovation and MIT) and Robert M. Hunt
(Federal Reserve Bank of Philadelphia) in a study published in May
2003 present extensive statistical data and analysis to corroborate
their hypothesis that software patenting has substituted rather than
promoted R&D investments.  Software patents are serving as cheap
alternatives to real innovation.

#ino: Bessen & Hunt show that strategic, anticompetitive and defensive use
of patents tends to concentrate in software patents, because they are
easier to obtain (they don't require experimentation or prototyping,
not even writing a program). They are also broader, because software
is not subject to physical constraints and can therefore be composed
into more complex systems, potentially infringing on hundreds of
patents per program. This causes a patent buildup simlar to a cold war
arms race that discourages innovation and competition, and instead of
bringing new products to consumers, reduces their choice and their
access to infomation society, resulting in significant costs and less
productivity for businesses.

#WWW: The original research paper

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bessenhunt03 ;
# txtlang: en ;
# multlin: t ;
# End: ;

