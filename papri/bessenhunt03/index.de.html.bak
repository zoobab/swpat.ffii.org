<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Hartmut PILCH und FFII">
<link href="http://www.a2e.de" rev="made">
<link href="http://www.ffii.org/verein/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005-06-13">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Reply-To" content="phm@a2e.de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="F&ouml;rderverein f&uuml;r eine Freie Informationelle Infrastruktur, Logikpatente, Geistiges Eigentum, Gewerblicher Rechtschutz, GE, GE &amp; GR, immaterielle Verm&ouml;gensgegenst&auml;nde, Immaterialg&uuml;terrecht, mathematische Methoden, Gesch&auml;ftsverfahren, Organisations- und Rechenregeln, Erfindung, Nicht-Erfindungen, computer-implementierte Erfindungen, computer-implementierbare Erfindungen, software-bezogene Erfindungen, software-implementierte Erfindungen, Softwarepatent">
<meta name="title" content="Bessen &amp; Hunt 2003/05: Empirischer Blick auf Softwarepatente">
<meta name="description" content="James Bessen (Research on Innovation und MIT) und Robert M. Hunt (Federal Reserve Bank of Philadelphia) legen in einer im Mai 2003 ver&ouml;ffentlichten Studie umfassende statistische Daten und Analysen vor, in denen sie ihre Hypothese untermauern, dass Softwarepatente  FuE-Investitionen eher ersetzen als f&ouml;rdern. Softwarepatente dienen als  billige Alternative zu echter Innovation.">
<title>Bessen &amp; Hunt 2003/05: Empirischer Blick auf Softwarepatente</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.de.html"-->
<div id="doktit">
<h1>Bessen &amp; Hunt 2003/05: Empirischer Blick auf Softwarepatente<br>
<!--#include virtual="../../banner0.de.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.de.html"-->
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ol type="1"><li><a href="#intro">Einf&uuml;hrung</a></li>
<li><a href="#links">Kommentierte Verweise</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h2>1. <a name="intro">Einf&uuml;hrung</a></h2>
</div>

<div class="sectbody">
Bessen &amp; Hunt zeigen, dass strategischer, gegen Wettbewerber  gerichteter und defensiver Gebrauch von Patenten sich besonders in  Softwarepatenten konzentriert, weil diese leichter zu erlangen sind (sie  erfordern weder Experimente noch das Erstellen von Prototypen und noch  nicht einmal die Entwicklung eines Programmes). Zugleich sind  Softwarepatente breiter, da Software keinen physikalischen  Beschr&auml;nkungen unterliegt und daher leicht zu immer komplexeren Systemen  zusammengesetzt werden kann, wobei ein solches Programm m&ouml;glicherweise  hunderte Patente verletzt. Dieser Gebrauch verursacht einen Aufbau von  Patentportfolios vergleichbar der Aufr&uuml;stungsspirale des Kalten Krieges,  der von Innovation und Wettbewerb abschreckt und der, statt Verbrauchern neue Produkte zu bringen, ihre Wahlm&ouml;glichkeiten und Teilnahme an der  Informationsgesellschaft einschr&auml;nkt, verbunden mit erheblichen Kosten  und geringerer Produktivit&auml;t in den Unternehmen.
</div>

<div class="secthead">
<h2>2. <a name="links">Kommentierte Verweise</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="http://www.researchoninnovation.org/swpat.pdf">The original research paper</a></b></dt>
<dd>James Bessen (Research on Innovation und MIT) und Robert M. Hunt (Federal Reserve Bank of Philadelphia) legen in einer im Mai 2003 ver&amp;ouml;ffentlichten Studie umfassende statistische Daten und Analysen vor, in denen sie ihre Hypothese untermauern, dass Softwarepatente  FuE-Investitionen eher ersetzen als f&amp;ouml;rdern. Softwarepatente dienen als  billige Alternative zu echter Innovation.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/papiere/bessenmaskin00/index.de.html">Bessen &amp; Maskin 2000: Sequentielle Innovation</a></b></dt>
<dd>Forschungspapier der Wirtschaftswissenschaftlichen Fakult&auml;t des Massachusetts Institute of Technology.  Die beiden Autoren stellen fest, dass die amerikanische Softwarepatentierungspraxis zumindest zu keinem merklichen Anstieg der Innovation oder der Forschungsinvestitionen in den letzten Jahren gef&uuml;hrt hat und erkl&auml;ren ihre Feststellungen mit einem mathematischen Modell der &quot;sequentiellen Innovation&quot;.  Mit diesem Modell l&auml;sst sich nachrechnen, dass im Bereich der Information, der Software, der Beratungsdienste u.a. die technische Entwicklung schneller voranschreitet, wenn keine harten &quot;gewerblichen Schutzrechte&quot; gew&auml;hrt werden.  Das Urheberrecht hingegen erlaubt im MIT-Berechnungsmodell eine nahezu optimale Balance der verschiedenen, z.T. zueinander gegenl&auml;ufigen Faktoren, von denen der technische Fortschritt bestimmt wird.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.de.html"-->
</div>
<!--#include virtual="../../valid.de.html"-->
<div id="dokadr">
http://swpat.ffii.org/papiere/bessenhunt03/index.de.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005-06-13
<a href="http://www.a2e.de">Hartmut PILCH</a><br>
deutsche Version 2005-03-28 von <a href="http://www.rudert-home.de">Andreas RUDERT</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/swpatpapri.el -->
<!-- mode: html -->
<!-- End: -->

