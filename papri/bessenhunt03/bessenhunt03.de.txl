<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen & Hunt 2003/05: Empirischer Blick auf Softwarepatente

#descr: James Bessen (Research on Innovation und MIT) und Robert M. Hunt
(Federal Reserve Bank of Philadelphia) legen in einer im Mai 2003
veröffentlichten Studie umfassende statistische Daten und Analysen
vor, in denen sie ihre Hypothese untermauern, dass Softwarepatente 
FuE-Investitionen eher ersetzen als fördern. Softwarepatente dienen
als  billige Alternative zu echter Innovation.

#ino: Bessen & Hunt zeigen, dass strategischer, gegen Wettbewerber 
gerichteter und defensiver Gebrauch von Patenten sich besonders in 
Softwarepatenten konzentriert, weil diese leichter zu erlangen sind
(sie  erfordern weder Experimente noch das Erstellen von Prototypen
und noch  nicht einmal die Entwicklung eines Programmes). Zugleich
sind  Softwarepatente breiter, da Software keinen physikalischen 
Beschränkungen unterliegt und daher leicht zu immer komplexeren
Systemen  zusammengesetzt werden kann, wobei ein solches Programm
möglicherweise  hunderte Patente verletzt. Dieser Gebrauch verursacht
einen Aufbau von  Patentportfolios vergleichbar der Aufrüstungsspirale
des Kalten Krieges,  der von Innovation und Wettbewerb abschreckt und
der, statt Verbrauchern neue Produkte zu bringen, ihre
Wahlmöglichkeiten und Teilnahme an der  Informationsgesellschaft
einschränkt, verbunden mit erheblichen Kosten  und geringerer
Produktivität in den Unternehmen.

#WWW: Das eigentliche Forschungspapier

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/swpatpapri.el ;
# mailto: andreas_ffii_20030226@rudert-home.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bessenhunt03 ;
# txtlang: xx ;
# End: ;

