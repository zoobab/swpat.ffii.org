<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen et Hunt 2003/05: Un Regard Empirique sur les Brevets Logiciels

#descr: Une nouvelle étude montre par des données et des analyses statistiques
approfondies que les brevets logiciels ont résulté de la substitution
des investissements de Recherche et Développement au profit des
investissements de brevetage et qu'ils tiennent lieu principalement
d'alternatives bon marché plutôt que de réelle innovation.

#ino: Bessen et Hunt montrent que l'usage stratégique, anti-compétitif et
défensif des brevets tend à se concentrer sur les brevets logiciels,
parce qu'ils sont plus faciles à obtenir (ils ne requièrent aucune
expérimentation ou prototypage, pas même d'écrire un programme). Ils
sont aussi plus étendus, car le logiciel n'est pas sujet à des
contraintes physiques et peut aussi être constituant de systèmes plus
complexes, en enfreignant potentiellement de centaines de brevets par
programme.Cela rend un développement de brevet similaire à une course
à l'armement de guerre froide qui décourage l'innovation et la
competition, au lieu d'amener de nouveaux produits aux consommateurs,
réduit leur choix et leur accès à une société d'information, en
résultent des coûts significatifs et des pertes de productivité pour
les entreprises.

#WWW: Le document de recherche original

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bessenhunt03 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

