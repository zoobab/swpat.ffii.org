<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO T 0208/84 (1986): Vicom

#descr: A pathbreaking decision of the European Patent Office's Technical
Board of Appeal, which states that an solution for faster processing
of image data is patentable even when it resides in a computer
program.

#mdat: Header Data

#tWs: Date of decision

#aWm: Case number

#pim: Application number

#lea: Title of the application

#pan: Applicant name

#pen: Opponent name

#eoi: Key Doctrines

#iet: Even if the idea underlying an invention may be considered to reside
in a mathematical method a claim directed to a technical process in
which the method is used does not seek protection for the mathematical
method as such.

#eba: A computer of known type set up to operate according to a new program
cannot be considered as forming part of the state of the art as
defined by Article 54(2) EPC.

#aaW: A claim directed to a technical process which process is carried out
under the control of a program (whether by means of hardware or
software), cannot be regarded as relating to a computer program as
such.

#gyb: A claim which can be considered as being directed to a computer set up
to operate in accordance with a specified program (whether by means of
hardware or software) for controlling or carrying out a technical
process cannot be regarded as relating to a computer program as such.

#sumfs: Summary of Facts and Submissions

#WWt: European patent application 79 300 903.6 filed on 22.05.79
(Publication No. 0 005 954), claiming priority of 26.05.78 (US) was
refused by a decision of the Examining Division 065 of the European
Patent Office dated 13.04.84. That decision was based on Claims 1-12
filed on 25.01.84.

#ttg: The reasons given for the refusal were that the independent method
Claims 1, 3, 5, 12 related to a mathematical method which is not
patentable by virtue of Article 52(2) (a) and (3) EPC, that the
dependent method Claims 2, 4, 6, 7 did not add technical features as
required by Rule 29(1) EPC and that the apparatus Claims 8-11 in the
absence of supporting disclosure of novel apparatus were unacceptable
in view of Article 52(1) and 54 EPC.

#iyn: Furthermore, the Examining Division considered that the normal
implementation of the claimed methods by a program run on a known
computer could not be regarded as an invention in view of Article
52(2)(c) and (3) EPC.

#cWW: The applicants lodged an appeal against this decision on 12.06.84.

#tpl: In the statement of grounds the appellants argued essentially as
follows:

#pfW: In a communication of 30.09.85, the Rapporteur of the Board informed
the appellants that if they were to amend their method claims so that
these would relate to the digital processing of images in the form of
a two-dimensional data array, the grant of a patent was conceivable. 
At the same time, it was indicated that the Board would probably remit
the case to the Examining Division to deal with any requirements of
the EPC which might not be met other than the allowability of the
claims under Article 52(2) and 52(3) EPC.

#fns: The appellants thereupon filed amended Claims 1-12 on 11.11.85 and
requested the grant of a European patent on the basis of these claims,
Claims 1 and 8 of which read as follows:

#ratio: Reasons for the Decision

#o1e: The appeal complies with Articles 106 to 108 and Rule 64 EPC and is
therefore admissible.

#rWn: In the decision under appeal the Examining Division has held that the
method of digitally filtering a two-dimensional data array
(representing a stored image) according to Claim 1 which was submitted
to the Examining Division was a mathematical method because at least
the characterising part of the claim would only add a different
mathematical concept and would not define new technical subject-matter
in terms of technical features. It was further considered that such
claims concerned only a mathematical way of approximation of the
transfer function of a two-dimensional finite impulse response (FIR)
filter implemented by direct or conventional convolution. Finally, the
Examining Division considered that digital image processing as such
was just a calculation carried out on two-dimensional arrays of
numbers (representing points of an image) using certain algorithms for
smoothing or sharpening the contrast between neighbouring data
elements in an array. Digital filtering had therefore to be considered
as a mathematical operation.

#WWi: Although the question as to whether a method for image processing is
susceptible of industrial application (Article 57 EPC) has not been
explicitly raised in the procedure before the Examining Division it
seems desirable to consider this issue first before addressing the
point of allowability of the claims under Articles 52(2) and (3) EPC.

#Wqd: The Board's present view is that the question should be answered
affirmatively.

#re3: The now effective method Claims 1-7 and 12 are directed to methods for
digitally processing images. One basic issue to be decided in the
present appeal is, therefore, whether or not such a method is excluded
from patentability under Article 52(2) and (3) EPC on the ground that
it is a mathematical method as such.

#pip: There can be little doubt that any processing operation on an electric
signal can be described in mathematical terms. The characteristic of a
filter, for example, can be expressed in terms of a mathematical
formula. A basic difference between a mathematical method and a
technical process can be seen, however, in the fact that a
mathematical method or a mathematical algorithm is carried out on
numbers (whatever these numbers may represent) and provides a result
also in numerical form, the mathematical method or algorithm being
only an abstract concept prescribing how to operate on the numbers. No
direct technical result is produced by the method as such. In contrast
thereto, if a mathematical method is used in a technical process, that
process is carried out on a physical entity (which may be a material
object but equally an image stored as an electric signal) by some
technical means implementing the method and provides as its result a
certain change in that entity. The technical means might include a
computer comprising suitable hardware or an appropriately programmed
general purpose computer.

#nis: The Board, therefore, is of the opinion that even if the idea
underlying an invention may be considered to reside in a mathematical
method a claim directed to a technical process in which the method is
used does not seek protection for the mathematical method as such.

#rpe: In contrast, a %(q:method for digitally filtering data) remains an
abstract notion not distinguished from a mathematical method so long
as it is not specified what physical entity is represented by the data
and forms the subject of a technical process, i.e. a process which is
susceptible of industrial application.

#geW: Rule 29(1) EPC requires that the claims shall be drafted %(q:in terms
of the technical features of the invention). The Board considers that
this condition is met if the features mentioned in the claims will be
understood by those skilled in the art as referring to the technical
means for carrying out the functions specified by such features.  If
convenient, therefore, the use of mathematical expressions (addition,
multiplication, convolution, logic conjunctions etc.) is admissible,
the overriding requirements always being that the claim be clear and
concise (Article 84 EPC) and that the person skilled in the art can
understand what technical means are necessary from the description
and/or his general knowledge of the field concerned (in order to
comply with Article 83 EPC).

#dio: For all these reasons, the Board has come to the conclusion that the
subject-matter of Claim 1 (and similarly that of the other method
Claims 2-7 and 12) is not barred from protection by Articles 52(2)(a)
and (3) EPC.

#WWc: The Board will now consider the Examining Division's argument that the
implementation of the claimed methods for image processing by a
program run on a computer could not be regarded as an invention under
Article 52(2)(c) and (3) EPC which seems tantamount to saying that a
claim directed to such subject-matter would seek protection for a
computer program as such.

#nor: The appellants have stressed that the application discloses new
hardware for carrying out the claimed methods but admit on the other
hand that at least in principle it is possible to implement the method
and apparatus according to the application by a suitably programmed
conventional computer although such a computer may not be optimized
for carrying out digital image processing (cf. page A-2 of the
Statement of Grounds).

#sWo: The Board is of the opinion that a claim directed to a technical
process which process is carried out under the control of a program
(be this implemented in hardware or in software), cannot be regarded
as relating to a computer program as such within the meaning of
Article 52(3) EPC, as it is the application of the program for
determining the sequence of steps in the process for which in effect
protection is sought. Consequently, such a claim is allowable under
Article 52(2)(c) and (3) EPC.

#liW: Concerning the apparatus Claim 8, the Examining Division has held that
it is not acceptable because a new apparatus is not clearly disclosed.
According to the decision under appeal, the claim when interpreted in
the light of the description and the drawings seems to imply only the
use of a conventional computer which could not provide the basis of an
acceptable product claim in view of Articles 52(1) and 54 EPC. The
Board understands this as meaning that the Examining Division was of
the opinion that a conventional computer programmed so as to carry out
a method according to one or more of the method claims is not novel.

#Weh: In the view of the Board, however, Article 54 EPC leaves no room for
such an interpretation. A computer of known type set up to operate
according to a new program cannot be considered as forming part of the
state of the art as defined by Article 54(2) EPC. This is particularly
apparent in the present case as Claims 8 - 11 clearly embrace also the
use of special hardware, for which some indications are given in the
description and also mixed solutions combining some special hardware
with an appropriate program.

#lat: In view of certain considerations by the Examining Division which
appear to apply to the apparatus claims as well (cf. paragraph 10
above) it remains to be examined if the present apparatus Claim 8
would be objectionable under Article 52(2)(c) as qualified by (3) EPC.
For reasons analogous to these given in paragraph 12 above, the Board
holds that this is not the case and the same applies to the other
apparatus Claims 9-11. Generally claims which can be considered as
being directed to a computer set up to operate in accordance with a
specified program (whether by means of hardware or software) for
controlling or carrying out a technical process cannot be regarded as
relating to a computer program as such and thus are not objectionable
under Article 52(2)(c) and (3) EPC.

#eWW: In arriving at this conclusion the Board has additionally considered
that making a distinction between embodiments of the same invention
carried out in hardware or in software is inappropriate as it can
fairly be said that the choice between these two possibilities is not
of an essential nature but is based on technical and economical
considerations which bear no relationship to the inventive concept as
such. Generally speaking, an invention which would be patentable in
accordance with conventional patentability criteria should not be
excluded from protection by the mere fact that for its implementation
modern technical means in the form of a computer program are used.
Decisive is what technical contribution the invention as defined in
the claim when considered as a whole makes to the known art. Finally,
it would seem illogical to grant protection for a technical process
controlled by a suitably programmed computer but not for the computer
itself when set up to execute the control.

#rrW: At least theoretically it could be questioned whether claims directed
to apparatus for carrying out a certain function should be limited to
the apparatus when indeed carrying out this function, which means in
the present case that under the control of the program the computer
steps through a succession of different configurations to effect
operations on the electric signal representing the image. The Board,
however, rejects this view as it would result in an undue limitation
of the possibilities of the patent owner to assert his rights.

#mWW: It may be mentioned in passing here that the computer program referred
to on page 14, line 16 onwards of the description merely serves to
calculate the element values of the small generating kernel and the
weighting values. It does not form part of the image processing
methods claimed, nor is it embodied in the apparatus claims. Indeed
such a program would not be patentable in view of the Board's
foregoing considerations.

#ain: In the course of the procedure, the Examining Division has also raised
objections concerning the absence of inventive step and insufficient
disclosure. The discussion on these matters between the Examining
Division and the appellant does not seem definitely concluded.

#wtm: In order not to deprive the appellants of an examination in two
instances and in accordance with the request expressed by the
appellants in the statement of grounds the Board deems it appropriate
to remit the case to the Examining Division to deal with the said
matters as it sees fit and to deal with any amendments which will be
required to comply inter alia with the provisions of Articles 83 and
84 and Rules 27 and 29 EPC.

#Ord: Order

#hjd: For these reasons, it is decided that:

#osW: The decision of the Examining Division dated 13 April 1984 is set
aside.

#tWf: The case is remitted to the Examining Division for further prosecution
on the basis of Claims 1-12 filed on 11 November 1985.

#xa2: DG3: DBA case T 0208/84

#euW: source text as published by the EPO

#eiW: the second major pathbreaking decision of the EPO after Vicom, quotes
Vicom in several places.

#9fW: EP 0005954 Vicom Method for Digital Image Processing

#tne: The original patent application.  It appears that the patent was
ultimately refused due to lack of novelty or inventive step.

#sid: contains detailed criticism of the Vicom doctrines.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epo-t840208.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epo-t840208 ;
# txtlang: xx ;
# End: ;

