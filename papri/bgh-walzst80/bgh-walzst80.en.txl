<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Ddr: The patent office has rejected as lacking novelty a patent
application, which according to its original designation pertained to
a process of cutting off the rest lengths of the rolling rods which
had been split down to a vending length.  Against this, the applicant
has filed a complaint based on the following patent claim nr 1:

#AKe: Work process during splitting of roller rods to cooling bed lengths
behind continuously working rolling lines, during which the rolling
workpieces are during roll-out captured by measuring devices and the
measured values are sent to a computer, which finds out the complete
length and, taking into consideration the maximal length of the pieces
to be conveyed over the cooling bed and the minimal length determined
by the running speed of the rolling pieces and the working rhythm of
the cooling bed, controls the subdivision into integer multiples of
the vending length with the further constraint that any rest length
shorter than the full vending length is appended to an integer
multiple of the vending length and that none of the remaining partial
lengths are cut whose length is a larger integer multiple than that of
this partial length with an appended rest length, characterised by the
fact that ...

#DRW: The admitted legal complaint remains without success.

#Def: The Federal Patent Court has given as a reason for its decision that
the teaching contained by claim 1, as far as it goes beyond the state
of the art as expressed in the generic concept of the claim,
constitutes a pure thinking instruction for the programming of a
computer, i.e. a a computing program.  This is only one of the
prerequisites for being able to control the workflow of a machine
equipement in a sensible way with help of a computing device.  A
patentability of such a teaching can moreover not be deduced from the
fact that the untechnical calculating program is combined with
technical features.  For neither is the computer modified in an
inventive way by use of the computing program, nor does the claim
teach a new and inventive way of using the computer.  Under these
circumstances it is moreover without meaning that the program serves a
technical purpose. ...

#Nnl: According to the explanations of the contested decision, the invention
consists in a splitting method of the known kind in which the goal of
splitting down to cooling bed lengths, i.e. achieving that in each
cold-cutting position the last cut removes only rest lengths, is
achieved by the following instructions:

#Lse: First, base your calculation of the complete length of the rolling rod
on the weight as actually used.

#Rke: On the basis of thus inferred length and the given conditions of
operation and sales, calculate with help of a computer partial lengths
and split these in such a way that the penultimate (next to last)
partial lengths are shorter by at least one vending length than the
last partial length which has the rest length appended to it.

#EeW: Then also with help of the computer before the last partial sections
calculate the to be expected total length of the rolling rods
precisely based on measurements that have meanwhile become possible by
known means and determine the difference to the previously inferred
value.

#Gir: Compensate this difference during the last partial length(s).

#Zes: The Federal Patent Court correctly remarks that the transformation of
the used material as well as its cooling and splitting into vending
lengths require technical means and dispositions, but that these
technical means -- measuring devices, computer, cold cutter -- are
known to the skilled person and are not subject to any further
elaboration, let alone inventive development, in the patent
application, and that moreover the patent application does not teach a
way of using the known technical means in a new and inventive way. 
The conclusion drawn herefrom that the given teaching is a %(q:pure
thinking instruction) and merely constitutes a %(q:systematically
recorded complete instruction, by which a problem can be solved by
calculation), correctly describes the subject matter of the invention
and therefore, according to the jurisdiction of the senate, leads to
the consequence that the applied-for teaching is not patentable due to
lack of technical character.

#Wdg: As the senate has explained in its %{DP} decision, the question of the
technical character of an invention is not dependent on its verbal
clothing.  It is therefore without decisive significance that in the
patent claim technical devices and entities are named.  It is moreover
not decisive that for the execution of the invention-based process
technical means are recommendable or constitute the only practically
viable mode of implementation (senate decision %{PV}).  Finally it is
without significance that the result obtained by application of the
teaching is used in the technical field (senate decision %{ST}).  No,
it only needs to be considered which instructions constitute the
claimed-to-be new and inventive core of the teaching, i.e. in which
steps the problem is led to a complete solution.  When looking at the
process steps which the contested decision correctly and
uncontradicted by the legal complaint identified as the decisive ones,
 then it is clear that the core of the teaching consists in an
untechnical thinking scheme and that, similar to the disposition
program case treated in the %{DP} decision, the realm of technology is
set foot upon only after the real problem solution has already been
completed. ... The measuring values (e.g. the operation conditions of
the cooling bed and the cold cutter) and means (measuring devices)
needed for obtaining measuring values are no more integral parts of
the problem solution, which does not exceed the realm of the
mental-schematic, than are the apparatusses used for calculating and
for controlling the device according to the results of the
calculation.  In comparison with the disposition program treated by
the senate in the %(dp:said decision), this teaching differs only in
that the values processed by the program relate to a technical process
and that the results obtained with help of the program are used in a
technical process.  As the senate has repeatedly said, such
differences do not justify viewing this teaching as a technical one.

#EWW: It was pointed out correctly that such a case is being submitted to
the senate's decision for the first time; an immediate control of the
production process by means of results obtained using the computer has
in no case been subject of the programs which the senate so far has
had to assess for technical character.  Yet the Federal Patent Court
has correctly reached the opinion that in such cases the criteria for
deciding can be no different than in the cases decided so far.  Here
also the concept of technical invention as found in the %(rs:caselaw
of this senate) is to be taken as a starting point, meaning, in
summary, that a technical invention is the plan-conformant use of
controllable natural fores outside the realm of human reasoning for
directly achieving a causally overseeable success.  In the case under
deliberation here the use of natural forces is in the explained sense
not part of the problem solution, and the achieved success -- the
aimed-for splitting of the rods -- does not directly result from
executing the instruction, but is indirectly achieved by independent
technical means which are likewise not part of the problem solution. 
Just like the computer in the previously decided cases, in this case
not only the computer but also the peripheral hardware, whose activity
is evaluated and controlled by the computer, is not subject of the
invention.  This could be seen differently only if the invention
changed the design or use of the technical means in an inventive way. 
Specially in the case of a process computer, a technical innovation
could be discerned if the claimed teaching required and disclosed new
and inventive controlling means or their new and inventive use for
influencing the production process.  However in the present case, even
according to its presentation by the applicant, there can be no talk
about anything of the sort.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh-walzst80 ;
# txtlang: en ;
# multlin: t ;
# End: ;

