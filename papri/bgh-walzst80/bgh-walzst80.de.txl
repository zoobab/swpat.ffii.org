<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Ddr: Das Patentamt hat die Patentanmeldung, welche nach ihrer
ursprünglichen Bezeichnung ein Verfahren zum Abtrennen der Restlängen
von den auf Verkaufslänge unterteilten Walzstäben betraf, mangels
Neuheit zurückgewiesen.  Mit der hiergegen eingelegten Beschwerde hat
die Anmelderin die Bekanntmachung mit folgendem Patentanspruch 1
beantragt:

#AKe: Arbeitsverfahren beim Unterteilen von Walzstäben auf Kühlbettlängen
hinter kontinuierlich arbeitenden Walzstraßen, bei dem das Walzgut
während der Auswalzung durch Messeinrichtungen erfasst wird und die
Messwerte einem Rechner zugeführt werden, der die Gesamtlänge
ermittelt und unter Berücksichtigung der maximalen, über das Kühlbett
zu fördernden Länge und der minimalen, aus der Auslaufgeschwindigkeit
des Walzgutes und dem Arbeitstakt des Kühlbettes sich ergebenden
Länge, die Unterteilung in Teillängen von ganzzahligen Vielfachen der
Verkaufslänge steuert mit der weiteren Maßgabe, dass die keine volle
Verkaufslänge ergebende Restlänge unter Berücksichtigung der maximalen
Kühlbettlänge einem ganzzahligen Vielfachen der Verkaufslänge
zugeschlagen wird und keine der übrigen Teillängen mit einem größeren
ganzahligen Vielfachen der Verkaufslänge geschnitten werden, als die
mit der Restlänge behaftete Teillänge, dadurch gekennzeichnet, dass
...

#DRW: Die zugelassene Rechtsbeschwerde bleibt erfolglos.

#Def: Das Bundespatentgericht hat zur Begründung seiner Entscheidung
angeführt, die durch den Patentanspruch 1 vermittelte Lehre, soweit
sie über den im Oberbegriff des Anspruchs aufgeführten Stand der
Technik hinausgehe, sei eine reine Denkanweisung für die
Programmierung eines Rechners, also ein Rechenprogramm; 
dieses sei
nur eine Voraussetzung dafür, dass unter Zuhilfenahme eines Rechners
der Arbeitsablauf einer maschinellen Anlage sinnvoll gesteuert werden
könne.  Die Patentierbarkeit einer solchen Lehre lasse sich auch nicht
daraus herleiten, dass das untechnische Rechenprogramm mit technischen
Merkmalen verknüpft sei.  Denn weder werde die mit Hilfe des
Rechenprogramms gesteuerte Anlage durch die Lehre des Patentanspruchs
gegenübert dem Stand der Technik in erfinderischer Weise verändert,
noch lehre der Patentanspruch eine neue, erfinderische Art der
Benutzung der Anlage.  Unter diesen Umständen besage es auch nichts,
dass das Programm einem technischen Zweck diene ...

#Nnl: Nach den Darlegungen des angefochtenen Beschlusses besteht die
Erfindung darin, bei einem Aufteilungsverfahren der bekannten Art das
Ziel der Aufteilung in Kühlbettlängen, nämlich zu bewirken, dass in
jeder Kaltscherenlage durch den letten Schnitt nur noch Restlängen
abgetrennt werden, durch folgende Anweisungen zu erreichen:

#Lse: Lege der rechnerischen Ermittlung der Gesamtlänge des zu
unterteilenden Walzstabes zunächst das Einsatzgewicht zugrunde.

#Rke: Rechne mit Hilfe eines REchners aus der so ermittelten Länge
entsprechend den vorgegebenen BEtriebs- und Verkaufsbedingungen
Teillängen aus und teile diese dabei so auf, dass die vorletzten
TEillängen um mindestens eine Verkaufslänge kürzer sind als die mit
der Restlänge behaftete letzte Teillänge.

#EeW: Errechne dann ebenfalls mit Hilfe des Rechners vor den letzten
Teilabschnitten die zu erwartende Gesamtlänge des Walzstabes genau
aufgrund von inzwischen in bekannter Weise möglich gewordenen
Messungen und stelle den Unterschied zu dem errechneten Wert fest.

#Gir: Gleiche diesen Unterschied bei der oder den vorletzten Teillängen aus.

#Zes: Zutreffend bemerkt das Bundespatentgericht, dass die Umformung des
Einsatzmaterials, dessen Kühlung und Aufteilung in Verkaufslängen
technische Mittel und Maßnahmen erforderten, dass diese technischen
Mittel -- Messeinrichtungen, Rechner, Kaltschere -- aber dem Fachmann
bekannt seien und in der Patentanmeldung keine nähere Darstellung,
erst recht keine erfinderische Weiterentwicklung erführen; ebensowenig
lehre die Patentanmeldung, mit den bekannten technischen Mitteln auf
eine neue, erfinderische Art umzugehen.  Der hieraus gezogene Schluss,
die gegebene Lehre sei deshalb eine %(q:reine Denkanweisung) und
stelle lediglich %(q:eine systematisch niedergeschriebene,
vollständige Anweisung) dar, %(q:nach der eine Aufgabe durch
Berechnungen gelöst werden kann), wird dem Gegenstand der Erfindung
gerecht und hat nach der Rechtsprechung des Senats zur Folge, dass die
angemeldete Lehre mangels eines technischen Gehalts dem Patentschutz
nicht zugänglich ist.

#Wdg: Wie der Senat in seiner Entscheidung %{DP} ausgeführt hat, entscheidet
sich die Frage nach dem technischen Charakter einer Lehre nicht nach
deren sprachlicher Einkleidung.  Es ist deshalb ohne entscheidende
Bedeutung, dass in dem Patentanspruch technische Vorrichtungen und
Größen genannt sind.  Entscheidend ist auch nicht, dass für die
Durchführung des erfindungsgemäßen Verfahrens technische Mittel
sinnvoll sind oder gar alleine in Betracht kommen (Entscheidung des
Senats %{PV}.  Schließlich ist es ohne Bedeutung, dass das durch die
Anwendung der Lehre gewonnene Ergebnis auf technischem Gebiet
Verwendung findet (Senatsentscheidung %{ST}.  Es kommt vielmehr
lediglich darauf an, in welchen Anweisungen der als neu und
erfinderisch beanspruchte Kern der Lehrezu sehen ist, das heißt, in
welchen Schritten das Problem der fertigen Lösung zugeführt wird. 
Betrachtet man die in dem angefochtenen Beschluss zutreffend und auch
von der Rechtsbeschwerde in diesem Punkt unbeanstandet als
entscheidend herausgestellten Verfahrensschritte unter den genannten
Gesichtspunkten, dann erweist sich, dass der Kern der Lehre in einem
untechnischen Denkschema besteht und dass, ähnlich dem in der
Entscheidung %{DP} behandelten Fall, das Gebiet der Technik erst
betreten wird, nachdem die eigentliche Problemlösung bereits
abgeschlossen ist.  ...  Die für die Gewinnung der Messwerte
bedeutsamen technischen Größen (z.B. die Betriebsbedingungen des
Kühlbetts und der Kaltschere) und Mittel (Messvorrichtungen) sind
ebensowenig Bestandteil der über den Bereich des
Gedanklich-Schematischen nicht hinausgehenden Problemlösung wie die
zur Berechnung und zur Steuerung der Anlage nach den Ergebnissen der
Berechnung eingesetzten Apparaturen.  Im Vergleich zu dem von dem
Senat in der %(dp:genannten Entscheidung) behandelten
Dispositionsprogramm liegen Unterschiede der hier zu beurteilenden
Lehre nur darin, dass die nach dem Programm verarbeiteten Werte eine
Beziehung zu technischen Vorgängen haben und dass die mit Hilfe des
Programms gewonnenen Ergebnisse Verwertung in einem technischen
Verfahren finden.  Wie der Senat wiederholt ausgesprochen hat,
rechtfertigen solche Unterschiede eine Beurteilung der Lehre als eine
technische nicht.

#EWW: Es trifft zu, dass ein solcher Fall erstmals der Entscheidung des
Senats unterbreitet wird; eine unmittelbare Steuerung des
Produktionsvorganges durch die mit Hilfe des Rechners gewonnenen
Ergebnisse war in keinem Fall Gegenstand der bisher vom Senat auf
ihren technischen Charakter zu beurteilenden Programme.  Zu Recht hat
aber das Bundespatentgericht die Auffassung vertreten, dass in solchen
Fällen keine anderen Maßstäbe gelten können als in den bisher
entschiedenen.  Auch hier ist von dem der %(rs:Rechtsprechung des
Senats) zu entnehmenden Begriff der technischen Erfindung auszugehen,
der sich zusammenfassend dahin formulieren lässt, dass darunter die
planmäßige Benutzung beherrschbarer Naturkräfte außerhalb der
menschlichen Verstandestätigkeit zur unmittelbaren Herbeiführung eines
kausal übersehbaren Erfolges zu verstehen ist. In dem zur Entscheidung
stehenden Fall ist die Benutzung von Naturkräften in dem genannten
Sinne nicht Bestandteil der Problemlösung, und der erstrebte Erfolg --
die erwünschte Aufteilung der Stäbe -- stellt sich aufgrund der
Anwendung der Anweisung nicht unmittelbar ein, sondern erst durch die
Benutzung technischer Mittel, die aber ebensowenig Anteil an der
Lösung haben.  Wie in den vom Senat bisher entschiedenen Fällen die
EDV-Anlage, so ist in diesem Falle außer dieser auch die (übrige)
maschinelle Einrichtung, deren Tätigkeit durch den Rechner ausgewertet
und gesteuert wird, nicht Gegenstand der Erfindung.  Anders wäre es
nur dann, wenn die Erfindung die Gestaltung oder die Nutzung der
technischen Mittel erfinderisch verändern würde.  Speiell im Falle des
Prozessrechners könnte eine als technisch zu beurteilende Neuerung
auch darin liegen, dass durch die beanspruchte Lehre neue und
erfinderische Steuerungsmittel oder deren neue und erfinderische
Verwendung zur Beeinflussung des Produktionsvorgangs gefordert und
offenbart würden.  Davon kann aber hier, auch nach den eigenen
Darlegungen der Anmelderin, keine Rede sein.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh-walzst80 ;
# txtlang: de ;
# multlin: t ;
# End: ;

