\begin{subdocument}{epo-gl78}{EPO 1978: Examination Guidelines}{http://swpat.ffii.org/papri/epo-gl78/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Adopted by the President of the European Patent Office in accordance with EPC 10.2a with effect from 1978-06-01.  Excerpts concerning the question of technical invention, limits of patentability, computer programs, industrial application etc.}
\begin{description}
\item[title:]\ EPO 1978: Examination Guidelines
\item[source:]\ Liedl 1980 /bib: Universit\"{a}t Konstanz 0168.6302.50, h16/1100, juc 530 l43c, 80-8-48/l88-001
\end{description}

\begin{sect}{form}{Guidelines for Formalities Examination}
...
\end{sect}

\begin{sect}{serch}{Guidelines for the Search}
...
\end{sect}

\begin{sect}{subst}{Guidelines for Substantive Examination}
\begin{sect}{intro}{Introduction}
...
\end{sect}

\begin{sect}{cont}{Content of European Application (other than claims)}
...
\end{sect}

\begin{sect}{claims}{The Claims}
...
\end{sect}

\begin{sect}{pat}{Patentability}
\begin{sect}{gen}{General: Art. 52}
\begin{sect}{basreq}{[Basic Requirements for Patentability]}
There are four basic requirements for patentability:
\begin{enumerate}
\item
There must be an \begin{quote}
{\it ``invention''}
\end{quote}.

\item
The invention must be \begin{quote}
{\it ``susceptible of industrial application''}
\end{quote}.

\item
The invention must be \begin{quote}
{\it ``new''}
\end{quote}.

\item
The invention must involve an \begin{quote}
{\it ``inventive step''}
\end{quote}.
\end{enumerate}

These requirements will be dealt with in turn in IV, 2 and 3,4,5 to 8 and 9 respectively.
\end{sect}

\begin{sect}{impreq}{[Implicit Requirements]}
In addition to these four basic requirements, the examiner should be aware of the following two requirements that are implicitely contained in the Convention and in the Regulations:

\begin{enumerate}
\item
The invention must be such that it can be carried out by a person skilled in the art (after proper instruction by the application); this follows from Article 83.
Instances where the invention fails to satisfy this requirement are given in II, 4.11.

\item
The invention must be of ``technical character'' to the extent that it must relate to a technical field (Rule 27, paragraph 1b), must be concerned with a technical problem, and must have technical features in terms of which the matter for which protection is sought can be defined in the claim (see III, 2.1).
This requirement of ``technical character'' may be decisive in determining whether or not an invention is excluded from patentability under Article 52, paragraphs 2 and 4 and Article 53, sub-paragraph (b).
\end{enumerate}
\end{sect}

\begin{sect}{nonprog}{[No Requirement of Progressiveness or Usefulness]}
The Convention does \underline{not} require explicitly or implicitly that an invention to be patentable must entail some technical progress or even any useful effect.  Nevertheless, advantageous effects, if any, with respect to the state of the art should be stated in the description, and any such effects are often important in determining ``inventive step'' (see IV, 9).
\end{sect}
\end{sect}

\begin{sect}{inv}{Inventions}
\begin{sect}{neglist}{[Definition through Exclusions]: Art 52(2)}
The Convention does not define what is meant by \begin{quote}
{\it ``invention''}
\end{quote}, but Art 52(2) contains a non-exhaustive list of things which shall not be regarded as inventions.

Several of the exclusions in this list comprise abstractions as well as materialisations thereof (e.g. an aesthetic creation as an abstract entity or as a picture, sculpture etc; a computer program as an abstract entity or defined in terms of a process for operating a computer or as a record, e.g. on magnetic tape).

An ``abstraction'' as such is never patentable.

Materialisations as products or processes may be patentable but only when the \begin{quote}
{\it ``invention''}
\end{quote} has technical (i.e. practical) features.

The examples given below will help to make this clear (see also IV, 2.2).

The items on the list in Art 52(2) will be dealt with in turn.

\begin{description}
\item[Discoveries:]\ ...
\item[Scientific Theories:]\ ...
\item[Mathematical Methods:]\ These are a particular example of the principle that purely abstract ore intellectual methods are not patentable.  For example, a shortcut method of division would not be patentable but a calculating machine designed to operate accordingly may well be patentable.  A mathematical method for designing electrical filters is not patentable; nevertheless filters designed according to this method could be patentable provided they have a novel technical feature to which a product claim can be directed.
\item[Aesthetic Creations:]\ An aesthetic creation relates by definition to an article (e.g. a painting or sculpture) having aspects which are other than practical or functional and the appreciation of which is essentially subjective.  If, however, the article happens also to have functional or technical features, it might be patentable, a tyre tread being an example of this.  The aesthetic effect itself is not patentable, neither in a product nor in a process claim. ... Nevertheless, if an aesthetic effect is obtained by a technical structure or other technical means, although the aesthetic effect itself is not patentable, the means of obtaining it may be.  For example, a fabric may be provided with an attractive appearance by means of a layered structure not previously used for this purpose, in which case a fabric incorporating such structure may be patentable. ...
\item[Schemes, rules and methods for performing mental acts, playing games or doing business:]\ These are further examples of items of an abstract or intellectual character.  In particular, a scheme for learning a language, a method of solving crossword puzzles, a game (as an abstract entity defined by its rules) or a scheme for organising a commercial operation would not be patentable.  However, novel apparatus for playing a game or carrying out a scheme might be patentable.
\item[Programs for computers:]\ A computer program may take various forms, e.g. an algorithm, a flow-chart or a series of coded instructions which can be recorded on a tape or other machine-readable record-medium, and can be regarded as a particular case of either a mathematical method (see above) or a presentation or information (see below).  If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program \emph{per se} or the program when recorded on magnetic tape.
\item[Presentations of information:]\ Any presentation of information characterised solely by the content of the information is not patentable.  This applies whether the claim is directed to the presentation of the information per se (e.g. by accoustical signals, spoken words, visual displays), to information recorded on a carrier (e.g. books characterised by their subject, gramophone records characterised by the musical piece recorded, traffic signs characterised by the data or programs recorded), or to processes and apparatus for presenting information (e.g. indicators or recorders characterised solely by the information indicated or recorded).  If, however, the presentation of information has new technical features there could be patentable subject matter in the information carrier or in the process or apparatus for presenting the information.  The arrangement or manner of representation, as distinguished from the information content, may well constitute a patentable technical feature.  Examples in which such a technical feature may be present are: a telegraph apparatus or communication system characterised by the use of a particular code to represent the characters (e.g. pulse code modulation); a measuring instrument designed to produce a particular form of graph for representing the measured information; a grammophone record characterised by a particular groove form to allow stereo recordings;  or a diapositive with a sound-track arranged at the side of it.
\end{description}
\end{sect}

\begin{sect}{examregl}{[Examine the contribution rather than the claim wording!]}
In considering whether an invention is present there are two general points the examiner must bear in mind.  Firstly, he should disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the alleged \begin{quote}
{\it ``invention''}
\end{quote} claimed makes to the known art.  If this contribution does not constitute an invention, there is not patentable subject matter.  This point is illustrated by the examples given in IV, 2.1\footnote{see \ref{neglist}, page \pageref{neglist}} of different ways of claiming a computer program.  As another example, if a manufactured article, which is itself of a kind which is patentable, happens to be known, then a claim directed to that article and distinguished from the known art solely by choosing a different colour for the article would not be for an invention unless the colour chosen provided a different technical (and not merely aesthetic) feature.  On the other hand, any exclusion from patentability applies only to the extent that the application relates to the excluded subject matter or activities as such.

Thus the exclusion might not apply if the invention also provides new technical features.  This is illustrated, for instance, by the examples given in IV, 2.1\footnote{see \ref{aesthe}, page \pageref{aesthe}} under \begin{quote}
{\it ``aesthetic creations''}
\end{quote}.  As a further example, a gramophone record distinguished solely by the music recorded thereon would not be patentable; if however, the form of the groove were modified so that the record, when used with an appropriate pick-up mechanism, functioned in a new way (as in the first stereo record), there could be patentable subject matter.
\end{sect}
\end{sect}

\begin{sect}{exc}{Exceptions to Patentability: Art. 53}
...
\end{sect}

\begin{sect}{ind}{Industrial Application: Art. 57}
\begin{quote}
{\it ``An invention shall be considered as susceptible of industrial application if it can be made or used in any kind of industry, including agriculture.''}
\end{quote} \begin{quote}
{\it ``Industry''}
\end{quote} should be understood in its broad sense of including any physical activity of ``technical character'' (see IV, 1.2\footnote{see \ref{impreq}, page \pageref{impreq}}), i.e. an activity which belongs to the useful or practical arts as distinct from the aesthetic arts; it does not necessarily imply the use of a machine or the manufacture of an article and could cover e.g. a process for dispersing fog, or a process for converting engergy from one form to another. Thus, Article 57 excludes from patentability very few \begin{quote}
{\it ``inventions''}
\end{quote} which are not already excluded by the list in Art 52(2) (see IV, 1.2)\footnote{see \ref{impreq}, page \pageref{impreq}}.

...
\end{sect}

\begin{sect}{novel}{Novelty; State of the Art}
...
\end{sect}

\begin{sect}{eukfl}{Conflict with other European applications}
...
\end{sect}

\begin{sect}{nvtst}{Test for Novelty}
...
\end{sect}

\begin{sect}{npred}{Non-prejudicial disclosures}
...
\end{sect}

\begin{sect}{invst}{Inventive step}
...\footnote{Very similar to the current examination guidelines.}
\end{sect}
\end{sect}

\begin{sect}{prior}{Priority}
...
\end{sect}

\begin{sect}{examp}{Examination Procedure}
...
\end{sect}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

