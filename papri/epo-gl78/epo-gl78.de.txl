<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPA 1978: Prüfungsrichtlinien

#descr: Angenommen durch den Präsidenten des europäischen Patentamts in
Übereinstimmung mit EPÜ 10.2a in der Fassung von 1987-06-01. Teile
betreffen die Frage der technischen Erfindung, den Grenzen der
Patentierbarkeit, Computerprogramme, industrielle Anwendung usw.

#Goe: Richtlinien für die Formprüfung

#Geh: Richtlinien für die Suche

#Gov: Richtlinien für die Sachprüfung

#Iot: Einführung

#CWA: Inhalt der Europäischen Anmeldung

#oWW: außer den Ansprüchen

#TWa: Die Ansprüche

#Pni: Patentierbarkeit

#Pir: Priorität

#Eto: Prüfungsprozedur

#EIo: Prüfung gemäß IV.1 des Zentralisierungsprotokolls

#Gnr: Allgemeines

#Art: Art. %s

#Iei: Erfindungen

#Esn: Ausnahmen von der Patentierbarkeit

#Iil: Gewerbliche Anwendung

#NWf: Neuheit; Stand der Technik

#CWa: Konflikt mit anderen europäischen Anmeldungen

#Tfo: Neuheitsprüfung

#Nds: Unschädliche Offenbarungen

#Ine: Erfinderische Tätigkeit

#Bea: Grundlegende Erfordernisse für die Patentierbarkeit

#Itr2: Implizite Erfordernisse

#Noe: Kein Erfordernis der Fortschrittlichkeit oder Nützlichkeit

#Tsf: Die Patentierbarkeit setzt voraus, dass alle folgenden vier
grundlegenden Bedingungen erfüllt werden:

#Tbi: Es muss eine %(qc:Erfindung) vorliegen.

#Tcd2: Die Erfindung muss %(qc:gewerblich anwendbar) sein.

#Tie: Die Erfindung muss %(qc:neu) sein.

#Ttx: Die Erfindung muss auf einer %(qc:erfinderischen Tätigkeit) beruhen.

#TaW: Auf diese Bedingungen wird im folgenden in den Abschnitten IV, 2 und
3,4,5 bis 8 bzw 9 eingegangen.

#Ihp: Außer diesen vier grundlegenden Bedingungen hat der Prüfer die beiden
folgenden Erfordernisse zu beachten, die implizit im Übereinkommen und
in der Ausführungsordnung enthalten sind.

#Taa: Die Erfindung muss so beschaffen sein, dass ein Fachmann sie (nach
entsprechender Anleitung durch die Anmeldung) ausführen kann

#toA: dies ergibt sich aus Artikel 83.

#Ini: Fälle, in denen dieses Erfordernis nicht erfüllt ist, sind in II, 4.11
aufgeführt.

#Tce: Die Erfindung muss insoweit technischen Charakter haben, als sie sich
auf ein%(tf:technisches Gebiet) bezieht, %(tp:ihr eine technische
Aufgabe zugrunde liegt) und sie %(tc:technische Merkmale aufweist,
durch deren Angabe der Gegenstand des Schutzbegehrens in den
Patentansprüchen definiert werden kann).  Dieses den %(q:technischen
Charakter) betreffende Erfordernis kann dafür entscheiden sein, ob
eine Erfindung nach Artikel 52(2) und (4) und Artikel 53(b) von der
Patentierbarkeit ausgeschlossen ist.

#RuPa: Regel %d %s

#see: siehe %s

#Trn: Dieses den %(q:technischen Charakter) betreffende Erfordernis kann
dafür entscheidend sein, ob eine Erfindung nach Art 52(2) und (4) und
Art 53(b) von der Patentierbarkeit ausgeschlossen ist.

#Tct: Das Übereinkommen schreibt %(e:nicht) ausdrücklich oder implizit vor,
dass eine Erfindung einen technischen Fortschritt oder sogar eine
nützliche Wirkung mit sich bringen muss, um patentierbar zu sein. 
Jedoch sind gegebenenfalls vorhandene vorteilhafte Wirkungen gegenüber
dem Stand der Technik %(sd:in der Beschreibung anzugeben), und solche
Wirkungen sind für die Bestimmung der %(q:erfinderischen Tätigkeit)
oftmals bedeutsam

#DnW: Definition durch Ausschlüsse

#EbW: Beurteile nicht den Anspruchswortlaut sondern den erfinderischen
Beitrag als solchen

#Tns: Im Übereinkommen ist die Bedeutung des Begriffs %(qc:Erfindung) zwar
nicht festgelegt, aber %(ap:52:2) enthält eine nicht erschöpfende
Aufzählung von Dingen, die nicht als Erfindungen angesehen werden.

#Ssw: Einige Ausnahmen in dieser Aufzählung schließen sowohl etwas
Abstraktes als auch dessen Verkörperung ein

#aWs: eine ästhetische Formschöpfung als etwas Abstraktes oder als Bild,
Skulptur usw

#aec: ein Programm für Datenverarbeitungsanlagen als etwas Abstraktes oder
in Form eines Verfahrens zum Betreiben einer Datenverarbeitungsanlage
oder als Aufzeichnung, z.B. auf einem Magnetband

#Aoe: Etwas %(q:Abstraktes) als solches ist nie patentierbar.

#Myt: Seine Verkörperung in Form von Erzeugnissen oder Verfahren kann
patentierbar sein, sofern die %(e:Erfindung) technische (d.h.
praktische) Merkmale aufweist.

#TnW: Die nachstehenden Beispiel sollen dies verdeutlichen

#seea: siehe auch %s

#TWe: Die einzelnen Punkte der Aufzählung in %(ap:52:2) werden der Reihe
nach behandelt.

#Dce: Entdeckungen

#Sih: Wissenschaftliche Theorien

#MaW: Mathematische Methoden

#Ttt: Hierbei handelt es sich nur um ein besonderes Beispiel für den
Grundsatz, dass rein abstrakte oder intellektuelle Methoden nicht
patentierbar sind.  Ein abgekürztes Dividierverfahren z.B. wäre nicht
patentierbar, eine danach arbeitende Rechenmaschine jedoch schon. 
Eine mathematsche Methode für das Entwerfen von elektrischen Filtern
ist nicht patentierbar; jedoch können Filter, die nach dieser Methode
entworfen worden sind, patentierbar sein, sofern sie ein neues
technisches Merkmal aufweisen, auf das ein Erzeugnisanspruch gerichtet
werden kann.

#Ate: Ästhetische Formschöpfungen

#Aet: Eine ästhetische Formschöpfung bezieht sich der Definition nach auf
ein Erzeugnis (beispielsweise ein Gemälde oder eine Skulptur) mit
Aspekten, die weder praktischer noch funktioneller Art sind und im
wesentlichen subjektiv zu beurteilen sind.  Sollte das Erzeugnis
jedoch auch funktionelle oder technische Merkmale haben, dann könnte
es patentierbar sein, z.B. die Lauffläche eines Reifens.  Der
ästhetische Aspekt selbst ist nicht patentierbar, und zwarweder in
einem Erzeugnis- noch in einem Verfahrensanspruch.  ... Jedoch können
in Fällen, in denen durch eine technische Anordnung oder andere
technische Mittel ein ästhetischer Effekt erzielt wird, zwar nicht der
ästhetische Effekt selbst, aber das Mittel zu dessen Erzielung
patentierbar sein.Beispielsweise könnte ein Gewebe mittels einer
Schichtstruktur, die zuvor nicht zu dem betreffenden Zweck verwendet
wurde, attraktiv gestaltet werden, und in einem solchen Fall könnte
ein Gewebe mit einer derartigen Struktur patentierbar sein. ...

#Sol: Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele
oder für geschäftliche Tätigkeiten

#Taa2: Es handelt sich hier um weitere Beispiele für Dinge mit abstraktem
oder intellektuellem Charakter.  Insbesondere ein Plan zur Erlerung
einer Sprache, ein Verfahren zur Lösung von Kreuzworträtseln, ein
Spiel (als etwas Abstraktes, das durch seine Regeln festgelegt ist)
oder ein Plan zur Organisation einer kommerziellen Tätigkeit würde
nicht patentierbar sein.  Jedoch könnte ein neues Gerät zur
Durchführung eines Spiels oder eines Plans patentierbar sein.

#Pso: Programme für Datenverarbeitungsanlagen

#Aso: Programme für Datenverarbeitungsanlagen können verschiedene Formen
haben, beispielsweise Algorithmen, Flussdiagramme oder Serien
codierter Befehle, die auf einem Band oder anderen maschinenlesbaren
Aufzeichnungsträgern gespeichert werden können; sie können als
Sonderfall entweder für eine mathematische Methode (siehe vorstehend)
oder eine Wiedergabe von Informationen (siehe nachstehend) betrachtet
werden.  Wenn der Beitrag zum bisherigen Stand der Technik lediglich
in einem Programm für Datenverarbeitungsanlagen besteht, ist der
Gegenstand nicht patentierbar, unabhängig davon, in welcher Form er in
den Ansprüchen dargelegt ist.  So wäre z.B. ein Patentanspruch für
eine Datenverarbeitungsanlage, die dadurch gekennzeichnet ist, dass
das besondere Programm in ihr gespeichert ist, oder für ein Verfahren
zum Betrieb einer durch dieses Programm gesteuerten
Datenverarbeitungsanlage in Steuerabhängigkeit von diesem Programm
ebenso zu beanstanden wie ein Patentanspruch für das Programm als
solches oder für das auf Magnettonband aufgenommene Programm.

#Pin: Wiedergabe von Informationen

#Aal: Jede Art der Wiedergabe von Informationen, die lediglich durch den
Inhalt der Informationen gekennzeichnet wird, ist nicht patentierbar. 
Dies gilt sowohl für Patentansprüche, die auf die Wiedergabe von
Informationen an sich (z.B. durch akustische Signale, durch das
gesprochen Wort oder durch visuelle Anzeige) gerichtet sind, als auch
für Patentansprüche, die auf Informationsträgern gespeicherte
Informationen (beispielsweise Bücher, die durch ihren Inhalt,
Schallplatten, die durch das aufgenommene Musikstück, Verkehrszeichen,
die durch ihre Warnhinweise, Magnetbänder von
Datenverarbeitungsanlagen, die durch die auf ihnen gespeicherten Daten
oder Programme gekennzeichnet sind) oder auf Verfahren und
Vorrichtungen zur Wiedergabe von Informationen (z.B. Anzeiger oder
Registriergeräte, die lediglich durch die angezeigten bzw die
registrierte Informationen gekennzeichnet sind) gerichtet sind. 
Schließt die Art der Wiedergabe der Informationen jedoch neue
technische Merkmale ein, so kann der Informationsträger bzw das
Verfahren oder die Vorrichtung für die Wiedergabe der Information
einen patentierbaren Gegenstand bilden.  Die Anordnung oder Art und
Weise der Wiedergabe kann im Unterschied zu dem Informationsgehalt
sehr wohl ein patentierbares technisches Merkmal darstellen. 
Beispiele für solche technischen Merkmale sind: ein Telegraphenapparat
oder ein Nachrichtensystem, das durch die Verwendung eines besonderen
Codes zur Wiedergabe der Buchstaben gekennzeichnet ist (z.B.
Puls-Code-Modulation); ein Messinstrument, das zur Wiedergabe der
durch Messen ermittelten Informationen eine besondere Kurve
aufzeichnet; eine Schallplatte mit besonderen Rillen für
Stereoaufnahme; ein Diapositiv mit seitlich angeordneter Tonspur.

#IWn: Bei der Prüfung der Frage, ob eine Erfindung vorliegt, muss der Prüfer
zwei generelle Punkte berücksichtigen.  Zunächst sollte er die Form
oder die Art des Patentanspruchs außer acht lassen und sich auf den
Inhalt konzentrieren, um festzustellen, welchen neuen Beitrag die
beanspruchte angebliche %(qc:Erfindung) zum Stand der Technik leistet.
 Stellt dieser Beitrag keine Erfindung dar, so liegt kein
patentierbarer Gegenstand vor.  Dieser Sachverhalt ist durch in IV.2.1
aufgefürten Beispiele anhand verschiedener Wege zur Beanspruchung
eines Programms für eine Datenverarbeitungsanlage erläutert.  Ein
weiteres Beispiel: Ist ein gewerbliches Erzeugnis, das seiner Art nach
selbst patentierbar ist, bekannt, so würde ein Patentanspruch auf
dieses Erzeugnis, das sich gegenüber dem Stand der Technik lediglich
durch eine andere Farbe unterscheidet, keine Erfindung darstellen, es
sei denn, dass die gewählte Farbe ein unterschiedliches technisches
(und nicht nur rein ästhetisches) Merkmal mit sich bringt. 
Ausschlüsse von der Patentierbarkeit gelten jedoch nur insoweit, wie
sich die betreffende Anmeldung auf die ausgeschlossenen Gegenstände
oder Tätigkeiten als solche bezieht.

#Ttm: Der Ausschluss kommt also unter Umständen nicht zum Tragen, wenn die
Erfindung auch neue technische Merkmale aufweist.  Dies wird durch die
Beispiele veranschaulicht, die in IV.2.1 unter %(q:ästhetische
Formschöpfungen) angegeben sind.  Darüber hinaus wäre beispielsweise
eine Schallplatte nicht patentierbar, wenn das Besondere daran
lediglich die darauf aufgezeichnete Musik wäre; würde jedoch die Form
der Rille so geändert, dass die Schallplatte bei Verwendung eines
speziellen Tonabnehmers auf eine neuartige Art und Weise funktioniert
(siehe beispielsweise im Falle der ersten Stereoschallplatten), so
könnte ein patentierbarer Gegenstand vorliegen.

#ccc: %(qc:Eine Erfindung gilt als gewerblich anwendbar, wenn ihr Gegenstand
auf irgendeinem gewerblichen Gebiet einschließlich der Landwirtschaft
hergestellt oder benutzt werden kann).  %(qc:Gewerbliches Gebiet) ist
im weitesten Sinne, d.h. so zu verstehen, dass es jede Ausführung
einer Tätigkeit %(q:technischen Charakters) (siehe IV, 1.2)
einschließt, d.h. eine Tätigkeit, die zu den nützlichen und
praktischen Techniken im Unterschied zu den ästhetischen Techniken
gehört; dies bedeutet nicht unbedingt die Verwendung einer Maschine
oder die Herstellung eines Erzeugnisses und könnte sich beispielsweise
auf ein Verfahren zur Auflösung von Nebel oderein Verfahren zur
Umwandlung von Energie von einer Form in eine andere erstrecken.

#Vha: den derzeitigen Prüfungsrichtlinien sehr ähnlich

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo-gl78 ;
# txtlang: de ;
# multlin: t ;
# End: ;

