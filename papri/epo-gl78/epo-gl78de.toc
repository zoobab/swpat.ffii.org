\contentsline {section}{\numberline {1}Richtlinien f\"ur die Formpr\"ufung}{2}{section.1}
\contentsline {section}{\numberline {2}Richtlinien f\"ur die Suche}{2}{section.2}
\contentsline {section}{\numberline {3}Richtlinien f\"ur die Sachpr\"ufung}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Einf\"uhrung}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Inhalt der Europ\"aischen Anmeldung\ (au\ss {}er den Anspr\"uchen)}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Die Anspr\"uche}{3}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Patentierbarkeit}{3}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Allgemeines: Art. 52}{3}{subsubsection.3.4.1}
\contentsline {paragraph}{[ Grundlegende Erfordernisse f\"ur die Patentierbarkeit ]}{3}{section*.3}
\contentsline {paragraph}{[ Implizite Erfordernisse ]}{3}{section*.4}
\contentsline {paragraph}{[ Kein Erfordernis der Fortschrittlichkeit oder N\"utzlichkeit ]}{4}{section*.5}
\contentsline {subsubsection}{\numberline {3.4.2}Erfindungen}{4}{subsubsection.3.4.2}
\contentsline {paragraph}{[ Definition durch Ausschl\"usse ]: Art 52(2)}{4}{section*.6}
\contentsline {paragraph}{[ Beurteile nicht den Anspruchswortlaut sondern den erfinderischen Beitrag als solchen ]}{6}{section*.7}
\contentsline {subsubsection}{\numberline {3.4.3}Ausnahmen von der Patentierbarkeit}{6}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Gewerbliche Anwendung}{6}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}Neuheit; Stand der Technik}{7}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}Konflikt mit anderen europ\"aischen Anmeldungen}{7}{subsubsection.3.4.6}
\contentsline {subsubsection}{\numberline {3.4.7}Neuheitspr\"ufung}{7}{subsubsection.3.4.7}
\contentsline {subsubsection}{\numberline {3.4.8}Unsch\"adliche Offenbarungen}{7}{subsubsection.3.4.8}
\contentsline {subsubsection}{\numberline {3.4.9}Erfinderische T\"atigkeit}{7}{subsubsection.3.4.9}
\contentsline {subsection}{\numberline {3.5}Priorit\"at}{7}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Pr\"ufungsprozedur}{7}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Pr\"ufung gem\"a\ss {} IV.1 des Zentralisierungsprotokolls}{7}{subsection.3.7}
