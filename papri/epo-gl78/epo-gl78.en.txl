<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO 1978: Examination Guidelines

#descr: Adopted by the President of the European Patent Office in accordance
with EPC 10.2a with effect from 1978-06-01.  Excerpts concerning the
question of technical invention, limits of patentability, computer
programs, industrial application etc.

#Goe: Guidelines for Formalities Examination

#Geh: Guidelines for the Search

#Gov: Guidelines for Substantive Examination

#Iot: Introduction

#CWA: Content of European Application

#oWW: other than claims

#TWa: The Claims

#Pni: Patentability

#Pir: Priority

#Eto: Examination Procedure

#EIo: Examination under IV.1 of the Protocol on Centralisation

#Gnr: General

#Art: Art. %s

#Iei: Inventions

#Esn: Exceptions to Patentability

#Iil: Industrial Application

#NWf: Novelty; State of the Art

#CWa: Conflict with other European applications

#Tfo: Test for Novelty

#Nds: Non-prejudicial disclosures

#Ine: Inventive step

#Bea: Basic Requirements for Patentability

#Itr2: Implicit Requirements

#Noe: No Requirement of Progressiveness or Usefulness

#Tsf: There are four basic requirements for patentability:

#Tbi: There must be an %(qc:invention).

#Tcd2: The invention must be %(qc:susceptible of industrial application).

#Tie: The invention must be %(qc:new).

#Ttx: The invention must involve an %(qc:inventive step).

#TaW: These requirements will be dealt with in turn in IV, 2 and 3,4,5 to 8
and 9 respectively.

#Ihp: In addition to these four basic requirements, the examiner should be
aware of the following two requirements that are implicitely contained
in the Convention and in the Regulations:

#Taa: The invention must be such that it can be carried out by a person
skilled in the art (after proper instruction by the application)

#toA: this follows from Article 83.

#Ini: Instances where the invention fails to satisfy this requirement are
given in II, 4.11.

#Tce: The invention must be of %(q:technical character) to the extent that
it must %(tf:relate to a technical field), must %(tp:be concerned with
a technical problem), and must %(tc:have technical features in terms
of which the matter for which protection is sought can be defined in
the claim)

#RuPa: Rule %d, paragraph %s

#see: see %s

#Trn: This requirement of %(q:technical character) may be decisive in
determining whether or not an invention is excluded from patentability
under Article 52, paragraphs 2 and 4 and Article 53, sub-paragraph
(b).

#Tct: The Convention does %(u:not) require explicitly or implicitly that an
invention to be patentable must entail some technical progress or even
any useful effect.  Nevertheless, advantageous effects, if any, with
respect to the state of the art should be %(sd:stated in the
description), and any such effects are often important in determining
%(q:inventive step)

#DnW: Definition through Exclusions

#EbW: Examine the contribution rather than the claim wording!

#Tns: The Convention does not define what is meant by %(qc:invention), but
%(ap:52:2) contains a non-exhaustive list of things which shall not be
regarded as inventions.

#Ssw: Several of the exclusions in this list comprise abstractions as well
as materialisations thereof

#aWs: an aesthetic creation as an abstract entity or as a picture, sculpture
etc

#aec: a computer program as an abstract entity or defined in terms of a
process for operating a computer or as a record, e.g. on magnetic tape

#Aoe: An %(q:abstraction) as such is never patentable.

#Myt: Materialisations as products or processes may be patentable but only
when the %(qc:invention) has technical (i.e. practical) features.

#TnW: The examples given below will help to make this clear

#seea: see also %s

#TWe: The items on the list in %(ap:52:2) will be dealt with in turn.

#Dce: Discoveries

#Sih: Scientific Theories

#MaW: Mathematical Methods

#Ttt: These are a particular example of the principle that purely abstract
ore intellectual methods are not patentable.  For example, a shortcut
method of division would not be patentable but a calculating machine
designed to operate accordingly may well be patentable.  A
mathematical method for designing electrical filters is not
patentable; nevertheless filters designed according to this method
could be patentable provided they have a novel technical feature to
which a product claim can be directed.

#Ate: Aesthetic Creations

#Aet: An aesthetic creation relates by definition to an article (e.g. a
painting or sculpture) having aspects which are other than practical
or functional and the appreciation of which is essentially subjective.
 If, however, the article happens also to have functional or technical
features, it might be patentable, a tyre tread being an example of
this.  The aesthetic effect itself is not patentable, neither in a
product nor in a process claim. ... Nevertheless, if an aesthetic
effect is obtained by a technical structure or other technical means,
although the aesthetic effect itself is not patentable, the means of
obtaining it may be.  For example, a fabric may be provided with an
attractive appearance by means of a layered structure not previously
used for this purpose, in which case a fabric incorporating such
structure may be patentable. ...

#Sol: Schemes, rules and methods for performing mental acts, playing games
or doing business

#Taa2: These are further examples of items of an abstract or intellectual
character.  In particular, a scheme for learning a language, a method
of solving crossword puzzles, a game (as an abstract entity defined by
its rules) or a scheme for organising a commercial operation would not
be patentable.  However, novel apparatus for playing a game or
carrying out a scheme might be patentable.

#Pso: Programs for computers

#Aso: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a mathematical method (see above) or a
presentation or information (see below).  If the contribution to the
known art resides solely in a computer program then the subject matter
is not patentable in whatever manner it may be presented in the
claims.  For example, a claim to a computer characterised by having
the particular program stored in its memory or to a process for
operating a computer under control of the program would be as
objectionable as a claim to the program %(e:per se) or the program
when recorded on magnetic tape.

#Pin: Presentations of information

#Aal: Any presentation of information characterised solely by the content of
the information is not patentable.  This applies whether the claim is
directed to the presentation of the information per se (e.g. by
accoustical signals, spoken words, visual displays), to information
recorded on a carrier (e.g. books characterised by their subject,
gramophone records characterised by the musical piece recorded,
traffic signs characterised by the data or programs recorded), or to
processes and apparatus for presenting information (e.g. indicators or
recorders characterised solely by the information indicated or
recorded).  If, however, the presentation of information has new
technical features there could be patentable subject matter in the
information carrier or in the process or apparatus for presenting the
information.  The arrangement or manner of representation, as
distinguished from the information content, may well constitute a
patentable technical feature.  Examples in which such a technical
feature may be present are: a telegraph apparatus or communication
system characterised by the use of a particular code to represent the
characters (e.g. pulse code modulation); a measuring instrument
designed to produce a particular form of graph for representing the
measured information; a grammophone record characterised by a
particular groove form to allow stereo recordings;  or a diapositive
with a sound-track arranged at the side of it.

#IWn: In considering whether an invention is present there are two general
points the examiner must bear in mind.  Firstly, he should disregard
the form or kind of claim and concentrate on the content in order to
identify the novel contribution which the alleged %(qc:invention)
claimed makes to the known art.  If this contribution does not
constitute an invention, there is not patentable subject matter.  This
point is illustrated by the examples given in %{REF} of different ways
of claiming a computer program.  As another example, if a manufactured
article, which is itself of a kind which is patentable, happens to be
known, then a claim directed to that article and distinguished from
the known art solely by choosing a different colour for the article
would not be for an invention unless the colour chosen provided a
different technical (and not merely aesthetic) feature.  On the other
hand, any exclusion from patentability applies only to the extent that
the application relates to the excluded subject matter or activities
as such.

#Ttm: Thus the exclusion might not apply if the invention also provides new
technical features.  This is illustrated, for instance, by the
examples given in %{REF} under %(qc:aesthetic creations).  As a
further example, a gramophone record distinguished solely by the music
recorded thereon would not be patentable; if however, the form of the
groove were modified so that the record, when used with an appropriate
pick-up mechanism, functioned in a new way (as in the first stereo
record), there could be patentable subject matter.

#ccc: %(qc:An invention shall be considered as susceptible of industrial
application if it can be made or used in any kind of industry,
including agriculture.) %(qc:Industry) should be understood in its
broad sense of including any physical activity of %(tc:technical
character), i.e. an activity which belongs to the useful or practical
arts as distinct from the aesthetic arts; it does not necessarily
imply the use of a machine or the manufacture of an article and could
cover e.g. a process for dispersing fog, or a process for converting
engergy from one form to another. Thus, Article 57 excludes from
patentability very few %(qc:inventions) which are not already excluded
by the list in %{REF}.

#Vha: Very similar to the current examination guidelines.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo-gl78 ;
# txtlang: en ;
# multlin: t ;
# End: ;

