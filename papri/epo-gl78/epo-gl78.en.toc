\contentsline {section}{\numberline {1}Guidelines for Formalities Examination}{2}{section.1}
\contentsline {section}{\numberline {2}Guidelines for the Search}{2}{section.2}
\contentsline {section}{\numberline {3}Guidelines for Substantive Examination}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Introduction}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Content of European Application (other than claims)}{2}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}The Claims}{2}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Patentability}{2}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}General: Art. 52}{2}{subsubsection.3.4.1}
\contentsline {paragraph}{[Basic Requirements for Patentability]}{2}{section*.3}
\contentsline {paragraph}{[Implicit Requirements]}{3}{section*.4}
\contentsline {paragraph}{[No Requirement of Progressiveness or Usefulness]}{3}{section*.5}
\contentsline {subsubsection}{\numberline {3.4.2}Inventions}{3}{subsubsection.3.4.2}
\contentsline {paragraph}{[Definition through Exclusions]: Art 52(2)}{3}{section*.6}
\contentsline {paragraph}{[Examine the contribution rather than the claim wording!]}{5}{section*.7}
\contentsline {subsubsection}{\numberline {3.4.3}Exceptions to Patentability: Art. 53}{6}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Industrial Application: Art. 57}{6}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}Novelty; State of the Art}{6}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}Conflict with other European applications}{6}{subsubsection.3.4.6}
\contentsline {subsubsection}{\numberline {3.4.7}Test for Novelty}{7}{subsubsection.3.4.7}
\contentsline {subsubsection}{\numberline {3.4.8}Non-prejudicial disclosures}{7}{subsubsection.3.4.8}
\contentsline {subsubsection}{\numberline {3.4.9}Inventive step}{7}{subsubsection.3.4.9}
\contentsline {subsection}{\numberline {3.5}Priority}{7}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Examination Procedure}{7}{subsection.3.6}
