\contentsline {section}{\numberline {1}Richtlinien f\"{u}r die Formpr\"{u}fung}{2}{section.1}
\contentsline {section}{\numberline {2}Richtlinien f\"{u}r die Suche}{2}{section.2}
\contentsline {section}{\numberline {3}Richtlinien f\"{u}r die Sachpr\"{u}fung}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Einf\"{u}hrung}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Inhalt der Europ\"{a}ischen Anmeldung (au{\ss }er den Anspr\"{u}chen)}{2}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Die Anspr\"{u}che}{2}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Patentierbarkeit}{2}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Allgemeines: Art. 52}{2}{subsubsection.3.4.1}
\contentsline {paragraph}{[Grundlegende Erfordernisse f\"{u}r die Patentierbarkeit]}{2}{section*.3}
\contentsline {paragraph}{[Implizite Erfordernisse]}{3}{section*.4}
\contentsline {paragraph}{[Kein Erfordernis der Fortschrittlichkeit oder N\"{u}tzlichkeit]}{3}{section*.5}
\contentsline {subsubsection}{\numberline {3.4.2}Erfindungen}{4}{subsubsection.3.4.2}
\contentsline {paragraph}{[Definition durch Ausschl\"{u}sse]: Art 52(2)}{4}{section*.6}
\contentsline {paragraph}{[Beurteile nicht den Anspruchswortlaut sondern den erfinderischen Beitrag als solchen]}{6}{section*.7}
\contentsline {subsubsection}{\numberline {3.4.3}Ausnahmen von der Patentierbarkeit: Art. 53}{6}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Gewerbliche Anwendung: Art. 57}{6}{subsubsection.3.4.4}
\contentsline {subsubsection}{\numberline {3.4.5}Neuheit; Stand der Technik}{7}{subsubsection.3.4.5}
\contentsline {subsubsection}{\numberline {3.4.6}Konflikt mit anderen europ\"{a}ischen Anmeldungen}{7}{subsubsection.3.4.6}
\contentsline {subsubsection}{\numberline {3.4.7}Neuheitspr\"{u}fung}{7}{subsubsection.3.4.7}
\contentsline {subsubsection}{\numberline {3.4.8}Unsch\"{a}dliche Offenbarungen}{7}{subsubsection.3.4.8}
\contentsline {subsubsection}{\numberline {3.4.9}Erfinderische T\"{a}tigkeit}{7}{subsubsection.3.4.9}
\contentsline {subsection}{\numberline {3.5}Priorit\"{a}t}{7}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Pr\"{u}fungsprozedur}{7}{subsection.3.6}
