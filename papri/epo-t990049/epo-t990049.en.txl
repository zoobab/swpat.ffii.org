<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO TBA 2002/03 T 49/99: information modelling not technical,
computer-implementation not new

#descr: In March 2002, a Technical Board of Appeal at the European Patent
Office (EPO) rejects a patent application for a computerised
information modelling system on the grounds that the subject matter is
not an invention according to Art 52 EPC.  The Board argues largely in
the original spirit of the EPO and differs significantly from some
other recent EPO caselaw.  This is an important reason why industrial
patent lawyers are pressing for new patentability legislation.  Under
a CEC/McCarthy directive, EPO decisions such as this one would no
longer be possible.

#WlO: The EPO (or German) jurisdiction did not give up the definition of
%(q:technical) by reference to %(q:use of controllable forces of
nature). Rather, the change consists in the fact that the EPO no
longer insisted that the new part of the teaching be technical and
that the technical part be new.  However not all EPO decisions use the
new laxist doctrine.  Occasionally the pendulum swings back in toward
the original spirit of Art 52 EPC.  This is an important reason why
industrial patent lawyers are pressing for a directive as proposed by
the European Commission and leading politicians in the European
Parliament's Legal Affairs Committee (JURI).  Under such a directive,
EPO decisions such as this one would no longer be possible.

#cii: This decision is not using %(q:both technical and non-technical
features) formula as found in %(cd:CEC directive Article 4 paragraph
3) but rather a reasoning similar to %(ca:CULT Amendment 15):

#ifa: The technical contribution shall be assessed of the difference between
scope of technical features of the patent claim considered as a whole
and the state of the art

#rtW: From reasons for the decision T 49/99:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo-t990049 ;
# txtlang: en ;
# multlin: t ;
# End: ;

