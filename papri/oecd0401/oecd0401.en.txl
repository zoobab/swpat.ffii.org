<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: OECD 2004/01 Report on Patents and Economic Performance

#descr: Conclusions drawn from a series of research activities and conferences
conducted by the Organisation for Economic Cooperation and Development
at the level of ministries of science, innovation and economics with
participation of many economists and patent lawyers.  While being
careful not to offend the faith of patent professionals, the report
does cast doubt on the efficiency of the patent system, particularly
in the areas of genes, software and business methods.

#ndi: Introduction

#oWp: Economic issues raised by patents

#tni: Software and services

#uyW: Conclusion: Policy issues and options

#eoW: Patenting experienced a sizeable boom in the last decade... Business
and public research increasingly use patents to protect their
inventions, and fostering this trend has been the objective of patent
policy in OECD countries over the past two decades, with a view to
encouraging investments in innovation and fostering the dissemination
of knowledge. To what extent has this been the case? What particular
aspects of patent policy in OECD countries can be seen as successful
in this regard, or have there been mainly failures? These questions
are central to this report.

#tcs: Growth in patenting corresponds to a new organisation of research that
is less centred on the individual firm and more based on knowledge
networks and markets: innovation processes throughout the OECD area
have become increasingly competitive, co-operative, globalised, and
more reliant on new entrants and technology-based firms. Market
mechanisms play a more central role in technology diffusion.
Businesses have been demanding more and more patents to accommodate
these new conditions.

#nWn: At the same time, patent regimes themselves have experienced major
changes that have encouraged an increase in patenting. Not only have
new types of inventions (software, genetic, and business methods) been
deemed patentable by some patent offices, but the ability of patent
holders to protect and enforce their rights has also increased,
leading many to call the past two decades a pro-patent policy era.
There is little doubt that many of these policy changes have helped
the patent system to cope with changes in innovation systems by
attracting more private-sector funding for R&D and supporting the
development of markets for technology to help diffuse patented
knowledge. In that sense, the patent system has been instrumental in
the recent waves of innovation which have occurred in the fields of
biotechnology and ICT.

#ica: This strengthening of patent systems in the European Union, Japan and
the United States has, however, raised new concerns and exacerbated
old ones. There have been numerous claims that patents of little
novelty or excessive breadth have been granted, allowing their holders
to extract undue rents from other inventors and from customers. This
has been of particular concern in software, biotechnology and business
methods, where patent offices and courts have had most difficulties in
responding to rapid change, building up institutional expertise,
evaluating prior art and determining correct standards for the breadth
of granted patents. More basically, it has also been asked whether
patentability might hamper the diffusion of knowledge, and therefore
innovation, notably in these new areas. Other concerns have been
raised about access to basic technologies, and research tools, which
seems to have been hindered sometimes by patent holders exercising
their right to exclude. As universities are becoming more likely to
patent and commercialise their own inventions, exemptions for research
use of existing inventions are under threat, with the danger of public
research being faced with rising costs and difficulties of access.

#mrs: Addressing these concerns and ensuring that patent systems continue to
fulfil their mission of both stimulating invention and promoting
diffusion of knowledge requires careful examination of broader issues.
This report summarises OECD work to date on the relationships between
patents, innovation and economic performance. It aims to place major
changes in patenting patterns and patent regimes in the economic
context, and to review the evidence regarding the links between
patenting, innovation and diffusion in areas of particular interest
(PROs, biotechnology, software and services). It provides
policy-relevant conclusions based on existing analysis, and identifies
policy issues and options for further consideration.

#tWr: Viewed from the angle of innovation policy, patents aim to foster
innovation in the private sector by allowing inventors to profit from
their inventions. The positive effect of patents on innovation as
incentive mechanisms has been traditionally contrasted with their
negative effect on competition and technology diffusion. Patents have
long been considered to represent a trade-off between incentives to
innovate on one hand, and competition in the market and diffusion of
technology on the other. However, recent evolutions in science and
technology and patent policy and progress in the economic analysis of
patents have nuanced this view: patents can hamper innovation under
certain conditions and encourage diffusion under others. The impact of
patents on innovation and economic performance is complex, and fine
tuning of patent design is crucial if they are to become an effective
policy instrument.

#raW: Empirical evidence tends to support the effectiveness of patents in
encouraging innovation, subject to some cross-industry variation. In a
series of surveys conducted in the United States, Europe and Japan in
the mid-1980s and 1990s, respondent companies reported patents as
being extremely important in protecting their competitive advantage in
a few industries, notably biotechnology, drugs, chemicals and, to a
certain extent, machinery and computers. Companies in other industries
reported that patents play a secondary, if not negligible, role as a
means of protection for their inventions, as they tend to rely more on
alternative means such as secrecy, market lead, advance on the
learning curve, technological complexity and control of complementary
assets (Levin, Klevorick, Nelson and Winter, 1987; Cohen, Nelson and
Walsh, 2000).

#sam: However, patent protection may also hamper further innovation,
especially when it limits access to essential knowledge, as may be the
case in emerging technological areas when innovation has a marked
cumulative character and patents protect foundational inventions. In
this context, too broad a protection on basic inventions can
discourage follow-on inventors if the holder of a patent for an
essential technology refuses access to others under reasonable
conditions. This concern has often been raised for new technologies,
most recently for genetic inventions (Bar-Shalom and Cook-Deegan,
2002; Nuffield Council on Bioethics, 2002; OECD, 2003a) and software
(Bessen and Maskin, 2000; Bessen and Hunt, 2003).

#fil: In addition, as has long been recognised, the main drawback of patents
is their negative effect on diffusion and competition. As patents are
an exclusive right that creates a temporary monopoly, the patent
holder can set a market price higher than the competitive price and
limit the total volume of sales. This negative impact on competition
could be magnified as patent holders try to strengthen their position
in negotiations with other firms, in an attempt to block access by
competitors to a key technology, or inversely, to avoid being blocked
by them (Shapiro, 2002). Such strategic patenting seems to have
developed over the past 15 years, notably in the electronics industry
(Hall and Ziedonis, 2001).

#nda: Nevertheless, patents can also have a positive impact on competition
when they enhance market entry and firm creation. Not only is there
evidence of small companies being able to assert their right in front
of larger ones thanks to their patent portfolio, but patents may also
be a decisive condition for entrepreneurs to obtain funds from venture
capitalists (Gans, Hsu and Stern, 2002). Moreover, patents may enhance
technology diffusion. Patenting means disclosing inventions which
might otherwise be kept secret. Industrial surveys show that the
reluctance of firms to patent their inventions is primarily due to the
fear of providing information to competitors. This has been confirmed
in the OECD/BIAC survey on the use and perception of patents in the
business community, sent to firms in OECD countries in 2003 and in
which respondents indicated their intensive use of patents as a source
of information (Box 2; Sheehan, Guellec and Martinez, 2003). Patents
also facilitate transactions in markets for technology: they can be
bought and sold as property titles or, more frequently, be subject to
licensing agreements which allow the licensee to use the patented
invention in return for payment of a fee or royalty (Arora, Fosfuri
and Gambardella, 2001; Vonortas, 2003). Finally, enhancing technology
diffusion has been the goal put forward by governments to encourage
universities to patent their inventions, with the objective of
licensing them to businesses that will further develop and
commercialise them (OECD, 2003b).

#afd: In summary, the traditional view of patents as a compromise between
incentives to innovate and barriers to technology diffusion, if not
incorrect, presents a rather partial picture, as patents can either
encourage or deter innovation and diffusion, depending on certain
conditions. In fact, the effect of patents on innovation and diffusion
depends on particular features of the patent regime. Patent subject
matter, patenting requirements and patent breadth are three basic
tools for policy makers involved in the design of patent regimes that
could be used to enhance both innovation and diffusion (Encaoua,
Guellec and Martinez, 2003):

#est: Patent subject matter is the domain of knowledge that can be patented,
if the patenting criteria of novelty, non-obviousness and usefulness
are also met. For instance, scientific discoveries and abstract ideas
are generally excluded. Its definition must be based on a careful
examination of when it is efficient for society to offer patent
protection in addition to other legal or market-based means of
protection.

#tew: Patenting requirement is the height of the inventive step required for
a patent application to be granted. It is understood as the extent of
the contribution made by an invention to the state of the art in a
particular technology field. The higher that contribution, the more
selective the process, thus the lower the number of patents granted.
The lower it is, the larger the likelihood of finding many inventions
with no significant social value. Conversely, too high a requirement
would discourage innovations which, while not being radical, are still
necessary for technological breakthrough to translate into actual
products and processes.

#hWe: The breadth of a patent is the extent of protection granted to patent
holders against imitators and follow-on inventors. Not only do
patentees obtain exclusive rights on their own invention but also on
other inventions which are deemed %(q:functionally equivalent),  and
to a certain extent on improvements of their inventions. Patents that
are too broad allow their holders to %(q:pre-empt the future), while
patents that are too narrow discourage research that feeds into
follow-on inventions.

#nla: The patentability of software-related inventions is currently one of
the most heated areas of debate. Software has become patentable in
recent years in most jurisdictions (although with restrictions in
certain countries, notably those signatories of the European Patent
Convention) and the number of software patents has risen rapidly.
However, there remain fundamental questions about whether software
should be patentable and, if so, whether specific characteristics of
software demand that different rules be applied to ensure that
patenting provides true incentives for innovation, allows follow-on or
incremental innovation and facilitates knowledge diffusion. The
patentability of business methods (often software-based) has further
fuelled the debate, especially as concerns the possibility that low
quality patents might block or impede the fledgling electronic
commerce sector.

#rlr: Since 1998, software-related inventions (and mathematical algorithms
in general) are patentable in the United States as long as they
produce a %(q:useful, concrete and tangible) result, in addition to
the usual criteria (novelty, non-obviousness and industrial
application). However, in Europe and to some extent in Japan, they are
only patentable if %(q:sufficiently technical in nature) (which
excludes business methods in particular), a position which has been
recently confirmed in Europe, although the legislative process is
still ongoing (Hall, 2003; Motohashi, 2003).

#e1t: Following permissive patentability trends, patents for software and
business method inventions have increased rapidly in recent years in
the United States. Various estimates indicate that the number of
software patents granted by the USPTO grew from fewer than 5 000 per
year in 1990 to approximately 20 000 in 2000, or approximately 15% of
all US patents granted in that year (Hunt and Bessen, 2003). In
contrast, business methods patents represent a very low share of the
total number of grants, with around 1 000 grants per year in the US
since 1998. Interestingly, software publishers account for only a
fraction of software patents (only 6% of software patents according to
one recent study) with the majority of software patents owned by large
firms in the ICT manufacturing and electrical machinery sectors. Large
software consultancies and other service-sector firms also account for
a small, but growing, number of patents to date. This pattern reflects
the increasing role of software and services business units within
large ICT firms, as well as the growing pervasiveness of embedded
software in a range of electrical and electronic devices.

#Wno: Growth in software and business methods reflects both increased
innovative activity and changes in patenting behaviours. R&D spending
by software and ICT firms has grown rapidly over the past decade.
Microsoft�s R&D expenditures alone grew from USD 270 million in 1991
to USD 4.4 billion in 2002. More than three-quarters of ICT firms
responding to the OECD/BIAC survey reported that they were generating
more inventions now than ten years ago (Sheehan, Guellec and Martinez,
2003). Nevertheless, the patenting strategies of these firms have also
changed. More than three-quarters of ICT firms in the survey reported
that they now patent technologies they would not have patented a
decade ago even if the technology had been patentable then. Software
and ICT firms see patents as an important bargaining chip in
negotiating alliances with other firms and as a means of generating
additional revenue via licensing. Indeed, more firms in the ICT sector
than in other sectors reported increases in outward licensing and
cross-licensing over the past decade. Other research has also
demonstrated the key role of strategic patenting in the semi-conductor
industry (Hall and Ziedonis, 2001).

#tdi: Does increased patenting for software and business methods stifle
innovation and facilitate anti-competitive behaviour?

#eWa: Software programmes tend to be complex, modular products that combine
multiple functions, each of which may be the subject of a different
patent. Increased patenting may therefore inhibit follow-on innovation
or the assembly of complex programmes as it increases transaction
costs. Interoperability also needs to be high, meaning that open
standards and interfaces are critical to ensuring innovation and
market entry. On the other hand, if patents give more protection, they
also could require more disclosure, which can be helpful for reducing
the exclusion effect generated by patents. Network effects are also
strong in the software sector, and switching costs can be high,
locking-in customers to dominant products, especially if
interoperability cannot be assured. In this context, patents might
contribute to enhancing competition and innovation by allowing new
market entrants to defend their technological position against
incumbents.

#eeW: In summary, when addressing the issue of software protection, the
following points should be considered:

#wsl: As in other areas, patent offices should ensure the quality of
software-related patents. Patents with extremely broad, abstract
claims have sometimes been granted, notably in the field of
Internet-related business methods. Not only should patented inventions
be novel and not excessively broad, but patent documents should also
disclose all the information necessary for a person skilled in the art
to be able to replicate the invention in a reasonable period of time.
The information disclosure requirement should be subject to the same
standards prevalent in other fields of technology, which stress the
importance of publicising patented source code for software-related
inventions.

#Wbt: The interaction of patents and copyright may be an obstacle to the
diffusion of technology in this area, and thus further innovation, as
patents protect the invention whereas copyright forbids the publicity
of the way in which the invention is implemented by forbidding reverse
engineering (Graham and Somaya, 2003). In addition, as copyright
forbids reverse engineering (closed source code is protected as such),
and as software patents do not have to reveal their source code,
disclosure of software knowledge is clearly hampered in the current
IPR setting. This calls for government attention focusing particularly
on the cross effects of copyright and patent, and on insufficient
disclosure requirements in software patents.

#etn: Software is pervasive. Less than 10% of software patents in the US are
granted to software companies. Actually, according to survey data,
between 25 and 40% of business expenditure R&D in all industry has a
software-like outcome, reflecting the fact that many operations which
used to be monitored by mechanical means are now mediated by software.
Hence, a special treatment of software in general regarding IP might
affect patterns of innovation beyond the software industry, and create
unintended effects on the R&D industry-wide.

#blo: Important segments of the software market are moving towards an
open-source approach, which clearly helps disclosure and follow-on
innovation, but the viability of the economic model for open source
software is uncertain. In current open source approaches, attracting
financing for innovation is not as straightforward as with
proprietary, closed source software that is sold in the marketplace.
To date, rewards to open source innovation have been essentially
non-monetary (e.g. reputation) or based on the provision of
complementary services (e.g. customisation, support). It would be
worth exploring whether patent protection could be useful to open
source software developers in creating sustainable business models and
markets for technology, while guaranteeing the disclosure of source
code. One aspect of this question is that patents might provide, as in
other fields, the protection that inventors require to fully disclose
their inventions, a necessary condition for an open source approach.

#WTe: The analyses presented in this report suggest a series of policy
issues and options, and recommended topics for more in-depth analysis
in the future. These concern the development of markets for technology
and the access to basic inventions, as well as the patent system
itself, its principles and the way it works.

#mgo: The paucity of economic evaluation of the patent system is striking.
Most of the changes to patent regimes implemented over the past two
decades were not based on hard evidence or economic analysis. It is
necessary to develop economic analysis in this domain that would
inform the policy debate, giving governments a clearer view beyond the
arguments put forward by pressure groups. Such analysis should rely
notably on quantitative evidence: an effort to build and make
available to analysts the corresponding databases has been initiated
notably by the OECD, but this work needs to be broadened. In addition,
more information is needed on the ways in which patents are used by
their holders, for instance as regards in-house implementation,
licensing contracts and business strategies...

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: caliu ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: oecd0401 ;
# txtlang: en ;
# multlin: t ;
# End: ;

