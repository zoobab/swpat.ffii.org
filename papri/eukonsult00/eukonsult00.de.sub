\begin{subdocument}{eukonsult00}{Europ\"{a}ische Konsultation \"{u}ber die Patentierbarkeit von Computer-Implementierbaren Organisations- und Rechenregeln (= Programmen f\"{u}r Datenverarbeitungsanlagen)}{http://swpat.ffii.org/papiere/eukonsult00/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Am 19. Okt 2000 ver\"{o}ffentlichte die Dienststelle f\"{u}r Gewerblichen Rechtsschutz der Europ\"{a}ischen Kommission (EuDGR) ein Sondierungspapier, welches eine rechtliche Argumentation darlegt, wie das Europ\"{a}ische Patentamt (EPA) sie in den letzten Jahren verwendet hat, um ihre Praxis der Patentierung von Programmen f\"{u}r Datenverarbeitungsanlagen und anderen Organisations- und Rechenregeln gegen den Buchstaben und Geist der geltenden Gesetze zu rechtfertigen.  Die Konsultation richtete sich offenbar an die Patentabteilungen diverser Unternehmen und Verb\"{a}nde und war als ein Man\"{o}ver zu ihrer Mobilisierung konzipiert.  Das Papier selber warb einseitig f\"{u}r den Standpunkt des Europ\"{a}ischen Patentamtes und stellte Fragen, die nur Patentjuristen verstehen und beantworten k\"{o}nnen.  Ferner wurde es von einer ``unabh\"{a}ngigen Studie'' best\"{a}tigt, welche eine bekannte Denkfabrik der Patentbewegung im Auftrag der EuDGR durchgef\"{u}hrt hatte.  Patentjuristen verschiedener Organisationen sandten applaudierende Antworten ein und erkl\"{a}rten dabei das bekannte Credo der Patentbewegung, wonach Patente grunds\"{a}tzlich in allen Gebieten die Innovation f\"{o}rdern und vor allem dem Wohle der kleinen und mittleren Unternehmen dienen.  Allerdings antworteten auch einige Verb\"{a}nde und Firmen sowie \"{u}ber 1000 Einzelpersonen, vor allem Programmierer, mit kritischen Stellungnahmen.  Die EuDGR hat die Stellungnahmen bisher nur schleppend und unvollst\"{a}ndig und in schwer konsultierbarer Form ver\"{o}ffentlicht.  Dem wollen wir abhelfen, und Sie k\"{o}nnen mitmachen.}
\begin{sect}{hist}{Wie es zustande kam}
In summer 2000 the scene was set for legalising software patents in Europe.  For several years, the EU Commission's Industrial Property Unit\footnote{http://europa.eu.int/comm/internal\_market/de/indprop/index.htm} had planned to rubberstamp the European Patent Office's policy of granting patents on rules or organisation and calculation.  The last necessary step was the official removal of all restrictions on patentability from the law or at least a deletion of the ``programs for computers'' in art 52 of the European Patent Convention (EPC).  Moreover, the European Commission was scheduled to become a major player in a future European patent system, which was to be based on Community law rather than on an intergovernmental negotiation process.  As a part of this transition, the European Commission was to issue a software patentability directive.  Under the name of ``clarification'' and ``harmonisation'' the existing legal framework was to be discarded and instead the practise of the European Patent Office was to be legalised and made binding for national courts, many of which had so far refused to follow the EPO on its adventurous course, either by sticking to the letter and spirit of the existing law or by developping their own halfway solutions.  In 1997 the European Commission outlined the contents of a software directive in a ``Greenpaper on the Community Patent'' and inserted it into the rear part of a long resolution text which was rubberstamped by the European Parliament.  Yet, this met with unexpected resistance.  By summer 2000 a Petition for a Software Patent Free Europe had gained the support of 50000 signatures, 200 software companies and numerous politicians of all camps.  Under the impression of these developments, the governments of FR+DE+UK decided not to delete the computer program exclusion for the time being and instead hand over the issue to the European Commission.  The patent law experts at the CEC Patent Unit then opened\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softpatde.htm} a public consultation on 2001-10-15 which was to last until 2001-12-15 and thus cover exactly those weeks, during which the EPC had been scheduled to be changed.  Art 52(2) EPC was as a result left untouched, and instead the patent movement\footnote{http://swpat.ffii.org/analyse/bewegung/index.de.html} was mobilised by means of a specifically focussed consultation call:

The basic question of whether computer programs (computer-implementable rules of organisation and calculation) are inventions in the sense of patent law (art 52 EPC) was not asked.  Instead, the CEC Patent Unit implied that the answer must be yes.  They talked about ``computer-implementable inventions'' and used this term as a synonym for ``computer-implementable rules of calculation and organisation'' rather than for true technical inventions which can be implemented under program control and whose patentability is beyond doubt.  Thus the EU Industrial Property Unit not only implied an answer where it should have asked a question, but also falsely attributed silly positions to the software patent critics, before even opening the consultation.

The accompanying ``independent study'', written by some well-known EU software patent lobby activists\footnote{http://swpat.ffii.org/papiere/clsr-rhart97/index.en.html}, again reiterated the usuall patent lawyer credo in the guise of a somewhat more objective wording.  This led the study to draw conclusions such as ``any proposition to extend the patent system beyond the status quo cannot be claimed to rely on scientific evidence''.  In this context ``status quo'' refers to the practise of the European Patent Office's Technical Boards of Appeal.  The study does not even consider the possibility of criticising that practise and returning to the European Patent Convention.  Nor does it explain the concept of ``technical invention'', which is the underlying spirit of the European Patent Convention.  Moreover the study predicts that software patents will gradually be accepted and used by the open source software community for its own benefit.

In spite of all this care taken to mobilise a network of patent professionals and create the impression of a pro-patent mainstream opinion, the results of the mobilisation campaign did not really look very convincing as a basis for arguing the case of software patentability.

While at the beginning, the CEC Indprop Unit (Industrial Property Unit in the General Directorate for the Internal Market of the Commission of the European Community) still wanted to quickly issue a directive and rallied national governments for this purpose, it gradually turned out that these governments were not very eager to go along.  Also, the CEC Patent Unit itself apparently found that this consultation was not a good basis for their plans.  They reported to the governments that the consultation result showed an ``overwhelming vote of the concerned industries in favor of software patents'' and only a minority, mainly from the ``opensource movement'', was clamoring against software patents.  But they did were not eager to display the results of the consultation on their website, and it was very hard to press them to publish at least a small part of the submissions.  Most submissions have not yet been published, often in spite of repeated requests by the authors to have them published, and those that have been published have been transformed to a PDF format that makes them hard to use in any hypertext-based dialogue.

Thus the work of evaluating the consultation has largely been left to private initiatives like ours.  The CEC Patent Unit has however ordered a summary analysis\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softpatanalyse.htm} from an ``independent contractor'', which has been available internally since April and was released to the public at the beginning of August, together with the installation of a web-based online discussion forum\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softforum.htm}.

The CEC online discussion forum is only web based, without an archive, incompatible with normal posting and referencing and automatisation mechanisms, depending on Javascript, in general poorly accessible, unacceptable for the people who have developped high standards of public communication in newsgroups and mailing lists\footnote{http://swpat.ffii.org/archiv/foren/index.de.html}.  Since the Internet became popular, our representatives and their PR agencies have been trying hard to reinvent the wheel of civil culture, apparently hoping to change its shape from a circle to a square that matches their tv-based communication habits.  Even though the Indprop Unit's attempt to at least engage in some form of dialogue seems commendable, citizens are not to blame for ignoring this type of government-designed ``e-democracy''.  The forum attracted only 50 comments -- none from pro swpat people -- that were later hard to find and died very soon.
\end{sect}

\begin{sect}{anal}{Some examples of the ``Independet Contractor'''s bias}
\begin{sect}{pg8}{Pg. 8:}
Here and elsewhere the report claims that the software patentability critics are unaware of 20000+ software patents granted. May be they just consider them invalid because the law does not allow them and are probably not enforceable.  In fact the report recognizes that almost everybody thinks the situation is ambigous and uncertain.  Little wonder that many people, specially not especialists in law, tend to disregard 20000 patents of uncertain practical value.

The report says that the anti software patent position is ``radical'' and would require ``extensive negotiation''. It is the first time we hear the majority of people in a debate called ``radical'' for defending the enforcement of current law.  We wonder what negotiation would be necessary, but I doubt the purporse of the report was to assess the necessary means to obey the majority. I thought it was intended to summarize the opinions, proposals and demands of the participants.
\end{sect}

\begin{sect}{pg12}{Pg. 12:}
The table that summarizes opinion on the desired patentability subject matter does not give numbers, only vague qualitative descriptions. \begin{quote}
The majority of responses fall into two distinct groups who hold substantially different views
\end{quote} fails to note that one of these two groups is majoritary and the other isn't.  I could also say that ``the majority of opinions of people fall into two distinct groups, those who think that murder is wrong and those that don't''.  It's true, but it doesn't clarify anything. Most people think that murder is wrong.  And I hope we're not looking for a compromise in that.

In 2.1.2 they say it's interesting to see that opponents to software patents use the term ``software patent'' and proponents use ``computer implemented invention patent''. It is not interesting once you realise that the few proponents are patent professionals and patent holders and the opponents are software developers, users or researchers. Little wonder that patent professionals are more used to the term invention and software engineers are more used to the word software rather than to euphemistic EPO slang.

\begin{quote}
{\it The size of the ``petition'' does not necessarily reflect the relative balance of views of the population as a whole}
\end{quote}. Certainly a call for comments is not a statistically planned survey, but obviously the minority in favor of software patents does not represent the majority of the population. I don't think the majority of the population has any position in the issue.  If the purpose of the patents is to promote innovation in some given field, certainly you want to listen to the developers, teachers, students and researchers in the field and not to the lawyers and managers, except when the field is law or management. I don't think lawyers will innovate in software. So probably the population as a whole would vote against software patents if told who advocates for one or the other option. But this is as much speculation as is the report statement.  The only thing we know for sure is that more than 90\percent{} of the people that cared to speak to the EC about software patents do not want them.
\end{sect}

\begin{sect}{pg13}{Pg.13}
I fail to understand the table by country, and would very much welcome an explanantion. It says:

\begin{quote}
{\it The ``open source proportion'' is the proportion of responses that was overtly from ``open source'' lobbies, the vast majority was from the ``petition'' organised by the Eurolinux Alliance.}
\end{quote}

\begin{quote}
{\it ``Patent Support'' is the proportion of responses sent directly to the European Comission that supported patents for computer implemented inventions and were not explicitly from ``open source'' groups.}
\end{quote}

This classification is quite irrelevant to the point of whether respondents supported or not extending the patentability to software, and apparently confuses the notions ``open source'' and ``against software patents'' in an attempt to associate opposition with a phenomenon regarded as minoritary.  On the other hand, the report fails to probe into the political background of the pro-swpat group.  This group apparently belongs to the same small stratum of people to which the organisers of the consultation themselves belong and which they mobilised by the very way in which they put the questions.
\end{sect}
\end{sect}

\begin{sect}{tasks}{Wie Sie helfen k\"{o}nnen}
Wir k\"{o}nnen uns die verbleibende Arbeit teilen.

\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf Look at the consultation results table and find an submission that you think needs further analysis.  What we need most is a plain text version, the \emph{mail} address of the author, and your description or recension of the text.
\begin{enumerate}
\item
Click on the respective PDF files.  Read the PDF documents.

\item
Contact the submittants, ask them to send you text versions of their submssions for publication on our website.

\item
Write a recension.  You may have noted that many of the links on the left side menu are broken.  They are placed there in anticipation of your recension.
\end{enumerate}}}
\filbreak

\item
{\bf {\bf Look at the Eurolinux Consultation\footnote{http://petition.eurolinux.org/consultation} page, write records for some of the more interesting recensions from there, contact the authors and the CEC Industrial Property Unit\footnote{http://europa.eu.int/comm/internal\_market/de/indprop/index.htm} to make sure they are published on the consultation page.}}
\filbreak

\item
{\bf {\bf Make meaningful contributions to the CEC online forum\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softforum.htm} and wherever appropriate, refer your readers to this consultation page (http://www.ffii.org/verein/leute/index.de.html).}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{eval}{Auswertung}
Most of the submissions fall into one of the following categories:
\begin{enumerate}
\item
Small-scale studies from individual researchers/academics, carefully crafted, with original reasoning and references to relevant literature, e.g.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from patent lawyers of large enterprises and industry associations that state the standard articles of faith from the credo of the patent movement and professionally play the mobilisation game of the CEC Industrial Property Unit, usually without more than schematic reference to the special situation of the body which they represent, written by a patent law expert of that body without review or even knowledge of other members of the organisation.  These articles generally show a lack of basic knowledge on the European concept of technical invention and the debate on computer-programs that has been raging since the 60s\footnote{http://swpat.ffii.org/analyse/erfindung/index.de.html}.  Instead they dwell much on the TRIPS fallacy\footnote{http://swpat.ffii.org/analyse/trips/index.de.html} and other well-known patent movement propaganda myths.  It sounds as if these lawyers draw all their knowledge from a limited repertoire, comprising not much more than the ``independent study'' on which this consultation was based.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from patent practicioners and their associations.  Very similar to the above, reflecting the credo of patent circles, without knowledge of the concept of technical invention, without consideration for the risks of unlimited patentability, with naive claims about the economics of software, like the above group, but often a little bit less schematic, sometimes more aggressive.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from other representatives of organisations with varying, less schematic viewpoints, not playing the game of the consultation paper\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/softde.pdf} but rather citing parts of it as appropriate for the (independently structured) argumentation.
\begin{itemize}
\item
...
\end{itemize}

\item
Statements of personal experience and opinion on software patentability, not closely oriented toward the paper
\begin{itemize}
\item
...
\end{itemize}

\item
Short political statements by individuals, most of whom fear negative effects of software patents.
\begin{itemize}
\item
...
\end{itemize}
\end{enumerate}

There are still more than 1300 unpublished statements, most of which probably belong to the last two categories and can be found on the Eurolinux consultation site\footnote{http://petition.eurolinux.org/consultation}.
\end{sect}

\begin{sect}{etc}{Weitere Lekt\"{u}re}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Eurolinux Consultation on Patentability of Computer Programs\footnote{http://petition.eurolinux.org/consultation}}}

\begin{quote}
On 2001-10-19 the European Commission's patent department announced a consultation on the question of software patentability in Europe.  Little later, the Eurolinux Alliance of IT companies and non-profit associations calls on its members and friends to contribute papers to the consultation and, as a basis for the discussion, published a knowledge database containing references to important articles and studies on this subject.  Approximately 1300 persons, mainly programmers and computer professionals but also scholars of various disciplines and normal citizens responded to the call and sent consultation submissions to the EC patent department via the gateway consultation@eurolinux.org.  This gateway served to ensure that the results were published at least on the Eurolinux pages, in case the EC patent department could fail to publish them promptly.
\end{quote}
\filbreak

\item
{\bf {\bf Responses to the British Patent Office's Consultation on Patentability of Computer Programs\footnote{http://www.patent.gov.uk/about/consultations/responses/index.htm}}}

\begin{quote}
Am 19. Okt 2001 k\"{u}ndigte die Dienststelle f\"{u}r Gewerblichen Rechtsschutz (DGR) der Europ\"{a}ischen Kommission eine Konsultation \"{u}ber die Patentierbarkeit von Computerprogrammen an.  Wenig sp\"{a}ter f\"{u}hrte das Britische Patentamt im Namen der britischen Regierung eine \"{a}hnliche Konsultation durch.  Diese Konsultation beruhte auf einer Reihe von Fragen, die, aners als die der EUK-DGR, sich nicht nur an Patentjuristen wandte sondern, wenigstens zum Teil, tats\"{a}chlich ein Konsultationsangebot auch f\"{u}r Informatiker darstellten.  Auch die juristischen Fragen waren nicht eng an die Sichtweise des Europ\"{a}ischen Patentamtes angebunden sondern schlossen die der britischen Patentgerichtsbarkeit mit ein.  Die britische Patentjurisprudenz kannte nie einen systematischen Begriff der technischen Erfindung, wie er von der kontinentalen (insb deutschen) Jurisprudenz der 70er und 80er Jahre ausgearbeitet wurde.  Sie beruhte auf einem Fallrecht, welches relativ stabil blieb und den Briten erlaubte, die extremen Entwicklungen des EPA von einer gewissen Distanz aus zu betrachten.  Somit waren die Briten relativ gut ger\"{u}stet, um eine objektivere und ehrlichere Konsultation durchzuf\"{u}hren.  Beim Durchbl\"{a}ttern der wohl-geordneten und interessanten britischen Konsultationseingaben werden Sie vielleicht dankbar anerkennend feststellen, dass sie dabei gute Arbeit geleistet haben. 
\end{quote}
\filbreak

\item
{\bf {\bf 2tes Treffen der EUK-Dienststelle f\"{u}r Gewerblichen Rechtsschutz mit einer Eurolinux-Delegation\footnote{http://www.eurolinux.org/news/euip017/index.de.html}}}

\begin{quote}
Zum zweiten Mal treffen Vertreter des Eurolinux-B\"{u}ndnisses Beamte, die an einem Entwurf f\"{u}r eine Europ\"{a}ische Richtlinie zur Patentierbarkeit von computer-implementierbaren Organisations- und Rechenregeln (Programmen f\"{u}r Datenverarbeitungsanlagen) arbeiten.  Bisherige Entw\"{u}rfe der Dienststelle deuten daraufhin, dass man sog. Softwarepatente legalisieren will.  Angesichts massiver Proteste aus der Softwarebranche veranstaltete die Dienststelle im Herbst 2000 eine Konsultation, die zu zahlreichen schriftlichen Eingaben f\"{u}hrte, aber nicht unbedingt in gleichem Ma{\ss}e neue Erkenntnisse zu Tage f\"{o}rderte.  Einer Masse von patentkritischen Softwareentwicklern steht eine geringe Zahl von patentbegeisterten Patentjuristen gegen\"{u}ber, die allerdings zum Teil im Namen gro{\ss}er Organisationen sprechen.  Von seiten der EU-Kommission wurde bisher kein weiterer Dialog organisiert.  Deshalb ist die Eurolinux-Allianz besonders gl\"{u}cklich, nun eine Gelegenheit zu einem Meinungsaustausch zu haben.l
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux-Appell an die Br\"{u}sseler Patentgesetzgeber\footnote{http://www.eurolinux.org/news/101/index.de.html}}}

\begin{quote}
Im M\"{a}rz 2001 sandte das Eurolinux-B\"{u}ndnis einen Brief an die Dienststelle f\"{u}r Gewerblichen Rechtsschutz bei der Europ\"{a}ische Kommission (EuK-DGR) mit der Bitte, die offenbar abgebrochene Konsultation \"{u}ber die Patentierbarkeit von computer-implementierbaren Organisations- und Rechenregeln wieder aufzunehmen.  Der Brief formuliert ferner einige Fragen und schl\"{a}gt einen methodologischen Rahmenwerk f\"{u}r weitere Diskussionen und Forschungen vor.  Damit reagierte das Eurolinux-B\"{u}ndnis auf interne Positionspapiere, in welchen die EuK-DGR sich zweifelhafter Methoden bediente, indem sie etwa die Konsultationsergebnisse versteckte und misinterpretierte, um nationale Regierungen dazu zu bewegen, ihr Projekt der Legalisierung von Softwarepatenten in Europa zu unterst\"{u}tzen.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente\footnote{http://swpat.ffii.org/termine/2001/bundestag/index.de.html}}}

\begin{quote}
Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte \"{O}ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver\"{o}ffentlichen hier das Gespr\"{a}chsprotokoll und die schriftlichen Eingaben.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente: Die faulen Tricks der DG15\footnote{http://www.dignatz.de/d/spotlight/artikel/swpatents\_dg15\_cw\_20010831\_001.html}}}

\begin{quote}
Gastkommentar von Eitel Dignatz\footnote{http://www.dignatz.de/} in http://www.computerwoche.de/
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente: Diskussions- und Nachrichtenverteiler\footnote{http://swpat.ffii.org/archiv/foren/index.de.html}}}

\begin{quote}
In the Internet age, high quality interactive consultations are even occurring when no government agency ``launches'' them.  Several mailing lists on patent affairs have had very lively discussions on the subject of this consultation.  Some of these lists have archives, where you can study for yourself, whether pro software patentability arguments have ever been able to stand the test of public debate.   Our experience is that this is nowhere the case, not even on the mailing list of the European Patent Office.
\end{quote}
\filbreak

\item
{\bf {\bf Robert Hart 1997: The Case for Patent Protection for Computer Program-Related Inventions\footnote{http://swpat.ffii.org/papiere/clsr-rhart97/index.en.html}}}

\begin{quote}
Patent lawyers try to create the impression that the United States introduced software patents after a conscious, public-opinion based debate. One of the most outspoken pro software patent activists at the European Commission, Robert Hart, argued the case in 1997 by misrepresenting the positions of some major patent-critical voices as being pro software patent. Hart later co-authored an ``independent study'' at the order of the European Commission.  Both use approximately the same methodology.
\end{quote}
\filbreak

\item
{\bf {\bf Die versteckten Patentpl\"{a}ne der Europ\"{a}ischen Kommission\footnote{http://www.eurolinux.org/news/agenda/index.de.html}}}

\begin{quote}
Jean-Paul Smets schreibt im Fr\"{u}hjahr 2000: Die Dienststelle f\"{u}r Gewerblichen Rechtsschutz in der Europ\"{a}ischen Kommission hat ein in sich stimmiges Geflecht von Scheinargumenten f\"{u}r Softwarepatente aufgebaut.  Sie lehnt sich dabei eng an eine neue Ideologie des geistigen Eigentums an, die von einflussreichen Kreisen in Washington seit einiger Zeit als nationale Interessenpolitik der USA vorangetrieben wird.  Wenn die Dienststelle f\"{u}r Gewerblichen Rechtsschutz sich durchsetzt, wird als n\"{a}chstes zwangsl\"{a}ufig die Legalisierung der Patentierbarkeit von Gesch\"{a}ftsverfahren und geistigen Verfahren auf die Tagesordnung kommen.
\end{quote}
\filbreak

\item
{\bf {\bf Pl\"{a}ne f\"{u}r eine Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/cr-bmueller00/index.de.html}}}

\begin{quote}
Der Autor ist federf\"{u}hrend beim Entwurf der neuen Richtlinie t\"{a}tig.  Er meint, Europa solle sich in Richtung auf amerikanische Verh\"{a}ltnisse bewegen.  Als Begr\"{u}ndung f\"{u}hrt er die Meinung von 44 zu Fragen der Rechtssystematik befragten Patentjuristen an.
\end{quote}
\filbreak

\item
{\bf {\bf EuroLinux Trifft Br\"{u}sseler Gesetzgeber\footnote{http://www.eurolinux.org/news/euipCA/index.de.html}}}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{http://swpat.ffii.org/analyse/erfindung/index.de.html}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

