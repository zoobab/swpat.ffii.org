\begin{subdocument}{eukonsult00}{European Consultation on the Patentability of Computer-Implemented Rules of Organisation and Calculation (= Programs for Computers)}{http://swpat.ffii.org/papers/eukonsult00/eukonsult00.en.html}{Workgroup\\swpatag@ffii.org}{On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.}
\begin{sect}{hist}{How it came about}
In summer 2000 the scene was set for legalising software patents in Europe.  For several years, the EU Commission's Industrial Property Unit\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/index.htm} had planned to rubberstamp the European Patent Office's policy of granting patents on rules or organisation and calculation.  The last necessary step was the official removal of all restrictions on patentability from the law or at least a deletion of the ``programs for computers'' in art 52 of the European Patent Convention (EPC).  Moreover, the European Commission was scheduled to become a major player in a future European patent system, which was to be based on Community law rather than on an intergovernmental negotiation process.  As a part of this transition, the European Commission was to issue a software patentability directive.  Under the name of ``clarification'' and ``harmonisation'' the existing legal framework was to be discarded and instead the practise of the European Patent Office was to be legalised and made binding for national courts, many of which had so far refused to follow the EPO on its adventurous course, either by sticking to the letter and spirit of the existing law or by developping their own halfway solutions.  In 1997 the European Commission outlined the contents of a software directive in a ``Greenpaper on the Community Patent'' and inserted it into the rear part of a long resolution text which was rubberstamped by the European Parliament.  Yet, this met with unexpected resistance.  By summer 2000 a Petition for a Software Patent Free Europe had gained the support of 50000 signatures, 200 software companies and numerous politicians of all camps.  Under the impression of these developments, the governments of FR+DE+UK decided not to delete the computer program exclusion for the time being and instead hand over the issue to the European Commission.  The patent law experts at the CEC Patent Unit then opened\footnote{http://indprop-en-dir/softpaten.htm} a public consultation on 2001-10-15 which was to last until 2001-12-15 and thus cover exactly those weeks, during which the EPC had been scheduled to be changed.  Art 52(2) EPC was as a result left untouched, and instead the patent movement\footnote{http://swpat.ffii.org/analysis/movement/swpatlijda.en.html} was mobilised by means of a specifically focussed consultation call:

\begin{itemize}
\item
{\bf {\bf The Economic Impact of Patentability of Computer Programs\footnote{http://swpat.ffii.org/papers/indprop-ipi00/indprop-ipi00.en.html}}}

\begin{quote}
A patent advocacy text by the London Intellectual Property Institute, ordered by the Industrial Property Unit at the European Commission (CEC Indprop), finished in spring 2000, held back until Oct 2000.  The name is misleading:  this is not an economic study.  There is only one chapter which deals with economics but even this chapter only roughly summarises third parties's works.   Basically this pseudo-study only restates well-known beliefs of civil servants from the british patent establishment who at the time were in charge of the European Commission's Industrial Property Unit at DG Markt (Directorate General for the Internal Market).  Yet, while main author Robert Hart is a well known patent lawyer and lobbyist, the economics chapter was written by an outsider, Peter Holmes.  It provides evidence to show that software patents have damaging effects on economic development and tries to balance this by adding some unreasoned statements in favor of software patents.  Holmes later explained that he had no other choice in view of the ``convictions'' of his partners.  Yet the CEC Indrop people did not like the study: they locked it away for half a year.  During this time the European patent establishment was preparing to rewrite Art 52 EPC so as to remove all limits of patentability.  In October 2000, after the plans been dropped, CEC/Indprop suddenly published the IPI treatise and used it as a basis for a ``consultation exercise''.  From then on, various pro software patent proposals from Brussels have again and again relied on this ``economic study'' for justification.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents - Commission launches Consultations\footnote{http://indprop-en-dir/softpaten.htm}}}
\filbreak

\item
{\bf {\bf CEC Indprop 2000-10-19: Swpat Consultation Paper\footnote{http://hsub/comp/softfr.pdf}}}

\begin{quote}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which echoes the rhetoric used by the European Patent Office in recent years to justify its practise of granting software patents against the letter and spirit of the EPC, and has called on companies and industry associations to comment on this reasoning.  The questions were about legal technicalities which understandable and interesting only for the patent lawyer friends of the IndProp unit, whose mobilisation was apparently the chief purpose of the consultation.
\end{quote}
\filbreak

\item
{\bf {\bf Replies to the EC swpat consultation\footnote{http://indprop-en-dir/softreplies.htm}}}
\filbreak

\item
{\bf {\bf Halbersztadt 2001/08/04: Analysis of Replies to the Consultation Paper\footnote{http://aful.org/wws/arc/patents/2001-08/msg00011.html}}}

\begin{quote}
Some additional numerical analysis to a report produced by PbT Consultants.

\begin{center}
\begin{tabular}{R{44}L{44}}
For Software Patents 85 responses from 1447 & 6\percent{}\\
Against Software Patent\\
\end{tabular}
\end{center}

Against in groups:

\begin{center}
\begin{tabular}{R{44}L{44}}
Individuals & 98,5\percent{}\\
SMEs & 95\percent{}\\
Large Enterprises & 81\percent{}\\
Associations & 45\percent{}\\
User & 99,6\percent{}\\
Student & 99,5\percent{}\\
Academic & 98,0\percent{}\\
Software Developer & 95,8\percent{}\\
IP Professional & 33\percent{}\\
Governmental & 22\percent{}\\
\end{tabular}
\end{center}
\end{quote}
\filbreak

\item
{\bf {\bf Halbersztadt 2001/08/12: Could you amend the statistics if they are wrong?\footnote{http://aful.org/wws/arc/patents/2001-08/msg00013.html}}}

\begin{quote}
The chief author of the evaluation report, Stewerd Davidson of PbT Consultants, had responded to Joz\`{e}f Halbersztadt, polish patent examiner, cautioning that Halbersztadt's method of statistical evaluation is not appropriate because the consultation was not a Gallup poll.  Instead it would be appropriate to just characterise ``the two viewpoints'' and let the reader weigh them.  Halbersztadt responds to this, charging (1) that the report is rich in numbers and tries to manipulate opinion with statistics anyway, (2) the attempted characterisation of viewpoints is shallow and also manipulative.
\end{quote}
\filbreak
\end{itemize}

The basic question of whether computer programs (computer-implemented rules of organisation and calculation) are inventions in the sense of patent law (art 52 EPC) was not asked.  Instead, the CEC Patent Unit implied that the answer must be yes.  They talked about ``computer-implemented inventions'' and used this term as a synonym for ``computer-implemented rules of calculation and organisation'' rather than for true technical inventions which can be implemented under program control and whose patentability is beyond doubt.  Thus the EU Industrial Property Unit not only implied an answer where it should have asked a question, but also falsely attributed silly positions to the software patent critics, before even opening the consultation.

The accompanying ``independent study'', written by some well-known EU software patent lobby activists\footnote{http://swpat.ffii.org/papers/clsr-rhart97/clsr-rhart97.en.html}, again reiterated the usuall patent lawyer credo in the guise of a somewhat more objective wording.  This led the study to draw conclusions such as ``any proposition to extend the patent system beyond the status quo cannot be claimed to rely on scientific evidence''.  In this context ``status quo'' refers to the practise of the European Patent Office's Technical Boards of Appeal.  The study does not even consider the possibility of criticising that practise and returning to the European Patent Convention.  Nor does it explain the concept of ``technical invention'', which is the underlying spirit of the European Patent Convention.  Moreover the study predicts that software patents will gradually be accepted and used by the open source software community for its own benefit.

In spite of all this care taken to mobilise a network of patent professionals and create the impression of a pro-patent mainstream opinion, the results of the mobilisation campaign did not really look very convincing as a basis for arguing the case of software patentability.

While at the beginning, the CEC Indprop Unit (Industrial Property Unit in the General Directorate for the Internal Market of the Commission of the European Community) still wanted to quickly issue a directive and rallied national governments for this purpose, it gradually turned out that these governments were not very eager to go along.  Also, the CEC Patent Unit itself apparently found that this consultation was not a good basis for their plans.  They reported to the governments that the consultation result showed an ``overwhelming vote of the concerned industries in favor of software patents'' and only a minority, mainly from the ``opensource movement'', was clamoring against software patents.  But they did were not eager to display the results of the consultation on their website, and it was very hard to press them to publish at least a small part of the submissions.  Most submissions have not yet been published, often in spite of repeated requests by the authors to have them published, and those that have been published have been transformed to a PDF format that makes them hard to use in any hypertext-based dialogue.

Thus the work of evaluating the consultation has largely been left to private initiatives like ours.  The CEC Patent Unit has however ordered a summary analysis\footnote{http://indprop-en-dir/softpatanalyse.htm} from an ``independent contractor'', which has been available internally since April and was released to the public at the beginning of August, together with the installation of a web-based online discussion forum\footnote{http://indprop-en-dir/softforum.htm}.

The CEC online discussion forum is only web based, without an archive, incompatible with normal posting and referencing and automatisation mechanisms, depending on Javascript, in general poorly accessible, unacceptable for the people who have developped high standards of public communication in newsgroups and mailing lists\footnote{http://swpat.ffii.org/archive/forum/swpatmrilu.en.html}.  Since the Internet became popular, our representatives and their PR agencies have been trying hard to reinvent the wheel of civil culture, apparently hoping to change its shape from a circle to a square that matches their tv-based communication habits.  Even though the Indprop Unit's attempt to at least engage in some form of dialogue seems commendable, citizens are not to blame for ignoring this type of government-designed ``e-democracy''.  The forum attracted only 50 comments -- none from pro swpat people -- that were later hard to find and died very soon.
\end{sect}

\begin{sect}{anal}{Some examples of the ``Independet Contractor'''s bias}
\begin{sect}{pg8}{page 8}
Here and elsewhere the report claims that the software patentability critics are unaware of 20000+ software patents granted. May be they just consider them invalid because the law does not allow them and are probably not enforceable.  In fact the report recognizes that almost everybody thinks the situation is ambigous and uncertain.  Little wonder that many people, specially not specialists in law, tend to disregard 20000 patents of uncertain practical value.

The report says that the anti software patent position is ``radical'' and would require ``extensive negotiation''. It is the first time we hear the majority of people in a debate called ``radical'' for defending the enforcement of current law.  We wonder what negotiation would be necessary, but we doubt the purporse of the report was to assess the realisability of the participants' demands.  We thought it was intended to summarize the opinions, proposals and demands of the participants, so that the interests at stake can be weighed more accurately.
\end{sect}

\begin{sect}{pg12}{page 12}
The table that summarizes opinion on the desired patentability subject matter does not give numbers, only vague qualitative descriptions. \begin{quote}
The majority of responses fall into two distinct groups who hold substantially different views
\end{quote} fails to note that one of these two groups is majoritary and the other isn't.   We could also say that ``the majority of opinions of people fall into two distinct groups, those who think that murder is wrong and those that don't''.  It's true, but it doesn't clarify anything. Most people think that murder is wrong.  And we hope we're not looking for a compromise in that.

In 2.1.2 they say it's interesting to see that opponents to software patents use the term ``software patent'' and proponents use ``computer implemented invention patent''. It is not interesting once you realise that the few proponents are patent professionals and patent holders and the opponents are software developers, users or researchers. Little wonder that patent professionals are more used to the term invention and software engineers are more used to the word software rather than to euphemistic EPO slang.

\begin{quote}
{\it The size of the ``petition'' does not necessarily reflect the relative balance of views of the population as a whole}
\end{quote}. Certainly a call for comments is not a statistically planned survey, but obviously the minority in favor of software patents does not represent the majority of the population. We don't think the majority of the population has any position in the issue.  If the purpose of the patents is to promote innovation in some given field, certainly you want to listen to the developers, teachers, students and researchers in the field and not to the lawyers and managers, except when the field is law or management.  We don't think lawyers will innovate in software. So probably the population as a whole would vote against software patents if told who advocates for one or the other option. But this is as much speculation as is the report statement.  The only thing we know for sure is that more than 90\percent{} of the people that cared to speak to the EC about software patents do not want them.
\end{sect}

\begin{sect}{pg13}{page 13}
I fail to understand the table by country, and would very much welcome an explanantion. It says:

\begin{quote}
{\it The ``open source proportion'' is the proportion of responses that was overtly from ``open source'' lobbies, the vast majority was from the ``petition'' organised by the Eurolinux Alliance.}
\end{quote}

\begin{quote}
{\it ``Patent Support'' is the proportion of responses sent directly to the European Comission that supported patents for computer implemented inventions and were not explicitly from ``open source'' groups.}
\end{quote}

This classification is quite irrelevant to the point of whether respondents supported or not extending the patentability to software, and apparently confuses the widespread and diverse opposition against software patentability (and for software copyright) with ``open source'' advocacy and even with paid lobbying, in an attempt to associate the opposition with a phenomenon regarded as minoritarian.  On the other hand, the report fails to probe into the political background of the pro-patent group.  This group apparently belongs to the same small stratum of people to which the organisers of the consultation themselves belong and which they tried mobilised by the very way in which they organised their consultation.
\end{sect}
\end{sect}

\begin{sect}{tasks}{How you can help}
Let's do the remaining work together.

\begin{itemize}
\item
{\bf {\bf How you can help us stop Software Patents\footnote{http://swpat.ffii.org/group/todo/swpatgunka.en.html}}}
\filbreak

\item
{\bf {\bf Look at the consultation results table and find an submission that you think needs further analysis.  What we need most is a plain text version, the \emph{mail} address of the author, and your description or recension of the text.
\begin{enumerate}
\item
Click on the respective PDF files.  Read the PDF documents.

\item
Contact the submittants, ask them to send you text versions of their submssions for publication on our website.

\item
Write a recension.  You may have noted that many of the links on the left side menu are broken.  They are placed there in anticipation of your recension.
\end{enumerate}}}
\filbreak

\item
{\bf {\bf Look at the Eurolinux Consultation\footnote{http://petition.eurolinux.org/consultation} page, write records for some of the more interesting recensions from there, contact the authors and the CEC Industrial Property Unit\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/index.htm} to make sure they are published on the consultation page.}}
\filbreak

\item
{\bf {\bf Make meaningful contributions to the CEC online forum\footnote{http://indprop-en-dir/softforum.htm} and wherever appropriate, refer your readers to this consultation page (http://swpat.ffii.org/papers/eukonsult00/eukonsult00.en.html).}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{eval}{Evaluation}
Most of the submissions fall into one of the following categories:
\begin{enumerate}
\item
Small-scale studies from individual researchers/academics, carefully crafted, with original reasoning and references to relevant literature, e.g.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from patent lawyers of large enterprises and industry associations that state the standard articles of faith from the credo of the patent movement and professionally play the mobilisation game of the CEC Industrial Property Unit, usually without more than schematic reference to the special situation of the body which they represent, written by a patent law expert of that body without review or even knowledge of other members of the organisation.  These articles generally show a lack of basic knowledge on the European concept of technical invention and the debate on computer-programs that has been raging since the 60s\footnote{http://swpat.ffii.org/analysis/invention/swpatkorcu.en.html}.  Instead they dwell much on the TRIPS fallacy\footnote{http://swpat.ffii.org/analysis/trips/swpattrips.en.html} and other well-known patent movement propaganda myths.  It sounds as if these lawyers draw all their knowledge from a limited repertoire, comprising not much more than the ``independent study'' on which this consultation was based.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from patent practicioners and their associations.  Very similar to the above, reflecting the credo of patent circles, without knowledge of the concept of technical invention, without consideration for the risks of unlimited patentability, with naive claims about the economics of software, like the above group, but often a little bit less schematic, sometimes more aggressive.
\begin{itemize}
\item
...
\end{itemize}

\item
Position papers from other representatives of organisations with varying, less schematic viewpoints, not playing the game of the consultation paper\footnote{http://hsub/comp/softfr.pdf} but rather citing parts of it as appropriate for the (independently structured) argumentation.
\begin{itemize}
\item
...
\end{itemize}

\item
Statements of personal experience and opinion on software patentability, not closely oriented toward the paper
\begin{itemize}
\item
...
\end{itemize}

\item
Short political statements by individuals, most of whom fear negative effects of software patents.
\begin{itemize}
\item
...
\end{itemize}
\end{enumerate}

There are still more than 1300 unpublished statements, most of which probably belong to the last two categories and were initially published on the Eurolinux consultation site\footnote{http://petition.eurolinux.org/consultation}.
\end{sect}

\begin{sect}{links}{Further Reading}
\begin{itemize}
\item
{\bf {\bf Responses to the British Patent Office's Consultation on Patentability of Computer Programs\footnote{http://www.patent.gov.uk/about/consultations/responses/index.htm}}}

\begin{quote}
On 2001-10-19 the European Commission's Industrial Property Unit announced a consultation on the question of software patentability in Europe.  Little later, the United Kingdom's Patent Office hosted an independent consultation on behalf of the British government.  This consultation was based on a set of questions that were, unlike those of the European Commission's Industrial Property Unit, not only directed to patent lawyers but, at least in part, quite suitable to be answered by computer professionals.  Also, the consultation questions were not narrowly based on jurisprudence of the European Patent Office but included that of the UKPTO and British courts.  British jurisprudence never had a systematic definition of technical invention such as the ones worked out by continental jurisprudence in the 70s and early 80s.  They relied more on ad hoc caselaw solutions and yet somehow managed to keep their caselaw fairly stable, watching the extreme changes at the EPO from a certain distance.  Thus the UKTPO was in a fairly good position to conduct a more objective consultation this time, and when you browse through their neatly ordered and interesting consultation web pages, you may want to applaud them for doing a good job.
\end{quote}
\filbreak

\item
{\bf {\bf 2nd meeting of the European Commission's Industrial Property Unit with a Eurolinux delegation\footnote{http://www.eurolinux.org/news/euip017/euip017.en.html}}}

\begin{quote}
For the second time, representatives of the Eurolinux Alliance are meeting officials who are in charge of drafting a European Directive on the Patentability of Computer-Implementable Rules of Organisation and Calculation (Programs for Computers).  A preliminary outline of a possible directive draft, published on 2000-10-19, proposed to legalise such patents and asked a series of questions concerning details of this approach. The consultation yielded written statements, often with well known faith-based arguments, and also with a well known distribution of forces:  a large number of individual software developpers, mostly speaking for themselves, against a small number of patent lawyers, mostly speaking in the name of big organisations.  So far no further questioning and dialogue has been organised.  The Eurolinux Alliance is therefore happy to have the chance to meet the consultation organisers and help pave the way for an in-depth discussion.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux letter to the EU patent legislators\footnote{http://www.eurolinux.org/news/101/euluxpr0101.en.html}}}

\begin{quote}
In late february 2001, the Eurolinux Alliance sent a letter to the European Commission's Industrial Property Unit (ECIPU), asking them to resume consultations on computer-implementable rules of organisation and calculation, which was seen to have been interrupted, and proposing a few questions and a methodological framework for further discussions and research.  Therebey the Eurolinux Alliance reacts to an internal position papers in which the ECIPU seemed to have adopted unsound methods, such as hiding and misinterpreting consultation results in order to push national governments to support their project of legalising software patents in Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament\footnote{http://swpat.ffii.org/events/2001/bundestag/swpbtag016.en.html}}}

\begin{quote}
Eight experts from the areas of law, informatics and economics will answer questions from MPs, based on written responses to a set of questions.  The interested public is also called to present its answers to any subset of these questions in writing.  We publish here the procedings and submissions.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente: Die faulen Tricks der DG15\footnote{http://www.dignatz.de/d/spotlight/artikel/swpatents\_dg15\_cw\_20010831\_001.html}}}

\begin{quote}
Eitel Dignatz\footnote{http://www.dignatz.de/} column in http://www.computerwoche.de/
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents: Discussion Rounds and News Channels\footnote{http://swpat.ffii.org/archive/forum/swpatmrilu.en.html}}}

\begin{quote}
In the Internet age, high quality interactive consultations are even occurring when no government agency ``launches'' them.  Several mailing lists on patent affairs have had very lively discussions on the subject of this consultation.  Some of these lists have archives, where you can study for yourself, whether pro software patentability arguments have ever been able to stand the test of public debate.   Our experience is that this is nowhere the case, not even on the mailing list of the European Patent Office.
\end{quote}
\filbreak

\item
{\bf {\bf Robert Hart 1997: The Case for Patent Protection for Computer Program-Related Inventions\footnote{http://swpat.ffii.org/papers/clsr-rhart97/clsr-rhart97.en.html}}}

\begin{quote}
Patent lawyers try to create the impression that the United States introduced software patents after a conscious, public-opinion based debate. One of the most outspoken pro software patent activists at the European Commission, Robert Hart, argued the case in 1997 by misrepresenting the positions of some major patent-critical voices as being pro software patent. Hart later co-authored an ``independent study'' at the order of the European Commission.  Both use approximately the same methodology.
\end{quote}
\filbreak

\item
{\bf {\bf The Hidden Patent Agenda of the European Commission\footnote{http://www.eurolinux.org/news/agenda/smets-agenda.en.html}}}

\begin{quote}
Jean-Paul Smets writes in spring 2000: The European Commission's Directorate for the Internal Market has built a consistent network of fallacies to argue in favour of software patents.  It is closely following a new proprietarist ideology championed by the United States government.  Expect the Directorate to push patents on intellectual methods in the near future.
\end{quote}
\filbreak

\item
{\bf {\bf Bernhard M|ller 2000: Plans for a Software Patent Directive\footnote{http://swpat.ffii.org/papers/cr-bmueller00/cr-bmueller00.de.html}}}

\begin{quote}
Der Autor ist federf|hrend beim Entwurf der neuen Richtlinie tdtig.  Er meint, Europa solle sich in Richtung auf amerikanische Verhdltnisse bewegen.  Als Begr|ndung f|hrt er die Meinung von 44 zu Fragen der Rechtssystematik befragten Patentjuristen an.
\end{quote}
\filbreak

\item
{\bf {\bf EuroLinux Meets EU Legislators\footnote{http://www.eurolinux.org/news/euipCA/euipCA.en.html}}}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/analysis/invention/swpatkorcu.en.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eukonsult00.el ;
% mode: latex ;
% End: ;

