
Dear decision makers:

My name is Xavi Drudis Ferran, I am an EU citizen and software
engineer and would like to comment on software patents (please feel
free to ask any other personal data you may need).  After having read,
among other material, the European Commission study in
http://www.europa.eu.int/comm/internal_market/en/intprop/indprop/study.pdf
as well as your consultation document in
http://www.europa.eu.int/comm/internal_market/en/intprop/indprop/soften.pdf
and having discussed this issue both on line and around with colleagues, I
have formed the following opinion.

a) Scope of harmonisation

* Should harmonisation take place on the basis of the elements
  contained in this document?

No. 

The document is a serious effort to address the issue but fails to
understand the very nature of the market and the creations it deals
with. It struggles for coherency in order to keep a compromise in
complex issues that have been so far subject to wide interpretation
and uncertainty, and have progressed from a poor vision in the EPC to
a confusing case law based on ever elusive concepts. It makes a good
job of trying to get order out of chaos and to reconcile the practice
and case law with the rules. 

But it presents a proposal that not for being somewhat clearer and
more coherent is any useful. The study recognises that there is no
clearly proven economic advantage in patenting software, and I find it
very dubious that there is any moral argument for it. Yet it proposes
a system that allows patents (thus taking away freedom from people in
exchange for nobody knows what) for any software (because all software
is technical), provided it is considered new, inventive and gives a
technical contribution. The need to improve the examination to reject
trivial patent applications is recognised but no clear clue on how to
achieve this better examination quality is suggested, not even does it
explain whether it is achievable at all.

I think the very nature of software makes it incompatible with
patents, and this proposal would lead to new ambiguity and
uncertainty as it would be put in practice. New cases would soon
arise in which the notion of "technical contribution" would have to be
interpreted with no clear definition. If "technical" means comprising
"knowledge and equipment" then there is no reason to pretend that
methods of doing businesses through computer equipment are less
"technical" than a cash register, so the pretended exclusion of
methods for doing businesses from patentability would lead to
inconsistent cases, doubt and uncertainty again, and that is only an
example.

* Should a more restrictive approach be adopted?.

Yes. 

Software should no be patentable in any way, and currently accepted
patents on software should provably be revoked (this does not seem
very fair, but it may be necessary to correct the current
situation). Due to the incremental nature of software development, to
the fact that the effort required to find ideas is much less than that
needed to produce a working program, and to the fact that a program is
only information, patents do not promote any kind of innovation in the
software business.  Society does not obtain anything useful from
software patents, and temporal monopolies on software are a great
burden to the industry and society as a whole.

* Should more liberal conditions coming closer to the practice in the
United States of America prevail in the future?.

No. 

The patent system in the USA is the result of their decision makers
refusing to decide what is good or bad for their country and just
lazily letting things happen as they come. There is no reason to think
it achieves any benefit for the society, and it has lots of
detractors, big and small.

If any harmonisation should be found between USA, Japan and Europe it
would have to be for all to drop software patents. When looking for
harmonisation it is foolish to just copy what your neighbour does. It
is wiser to sit and try to decide what of the two options is better
and change the worse, or find a new one with as many benefits as
possible from both systems and as few drawbacks as possible.

The EU seems confident that methods of doing business should not be
patentable, and there is discrepancy with the USA, but that is not
seen as a problem. So why should discrepancies in software patents be
a concern?. I understand the convenience of harmonisation within
Europe, but not by getting closer to the USA situation.

Even if software patents were a good idea in the USA there is no
reason to think they have to be good here. I don't know if this is a
very sound argument, but evidently, European software businesses would
be severely damaged by allowing USA companies to apply for patents in
Europe in a field on which they have far longer experience and bigger
portfolios. Anyway, software patents are a bad idea, and we should try
to escape the problems the USA will have (and already has) with them.

b) Impact of harmonisation

What would be the impact of the preferred option (banning software
patents) on:

* innovation in software and underlying knowledge and techniques.

Innovation is inherent in software. In other industries, the money is
made by selling goods that are developed through basic research,
experimental studies, prototypes and a laborious process, because
there is a physical object that must be brought into shape to fulfil
an idea. Once that expensive development is done, there's still an
expensive production process that must be carried out in order to be
able to sell the product. So any company has the option of
concentrating in a good production process, or a good distribution
channel, etc. Innovation is not necessary to be successful in the
market, but it is good for society since it leads to better
products. So it may be reasonable for society to sponsor innovation in
these industries, even in exchange for some freedoms.  Industry may be
tempted to invest in research and development of innovative products
knowing that it will enjoy a limited monopoly on it, so that what is
invested can be saved in having to deal with competition.

In software, ideas are much closer to the sold object, and all the
cost is in development. Once a product has been developed there are
negligible costs to its mass production and distribution.  There is
not much room for a company that pretends just copying the same as has
been done before, because either it has something new to offer, or it
won't be able to produce the copies better or to distribute them
better, and so it won't be able to compete. It can try to develop a
better version of another company's innovation, but it is unlikely
that they make it so much better that they can compensate for the head
start of the original innovator. There is no way of making software
without innovation, although this innovation is often much more
progressive and result of a combination of already known techniques,
so it is hard to tell what is obvious and what is not.

The only way for a software company to survive without innovation is
to enjoy a monopoly. And one way of getting a monopoly is through
software patents, so in order to encourage innovation in software, we
should get rid of software patents.

All software is just a combination of the basic arithmetic and logic
operations all computers can perform, in increasingly more complex and
useful elements. There is no new physical property or natural force
you can think of to obtain a new program. The only way is to combine
already existing elements and techniques in more useful formulas and
algorithms. So the more available resources to combine, the more
likely the industry is to produce innovative applications and useful
creations. Patents do only get in the way.

Once patents on software are allowed, the only reasonable play in the
market is a buildup of offensive or defensive patent portfolios by all
players, quite like weaponry in the cold war, so that you have
reasonable access to techniques by cross licensing or just counter
suing infringements.

Obtaining patents is a necessarily slow and expensive process, because
the patent must be well documented and checked. Expenses must be paid
by the company applying for the patent, which is only natural, but
that drains resources from innovation. And the incentives for
innovation are already there in a market without software patents, so
a market without software patents will enjoy more innovation than a
market with software patents.

* The ability of SMEs to enter the market of innovative software tools
and services and the market of innovative applications of software.

I've read in the EU Commission study that SMEs should apply for
software patents to raise funds and get the venture capital. I can
hardly believe that this would place them in a better position than in
the absence of software patents.

Software is relatively cheap to produce. It takes human labour but not
very expensive equipment.  Computers are relatively cheap, software
development tools vary in price, but they are not the more important
factor. Software SMEs and startups only need a good idea and a few
good software engineers. With that they may develop software that they
may sell without significant production and distribution costs.

Good software engineers do their job through knowledge of techniques
and tools to use, by combining algorithms and data structures in
useful ways. Without software patents they can draw on a wider set of
ideas and techniques than with software patents, so they become more
productive.  In a market with software patents, the SMEs have no other
option than to do an extra effort to get their software done, and then
pay for the cost of applying for a patent. And that does not even
mention the uncertainty of whether they infringe on already existing
patents and the cost of checking it and possibly paying licenses. They
can possibly get some more funds once they get a patent, but they need
to spend much more before they get the patent and the their software
product. So, in the end, it is harder for them to enter a market with
software patents than a market without them.

The fact that patents can be used to raise more money for small
companies is not as clear as the fact that software companies that are
indeed small and have little money are no longer allowed to exist in a
software patent market. Available venture capital can benefit more
SMEs if they are able to enter the market at a lower cost than if they
have to pay for patents. And it is better to have more SMEs able to
survive with less money and innovating than less SMEs able to survive
with more expenses (in patents) and more funding (because of patents),
because the amount of innovation would depend on the number of SMEs
more than in the money they get, if they get it and expend it in the
patents themselves.  I think I'm not explaining myself. If there is 10
euros of venture capital and SMEs can only get 1 euro each without
patents and 2 euros with patents, that means that there'd be only 5
SMEs in a system with patents and 10 in one without patents. If the 10
SMEs produce one innovation each, we are better off without patents,
because the cost of the patent system will keep the 5 SMEs to produce
2 innovations each. Well, I don't know if I just proved anything about
patents, or I just proved that I am not an economist...

Patents do only benefit companies which already enjoy a portfolio of
patents they can use for cross-licensing or for arrangements to avoid
legal action when they infringe others' patents. The longer they are
in the market the more patents they get and the more comfortable they
are.  Newcomers have more difficulties to compete with established big
companies.  And that is an advantage for big enterprises, not SMEs.

* The creation and dissemination of free / open source software.

Obviously it is much easier to sue someone for infringing a patent
when the source code is available than when it is kept secret. Since
it may become very difficult not to infringe on any patent when
developing software, patents may be a powerful incentive to keep the
source code secret and abandon open source initiatives. This leads to
a decrease in the overall quality, security and interoperability of
all software.

Free software developers are not always in companies that can invest
in patent portfolios or raise money by getting software patents. Even
for open source companies, patents are a burden.  Patents are
expensive and they can give no benefit in an open source product,
because for the program to be truly open the patent must be licensed
to everybody at no charge, and anyone should be allowed to modify or
use the patented program. This takes away any force a patent may
have. It no longer creates any monopoly, it no longer can be used to
sue people using the invention, it no longer can be bargained for
cross-licensing. Since the patent gives no power to the owner, the
open source company won't get any extra funding for owning the patent,
so there is no use for a software patent in the open source community,
but there is the threat of being sued by others.
 
Free software developers, either companies or individuals, can be sued
for infringing in a patent they don't even know. This makes more
difficult to contribute to open source projects, and also to accept
contributions, because a much more strict version control and
contributor identification is required to make everyone answerable for
his or her patent infringements. It is even worse when patents are
poorly examined and given to obvious programs or techniques, which has
happened in the USA and Europe. 

The Commission declares itself concerned and willing to try hard to
prevent undeserved patents, but neither the Commission, nor the EPO nor
anyone else has shown any hopeful strategy for doing so. I've read
about contest style schemes in which the patentee is punished somehow
if the patent is busted, and the patent buster is rewarded, but I
think even this is a bad idea, because having people chasing one
another is less socially productive than having everybody doing real
work and solving new problems. So much so, that it could easily happen
that the contest does not offer enough motivation for skilled
professionals to act as patent busters and, therefore, does not improve
the quality. Intuitively, one cannot raise the quality of a system
that is inherently counter-productive.

Free software is not only developed by companies that can be aware of
the software patents issued and have lawyers to ponder the best patent
policies. Software development is also done in house in organisations
that have little to do with the software market except as consumers,
to fulfil those of their needs that are not well addressed by
products in the market. And they are likely to choose to open source
their efforts to benefit from contributions by others. This brings
about solutions that are useful because they address the real needs of
the users. But if they risk being sued about software patents those
companies are likely to either quit their software development or to
try to make it more secret by not collaborating with others that might
be interested in the development, and making it closed source.

It is almost impossible to develop or distribute free software in a
world in which any company can get patents to stop any free project
they chose, or at least to cause it to spend money and time in legal
proceedings, as soon as it gets in the way of their monopolies. There
is already much trouble and the open source community has vastly
expressed its concern. On the contrary, if there would be no software
patents, the synergy created by the free exchange of source code would
allow the continual improvement and surfacing of readily available
software of higher quality and reliability. This would benefit the
whole software business and, most important, the consumers.

* The position of the European software industry in global
  competition

USA and Japan have both more software patents than Europe, and their
companies are more used to apply for, bargain, defend with, attack
with and evaluate software patents than European industry is.  If
Europe would be allowed to be exempt from software patents, our
industry could concentrate in developing better technical solutions
or, those who choose to operate in foreign markets, obtain patents in
the USA or Japan for their operations there. A software industry that
learns to compete in terms of quality and research will be in a better
shape to compete with industries that have to worry with patent
portfolios, lawsuits and exclusionary tactics. A company that has
grown thanks to its monopoly in some technique in the USA or Japan
will probably be less competent in a market where it can't enjoy such
a privilege and must offer better products than their European
competitors. A company that has grown in the USA having to defend
itself against competitor patents has not been able to concentrate in
research and development as much as a European company
which has been able to concentrate its efforts in its products.

It may seem that European software companies that want to compete in
the USA or Japan will have a weaker position there if they can't fight
patents with patents, but they can choose two alternatives: Either
they file software patents in the USA and Japan, and then they choose
to be in a similar position than if they were in a Europe with
software patents, or, more wisely, they choose to offer their products
by Internet from inside Europe, where USA or Japanese software patents
are not enforceable. This would put USA companies in a difficult
position and is exactly what USA fears and the reason it is pushing
Europe to adopt their patent system. Europe should resist their
pressure.

Anyway, software patents simply don't work, and sooner or later USA
and Japan will get rid of them. There is abundant protest in the USA
about software patents already, and when criticism comes from such
different sources as the Free Software Foundation or Oracle
Corporation, we cannot be sure that USA will keep software patents for
ever. Europe had better get rid of them right now.

* The general development of the Information Society.

The information society is based on an scheme where information flows
freely from place to place, and this allows people and organisations
to benefit from more efficiency in their work as well as less waste of
effort in information exchange, in rediscovering already available
knowledge and in finding uses for the knowledge they have. Software is
both information that can flow and a tool to manipulate information so
that people can concentrate in less mechanical and more valuable
activities that transform information into knowledge.

Software patents are a way to publish information about software, but
at the same time they restrict the use of the published information to
the patent owner or its licensees. This is an artificial barrier to
the free flow of usable information and thus it is against the spirit
of the Information Society.

Patents are a token from the Industrial Society (or they are even
older, but at least they make some sense in the industrial
society). In the industrial society the main activity is the
manufacturing of goods, physical objects. Physical objects take effort
and resources to produce, and can be used only by one owner at a
time. So it takes many people and machines to make them if they are to
be used by many people. The most common jobs are manual or mechanical,
and have to do with producing the same good or sort of goods over and
over again. The creation of knowledge is a less common activity, due
to communication inefficiency, and information storage costs.
Innovations are useful and desirable, but they are scarce, and it is
worthwhile to promote innovation by giving limited monopolies to
innovators so that only they or those who they choose can apply the
new knowledge they have discovered. This is inconvenient for the
competitors, but society obtains new knowledge that wouldn't otherwise
be there. So it may be a good deal.

In the information society, the goods become information assets, easy
to move around and duplicate, and there is no longer any sense in
having people produce the same goods over and over. The most common
jobs are intellectual and depend on the use and production of new
knowledge.  Innovation is no longer scarce and it becomes an everyday
activity. Workers in the information society create knowledge, new
knowledge, based on existing ready-to-use knowledge which is available
thanks to better information communication and storage means.

There is no longer any need to promote innovation, just like there was
no need to promote tightening of bolts in the Industrial
Society. Giving a patent to a software developer in the information
society is like giving a limited monopoly to an Industrial society
executive just for opening a new factory, not for inventing a new
device or production process. Just like opening a new factory is
already a common and profitable activity in the Industrial Society and
needs no incentive, innovating in software is a common and profitable
activity in the information society and needs little incentive and
absolutely no privilege.

But I wonder if all this has much to do with the information society,
maybe it is closer to information economy. Society is about
people. The information society might bring more than economic
benefits to people. More and better information together with more
easily available knowledge does not only make people richer, it can
make them happier, healthier and more responsible. By allowing people
to communicate with other people that share the same interests, giving
them more access to information and a little bigger chance to speak up
and be heard, information society has some potential for increasing
European welfare. One of the emerging trends in this scenario is the
change in leisure activities. Leisure may turn from passively watching
TV to actively researching some topic of interest or creating or
disseminating art or knowledge in the preferred form.

One of the possible leisure activities (or hobbies if you like) is
software development and many others may depend on the use of
computers and software in processes that may be affected by software
patents.  For example, many users that have set up a web page on their
favourite topic with free tools on their home computer could be sued
by a patent in the compression algorithm used to make the images on
their pages faster to download. The patent owner has already demanded
money for such a use of tools that include their compression
algorithm. Software is one of the ingredients of our culture, and
software patents pose a threat of cultural repression or inhibition.

Not surprisingly, the Information Society was born from the use of
open and unpatented technologies (TCP/IP, http, HTML, SMTP ... ). It
is the free access to the innovations that make the technology
explosively fruitful, not the amount or quality of innovation. But in
my opinion, both the amount and quality of innovation and the free
access to it would benefit from the suppression of any software
patents.

So software patents do not only affect the economy of a given
industry, but the welfare of society as a whole, and a Europe free
from the troubles about software patents is much more favourable soil
for a new rich and diverse information culture to grow on. Software
patents centralise in a few companies the use of innovations that
would be more fertile if left free in the hands of the people.


--
Xavier Drudis Ferran
xdrudis@tinet.org


