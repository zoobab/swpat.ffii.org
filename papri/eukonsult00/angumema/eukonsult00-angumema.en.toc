\contentsline {section}{\numberline {1}About the Paper and the Author}{2}{section.1}
\contentsline {section}{\numberline {2}Position Paper}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}On the Main Questions}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Scope of Harmonisation}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Effects of Harmonisation}{3}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Key Elements}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}i. Principle}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}ii. Complementarity}{6}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}iii. Technical Contribution}{6}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}iv. Technical Considerations}{6}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}v. Technical and Non-Technical Features}{8}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}iv. Permissible Claims}{8}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}vii. General Patent Law}{9}{subsubsection.2.2.7}
\contentsline {subsection}{\numberline {2.3}Summary}{9}{subsection.2.3}
\contentsline {section}{\numberline {3}Related Reading}{10}{section.3}
