\contentsline {section}{\numberline {1}\"{U}ber das Papier und den Autor}{2}{section.1}
\contentsline {section}{\numberline {2}Stellungnahme}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Zu den Hauptfragen}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Umfang der Harmonisierung}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Auswirkungen der Harmonisierung}{2}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Zu den Schl\"{u}sselelementen}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}i. Grundsatz}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}ii. Komplementarit\"{a}t}{6}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}iii. Technischer Beitrag}{6}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}iv. Technische \"{U}berlegungen}{6}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}v. Technische und nichttechnische Merkmale}{7}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}vi. Zul\"{a}ssige Patentanspr\"{u}che}{8}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}vii. Allgemeines Patentrecht....}{8}{subsubsection.2.2.7}
\contentsline {subsection}{\numberline {2.3}Zusammenfassend}{9}{subsection.2.3}
\contentsline {section}{\numberline {3}Weitere Lekt\"{u}re}{10}{section.3}
