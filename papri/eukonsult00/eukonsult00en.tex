\documentclass[11pt]{scrartcl}
\usepackage{hyperref}
\usepackage{epsfig}
\usepackage{array}
\sloppy
\pagestyle{plain}
\input{el2tex}
\input{coldef}
\begin{document}
\title{European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\\
{\tt\normalsize http://swpat.ffii.org/vreji/papri/eukonsult00/indexen.html}}
\author{PILCH Hartmut}
\maketitle
\begin{abstract}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.
\end{abstract}
\tableofcontents
\section*{}

\section{How it came about\label{hist}}

In summer 2000 the scene was set for legalising software patents in Europe.  For several years, the EU Commission's Industrial Property Unit had planned to rubberstamp the European Patent Office's policy of granting patents on rules or organisation and calculation.  The last necessary step was the official removal of all restrictions on patentability from the law or at least a deletion of the ``programs for computers'' in art 52 of the European Patent Convention (EPC).  Moreover, the European Commission was scheduled to become a major player in a future European patent system, which was to be based on Community law rather than on an intergovernmental negotiation process.  As a part of this transition, the European Commission was to issue a software patentability directive.  Under the name of ``clarification'' and ``harmonisation'' the existing legal framework was to be discarded and instead the practise of the European Patent Office was to be legalised and made binding for national courts, many of which had so far refused to follow the EPO on its adventurous course, either by sticking to the letter and spirit of the existing law or by developping their own halfway solutions.  In 1997 the European Commission outlined the contents of a software directive in a ``Greenpaper on the Community Patent'' and inserted it into the rear part of a long resolution text which was rubberstamped by the European Parliament.  Yet, this met with unexpected resistance.  By summer 2000 a Petition for a Software Patent Free Europe had gained the support of 50000 signatures, 200 software companies and numerous politicians of all camps.  Under the impression of these developments, the governments of FR+DE+UK decided not to delete the computer program exclusion for the time being and instead hand over the issue to the European Commission.  The patent law experts at the CEC Patent Unit then opened\ (http://europa.eu.int/comm/internal\_market/de/intprop/indprop/softpaten.htm) a public consultation on 2001-10-15 which was to last until 2001-12-15 and thus cover exactly those weeks, during which the EPC had been scheduled to be changed.  Art 52(2) EPC was as a result left untouched, and instead the patent movement\ (http://swpat.ffii.org/stidi/lijda/indexen.html) was mobilised by means of a specifically focussed consultation call:

\begin{itemize}
\item
Software Patents - Commission launches Consultations\ (http://europa.eu.int/comm/internal\_market/de/intprop/indprop/softpaten.htm)

\item
IPI Study: The Economic Impact of Patentability of Computer Programs\ (http://europa.eu.int/comm/internal\_market/en/intprop/indprop/studyintro.htm)

\item
Replies to the EC swpat consultation\ (http://europa.eu.int/comm/internal\_market/en/intprop/indprop/softreplies.htm)
\end{itemize}

The basic question of whether computer programs (computer-implementable rules of organisation and calculation) are inventions in the sense of patent law (art 52 EPC) was not asked.  Instead, the CEC Patent Unit implied that the answer must be yes.  They talked about ``computer-implementable inventions'' and used this term as a synonym for ``computer-implementable rules of calculation and organisation'' rather than for true technical inventions which can be implemented under program control and whose patentability is beyond doubt.  Thus the EU Industrial Property Unit not only implied an answer where it should have asked a question, but also falsely attributed silly positions to the software patent critics, before even opening the consultation.

The accompanying ``independent study'', written by some well-known EU software patent lobby activists\ (http://swpat.ffii.org/vreji/papri/clsr-rhart97/indexen.html), again reiterated the usuall patent lawyer credo in the guise of a somewhat more objective wording.  This led the study to draw conclusions such as ``any proposition to extend the patent system beyond the status quo cannot be claimed to rely on scientific evidence''.  In this context ``status quo'' refers to the practise of the European Patent Office's Technical Boards of Appeal.  The study does not even consider the possibility of criticising that practise and returning to the European Patent Convention.  Nor does it explain the concept of ``technical invention'', which is the underlying spirit of the European Patent Convention.  Moreover the study predicts that software patents will gradually be accepted and used by the open source software community for its own benefit.

In spite of all this care taken to mobilise a network of patent professionals and create the impression of a pro-patent mainstream opinion, the results of the mobilisation campaign did not really look very convincing as a basis for arguing the case of software patentability.

While at the beginning, the CEC Indprop Unit\ (Industrial Property Unit in the General Directorate for the Internal Market of the Commission of the European Community) still wanted to quickly issue a directive and rallied national governments for this purpose, it gradually turned out that these governments were not very eager to go along.  Also, the CEC Patent Unit itself apparently found that this consultation was not a good basis for their plans.  They reported to the governments that the consultation result showed an ``overwhelming vote of the concerned industries in favor of software patents'' and only a minority, mainly from the ``opensource movement'', was clamoring against software patents.  But they did were not eager to display the results of the consultation on their website, and it was very hard to press them to publish at least a small part of the submissions.  Most submissions have not yet been published, often in spite of repeated requests by the authors to have them published, and those that have been published have been transformed to a PDF format that makes them hard to use in any hypertext-based dialogue.

Thus the work of evaluating the consultation has largely been left to private initiatives like ours.  The CEC Patent Unit has however ordered a summary analysis\ (http://europa.eu.int/comm/internal\_market/en/intprop/indprop/softpatanalyse.htm) from an ``independent contractor'' which has been available internally since April and was released to the public at the beginning of August, together with the installation of a web-based online discussion forum\ (http://europa.eu.int/comm/internal\_market/en/indprop/softforum.htm).

The CEC online discussion forum is only web based, without an archive, incompatible with normal posting and referencing and automatisation mechanisms, depending on Javascript, in general poorly accessible, unacceptable for the people who have developped high standards of public communication in newsgroups and mailing lists\ (http://swpat.ffii.org/vreji/mrilu/indexen.html).  Since the Internet became popular, our representatives and their PR agencies have been trying hard to reinvent the wheel of civil culture, apparently hoping to change its shape from a circle to a square that matches their tv-based communication habits.  Even though the Indprop Unit's attempt to at least engage in some form of dialogue seems commendable, citizens are not to blame for ignoring this type of government-designed ``e-democracy''.

\section{How you can help\label{help}}

Let's do the remaining work together.

\begin{itemize}
\item
Look at the consultation results table and find an submission that you think needs further analysis.  What we need most is a plain text version, the \emph{mail} address of the author, and your description or recension of the text.
\begin{enumerate}
\item
Click on the respective PDF files.  Read the PDF documents.

\item
Contact the submittants, ask them to send you text versions of their submssions for publication on our website.

\item
Write a recension.  You may have noted that many of the links on the left side menu are broken.  They are placed there in anticipation of your recension.
\end{enumerate}

\item
Look at the Eurolinux Consultation\ (http://petition.eurolinux.org/consultation) page, write records for some of the more interesting recensions from there, contact the authors and the CEC Industrial Property Unit to make sure they are published on the consultation page.

\item
Make meaningful contributions to the CEC online forum and wherever appropriate, refer your readers to this consultation page\ (http://swpat.ffii.org/vreji/papri/eukonsult00/indexen.html).
\end{itemize}

\section{Consultation Submissions\label{tabl}}

\section{Evaluation\label{eval}}

Most of the submissions fall into one of the following categories:
\begin{enumerate}
\item
Small-scale studies from individual researchers/academics, carefully crafted, with original reasoning and references to relevant literature, e.g.
\begin{itemize}
\item
Brian Kahin\ (http://swpat.ffii.org/vreji/papri/eukonsult00/maryland/indexen.html)

\item
Jean-Paul Smets\ (http://swpat.ffii.org/vreji/papri/eukonsult00/smets/indexen.html)

\item
Seoane \& Fernandez\ (http://swpat.ffii.org/vreji/papri/eukonsult00/fernandez/indexen.html)

\item
Richard Castanet\ (http://swpat.ffii.org/vreji/papri/eukonsult00/castanet/indexen.html)

\item
Xavier Drudis Ferran\ (http://swpat.ffii.org/vreji/papri/eukonsult00/ferran/indexen.html)

\item
G\"unter Sch\"olch: Comments on the Consultation Paper\ (http://swpat.ffii.org/vreji/papri/eukonsult00/angumema/indexen.html)

\item
Albert Endres\ (http://swpat.ffii.org/vreji/papri/eukonsult00/endres/indexde.html)

\item
Richard Watts\ (http://swpat.ffii.org/vreji/papri/eukonsult00/watts/indexen.html)

\item
Herman Bruyninckx\ (http://swpat.ffii.org/vreji/papri/eukonsult00/bruyninckx/indexen.html)

\item
Edward Avis\ (http://swpat.ffii.org/vreji/papri/eukonsult00/avis/indexen.html)

\item
...
\end{itemize}

\item
Position papers from patent lawyers of large enterprises and industry associations that state the standard articles of faith from the credo of the patent movement and professionally play the mobilisation game of the CEC Industrial Property Unit, usually without more than schematic reference to the special situation of the body which they represent, written by a patent law expert of that body without review or even knowledge of other members of the organisation.  These articles generally show a lack of basic knowledge on the European concept of technical invention and the debate on computer-programs that has been raging since the 60s\ (http://swpat.ffii.org/stidi/korcu/indexen.html).  Instead they dwell much on the TRIPS fallacy\ (http://swpat.ffii.org/stidi/trips/indexen.html) and other well-known patent movement propaganda myths.  It sounds as if these lawyers draw all their knowledge from a limited repertoire, comprising not much more than the ``independent study'' on which this consultation was based.
\begin{itemize}
\item
IBM\ (http://swpat.ffii.org/vreji/papri/eukonsult00/ibm/indexen.html)

\item
Alcatel\ (http://swpat.ffii.org/vreji/papri/eukonsult00/alcatel/indexfr.html)

\item
France Telecom\ (http://swpat.ffii.org/vreji/papri/eukonsult00/telecom/indexfr.html)

\item
Philips\ (http://swpat.ffii.org/vreji/papri/eukonsult00/philips/indexen.html)

\item
Bull\ (http://swpat.ffii.org/vreji/papri/eukonsult00/colombe/indexen.html)

\item
GE Int\ (http://swpat.ffii.org/vreji/papri/eukonsult00/coppenholle/indexen.html)

\item
GE GXS\ (http://swpat.ffii.org/vreji/papri/eukonsult00/geglobal/indexen.html)

\item
Maitland Kalton\ (http://swpat.ffii.org/vreji/papri/eukonsult00/kalton/indexen.html)

\item
EICTA\ (http://swpat.ffii.org/vreji/papri/eukonsult00/eicta/indexen.html)

\item
EISA\ (http://swpat.ffii.org/vreji/papri/eukonsult00/eisa/indexen.html)

\item
Business Software Alliance Comments to the European Commission Consultation Paper on the Patentability of Computer-Implemented Inventions\ (http://swpat.ffii.org/vreji/papri/eukonsult00/bsa/indexen.html)

\item
FEI\ (http://swpat.ffii.org/vreji/papri/eukonsult00/fei/indexen.html)

\item
German Informatics Corporatists worried about Systematics of Patent Law: GI Position Paper on Patentability of Computer Implementable Rules of Organisation and Calculation\ (http://swpat.ffii.org/vreji/papri/eukonsult00/mayr/indexde.html)

\item
...
\end{itemize}

\item
Position papers from patent practicioners and their associations.  Very similar to the above, reflecting the credo of patent circles, without knowledge of the concept of technical invention, without consideration for the risks of unlimited patentability, with naive claims about the economics of software, like the above group, but often a little bit less schematic, sometimes more aggressive.
\begin{itemize}
\item
TMPDF\ (http://swpat.ffii.org/vreji/papri/eukonsult00/tmpdf/indexen.html)

\item
FICPI\ (http://swpat.ffii.org/vreji/papri/eukonsult00/ficpi/indexen.html)

\item
\"OVGRUR\ (http://swpat.ffii.org/vreji/papri/eukonsult00/sonn/indexde.html)

\item
H\"ossle \& Kudlek\ (http://swpat.ffii.org/vreji/papri/eukonsult00/hossle/indexde.html)

\item
MPI\ (http://swpat.ffii.org/vreji/papri/eukonsult00/planck/indexde.html)

\item
ACC BE\ (http://swpat.ffii.org/vreji/papri/eukonsult00/parker/indexen.html)

\item
Kiani \& Springorum\ (http://swpat.ffii.org/vreji/papri/eukonsult00/kiani/indexde.html)

\item
Stephan Freischem\ (http://swpat.ffii.org/vreji/papri/eukonsult00/freischem/indexde.html)

\item
Alain Michelet\ (http://swpat.ffii.org/vreji/papri/eukonsult00/michelet/indexfr.html)

\item
Erwin Basinsky\ (http://swpat.ffii.org/vreji/papri/eukonsult00/basinsky/indexen.html)

\item
...
\end{itemize}

\item
Position papers from other representatives of organisations with varying, less schematic viewpoints, not playing the game of the consultation paper but rather citing parts of it as appropriate for the (independently structured) argumentation.
\begin{itemize}
\item
BBC\ (http://swpat.ffii.org/vreji/papri/eukonsult00/bbc/indexen.html)

\item
CLEDIPA\ (http://swpat.ffii.org/vreji/papri/eukonsult00/cledipa/indexen.html)

\item
BILETA\ (http://swpat.ffii.org/vreji/papri/eukonsult00/bileta/indexen.html)

\item
VOSN\ (http://swpat.ffii.org/vreji/papri/eukonsult00/dijk/indexen.html)

\item
ISOC FR\ (http://swpat.ffii.org/vreji/papri/eukonsult00/isf/indexfr.html)

\item
ISOC LU\ (http://swpat.ffii.org/vreji/papri/eukonsult00/walle/indexfr.html)

\item
EuroLinux\ (http://swpat.ffii.org/vreji/papri/eukonsult00/eurolinux/indexen.html)

\item
...
\end{itemize}

\item
Statements of personal experience and opinion on software patentability, not closely oriented toward the paper
\begin{itemize}
\item
Nick Longo\ (http://swpat.ffii.org/vreji/papri/eukonsult00/longo/indexen.html)

\item
Pierre Clerc\ (http://swpat.ffii.org/vreji/papri/eukonsult00/pclerc/indexfr.html)

\item
Daniel R\"odding\ (http://swpat.ffii.org/vreji/papri/eukonsult00/roedding/indexde.html)

\item
Tobias Hoevekamp\ (http://swpat.ffii.org/vreji/papri/eukonsult00/hoevekamp/indexde.html)

\item
RA Trapp\ (http://swpat.ffii.org/vreji/papri/eukonsult00/trapp/indexde.html)

\item
Niels K Jensen\ (http://swpat.ffii.org/vreji/papri/eukonsult00/jensen/indexen.html)

\item
J-Brice Demoulin\ (http://swpat.ffii.org/vreji/papri/eukonsult00/demoulin/indexfr.html)

\item
Philip Hands\ (http://swpat.ffii.org/vreji/papri/eukonsult00/hands/indexen.html)

\item
Sebastian Schaffert\ (http://swpat.ffii.org/vreji/papri/eukonsult00/schaffert/indexde.html)

\item
...
\end{itemize}

\item
Short political statements by individuals, most of whom fear negative effects of software patents.
\begin{itemize}
\item
Alan Isaac\ (http://swpat.ffii.org/vreji/papri/eukonsult00/isaac/indexen.html)

\item
Dominique Liebart\ (http://swpat.ffii.org/vreji/papri/eukonsult00/dliebart/indexfr.html)

\item
Darche\ (http://swpat.ffii.org/vreji/papri/eukonsult00/darche/indexen.html)

\item
Maurice Clerc\ (http://swpat.ffii.org/vreji/papri/eukonsult00/clerc/indexfr.html)

\item
Gervase Markham\ (http://swpat.ffii.org/vreji/papri/eukonsult00/markham/indexen.html)

\item
Annelis Moens\ (http://swpat.ffii.org/vreji/papri/eukonsult00/moens/indexen.html)

\item
...
\end{itemize}
\end{enumerate}

There are still more than 1300 unpublished statements, most of which probably belong to the last two categories and can be found on the Eurolinux consultation site\ (http://petition.eurolinux.org/consultation).

\section{Further Reading\label{etc}}

\begin{itemize}
\item
{\bf {\bf The Eurolinux Software Patent Consultation Page\footnote{http://petition.eurolinux.org/consultation}}}
\filbreak

\item
{\bf {\bf United Kingdom Patent Office consultation on the question as to whether or not patents should be granted for computer software or ways of doing business: responses\footnote{http://www.patent.gov.uk/about/consultations/responses/index.htm}}}
\filbreak

\item
{\bf {\bf 2nd meeting of the European Commission's Industrial Property Unit with a Eurolinux delegation\footnote{http://www.eurolinux.org/news/euip017/indexen.html}}}

\begin{quote}
For the second time, representatives of the Eurolinux Alliance are meeting officials who are in charge of drafting a European Directive on the Patentability of Computer-Implementable Rules of Organisation and Calculation (Programs for Computers).  A preliminary outline of a possible directive draft, published on 2000-10-19, proposed to legalise such patents and asked a series of questions concerning details of this approach. The consultation yielded written statements, often with well known faith-based arguments, and also with a well known distribution of forces:  a large number of individual software developpers, mostly speaking for themselves, against a small number of patent lawyers, mostly speaking in the name of big organisations.  So far no further questioning and dialogue has been organised.  The Eurolinux Alliance is therefore happy to have the chance to meet the consultation organisers and help pave the way for an in-depth discussion.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux letter to the EU patent legislators\footnote{http://www.eurolinux.org/news/101/indexen.html}}}

\begin{quote}
In late february 2001, the Eurolinux Alliance sent a letter to the European Commission's Industrial Property Unit (ECIPU), asking them to resume consultations on computer-implementable rules of organisation and calculation, which was seen to have been interrupted, and proposing a few questions and a methodological framework for further discussions and research.  Therebey the Eurolinux Alliance reacts to an internal position papers in which the ECIPU seemed to have adopted unsound methods, such as hiding and misinterpreting consultation results in order to push national governments to support their project of legalising software patents in Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament \footnote{http://swpat.ffii.org/penmi/bundestag-2001/indexen.html}}}

\begin{quote}
8 experts from the areas of law, informatics and economics will answer questions from MPs, based on written responses to the questions of the expert hearing as specified below.  The interested public is also called to present its answers to any subset of these questions in writing.  This expert hearing is only a beginning.   If you want to make a submission, you may send it to the FFII.  We will try to help you both to write them well and to assure maximum exposure and diffusion by publication on our site, so as to promote a competent public dialogue on this subject.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents: Discussion Rounds and News Channels\ (http://swpat.ffii.org/vreji/mrilu/indexen.html)}}

\begin{quote}
In the Internet age, high quality interactive consultations are even occurring when no government agency ``launches'' them.  Several mailing lists on patent affairs have had very lively discussions on the subject of this consultation.  Some of these lists have archives, where you can study for yourself, whether pro software patentability arguments have ever been able to stand the test of public debate.   Our experience is that this is nowhere the case, not even on the mailing list of the European Patent Office.
\end{quote}
\filbreak

\item
{\bf {\bf Robert Hart 1997, The Case for Patent Protection for Computer Program-Related Inventions\footnote{http://swpat.ffii.org/vreji/papri/clsr-rhart97/indexen.html}}}

\begin{quote}
Patent lawyers try to create the impression that the United States introduced software patents after a conscious, public-opinion based debate.  One of the most outspoken pro-swpat activists at the European Commission, Robert Hart, argued the case in 1997 by misrepresenting the positions of some major patent-critical voices as being pro software patent.  Hart later co-authored an ``independent study'' for the European Commission.
\end{quote}
\filbreak

\item
{\bf {\bf The Hidden Patent Agenda of the European Commission\footnote{http://www.eurolinux.org/news/agenda/indexen.html}}}

\begin{quote}
Jean-Paul Smets writes in spring 2000: The European Commission's Directorate for the Internal Market has built a consistent network of fallacies to argue in favour of software patents.  It is closely following a new proprietarist ideology championed by the United States government.  Expect the Directorate to push patents on intellectual methods in the near future.
\end{quote}
\filbreak

\item
{\bf {\bf Bernhard M\"uller announces extension of patentability by EC directive\footnote{http://swpat.ffii.org/vreji/papri/cr-bmueller00/indexde.html}}}

\begin{quote}
The author is a leading drafter of the new EC directive on software patentability.  In this article, published in early 2000, he asserts that public opinion in Europe is in favor of moving into the direction of the USA.  To support this claim, he presents the results of a survey conducted by his office among 44 patent lawyers.
\end{quote}
\filbreak

\item
{\bf {\bf Eurolinux meets EU patent legislators\footnote{http://www.eurolinux.org/news/euipCAen.html}}}

\begin{quote}
A meeting between 8 Eurolinux representatives and 3 members of the Industrial Property Unit (IPU) of the European Commission's General Directorate for the Internal Market took place on 1999-10-15 16.00-17.00 in Brussels in the IPU office.
\end{quote}
\filbreak

\item
{\bf {\bf Guetlin 2001: Seminararbeit Softwarepatente\footnote{http://swpat.ffii.org/vreji/papri/unizh-guetlin01/indexde.html}}}

\begin{quote}
Die Autorin ist Sinologin mit Nebenfach Informatikrecht am Institut f\"ur Informatik der Universit\"at Z\"urich.  Sie hat rechtliche Quellen und Doktrinen systematisch aufgearbeitet und \"ubersichtlich dargestellt.  Insbesondere hat sid den Technikbegriff des BGH verst\"andlich dargestellt.  Um emotionslose \"Aquidistanz bem\"uht, l\"asst sie dabei manchmal Argumente aus dem EPA-Freundeskreis recht unkritisch durchgehen (\"ubernimmt z.B. deren Behauptung, die \"Anderung der EPA-Rechtsprechung sei durch \"Anderung der technischen Inhalte motiviert, nimmt Patentabteilungs-Stellungnahmen im Namen grosser Organisationen f\"ur bare M\"unze etc), bietet dennoch eine sehr empfehlenswerte Einf\"uhrung in die Thematik und auch einen erhellenden \"Uberblick \"uber die Swpat-Konsultation auf EU-Ebene.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/stidi/korcu/indexen.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant law science literature.
\end{quote}
\filbreak
\end{itemize}
\end{document}