     Gesellschaft fu"r Informatik e.V. (C) 2001    Seite 1 von 11      

  Stellungnahme der Gesellschaft fu"r Informatik e.V. (GI)  
      zum patentrechtlichen Schutz von Software   
  in Ansehung des Sondierungspapiers der EU-Kommission 

  (Verabschiedet vom Pra"sidium der GI am 30. Juni 2001) 

 

Zu a) Umfang der Harmonisierung  
Die Diskussion um die Patentfa"higkeit von Software besteht schon seit
vielen Jahren. Wa"hrend die Rechtsprechung hierzu zuna"chst Ende der
siebziger und in der ersten Ha"lfte der achtziger Jahre eine eher
restriktive Haltung eingenommen hatte, da man nach damaliger Auffassung
unter Software nichts Technisches im eigentlichen Sinne verstand, wurde
diese Haltung unter dem Eindruck, dass Software im Gegensatz zur Literatur
kein reines Sprachwerk darstellt, sondern dass der sprachwerkliche Aspekt -
so er denn im Einzelfall durch Verwendung einer Programmiersprache vorliegt
- nur der Definition und Beschreibung von etwas Funktionalem, na"mlich einer
abstrakten Maschine und somit einem technischen Gegenstand dient, nach und
nach aufgegeben. Hierdurch wurde Software - vor allem seit Beginn der
neunziger Jahre - mehr und mehr als patentfa"hig angesehen. Aktuell wird nun
in Europa vor dem Hintergrund von z.T. sicherlich berechtigterweise mit
Sorge betrachteten Fehlentwicklungen im amerikanischen Rechtsraum, so
insbesondere vor dem Hintergrund

-  der Patentierung von durch Software realisierten Gescha"ftsmethoden
   unabha"ngig von den technischen Aspekten ihrer Implementierung, wie  

-  der mangelhaften fachlichen Pru"fung, ob die angemeldete Erfindung
   tatsa"chlich eine erfinderische Leistung darstellt und

-  des hinsichtlich der Mo"glichkeit, zu Unrecht erteilte Patente angreifen
   zu ko"nnen, unterentwickelten US-amerikanischen Verfahrensrechts

die Frage der Patentfa"higkeit von Software - und damit letztlich auch
u"berhaupt von Technik - neu diskutiert. Insbesondere die OpenSource
Bewegung hegt hier erhebliche Befu"rchtungen. 


     GI zum patentrechtlichen Schutz von Software    Seite 2 von 11  

Insgesamt gesehen scheint die Diskussion derzeit stark polarisiert, wobei
sie weniger auf einer sachlichen, denn auf einer ideologischen Ebene
gefu"hrt wird. Hier erscheint somit aus Sicht der GI eine sachliche
Betrachtung angezeigt:

Vor dem Hintergrund der bereits erwa"hnten in Europa mit Sorge betrachteten
und als Argument gegen das Patentsystem immer wieder herangezogenen
Fehlentwicklungen im amerikanischen Rechtsraum ist die GI der Auffassung,
dass die Harmonisierung auf der Grundlage des durch Art. 27 Abs. 1 TRIPS
geschaffenen Rechtsrahmens erfolgen sollte.

Dies heisst, dass fu"r Software - bei Vorliegen der individuellen
Patentierungsvoraussetzungen Neuheit, Erfindungsho"he, gewerbliche
Anwendbarkeit und ausreichende Offenbarung - immer dann Patente erteilt
werden sollten, so die Software denn als computer-implementierte Erfindung
zu einem Gebiet der Technik geho"rt.

Dies insbesondere  aus folgenden Gru"nden: 

1.  Das seit jeher bestehende Problem des angemessenen Schutzes fu"r
    Software liegt in deren Doppelcharakter begru"ndet, der, jedenfalls
    heute noch, den meisten Softwareprodukten zu eigen ist: Einerseits die
    sprachliche Verko"rperung des Programmtextes, andererseits die hierdurch
    definierte abstrakte Maschine.  Zum einen handelt es sich somit -
    zumindest im Falle herko"mmlicher Programmiermethoden - um ein
    Sprachwerk, das unter dem Schutz des Urheberrechts steht, zum anderen in
    Form der durch das Sprachwerk definierten abstrakten Maschine jedoch um
    einen technischen Gegenstand.  Dieser Doppelcharakter der Software hat
    bereits in der Vergangenheit zu vielen unauflo"sbaren Widerspru"chen
    gefu"hrt, sofern dem technischen Aspekt der Patentschutz versagt wurde;
    dies vor allem vor dem Hintergrund der prinzipiellen A"quivalenz von
    Hard- und Softwarelo"sungen. Software den Patentschutz zu versagen,
    hiesse daher ihren technischen Charakter entgegen der Realita"t fiktiv
    zu verneinen und damit unauflo"sbare Widerspru"che mit eben dieser
    Realita"t herzustellen und die hiermit verbundenen Probleme
    herbeizufu"hren. Dies findet sich auch besta"tigt, wenn man die
    gegenla"ufige Entwicklung auf Seiten der Hardware betrachtet: Hier
    werden Schaltkreise, also klassische Technik mehr und mehr durch
    Software beschrieben und auch mittels dieser Software erzeugt (vgl.
    bspw. etwa VHDL-Programmierung, wie etwa von Fa. Hoschar). Die Grenzen
    zwischen Hard- und Software sind hier somit la"ngst aufgelo"st, eine
    patentrechtliche Unterscheidung ist daher sinnlos geworden. 


     GI zum patentrechtlichen Schutz von Software    Seite 3 von 11  

2.  Ebenso ist hervorzuheben, dass auch im Falle neuerer Tendenzen in der
    Softwareentwicklung der sprachwerkliche Aspekt immer mehr in den
    Hintergrund tritt. Die Programmiersprache wird immer mehr durch
    graphisch orientierte Entwicklungswerkzeuge abgelo"st, deren Produkt
    kein Sprachwerk mehr, sondern vielmehr eine Art modulorientierter
    "Schaltplan" ist, der den technischen Teil der Software deutlicher
    hervortreten la"sst. Inwieweit hier ein urheberrechtlicher Schutz
    u"berhaupt besteht und wie weit bzw. wie eng dieser u"berhaupt greift,
    ist dabei vo"llig ungekla"rt. Eine a"hnliche Problematik ergibt sich
    fu"r bestimmte Gebiete der ku"nstlichen Intelligenz: So kann die
    Topologie eines neuronalen Netzes (eine Technik der ku"nstlichen
    Intelligenz) wohl kaum als Sprachwerk bezeichnet werden. Will man
    derartige Produkte daher schutzlos stellen ?

Angesichts dieser Problematik haben Patentbeho"rden und Gerichte in Europa
vor allem in den letzten zehn Jahren versucht ihre Erteilungspraxis auch auf
die Erfordernisse der Patentierung von Software einzurichten.

Fu"r den Patentschutz computer-implementierter Erfindungen bietet diese
Erteilungspraxis der europa"ischen wie der deutschen Beho"rden, wie auch die
Rechtsprechung der nationalen (Bundespatentgericht, BGH) und europa"ischen
Gerichte (Beschwerdekammern des Europa"ischen Patentamts) nach Auffassung
der GI eine sachgerechte Grundlage, da diese, wenn auch unterschiedlich
pointiert, sa"mtlich und im Gegensatz zur US-amerikanischen Praxis am
Erfordernis des technischen Charakters der jeweiligen
computer-implementierten Erfindung festhalten, was aus Sicht der GI ein
geeignetes Korrektiv zur allzu u"berbordenden Patentierungspraxis im
USamerikanischen Rechtsraum darstellt.

Zu den einzelnen Elementen des Sondierungspapiers der EU-Kommission zu
diesem Thema ist dabei aus der Perspektive der GI folgendes zu bemerken,
wobei der dortige Punkt i. bereits vorstehend in der Einleitung abgehandelt
wurde:

Zu ii. - Komplementarita"t von Patent- und Urheberrechtsschutz  

Zu Unrecht geht hier - nach Auffassung der GI - das Sondierungspapier der
EU-Kommission von einer Komplementarita"t, d.h. einem wechselseitigen
Ausschluss von Patent- und Urheberrecht aus. Nach Ansicht der GI ist hierbei
hingegen im Falle des Vorliegens eines Patents fu"r eine Software von einem
u"berlagernden Rechtsschutz durch die unterschiedlichen Regime des 

     GI zum patentrechtlichen Schutz von Software    Seite 4 von 11  

Patent- und Urheberrechts nach Art einer Kumulation auszugehen, wie dies in
anderen Gebieten des geistigen Eigentums seit jeher der Fall ist, wo etwa
eine Bildmarke sowohl einerseits als Marke unter dem Gesichtspunkt des
Herkunftshinweises auf ein bestimmtes Unternehmen aber andererseits auch
hinsichtlich ihrer bildlichen Darstellung - bei Vorliegen der
Voraussetzungen einer individuellen scho"pferischen Leistung - als Werk der
bildenden Kunst Urheberrechtsschutz geniessen kann.

Hiervon geht auch die EU-Richtlinie zum Schutz von Computerprogrammen
(91/250/EWG) aus, die aus urheberrechtlicher Sicht ausdru"cklich klarstellt,
dass ein Urheberrechtsschutz etwaigen Patentrechten nicht entgegensteht
(vgl. Art. 9 Abs. 1 der Richtlinie).

Von daher bedarf es aus Sicht der GI hier - zur Vermeidung von etwaigen
Missversta"ndnissen - auch aus patentrechtlicher Sicht einer solchen
ausdru"cklichen Klarstellung.

Zu iii. - Das Erfordernis eines nicht naheliegenden technischen Beitrages  

Dieses Erfordernis ha"lt die GI in der im Sondierungspapier wiedergegebenen
Form fu"r problematisch, da es zumindest Anmutungen an die in Deutschland
sogenannte ehemals entwickelte Kerntheorie entha"lt und so den - wenn auch
ungewollten - Eindruck erwecken ko"nnte, der erfinderische Beitrag einer
computer-implementierten Erfindung mu"sse ausserhalb des Gebietes der
Software liegen. Dies kann und darf aus den eingangs bereits erwa"hnten
Gru"nden jedoch nicht sein. Vielmehr muss es nach Auffassung der GI in
U"bereinstimmung mit der derzeitigen Rechtsprechung des BGH so sein, dass
die beanspruchte Erfindung in einer Gesamtschau darauf hin zu beurteilen
ist, ob sie einen technischen Beitrag liefert, der sich insbesondere auch
allein aus softwarebezogenen und nach Auffassung der GI daher aus den o.a.
Gru"nden allein bereits schon technischen Merkmalen, aber auch aus einer
Wechselwirkung zwischen technischen und untechnischen Merkmalen ergeben
kann, wobei untechnische Merkmale ausdru"cklich nicht mit softwarebezogenen
Merkmalen zu verwechseln sind. Eine isolierte Betrachtung der einzelnen
Merkmale ist hierbei ungeeignet, derartige technische Beitra"ge angemessen
zu beru"cksichtigen.

Zu iv. - Das Kriterium der "technischen U"berlegungen"  

Das Kriterium der sogenannten "technischen U"berlegungen" als einer
expliziten Mo"glichkeit des Nachweises eines technischen Beitrages, das
prima"r an die sogenannte SOHEI-Entscheidung der Beschwerdekammern des
Europa"ischen Patentamts anknu"pft, ist dem Grunde nach zu begru"ssen, 

     GI zum patentrechtlichen Schutz von Software    Seite 5 von 11  

da hiermit anerkannt wird, dass bereits in der fachma"nnischen Erkenntnis
eines computerimplementierten Lo"sungsweges, d.h. in der Erkenntnis der
technischen Mo"glichkeit einer computer-implementierten Lo"sung und ihrer
Umsetzung eine patentierbare Erfindung liegen kann.  Dabei kommt es jedoch -
jedenfalls aus Sicht der GI - nicht so sehr, wie es der erste Absatz der
Anmerkungen im Sondierungspapier vermuten la"sst, auf die Kenntnis der
Hardwarefunktionen des Computers, sondern vielmehr auf die ,intelligente`
und damit womo"glich erfinderische Erkenntnis der Mo"glichkeit einer
softwaretechnischen Implementierung und ihrer Umsetzung in zumindest einer
Ausfu"hrungsform an, da die Hardwarefunktionen eines Computers, so es sich
denn um seine gebra"uchlichste Erscheinungsform in Gestalt des
von-NeumannschenUniversalcomputers handelt, dem Fachmann, also dem
Informatiker oder einschla"gig ta"tigen Ingenieur, ohnehin bekannt sind.

Die Anwendung dieses Kriteriums entspricht zudem der langja"hrigen Forderung
der GI, Leistungen auf dem Gebiet der Informatik nach den gleichen oder
zumindest a"hnlichen Kriterien wie Leistung in anderen ingenieurma"ssig
betriebenen Disziplinen zu behandeln; sie wird daher unter den vorstehenden
Voraussetzungen ausdru"cklich begru"sst.

Zu v. -  Beurteilung technischer und nichttechnischer Merkmale - 
         Bedeutung fu"r gescha"ftliche Ta"tigkeiten ("business methods") 

Wie bereits Eingangs erwa"hnt, beobachtet die Facho"ffentlichkeit und auch
die GI die Tendenz zur Patentierung rein gescha"ftlicher Verfahren im
US-amerikanischen Rechtsraum mit Sorge. Eine vergleichbare Rechtsentwicklung
in Europa ha"lt die Gesellschaft jedenfalls fu"r grundsa"tzlich wenig
wu"nschenswert. Dies gilt insbesondere im Hinblick auf die fu"r die GI als
Interessensvertretung der professionellen Informatiker Deutschlands
relevanten computerimplementierten, insbesondere im Internet realisierten,
Gescha"ftsmethoden. Diese bedu"rfen jedoch einer Gliederung nach denkbaren
Fallkonstellationen, wie folgt

1.  Die Gescha"ftsmethode fu"r sich genommen ist bekannt und ihre
    Implementierung fu"r sich genommen dem Durchschnittsfachmann gela"ufig.

2.  Die Gescha"ftsmethode fu"r sich genommen ist bekannt und ihre
    Implementierung fu"r sich genommen dem Durchschnittsfachmann nicht
    gela"ufig.

3.  Die Gescha"ftsmethode fu"r sich genommen ist neu und ihre
    Implementierung fu"r sich genommen dem Durchschnittsfachmann nicht
    gela"ufig. 

     GI zum patentrechtlichen Schutz von Software    Seite 6 von 11 

4.  Die Gescha"ftsmethode fu"r sich genommen ist neu und ihre
    Implementierung fu"r sich genommen dem Durchschnittsfachmann gela"ufig.

Zuna"chst zu 1.:  Die Gescha"ftsmethode fu"r sich genommen ist bekannt und
                  ihre Implementierung fu"r sich genommen dem
                  Durchschnittsfachmann gela"ufig.

Diese Fallgruppe Fall erregt die besondere Besorgnis der Gesellschaft, da
sie aus Sicht der GI die Mehrzahl der bislang zum Patent angemeldeten
sogenannten "business methods" betreffen du"rfte. Hier steht zu befu"rchten,
dass ungerechtfertigte Monopolpositionen aufgebaut werden, die ihre
vorgebliche Berechtigung allein aus dem Umstand ziehen wollen, eine an und
fu"r sich bekannte gescha"ftliche Verfahrensweise mit u"blichen Mitteln
Computer- bzw. Internet-tauglich gemacht zu haben. Im Klartext heisst dies,
dass jedenfalls nach Auffassung der GI eine fu"r sich genommen nicht
patentfa"hige, da untechnische Gescha"ftsmethode nicht allein dadurch zu
einem technischen Gegenstand wird, dass sie sich technischer Mittel, also
einer Softwareimplementierung bedient, ebenso wenig wie ein Roman dadurch
technisch wird, dass er mit einem technischen Gegenstand, sei es mit einem
Textverarbeitungssystem, sei es mit einer traditionellen Schreibmaschine
geschrieben wird. Das Erfordernis der Technik sollte hier - im Gegensatz zu
den USA - als Korrektiv zu einem sonst zu weit ausufernden Patentwesen
unbedingt beibehalten werden. In diesem Zusammenhang begru"sst die GI
ausdru"cklich die Entscheidung "Pension Benefits System" (Az. T 0931/95) der
Beschwerdekammern des Europa"ischen Patentamts vom Ergebnis, nicht jedoch
von der Herangehensweise an das Problem her (s.u.).

Hieraus ist jedoch nicht zu entnehmen, dass die GI die Mo"glichkeit einer
Patentierung immer auch fu"r den Fall ablehnt, dass die Gescha"ftsmethode
zwar neu, ihre Implementierung aber gleichfalls mit den dem
Durchschnittsfachmann gela"ufigen Mitteln zu bewerkstelligen ist (s.u.).

Zu 2. und 3.:  Die Gescha"ftsmethode fu"r sich genommen ist bekannt und ihre
               Implementierung fu"r sich genommen dem Durchschnittsfachmann
               nicht gela"ufig, sowie: die Gescha"ftsmethode fu"r sich
               genommen ist neu und ihre Implementierung fu"r sich genommen
               dem Durchschnittsfachmann nicht gela"ufig

Hier  stellt sich die Situation jedenfalls nach Auffassung der GI recht klar
dar, da beide Fallgruppen der SOHEI-Entscheidung der Beschwerdekammern des
Europa"ischen Patentamts unterfallen und hiernach eine dem
Durchschnittsfachmann nicht gela"ufige Implementierung von technischen 

     GI zum patentrechtlichen Schutz von Software    Seite 7 von 11  

U"berlegungen zeugt, die einen die Patentfa"higkeit begru"ndenden
technischen Beitrag ergeben, der die Erfindung dem Patentschutz zuga"nglich
machen sollte.

In diesen Fa"llen steht die Gescha"ftsmethode somit nicht im Zentrum der
Erfindung sondern wird allenfalls als Seiteneffekt u"ber den Schutz ihrer
womo"glich erfinderischen Implementierung geschu"tzt. Dies ist auch in
anderen Gebieten der Technik seit jeher mo"glich (vgl. etwa die
Lamellenku"hler-Entscheidung des BGH oder auch das Patent des Ku"nstlers
Yves Klein auf die Herstellung seines speziellen besonders intensiv
anmutenden Blau's) und begegnet aus Sicht der GI keinen Bedenken. Daru"ber
hinaus sei darauf hingewiesen, dass hiermit - wie vom Gesetzgeber vorgesehen
- auch kein Schutz der implementierten Gescha"ftsmethode als solche erlangt
wird, da andere, nicht beanspruchte Implementierungen selbstversta"ndlich
weiterhin mo"glich sind. Nur in dem seltenen Fall, dass sich die
Gescha"ftsmethode nur im Wege der beanspruchten Implementierung realisieren
la"sst, wird hierbei - gleichsam als Nebeneffekt - ein solcher Schutz
erreicht.

Schliesslich zu 4.:  Die Gescha"ftsmethode fu"r sich genommen ist neu und
                     ihre Implementierung fu"r sich genommen dem
                     Durchschnittsfachmann gela"ufig.

Die letzte Fallgruppe stellt schliesslich aus Sicht der GI das eigentliche
Problem der auf computer-implementierten Erfindungen gerichteten sogenannten
'business method' - Patentantra"ge dar.

Hier ist eine rein an den Einzelmerkmalen der jeweilig beantragten
Patentanspru"che orientierte Betrachtung, wie sie etwa der ju"ngsten Pension
Benefit Entscheidung der Beschwerdekammern des Europa"ischen Patentamts
(s.o.). zu entnehmen ist, ungeeignet, zu einer dem Einzelfall gerecht
werdenden Beurteilung zu kommen.

Dies aus folgendem Grund: Wie bereits zu Pkt. iii (s.o.) dargelegt, muss es
nach Auffassung der GI in U"bereinstimmung mit der derzeitigen
Rechtsprechung des BGH so sein, dass die beanspruchte Erfindung in einer
Gesamtschau darauf hin zu beurteilen ist, ob sie einen technischen Beitrag
liefert, der sich im Einzelfall auch aus einer Wechselwirkung zwischen
technischen und untechnischen Merkmalen erst ergeben kann. So ist es etwa
denkbar, dass ein technischer Beitrag darin besteht, dass der Erfinder es
erkannt hat, dass eine neue Gescha"ftsmethode u"berhaupt erst durch die
Verwendung geeigneter technischer Mittel, wie etwa Datenweitverkehrsnetze
und unter Verwendung fu"r sich genommen u.U. u"blicher Implementierungen
mo"glich wird. Hier liegt dann ein Fall vor, in dem der technische Beitrag
und die evtl. erfinderische Leistung darin be- 

      GI zum patentrechtlichen Schutz von Software Seite 8 von 11 

steht, die Eignung technischer Mittel fu"r die Durchfu"hrung vo"llig neuer
gescha"ftlicher Verfahren zu erkennen. Es tritt eine Wechselwirkung zwischen
technischen und untechnischen Merkmalen ein, die im Einzelfall auch
erfinderisch sein kann. Das gescha"ftliche Verfahren ist hier ohne die in
erfinderischer Weise als geeignet erkannte Technik u"berhaupt nicht denkbar.
Rechtsystematisch wa"re dieser Fall somit der SOHEI-Rechtsprechung, also den
notwendigen technischen U"berlegungen zuzuordnen. Insgesamt gesehen du"rfte
dieser Fall jedoch eher selten gegeben sein, da er

1.  die Entwicklung einer vo"llig neuen Gescha"ftsmethode ,  

2.  deren Abha"ngigkeit von der als geeignet erkannten Technik, und 

3.  den Erkenntnisprozess hinsichtlich der Eignung der Technik als
    erfinderisch, d.h. fu"r den Durchschnittsfachmann als nicht naheliegend

voraussetzt. Diese Hu"rden, insbesondere die Voraussetzung zu 3.) du"rften
eher selten zu u"berwinden sein. Gelingt dies jedoch, so ist jedenfalls aus
der Sicht der GI eine technische Innovation, na"mlich eine vo"llig von der
verwendeten Technik abha"ngige neue gescha"ftliche Ta"tigkeit geboren, die
auch in rein technischer Hinsicht, na"mlich in Hinsicht auf die Eignung der
verwendeten Technik, neu und erfinderisch ist und damit Patentschutz
verdient.

Zu vi. -  Zula"ssige Patentanspru"che  

Die zula"ssigen Patentanspru"che sollten sich auf alle Kategorien
computer-implementierbarer Erfindungen beziehen du"rfen. Also insbesondere
Erzeugnisanspru"che, die auf den in bestimmter Weise programmtechnisch
eingerichteten Computer zielen, wie auch Verfahrensanspru"che, die das im
Computer zur Anwendung gelangende Verfahren betreffen. Daru"ber hinaus
sollten selbstversta"ndlich auch, wie es der derzeitigen europa"ischen
Rechtssituation entspricht, sogenannte Beauregard-Anspru"che(1), insbesondere
sogenannte medialess-Beauregard-Anspru"che zu- 

(1) Im Falle der sogenannten Beauregard-Anspru"che handelt es sich um
Patentanspru"che die auf ein Computerprogrammprodukt auf einem Datentra"ger
gerichtet sind, um die Durchsetzung des Patents im Verletzungsfalle
womo"glich zu erleichtern, da der Patentinhaber ansonsten auf den u.U.
schwa"cheren Schutz des $ 9 Ziff. 2 PatG, welcher es verbietet, ein
Verfahren unter bestimmten dort genannten Voraussetzungen (positive Kenntnis
oder in Ermangelung dessen Offensichtlichkeit des Verbots) zur Anwendung
anzubieten oder des $ 10 PatG, welcher die sogenannte mittelbare
Patentverletzung normiert verwiesen ist.  Erstmalig wurden in Europa
derartige Beauregard-Anspru"che durch die Entscheidungen T 935/97 und
1173/97 der Beschwerdekammern des Europa"ischen Patentamts zugelassen.
Erga"nzend zu diesen Beauregard-Anspru"chen kommen auch sogenannte
'medialess'-Beauregard-Anspru"che in Frage, bei welchen das Computerprogramm
nicht auf einem Medium sondern auf einem Tra"gersignal, also etwa zum
Download im Netz geschu"tzt wird.

         GI zum patentrechtlichen Schutz von Software Seite 9 von 11 

la"ssig sein, die das Computerprogrammprodukt auf einem Datentra"ger oder
ganz ohne Datentra"ger betreffen, soweit die der Implementierung des
Programms zugrunde liegende Erfindung selbst technischer Natur ist und die
weiteren Patentierungsvoraussetzungen im Einzelfall erfu"llt.  Schliesslich
sind insbesondere auch solche Anspru"che zuzulassen, die sogenannte
funktionale Informationen betreffen, d.h. solche Informationen, die nicht
rein der menschlichen Unterrichtung dienen, sondern vielmehr der
planma"ssigen Steuerung von Gera"ten, wie etwa bestimmte Formate fu"r Bild-
oder Tondaten oder A"hnliches. Zudem sollten die zula"ssigen Anspru"che
nicht abschliessend normativ geregelt werden, damit hier der
patentrechtlichen Fassbarkeit der zuku"nftigen technischen Entwicklung nicht
entgegengewirkt wird. Hier ist es vielmehr erforderlich der Rechtsprechung
im Einzelfall den fu"r die Anpassung an die Technik notwendigen
Bewegungsspielraum zu lassen.

Zu vii. -  Allgemeines Patentrecht bleibt wesentliche Schutzgrundlage  

Die Forderung, dass das allgemeine Patentrecht auch wesentliche
Schutzgrundlage der computer-implementierten Erfindungen bleibt, entspricht
der Auffassung der GI, dies insbesondere angesichts der Tatsache, dass im
europa"ischen Verfahrensrecht das Institut des Einspruchs gegeben ist,
welches dem Wettbewerber erlaubt, gegen seiner Auffassung nach zu Unrecht
erteilte Patente vorgehen zu ko"nnen, ohne die ho"heren Risiken eines
Zivilprozesses auf sich nehmen zu mu"ssen.

In materiellrechtlicher Hinsicht erlaubt sich die Gesellschaft jedoch einige
Anmerkungen, aus der sich fu"r die derzeitige Erteilungspraxis etliche
Anregungen ergeben:

Viele der derzeit in die Diskussion geratenen Patente sind kein Problem der
prinzipiellen Patentfa"higkeit von Software, sondern vielmehr ein Produkt
einer womo"glich unzureichenden Pru"fungspraxis in den USA. Um diese
Problematik anhand eines Beispiels etwas zu veranschaulichen, sei darauf
hingewiesen, dass das in der Diskussion immer wieder angefu"hrte (inzwischen
zuru"ckgenommene) "Amazon 1-click-Patent" auch in Europa angemeldet wurde,
hier jedoch aufgrund des qualitativ wohl besseren
Patenterteilungs-Verfahrens ein Recherchenbericht erstellt wurde, der eine
Erteilung des Patentes - zumindest im US-amerikanischen Umfang eher
unwahrscheinlich erscheinen la"sst.

Hier gilt es jedoch gleichwohl, den in der O"ffentlichkeit Besorgnis
erregenden Entwicklungen hinsichtlich der Erteilung sogenannter - ohne
Zweifel unerwu"nschter - "Trivialpatente" von Anfang an entgegenzuwirken: Es
muss darauf geachtet werden, dass die Patenta"mter in die Lage versetzt
werden, den neuen an sie gerichteten Anforderungen hinsichtlich der Pru"fung
von Softwareerfindungen sowohl sachlich, d.h. durch entsprechende
Recherchemo"glichkeiten - auch in Nicht-Patentliteratur - als auch personell
durch geeignete Fachleute (Informatiker in den Pru"fungs- und

     GI zum patentrechtlichen Schutz von Software Seite 10 von 11 

Einspruchsabteilungen, wie auch bei den Patentgerichten) nachkommen zu
ko"nnen, um hier eine gleichermassen hohe Qualita"t der erteilten Patente zu
gewa"hrleisten. Dabei ist bei Softwareerfindungen neben den Kriterien
Neuheit, Erfindungsho"he und gewerbliche Anwendbarkeit insbesondere auch auf
die patentrechtliche Forderung der ausreichenden Offenbarung der Erfindung
sorgfa"ltig zu achten, damit Lo"sungen und keine Probleme patentiert werden. 
Die GI meint hier ausgemacht zu haben, dass eine gro"ssere Anzahl erteilter
Patente, insbesondere auch solche, die Anlass zu Diskussionen in der
O"ffentlichkeit gegeben haben in dieser Hinsicht zumindest zweifelhaft sind.
Nur bei Gewa"hrleistung der vorgenannten Voraussetzungen wird das bestehende
patentrechtliche System nicht diskreditiert und kann auch in der
O"ffentlichkeit auf weitere Akzeptanz hoffen.

Eine brachenspezifische Verku"rzung der Patentdauer auf etwa 5 Jahre scheint
der Dynamik der Softwarebranche angemessen.

Zu b)   Auswirkungen der Harmonisierung  

Bei einem Versagen des Patentschutzes fu"r Software wa"re zu befu"rchten,
dass die Investoren in Software wieder vermehrt auf Geheimhaltung statt auf
Offenheit (eine Folge des Patentrechts und im u"brigen auch eine Forderung
der OpenSource Bewegung) setzen wu"rden, was vor allem in Bereichen wie der
Kryptographie, des E-Governement etc. sicherlich eher unerwu"nscht ist.  Zum
anderen wu"rde aber sicherlich auch eine Entwicklung einsetzen, die vermehrt
mit Methoden des Wettbewerbsrechts (Stichwort: wettbewerbliche Eigenart)
Schutz fu"r die technischen Aspekte der Software suchen wu"rde, da der
Urheberrechtsschutz fu"r Software - wie bereits dargelegt - oftmals - wenn
u"berhaupt - nur einen unzureichenden Schutzbereich abgibt. Was dies
bedeutet, kann angesichts der manchmal nur schwer kalkulierbaren, weil kaum
recherchierbaren Lage im Designrecht beobachtet werden. Hier mu"ssen die
Marktteilnehmer damit rechnen, von ihren Wettbewerbern auf Unterlassung in
Anspruch genommen zu werden, und zwar nicht aus einem Schutzrecht, welches
fu"r sie recherchierbar gewesen wa"re, sondern allein aus einer
Marktposition des womo"glichen Kla"gers heraus, der in einer bestimmten
Gestaltung als wettbewerbliche Eigenart des Produktes einen Hinweis auf sein
Unternehmen sieht, etwa in der Art, wie eine Bedienung der Software
(Stichwort: look and feel) womo"glich auch einen Herkunftshinweis auf einen
bestimmten Hersteller zu vermitteln vermag. Eine solche Entwicklung auch im
Softwarebereich wa"re sicherlich gleichfalls nicht unproblematisch. Die
Gewa"hrung eines recherchierbaren Patentschutzes vermag einer solchen
Tendenz wirksam zu begegnen, da der erga"nzende wettbewerbsrechtliche
Leistungsschutz anerkanntermassen nicht zu einer ReMonopolisierung eines an
sich abgelaufenen Schutzrechtes fu"hren darf.

     GI zum patentrechtlichen Schutz von Software    Seite 11 von 11  

Auch treffen die eher wirtschaftlich motivierten Argumente von Patentgegnern
nicht zu: Patentrechtlichen Schutz suchen oftmals kleine und mittlere
Unternehmen fu"r ihre auf Software basierenden Ideen, etwa im Bereich des
e-Business oder auch der Telekommunikation, um sich so vor der Nachahmung
durch Marktbeherrscher zu schu"tzen (vgl. z.B. Wolfgang Klauser und Achim
Rust in: Der Spiegel 31/2000, 83). Dabei kommt auch der Frage, dass auf
diese Weise Venture Capital Unternehmen viel eher bereit sind solchen
"Startups", die Schutzrechte auf ihre Ideen angemeldet haben, Kapital zur
Verfu"gung zu stellen, eine immer gro"ssere Bedeutung zu.

Schliesslich sollte im Rahmen der Diskussion auch dem Aspekt der
internationalen Verflechtungen Aufmerksamkeit gewidmet werden. Da in den USA
ein Patentschutz auf Software und insbesondere auch e-Business Methoden
mo"glich ist und auch bleiben wird, wu"rden in dem Falle, dass der Weg
hierzu in Europa jedoch versperrt wu"rde, hiesige Unternehmen in vielen
Fa"llen nicht vor einer Inanspruchnahme hinsichtlich der Verletzung
amerikanischer Schutzrechte bewahrt bleiben. Dies kann etwa im Internet auch
bereits dann geschehen, wenn das vorgehaltene Angebot zwar nur in Europa in
das Netz eingestellt wu"rde, sich gleichwohl aber auch an USamerikanische
Verkehrskreise wendet, die es ja jederzeit abrufen und wahrnehmen ko"nnen.
Ein Versagen des Patentschutzes in Europa ha"tte dann zur Folge, dass die
Unternehmen zwar unter Umsta"nden in Anspruch genommen werden ko"nnten,
ihnen aber oftmals ein "Deal" im Rahmen von Cross-Lizenzvertra"gen in Europa
mangels Patentschutzmo"glichkeiten hier versagt bliebe.  Diesem Umstand
wurde im Rahmen der Diskussion bislang nach Kenntnis der Gesellschaft noch
u"berhaupt gar keine Aufmerksamkeit gewidmet.

Zusammenfassend kann daher gesagt werden:  

Ein Versagen des patentrechtlichen Schutzes von Software ist letztlich
gleichbedeutend mit einer Ablehnung patentrechlichen Schutzes von
technischen Gegensta"nden u"berhaupt, da Software immer auch - meist sogar
ganz u"berwiegend - eine technische Seite aufweist. 
