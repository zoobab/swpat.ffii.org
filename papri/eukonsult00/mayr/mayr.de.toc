\contentsline {section}{\numberline {1}Das GI-Papier als FAQ: Sammlung Popul\"{a}rer Irrt\"{u}mer}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Abstrakte Maschinen technisch? ergo patentierbar?}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Gruselpatente nur in USA? Kritik nur von Ideologen?}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Sachzw\"{a}nge als Ursprung aller Sachlichkeit ?}{6}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Dualer Gegenstand erfordert zweigleisigen Rechtschutz ?}{7}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Visuelles Programmieren = Technisches Erfinden ?}{10}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Objektivierte menschliche Intelligenz besonders patentbed\"{u}rftig ?}{11}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}``Technik'': Ein Wort als Bollwerk ?}{12}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Urheberrecht und Patentrecht in reibungsloser Kumulation ?}{13}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}EPA-Forderung nach technischem Beitrag zu restriktiv ?}{14}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Freiheit f\"{u}r die Zensur von Textstrukturen aller Art ?}{18}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}Einspruchsverfahren als Allheilmittel ?}{19}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}K\"{u}rzere Laufzeit f\"{u}r Softwarepatente trotz Nicht-Unterscheidbarkeit und TRIPS ?}{20}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Patente f\"{o}rdern Offenlegung ?}{20}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}Patente fangen Wildwuchs des Wettbewerbsrechts ab ?}{21}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}Patente n\"{u}tzen kleineren und mittleren Unternehmen ?}{22}{subsection.1.15}
\contentsline {subsection}{\numberline {1.16}Patentsystem steht und f\"{a}llt mit Softwarepatenten ?}{22}{subsection.1.16}
\contentsline {section}{\numberline {2}Kritik am GI-Papier und weitere Lekt\"{u}re}{23}{section.2}
