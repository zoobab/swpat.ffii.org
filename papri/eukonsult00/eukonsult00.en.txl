<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: European Consultation on the Patentability of Computer-Implemented Rules of Organisation and Calculation (= Programs for Computers)

#descr: On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an %(q:independent study), carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.

#Hte: How it came about

#DsI: Some examples of the %(q:Independet Contractor)'s bias

#Hon: How you can help

#CtW: Consultation Submissions

#Elt: Evaluation

#Fea: Further Reading

#IEe: In summer 2000 the scene was set for legalising software patents in Europe.  For several years, the %(ip:EU Commission's Industrial Property Unit) had planned to rubberstamp the European Patent Office's policy of granting patents on rules or organisation and calculation.  The last necessary step was the official removal of all restrictions on patentability from the law or at least a deletion of the %(q:programs for computers) in art 52 of the European Patent Convention (EPC).  Moreover, the European Commission was scheduled to become a major player in a future European patent system, which was to be based on Community law rather than on an intergovernmental negotiation process.  As a part of this transition, the European Commission was to issue a software patentability directive.  Under the name of %(q:clarification) and %(q:harmonisation) the existing legal framework was to be discarded and instead the practise of the European Patent Office was to be legalised and made binding for national courts, many of which had so far refused to follow the EPO on its adventurous course, either by sticking to the letter and spirit of the existing law or by developping their own halfway solutions.  In 1997 the European Commission outlined the contents of a software directive in a %(q:Greenpaper on the Community Patent) and inserted it into the rear part of a long resolution text which was rubberstamped by the European Parliament.  Yet, this met with unexpected resistance.  By summer 2000 a Petition for a Software Patent Free Europe had gained the support of 50000 signatures, 200 software companies and numerous politicians of all camps.  Under the impression of these developments, the governments of FR+DE+UK decided not to delete the computer program exclusion for the time being and instead hand over the issue to the European Commission.  The patent law experts at the CEC Patent Unit then %(le:opened) a public consultation on 2001-10-15 which was to last until 2001-12-15 and thus cover exactly those weeks, during which the EPC had been scheduled to be changed.  Art 52(2) EPC was as a result left untouched, and instead the %(pm:patent movement) was mobilised by means of a specifically focussed consultation call:

#E1i: CEC Indprop 2000-10-19: Swpat Consultation Paper

#AWe: On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which echoes the rhetoric used by the European Patent Office in recent years to justify its practise of granting software patents against the letter and spirit of the EPC, and has called on companies and industry associations to comment on this reasoning.  The questions were about legal technicalities which understandable and interesting only for the patent lawyer friends of the IndProp unit, whose mobilisation was apparently the chief purpose of the consultation.

#itb: Some additional numerical analysis to a report produced by PbT Consultants.

#ewe: The chief author of the evaluation report, Stewerd Davidson of PbT Consultants, had responded to Jozèf Halbersztadt, polish patent examiner, cautioning that Halbersztadt's method of statistical evaluation is not appropriate because the consultation was not a Gallup poll.  Instead it would be appropriate to just characterise %(q:the two viewpoints) and let the reader weigh them.  Halbersztadt responds to this, charging (1) that the report is rich in numbers and tries to manipulate opinion with statistics anyway, (2) the attempted characterisation of viewpoints is shallow and also manipulative.

#fWf: For Software Patents 85 responses from 1447

#ifP: Against Software Patent

#aiu: Against in groups:

#nvu: Individuals

#SME: SMEs

#rnr: Large Enterprises

#sio: Associations

#Use: User

#tdn: Student

#cdm: Academic

#fWo: Software Developer

#Wen: IP Professional

#ont: Governmental

#Ttw: The basic question of whether computer programs (computer-implemented rules of organisation and calculation) are inventions in the sense of patent law (art 52 EPC) was not asked.  Instead, the CEC Patent Unit implied that the answer must be yes.  They talked about %(q:computer-implemented inventions) and used this term as a synonym for %(q:computer-implemented rules of calculation and organisation) rather than for true technical inventions which can be implemented under program control and whose patentability is beyond doubt.  Thus the EU Industrial Property Unit not only implied an answer where it should have asked a question, but also falsely attributed silly positions to the software patent critics, before even opening the consultation.

#Tmi: The accompanying %(q:independent study), written by some %(rh:well-known EU software patent lobby activists), again reiterated the usuall patent lawyer credo in the guise of a somewhat more objective wording.  This led the study to draw conclusions such as %(q:any proposition to extend the patent system beyond the status quo cannot be claimed to rely on scientific evidence).  In this context %(q:status quo) refers to the practise of the European Patent Office's Technical Boards of Appeal.  The study does not even consider the possibility of criticising that practise and returning to the European Patent Convention.  Nor does it explain the concept of %(q:technical invention), which is the underlying spirit of the European Patent Convention.  Moreover the study predicts that software patents will gradually be accepted and used by the open source software community for its own benefit.

#Ylt: In spite of all this care taken to mobilise a network of patent professionals and create the impression of a pro-patent mainstream opinion, the results of the mobilisation campaign did not really look very convincing as a basis for arguing the case of software patentability.

#Whi: While at the beginning, the %(ip:CEC Indprop Unit) still wanted to quickly issue a directive and rallied national governments for this purpose, it gradually turned out that these governments were not very eager to go along.  Also, the CEC Patent Unit itself apparently found that this consultation was not a good basis for their plans.  They reported to the governments that the consultation result showed an %(q:overwhelming vote of the concerned industries in favor of software patents) and only a minority, mainly from the %(q:opensource movement), was clamoring against software patents.  But they did were not eager to display the results of the consultation on their website, and it was very hard to press them to publish at least a small part of the submissions.  Most submissions have not yet been published, often in spite of repeated requests by the authors to have them published, and those that have been published have been transformed to a PDF format that makes them hard to use in any hypertext-based dialogue.

#TUr: Thus the work of evaluating the consultation has largely been left to private initiatives like ours.  The CEC Patent Unit has however ordered a %(sa:summary analysis) from an %(q:independent contractor), which has been available internally since April and was released to the public at the beginning of August, together with the installation of a web-based %(pf:online discussion forum).

#Tnt: The CEC online discussion forum is only web based, without an archive, incompatible with normal posting and referencing and automatisation mechanisms, depending on Javascript, in general poorly accessible, unacceptable for the people who have developped high standards of public communication in newsgroups and %(mr:mailing lists).  Since the Internet became popular, our representatives and their PR agencies have been trying hard to reinvent the wheel of civil culture, apparently hoping to change its shape from a circle to a square that matches their tv-based communication habits.  Even though the Indprop Unit's attempt to at least engage in some form of dialogue seems commendable, citizens are not to blame for ignoring this type of government-designed %(q:e-democracy).  The forum attracted only 50 comments -- none from pro swpat people -- that were later hard to find and died very soon.

#Hmi: Here and elsewhere the report claims that the software patentability critics are unaware of 20000+ software patents granted. May be they just consider them invalid because the law does not allow them and are probably not enforceable.  In fact the report recognizes that almost everybody thinks the situation is ambigous and uncertain.  Little wonder that many people, specially not specialists in law, tend to disregard 20000 patents of uncertain practical value.

#TWW: The report says that the anti software patent position is %(q:radical) and would require %(q:extensive negotiation). It is the first time we hear the majority of people in a debate called %(q:radical) for defending the enforcement of current law.  We wonder what negotiation would be necessary, but we doubt the purporse of the report was to assess the realisability of the participants' demands.  We thought it was intended to summarize the opinions, proposals and demands of the participants, so that the interests at stake can be weighed more accurately.

#Thp: The table that summarizes opinion on the desired patentability subject matter does not give numbers, only vague qualitative descriptions. %(bq:The majority of responses fall into two distinct groups who hold substantially different views) fails to note that one of these two groups is majoritary and the other isn't.   We could also say that %(q:the majority of opinions of people fall into two distinct groups, those who think that murder is wrong and those that don't).  It's true, but it doesn't clarify anything. Most people think that murder is wrong.  And we hope we're not looking for a compromise in that.

#Ill: In 2.1.2 they say it's interesting to see that opponents to software patents use the term %(q:software patent) and proponents use %(q:computer implemented invention patent). It is not interesting once you realise that the few proponents are patent professionals and patent holders and the opponents are software developers, users or researchers. Little wonder that patent professionals are more used to the term invention and software engineers are more used to the word software rather than to euphemistic EPO slang.

#cWn: %(bc:The size of the %(q:petition) does not necessarily reflect the relative balance of views of the population as a whole). Certainly a call for comments is not a statistically planned survey, but obviously the minority in favor of software patents does not represent the majority of the population. We don't think the majority of the population has any position in the issue.  If the purpose of the patents is to promote innovation in some given field, certainly you want to listen to the developers, teachers, students and researchers in the field and not to the lawyers and managers, except when the field is law or management.  We don't think lawyers will innovate in software. So probably the population as a whole would vote against software patents if told who advocates for one or the other option. But this is as much speculation as is the report statement.  The only thing we know for sure is that more than 90% of the people that cared to speak to the EC about software patents do not want them.

#Iye: I fail to understand the table by country, and would very much welcome an explanantion. It says:

#ThW: The %(q:open source proportion) is the proportion of responses that was overtly from %(q:open source) lobbies, the vast majority was from the %(q:petition) organised by the Eurolinux Alliance.

#cWn2: %(q:Patent Support) is the proportion of responses sent directly to the European Comission that supported patents for computer implemented inventions and were not explicitly from %(q:open source) groups.

#TtT: This classification is quite irrelevant to the point of whether respondents supported or not extending the patentability to software, and apparently confuses the widespread and diverse opposition against software patentability (and for software copyright) with %(q:open source) advocacy and even with paid lobbying, in an attempt to associate the opposition with a phenomenon regarded as minoritarian.  On the other hand, the report fails to probe into the political background of the pro-patent group.  This group apparently belongs to the same small stratum of people to which the organisers of the consultation themselves belong and which they tried mobilised by the very way in which they organised their consultation.

#LWo: Let's do the remaining work together.

#LWx: Look at the consultation results table and find an submission that you think needs further analysis.  What we need most is a plain text version, the %(e:mail) address of the author, and your description or recension of the text.

#Lae: Click on the respective PDF files.  Read the PDF documents.

#AWn: Contact the submittants, ask them to send you text versions of their submssions for publication on our website.

#WWt: Write a recension.  You may have noted that many of the links on the left side menu are broken.  They are placed there in anticipation of your recension.

#LWW: Look at the %(el:Eurolinux Consultation) page, write records for some of the more interesting recensions from there, contact the authors and the %(ep:CEC Industrial Property Unit) to make sure they are published on the consultation page.

#MWr: Make meaningful contributions to the %(of:CEC online forum) and wherever appropriate, refer your readers to %(ur:this consultation page).

#MsW: Most of the submissions fall into one of the following categories:

#Sao: Small-scale studies from individual researchers/academics, carefully crafted, with original reasoning and references to relevant literature, e.g.

#Pif: Position papers from patent lawyers of large enterprises and industry associations that state the standard articles of faith from the credo of the patent movement and professionally play the mobilisation game of the CEC Industrial Property Unit, usually without more than schematic reference to the special situation of the body which they represent, written by a patent law expert of that body without review or even knowledge of other members of the organisation.  These articles generally show a lack of basic knowledge on the %(sk:European concept of technical invention and the debate on computer-programs that has been raging since the 60s).  Instead they dwell much on the %(tr:TRIPS fallacy) and other well-known patent movement propaganda myths.  It sounds as if these lawyers draw all their knowledge from a limited repertoire, comprising not much more than the %(q:independent study) on which this consultation was based.

#PWj: Position papers from patent practicioners and their associations.  Very similar to the above, reflecting the credo of patent circles, without knowledge of the concept of technical invention, without consideration for the risks of unlimited patentability, with naive claims about the economics of software, like the above group, but often a little bit less schematic, sometimes more aggressive.

#Pci: Position papers from other representatives of organisations with varying, less schematic viewpoints, not playing the game of the %(cp:consultation paper) but rather citing parts of it as appropriate for the (independently structured) argumentation.

#SdW: Statements of personal experience and opinion on software patentability, not closely oriented toward the paper

#Sda: Short political statements by individuals, most of whom fear negative effects of software patents.

#TuW: There are still more than 1300 unpublished statements, most of which probably belong to the last two categories and were initially published on the %(ec:Eurolinux consultation site).

#con: %(ED) column in %(CW)

#IWe: In the Internet age, high quality interactive consultations are even occurring when no government agency %(q:launches) them.  Several mailing lists on patent affairs have had very lively discussions on the subject of this consultation.  Some of these lists have archives, where you can study for yourself, whether pro software patentability arguments have ever been able to stand the test of public debate.   Our experience is that this is nowhere the case, not even on the mailing list of the European Patent Office.

#Tfn

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eukonsult00.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eukonsult00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

