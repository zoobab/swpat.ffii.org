<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Das TRIPs Abkommen - Immaterialg|terrechte im Licht der globalisierten
Handelspolitik

#descr: Dissertation der Rechtswissenschaftlichen Fakultdt der Universitdt
Z|rich zur Erlangung der W|rde eines Doktors der Rechtswissenschaft,
vorgelegt von Alesch Staehelin von Wattvil und Lichtgensteig SG,
genehmigt auf Antrag von Prof. Dr. Manfred Rehbinder.  Kapitel 7 S.
87-99: Patente.  Es wird ausf|hrlich die Bedeutung von Art 27 TRIPs
gew|rdigt.  Diese Bedeutung liegt im Abbau diverser
freihandelswidriger Praktiken nationaler Patentdmter, so z.B. der
Verpflichtung, das Produkt im Inland herzustellen oder der
Nichtber|cksichtigung von ausldndischen Erfindungszeitpunkten beim
amerikansichen Ersterfinderprinzip.  Der Erfindungs- und
Technikbegriff selbst wird von Art 27 TRIPs nicht ber|hrt: '...
innerhalb der Staatengemeinschaft weichen die Vorstellungen |ber
patentierbare Erfindungen stark voneinander ab.'

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: trips-staeh97 ;
# txtlang: xx ;
# End: ;

