<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kortum & Lerner 1998: What is behind the recent surge in patenting

#descr: Since the late 1980s, the number of patents granted to US companies by
the USPTO has sharply risen.  Many people believe that this is due to
a more patent-friendly policy created by political changes in the
early 80s such as the Bayh-Dole act and the institution of the Court
of Appeal for the Federal Circuit (CAFC).  This study collects
statistical data to suggest that a surge in patentable innovation and
an improvement in patent-oriented innovation managment may be more
important causes.  It also shows that software and biotech, while
considered to be the most important areas of innovation, still amount
for a total of only about 5% of the US patents.  While patent-oriented
innovation was on the rise, R&D investments on the whole dropped. 
This is one of a series of studies by Samuel Kortum and Josh Lerner
from the Department of Economics of Boston University.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: korler98 ;
# txtlang: en ;
# multlin: t ;
# End: ;

