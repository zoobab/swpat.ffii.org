<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wüsthoff 2001: Strategic Advice to Banks on Business Method Patents in
Europe

#descr: A prestigious german patent law firm explains that US business method
patents can in most cases be europeanised without much difficulty,
because the %(q:technical contribution) requirement does not present a
real obstacle.  They estimate the costs of researching which business
method patents a bank may be infringing to around 2 manyears of work
by professionals in the fields of patent law and data processing, plus
costs for licensing negotiations and buildup of a defensive patent
portfolio.

#low: currently not available there due to a redesign but you can obtain it
from Alexander Maron by mail to wuesthoff at wuesthoff de.

#Wit: The statement above originates from the acclaimed German IP law firm
%(q:Wuesthoff & Wuesthoff):

#Wej: In the last section before the Annex, they say:

#SWa: V. Strategic Considerations

#WWl2: Recently my firm had to explore the patent situation world-wide for
one the major banks. All existing U.S. patents and all published
European patent documents have been searched regarding any potential
risk for the banking business. The search was performed on the basis
of a profile combining both the leading banks, service providers in
that field and also key-terms relevant in the field of banking. The
search revealed more than 5000 patent documents which could possibly
have an impact on the activities of the bank.  The next step will be
to evaluate those patents regarding scope of protection and the
possible relevancy. The task being a list of patents which might be
infringed by the bank's business. Possible counter-measures are
envisaged in a next step (oppositions, negotiations etc.).

#tle: It is assumed that the evaluation of all patents will need two or
three patent experts, combined with two or three IT-experts from the
bank itself and a time period of three to six months. This is the
%(q:defensive) measure. As an %(q:offensive) measure it is advisable
that banks start filing patent applications wherever possible and
promising. Of course, the liberal practice in the U.S. requires to
file almost any business method for patent. These business methods are
always implemented by a computer (I have not seen any other example).
This computer implementation in most cases gives the opportunity to
look for some sort of a %(q:technical effect) or %(q:technical
consideration) or %(q:technical problem) etc. which might help to
obtain a European patent, in the long run. When studying a business
method computer software program it turns out that in most cases that
program includes at least one aspect (feature) which might qualify
under the European standards as %(q:technical). It is an advisable
measure for any bank these days to try to develop a patent portfolio
in order to have at least some arguments in hand when approached by
others for patent infringement.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: wuesthoff-bm01 ;
# txtlang: en ;
# multlin: t ;
# End: ;

