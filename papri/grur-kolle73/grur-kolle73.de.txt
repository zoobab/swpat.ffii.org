<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Dieser zweiteilige Artikel konstatiert eine ideologische Versteinerung in der Frage der Patentierbarkeit von Computerprogrammen und erläutert die unterschiedlichen Positionen der verschiedenen Länder.  Kontinentaleuropa und Australien lehnen die Patentierbarkeit ab, während England und die USA ihr zuneigen.
title: Gert Kolle: Der Rechtsschutz von Computerprogrammen aus nationaler und internationaler Sicht
IeW: Im Laufe der Zeit haben sich im Prinzip zwei Blöcke herausgebildet. Der anglo-amerikanische Rechtskreis, in dem mit Ausnahme Australiens die Computerprogramme in sehr weitgehendem Umfang zum Patentschutz zugelassen werden, einerseits, und der kontinental-europäische Rechtskreis, in dem mit Ausnahme der Bundesrepublik die Patentierbarkeit der Computerprogramme generell verneint wird, andererseits.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: grur-kolle73 ;
# txtlang: de ;
# End: ;

