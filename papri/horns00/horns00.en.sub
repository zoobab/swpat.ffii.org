\begin{subdocument}{horns00}{Horns 2000: Some Observations on the Controversy on %(q:Software Patents)}{http://swpat.ffii.org/papri/horns00/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2003/12/17 by FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{A German software patent attorney calls on his colleagues to lobby for the survival of their profession which he sees under attack by a coalition of neoliberals and leftists riding on a wave of growing popularity of open source software among politicians in Europe.   In particular the people from FFII and Eurolinux have successfully spread myths about the meaning of Art 52 EPC and proposed restrictions on patentability which would be disastrous for the patent system, because they would relegate it to a marginal role in information society, Horns warns.  If patent lawyers continue to be complacent about this development and to rely solely on their traditional means of paper based communication, they risk losing control of the legislative process.  Horns warns against underestimating the political potential of the free software movement.  It strikes a resonating chord with politicians and patent attorneys should therefore try to make peace with this movement and instead establish the patent system in other parts of the software industry.   In particular  they should try to build an economic rationale for the patent system in the area of embedded software.  Much of the argumentation of Horns is built on treating law texts in a formalistic way (e.g. putting ``software patents'' and ``as such'' in double quotes, arguing that Art 52 is about claim language rather than about inventions, quoting an overwhelming mass of more or less irrelevant law sources while suppressing relevant ones), as well as on historical myths about the EPC and on false assertions about FFII/Eurolinux.}
\begin{itemize}
\item
{\bf {\bf Horns: Some Observations on the Controversy on %(q:Software Patents)\footnote{\url{http://www.ipjur.com/episwpat.php3}}}}

\begin{quote}
The original article: apparently a speech held before patent attorney members of EPI.
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.en.html}}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf phm 2001/07: Weckruf von PA Horns an PA-Kollegen\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Jul/0090.html}}}}

\begin{quote}
first comment on this text
\end{quote}
\filbreak

\item
{\bf {\bf dr lenz 2001/07: Rechtfertigung im Pharmabereich nicht trivial\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Jul/0091.html}}}}

\begin{quote}
Contrary to what Horns asserts in his text, justifying the patent system in the pharmaceutical area is not a trivial task.  It is not always clear that patents are needed to sustaining R\&D in this field, and the price for this R\&D is often counted in thousands of lives of people particularly in poor countries.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

