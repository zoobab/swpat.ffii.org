<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#HtW: Horns 2000: Some Observations on the Controversy on %(q:Software
Patents)

#Aob: A German software patent attorney calls on his colleagues to lobby for
the survival of their profession which he sees under attack by a
coalition of neoliberals and leftists riding on a wave of growing
popularity of open source software among politicians in Europe.   In
particular the people from FFII and Eurolinux have successfully spread
myths about the meaning of Art 52 EPC and proposed restrictions on
patentability which would be disastrous for the patent system, because
they would relegate it to a marginal role in information society,
Horns warns.  If patent lawyers continue to be complacent about this
development and to rely solely on their traditional means of paper
based communication, they risk losing control of the legislative
process.  Horns warns against underestimating the political potential
of the free software movement.  It strikes a resonating chord with
politicians and patent attorneys should therefore try to make peace
with this movement and instead establish the patent system in other
parts of the software industry.   In particular  they should try to
build an economic rationale for the patent system in the area of
embedded software.  Much of the argumentation of Horns is built on
treating law texts in a formalistic way (e.g. putting %(q:software
patents) and %(q:as such) in double quotes, arguing that Art 52 is
about claim language rather than about inventions, quoting an
overwhelming mass of more or less irrelevant law sources while
suppressing relevant ones), as well as on historical myths about the
EPC and on false assertions about FFII/Eurolinux.

#Hno: Horns: Some Observations on the Controversy on %(q:Software Patents)

#Tnt2: The original article: apparently a speech held before patent attorney
members of EPI.

#pks: phm 2001/07: Weckruf von PA Horns an PA-Kollegen

#fmW: first comment on this text

#dha: dr lenz 2001/07: Rechtfertigung im Pharmabereich nicht trivial

#Cnd: Contrary to what Horns asserts in his text, justifying the patent
system in the pharmaceutical area is not a trivial task.  It is not
always clear that patents are needed to sustaining R&D in this field,
and the price for this R&D is often counted in thousands of lives of
people particularly in poor countries.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: horns00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

