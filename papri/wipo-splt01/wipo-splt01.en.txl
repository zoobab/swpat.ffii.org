<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Substantive Patent Law Treaty Draft: Unlimited Patentability and
Strict Limits on Patent Quality to be hardcoded into international law

#descr: Through international patent treaties, the worldwide patent movement,
a lobby of patent lawyers who speak in the name of multinational
corporations, industry associations and governments, has already
installed an unflexible system, where patents of 20 years runtime have
to be granted for %(q:any invention in any field of technology).  Now
the arm of the patent movement at the United Nations, the %(e:World
Intellectual Property Organisation) (WIPO) seems to be preparing a
further advance in what a study report of the United Nations
Development Project (UNDP) calls %(q:the relentless march of
intellectual property).

#Adi: A %{SPLT} will be discussed during %(nr:WIPO negotiation rounds) in
May and November 2001.

#WWd: WIPO proposes worldwide unlimited patentability, rejecting limitation
to the technical field

#WWl: WIPO undefines %(q:industrial application)

#Wfs: WIPO limits rights of the public to insist on patent quality

#AWW: Article 11: Patentable Inventions

#PaW: Patents shall be available for any invention, provided they are new,
involve an inventive step (are non-obvious) and are capable of
industrial application (are useful), in accordance with the
requirements prescribed in this Treaty.

#Tho: This is a repetition of %(tr:TRIPs Art 27.1), only without the
subphrase %(q:in all fields of technology) and thus even a little bit
broader.  It sets a minimum standard of what must be patentable, based
on abstract wordings open to interpretation from the traditional
viewpoint of different countries, especially US and EU.  The practise
of recent years has shown that this provision is used only to extend
the rights of the patent applicant and never those of the public.  The
concept of %(q:technology) has so far served at least in Europe as one
of the possible delimiting criteria.  Now WIPO is sending out the
signal that even this criterion is obsolete or should be removed. 
Apparently this corresponds to the opinion of leading European patent
law experts represented in WIPO, who have in recent years done
everything possible to %(kc:void the implicit requirement that there
be a %(q:technical invention) of its restrictive meaning).

#Wnl: We would propose as a replacement for the WIPO formula, the following
constitutional statement:

#PgW: Patents shall be available for inventions of any type, provided that
most of the interested social scientists, technology experts and
members of the general public of the world have, after adequate study,
largely agreed that granting such patents is beneficial to the
progress of science and technology and to the overall welfare of the
people of the world.

#Bop: Based on a current somewhat immature consensus, perhaps the following
more specific regulation could be discussed:

#Prl: Patents %(e:shall) be available for inventions whose disclosure
significantly contributes to the progress of the empirical science of
nature, i.e. teachings on how to use controllable physical forces to
directly cause a result that can not be logically deduced from prior
knowledge about physical causality.

#Pbw: Patents %(q:may) be granted for inventions that are new, non-obvious
and useful, as far as the monopoly rights to such ideas do not impede
basic human freedoms such as the freedom of individual activity
(independent from industrial organisations), the freedom of the spoken
and written word in any type of language, the freedom of determining
terms and forms of contract, as well as the freedom of international
trade.

#Iae: In general, there is no mature consensus on where the patent system is
beneficial and where not, and therefore it is inadmissible to hardcode
anything very specific beyond the initial constitutional statement
into international treaties.

#IWl: Industrial Applicability/Utility

#Aga: An invention shall be considered industrially applicable (useful) if,
according to its nature, it can [be made or used in any kind of
industry] [accomplish a practical application in the field of art], as
prescribed in the Regulations.

#Ngj: Not long ago %(q:industrial application) was generally understood to
mean something like %(q:an application in the automated production of
material goods).  While in the English language the word %(q:industry)
has acquired a very broad sense, including the %(q:software industry),
the %(q:sex industry) and the %(q:patent industry), in other languages
such as French it is still restricted to its traditional meaning, and
this has been upheld in court judgements where software was not deemed
to be patentable, because it did not constitute the inventive part of
an industrial production process (see the Schlumberger case).

#TWg: The above proposal apparently pleads in favor of the recent
anglo-saxon broad view of %(q:industry), which is equal to voiding
this traditional patentability limitation of all its meaning. 
Moreover the WIPO wording is circular, defining %(q:industrial) by
%(q:industry).  We again propose to replace this by two clear
definitions, one for the upper and one for the lower limit:

#Atg: An invention %(e:shall) be considered industrially applicable if it
contributes to progress in the art of producing material goods.

#AWb: An invention %(e:may) be considered industrially applicable if
reworking it typically implies substantial investment in equipment and
paid staff.

#Mur: Many of the proposed articles set upper limits on what individual
countries may demand from a patentee.  Thus any future improvement in
favor of more rights for the public is forestalled.

#Ast: Adequate Disclosure in Application as a Whole

#TfW: The disclosure of the invention in the application as a whole shall be
adequate, if, as of the dateof filing of the application, it sets
forth the invention in a manner sufficiently clear and complete for
the invention to be carried out by a person skilled in the art, as
prescribed in the Regulations.

#Ims: In respect of the disclosure, no requirement additional to or
different from those provided for in par (1) may be imposed.

#AWc: Article 4: Description

#TaW: The description part of the application shall have the contents, and
be presented in the order, prescribed in the Regulations.

#Iev: In respect of the description, no requirement additional or different
from those provided for in paragraph (1) may be imposed.

#Aqn: Article 5: Requirements Concerning Claims

#Tra: The claims shall define the matter forwhich protection is sought, as
prescribed in the Regulations.

#Thi: The claims, both individually and in their totality, shall be clear
and concise, as prescribed in the Regulations.

#TWa: The claims shall be supported by the descriptions and drawings, as
prescribed in the Regulations.

#Tre: Theclaims shall be presented as prescribed in the Regulations.

#Iif: In respect of the claims, no requirement additional to or different
from those provided for in paragraphs (1) to (4) may be imposed.

#Sbs: Some countries might want to require more modern forms of description
or disallow function claims and other abuses that have spread in the
patent system in recent years. These regulations tell them that they
may not restrict such abuses, but may have to condone new abuses that
evolve within the formal framework, as time passes.

#Isc: In principle, states should be free to impose additional or different
requirements on a patentee, if such requirements are justified not by
some complacency or protectionism on the part of the bureaucracy but
by the constitutional purpose of the patent system.  This could be
formulated in the treaty as

#Imn: In respect of ... no additional or different requirement may be
imposed unless there is a broad informed consensus among social
scientists, technology experts and the general public that such a
requirement serves to promote the progress of science and technology
and the welfare of the people of the world.  Any such requirement must
be decreed by the concerned state's highest legislative authority and
published as a formal law.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: wipo-splt01 ;
# txtlang: en ;
# multlin: t ;
# End: ;

