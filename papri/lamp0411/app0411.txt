
   [1]No Software Patents! [2]No Software Patents! 
     [3]textsize [English....]  set language
   [4]Start Page
   [sectionbg.gif]
   [5]Additional Explanation
   [sectionbg.gif]
   [6]Appeal to the EU Council
   [7]The Basics
   [8]The Dangers
   [9]The Untruths
   [10]The Politics
   [11]Press
   [12]Forum
   [13]How to Help
   [14]About The Campaign
   [15][btnhelp_en.gif] 

                                             [16]printer-friendly version

Appeal to the EU Council

                                     by
            Linus Torvalds, Michael Widenius and Rasmus Lerdorf
                              23 November 2004

   Later this week, on November 25th and 26th, the EU Competitiveness
   Council will convene and soon attempt to formally adopt a proposed
   "Directive on the Patentability of Computer-Implemented Inventions",
   commonly referred to as the "software patent directive". On May 18th,
   the Council reached political agreement on a draft legislation,
   however, did not take a formal decision to adopt it.

   We urge the governments of the EU member states, which are represented
   in the EU Council, to oppose the debateless adoption of the said
   proposal as a so-called "A item". In the interest of Europe, such a
   deceptive, dangerous and democratically illegitimate proposal must not
   become the Common Position of the member states.

   We ask all webmasters to help prevent the legalization of software
   patents in the EU by placing a link to the campaign website
   [17]www.NoSoftwarePatents.com.

                              ---   ---   ---

   The draft directive in question is deceptive because it leads laymen,
   and even those legal professionals who are not familiar with the
   intricacies of patent law, to falsely believe that it would exclude
   software from patentability. However, it is actually a compilation of
   the entirety of the excuses with which the patent system has, for many
   years, been circumventing article 52 of the European Patent Convention
   in order to grant patents on software ideas.

   Those who say that the directive would not allow patents on software
   attach a peculiar definition to the term "software" that is
   hair-splitting. The proper way to distinguish between software patents
   and patents on computer-controlled devices is to exclude the
   processing, handling and presentation of information from the
   definition of the word "technical" for the purposes of patent law, to
   disallow patents on innovations in the field of data processing, and
   to establish the hard and fast requirement that natural forces are
   used to control physical effects beyond the digital sphere.

   The legislation in question contains many provisions that appear to be
   helpful if one understands "technical" in a common-sense way. However,
   the patent system has previously expressed and demonstrated its own
   definition of that term, which is one that encompasses almost anything
   that a computer can possibly do. Moreover, article 5 (2) of the
   legislative proposal tears down all barriers to the patentability of
   software by expressly allowing so-called "program claims".

                              ---   ---   ---

   Software patents are dangerous to the economy at large, and
   particularly to the European economy. Lawmakers should heed the
   warnings of such reputable organizations as Deutsche Bank Research,
   the Kiel Institute for World Economics, and PricewaterhouseCoopers.

   At first sight, a patent appears to protect an inventor but the actual
   implications may be the opposite, dependent upon the field. Copyright
   serves software authors while patents potentially deprive them of
   their own independent creations. Copyright is fair because it is
   equally available to all. A software patent regime would establish the
   law of the strong, and ultimately create more injustice than justice.

   In particular, we believe that the economic opportunities of the new
   EU member states are endangered by software patents. The many talented
   software developers in those countries should be given a fair chance.
   The average cost of a European patent is in the range from 30,000 to
   50,000 Euros, and a company needs a very large number of such patents
   in order to be able to enter into "cross-licensing" agreements with
   multinationals that own tens of thousands of patents each.

   The political decision on the patentability of software should be
   based on merits, economic logic and ethical considerations, not on
   whatever may have been the practice of the patent system in recent
   years. Let us all look ahead, not back.

                              ---   ---   ---

   If the EU Council adopted the legislative proposal of May 18th, it
   would do so without democratic legitimacy. The idea of a debateless
   and voteless adoption of an "A item" is only to speed up and simplify
   the process if a qualified majority is in place. In this particular
   case, there isn't.

   As of November 1st, new voting weights apply in the EU under the Act
   of Accession. The collective number of votes of all countries that
   affirmatively supported the legislative proposal on May 18th amounts
   to 216, falling short of the required 232. It would set a more than
   regrettable precedent for European democracy if the EU Council adopted
   a Common Position on an insufficient basis.

   Furthermore, the 216 votes include those of the Netherlands and of
   Germany against the will of the national parliaments of those
   countries. On July 1st, a broad majority of the Tweede Kamer passed a
   resolution that the Dutch government withdraw its support for the
   legislative proposal in question. On October 21st, all four groups in
   the German Bundestag took a similar position and criticized the
   legislative proposal of May 18th as a legislation that would allow
   software patents.

                              ---   ---   ---

   For the sake of innovation and a competitive software market, we
   sincerely hope that the European Union will seize this opportunity to
   exclude software from patentability and gain a major competitive
   advantage in the information age.
                             Latest News
   Appeal to EU Council by Linus Torvalds, Michael Widenius, Rasmus
   Lerdorf
     [18]>> joint statement
     [19]>> press release
   Polish cabinet: Can't support proposal for EU software patent
   directive
     [20]>> press release
     [21]>> ZDNet commentary
     [22]>> It's 1999 again!
   Failure-prone European Patent Office [23]>>
   "Rich player's business" [24]>>
   12 game publishers sued [25]>>
   1 NOV 04: No more qualified majority for software patents in EU
   Council
     [26]>> press release
     [27]>> analysis
   SUN learning quickly [28]>>
   Translation of German Conservatives' motion [29]>>
   News Archive [30]>>
     _________________________________________________________________

     [31]Catal�    [32]Cesky    [33]Deutsch    [34]Eesti    [35]English
     [36]Espa�ol    [37]Fran�ais    [38]Italiano
     [39]Latviski    [40]Lietuviskai    [41]Magyar    [42]Nederlands    [
   43]Polski    [44]Portugu�s    [45]Suomi

Verweise

   1. file://localhost/
   2. file://localhost/
   3. LYNXIMGMAP:file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/lamp0411/app0411.html#ttt
   4. file://localhost/en/m/intro/index.html
   5. file://localhost/en/m/intro/explain.html
   6. file://localhost/en/m/intro/app0411.html
   7. file://localhost/en/m/basics/index.html
   8. file://localhost/en/m/dangers/index.html
   9. file://localhost/en/m/untruths/index.html
  10. file://localhost/en/m/politics/index.html
  11. file://localhost/en/m/press/index.html
  12. file://localhost/en/m/forum/index.html
  13. file://localhost/en/m/help/index.html
  14. file://localhost/en/m/about/index.html
  15. file://localhost/en/m/help/email.html
  16. file://localhost/en/m/intro/app0411.html?print=true
  17. http://www.nosoftwarepatents.com/
  18. file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/intro/app0411.html
  19. http://www.nosoftwarepatents.com/docs/041123(EN)PRac.pdf
  20. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=158
  21. http://comment.zdnet.co.uk/0,39020505,39174245,00.htm
  22. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=177
  23. http://nosoftwarepatents.com/phpBB2/viewtopic.php?p=727#727
  24. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=108
  25. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=104
  26. http://www.nosoftwarepatents.com/docs/(EN)041101PRqm.pdf
  27. http://www.nosoftwarepatents.com/docs/041101qm.pdf
  28. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=93
  29. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=62
  30. http://nosoftwarepatents.com/phpBB2/viewtopic.php?t=154
  31. file://localhost/ca/m/intro/app0411.html
  32. file://localhost/cz/m/intro/app0411.html
  33. file://localhost/de/m/intro/app0411.html
  34. file://localhost/ee/m/intro/app0411.html
  35. file://localhost/en/m/intro/app0411.html
  36. file://localhost/es/m/intro/app0411.html
  37. file://localhost/fr/m/intro/app0411.html
  38. file://localhost/it/m/intro/app0411.html
  39. file://localhost/lv/m/intro/app0411.html
  40. file://localhost/lt/m/intro/app0411.html
  41. file://localhost/hu/m/intro/app0411.html
  42. file://localhost/nl/m/intro/app0411.html
  43. file://localhost/pl/m/intro/app0411.html
  44. file://localhost/pt/m/intro/app0411.html
  45. file://localhost/fi/m/intro/app0411.html
