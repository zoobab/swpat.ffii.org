\begin{subdocument}{beresford00}{Keith Beresford 2000: Patenting Software Under the European Patent Convention}{http://swpat.ffii.org/papri/beresford00/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{This is a highly practical guide to registering software patents in Europe. The report explains how to formulate a software patent specification and how to formulate claims. It also provides examples of granted software patents, and European Patent Office and UK case law.}
\begin{description}
\item[title:]\ Keith Beresford 2000: Patenting Software Under the European Patent Convention
\item[source:]\ Sweet \& Maxwell London 2000
\end{description}

\begin{sect}{intro}{introduction}
This is a highly practical guide to registering software patents in Europe. The report explains how to formulate a software patent specification and how to formulate claims. It also provides examples of granted software patents, and European Patent Office and UK case law.

\begin{quote}
{\it Contents: The European Patent System and the definition of invention.  Technical features and technical effects in software. Claim formulation for maximum protection. Supporting the claims by the description. Examples of granted software patents: user interfaces; software for generating computer programs; the rejected document processing cases; business model patents and E-commerce postscript. Jurisdiction: UK and EU.}

{\it Abstract: It is commonly held that software, e-commerce and business models related inventions are inhenrently unpatentable in Europe. In reviewing the situation, this article emphasises the large number of inventions in this field which have in fact been patented through the EPO or through national patent offices in Europe. [..] He shows that, if proper attention is paid to both the substance and the form of claims and description, to direct the reader to the technical problem and its solution, effective protection is in fact very often available.}
\end{quote}
\end{sect}

\begin{sect}{bmex}{Business method examples}
Beresford gives the following examples:

\begin{quote}
{\it Examples: business management and financial systems:
\begin{itemize}
\item
EP407026\footnote{\url{../../pikta/txt/ep/0407/026}} (Reuters)

\item
EP209807\footnote{\url{../../pikta/txt/ep/0209/807}} (Sohei)

\item
EP701717\footnote{\url{../../pikta/txt/ep/0701/717}} (Shepherd)

\item
EP838063\footnote{\url{}} (Realkredit Danmark AS)
\end{itemize}}
\end{quote}
\end{sect}

\begin{sect}{tech}{On technical effect (2.29)}
\begin{quote}
{\it [Here] there has arisen one of the main misconceptions concerning the patentability of software implemented inventions under the European Patent Convention: this requirement is very far from being as restrictive as is frequently believed}

{\it ... thus for example ... software that takes up less space in the computer, data structures that take up less memory space, file arrangements which take up less memory space, software which will operate more quickly, improved control facilities for the human operator of the computer system, software which makes it possible to perform by computer something which could previously only be done manually or mentally, or software which is more reliable.}

{\it ... basis for patentability has been found to arise in new user interfaces which make the computer easier or more efficient to operate (Sohei), or which ``ease the mental burden'' on the operator (T 95/0333)}
\end{quote}
\end{sect}

\begin{sect}{invstep}{On Inventive Step}
Beresford approvingly cites an 1958 English patent case (not software) where the judge said

\begin{quote}
{\it It is well settled that the validity of a patent, challenged on the ground of inventiveness, may be established though the inventive step represent a very slight advance.}
\end{quote}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf K. Beresford, World Patent Information 23 (2001) 253-263: European patents for software, E-commerce and business model inventions.}}
\filbreak

\item
{\bf {\bf Book Info Beresford 2000\footnote{\url{http://www.smlawpub.co.uk/products/cat/mydetails.cfm?title=5414&detail=5414}}}}

\begin{quote}
The publisher praises the book.
\end{quote}
\filbreak

\item
{\bf {\bf Interpretation of art 52 of the European Patent Convention in view of the question, to what extent software is patentable\footnote{\url{http://localhost/swpat/stidi/epc52/exeg/index.en.html}}}}

\begin{quote}
Dr. Karl Friedrich Lenz, professor for German and European Law at Aoyama Gakuin University in Tokyo, investigates using the various universally accepted methods of law interpretation which meaning has to be attributed to the text of art 52 EPC today and reaches the conclusion that the Technical Boards of Appeal of the European Patent Office have for some time now regularly granted patents on programs for computers as such and are showing a disturbing willingness to substitute their own value judgements for those given by the legislator.
\end{quote}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{\url{http://localhost/swpat/gasnu/uk/index.en.html}}}}

\begin{quote}
The United Kingdom's patent matters are run nearly exclusively by the UK Patent Office which has been a relentless promoter of software patentability in Europe since the 1970s.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.en.html}}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

