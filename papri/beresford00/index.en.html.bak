<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/girzu/index.en.html" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/01/06">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Software Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="Keith Beresford 2000: Patenting Software Under the European Patent Convention">
<meta name="description" content="This is a highly practical guide to registering software patents in Europe. The report explains how to formulate a software patent specification and how to formulate claims. It also provides examples of granted software patents, and European Patent Office and UK case law.">
<title>Beresford 2000</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>Keith Beresford 2000: Patenting Software Under the European Patent Convention<br>
<!--#include virtual="../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
<div id="papri">
<dl><dt>title:</dt>
<dd>Keith Beresford 2000: Patenting Software Under the European Patent Convention</dd>
<dt>source:</dt>
<dd>Sweet &amp; Maxwell London 2000</dd></dl>
</div>
<div class="sectmenu">
<ol type="1"><li><a href="#intro">Introduction</a></li>
<li><a href="#bmex">Business method examples</a></li>
<li><a href="#tech">On technical effect (2.29)</a></li>
<li><a href="#invstep">On Inventive Step</a></li>
<li><a href="#links">Annotated Links</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h2>1. <a name="intro">introduction</a></h2>
</div>

<div class="sectbody">
This is a highly practical guide to registering software patents in Europe. The report explains how to formulate a software patent specification and how to formulate claims. It also provides examples of granted software patents, and European Patent Office and UK case law.<p><div class=cit>
Contents: The European Patent System and the definition of invention.  Technical features and technical effects in software. Claim formulation for maximum protection. Supporting the claims by the description. Examples of granted software patents: user interfaces; software for generating computer programs; the rejected document processing cases; business model patents and E-commerce postscript. Jurisdiction: UK and EU.<p>Abstract: It is commonly held that software, e-commerce and business models related inventions are inhenrently unpatentable in Europe. In reviewing the situation, this article emphasises the large number of inventions in this field which have in fact been patented through the EPO or through national patent offices in Europe. [..] He shows that, if proper attention is paid to both the substance and the form of claims and description, to direct the reader to the technical problem and its solution, effective protection is in fact very often available.
</div>
</div>

<div class="secthead">
<h2>2. <a name="bmex">Business method examples</a></h2>
</div>

<div class="sectbody">
Beresford gives the following examples:<p><div class=cit>
Examples: business management and financial systems:
<ul><li><a href="../../pikta/txt/ep/0407/026">EP0407026</a> (Reuters)</li>
<li><a href="../../pikta/txt/ep/0209/807">EP0209807</a> (Sohei)</li>
<li><a href="../../pikta/txt/ep/0701/717">EP701717</a> (Shepherd)</li>
<li><a href="http://kwiki.ffii.org/Ep0838062En">EP838063</a> (Realkredit Danmark AS)</li></ul>
</div>
</div>

<div class="secthead">
<h2>3. <a name="tech">On technical effect (2.29)</a></h2>
</div>

<div class="sectbody">
<div class=cit>
[Here] there has arisen one of the main misconceptions concerning the patentability of software implemented inventions under the European Patent Convention: this requirement is very far from being as restrictive as is frequently believed<p>... thus for example ... software that takes up less space in the computer, data structures that take up less memory space, file arrangements which take up less memory space, software which will operate more quickly, improved control facilities for the human operator of the computer system, software which makes it possible to perform by computer something which could previously only be done manually or mentally, or software which is more reliable.<p>... basis for patentability has been found to arise in new user interfaces which make the computer easier or more efficient to operate (Sohei), or which &quot;ease the mental burden&quot; on the operator (T 95/0333)
</div>
</div>

<div class="secthead">
<h2>4. <a name="invstep">On Inventive Step</a></h2>
</div>

<div class="sectbody">
Beresford approvingly cites an 1958 English patent case (not software) where the judge said<p><div class=cit>
It is well settled that the validity of a patent, challenged on the ground of inventiveness, may be established though the inventive step represent a very slight advance.
</div>
</div>

<div class="secthead">
<h2>5. <a name="links">Annotated Links</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;">K. Beresford, World Patent Information 23 (2001) 253-263: European patents for software, E-commerce and business model inventions.</b></dt>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="http://www.smlawpub.co.uk/products/cat/mydetails.cfm?title=5414&amp;detail=5414">Book Info Beresford 2000</a></b></dt>
<dd>The publisher praises the book.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/stidi/epc52/exeg/index.en.html">Interpretation of art 52 of the European Patent Convention in view of the question, to what extent software is patentable</a></b></dt>
<dd>Dr. Karl Friedrich Lenz, professor for German and European Law at Aoyama Gakuin University in Tokyo, investigates using the various universally accepted methods of law interpretation which meaning has to be attributed to the text of art 52 EPC today and reaches the conclusion that the Technical Boards of Appeal of the European Patent Office have for some time now regularly granted patents on programs for computers as such and are showing a disturbing willingness to substitute their own value judgements for those given by the legislator.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/gasnu/uk/index.en.html">The UK Patent Family and Software Patents</a></b></dt>
<dd>The United Kingdom's patent matters are run nearly exclusively by the UK Patent Office which has been a relentless promoter of software patentability in Europe since the 1970s.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/stidi/korcu/index.en.html">Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs</a></b></dt>
<dd>So far computer programs and other <em>rules of organisation and calculation</em> are not <em>patentable inventions</em> according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/papri/beresford00/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/01/06 (2004/08/24)
<a href="/girzu/index.en.html">Workgroup</a><br>
english version 2004/08/16 by <a href="http://www.a2e.de">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el -->
<!-- mode: html -->
<!-- End: -->

