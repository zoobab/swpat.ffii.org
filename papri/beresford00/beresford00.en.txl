<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Keith Beresford 2000: Patenting Software Under the European Patent
Convention

#descr: This is a highly practical guide to registering software patents in
Europe. The report explains how to formulate a software patent
specification and how to formulate claims. It also provides examples
of granted software patents, and European Patent Office and UK case
law.

#itm: Business method examples

#tlW: On technical effect (2.29)

#WnW: On Inventive Step

#nrc: Contents: The European Patent System and the definition of invention. 
Technical features and technical effects in software. Claim
formulation for maximum protection. Supporting the claims by the
description. Examples of granted software patents: user interfaces;
software for generating computer programs; the rejected document
processing cases; business model patents and E-commerce postscript.
Jurisdiction: UK and EU.

#adt: Abstract: It is commonly held that software, e-commerce and business
models related inventions are inhenrently unpatentable in Europe. In
reviewing the situation, this article emphasises the large number of
inventions in this field which have in fact been patented through the
EPO or through national patent offices in Europe. [..] He shows that,
if proper attention is paid to both the substance and the form of
claims and description, to direct the reader to the technical problem
and its solution, effective protection is in fact very often
available.

#iox: Beresford gives the following examples:

#sea: Examples: business management and financial systems:

#lWk: Realkredit Danmark AS

#rWe: Beresford approvingly cites an 1958 English patent case (not software)
where the judge said

#tvp: It is well settled that the validity of a patent, challenged on the
ground of inventiveness, may be established though the inventive step
represent a very slight advance.

#keW: Book Info Beresford 2000

#ppe: The publisher praises the book.

#dob: K. Beresford, World Patent Information 23 (2001) 253-263: European
patents for software, E-commerce and business model inventions.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: beresford00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

