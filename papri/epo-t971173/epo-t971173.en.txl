<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO T 1173/97 (1998): IBM Computer Program Product

#descr: According to this pathbreaking decision of the European Patent
Office's Technical Board of Appeal, direct claims to computer programs
are permissible under Art 52 of the European Patent Convention.  In
the words of the Board: %(q:A computer program product is not excluded
from patentability under Article 52%(pe:2) and %(pe:3) EPC if, when it
is run on a computer, it produces a further technical effect which
goes beyond the %(q:normal) physical interactions between
%(tp|program|software) and %(tp|computer|hardware).)

#mdat: Header Data

#edo: Headnote

#dWs: A computer program product is not excluded from patentability under
Article 52(2) and (3) EPC if, when it is run on a computer, it
produces a further technical effect which goes beyond the %(q:normal)
physical interactions between program (software) and computer
(hardware).

#aWm: Case Number

#pln: Appellant

#pet: Representative

#ish: Teufel, Fritz, Dipl.-Phys. IBM Deutschland Informationssysteme GmbH
Patentwesen und Urheberrecht 70548 Stuttgart (DE)

#inp: Decision under appeal

#g1l: Decision of the Examining Division of the European Patent Office
posted 28 July 1997 refusing European patent application No. 91 107
112.4 pursuant to Article 97(1) EPC.

#pWB: Composition of the Board

#MnW: Chairman: Members: P. K. J. van den Berg V. Di Cerbo R. R. K.
Zimmermann

#sumfs: Summary of Facts and Submissions

#oli: Conclusion

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epo-t971173.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epo-t971173 ;
# txtlang: xx ;
# End: ;

