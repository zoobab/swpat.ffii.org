\select@language {french}
\contentsline {section}{\numberline {1}Header Data}{1}{section.1}
\contentsline {section}{\numberline {2}Summary of Facts and Submissions}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}I.}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}II.}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}III.}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}IV.}{3}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}V.}{3}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}VI.}{4}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}VII.}{4}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}VIII.}{5}{subsection.2.8}
\contentsline {section}{\numberline {3}Reasons for the Decision}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}2. TRIPS}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}2.1}{6}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}2.2}{6}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}2.3}{6}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}2.4}{7}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}2.5}{7}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}2.6}{7}{subsubsection.3.2.6}
\contentsline {subsection}{\numberline {3.3}3. The relevant source of substantive patent law}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}4. Exclusion under Article 52(2) and (3) EPC}{8}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}4.1}{8}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}4.2}{8}{subsubsection.3.4.2}
\contentsline {subsection}{\numberline {3.5}5. Interpretation of ``as such''}{9}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}5.1}{9}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}5.2}{9}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}5.3}{9}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}5.4}{9}{subsubsection.3.5.4}
\contentsline {subsubsection}{\numberline {3.5.5}5.5}{9}{subsubsection.3.5.5}
\contentsline {subsection}{\numberline {3.6}6. Technical character of programs for computers}{9}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}6.1}{9}{subsubsection.3.6.1}
\contentsline {subsubsection}{\numberline {3.6.2}6.2}{10}{subsubsection.3.6.2}
\contentsline {subsubsection}{\numberline {3.6.3}6.3}{10}{subsubsection.3.6.3}
\contentsline {subsubsection}{\numberline {3.6.4}6.4}{10}{subsubsection.3.6.4}
\contentsline {subsubsection}{\numberline {3.6.5}6.5}{10}{subsubsection.3.6.5}
\contentsline {subsubsection}{\numberline {3.6.6}6.6}{10}{subsubsection.3.6.6}
\contentsline {subsection}{\numberline {3.7}7. Case law of the boards of appeal of the EPO}{11}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}7.1}{11}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}7.2}{11}{subsubsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.3}7.3}{11}{subsubsection.3.7.3}
\contentsline {subsubsection}{\numberline {3.7.4}7.4}{12}{subsubsection.3.7.4}
\contentsline {subsection}{\numberline {3.8}8.}{12}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}9. Claim for a computer program product}{12}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}9.1}{12}{subsubsection.3.9.1}
\contentsline {subsubsection}{\numberline {3.9.2}9.2}{12}{subsubsection.3.9.2}
\contentsline {subsubsection}{\numberline {3.9.3}9.3}{12}{subsubsection.3.9.3}
\contentsline {subsubsection}{\numberline {3.9.4}9.4}{13}{subsubsection.3.9.4}
\contentsline {subsubsection}{\numberline {3.9.5}9.5}{13}{subsubsection.3.9.5}
\contentsline {subsubsection}{\numberline {3.9.6}9.6}{13}{subsubsection.3.9.6}
\contentsline {subsubsection}{\numberline {3.9.7}9.7}{14}{subsubsection.3.9.7}
\contentsline {subsubsection}{\numberline {3.9.8}9.8}{14}{subsubsection.3.9.8}
\contentsline {subsection}{\numberline {3.10}10. Interpretation according to the Vienna Convention}{14}{subsection.3.10}
\contentsline {subsubsection}{\numberline {3.10.1}10.1}{14}{subsubsection.3.10.1}
\contentsline {subsubsection}{\numberline {3.10.2}10.2}{15}{subsubsection.3.10.2}
\contentsline {subsection}{\numberline {3.11}11. Further case law of the boards}{15}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}11.1}{15}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}11.2}{15}{subsubsection.3.11.2}
\contentsline {subsubsection}{\numberline {3.11.3}11.3}{15}{subsubsection.3.11.3}
\contentsline {subsubsection}{\numberline {3.11.4}11.4}{16}{subsubsection.3.11.4}
\contentsline {paragraph}{11.4.1}{16}{section*.3}
\contentsline {paragraph}{11.4.2 }{16}{section*.4}
\contentsline {paragraph}{11.4.3}{16}{section*.5}
\contentsline {paragraph}{11.4.4}{16}{section*.6}
\contentsline {subsubsection}{\numberline {3.11.5}11.5}{16}{subsubsection.3.11.5}
\contentsline {subsection}{\numberline {3.12}12. The exact wording of the claims}{17}{subsection.3.12}
\contentsline {subsubsection}{\numberline {3.12.1}12.1}{17}{subsubsection.3.12.1}
\contentsline {subsubsection}{\numberline {3.12.2}12.2}{17}{subsubsection.3.12.2}
\contentsline {subsubsection}{\numberline {3.12.3}12.3}{18}{subsubsection.3.12.3}
\contentsline {subsection}{\numberline {3.13}13. [ Conclusion ]}{18}{subsection.3.13}
