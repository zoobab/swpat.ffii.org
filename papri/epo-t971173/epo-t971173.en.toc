\contentsline {section}{\numberline {1}Header Data}{3}{section.1}
\contentsline {section}{\numberline {2}Summary of Facts and Submissions}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}I.}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}II.}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}III.}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}IV.}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}V.}{5}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}VI.}{6}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}VII.}{6}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}VIII.}{6}{subsection.2.8}
\contentsline {section}{\numberline {3}Reasons for the Decision}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}2. TRIPS}{8}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}2.1}{8}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}2.2}{8}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}2.3}{8}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}2.4}{9}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}2.5}{9}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}2.6}{9}{subsubsection.3.2.6}
\contentsline {subsection}{\numberline {3.3}3. The relevant source of substantive patent law}{9}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}4. Exclusion under Article 52(2) and (3) EPC}{10}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}4.1}{10}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}4.2}{10}{subsubsection.3.4.2}
\contentsline {subsection}{\numberline {3.5}5. Interpretation of ``as such''}{10}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}5.1}{10}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}5.2}{10}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}5.3}{11}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}5.4}{11}{subsubsection.3.5.4}
\contentsline {subsubsection}{\numberline {3.5.5}5.5}{11}{subsubsection.3.5.5}
\contentsline {subsection}{\numberline {3.6}6. Technical character of programs for computers}{11}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}6.1}{11}{subsubsection.3.6.1}
\contentsline {subsubsection}{\numberline {3.6.2}6.2}{11}{subsubsection.3.6.2}
\contentsline {subsubsection}{\numberline {3.6.3}6.3}{11}{subsubsection.3.6.3}
\contentsline {subsubsection}{\numberline {3.6.4}6.4}{12}{subsubsection.3.6.4}
\contentsline {subsubsection}{\numberline {3.6.5}6.5}{12}{subsubsection.3.6.5}
\contentsline {subsubsection}{\numberline {3.6.6}6.6}{12}{subsubsection.3.6.6}
\contentsline {subsection}{\numberline {3.7}7. Case law of the boards of appeal of the EPO}{12}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}7.1}{12}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}7.2}{13}{subsubsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.3}7.3}{13}{subsubsection.3.7.3}
\contentsline {subsubsection}{\numberline {3.7.4}7.4}{13}{subsubsection.3.7.4}
\contentsline {subsection}{\numberline {3.8}8.}{14}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}9. Claim for a computer program product}{14}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}9.1}{14}{subsubsection.3.9.1}
\contentsline {subsubsection}{\numberline {3.9.2}9.2}{14}{subsubsection.3.9.2}
\contentsline {subsubsection}{\numberline {3.9.3}9.3}{14}{subsubsection.3.9.3}
\contentsline {subsubsection}{\numberline {3.9.4}9.4}{14}{subsubsection.3.9.4}
\contentsline {subsubsection}{\numberline {3.9.5}9.5}{15}{subsubsection.3.9.5}
\contentsline {subsubsection}{\numberline {3.9.6}9.6}{15}{subsubsection.3.9.6}
\contentsline {subsubsection}{\numberline {3.9.7}9.7}{15}{subsubsection.3.9.7}
\contentsline {subsubsection}{\numberline {3.9.8}9.8}{16}{subsubsection.3.9.8}
\contentsline {subsection}{\numberline {3.10}10. Interpretation according to the Vienna Convention}{16}{subsection.3.10}
\contentsline {subsubsection}{\numberline {3.10.1}10.1}{16}{subsubsection.3.10.1}
\contentsline {subsubsection}{\numberline {3.10.2}10.2}{16}{subsubsection.3.10.2}
\contentsline {subsection}{\numberline {3.11}11. Further case law of the boards}{17}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}11.1}{17}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}11.2}{17}{subsubsection.3.11.2}
\contentsline {subsubsection}{\numberline {3.11.3}11.3}{17}{subsubsection.3.11.3}
\contentsline {subsubsection}{\numberline {3.11.4}11.4}{17}{subsubsection.3.11.4}
\contentsline {paragraph}{11.4.1}{17}{section*.3}
\contentsline {paragraph}{11.4.2 }{18}{section*.4}
\contentsline {paragraph}{11.4.3}{18}{section*.5}
\contentsline {paragraph}{11.4.4}{18}{section*.6}
\contentsline {subsubsection}{\numberline {3.11.5}11.5}{18}{subsubsection.3.11.5}
\contentsline {subsection}{\numberline {3.12}12. The exact wording of the claims}{18}{subsection.3.12}
\contentsline {subsubsection}{\numberline {3.12.1}12.1}{18}{subsubsection.3.12.1}
\contentsline {subsubsection}{\numberline {3.12.2}12.2}{19}{subsubsection.3.12.2}
\contentsline {subsubsection}{\numberline {3.12.3}12.3}{19}{subsubsection.3.12.3}
\contentsline {subsection}{\numberline {3.13}13. [ Conclusion ]}{19}{subsection.3.13}
\contentsline {section}{\numberline {4}ORDER}{20}{section.4}
\contentsline {section}{\numberline {5}Annotated Links}{20}{section.5}
