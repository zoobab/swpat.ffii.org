1 2

Deutscher Bundestag
15. Wahlperiode

Drucksache 15/xxx

3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33

Antrag
der Abgeordneten Dr. Uwe K�ster, Dirk Manzewski, J�rg Tauss, Ulrich Kelber, Dr. Axel Berg, Dr. Herta D�ubler-Gmelin, ..., Franz M�ntefering und der Fraktion der SPD sowie der Abgeordneten Grietje Bettin, Jerzy Montag, ..., Katrin G�ring-Eckardt, Krista Sager und der Fraktion B�NDNIS 90/DIE GR�NEN

Wettbewerb und Innovationsdynamik im Softwarebereich sichern � Patentierung von Computerprogrammen effektiv begrenzen
Der Bundestag wolle beschlie�en: I. Der Deutsche Bundestag stellt fest: In einer globalen Wissens- und Informationsgesellschaft und einer zunehmend wissensbasierten Weltwirtschaft gewinnen informationstechnische L�sungen zunehmend an Bedeutung. Die Rahmenbedingungen f�r die Entwicklung leistungsf�higer, kosteng�nstiger, verl�sslicher und nicht zuletzt sicherer Computerprogramme oder Software werden zu einem kritischen Faktor des deutschen Innovationssystems. Getragen wird die dynamische Entwicklung der deutschen wie europ�ischen Softwarebranche insbesondere auch durch kleine und mittlere Unternehmen. Es gilt dar�ber hinaus, den bestehenden Wettbewerbsvorteil europ�ischer und insbesondere auch deutscher Softwareentwickler und KMU im zukunftstr�chtigen Bereich der Open Source-Entwicklung gegen�ber etwa der amerikanischen Konkurrenz zu verteidigen und diesen Standortvorteil auch wirtschaftlich zu verwerten. Die EU-Kommission hat am 20. Februar 2002 ihren Vorschlag f�r die Richtlinie des Europ�ischen Parlamentes und des Rates �ber die Patentierbarkeit computerimplementierter Erfindungen vorgelegt (KOM(2002)92 endg�ltig). Das Europ�ische Parlament hat am 24. September wesentliche �nderungen beschlossen, am 18. Mai 2004 hat der Rat der Europ�ischen Union sich auf einen gemeinsamen Standpunkt einigen k�nnen (Ratsdokument Nr. 9713/04). Der Deutsche Bundestag begr��t grunds�tzlich die Initiative zur europ�ischen Vereinheitlichung der Patentierungspraxis in diesem Bereich. Er bekr�ftigt seine �berzeugung, dass der hinreichende Schutz des geistigen Eigentums unverzichtbar ist zum Erhalt und zur Entwicklung der kreativen gesellschaftlichen Potenziale im Interesse der Kreativen,

-1-


1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38

der Verbraucherinnen und Verbraucher wie der Kultur, Wirtschaft und Gesellschaft insgesamt. Zudem h�ngt die Innovationsdynamik in vielen Wirtschaftsbereichen � zunehmend auch in klassischen Wirtschaftsbereichen wie beispielsweise Maschinenbau-, Automobil- sowie Elektroindustrie � in wachsendem Ma�e von der steigenden Leistungsf�higkeit und erfolgreichen Integration von informationstechnischen Komponenten ab. Der Deutsche Bundestag teilt die �berzeugung, dass technische Erfindungen auch dann, wenn sie Softwarekomponenten enthalten, dem Schutz des Patentrechts zug�nglich sein m�ssen. Gleichwohl ist der Deutsche Bundestag zu der Auffassung gelangt, dass der gegenw�rtige Diskussionsstand zum Richtlinienentwurf bisher f�r zentrale Fragen keine hinreichenden L�sungen aufweist. Die Richtlinie zu computerimplementierten Erfindungen wird nur dann positive wirtschaftliche, rechtliche wie technische Effekte haben k�nnen, wenn sie zu mehr Rechtssicherheit und zur hinreichend hohen und eindeutigen Voraussetzungen einer Patentierbarkeit von Computerprogrammen im Zusammenhang mit technischen Erfindungen f�hrt. Dazu geh�rt die Festlegung eindeutiger Kriterien zur Unterscheidung von patentf�higen und nicht-patentf�higen computergest�tzten L�sungen, der effektive Ausschluss innovationsirrelevanter Patente (so genannter Trivialpatente) sowie grunds�tzlich die Begrenzung zul�ssiger patentrechtlicher Anspr�che auf Verfahrens- und Erzeugnisanspr�che. Ausufernde Patentanspr�che oder Trivialpatente bergen die Gefahr, kurzfristig Monopolisierungstendenzen zu bef�rdern sowie mittelfristig die gesellschaftlichen Akzeptanz des Patentsystems als effektives Innovations- und Fortschrittsinstrument auszuh�hlen. Hier ist eine unabh�ngige Evaluierung der umstrittenen j�ngeren Patentierungspraxis des Europ�ischen Patentamtes sicherzustellen. Hinsichtlich der wirtschaftlichen Effekte gehen zahlreiche Fachver�ffentlichungen und Studien davon aus, dass bei einer zu weit gehende Patentierbarkeit von Computerprogrammen bzw. einer nicht hinreichend abgesicherten Erfindungsh�he von einer erheblich verringerten Innovationsdynamik, deutlich erh�hten Rechtsunsicherheiten und einer Verschlechterung der Bedingungen f�r Open-Source-Konzepte ausgegangen werden muss. Insbesondere kleine und mittlere Unternehmen erwarten von einer weitgehenden Patentierbarkeit von Software kaum positive �konomische Effekte, aber durchaus aufgrund u.a. der Patentrecherche- und Patentdurchsetzungskosten einen hohen personellen, juristischen und finanziellen Aufwand sowie erhebliche wirtschaftliche und rechtliche Risiken. Der Deutsche Bundestag begr��t daher sowohl die mittelstandorientierten Aufkl�rungsinitiativen der Bundesregierung als auch die Verbesserungen der speziellen Informationsangebote der Patent�mter hinsichtlich der Chancen, Verfahren und auch Grenzen des Patentschutzes f�r KMU. Einen hinreichenden Ausgleich f�r die bestehenden Rechtsunsicherheiten k�nnen sie f�r die Softwarebranche aber nicht leisten. Zudem sind die steigenden Anforderungen an Interoperabilit�t, Verf�gbarkeit, Stabilit�t sowie nichtdiskrimi-

-2-


1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33

nierenden Zugang zu offenen technischen Infrastrukturen st�rker zu ber�cksichtigen � hier spielen die Durchsetzung offener Standards und der Einsatz von Open SourceSoftware eine wichtige Rolle. Die urheberrechtlichen Privilegien hinsichtlich der Dekompilierung und Interoperabilit�t gen�gen den zentralen Anforderungen interoperabler Systeme und offener Plattformen allein nicht und sind durch eine patentrechtliche Vorschrift zu erg�nzen. Insgesamt spricht hinsichtlich Computerprogrammen vieles weiterhin f�r den Grundsatz, diese prim�r urheberrechtlich zu sch�tzen und eine zu weitgehende Patentierbarkeit auszuschlie�en. Computerimplementierte technische Erfindungen sind daher so eng wie m�glich auszulegen. Der Deutsche Bundestag begr��t daher die Zielrichtung der Beschl�sse des Europ�ischen Parlaments. Er begr��t ausdr�cklich die j�ngste Initiative der Bundesregierung zu einem ,,Runden Tisch", um im Dialog mit Beteiligten und Betroffenen m�gliche Kompromisswege auszuloten. II. Der Deutsche Bundestag fordert die Bundesregierung auf: 1. bei kommenden Debatten und Ma�nahmen zur Reform des Schutzes geistigen Eigentums bei Computerprogrammen sowie im informationstechnischen Bereich verst�rkt standort-, wettbewerbs- und innovationspolitische Aspekte sowie die besonderen Entwicklungsbedingungen und spezifischen Merkmale von Computerprogrammen zu ber�cksichtigen; 2. den begonnenen Dialog mit kleinen und mittleren Softwareunternehmen, der Open Source-Gemeinde sowie mit anderen zivilgesellschaftlichen Vertretern fortzusetzen und zu intensivieren; 3. ihre Bem�hungen zur verbesserten Information insbesondere kleiner und mittlerer Unternehmen �ber die Chancen einer aktiven Patentpolitik weiter zu f�hren. Hinsichtlich der weiteren Beratung des Richtlinienentwurfs auf europ�ischer Ebene fordert der Deutsche Bundestag die Bundesregierung auf: 4. sich auf europ�ischer Ebene daf�r einzusetzen, eine unabh�ngige Evaluierung der Entscheidungspraxis der Patent�mter, insbesondere des EPA, durchzuf�hren. Dies kann beispielsweise ein integraler Bestandteil des vorgesehenen Berichtes �ber die Auswirkungen der Richtlinie sein; 5. bei der weiteren Kompromisssuche die Zielrichtung der Beschl�sse des Europ�ischen Parlaments vom 24. September 2003 und auch die zuk�nftigen Ergebnisse des Runden Tisches beim BMJ st�rker zu ber�cksichtigen;

-3-


1 2 3 4 5 6 7 8 9 10 11 12 13 14

6. auf eine restriktive, eindeutige und praktikable Begrenzung patentf�higer computerimplementierter Erfindungen sowie einen effektiven Ausschluss von Trivialpatenten hinzuwirken, beispielsweise �ber einen tragf�higen Technikbegriff;

7.

sicherzustellen, dass die Patentierbarkeit von Algorithmen und Gesch�ftsmethoden ausgeschlossen bleibt;

8.

sich daf�r einzusetzen, dass der Umfang der zul�ssigen patentrechtlichen Anspr�che auf Erzeugnis- und Verfahrensanspr�che begrenzt wird und selbst�ndige Programmanspr�che ausgeschlossen werden;

9.

sich entschieden daf�r einzusetzen, dass alternative Entwicklungskonzepte wie insbesondere Open Source-Projekte nicht beeintr�chtigt werden;

10. darauf hinzuwirken, dass ein m�glichst umfassendes patentrechtliches Interoperabilit�tsprivileg als Vorschrift aufgenommen wird.

Berlin, den xxx

-4-


