<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EICTA 2004/02/16: Praise for Irish Presidency's Policy on Software Patents

#descr: The European Information and Telecommunication Industry Association EICTA comments on a working document 5507/04 of the EU Council's Working Party on Intellectual Property (Patents), a text which proposes unlimited patentability and patent enforcability in the field of software and discards all limiting amendments for which the European Parlament had voted on 2003/09/24, alongside with limiting provisions from the European Commission, thereby following the demands of EICTA.  As might be expected, EICTA commends the Irish Presidency for this proposal and recommends a few minor changes which would bring the paper still closer in line with the policy of the European Patent Office and close a few loopholes (from EICTA's perspective) through which the validity of some software patent claims could possibly be questioned.  As always, the EICTA position is dominated by the patent lawyers of large corporations who, through their influence in the Standing Advisory Committee of the European Patent Office, have shaped the policies of that office.  As always, EICTA consistently promotes Newspeak (euphemistic wordings conducive to obscuring the discussion, such as %(q:CII), %(q:program product claims)) an makes wild unsubstantiated claims about the interests of the industry.  Here FFII republishes the EICTA paper and inserts some comments.

#s6r: Brussels, 16 February 2004

#eoW: Proposal for a Directive on the patentability of computerimplemented inventions EICTA Response on the Irish Presidency Compromise text 5570/04 of 29 January 2004

#Wmr: EICTA  commends   the   Irish   Presidency   in   proposing   a   compromise   text   which   incorporates   a significant number (24) of the amendments adopted by the European Parliament at the plenary vote on 24 September 2004, while avoiding the provisions which would be most harmful to innovative organisations including large companies and SMEs, as well as individual innovators, in many different industrial sectors.  Such companies are increasingly reliant on softwarerelated innovation in a broad range of products including mobile phones, medical instruments, cars, and domestic appliances etc.

#Wrn: There is nothing in the European Parliament's amendments that touches patentability of %(q:mobile phones, medical instruments, cars and domestic apliances), regardless of whether the concerned processes run under program control or not.  Moreover limitation of patentability, as proposed by the Parliament, has been shown to be beneficial for innovation and has received support from many innovative organisations.  EICTA's claims to the contrary can not rest on any economic evidence.

#WWW: EICTA believes that the Irish Presidency's compromise proposal, subject to an important improvement in Article 5 discussed below, represents an acceptable basis on which to build the Council's Common Position, although in many respects the newly introduced amendments make it less clear than the earlier %(dk:%(q:Common Approach) text endorsed by the Council in November 2002).

#let: The Annex includes a more detailed analysis of the individual amendments proposed by the Irish Presidency. As mentioned, however, there is one important respect in which the Irish Presidency's  compromise proposal leaves room for improvement, namely in Article 5 regarding %(q:program product claims).   The current wording of Article 5(2) would change the law as it stands and would place unnecessary constraints on how patent claims are drafted which would be unique in the field of Computer implemented Inventions (CIIs).  These constraints would complicate and increase the cost of drafting patents particularly for small and medium size enterprises and individual inventors, and would render enforcement more difficult and uncertain.

#Wmt: Ideally, the Council should adopt language for Article 5(2) similar to Amendment 18 in the McCarthy Report from the Legal Affairs Committee of the European Parliament, which was unfortunately not adopted by Parliament, as follows: %(bc:Article 5.2. A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.) A Council Common Position based on the based on the Common Approach or, failing that, on the Irish Presidency compromise proposal, but incorporating the above language on program product claims would best reflect existing practice in Europe.   EICTA is very conscious that the current legal framework for CIIs in Europe is serving all stakeholders well, including not only large companies, but also SMEs, and individual inventors. And, the Open Source community has certainly flourished in this environment.   EICTA does not want to see the delicate balance reflected in the status quo disturbed, i.e. the scope of what is currently patentable should neither be increased nor reduced.  EICTA believes that it would be better to have no directive at all than a directive which would damage European industry.

#nWe: o Recital 7b.   This implies that the current legal climate may not be conducive to investment and innovation in the field of software. Furthermore it is not clear what is meant by different interpretations of the provisions of the EPC. Strictly speaking, it is only open for the EPO to interpret its provisions.  The Patent Offices and courts in the Member States do not interpret the EPC as such, but their own respective national versions of it.

#hat: It can be doubted whether the massive granting of broad and trivial patents on rules of organisation and calculation by the EPO on a legally insecure basis is helpful in fostering investment and innovation in the field of software.

#WWm: Recital 13a.   This introduces a new term %(q:data processing method),  but without any definition of what that term means. It would be clearer to revert to the original Common Approach text by deleting this term.

#Rmh: Many terms in the proposal are undefined.  Rather than demand a definition of commonly used terminology such as %(q:data processing method), it might be better to demand a definition of %(q:technical).  The Council has, following the advice of the circles who dominate EICTA's patent policy, deleted all such definitions.

#umc: Recital  13b.    To  the   extent  that  this  means  that  you  cannot  save  an   otherwise unpatentable invention by reciting a technical means, EICTA agrees. However, the language seems to point away from the practice established in current jurisprudence of analysing patentability by looking at the claim as whole.  EICTA cannot support any approach to assessing patentability based on dividing claims into novel and known features.

#ius: Patentability is in general assessed by dividing claims into novel and known parts.  Without such a division, a new artistic pattern on a lampshade would be patentable, because the lampshade is technical.  This is common sense in all fields of technology.  The %(q:holistic) approach creates a special regime for software, designed solely to circumvent Art 52 EPC.

#cWh: o Recital 13c. However the word %(q:foreseen) should be replaced by %(q:claimed).

#1bW: Recital 13d.  This seems to be inconsistent with Article 5.2.

#rle: Recital 14. But the word %(q:currently) should be inserted before %(q:unpatentable) in the last sentence.  EICTA would strongly prefer %(q:trivial) to be replaced with %(q:non technical) in line with COM, E, S, P, IRL.

#ael: As EPO practise has shown, most software patents are trivial, and only to the extent that they are mathematical (i.e. non-technical) can they be non-trivial.  It is understandable that EICTA does not want the legislator to state concern about trivial patents.

#6sc: Recital 16.  The logic and purpose of the second sentence is unclear.

#een: Recital 17. The original wording conveyed more legal certainty, i.e. %(q:shall) instead of %(q:should).

#Wsc: Articles 4.1 and 4.2.  Seem to be duplicative. The version of Article 4 in the Common Approach seems more elegant.

#fnW: Article 7.  The word %(q:especially) should be replaced by %(q:including).  The impact of the directive should be monitored for all portions of the European business and user communities.  While small and medium enterprises and the open source community are important groups to include in the scope of monitoring activity, the monitoring of the directive's  impact should avoid focus on certain interest groups and not others.

#WnW: Article 8(b). It is not appropriate to consider the term of the patent only in the context of CIIs. The term of he patent has to be the same for all technologies.

#ofn: According to the Council's definition, %(q:computer-implemented inventions) exist in all fields of technology.  Therefore the question of reduction of the term indeed becomes utterly meaningless, even if revision of Art 32 TRIPs is to be envisaged.

#tln: Article 8(ca). It is not clear what this provision really means and what the Commission is being called to report on.

#ltW: Articles 8(cb) and 8(cc). The value of these provisions is questionable. Even if the Commission reported on such aspects it would not have the competence to follow through.

#asw: original document. user vitke password mipri

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: eicta-cons0401 ;
# txtlang: cs ;
# multlin: t ;
# End: ;

