COUNCIL OF THE EUROPEAN UNION Brussels, 17 March 2004

7230/04

Interinstitutional File: 2002/0047 (COD)

LIMITE PI 30 CODEC 347

WORKING DOCUMENT

from: to: No. prev. doc.: Presidency Working Party on Intellectual Property (Patents) 7377/04 PI 34 CODEC 370

No. Cion prop.: 6580/02 PI 10 CODEC 242

Subject:

Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions - Consolidated/annotated text

Delegations will find in Annex a consolidated/annotated text of the proposed Directive, which takes into account discussions in the Working Party on 2 March 2004.

Changes to 5570/04 are highlighted.

______________

7230/04 LK/mg 1

EN DG C I


Proposal for a DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on the patentability of computer-implemented inventions1

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,

Having regard to the proposal from the Commission2,

Having regard to the opinion of the Economic and Social Committee3,

Acting in accordance with the procedure laid down in Article 251 of the Treaty4,

Whereas:

(1)

The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

1

2 3 4

BE, SE, FR, EL, FI: general scrutiny reservation. DE: scrutiny reservation on Articles 5 to 9. ES, DK : parliamentary scrutiny reservation. OJ C, , p. OJ C, , p. OJ C, , p.

7230/04 LK/mg 2

EN DG C I


(2)

(3)

(4)

Differences exist in the protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market. Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently. The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.

(5)

Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation. Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

(6)

The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994)5. Article 27(1) of TRIPS provides that patents shall be

5 OJ L 336, 23.12.1994, p. 1.

7230/04 LK/mg 3

EN DG C I


available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.6

(7)

Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

(7a)

Accordingly, a computer program as such, in particular the expression of a computer program in source code or object code or in any other form, cannot constitute a patentable invention.7

(7b)

The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.8 The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

(8)

Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.

6 7 8 IT: delete the last sentence of recital 6. IT, ES, LU: transfer recital 7a to the main body of the Directive. DE, IT: replace recital 7b with a new recital 5a as in EP amendment 88.

7230/04 LK/mg 4

EN DG C I


9

(9)

(10)

(11)

(12)

(13)

In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs10, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright. In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology. It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art.11 Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable.12 A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention.

(13a) However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method, data processing method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

9

10

11 12

DE: insert a new recital as follows: "According to Article 10(1) of TRIPS, computer programs are protected, whether in source or object code, under the Berne Convention (1971)." OJ L 122 , 17.5.1991 p. 42� Directive amended by Directive 93/98/EEC (OJ L 290, 24.11.1993, p. 9). DE: replace by a recital along the lines of EP amendment 84. FR, ES, LU: replace "although" by "even if".

7230/04 LK/mg 5

EN DG C I


(13b) If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

(13c) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

(13d) The scope of the exclusive rights conferred by any patent are defined by the claims.13 Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.14

(14)

The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as obvious or non-technical procedures and business methods.

13 14

ES: specify that the claims are interpreted having regard to the description and the drawings. FI, DE, UK, ES, EL, BE, SE, DK: Reservation on recital 13d, on the grounds that this might not cover sufficiently the whole of Article 5. FI suggested inserting an additional recital 13da as follows: "Computer-implemented inventions can also be claimed as a reference to a computer program, either on its own or on a carrier, which, when loaded and executed in a programmed apparatus, puts into force a product or a process claimed in the same patent application." Several delegations entered a scrutiny reservation on the Finnish proposal, while UK spoke against it in order to avoid, for tactical reasons, overflagging the provision of Article 5(2).

7230/04 LK/mg 6

EN DG C I


(15)

(16)

(17)

(18)

This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible. The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.15 The provisions of this Directive are without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them. The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

15 DE: delete second sentence of recital 16.

7230/04 LK/mg 7

EN DG C I


(19)

Since the objectives of the proposed action, namely to harmonise national rules on computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.

HAVE ADOPTED THIS DIRECTIVE:

Article 1 Scope This Directive lays down rules for the patentability of computer-implemented inventions.

Article 2 Definitions For the purposes of this Directive the following definitions shall apply:

(a)

"computer-implemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;

7230/04 LK/mg 8

EN DG C I


(b)

"technical contribution" means a contribution to the state of the art in a field of technology which is not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether

or not these are accompanied by non-technical features.16 17

Article 3 Computer-implemented inventions as a field of technology - Deleted -

18

19

Article 4 Conditions for patentability

1.

In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

2. Deleted.

16

17

18

19

ES: clarify that the technical contribution, as a condition of inventive step, should be assessed solely on the basis of technical features. DE: take on board EP amendments 107 and 69. IT: take on board the last sentence of EP amendments 107 and 69. AT, NL: keep text of point (b) unchanged. COM would prefer this Article to be maintained, either as Article 3 or as a first sentence of Article 4, rather than including its contents in a recital. It could however reconsider this position if Recitals 7a and 12 were to remain as they now stand. IT: as an alternative to the last sentence of EP amendments 107 and 69, one could take on board EP amendment 45. NL, UK, AT, FR are strongly opposed to this EP amendment, on the grounds that it would exclude from patentability more than is needed. As a compromise, DE suggested deleting recital 13a.

7230/04 LK/mg 9

EN DG C I


Article 4a Exclusions from patentability

A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

Article 5 Form of claims

1.

Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

20

2.

A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1.21

20 21 IT: take on board EP amendments 103 and 119, as well as 104 and 120. COM: strong reservation on Article 5(2).

7230/04 LK/mg 10

EN DG C I


Article 6 Relationship with Directive 91/250 EC

The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

22

Article 7 Monitoring The Commission shall monitor the impact of23 computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and the open source community24, and electronic commerce.

Article 8 Report on the effects of the Directive

The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

(a)

the impact of patents for computer-implemented inventions on the factors referred in Article 7;

22

23 24

COM: need to provide for competition aspect, perhaps in a recital. LU, AT: need for wording providing better guarantees for interoperability. LU undertook to table a drafting suggestion. UK: insert "the patentability of". FR: replace "open source community" by "users and producers of free software".

7230/04 LK/mg 11

EN DG C I


(b) whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate, and whether it would be desirable and legally possible having regard to the Community's international obligations to make modifications to such rules;

(c)

whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties;

(ca) whether difficulties have been experienced in respect of the relationship between the protection by patent of computer-implemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EEC and whether any abuse of the patent system has occurred in relation to computer-implemented inventions;

(cb) how the requirements of this Directive have been taken into account in the practice of the European Patent Office and in its examination guidelines;

(cc) the aspects in respect of which it may be necessary to prepare for a diplomatic conference to revise the European Patent Convention;

(cd) the impact of patents for computer-implemented inventions on the development and commercialisation of interoperable computer programs and systems;

(d)

whether difficulties have been experienced in respect of the relationship between the protection by patents of computer-implemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EC.

Article 8a

In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall review the impact of this Directive and, where necessary, submit proposals for amending legislation to the European Parliament and the Council.

7230/04 LK/mg 12

EN DG C I


1.

Article 9 Implementation Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by ... (eighteen25 months from its entry into force) at the latest. They shall forthwith inform the Commission thereof.

2.

When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive

Article 10 Entry into force

This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.

Article 11 Addressees

This Directive is addressed to the Member States. Done at Brussels,

For the European Parliament The President

For the Council The President ________________________

25 NL, EL, IT: replace "eighteen" by "twenty four".

7230/04 LK/mg 13

EN DG C I


