RAT DER EUROP�ISCHEN UNION Br�ssel, den 17. M�rz 2004 (22.03) (OR. en)

7230/04

Interinstitutionelles Dossier: 2002/0047 (COD)

LIMITE PI 30 CODEC 347

ARBEITSDOKUMENT

des f�r Vorsitzes die Gruppe "Geistiges Eigentum" (Patente)

Nr. Vordokument: 7377/04 PI 34 CODEC 370

Nr. Kommissionsvorschlag: 6580/02 PI 10 CODEC 242

Betr.:

Vorschlag f�r eine Richtlinie des Europ�ischen Parlaments und des Rates �ber die Patentierbarkeit computerimplementierter Erfindungen - konsolidierte/kommentierte Fassung

Die Delegationen erhalten in der Anlage eine konsolidierte/kommentierte Fassung des Richtlinienvorschlags, in der die in der Gruppe "Geistiges Eigentum" am 2. M�rz 2004 gef�hrten Beratungen Ber�cksichtigung finden.

Die �nderungen gegen�ber Dokument 5570/04 sind gekennzeichnet.

________________________

7230/04 hka/MS/he 1

DE DG C I


Vorschlag f�r eine

RICHTLINIE DES EUROP�ISCHEN PARLAMENTS UND DES RATES

�ber die Patentierbarkeit computerimplementierter Erfindungen 1

DAS EUROP�ISCHE PARLAMENT UND DER RAT DER EUROP�ISCHEN UNION -

gest�tzt auf den Vertrag zur Gr�ndung der Europ�ischen Gemeinschaft, insbesondere auf Artikel 95,

auf Vorschlag der Kommission , 2

nach Stellungnahme des Wirtschafts- und Sozialausschusses , 3

gem�� dem Verfahren des Artikels 251 EG-Vertrag , 4

in Erw�gung nachstehender Gr�nde:

(1)

Damit der Binnenmarkt verwirklicht wird, m�ssen Beschr�nkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen beg�nstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium f�r den Erfolg des Binnenmarkts. Es ist unerl�sslich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam, transparent und einheitlich gesch�tzt sind, wenn Investitionen auf diesem Gebiet gesichert und gef�rdert werden sollen.

1 2 3 4 BE, SE, FR, EL, FI: allgemeiner Pr�fungsvorbehalt. ES, DK: Parlamentsvorbehalt. ABl. C vom , S. ABl. C vom , S. ABl. C vom , S.

7230/04 hka/MS/he 2

DE DG C I


(2)

(3)

(4)

(5)

(6)

Die Patentpraxis und die Rechtsprechung in den einzelnen Mitgliedstaaten hat zu Unterschieden beim Schutz computerimplementierter Erfindungen gef�hrt. Solche Unterschiede k�nnten den Handel st�ren und somit verhindern, dass der Binnenmarkt reibungslos funktioniert. Die Ursachen f�r die Unterschiede liegen darin begr�ndet, dass die Mitgliedstaaten neue, voneinander abweichende Verwaltungspraktiken eingef�hrt oder die nationalen Gerichte die geltenden Rechtsvorschriften unterschiedlich ausgelegt haben; diese Unterschiede k�nnten mit der Zeit noch gr��er werden. Die zunehmende Verbreitung und Nutzung von Computerprogrammen auf allen Gebieten der Technik und die weltumspannenden Verbreitungswege �ber das Internet sind ein kritischer Faktor f�r die technologische Innovation. Deshalb sollte sichergestellt sein, dass die Entwickler und Nutzer von Computerprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden. Aus diesen Gr�nden sollten die f�r die Patentierbarkeit computerimplementierter Erfindungen ma�geblichen Rechtsvorschriften vereinheitlicht werden, um sicherzustellen, dass die daraus folgende Rechtssicherheit und das Anforderungsniveau f�r die Patentierbarkeit dazu f�hren, dass innovative Unternehmen den gr��tm�glichen Nutzen aus ihrem Erfindungsprozess ziehen und Anreize f�r Investitionen und Innovationen geschaffen werden. Die Rechtssicherheit wird auch dadurch sichergestellt, dass bei Zweifeln �ber die Auslegung dieser Richtlinie die Gerichte der Mitgliedstaaten den Gerichtshof der Europ�ischen Gemeinschaften anrufen k�nnen und die letztinstanzlichen Gerichte der Mitgliedstaaten hierzu verpflichtet sind. Die Gemeinschaft und ihre Mitgliedstaaten sind auf das �bereinkommen �ber handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-�bereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 �ber den Abschluss der �bereink�nfte im Rahmen der multilateralen Verhandlungen der UruguayRunde (1986-1994) im Namen der Europ�ischen Gemeinschaft in Bezug auf die in ihre Zust�ndigkeiten fallenden Bereiche . Nach Artikel 27 Absatz 1 des TRIPS-�bereinkommens

5

5 ABl. L 336 vom 23.12.1994, S. 1.

7230/04 hka/MS/he 3

DE DG C I


sollen Patente f�r Erfindungen auf allen Gebieten der Technik erh�ltlich sein, sowohl f�r Erzeugnisse als auch f�r Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen T�tigkeit und sind gewerblich anwendbar. Gem�� dem TRIPS-�bereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erh�ltlich sein und Patentrechte ausge�bt werden k�nnen. Diese Grunds�tze sollten demgem�� auch f�r

computerimplementierte Erfindungen gelten. 6

(7)

Nach dem �bereinkommen �ber die Erteilung europ�ischer Patente (Europ�isches Patent�bereinkommen) vom 5. Oktober 1973 (EP�) und den Patentgesetzen der Mitgliedstaaten gelten Programme f�r Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, �sthetische Formsch�pfungen, Pl�ne, Regeln und Verfahren f�r gedankliche T�tigkeiten, f�r Spiele oder f�r gesch�ftliche T�tigkeiten sowie die Wiedergabe von Informationen ausdr�cklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt jedoch nur, und hat auch ihre Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenst�nde oder T�tigkeiten als solche bezieht, da die besagten Gegenst�nde und T�tigkeiten als solche keinem Gebiet der Technik zugeh�ren.

(7a) Ein Computerprogramm als solches, insbesondere die Ausdrucksform eines Computerprogramms in Quellcode oder Objektcode oder in jeder anderen Form kann folglich keine

patentierbare Erfindung darstellen. 7

(7b) Durch diese Richtlinie soll das Europ�ische Patent�bereinkommen nicht ge�ndert, sondern die unterschiedliche Auslegung seiner Bestimmungen vermieden werden . Die dadurch

8

entstehende Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen.

(8)

Der Patentschutz versetzt die Innovatoren in die Lage, Nutzen aus ihrer Kreativit�t zu ziehen. Patentrechte sch�tzen zwar Innovationen im Interesse der Gesellschaft allgemein; sie sollten aber nicht in wettbewerbswidriger Weise genutzt werden.

6 7

8

IT schl�gt vor, den letzten Satz von Erw�gungsgrund 6 zu streichen. IT, ES, LU w�nschen eine Umstellung von Erw�gungsgrund 7a in den Hauptteil der Richtlinie. DE, IT schlagen vor, den Erw�gungsgrund 7b durch einen neuen Erw�gungsgrund 5a (siehe EP-Ab�nderung 88) zu ersetzen.

7230/04 hka/MS/he 4

DE DG C I


9

(9) Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 �ber den Rechtsschutz von

Computerprogrammen 10 sind alle Ausdrucksformen von originalen Computerprogrammen

wie literarische Werke durch das Urheberrecht gesch�tzt. Die Ideen und Grunds�tze, die einem Element eines Computerprogramms zugrunde liegen, sind dagegen nicht durch das Urheberrecht gesch�tzt. (10) Damit eine Erfindung als patentierbar gilt, sollte sie technischen Charakter haben und somit einem Gebiet der Technik zuzuordnen sein. (11) F�r Erfindungen gilt ganz allgemein die Voraussetzung, dass sie, um das Kriterium der erfinderischen T�tigkeit zu erf�llen, einen technischen Beitrag zum Stand der Technik leisten

sollten. 11

(12) Eine computerimplementierte Erfindung erf�llt folglich trotz der Tatsache, dass sie einem Gebiet der Technik zugerechnet wird, sofern sie keinen technischen Beitrag zum Stand der Technik leistet, z.B. weil dem besonderen Beitrag die Technizit�t fehlt, nicht das Kriterium

der erfinderischen T�tigkeit und ist somit nicht patentierbar. 12

(13) Wenn eine festgelegte Prozedur oder Handlungsfolge in einer Vorrichtung, z.B. einem Computer, abl�uft, kann sie einen technischen Beitrag zum Stand der Technik leisten und somit eine patentierbare Erfindung darstellen. (13a) Allerdings reicht allein die Tatsache, dass eine ansonsten nicht patentierbare Methode in einer Vorrichtung wie einem Computer angewendet wird, nicht aus, um davon auszugehen, dass ein technischer Beitrag geleistet wird. Folglich kann eine computerimplementierte Gesch�fts-, Datenverarbeitungs- oder andere Methode, bei der der einzige Beitrag zum Stand der Technik nichttechnischen Charakter hat, keine patentierbare Erfindung darstellen.

9

10

11

12

DE w�nscht die Einf�gung eines neuen Erw�gungsgrunds mit folgendem Wortlaut: "Gem�� Artikel 10 Absatz 1 des TRIPS-�bereinkommens sind Computerprogramme sowohl in Quellcode als auch in Objektcode durch die Berner �bereinkunft (1971) gesch�tzt." ABl. L 122 vom 17.5.1991, S. 42 � ge�ndert durch Richtlinie 93/98/EWG (ABl. L 290 vom 24.11.1993, S. 9). DE schl�gt vor, diesen Erw�gungsgrund durch einen anderen - etwa wie in der EPAb�nderung 84 vorgeschlagen - zu ersetzen. FR, ES, LU schlagen vor, die Worte "trotz der Tatsache, dass" durch "selbst wenn" zu ersetzen.

7230/04 hka/MS/he 5

DE DG C I


(13b) Bezieht sich der Beitrag zum Stand der Technik ausschlie�lich auf einen nichtpatentierbaren Gegenstand, kann es sich nicht um eine patentierbare Erfindung handeln, unabh�ngig davon, wie der Gegenstand in den Patentanspr�chen dargestellt wird. So kann beispielsweise das Erfordernis eines technischen Beitrags nicht einfach dadurch umgangen werden, dass in den Patentanspr�chen technische Hilfsmittel spezifiziert werden. (13c) Au�erdem ist ein Algorithmus von Natur aus nichttechnischer Art und kann deshalb keine technische Erfindung darstellen. Allerdings kann eine Methode, die die Benutzung eines Algorithmus umfasst, unter der Voraussetzung patentierbar sein, dass die Methode zur L�sung eines technischen Problems angewandt wird. Allerdings w�rde ein f�r eine derartige Methode gew�hrtes Patent kein Monopol auf den Algorithmus selbst oder seine Anwendung in einem von dem Patent nicht betroffenen Kontext verleihen. (13d) Der Anwendungsbereich der ausschlie�lichen Rechte, die durch ein Patent gew�hrt werden, Erfindungen m�ssen unter Bezugnahme entweder auf ein Erzeugnis wie beispielsweise eine programmierte Vorrichtung oder ein Verfahren, das in einer solchen Vorrichtung verwirklicht wird, angemeldet werden. Werden demnach einzelne Software-Elemente in einem Kontext benutzt, bei dem es nicht um die Verwirklichung eines rechtm��ig beanspruchten Erzeugnisses oder Verfahrens handelt, stellt eine solche Verwendung keine Patentverletzung

wird durch die Patentanspr�che definiert. 13 Patentanspr�che auf computerimplementierte

dar. 14

(14) Um computerimplementierte Erfindungen rechtlich zu sch�tzen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage f�r den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarheit zu gew�hrleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie nahe liegende oder nichttechnische Trivial-Vorg�nge und Gesch�ftsmethoden, als patentf�hig zu erachten.

13

14

Nach Auffassung von ES m�sste zum Ausdruck kommen, dass die Anspr�che anhand der Beschreibung und der Zeichnungen auszulegen sind. FI, DE, UK, ES, EL, BE, SE, DK haben einen Vorbehalt zu Erw�gungsgrund 13d, da er ihres Erachtens m�glicherweise nicht die Gesamtheit des Artikels 5 abdeckt. FI schlug die Einf�gung eines neuen Erw�gungsgrunds 13da mit folgendem Wortlaut vor: "Patentanspr�che auf computerimplementierte Erfindungen k�nnen auch als Bezugnahme auf ein Computerprogramm als solches oder aber auf einem Tr�ger angemeldet werden, das wenn es auf eine programmierte Vorrichtung geladen und dort verwirklicht wird, ein Erzeugnis oder ein Verfahren aktiviert, f�r das im Rahmen der gleichen Patentanmeldung ein Patentanspruch angemeldet wird." Mehrere Delegationen legten einen Pr�fungsvorbehalt zu dem finnischen Vorschlag ein, w�hrend UK sich dagegen aussprach, um aus taktischen Gr�nden eine �berlappung mit der Bestimmung des Artikels 5 Absatz 2 zu vermeiden.

7230/04 hka/MS/he 6

DE DG C I


(15) Diese Richtlinie sollte sich auf die Festlegung bestimmter Patentierbarkeitsgrunds�tze beschr�nken; im Wesentlichen sollen diese Grunds�tze einerseits die Schutzf�higkeit von Erfindungen sicherstellen, die einem Gebiet der Technik zugeh�ren und einen technischen Beitrag zum Stand der Technik leisten, andererseits Erfindungen vom Schutz ausschlie�en, die keinen technischen Beitrag zum Stand der Technik leisten.

(16) Die Wettbewerbsposition der europ�ischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern wird sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausger�umt sind und die Rechtslage transparenter ist. Beim derzeitigen Trend der klassischen verarbeitenden Industrie zur Verlagerung ihrer Betriebe in Niedriglohnl�nder au�erhalb der Europ�ischen Union liegt die Bedeutung des

Urheberrechtsschutzes und insbesondere des Patentschutzes auf der Hand. 15

(17) Die Bestimmungen dieser Richtlinie lassen die Anwendung der Artikel 81 und 82 des Vertrags unber�hrt, insbesondere wenn ein marktbeherrschender Lieferant sich weigert, den Einsatz einer patentierten Technik zu gestatten, die f�r den alleinigen Zweck erforderlich ist, die Umwandlung der in zwei unterschiedlichen Computersystemen oder Netzen verwendeten Konventionen zu gew�hrleisten und somit die Daten�bermittlung und den Datenaustausch zwischen den Systemen oder Netzen zu erm�glichen.

(18) Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere gem�� den Vorschriften in Bezug auf die Dekompilierung und die Interoperabilit�t. Insbesondere erfordern Handlungen, die gem�� Artikel 5 und 6 jener Richtlinie keine Genehmigung des Rechtsinhabers in Bezug auf dessen Urheberrechte an dem oder in Zusammenhang mit dem Computerprogramm erfordern, f�r die aber ohne Artikel 5 oder 6 jener Richtlinie eine solche Genehmigung erforderlich w�re, keine Genehmigung des Rechtsinhabers in Bezug auf die Patentrechte des Rechtsinhabers an dem oder in Zusammenhang mit dem Computerprogramm.

15 DE schl�gt vor, den zweiten Satz des Erw�gungsgrunds 16 zu streichen.

7230/04 hka/MS/he 7

DE DG C I


(19) Gem�� Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiarit�tsprinzip t�tig werden, da die Ziele der vorgeschlagenen Ma�nahme, also die Harmonisierung der nationalen Vorschriften f�r computerimplementierte Erfindungen, auf Ebene der Mitgliedstaaten nicht ausreichend erreicht werden k�nnen und daher wegen ihres Umfangs oder ihrer Wirkung besser auf Gemeinschaftsebene erreicht werden k�nnen. Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verh�ltnism��igkeit, da sie nicht �ber das f�r die Erreichung der Ziele erforderliche Ma� hinausgeht -

HABEN FOLGENDE RICHTLINIE ERLASSEN:

Artikel 1 Anwendungsbereich

Diese Richtlinie legt Vorschriften f�r die Patentierbarkeit computerimplementierter Erfindungen fest.

Artikel 2 Begriffsbestimmungen

F�r die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen:

a)

"Computerimplementierte Erfindung" ist jede Erfindung, zu deren Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

7230/04 hka/MS/he 8

DE DG C I


b)

"Technischer Beitrag" ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technologie, der f�r eine fachkundige Person nicht nahe liegend ist. Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der technische Merkmale umfassen muss, die ihrerseits mit nichttechnischen

Merkmalen versehen sein k�nnen, vom Stand der Technik abhebt. 16 17

Artikel 3 Computerimplementierte Erfindungen als Gebiet der Technik

- Gestrichen 18

19

Artikel 4 Voraussetzungen der Patentierbarkeit

(1) Um patentierbar zu sein, m�ssen computerimplementierte Erfindungen neu sein, auf

einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen T�tigkeit zu erf�llen, m�ssen computerimplementierte Erfindungen einen technischen Beitrag leisten.

(2) Gestrichen.

16

17

18

19

Nach Auffassung von ES sollte klargestellt werden, dass der technische Beitrag als eine Bedingung erfinderischer T�tigkeit lediglich auf der Grundlage der technischen Merkmale bewertet werden sollte. DE pl�diert daf�r, die EP-Ab�nderungen 107 und 69 zu �bernehmen, IT daf�r, den letzten Satz der EP-Ab�nderungen 107 und 69 zu �bernehmen. AT und NL wollen den Buchstaben b unver�ndert belassen. Die Kommission w�rde diesen Artikel lieber beibehalten - entweder als Artikel 3 oder als ersten Satz von Artikel 4 -, anstatt dessen Inhalt in einen Erw�gungsgrund aufzunehmen. Sie k�nnte jedoch ihren Standpunkt �berdenken, wenn die Erw�gungsgr�nde 7a und 12 in der jetzigen Fassung beibehalten w�rden. Nach Auffassung von IT k�nnte als Alternative zum letzten Satz der EP-Ab�nderungen 107 und 69 die EP-Ab�nderung 45 �bernommen werden. NL, UK, AT, FR sind eindeutig gegen diese EP-Ab�nderung, da dadurch mehr als erforderlich von der Patentierbarkeit ausgenommen w�rde. Als Kompromiss schlug DE vor, den Erw�gungsgrund 13a zu streichen.

7230/04 hka/MS/he 9

DE DG C I


Artikel 4a Ausschluss von der Patentierbarkeit

Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausf�hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausf�hrung ein Computerprogramm eingesetzt wird und durch die Gesch�ftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentf�hig, wenn sie �ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.

Artikel 5 Form des Patentanspruchs

(1) Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung

entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf�hrung von Software verwirklicht wird.

20

(2) Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder

auf ein auf einem Datentr�ger vorliegendes Programm, ist nur zul�ssig, insoweit das Programm, wenn es auf einem Computer, auf einem programmierten Computernetz oder einer sonstigen programmierbaren Vorrichtung installiert und ausgef�hrt wird, einen in derselben Patentanmeldung

erhobenen Erzeugnis- oder Verfahrensanspruch gem�� Absatz 1 begr�ndet. 21

20 21 IT tritt f�r eine �bernahme der EP-Ab�nderungen 103 und 119 sowie 104 und 120 ein. KOM legte einen entschiedenen Vorbehalt zu Artikel 5 Absatz 2 ein.

7230/04 hka/MS/he 10

DE DG C I


Artikel 6 Konkurrenz zur Richtlinie 91/250/EWG

Rechte, die aus Patenten erwachsen, die f�r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber�hrt von urheberrechtlich zul�ssigen Handlungen gem�� Artikel 5 und 6 der Richtlinie 91/250/EWG �ber den Rechtsschutz von Computerprogrammen, insbesondere hinsichtlich der Bestimmungen �ber die Dekompilierung und die Interoperabilit�t.

22

Artikel 7 Beobachtung

Die Kommission beobachtet, wie sich 23 computerimplementierte Erfindungen auf die Innovations-

t�tigkeit und den Wettbewerb in Europa und weltweit sowie auf die europ�ischen Unternehmen, insbesondere auf die kleinen und mittleren Unternehmen und die Open-Source-Bewegung , und den elektronischen Gesch�ftsverkehr auswirken.

24

Artikel 8 Bericht �ber die Auswirkungen der Richtlinie

Die Kommission legt dem Europ�ischen Parlament und dem Rat sp�testens am [DATUM (drei Jahre nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor �ber:

a)

die Auswirkungen von Patenten auf computerimplementierte Erfindungen auf die in Artikel 7 genannten Faktoren;

22

23

24

KOM erachtet es f�r erforderlich, den Wettbewerbsaspekt zu behandeln, eventuell in einem Erw�gungsgrund. LU, AT halten einen Wortlaut f�r notwendig, der bessere Garantien f�r die Interoperabilit�t bietet. LU erkl�rte sich bereit, einen Textvorschlag vorzulegen. UK w�nscht die Einf�gung der Worte "die Patentierbarkeit" (und die entsprechenden �nderungen im Satz: computerimplementierter Erfindungen ... auswirkt.) FR beantragt, "die Open-Source-Bewegung" durch "Nutzer und Hersteller freier Software" zu ersetzen.

7230/04 hka/MS/he 11

DE DG C I


b)

c)

ca)

cb)

cc)

cd)

d)

die Angemessenheit der Regeln f�r die Laufzeit des Patents und die Festlegung der Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die Neuheit, die erfinderische T�tigkeit und den eigentlichen Patentanspruch, und ihre Einsch�tzung, ob es unter Ber�cksichtigung der internationalen Verpflichtungen der Gemeinschaft w�nschenswert und rechtlich m�glich w�re, diese Regeln zu �ndern; etwaige Schwierigkeiten, die in Mitgliedstaaten aufgetreten sind, in denen Erfindungen vor Patenterteilung nicht auf Neuheit und Erfindungsh�he gepr�ft werden, und etwaige Schritte, die unternommen werden sollten, um diese Schwierigkeiten zu beseitigen; etwaige Schwierigkeiten, die im Hinblick auf das Verh�ltnis zwischen dem Schutz durch Patente auf computerimplementierte Erfindungen und dem Schutz von Computerprogrammen durch das Urheberrecht, wie es die Richtlinie 91/250/EWG vorsieht, aufgetreten sind, sowie etwaige Missbr�uche im Patentsystem in Verbindung mit computerimplementierten Erfindungen; die Art und Weise, in der die Anforderungen dieser Richtlinie in der Praxis des Europ�ischen Patentamts und in seinen Pr�fungsrichtlinien ber�cksichtigt worden sind; die Aspekte, unter denen es unter Umst�nden notwendig ist, eine diplomatische Konferenz zur �berarbeitung des Europ�ischen Patent�bereinkommens vorzubereiten; die Auswirkungen von Patenten f�r computerimplementierte Erfindungen auf die Entwicklung und Kommerzialisierung interoperierender Computerprogramme und -systeme; etwaige Schwierigkeiten, die im Zusammenhang mit der Beziehung zwischen dem Patentschutz von computerimplementierten Erfindungen und dem Urheberrechtschutz von Computerprogrammen nach der Richtlinie 91/250/EWG aufgetreten sind.

Artikel 8a

Anhand der Beobachtung gem�� Artikel 7 und des gem�� Artikel 8 zu erstellenden Berichts �berpr�ft die Kommission die Auswirkungen dieser Richtlinie und unterbreitet dem Europ�ischen Parlament und dem Rat erforderlichenfalls Vorschl�ge zur �nderung der Rechtsvorschriften.

7230/04 hka/MS/he 12

DE DG C I


Artikel 9 Umsetzung

(1) Die Mitgliedstaaten setzen die Rechts- und Verwaltungsvorschriften in Kraft, die erfor-

derlich sind, um dieser Richtlinie sp�testens ... (18 25 Monate nach Inkrafttreten dieser Richtlinie)

nachzukommen. Sie setzen die Kommission unverz�glich davon in Kenntnis.

Wenn die Mitgliedstaaten diese Vorschriften erlassen, nehmen sie in den Vorschriften selbst oder durch einen Hinweis bei der amtlichen Ver�ffentlichung auf diese Richtlinie Bezug. Die Mitgliedstaaten regeln die Einzelheiten der Bezugnahme.

(2) Die Mitgliedstaaten teilen der Kommission den Wortlaut der innerstaatlichen Rechts-

vorschriften mit, die sie auf dem unter diese Richtlinie fallenden Gebiet erlassen.

Artikel 10 Inkrafttreten

Diese Richtlinie tritt am zwanzigsten Tag nach ihrer Ver�ffentlichung im Amtsblatt der Europ�ischen Union in Kraft.

Artikel 11 Adressaten

Diese Richtlinie ist an die Mitgliedstaaten gerichtet.

Geschehen zu Br�ssel am

Im Namen des Europ�ischen Parlaments Der Pr�sident ________________________ Im Namen des Rates Der Pr�sident

25 NL, EL, IT sprechen sich daf�r aus, "18" durch "24" zu ersetzen.

7230/04 hka/MS/he 13

DE DG C I


