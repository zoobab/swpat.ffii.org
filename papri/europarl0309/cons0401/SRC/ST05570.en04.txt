Brussels, 29 January 2004

COUNCIL OF THE EUROPEAN UNION

5570/04

Interinstitutional File: 2002/0047 (COD)

LIMITE PI 7 CODEC 89

WORKING DOCUMENT

from: to: No. prev. doc.: Presidency Working Party on Intellectual Property (Patents) 14720/03 PI 114 CODEC 1582

No. Cion prop.: 6580/02 PI 10 CODEC 242

Subject:

Proposal for a Directive of the European Parliament and of the Council on the patentability of computerimplemented inventions  Presidency compromise proposal

Delegations will find in Annex an annotated compromise proposal from the Presidency, which takes into account discussions in the Working Party  on 19 November 2003.

Any changes  to 14017/02 are highlighted. ______________

5570/04 LK/mg 1

EN DG C I


Proposal for a DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on the patentability of computerimplemented inventions1

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,

Having regard to the proposal from the Commission2,

Having regard to the opinion of the Economic and Social Committee3,

Acting in accordance with the procedure laid down in Article 251 of the Treaty4,

Whereas:

(1)

The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent5 and harmonised protection of computerimplemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

1 2 3 4 5

 

B, I, F: general scrutiny reservation on the proposal. E: parliamentary scrutiny.          OJ C, , p.  OJ C, , p.  OJ C, , p. 

        EP Amd. No. 1.

5570/04 LK/mg 2

EN DG C I


 (2)

(3)

(4)

Differences exist in the protection of computerimplemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market. Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently. The steady increase in the distribution and use of computer programs in all fields of technology and in their worldwide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.

(5)

Therefore, the legal rules governing the patentability of computerimplemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation. Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.6

(6)

The Community and its Member States are bound by the Agreement on traderelated aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (19861994)7. Article 27(1) of TRIPS provides that patents shall be 

6 7         EP Amd. No. 2. OJ L 336, 23.12.1994, p. 1.

5570/04 LK/mg 3

EN DG C I


available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computerimplemented inventions.

(7)

Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subjectmatter or activities as such, because the said subjectmatter and activities as such do not belong to a field of technology.

(7a)

Accordingly, a computer program as such, in particular the expression of a computer program in source code or object code or in any other form, cannot constitute a patentable invention.

(7b)(new) The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.8 The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software9.

(8)

8 9

Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anticompetitive.         EP Amd. No 3   D, UK ,S, E reservation. B: Scrutiny reservation: B.         EP Amd No. 88 (2nd sentence only).

5570/04 LK/mg 4

EN DG C I


(9)

(10)

(11)

(12)

(13)

In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs10, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright.  In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology. It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art. Accordingly, although a computerimplemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable.11 A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. 

(13a) However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computerimplemented business method, data processing method or other method in which the only contribution to the state of the art is non technical cannot constitute a patentable invention.12

10

OJ L 122 , 17.5.1991 p. 42� Directive amended by Directive 93/98/EEC (OJ L 290, 24.11.1993, p. 9).

    11 12

5570/04

F: Scrutiny reservation on Recital 12, suggests replacing "although" by "even if".  EP Amd. No. 85. LK/mg DG C I

5

EN


(13b) [... ] If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims.  For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.13 

(13c) Furthermore, an algorithm [... ] is inherently nontechnical and therefore cannot constitute a technical invention.  Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem.  However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.14

(13d)(new)  The scope of the exclusive rights conferred by any patent are defined by the claims. Computerimplemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.15

(14)

13 14 15      

The legal protection of computerimplemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law[... ] remain the essential basis for the legal protection of computerimplemented inventions. [... ]. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as [trivial] [nontechnical] procedures and business methods.16 EP Amd. No. 7. EP Amd. No. 8  DK: Scrutiny reservation. EP Amd. No. 9  FIN, UK, E, EL, B, S, DK: Reservation re compatibility with Article 5

of 14017/02.   EP  Amd. No. 86  D, A, FIN, I, L can accept it. COM, E, S, P, IRL want to replace "trivial" by "nontechnical".

16

5570/04 LK/mg 6

EN DG C I


(15)

(16)

 (17)

 (18)

This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible. The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computerimplemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to lowcost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is selfevident.17 This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty18 The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability [... ]. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.19

17 18 19      

5570/04

EP Amd. No. 11. EP Amd. No. 12. EP Amd. No. 13  DK, E: Reservation:  EL: Scrutiny reservation. LK/mg DG C I

7

EN


 (19)

Since   the   objectives   of   the   proposed   action,   namely   to   harmonise   national   rules   on computerimplemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set  out in that Article, this  Directive  does not  go beyond what is necessary to achieve those objectives.

HAVE ADOPTED THIS DIRECTIVE:

Article 1 Scope This Directive lays down rules for the patentability of computerimplemented inventions.

Article 2

Definitions For the purposes of this Directive the following definitions shall apply:

(a)

" computerimplemented invention" means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;

(b)

" technical contribution" means a contribution to the state of the art in a field of technology which is not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by nontechnical features.

5570/04 LK/mg 8

EN DG C I


Article 3 Computerimplemented inventions as a field of technology  Deleted  20 21

Article 4 Conditions for patentability

1. 

In order to be patentable, a computerimplemented invention must be susceptible of industrial application and new and involve an inventive step.  In order to involve an inventive step, a computerimplemented invention must make a technical contribution22.

2.(new) 

Member States shall ensure that a computerimplemented invention making a technical contribution constitutes a necessary condition of involving an inventive step23.

Article 4a Exclusions from patentability

A computerimplemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run

20   COM would prefer this Article to be maintained, either as Article 3 or as a first sentence

21 22 23

of Article 4, rather than including its contents in a recital. It could however reconsider this position if  Recitals 7a and 12 were to remain as they now stand.        EP Amd. No. 15.       EP Amd. No. 16.       Ditto. E: Reservation.

5570/04 LK/mg 9

EN DG C I


shall not be patentable.24

24   EP Amd. 17 corresponds to 14017/02.

5570/04 LK/mg 10

EN DG C I


Article 5 Form of claims

1.

Member States shall ensure that a computerimplemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

2.

A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1.

I.

Article 6 Relationship with Directive 91/250 EC

The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability25.

II.

Article 7 Monitoring

The Commission shall monitor the impact of [... ] computerimplemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and mediumsized enterprises, and the open source community, and electronic

25   EP Amd. No. 19.

5570/04 LK/mg 11

EN DG C I


commerce.26

26   EP Amd. No. 71.

5570/04 LK/mg 12

EN DG C I


Article 8 Report on the effects of the Directive

The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

(a)

the impact of patents for computerimplemented inventions on the factors referred in Article 7;

 (b)  whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate, and whether it would be desirable and legally possible having regard to the Community's international obligations to make modifications to such rules;27

(c)  whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties;

(ca)(new) whether difficulties have been experienced in respect of the relationship between the protection by patent of computerimplemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EEC and whether any abuse of the patent system has occurred in relation to computer implemented inventions;28

(cb)(new) the aspects in respect of which it may be necessary to prepare for a diplomatic conference to revise the European Patent Convention;29

27 28 29       EP Amd. No. 92 reformulated.  EP Amd. No. 23   DK: reservation on  reference to " abuse". EP Amd. No. 25 reformulated. 

5570/04 LK/mg 13

EN DG C I


(cc)(new) how the requirements of this Directive have been taken into account in the practice of the European Patent Office and in its examination guidelines;30

(cd)(new) the impact of patents for computerimplemented inventions on the development and commercialisation of interoperable computer programs and systems;31

(d)

whether difficulties have been experienced in respect of the relationship between the protection by patents of computerimplemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EC.

Article 8a (new)

In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall review the impact of this Directive and, where necessary, submit proposals for amending legislation to the European Parliament and the Council.32

Article 9 Implementation

1.

Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than eighteen months after its entry into force. 33They shall forthwith inform the Commission thereof. 

30 31 32 33        

When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. EP Amd. No. 26  D: reservation. EP Amd. No. 89 reformulated.  EP Amd. No. 27. EP Amd. No. 28.

5570/04 LK/mg 14

EN DG C I


2. 

Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive

Article 10 Entry into force

This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.

Article 11 Addressees

This Directive is addressed to the Member States. Done at Brussels,

For the European Parliament The President For the Council The President

________________________

5570/04 LK/mg 15

EN DG C I


