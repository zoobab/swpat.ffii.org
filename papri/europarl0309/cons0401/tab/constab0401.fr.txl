<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Directive Brevet Logiciel UE: Versions du Parlement et du Conséil

#descr: In September 2003 the European Parliament amended a proposal from the
European Commission for patentability of software in such a way that
it became a proposal that clearly disallows software patents.   The EU
Council of Ministers and the European Commission subsequently
disregarded the European Parliament's Amendments and reached
%(q:political agreement) in May 2004 in favor of a text that goes even
further than the previous text of the Commission and allows US-style
unlimited patentability, but shrouds this in a veil of convoluted
wording, characterised by multiple negations and other misleading
syntactic devices.  Below you can find out yourself by carefully
reading the core provisions of the proposed EU directive %(q:on the
patentability of computer-implemented inventions) which we have
collected in a table, together with short explanations of the
implications.  At the end we add pointers to the originals from the
Parliament and Council websites as well as to related analyses and
news reports.

#implik: Implications

#keW: A patent holder is entitled to block interoperability as much as he
wants, as long as he doesn't violate antitrust law.  Patent law itself
imposes no limits.  Moreover, even if anti-trust law is violated, it
is very unlikely that there will be any effective legal remedy, given
the complexity of the concerned procedures, as shown in the EU vs
Microsoft case, where the Samba project's freedom to use Microsoft's
standards was not assured.  This recital is a new repressive measure
against software developpers and users taken by the Council in 2004.  
The Commission had at least left room for interpretation here.  See
also Article 6 and 6a.

#uig: A %(q:computer-implemented invention) is any process that runs under
computer control, including general purpose data processing processes
(= computer programs), mechanical processes, chemical processes and
anything whatsoever if only the claimant choses to incorporate
computer features in the claim.  Thus it is up to the patent attorney
to decide whether the directive applies to his claims or not.

#pps: A %(q:computer-implemented invention) is a non-computing process (e.g.
chemical process) that runs under computer control.  It might have
been better to call this a %(q:computer-aided invention) or
%(q:computerised invention).  Computers alone can %(q:implement) only
computing processes (= computer programs), which are not patentable
inventions according to Art 52 EPC.

#nyW: %(q:technical contribution) is a contribution in a field of technology
which is not obvious, but which can be entirely non-technical itself
(the %(q:claim as a whole) must contain technical features, but the
technical contribution itself does not).  Thanks to the insertion of
the word %(q:new) upon request of the German delegation, the
%(qw:technical contribution) seems to have become subject to a novelty
test, just like the %(q:invention), of which it was originally a
synonym.  Thereby the Council seems to be departing from EPO practise
and moving toward the Parliament's view.  However, the Council's
%(q:technical contribution) may consist only of non-technical
features: it is enough if the %(q:claim as a whole) contains technical
features.

#oWu: %(q:technical contribution) is the same as an invention: %(q:what you
invent is your technical contribution to the state of the art). 
Thereby the Parliament clearly departs from the recent
counter-intuitive usages of the EPO and returns to the traditional
usage of the term.  Moreover, the Parliament explains the term
%(q:technical) both by definition and by a negative example, thereby
also clarifying the application of Art 27 TRIPs in European patent
law.  Moroever, the %(q:technical contribution) must, according to the
Parliament, consist solely of technical features, as explained in Art
4(3).

#ton: For the Parliament, the term %(q:technical) means something.  For the
Council it does not.  An alternative phrasing could have been
%(q:technology = applied natural science).

#ihE: A clarification of another key terms used in Art 27 TRIPs and in the
directive proposal.  The Parliament returns to a traditional
definition that is found implicitely and explicitely in various
national patent laws in Europe and in Art 52(4) EPC, but has been
largely forgotton or diluted at the EPO.

#kcf: Leave the magic words from TRIPs undefined, so that they can continue
to be used to push for extension of patentability.

#WfW: Data processing has nothing to do with the machinery used for it.  The
latter is patentable, the former not.  This is a clear and simple
negative definition of a key term used in the %(tr:TRIPs treaty).  It
is achieved by a straightforward semantic transformation of Art 52(2)
EPC.

#cWc: The %(q:technical contribution) may consist solely of non-technical
features

#eWi: The %(q:technical contribution) must consist of technical features.

#xti: Another definition of %(q:technical contribution) aka %(q:invention)
on the basis of the terms %(q:technical) and %(q:industrial) with
their traditional clearly delimiting meanings.

#stm: This article is based on a JURI amendment (= amendment from the people
who also run the Council's working party) that made it through the EP
Plenary Session only because it is meaningless and therefore harmless.
 The Council's additions however make this amendment harmful.  They
use the wording of Art 52 EPC and insinuate that a %(q:program as
such) is only the %(q:expression) side of this program, i.e. narrow
copyright-style claims that nobody would write into a patent anyway. 
This interpretation does not conform to the usual %(ex:requirements of
interpretation of laws) and is not currently in use at the patent
courts.  It deprives Art 52 EPC of all limiting meaning.

#isW: The EP's version was brought into JURI (Legal Affairs Committee) by
%(am:Arlene McCarthy) in spring of 2003.  Like most of the %(ju:JURI
amendments), it was inspired and probably drafted by circle around the
%(dk:Council's Patent Working Party).  Unlike many of the JURI
amendments, this one made its way into the final plenary decision of
September 2003.  This is because it is unclear and therefore harmless
in a context where it co-exists with clearer provisions.  Nobody
except perhaps some authoritative EPO Board of Appeal knows what
%(q:normal physical interaction between a program and a computer)
could be.  The term means as much as %(q:normal physical interaction
between a recipe and a cook): nothing or whatever the EPO may want it
to mean.

#odi: Another negative definition of %(q:technical contribution) that really
excludes something.  It is equivalent to Art 3a and based on the
German Federal Patent Court's %(bp:Error Search decision of 2002)
which held that any business method would become patentable if the
court were to follow the EPO's doctrine of viewing %(q:improvements in
computing efficiency) as technical contributions.

#lng: The article implies that a claim to a computer running software or to
a computing process running on general purpose computing equipment (=
a computer program) is permissible.

#eWa: FFII's proposal for this had been:

#WWt: You can get a monopoly on a certain kind of computer program on its
own %(q:only) if you also asked for a monopoly on this kind of
computer program executing on a computer.  The practical result is
that not only the usage of such computer programs, but also their
publication and distribution is forbidden.  Together with the
Council's Art 4A, this provision reinterprets Art 52 EPC to make it
meaningless: only a narrow claim to a particular textual expression of
a program is construed to be directed to that program %(q:as such),
whereas a broad claim to a whole class of %(q:programs on their own)
is not directed to these programs as such.

#por: Program claims are not permissible, nor is it permissible to obtain a
monopoly on publication and distribution by other forms of claims. 
The Parliament concretises Art 10 of the European Convention on Human
Rights, which mandates freedom of publication.  During the JURI vote
of 2003-06-17, this amendment was not put to vote because the
equivalent of the Council's Art 5(2) had already been approved, and
these two were considered mutually exclusive by the rapporteur Arlene
McCarthy.  However the Parliament's plenary session scrapped Art 5(2)
and voted for 5.1b instead.

#hfW: No objections against the EPO's practise of granting patents on
problem specifications where not even one implementation example is
disclosed in a verifiable way.

#koW: The patent system should take its disclosure function seriously.  If
patents are a %(q:monopoly in return for disclosure) deal, then
programming logic is on the disclosure side of that deal.

#csw: entirely impractical and only exeptional restriction of patent
priviledges. Reverse engineering and decompilation may not be
forbidden by using patents (this is a tautology: patents simply cannot
be used to forbid those things, they fall entirely under copyright
law: patents are about usage of something, not about studying it).

#erj: The Parliament also left this meaningless text from the Commission's
proposal untouched but supplemented it with Art 6a, which the Council
rejects.

#uah: Use of patented techniques for interoperation is a patent
infringement.  Exceptions for the benefit of interoperability can be
made only in rare cases after lengthy and unpracticable legal
procedings.  See also Recital 17.

#tel: The right to interoperate limits the right of the patent holder to
enforce his patent.  As long as software patents stay invalid, this
clause should never need to be used, but it is a useful clarification
of Art 30 TRIPs nevertheless.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: constab0401 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

