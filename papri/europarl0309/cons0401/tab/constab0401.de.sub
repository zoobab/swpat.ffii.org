\begin{subdocument}{constab0401}{EU-Softwarepatent-Richtlinie: Versionen von Parlament und Rat}{http://localhost/swpat/papiere/europarl0309/cons0401/tab/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/02/10 von PILCH Hartmut\footnote{\url{http://www.ffii.org/\~phm}}}{In September 2003 the European Parliament amended a proposal from the European Commission for patentability of software in such a way that it became a proposal that clearly disallows software patents.   The EU Council of Ministers and the European Commission subsequently disregarded the European Parliament's Amendments and reached ``political agreement'' in May 2004 in favor of a text that goes even further than the previous text of the Commission and allows US-style unlimited patentability, but shrouds this in a veil of convoluted wording, characterised by multiple negations and other misleading syntactic devices.  Below you can find out yourself by carefully reading the core provisions of the proposed EU directive ``on the patentability of computer-implemented inventions'' which we have collected in a table, together with short explanations of the implications.  At the end we add pointers to the originals from the Parliament and Council websites as well as to related analyses and news reports.}
\begin{sect}{tab}{Tabelle}
\begin{center}
\begin{center}
\begin{longtable}{|C{22}|C{22}|C{22}|C{22}|}
\hline
{\bf Rat 2004-05-18} & Implications & {\bf Parlament 2003-09-24} & Implications\\\hline
\endhead
{\bf Erw\"{a}gung 17\par

Die Bestimmungen dieser Richtlinie lassen die Anwendung der Artikel 81 und 82 des Vertrags unber\"{u}hrt, insbesondere wenn ein marktbeherrschender Lieferant sich weigert, den Einsatz einer patentierten Technik zu gestatten, die f\"{u}r den alleinigen Zweck erforderlich ist, die Umwandlung der in zwei unterschiedlichen Computersystemen oder Netzen verwendeten Konventionen zu gew\"{a}hrleisten und somit die Daten\"{u}bermittlung und den Datenaustausch zwischen den Systemen oder Netzen zu erm\"{o}glichen.} & A patent holder is entitled to block interoperability as much as he wants, as long as he doesn't violate antitrust law.  Patent law itself imposes no limits.  Moreover, even if anti-trust law is violated, it is very unlikely that there will be any effective legal remedy, given the complexity of the concerned procedures, as shown in the EU vs Microsoft case, where the Samba project's freedom to use Microsoft's standards was not assured.  This recital is a new repressive measure against software developpers and users taken by the Council in 2004.   The Commission had at least left room for interpretation here.  See also Article 6 and 6a. & {\bf Erw\"{a}gung 17\par

Die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 des Vertrags sollen durch diese Richtlinie unber\"{u}hrt bleiben.} & \\\hline
{\bf Artikel 1\par

Anwendungsbereich\par

Diese Richtlinie legt Vorschriften f\"{u}r die Patentierbarkeit computerimplementierter Erfindungen fest.} &  & {\bf} & \\\hline
{\bf Artikel 2a\par

a) ``Computerimplementierte Erfindung`` ist jede Erfindung, zu deren Ausf\"{u}hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.} & A ``computer-implemented invention'' is any process that runs under computer control, including general purpose data processing processes (= computer programs), mechanical processes, chemical processes and anything whatsoever if only the claimant choses to incorporate computer features in the claim.  Thus it is up to the patent attorney to decide whether the directive applies to his claims or not. & {\bf Artikel 2a\par

a) 'Computerimplementierte Erfindung' ist jede Erfindung im Sinne des Europ\"{a}ischen Patent\"{u}bereinkommens, zu deren Ausf\"{u}hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die in ihren Implementationen - au{\ss}er den technischen Merkmalen, die jede Erfindung beisteuern muss - mindestens ein nichttechnisches Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.} & A ``computer-implemented invention'' is a non-computing process (e.g. chemical process) that runs under computer control.  It might have been better to call this a ``computer-aided invention'' or ``computerised invention''.  Computers alone can ``implement'' only computing processes (= computer programs), which are not patentable inventions according to Art 52 EPC.\\\hline
{\bf Artikel 2b\par

b) ``Technischer Beitrag`` ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technologie, der neu und f\"{u}r eine fachkundige Person nicht nahe liegend ist. Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der technische Merkmale umfassen muss, die ihrerseits mit nichttechnischen Merkmalen versehen sein k\"{o}nnen, vom Stand der Technik abhebt.} & ``technical contribution'' is a contribution in a field of technology which is not obvious, but which can be entirely non-technical itself (the ``claim as a whole'' must contain technical features, but the technical contribution itself does not).  Thanks to the insertion of the word ``new'' upon request of the German delegation, the technical contribution seems to have become subject to a novelty test, just like the ``invention'', of which it was originally a synonym.  Thereby the Council seems to be departing from EPO practise and moving toward the Parliament's view.  However, the Council's ``technical contribution'' may consist only of non-technical features: it is enough if the ``claim as a whole'' contains technical features. & {\bf Artikel 2b\par

b) 'Technischer Beitrag', auch 'Erfindung' genannt, ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der f\"{u}r eine fachkundige Person nicht nahe liegend ist. Die Technizit\"{a}t des Beitrags ist eine von vier Voraussetzungen f\"{u}r die Patentierbarkeit. Zus\"{a}tzlich muss der technische Beitrag neu, nicht naheliegend und gewerblich anwendbar sein, damit ein Patent erteilt werden kann. Die Nutzung der Kr\"{a}fte der Natur zur Beherrschung der physikalischen Wirkungen \"{u}ber die numerische Darstellung der Informationen hinaus geh\"{o}rt zu einem Gebiet der Technik. Die Verarbeitung, die Bearbeitung und die Darstellungen von Informationen geh\"{o}ren nicht zu einem Gebiet der Technik, selbst wenn daf\"{u}r technische Vorrichtungen verwendet werden.} & ``technical contribution'' is the same as an invention: ``what you invent is your technical contribution to the state of the art''.  Thereby the Parliament clearly departs from the recent counter-intuitive usages of the EPO and returns to the traditional usage of the term.  Moreover, the Parliament explains the term ``technical'' both by definition and by a negative example, thereby also clarifying the application of Art 27 TRIPs in European patent law.  Moroever, the ``technical contribution'' must, according to the Parliament, consist solely of technical features, as explained in Art 4(3).\\\hline
{\bf} &  & {\bf Artikel 2ba\par

ba) 'Gebiet der Technik' ist ein gewerbliches Anwendungsgebiet, das zur Erreichung vorhersehbarer Ergebnisse der Nutzung kontrollierbarer Kr\"{a}fte der Natur bedarf. 'Technisch' bedeutet 'einem Gebiet der Technik zugeh\"{o}rig'.} & For the Parliament, the term ``technical'' means something.  For the Council it does not.  An alternative phrasing could have been ``technology = applied natural science''.\\\hline
{\bf} &  & {\bf Artikel 2bb\par

bb) 'Industrie' im Sinne des Patentrechts ist die automatisierte Herstellung materieller G\"{u}ter;} & A clarification of another key terms used in Art 27 TRIPs and in the directive proposal.  The Parliament returns to a traditional definition that is found implicitely and explicitely in various national patent laws in Europe and in Art 52(4) EPC, but has been largely forgotton or diluted at the EPO.\\\hline
{\bf Artikel 3\par

--- Gestrichen ---} & Leave the magic words from TRIPs undefined, so that they can continue to be used to push for extension of patentability. & {\bf Artikel 3a\par

Die Mitgliedstaaten stellen sicher, dass die Datenverarbeitung nicht als Gebiet der Technik im Sinne des Patentrechts betrachtet wird und dass Innovationen im Bereich der Datenverarbeitung nicht als Erfindungen im Sinne des Patentrechts betrachtet werden.} & Data processing has nothing to do with the machinery used for it.  The latter is patentable, the former not.  This is a clear and simple negative definition of a key term used in the TRIPs treaty\footnote{\url{http://localhost/swpat/stidi/trips/index.de.html}}.  It is achieved by a straightforward semantic transformation of Art 52(2) EPC.\\\hline
{\bf} & siehe Artikel 2b: The ``technical contribution'' may consist solely of non-technical features & {\bf Artikel 4(3)\par

Bei der Ermittlung des signifikanten Ausma{\ss}es des technischen Beitrags wird beurteilt, inwieweit sich alle technischen Merkmale, die der Gegenstand des Patentanspruchs in seiner Gesamtheit aufweist, vom Stand der Technik abheben, unabh\"{a}ngig davon, ob neben diesen Merkmalen nichttechnische Merkmale gegeben sind.} & The ``technical contribution'' must consist of technical features.\\\hline
{\bf} &  & {\bf Artikel 4.3a\par

(3a) Bei der Feststellung, ob eine gegebene computerimplementierte Erfindung einen technischen Beitrag leistet, wird gepr\"{u}ft, ob sie eine neue Lehre \"{u}ber die Beziehung zwischen Ursache und Wirkung in der Nutzung kontrollierbarer Kr\"{a}fte der Natur darstellt, und ob sie sowohl im Hinblick auf die Methode als auch auf das Ergebnis eine industrielle Anwendung im engen Sinne dieses Ausdrucks hat.} & Another definition of ``technical contribution'' aka ``invention'' on the basis of the terms ``technical'' and ``industrial'' with their traditional clearly delimiting meanings.\\\hline
{\bf Artikel 4.A\par

Ein Computerprogramm als solches kann keine patentierbare Erfindung darstellen.\par

Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausf\"{u}hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausf\"{u}hrung ein Computerprogramm, sei es als Quellcode, als Objektcode oder in anderer Form ausgedr\"{u}ckt, eingesetzt wird und durch die Gesch\"{a}ftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentf\"{a}hig, wenn sie \"{u}ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.} & This article is based on a JURI amendment (= amendment from the people who also run the Council's working party) that made it through the EP Plenary Session only because it is meaningless and therefore harmless.  The Council's additions however make this amendment harmful.  They use the wording of Art 52 EPC and insinuate that a ``program as such'' is only the ``expression'' side of this program, i.e. narrow copyright-style claims that nobody would write into a patent anyway.  This interpretation does not conform to the usual requirements of interpretation of laws and is not currently in use at the patent courts.  It deprives Art 52 EPC of all limiting meaning. & {\bf Artikel 4.A\par

Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausf\"{u}hrung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausf\"{u}hrung ein Computerprogramm eingesetzt wird und durch die Gesch\"{a}ftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentf\"{a}hig, wenn sie \"{u}ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.} & The EP's version was brought into JURI (Legal Affairs Committee) by Arlene McCarthy\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/amccarthy0302/index.de.html}} in spring of 2003.  Like most of the JURI amendments\footnote{\url{}}, it was inspired and probably drafted by circle around the Council's Patent Working Party\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/dkpto0209/index.de.html}}.  Unlike many of the JURI amendments, this one made its way into the final plenary decision of September 2003.  This is because it is unclear and therefore harmless in a context where it co-exists with clearer provisions.  Nobody except perhaps some authoritative EPO Board of Appeal knows what ``normal physical interaction between a program and a computer'' could be.  The term means as much as ``normal physical interaction between a recipe and a cook'': nothing or whatever the EPO may want it to mean.\\\hline
{\bf} &  & {\bf Artikel 4.B\par

Die Mitgliedsstaaten sorgen daf\"{u}r, dass computerimplementierte L\"{o}sungen technischer Probleme nicht allein deshalb als patentf\"{a}hige Erfindungen angesehen werden, weil sie Einsparungen von Ressourcen innerhalb eines Datenverarbeitungssystems erm\"{o}glichen.} & Another negative definition of ``technical contribution'' that really excludes something.  It is equivalent to Art 3a and based on the German Federal Patent Court's Error Search decision of 2002\footnote{\url{http://localhost/swpat/papiere/bpatg17-suche02/index.de.html}} which held that any business method would become patentable if the court were to follow the EPO's doctrine of viewing ``improvements in computing efficiency'' as technical contributions.\\\hline
{\bf Artikel 5(1)\par

Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausf\"{u}hrung von Software verwirklicht wird.} & The article implies that a claim to a computer running software or to a computing process running on general purpose computing equipment (= a computer program) is permissible. & {\bf Artikel 5(1)\par

Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung nur entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um eine programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein technisches Produktionsverfahren handelt.} & FFII's proposal for this had been:
Die Mitgliedsstaaten sorgen daf\"{u}r, dass eine computerisierte Erfindunge als ein Erzeugnis, d.h. ein Satz von an eine Datenverarbeitungsanlage angeschlossenen Ger\"{a}ten, oder als ein von solchen Ger\"{a}ten ausgef\"{u}hrtes Verfahren beansprucht werden kann.\\\hline
{\bf Artikel 5(2)\par

Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datentr\"{a}ger vorliegendes Programm, ist nur zul\"{a}ssig, insoweit das Programm, wenn es auf einem Computer, auf einem programmierten Computernetz oder einer sonstigen programmierbaren Vorrichtung installiert und ausgef\"{u}hrt wird, einen in derselben Patentanmeldung erhobenen Erzeugnis- oder Verfahrensanspruch gem\"{a}{\ss} Absatz 1 begr\"{u}ndet.} & You can get a monopoly on a certain kind of computer program on its own ``only'' if you also asked for a monopoly on this kind of computer program executing on a computer.  The practical result is that not only the usage of such computer programs, but also their publication and distribution is forbidden.  Together with the Council's Art 4A, this provision reinterprets Art 52 EPC to make it meaningless: only a narrow claim to a particular textual expression of a program is construed to be directed to that program ``as such'', whereas a broad claim to a whole class of ``programs on their own'' is not directed to these programs as such. & {\bf Artikel 5.1b\par

(1b) Die Mitgliedstaaten stellen sicher, dass die Erstellung, die Bearbeitung, die Verarbeitung, die Verbreitung und die Ver\"{o}ffentlichung von Informationen in jedweder Form niemals eine direkte oder indirekte Patentverletzung darstellen k\"{o}nnen, selbst wenn daf\"{u}r technische Vorrichtungen verwendet werden.} & Program claims are not permissible, nor is it permissible to obtain a monopoly on publication and distribution by other forms of claims.  The Parliament concretises Art 10 of the European Convention on Human Rights, which mandates freedom of publication.  During the JURI vote of 2003-06-17, this amendment was not put to vote because the equivalent of the Council's Art 5(2) had already been approved, and these two were considered mutually exclusive by the rapporteur Arlene McCarthy.  However the Parliament's plenary session scrapped Art 5(2) and voted for 5.1b instead.\\\hline
{\bf} & No objections against the EPO's practise of granting patents on problem specifications where not even one implementation example is disclosed in a verifiable way. & {\bf Artikel 5.1d\par

(1d) Die Mitgliedstaaten stellen sicher, dass in allen F\"{a}llen, in denen in einem Patentanspruch Merkmale genannt sind, die die Verwendung eines Computerprogramms erfordern, eine gut funktionierende und gut dokumentierte Referenzimplementierung eines solchen Programms als Teil der Beschreibung ohne einschr\"{a}nkende Lizenzbedingungen ver\"{o}ffentlicht wird.} & The patent system should take its disclosure function seriously.  If patents are a ``monopoly in return for disclosure'' deal, then programming logic is on the disclosure side of that deal.\\\hline
{\bf Artikel 6\par

Rechte, die aus Patenten erwachsen, die f\"{u}r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, ber\"{u}hren nicht die urheberrechtlich zul\"{a}ssigen Handlungen gem\"{a}{\ss} Artikel 5 und 6 der Richtlinie 91/250/EWG \"{u}ber den Rechtsschutz von Computerprogrammen, insbesondere hinsichtlich der Bestimmungen \"{u}ber die Dekompilierung und die Interoperabilit\"{a}t.} & entirely impractical and only exeptional restriction of patent priviledges. Reverse engineering and decompilation may not be forbidden by using patents (this is a tautology: patents simply cannot be used to forbid those things, they fall entirely under copyright law: patents are about usage of something, not about studying it). & {\bf Artikel 6\par

Rechte, die aus Patenten erwachsen, die f\"{u}r Erfindungen im Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unber\"{u}hrt von urheberrechtlich zul\"{a}ssigen Handlungen gem\"{a}{\ss} Artikel 5 und 6 der Richtlinie 91/250/EWG \"{u}ber den Rechtsschutz von Computerprogrammen, insbesondere hinsichtlich der Bestimmungen \"{u}ber die Dekompilierung und die Interoperabilit\"{a}t.} & The Parliament also left this meaningless text from the Commission's proposal untouched but supplemented it with Art 6a, which the Council rejects.\\\hline
{\bf} & Use of patented techniques for interoperation is a patent infringement.  Exceptions for the benefit of interoperability can be made only in rare cases after lengthy and unpracticable legal procedings.  See also Recital 17. & {\bf Artikel 6a\par

Die Mitgliedstaaten stellen sicher, dass in allen F\"{a}llen, in denen der Einsatz einer patentierten Technik f\"{u}r einen bedeutsamen Zweck wie die Konvertierung der in zwei verschiedenen Computersystemen oder -netzen verwendeten Konventionen ben\"{o}tigt wird, um die Kommunikation und den Austausch von Dateninhalten zwischen ihnen zu erm\"{o}glichen, diese Verwendung nicht als Patentverletzung gilt.} & The right to interoperate limits the right of the patent holder to enforce his patent.  As long as software patents stay invalid, this clause should never need to be used, but it is a useful clarification of Art 30 TRIPs nevertheless.\\\hline
\end{longtable}
\end{center}
\end{center}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf letzte \"{o}ffentliche Version auf Rats-Website\footnote{\url{http://register.consilium.eu.int/pdf/en/04/st09/st09713.en04.pdf}}}}
\filbreak

\item
{\bf {\bf The amendments approved, from the EP.\footnote{\url{http://www3.europarl.eu.int/omk/omnsapir.so/pv2?PRG=DOCPV&APP=PV2&LANGUE=EN&SDOCTA=2&TXTLST=1&POS=1&Type_Doc=RESOL&TPV=PROV&DATE=240903&PrgPrev=PRG@TITRE|APP@PV2|TYPEF@TITRE|YEAR@03|Find@\%2a\%69\%6e\%76\%65\%6e\%74\%69\%6f\%6e\%73|FILE@BIBLIO03|PLAGE@1&TYPEF=TITRE&NUMB=1&DATEF=030924}}}}

\begin{quote}
consolidated version by EP, changes were still being applied by the secretariat during the days after the vote.  E.g. ``contribute'' in ``technical features that any invention must contribute'' was changed to ``posess''.
\end{quote}
\filbreak

\item
{\bf {\bf EU Rat 2004 Vorschlag zu Softwarepatenten\footnote{\url{http://localhost/swpat/papiere/europarl0309/cons0401/index.de.html}}}}

\begin{quote}
Der Ministerrat der EU hat sich auf ein Papier geeinigt, welches Gegenvorschl\"{a}ge zu den \"{A}nderungsvorschl\"{a}gen des Europ\"{a}ischen Parlamentes zur Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'' enth\"{a}lt.  Im Gegensatz zur Version des Europ\"{a}ischen Parlamentes erlaubt die Version des Rates grenzenlose Patentierbarkeit und Patent-Durchsetzbarkeit.  Gem\"{a}{\ss} der Version des Rates gelten ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmethoden von vorneherein als Erfindungen im Sinne des Patentrechts.   Schon die Ver\"{o}ffentlichung einer funktionsf\"{a}higen Beschreibung einer patentierten Idee stellt laut Ratsvorschlag eine Patentverletzung dar.  Protokolle und Dateiformate k\"{o}nnen direkt patentiert werden und d\"{u}rfen dann ohne eine Lizenz des Patentinhabers nicht verwendet werden.  F\"{u}r den unge\"{u}bten Leser sind diese Implikationen nicht unbedingt sofort ersichtlich.  Wir erkl\"{a}ren, woraus sie sich ergeben.
\end{quote}
\filbreak

\item
{\bf {\bf Scheinbegrenzungen der Patentierbarkeit im Ratsentwurf\footnote{\url{http://localhost/swpat/briefe/cons0406/text/index.de.html}}}}

\begin{quote}
Wir versuchen einen kurzen \"{U}berblick \"{u}ber die T\"{a}uschungstaktiken des Ratsentwurfs zur Software-Patentierung vom 18.05.2004 zu geben.
\end{quote}
\filbreak

\item
{\bf {\bf Dringender Aufruf an nationale Regierungen und Parlamente\footnote{\url{http://localhost/swpat/briefe/cons0406/index.de.html}}}}

\begin{quote}
Die Regierungen Europas sind dabei, ihre Unterschrift unter einen Richtlinienentwurf zu setzen, der uneingeschr\"{a}nkte Patentierbarkeit und Patentdurchsetzbarkeit auf ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmethoden erlaubt. Die Einigung im Ministerrat vom 18. Mai 2004 verwirft dabei ohne jede Rechtfertigung und auf demokratisch nicht legitimierte Weise wohlbedachte Entscheidungen des Europ\"{a}ischen Parlaments und der beratenden EU-Organe. Die Stimmenmehrheit auf der entscheidenden Sitzung wurde stattdessen durch irref\"{u}hrende Formulierungen und fragw\"{u}rdige diplomatische Winkelz\"{u}ge gesichert. Die Unterzeichnenden, f\"{u}hrend im Bereich von Softwareinnovation und der Diskussion \"{u}ber deren Sicherung im rechtlichen Rahmen Europas, fordern von den verantwortlichen Politikern, die Notbremse zu ziehen und den Prozess der Gesetzgebung im Wettbewerbsrecht zu reorganisieren.
\end{quote}
\filbreak

\item
{\bf {\bf Rat der EU will Parlamentsbeschluss ohne Diskussion verwerfen\footnote{\url{http://localhost/swpat/log/04/cons0507/index.de.html}}}}

\begin{quote}
Der Ministerrat der EU demonstriert zur Zeit, dass das Konzept der Demokratie der EU fremd ist.  An diesem Mittwoch setzte die irische Ratspr\"{a}sidentschaft zusammen mit der Europ\"{a}ischen Kommission einen Gegenvorschlag zur Softwarepatent-Richtlinie durch, wobei nur wenige L\"{a}nder, darunter Belgien und Deutschland, Widerstand zeigten. Der neue Text schl\"{a}gt vor, alle begrenzenden \"{A}nderungen, die das Europ\"{a}ische Parlament beschlossen hat, r\"{u}ckg\"{a}ngig zu machen. Stattdessen wird die aufweichende Formulierung des urspr\"{u}nglichen Kommissionsentwurf wiederhertestellt und direkte Patentierbarkeit von Datenstrukturen und Prozessbeschreibungen als Sahneh\"{a}ubchen obendrauf gesetzt.  Der Vorschlag ist nun zur Best\"{a}tigung ohne weitere Diskussion beim Ministerratstreffen am 17.-18. Mai vorgesehen, sofern keiner der Mitgliedstaaten seine Entscheidung \"{a}ndert.  Mit einer gerade in Wahlkampfzeiten bemerkenswerten Einigkeit verurteilen Mitglieder des Europ\"{a}ischen Parlaments aus allen politischen Parteien diese unverhohlene Missachtung der Demokratie in Europa.
\end{quote}
\filbreak

\item
{\bf {\bf Warum Amazon One Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/tech/index.en.html}}}}

\begin{quote}
Gem\"{a}ss des Richtlinienvorschlags COM(2002)92 der Europ\"{a}ischen Kommission (CEC) f\"{u}r ``Patentierbarkeit Computer-Implementierter Erfindungen'' und die \"{u}berarbeitete Version genehmigt durch das Komitee f\"{u}r Rechtsangelegenheiten und Binnenmarkt (JURI) des Europa Parlaments, sind Algorithmem und Gesch\"{a}ftmethoden, wie etwa Amazon One Click Shopping, ohne Zweifel als patentierbare Gegenst\"{a}nde zu betrachten. Die ist so weil \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO). The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Was ist eine Computerimplementierte Erfindung?\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/kinv/index.de.html}}}}

\begin{quote}
Eine Waschmaschine mit eingebettetem Rechner?  Oder ein allgemein einsetzbares Datenverarbeitungsprogramm?  In der \"{o}ffentlichen Debatte sagen die Bef\"{u}rworter des EU Softwarepatent-Richtlinienvorschlags, dass sie keine Patente auf ``reine Software'' wollen, sondern nur auf ``Computerimplementierte Erfindungen'', womit sie, wie sie sagen, ``Waschmaschinen und Mobiltelefone'' meinen.  Art 2 des Vorschlags widerspricht dem jedoch, ebenso wie die Geschichte und die Praxis des EPA, Patente auf ``Computerimplementierte Erfindungen'' zu erteilen.
\end{quote}
\filbreak

\item
{\bf {\bf Interoperabilit\"{a}t und die Softwarepatent-Richtlinie: Welche Schranken sind Erforderlich?\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/itop/index.en.html}}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  After the summer pause of 2003, Arlene McCarthy MEP proposed an amendment to Art 6a which would render Art 6a meaningless.  The movement against Art 6a was joined by Wuermeling (EPP), Manders (ELDR) as well as the governments of UK and US.  Yet explanations as to what is wrong with the Interoperability Privilege remain very vague.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf Programmanspr\"{u}che: Verbote der Ver\"{o}ffentlichung N\"{u}tzlicher Patentbeschreibungen\footnote{\url{http://localhost/swpat/papiere/eubsa-swpat0202/prog/index.de.html}}}}

\begin{quote}
Patentanspr\"{u}che auf ein ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', werden genannt ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' oder ``information claims''. Patente die diesen Anspruch enthalten werden manchmal auch ``text patents'' oder ``information patents'' genannt. Solche Patente monopolisieren nicht l\"{a}nger ein physikalisches Objekt sondern eine Beschreibung eines solchen Objekts.  Ob dies erlaubt werden sollte ist eine der kontroversen Fragen in der Auseinandersetzung um die vorgeschlagene EU Software Patent Richtlinie. Wir versuchten zu erkl\"{a}ren wie diese Debatte aufkam und was wirklich auf dem Spiel steht.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{\url{http://localhost/swpat/papiere/europarl0309/index.de.html}}}}

\begin{quote}
Konsolidierte Version der wesentlichen Bestimmungen (Art 1-6) der ge\"{a}nderten Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'', f\"{u}r die das Europ\"{a}ische Parlament am 24. September 2003 gestimmt hat.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/cons0401.el ;
% mode: latex ;
% End: ;

