\begin{subdocument}{constab0401}{EU Software Patent Directive: Parliament's vs Council's Version}{http://localhost/swpat/papers/europarl0309/cons0401/tab/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\translated by 2004/10/21}{In September 2003 the European Parliament amended a proposal from the European Commission for patentability of software in such a way that it became a proposal that clearly disallows software patents.   The EU Council of Ministers and the European Commission subsequently disregarded the European Parliament's Amendments and reached ``political agreement'' in May 2004 in favor of a text that goes even further than the previous text of the Commission in imposing US-style unlimited patentability, but shrouds this in a veil of convoluted wording, characterised by multiple negations and other misleading syntactic devices.  Below you can find out yourself by carefully reading the core provisions of the proposed EU directive ``on the patentability of computer-implemented inventions'' which we have collected in a table, together with short explanations of the implications.  At the end we add pointers to the originals from the Parliament and Council websites as well as to related analyses and news reports.}
\begin{sect}{tab}{table}
\begin{center}
\begin{center}
\begin{longtable}{|C{22}|C{22}|C{22}|C{22}|}
\hline
{\bf CEC 2004-05-18} & Implications & {\bf European Parliament 2003-09-24} & Implications\\\hline
\endhead
{\bf Recital 17\par

The provisions of this Directive are without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them.} & A patent holder is entitled to block interoperability as much as he wants, as long as he doesn't violate antitrust law.  Patent law itself imposes no limits.  Moreover, even if anti-trust law is violated, it is very unlikely that there will be any effective legal remedy, given the complexity of the concerned procedures, as shown in the EU vs Microsoft case, where the Samba project's freedom to use Microsoft's standards was not assured.  This recital is a new repressive measure against software developpers and users taken by the Council in 2004.   The Commission had at least left room for interpretation here.  See also Article 6 and 6a. & {\bf Recital 17\par

This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.} & \\\hline
{\bf Article 1\par

Scope\par

This Directive lays down rules for the patentability of computer-implemented inventions.} &  & {\bf} & \\\hline
{\bf Article 2a\par

(a) ``computer-implemented invention`` means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;} & A ``computer-implemented invention'' is any process that runs under computer control, including general purpose data processing processes (= computer programs), mechanical processes, chemical processes and anything whatsoever if only the claimant choses to incorporate computer features in the claim.  Thus it is up to the patent attorney to decide whether the directive applies to his claims or not. & {\bf Article 2a\par

'computer-implemented invention' means any invention within the meaning of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;} & A ``computer-implemented invention'' is a non-computing process (e.g. chemical process) that runs under computer control.  It might have been better to call this a ``computer-aided invention'' or ``computerised invention''.  Computers alone can ``implement'' only computing processes (= computer programs), which are not patentable inventions according to Art 52 EPC.\\\hline
{\bf Article 2b\par

(b) ``technical contribution`` means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features.} & ``technical contribution'' is a contribution in a field of technology which is not obvious, but which can be entirely non-technical itself (the ``claim as a whole'' must contain technical features, but the technical contribution itself does not).  Thanks to the insertion of the word ``new'' upon request of the German delegation, the technical contribution seems to have become subject to a novelty test, just like the ``invention'', of which it was originally a synonym.  Thereby the Council seems to be departing from EPO practise and moving toward the Parliament's view.  However, the Council's ``technical contribution'' may consist only of non-technical features: it is enough if the ``claim as a whole'' contains technical features. & {\bf Article 2b\par

``technical contribution'', also called ``invention'', means a contribution to the state of the art in a technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.} & ``technical contribution'' is the same as an invention: ``what you invent is your technical contribution to the state of the art''.  Thereby the Parliament clearly departs from the recent counter-intuitive usages of the EPO and returns to the traditional usage of the term.  Moreover, the Parliament explains the term ``technical'' both by definition and by a negative example, thereby also clarifying the application of Art 27 TRIPs in European patent law.  Moroever, the ``technical contribution'' must, according to the Parliament, consist solely of technical features, as explained in Art 4(3).\\\hline
{\bf} &  & {\bf Article 2ba\par

(ba) 'technical field' means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. 'Technical' means 'belonging to a technical field'.} & For the Parliament, the term ``technical'' means something.  For the Council it does not.  An alternative phrasing could have been ``technology = applied natural science''.\\\hline
{\bf} &  & {\bf Article 2bb\par

'industry' within the meaning of patent law means the automated production of material goods;} & A clarification of another key terms used in Art 27 TRIPs and in the directive proposal.  The Parliament returns to a traditional definition that is found implicitely and explicitely in various national patent laws in Europe and in Art 52(4) EPC, but has been largely forgotton or diluted at the EPO.\\\hline
{\bf Article 3\par

- Deleted -} & Leave the magic words from TRIPs undefined, so that they can continue to be used to push for extension of patentability. & {\bf Article 3a\par

Member States shall ensure that data processing is not considered to be a field of technology within the meaning of patent law, and that innovations in the field of data processing are not considered to be inventions within the meaning of patent law.} & Data processing has nothing to do with the machinery used for it.  The latter is patentable, the former not.  This is a clear and simple negative definition of a key term used in the TRIPs treaty\footnote{\url{http://localhost/swpat/analysis/trips/index.en.html}}.  It is achieved by a straightforward semantic transformation of Art 52(2) EPC.\\\hline
{\bf} & Article 2b ff.: The ``technical contribution'' may consist solely of non-technical features & {\bf Article 4(3)\par

The significant extent of the technical contribution shall be assessed by consideration of the difference between all of the technical features included in the scope of the patent claim considered as a whole and the state of the art, irrespective of whether or not such features are accompanied by non-technical features.} & The ``technical contribution'' must consist of technical features.\\\hline
{\bf} &  & {\bf Article 4.3a\par

3a. In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of nature and has an industrial application in the strict sense of the expression, in terms of both method and result.} & Another definition of ``technical contribution'' aka ``invention'' on the basis of the terms ``technical'' and ``industrial'' with their traditional clearly delimiting meanings.\\\hline
{\bf Article 4.A\par

A computer program as such cannot constitute a patentable invention.\par

A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.} & This article is based on a JURI amendment (= amendment from the people who also run the Council's working party) that made it through the EP Plenary Session only because it is meaningless and therefore harmless.  The Council's additions however make this amendment harmful.  They use the wording of Art 52 EPC and insinuate that a ``program as such'' is only the ``expression'' side of this program, i.e. narrow copyright-style claims that nobody would write into a patent anyway.  This interpretation does not conform to the usual requirements of interpretation of laws and is not currently in use at the patent courts.  It deprives Art 52 EPC of all limiting meaning. & {\bf Article 4.A\par

A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.} & The EP's version was brought into JURI (Legal Affairs Committee) by Arlene McCarthy\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/amccarthy0302/index.en.html}} in spring of 2003.  Like most of the JURI amendments\footnote{\url{}}, it was inspired and probably drafted by circle around the Council's Patent Working Party\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/dkpto0209/index.en.html}}.  Unlike many of the JURI amendments, this one made its way into the final plenary decision of September 2003.  This is because it is unclear and therefore harmless in a context where it co-exists with clearer provisions.  Nobody except perhaps some authoritative EPO Board of Appeal knows what ``normal physical interaction between a program and a computer'' could be.  The term means as much as ``normal physical interaction between a recipe and a cook'': nothing or whatever the EPO may want it to mean.\\\hline
{\bf} &  & {\bf Article 4.B\par

Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.} & Another negative definition of ``technical contribution'' that really excludes something.  It is equivalent to Art 3a and based on the German Federal Patent Court's Error Search decision of 2002\footnote{\url{http://localhost/swpat/papers/bpatg17-suche02/index.de.html}} which held that any business method would become patentable if the court were to follow the EPO's doctrine of viewing ``improvements in computing efficiency'' as technical contributions.\\\hline
{\bf Article 5(1)\par

Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.} & The article implies that a claim to a computer running software or to a computing process running on general purpose computing equipment (= a computer program) is permissible. & {\bf Article 5(1)\par

Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.} & FFII's proposal for this had been:
Member States shall ensure that a computerised invention may be claimed as a product, that is a set of devices connected to a data processing system, or as a process carried out by such devices.\\\hline
{\bf Article 5(2)\par

A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1.} & You can get a monopoly on a certain kind of computer program on its own ``only'' if you also asked for a monopoly on this kind of computer program executing on a computer.  The practical result is that not only the usage of such computer programs, but also their publication and distribution is forbidden.  Together with the Council's Art 4A, this provision reinterprets Art 52 EPC to make it meaningless: only a narrow claim to a particular textual expression of a program is construed to be directed to that program ``as such'', whereas a broad claim to a whole class of ``programs on their own'' is not directed to these programs as such. & {\bf Article 5.1b\par

1b. Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.} & Program claims are not permissible, nor is it permissible to obtain a monopoly on publication and distribution by other forms of claims.  The Parliament concretises Art 10 of the European Convention on Human Rights, which mandates freedom of publication.  During the JURI vote of 2003-06-17, this amendment was not put to vote because the equivalent of the Council's Art 5(2) had already been approved, and these two were considered mutually exclusive by the rapporteur Arlene McCarthy.  However the Parliament's plenary session scrapped Art 5(2) and voted for 5.1b instead.\\\hline
{\bf} & No objections against the EPO's practise of granting patents on problem specifications where not even one implementation example is disclosed in a verifiable way. & {\bf Article 5.1d\par

1d. Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program shall be published as a part of description without any restricting licensing terms.} & The patent system should take its disclosure function seriously.  If patents are a ``monopoly in return for disclosure'' deal, then programming logic is on the disclosure side of that deal.\\\hline
{\bf Article 6\par

The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.} & entirely impractical and only exeptional restriction of patent priviledges. Reverse engineering and decompilation may not be forbidden by using patents (this is a tautology: patents simply cannot be used to forbid those things, they fall entirely under copyright law: patents are about usage of something, not about studying it). & {\bf Article 6\par

The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.} & The Parliament also left this meaningless text from the Commission's proposal untouched but supplemented it with Art 6a, which the Council rejects.\\\hline
{\bf} & Use of patented techniques for interoperation is a patent infringement.  Exceptions for the benefit of interoperability can be made only in rare cases after lengthy and unpracticable legal procedings.  See also Recital 17. & {\bf Article 6a\par

Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.} & The right to interoperate limits the right of the patent holder to enforce his patent.  As long as software patents stay invalid, this clause should never need to be used, but it is a useful clarification of Art 30 TRIPs nevertheless.\\\hline
\end{longtable}
\end{center}
\end{center}
\end{sect}

\begin{sect}{links}{local copy}
\begin{itemize}
\item
{\bf {\bf latest public version on Council website\footnote{\url{http://register.consilium.eu.int/pdf/en/04/st09/st09713.en04.pdf}}}}
\filbreak

\item
{\bf {\bf The amendments approved, from the EP.\footnote{\url{http://www3.europarl.eu.int/omk/omnsapir.so/pv2?PRG=DOCPV&APP=PV2&LANGUE=EN&SDOCTA=2&TXTLST=1&POS=1&Type_Doc=RESOL&TPV=PROV&DATE=240903&PrgPrev=PRG@TITRE|APP@PV2|TYPEF@TITRE|YEAR@03|Find@\%2a\%69\%6e\%76\%65\%6e\%74\%69\%6f\%6e\%73|FILE@BIBLIO03|PLAGE@1&TYPEF=TITRE&NUMB=1&DATEF=030924}}}}

\begin{quote}
consolidated version by EP, changes were still being applied by the secretariat during the days after the vote.  E.g. ``contribute'' in ``technical features that any invention must contribute'' was changed to ``posess''.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council 2004 Proposal on Software Patents\footnote{\url{http://localhost/swpat/papers/europarl0309/cons0401/index.en.html}}}}

\begin{quote}
The Council of Ministers has reached political agreement on a paper which contains alternative suggestions to the amendments on the directive ``on the patentability of computer-implemented inventions'' passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, ``computer-implemented'' algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.
\end{quote}
\filbreak

\item
{\bf {\bf Fake Limits on Patentability in the Council Proposal\footnote{\url{http://localhost/swpat/letters/cons0406/text/index.en.html}}}}

\begin{quote}
We try to produce a short overview of the deceptive tricks in the Council's software patent proposal of 2004-05-18.
\end{quote}
\filbreak

\item
{\bf {\bf Urgent Call to National Governments and Parliaments\footnote{\url{http://localhost/swpat/letters/cons0406/index.en.html}}}}

\begin{quote}
Europe's governments are about to put their stamps under a directive proposal for unlimited patentability and unfettered patent enforcement of ``computer-implemented'' algorithms and business methods.  The agreement by the Council of Ministers of 2004-05-18 discards well-deliberated decisions of the European Parliament and the EU's consultative organs without any justification and without democratic legitimation.  The majority was secured by deceptive packaging and by questionable diplomatic maneuvering at the decisive session.  The undersigned, who represent the leaders of software innovation and informed discussion on software innovation policy in Europe, ask the responsible politicians to pull the emergency brake and to reorganise the process of competitivity legislation in the Council.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council Plans to Scrap Parliamentary Vote without Discussion\footnote{\url{http://localhost/swpat/log/04/cons0507/index.en.html}}}}

\begin{quote}
The EU Council of Ministers is demonstrating that the concept of democracy is alien to the EU.  This Wednesday, the Irish Presidency and the European Commission managed to secure support for a counter-proposal on the software patents directive, with only a few delegations - including Belgium and Germany - showing resistance. The new text proposes to discard all the amendments from the European Parliament which limit patentability.  Instead the lax language of the original Commission proposal is to be reinstated in its entirety, with direct patentability of computer programs, data structures and process descriptions added as icing on the cake. The proposal is now scheduled to be confirmed without discussion at a meeting of ministers on 17-18 May, unless one of the Member States changes its vote.  In a remarkable sign of unity in times of imminent elections, members of the European Parliament from all groups across the political spectrum are condemning this blatant disrespect for democracy in Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/tech/index.en.html}}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf What is a Computer-Implemented Invention?\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/kinv/index.en.html}}}}

\begin{quote}
A washing machine with an embedded computer?  Or a general-purpose data processing program?  In their public discourse, the promoters of the EU software patent directive proposal say they do not want patents on ``pure software'' but only on ``computer-implemented inventions'', by which they say they mean ``washing machines and mobile phones''.  Art 2 of the proposal itself however says otherwise, as does the history and practise of granting patents for ``computer-implemented inventions'' at the EPO.
\end{quote}
\filbreak

\item
{\bf {\bf Interoperability and the Software Patents Directive: What Degree of Exemption is Needed\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/itop/index.en.html}}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  After the summer pause of 2003, Arlene McCarthy MEP proposed an amendment to Art 6a which would render Art 6a meaningless.  The movement against Art 6a was joined by Wuermeling (EPP), Manders (ELDR) as well as the governments of UK and US.  Yet explanations as to what is wrong with the Interoperability Privilege remain very vague.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf Program Claims: Bans on Publication of Patent Descriptions\footnote{\url{http://localhost/swpat/papers/eubsa-swpat0202/prog/index.en.html}}}}

\begin{quote}
Patent Claims to ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', are called ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' or ``information claims''.  Patents which contain these claims are sometimes called ``text patents'' or ``information patents''.  Such patents no longer monopolise a physical object but a description of such an object.  Whether this should be allowed is one of the controversial questions in the struggle about the proposed EU Software Patent Directive.  We try to explain how this debate emerged and what is really at stake.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{\url{http://localhost/swpat/papers/europarl0309/index.en.html}}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/cons0401.el ;
% mode: latex ;
% End: ;

