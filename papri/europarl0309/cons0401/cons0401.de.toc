\contentsline {section}{\numberline {1}Kollision statt Kompromiss}{2}{section.1}
\contentsline {section}{\numberline {2}Programm-Anspr\"{u}che: alle T\"{u}ren zugeschlagen}{3}{section.2}
\contentsline {section}{\numberline {3}Programmtext-Ver\"{o}ffentlichung: Ein weiterer Schritt weg vom Parlament}{7}{section.3}
\contentsline {section}{\numberline {4}Interoperabilit\"{a}tsprivileg gestrichen, Grenzenlose Durchsetzbarkeit gesichert}{8}{section.4}
\contentsline {section}{\numberline {5}Ersatzlose Streichung der Definitionen von ``Technik'', ``Industrie'', ``Erfindung''}{10}{section.5}
\contentsline {section}{\numberline {6}Systemwidrige Vermengung von Technizit\"{a}t und Erfindungsh\"{o}he}{12}{section.6}
\contentsline {section}{\numberline {7}Anwendungsgebiet der Richtlinie auf beliebige Verfahrenserfindungen ausgeweitet}{13}{section.7}
\contentsline {section}{\numberline {8}Scheinbegrenzungen, Irref\"{u}hrende Schachtels\"{a}tze}{15}{section.8}
\contentsline {section}{\numberline {9}Mutwillige Zerst\"{o}rung des EU-Richtlinienprojektes?}{16}{section.9}
\contentsline {section}{\numberline {10}Kommentierte Verweise}{16}{section.10}
