<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Council 2004 Proposal on Software Patents

#descr: The Council of Ministers has reached political agreement on a paper
which contains alternative suggestions to the amendments on the
directive %(q:on the patentability of computer-implemented inventions)
passed by the European Parliament (EP). In contrast to the EP version,
the council version permits unlimited patentability and patent
enforceability. Following the current version,
%(q:computer-implemented) algorithms and business methods would be
inventions in the sense of patent law, and the publication of a
functional description of a patented idea would constitute a patent
infringement. Protocols and data formats could be patented and would
then not be freely usable even for interoperability purposes. These
implications might not be apparent to the casual reader.  Here we try
to decipher the misleading language of the proposal and explain its
implications.

#ltp: Collision instead of Compromise

#kWr: Program Publication Not Allowed, All Bridges Torn Down

#eiW: No Program Disclosure: Another Step Away from the Parliament

#bea: Interoperation with Patented Standards Not Allowed

#tng: Patentability Limited by Meaningless Terms, Definitions Removed

#tef: Technicity Mingled with Non-Obviousness

#edj2: Scope of Directive Enlarged to Cover Any Process Invention

#bra: Fake Limits, Convoluted Ambiguities

#ien: Deliberate Destruction of EU Directive Project?

#ruo: The %(q:working document) from the Irish Presidendy reflects the state
of negotiations at the level of the %(q:Council's Working Party on
%(tp|Intellectual Property|Patents)), below abbreviated as %(q:Patent
Working Party) or %(q:Party), achieved at their last meeting in
Brussels on 2003/11/27.   Apart from a few wording details, this
document is identical with the Party's document of November 2002.  Of
all proposals known so far, the Party's drafts embody the most
far-reaching endorsement of unlimited patentability.  However the
radicality is often hidden in subclauses and enveloped in empty
limitation rhetoric, so that unexperienced and unattentive readers may
easily overlook it.

#utd: A few wordings from the %(ju:report of the Legal Affairs Committee
JURI of June 2003) have been incorporated, which only reinforce the
general line of unlimited patentability .  The JURI report itself was
based on the %(dk:Council working party paper of November 2002) and
largely ignored the recommendations of the %(ci:other two involved
committees, CULT and ITRE).  The Parliament's plenary, on the
contrary, decided to follow CULT and ITRE in limiting patentability
and to rebuke JURI (and thereby the Council Working Party) as far as
necessary.  In the Plenary Vote of 2004/09/24, the MEPs replaced fake
limits with real limits.  JURI wordings were retained wherever they
did not make real limitation impossible.  Now the Council Working
Party again sides with its ally, JURI, and ignores the amendments of
the European Parliament's plenary and of CULT and ITRE.  Moreover,
some of the more patent-critical positions of individual Party members
of November 2002, which resemble positions voted for by the Parliament
in September 2003, have been deleted in the Party's new version.  To
call this version a %(q:compromise paper), as the Irish Presidency has
done, is therefore highly misleading.  The previous %(q:compromise
paper) of 2002 already failed to disclose the interests between which
a compromise was allegedly found.  This time again there is no
disclosure of the compromise parties and their interests, but since
the highlighted changes to the previous version all come from the
European Parliament, at first sight it may seem that this paper
intends to strike a compromise between Council and Parliament.  Upon
closer look however it appears that the Council is being moved away
from the Parliament, not toward it.

#nie: In the following we will make this clear by comparing the Council
proposals concerning some of the central disputed questions with those
of the Parliament.

#gsW: Program Claims are patent claims of the form

#ben: computer program, upon whose loading into the memory of a computer
system the computation process according to Claim N is executed.

#oWs: The wording %(q:computer program) in the above can be replaced by
%(q:program on disk), %(q:computer program product), %(q:data
structure), %(q:computer readable medium) or other variants.  The
point is that the object of the claim is no longer a material product
or process but a construct consisting of data, information or other
abstract entities, optionally in combination with an unspecified and
non-inventive carrier medium.

#Wtt: This type of claim was introduced in Europe in 1998 by two decisions
of the Technical Boards of Appeal of the European Patent Office.  In
the USA too it was introduced by the Patent Office only recently:
after the hearings of 1994 (which signalled overwhelming opposition to
software patents).  Program claims are controversial, because

#dyr: Program claims break with the traditional systematics of patent law,
according to which physical objects are claimed in return for
disclosure of information objects, i.e. information is on the
disclosure side, not on the monopoly side of the patent deal;

#tvW: Program claims literally cover programs for computers as such and thus
can hardly be justified under any meaningful interpretation of Art 52
EPC;

#nce: Program claims literally run counter to the freedom of publication as
stipulated in Art 10 of the European Convention on Human Rights (ECHR)

#r1s: Patent monopolies are not among those rights that can, according to
Art 10(2) ECHR, justify a limitation of the freedom of publication

#reo: Program claims directly subject developpers and distributors of
software as well as %(tp:intermediaries: e.g., Internet service
providers) to the heavy weapons of patent litigation, by treating them
as producers and distributors of industrial products.

#cgd: Program claims do not serve a practical purpose: it would be more
straightforward to pursue the marketing of information goods as
%(e:contributory infringement) and to separately regulate the
modalities of this type of infringement, taking into consideration the
particularities of the information economy.

#dea: The European Commission's proposal advises against program claims.

#leW: The European Parliament (CULT+ITRE+Plenary) follows the Commission in
this point and, in addition, with its new Art 7 (voted as 5.1b),
stiupulates that freedom of publication may not be limited by patent
claims:

#ing: The Council Working Party refuses the CEC and EP proposals and, with
its Art 5.2, insists on program claims:

#ise: The wording is misleading.

#crW: Any program can be claimed as a computing process executed on general
purpose computing equipment.  The additional presence of a process
claim does not limit the value of the program claim in any way.

#loe: see above

#iWe: The negation particle %(q:not) lies.  It negates the sentence under a
condition which is always false.  Try to read the sentence without the
%(q:not, unless ...) phrase and you will understand what it means!

#tph: It should be noted that the german version deviates significantly from
the english and french one.  %(q:put into force) and %(q:mettre en
oevre) (make work in practise) is worlds apart from %(q:begründen)
(provide a rationale for).  We base our analysis on the english and
french version.  It would have to be partially rewritten for the
german one which, in a Freudian error, seems to directly admit that it
is the presence of a program which provides the rationale for
patenting.

#rnp: The existence of program claims changes the systematics of patent law
and puts software at the center rather than on the periphery of the
system.

#cnr: The Council Working Party wants program claims be be seen %(q:only) as
derived claims, of %(q:purely declaratory nature), as was said in the
justifications of the corresponding JURI amendments.  This can not be
true, because

#rcg: From any program claim an equivalent process claim can be derived, but
not vice versa.  Once a program claim is present, the process claim
becomes redundant, but not vice versa.

#aoc: A program claim is %(tp|broader|i.e. less specific) than the process
claim corresponding to it.  The process claim specifies not only the
data structure but also a use of this data structure (e.g. execution
on a computer in connection with peripheral equipment).

#Ebn: The broadest claim in a patent specification (i.e. the claims with the
fewest features) usually most closely describes what was contributed
to the prior art.  Thus the %(q:technical contribution) of a software
patent has to be searched for within the (non-technical) scope of the
program claim, whereas the %(tp|additional technical feature of the
process claim|%(q:execution in computer memory)) is only of
declaratory nature.

#Wet: It is always possible to formulate independent claims as
%(tp|dependent|declaratory) claims and vice versa.  Such formulation
exercises only obfuscate the real dependency relations.

#Wsg: The Examination Guidelines of the European Patent Office of 1978 also
take this view when explaining the different conceivable claim forms
of %(q:programs for computers):

#rdr: Here too the EPO assumes that a %(q:claim to the program as such or a
program recorded on magnetic tape) is the normal form of a software
claim.  It however also points out that the same object can be
declared as a %(q:process for operating a .. computer) or as a
%(q:computer, characterised by that ..) and that such attempts at
obtaining software patents by redeclaration are to be expected.

#rww: Whereas the EPO's initial guidelines correctly exposed redeclaration
of programs to processes as a declaratory trick, its stewards today
assume that not only is this trick admissible, but redeclaration from
there to the programs as such must also be admissible, since it is
only a redeclaration.

#mis: From the existence of program claims it follows that, to speak with
the words of the EPO of 1978, %(e:the contribution to prior art
consists only in a program for computers), i.e. that the %(q:technical
contribution) is confined to calculation with the abstract entities of
virtual machine.  Thus it is assured that the indeterminate legal
concepts by which the Council Working Party wants to limit
patentability (see below) can never have a limiting meaning.  For
example, an exclusion of business methods can not be achieved under
these conditions, because business methods are concretisations of
computing rules (programs for computers).

#rrr: By treating information objects as industrial goods, the Council
Working Party also precludes all discussions about limitation of
patent enforcability with respect not only to freedom of publication
but to any freedom that might be founded in the informational nature
of the object.

#idK: The situation is made only worse by the insertion of dissimulating
recitals, such as 7a:

#Wtw: The unexperienced reader (e.g. a minister) will believe that this
excludes program claims.  Unfortunately the opposite is true.  In
connection with Art 5(2) the recital says that a claim to %(q:program,
characterised by [its functions]) is not a claim to a program as such.
 As an explanation of the apparent contradiction it suggests that a
%(q:program as such) is to be construed as the %(q:expression) aspect
of a program (rather than the function aspect) .  I.e. a narrow claim
to %(q:program, characterised by that the text in listing 1 is used)
is to be rejected as a claim to individual expression, whereas a broad
claim to a %(q:program, characterised by that it [does what the one in
listing 1] does) is allowed.  Nobody is interested in narrow claims. 
A claim to one particular listing would merely constitute an weak form
of copyright which nobody wants and to which nobody objects.  The
purpose of raising the non-issue of %(q:expression patents) in the
Council's proposal is not to regulate something but to confuse the
legislator about what is being regulated.

#br2: In the Council Working Paper of Nov 2002 Art 2.4B read as follows:

#hhw: The European Parlament's Art 7 makes it even clearer that the patent
system should promote publication of program texts as a part of the
disclosure rather than forbid it as part of the monopoly:

#egm: The Council Working Paper has thus moved away from compromise with the
Parliament.

#i0l: The European Commission's Art 6 proposes a meaningless limitation on
patent enforcement, which the Council further narrows down:

#moi: This provision allows decompilation of patented programs, an act of
research in the private sphere which is not covered by patents anyway.
 The programs resulting from such research may however still be
patent-encumbered, and Art 6 implies that such patents may be enforced
against the programs, even when these programs are used only for
purposes of interoperabilty.  Thus, Art 6 pretends to protect
interoperability but in fact does the opposite: it ensures that nobody
can use patent-encumbered communication conventions without a license
from the patentee.

#tKa: The European Parliament was not misled.  All three commissions and the
Plenary voted for a real interoperability privilege:

#Wtr: The Council Working Paper rejects the Parliament's vote and insists on
ensuring domination of patentees over communication conventions.

#sWt: When a file format can be covered by patents, it becomes impossible
for competitors to convert from it to another file format.  This
reinforces monopolistic tendencies in the software market.  
Standardisation Consortia such as the W3C (responsible for the world
wide web standards) are regularly slowed down by attempts of patent
owners to impose patent taxes on a standard.  Patent-Taxation imposes
heavy burdens on the managment of the standard itself and of the
compliant software.  Free software and shareware can usually not
comply.  Due to the high transaction costs, formalised standards
become less competitive in relation to de-facto standards, where one
vendor alone decides.  Microsoft is already pushing patented protocols
into the market under licensing conditions which exclude free
software.  Even the European Commission's %(cm:recent competition
procedings against Microsoft) have failed to change this.  Anti-trust
law alone, without further provisions such as Art 6a, is out of the
reach of normal market participants and provides insufficient remedies
against the anti-competitive effects of proprietary communication
standards.

#tWW: In its version of March 17, the paper goes yet one step further.  A
newly worded recital 17 indicates that the right of interoperation
shall not be regulated within patent law but only within antitrust
law:

#Wde: Hereby the working party creates the impression that it is taking the
needs for freedom of interoperation into account.   Upon closer
reading it turns out however that the exact opposite is achieved:
without a costly, lengthy, and rarely successful legal procedure
software creators shall, according to the will of the Council patent
working party, be not be allowed to interoperate.

#uaW: By this hardening of its position the Council Working Group responds
to the following proposal from the Luxemburg Delegation:

#Wnb: Delegations will find below a proposal by the Luxembourg delegation
for an interoperability clause (new article 6a) to be included in the
text of the proposed Directive. The proposed text is based on
amendment 76 of the European Parliament, but is more restrictive in
that it takes the example in the original amendment and uses it as the
only acceptable exception for the purpose of ensuring
interoperability. This stricter wording should make the provision
fully compatible with Article 30 TRIPS.

#EWe: This Proposal corresponds to the article in the form in which it was
approved by the parliamentary committees CULT, ITRE and JURI.  It
avoids the term %(q:significant purpose), which had been newly
introduced in the plenary vote.  Thereby the Luxemburg delegation
fully satisfies the %(kk:criticism of the Commission) and the
requirement of the three-step test (limited limitation) according to
Art 30 TRIPs.  By refusing the Luxemburg proposal and with it the
position of all three concerned committees of the European Parliament,
the patent working party shows that Art 30 TRIPs is not its primary
concern.  Rather, it must be assumed that rent-seeking interests
associated with the dominance of communication standards, as recently
advocated in the name of the CEOs of %(te:five large European
telecommunication companies), are the driving forces behind the
Council patent working party's intransigence.

#efW: In European patent law, patentable inventions are limited in two ways:

#edj: Negative definition: certain types of achievements are declared to be
non-inventions in Art 52(2) EPC.  Among these are achievements in the
fields of mathematics, programming and business management.

#fsu: Positive definition: law courts in Germany and elsewhere have defined
%(q:technical invention) as %(q:solution of a problem by controllable
forces of nature) or similar and thereby generalised the list of
negative definitions in Art 52(2) EPC.

#ein: The Commission and Council reject any definition of what is a
patentable invention, both in negative or positive ways.  On the one
hand, the Commission makes patentability hinge on abstract concepts
such as %(q:technical contribution), on the other it refuses to define
these concepts or provides only tautological definitions.

#eWn: The Parlament has proposed both positive and negative definitions.

#eai: Negative

#oii: Positive

#iNe: Positive and Negative united

#nWg: The Council Working Party proposes to remove all these definitions
and, in addition, to make it clear that the concept of technicity,
even if defined by a judge, can not be used to limit patentability:

#ecb: The Council says that it is enough if there is something technical in
the scope of some of the claims.  Claims without any technical
component, such as program claims, are possible.  Moreover the Council
states clearly that the technical contribution, i.e. the new solution
which justifies the granting of a patent, may consist solely of
non-technical features.   It is enough if new %(tp|calculation
rules|algorithm) are combined with %(tp|known equipment|universal
computer) for solving a %(q:technical problem), i.e. for reducing the
number of needed mouse clicks.  Such a %(q:solution to a technical
problem) will be called  %(q:technical contribution), regardless of
whether the solution itself is technical.  According to the EPO's
system, on which this paragraph is built, the %(q:technical
contribution) needn't be novel or non-obvious.  If a %(q:technical
problem) can be constructed as part of the non-obviousness test, there
is a technical contribution.  By writing that the %(q:technical
contribution) must be non-obvious (and later, based on a German
amendment, even that it must be novel), the Council's system suddenly
breaks the EPO's logic and returns to the traditional understanding of
%(q:technical contribution) as a synonym for %(q:invention).  It is
very unclear what this means in the context of the EPO doctrines which
the Council is trying to codify.

#hen: The Parliament equates %(q:technical contribution) with
%(q:invention).  This is the common usage of the terms in older
manuals of patent law as well as in much of today's case law.  
Accordingly, an %(e:invention) is a contribution of technical
knowledge to the state of the art, also called %(q:technical teaching)
or %(q:technical contribution).  The patent claims must contain the
%(tp|contribution|e.g. an innovative algorithm) in combination with
%(tp|non-inventive extensions|%(q:run the algorithm on a computer))
that define a scope of exclusion, i.e. tell the reader what he is not
allowed to do.  The patent applicant is free to claim his contribution
in combination with any extension features he likes, and usually these
extension features will refer to something tangible, such as execution
of some process on a computer.  Thus by %(q:requiring) that the claim
scope %(q:must contain technical features), the Council Working Party
does not exclude any software or business method from patentability. 
It does however seem to exclude program claims, and we can only
speculate how the Working Party wants to resolve this contradiction. 
Moreover, if only the claim scope as a whole needs to contain
%(q:technical features), then it follows that the %(q:technical
contribution) may consist solely of non-technical features.  In other
words, an algorithm can be a %(q:technical contribution), if the scope
of [some of] the claims contains technical features (e.g. references
to computer equipment).  This moreover leads the Council Working Party
to the conclusion, written in Recital 12, that all
%(q:computer-implemented) claim objects, including algorithms,
business methods etc, %(q:belong to a field of technology):

#aWW: This Recital 12 says exactly what the European Commission says in its
much-criticised Article 3, whose deletion the Council Working Party
recommends:

#RiW: The only difference between Council Recital 12 and Commission Article
3 consists in the form of presentation.  While the Commission says
bluntly what it wants, the Council hides its intention in an
%(q:although) clause and envelopes it with meaningless limitation
rhetorics.

#esK: The French delegation in the Council rejected the implication of
Recital 12, as did the European Parliament.  When Arlene McCarthy
tried to pull the Council's trick on JURI, her credibility suffered. 
When JURI supported the trick with a 2/3 majority, it was heading for
defeat in the Plenary.

#Rkt: This might have led people to assume that the Council Patent Working
Party would learn the lesson and at least follow the French
recommendation.  However the opposite happened:  the old losing tactic
is maintained and the French delegation's reservations are removed.

#dnG: According to the EPO's Pension Benefits Doctrine of 2000, which forms
the basis of the Commission and Council proposals, anything running on
a computer is a technical invention.  In order to be able to
nevertheless reject some business method patent claims, the EPO says
that the technicity requirement is somehow implicit in the
non-obviousness requirement.  Very few people understand what this
means, and even patent lawyers regularly complain they can not
understand the EPO's esoteric teachings on this point.  Among the
critics are leading patent law scholars and judges who can hardly be
suspected of opposing software patentability.  The US Trade
Representative has %(us:pointed out) that this EPO doctrine violates
the system of Art 27 TRIPs. The Commission's Explanatory Memorandum of
2002/02/20 comments: %(orig:As set out in recital 11 and Article 3,
the presence of a %(q:technical contribution) is to be assessed not in
connection with novelty but under inventive step. Experience has shown
that this approach is the more straightforward to apply in practice.) 
In other words: even the Commission tacitly acknowledges that the
Pension Benefits doctrine is difficult to justify in theory.  The most
direct practical result of this doctrine is that computer algorithms
and business methods can no longer be rejected without first
conducting an expensive novelty search.  Since many national patent
offices (e.g. the French PO) do not conduct novelty searches, they can
no longer reject any patent for lack of technical contribution, not
even under the laxist criteria proposed by this directive.  Moreover,
the Pension Benefits doctrine creates maximal confusion and legal
uncertainty around the concept of technical contribution, so that
authoritative patent courts are no longer accountable on the grounds
of any law.

#gfy: The term %(q:computer-implemented invention) was introduced and
defined by the EPO in 2000 as a euphemism for %(q:programs for
computers) (rules for operation of conventional elements of a data
processing system, such as processor, storage, input/output etc),
which are not inventions in the sense of patent law.  The Commission's
proposal is a simple copy of the EPO's definition.

#xrW: The EPO and Commission are careful to define %(q:computer-implemented
inventions) narrowly, so that really only programs for computers are
covered.  The prima facie novel contribution must consist in the way
in which the data processing system is programmed, not in peripheral
technical processes.  A new way of applying heat to automobile tyres
under program control would %(e:not) be a %(q:computer-implemented
invention) according to the EPO/Commission definition.

#uht: Due to such bluntness, as seen in the definition of
%(q:computer-implemented invention) and the assertion of Art 3 that
all such %(q:inventions) are patentable, the Commission quickly became
a target of widespread criticism.  The Council Working Party tries to
avoid the Commission's bluntness while maintaining its doctrines. 
Thus, in the Council paper both the programmer's work and the process
engineer's work would be called %(q:computer-implemented invention). 
The Council's term encompasses anything that involves a computer.

#hte: The parliament has chosen to redefine the term
%(q:computer-implemented invention) in such a way that it means
exactly what the EPO/Commission do not want it to mean and excludes
exactly what they want: i.e. it refers only to peripheral technical
processes, not to programs for computers as such.

#nfg: Thus the Parliament says clearly what Commission and Council often
pretend they want to say: the computer-executed algorithms which
control an anti-blocking system are not patentable, whereas the
anti-blocking system as a whole, with machinery, heat, friction and a
new way to use them specified in the claims, may be a patentable
invention.  While the work of the process engineer is patentable, that
of the programmer is not.

#ahe: The Council does not want to decide what the Orwellian word
%(q:computer-implemented invention) should refer to: programs for
computers as such (Commission proposal) or computerised technical
inventions (Parliament proposal).   This may seem politically clever,
but it has grave consequences for legal practise.  Under the Council
version, it is up to the patent attorney to decide whether a process
should be treated as a %(q:computer-implemented invention), i.e.
whether the special regime of the directive should apply to it.  Any
patentable process must be repeatable and will therefore in practise
run automatically, i.e. under computer control.  If the patent
attorney puts this feature into his claim, the Council directive
applies with all its special provisions, which deviate considerably
from normal patent law.  E.g. according to Art 5(2) it becomes
possible to claim the program without the process hardware.  Thus a
turing-complete description of a chemical process will be claimable,
regardless of whether the program logic is novel or not.  Such a
tendency can indeed be observed today already.  It allows patent
applicants to control secondary programming and publishing markets. 
The principle that patents should serve de diffusion of technical
information is thereby discarded on a systematic basis, not only for
the special case of software patents but for all patents.

#mts: We have repeatedly pointed out rhetorical tricks by which the Council
paper misleads unattentive readers.

#egr: We have %(cj:listed and analysed) some of these tricks in summer 2003,
when they were copied-and-pasted by Arlene McCarthy and the JURI
committee.

#ebw: A detailed analysis of the rhetorical strategems which pervade the
Council Working Paper is still to be provided here.

#ute: The most frequently used rhetocial ploy works as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always
met.

#rWn: The members of the EU Council's Patent Working Party have further
hardened their previous position favor of unlimited patentability and
patent enforcability with respect to software.  Thereby they have
taken an extreme counter-position to the European Parliament which
makes any negotiation very difficult.  Some of the positions taken by
the Party, such as its insistence on program claims, do not serve any
recognisable interest apart from that of making negotiations
difficult.  This raises questions about the intentions of the Party.

#dnd: The Party members meet regularly both in the Council of the European
Union and in the Administrative Council of the European Patent
Organisation (EPO) which runs the European Patent Office (EPO).  They
constitute a personal link between the EPO and the EU.  Within the
EPO, they are the masters of legislation.  They are reluctant to share
this turf with anyone.  EU participation is welcome as long as this
means that the EU will rubberstamp the Party's policies without
questions.  When the EU starts to question the Party's wisdom, the
Party closes down all communication channels and rallies around the
positions around which its authority has evolved, i.e. the infinite
wisdom of the Technical Boards of Appeal.  The Party would be quite
happy give up the EU directive project altogether and to pursue its
line through diplomatic conferences and other intergovernmental
backroom processes where no parliament and no public discussion is
involved.  Frits Bolkestein %(sp:pronounced) this well-known secret in
public in the European Parliament one day before the plenary vote.

#riA: The situation has come to the point where the Party's monopoly clearly
stands in the way of the development of the EU's legislative
competence in vital areas of economic policy.  The member states must
decide whether they want to move forward or backward.

#tnW: explains fairly concisely why the Council's %(q:compromise document)
is the worst of all proposals on software patents that have come out
of the Brussels process.

#spW: Council Presidency 2004-03-17 %(q:Compromise Paper) on Software Patent
Directive

#hel: published 2-3 weeks later on the consilium site in English and German,
with all information about positions of national delegations deleted.

#til: latest public version on Council website

#sWW: Translations to the last remaining EU languages appeared around 18th
of August.

#nmk: Council Document 2004-05-25

#tWs: latest published version

#lcW: HTML converted from the original MSWord document with OpenOffice.

#cgW: Council Working Document ST09051

#ebn: a recent unpublished version with footnotes about positions of
national delegations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: cons0401 ;
# txtlang: en ;
# multlin: t ;
# End: ;

