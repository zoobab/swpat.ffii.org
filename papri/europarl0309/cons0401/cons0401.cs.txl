<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Council 2004 Proposal on Software Patents

#descr: The Council of Ministers has reached political agreement on a paper which contains alternative suggestions to the amendments on the directive %(q:on the patentability of computer-implemented inventions) passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, %(q:computer-implemented) algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.

#ltp: Kollision statt Kompromiss

#kWr: Programm-Ansprüche: alle Türen zugeschlagen

#eiW: Programmtext-Veröffentlichung: Ein weiterer Schritt weg vom Parlament

#bea: Interoperabilitätsprivileg gestrichen, Grenzenlose Durchsetzbarkeit gesichert

#tng: Ersatzlose Streichung der Definitionen von %(q:Technik), %(q:Industrie), %(q:Erfindung)

#tef: Systemwidrige Vermengung von Technizität und Erfindungshöhe

#edj2: Anwendungsgebiet der Richtlinie auf beliebige Verfahrenserfindungen ausgeweitet

#bra: Scheinbegrenzungen, Irreführende Schachtelsätze

#ien: Mutwillige Zerstörung des EU-Richtlinienprojektes?

#ruo: Die Version der Rats-Arbeitsgruppe gleicht bis auf unwesentliche Details der Version vom November 2001.  Von allen bisherigen Vorschlägen stellt sie die radikalsten Forderungen nach grenzenloser Patentierbarkeit dar.  Die Radikalität wird jedoch in Nebensätzen versteckt und von belangloser Begrenzungs-Rhetorik umhüllt, so dass unerfahrene und unaufmerksame Leser sie leicht übersehen.

#utd: Es wurden lediglich in einige Formulierungen aus dem %(ju:Beschluss des Rechtsausschusses JURI vom Juni 2003) (McCarthy-Bericht) übernommen, die nichts zur Begrenzung der Patentierbarkeit beitragen sondern allenfalls geeignet sind, den Anschein einer Begrenzung zu erwecken.  Die JURI-Änderungsanträge lehnten sich eng an das %(dk:Rats-Arbeitspapier vom November 2002) an und wandten sich gegen die von den %(ci:beiden beratenden Ausschüssen) ausgearbeiteten Empfehlungen, ohne hierfür eine Begründung auch nur zu versuchen.  Das Plenum des EU-Parlaments verweigerte daher JURI (und damit der Rats-Arbeitsgruppe) seine Gefolgschaft.   In der Plenarabstimmung vom 24. September 2003 setzten die Abgeordneten den Scheinbegrenzungen echte Begrenzungen entgegen.  JURI-Formulierungen wurden nur zur Gesichtswahrung beibehalten, soweit sie dem Ziel der echten Begrenzung nicht entgegenstanden.  Indem die Ratsarbeitsgruppe nun wiederum die Änderunganträge des Plenums und der Ausschüsse CULT und ITRE ignoriert und stattdessen JURI-Formulierungen übernimmt,die das Plenum nur wegen ihrer Belanglosigkeit duldete, verunstaltet sie ihren eigenen Entwurf vom November 2002 und vergrößert dabei nur noch den Abstand zwischen Rat und Parlament.  Die Bezeichnung %(q:Kompromisspapier) auf dem Schreiben der Ratspräsidentschaft ist daher diesmal in besonders hohem Maße irreführend.  Auch beim vorigen %(q:Kompromisspapier) vom November 2002, wurde nicht offengelegt, zwischen welchen Interessen denn nun ein Kompromiss geschlossen wurde.  Diesmal kann es den Umständen nach nur um einen Kompromiss zwischen Parlament und Rat gehen.  Bei näherem Hinsehen zeigt sich jedoch, dass genau das Gegenteil erreicht wird: die Rats-Arbeitsgruppe entfernt sich noch weiter vom Parlament.

#nie: Im folgenden möchten wir dies durch einen Vergleich der Ratsvorschläge mit denen des Parlaments im einigen zentralen Streitfragen verdeutlichen.

#gsW: Programmansprüche sind Patentansprüche der Form

#ben: Computerprogramm, bei dessen Ladung in den Arbeitsspeicher eines Rechners der Rechenvorgang gemäß obigem Anspruch N ausgeführt wird.

#oWs: Statt %(q:Computerprogramm) kann auch %(q:Programm auf Datenträger), %(q:Programmprodukt), %(q:Datenstruktur), %(q:Computerlesbares Medium) oder anderes mehr stehen.  Wesentlich ist, dass nicht mehr nur materielle Erzeugnisse und Prozesse sondern auch Informationskonstrukte beansprucht werden.

#Wtt: Diese Anspruchsart wurde 1998 durch zwei Entscheidungen der Technischen Beschwerdekammern des EPA n Europa eingeführt.  Auch in den USA kam sie erst 1994 auf.  Sie ist umstritten, weil

#dyr: sie offen mit der traditionellen Systematik des Patentrechts bricht, wonach der Anspruchsgegenstand technischer Natur sein muss und Informationskonstrukte im Gegenzug hierfür offenbart -- d.h. zu Gemeingut gemacht -- werden;

#tvW: damit wörtlich Programmen für Datenverarbeitungsanlagen als solche beansprucht werden, was sich nur noch unter größten (und nach %(ex:üblicher Auslegungsmethodik) unzulässigen) Verrenkungen in Einklang mit Art 52(2),(3) bringen lässt;

#nce: sie der Publikationsfreiheit, wie sie in Art 10 der Europäischen Menschenrechtskonvention (EMRK) festgehalten wird, systematisch zuwiderläuft

#r1s: Patentrechte gehören nicht zu den Rechten, die gemäß Art 10(2) EMRK eine Schranke der Publikationsfreit darstellen könnten

#reo: sie Entwickler und Distributoren von Software ebensowie andere Informationsmittler (z.B. Internet-Zugangsanbieter) direkt den schweren Geschossen der Patentjustiz aussetzt, als handele es sich um Lieferanten von Industriegütern;

#cgd: sie keinem praktischen Zweck dient: es läge näher, die Vermarktung von Informationsgütern als %(e:mittelbare Patentverletzung) zu verfolgen und die Modalitäten dieser Verfolgung im Hinblick auf die Besonderheiten der Informationsökonomie getrennt zu regeln;

#dea: Die Europäischen Kommission sieht in ihrem Richtlinienvorschlag Programmansprüche nicht vor.

#leW: Das Europäische Parlament (CULT+ITRE+Plenum) folgt hierin der Kommission und bekräftigt darüber hinaus mit seinem neuen Art 7 (in Abstimmung als 5.1b gebilligt) die Publikationsfreiheit als vorrangiges Rechtsgut:

#ing: Die Rats-Arbeitsgruppe lehnt die Vorschläge von Kommission und EP ab und beharrrt mit ihrem Art 5(2) auf Programmansprüchen:

#ise: Die Formulierung %(q:sind nur zulässig, insoweit ...) ist in mehrfacher Hinsicht irreführend:

#crW: Der %(q:insoweit)-Satz schränkt nichts ein.  Zu jedem Programmanspruch lässt sich ein passender Verfahrens- oder Erzeugnisanspruch formulieren, von dem dann pro forma der Programmanspruch abhängt

#loe: s. oben

#iWe: Die Partikel %(q:nur) lügt.  Sie erfüllt keinen regulatorischen Zweck.  Man sollte den Satz mal zur Übung ohne diese Partikel (und auch ohne den belanglosen %(q:insoweit)-Satz) lesen.

#tph: It should be noted that the german version deviates significantly from the english and french one.  %(q:put into force) and %(q:mettre en oevre) (make work in practise) is worlds apart from %(q:begründen) (provide a rationale for).  We base our analysis on the english and french version.  It would have to be partially rewritten for the german one which, in a Freudian error, seems to directly admit that it is the presence of a program which provides the rationale for patenting.

#rnp: Programmansprüche verändern die Systematik des Patentrechts und bringen Software vom Rand in die Mitte des Systems.

#cnr: Die Ratsarbeitsgruppe beteuert freilich, sie sehe Programmansprüche %(q:nur) als abgeleitete Ansprüche, %(q:rein deklaratorischer Natur), wie es %(tp|in diesem Umfeld|etwa bei vom Rats-Papier inspirierten Änderungsanträgen im EP) immer wieder hieß.  Welcher Natur diese Ansprüche sind, ergibt sich jedoch nicht aus den Wünschen ihrer Befürworter sondern aus den von ihnen in der Rechtspraxis geschaffenen Fakten.  Zu diesen gehört:

#rcg: Aus jedem Programmanspruch kann ein Verfahrensanspruch abgeleitet werden.  Umgekehrtes nicht gilt.

#aoc: Ein Programmanspruch ist breiter als der ihm entsprechende Verfahrensanspruch.  Der Verfahrensanspruch spezifiziert über das Programm hinaus dessen Ausführung im Speicher einer Datenverarbeitungsanlage.  Dieses zusätzlich einengende Merkmal trägt nichts zur Erfindung bei.

#Ebn: Der breiteste Anspruch einer Patentschrift (d.h. der mit den wenigsten Merkmalen) beschreibt in der Regel am genauesten den Bereich, der zur Erfindung beiträgt.  Die erfindungswesentlichen Merkmale eines Softwarpatentes finden sich im Programmanspruch.  Nicht der Programmanspruch sondern der Verfahrensanspruch mit seinen zusätzlichen erfindungs-unwesentlichen Merkmalen ist insoweit deklaratorischer Natur.

#Wet: Es ist immer möglich, unabhängige Ansprüche als abhängige (deklaratorische) Ansprüche zu formulieren und umgekehrt.  Solche Formulierungskünste sind geeignet, die tatsächlichen Abhängigkeitsverhältnisse zu verschleiern.

#Wsg: Die Prüfungsrichtlinien des Europäischen Patentamtes von 1978 nehmen auch diesen Standpunkt ein, wenn sie die verschiedenen Anspruchsformen, in denen die gemäß Art 52 EPÜ von der Patentierbarkeit ausgeschlossenen %(q:Programme für Datenverarbeitungsanlagen) daher kommen könnten, wie folgt erklärt:

#rdr: Auch hier wird davon ausgegangen, dass ein %(q:Patentanspruch für das Programm als solches oder für das auf Magnettonband aufgenommene Programm) die normale Form eines Software-Anspruches wäre.  Es wird aber zugleich darauf hingewiesen, dass sich der selbe Gegenstand auch als %(q:Verfahren zum Betrieb einer ... Datenverarbeitungsanlage) oder als %(q:Datenverarbeitungsanlage, dadurch gekennzeichnet, dass ...) deklarieren lässt, und dass solche Versuche, Software durch Umdeklarierung zu patentieren, zu erwarten sind.

#rww: Während das EPA 1978 richtig die Umdeklarierung von Programmen zu Prozessen als deklaratorischen Trick entlarvte und verbot, erklären seine Lenker heute nicht nur diesen Trick für zulässig sondern folgern auch noch daraus, dass der umgekehrte Weg der Umdeklarierung von Prozessansprüchen zu den zugrundeliegenden Programmansprüchen auch zulässig sein muss, da es sich ja nur um eine Umdeklarierung handelt.

#mis: Aus der Patentierbarkeit von Programmen für Datenverarbeitungsanlagen folgt, dass, um mit den Worten des EPA von 1978 zu sprechen, %(e:der Beitrag zum bisherigen Stand der Technik lediglich in einem Programm für Datenverarbeitungsanlagen besetehen muss), m.a.W. dass der %(q:technische Beitrag) sich im Rechnen mit den abstrakten Entitäten des Neumannschen Universalrechners erschöpfen kann.  Damit wird verhindert, dass den unbestimmten Rechtsbegriffen, mit denen die Rats-Arbeitsgruppe die Patentierbarkeit begrenzt sehen möchte (s. unten), jemals eine begrenzende Bedeutung zukommen kann.  Auch mit einer Begrenzung der Patentierbarkeit von Geschäftsmethoden kann unter diesen Vorzeichen nicht gerechnet werden.   Denn Geschäftsmethoden sind Konkretisierungen von Datenverarbeitungsprogrammen.

#rrr: Wer Programmansprüche vorschlägt, beendet damit im voraus jede Diskussion über Schranken der Patentdurchsetzung im Hinblick auf entgegenstehende Rechtsgüter aller Art.  Wenn schon die Publikationsfreiheit offen durch Patente beschränkt wird, kann auch weniger zentralen Freiheiten, die als Schranken der Patentierbarkeit oder Patentdurchsetzbarkeit gelten und deren Kodifizierung z.T. vom Parlament vorgeschlagen wurde, keine vorrangige Bedeutung mehr zukommen.

#idK: The situation is made only worse by the insertion of dissimulating recitals, such as 7a:

#Wtw: The unexperienced reader (e.g. a minister) will believe that this excludes program claims.  Unfortunately the opposite is true.  In connection with Art 5(2) the recital says that a claim to %(q:program, characterised by [its functions]) is not a claim to a program as such.  As an explanation of the apparent contradiction it suggests that a %(q:program as such) is to be construed as the %(q:expression) aspect of a program (rather than the function aspect) .  I.e. a narrow claim to %(q:program, characterised by that the text in listing 1 is used) is to be rejected as a claim to individual expression, whereas a broad claim to a %(q:program, characterised by that it [does what the one in listing 1] does) is allowed.  Nobody is interested in narrow claims.  A claim to one particular listing would merely constitute an weak form of copyright which nobody wants and to which nobody objects.  The purpose of raising the non-issue of %(q:expression patents) in the Council's proposal is not to regulate something but to confuse the legislator about what is being regulated.

#br2: Im Rats-Arbeitspapier vom November 2002 stand unter Art 2.4B zu lesen:

#hhw: Das Europäische Parlament macht in Art 5.1D noch deutlicher, dass das Patentwesen die Veröffentlichung von Programmtexten als Teil der Offenbarung fördern und nicht etwa als Teil des Monopols verbieten soll:

#egm: Wenn der Rats-Arbeitsgruppe an einem Kompromiss mit dem Parlament gelegen wäre, hätte sie den Vorschlag der französischen Ratsdelegation zumindest erneut als Minderheitenvorschlag mit in ihr Papier aufgenommen.  Aber das Gegenteil geschah: alle Hinweise auf eine Pflicht zur Veröffentlichung von Programmtext im Rahmen der Offenbarung wurden aus dem Ratsvorschlag gestrichen.

#i0l: Die Europäische Kommission schlug nach langem internen Tauziehen im Februar 2002 ein wirkungsloses Interoperabilitätsprivileg vor, welches der Rat weiter einschränkt:

#moi: Diese Bestimmung erlaubt nur das Dekompilieren von Programmen im privaten Bereich, das durch Patente ohnehin nicht beschränkt wird.  Will man die aufgrund der Dekompilation erarbeiteten interoperablen Programme dann verwenden oder veröffentlichen, so verletzt man Patente.  Die Analogie zum Urheberrecht ist irreführend, da ein selbst erarbeites interoperables Programm niemals das Urheberrecht verletzt.

#tKa: Das Plenum und die drei Ausschüsse des EU-Parlamentes ließen sich nicht irreführen.  Sie stimmten für ein echtes Interoperabilitätsprivileg:

#Wtr: Diesen Vorschlag nimmt das Ratspapier nicht an.

#sWt: When a file format can be covered by patents, it becomes impossible for competitors to convert from it to another file format.  This reinforces monopolistic tendencies in the software market.   Standardisation Consortia such as the W3C (responsible for the world wide web standards) are regularly slowed down by attempts of patent owners to impose patent taxes on a standard.  Patent-Taxation imposes heavy burdens on the managment of the standard itself and of the compliant software.  Free software and shareware can usually not comply.  Due to the high transaction costs, formalised standards become less competitive in relation to de-facto standards, where one vendor alone decides.  Microsoft is already pushing patented protocols into the market under licensing conditions which exclude free software.  Even the European Commission's %(cm:recent competition procedings against Microsoft) have failed to change this.  Anti-trust law alone, without further provisions such as Art 6a, is out of the reach of normal market participants and provides insufficient remedies against the anti-competitive effects of proprietary communication standards.

#tWW: In its version of March 17, the paper goes yet one step further.  A newly worded recital 17 indicates that the right of interoperation shall not be regulated within patent law but only within antitrust law:

#Wde: Hereby the working party creates the impression that it is taking the needs for freedom of interoperation into account.   Upon closer reading it turns out however that the exact opposite is achieved: without a costly, lengthy, and rarely successful legal procedure software creators shall, according to the will of the Council patent working party, be not be allowed to interoperate.

#uaW: By this hardening of its position the Council Working Group responds to the following proposal from the Luxemburg Delegation:

#Wnb: Delegations will find below a proposal by the Luxembourg delegation for an interoperability clause (new article 6a) to be included in the text of the proposed Directive. The proposed text is based on amendment 76 of the European Parliament, but is more restrictive in that it takes the example in the original amendment and uses it as the only acceptable exception for the purpose of ensuring interoperability. This stricter wording should make the provision fully compatible with Article 30 TRIPS.

#EWe: This Proposal corresponds to the article in the form in which it was approved by the parliamentary committees CULT, ITRE and JURI.  It avoids the term %(q:significant purpose), which had been newly introduced in the plenary vote.  Thereby the Luxemburg delegation fully satisfies the %(kk:criticism of the Commission) and the requirement of the three-step test (limited limitation) according to Art 30 TRIPs.  By refusing the Luxemburg proposal and with it the position of all three concerned committees of the European Parliament, the patent working party shows that Art 30 TRIPs is not its primary concern.  Rather, it must be assumed that rent-seeking interests associated with the dominance of communication standards, as recently advocated in the name of the CEOs of %(te:five large European telecommunication companies), are the driving forces behind the Council patent working party's intransigence.

#efW: Im Europäischen Patentrecht wird die Patentfähige Erfindung auf zweierlei Wegen eingegrenzt:

#edj: Negative Definition: bestimmte Arten von Leistungen werden vom Gesetz (Art 52(2) EPÜ, §1(2) PatG) als Nicht-Erfindungen definiert.  Hierzu gehören z.B. Leistungen auf dem Gebiet der Mathematik, der Geschäftsplanung und der Programmierung.

#fsu: Positive Definition: die Rechtsprechung des Bundesgerichtshofs (BGH) hat die %(q:technische Erfindung) als %(q:Lösung eines Problems durch Einsatz beherrschbarer Naturkräfte) definiert und damit ein Beurteilungskriterium geschaffen, das mit den ausdrücklichen Negativen Definitionen in Einklang steht und zugleich auf viele Fälle anwendbar ist, für deren Beurteilung sich aus den Negativdefintionen nur schwer eine Anleitung entnehmen lässt.

#ein: Die Kommission und der Rat schlagen vor, sowohl auf negative als auch auf positive Definitionen der Begriffe %(q:Technik), %(q:Industrie) und %(q:Erfindung) zu verzichten.  Einerseits machen Kommission und Rat die Frage der Patentierbarkeit ausschließlich an abstrakten Begriffen wie %(q:technischer Beitrag) fest, andererseits definieren sie diese Begriffe allenfalls zirkulär, was einer Nicht-Definition gleichkommt.

#eWn: Das Parlament hingegen hat negative und positive Definitionen vorgeschlagen.

#eai: Negativ

#oii: Positiv

#iNe: Positiv und Negativ vereint

#nWg: Demgegenüber beharrt der Rat auf einer zirkulären Definition und stellt überdies noch klar, dass der Technikbegriff, sollte er denn einmal wirklich mit Bedeutung gefüllt werden, nicht zur Begrenzung der Patentierbarkeit herangezogen werden darf:

#ecb: M.a.W.: nur der %(q:Anspruchsbereich) muss technische Merkmale umfassen.  Der %(q:technische Beitrag) hingegen kann auch aus nicht-technischen Merkmalen bestehen.  Auch die Kombination neuer Nicht-Technik (z.B. Algorithmus) mit alter Technik (z.B. Universalrechner) kann zur Lösung eines %(q:technischen Problems) (z.B. Reduzierung der Zahl erforderlicher Mausklicks) beitragen und somit einen %(q:technischen Beitrag) darstellen.  Dieser %(q:technische Beitrag) wurde zunächst nur als ein Aspekt der Erfindungshöhe behandelt, d.h. er konnte für sich genommen altbekannt sein und muss nicht einmal den üblichen Anforderungen an eine patentfähige Erfindung %(q:Neuheit, Erfindungshöhe und Gewerblichen Anwendbarkeit) standhalten.  Nach dem von den Deutschen im Mai 2004 errungenen %(q:Kompromiss) muss dieser %(q:technische Beitrag) nun entgegen den üblichen Gepflogenheiten des EPA auch %(q:neu und nicht naheliegend) sein.  Damit wird der %(q:technische Beitrag) erneut dem Begriff %(q:Erfindung) angenähert, von dem das EPA ihn zu trennen versuchte.  Die Bedeutung dieser Neuerung ist wegen ihrer fehlenden Verankerung in dem hier kodifizierten EPA-System unklar.

#hen: Das Parlament setzt %(q:technischer Beitrag) im Geiste der traditionellen Patentrechtsprechung, wie die deutschen Gerichte sie entwickelt und z.T. bis heute beibehalten haben, mit %(q:Erfindung) gleich.  Es muss demnach ein Beitrag geleistet (= eine Lehre offenbart) werden, die technisch und innovativ zugleich sein muss.  Kommission und Rat hingegen wenden die Pension-Benefits-Doktrin des EPA von 2000 an, wonach es genügt, wenn irgendwo im Anspruchsbereich an einer Stelle etwas neues und an einer anderen etwas technisches zu finden ist.  Damit ist jeder Anspruch auf ein Verfahren zum Betrieb einer Datenverarbeitungsanlage (= Programm für Datenverarbeitungsanlagen) von vorneherein eine %(q:Erfindung), die %(q:auf einem Gebiet der Technik liegt).  Dies bringt der Rat im November 2002 in Erwägung 12 zum Ausdruck:

#aWW: Die Rats-Arbeitsgruppe sagt in dem oben zitierten Erwägungsgrund 12 genau das, was die Kommission in ihrem Artikel 3 sagt, dessen Streichung die Rats-Arbeitsgruppe empfiehlt:

#RiW: Der einzige Unterschied zwischen Rat-Erwägungsgrund 12 und Kommissions-Artikel 3 besteht darin, dass ersterer die zentrale Aussage in einen unauffälligen %(q:Obwohl)-Nebensatz versteckt und mit belangloser Begrenzungsrhetorik umhüllt.

#esK: Die Bedenken der französischen Delegation wurden vom Europäischen Parlament geteilt.  Der vom Rats inspirierte Ansatz der Berichterstatterin Arlene McCarthy führte im Rechtsausschuss zu Glaubwürdigkeitsverlust und im Plenum zur Niederlage.

#Rkt: Man hätte nun erwarten können, dass die Ratsarbeitsgruppe die Bedenken der französischen Delegation aufnimmt und wenigstens die Konjunktion %(q:obwohl) zu %(q:selbst wenn) ändert.  Fehlanzeige: die Version vom Januar 2004 enthält die alte Formulierung --- ohne Erwähnung der Bedenken.

#dnG: Die Vorstellung, dass man die Technizität als eigenständiges Kriterium abschafft und dann im Rahmen der Erfindungshöheprüfung Technizitätsüberlegungen mit einfließen lässt, ist schwer zu vermitteln und mit der Systematik des EPÜ ebenso wie des TRIPs-Abkommens kaum vereinbar.  Ähnlich wie das Wunschdenken von der %(q:deklaratorischen Natur von Programmansprüchen) (s. oben) ist diese Vorstellung des Rates darauf angelegt, durch die Rechtspraxis der letzten Reste einer eventuell vorhandenen Bedeutung beraubt zu werden.  Die Vermengung von Technizität und Erfindungshöhe wird dem EPA seit Jahren von zahlreichen Rechtsexperten zum Vorwurf gemacht.  Zu den Kritikern der vom Europäischen Parlament bestellten Rechtsgutachter %(bh:Bakels und Hugenholtz), des Vorsitzenden des BGH-Patentsenats Richter Klaus J. %(km:Melullis) ebenso wie die %(us:Regierung der USA), deren Patentjuristen hierin eine Verletzung von TRIPs sehen.  Der Patentjurist Ralf Nack, der von der Bundesregierung mit Gutachten beauftragt und von der Kommission in ihrem Entwurf zitiert wurde, kommentierte die Lehre von der %(q:Technizitätsprüfung im Rahmen der Erfindungshöheprüfung) mit dem Satz %(q:Das Chaos ist nicht mehr zu überbieten) und forderte in seiner Dissertation, der Begriff %(q:technischer Beitrag) müsse als Synonym von %(q:Erfindung) erkannt werden und Art 52(2) EPÜ müsste als eine Liste von Gegenständen verstanden werden, die %(q:keine technischen Beiträge im Sinne des Patentrechts) darstellen.

#gfy: Der Begriff %(q:Computer-Implementierte Erfindung) dient seiner (von EPA 2000 und Kommission 2002 gelieferten) Definition nach als Euphemismus für %(q:Programme für Datenverarbeitungsanlagen) (Anweisungen zum Rechnen mit den herkömmlichen Elementen eines Datenverarbeitungssystems), die laut Gesetzeslage bekanntlich nicht patentierbar sind.

#xrW: EPA und Kommission definiert %(q:computer-implementierte Erfindungen) so, dass nur Programme für Datenverarbeitungsanlagen umfasst sind.  Die Neuheit des beanspruchten Systems muss in der Art liegen, wie es programmiert ist.

#uht: Das war der Rats-Arbeitsgruppe zu deutlich.  Sie plädiert heute ebenso wie im November 2002 für eine Definition, die sowohl reine Programme (Datenverarbeitungsverfahren) als auch Lehren zur programmgesteuerten industriellen Nutzung von Naturkräften umfasst:

#hte: Das Parlament hingegen hat den Begriff spiegelbildlich zur Kommission definiert:  er umfasst nunmehr nur noch Lehren zur programmierten Nutzung von Naturkräften:

#nfg: Hiermit wird deutlich gesagt, dass der Computer und die Algorithmen, die zur Steuerung einer technischen Erfindung eingesetzt werden, nicht technischer Natur sind.  Eine Erfindung könnte, etwa beim ABS, in der Art liegen, wie Bremskräfte eingesetzt werden.

#ahe: The Council does not want to decide what the Orwellian word %(q:computer-implemented invention) should refer to: programs for computers as such (Commission proposal) or computerised technical inventions (Parliament proposal).   This may seem politically clever, but it has grave consequences for legal practise.  Under the Council version, it is up to the patent attorney to decide whether a process should be treated as a %(q:computer-implemented invention), i.e. whether the special regime of the directive should apply to it.  Any patentable process must be repeatable and will therefore in practise run automatically, i.e. under computer control.  If the patent attorney puts this feature into his claim, the Council directive applies with all its special provisions, which deviate considerably from normal patent law.  E.g. according to Art 5(2) it becomes possible to claim the program without the process hardware.  Thus a turing-complete description of a chemical process will be claimable, regardless of whether the program logic is novel or not.  Such a tendency can indeed be observed today already.  It allows patent applicants to control secondary programming and publishing markets.  The principle that patents should serve de diffusion of technical information is thereby discarded on a systematic basis, not only for the special case of software patents but for all patents.

#mts: Wir haben bereits mehrfach rhetorische Kniffe aufgezeigt, mit denen das Ratspapier den unerfahrenen Leser hinters licht führt.

#egr: Eine tabellarische Auflistung einiger dieser Kniffe haben wir im Sommer 2003 anhand der später vom Plenum niedergestimmten JURI-Änderungsvorschläge %(cj:analysiert).  Diese Vorschläge lehnten sich eng an das Ratspapier vom November 2002 an und stammen vermutlich aus der selben Quelle.

#ebw: Eine ähnliche tabellarische Analyse der zahlreichen Scheinbegrenzungs-Bestimmungen des Ratspapiers wäre hier noch zu liefern.

#ute: The most frequently used rhetocial ploy works as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always met.

#rWn: Die Mitglieder der Rats-Arbeitsgruppe haben gegen jede mögliche Begrenzung der Patentierbarkeit und Patentdurchsetzbarkeit Stellung bezogen und damit eine extreme Gegenposition zum Votum des Parlamentes vom September 2004 eingenommen, mit der alle Wünsche der Patentabteilungen der Großunternehmen erfüllt werden.  In einigen Punkten hat die Ratsarbeitsgruppe frühere Positionen weiter verhärtet.  Durch das Pochen auf Programmansprüchen, die den eigenen Aussagen der Arbeitsgruppe zufolge nur formale Bedeutung haben, setzt die Rats-Arbeitsgruppe der Suche nach Kompromissen zwischen unterschiedlichen Interessen von vorneherein enge Grenzen.  Hieraus ergeben sich Fragen nach der Motiviation der Arbeitsgruppenmitglieder.

#dnd: Die Gruppenmitglieder treffen sich sowohl in der Rats-Arbeitsgruppe als auch im Verwaltungsrat des Europäischen Patentamtes regelmäßig.  Sie stellen durch Personalunion das Band zwischen der EU und der Europäischen Patentorganisation (EPO) dar.  Innerhalb der EPO sind sie Herren der Gesetzgebung.  Sie sind ungern bereit, ihre angestammte Domäne im Rahmen der EU mit anderen zu teilen.  Sie rütteln nur sehr ungern an dem gewachsenen Konsens, der ihre politische Stärke ausmacht.  Eine Beteiligung der EU ist für die Herren der Gesetzgebung nur insoweit akzeptabel, wie die EU ihren Vorgaben folgt.  Vorgebliche Ziele wie %(q:Harmonisierung) erweisen sich als bestenfalls zweitrangig.   Notfalls lässt man gerne die EU-Richtlinie platzen und ändert in den Hinterzimmern einer neuen Regierungskonferenz den ungeliebten Art 52 EPÜ.  Frits Bolkestein %(sp:sprach) dieses Geheimnis am 23. September erstmals offen vor dem EP aus.  Es ergibt sich darüber hinaus aus zahlreichen öffentlichen und halb-öffentlichen Stellungnahmen aus dem Umfeld der Europäischen Patentorganisation.

#riA: In dieser Situation dürfte es nunmehr unmöglich sein, sowohl die politischen Besitzstände der EPO-Verwalter zu wahren als auch die Patentgesetzgebung der EU voranzubringen.  Die Mitgliedsstaaten müssen sich entscheiden.

#tnW: explains fairly concisely why the Council's %(q:compromise document) is the worst of all proposals on software patents that have come out of the Brussels process.

#spW: Council Presidency 2004-03-17 %(q:Compromise Paper) on Software Patent Directive

#hel: published 2-3 weeks later on the consilium site in English and German, with all information about positions of national delegations deleted.

#til: latest public version on Council website

#nmk: Council Document 2004-05-25

#tWs: latest published version

#lcW: HTML converted from the original MSWord document with OpenOffice.

#cgW: Council Working Document ST09051

#ebn: a recent unpublished version with footnotes about positions of national delegations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: cons0401 ;
# txtlang: cs ;
# multlin: t ;
# End: ;

