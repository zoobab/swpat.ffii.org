<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Conseil de l'UE 2004 : Proposition sur les brevets logiciels

#descr: La présidence irlandaise du Conseil de l'UE a distribué aux
représentants des gouvernements un papier contenat des suggestions
alternatives aux amendements à la directive %(q:sur la brevetabilité
des inventions mises en oeuvre par ordinateur) votés par le Parlement
européen (PE). Contrastant avec la version du PE, la version du
Conseil autorise une brevetabilité illimitée et le respect légal des
brevets. Selon la version actuelle, les algorithmes %(q:mis en oeuvre
par ordinateur) et les méthodes pour l'exercice d'activités
économiques seraient des inventions au sens du droit des brevets et la
publication d'une description fonctionnelle d'une idée brevetée
constituerait une infraction au brevet. Les protocoles et les formats
de données pourraient être brevetés et ne seraient alors plus
librement utilisables même dans un objectif d'interopérabilité. Ces
conséquences peuvent ne pas sauter aux yeux d'un lecteur non concerné.
 Nous tentons ici de déc  hiffrer le language opaque de la proposition
et d'expliciter ses conséquences.

#ltp: Conflit à la place de compromis

#kWr: Publication de programmes non autorisée, démolition de tous les ponts

#eiW: Aucune divulgation de programme : un pas de plus s'éloignant du
Parlement

#bea: Interopérabilité non autorisée avec les standards brevetés

#tng: Brevetabilité limitée par des termes dénués de sens, définitions
supprimées

#tef: Mélange du caractère technique avec la non évidence

#edj2: %(q:Invention mise en oeuvre par ordinateur) enlargi à n'importe quel
procédé brevetable

#bra: Fausses limites, ambiguïtés tarabiscotées

#ien: Destruction délibérée du projet de directive européenne ?

#ruo: Le %(q:document de travail) de la présidence irlandaise reflète l'état
des négociations atteint au niveau du %(q:Groupe de travail du Conseil
sur %(tp:la Propriété intellectuelle|les Brevets)), abbrégé ci-dessous
en %(q:Groupe de travail sur les brevets) ou %(q:Groupe), lors de sa
dernière réunion à Bruxelles le 27/11/2003. Excepté quelques détails
de rédaction, ce document est identique à celui du Groupe de novembre
2002.  De toutes les propositions connues jusqu'ici, les documents de
travail du Groupe expriment l'approbation la plus large d'une
brevetabilité illimitée.  Toutefois, la radicalité est souvent cachée
dans les expressions et enveloppée dans une rhétorique creuse à propos
des limitations, de telle sorte que les lecteurs inexpérimentés et
inattentifs passent facilement dessus.

#utd: Quelques propos du %(ju:raport de juin 2003 de la commission
parlementaire JURI aux affaires légales) ont été repris, ne faisant
que confirmer la ligne générale d'une brevetabilité illimitée.  Le
rapport de la commission JURI était lui-même basé sur le %(dk:document
de novembre 2002 du Groupe de travail du Conseil) et ignoraient en
grande partie les recommendations des %(ci:deux autres commissions
parlementaires impliquées, CULT et ITRE).  Le Parlement, au contraire,
a décidé en scéance plénière de suivre les commissions CULT et ITRE en
limitant la brevetabilité et de réprimander la commission JURI (et par
conséquent le Groupe de travail du Conseil) autant que nécessaire. 
Avec le vote en plénière du 24/09/2003, les parlementaires européens
ont remplacé les fausses limites par des vraies.  Les formulations de
la commission JURI ont été retenues partout où elles n'empêchaient
aucune limitation véritable.  Maintenant, le Groupe de travail du Co 
nseil se range aux côtés de son allié, la commission JURI, et ignore
les amendements en plénière du Parlement européen et ceux des
commissions CULT et ITRE.  Plus encore, certaines des positions les
plus critiques envers les brevets de la part de délégations
individuelles du Groupe en novembre 2002, ressemblant aux positions
votées par le Parlement en septembre 2003, ont été supprimées dans la
nouvelle version du Groupe.  Appeler cette version un %(q:document de
compromis), comme l'a fait la présidence irlandaise, est par
conséquent hautement fallacieux.  Le précédent %(q:document de
compromis) de 2002 n'était déjà pas parvenu à exprimer les intérêts
entre lesquels un compromis avait prétendument été trouvé.  Cette fois
encore, les parties du compromis et leurs intérêts respectifs ne sont
pas exposés mais puisque les modifications sur lesquels l'accent a été
mis par rapport à la version précédente proviennent toutes du
Parlement européen, i  l semble à première vue que ce document est
destiné à atteindre un compromis entre le Conseil et le Parlement. 
Toutefois, en y regardant de plus près, il apparaît que le Conseil
s'éloigne du Parlement plutôt qu'il ne s'en rapproche.

#nie: Dans la suite de notre analyse, nous allons éclaircir ceci en
comparant les proposition du Conseil avec celles du parlement à propos
de questions centrales faisant débat.

#gsW: Les revendications sur les programmes sont des revendications de
brevets du style

#ben: un programme d'ordinateur, avec lequel, chargé dans la mémoire d'un
ordinateur,  est exécuté le procédé de calcul selon la revendication
N.

#oWs: La formulation %(q:programme d'ordinateur) ci-dessus peut être
remplacée par %(q:programme sur un disque), %(q:produit d'un programme
d'ordinateur), %(q:structure de données), %(q:support lisible par un
ordinateur) ou d'autres variantes.  Le tout est que l'objet de la
revendication ne soit plus un produit matériel ni un procédé mais un
composite de données, d'informations ou d'autres entités abstraites,
éventuellement combinées à un support non précisé et non inventif.

#Wtt: Ce type de revendication est apparu en Europe en 1998 lors de deux
décisions de la chambre de recours technique de l'Office européen des
brevets.  Aux USA également, il n'a été introduit par l'Office des
brevets que récemment : après les auditions de 1994 (qui ont révélé
une opposition écrasante aux brevets logiciels).  Les revendications
de programme sont controversées car

#dyr: Les revendications de programmes rompent avec la logique
traditionnelle du droit des brevets, selon laquelle des objets
physiques sont revendiqués en échange d'une révélation d'objets
informationnels, i.e. l'information est du côté des révélations pas du
côté du monopole dans le contrat induit par les brevets ;

#tvW: Les revendications de programmes couvrent littéralement des programmes
d'ordinateur en tant que tels et sont ainsi difficilement justifiables
sous aucune interprétation sensée de l'article 52 de la Convention sur
le brevet européen (CBE);

#nce: Les revendications de programmes vont littéralement à l'encontre de la
liberté de publication telle que la stipule l'article 10 de la
Convention européenne sur les droits de l'homme (CEDH)

#r1s: Les monopoles de brevets ne font pas partie de ces droits qui peuvent,
selon l'article 10(2) de la CEDH, justifier une limitation de la
liberté de publication

#reo: Les revendications de programmes visent directement aux développeurs
et aux distributeurs de logiciels aussi bien qu'aux
%(tp:intermédiaires : par ex. les fournisseurs d'accès Internet) avec
les armes lourdes des contentieux de brevets, en les traitant comme
des producteurs et des distributeurs de produits industriels.

#cgd: Les revendications de programmes ne désservent aucun but pratique : il
serait plus honnête de poursuivre le marketing des biens
informationnels pour %(e:infraction annexe) et de régler à part les
modalités de ce type d'infraction, en prenant en consideration les
spécificité de l'économie informationnelle.

#dea: La proposition de la Commission européenne déconseille les
revendications de programmes.

#leW: Le Parlement européen (commissions CULT et ITRE et session plénière)
suit la Commission sur ce point et, en outre, avec son nouvel article
7 paragraphe 3, stipule que la liberté de publication ne pouvait être
limitée par des revendications de brevet :

#ing: Le Groupe de travail du Conseil refuse les propositions de la
Commission (CEC) et du Parlement (PE) et, avec son article 5.2,
insiste sur les revendications de programmes :

#ise: La formulation est trompeuse à différents égards :

#crW: La clause %(q:n'est autorisée) ne limite rien du tout.  N'importe
quelle revendication de programme peut être reformulée en tant que
revendication de procédé ou de produit dont cette revendication de
programme est alors prétendue dépendre.

#loe: voir ci-dessus

#iWe: La locution %(q:que si) est fallacieuse.  Elle ne remplit aucun rôle
de régulation.  Essayez de relire la phrase sans cette locution -- et
sans la clause dénuée de sens %(q:n'est que si ...) !

#tph: It should be noted that the german version deviates significantly from
the english and french one.  %(q:put into force) and %(q:mettre en
oevre) (make work in practise) is worlds apart from %(q:begründen)
(provide a rationale for).  We base our analysis on the english and
french version.  It would have to be partially rewritten for the
german one which, in a Freudian error, seems to directly admit that it
is the presence of a program which provides the rationale for
patenting.

#rnp: L'existence de revendications de programmes modifie la logique du
droit des brevets et met le logiciel au centre plutôt qu'à la
périphérie du système.

#cnr: Le Groupe de travail du Conseil revendique qu'il ne voit les
revendications de programmes %(q:uniquement) comme des revendications
dérivées, %(q:d'une nature purement déclaratoire), comme il a été dit
dans les justifications des amendements correspondants de la
commission JURI.  Ceci ne peux être vrai, car

#rcg: De toute revendication de programme, une revendication équivalente de
procédé peut être dérivée.  Cela ne fonctionne pas en sens inverse.

#aoc: Une revendication de programme est %(tp|plus large|i.e. moins
spécifique) que la revendication de procédé correspondante.  La
revendication de procédé spécifie non seulement la structure de
données mais également une utilisation de cette structure de données
(par ex. l'exécution par un ordinateur, voire par un équipment
spécialisé).

#Ebn: La revendication la plus large dans une spécification de brevet (i.e.
les revendications avec le moins de fonctionnalités) décrit
habituellement le plus précisément la contribution à l'état de l'art. 
La %(q:contribution technique) d'un brevet logiciel se trouve dans la
revendication de programme, alors que les fonctionnalités
supplémentaires de %(q:l'exécution en mémoire sur un odinateur) est
seulement de nature déclaratoire.

#Wet: Il est toujours possible de formuler des revendications indépendantes
comme des revendications %(tp|dépendante|déclaratoires) et vice versa.
 De tels exercices de formulation ne font qu'embrouiller les relations
de dépendance.

#Wsg: Les Directives relatives à l'examen de 1978 du Bureau européen des
brevets explique que l'exclusion des %(q:programmes d'ordinateur) de
l'article 52(2) CBE comme ceci :

#rdr: Ici aussi l'OEB suppose qu'une %(q:revendication au programme en tant
que tel ou au programme enregistré sur bande magnétique) est la forme
normal d'une revendication de logiciel.  Cependant, il souligne
également que le même objet peut être déclaré en tant que %(q:procédé
faire fonctionner un .. ordinateur) ou en tant que %(q:ordinateur,
caractérisé par ..) et qu'il faut s'attendre à de telles tentatives
pour obtenir des brevets logiciels avec des redéclarations.

#rww: Alors que les directives initiales de l'OEB présentaient correctement
la redéclaration de programmes en procédés comme étant une astuce
declaratoire, ses serviteurs supposent aujourd'hui que non seulement
cette astuce est admissible mais que la redéclaration à partir de là
des programmes en tant que tels doit également être admissible,
puisqu'il ne s'agit que d'une redéclaration.

#mis: Avec la brevetabilité des programmes d'ordinateur, il s'en suit, pour
parler comme l'OEB en 1978, que %(e:la contribution à l'état de l'art
doit seulement résider dans un programme d'ordinateur), i.e. que la
%(q:contribution technique) peut être confinée au calcul avec les
entités abstraite d'une machine virtuelle comme le Calculateur
Universel de Von Neumann (sur lequel se basent tous les ordinateurs
modernes).  On est ainsi assuré que les concepts légaux indéterminés
avec lesquels le Groupe de travail du Conseil veut limiter la
brevetabilité (voir ci-dessous) ne pouront jamais signifier de
véritables limites.  L'exclusion des méthodes pour l'exercice
d'activités économiques doit également échouer sous de telles
conditions, car les méthodes pour l'exercice d'activités économiques
sont des concrétisations des règles sur le traitement des données.

#rrr: En traitant les objets informationnels comme des biens matériels, le
Groupe de travail du Conseil empêche également toute discussion sur
les limitations de la brevetabilité par respect non seulement de la
liberté de publication mais également de toute liberté pouvant être
trouvée dans la nature informationnelle de l'objet.

#idK: The situation is made only worse by the insertion of dissimulating
recitals, such as 7a:

#Wtw: The unexperienced reader (e.g. a minister) will believe that this
excludes program claims.  Unfortunately the opposite is true.  In
connection with Art 5(2) the recital says that a claim to %(q:program,
characterised by [its functions]) is not a claim to a program as such.
 As an explanation of the apparent contradiction it suggests that a
%(q:program as such) is to be construed as the %(q:expression) aspect
of a program (rather than the function aspect) .  I.e. a narrow claim
to %(q:program, characterised by that the text in listing 1 is used)
is to be rejected as a claim to individual expression, whereas a broad
claim to a %(q:program, characterised by that it [does what the one in
listing 1] does) is allowed.  Nobody is interested in narrow claims. 
A claim to one particular listing would merely constitute an weak form
of copyright which nobody wants and to which nobody objects.  The
purpose of raising the non-issue of %(q:expression patents) in the
Council's proposal is not to regulate something but to confuse the
legislator about what is being regulated.

#br2: Dans le document de travail de novembre 2002 du Conseil l'article 2.4B
est rédigé comme suit :

#hhw: L'article 7 du Parlement européen est encore plus explicite sur le
fait que le système de brevets devrait promouvoir la publication des
textes des programmes comme faisant partie de la divulgation au lieu
de l'interdire comme faisant aprtie du monopole :

#egm: Le document de travail du Conseil s'est ainsi éloigné du compromis
avec le Parlement.

#i0l: L'article 6 de la Commission européenne propose une limitation dénuée
de sens de l'application en justice des brevets, limitation que le
Conseil a réduit plus encore :

#moi: Cette disposition autorise la décompilation de programmes brevetés, un
acte de recherche dans la sphère privée qui n'est de toute façon pas
couvert par les brevets.  Des brevets peuvent cependant encore peser
sur les programmes résultant d'une telle recherche et l'article 6
implique que de tels brevets peuvent être appliqués en justice contre
les programmes, même lorsque ces programmes ne sont employés que dans
un but d'interopérabilité.  Ainsi, l'article 6 prétend protéger
l'interopérabilité mais accomplit en fait l'inverse : il s'assure que
personne ne peut employer des standards de communication encombrés
d'un brevet sans autotisation du titulaire du brevet.

#tKa: Le Parlement européen ne s'est pas laisssé tromper.  Les trois
commissions parlementaires et la session plénière ont voté pour un
réel privilège d'interopérabilité :

#Wtr: Le document de travail du Conseil rejette le vote du Parlement et
persiste à assurer la domination des titulaires de brevets sur les
standards de communication.

#sWt: Lorsqu'un format de fichier peut être couvert par des brevets, il
devient impossible pour les concurrents de le convertir vers un autre
format.  Ceci renforce les tendances monopolistiques sur le marché du
logiciel.   Les Consortiums de standardisation comme le W3C
(responsable des standards du world wide web) sont régulièrement
ralentis par des tentatives des propriétaires de brevets d'imposer des
taxes de brevet à un standard.  Les standards brevetés et taxés
imposent de lourdes charges sur la gestion du standard lui-même et sur
les logiciels s'y conformant.  Le logiciel libre ne peut en général
s'y  plier. Lorsque qu'un éditeur domine seul un standard avec des
brevets, les problèmes empirent.  Microsoft pousse d'ores et déjà les
brevets sur le marché avec des conditions de licence qui excluent le
logiciel libre.  Même les procédures concurrentielles en cours de la
Commission europénenne contre Microsoft ont jusqu'ici échoué à changer
ceci.  Le dr  oit de la concurrence seul, sans dispositions
supplémentaires telles que celle de l'article 6a, est hors de portée
des acteurs classiques du marché et offre des remèdes insufisants
contre les pratiques anti-concurrentiels avec lesquelles la domination
des standards de communication tend à être associée.

#tWW: Dans sa mise à jour du 17 mars, le document va encore plus loin. Une
nouvelle reformulation du considérant 17 indique que le droit à
l'interopérabilité ne doit pas être régulé au sein du droit des
brevets mais uniquement dans le droit anti-concurrentiel.

#Wde: En agissant ainsi, le Groupe de travail donne l'inpression qu'il prend
en copte le droit à l'interopérabilité. En y regardant de plus près,
il s'avère cependant qu'on aboutit à l'effet exactement inverse : à
moins de se se lancer dans des procédures juridiques coûteuses,
longues et rarement couronnées de succès, les créateurs de logiciels
ne doivent pas, d'après le Groupe de travail sur les brevets du
Conseil, être autorisés à interopérer.

#uaW: Par ce durcissement de sa position, le Groupe de travail du Conseil
répond à la proposition suivante de la délégation luxembourgeoise :

#Wnb: Les délegations trouveront ci-dessous une proposition de la délégation
luxembourgeoise sur une clause d'interopérabilité (nouvel article 6
bis) à insérer dans le texte de la proposition de Directive. Le texte
proposé se fonde sur l'amendement 76 du Parlement européen mais est
plus restrictif en cela qu'il prend l'exemple de l'amendement
originale et l'emploie comme étant la seule exception acceptable pour
garantir l'inetropérabilité. Cette reformulation plus stricte devrait
rendre la disposition complètement en phase avec l'article 30 des
ADPIC.

#EWe: Cette proposition correspond à l'article sous la forme dans laquelle
il a été aprouvé par les commissions parlementaires CULT, ITRE et
JURI. Il évite le terme de %(q:une fin significative), qyui avait été
introduit pour la première fois lros du vote en plénière. Ainsi, la
délégation luxembourgeoise répond complétement aux %(kk:critiques de
la Commission) et à l'exigence du test en trois étapes (exceptions
limitées) de l'article 30 des ADPIC. En refusant la proposition
luxembourgeoise et avec, celle des trois commissions parlementaires,
le Groupe de travail sur les brevets démontre que l'article 30 des
ADPIC n'est pas sa préoccupation première. On doit plutôt penser que
ce qui guide l'intransigeance du Groupe de travail sur les brevets du
Conseil, ce sont davantage les intérêts à la recherche de profit
associés aux standards de communication, comme cela a été récemment
défendu au nom de PDG de %(te:cinq grosses entreprises européennes de
tél  écommunication).

#efW: Dans les droit des brevets européen, les inventions brevetables sont
limités par deux moyens :

#edj: Une définition négative : certains types de réalisation sont jugés ne
pas êtres des inventions au sens de l'article 52(2) CBE.  Parmis ces
réalisations, on trouve celle dans les domaines des mathématiques, de
la programmation et des activités économiques.

#fsu: Une définition positives : les cours de tribunaux en Allemagne et
ailleurs ont définit une %(q:invention techniquen) comme une
%(q:solution à un problème utilisant les forces contrôlables de la
nature) ou des définitions similaires et ont ainsi généralisé la
listes des définitions négatives de l'article 52(2) CBE.

#ein: La Commission et le Conseil rejettent toute définition de ce qu'est
une invention brevetable, tant dans un sens négatif que positif.  D'un
côté, La Commission l'articule autours de concepts abstraits tels que
la %(q:contribution technique), de l'autre elle refuse de définir ces
concepts et n'offre que des définitions tautologiques.

#eWn: Le Parlement a proposé à la fois des définitions positives et
négatives.

#eai: Négative

#oii: Positive

#iNe: Positive et negative conjuguées

#nWg: Le Conseil insiste sur ses non-définitions et, de plus, affirme
clairement que le concept d'invention technique, même s'il est défini
par un tribunal, ne doit pas servir à limiter la brevetabilité.

#ecb: Le Conseil affirme qu'il suffit qu'il y ait quelque chose de technique
dans la portée de certaines des revendications.  Les revendications
sans aucune composante technique, telles que les revendications de
programmes, sont possibles.  Plus encore, le Conseil déclare que la
contribution technique, i.e. la solution nouvelle qui justifie
l'obtention d'un brevet, peut résider uniquement dans des
caractéristiques non techniques.   Il suffit que de nouvelles
%(tp|règles de calcul|algorithmes) soient combinées avec un
%(tp|équipement connu|ordinateur universel) pour résoudre un
%(q:problème technique), i.e. pour réduire le nombre de clics de
souris nécéssaire.  Une telle %(q:solution à un problème technique)
sera appelée  %(q:contribution technique), indépendamment du fait que
la solution soit elle-même technique.  La %(q:contribution technique)
n'a même pas besoin d'être nouvelle ou non évidente.  Il suffit qu'un
%(q:problème technique) puisse être établ  i et vaguement associé
d'une manière ou d'une autre avec %(q:l'activité inventive).

#hen: Le Parlement assimile %(q:contribution technique) à %(q:invention). 
Il s'agit de l'usage courant des termes trouvé dans les plus anciens
manuels de droit des brevets ainsi que dans la plupart des procès de
nos jours.   En conséquence, une %(e:invention) est une contribution
des connaissances techniques à l'état de l'art, appelé également
%(q:enseignement technique) ou %(q:contribution technique).  Les
revendications de brevets doivent refléter l'invention et en même
temps définir la porté de l'exclusivité justifiée ainsi.  De ce fait,
certaines des caractéristiques revendiquées pourraient décrire une
contribution non technique (par ex. un algorithme innovant), alors que
certaines caractéristiques supplémentaires revendiquées peuvent être
techniques (par ex. %(q:charger le programme en mémoire et
l'exécuter)), mais sans décrire d'innovation. Elles servent plutôt à
limiter une portée d'exclusion.  Puisque un juriste des brevets est
libre de définir n'importe quelle portée d'exclusion pourvu que cette
portée contienne la contribution, il est toujours possible et de fait
presque inévitable que la portée d'exclusion contienne des
caractéristiques techniques.  Ainsi en %(q:exigeant) que la portée des
revendications %(q:doive inclure des caractéristiques techniques), le
Groupe de travail du Conseil ne pose aucune limite.  Plus encore, il
implique que la %(q:contribution technique) puisse être composée
uniquement de caractéristiques non techniques.  Par ex. l'algorithme
non technique peut compter comme une contribution technique, si la
portée de la revendication dans son ensemble contient des
caractéristiques techniques.  Ceci conduit de surcroît à la conclusion
que, comme il est écrit dans le considérant 12 du Groupe de travail,
tous les objets %(q:mis en oeuvre par ordinateur) revendiqués, y
compris les algorithmes, les méthodes pour l'exercice d'activités
économiques, etc., %(q:font partie d'un domaine technique) :

#aWW: Ce considérant 12 dit exactement la même chose que la Commission
européenne dans son très contreversé article 3, dont la suppression
est recommandée par le Groupe de travail du Conseil :

#RiW: La seule différence entre le considérant 12 du Conseil et l'article 3
de la Commission réside dans la forme.  Alors que la Commission dit ce
qu'elle veux sans prendre de gants, le Conseil dissimule ses
intentions derrière la locution %(q:bien que) et les enveloppe dans
une rhétorique dénuée de sens à propos des limitations.

#esK: La délégation française au Conseil a rejeté les conséquences du
considérant 12, comme l'a fait le Parlement européen.  Lorsqu'Arlene
McCarthy a essayé de rejouer le tour de passe passe à la commission
JURI, sa crédibilité en a souffert. Lorsque la commission JURI a
soutenu ce tour avec une majorité de 2/3, il était promis à la défaite
en scéance plénière.

#Rkt: Ceci a pu amené certaines personnes à penser que le Groupe de travail
sur les brevets du Conseil retiendrait la leçon et suivrait au moins
la recommandation de la France.  Malgré tout, le contraire s'est
produit :  la vieille tactique perdante est maintenue et les réserves
de la délégation françaises supprimées.

#dnG: Selon la doctrine de 2000 de l'OEB sur les caisses de retraites
(Pension Benefits), qui est à la base des propositions de la
Commission et du Conseils, tout ce qui tourne sur un ordinateur est
une invention technique.  Afin de pouvoir néanmoins rejeter certaines
revendications de brevets sur des méthodes pour l'exercice d'activités
économiques, l'OEB affirme que la condition de technicité est d'une
façon ou d'une autre implicite dans la condition de non évidence. 
Très peu de gens comprenne ce que cela signifie et même les juristes
des brevets se pleignent régulièrement qu'ils n'arrivent pas à
comprendre les instructions ésotériques de l'OEB sur ce point.  Parmis
les détracteurs se trouvent des savants et des juges réputés, dont
certains peuvent difficilement être suspectés d'être des opposants à
la brevetabilité du logiciel.  Le représentant des États-Unis pour le
commerce extérieur %(us:a fait remarqué) que cette doctrine doctrine
de l'OEB violait le système de l'article 27 des ADPIC.  L'exposé des
motifs de la Commission, le 20/02/2000, commente : %(orig:Comme
l'indiquent le considérant 11 et l'article 4, la présence d'une
%(q:contribution technique) doit être évaluée non pas en relation avec
la nouveauté mais en tant qu'activité inventive. L'expérience montre
que cette démarche est plus simple à appliquer en pratique.) 
AUtrement dit : même la Commission reconnaît tacitement que la
doctrine de caisse de retraite est difficile à justifier en théorie. 
Le résultat pratique le plus immédiat de cette doctrine est que les
algorithmes et les méthodes pour l'exercice d'activités économiques ne
peuvent plus être rejetés sans d'abord mener une recherche en
nouveauté coûteuse.  Puisque plusieurs offices nationaux des brevets
(par ex. : l'Office de brevets français) ne mènent pas de recherche en
nouveauté, il ne peuvent plus rejeter de brevet pour manque de
contribution technique, pas même selon les critères laxistes proposés
par cette directive.  Plus encore, la doctrine des caisses de retraite
crée une confusion maximale et une insécurité juridique autour du
concept de contribution technique, de telle sorte que les tribunaux
faisant autorité en matière de brevets ne peuvent plus rendre de
comptes en se basant sur une quelconque loi.

#gfy: Le terme %(q:invention mis en oeuvre par ordinateur) a été introduit
et définit par l'OEB en 2000 comme une euphémisme pour %(q:programmes
d'ordinateurs) (règles pour le fonctionnement d'éléments
conventionnels d'un système de traitement de données, tels qu'un
processeur, une unité de stockage, des entrées/sorties, etc.), qui ne
sont pas des inventions au sens de droit des brevets.  La proposition
de la Commission est une simple copie de la définition de l'OEB.

#xrW: L'OEB et la Commission sont prudents lorsqu'ils définissent
étroitement %(q:les inventions mises en peuvre par ordinateur), de
telle sorte que seuls les véritables programmes d'ordinateur soient
couverts.  La contribution à première vue nouvelle doit consister dans
un moyen par lequel le système de traitement de données est programmé,
pas dans les procédé techniques périphériques.  Un nouveau moyen de
faire jouer la température sur des pneux d'automobile sous le contrôle
d'un programme ne serait %(e:pas) une %(q:invention mise en oeuvre par
ordinateur) selon la définition de l'OEB et de la Commission.

#uht: La franchise brutale de la Commission est devenue une cible facile de
critique pour beaucoup de gens.  Comme dans le cas de l'article 3, le
Groupe de travail du Conseil n'a rien fait pour changer la doctrine de
la Commission mais s'est plutôt attaché à embrouiller la formulation. 
Ainsi, dans le document du Conseil, à la fois les méthodes de pur
traitement de données et les nouveaux moyens de contrôler les forces
de la nature pourraient être des %(q:inventions mises en oeuvre par
ordinateur).  Les termes du Conseil couvrent tout procédé contrôlé par
ordinateur et de ce fait tout objet revendiqué dans tous les domaines
techniques et non techniques.  On pourrait se demander quel objectif
le terme pourrait encore servir avec une définition aussi large.

#hte: Le parlement a choisi de redéfinir le terme %(q:invention mise en
oeuvre par ordinateur) de telle façon qu'il veuille dire exactement ce
que l'OEB et la Commission ne veulent pas qu'il signifie et qu'il
exclue exactemnet ce qu'il veulent: i.e. il ne se réfère qu'au procédé
technique périphériques, pas aux programmes d'ordinateur en tant que
tels.

#nfg: Ainsi le Parlement formule clairement ce que la Commission et le
Conseil prétendent souvent dire : les algorithmes exécuté par
ordinateur contrôlant un système anti-bloquant ne sont pas
brevetables, alors que le système anti-bloquant dans son ensemble,
avec les mécanisme, la température, les frottements et un nouveau
moyen de les utiliser dans les revendications, est potentiellement une
invention brevetable.  Alors que le travail d'un ingénieur des
procédés est brevetable, celui d'un programmeur ne l'est pas.

#ahe: The Council does not want to decide what the Orwellian word
%(q:computer-implemented invention) should refer to: programs for
computers as such (Commission proposal) or computerised technical
inventions (Parliament proposal).   This may seem politically clever,
but it has grave consequences for legal practise.  Under the Council
version, it is up to the patent attorney to decide whether a process
should be treated as a %(q:computer-implemented invention), i.e.
whether the special regime of the directive should apply to it.  Any
patentable process must be repeatable and will therefore in practise
run automatically, i.e. under computer control.  If the patent
attorney puts this feature into his claim, the Council directive
applies with all its special provisions, which deviate considerably
from normal patent law.  E.g. according to Art 5(2) it becomes
possible to claim the program without the process hardware.  Thus a
turing-complete description of a chemical process will be claimable,
regardless of whether the program logic is novel or not.  Such a
tendency can indeed be observed today already.  It allows patent
applicants to control secondary programming and publishing markets. 
The principle that patents should serve de diffusion of technical
information is thereby discarded on a systematic basis, not only for
the special case of software patents but for all patents.

#mts: Nous avons dénoncé encore et encore les manèges rhétoriques par
lesquels le document du Conseil induisait en erreur les lecteurs
inattentifs.

#egr: Nous avons %(cj:énuméré et analysé) certains de ces manèges durant
l'été 2003, lorqu'ils étaient copiés/collés par Arlene McCarthy et la
commission JURI.

#ebw: Une analyse détaillée des stratagèmes rhétoriques qui envahissent le
document de travail du Conseil doit encore être produite ici.

#ute: The most frequently used rhetocial ploy works as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always
met.

#rWn: Les membres du Groupe de travail du Conseil de l'UE ont durci plus
encore leur précédente position en faveur d'une brevetabilité
illimitée et de la défense des brevets en justice en ce qui concerne
les logiciels.  Ils ont pris ainsi une position à l'extrême opposé de
celle du Parlement européen, ce qui rend toute négociation très
difficile.  Certaines des positions prises par le Groupe, comme son
insistance sur les revendications de programmes, ne servent aucun
intérêt connu si ce n'est celui de rendre les négociations difficiles.
 Ceci soulève des questions sur les intentions du Groupe.

#dnd: Les membres du Groupe se rencontre régulièrement à la fois au Conseil
de l'Union européenne et au Conseil administratif de l'Office européen
des brevets (OEB) qui dirige l'OEB.  Ils établissent un lien personnel
entre l'OEB et l'UE.  Au sein de l'OEB, il sont les maîtres de la
législation.  Ils sont réticents à partager cette chasse gardée avec
quiconque.  La participation de l'UE est la bienvenue si elle signifie
que l'UE va approuver sans discussion la politique du Groupe.  Lorsque
l'UE commence à questionner le bon sens du Groupe, ce dernier coupe
court à toute communication et se rassemble autours des positions que
son autorité a façonnés, i.e. l'infinie sagesse des Chambres de
recours techniques.  Le Groupe serait plus heureux d'abandonner tout à
fait le projet de directive de l'UE et de poursuivre sa ligne au cours
des conférences diplomatiques et autres coulisses de processus
intergouvernementaux où aucun parlementaire et aucune discussion publi
 c n'interviennent.  Frits Bolkestein %(sp:a prononcé) ce secret de
polichinelle en public au Parlement européen la veille du vote en
plénière.

#riA: La situation en est arrivée au point où le monopole du Groupe se met
clairement en travers de l'élargissement des attributions législatives
de l'UE dans les secteurs vitaux de la politique économique.  Les
États membres doivent décider s'il veulent avancer ou reculer.

#tnW: explains fairly concisely why the Council's %(q:compromise document)
is the worst of all proposals on software patents that have come out
of the Brussels process.

#spW: %(q:Papier de Compromis) de la Présidence Irlandaise sur Brevets
Logiciels due 2004-03-17

#hel: publié 2-3 semaines plus tard sur site consilium en version anglaise
et allemande, avec données sur les positions des délégations
nationales éffacés.

#til: dernière version publique sur site web Conséil

#sWW: Translations to the last remaining EU languages appeared around 18th
of August.

#nmk: Council Document 2004-05-25

#tWs: latest published version

#lcW: HTML à partir du document MSWord original.

#cgW: Document de travail du Conseil ST09051

#ebn: a recent unpublished version with footnotes about positions of
national delegations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: cons0401 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

