<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Werkgroep EU-Raad van Ministers 2004 Werkdocument Softwarepatenten

#descr: Het Ierse voorzitterschap van de EU-Raad heeft een document verspreid
onder de verschillende ministeries, dat alternatieven suggereert voor
de amendementen die het Europees Parlement (EP) gestemd heeft voor de
richtlijn %(q:over de octrooieerbaarheid van in computers
geïmplementeerde uitvindingen). De versie van de Raad legt, in sterk
contrast met de EP-versie, absoluut geen beperkingen op wat betreft
octooieerbaarheid en het afdwingen van octrooien. Volgens de huidige
versie zouden %(q:in computers geïmplementeerde) algoritmen en
methoden voor bedrijfsvoering uitvindingen worden in de zin van het
octrooirecht, en de publicatie van een functionele beschrijving van
een gepatenteerd idee zou een patentinbreuk vormen.
Communicatieprotocols en gegevensformaten zouden kunnen gepatenteerd
worden en zouden dan niet vrijelijk te gebruiken zijn, zelfs niet voor
interoperabiliteitsdoeleinden. Deze implicaties zijn echter misschien
niet onmiddellijk duidelijk voor de onbedachtzame lezer. Hier proberen
wij de misleidende taal van het voorstel te ontcijferen en haar
implicaties uit te leggen.

#ltp: Botsing in plaats van Compromis

#kWr: Publicatie van Programma's Niet Toegelaten, Alle Bruggen Verbrand

#eiW: Geen Vrijgave van Programma's: Nog een Stap Verder Weg van het
Parlement

#bea: Interoperabiliteit met Gepatenteerde Standaarden Niet Toegestaan

#tng: Patenteerbaarheid Gelimiteerd door Betekenisloze Termen, Definities
Verwijderd

#tef: Technische Aard Vermengd met het Niet voor de Hand Liggend Zijn

#edj2: %(q:In een computer geïmplementeerde Uitvinding) als Trojaans Paard

#bra: Nietszeggende Beperkingen, Ingewikkelde Dubbelzinnigheden

#ien: Opzettelijke Vernietiging van de EU-Richtlijn?

#ruo: Het %(q:werkdocument) van het Ierse Voorzitterschap geeft de staat
weer van de onderhandelingen op het niveau van de %(q:Werkgroep voor
%(tp|Intellectueel Eigendom|Octrooien) van de Raad), hieronder
afgekort als %(q:Patentenwerkgroep) of %(q:Werkgroep), zoals bereikt
op hun laatste bijeenkomst in Brussel op 2003/11/27. Afgezien van een
paar formuleringen, is dit document identiek aan het document van de
Werkgroep van november 2002. Van alle tot hiertoe bekende voorstellen,
belichaamt het ontwerp van de Werkgroep de meest verregaande
goedkeuring van onbeperkte octrooieerbaarheid. Deze radicaliteit is
echter vaak verborgen in subclausules en omgeven door holle
beperkingsretoriek, zodat onervaren of onbedachtzame lezers er
gemakkelijk kunnen over lezen.

#utd: Een paar formuleringen van het %(ju:rapport van het Committee van
Juridische Zaken JURI van juni 2003) zijn opgenomen, maar deze
bevestigen enkel de algemene lijn van onbeperkte octrooieerbaarheid.
Het JURI-rapport zelf was gebaseerd op het %(dk:werkdocument van de
Werkgroep uit november 2002) en negeerde grotendeels de aanbevelingen
van de %(ci:andere twee betrokken committees, CULT en ITRE). In haar
plenaire bijeenkomst besloot het EP echter CULT en ITRE te volgen door
octrooieerbaarheid te beperken en om JURI terug te fluiten (en
bijgevolg ook de Werkgroep van de Raad) voor zover dit nodig was.  In
de Plenaire Stemming van 2004/09/24 vervingen de Europarlementsleden
nietszeggende beperkingen door echte beperkingen. JURI-formuleringen
werden behouden daar waar zij echte beperkingen niet onmogelijk
maakten. Nu kiest de Werkgroep van de Raad weerom de kant van zijn
bondgenoot, JURI, en negeert de amendementen van het Europees
Parlement en van CULT en ITRE.  Bovendien zijn een aantal kritieken
i.v.m. octrooien van individuele Werkgroepleden uit november 2002, die
gelijkaardig zijn aan stellingen die het Europees Parlement in
plenaire in september 2003 heeft aangenomen, verwijderd uit de nieuwe
versie van de Werkgroep. Deze versie bestempelen als een
%(q:compromisdocument) is bijgevolg uiterst misleidend.  Het vorige
%(q:compromisdocument) uit 2002 gaf niet aan tussen welke belangen een
compromis zou gevonden zijn, en ook deze keer is er geen vermelding
van de betrokken partijen en hun belangen. Vermits de benadrukte
veranderingen ten opzichte van de vorige versie echter allemaal van
het Europees Parlement komen, lijkt het op het eerste zicht dat dit
document een compromis tracht te bekomen tussen de Raad en het
Parlement. Bij nader inzien blijkt het echter dat de Raad verder dan
ooit van het Parlement staat.

#nie: In wat volgt zullen we dit duidelijk maken door de voorstellen van de
Raad over een aantal belangrijke betwiste vragen te vergelijken met
deze van het Parlement.

#gsW: Programmaconclusies zijn octrooiconclusies van de vorm

#ben: een computerprogramma, waardoor na het laden ervan in het geheugen van
een computersysteem, de berekening zoals beschreven in Conclusie N
wordt uitgevoerd.

#oWs: De formulering %(q:computerprogramma) in bovenstaande zin kan
vervangen worden door %(q:een programma op schijf), %(q:een
computerprogrammaproduct), %(q:een gegevensstructuur), %(q:een medium
dat leesbaar is door een computer) of andere varianten. Het punt is
dat het object van de conclusie niet langer een materieel product of
proces is, maar een constructie bestaande uit gegevens, informatie of
andere abstracte entiteiten, mogelijk in combinatie met een niet
verder gespecificeerd en niet-inventief medium.

#Wtt: Dit type van conclusie is ingevoerd in Europa in 1998 door twee
beslissingen van de Technische Raad van Beroep van het Europese
Octrooibureau. In de VS zijn ze ook nog maar recent ingevoerd door het
US Patent Office: dit gebeurde na de hoorzittingen van 1994 (die een
overweldigende tegenstand tegen softwarepatenten aangaven).
Programmaconclusies zijn controversieel vanwege het feit dat

#dyr: Programmaconclusies breken met de traditionele systematiek van het
octrooirecht, volgens dewelke fysische objecten gemonopoliseerd worden
in ruil voor het vrijgeven van %(q:informatie-objecten), i.e.
informatie is wat vrijgegeven wordt, niet wat gemonopoliseerd wordt
door het toekennen van het patent;

#tvW: Programmaconclusies omvatten letterlijk computerprogramma's als
zodanig en kunnen dus nauwelijks worden gerechtvaardigd door eender
welke zinvolle interpretatie van Art 52 EPC;

#nce: Programmaconclusies gaan regelrecht in tegen de vrijheid tot
publicatie zoals gestipuleerd in Art 10 van het Europees Verdrag over
de Rechten van de Mens (ECHR)

#r1s: Octrooimonopolies vallen niet onder die rechten die, volgens Art 10(2)
EHCR, een beperking van de vrijheid tot publicatie kunnen
rechtvaardigen

#reo: Programmaconclusies onderwerpen ontwikkelaars en verdelers van
software, evenals %(tp:tussenpersonen: b.v. Internet service
providers), aan de dreiging van octrooiprocessen, door ze als
producenten en verdelers van industriële producten te beschouwen;

#cgd: Programmaconclusies hebben geen praktisch nut: het zou eenvoudiger
zijn om het aanbieden van informatiegoederen als %(e:indirecte
octrooiinbreuk) te vervolgen en om de modaliteiten van dit type van
inbreuken apart te reguleren, hierbij de bijzonderheden van de
informatie-economie in acht nemende.

#dea: Het voorstel van de Europese Commissie adviseert tegen
programmaconclusies

#leW: Het Europees Parlement (CULT+ITRE+Plenaire) volgt de Commissie wat dit
punt betreft en, met haar nieuw Art 7 (gestemd als Art 5.1d),
stipuleert ze bovendien dat vrijheid tot publicatie niet mag beperkt
worden door octrooiconclusies:

#ing: De Werkgroep van de Raad verwerpt de voorstellen van de CEC en het EP
en dringt aan, met haar artikel 5.2, op programmaconclusies:

#ise: Deze formulering is misleidend op verschillende manieren:

#crW: De %(q:indien)-clausule is op geen enkele manier beperkend. Eender
welke programmaconclusie kan verwoord worden als een proces- of
productconclusie van dewelke deze programmaconclusie kan afhankelijk
gemaakt worden.

#loe: zie hierboven

#iWe: Het voorzetsel %(q:slechts) liegt. Het vervult geen regulerende
functie.  Lees de zin opnieuw zonder dit voorzetsel en zonder de
betekenisloze indien-clausule!

#tph: Het moet opgemerkt worden dat de Duitse versie sterk afwijkt van de
Engelse, Nederlandse en Franse versies. %(q:put into force) en
%(q:mettre en oeuvre) (in de praktijk laten werken) is mijlen
verwijderd van %(q:begründen) (rechtvaardigen). We baseren onze
analyse op de Engelse en Franse versies. Ze zou gedeeltelijk
herschreven moeten worden voor de Duitse die, als het ware een
Freudiaanse fout makens, direct lijkt toe te geven dat het de
aanwezigheid van een programma is dat in de rechtvaardiging voor het
patent voorziet.

#rnp: Het bestaan van programmaconclusies veranderd de systematiek van het
octrooirecht en plaatst software eerder in het midden dan aan de rand
van het systeem.

#cnr: De Werkgroep van de Raad beweert dat het programmaconclusies
%(q:enkel) als afgeleide conclusies ziet, van %(q:pure bevestigende
aard), zoals gezegd in de rechtvaardigingen van de overeenkomstige
JURI-amendementen. Dit kan echter niet waar zijn, vermits:

#rcg: Van elke programmaconclusie kan een equivalente procesconclusie
afgeleid worden. Omgekeerd gaat dit niet.

#aoc: Een programmaconclusie is %(tp|algemener|i.e., minder specifiek) dan
de overeenkomstige procesconclusie. De procesconclusie bepaalt niet
enkel de gegevensstructuur, maar eveneens een gebruik van deze
gegevensstructuur (b.v. uitvoering door een computer of zelfs door
gespecialiseerde apparatuur).

#Ebn: De meest algemene conclusie in een octrooispecificatie (i.e., de
conclusie met de minste eigenschappen) beschrijft meestal het best wat
is toegevoegd aan de stand van de techniek. De %(q:technische
bijdrage) van een softwarepatent kan gevonden worden in de
programmaconclusie, terwijl de toegevoegde kenmerken zoals
%(q:uitgevoerd in computergeheugen) enkel van bevestigende aard zijn.

#Wet: Het is steeds mogelijk om onafhankelijke conclusies te formuleren als
%(tp|afhankelijke|bevestigende) conclusies en vice versa. Dergelijke
forumuleringsoefeningen maken de echte afhankelijkheden enkel
ondoorgrondelijker.

#Wsg: De Onderzoeksrichtlijnen van het Europese Octrooibureau (EOB) uit 1978
leggen de uitsluiting van %(q:programma's voor computers) in Art 52(2)
EPC uit als volgt:

#rdr: Hier neemt ook het EOB aan dat een %(q:conclusie op het programma als
zodanig of een programma opgenomen op een magneetband) de normale vorm
is van een programmaconclusie. Het merkt echter eveneens op dat
hetzelfde object aangegeven kan zijn als %(q:proces voor het besturen
van een .. computer) or als een %(q:computer, gekarakteriseerd door
dat ..) en dat dergelijke pogingen tot het bekomen van softwarepatenen
door herformuleringen verwacht kunnen worden.

#rww: Waar de oorspronkelijke EOB-richtlijnen correct de herformulering van
programma's als processen als een woordspelletje aanduidde, nemen zijn
beheerders vandaag niet alleen aan dat deze truc toegelaten is, maar
dat herformulering van die vorm naar de programma's als zodanig
eveneens toegelaten moet zijn, aangezien het slechts een
herformulering is.

#mis: Uit de octrooieerbaarheid van computerprogramma's volgt, om het met de
woorden van het EOB uit 1978 te zeggen, dat %(e:de bijdrage tot de
stand van de techniek enkel moet bestaan uit een computerprogramma),
i.e., dat de %(q:technische bijdrage) beperkt kan worden tot rekenen
met abstracte entiteiten van virtuele machines zoals Von Neumann's
Universele Computer (die aan de basis ligt van elke moderne computer).
Hierdoor wordt aangetoond dat de onbepaalde wettelijke concepten
waardoor de Werkgroep van de octrooieerbaarheid wil beperken (zie
verder) nooit een beperkende betekenis kunnen hebben. Een uitzondering
van methoden voor bedrijfsvoering kan evenmin bekomen worden onder
deze voorwaarden, omdat methoden voor bedrijfsvoering gewoon een
specifieke vorm van regels voor gegevensverwerking zijn.

#rrr: Door informatie-objecten als industriële goederen te beschouwen, sluit
de Werkgroep van de Raad ook alle discussies uit over de beperking van
octrooieerbaarheid met betrekking tot niet enkel de vrijheid tot
publicatie, maar tot elke vrijheid die zou kunnen worden gevonden in
de informatieaard van het object.

#idK: De situatie wordt alleen maar erger gemaakt door het toevoegen van
omfloersende toelichtingen, zoals 7a:

#Wtw: De onervaren lezer zal geloven dat dit programmaconclusies uitsluit.
Jammer genoeg is het omgekeerde waar. Samen met Art 5(2) zegt de
toelichting dat een conclusie op een %(q:programma, gekarakteriseerd
door zijn [functionaliteit]) geen conclusie is op een programma als
zodanig. Als uitleg voor de schijnbare tegenspraak suggereert het dat
een %(q:programma als zodanig) moet geïnterpreteerd worden als het
%(q:uitdrukkingsaspect) van het programma (i.p.v. het functionele
aspect). I.e., een beperkte conclusie op %(q:een programma,
gekarakteriseerd door de tekst die in tekstvak 1 staat) moet geweigerd
worden als een conclusie op een individuele uitdrukking, terwijl een
zeer brede conclusie op %(q:een programma, gekarakteriseerd door dat
het [doet wat in tekstvak 1 staat]) is toegelaten. Niemand is
geïnteresseerd in beperkte conclusies. Een conclusie op een bepaalde
programmatekst zou enkel overeenkomen met een soort zwakke vorm van
auteursrecht en waartegen niemand een bezwaar zou hebben. Het doel van
het opwerpen van het non-issue van %(q:uitdrukkingsoctrooien) in het
voorstel van de Raad is niet om iets te regelen, maar om de wetgevers
te verwarren over wat geregeld wordt.

#br2: In het werkdocument van 2002 van de Raad, gaar Art 2.4B als volgt:

#hhw: Art 7 van het Europees Parlement stelt nog duidelijker dat het
patentensysteem publicaties van programmateksten (broncode) als
gedeelte van de vrijgave moet aanmoedigen, i.p.v. het te verbieden als
deel van het toegekende monopolie:

#egm: Het nieuwe werkdocument van de Raad vermeld een dergelijke eis in geen
enkele vorm, en heeft zich dus verder gedistantieerd van een compromis
met het Parlement.

#i0l: Art 6 van de Europese Commissie stelt een betekenisloze beperking van
de afdwingbaarheid van octrooien voor, dewelke de Raad nog meer
beperkt:

#moi: Deze voorziening laat decompilatie (het terug omzetten van machinecode
naar programmacode) van geoctrooieerde programma's toe, een
onderzoeksdaad gesteld in de privésfeer dewelke om te beginnen al
helemaal niet door octrooien kan verboden worden. De programma's die
uit dergelijk onderzoek voortspruiten kunnen echter nog steeds inbreuk
maken op octrooien, en Art 6 impliceert dat dergelijke octrooien mogen
afgedwongen worden tegen de programma's, zelfs wanneer dit enkel
gebeurt om interoperabiliteit te waarborgen. Bijgevolg beweert Art 6
interoperabiliteit te beschermen, maar doet in praktijk net het
omgekeerde: het zorgt ervoor dat niemand communicatiestandaarden kan
gebruiken die onder één of meerdere patenten vallen, zonder een
licentie van de octrooihouders.

#tKa: Het Europees Parlement werd niet misleid. Alle Committees (CULT, ITRE,
JURI) en de Plenaire stemden voor een echt
interoperabiliteitsprivilegie:

#Wtr: Het werkdocument van de Raad verwerpt de stem van het Parlement en
dringt aan op het laten domineren van communicatiestandaarden door
octrooihouders.

#sWt: Wanneer een bestandsformaat onder octrooien kan vallen, wordt het
onmogelijk voor een concurrent om het te converteren naar een ander
bestandsformaat. Dit versterkt monopolistische tendensen in de
softwaremarkt. Standaardisatieconsortia zoals de W3C (verantwoordelijk
voor de world wide web-standaarden) worden regelmatig vertraagd door
pogingen van octrooi-eigenaars om octrooitaxen te heffen op een
standaard. Standaarden waarop een octrooitax rust, leggen zware lasten
op de schouders van de beheerders van de standaard zelf en op de
makers van software die deze standaarden ondersteunt.  Vrije software
kan hier meestal niet aan voldoen. Wanneer één maker op zijn eentje
een standaard domineert met octrooien, wordt de situatie nog erger.
Microsoft is reeds bezig met het introduceren van geoctrooieerde
protocollen die beschikbaar zijn onder licentievoorwaarden die vrije
software expliciet uitsluiten. Zelfs de huidige concurrentieprocedures
van de Europese Commissie tegen Microsoft hebben daar tot hiertoe
niets aan veranderd. Concurrentiewetgeving op zichzelf, zonder verder
voorzieningen zoals deze van Art 6a, valt buiten het bereik van gewone
spelers op de vrije markt. Ze voorziet eveneens onvoldoende remedies
tegen anti-competitieve praktijken met dewelke de dominantie van
communicatiestandaarden normaal gezien worden geassocieerd.

#tWW: In de versie van 17 maart gaat de tekst nog een stap verder. Een nieuw
verwoorde toelichting 17 geeft aan dat het recht van
interoperabiliteit niet geregeld zal worden in het octrooirecht, maar
enkel met het mededingingsrecht:

#Wde: Hierdoor geeft de Werkgroep de indruk dat het rekening houdt met de
noden voor interoperabiliteit. Bij nader inzien blijkt echter dat net
het omgekeerde bewerkstelligd wordt: zonder een kostelijke, lang en
zelden succesvolle juridische procedure zulle softwaremakers, volgens
de versie van de Octrooiwerkgroep van de Raad, hun programma's niet
mogen laten samenwerken met deze van derden.

#uaW: Door deze verstrakking van zijn positie beantwoord de Werkgroep van de
Raad het volgende voorstel van de Luxemburgse Delegatie:

#Wnb: Delegaties zullen hieronder een voorstel vinden van de Luxemburgse
delegatie voor een interoperabiliteitsclausule (nieuw artikel 6a) om
toe te vaken aan de tekst van de voorgestelde Richtlijn. De
voorgestelde tekst is gebaseerd op amendement 76 van het Europees
Parlement, maar is beperkter in de zin dat het het voorbeeld neemt van
het oorspronkelijk amendement en dat als de enige toelaatbare
uitzondering toelaat om interoperabiliteit te bekomen. Deze striktere
verwoording zou de voorziening volledig overeenkomstig Artikel 30
TRIPS moeten maken.

#EWe: Dit voorstel komt overeen met het artikel in de vorm waarin het was
goedgekeurd door de Parlementaire Commissies CULT, ITRE en JURI. Het
vermijdt de term %(q:belangrijk doel), wat toegevoegd werd in de
plenaire stemming. Hierdoor kwam de Luxemburgse delegatie volledig
tegemoet aan de %(kk:kritiek van de Commissie) en de vereist van de
drie-stappen-test (beperkte beperking) van Art 30 TRIPs. Door het
voorstel van Luxemburg te weigeren en daarmee de positie van de drie
betrokken commissies van het Europees Parlement, toont de
octrooiwerkgroep aan dat Art 30 TRIPs niet haar hoofdzorg is. Het
lijkt er eerder op dat belangen rond tolheffing op
communicatiestandaarden, waarvoor recent geijverd werd door de CEO's
van %(te:vijf grote Europese telecommunicatiebedrijven), de drijvende
kracht zijn achter de onverzettelijkheid van de octrooiwerkgroep van
de Raad.

#efW: In de Europese octrooiwetgeving worden octrooieerbare uitvindingen
beperkt op twee manieren:

#edj: Negatieve definitie: bepaalde types van verwezenlijkingen worden
gedefinieerd als zijnde non-uitvindingen in Art 52(2) EPC. Hieronder
vallen o.a. verwezenlijkingen op gebied van informatie, programmeren
en bedrijfsvoering.

#fsu: Positieve definitie: rechtbanken in Duitsland en elders hebben
%(q:technische uitvinding) gedefinieerd als %(q:oplossing van een
probleem gebruikmakend van beheersbare natuurkrachten) of iets
gelijkaardigs, en hierdoor de lijst van negatieve definities in Art
52(2) EPC veralgemeend.

#ein: De Commissie en de Raad verwerpen elke definitie van wat een
octrooieerbare uitvinding is, zowel in negatieve als in positieve zin.
Aan de ene kant laat de Commissie octrooieerbaarheid afhangen van
abstracte concepten zoals %(q:technische bijdrage), aan de andere kant
weigert ze deze concepten te definiëren, of verstrekt ze enkel
tautologische definities.

#eWn: Het Parlement heeft zowel positieve als negatieve definities
voorgesteld.

#eai: Negatieve

#oii: Positieve

#iNe: Positieve en negatieve samen

#nWg: De Raad staat op haar non-definitie en maakt bovendien duidelijk dat
het concept van technische uitvinding, zelfs indien het gedefinieerd
wordt door een rechtbank, niet kan dienen tot het beperken van
octrooieerbaarheid.

#ecb: De Raad zegt dat het voldoende is indien er iets technisch voorkomt in
het bereik van sommige conclusies. Conclusies zonder enige technische
component, zoals programmaconclusies, zijn bijgevolg mogelijk (zolang
er ook andere conclusies met technische componenten binnen hun bereik
zijn). Bovendien stelt de Raad duidelijk dat de technische bijdrage,
i.e. de nieuwe oplossing die het toekennen van het octrooi
rechtvaardigt, mag bestaan uit enkel en alleen niet-technische
elementen.  Het is genoeg indien nieuwe %(tp|wiskundige
regels|algoritmen) gecombineerd worden met %(tp|gekende apparaten|de
universele computer) om een %(q:technisch probleem) op te lossen, b.v.
om het aantal benodigde muisklikken te verminderen. Een dergelijke
%(q:oplossing voor een technisch probleem) wordt dan een
%(q:technische bijdrage) genoemd, ongeacht of de oplossing zelf
technisch is. De %(q:technische bijdrage) moet niet eens nieuw of niet
voor de hand liggend zijn. Het is genoeg indien een %(q:technisch
probleem) geconstrueerd kan worden en op een of andere manier
geassocieerd kan worden met de %(q:uitvinderswerkzaamheid).

#hen: Het Parlement stelt %(q:technische bijdrage) gelijk aan
%(q:uitvinding). Dit het gewone gebruik van deze termen in oudere
handleidingen over het octrooirecht, evenals in een groot deel van de
hedendaagse rechtspraak. Bijgevolg is een %(e:uitvinding) een bijdrage
van technische kennis aan de stand van de techniek, ook %(q:technische
leerregel) of %(q:technische bijdrage) genoemd. De octrooiconclusies
moeten de uitvinding belichamen en tezelfdertijd de reikwijdte bepalen
van de exclusiviteit die erdoor gerechtvaardigd wordt. Op die manier
kunnen sommige conclusie-elementen een niet-technische bijdrage
beschrijven (b.v. een vernieuwend algoritme). Bijkomende
conclusie-elementen kunnen dan weer technisch zijn (b.v. %(q:laad het
programma in het geheugen en voer het uit)), maar geen enkele
vernieuwing beschrijven. Deze laatsten dienen eerder om de reikwijdte
van de conclusies te beperken.  Vermits een octrooiadvocaat vrij is om
eender welke reikwijdte te definiëren wat de conclusies betreft,
zolang dat deze reikwijdte de technische bijdrage omvat, is het steeds
mogelijk, en in feite zelfs onvermijdelijk, dat de reikwijdte van de
conclusies technische elementen zal omvatten. Door te %(q:eisen) dat
de reikwijdte van de conclusies %(q:technische elementen moet
bevatten), legt de Werkgroep van de Raad dus niet de minste beperking
op. Bovendien impliceert dit dat de technische bijdrage kan bestaan
uit enkel en alleen niet-technische elementen. B.v., een
niet-technisch algoritme kan meetellen als een technische bijdrage,
indien de reikwijdte van de conclusies in zijn geheel technische
elementen omvat. Dit leidt tot het besluit dat, zoals geschreven in
Toelichting 12 van het voorstel van de Werkgroep, alle %(q:in
computers geïmplementeerde) conclusie-objecten, inclusief algoritmen,
methoden voor bedrijfsvoering etc, %(q:tot een technisch veld
behoren).

#aWW: Deze Toelichting 12 zegt net hetzelfde als wat de Commissie zegt in
haar zwaar bekritiseerde Artikel 3, waarvan de verwijdering ook
aanbevolen wordt door de Werkgroep van de Raad:

#RiW: Het enige verschil tussen Toelichting 12 van de Raad en Artikel 3 van
de Commissie, is de presentatievorm. De Commissie zegt
rechttoe-rechtaan wat ze wilt, de Raad verbergt haar bedoelingen in
een %(q:hoewel)-formulering en omhult haar met betekenisloze
beperkingsretoriek.

#esK: De Franse delegatie in de Raad verwierp de implicaties van Toelichting
12, evenals het Europees Parlement. Toen Arlene McCarthy de truc van
de Raad probeerde uit te halen met het JURI-Committee, daalde haar
geloofwaardigheid. Toen JURI hem steunde met een 2/3 meerderheid,
stevende hij af op verwerping in de Plenaire.

#Rkt: Sommige mensen zouden hierdoor geneigd zijn te denken dat de Werkgroep
van de Raad zijn les zou geleerd hebben en op zijn minst de Franse
aanbeveling zou volgen. Het tegendeel is echter gebeurd: de oude en
onsuccesvolle tactiek wordt behouden en de bedenkingen van de Franse
delegatie zijn verwijderd.

#dnG: Volgens de doctrine van het EOB in de Pensioenvoordelenzaak uit 2000,
dewelke de basis vormt voor de voorstellen van de Commissie en de
Raad, is alles wat uitgevoerd wordt door een computer een technische
uitvinding. Om niettemin in staat te zijn sommige conclusies i.v.m.
methoden voor bedrijfsvoering te weigeren, zegt het EOB dat de
voorwaarde voor de technische aard op een of andere manier impliciet
is opgenomen in de voorwaarde wat betreft de uitvinderswerkzaamheid.
Zeer weinig mensen begrijpen wat dit betekent, en zelfs
octrooiadvocaten klagen regelmatig dat ze de EOB's esoterische
leerregels op dit gebied niet begrijpen. Onder de critici bevinden
zich bekende geleerden en rechters, waaronder sommigen zeer moeilijk
verdacht kunnen worden van tegen softwarepatenten te zijn. De VS
Handelsvertegenwoordige heeft %(us:gewezen op het feit) dat deze
EPO-doctrine het systeem van Art 27 TRIPS overtreedt. De Toelichting
van de Commissie van 2002/02/20 merkt op: %(orig:Zoals in toelichting
11 en Artikel 3 aangegeven, moet het aanwezig zijn van een
%(q:technische bijdrage) niet worden beoordeeld in combinatie met
nieuwheid, maar onder uitvinderswerkzaamheid. Ervarding heeft geleerd
dat deze aanpak eenvoudiger is om toe te passen in de praktijk.) Met
andere woorden: zelfs de Commissie geeft schoorvoetend toe dat de
doctrine van de Pensioenvoordelenzaak zeer moeilijk te rechtvaardigen
is in theorie. Het meest directe praktische gevolg van deze doctrine
is dat computeralgoritmen en bedrijfsmethoden niet langer geweigerd
kunnen worden zonder eerst een duur nieuwheidsonderzoek te verrichten.
Vermits vele nationale octrooibureaus (b.v. het Franse OB) geen
nieuwheidsonderzoeken doen, kunnen examinators geen enkel patent meer
uitsluiten wegens een gebrek aan een technische bijdrage, zelfs niet
onder de meest laxe criteria voorgesteld door deze richtlijn.
Bovendien creëert de doctrine van de Pensioenvoordelenzaak een maximum
aan verwarring en juridische onzekerheid rond het concept van
technische bijdrage, zodat gezaghebbende  octrooirechtbanken niet
langer ter verantwoording kunnen geroepen worden op grond van eender
welke wet.

#gfy: De term %(q:in een computer geïmplementeerde uitvinding) werd
ingevoerd en gedefinieerd door het EOB in 2000 als een eufemisme voor
%(q:computerprogramma's) (regels voor het gebruik van conventionele
elementen van een gegevensverwerkingssysteem, zoals een processor,
opslag, invoer/uitvoer etc), dewelke geen uitvindingen zijn in de zin
van het octrooirecht. Het voorstel van de Commissie is gewoon een
kopie van de definitie van het EOB.

#xrW: Het EOB en de Commissie zijn voorzichtig om %(q:in een computer
geïmplementeerde uitvinding) zeer nauw te definiëren, zodat enkel pure
computerprogramma's eronder vallen. De %(q:prima facie) nieuwe
bijdrage moet bestaan uit de manier waarom het
gegevensverwerkingssysteem is geprogrammeerd, niet uit perifere
technische processen. Een nieuwe manier om autobanden aan hitte bloot
te stellen via controle door een computerprogramma, zou %(e:geen)
%(q:in een computer geïmplementeerde uitvinding) zijn volgens de
definitie van het EOB en de Commissie.

#uht: De rechttoe-rechtaan formulering van de Commissie werd een gemakkelijk
te bekritiseren onderwerp voor veel mensen. Net zoals in het geval van
Artikel 3, heeft de werkgroep van de Raad niets gedaan om de doctrine
van de Commissie te wijzigen, maar heeft ze zich bezig gehouden met
het verwarren van de formuleringen. In het document van de Raad kunnen
bijgevolg zowel pure gegevensverwerkingsprocessen als nieuwe manieren
om natuurkrachten te beheersen %(q:in computers geïmplementeerde
uitvindingen) zijn. De formulering van de Raad slaat op elk proces
aangestuurd door een computer en omvat zo alle objecten in alle
technische en non-technische velden. Men kan zich afvragen welk nut
deze term bijgevolg nog kan hebben, met een dergelijke algemene
definitie.

#hte: Het parlement heeft gekozen om de term %(q:in een computer
geïmplementeerde uitvinding) op een dergelijk manier te definiëren dat
hij exact betekent wat het EOB en de Commissie niet willen, en dat hij
juist uitsluit wat zij willen: daar refereert hij enkel naar perifere
technische processen, en niet naar computerprogramma's als zodanig.

#nfg: Op die manier zegt het Parlement duidelijk wat de Commissie en de Raad
vaak beweren dat ze beogen: de door een computer uitgevoerde
algoritmen die een anti-blokkeringssysteem (ABS) controleren, zijn
niet octrooieerbaar, maar het ABS-systeem in zijn geheel, met
technische constructies, hitte, wrijving en een nieuwe manier om ze te
gebruiken zoals beschreven in de conclusies, kan een octrooieerbare
uitvinding zijn.  Hoewel het werk van de procesingenieur
octrooieerbaar is, is dat van de programmeur het niet.

#ahe: De Werkgroep wil niet beslissen waar het Orwelliaanse woord %(q:in een
computer geïmplementeerde uitvinding) nu naar moet verwijzen:
computerprogramma's als zodanig (voorstel van de Commissie) of
gecomputeriseerde technische uitvindingen (voorstel van het
Parlement). Dit kan misschien politiek gezien slim lijken, maar het
heeft zware gevolgen voor de juridische praktijk. Onder de versie van
de Werkgroep, kunnen octrooiadvocaten zelf beslissen of een proces als
een %(q:in een computer geïmplementeerde uitvinding) behandeld moet
worden, i.e. of het speciaal regime geïntroduceerd door deze richtlijn
erop toegepast moet worden. Eender welk octrooieerbaar proces moet
reproduceerbaar zijn en zal bijgevolg in de praktijk automatisch
uitgevoerd worden, i.e. onder de controle van een computer. Indien de
octrooiadvocaat deze eigenschap in de octrooiconclusies plaatst, zal
de zal de richtlijn van de Werkgroep van toepassing zijn, dewelke
sterk afwijkt van de gewone octrooiwetgeving. Volgens Art 5(2) wordt
het b.v. mogelijk om een computerprogramma te claimen zonder
proceshardware. Op deze manier kan men een Turing-complete
beschrijving van een chemisch proces in de conclusies opnemen,
ongeacht of de programmalogica nieuw is of niet. Een dergelijke
tendens kan ook vandaag de dag reeds opgemerkt worden. Dit geeft tot
gevolg dat octrooiaanvragers secundaire programmeer- en
publicatiesectoren van de markt kunnen controleren. Het principe dat
octrooien als doel hebben technische informatie te verspreiden (i.p.v.
de informatie zelf te monopoliseren), wordt hierdoor op systematische
wijze ontkracht, en niet enkel voor het speciaal geval van
softwarepatenten.

#mts: We hebben herhaaldelijk aangetoond met welke retorische trucs het
document van de Raad de onoplettende lezer tracht te misleiden.

#egr: We hebben een aantal van deze trucs %(cj:opgesomd en geanalyseerd) in
de zomer van 2003, toen ze gekopieerd en geplakt waren door Arlene
McCarthy en het JURI-Committee.

#ebw: Een gedetailleerde analyse van de retorische strategieën die het
werkdocument van de Raad doorspekken zal hier nog toegevoegd worden.

#ute: De meeste gebruikte retorische truc werkt als volgt:

#inW: [A] is niet octrooieerbaar, tenzij aan [voorwaarde B] voldaan is

#hnr: waarbij, bij nader inzien, aan voorwaarde B altijd voldaan is.

#rWn: De leden van de EU-Raad Werkgroep hebben hun vorige positie vóór
onbeperkte octrooieerbaarheid en afdwingbaarheid van octrooien met
betrekking tot software verder versterkt. Hierdoor hebben ze een
extreme tegenpositie ingenomen t.o.v. het Europees Parlement, wat
verdere onderhandelingen zeer moeilijk maakt. Bepaalde posities
ingenomen door de Werkgroep, zoals het vasthouden aan
programmaconclusies, dienen geen enkel herkenbaar belang, afgezien van
het moeilijk maken van de onderhandelingen. Dit werpt vraagtekens op
bij de bedoelingen van de Werkgroep.

#dnd: De leden van de Werkgroep komen regelmatig samen, zowel in de Raad van
de Europese Unie als in de Administratieve Raad van het Europese
Octrooibureau (EOB), dewelke het EOB bestuurt. Zij zijn een
persoonlijke verbinding tussen het EOB en de EU. Binnenin het EOB zijn
ze de specialisten wat betreft de wetgeving. Ze zijn terughoudend om
deze macht met eender wie te delen. EU-deelneming is welkom, zolang
dat dit betekent dat de EU het beleid van de Werkgroep goedkeurt
zonder vragen te stellen. Wanneer de EU de wijsheid van de Werkgroep
in vraag stelt, sluit zij alle communicatiekanalen naar buiten toe af
en schaart ze zich rond de posities waarrond haar gezag ontstaan is,
i.e. de oneindige wijsheid van de Technische Raad van Beroep.  De
Werkgroep zou maar al te graag het project rond de EU-richtlijn
opgeven en haar lijn doorzetten via diplomatische conferenties en
andere ondoorzichtige intergouvernementele wegen, waar geen parlement
en geen publieke discussie ter sprake komt. Frits Bolkestein
%(sp:vermeldde) dit publieke geheim openbaar in het Europees Parlement
de dag voor de plenaire stemming.

#riA: De situatie is gekomen tot op een punt waar het monopolie van de
Werkgroep duidelijk in de weg staat van de ontwikkeling van de EU's
wetgevende macht op vitale gebieden wat betreft economische politiek.

#tnW: explains fairly concisely why the Council's %(q:compromise document)
is the worst of all proposals on software patents that have come out
of the Brussels process.

#spW: Council Presidency 2004-03-17 %(q:Compromise Paper) on Software Patent
Directive

#hel: published 2-3 weeks later on the consilium site in English and German,
with all information about positions of national delegations deleted.

#til: latest public version on Council website

#sWW: Translations to the last remaining EU languages appeared around 18th
of August.

#nmk: Council Document 2004-05-25

#tWs: latest published version

#lcW: HTML van het oorspronkelijke MSWord document.

#cgW: Werkdocument ST09051 van de Raad

#ebn: a recent unpublished version with footnotes about positions of
national delegations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: jmaebe ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: cons0401 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

