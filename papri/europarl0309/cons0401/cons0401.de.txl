<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Rat 2004 Vorschlag zu Softwarepatenten

#descr: Der Ministerrat der EU hat sich auf ein Papier geeinigt, welches
Gegenvorschläge zu den Änderungsvorschlägen des Europäischen
Parlamentes zur Richtlinie %(q:über die Patentierbarkeit
computer-implementierter Erfindungen) enthält.  Im Gegensatz zur
Version des Europäischen Parlamentes erlaubt die Version des Rates
grenzenlose Patentierbarkeit und Patent-Durchsetzbarkeit.  Gemäß der
Version des Rates gelten %(q:computer-implementierte) Algorithmen und
Geschäftsmethoden von vorneherein als Erfindungen im Sinne des
Patentrechts.   Schon die Veröffentlichung einer funktionsfähigen
Beschreibung einer patentierten Idee stellt laut Ratsvorschlag eine
Patentverletzung dar.  Protokolle und Dateiformate können direkt
patentiert werden und dürfen dann ohne eine Lizenz des Patentinhabers
nicht verwendet werden.  Für den ungeübten Leser sind diese
Implikationen nicht unbedingt sofort ersichtlich.  Wir erklären,
woraus sie sich ergeben.

#ltp: Kollision statt Kompromiss

#kWr: Programm-Ansprüche: alle Türen zugeschlagen

#eiW: Programmtext-Veröffentlichung: Ein weiterer Schritt weg vom Parlament

#bea: Interoperabilitätsprivileg gestrichen, Grenzenlose Durchsetzbarkeit
gesichert

#tng: Ersatzlose Streichung der Definitionen von %(q:Technik),
%(q:Industrie), %(q:Erfindung)

#tef: Systemwidrige Vermengung von Technizität und Erfindungshöhe

#edj2: Anwendungsgebiet der Richtlinie auf beliebige Verfahrenserfindungen
ausgeweitet

#bra: Scheinbegrenzungen, Irreführende Schachtelsätze

#ien: Mutwillige Zerstörung des EU-Richtlinienprojektes?

#ruo: Die Version der Rats-Arbeitsgruppe gleicht bis auf unwesentliche
Details der Version vom November 2002.  Von allen bisherigen
Vorschlägen stellt sie die radikalsten Forderungen nach grenzenloser
Patentierbarkeit dar.  Die Radikalität wird jedoch in Nebensätzen
versteckt und von belangloser Begrenzungs-Rhetorik umhüllt, so dass
unerfahrene und unaufmerksame Leser sie leicht übersehen.

#utd: Es wurden lediglich in einige Formulierungen aus dem %(ju:Beschluss
des Rechtsausschusses JURI vom Juni 2003) (McCarthy-Bericht)
übernommen, die nichts zur Begrenzung der Patentierbarkeit beitragen
sondern allenfalls geeignet sind, den Anschein einer Begrenzung zu
erwecken.  Die JURI-Änderungsanträge lehnten sich eng an das
%(dk:Rats-Arbeitspapier vom November 2002) an und wandten sich gegen
die von den %(ci:beiden beratenden Ausschüssen) ausgearbeiteten
Empfehlungen, ohne hierfür eine Begründung auch nur zu versuchen.  Das
Plenum des EU-Parlaments verweigerte daher JURI (und damit der
Rats-Arbeitsgruppe) seine Gefolgschaft.   In der Plenarabstimmung vom
24. September 2003 setzten die Abgeordneten den Scheinbegrenzungen
echte Begrenzungen entgegen.  JURI-Formulierungen wurden nur zur
Gesichtswahrung beibehalten, soweit sie dem Ziel der echten Begrenzung
nicht entgegenstanden.  Indem die Ratsarbeitsgruppe nun wiederum die
Änderunganträge des Plenums und der Ausschüsse CULT und ITRE ignoriert
und stattdessen JURI-Formulierungen übernimmt,die das Plenum nur wegen
ihrer Belanglosigkeit duldete, verunstaltet sie ihren eigenen Entwurf
vom November 2002 und vergrößert dabei nur noch den Abstand zwischen
Rat und Parlament.  Die Bezeichnung %(q:Kompromisspapier) auf dem
Schreiben der Ratspräsidentschaft ist daher diesmal in besonders hohem
Maße irreführend.  Auch beim vorigen %(q:Kompromisspapier) vom
November 2002, wurde nicht offengelegt, zwischen welchen Interessen
denn nun ein Kompromiss geschlossen wurde.  Diesmal kann es den
Umständen nach nur um einen Kompromiss zwischen Parlament und Rat
gehen.  Bei näherem Hinsehen zeigt sich jedoch, dass genau das
Gegenteil erreicht wird: die Rats-Arbeitsgruppe entfernt sich noch
weiter vom Parlament.

#nie: Im folgenden möchten wir dies durch einen Vergleich der Ratsvorschläge
mit denen des Parlaments im einigen zentralen Streitfragen
verdeutlichen.

#gsW: Programmansprüche sind Patentansprüche der Form

#ben: Computerprogramm, bei dessen Ladung in den Arbeitsspeicher eines
Rechners der Rechenvorgang gemäß obigem Anspruch N ausgeführt wird.

#oWs: Statt %(q:Computerprogramm) kann auch %(q:Programm auf Datenträger),
%(q:Programmprodukt), %(q:Datenstruktur), %(q:Computerlesbares Medium)
oder anderes mehr stehen.  Wesentlich ist, dass nicht mehr nur
materielle Erzeugnisse und Prozesse sondern auch
Informationskonstrukte beansprucht werden.

#Wtt: Diese Anspruchsart wurde 1998 durch zwei Entscheidungen der
Technischen Beschwerdekammern des EPA n Europa eingeführt.  Auch in
den USA kam sie erst 1994 auf.  Sie ist umstritten, weil

#dyr: sie offen mit der traditionellen Systematik des Patentrechts bricht,
wonach der Anspruchsgegenstand technischer Natur sein muss und
Informationskonstrukte im Gegenzug hierfür offenbart -- d.h. zu
Gemeingut gemacht -- werden;

#tvW: damit wörtlich Programmen für Datenverarbeitungsanlagen als solche
beansprucht werden, was sich nur noch unter größten (und nach
%(ex:üblicher Auslegungsmethodik) unzulässigen) Verrenkungen in
Einklang mit Art 52(2),(3) bringen lässt;

#nce: sie der Publikationsfreiheit, wie sie in Art 10 der Europäischen
Menschenrechtskonvention (EMRK) festgehalten wird, systematisch
zuwiderläuft

#r1s: Patentrechte gehören nicht zu den Rechten, die gemäß Art 10(2) EMRK
eine Schranke der Publikationsfreit darstellen könnten

#reo: sie Entwickler und Distributoren von Software ebensowie andere
Informationsmittler (z.B. Internet-Zugangsanbieter) direkt den
schweren Geschossen der Patentjustiz aussetzt, als handele es sich um
Lieferanten von Industriegütern;

#cgd: sie keinem praktischen Zweck dient: es läge näher, die Vermarktung von
Informationsgütern als %(e:mittelbare Patentverletzung) zu verfolgen
und die Modalitäten dieser Verfolgung im Hinblick auf die
Besonderheiten der Informationsökonomie getrennt zu regeln;

#dea: Die Europäischen Kommission sieht in ihrem Richtlinienvorschlag
Programmansprüche nicht vor.

#leW: Das Europäische Parlament (CULT+ITRE+Plenum) folgt hierin der
Kommission und bekräftigt darüber hinaus mit seinem neuen Art 7 (in
Abstimmung als 5.1b gebilligt) die Publikationsfreiheit als
vorrangiges Rechtsgut:

#ing: Die Rats-Arbeitsgruppe lehnt die Vorschläge von Kommission und EP ab
und beharrrt mit ihrem Art 5(2) auf Programmansprüchen:

#ise: Die Formulierung %(q:sind nur zulässig, insoweit ...) ist in
mehrfacher Hinsicht irreführend:

#crW: Der %(q:insoweit)-Satz schränkt nichts ein.  Zu jedem Programmanspruch
lässt sich ein passender Verfahrens- oder Erzeugnisanspruch
formulieren, von dem dann pro forma der Programmanspruch abhängt

#loe: s. oben

#iWe: Die Partikel %(q:nur) lügt.  Sie erfüllt keinen regulatorischen Zweck.
 Man sollte den Satz mal zur Übung ohne diese Partikel (und auch ohne
den belanglosen %(q:insoweit)-Satz) lesen.

#tph: Es sollte angemerkt werden, dass die deutsche Version stark von der
französischen und englischen abweicht.  %(q:mettre en oevre) oder
%(q:put into force) bedeutet %(q:verwirklichen), nicht %(q:begründen).
 Wir gehen davon aus, dass es sich bei der deutschen Fassung um einen
Freudschen Fehler handelt.   Es ist offensichtlich nicht die Intention
der Arbeitsgruppe, zuzugeben, dass die Patentierbarkeit in der
Anwesenheit eines Programmes begründet liegt.

#rnp: Programmansprüche verändern die Systematik des Patentrechts und
bringen Software vom Rand in die Mitte des Systems.

#cnr: Die Ratsarbeitsgruppe beteuert freilich, sie sehe Programmansprüche
%(q:nur) als abgeleitete Ansprüche, %(q:rein deklaratorischer Natur),
wie es %(tp|in diesem Umfeld|etwa bei vom Rats-Papier inspirierten
Änderungsanträgen im EP) immer wieder hieß.  Welcher Natur diese
Ansprüche sind, ergibt sich jedoch nicht aus den Wünschen ihrer
Befürworter sondern aus den von ihnen in der Rechtspraxis geschaffenen
Fakten.  Zu diesen gehört:

#rcg: Aus jedem Programmanspruch kann ein Verfahrensanspruch abgeleitet
werden.  Umgekehrtes nicht gilt.

#aoc: Ein Programmanspruch ist breiter als der ihm entsprechende
Verfahrensanspruch.  Der Verfahrensanspruch spezifiziert über das
Programm hinaus dessen Ausführung im Speicher einer
Datenverarbeitungsanlage.  Dieses zusätzlich einengende Merkmal trägt
nichts zur Erfindung bei.

#Ebn: Der breiteste Anspruch einer Patentschrift (d.h. der mit den wenigsten
Merkmalen) beschreibt in der Regel am genauesten den Bereich, der zur
Erfindung beiträgt.  Die erfindungswesentlichen Merkmale eines
Softwarpatentes finden sich im Programmanspruch.  Nicht der
Programmanspruch sondern der Verfahrensanspruch mit seinen
zusätzlichen erfindungs-unwesentlichen Merkmalen ist insoweit
deklaratorischer Natur.

#Wet: Es ist immer möglich, unabhängige Ansprüche als abhängige
(deklaratorische) Ansprüche zu formulieren und umgekehrt.  Solche
Formulierungskünste sind geeignet, die tatsächlichen
Abhängigkeitsverhältnisse zu verschleiern.

#Wsg: Die Prüfungsrichtlinien des Europäischen Patentamtes von 1978 nehmen
auch diesen Standpunkt ein, wenn sie die verschiedenen
Anspruchsformen, in denen die gemäß Art 52 EPÜ von der
Patentierbarkeit ausgeschlossenen %(q:Programme für
Datenverarbeitungsanlagen) daher kommen könnten, wie folgt erklärt:

#rdr: Auch hier wird davon ausgegangen, dass ein %(q:Patentanspruch für das
Programm als solches oder für das auf Magnettonband aufgenommene
Programm) die normale Form eines Software-Anspruches wäre.  Es wird
aber zugleich darauf hingewiesen, dass sich der selbe Gegenstand auch
als %(q:Verfahren zum Betrieb einer ... Datenverarbeitungsanlage) oder
als %(q:Datenverarbeitungsanlage, dadurch gekennzeichnet, dass ...)
deklarieren lässt, und dass solche Versuche, Software durch
Umdeklarierung zu patentieren, zu erwarten sind.

#rww: Während das EPA 1978 richtig die Umdeklarierung von Programmen zu
Prozessen als deklaratorischen Trick entlarvte und verbot, erklären
seine Lenker heute nicht nur diesen Trick für zulässig sondern folgern
auch noch daraus, dass der umgekehrte Weg der Umdeklarierung von
Prozessansprüchen zu den zugrundeliegenden Programmansprüchen auch
zulässig sein muss, da es sich ja nur um eine Umdeklarierung handelt.

#mis: Aus der Patentierbarkeit von Programmen für Datenverarbeitungsanlagen
folgt, dass, um mit den Worten des EPA von 1978 zu sprechen, %(e:der
Beitrag zum bisherigen Stand der Technik lediglich in einem Programm
für Datenverarbeitungsanlagen besetehen muss), m.a.W. dass der
%(q:technische Beitrag) sich im Rechnen mit den abstrakten Entitäten
des Neumannschen Universalrechners erschöpfen kann.  Damit wird
verhindert, dass den unbestimmten Rechtsbegriffen, mit denen die
Rats-Arbeitsgruppe die Patentierbarkeit begrenzt sehen möchte (s.
unten), jemals eine begrenzende Bedeutung zukommen kann.  Auch mit
einer Begrenzung der Patentierbarkeit von Geschäftsmethoden kann unter
diesen Vorzeichen nicht gerechnet werden.   Denn Geschäftsmethoden
sind Konkretisierungen von Datenverarbeitungsprogrammen.

#rrr: Wer Programmansprüche vorschlägt, beendet damit im voraus jede
Diskussion über Schranken der Patentdurchsetzung im Hinblick auf
entgegenstehende Rechtsgüter aller Art.  Wenn schon die
Publikationsfreiheit offen durch Patente beschränkt wird, kann auch
weniger zentralen Freiheiten, die als Schranken der Patentierbarkeit
oder Patentdurchsetzbarkeit gelten und deren Kodifizierung z.T. vom
Parlament vorgeschlagen wurde, keine vorrangige Bedeutung mehr
zukommen.

#idK: Nur noch schlimmer wird die Situation durch die Einfügung
irreführender Erwägungsgründe wie 7a:

#Wtw: Der unerfahrene Leser (z.B. ein Minister) dürfte annehmen, dass
hierdurch Programmansprüche ausgeschlossen werden.  Das Gegenteil ist
freilich der Fall.  Es wird behauptet, dass auch dann, wenn ein
%(q:Programm, dadurch gekennzeichnet dass ...) beansprucht wird, sich
das Patentbegehren nicht auf ein Programm als solches richte.   Wie
dies möglich ist, wird nicht erklärt, aber es wird suggeriert, dass
unter einem %(q:Programm als solchem) nur eine bestimmte Ausdrucksform
des Programms zu verstehen sei.  D.h. es soll nicht möglich sein, ein
%(q:Programm, dadurch gekennzeichnet, dass der Wortlaut von Listing 1
verwendet wird) zu beanspruchen, wohl jedoch soll es möglich sein,
Listing 1 (und tausende von weiteren Listings) dadurch patentrechtlich
zu erfassen, dass man die darin ausgedrückten Ideen und
Funktionalitäten beansprucht.  Selbstverständlich geht es den
Patentanmeldern immer nur um letzteres, eine direkte Beanspruchung
bestimmter Ausdrucksformen hat nie zur Debatte gestanden.  Der einizge
Sinn dieser Bestimmung ist es, den für die Gesetzgebung zuständigen
Politikern Sand in die Augen zu streuen.

#br2: Im Rats-Arbeitspapier vom November 2002 stand unter Art 2.4B zu lesen:

#hhw: Das Europäische Parlament macht in Art 5.1D noch deutlicher, dass das
Patentwesen die Veröffentlichung von Programmtexten als Teil der
Offenbarung fördern und nicht etwa als Teil des Monopols verbieten
soll:

#egm: Wenn der Rats-Arbeitsgruppe an einem Kompromiss mit dem Parlament
gelegen wäre, hätte sie den Vorschlag der französischen Ratsdelegation
zumindest erneut als Minderheitenvorschlag mit in ihr Papier
aufgenommen.  Aber das Gegenteil geschah: alle Hinweise auf eine
Pflicht zur Veröffentlichung von Programmtext im Rahmen der
Offenbarung wurden aus dem Ratsvorschlag gestrichen.

#i0l: Die Europäische Kommission schlug nach langem internen Tauziehen im
Februar 2002 ein wirkungsloses Interoperabilitätsprivileg vor, welches
der Rat weiter einschränkt:

#moi: Diese Bestimmung erlaubt nur das Dekompilieren von Programmen im
privaten Bereich, das durch Patente ohnehin nicht beschränkt wird. 
Will man die aufgrund der Dekompilation erarbeiteten interoperablen
Programme dann verwenden oder veröffentlichen, so verletzt man
Patente.  Die Analogie zum Urheberrecht ist irreführend, da ein selbst
erarbeites interoperables Programm niemals das Urheberrecht verletzt.

#tKa: Das Plenum und die drei Ausschüsse des EU-Parlamentes ließen sich
nicht irreführen.  Sie stimmten für ein echtes
Interoperabilitätsprivileg:

#Wtr: Diesen Vorschlag nimmt das Ratspapier nicht an.

#sWt: Die Gefahr eines Missbrauchs von Datenverarbeitungspatenten für die
Monopolisierung formeller und informeller Kommunikationskonventionen
(Datenaustausch-Standards) ist offensichtlich.  Das
World-Wide-Web-Konsortium hat ständig mit Versuchen zu kämpfen,
Kommunikations-Standards gebührenpflichtig zu machen.  Durch die
Monopolisierung von Schnittstellen werden regelmäßig sekundäre Märkte
abgeschottet.  In den Monaten nach dem Parlamentsbeschluss
veröffentlichte Microsoft ein neues Programm zur Lizenzierung diverser
patentierter Protokolle, bei dem freie Software systematisch von der
Interoperation ausgeschlossen wird.  Das neuliche
%(cm:EU-Kartellverfahren) hat Microsoft lediglich zur Auflage gemacht,
dass für die Interoperation (Kompatibilität) mit Microsoft-Anwendungen
%(q:faire Gebühren) zu berechnen seien, d.h. dass freie Software
ausgeschlossen werden kann.  Wo die entsprechenden Patente nicht einem
Monopolisten gehören, kommt es erst gar nicht zu Kartellverfahren.  Es
ist daher notwendig, eine wettbewerbsmotivierte Schrankenbestimmung
unmittelbar in das Patentrecht einzubauen.  Hiergegen wurden bislang
von den Besitzern der europäischen Patentgesetzgebung nur ungültige
rechtssystematische Einwände (z.B. sinnentleerende Auslegung von Art
30 TRIPs) erhoben.

#tWW: In der %(cm:Version vom 17. März) geht das Papier noch einen Schritt
weiter.  Ein neu formulierter Erwägungsgrund 17 deutet an, dass das
Recht der Interoperation nicht im Rahmen des Patentrechts sondern nur
im Rahmen des Wettbewerbsrecht geregelt werden soll:

#Wde: Hierdurch wird der Eindruck erweckt, es solle dem Bedürfnis nach
Interoperationsfreiheit Rechnung getragen werden.  Bei näherem Lesen
ergibt sich jedoch, dass genau das Gegenteil erreicht wird:  ohne ein
kostspieliges, langwieriges und selten erfolgreiches Verfahren soll es
nach dem Willen der Ratsarbeitsgruppe kein Recht auf Interoperation
geben.

#uaW: Mit dieser Verhärtung ihres Standpunktes antwortete die
Ratsarbeitsgruppe auf den folgenden Antrag der Luxemburger Delegation:

#Wnb: Delegations will find below a proposal by the Luxembourg delegation
for an interoperability clause (new article 6a) to be included in the
text of the proposed Directive. The proposed text is based on
amendment 76 of the European Parliament, but is more restrictive in
that it takes the example in the original amendment and uses it as the
only acceptable exception for the purpose of ensuring
interoperability. This stricter wording should make the provision
fully compatible with Article 30 TRIPS.

#EWe: Dieser Antrag entspricht dem Artikel in seiner von den
Parlamentsausschüssen CULT, ITRE und JURI gebilligten Form.  Er meidet
den in der Plenarabstimmung neu eingeführten Ausdruck %(q:bedeutsamer
Zweck).  Damit tut die Luxemburger Delegation der in %(kk:Kritik der
Kommission) und dem Erfordernis des Dreistufentest (Beschränkung der
Beschränkung) gemäß Art 30 TRIPs Genüge.  Indem sie den Luxemburger
Antrag ablehnt, dokumentiert die Ratsarbeitsgruppe, dass bei den
Verhandlungen in der Ratsarbeitsgruppe nicht Art 30 TRIPs sondern um
eher die Interessen einiger Akteure an der Dominanz von
Kommunikationsstandards, wie sie kürzlich im Namen der %(te:5 größten
Telekommunikationsunternehmen) formuliert wurden, im Vordergrund
stehen.

#efW: Im Europäischen Patentrecht wird die Patentfähige Erfindung auf
zweierlei Wegen eingegrenzt:

#edj: Negative Definition: bestimmte Arten von Leistungen werden vom Gesetz
(Art 52(2) EPÜ, §1(2) PatG) als Nicht-Erfindungen definiert.  Hierzu
gehören z.B. Leistungen auf dem Gebiet der Mathematik, der
Geschäftsplanung und der Programmierung.

#fsu: Positive Definition: die Rechtsprechung des Bundesgerichtshofs (BGH)
hat die %(q:technische Erfindung) als %(q:Lösung eines Problems durch
Einsatz beherrschbarer Naturkräfte) definiert und damit ein
Beurteilungskriterium geschaffen, das mit den ausdrücklichen Negativen
Definitionen in Einklang steht und zugleich auf viele Fälle anwendbar
ist, für deren Beurteilung sich aus den Negativdefintionen nur schwer
eine Anleitung entnehmen lässt.

#ein: Die Kommission und der Rat schlagen vor, sowohl auf negative als auch
auf positive Definitionen der Begriffe %(q:Technik), %(q:Industrie)
und %(q:Erfindung) zu verzichten.  Einerseits machen Kommission und
Rat die Frage der Patentierbarkeit ausschließlich an abstrakten
Begriffen wie %(q:technischer Beitrag) fest, andererseits definieren
sie diese Begriffe allenfalls zirkulär, was einer Nicht-Definition
gleichkommt.

#eWn: Das Parlament hingegen hat negative und positive Definitionen
vorgeschlagen.

#eai: Negativ

#oii: Positiv

#iNe: Positiv und Negativ vereint

#nWg: Demgegenüber beharrt der Rat auf einer zirkulären Definition und
stellt überdies noch klar, dass der Technikbegriff, sollte er denn
einmal wirklich mit Bedeutung gefüllt werden, nicht zur Begrenzung der
Patentierbarkeit herangezogen werden darf:

#ecb: M.a.W.: nur der %(q:Anspruchsbereich) muss technische Merkmale
umfassen.  Der %(q:technische Beitrag) hingegen kann auch aus
nicht-technischen Merkmalen bestehen.  Auch die Kombination neuer
Nicht-Technik (z.B. Algorithmus) mit alter Technik (z.B.
Universalrechner) kann zur Lösung eines %(q:technischen Problems)
(z.B. Reduzierung der Zahl erforderlicher Mausklicks) beitragen und
somit einen %(q:technischen Beitrag) darstellen.  Dieser
%(q:technische Beitrag) wurde zunächst nur als ein Aspekt der
Erfindungshöhe behandelt, d.h. er konnte für sich genommen altbekannt
sein und muss nicht einmal den üblichen Anforderungen an eine
patentfähige Erfindung %(q:Neuheit, Erfindungshöhe und Gewerblichen
Anwendbarkeit) standhalten.  Nach dem von den Deutschen im Mai 2004
errungenen %(q:Kompromiss) muss dieser %(q:technische Beitrag) nun
entgegen den üblichen Gepflogenheiten des EPA auch %(q:neu und nicht
naheliegend) sein.  Damit wird der %(q:technische Beitrag) erneut dem
Begriff %(q:Erfindung) angenähert, von dem das EPA ihn zu trennen
versuchte.  Die Bedeutung dieser Neuerung ist wegen ihrer fehlenden
Verankerung in dem hier kodifizierten EPA-System unklar.

#hen: Das Parlament setzt %(q:technischer Beitrag) im Geiste der
traditionellen Patentrechtsprechung, wie die deutschen Gerichte sie
entwickelt und z.T. bis heute beibehalten haben, mit %(q:Erfindung)
gleich.  Es muss demnach ein Beitrag geleistet (= eine Lehre
offenbart) werden, die technisch und innovativ zugleich sein muss. 
Kommission und Rat hingegen wenden die Pension-Benefits-Doktrin des
EPA von 2000 an, wonach es genügt, wenn irgendwo im Anspruchsbereich
an einer Stelle etwas neues und an einer anderen etwas technisches zu
finden ist.  Damit ist jeder Anspruch auf ein Verfahren zum Betrieb
einer Datenverarbeitungsanlage (= Programm für
Datenverarbeitungsanlagen) von vorneherein eine %(q:Erfindung), die
%(q:auf einem Gebiet der Technik liegt).  Dies bringt der Rat im
November 2002 in Erwägung 12 zum Ausdruck:

#aWW: Die Rats-Arbeitsgruppe sagt in dem oben zitierten Erwägungsgrund 12
genau das, was die Kommission in ihrem Artikel 3 sagt, dessen
Streichung die Rats-Arbeitsgruppe empfiehlt:

#RiW: Der einzige Unterschied zwischen Rat-Erwägungsgrund 12 und
Kommissions-Artikel 3 besteht darin, dass ersterer die zentrale
Aussage in einen unauffälligen %(q:Obwohl)-Nebensatz versteckt und mit
belangloser Begrenzungsrhetorik umhüllt.

#esK: Die Bedenken der französischen Delegation wurden vom Europäischen
Parlament geteilt.  Der vom Rats inspirierte Ansatz der
Berichterstatterin Arlene McCarthy führte im Rechtsausschuss zu
Glaubwürdigkeitsverlust und im Plenum zur Niederlage.

#Rkt: Man hätte nun erwarten können, dass die Ratsarbeitsgruppe die Bedenken
der französischen Delegation aufnimmt und wenigstens die Konjunktion
%(q:obwohl) zu %(q:selbst wenn) ändert.  Fehlanzeige: die Version vom
Januar 2004 enthält die alte Formulierung --- ohne Erwähnung der
Bedenken.

#dnG: Die Vorstellung, dass man die Technizität als eigenständiges Kriterium
abschafft und dann im Rahmen der Erfindungshöheprüfung
Technizitätsüberlegungen mit einfließen lässt, ist schwer zu
vermitteln und mit der Systematik des EPÜ ebenso wie des
TRIPs-Abkommens kaum vereinbar.  Ähnlich wie das Wunschdenken von der
%(q:deklaratorischen Natur von Programmansprüchen) (s. oben) ist diese
Vorstellung des Rates darauf angelegt, durch die Rechtspraxis der
letzten Reste einer eventuell vorhandenen Bedeutung beraubt zu werden.
 Die Vermengung von Technizität und Erfindungshöhe wird dem EPA seit
Jahren von zahlreichen Rechtsexperten zum Vorwurf gemacht.  Zu den
Kritikern der vom Europäischen Parlament bestellten Rechtsgutachter
%(bh:Bakels und Hugenholtz), des Vorsitzenden des BGH-Patentsenats
Richter Klaus J. %(km:Melullis) ebenso wie die %(us:Regierung der
USA), deren Patentjuristen hierin eine Verletzung von TRIPs sehen. 
Der Patentjurist Ralf Nack, der von der Bundesregierung mit Gutachten
beauftragt und von der Kommission in ihrem Entwurf zitiert wurde,
kommentierte die Lehre von der %(q:Technizitätsprüfung im Rahmen der
Erfindungshöheprüfung) mit dem Satz %(q:Das Chaos ist nicht mehr zu
überbieten) und forderte in seiner Dissertation, der Begriff
%(q:technischer Beitrag) müsse als Synonym von %(q:Erfindung) erkannt
werden und Art 52(2) EPÜ müsste als eine Liste von Gegenständen
verstanden werden, die %(q:keine technischen Beiträge im Sinne des
Patentrechts) darstellen.

#gfy: Der Begriff %(q:Computer-Implementierte Erfindung) dient seiner (von
EPA 2000 und Kommission 2002 gelieferten) Definition nach als
Euphemismus für %(q:Programme für Datenverarbeitungsanlagen)
(Anweisungen zum Rechnen mit den herkömmlichen Elementen eines
Datenverarbeitungssystems), die laut Gesetzeslage bekanntlich nicht
patentierbar sind.

#xrW: EPA und Kommission definiert %(q:computer-implementierte Erfindungen)
so, dass nur Programme für Datenverarbeitungsanlagen umfasst sind. 
Die Neuheit des beanspruchten Systems muss in der Art liegen, wie es
programmiert ist.

#uht: Das war der Rats-Arbeitsgruppe zu deutlich.  Sie plädiert heute ebenso
wie im November 2002 für eine Definition, die sowohl reine Programme
(Datenverarbeitungsverfahren) als auch Lehren zur programmgesteuerten
industriellen Nutzung von Naturkräften umfasst:

#hte: Das Parlament hingegen hat den Begriff spiegelbildlich zur Kommission
definiert:  er umfasst nunmehr nur noch Lehren zur programmierten
Nutzung von Naturkräften:

#nfg: Hiermit wird deutlich gesagt, dass der Computer und die Algorithmen,
die zur Steuerung einer technischen Erfindung eingesetzt werden, nicht
technischer Natur sind.  Eine Erfindung könnte, etwa beim ABS, in der
Art liegen, wie Bremskräfte eingesetzt werden.

#ahe: Der Rat möchte nicht entscheiden, auf was sich das neue EPA-Unwort
%(q:computer-implementierte Erfindung) beziehen soll: auf
Computerprogrammen als solche (Kommission) oder auf technischen
Erfindungen (Parlament).  Dies mag politisch schlau erscheinen, aber
es hat gravierende Folgen für die Rechtspraxis.  Es liegt nunmehr in
der Hand des Patentanwaltes, zu entscheiden, ob ein Verfahren als
%(q:computer-implementierte Erfindung) zu gelten hat oder nicht.  Wird
in den Ansprüchen ein Computer genannt, so handelt es sich um eine
%(q:computer-implementierte Erfindung) und die Bestimmungen dieser
Richtlinie kommen zur Anwendung.  Diese Bestimmungen weichen freilich
gerade in der Ratsversion erheblich von den üblichen Bestimmungen des
Patentrechts ab.  So wird es z.B. möglich, zu jedem chemischen
Verfahren (ein solches wird meistens automatisiert und somit nach
Rats-Definition %(q:computer-implementiert) ablaufen) einen
Programm-Anspruch zu erhalten, egal ob die Programmlogik neu ist oder
nicht.   Eine solche Tendenz ist seit einigen Jahren am Europäischen
Patentamt tatsächlich zu beobachten.  Sie kommt einem Bedürfnis der
Patentanmelder, den Markt für Informationen über ihre Verfahren zu
monopolisieren.  In etwas verallgemeinerter Form bedeutet dies die
Sanktionierung dessen, was noch vor kurzem von einem Hamburger Gericht
verweigert wurde:  nicht nur der Markt des Schiffsbaus sondern auch
der Markt der Fachliteratur zum Thema Schiffbau wird monopolisiert. 
Das Prinzip, wonach Patente die Veröffentlichung fördern sollen, wird
endgültig über Bord geworfen.

#mts: Wir haben bereits mehrfach rhetorische Kniffe aufgezeigt, mit denen
das Ratspapier den unerfahrenen Leser hinters licht führt.

#egr: Eine tabellarische Auflistung einiger dieser Kniffe haben wir im
Sommer 2003 anhand der später vom Plenum niedergestimmten
JURI-Änderungsvorschläge %(cj:analysiert).  Diese Vorschläge lehnten
sich eng an das Ratspapier vom November 2002 an und stammen vermutlich
aus der selben Quelle.

#ebw: Eine ähnliche tabellarische Analyse der zahlreichen
Scheinbegrenzungs-Bestimmungen des Ratspapiers wäre hier noch zu
liefern.

#ute: Die am häufigsten verwendete rhetorische List funktioniert wie folgt:

#inW: [A] ist nicht patentfähig, wenn nicht [Bedingung B] erfüllt ist.

#hnr: wobei sich bei näherem Hinsehen ergibt, dass B immer erfüllt ist.

#rWn: Die Mitglieder der Rats-Arbeitsgruppe haben gegen jede mögliche
Begrenzung der Patentierbarkeit und Patentdurchsetzbarkeit Stellung
bezogen und damit eine extreme Gegenposition zum Votum des Parlamentes
vom September 2004 eingenommen, mit der alle Wünsche der
Patentabteilungen der Großunternehmen erfüllt werden.  In einigen
Punkten hat die Ratsarbeitsgruppe frühere Positionen weiter verhärtet.
 Durch das Pochen auf Programmansprüchen, die den eigenen Aussagen der
Arbeitsgruppe zufolge nur formale Bedeutung haben, setzt die
Rats-Arbeitsgruppe der Suche nach Kompromissen zwischen
unterschiedlichen Interessen von vorneherein enge Grenzen.  Hieraus
ergeben sich Fragen nach der Motiviation der Arbeitsgruppenmitglieder.

#dnd: Die Gruppenmitglieder treffen sich sowohl in der Rats-Arbeitsgruppe
als auch im Verwaltungsrat des Europäischen Patentamtes regelmäßig. 
Sie stellen durch Personalunion das Band zwischen der EU und der
Europäischen Patentorganisation (EPO) dar.  Innerhalb der EPO sind sie
Herren der Gesetzgebung.  Sie sind ungern bereit, ihre angestammte
Domäne im Rahmen der EU mit anderen zu teilen.  Sie rütteln nur sehr
ungern an dem gewachsenen Konsens, der ihre politische Stärke
ausmacht.  Eine Beteiligung der EU ist für die Herren der Gesetzgebung
nur insoweit akzeptabel, wie die EU ihren Vorgaben folgt.  Vorgebliche
Ziele wie %(q:Harmonisierung) erweisen sich als bestenfalls
zweitrangig.   Notfalls lässt man gerne die EU-Richtlinie platzen und
ändert in den Hinterzimmern einer neuen Regierungskonferenz den
ungeliebten Art 52 EPÜ.  Frits Bolkestein %(sp:sprach) dieses
Geheimnis am 23. September erstmals offen vor dem EP aus.  Es ergibt
sich darüber hinaus aus zahlreichen öffentlichen und halb-öffentlichen
Stellungnahmen aus dem Umfeld der Europäischen Patentorganisation.

#riA: In dieser Situation dürfte es nunmehr unmöglich sein, sowohl die
politischen Besitzstände der EPO-Verwalter zu wahren als auch die
Patentgesetzgebung der EU voranzubringen.  Die Mitgliedsstaaten müssen
sich entscheiden.

#tnW: explains fairly concisely why the Council's %(q:compromise document)
is the worst of all proposals on software patents that have come out
of the Brussels process.

#spW: %(q:Kompromisspapier) der Ratspräsidentschaft zur
Softwarepatent-Richtlinie von 2004-03-17

#hel: 2-3 Wochen später auf Consilium-Netzsitz in englischer und deutscher
Fassung veröffentlicht, wobei alle Angaben über Positonen der
nationalen Delegationen gelöscht wurden.

#til: letzte öffentliche Version auf Rats-Website

#sWW: Translations to the last remaining EU languages appeared around 18th
of August.

#nmk: Council Document 2004-05-25

#tWs: letzte veröffentlichte Version

#lcW: HTML mit Openoffice vom ursprünglichen Word-Dokument her konvertiert.

#cgW: Council Working Document ST09051

#ebn: eine neuere unveröffentlichte Version mit Fußnoten über Positionen
einzelner nationaler Delegationen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: cons0401 ;
# txtlang: de ;
# multlin: t ;
# End: ;

