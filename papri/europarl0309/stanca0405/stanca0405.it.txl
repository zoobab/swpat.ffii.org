<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lucio Stanca 2004-05-13: Posizione in materia di Brevettabilitá del Software

#descr: In a letter to his colleagues, the Italian Minister of Innovation and Technology denounces the EU Council Presidency's plan to legalise patents on %(q:computer-implemented) algorithms and business methods and asks for a clear Italian position against these plans in the Council.

#mMW: Roma 13 Maggio 2004

#ToW: On. Rocco BUTTIGLIONE, Ministro per le Politiche Comunitarie, Piazza Nicosia, 20 Roma

#oeW: On. Antonio MARZANO, Ministro per le Attività Politiche, Via Molise, 2 Roma

#Rea: On. Letizia MORATTI, Ministro dell'Istruzione, dell'Università e della Ricerca, Viale Trastevere, Roma

#rog: Cari colleghi,

#dii: Ho esaminato la proposta di Direttiva in materia di brevettabilità del software in discussione il 17 maggio 2004 presso il Consiglio Competitività e vi trasmetto la mia posizione.

#rsW: Fermo restando che:

#ual: condivido assolutamente la necessità di addivenire ad un'armonizzazione della materia all'interno dell'Unione

#Woi: condivido la necessità di un intervento normativo specifico su una materia così nuova ed in evoluzione, oggi troppo soggetta ad interpretazioni in mancanza di riferimenti normativi chiari ed univoci.

#ine: tutti condividiamo il principio della non brevettabilità del software in quanto tale.

#mtn: Vi trasmetto le mie forti perplessità in materia.

#uct: Il principio generale della non brevettabilità del software si traduce nella proposta di Direttiva in questione in una generale e dichiarata accettazione del principio della brevettabilità del software, subordinata però alle tre condizioni generali di brevettabilità che per il software risultano specificate in modo tuttora inadeguato. Ciò lascia un grande spazio all'interpretabilità, con rischio di estendere i casi di applicabilità del brevetto del software già possibili nella situazione attuale (30.000 brevetti software e simili già concessi dall'European Patent Office). Pertanto sarebbe auspicabile nell'ambito della norma una più dettagliata e delimitante definizione delle condizioni di applicabilità del brevetto.

#esa: La sostanziale eliminazione degli emendamenti parlamentari dalla proposta in votazione, il non inserimento della proposta italiana agli atti e della proposta di Germania, Belgio e Danimarca in materia di

#nci: Il riconoscimento della tutela in cambio della condivisione della conoscenza, principio fondante del %(q:contratto) di brevetto, per essere realizzato necessita di strumenti adeguati di pubblicità/condivisione, che nel caso del software oggi non esistono e non sono previsti nella proposta di Direttiva. Ciò tenderà inevitabilmente a svantaggiare le PMI del settore e a creare un ulteriore ampliamento del contenzioso.

#Wop: Un eccessivo ricorso al brevetto del software potrebbe avere conseguenze rilevanti sulla concorrenza del mercato del software.  La Direttiva in discussione ad esempio conferma l'uso attuale del brevetto per protocolli ed altri elementi software che rendono più difficoltosa l'interoperabilità tra software prodotti da aziende diverse, limitando di fatto lo sviluppo del mercato.

#lid: L'apertura prevista dalla proposta di Direttiva di monitorare l'applicazione e rivedere la normativa stessa appare di fatto assolutamente inefficace nel mercato del software. Infatti ai tre anni di monitoraggio previsti si aggiungono i due anni concessi per la ratifica da parte degli stati membri e l'anno (minimo) necessario per condurre una revisione della norma in discussione, tempi incompatibili con la dinamicità di questo mercato.

#Wci: In conclusione

#iWt: In considerazione del fatto che lo sviluppo delle grandi innovazioni della Società dell'Informazione, Internet, il WWW e, più di recente, le applicazioni Open Source, sono il risultato di una grande collaborazione a livello mondiale, oggi considerate beni digitali accessibili da tutti (Digital Commons), la proposta di Direttiva sembra andare in controtendenza rispetto agli sviluppi degli ultimi anni.

#cWt: Per i motivi indicati vi chiedo quindi di rappresentare al Consiglio Competitività la posizione critica del Governo Italiano.

#oeo: Data la complessità e le implicazioni della materia e la sua rilevanza per lo sviluppo dell'industria ICT italiana ed europea, raccomando di evitare decisioni affrettate ma di avviare iniziative comuni per approfondire ulteriormente la materia.

#uWn: Lucio Stanca

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/stanca0405.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: stanca0405 ;
# txtlang: it ;
# multlin: t ;
# End: ;

