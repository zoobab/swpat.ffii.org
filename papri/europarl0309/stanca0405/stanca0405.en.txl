<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lucio Stanca 2004-05-13 Letter on Software Patents

#descr: In a letter to his colleagues, the Italian Minister of Innovation and Technology denounces the EU Council Presidency's plan to legalise patents on %(q:computer-implemented) algorithms and business methods and asks for a clear Italian position against these plans in the Council.

#mMW: Rome 13 May 2004

#ToW: On. Rocco BUTTIGLIONE, Ministro per le Politiche Comunitarie, Piazza Nicosia, 20 Roma

#oeW: On. Antonio MARZANO, Ministro per le Attività Politiche, Via Molise, 2 Roma

#Rea: On. Letizia MORATTI, Ministro dell'Istruzione, dell'Università e della Ricerca, Viale Trastevere, Roma

#rog: Dear Colleagues,

#dii: I have examined the Directive proposal on the matter of patentability of software to be discussed on May 17, 2004 in the Competition Council and here I transmit you my position.

#rsW: Given that:

#ual: I fully agree with the necessity to harmonize the matter inside the Union

#Woi: I agree with the necessity of a specific norm on a matter so new and evolving, today too subject to interpetation because we miss specific clear and unique rules.

#ine: we all agree to the principle of the non-patentability of software as such.

#mtn: I transmit you my perplexity on the matter.

#uct: The generic principle of non-patentability of software translates in this Directive proposal into a common and declared acceptance of the principle of patentability of software, but under the three generic conditions for the patentability, which, for the software field, are specified in an inadequate way.  That leaves much space to interpretation, with the risk of extending the cases of applications of software patents, yet possible in the actual situation (30.000 software and similar patents yet released by the European Patent Office).  Therefore it would be advisable to pursue a more detailed and limiting definition of the conditions of patentability.

#esa: technical

#nci: The acknowledgement of the protection in exchange of the offering of knowledge, founding principle of the patent %(q:contract) needs, to be realized, adequate instruments of offering knowledge, which in case of software, today, does not exist and are not foreseen in the Directive proposal. That will inevitably disadvantage the SMEs of the sector and will enlarge the contentions.

#Wop: An excessive usage of software patents may have relevant consequences on competition in the software market. The Directive in discussion confirms, for example, actual use of patents on protocols and other software elements which make more difficult to pursue interoperability between programs produced by different enterprises, limiting in fact the development of the market.

#lid: The monitoring and possible review provided by the Directive proposal seem completely uneffective in the software market. To the three years of monitoring there add 2 years provided for ratification by member states and another year (minimum) necessary to conduct a revision of the norm in discussion; these times are incompatible with the dynamicity of this market.

#Wci: In conclusion

#iWt: In consideration of the fact that the development of big innovations in the Society of Information, Internet, the WWW and more recently, the Open Source applications, are the result of a big collaboration at a worldwide level, considered today as digital goods accessible to everyone (Digital Commons), the Directive proposal seem to be in counter-tendency to the developments of recent years.

#cWt: For the presented motivations, I ask you to represent at the Competitivity Council, the critical position of the Italian Government.

#oeo: Given the complexity and implications of the matter and its relevance for the development of the Italian and European ICT Industry, I recommend to avoid hurried decisions and to start common initiatives to further study the matter.

#uWn: Lucio Stanca

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/stanca0405.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: stanca0405 ;
# txtlang: en ;
# multlin: t ;
# End: ;

