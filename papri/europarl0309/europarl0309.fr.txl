<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mpW: The amendments approved, from the EP.

#sdn: consolidated version by EP, changes were still being applied by the
secretariat during the days after the vote.  E.g. %(q:contribute) in
%(q:technical features that any invention must contribute) was changed
to %(q:posess).

#WMW: Europarl 03-09-24: Arlene McCarthy's summary of the vote

#fth: Mentions mainly the parts of the vote that are congruent with her own
proposals and unimportant aspects, such as the novelty grace period,
but omits everything that really limits patentability.

#purp: Objectif

#defi: Définitions

#efn: Champs de Technique

#eai: Rèles de Brevetabilité

#uoa: Exclusions de la brevetabilité

#ono: Formulation de la revendication et autres dispositions

#tel: Interopérabilité

#mon: Monitoring

#rep: Report on the effects of the Directive

#ass: Impact assessment

#titl: Directive concernant la brevetabilité des inventions mises en oeuvre
par ordinateurs

#Art1: Cette directive établit les règles pour la brevetabilité des
inventions mises en oeuvre par ordinateurs.

#Am36: %(q:invention mise en oeuvre par ordinateur) désigne toute invention
au sens de la Convention sur le brevet européen dont l'exécution
implique l'utilisation d'un ordinateur, d'un réseau informatique ou
d'un autre appareil programmable et présentant dans sa mise en oeuvre
une ou plusieurs caractéristiques non techniques qui sont réalisées
totalement ou en partie par un ou plusieurs programmes d'ordinateurs,
en plus des caractéristiques techniques que toute invention doit
apporter;

#Am107: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in technical field. The technical
character of the contribution is one of the four requirements for
patentability. Additionally, to deserve a patent, the technical
contribution has to be new, non-obvious, and susceptible of industrial
application. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes.

#Am97: %(q:technical field) means an industrial application domain requiring
the use of controllable forces of nature to achieve predictable
results. %(q:Technical) means %(q:belonging to a technical field).

#Am38: %(q:industrie) dans le sens de la loi sur les brevets signifie
%(q:production automatisée de biens matériels);

#Am45: Les Etats membres veillent à ce que le traitement des données ne soit
pas considéré comme un domaine technique au sens du droit des brevets
et à ce que les innovations en matière de traitement des données ne
constituent pas des inventions au sens du droit des brevets.

#Am16N1: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and involve an inventive
step. In order to involve an inventive step, a computer-implemented
invention must make a technical contribution.

#Am16N2: Member States shall ensure that a computer-implemented invention
making a technical contribution constitutes a necessary condition of
involving an inventive step.

#Am100P1: The significant extent of the technical contribution shall be assessed
by consideration of the difference between the technical elements
included in the scope of the patent claim considered as a whole and
the state of the art, irrespective of whether or not such features are
accompanied by non-technical features.

#Am70: Pour déterminer si une invention mise en oeuvre par ordinateur apporte
une contribution technique, il y a lieu d'établir si elle apporte une
connaissance nouvelle sur les relations de causalité en ce qui
concerne l'utilisation des forces contrôlables de la nature et si elle
a une application industrielle au sens strict de l'expression, tant
sous l'angle de la méthode que sous celui du résultat.

#Am17: Une invention mise en oeuvre par ordinateur n'est pas considérée comme
apportant une contribution technique uniquement parce qu'elle implique
l'utilisation d'un ordinateur, d'un réseau ou d'un autre appareil
programmable. En conséquence, ne sont pas brevetables les inventions
impliquant des programmes d'ordinateurs, qui mettent en oeuvre des
méthodes commerciales, des méthodes mathématiques ou d'autres
méthodes, si ces inventions ne produisent pas d'effets techniques en
dehors des interactions physiques normales entre un programme et
l'ordinateur, le réseau ou un autre appareil programmable sur lequel
il est exécuté.

#Am60: Les Etats membres veillent à ce que les solutions, mises en oeuvre par
ordinateur, à des problèmes techniques ne soient pas considérées comme
des inventions brevetables au seul motif qu'elles améliorent
l'efficacité de l'utilisation des ressources dans le système de
traitement des données.

#Am101: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is as a programmed device, or as a
technical production process.

#Wce: Member States shall ensure that patent claims granted in respect of
computer-implemented inventions include only the technical
contribution which justifies the patent claim. A patent claim to a
computer program, either on its own or on a carrier, shall not be
allowed.

#Am103: Les Etats membres veillent à ce que la production, la manipulation, le
traitement, la distribution et la publication de l'information, sous
quelque forme que ce soit, ne puisse jamais constituer une contrefaçon
de brevet, directe ou indirecte, même lorsqu'un dispositif technique
est utilisé dans ce but.

#Am104N1: Les Etats membres veillent à ce que l'utilisation d'un programme
d'ordinateur à des fins qui ne relèvent pas de l'objet du brevet ne
puisse constituer une contrefaçon de brevet, directe ou indirecte.

#Am104N2: Les Etats membres veillent à ce que, lorsqu'une revendication de
brevet mentionne des caractéristiques impliquant l'utilisation d'un
programme d'ordinateur, la description publiée comprenne une mise en
½uvre de référence, opérationnelle et bien documentée, de ce
programme, sans conditions de licence restrictives.

#Am19: Les droits conférés par les brevets d'invention délivrés dans le cadre
de la présente directive ne portent pas atteinte aux actes permis en
vertu des articles 5 et 6 de la directive 91/250/CEE concernant la
protection juridique des programmes d'ordinateurs par un droit
d'auteur, notamment en vertu des dispositions particulières relatives
à la décompilation et à l'interopérabilité.

#Am76P1: Les Etats membres veillent à ce que, lorsque le recours à une
technique brevetée est nécessaire à une fin significative, par exemple
pour assurer la conversion des conventions utilisées dans deux
systèmes ou réseaux informatiques différents, de façon à permettre
entre eux la communication et l'échange de données, ce recours ne soit
pas considéré comme une contrefaçon de brevet.

#Am71: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, and on European businesses, especially small and
medium-sized enterprises and the open source community, and electronic
commerce.

#Art81: The Commission shall report to the European Parliament and the Council
by [DATE (three years from the date specified in Article 9(1))] at the
latest on

#Art81a: the impact of patents for computer-implemented inventions on the
factors referred to in Article 7;

#Am92: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims, are adequate;
and

#Art82c: whether difficulties have been experienced in respect of Member States
where the requirements of novelty and inventive step are not examined
prior to issuance of a patent, and if so, whether any steps are
desirable to address such difficulties.

#Am23: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-implemented
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-implemented
inventions;

#Am24: whether it would be desirable and legally possible having regard to
the Community's international obligations to introduce a 'grace
period' in respect of elements of a patent application for any type of
invention disclosed prior to the date of the application;

#Am25: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the Convention on the Grant of
European Patents, also in the light of the advent of the Community
patent;

#Am26: how the requirements of this Directive have been taken into account in
the practice of the European Patent Office and in its examination
guidelines.

#Am81: whether the powers delegated to the EPO are compatible with the need
to harmonise Community legislation, and with the principles of
transparency and accountability.

#Am89: the impact on the conversion of the conventions used in two different
computer systems to allow communication and exchange of data;

#Am93: whether the option outlined in the Directive concerning the use of a
patented invention for the sole purpose of ensuring interoperability
between two systems is adequate;

#Am94: In this report the Commission shall justify why it believes an
amendment of the Directive in question necessary or not and, if
required, will list the points which it intends to propose an
amendment to.

#Am27: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
assess the impact of this Directive and, where necessary, submit
proposals for amending legislation to the European Parliament and the
Council.

#rFr: CEC proposal and FFII counter-proposal.

#nin: Amendment proposals on which the EP voted on 2003/09/24

#ent: Amendment Proposals on which JURI voted in June

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

