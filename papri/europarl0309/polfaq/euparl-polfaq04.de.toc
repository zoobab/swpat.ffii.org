\contentsline {section}{\numberline {1}Wie ist das Verh\"{a}ltnis der Art. 9-11 und 27 TRIPS im Hinblick auf computer-implementierte Erfindungen zu werten?}{2}{section.1}
\contentsline {section}{\numberline {2}Wie ist das Verh\"{a}ltnis zwischen Urheberrechts- und Patentschutz zu bewerten?}{3}{section.2}
\contentsline {section}{\numberline {3}Welche Auswirkungen h\"{a}tte der ``worst case'', sprich die Verabschiedung des Richtlinienentwurfs in der vom Rat im November 2002 vorgelegten Form (konkrete Beispiele)?}{4}{section.3}
\contentsline {section}{\numberline {4}Welche Ver\"{a}nderungen sollten aus rechtstechnischer Sicht an dem vom EP vorgelegten Entwurf vorgenommen werden.}{4}{section.4}
\contentsline {section}{\numberline {5}Was unterscheidet Software von allen \"{u}brigen der Patentierung zug\"{a}nglichen ``Techniken'', das eine andere Behandlung rechtfertigt?}{5}{section.5}
\contentsline {section}{\numberline {6}Ist eine Herausnahme von Software aus dem geltenden Patentrecht zu rechtfertigen - wenn ja, warum?}{6}{section.6}
\contentsline {section}{\numberline {7}Unterschiede zwischen (auch kommerzieller) OSS und Closed-Source-Software, die eine andere Behandlung rechtfertigt?}{6}{section.7}
\contentsline {section}{\numberline {8}EPA-Praxis: Unterstellt, es werden (rechtswidrige) Softwarepatente erteilt: Wie sieht die Einspruchsquote hiergegen aus? Sollte diese gering sein - warum? Gibt es M\"{o}glichkeit z.B. f\"{u}r Bundesregierung / Fachverb\"{a}nde o.\"{A}., hier Unterst\"{u}tzung zu leisten?}{7}{section.8}
\contentsline {section}{\numberline {9}Welche Strategie verfolgt die OSS-Gemeinde?}{8}{section.9}
\contentsline {section}{\numberline {10}Welche Art von Entwicklungen im Softwarebereich sollten / m\"{u}ssen patentierbar sein?}{8}{section.10}
\contentsline {section}{\numberline {11}Welche Patente sind noch tragbar (konkrete Beispiele), so dass die Kreativit\"{a}t der OSS-Programmierer nicht eingeschr\"{a}nkt wird?}{9}{section.11}
\contentsline {section}{\numberline {12}Kommentierte Verweise}{9}{section.12}
