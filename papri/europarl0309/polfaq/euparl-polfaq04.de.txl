<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fragen und Antworten zum den Änderungen des Europäischen Parlamentes

#descr: Aus der Perspektive von Politikern

#eWe: Wie ist das Verhältnis der Art. 9-11 und 27 TRIPS im Hinblick auf computer-implementierte Erfindungen zu werten?

#seu: Wie ist das Verhältnis zwischen Urheberrechts- und Patentschutz zu bewerten?

#tie: Welche Auswirkungen hätte der %(q:worst case), sprich die Verabschiedung des Richtlinienentwurfs in der vom Rat im November 2002 vorgelegten Form (konkrete Beispiele)?

#uSu: Welche Veränderungen sollten aus rechtstechnischer Sicht an dem vom EP vorgelegten Entwurf vorgenommen werden.

#SWr: Was unterscheidet Software von allen übrigen der Patentierung zugänglichen %(q:Techniken), das eine andere Behandlung rechtfertigt?

#see: Ist eine Herausnahme von Software aus dem geltenden Patentrecht zu rechtfertigen - wenn ja, warum?

#sse: Unterschiede zwischen (auch kommerzieller) OSS und Closed-Source-Software, die eine andere Behandlung rechtfertigt?

#WWA: Wie ist die aktuelle Patentierungspraxis des EPA zu bewerten? Welche (negativen) Beispielsfälle aus der aktuellen EPA-Praxis lassen sich anführen?

#fsW: EPA-Praxis: Unterstellt, es werden (rechtswidrige) Softwarepatente erteilt: Wie sieht die Einspruchsquote hiergegen aus? Sollte diese gering sein - warum? Gibt es Möglichkeit z.B. für Bundesregierung / Fachverbände o.Ä., hier Unterstützung zu leisten?

#WfG: Welche Strategie verfolgt die OSS-Gemeinde?

#vrp: Welche Art von Entwicklungen im Softwarebereich sollten / müssen patentierbar sein?

#dsi: Welche Patente sind noch tragbar (konkrete Beispiele), so dass die Kreativität der OSS-Programmierer nicht eingeschränkt wird?

#riW: Der Begriff %(q:computer-implementierte Erfindungen) ist wegen seiner Mehrdeutigkeit und Tendenziosität für eine verständnisfördernde Diskussion ungeeignet, s. %(DOK).

#bge: Das TRIPs-Abkommen trifft keine zwingenden Aussagen über die Patentierbarkeit von Software.

#zuW: Es lässt sich zur Argumentation sowohl für als auch gegen die Patentierbarkeit von Software heranziehen.

#etW: Die TRIPs-basierte Argumentation für Softwarepatente ist hinreichend bekannt, s. z.B. %(ds:Schiuma 2000).

#avi: Gegen Patentierbarkeit von Software nach TRIPs spricht:

#dWW: Der Grundsatz der weitestmöglichen Trennung der Gebiete des Immaterialgüterrechts, s. folgende Frage.  TRIPs verlangt ausdrücklich das Urheberrecht für Software.  Bei Anwendung des Trennungsgrundsatzes folgt hieraus die Nichtpatentierbarkeit, wie sie zur Zeit der Beratungen um TRIPs auch weithin anerkannt war.

#nfW: In Art 27 TRIPs werden die Schlüsselbegriffe %(q:Technik), %(q:Industrie) und %(q:Erfindung) genannt, mit denen die Patentierbarkeit begrenzt werden soll.  Diese können nur dann begrenzende Wirkung entfalten, wenn sie mit dem %(sk:klassischen Begriff der Technischen Erfindung) verbunden werden, aus dem die Nichtpatentierbarkeit von Software folgt.  Gelingt es nicht, den TRIPs-Schlüsselbegriffen eine klare begrenzende Bedeutung zuzuweisen, so werden Ziele des TRIPs-Vertrages und seines WTO-Kontextes (freier Wettbewerb im international Handel zu vorhersehbaren Spielregeln) konterkariert.

#den: Nach dem Grundsatz der weitestmöglichen Trennung von Immaterialgüterrechtssystemen sollte weitestgehend vermieden werden, dass zweierlei Rechte sich auf den gleichen Gegenstand beziehen können.  Es ist möglich, per Urheberrecht erworbenes Eigentum durch Patente zu entwerten.  Es wird sowohl beim Urheberrecht als auch beim Patentrecht Nachahmung in einem bestimmten (engeren oder weiteren) Bereich ausgeschlossen.  Patente erlauben eine Ausschlussbreite, die das Urheberrecht aufgrund wohlüberlegter Güterabwägungen versagt.  Zu den in die Abwägung einbezogenen Gütern gehören Freiheiten, die Verfassungsrang genießen.  S. %(kk:Stellungnahme des Patentprüfers Dr. Swen Kiesewetter-Köbinger) in der %(ba:Bundestagsanhörung von 2001).  Die lange Rechtstradition, auf die diese Sicht sich stützt findet vor allem in den BGH-Beschlüssen %(LST) mustergültige Formulierung.

#Wwn: Anmerkungen zum RA-Gutachten

#hci: Patent- und UrhR haben verschiedene Schutzbereiche.  Der Schutzgegenstand ist in beiden Fällen ein logisches Konstrukt.  Die Unterscheidung zwischen verschiedenen %(q:Schutzgegenständen) ist künstlich.  Statt %(q:Schutz) sollte man nach möglichkeit %(q:Ausschluss) sagen.  Der Begriff %(q:Ausschlussrecht) ist in der deutschen Rechtstradition etabliert und sachlicher, da er nicht für den Rechteinhaber Partei ergreift.

#ela: Man sollte es vermeiden, bei Software von %(q:Erfindungen) zu sprechen.  %(q:Erfindung) impliziert Patentierbarkeit.  Passender ist %(q:Innovation).

#uWi: Den Überlegungen zur Dekompilation ist zuzustimmen, auch wenn diese nicht den einzigen oder wichtigsten Grund gegen die Überschneidung von Ausschlussrechten darstellen.  Weitere Gründe erläutert Kiesewetter-Köbinger, s. oben.  Als Beispiel zu (3) sei noch erwähnt: Im Prozess Stac vs Microsoft wurde Microsoft wegen Patentverletzung und Stac wegen Dekompilierung verurteilt.

#WRs: Der Rats-Entwurf würde die derzeitige Praxis des Europäischen Patentamtes in vollem Umfang legalisieren und damit die Verhältnisse in Europa, was Softwarepatente betrifft, denen in der USA angleichen.  Die meisten der Gruselpatente, über deren Durchsetzung in den USA in den letzten Jahren berichtet wird, sind auch am EPA erteilt worden.  Der Ratsentwurf enthält nichts, was sie verhindern könnte.  Ähnlich wie in den USA bleibt nur akribische Neuheitsrecherche.  Das europäische Patentamt neigt dazu, sich mit noch geringerer Erfindungshöhe zufrieden zu geben als das amerikanische (s. unten), und Patentansprüche werden dank Äquivalenzdoktrin stärker zugunsten des Inhabers ausgelegt als in den USA.

#rWn: Anmerkungen zu RA-Stellungnahme

#meg: Zustimmung.  Wenn man von einem %(q:Status Quo) spricht, sollte man hierbei beachten, dass er Rechtsmeinungen mit einbezieht, die den Technischen Beschwerdekammern des EPA die Gefolgschaft versagen, und dass auch die Praxis der TBK jederzeit durch eine Anrufung der Großen Beschwerdekammer oder durch andere Mittel zurück auf den Weg einer ordentlichen Gesetzesauslegung gebracht werden kann.  Mehr zur Gesetzeswidrigkeit der derzeitigen Spruchpraxis s. unten.

#Wds: Art 4(1) muss gestrichen werden, denn hier wird der Begriff %(q:technischer Beitrag) im Sinne der neueren EPA-Rechtsprechung (als %(q:Aspekt der erfinderischen Tätigkeit)) und damit nicht im Sinne der Art 2 und 5 (als Synonym für %(q:technische Erfindung), %(q:technische Erfindung) etc) verwendet.

#rni: Auch vereinzelte Erwägungsgründe stehen im Widerspruch zu den zentralen Aussagen der EP-Fassung, noch im Detail auszuarbeiten.

#Wut: zu RA-Gutachten

#ugo: Eine systematische Begründung für die unetschiedliche Behandlung von Industrie und %(q:Einsatz durch Handwerker und Freiberufler) ist durchaus ersichtlich.  Das Patentsystem ist ein Spiel für die Schwerindustrie, das hohen finanziellen Einsatz und viel Ausdauer erfordert.

#ren: Die Anforderung der %(q:industriellen Anwendbarkeit) wird durchaus auch von Verfahren erfüllt, welche Handwerker und Freiberufler einsetzen.  Dieses Kriterium ist auch dann sehr weich, wenn es eng gefasst wird.

#Wnn: Software gehört laut juristischen Definitionen zum %(q:tertiären Sektor).  Die Einteilung nach Sektoren stellt eine der wenigen Möglichkeiten zur Abgrenzung des Patentwesens dar.  Auch TRIPs sieht mit dem Stichwort %(q:industrial application) / %(q:application industrielle) / %(q:gewerbliche Anwendung) diese Möglichkeit vor.

#ksW: Ähnlich wie beim Technik gibt es kaum alternativen zur Festmachung des Begriffs am Materiellen.  Im weiteren sinne ist natürlich auch Amazons Buchhandel ein Gewerbe.

#fsm: Auch die Tätigkeit von Freiberuflern gilt nicht als %(q:gewerblich).  Der Begriff %(q:gewerblich) verdankt seine Stellung in der deutschen Terminologie möglicherweise sprachpuristischen Bemühungen der Rechtssprache des 19. Jahrhunderts.  Gemeint war 1873 auch %(q:Industrie einschließlich Bergbau).  Im japanischen Patentrecht war vom %(q:produzierenden Gewerbe) die Rede.

#Ens: Insbesondere die Spanier und Franzosen im Europäischen Parlament legen wert auf eine Begrenzung der Patentierbarkeit durch den Begriff %(q:Industrie).  Dies entspricht ihrer Tradition und hat die Unterstützung von Rechtsgelehrten wie dem spanischen Professor Bercowitz, der vom EP angehört wurde.

#sWn: Man kann die Patentierbarkeit von Software dann ausschließen, wenn man den Begriff %(q:gewerbliche/industrielle Anwendbarkeit) weiterhin unklar lässt.  Das Problem muss nicht im Rahmen einer Softwarepatent-Richtlinie gelöst werden.  In diesem Fall sollte man den Begriff dann aber auch nicht verwenden, um Begrenzungen der Patentierbarkeit vorzutäuschen.

#lve: Software ist reine Information.  Man kann Software auf seine Festplatte laden und quasi-kostenlos vervielfältigen.  Mit einem Auto geht dies nicht.  Hieraus ergibt sich eine in vielfacher Hinsicht andere Ökonomie.  Dass diese im Softwarebereich innovationshemmend wirkt, ist durch zahlreiche Studien, darunter auch von der Bundesregierung in Auftrag gegebene, belegt.

#ken: Software ist ein reines Geistesprodukt.  Es erfordert kein Experimentieren mit Naturkräften.  Software ist in dem Maße genial und bewundersnwert, wie sie abstrakt und untechnisch ist.  Auch hieraus ergeben sich zahlreiche Implikationen.  S. u.a. %(LST).

#tWn: Die Nichtpatentierbarkeit von Software folgt aus dem traditionellen Verständnis der Technischen Erfindung als %(q:Lösung von Problemen durch Einsatz beherrschbarer Naturkräfte).  Der naturkräftebasierte Erfindungsbegriff stellt die einzige bekannte Möglichkeit dar, ein Ausufern der Patentierbarkeit ins Unübersehbare zu verhindern, s. %(dp:Dispositionsprogramm-Beschluss).  Geschäftsmethodenpatente sind eine Konkretisierung von Softwarepatenten.  Softwarepatente sind die abstrakteste aller Patentkategorien.

#xB2: Software unterliegt dem Urheberrecht.

#eWg: Die Frage ist möglicherweise falsch.  Man kann genauso umgekehrt fragen: was unterscheidet mechanisches Konstruieren so sehr von Software, dass es dem Patentrecht und nicht dem Urheberrecht unterliegt?  Warum sollen Mechaniker nicht die gleichen Freiheiten genießen, die wir Softwareentwicklern zugestehen?  Dabei darf nicht davon ausgegangen werden, dass das Patentwesen ein Erfolgsmodell oder gar ein moralischer Maßstab sei.  Insbesondere gibt es keine Pflicht, mit anderen mitzuleiden.

#loe: s. oben

#caW: Es ist nicht richtig, von Ausnahme oder Herausnahme zu sprechen.

#sij: In Art 52(2) EPÜ sind Nicht-Erfindungen, d.h. Gegenstände, die keine Erfindungen im Sinne des Patentrechts darstellen, aufgelistet.  Software-Innovationen (Algorithmen) sind keine Erfindungen.  Sie sind %(e:ausgeschlossen), nicht %(e:ausgenommen).  Das Patentrecht kann nicht funktionieren, wenn es über den Bereich der technischen Erfindung ausgeweitet wird.

#wgc: Freie Software und Shareware bekommen schwer Zutritt zu üblichen Patentverwertungskartellen, da sie deren Forderungen nach Stückzahlabrechnung nicht Folge leisten können.

#sle: Entwickler freier Software können aufgrund der Regeln ihres Entwicklungsmodells nur zu sehr nachteiligen Bedingungen im Patentopoly mitspielen.  Patentopoly ist ein Spiel für die Schwerindustrie, das viel Kapital und langen Atem und die Bereitschaft zur Verfolgung von Verletzungen und Maximalverwertung gegen andere Mitspieler verlangt.  Auch große Firmen wie IBM werden im Falle eines Patentstreites nicht unbedingt ihre Ressourcen hinter ein Projekt der Freien Software werfen, das ihnen wenig exklusiven Nutzen bringt.  IBM hält sich aus dem Distributionsgeschäft zurück, weil die Firma Patentangriffe fürchtet.

#rwc: Freie Software ist leichter angreifbar, weil der Quelltext zur Einsicht offen liegt.

#edW: In anderer Hinsicht sind Autoren proprietärer Software stärker gefährdet.  Z.B. sind sie die dankbareren Zielscheiben für Patentverwertungsfirmen, die nur mit dubiosen Patenten unter Geheimhaltungsauflage punktuell abkassieren wollen.  Ferner können sie nicht so leicht auf Vorveröffentlichungen verweisen.  Der FFII hat in seinem Gegenvorschlag, aus dem das EP viele Ideen übernahm, daher auch verlangt, dass Software in binärer Form auch als Vorveröffentlichung gelten müsse.  Zugleich hat er sich Vorschlägen nach einer Sonderbehandlung freier Software gegenüber sehr zurückhaltend gezeigt.  Solche Vorschläge sind nicht prinzipiell ungerechtfertigt, aber sie senden eine falsche Botschaft aus.  Die Entwickler proprietärer Software haben es keineswegs verdient, dass man sie einem Patentregime unterwirft.

#rni2: Die aktuelle Praxis des EPA unterscheidet sich in bezug auf Software nicht wesentlich von der US-Praxis.

#eee: Das EPA genoss bis ca Mitte der 90er Jahre den Ruf, in einigen Gebieten relativ gute Neuheitsrecherchen zu leisten.  Dann wurde zunehmend an personeller Kompetenz gespart, um der Nachfrage nach kostengünstigem Patentschutz zu entsprechen.  Im Hinblick auf Software galt das EPA nie als besonders kompetent.  Software-Neuheitsrecherche ist allerdings auch bei einer Vervielfachung der Personalkosten nicht unbedingt in angemessener Weise zu leisten.

#hsn: Hinsichtlich der Anforderungen an Erfindungshöhe unterscheidet sich das EPA nicht wesentlich vom US-Patentamt.  Von Vergleichsstudien bescheinigt worden, dass es im Schnitt etwas geringere Anforderungen als die Patentämter der USA und Japans stellt.

#hed: Die vom EPA erteilten Patentansprüche neigen den selben Vergleichsstudien zufolge dazu, in ähnlich gelagerten Fällen etwas breiter zu sein.  In Europa werden dank Äquivalenzlehre Patentansprüche überdies generell breiter ausgelegt als in den USA.

#ces: Die Anforderungen hinsichtlich Erfindungshöhe und Anspruchsbreite lassen sich nicht ohne weiteres strenger gestalten.  Das Patentwesen bietet keine brauchbaren Regeln, mit denen sich die Anspruchsbreite und Erfindungshöhe in auf ein für den Fachmann vernünftig erscheinenden Rahmen halten lassen.  Ein Ausufern ins Absurde wurde in der Vergangenheit nur durch zusätzliche Filter wie den Begriff der %(e:technischen Erfindung) verhindert.  Diesen hat das EPA durch eine Reihe von Entscheidungen seit 1986 bis zur Unkenntlichkeit und Unbrauchbarkeit aufgeweicht.

#tcj: Patente auf %(q:computer-implementierte Geschäftsmethoden) werden im allgemeinen erteilt und von den Technischen Beschwerdekammern aufrecht erhalten.  Das EPA spielt noch hier und da in unsystematischer und intransparenter Weise mit dem alten Technikbegriff, um einige ungeschickt formulierte Patentansprüche auf Geschäftsmodelle, didaktische Methoden etc zurückzuweisen.  Bei der Formulierung der Ansprüche muss auf das Einsparen von Rechenressourcen abgehoben werden.  Ein Patentanwalt, dem es nicht gelingt, unter diesen Bedingungen Geschäftsmodelle beim EPA zur Patenterteilung zu bringen, wäre sein Honorar nicht wert.

#cae: Die Rechtsprechung der Technischen Beschwerdekammern beruft sich seit etwa 1986 auf hausgemachte Doktrinen, die mit den üblichen Methoden der Gesetzesauslegung nicht vereinbar sind.

#eih: Das Gesetz, das auf diese Weise zugunsten einer willkürlichen und kasuistischen Rechtsprechung ausgehebelt wurde, ist keineswegs unklar oder unzeitgemäß.  Bis wohin die Rechtsprechung gehen durfte und ab wann die Grenze ins Gesetzeswidrige überschritten würde, haben Lehrbücher und Kommentare der 70er und 80er Jahre ausführlich erläutert.  In diesen Werken wird auch erklärt, dass die Datenverarbeitung gerade deshalb, weil sie als Hilfsmittel alle Wirtschaftszweige durchdringt, patentfrei bleiben muss.  Durch die inzwischen weiter gewachsene Durchdringung ist das Gesetz von 1973 nur noch zeitgemäßer.  Die Doktrinen der 70er und 80er Jahre sind heute mit der gleichen Klarheit auf Beispielpatente anwendbar wie damals.  Ob man sie anwendet, ist nur eine Frage des politischen Willens.  Dieser änderte sich in den späten 80er Jahren.

#Wja: Die Prüfer des EPA erteilen in der Regel Patente nach den Vorgaben der Technischen Beschwerdekammern dieses Amtes.  Diese Spruchkörper haben gesetzeswidrige Rechtsnormen gesetzt, denen mithilfe einer Richtlinie Geltung verschafft werden soll.  Im Falle eines Einspruchs bei den Technischen Beschwerdekammern kommen jedoch i.d.R. nur deren eigene Rechtsnormen zur Anwendung.

#nqe: Anders könnte es aussehen, wenn ein Einspruch bis zur Großen Beschwerdekammer des EPA verfolgt würde.  Nur die Große Beschwerdekammer ist in Rechtsfragen zuständig und sie hat zur Frage der Auslegung von Art 52 EPÜ bislang nicht gesprochen.

#enW: Es kann kein Zweifel daran bestehen, dass die Beschwerdekammern für Einflüsterungen des um den Verwaltungsrat des EPA versammelten Beraterkreises sehr empfänglch sind und im Konsens mit dem Präsidenten des EPA handeln.  Auf den hochpolitischen Richtungsentscheidungen der TBA von 1998 fanden sich Vermerke des Präsidenten, wonach das Amt gedenkt, seine Praxis nach dieser Entscheidung auszurichten.  Die TBA-Richter werden vom Präsidenten ernannt und der Präsident kann die Verlängerung ihrer Amtszeit nach 5 Jahren verhindern.  Es ist nur eine Frage des politischen Willens, ob man die EPA-Kammmern dazu bewegen kann, die Fehlentwicklung der letzten Jahre zu korrigieren.

#WPh: Es kann auch sehr hilfreich sein, durch Prozesse auf nationale Gerichte einzuwirken.

#eWR: Der normale Weg der Beeinflussung wäre jedoch die Verabschiedung eines Gesetzes, etwa die Umsetzung der anstehenden EU-Richtlinie gemäß EP-Fassung.

#aiw: Falls die Rats-Fassung umgesetzt würde, wäre jede Möglichkeit einer Besserung auf dem Justizwege auf Dauer verbaut.

#oWs: Eine solche Gemeinde ist schwer auszumachen.

#igb: Einige Hinweise auf Strategien des FFII ergeben sich aus

#Kie: Keine.

#rlr: Anweisungen zum Betrieb von Datenverarbeitungsanlagen, egal in welcher Form sie beansprucht werden, müssen außen vor bleiben.  Das ist eine klare Regel, die leicht handhabbar ist.  Etwas schwieriger wird die Abgrenzung bei programmgesteuerten Spezialausrüstungen.

#ocn: Ein Antiblockiersystem sollte patentierbar sein, insoweit nicht die Steuerungslogik (Software) sondern der neuartige Umgang mit Bremskräften im Vordergrund steht und beansprucht wird.  Die Ausführung eines Steuerungsprogramms auf einem Universalrechner (z.B. zu Simulationszwecken) darf kein Patent verletzen, die erfinderische Nutzung der Bremskräfte hingegen schon.

#lnv: Fertige Handlungsanleitungen zur Abgrenzung zwischen Software und Naturkräfteerfindungen und zur Auslegung von Art 52 EPÜ bieten u.a.

#nnW: Softwarepatente sind dann tragbar, wenn sie einen engen Anspruchsbereich definieren, der etwa dem des Urheberrechts entspricht oder nicht weit darüber hinausgeht.

#Wll: Solche Sotwarepatente will jedoch niemand.  Die Ausweitung der Patentierbarkeit um 1990 lässt sich als Richterflucht vor dem Urheberrecht begreifen (s. Stellungnahme Kiesewetter-Köbinger).

#WWn: Das Patentwesen kennt bislang keine Regel, die dafür sorgen könnte, dass nur Ansprüche erteilt werden können, die Softwareentwicklern gerecht erscheinen.

#tku: Somit werden, sobald man Softwarepatente erlaubt, regelmäßig zu breite Ansprüche zur Erteilung kommen.  Diese könnten als akzeptabel empfunden werden, wenn die Laufzeit nicht länger als 1 Jahr betrüge.

#nte: Auch dann bestünde noch das Problem der Trennung von Immaterialgüterrechten.  Es müsste ein integriertes Ausschlussrecht für den Bereich der Software geschaffen werden.  Bevor ein solches verdienstvolles Projekt begonnen werden kann, muss der Übergriff des verkehrsfremden Patentwesenes abgewehrt werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/cons0401.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: euparl-polfaq04 ;
# txtlang: de ;
# multlin: t ;
# End: ;

