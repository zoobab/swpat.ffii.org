<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mpW: De door het EP goedgekeurde amendementen

#sdn: Samengestelde versie door het EP, er werden nog veranderingen
doorgevoerd gedurende de dagen na de stemming.

#WMW: Europarl 03-09-24: Arlene McCarthy's summary of the vote

#fth: Mentions mainly the parts of the vote that are congruent with her own
proposals and unimportant aspects, such as the novelty grace period,
but omits everything that really limits patentability.

#purp: Werkingssfeer

#defi: Definities

#efn: In computers geïmplementeerde uitvindingen als een gebied van de
technologie

#eai: Voorwaarden voor octrooieerbaarheid

#uoa: Uitsluitingen van octrooieerbaarheid

#ono: Vorm van de conclusies; en verdere voorzieningen

#tel: Interoperabiliteit

#mon: Toezicht

#rep: Verslag over de gevolgen van de richtlijn

#ass: Evaluatie van de gevolgen

#titl: Richtlijn betreffende de octrooieerbaarheid van in computers
geïmplementeerde uitvindingen

#Art1: Deze richtlijn stelt regels vast voor de octrooieerbaarheid van in
computers geïmplementeerde uitvindingen.

#Am36: %(q:in computers geïmplementeerde uitvinding): uitvinding in de zin
van het Europees Octrooiverdrag voor de werking waarvan het gebruik
van een computer, computernetwerk of een ander programmeerbaar
apparaat nodig is en die in zijn toepassingen een of meer
niet-technische kenmerken heeft die geheel of gedeeltelijk door middel
van een computerprogramma of computerprogramma's worden gerealiseerd,
naast de technische kenmerken die elke uitvinding moet hebben;

#Am107: %(q:technische bijdrage), ook wel %(q:uitvinding) genoemd: bijdrage
tot de stand van de techniek op een technisch gebied. De technische
aard van de bijdrage is een van de vier voorwaarden voor
octrooieerbaarheid. Voorts moet een technische bijdrage, om voor een
octrooi in aanmerking te komen, nieuw zijn, niet voor de hand liggen
en op industriële schaal toepasbaar zijn. Het gebruik van de krachten
van de natuur om fysieke effecten te beheersen, buiten de digitale
presentatie van de informatie om, behoort tot een gebied van de
technologie. De verwerking, de manipulatie en de presentatie van
informatie behoren daarentegen niet tot een gebied van de technologie,
zelfs indien daartoe technische apparaten worden gebruikt.

#Am97: %(q:technisch gebied): een industrieel toepassingsterrein dat het
gebruik van beheersbare natuurkrachten vereist voor het verkrijgen van
voorspelbare resultaten. %(q:Technisch): (%q:behorend tot een
technisch gebied).

#Am38: %(q:industrie): in octrooirechtelijke zin: %(q:geautomatiseerde
productie van materiële goederen);

#Am45: De lidstaten zorgen ervoor dat gegevensverwerking niet wordt beschouwd
als een gebied van de technologie in octrooirechtelijke zin en dat
innovaties op het gebied van gegevensverwerking geen uitvindingen zijn
in octrooirechtelijke zin.

#Am16N1: Om octrooieerbaar te zijn moet een in computers geïmplementeerde
uitvinding geschikt zijn voor industriële toepassing, nieuw zijn en op
uitvinderswerkzaamheid te berusten. Om op uitvinderswerkzaamheid te
berusten moet een in computers geïmplementeerde uitvinding een
technische bijdrage leveren.

#Am16N2: De lidstaten zorgen ervoor dat een in computers geïmplementeerde
uitvinding die een technische bijdrage levert, een noodzakelijke
voorwaarde voor het bestaan van uitvinderswerkzaamheid is.

#Am100P1: Of de technische bijdrage significante omvang heeft, wordt beoordeeld
door het bepalen van het verschil tussen de technische kenmerken in de
omvang van de octrooiconclusie in hun geheel beschouwd, en de stand
van de techniek, ongeacht of deze kenmerken gepaard gaan met
niet-technische kenmerken.

#Am70: Of een in computers geïmplementeerde uitvinding een technische
bijdrage levert tot de stand van de techniek, wordt bepaald aan de
hand van de vraag of zij nieuw inzicht verschaft in het oorzakelijk
verband bij het gebruik van beheersbare natuurkrachten en of zij
industrieel toepasbaar in de enge zin, zowel wat de methode als wat
het resultaat betreft

#Am17: Een in computers geïmplementeerde uitvinding wordt niet geacht een
technische bijdrage te leveren louter op grond van het feit dat
daarbij gebruik wordt gemaakt van een computer, een netwerk of andere
programmeerbare apparatuur. Bijgevolg zijn uitvindingen waarbij
gebruik wordt gemaakt van computerprogramma's met toepassing van
bedrijfsmethoden, mathematische of andere methoden en die geen
technische resultaten produceren buiten de normale fysieke interactie
tussen een programma en de computer, een netwerk of andere
programmeerbare apparatuur waarop dit ten uitvoer wordt gebracht, niet
octrooieerbaar.

#Am60: De lidstaten zorgen ervoor dat in computers geïmplementeerde
oplossingen voor technische problemen niet worden beschouwd als
octrooieerbare uitvindingen enkel en alleen omdat zij de
doeltreffendheid bij het gebruik van middelen binnen het
gegevensverwerkingssysteem verhogen.

#Am101: De lidstaten zorgen ervoor dat een in computers geïmplementeerde
uitvinding alleen kan worden geclaimd als product, dat wil zeggen als
een geprogrammeerd instrument of als een technisch productieprocédé.

#Wce: De lidstaten zorgen ervoor dat voor in computers geïmplementeerde
uitvindingen verleende octrooiaanspraken alleen betrekking hebben op
de technische bijdrage die de basis van de octrooiaanvraag vormt. Een
octrooiaanspraak op een computerprogramma, het weze een
computerprogramma als zodanig dan wel een op een drager opgeslagen
computerprogramma, is niet toelaatbaar.

#Am103: De lidstaten zorgen ervoor dat de productie, manipulatie, verwerking,
distributie en publicatie van informatie in welke vorm dan ook nooit
direct of indirect inbreuk maakt op een octrooi, ook al worden daartoe
technische apparaten gebruikt.

#Am104N1: De lidstaten zorgen ervoor dat het gebruik van een computerprogramma
voor doeleinden die niet onder het toepassingsgebied van het octrooi
vallen, geen directe of indirecte inbreuk op het octrooi kan zijn.

#Am104N2: De lidstaten zorgen ervoor dat als in een octrooiconclusie kenmerken
worden genoemd die het gebruik van een computerprogramma impliceren,
een goed werkende en goed gedocumenteerde referentie-implementering
van een dergelijk programma wordt gepubliceerd als onderdeel van de
beschrijving, zonder beperkende licentievoorwaarden.

#Am19: De rechten die zijn toegekend met octrooien die binnen de
werkingssfeer van deze richtlijn voor uitvindingen zijn verleend,
laten handelingen onverlet die zijn toegestaan op grond van de
artikelen 5 en 6 van Richtlijn 91/250/EEG betreffende de
auteursrechtelijke bescherming van computerprogramma's, met name de
daarin opgenomen bepalingen betreffende decompilatie en
compatibiliteit.

#Am76P1: De lidstaten zorgen ervoor dat gebruik van een geoctrooieerde techniek
met een belangrijk doel zoals de omzetting van de conventies die in
twee verschillende computersystemen of netwerken worden gebruikt om
communicatie en gegevensuitwisseling tussen beide mogelijk te maken,
niet wordt beschouwd als een inbreuk op het octrooi.

#Am71: De Commissie volgt welke invloed in computers geïmplementeerde
uitvindingen hebben op innovatie en mededinging, zowel in Europa als
internationaal, en op het Europese bedrijfsleven, in het bijzonder
kleine en middelgrote ondernemingen en de opensourcegemeenschap, en de
elektronische handel.

#Art81: De Commissie brengt bij het Europees Parlement en de Raad tegen
uiterlijk [DATUM (drie jaar vanaf de in artikel 9, lid 1, vermelde
datum)] verslag uit over:

#Art81a: de invloed van octrooien voor in computers geïmplementeerde
uitvindingen op de in lid 7 genoemde factoren;

#Am92: de vraag of de regels betreffende de octrooitermijn en het bepalen van
de octrooieerbaarheidseisen, meer bepaald nieuwheid,
uitvinderswerkzaamheid en de passende omvang van de conclusies,
adequaat zijn; en

#Art82c: of zich moeilijkheden hebben voorgedaan met betrekking tot lidstaten
waar niet wordt nagegaan of aan de eisen inzake nieuwheid en
uitvinderswerkzaamheid wordt voldaan voordat octrooi wordt verleend,
en zo ja, of maatregelen wenselijk zijn om deze moeilijkheden te
verhelpen.

#Am23: de vraag of er zich problemen hebben voorgedaan inzake de relatie
tussen de bescherming door octrooien van in computers geïmplementeerde
uitvindingen en de bescherming door auteursrecht van
computerprogramma's als geregeld in Richtlijn 91/250/EEG en of er
misbruik van het octrooisysteem voor wat betreft in computers
geïmplementeerde uitvindingen heeft plaatsgevonden;

#Am24: de vraag of het wenselijk en juridisch mogelijk zou zijn om, gelet op
de internationale verplichtingen van de Gemeenschap, een
'gratieperiode' voor elementen van een octrooiaanvraag voor elk type
uitvinding die voor de datum van aanvraag worden onthuld, in te
voeren.

#Am25: de eventuele noodzaak een diplomatieke conferentie voor te bereiden
ter herziening van het Europees Octrooiverdrag, mede in het licht van
de totstandkoming van het Gemeenschapsoctrooi;

#Am26: de manier waarop met de voorschriften van deze richtlijn rekening is
gehouden in de praktijk van het Europees Octrooibureau en zijn
onderzoeksrichtsnoeren.

#Am81: de vraag of de aan het Europees Octrooibureau verleende bevoegdheden
stroken met de vereisten voor de harmonisering van de EU-wetgeving,
alsmede met de beginselen van transparantie en verantwoordingsplicht.

#Am89: de gevolgen voor de omzetting van de conventies in twee verschillende
computersystemen om communicatie en gegevensuitwisseling mogelijk te
maken,

#Am93: de vraag of de in deze richtlijn genomen optie met betrekking tot het
gebruik van een geoctrooieerde uitvinding met als enig doel
interoperabiliteit tussen twee systemen mogelijk te maken, adequaat
is;

#Am94: In dit verslag motiveert de Commissie waarom ze van oordeel is dat er
al dan niet een wijziging van onderhavige richtlijn nodig is en somt
ze desgevallend de punten op waarop ze zich voorneemt een voorstel tot
wijziging te formuleren.

#Am27: In het kader van het toezicht overeenkomstig artikel 7 en het verslag
dat moet worden opgesteld overeenkomstig artikel 8 evalueert de
Commissie de gevolgen van deze richtlijn en dient zij, voorzover
noodzakelijk, bij het Europees Parlement en de Raad voorstellen in tot
wijziging van de wetgeving.

#rFr: voorstel EC en FFII tegenvoorstel.

#nin: Voorstellen amendementen die door het EP op 2003/09/24 werden gestemd.

#ent: Voorstellen amendementen waarover JURI stemde in juni

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: dietervu ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl0309 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

