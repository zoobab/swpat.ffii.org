<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mpW: Anträge, die vom europäischen Parlament genehmigt wurden.

#sdn: consolidated version by EP, changes were still being applied by the
secretariat during the days after the vote.  E.g. %(q:contribute) in
%(q:technical features that any invention must contribute) was changed
to %(q:posess).

#WMW: Europarl 03-09-24: Arlene McCarthy's summary of the vote

#fth: Mentions mainly the parts of the vote that are congruent with her own
proposals and unimportant aspects, such as the novelty grace period,
but omits everything that really limits patentability.

#purp: Zweck

#defi: Definitionen

#efn: Gebiete der Technik

#eai: Regeln der Patentierbarkeit

#uoa: Ausschluss von der Patentierbarkeit

#ono: Anspruchsformen und weitere Bestimmungen

#tel: Interoperabilität

#mon: Monitoring

#rep: Report on the effects of the Directive

#ass: Impact assessment

#titl: Directive on the patentability of computer-implemented inventions

#Art1: Diese Richtlinie gibt Regeln vor zur Patentierbarkeit von
computer-implementierten Erfindungen.

#Am36: %(q:Computerimplementierte Erfindung) ist jede Erfindung im Sinne des
Europäischen Patentübereinkommens, zu deren Ausführung ein Computer,
ein Computernetz oder eine sonstige programmierbare Vorrichtung
eingesetzt wird und die in ihren Implementationen ­ außer den
technischen Merkmalen, die jede Erfindung beisteuern muss  mindestens
ein nichttechnisches Merkmal aufweist, das ganz oder teilweise mit
einem oder mehreren Computerprogrammen realisiert wird.

#Am107: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in technical field. The technical
character of the contribution is one of the four requirements for
patentability. Additionally, to deserve a patent, the technical
contribution has to be new, non-obvious, and susceptible of industrial
application. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes.

#Am97: %(q:technical field) means an industrial application domain requiring
the use of controllable forces of nature to achieve predictable
results. %(q:Technical) means %(q:belonging to a technical field).

#Am38: %(q:Industrie) im Sinne des Patentrechts ist die %(q:automatisierte
Herstellung materieller Güter);

#Am45: Member states shall ensure that data processing is not considered to
be a field of technology in the sense of patent law, and that
innovations in the field of data processing are not considered to be
inventions in the sense of patent law.

#Am16N1: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and involve an inventive
step. In order to involve an inventive step, a computer-implemented
invention must make a technical contribution.

#Am16N2: Member States shall ensure that a computer-implemented invention
making a technical contribution constitutes a necessary condition of
involving an inventive step.

#Am100P1: The significant extent of the technical contribution shall be assessed
by consideration of the difference between the technical elements
included in the scope of the patent claim considered as a whole and
the state of the art, irrespective of whether or not such features are
accompanied by non-technical features.

#Am70: In determining whether a given computer-implemented invention makes a
technical contribution, the following test shall be used: whether it
constitutes a new teaching on cause-effect relations in the use of
controllable forces of natures and has an industrial application in
the strict sense of the expression, in terms of both method and
result.

#Am17: Bei computerimplementierten Erfindungen wird nicht schon deshalb von
einem technischen Beitrag ausgegangen, weil zu ihrer Ausführung ein
Computer, ein Computernetz oder eine sonstige programmierbare
Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren
Ausführung ein Computerprogramm eingesetzt wird und durch die
Geschäftsmethoden, mathematische oder andere Methoden angewendet
werden, nicht patentfähig, wenn sie über die normalen physikalischen
Interaktionen zwischen einem Programm und dem Computer,
Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in
der es abgespielt wird, keine technischen Wirkungen erzeugen.

#Am60: Mitgliedsstaaten sollen sicherstellen, dass computerimplementierte
Lösungen zu einem technischen Problem nicht nur deshalb als
patentierbare Erfindungen angesehen werden weil sie die Effizienz beim
Verbrauch von Resourcen innerhalb des Datenverarbeitunsgssystems
verbessern.

#Am101: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is as a programmed device, or as a
technical production process.

#Wce: Member States shall ensure that patent claims granted in respect of
computer-implemented inventions include only the technical
contribution which justifies the patent claim. A patent claim to a
computer program, either on its own or on a carrier, shall not be
allowed.

#Am103: Member States shall ensure that the production, handling, processing,
distribution and publication of information, in whatever form, can
never constitute direct or indirect infringement of a patent, even
when a technical apparatus is used for that purpose.

#Am104N1: Member States shall ensure that the use of a computer program for
purposes that do not belong to the scope of the patent cannot
constitute a direct or indirect patent infringement.

#Am104N2: Member States shall ensure that whenever a patent claim names features
that imply the use of a computer program, a well-functioning and well
documented reference implementation of such a program is published as
part of the patent description without any restricting licensing
terms.

#Am19: Rechte, die aus Patenten erwachsen, die für Erfindungen im
Anwendungsbereich dieser Richtlinie erteilt werden, bleiben unberührt
von urheberrechtlich zulässigen Handlungen gemäß Artikel 5 und 6 der
Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen,
insbesondere hinsichtlich der Bestimmungen über die Dekompilierung und
die Interoperabilität.

#Am76P1: Member States shall ensure that, wherever the use of a patented
technique is needed for a significant purpose such as ensuring
conversion of the conventions used in two different computer systems
or networks so as to allow communication and exchange of data content
between them, such use is not considered to be a patent infringement.

#Am71: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, and on European businesses, especially small and
medium-sized enterprises and the open source community, and electronic
commerce.

#Art81: The Commission shall report to the European Parliament and the Council
by [DATE (three years from the date specified in Article 9(1))] at the
latest on

#Art81a: the impact of patents for computer-implemented inventions on the
factors referred to in Article 7;

#Am92: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims, are adequate;
and

#Art82c: whether difficulties have been experienced in respect of Member States
where the requirements of novelty and inventive step are not examined
prior to issuance of a patent, and if so, whether any steps are
desirable to address such difficulties.

#Am23: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-implemented
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-implemented
inventions;

#Am24: whether it would be desirable and legally possible having regard to
the Community's international obligations to introduce a 'grace
period' in respect of elements of a patent application for any type of
invention disclosed prior to the date of the application;

#Am25: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the Convention on the Grant of
European Patents, also in the light of the advent of the Community
patent;

#Am26: how the requirements of this Directive have been taken into account in
the practice of the European Patent Office and in its examination
guidelines.

#Am81: whether the powers delegated to the EPO are compatible with the need
to harmonise Community legislation, and with the principles of
transparency and accountability.

#Am89: the impact on the conversion of the conventions used in two different
computer systems to allow communication and exchange of data;

#Am93: whether the option outlined in the Directive concerning the use of a
patented invention for the sole purpose of ensuring interoperability
between two systems is adequate;

#Am94: In this report the Commission shall justify why it believes an
amendment of the Directive in question necessary or not and, if
required, will list the points which it intends to propose an
amendment to.

#Am27: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
assess the impact of this Directive and, where necessary, submit
proposals for amending legislation to the European Parliament and the
Council.

#rFr: CEC proposal and FFII counter-proposal.

#nin: Amendment proposals on which the EP voted on 2003/09/24

#ent: Amendment Proposals on which JURI voted in June

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

