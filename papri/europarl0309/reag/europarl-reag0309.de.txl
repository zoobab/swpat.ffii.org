<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#thh: A patent attorney trying to mobilise his profession for backlash in
the Council.

#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on
FFII %(q:misinformation campaign), will be %(q:thrown into the
dustbin) by European Commission or Council and, if not, attacked on
the basis of TRIPs by friends from US.

#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests
with industry interests, attributes limiting amendments to
%(q:environmentalists and socialists), extensively quotes EICTA
statements.

#Wbi: Patentanwälte von großen Informations- und Kommunikationstechnischen
Unternehmen, die im Namen einer Europäischen Industrie-Vereinigung
sprechen, sind sehr unglücklich über das Ergebnis der Abstimmung im
Europäischen Parlament

#foh: An informative account of what happened.

#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies
competence of the parliament in matters of patent legislation,
predicts that European Commission and Council will withdraw directive
and entrust %(q:patent experts) from national governments with
legislation via the European Patent Organisation.

#vPi: Christian Engström, swedish software developper, refutes statements by
UK patent lawyer Alex Batteson who asked that the European Parliament
should be stripped of its right to legislate on patent matters, as by
voting against software patents it had shown its incompetence.

#rsw: Part II of Engström's refutal of Batteson and assessments of the
strengths and weaknesses of parliamentary democracy, as shown in the
software patent vote.

#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried
about the attempts of Horns and other patent lawyers to declare
themselves %(q:experts) in this matter and deny the competence of the
European Parliament, points out that this runs counter to recent
principle decisions of the German Constitutional Court.  Moreover Lenz
confesses himself guilty of what Horns calls a %(q:misinformation
campaign) about the exclusion of software from patentability by the
European Patent Convention and expresses doubt about the correctness
of Horns's assertions.

#rme: Antwort von Hartmut Pilch an Axel Horns in einer
Mailinglisten-Diskussion zum Broersma Artikel

#lvt: Der Brüsseler Korrespondent Paul Meller berichtet, daß nun die Rollen
der Unterstützer und der Gegner der Direktive getauscht wurden. Er
zitiert Hartmut Pilch und Laura Creighton.

#uoe: Antwort von Hartmut Pilch an PA Axel Horns

#ins: Horns bezichtigt das Europaparlament, dem FFII auf einen
dilettantischen und abenteuerlichen Kurs gefolgt zu sein, dem nun die
Patentprofis um Bolkestein, im Rat und in der US-Regierung ein Ende
bereiten werden, indem sie die Richtlinie abschießen.  Pilch widerlegt
die Argumente.

#Bolkestein030923T: Bolkensteins Drohung

#Bolkestein030923D: One day before the vote Bolkestein told the MEPs: %(bc|Now I am aware
that the large number of amendments to the McCarthy report have been
tabled.  Many of those try to re-introduce ideas and themes which were
already considered and rejected by the committee during the
preparation of the report.  There are some interesting points, but in
the main, I am afraid that the majority of those amendments will be
unacceptable to the Commission.  And I must confess, to being very
concerned about this situation.  Many of these amendments are
fundamental, and there is the very real possibility of the failure of
the proposal if the parliament chooses to accept them.  If that were
to happen, there would I fear be two consequences, neither of which I
suspect has been forseen by some mebers of parliament, and neither of
which I can only assume would advance the objectives which seem to lie
behind a number of amendments.  Firstly, in the complete absence of
harmonisation at the level of the community, the European and various
national patent offices would be free to continue their current
practice of issuing patents for software-implemented inventions which
may blur or even cross the line in undermining the exclusion from
patentability of software as such under article 52 of the European
Patent Convention.  And the result would be not only continuing legal
uncertainty and divergence for inventors; but also erode the position
which I think almost everyone in this room and above all the
Commission itself wants -- namely to maintain the exclusion of pure
software from patentability.  That we do not want.   That the proposal
rejects.  And secondly, in the absence of harmonisation at Community
level, member states would be very likely to pursue harmonisation at
the European level instead.  And may I explain what I mean by that
remark.  Unlike many fields, patents are unusual in that as a result
of the existence of the European Patent Convention, and the creation
of the European Patent Office, there already exists a supranational
patent system, which covers the whole of the European Union, and
indeed beyond, and which can act independently of the Community's
legislative process.  Now if we fail in our efforts to achieve a
harmonisation of patent law relating to computer-implemented
inventions in the European Union, we may well be confronted with a
renegotiation of the European Patent Convention. And if I may be
blunt, President, the process of renegotiation of the European Patent
Convention would not require any contribution from this parliament. 
So the situation is clear: there is a single objective but a choice of
means.  Either we proceed using the community method, or we take a
back seat and watch while member states go via the route of an
intergovernmental treaty.  And I think it is clear which route would
give European citizens a greater say through this parliament in patent
legislation in an area which is so crucial to our economy.)

#WWt: Heise 2003-10-23: EU-Parlament erhaelt Unterstuetzung bei
Software-Patenten

#eWG: Zitiert eine Presseerklärung des DMMV.  Dessen Vorstandsmitglied Rudi
Gallist sieht im Ansatz des Europäischen Parlamentes einen guten
Kompromiss zwischen Erfordernis des Patentschutzes für den klassischen
Bereich der Technik und dem Bedürfnis der Softwarebranche,
insbesonders der KMU, nach Freiheit von Patentverletzungsrisiken. 
Ferner kommt Daniel Riek vom Linux-Verband zu Wort, der die
Bitkom-Stellungnahme vom Vortag scharf kritisiert.

#1rn: Heise 03-10-22: %(q:IT-Verband ruft EU auf den rechten Weg zurück)

#tEo: Bericht über Bitkom-PE und Warnung von FFII UK

#Wad: CEC 2003-10: Inakzeptable Anträge

#ssn: In a first reaction to the European Parliament's amendments, the
European Commission's patent experts (i.e. the authors of the amended
draft) list the amendments which they say are %(q:inacceptable to the
Commission).  The list is long.  It comprises all amendments that can
limit patentability or patent enforcability in any way.  The only
%(q:acceptable) amendments are the cosmetic ones from JURI.  CEC does
not give any reasoning as to why it can't accept the others.  This CEC
statement was published on the Coucil website two weeks after the EP
vote, shortly before a first meeting of the Council's %(q:Patent
Working Party).

#aea: 2002 haben die Patentbeauftragten des Rats auf unbegrenzte
Patentierbarkeit gedrängt, obwohl sie laut den Verfahrensregeln des
EU-Gesetzgebung gar nicht an der Reihe waren. Genauso wie der
Binnenmarkt-Ausschuss der Europäischen Kommission, ist die
%(q:Arbeitsgruppe für Patentrecht) des Rates eine Institution auf
welche die Patentabteilungen großer IT-Konzerne zählen können. Ihre
Mitglieder sind immer dazu bereit, entgegen schriftlicher Anweisungen
ihrer eigenen Regierungen zu handeln, falls der Konsens der
Patentlobby dies verlangt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl-reag0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

