<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#EuroparlReag0309T: Réactions à la décision du 24 septembre 2003 du Parlement de l'UE

#EuroparlReag0309D: Frits Bolkestein, Malcolm Harbour et d'autres politiciens se sont joints à des juristes en brevets pour dire en choeur que %(q:le Parlement européen a ruiné ses chances de participation démocratique) en ne votant pas dans le sens voulu par le monde du brevet. Des préparatifs sont en cours afin de retirer la directive et de légaliser les brevets logiciels par le truchement d'un accord entre officiels nationaux, habitués qu'ils sont à décider entre eux de la conduite à suivre en matières de brevet.

#thh: Un avocat en brevets tente de mobiliser la profession pour contre-attaquer au Conseil.

#Wca: L'avocat en brevets Horns déclare que la décision du PE est une %(q:absurdité), basée sur une %(q:campagne de désinformation) de la FFII, qu'elle sera %(q:jetée à la poubelle) par la Commission européenne ou le Conseil ou, dans le cas contrairen, attaquée par des amis américains sur base du TRIPs.

#sey: Le correspondant à Bruxelles, Matthew Newman, confond les intérêts des avocats en brevets avec les intérêts de l'industrie, attribue les amendements limitatifs à des %(q:écologistes et socialistes), cite abondamment les déclarations de l'EICTA.

#Wbi: Les avocats en brevets des grandes sociétés ICT, parlant au nom d'une organisation de l'industrie européenne, sont très mécontents du résultat du vote au Parlement.

#foh: Un bilan assez informatif de ce qui s'est produit

#cWi: Avocat en brevets britannique et ancien examinateur à l'OEB, Alex Batteson dénie au Parlement toute compétence en droit des brevets, prédit que la Commission européenne et le Conseil retireront la directive et laisseront des %(q:experts en brevet) des gouvernements nationaux décider de la législation via l'Office européen des brevets (OEB).

#vPi: Christian Engström, développeur de logiciel suédois, réfute les déclarations de l'avocat britannique Alex Batteson qui a demandé que le Parlement européen soit privé de son pouvoir de légiférer en matières de brevet, puisqu'en votant contre les brevets logiciels, il a montré son incompétence.

#rsw: Deuxième partie de la réfutation des arguments de Batteson par Engström, et évaluation des forces et faiblesses de la démocratie parlementaire, telle qu'on la vue à l'oeuvre dans le cas du vote sur les brevets logiciels.

#tpe: Le Dr Lenz, professeur de droit allemand et européen à Tokyo, s'inquiète des tentatives de Horns et d'autres avocats en brevet de s'investir du titre d'%(q:experts) en ce domaine et de dénier sa compétence au Parlement européen, et fait remarquer que ceci va à l'encontre des décisions de principe de la Cours constitutionnelle allemande. De plus, Lenz avoue avoir participé à ce que Horns appelle une %(q:campagne de désinformation) sur l'exclusion des logiciels de la brevetabilité selon la Convention sur le Brevet Européen (CBE), et exprime de sérieux doutes sur la validité des assertions de Horns.

#rme: Réponse de Hartmut Pilch à Axel Horns, dans une dicussion sur mailing list, concernant l'article de Broersma

#lvt: Paul Meller, correspondant à Bruxelles, rapporte qu'à présent partisans et opposants de la directive ont échangés leur rôle. Citations de Hartmut Pilch et Laura Creighton.

#uoe: Hartmut Pilch répond à l'avocat Axel Horns

#ins: Horns accuse le Parlement européen de dilettantisme inspiré par la FFII et prévoit que Bolkestein, le Conseil et les amis américains élimineront la directive. Pilch réfute les arguments avancés.  Discussion en allemand.

#aea: En 2002, les administrateurs en brevet du Conseil ont poussé à une brevetabilité illimitée, bien que selon le règles de fonctionnement de la législation de l'UE, ils ne pouvaient pas encore s'impliquer. A l'instar du Directoire pour le Marché intérieur de la Commission européenne, le %(q:Groupe de travail sur la politique des brevets) du Conseil est une institution sur la quelle peut compter le département brevet des grosses sociétés IT. Ses membres sont toujours prompts à agir contre les instructions écrites de leur propre gouvernement, pour maintenir le consensus du lobby des brevets.

#Bolkestein030923T: Les menaces de Bolkestein

#Bolkestein030923D: One day before the vote Bolkestein told the MEPs: %(bc|Now I am aware that the large number of amendments to the McCarthy report have been tabled.  Many of those try to re-introduce ideas and themes which were already considered and rejected by the committee during the preparation of the report.  There are some interesting points, but in the main, I am afraid that the majority of those amendments will be unacceptable to the Commission.  And I must confess, to being very concerned about this situation.  Many of these amendments are fundamental, and there is the very real possibility of the failure of the proposal if the parliament chooses to accept them.  If that were to happen, there would I fear be two consequences, neither of which I suspect has been forseen by some mebers of parliament, and neither of which I can only assume would advance the objectives which seem to lie behind a number of amendments.  Firstly, in the complete absence of harmonisation at the level of the community, the European and various national patent offices would be free to continue their current practice of issuing patents for software-implemented inventions which may blur or even cross the line in undermining the exclusion from patentability of software as such under article 52 of the European Patent Convention.  And the result would be not only continuing legal uncertainty and divergence for inventors; but also erode the position which I think almost everyone in this room and above all the Commission itself wants -- namely to maintain the exclusion of pure software from patentability.  That we do not want.   That the proposal rejects.  And secondly, in the absence of harmonisation at Community level, member states would be very likely to pursue harmonisation at the European level instead.  And may I explain what I mean by that remark.  Unlike many fields, patents are unusual in that as a result of the existence of the European Patent Convention, and the creation of the European Patent Office, there already exists a supranational patent system, which covers the whole of the European Union, and indeed beyond, and which can act independently of the Community's legislative process.  Now if we fail in our efforts to achieve a harmonisation of patent law relating to computer-implemented inventions in the European Union, we may well be confronted with a renegotiation of the European Patent Convention. And if I may be blunt, President, the process of renegotiation of the European Patent Convention would not require any contribution from this parliament.  So the situation is clear: there is a single objective but a choice of means.  Either we proceed using the community method, or we take a back seat and watch while member states go via the route of an intergovernmental treaty.  And I think it is clear which route would give European citizens a greater say through this parliament in patent legislation in an area which is so crucial to our economy.)

#WWt: Heise 2003-10-23: EU-Parlament erhaelt Unterstuetzung bei Software-Patenten

#eWG: Quotes press release of DMMV, which claims to be the largest software industry association of Germany.  DMMV board member Rudolf Gallist, a former top manager of Microsoft Germany, applauds the European Parliament's decision, because it allows the software industry, in particular SMEs, to be free from the threat of patent infringement.  He says that the Parliament has struck a good compromise between the need of patenting in classical industries and the need to keep data processing free from patents.  The article also quotes Daniel Riek from Linux-Verband (association of Linux-related companies) who sharply criticises the Bitkom press release.

#1rn: Heise 03-10-22: %(q:IT-Verband ruft EU auf den rechten Weg zurück)

#tEo: Bericht über Bitkom-PE und Warnung von FFII UK

#Wad: CEC 2003-10: Inacceptable Amendments

#ssn: In a first reaction to the European Parliament's amendments, the European Commission's patent experts (i.e. the authors of the amended draft) list the amendments which they say are %(q:inacceptable to the Commission).  The list is long.  It comprises all amendments that can limit patentability or patent enforcability in any way.  The only %(q:acceptable) amendments are the cosmetic ones from JURI.  CEC does not give any reasoning as to why it can't accept the others.  This CEC statement was published on the Coucil website two weeks after the EP vote, shortly before a first meeting of the Council's %(q:Patent Working Party).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl0309.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl-reag0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

