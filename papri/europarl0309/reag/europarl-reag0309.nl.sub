\begin{subdocument}{europarl-reag0309}{Reactions to the EU Parliament's Vote of 2003/09/24}{http://swpat.ffii.org/papri/europarl0309/reag/europarl-reag0309.nl.html}{Werkgroep\\swpatag@ffii.org\\Nederlandse versie 2003/10/11 door Dieter VAN UYTVANCK\footnote{http://www.student.kun.nl/dieter.vanuytvanck/}}{Frits Bolkestein, Malcolm Harbour and other politicians have joined in a choir of patent lawyers who are saying that ``the European Parliament ruined its chances of democratic participation'' by failing to vote as the patent establishment had asked it to vote.  Preparations are under way to withdraw the directive and legalise software patents by means of an agreement between national patent officials, who are used to deciding patent policies among themselves.}
\begin{sect}{links}{koppelingen met voetnoten}
\begin{itemize}
\item
{\bf {\bf EU Parlement Stemt voor Echte Beperkingen op Octrooieerbaarheid\footnote{http://swpat.ffii.org/lisri/03/plen0924/swnplen030924.nl.html}}}

\begin{quote}
In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on ``patentability of computer-implemented inventions'' with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003-10: Inacceptable Amendments\footnote{http://register.consilium.eu.int/pdf/en/03/st11/st11503.en03.pdf}}}

\begin{quote}
In a first reaction to the European Parliament's amendments, the European Commission's patent experts (i.e. the authors of the amended draft) list the amendments which they say are ``inacceptable to the Commission''.  The list is long.  It comprises all amendments that can limit patentability or patent enforcability in any way.  The only ``acceptable'' amendments are the cosmetic ones from JURI.  CEC does not give any reasoning as to why it can't accept the others.  This CEC statement was published on the Coucil website two weeks after the EP vote, shortly before a first meeting of the Council's ``Patent Working Party''.

zie ook Europarl 2003-09-24: Geamendeerde softwareoctrooi-richtlijn\footnote{http://swpat.ffii.org/papri/europarl0309/europarl0309.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf Heise 03-10-22: ``IT-Verband ruft EU auf den rechten Weg zur\"{u}ck''\footnote{http://www.heise.de/newsticker/data/anw-22.10.03-001/}}}

\begin{quote}
Bericht \"{u}ber Bitkom-PE und Warnung von FFII UK

zie ook Bitkom-PE 2003-03-22: \footnote{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay\&ObjectID=D321C079-46F6-4D51-9B08AA448674390A\&MenuNodeID=4C872DB6-8470-4B01-A36FD8C1EBA2E22D} en EU Council Patent Workgroup Meeting\footnote{http://swpat.ffii.org/lisri/03/cons1023/SwnCons031023.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf DMMV 2003-10-23: DMMV empfiehlt EU-Ministerrat, der Parlamentsentscheidung zu Softwarepatenten zu folgen}}

\begin{quote}
Der Mitte 2003 neu gegr\"{u}ndete Arbeitskreis der Softwareindustrie innerhalb des Deutschen MultiMediaVerbandes (DMMV) h\"{a}lt Softwarepatente wie von EU-Kommission und Rat bef\"{u}rwortet f\"{u}r mittelstandsfeindlich und verweist auf Gefahren, die verschiedenen mittelst\"{a}ndisch gepr\"{a}gten neuen Branchen drohen.  Der DMMV ist daher mit den \"{A}nderungen des EU-Parlamentes zufrieden und verweist auf einen breiten Konsens unterschiedlicher Str\"{o}mungen, der hinter diesen \"{A}nderungen stehe.
\end{quote}
\filbreak

\item
{\bf {\bf Tauss \& Kelber 2003-09-24: EP erkl\"{a}rt Trivialpatenten eine Absage\footnote{http://swpat.ffii.org/papri/europarl0309/spdmdb030924/spdmdb030924.de.html}}}

\begin{quote}
Pressemitteilung der zweiter Bundesabgeordneter, die in der Politik der SPD zu Fragen der Informationellen Infrastrukturen f\"{u}hrend sind.  Tauss und Kelber fordern die Kommission und den Rat auf, den Beschluss des EP zu unterst\"{u}tzen.
\end{quote}
\filbreak

\item
{\bf {\bf Bolkestein's Threats\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/plendeba0309.en.html\#bolk}}}

\begin{quote}
One day before the vote Bolkestein told the MEPs: \begin{quote}
{\it Now I am aware that the large number of amendments to the McCarthy report have been tabled.  Many of those try to re-introduce ideas and themes which were already considered and rejected by the committee during the preparation of the report.  There are some interesting points, but in the main, I am afraid that the majority of those amendments will be unacceptable to the Commission.  And I must confess, to being very concerned about this situation.  Many of these amendments are fundamental, and there is the very real possibility of the failure of the proposal if the parliament chooses to accept them.  If that were to happen, there would I fear be two consequences, neither of which I suspect has been forseen by some mebers of parliament, and neither of which I can only assume would advance the objectives which seem to lie behind a number of amendments.  Firstly, in the complete absence of harmonisation at the level of the community, the European and various national patent offices would be free to continue their current practice of issuing patents for software-implemented inventions which may blur or even cross the line in undermining the exclusion from patentability of software as such under article 52 of the European Patent Convention.  And the result would be not only continuing legal uncertainty and divergence for inventors; but also erode the position which I think almost everyone in this room and above all the Commission itself wants -- namely to maintain the exclusion of pure software from patentability.  That we do not want.   That the proposal rejects.  And secondly, in the absence of harmonisation at Community level, member states would be very likely to pursue harmonisation at the European level instead.  And may I explain what I mean by that remark.  Unlike many fields, patents are unusual in that as a result of the existence of the European Patent Convention, and the creation of the European Patent Office, there already exists a supranational patent system, which covers the whole of the European Union, and indeed beyond, and which can act independently of the Community's legislative process.  Now if we fail in our efforts to achieve a harmonisation of patent law relating to computer-implemented inventions in the European Union, we may well be confronted with a renegotiation of the European Patent Convention. And if I may be blunt, President, the process of renegotiation of the European Patent Convention would not require any contribution from this parliament.  So the situation is clear: there is a single objective but a choice of means.  Either we proceed using the community method, or we take a back seat and watch while member states go via the route of an intergovernmental treaty.  And I think it is clear which route would give European citizens a greater say through this parliament in patent legislation in an area which is so crucial to our economy.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Plenary Debate 03/09/23\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/plendeba0309.en.html}}}

\begin{quote}
Rough Transcript of the Speeches given in the Plenary Debate of 2003/09/23.
\end{quote}
\filbreak

\item
{\bf {\bf ZDNet UK News: Patents directive wins European Parliament OK\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116642,00.htm}}}

\begin{quote}
Een informatieve samenvatting van wat gebeurd is
\end{quote}
\filbreak

\item
{\bf {\bf CEOs of big telcos sign letter against Europarl Amendments\footnote{http://swpat.ffii.org/lisri/03/telcos1107/telcos031107.en.html}}}

\begin{quote}
The chief executive officers of Alcatel, Ericsson, Nokia and Siemens have signed a letter to the European Commission and the European Council which complains about the European Parliament's amendments to the proposed software patent directive, saying that these will effectively remove the value of most of the patents of their companies and thereby harm the competitiveness of Europe's industry and violate the TRIPs treaty.  FFII points out that the Directive indeed threatens the interests of the patent departments of such companies, but not of the companies themselves: The letter is characterised by untruthful dogmatic assertions which say much about the thinking of patent departments and little about the interests of their companies, many of whose employees, especially software developers, support the positions of FFII.
\end{quote}
\filbreak

\item
{\bf {\bf Kai Brandt 2003: Patent Protection in Europe in Danger\footnote{http://swpat.ffii.org/papri/siemens-brandt03/siemens-brandt03.en.html}}}

\begin{quote}
In an internal journal of Siemens, Dr. Kai Brandt, an independent patent attorney residing in Munich and member of the Siemens patent department, writes that the European Parliament voted to ban patenting of all innovative industrial processes that make use of software, that there is no R\&D without patents, that the European Commission's original proposal was well balanced, that the EP voted for amendments because it was misled to believe that patents and opensource software are incompatible, and that Siemens boss Heinrich von Pierer has teamed up with other CEOs and associations to prevent this disaster, which is only in the interest of a few software distributors and against the interests of all innovative SMEs.  Brandt fails to give his audience any usable pointers that could allow them to inform themselves about the other side's arguments.
\end{quote}
\filbreak

\item
{\bf {\bf Reuters echoing the telcos FUD}}

\begin{quote}
An article by Reuters News Agency which reports about the letter of the 5 CEOs and adds some comments by a leading employee of Ericsson who is worried that since the use of computers is progressing hand making ``inventing'' very easy, what formerly was a patentable invention no longer be so in the future.  But without patent protection, Ericsson would go broke and therefore Ericsson would have to move out of Sweden.  Note that Ericsson is already under obligation to move out of Sweden, because swedish voters didn't heed their CEO's threats when voting to stay out of the euro zone for another few years.
\end{quote}
\filbreak

\item
{\bf {\bf Business Week on European Parliament's vote: leftists erasing billions in intellectual property\footnote{http://swpat.ffii.org/lisri/03/bswk1216/bswk031216.en.html}}}

\begin{quote}
An editorial of the US magazine BusinessWeek reports that a group of ``left-leaning politicians'' and ``open-source advocates'' by ``last-minute lobbying'' ``upended'' a directive proposal in such a way that it actually ``bans software patents'', thereby creating an ``industry-specific exemption'' which violates the TRIPs treaty amd ``erases billions in intellectual property granted by the EPO''.  The author gives Europe a lot of advice, demanding that Europe should set an example by finding a formula that ``spurs innovation while safeguarding intellectual property''.  The article contains various contradictions and false assertions.  Jim Bessen (innovation economics researcher at MIT) and others have written letters to the editor.
\end{quote}
\filbreak

\item
{\bf {\bf EICTA reaction\footnote{http://www.eicta.org/dls/Logon/TopLogon.asp?URL=/Common/GetFile.asp?\&logonname=guest\&ID=6861\&mfd=off}}}

\begin{quote}
Zeer ongelukkig met de richtlijn

zie ook EICTA reaction\footnote{http://www.eicta.org/dls/Logon/TopLogon.asp?URL=/Common/GetFile.asp?\&logonname=guest\&ID=6861\&mfd=off} en EICTA and Software Patents\footnote{http://swpat.ffii.org/gasnu/eicta/swpateicta.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Down Jones Newsletter about the amended directive\footnote{http://www.quicken.com/investments/news\_center/story/?story=NewsStory/dowJones/20030924/ON200309240711000338.var\&column=P0DFP}}}

\begin{quote}
Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to ``environmentalists and socialists'', extensively quotes EICTA statements.
\end{quote}
\filbreak

\item
{\bf {\bf IDG.com.sg: reversal of positions on Directive\footnote{http://www.idg.com.sg/idgwww.nsf/unidlookup/955189AAA890112C48256DB10014D9C7?OpenDocument}}}

\begin{quote}
Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.
\end{quote}
\filbreak

\item
{\bf {\bf ZDNet UK News: Software patent limits 'go too far'\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm}}}

\begin{quote}
UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust ``patent experts'' from national governments with legislation via the European Patent Organisation.

zie ook Christian Engstroem: Democracy not so bad (I)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013781\&AT=39116709-39020651t-10000022c}
\end{quote}
\filbreak

\item
{\bf {\bf Christian Engstroem: Democracy not so bad (I)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013781\&AT=39116709-39020651t-10000022c}}}

\begin{quote}
Christian Engstr\"{o}m, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.

zie ook ZDNet UK News: Software patent limits 'go too far'\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm} en Christian Engstroem: Democracy not so bad (II)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013782\&AT=39116709-39020651t-10000022c}
\end{quote}
\filbreak

\item
{\bf {\bf Horns 03-09-25: Severe Defeat for the Users of the Patent System\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0749.html}}}

\begin{quote}
Een octrooiadvocaat probeert zijn confraters te mobiliseren om terug te slaan via de Raad van Europa.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/swpathorns.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf PA Axel H. Horns' blog on IPR: the day after\footnote{http://www.ipjur.com/2003\_09\_01\_archive.php3\#106451647590989047}}}

\begin{quote}
Patent attorney Horns says that EP decision is ``rubbish'', based on FFII ``misinformation campaign'', will be ``thrown into the dustbin'' by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/swpathorns.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Lenz Blog: Horns Blog on Patent Vote\footnote{http://k.lenz.name/LB/archives/000617.html}}}

\begin{quote}
Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves ``experts'' in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a ``misinformation campaign'' about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/swpathorns.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf FFII Softwarepatente Diskussion Mailingliste: Re: Matthew Broersma : Software patent limits 'go too far' (fwd)\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0873.html}}}

\begin{quote}
Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/swpathorns.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Hartmut Pilch responds to PA Axel Horns\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0882.html}}}

\begin{quote}
Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/swpathorns.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Council of the European Union and Software Patents\footnote{http://swpat.ffii.org/gasnu/consilium/swpatconsilium.en.html}}}

\begin{quote}
Together with the European Commission and the European Parliament, the Council is one of the three pillars of the European Union, which jointly legislate in a \emph{co-decision procedure}.  It is a forum where the national governments and their specialised ministries meet.  The question of how to limit patentability is handled in the ``Council Working Party on Intellectual Property and Patents''.  This council has been holding increasingly frequent meetings to discuss the European Commission's proposal for a software patentability directive and come up with a counter-proposal.  The national delegations are mostly composed of national patent office representatives or people whose career path is confined to the national patent establishment and who are factually dependent on this establishment in many ways.  Some delegations, such as the french and belgians, have comprised independent delegates and been fairly critical of the CEC proposal.  Others have been even more pro-patent than the CEC.  All have focussed on textual questions and caselaw rather than on what kind of output they want from the legislation in terms of patents granted/rejected and economic policy objectives.
\end{quote}
\filbreak

\item
{\bf {\bf CEU/DKPTO 2002/09/23..: Software Patentability Directive Amendment Proposal\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/dkpto0209/dkpto0209.en.html}}}

\begin{quote}
In 2002 streefde de administratie van de Raad naar onbeperkte octrooieerbaarheid, hoewel het volgens de procedureregels van de EU-wetgeving nog niet hun beurt was.  Net zoals het Directoraat voor de Interne Markt van de Europese Commissie, is de ``werkgroep voor octrooibeleid'' van de Raad een instituut waarop de octrooiafdelingen van grote IT-bedrijven kunnen regelen.  Haar leden zijn altijd bereid te handelen tegen schriftelijke instructies van hun eigen regering in, indien de consensus van de patentenlobby dit vraagt.
\end{quote}
\filbreak

\item
{\bf {\bf Robin Webb (UK PTO \& Gov't) 2002-02-20: proposes to remove all limits to patentability and to rewrite Art 52 EPC so as to reflect EPO practise\footnote{http://swpat.ffii.org/papri/ukpo-swpat0202/ukpo-swpat0202.en.html}}}

\begin{quote}
The UK PTO conducted its own consultation, which showed an overwhelming wish of software professionals to be free of software patents.  But the UK PTO, speaking in the name of the UK government, reinterprets this as a legitimation to remove all limits on patentability by modifying Art 52 EPC at the Diplomatic Conference in June 2002.  The proposal would render Art 52 tautological.  Given that an ``invention'' in the meaning of Art 52 is the same as a ``technical contribution to the state of the art'', the UKPO proposal is tautological: \begin{quote}
{\it the following are not inventions, unless in their inventive step they make a technological contribution to the state of the art}
\end{quote} just means \begin{quote}
{\it the following are not inventions unless in their inventive step they are inventions}
\end{quote} or, after removing the misplaced ``inventive step'' requirement, which is dealt with in Art 56 EPC and not in Art 52 EPC, the UKPO's proposal boils down to: \begin{quote}
{\it The following are not inventions unless they are inventions}
\end{quote}.  In order to arrive at this recommendation, the UKPO conducted a consultation, it says.  The purpose of this UKPO proposal was to help CEC Commissioner Bolkestein persuade the grudging European Commission to adopt their directive proposal in February 2002, a proposal written by UK patent office people together with BSA in Brussels.  ``If the European Commission doesn't adopt the proposal, we will sidestep the EU by pressing ahead in the European Patent Organisation'', was the UKPO's (and thereby the UK government's) message.

zie ook The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org/gasnu/uk/swpatuk.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Petition Initiators Thank the European Parliament\footnote{http://swpat.ffii.org/lisri/03/epet0929/swnepet030929.en.html}}}

\begin{quote}
Last Wednesday the Parliament voted against software patents and for freedom of publication, freedom of interoperation and other basic values of the information society, thereby reversing the thrust of a directive proposal from the European Commission, so as to basically satisfy the demands of a quarter million signatories of the ``Eurolinux Petition for a Software Patent Free Europe'' and 30 eminent computer scientists.  The initiators of both petitions will speak before the European Parliament's Petition Committee on tuesday 18.00 to express their thanks and explore with MEPs what still needs to be done.
\end{quote}
\filbreak

\item
{\bf {\bf Philippe Aigrain 2003-09-30: Intervention devant le Comit\'{e} des p\'{e}titions du Parlement Europ\'{e}en\footnote{http://swpat.ffii.org/lisri/03/epet0929/aigrain/AigrainEpet030930.fr.html}}}

\begin{quote}
A petition of 33 famous computer scientists had ``urged the Members of the European Parliament, whatever their party affiliation, to adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the underlying ideas of software (or algorithms), of information processing methods, of representations of information and data, and of software interaction between human beings and computers.'' in May 2003.  Philippe Aigrain, initiator of the petition, thanks MEPs for basically fulfilling the demands of the petitioners, and warns of various pressures and fallacies from the patent lawyer lobby with which the Parliament will be confronted in the coming months.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/europarl0309.el ;
% mode: latex ;
% End: ;

