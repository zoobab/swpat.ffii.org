<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Amendment Proposals in 2005

#descr: The European Parliament is facing the challenge of reaffirming the
effective exclusion of software patentability for which it had voted
on 24th September 2003, so as to be able to force the Council and
Commission to conduct a discussion on substance.  Here we try to
collect amendments that may be worth tabling in performing this task.

#dlI: Amendments tabled in JURI so far

#aoe: rapporteur

#arr: shadow rapporteur

#isg: Eastern-European cross-partisan group

#iea2: Liberals

#ule: In the following sections we work out possible amendments with
justifications.  Not all of them are necessarily preferred choices of
FFII, but all are approved by us after having been talked through with
our advisors.  We try to provide various options for members of the
European Parliament or representatives of national governments in the
Council who may be pursuing different strategies.  Most of the
following amendments have been tabled by one or the other of the
MEPs/groups listed above.

#text: Throughout the text

#WhW: According to %(ra:rule 151), it is permissible to table %(q:amendments
which seek to make identical changes to a particular form of words
throughout the text).

#ume: computer-implemented invention

#pdn: computer-aided invention

#nen: In their press release upon adoption of the %(q:Common Position), the
Council says that that their text did not allow patening of software
as such but only of washing machines, mobile phones etc, which they
called %(q:computer-aided inventions).

#WWa: When a solution is %(q:aided) by a computer, such as is the case e.g.
in %(q:computer-aided design) and %(q:computer-aided manufacturing)
(CAD/CAM), the patent claim would usually not be directed to the
software as such, but to an industrial engineering process.  A
computer can %(e:aid) such a process but not %(e:implement) it.  A
computer alone can only %(q:implement) a software solution, and
software running on a computer is nothing more than software as such
and thus not an %(q:invention) in the sense of patent law.

#Tit: Title

#EWs: Proposal for a directive of the European Parliament and of the Council
on the patentability of computer-%(s:implemented) inventions

#ehr: Proposal for a directive of the European Parliament and of the Council
on the patentability of computer-%(s:aided) inventions

#ril: Articles

#vtm: This Directive lays down rules for the patentability of
computer-%(s:implemented) inventions.

#Tap: This directive lays down %(s:limiting) rules for the patentability of
computer-%(s:aided) inventions.

#srp: %(q:computer-implemented invention) means %(s:any) invention the
performance of which involves the use of a %(s:computer, computer
network or other) programmable apparatus, %(s:the invention having one
or more features which are realised wholly or partly by means of a
computer program or computer programs);

#rpt: %(s:%(q:Computer-aided invention), also called
%(q:computer-implemented invention)), means an invention %(s:in the
sense of patent law) the performance of which involves the use of a
programmable apparatus.

#teW: As long as the old term is still in use, we need to define it here as
a synonym.

#ffx: We have elided wordings in the Council text which are unclear and
redundant and whose only purpose seems to be to suggest that the
claimed invention can consist in nothing but software running on a
computer.

#etq: Our proposed amendment is a simplified version of an amendment adopted
in 1st reading.

#fiu: %(q:technical contribution) means a contribution to the state of the
art in a field of technology which is new and not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, %(s:which) must
comprise technical features, irrespective of whether or not these are
accompanied by non-technical features.

#WWi: %(q:technical contribution), also called invention, means a
contribution to the state of the art in a field of technology.  The
technical character of the contribution is one of the four
requirements of patentability.  Additionally, to deserve a patent, the
technical contribution must be new, non-obvious to the person skilled
in the art, and susceptible of industrial application.  The use of
forces of nature to control physical effects belongs to a field of
technology. The processing, handling, and presentation of information
do not belong to a field of technology, regardless of whether
technical devices are employed for such purposes;

#mjW: Upon close reading, it appears that according to the Council's text
the %(q:technical contribution) may consist solely of non-technical
features.  The text is full of redundant statements and misleading
ambiguities, but does contain some usable elements.

#tte: The concept of %(q:technical contribution) has pervaded the discussion
about the directive and generated great confusion and therefore to
some extent deserves to be clarified.  While intuitively and in the
subjective belief of most discutants the %(q:technical contribution)
appears to be related to the question of patentable subject matter
(Art 52 EPC), the EPO used the term as a means of abolishing the
subject matter test by  mixing it into the non-obviousness test (Art
56 EPC) in obcure ways, which national courts and ministerial patent
officials have found difficult to follow.  It is thus particularly
important that, as far as the written law uses this term, it is
understood to be connected to the concept of %(q:invention)
(patentable subject matter) and dissociated from all other conditions
of patentability.

#tWW: A similar amendment that was adopted in first reading by the EP.  We
have added some ideas of the Council such as that of subtracting the
prior art from the claimed object.  If worded carefully, as we
attempted, this can help provide further clarification.

#tal: This is the Parliament's Art 2(b) from the 1st reading.  It is one of
the very few substantial amendment of the Parliament that were ever
brought into the discussion by several delegations of ministerial
patent officials in the Council's working party.   Yet, since the
current Council version of Art 2(b) is not based on the Parliament's
work, we retable the Parliament's version as an independent provision.

#sfW: A %(q:field of technology) is a discipline of applied science in which
new knowledge is gained by experimentation with controllable forces of
nature. %(q:Technical) means %(q:belonging to a field of technology);

#eqo: This amendment clarifies the term %(q:field of technology) from Art 27
TRIPs.

#iea: It is an improved version of the Parliament's former amendment 2(c).

#WWt: A discipline is normally characterised not by its domain of
application but by the way in which it gains knowledge.  For patent
granting, what matters is where the achievement lies, not to which
domain it is applied.  Also, %(q:industrial applicablity) is a
requirement is a separate requirement of patentability.  Patentability
requirements should stand on their own, relying on each other as
little as possible.

#icW: %(q:Industry) in the sense of patent law means commercially organised
production of material goods;

#ooW: Information goods can be reproduced within seconds on millions of
computers worldwide at zero cost.  More than material goods,
information goods are suitable for production by small businesses and
freelancers.  The economics differ, and the business models for
information goods tend to be closer to those of the service sector
than of the classical %(q:industry) sector.

#rhn: This amendments clarifies a central term of Art 27 TRIPs which has
been used in several provisions and amendments within this directive.

#mth: We do not want innovations in the %(q:music industry) or %(q:legal
services industry) to meet the TRIPS requirement of %(q:industrial
applicability). The word %(q:industry) is nowadays often used in
extended meanings which are not appropriate in the context of patent
law.

#ess: In the tradition of patent law, %(q:industry) refers to the primary
and secondary sector, i.e. it includes agriculture.  The distinction
between these sectors and the tertiary (software and service) sector
is economically meaningful.  E.g. in the anti-trust procedings against
IBM, the company was split into two along these lines.

#lda: This amendment corresponds to article 2(d) in the consolidated text of
the EP’s first reading, except that we replaced %(q:automated) with
%(q:commercially organised), so as to approximate the original meaning
in the legal tradition more closely.

#eWn: It should be noted that the requirement of industrial applicability in
itself has very little excluding force.   Most advances in the area of
mathematics or business methods are applicable to industry, no matter
how the term is defined.

#tnl: This amendment clarifies Art 27 TRIPs by a negative definition of
%(q:fields of technology).

#anf: Data processing is a branch of mathematics, a mental activity whose
innovative advances lie in the area of abstraction, and whose
technical aspects, if existent at all, are known and trivial.  This
amendment in no way affects the patentability of the computers
themselves, or of any processes involved in implementing the abstract
data processing machine into silicon, wood or DNA.

#eit: Strictly speaking, the amendment does not even exclude software from
patentability.  Rather, it forbids certain extensive interpretations
of Art 27 TRIPs which have been used to circumvent Article 52 of the
European Patent Convention and to reduce the freedom of the judiciary
to interpret this article in meaningful ways.

#ttP: This amendment corresponds to article 3 in the consolidated text of
the EP’s first reading.

#rWn: In order to be patentable, a computer-implemented invention must
%(s:be susceptible of industrial application and new and involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution).

#nWW: In order to be patentable, an invention must %(s:make a technical
contribution.  The technical contribution must be new and involve an
inventive step.  If there is no technical contribution, there is no
patentable subject matter, and no invention).

#inb: Even the Council's Art 2(b) agrees that the %(q:technical
contribution) must be new and involve an inventive step, and not vice
versa.  Our second sentence makes it absolutely clear that the
%(q:technical contribution) requirement is closely connected to the
requirement of patentable subject matter and dissociated from that of
non-obviousness.

#tpt: Moreover we delete the attribute %(q:computer-implemented), since the
above logic applies to all patentable inventions.  We cannot see any
advantage in creating sui generis software patent law.

#War: Member States shall ensure that data processing solutions are not
considered to be patentable inventions when they only improve
efficiency in the use of resources within data processing systems.

#wnm: Nobody ever writes software without trying to optimise the use of
computing resources.

#sfb: This amendment makes sure that this fact does not justify the granting
of a patent. This codifies both UK case law (Gale's application) and
Germany’s case law (BPatG ruling in the Error Search case).  As the
German court found: if an improvement of efficiency in the use of
computing ressources, such as time or data space, is deemed to be a
technical contribution, then all computer-implemented business methods
become patentable.

#dsl: This amendment corresponds to article 6 in the consolidated text of
the EP’s first reading, except that we have changed
%(q:computer-implemented) to %(q:computer-aided).

#ufe: %(s:A) computer program %(s:as such) %(s:cannot) constitute a
patentable invention.

#fno: Programs for computers are not inventions in the sense of patent law.

#uAs: Art 52(2) EPC states that programs for computers are not inventions in
the sense of patent law.  It is a good idea to transfer this provision
into EU law.  The additional provision of Art 52(3) (exclusion
pertains only to subject matter as such) should be reflected in an
additional clause, which also clarifies this provision.  The EU law
should be clearer, not less clear, than Art 52 EPC.

#Aso: A computer program is a solution of a problem by calculation with the
abstract entities of a generic data processing machine, such as input,
output, processor, memory, storage and interfaces for information
exchange with external systems and human users.  A computer program
may take various forms, e.g. a computing process, an algorithm, or a
text recorded on a medium.  If the contribution to the known art
resides solely in a computer program then the subject matter is not
patentable in whatever manner it may be presented in the claims.

#CeW: We propose to replace the Council's amendment with a text which
concretises the meaning of Art 52(2) and (3) EPC.  Our proposal is
based on the explanation given in the original Examination Guidelines
of 1978 and subsequent caselaw.

#moe: The Commission’s last minute amendments inserted at the Council 18 May
2004 meeting redefine a %(q:computer program as such) to referring to
the %(q:source code or machine code) of an individual computer
program, as defined by copyright.  This is meaningless in the context
of patent law.  The effect of the Council's proposal can only be to
make Art 52 EPC meaningless.

#aWr: The %(q:normal interaction between programs and computers) is about as
well defined as the %(q:normal interaction between the cook and the
recipe).  It is a legal formula which the EPO invented in 1998 in
order to circumvent Art 52 EPC.  Only two years later, the EPO itself
commented on this formula as follows:

#sWW: There is no need to consider the concept of %(q:further technical
effect) in examination, and it is preferred not to do so for the
following reasons: firstly, it is confusing to both examiners and
applicants; secondly, the only apparent reason for distinguishing
%(q:technical effect) from %(q:further technical effect) in the
decision was because of the presence of %(q:programs for computers) in
the list of exclusions under Article 52(2) EPC. If, as is to be
anticipated, this element is dropped from the list by the Diplomatic
Conference, there will no longer be any basis for such a distinction.
It is to be inferred that the BoA would have preferred to be able to
say that no computer-implemented invention is excluded from
patentability by the provisions of Articles 52(2) and (3) EPC.

#ime: Member States shall ensure that a computer-%(s:implemented) invention
may be claimed as a product, that is as a %(s:programmed computer, a
programmed computer network or other) programmed apparatus, or as a
process carried out by such a %(s:computer, computer network or)
apparatus %(s:through the execution of software).

#roW: Member States shall ensure that a computer-aided invention may be
claimed as a product, that is as a programmed apparatus, or as a
process carried out by such an apparatus.

#woW: Software in combination with generic computing equipment is still not
more than software (as such).  Suggestions that software can be
patentable are outside the scope of this article should be avoided.

#weW: A claim to a computer program, either on its own or on a carrier,
shall not be allowed %(s:unless that program would, when loaded and
executed in a computer, programmed computer network or other
programmable apparatus, put into force a product or process claimed in
the same patent application in accordance with paragraph 1).

#mWa: A patent claim to a computer program, either on its own or on a
carrier, shall not be allowed.

#cee: It is contradictory to say that computer programs at the same time
cannot be inventions, and saying that they nevertheless can be claimed
in a patent. Additionally, the condition after the %(q:unless) can
always be fulfilled.

#nmi: The Commission purposefully did not include these so-called
%(q:program claims) in its original proposal, as allowing patent
monopolies on programs on their own is hard to defend if you at the
same time want to maintain that %(q:program as such) are not
patentable.

#nlc: Getting rid of this Council amendment is one of the most basic
requirements. In first reading, the EP rejected a similar amendment,
and the replacement is part of an amendment which was adopted (article
7 paragraph 2 of the consolidated version).

#uon: Member States shall ensure that the distribution and publication of
information, in whatever form, can never constitute direct or indirect
infringement of a patent.

#Wiu: Freedom of publication, as stipulated in Art 10 ECHR, can be limited
by copyright but not by patents.  Patent rights are broad and unsuited
for information goods.  This amendment does not make any patents
invalid, rather it limits the ways in which a patent owner can enforce
his patents.  Such a provision should be complemented by other
provisions which make sure that information patents are not granted in
the first place.

#rWf: This amendment is a simplified and reduced version of article 7
paragraph 3 in the consolidated text of the EP’s first reading.

#raf: Member States shall ensure that whenever a patent claim names features
that imply the use of a computer program, an well-functioning and
well-documented program text shall be published as part of the patent
description without any restricting licensing terms.

#reW: A program listing is an excellent means of describing to a skilled
person what a computer-aided process does.  This amendments ensures
that the obligation of disclosure is taken seriously, and that
software is treated as a means of describing the invention, rather
than as an invention in itself.  The Commission's objection that
patent law does not normally require the disclosure of a full
reference implementation does not apply, because we are not asking for
an implementation of the invention but only for an accurate
description.

#naa: This requirement makes it a little more difficult to block people from
doing things you even haven't done yourself, but which are obviously
possible since the computing model is perfectly defined and you always
know what you can do with a computer. When you publish working source
code you at least offer some real knowledge on how to solve the
problem, unlike when you say in the claims language that a
%(q:processor means coupled to input output means so that they compute
a function such that the result of said function when output through
said output means solves the problem the user wanted to solve).

#qrn: Note that this amendment does not require that the source code for all
programs of the patent owner which use these features be disclosed. He
only has to provide a single, simple text which describes the
monopolised functionality in a programming language.

#Whe: This amendment corresponds to article 7 paragraph 5 in the
consolidated text of the EP’s first reading.

#WWn: Member States shall ensure that, wherever the use of a patented
technique is needed for the sole purpose of ensuring conversion of the
conventions used in two different data processing systems so as to
allow communication and exchange of data content between them, such
use is not considered to be a patent infringement.

#Wno: Interoperability of data processing systems (e.g. computers) lies at
the foundation of the information economy and allows for fair
competition by all players large and small.

#pou: Article 6 of the Council only refers to the exemption provided for by
the Copyright directive. This means that a software developer is
allowed to find out how to make his data processing system
interoperable with that of a competitor, but afterwards he cannot
necessarily use his gained knowledge, since that could be covered by
patents.

#Whe2: This amendment makes sure that patents also cannot be used to prevent
interoperability. It was passed in an almost identical form by ITRE
and JURI prior to the first reading (the second underlined part read
%(q:computer systems or networks)). In first reading, a more sweeping
version of this amendment was passed, which appeared as Article 9 in
the consolidated version.

#Wta: The first underlined part reverts to the spirit of the original
ITRE/JURI version of the interoperability exemption (which is more
limited), which was also supported by Luxembourg and several others in
the Council (but didn’t make it).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/amends05.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: amends05 ;
# txtlang: xx ;
# End: ;

