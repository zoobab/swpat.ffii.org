<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Amendment Proposals in 2005

#descr: The European Parliament is facing the challenge of reaffirming the
effective exclusion of software patentability for which it had voted
on 24th September 2003, so as to be able to force the Council and
Commission to conduct a discussion on substance.  Here we try to
collect pointers to documents that may be worth consulting by members
of the European Parliament when amending the Council's Uncommon
Position.

#mdn: Amendments

#oeW: Draft set of amendments (based on selected articles from the first
reading)

#dMc: Amendments in MS Word doc format

#lupd: last update: 2005/03/31, 10:00 am

#elr: Amendments in Portable Document Format (PDF)

#Wpt: The general outline of steps we deem appropriate to start fixing the
Council text

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/amends05.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: amends05 ;
# txtlang: xx ;
# End: ;

