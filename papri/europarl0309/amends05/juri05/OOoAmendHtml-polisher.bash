#name=juri05 admendments
name=566052XM
src=$name.html
dest=$name-polished.html
#sed 's/<FONT COLOR="#000080">.*<FONT COLOR="#000000">//g
#s/table width=[/TABLE ALIGN="center"/;
sed 's/&lt;[^&]*&gt;//g;
/<META NAME=/d
s/TABLE WIDTH=[0-9]*/TABLE ALIGN="center"/;
s/COL WIDTH=[0-9]*/COL WIDTH=50%/;
s/TD WIDTH=[0-9]*/TD/;
s/ CLASS="western"//;
s/ FACE="Times New Roman, serif"//;
s/ STYLE="[^"]*"//;
s/.SPAN LANG="[A-z-]*"...SPAN.//g;
s/.SPAN LANG="[A-z-]*"...SPAN.//g;
s/.FONT SIZE=....FONT.//;
s/.FONT FACE="[A-z, -]*"...FONT.//;
s/.FONT COLOR="#[0-9]*"...FONT.//;
s/.FONT SIZE=....FONT.//;
s/.FONT FACE="[A-z, -]*"...FONT.//;
s/.FONT COLOR="#[0-9]*"...FONT.//;
s/ COLOR="#.0.0.0"//;
s/ CELLSPACING=0//;
s/ FRAME=VOID//;
s/ BORDERCOLOR="#ffffff"//;
s/ RULES=COLS//;
s/<FONT>\([^<>]*\)<\/FONT>/\1/;
s/<FONT>//g;
s/<FONT SIZE=.>//g;
s/<FONT FACE="[^"]*">//g;
s/<\/FONT>//g;
s/ ALIGN=CENTER//;
s/ ALIGN=LEFT//;
s/ ALIGN=JUSTIFY//;
s/ ALIGN=RIGHT//;
s/P LANG="..-.."/P/;
s/<SPAN LANG="..-..">\([^<>]*\)<\/SPAN>/\1/g;
s/<\/SPAN><SPAN LANG="..-..">//g;
s/<CENTER>//
s/<\/CENTER>//
' $src |
awk -F '>' '{
	if ($0 ~ /^[ 	]*$/)
		next;
#print "^"
	gsub(/Amendement/, "Amendment");
	gsub(/Articel/, "Article");
	gsub(/Article2,/, "Article 2,");
        gsub(/\> Amendement/, ">Amendment");
        gsub(/\<P\> /, "<P>");
        gsub(/[ 	]*$/, "");
        gsub(/	 /, "	");
        gsub(/^ *</, "<");
        gsub(/[^ 	]\([ 	]*\)/, " ");
	if ($0 ~ /^62/) {
		#print "TEST:" $0
		#getline
		#print "TEST:" $0 "--"
		#if ($0 ~ /<P>$/) print "HEY" else print "BOO"
	}
	if (strip == 1)
		gsub(/^[ 	]*/, "");
	strip=0;
	if ($1=="<P" || $0 ~ /^\<P\>$/) {
		printf "%s", $0;
		strip=1;
		#printf "FOO:%s--", $0;
	} else {
		test=lastline $0;
		if (test ~/Amendment/ || test ~/Or\./ || test ~/Paragraphe$/ || test ~/Recital$/ || test ~/Considerando/ || test ~/Artykul/ || test ~/Art.*culo/ || test ~/Artikel/ || test ~/Article$/ || test ~/Article 2,$/ || $1 ~/^.SPAN LANG=/)
			printf "%s ", $0;
		else
			print;
	}
	lastline=$0
} ' |
sed 's/<P><P>/<P>/;
s/<SPAN LANG="..-..">\([^<>]*\)<\/SPAN>/\1/g;
s/<SPAN LANG="..-..">//g;
s/<\/SPAN>//g;
s/ <P> <I>/<P><I>/g;
s/<\/P> <P> /<\/P><P>/g;
s/<\/P> <P>/<\/P><P>/g;
' >$dest
exit
s/.FONT...FONT.//;
s/.SPAN LANG="[A-z-]*"...SPAN.//;
s/.FONT SIZE=...FONT.//;
s/.FONT FACE="[A-z-]*"...FONT.//;
s/ COLOR="#000080"\>//;
: '
s/ RULES=COLS//g
s/TABLE WIDTH=... /TABLE /;
s/<CENTER>//g
s/<\/CENTER>/<br> <br>/g
s/<\/center>/&<\/FONT>/g
s/^<center>/&<FONT COLOR="#0000ff">/
'
ls -l $src $dest
mozilla $dest
