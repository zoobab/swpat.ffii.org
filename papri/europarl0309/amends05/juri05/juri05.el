(am 
(aut "Daniel Stroz")
(nr 40)
(tit (ML eoo "rejette la position commune"))
(lang "fr")
(just (ML Wqi "En proposant la brevetabilité des inventions mises en oeuvre par ordinateur, la Commission ouvre la voie à la brevetabilité du savoir humain. De plus, cette directive ne répond pas aux enjeux économiques, scientifiques et culturels du secteur du logiciel ainsi qu'à la nécessité de promouvoir l'innovation. Pour toutes ces raisons et pour répondre à la forte opposition de scientifiques, d'éditeurs de logiciels, la position commune doit être rejetée"))
)

(am 
(aut (ml "Evelin Lichtenberger") (ml "Monica Frassoni"))
(nr 41)
(ref (ML pto "Apartado 1"))
(tit (ML eCs "Rejects the Common Position"))
(lang "en")
(just (ML etu "The Common Position was questioned by several Member states in the Council, even when the political agreement was finally adopted earlier this year. This means that there are strong doubts about the content of the Common Position."))
)

(am
(aut (ml "Barbara Kudrycka") (ml "Tadeusz Zwiefka"))
(nr 42)
(ref tit)
(old (ML ehr "Proposal for a directive of the European Parliament and of the Council on the patentability of computer-implemented inventions"))
(nov (ML rfm "Proposal for a directive of the European Parliament and of the Council on the patentability of computer-aided inventions"))
(lang "en")
(just (ML ste "This replacement is to be performed at all places in the text where the expression %(q:computer-implemented invention) is used.") (ML nwh "In their press release upon adoption of the %(q:Common Position), the Council says that that their text did not allow patening of software as such but only of washing machines, mobile phones etc, which they called %(q:computer-aided inventions).") (ML dth "When a solution is %(q:aided) by a computer, such as is the case e.g. in %(q:computer-aided design) and %(q:computer-aided manufacturing) (CAD/CAM), the claim is usually not directed to the software as such, but to an industrial engineering process. A computer can aid such a process but not implement it. A computer alone can only implement a software solution, and software together with a computer is nothing more than software as such and thus not an %(q:invention) in the sense of patent law. The Council has good reasons to avoid this misleading term in its press release. The same reasons apply to the whole directive text."))
)

(am
(aut "Fausto Bertinotti")
(nr 43)
(ref tit)
(old (ML ehr2 "Proposal for a directive of the European Parliament and of the Council on the patentability of computer-implemented inventions"))
(nov (ML rfm2 "Proposal for a directive of the European Parliament and of the Council on the patentability of computer-aided inventions"))
(lang "en")
(just (ML ste2 "This replacement is to be performed at all places in the text where the expression %(q:computer-implemented invention) is used.") (ML nwh2 "In their press release upon adoption of the %(q:Common Position), the Council says that that their text did not allow patening of software as such but only of washing machines, mobile phones etc, which they called %(q:computer-aided inventions).") (ML dth2 "When a solution is %(q:aided) by a computer, such as is the case e.g. in %(q:computer-aided design) and %(q:computer-aided manufacturing) (CAD/CAM), the claim is usually not directed to the software as such, but to an industrial engineering process. A computer can aid such a process but not implement it. A computer alone can only implement a software solution, and software together with a computer is nothing more than software as such and thus not an %(q:invention) in the sense of patent law. The Council has good reasons to avoid this misleading term in its press release. The same reasons apply to the whole directive text."))
)

(am
(aut (ml "Mojca Drčar Murko") "Toine Manders" "Diana Wallis")
(nr 44)
(ref tit)
(old (ML lCr "Directive 2004/../EC of the European Parliament of the Council on the patentability of computer implemented inventions"))
(nov (ML lWt "Directive 2004/../EC of the European Parliament of the Council on the patentability of computer operated inventions"))
(or "en")
(just (ML WeW "The title of the directive should undoubtedly express its scope, which is dealing with computer-operated devices, not with software. The word %(q:implemented) might suggest that patenting of software is under circumstances possible."))
)

(am
(aut "Manuel Medina Ortega")
(nr 45)
(ref art 1)
(old (ML ene "La presente Directiva establece normas para la patentabilidad de las invenciones implementadas en ordenador."))
(nov (ML tdB "La presente Directiva establece normas relativas a los límites de la patentabilidad y a la posibilidad de hacer valer las patentes en relación con los programas informáticos."))
(lang es)
(just (ML eWi "El término %(q:invención aplicada a través de ordenador) no es utilizado por los informáticos. De hecho, no se utiliza en absoluto. Fue introducido por el OEP en mayo de 2000 para legitimar las patentes de métodos empresariales, al igual que para equiparar la práctica de OEP a la de Japón y EEUU.  El término " invención aplicada a través de ordenador " implica que las normas de cálculo formadas en los términos del ordenador de uso general sean invenciones patentables.") (ML Wla "Esta implicación está en contradicción con el artículo 52 CEP, según el cual los algoritmos, los métodos y los programas empresariales para los ordenadores no son invenciones en el sentido de la ley de patentes. No puede ser objetivo de la presente directiva declarar todas las clases de ideas " aplicadas a través de ordenador " como invenciones patentables. El objetivo debe ser, más bien, aclarar los límites de la patentabilidad con respeto al procesamiento de datos automáticos y sus diversos campos (técnicos y no técnicos) de aplicación, y esto debe expresarse con una redacción sencilla e inequívoca.")
)
)

(am
(aut "Barbara Kudrycka" "Tadeusz Zwiefka")
(nr 46)
(ref art 1)
(old (ML vtm "This Directive lays down rules for the patentability of computer-implemented inventions."))
(nov (ML eWk "This directive lays down limiting rules for the patentability of computer-aided inventions."))
(lang en)
(just (ML Wti "Only software solutions can be %(q:computer-implemented). The aim of the directive is not to state that software solutions are inventions in the sense of patent law."))
)

(am
(aut "Fausto Bertinotti")
(nr 47)
(ref art 1)
(old (ML vtm2 "This Directive lays down rules for the patentability of computer-implemented inventions."))
(nov (ML eWk2 "This directive lays down limiting rules for the patentability of computer-aided inventions."))
(lang en)
(just (ML Wti2 "Only software solutions can be %(q:computer-implemented). The aim of the directive is not to state that software solutions are inventions in the sense of patent law."))
)

(am
(aut "Evelin Lichtenberger" "Monica Frassoni")
(nr 48)
(ref art 1)
(old (ML vtm3 "This Directive lays down rules for the patentability of computer-implemented inventions."))
(nov (ML ehs "This Directive lays down rules concerning the patentability of computer-assisted inventions."))
(lang en)
(just (ML WWW "The term %(q:implemented) is not suitable, as computer-implemented software is not an invention, as software is not patentable. The computer and its program are used only to control a hardware invention, hence the change of word. Moreover, the expression computer implemented invention is not in use among specialist, on the contrary of the expression %(q:computer-assisted) such as in the software %(q:computer-assisted design / computer-assisted manufacturing)."))
)

(am
(aut "Hans-Peter Mayer")
(nr 49)
(art 2)
(old (ML wWf "Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen"))
(nov (ML wWf2 "Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen"))
(old (spaced "a)" (ML eai "%(q:Computerimplementierte Erfindung) ist eine Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.")))
(nov (spaced "a)" (ML eai2 "%(q:Computerimplementierte Erfindung) ist eine Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die mindestens ein Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.")))
(old (spaced "b)" (ML ide "%(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der neu und für eine fachkundige Person nicht nahe liegend ist. Zur Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs, der technische Merkmale umfassen muss, die ihrerseits mit nichttechnischen Merkmalen versehen sein können, in seiner Gesamtheit vom Stand der Technik abhebt.")))
(nov 
 (spaced "b)" (ML geP "%(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der neu und für eine fachkundige Person nicht nahe liegend ist."))
 (spaced "c)" (ML eee "Software leistet einen technischen Beitrag, soweit sie bei der digitalen Verwaltung, Verarbeitung und Darstellung von Daten den Einsatz beherrschbarer Naturkräfte zur unmittelbaren Herbeiführung des Ergebnisses direkt benutzt.")) ) 
(lang de)
(just (ML uzv "Der hier vorgelegte Änderungsantrag soll den Bereich, in dem Software vom Patentschutz im Rahmen einer technischen Entwicklung mit umfasst ist, näher fixieren und genauer abgrenzen."))
)

(am
(aut "Barbara Kudrycka" "Tadeusz Zwiefka")
(nr 50)
(ref art 2 (pt a))
(old (spaced "a)" (ML WWa "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(nov (spaced "(a)" (ML gns "%(q:Computer-aided invention), also called %(q:computer-implemented invention), means an invention in the sense of patent law the performance of which involves the use of a programmable apparatus;")))
(lang en)
(just (ML Wur "In their press release upon adoption of the %(q:Common Position), the Council says that that their text did not allow patening of software as such but only of washing machines, mobile phones etc, which they called %(q:computer-aided inventions). When a solution is %(q:aided) by a computer, such as is the case e.g. in %(q:computer-aided design) and %(q:computer-aided manufacturing) (CAD/CAM), the claim is usually not directed to the software as such, but to an industrial engineering process. A computer can aid such a process but not implement it. A computer alone can only %(q:implement) (= run) a software solution, and software running on a computer is nothing more than software as such and thus not an %(q:invention) in the sense of patent law.") (ML tse "As long as the old term is still in use, it has to be defined here as a synonym.") (ML Wro "This amendment removes wordings from the Council text which are unclear and redundant and whose only purpose seems to be to suggest that the claimed invention can consist in nothing but software running on a computer.") (ML eni "This amendment is a simplified version of an amendment adopted in 1st reading."))
)

(am
(aut "Fausto Bertinotti")
(nr 51)
(ref art 2 (pt a))
(old (spaced "(a)" (ML WWa2 "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(nov (spaced "(a)" (ML gns2 "%(q:Computer-aided invention), also called %(q:computer-implemented invention), means an invention in the sense of patent law the performance of which involves the use of a programmable apparatus;")))
(lang en)
(just (ML nwh3 "In their press release upon adoption of the %(q:Common Position), the Council says that that their text did not allow patening of software as such but only of washing machines, mobile phones etc, which they called %(q:computer-aided inventions).") (ML WpW "When a solution is %(q:aided) by a computer, such as is the case e.g. in %(q:computer-aided design) and %(q:computer-aided manufacturing) (CAD/CAM), the claim is usually not directed to the software as such, but to an industrial engineering process. A computer can aid such a process but not implement it. A computer alone can only %(q:implement) (= run) a software solution, and software running on a computer is nothing more than software as such and thus not an %(q:invention) in the sense of patent law.") (ML tse2 "As long as the old term is still in use, it has to be defined here as a synonym.") (ML Wro2 "This amendment removes wordings from the Council text which are unclear and redundant and whose only purpose seems to be to suggest that the claimed invention can consist in nothing but software running on a computer.") (ML eni2 "This amendment is a simplified version of an amendment adopted in 1st reading."))
)

(am  
(aut "Piia-Noora Kauppi")
(nr 52)
(ref art 2 (pt a))
(old (spaced "(a)" (ML WWa3 "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(nov (spaced "(a)" (ML noo "%(q:computer-implemented invention) means an invention within the meaning of the European Patent Convention, the performance of which involves the use of a computer, computer network or programmable apparatus.")))
(lang en)
(just (ML Wtn "Reference to the European Patent Convention definition is clearer."))
)

(am
(aut "Andrzej Jan Szejna")
(nr 52)
(ref art 2 (pt a))
(old (spaced "a)" (ML ajr "%(q:wynalazek realizowany przy pomocy komputera) oznacza wynalazek, którego dokonanie wymaga użycia komputera, sieci komputerowej lub innego urządzenia dającego się zaprogramować, wynalazek posiadający jedną lub więcej cech realizowanych w całości lub w części przy pomocy programu komputerowego lub programów komputerowych;")))
(nov (spaced "a)" (ML rWi "%(q:wynalazek realizowany przy pomocy komputera) oznacza wynalazek w sensie prawa patentowego, którego zastosowanie wymaga dodatkowo użycia komputera, sieci komputerowej lub innego urządzenia dającego się zaprogramować,  jako urządzenia sterującego, co zmienia jedną lub więcej cech stanu techniki w danej dziedzinie;")))
(lang pl)
(just (ML iod "Stwierdzenie to stawia znak równości pomiędzy %(q:wynalazek realizowany przy pomocy komputera) a %(q:wymagający dodatkowo użycia komputera)."))
)

(am
(aut "Evelin Lichtenberger" "Monica Frassoni")
(nr 54)
(ref art 2 (pt a))
(old (spaced "a)" (ML WWa4 "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(nov (spaced "a)" (ML Wnm "%(q:computer-assisted invention) means any invention the performance of which involves the use of a computer, a computer network or other programmable apparatus and having one or more non-technical features which are realised wholly or partly by means of a computer programme or computer programmes besides the technical features that any invention must contribute.")))
(lang en)
(just (ML rrt "This is very close to EP 1st reading article 2a ; this amendment aims at ensuring that software cannot be patentable, but rather only computer-assisted hardware inventions, software needs to be excluded from the %(q:technical) field, as Parliament did at first reading."))
)

(am
(aut "Manuel Medina Ortega")
(nr 55)
(ref art 2 (pt a))
(old (spaced "a)" (ML jua "%(q:invenciones implementadas en ordenador), toda invención para cuya ejecución se requiera la utilización de un ordenador, una red informática u otro aparato programable, teniendo la invención una o más características que se realicen total o parcialmente mediante un programa o programas de ordenador;")))
(nov (spaced "(a)" (ML loa "%(q:invenciones asistidas por ordenador), toda invención en el sentido del Convenio de Patente Europea para cuya ejecución se requiera la utilización de un ordenador, una red informática u otro aparato programable y teniendo en su aplicación una o más características no técnicas que se realicen total o parcialmente mediante un programa o programas de ordenador, además de las características que cualquier invención deba aportar.")))
(lang es)
(just (ML Wee "El artículo 52 del CEP establece claramente que un programa de ordenador independiente (o un programa de ordenador como tal) no puede constituir una invención patentable. Esta enmienda aclara que una innovación es solamente patentable si se ajusta al artículo 52 del EPC, independientemente de si un programa de ordenador forma parte de su aplicación o no.") (ML ado "Esta enmienda se adoptó en una forma ligeramente diferente en la primera lectura del PE. Se establecía %(q:invención aplicada a través de ordenador) en vez de %(q:invención asistida por ordenador). La razón para este cambio de la terminología se encuentra en la justificación para el artículo 1."))
)

(am
(aut "Malcolm Harbour")
(nr 56)
(ref art 2 (pt a))
(old (spaced "a)" (ML WWr "%(q:computer implemented inventions) means any invention, the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(nov (spaced "a)" (ML vro "%(q:computer implemented inventions) means any invention within the meaning of the European Patent Convention, the performance of which involves the use of a computer, computer network or other programmable apparatus, the invention having one or more features which are realised wholly or partly by means of a computer program or computer programs;")))
(lang en)
(just (ML ePt "Reference to the EPC clarifies the text."))
)

(am
(aut "Edith Mastenbroek" "Evelin Lichtenberger")
(nr 57)
(ref art 2 (pt b))
(old (ML fst "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art.  The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (ML aoo "An %(q:invention) in the sense of patent law is a contribution to the state of the art in a field of technology.  The contribution is the set of features by which the scope of the patent claim as a whole is claimed to differ from the prior art.  The contribution must be a technical one, i.e. it must comprise technical features and belong to a field of technology.  Without a technical contribution, there is no patentable subject matter and no invention.  The technical contribution must fulfill the conditions for patentability.  In particular, the technical contribution must be novel and not obvious to a person skilled in the art."))
(lang en)
(just (ML afm "Upon close reading, it appears that according to the Council's text the %(q:technical contribution) may consist solely of non-technical features.  The text is full of redundant statements and misleading ambiguities, but does contain some usable elements.") (ML iWe "The concept of %(q:technical contribution) has pervaded the discussion about the directive and generated great confusion and therefore to some extent deserves to be clarified.  While intuitively and in the subjective belief of most discutants the %(q:technical contribution) appears to be related to the question of patentable subject matter (Art 52 EPC), the EPO used the term as a means of abolishing the subject matter test by mixing it into the non-obviousness test (Art 56 EPC) in obscure ways, which national courts and ministerial patent officials have found difficult to follow.  It is thus particularly important that, as far as the written law uses this term, it is understood to be connected to the concept of %(q:invention) (patentable subject matter) and dissociated from all other conditions of patentability.") (ML Wae "A similar amendment that was adopted in first reading by the EP. This amendment adds some ideas of the Council such as that of subtracting the  prior art from the claimed object.  If worded carefully like here, this can help provide further clarification."))
)

(am
(aut "Andrzej Jan Szejna")
(nr 58)
(ref art 2 (pt b))
(old (ML znb "%(q:wkład techniczny) oznacza wkład do stanu techniki w dziedzinie technologii, który jest nowy i nieoczywisty dla specjalisty z danej dziedziny. Wkład o charakterze technicznym ocenia się poprzez rozważenie różnicy pomiędzy stanem techniki oraz zakresem zastrzeżenia patentowego jako całości, które musi zawierać cechy o charakterze technicznym, bez względu na to, czy towarzyszą mu cechy nieposiadające charakteru technicznego.")) 
(nov (ML Wep "%(q:wkład techniczny) oznacza działanie zmieniające stan techniki tak, że staje się on istotnie nowy i nieoczywisty. Wkład techniczny ocenia się poprzez rozważenie różnicy pomiędzy dotychczasowym stanem techniki oraz stanem po uwzględnieniu zakresu zastrzeżeń patentowych, które muszą zawierać cechy o charakterze technicznym, a więc dotyczyć układów materialnych takich jak konstrukcje i tworzywa, oraz materiały, substancje i energia, a także procesy ich wytwarzania i przetwarzania, bez względu na to, czy zastrzeżeniom tym towarzyszą cechy nieposiadające charakteru technicznego."))
(lang pl)
(just (ML eni3 "Jeśli parametry stanu układu materialnego ulegają zmianie mamy do czynienia ze zmianą stanu techniki. Jeśli natomiast ten nowy stan prowadzi do korzyści o charakterze ekonomicznym lub innym, wówczas mówimy o wkładzie technicznym."))
)

(am
(aut "Evelin Lichtenberger" "Monica Frassoni")
(nr 59)
(ref art 2 (pt b))
(old (ML fiu "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (ML oWe "%(q:technical contribution) means a new way and non obvious for a person skilled in the state of the art to use forces of nature to solve a problem in a technical field ;"))
(lang en)
(just (ML Woh "This is very close to EP 1st reading article 2a ; In order to ensure that software cannot be patentable, but rather only computer-controlled hardware inventions, software needs to be excluded from the %(q:technical) field, as Parliament did at first reading."))
)

(am
(aut "Manuel Medina Ortega")
(nr 60)
(ref art 2 (pt b))
(old (ML crs "%(q:contribución técnica), una contribución al estado de la técnica en un campo de la tecnología que sea nueva y no evidente para un experto en la materia. La contribución técnica deberá evaluarse considerando la diferencia entre el estado de la técnica y el ámbito de la reivindicación de la patente considerada en su conjunto, que debe incluir características técnicas con independencia de que éstas estén acompañadas o no de características no técnicas." _))
(nov (ML Eea "%(q:contribución técnica), también llamada %(q:invención), significa una contribución en un campo de la tecnología. El carácter técnico de la contribución es uno de los cuatro requisitos para la patentabilidad. Además, para merecer una patente, la contribución técnica tiene que ser nueva, no obvia, y susceptible de aplicación industrial. El uso de fuerzas naturales para controlar los efectos físicos más allá de la representación digital de la información pertenece a un campo de la tecnología. El tratamiento, manejo, y presentación de la información no pertenece a un campo de la tecnología, ni siquiera donde los dispositivos técnicos son empleados para tales fines. En todo caso, las características no técnicas de la invención no serán tomadas en consideración en la evaluación de la contribución técnica.")) 
(lang es)
(just (ML udW "El texto del Consejo combina la prueba de la invención (contribución técnica) con otras pruebas no obvias (escalón de invención) y nuevas, debilitando, por ello, todas las pruebas, desviándose del artículo 52 CPE, y creando problemas prácticos.") (ML iaW "Las %(q:cuatro fuerzas de la naturaleza) son un concepto reconocido de epistemología (teoría de la ciencia). Mientras que las matemáticas y el procesamiento de datos son abstractos y sin relación al que se relaciona con las fuerzas de la naturaleza, podría, sin embargo, sostenerse que algunos métodos empresariales dependen de la química de las células cerebrales del cliente. Éstas  son fuerzas no controlables, es decir, sujetas a la libre voluntad.") (ML etl "Por tanto el término %(q:fuerzas controlables de la naturaleza) excluye claramente lo que necesita ser excluido y sin embargo proporciona bastante flexibilidad para la inclusión de posibles campos futuros de ciencia natural aplicada más allá de las actualmente reconocidas %(q:4 fuerzas de la naturaleza).") (ML ica "Este concepto se ha formulado en la mayor parte de las jurisdicciones e incluso se ha incorporado a  la legislación de países como Japón y Polonia. La justificación clásica para el carácter técnico de las %(q:invenciones aplicadas a través de ordenador) no es que el significado de %(q:técnico) haya cambiado, sino  que el ordenador efectivamente consume la energía de una manera controlada, y que la %(q:invención) debe %(q:considerarse como un todo).") (ML eon "Los críticos de esta opinión, por ejemplo el Tribunal Federal Alemán de Patentes, argumentan que la solución es completada mediante un cálculo anterior abstracto, y solamente durante la aplicación no inventiva sobre un sistema de procesamiento de datos, entran en juego las fuerzas de la naturaleza."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 61)
(ref art 2 (pt b))
(old (ML fiu2 "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (ML irp "%(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in a technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field.  The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes. The method of data processing by using a computer, network or other programmable apparatus is not considered to belong to a field of technology."))
(lang en)
(just (ML cix "The definition of %(q:technical contribution) has to be clear in order to determine what is patentable and what is not. Reference to %(q:technical features) is too vague."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 62)
(ref "Article 2 point b)")
(old (ML fst2 "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art.  The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (ML aoo2 "An %(q:invention) in the sense of patent law is a contribution to the state of the art in a field of technology.  The contribution is the set of features by which the scope of the patent claim as a whole is claimed to differ from the prior art.  The contribution must be a technical one, i.e. it must comprise technical features and belong to a field of technology.  Without a technical contribution, there is no patentable subject matter and no invention.  The technical contribution must fulfill the conditions for patentability.  In particular, the technical contribution must be novel and not obvious to a person skilled in the art."))
(lang en)
(just (ML afm2 "Upon close reading, it appears that according to the Council's text the %(q:technical contribution) may consist solely of non-technical features.  The text is full of redundant statements and misleading ambiguities, but does contain some usable elements.") (ML iWe2 "The concept of %(q:technical contribution) has pervaded the discussion about the directive and generated great confusion and therefore to some extent deserves to be clarified.  While intuitively and in the subjective belief of most discutants the %(q:technical contribution) appears to be related to the question of patentable subject matter (Art 52 EPC), the EPO used the term as a means of abolishing the subject matter test by mixing it into the  non-obviousness test (Art 56 EPC) in obcure ways, which national courts and ministerial patent officials have found difficult to follow.  It is thus particularly important that, as far as the written law uses this term, it is understood to be connected to the concept of %(q:invention) (patentable subject matter) and dissociated from all other conditions of patentability.") (ML Wae2 "A similar amendment that was adopted in first reading by the EP. This amendment adds some ideas of the Council such as that of subtracting the  prior art from the claimed object.  If worded carefully like here, this can help provide further clarification."))
)

(am
(aut "Fausto Bertinotti")
(nr 63)
(ref "Article 2 point b)")
(old (ML fst3 "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art.  The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (ML aoo3 "An %(q:invention) in the sense of patent law is a contribution to the state of the art in a field of technology.  The contribution is the set of features by which the scope of the patent claim as a whole is claimed to differ from the prior art.  The contribution must be a technical one, i.e. it must comprise technical features and belong to a field of technology.  Without a technical contribution, there is no patentable subject matter and no invention.  The technical contribution must fulfill the conditions for patentability.  In particular, the technical contribution must be novel and not obvious to a person skilled in the art."))
(lang en)
(just (ML afm3 "Upon close reading, it appears that according to the Council's text the %(q:technical contribution) may consist solely of non-technical features.  The text is full of redundant statements and misleading ambiguities, but does contain some usable elements.") (ML iWe3 "The concept of %(q:technical contribution) has pervaded the discussion about the directive and generated great confusion and therefore to some extent deserves to be clarified.  While intuitively and in the subjective belief of most discutants the %(q:technical contribution) appears to be related to the question of patentable subject matter (Art 52 EPC), the EPO used the term as a means of abolishing the subject matter test by mixing it into the  non-obviousness test (Art 56 EPC) in obcure ways, which national courts and ministerial patent officials have found difficult to follow.  It is thus particularly important that, as far as the written law uses this term, it is understood to be connected to the concept of %(q:invention) (patentable subject matter) and dissociated from all other conditions of patentability.") (ML Wae3 "A similar amendment that was adopted in first reading by the EP. This amendment adds some ideas of the Council such as that of subtracting the  prior art from the claimed object.  If worded carefully like here, this can help provide further clarification."))
)

(am
(aut "Arlene McCarthy")
(nr 64)
(ref "Article2, point b)")
(old (ML fiu3 "%(q:technical contribution) means a contribution to the state of the art in a field of technology which is new and not obvious to a person skilled in the art. The technical contribution shall be assessed by consideration of the difference between the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective of whether or not these are accompanied by non-technical features."))
(nov (linul (ML tyb "%(q:technical contribution) means the application of a new process using physical forces in an inventive and non-obvious way, subject to the following provisos:") (ML tWc "The process may require a computation using a computer, but the normal physical processes of a computer cannot be part of the %(q:technical contribution): a new process using physical forces separate from all computation is required.") (ML hno "The process may perform communication with computers or people, but the physical processes of a pre-existing communication apparatus cannot be part of the %(q:technical contribution): a new process using physical forces separate from any pre-existing communication apparatus is required.")))
(lang en)
(just (ML tnr "Ensures compatibility with the software copyright directive and ensures users have access to interoperable programme systems and networks."))
)

(am
(aut "Jean-Paul Gauzès")
(nr 65)
(ref "Articel 2, point b)")
(old (ML lnq "%(q:contribution technique) désigne une contribution à l’état de l’art dans un domaine technique, qui est nouvelle et non évidente pour une personne du métier.   La contribution technique est évaluée en prenant en considération la différence entre l’état de la technique et l’objet de la revendication considérée dans son ensemble qui doit comprendre des caractéristiques techniques, qu’elles soient ou non accompagnées de caractéristiques non techniques."))
(nov (ML toW "%(q:contribution technique) désigne une solution à un problème dans un domaine technique."))
(lang fr)
(just (ML niW "L’article 2(b), combiné avec l’article 2(c) suggéré, propose une définition non-tautologique de l’expression %(q:contribution technique).  Il est clair qu’une  contribution technique correspond à une solution technique :  une solution à un problème dans un domaine technique.  L’évaluation mentionnée à la seconde phrase de l’article 2(b) est relative à une approche largement utilisée pour la détermination de l’activité inventive.  La seconde phrase est par conséquent supprimée de l’article pour se référer seulement à une contribution technique."))
)

(am
(aut "Othmar Karas")
(nr 66)
(ref "Article 2, point (b) a (new)")
(nov (ML oeW "%(q:Interoperability) means the ability of a computer program to communicate and exchange information with other computer programs and mutually to use the information which has been exchanged, including the ability to use, convert, or exchange file formats, protocols, schemas, interface information or conventions, so as to permit such a computer program to work with other computer programs and with users in all the ways in which they are intended to function."))
(lang en)
(just (ML cho "It is necessary to design the meaning of interoperability."))
)

(am
(aut "Klaus-Heiner Lehne")
(nr 67)
(ref "Article 2 point (b) a (new)")
(nov (ML amm "%(q:Technical) means the identification of a physical effect which goes beyond the digital representation of information and the normal physical interaction between software and hardware of a computer, network or other programmable apparatus."))
(lang en)
(just (ML fno "It is necessary to make clear that the term %(q:technical) demands the identification of a physical effect. Such an effect must be produced in addition to effects with regard to digital representation of information and such caused merely by the interaction between software and hardware with regard to operability."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 68)
(ref "Article 2, point (b a) (new)")
(nov (ML sfW "%(q:field of technology) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a field of technology)."))
(lang en)
(just (ML tle "These definitions are essential, especially to clarify the term %(q:field of technology)."))
)

(am
(aut "Arlene McCarthy")
(nr 69)
(ref "Article2, point b a) (new)")
(nov (ML oeW2 "%(q:Interoperability) means the ability of a computer program to communicate and exchange information with other computer programs and mutually to use the information which has been exchanged, including the ability to use, convert, or exchange file formats, protocols, schemas, interface information or conventions, so as to permit such a computer program to work with other computer programs and with users in all the ways in which they are intended to function."))
(lang en)
(just (ML lpe "This amendment clarifies that the physical use of a computer cannot be considered as the %(q:technical contribution)."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 70)
(ref "Article 2 point b a) (new)")
(nov (ML axs "A %(q:field of technology) is a discipline of applied sciences in which new knowledge is gained by experimentation with controllable forces of nature. %(q:Technical) means %(q:belonging to a field of technology);"))
(lang en)
(just (ML mqm " This amendment clarifies the term %(q:field of technology) from Art 27 TRIPs.") (ML mag "It is an improved version of the Parliament's first reading article 2(c).") (ML WWe "A discipline is normally characterised not by its domain of application but by the way in which it gains knowledge. For patent granting, what matters is where the achievement lies, not to which domain it is applied. Also, %(q:industrial applicability) is a separate requirement of patentability. Patentability requirements should stand on their own, relying on each other as little as possible."))
)

(am
(aut "Fausto Bertinotti")
(nr 71)
(ref "Article 2 point b a) (new)")
(nov (ML axs2 "A %(q:field of technology) is a discipline of applied sciences in which new knowledge is gained by experimentation with controllable forces of nature. %(q:Technical) means %(q:belonging to a field of technology);"))
(lang en)
(just (ML eqo "This amendment clarifies the term %(q:field of technology) from Art 27 TRIPs.") (ML mag2 "It is an improved version of the Parliament's first reading article 2(c).") (ML for "A discipline is normally characterised not by its domain of application but by the way in which it gains knowledge. For patent granting, what matters is where the achievement lies, not to which domain it is applied. Also, %(q:industrial applicability) is a requirement is a separate requirement of patentability. Patentability requirements should stand on their own, relying on each other as little as possible."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 72)
(ref "Article 2, point b a)(new)")
(nov (ML noW "%(q:Technical) means %(q:belonging to a field of technology);"))
(lang en)
(just (ML Woh2 "This is very close to EP 1st reading article 2a ; In order to ensure that software cannot be patentable, but rather only computer-controlled hardware inventions, software needs to be excluded from the %(q:technical) field, as Parliament did at first reading."))
)

(am
(aut "Jean-Paul Gauzès")
(nr 73)
(ref "Article 2, point b bis) (nouveau)")
(nov (ML nle "%(q:domaine technique) signifie toute activité utilisant directement ou indirectement des forces de la nature contrôlables afin d’obtenir des résultats prévisibles dans le monde physique tels que des signaux électriques, radios ou lumineux. Le traitement de l’information dans le but d’exécuter ou de soutenir une telle activité doit être considéré comme appartenant à un domaine technique, alors que le traitement de l’information à des fins de calcul, de gestion de valeurs financières  ou de traitement de texte ne doit pas être considéré comme appartenant à un domaine technique."))
(lang en)
(just (ML Wtd " La première phrase codifie essentiellement une doctrine utilisée dans la jurisprudence actuelle allemande (« Logikverifikation » de 1999, BGHZ 143, 255) et propose quelques exemples de types de résultats à considérer. Les signaux électriques, radios et lumineux, qui ont fait l’objet d’une mention dans le document de travail 2002/0047 (COD), sont caractéristiques de la technologie moderne.") (ML nqC "La seconde phrase clarifie que le traitement de l’information pour exécuter ou soutenir des activités tels que mentionnées à la première phrase est considéré comme technique, alors que le traitement à d’autre fins (par exemple : méthode de gestion) ne peut être considéré comme technique. Cette limite reflète la pratique actuelle en Allemagne et en Europe."))
)

(am
(aut "Manuel Medina Ortega")
(nr 74)
(ref "Artículo 2, punto b bis) (nuevo)")
(nov (ML faq "%(q:campo de la tecnología) significa ámbito de aplicación industrial requiriendo el uso de fuerzas de la naturaleza controlables para alcanzar resultados fiables. %(q:Técnico) significa %(q:perteneciente a un campo de la tecnología);"))
(lang es)
(just (ML als "El hecho de que los aparatos programables, tales como un ordenador genérico, hagan uso de efectos físicos para procesar la información no debe utilizarse para permitir la protección de patentes a programas que funcionan por medio de tales instrumentos.") (ML anp "Esta enmienda también aclara el término no definido de ADPIC %(q:campo de tecnología)") (ML rWW "Esta enmienda corresponde a la letra c del artículo 2 del texto consolidado de la primera lectura del Parlamento Europeo."))
)

(am
(aut "Manuel Medina Ortega")
(nr 75)
(ref "Artículo 2, punto b ter) (nuevo)")
(nov (ML eWd "%(q:industria) en el sentido de la ley de patentes significa producción automatizada de bienes materiales;"))
(lang es)
(just (ML sfa "Se quiere evitar que las innovaciones en la %(q:industria de la música) o la %(q:industria de los servicios jurídicos) se ajusten con los requisitos de ADPIC relativos a la %(q:aplicabilidad industrial). El concepto de %(q:industria) abarca amplios significados y es utilizado frecuentemente en la actualidad de manera inapropiada en el contexto de la ley de patentes.") (ML oli "Esta enmienda corresponde a la letra d) del artículo 2 del texto consolidado de la primera lectura del PE."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 76)
(ref "Article 2, point b b)(new)")
(nov (ML oio "%(q:Field of technology) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results."))
(lang en)
(just (ML Wcn "This is very close to EP 1st reading article 2a ; In order to ensure that software cannot be patentable, but rather only computer-controlled hardware inventions, software needs to be excluded from the ‘technical’ field, as Parliament did at first reading."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 77)
(ref "Article 2, point (b b) (new)")
(nov (ML Woe "The production and distribution of information goods is not an %(q:industry) in the sense of patent law."))
(lang en)
(just (ML fce "Information goods can be reproduced on millions of computers within seconds at near to zero cost. More than material goods, information goods are suitable for production by freelancers. The economics differ, and the business models for information goods tend to be closer to those of the service sector than of the classical %(q:industry) sector.") (ML idr "This amendment clarifies, using a negative definition, a central term of Art 27 TRIPs which has been used in several provisions and amendments within this directive. If the term is to retain any limiting meaning at all, production of information goods can not fall within it."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 78)
(ref "Article 2 point b b) (new)")
(nov (ML Woe2 "The production and distribution of information goods is not an %(q:industry) in the sense of patent law."))
(lang en)
(just (ML fce2 "Information goods can be reproduced on millions of computers within seconds at near to zero cost. More than material goods, information goods are suitable for production by freelancers. The economics differ, and the business models for information goods tend to be closer to those of the service sector than of the classical %(q:industry) sector.") (ML taj "This amendment clarifies, using a negative definition, a central term of Art 27 TRIPs which has been used in several provisions and amendments within this directive. If the term is to retain any limiting meaning at all, production information goods can not fall within it."))
)

(am
(aut "Fausto Bertinotti")
(nr 79)
(ref "Article 2 point b b) (new)")
(nov (ML Woe3 "The production and distribution of information goods is not an %(q:industry) in the sense of patent law."))
(lang en)
(just (ML fce3 "Information goods can be reproduced on millions of computers within seconds at near to zero cost. More than material goods, information goods are suitable for production by freelancers. The economics differ, and the business models for information goods tend to be closer to those of the service sector than of the classical %(q:industry) sector.") (ML idr2 "This amendment clarifies, using a negative definition, a central term of Art 27 TRIPs which has been used in several provisions and amendments within this directive. If the term is to retain any limiting meaning at all, production of information goods can not fall within it."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 80)
(ref "Article 2, point (b c) (new)")
(nov (ML oeW3 "%(q:Interoperability) means the ability of a computer program to communicate and exchange information with other computer programs and mutually to use the information which has been exchanged, including the ability to use, convert, or exchange file formats, protocols, schemas, interface information or conventions, so as to permit such a computer program to work with other computer programs and with users in all the ways in which they are intended to function."))
(lang en)
(just (ML sWn "It is necessary to define the term %(q:interoperability) in the articles."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 81)
(ref "Article 2 point b c) (new)")
(nov (ML icW "%(q:Industry) in the sense of patent law means commercially organised production of material goods;"))
(lang en)
(just (ML e7m "This amendments clarifies, using a positive defintion, a central term of Art 27 TRIPs which has been used in several provisions and amendments within this directive.") (ML glc "Innovations in the %(q:music industry) or %(q:legal services industry) should not meet the TRIPS requirement of %(q:industrial applicability). The word %(q:industry) is nowadays often used in extended meanings which are not appropriate in the context of patent law.") (ML ess "In the tradition of patent law, %(q:industry) refers to the primary and secondary sector, i.e. it includes agriculture. The distinction between these sectors and the tertiary (software and service) sector is economically meaningful. E.g. in the anti-trust procedings against IBM, the company was split into two along these lines.") (ML ltn "This amendment corresponds to article 2(d) in the consolidated text of the EP’s first reading, except that %(q:automated) was replaced with %(q:commercially organised), so as to approximate the original meaning in the legal tradition more closely.") (ML rMW "It should be noted that the requirement of industrial applicability in itself has very little excluding force. Most advances in the area of mathematics or business methods are applicable to industry, no matter how the term is defined."))
)

(am
(aut "Fausto Bertinotti")
(nr 82)
(ref "Article 2 point b c) (new)")
(nov (ML icW2 "%(q:Industry) in the sense of patent law means commercially organised production of material goods;"))
(lang en)
(just (ML e7m2 "This amendments clarifies, using a positive defintion, a central term of Art 27 TRIPs which has been used in several provisions and amendments within this directive.") (ML glc2 "Innovations in the %(q:music industry) or %(q:legal services industry) should not meet the TRIPS requirement of %(q:industrial applicability). The word %(q:industry) is nowadays often used in extended meanings which are not appropriate in the context of patent law.") (ML ess2 "In the tradition of patent law, %(q:industry) refers to the primary and secondary sector, i.e. it includes agriculture. The distinction between these sectors and the tertiary (software and service) sector is economically meaningful. E.g. in the anti-trust procedings against IBM, the company was split into two along these lines.") (ML ltn2 "This amendment corresponds to article 2(d) in the consolidated text of the EP’s first reading, except that %(q:automated) was replaced with %(q:commercially organised), so as to approximate the original meaning in the legal tradition more closely.") (ML rMW2 "It should be noted that the requirement of industrial applicability in itself has very little excluding force. Most advances in the area of mathematics or business methods are applicable to industry, no matter how the term is defined."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 83)
(ref "Article 2, point b c)(new)")
(nov (ML Wsr "%(q:information processing method) means any processing method handling digitally represented information."))
(lang en)
(just (ML ese "To garantee that pure  software cannot be patented, but only material inventions controlled by computers, we must exclude the field of software from the field of what is %(q:technical_) as did the Parliament in its 1st reading. The world of software is the world of digital information."))
)

(am
(aut "Michel Rocard")
(nr 84)
(ref "Article 2, point b) quinquies (nouveau)")
(nov (ML ime "%(q:interopérabilité) signifie la capacité pour un programme informatique de communiquer, échanger des informations avec d'autres programmes informatiques et utiliser mutuellement l'information échangée, ce qui comprend la capacité d'utiliser, de convertir et d'échanger des formats de fichiers, des protocoles, des schémas et des informations relatives à des interfaces ou des conventions, afin de permettre à ce programme informatique de communiquer et de coopérer avec d'autres programmes informatiques et avec des utilisateurs de toutes les façons dont ils sont destinés à interagir."))
(lang fr)
(just (ML trW "Il est essentiel, d'autre part, dans le cadre de cette directive, de fournir une définition précise de l'interopérabilité et des actes qui lui sont nécessaires."))
)

(am
(aut "Manuel Medina Ortega")
(nr 85)
(ref "Artículo 3")
(old (ML eun "Para ser patentable, una invención implementada en ordenador deberá ser susceptible de aplicación industrial, ser nueva y suponer una actividad inventiva. Para entrañar una actividad inventiva, la invención implementada en ordenador deberá aportar una contribución técnica."))
(nov)
(lang es)
(just (ML pso "La primera frase fue ya incorporada  en el artículo 2 b) de la resolución aprobada por el PE. La segunda frase mezcla la %(q:invención) o %(q:contribución técnica) con la condición de escalón de invención.") (ML But "El Consejo saca esta %(q:contribución técnica en el escalón de invención) de la Oficina Europea de Patentes, donde se utiliza para permitir que ordenadores genéricos que funcionan con programas informáticos sean considerados invenciones patentables.") (ML lne "Con las definiciones del Parlamento, no es necesaria ninguna condición adicional para la patentabilidad aparte de los mencionados en el artículo 52 del CEP (según lo mencionado en el artículo2 b))"))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 86)
(ref art 3)
(old (ML rte "In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and must involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution."))
(nov (ML iie "Member States shall ensure that inventions are patentable irrespective of whether or not they use computerised means and that, vice versa, no one may patent algorithms, software or information processing methods, whether or not they are combined with technical mechanisms."))
(lang en)
(just (ML fIt "The definition of inventive step supplied by the Council is not suitable, as it refers to the definition of a technical contribution, which is not itself explicitly defined, but which does, however, make reference, by means of the word ‘contribution’, to an inventive step. In order to break this vicious circle, inventive step needs to be defined in relation to itself. In order for a new computer program executed on a technical device not to be a patentable invention, the inventive step has to be evaluated solely in relation to the technical features of the patent claim."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 87)
(ref art 3)
(old (ML kae "In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and must make involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution."))
(nov (ML oas "In order to be patentable, a computer-implemented invention must be susceptible of industrial application and make a technical contribution. The technical contribution must be new and involve an inventive step."))
(lang en)
(just (ML etn "The inventive step should be in the technical contribution for the computer-implemented invention to be patentable."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 88)
(ref "Article 3")
(old (ML rWt "In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and must involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. "))
(nov (ML tsi "In order to be patentable, an [computer-aided] invention must make a technical contribution. The technical contribution must be new and involve an inventive step. If there is no technical contribution, there is no patentable subject matter, and no invention."))
(lang en)
(just (ML eWn "Even the Council's Art 2(b) agrees that the %(q:technical contribution) must be new and involve an inventive step, and not vice versa. The second sentence makes it absolutely clear that the %(q:technical contribution) requirement is closely connected to the requirement of patentable subject matter and dissociated from that of non-obviousness.") (ML ecr "Moreover, this amendment deletes the attribute %(q:computer-implemented), since the above logic applies to all patentable inventions. There is no advantage in creating sui generis software patent law."))
)

(am
(aut "Fausto Bertinotti")
(nr 89)
(ref "Article 3")
(old (ML rWt2 "In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and must involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. "))
(nov (ML rWs "In order to be patentable, a computer-aided invention must make a technical contribution. The technical contribution must be new and involve an inventive step. If there is no technical contribution, there is no patentable subject matter, and no invention.")
(lang en)
(just (ML Wve "Even the Council's Art 2(b) agrees that the %(q:technical contribution) must be new and involve an inventive step, and not vice versa. The second sentence makes it absolutely clear that the " technical contribution " requirement is closely connected to the requirement of patentable subject matter and dissociated from that of non-obviousness.") (ML ecr2 "Moreover, this amendment deletes the attribute %(q:computer-implemented), since the above logic applies to all patentable inventions. There is no advantage in creating sui generis software patent law."))
)

(am
(aut "Andrzej Jan Szejna")
(nr 90)
(ref art 3)
(old (ML pna "Wynalazek realizowany przy pomocy komputera posiada zdolność patentową, jeżeli nadaje się do przemysłowego stosowania, jest nowy oraz posiada poziom wynalazczy. Wynalazek realizowany przy pomocy komputera posiada poziom wynalazczy, jeżeli wnosi wkład techniczny."))
(nov (ML cnW "Wynalazek realizowany przy pomocy komputera posiada zdolność patentową, jeżeli wnosi wkład techniczny i nadaje się do praktycznego, w tym do przemysłowego, stosowania, jest nowy oraz zmienia dotychczasowy stan techniki."))
(lang pl)
(just (ML yon "Artykuł ten podnosi praktyczne znaczenie stosowanego wynalazku, w którym elementy nowości dotyczą zarówno software'u, jak i hardware'u i zmieniają dotychczasowy stan techniki."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 91)
(ref "Article 3 second subparagraph (new)")
(nov (ML sis "In order to be patentable, a computer-controlled invention must be new, susceptible of industrial application and involve an inventive step. The inventive step shall be assessed by consideration of the difference between all of the technical features included in the scope of the patent claim considered as a whole and the state of the art, irrespective of whether or not such features are accompanied by non-technical features."))
(lang en)
(just (ML i44 "This first sentence of this amendment restates the former EP 1st reading amendment 4.1 first part and the second sentence restates former EP 4.3 which were deleted by Council.") (ML sin "The definition of the inventive step of the CCP is insufficient because it refers to the definition of what a technical contribution is, but this is not defined precisely in the text although there is a reference (with the word %(q:contribution)) to an inventive activity. to break this vicious circle we must define the inventive activity as such. So that a new computer programme executed on a technical apparatus can not constitute a patentable invention, the inventive step must be assessed in relation to the sole technical characteristics of the patent claim."))
)

(am
(aut "Edith Mastenbroek, Evelin Lichtenberger")
(nr 92)
(ref "Article 3, second subparagraph (new)")
(nov (ML ato "Member States shall ensure that data processing is not considered to be a field of technology within the meaning of patent law, and that innovations in the field of data processing are not considered to be inventions within the meaning of patent law."))
(lang en)
(just (ML tnl "This amendment clarifies Art 27 TRIPs by a negative definition of %(q:fields of technology).") (ML cnp "Data processing is a branch of mathematics, a mental activity whose innovative advances lie in the area of abstraction, and whose technical aspects, if existent at all, are known and trivial. This amendment in no way affects the patentability of the computers themselves, or of any processes involved in implementing the abstract data processing machine into silicon, wood or DNA."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 93)
(ref "Article 3 second subparagraph (new)")
(nov (ML ato2 "Member States shall ensure that data processing is not considered to be a field of technology within the meaning of patent law, and that innovations in the field of data processing are not considered to be inventions within the meaning of patent law."))
(lang en)
(just (ML tnl2 "This amendment clarifies Art 27 TRIPs by a negative definition of %(q:fields of technology).") (ML cnp2 "Data processing is a branch of mathematics, a mental activity whose innovative advances lie in the area of abstraction, and whose technical aspects, if existent at all, are known and trivial. This amendment in no way affects the patentability of the computers themselves, or of any processes involved in implementing the abstract data processing machine into silicon, wood or DNA.") (ML toa "Strictly speaking, the amendment does not even exclude software from patentability. Rather, it forbids certain extensive interpretations of Art 27 TRIPs which have been used to circumvent Article 52 of the European Patent Convention and to reduce the freedom of the judiciary to interpreting this article in meaningful ways (i.e., this amendment makes sure that one cannot interpret TRIPs in a way which makes it require software patents, but does not say anything about whether or not it allows them).") (ML ttP "This amendment corresponds to article 3 in the consolidated text of the EP’s first reading."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 94)
(ref "Article 3, second subparagraph (new)")
(nov (ML pcb "The application for a patent must disclose the invention in a manner sufficiently clear and complete for it to be carried out by a person skilled in the art."))
(lang en)
(just (ML akh "This amendment clarifies expressly that a patent application has to disclose an invention clearly and comprehensively, so that it can be implemented by someone working in the field.   The expression %(q:a person skilled in the art) is a well-established term of patent law which means someone of ordinary skill in the relevant technical field."))
)

(am
(aut "Fausto Bertinotti")
(nr 95)
(ref "Article 3 second subparagraph (new)")
(nov (ML ato3 "Member States shall ensure that data processing is not considered to be a field of technology within the meaning of patent law, and that innovations in the field of data processing are not considered to be inventions within the meaning of patent law."))
(lang en)
(just (ML tnl3 "This amendment clarifies Art 27 TRIPs by a negative definition of %(q:fields of technology).") (ML cnp3 "Data processing is a branch of mathematics, a mental activity whose innovative advances lie in the area of abstraction, and whose technical aspects, if existent at all, are known and trivial. This amendment in no way affects the patentability of the computers themselves, or of any processes involved in implementing the abstract data processing machine into silicon, wood or DNA.") (ML toa2 "Strictly speaking, the amendment does not even exclude software from patentability. Rather, it forbids certain extensive interpretations of Art 27 TRIPs which have been used to circumvent Article 52 of the European Patent Convention and to reduce the freedom of the judiciary to interpreting this article in meaningful ways (i.e., this amendment makes sure that one cannot interpret TRIPs in a way which makes it require software patents, but does not say anything about whether or not it allows them).") (ML ttP2 "This amendment corresponds to article 3 in the consolidated text of the EP’s first reading."))
)

(am
(aut "Klaus-Heiner Lehne")
(nr 96)
(ref "Article 3 a (new)")
(nov (ML pcb2 "The application for a patent must disclose the invention in a manner sufficiently clear and complete for it to be carried out by a person skilled in the art."))
(lang en)
(just (ML akh2 "This amendment clarifies expressly that a patent application has to disclose an invention clearly and comprehensively, so that it can be implemented by someone working in the field.   The expression %(q:a person skilled in the art) is a well-established term of patent law which means someone of ordinary skill in the relevant technical field."))
)

(am
(aut "Manuel Medina Ortega")
(nr 97)
(ref "Artículo 4, apartado 1")
(old (ML Wpc "Un programa de ordenador como tal no podrá constituir una invención patentable."))
(nov)
(lang es)
(just (ML Bat "Las últimas enmiendas de la Comisión incorporadas en la reunión del Consejo 18 de mayo de 2004 redefinen un programa de ordenador como tal para hacer referencia al código de fuentes o al código automático de un programa individual de ordenador.") (ML lih "Nadie está interesado en las patentes de tales programas de ordenador individuales. Estas inserciones no responden a ningún propósito regulador sino que más bien imponen una interpretación del artículo 52 CEP que priva de sentido a la ley y es rechazada por el Tribunal Federal Alemán e incluso por la OEP.") (ML WsW "La doctrina de los %(q:efectos técnicos más allá de la interacción física normal) es un formalismo introducido con la única intención de convertir los métodos empresariales aplicados por ordenador en patentables, según lo explicado en el apéndice 6 de un informe enviado por la OEP a las oficinas de patentes de EEUU y Japón: http://www.european-patente-office.org/tws/appendix6.pdf") (ML iWx "El cambio de %(q:invención) a %(q:innovación) es necesario porque %(q:invención) es sinónimo de %(q:materia legal) en el CEP.") (ML qnj "La redacción original sugiere que todo lo que se puede hacer en un ordenador es por definición una invención (y por lo tanto llega a ser patentable si es igualmente nuevo, no obvio e industrialmente aplicable)."))
)

(am
(aut "Manuel Medina Ortega")
(nr 98)
(ref "Artículo 4, apartado 2")
(old (ML Wir "No se considerará que una invención implementada en ordenador aporta una contribución técnica meramente porque implique el uso de un ordenador, red u otro aparato programable. En consecuencia, no serán patentables las invenciones que utilizan programas informáticos expresados en código fuente, en código objeto o en cualquier otra forma que implementan métodos para el ejercicio de actividades económicas, matemáticos o de otro tipo y no producen efectos técnicos, aparte de la normal interacción física entre un programa y el ordenador, red o aparato programable de otro tipo en que se ejecute"))
(nov (ML uio "2. No se considerará que una innovación asistida por ordenador aporta una contribución técnica meramente porque implique el uso de un ordenador, red u otro aparato programable."))
(lang es)
(just (ML cte "Ver justificación de la enmienda al artículo 4 apartado 1 de Manuel Medina Ortega."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 99)
(ref "Article 4 paragraph 1")
(old (ML roa "A computer program as such cannot constitute a patentable invention."))
(nov (ML fno2 "Programs for computers are not inventions in the sense of patent law."))
(lang en)
(just (ML vsh "Art 52(2) EPC states that programs for computers are not inventions in the sense of patent law. It is a good idea to transfer this provision into EU law. The additional provision of Art 52(3) (exclusion only pertains to computer programs as such) should be reflected in an additional clause (amendment to Art 4.2), which also clarifies the above provision. The EU law should be clearer, not less clear, than Art 52 EPC."))
)

(am
(aut "Fausto Bertinotti")
(nr 100)
(ref "Article 4 paragraph 1")
(old (ML roa2 "A computer program as such cannot constitute a patentable invention."))
(nov (ML fno3 "Programs for computers are not inventions in the sense of patent law."))
(lang en)
(just (ML vsh2 "Art 52(2) EPC states that programs for computers are not inventions in the sense of patent law. It is a good idea to transfer this provision into EU law. The additional provision of Art 52(3) (exclusion only pertains to computer programs as such) should be reflected in an additional clause (amendment to Art 4.2), which also clarifies the above provision. The EU law should be clearer, not less clear, than Art 52 EPC."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 101)
(ref "Article 4, paragraph 1")
(old (ML eob "1 A computer program as such cannot constitute a patentable invention."))
(nov (ML WiW "1 Programs for computers are not inventions in the sense of patent law."))
(just (ML vsh3 "Art 52(2) EPC states that programs for computers are not inventions in the sense of patent law. It is a good idea to transfer this provision into EU law. The additional provision of Art 52(3) (exclusion only pertains to computer programs as such) should be reflected in an additional clause (amendment to Art 4.2), which also clarifies the above provision. The EU law should be clearer, not less clear, than Art 52 EPC."))
)

(am
(aut " Toine Manders")
(nr 102)
(ref "Article 4, paragraph 1")
(old (ML roa3 "A computer program as such cannot constitute a patentable invention."))
(nov (ML uWW "A computer program as such cannot constitute a patentable invention. Computer programs as such are effectively protected by copyright. The objective of the directive is to provide a proper legal certainty as an incentive for innovation for inventors and does not affect computer-programmers."))
(lang en)
(just (ML rdr "Although for the legal point of view it is clear that the directive will not affect the open-source, it must be clearly pointed out that the objective of the directive is to make the computer-implemented inventions patentable and not computer programmes as such. The target group it concerns are inventors and not computer programmers."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 103)
(ref "Article 4, paragraph 1")
(old (ML roa4 "A computer program as such cannot constitute a patentable invention."))
(nov (ML efc "The content of a computer program cannot constitute a patentable invention. Software shall remain under the protection of copyright according to Directive 91/250 CEE."))
(lang en)
(just (ML ith "A patent cannot claim a monopole on software, whatever carrier the software is on because this would mean that software is patentable."))
)

(am
(aut "Piia-Noora Kauppi")
(nr 104)
(ref "Article 4, paragraph 1 a (new)")
(nov (ML WlW "A computer program is a solution of a problem by calculation with the abstract entities of a generic data processing machine, such as input, output, processor, memory, storage as well as interfaces for information exchange with external systems and human users. A computer program may take various forms, e.g. a computing process, an algorithm, or a text recorded on a medium. If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims."))
(lang en)
(just (ML oih "Definition of the computer program is important for determining the patentability."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 105)
(ref "Article 4 paragraph 2")
(old (ML tWo "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable."))
(nov (ML WlW2 "A computer program is a solution of a problem by calculation with the abstract entities of a generic data processing machine, such as input, output, processor, memory, storage as well as interfaces for information exchange with external systems and human users. A computer program may take various forms, e.g. a computing process, an algorithm, or a text recorded on a medium. If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims."))
(lang en)
(just (ML t5i "This amendment proposes to replace the Council's amendment with a text which concretises the meaning of Art 52(2) and 52(3) EPC. This proposal is based on the explanation given in the original EPO Examination Guidelines of 1978 and subsequent caselaw.") (ML tdc "The Commission’s last minute amendments inserted at the Council 18 May 2004 meeting redefine a %(q:computer program as such) to referring to the %(q:source code or machine code) of an individual computer program, as defined by copyright. This is meaningless in the context of patent law. The effect of the Council's proposal can only be to make Art 52 EPC meaningless.") (ML Wga "The %(q:normal interaction between programs and computers) is about as well defined as the %(q:normal interaction between the cook and the recipe). It is a legal formula which the EPO invented in 1998 in order to circumvent Art 52 EPC. Only two years later, the EPO itself commented this formula as follows:") (orig (quot (ML ace "There is no need to consider the concept of %(q:further technical effect) in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing %(q:technical effect) from %(q:further technical effect) in the decision was because of the presence of %(q:programs for computers) in the list of exclusions under Article 52(2) EPC.")) (quot (ML dee "If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the Board of Appeals would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC."))))
)

(am
(aut "Fausto Bertinotti")
(nr 106)
(ref "Article 4 paragraph 2")
(old (ML tWo2 "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable."))
(nov (ML WlW3 "A computer program is a solution of a problem by calculation with the abstract entities of a generic data processing machine, such as input, output, processor, memory, storage as well as interfaces for information exchange with external systems and human users. A computer program may take various forms, e.g. a computing process, an algorithm, or a text recorded on a medium. If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims."))
(lang en)
(just (ML t5i2 "This amendment proposes to replace the Council's amendment with a text which concretises the meaning of Art 52(2) and 52(3) EPC. This proposal is based on the explanation given in the original EPO Examination Guidelines of 1978 and subsequent caselaw.") (ML tdc2 "The Commission’s last minute amendments inserted at the Council 18 May 2004 meeting redefine a %(q:computer program as such) to referring to the %(q:source code or machine code) of an individual computer program, as defined by copyright. This is meaningless in the context of patent law. The effect of the Council's proposal can only be to make Art 52 EPC meaningless.") (ML Wga2 "The %(q:normal interaction between programs and computers) is about as well defined as the %(q:normal interaction between the cook and the recipe). It is a legal formula which the EPO invented in 1998 in order to circumvent Art 52 EPC. Only two years later, the EPO itself commented this formula as follows:") (orig (quot (ML ace2 "There is no need to consider the concept of %(q:further technical effect) in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing %(q:technical effect) from %(q:further technical effect) in the decision was because of the presence of %(q:programs for computers) in the list of exclusions under Article 52(2) EPC.")) (quot (ML dee2 "If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the Board of Appeals would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC."))))
)

(am
(aut "Andrzej Jan Szejna")
(nr 107)
(ref art 4 par 2)
(old  (ML Wke "Nie uznaje się wynalazku realizowanego przy pomocy komputera za wnoszący wkład techniczny tylko ze względu na wykorzystanie komputera, sieci lub innego urządzenia dającego się zaprogramować. Odpowiednio, nie posiadają zdolności patentowej wynalazki obejmujące programy komputerowe, wyrażone jako kod źródłowy, kod obiektu, czy w jakiejkolwiek innej formie, które wdrażają metody prowadzenia działalności gospodarczej, metody matematyczne lub inne metody i które nie przynoszą żadnych efektów technicznych wykraczających poza normalne fizyczne interakcje pomiędzy programem a komputerem, siecią lub innym urządzeniem dającym się zaprogramować, w którym jest on stosowany."))
(nov (ML yja "Nie uznaje się wynalazku realizowanego przy pomocy komputera za wnoszący wkład techniczny tylko ze względu na wykorzystanie samego komputera, sieci lub innego urządzenia dającego się zaprogramować bez możliwości praktycznego ich zastosowania do uruchamiania i sterowania układami materialnym.  Odpowiednio, nie posiadają zdolności patentowej wynalazki obejmujące wyłącznie programy komputerowe, wyrażone jako kod źródłowy, kod obiektu, czy w jakiejkolwiek innej formie, oraz takie, które wdrażają metody prowadzenia działalności gospodarczej, a także matematyczne metody obliczeniowe, teksty zapisane na nośniku oraz same algorytmy jako takie."))
(lang pl)
(just (ML nnW "W tym punkcie zawarte są dwa stwierdzenia: pierwsze, eliminujące możliwość wykorzystania komputera lub sieci lub innego urządzenia sterującego do opracowywania programów będących przedmiotem patentowania, drugie, wykluczające programy wewnętrzne komputerowe, a także wszelkie metody matematyczne oraz różne algorytmy, a także metody prowadzenia działalności gospodarcze, jako rozwiązanie możliwe do patentowania."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 108)
(ref art 4 par 2)
(old  (ML tWo3 "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable."))
;; ??
(nov (ML tep "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, computer programs, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run, shall not be patentable."))
(lang en)
(just (ML fIt2 "The definition of inventive step supplied by the Council is not suitable, as it refers to the definition of a technical contribution, which is not itself explicitly defined, but which does, however, make reference, by means of the word ‘contribution’, to an inventive step. In order to break this vicious circle, inventive step needs to be defined in relation to itself. In order for a new computer program executed on a technical device not to be a patentable invention, the inventive step has to be evaluated solely in relation to the technical features of the patent claim."))
)

(am
(aut " Toine Manders")
(nr 109)
(ref art 4 par 2)
(old (ML tjr "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable."))
(nov (ML vtr "A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.") (spaced "2a" (ML ent "Inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.")))
(lang en)
(just (ML tpo "A technical amendment to define more clear in the text that inventions involving computer programs, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run are not patentable."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 110)
(ref art 4 par "2a" nov)
(nov (ML tri "The Member States shall ensure that data processing is not considered to be a field of technology as defined under patent law, and that innovations in the field of data processing are not considered inventions as defined under patent law."))
(lang en)
(just (ML iWt "This is EP 1st reading article 3. This ensures that software is not considered technical, which is important for compliance to TRIPS."))
)

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 111)
(ref art 4 par "2a" nov)
(nov (ML his "Member States shall ensure that data processing solutions are not considered to be patentable inventions merely because they improve efficiency in the use of resources within data processing systems."))
(lang en)
(just (ML Wnp " Nobody ever writes software without trying to optimise the use of computing resources.") (ML Wtc "This amendment makes sure that this fact does not justify the granting of a patent. This codifies both UK case law (Gale's application) and Germany’s case law (BpatG’s ruling in the Error Search case). As the German court found: if an improvement of efficiency in the use of computing ressources, such as time or data space, is deemed to be a technical contribution, then all computer-implemented business methods become patentable.") (ML WoW "This amendment corresponds to article 6 in the consolidated text of the EP’s first reading, except that " computer-implemented " was changed into " computer-aided "."))
)

(am
(aut "Fausto Bertinotti")
(nr 112)
(ref art 4 par "2a" nov)
(nov (ML his2 "Member States shall ensure that data processing solutions are not considered to be patentable inventions merely because they improve efficiency in the use of resources within data processing systems."))
(lang en)
(just (ML wnm "Nobody ever writes software without trying to optimise the use of computing resources.") (ML Wtc2 "This amendment makes sure that this fact does not justify the granting of a patent. This codifies both UK case law (Gale's application) and Germany’s case law (BpatG’s ruling in the Error Search case). As the German court found: if an improvement of efficiency in the use of computing ressources, such as time or data space, is deemed to be a technical contribution, then all computer-implemented business methods become patentable.") (ML niW2 "This amendment corresponds to article 6 in the consolidated text of the EP’s first reading, except that %(q:computer-implemented) was changed into %(q:computer-aided)."))
)


(am
(aut "Piia-Noora Kauppi")
(nr 113)
(ref "Article 4, paragraph 2 a (new)")
(nov (ML his3 "Member States shall ensure that data processing solutions are not considered to be patentable inventions merely because they improve efficiency in the use of resources within data processing systems."))
(lang en)
(just (ML lpt "Self explanatory."))
)

(am
(aut "Manuel Medina Ortega")
(nr 114)
(ref art 4 bis nov)
(nov (ML ler "Los Estados miembros garantizarán que el procesamiento de datos no se considere un ámbito de tecnología en el sentido del derecho de patentes y que las innovaciones en el ámbito del procesamiento de datos tampoco se considere invenciones en el sentido del derecho de patentes."))
(lang es)
(just (ML Dtp "Se garantiza el cumplimiento del ADPIC asegurándose que los programas informáticos no pertenezcan al ámbito de la tecnología. Obsérvese que esto no excluye a los dispositivos utilizados para el procesamiento de los datos de la patentabilidad.") (ML tlc "Un ordenador puede solamente llevar a cabo dicho procesamiento, pero crear  una nueva clase de ordenadores es un progreso en la ingeniería electrónica y no en el procesamiento de datos."))
)

(am
(aut "Manuel Medina Ortega")
(nr 115)
(ref art 4 ter nov)
(nov (ML tlW "Artículo 4 ter")
(ML Wee2 "Los Estados miembros garantizarán que las soluciones a los problemas técnicos aplicadas a través de ordenador no sean consideradas como invenciones patentables cuando solamente mejoren la eficacia en el uso de recursos en los sistemas de procesamiento de datos."))
(lang es)
(just (ML Wea "Se garantiza que hacer que un programa funcione con mayor rapidez o que use menos memoria no pueda ser utilizado como justificación para conceder una patente.") (ML aer "Esta enmienda corresponde al artículo 6 del texto consolidado en primera lectura del PE."))
)

(am
(aut "Andrzej Jan Szejna")
(nr 116)
(ref art 5 par 1)
(old (ML zmr "Państwa Członkowskie zapewnią możliwość dokonania zastrzeżenia wynalazku realizowanego przy pomocy komputera jako produktu w formie zaprogramowanego komputera, zaprogramowanej sieci komputerowej lub innych zaprogramowanych urządzeń, albo jako procesu przeprowadzanego przez taki komputer, sieć komputerową lub urządzenie poprzez wykonanie oprogramowania."))
(nov (ML Wzw "Państwa Członkowskie zapewnią możliwość zastrzeżenia wynalazku realizowanego przy pomocy komputera jako produktu w formie maszyn i urządzeń technicznych z dodatkiem komputera, sieci komputerowej lub innego urządzenia dającego się zaprogramować albo jako procesu o charakterze technicznym, uruchamianego i sterowanego za pośrednictwem sensorów, wyłączników i przełączników przez taki komputer, sieć komputerową lub inne urządzenie sterujące."))
(lang pl)
(just (ML xdn "Bardzo ważne jest, by uznać za %(q:nową jakość), która będzie miała zdolność patentową, skojarzenie maszyn i urządzeń, z dodatkiem komputera lub innych sieci lub urządzeń programowalnych, łącznie ze stosownymi programami. Za kryterium tej " nowej jakości " powinno się przyjąć nowe, dotychczas nieznane cechy użytkowe wyżej wymienionych skojarzeń - urządzenie plus komputer plus program."))
)

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 117)
(ref "Article 5 paragraph 1")
(old (ML tct " 1. Member States shall ensure that a computer-implemented invention may be claimed as a product that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software."))
       <entry>
 1. Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a programmed device, or as a technical production process.

       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 This is 1st reading article 7.1 ; it guarantees that only technical inventions can be claimed.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 118)
(ref "Art)iculo 5, apartado 1")
(old (ML ide2 " 1. Los Estados miembros garantizarán que las invenciones implementadas en ordenador puedan reivindicarse como producto, es decir, como ordenador programado, red informática programada u otro aparato programado, o como procedimiento realizado por un ordenador, red informática o aparato mediante la ejecución de un programa."))
(nov (ML adW " 1. Los Estados miembros garantizarán que las invenciones asistidas en ordenador puedan reivindicarse sólo como producto, es decir, como dispositivo programado, o como proceso de producción técnico.")
(lang es)
  <para>
Justification
  </para>

  <para>
 Un proceso llevado a cabo por un ordenador por definición corresponde a los programas informáticos que son ejecutados por dicho ordenador. Hay que garantizar que solamente las invenciones técnicas, posiblemente ejecutadas bajo control del ordenador, puedan reivindicarse, y no solamente los programas informáticos que se utilizan para su control (ni esos mismos programas informáticos cuando se ejecutan en un ordenador de mesa ordinario cuando no es utilizado para controlar dicha invención técnica).
  </para>

  <para>
<emphasis>Esta enmienda corresponde al artículo 7.1  del texto consolidado en primera lectura por el PE.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 119)
(ref "Article 5 paragraph 1")
(old (ML tWu " 1. Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software."))
(nov (ML nps " 1. Member States shall ensure that a computer-aided invention may be claimed as a product, that is as a programmed apparatus, or as a process carried out by such an apparatus.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Software in combination with generic computing equipment is still not more than software (as such).  Suggestions that software can be patentable are outside the scope of this article and should be avoided.
  </para>

  <para>
<emphasis>This amendment roughly corresponds to article 7.1 in the consolidated text of the EP’s first reading (except that ”implemented” has been replaced with ”aided”, ”device” with ”apparatus” and ”technical production” has been deleted from ”technical production process”).</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 120)
(ref "Article 5 paragraph 1")
(old (ML tWu2 " 1. Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software."))
(nov (ML nps2 " 1. Member States shall ensure that a computer-aided invention may be claimed as a product, that is as a programmed apparatus, or as a process carried out by such an apparatus.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Software in combination with generic computing equipment is still not more than software (as such).  Suggestions that software can be patentable are outside the scope of this article and should be avoided.
  </para>

  <para>
<emphasis>This amendment roughly corresponds to article 7.1 in the consolidated text of the EP’s first reading (except that ”implemented” has been replaced with ”aided”, ”device” with ”apparatus” and ”technical production” has been deleted from ”technical production process”).</emphasis>
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 121)
(ref "Article 5, paragraph 1")
(old (ML ect "1. Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software."))
(nov (ML uas "1. Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed apparatus, or as a process carried out by such an apparatus.")
(lang en)
  <para>
Justification
  </para>

  <para>
Software in combination with generic computing equipment is still not more than software (as such). Suggestions that software can be patentable are outside the scope of this article and should be avoided.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 122)
(ref "Article 5 paragraph 1 a(new)")
(old (ML _903 " "))
(nov (ML eid "1a. Member States shall ensure that the distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent.")
(lang en)
(just (ML ets " Freedom of publication, as stipulated in Art 10 ECHR, can be limited by copyright but not by patents. Patent rights are broad and unsuited for information goods. This amendment does not make any patents invalid, rather it limits the ways in which a patent owner can enforce his patents. Such a provision should be complemented by other provisions which make sure that information patents are not granted in the first place.") (ML aef "This amendment is a simplified and reduced version of article 7 paragraph 3 in the consolidated text of the EP’s first reading."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 123)
(ref "Article 5 paragraph 1 a(new)")
(old (ML xB2 " "))
(nov (ML eid2 "1a. Member States shall ensure that the distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Freedom of publication, as stipulated in Art 10 ECHR, can be limited by copyright but not by patents. Patent rights are broad and unsuited for information goods. This amendment does not make any patents invalid, rather it limits the ways in which a patent owner can enforce his patents. Such a provision should be complemented by other provisions which make sure that information patents are not granted in the first place.
  </para>

  <para>
<emphasis>This amendment is a simplified and reduced version of article 7 paragraph 3 in the consolidated text of the EP’s first reading.</emphasis>
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 124)
(ref "Article 5, paragraph 1 b (new)")
(old (ML Lj8 " "))
(nov (ML wke "1b.  Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, an well-functioning and well-documented program text shall be published as part of the patent description without any restricting licensing terms.")
(lang en)
(just (ML hne " A program listing is an excellent means of describing to a skilled person what a computer-aided process does. This amendment ensures that the obligation of disclosure is taken seriously, and that software is treated as a means of describing the invention, rather than as an invention in itself. The Commission's objection that patent law does not normally require the disclosure of a full reference implementation does not apply, because this amendment does not ask for a reference implementation but only for an accurate description.") (ML oad "This requirement makes it a little more difficult to block people from doing things you even haven't done yourself, but which are obviously possible since the computing model is perfectly defined and you always know in advance what is theoretically possible with a computer. When you publish working source code you at least offer some real knowledge on how to solve the problem, unlike when you say in the claims language that a " processor means coupled to input output means so that they compute a function such that the result of said function when output through said output means solves the problem the user wanted to solve ".") (ML qrn "Note that this amendment does not require that the source code for all programs of the patent owner which use these features be disclosed. He only has to provide a single, simple text which describes the monopolised functionality in a programming language.") (ML rWo "This amendment roughly corresponds to article 7 paragraph 5 in the consolidated text of the EP’s first reading (it’s been made more clear that only an example must be provided)."))
(am
(aut "Fausto Bertinotti")
(nr 125)
(ref "Article 5, paragraph 1 b (new)")
(old (ML Jcx " "))
(nov (ML wke2 "1b.  Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, an well-functioning and well-documented program text shall be published as part of the patent description without any restricting licensing terms.")
(lang en)
(just (ML hne2 " A program listing is an excellent means of describing to a skilled person what a computer-aided process does. This amendment ensures that the obligation of disclosure is taken seriously, and that software is treated as a means of describing the invention, rather than as an invention in itself. The Commission's objection that patent law does not normally require the disclosure of a full reference implementation does not apply, because this amendment does not ask for a reference implementation but only for an accurate description.") (ML oad2 "This requirement makes it a little more difficult to block people from doing things you even haven't done yourself, but which are obviously possible since the computing model is perfectly defined and you always know in advance what is theoretically possible with a computer. When you publish working source code you at least offer some real knowledge on how to solve the problem, unlike when you say in the claims language that a " processor means coupled to input output means so that they compute a function such that the result of said function when output through said output means solves the problem the user wanted to solve ".") (ML qrn2 "Note that this amendment does not require that the source code for all programs of the patent owner which use these features be disclosed. He only has to provide a single, simple text which describes the monopolised functionality in a programming language.") (ML rWo2 "This amendment roughly corresponds to article 7 paragraph 5 in the consolidated text of the EP’s first reading (it’s been made more clear that only an example must be provided)."))
(am
(aut "Andrzej Jan Szejna")
(nr 126)
(ref "Artykul 5, paragraph 2")
(old (ML uaW " 2. Zastrzeżenie programu komputerowego, samego lub na nośniku, jest niedozwolone, chyba, że program ten, w czasie ładowania i wykonywania na dającym się zaprogramować komputerze, dającej się zaprogramować sieci lub innym urządzeniu dającym się zaprogramować, uruchamia produkt lub proces zastrzeżony w tym samym wniosku patentowym zgodnie z ust. 1."))
(nov (ML zwo " 2. Zastrzeżenie patentowe, jako główne zastrzeżenie wynalazku, nie może dotyczyć  programu komputerowego, samego lub na nośniku. Państwa członkowskie zapewnią, by zastrzeżenia patentowe, dotyczące wynalazków, realizowanych przy pomocy komputera, odnosiły się do zmian stanu techniki. ")
(lang pl)
  <para>
Justification
  </para>

  <para>
***
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Malcolm Harbour")
(nr 127)
(ref "Article 5, paragraph 2")
(old (ML Wcm " 2. A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a programmable computer, programmable computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1."))
(nov (ML sWa " 2. A claim to a computer program product, on its own or on a carrier shall be allowed only if the invention realised by a computer program would, when loaded or run on a computer, computer network or other programmable apparatus, have an antecedent main claim in the same patent with a product or process claim as described in Article 5.1.")
(lang en)
  <para>
Justification
  </para>

  <para>
An inventive product which can be realised by a computer program must be able to be distributed by carrier
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 128)
(ref "Article 5 paragraph 2")
(old (ML Wcm2 " 2. A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a programmable computer, programmable computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1."))
(nov (ML leh " 2. A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.")
(lang en)
  <para>
Justification
  </para>

  <para>
It is contradictory to say that computer programs at the same time cannot be inventions, and saying that they nevertheless can be claimed in a patent. Additionally, the condition after the "unless" in the Council version can always be fulfilled.
  </para>

  <para>
<emphasis>The Commission purposefully did not include these so-called "program claims" in its original proposal, as allowing patent monopolies on programs on their own is hard to defend if you at the same time want to maintain that "program as such" are not patentable.</emphasis>
  </para>

  <para>
<emphasis>Getting rid of this Council amendment is one of the most basic requirements. In first reading, the EP rejected a similar amendment, and the replacement is part of an amendment which was adopted (article 7 paragraph 2 of the consolidated version).</emphasis>
  </para>

(am
(aut "Fausto Bertinotti")
(nr 129)
(ref "Article 5 paragraph 2")
(old (ML Wcm3 " 2. A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a programmable computer, programmable computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1."))
(nov (ML leh2 " 2. A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.")
(lang en)
  <para>
Justification
  </para>

  <para>
It is contradictory to say that computer programs at the same time cannot be inventions, and saying that they nevertheless can be claimed in a patent. Additionally, the condition after the "unless" in the Council version can always be fulfilled.
  </para>

  <para>
<emphasis>The Commission purposefully did not include these so-called "program claims" in its original proposal, as allowing patent monopolies on programs on their own is hard to defend if you at the same time want to maintain that "program as such" are not patentable.</emphasis>
  </para>

  <para>
<emphasis>Getting rid of this Council amendment is one of the most basic requirements. In first reading, the EP rejected a similar amendment, and the replacement is part of an amendment which was adopted (article 7 paragraph 2 of the consolidated version).</emphasis>
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 130)
(ref "Article 5, paragraph 2")
(old (ML wom "2 A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a programmable computer, programmable computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1."))
(nov (ML ara "2 A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.")
(lang en)
(just (ML lpt2 "Self-explanatory."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 131)
(ref "Article 5 paragraph 2")
(old (ML Wcm4 " 2. A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a programmable computer, programmable computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1."))
(nov (ML onl "2. A claim to a computer program, either on its own or on a carrier, shall not be allowed.")
(lang en)
  <para>
Justification
  </para>

  <para>
 This is 1st reading article 7.1 ; it specifies that a computer program cannot be claimed either on its own or on any carrier : that would be tantamount to allowing software patentability on the basis of considering that software can possess patentable technical features. The programme claims are considered as patents on software as such, therefore these should not be included in this Directive.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 132)
(ref "Articulo 5, apartado 2")
(old (ML Wdn " 2. Una reivindicación de un programa informático, por sí solo o en un soporte, no se considerará como admitida a menos que dicho programa, si estuviera cargado y ejecutado en un ordenador programable, una red informática programable u otro aparato programable, ejecutara un producto o un procedimiento reivindicado en la misma solicitud de patente de conformidad con el apartado 1."))
(nov (ML cri " 2. Una reivindicación de un programa informático, por sí solo o en un soporte, no se considerará como admitida.")
(lang es)
(just (ML ect2 " Es contradictorio decir que los programas informáticos no pueden al mismo tiempo ser invenciones y alegar, sin embargo, que pueden reivindicarse en una patente. Además, la condición después de " a menos " puede cumplirse siempre.") (ML ies "La Comisión decididamente excluyó las llamadas " reivindicaciones de programas " en su propuesta original, ya que permitir monopolios de patentes en programas individuales es complicado defender si se quiere al mismo tiempo mantener que programas como tales no son patentables.") (ML Wir2 "Eliminar esta enmienda del Consejo es una de las exigencias esenciales. En primera lectura, el PE rechazó una enmienda similar, y el nuevo texto forma parte de una enmienda que fue adoptada (artículo 7 párrafo 2 de la versión consolidada)."))
(am
(aut "Klaus-Heiner Lehne")
(nr 133)
(ref "Article 5, paragraph 2")
(old (ML teW "(2) A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into force a product or process claimed in the same patent application in accordance with paragraph 1. "))
(nov (ML Wce "(2) A claim to a computer program, either on its own or on a carrier, shall not be allowed unless that program would, when loaded and executed in a computer, programmed computer network or other programmable apparatus, put into effect a product or process claimed for the invention in accordance with paragraph 1.")
(lang en)
  <para>
Justification
  </para>

  <para>
It is necessary to make clear that the program on its own cannot be the object of claims but as part of the computer-implemented invention which is realized by its execution. Only in this context the claims raised in accordance with paragraph 1 extend to the program.
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 134)
(ref "Artykul 5, paragraph 2 a (new)")
(old (ML 10O " "))
(nov (ML woe " 2a. Państwa członkowskie zapewnią, ze wytwarzanie, przetwarzanie oraz upowszechniania i publikowanie informacji w jakiejkolwiek formie nie może stanowić podstawy do pośredniego lub bezpośredniego naruszenia praw patentowych nawet, gdy do tego celu stosuje się opatentowane urządzenia techniczne. ")
(lang pl)
  <para>
Justification
  </para>

  <para>
 Ograniczenia w wymianie informacji mogą wyhamować rozwój nauki i badań podstawowych.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 135)
(ref "Article 5 paragraph 2 a (new)")
(old (ML cwh " "))
(nov (ML tWn "2a. Member States shall ensure that the production, handling, processing, distribution and publication of information in any form can never constitute a patent infringement, either direct or indirect, even if a technical device is used for this purpose")
(lang en)
  <para>
Justification
  </para>

  <para>
 This is 1st reading article 7.3 ; it aims at ensuring freedom of information.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 136)
(ref "Article 5, paragraph 2 a (new)")
(old (ML 2Iw " "))
(nov (ML nal "(2a) Where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.")
(lang en)
  <para>
Justification
  </para>

  <para>
Only when software elements are used in the context of realising the computer-implemented invention the claims raised in accordance with paragraph 1 extend to the software and infringements may take place. This should not only be mentioned in Recital 17 but also in Article 5.
  </para>

(am
(aut "Malcolm Harbour")
(nr 137)
(ref "Article 5, paragraph 2 a (new)")
(old (ML bF9 " "))
(nov (ML rtW "2a. A claim described in Article 5.2 only gives protection for the use which is described in the respective patent.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Complements the clearer wording of 5.2.
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 138)
(ref "Artykul 5, paragraph 2 b (new)")
(old (ML cXL " "))
(nov (ML Dtb "2b.Państwa członkowskie zapewnią, aby korzystanie z programów komputerowych dla celów, które nie są objęte zakresem zastrzeżenia patentowego, nie mogło stanowić pośredniego lub bezpośredniego naruszania praw patentowych.")
(lang pl)
  <para>
Justification
  </para>

  <para>
 ***
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 139)
(ref "Article 5 paragraph 2 b (new)")
(old (ML wym " "))
(nov (ML nWr " 2b. Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Only inventions can be patented.
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 140)
(ref "Artykul 5, paragraph 2 c (new)")
(old (ML 26R " "))
(nov (ML poz " 2c. Państwa członkowskie zapewnią, że jeśli zastrzeżenia patentowe dotyczą cech o charakterze technicznym, które zakładają zastosowanie programów komputerowych, by dobre udokumentowanie zastosowania takich programów wymagało ich opublikowania jako części opisu patentowego oraz w formie konkretnych przykładów patentowych, nie zaś w postaci oddzielnych, głównych czy też dodatkowych zastrzeżeń patentowych.")
(lang pl)
  <para>
Justification
  </para>

  <para>
 Chodzi o zapewnienie autorom programów komputerowych z jednej  strony praw autorskich z drugiej zaś strony o wyeliminowanie pseudowynalazków - poprzez konieczność ujawnienia konkretnych programów komputerowych  w przykładach patentowych.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>


(am
(aut "Piia-Noora Kauppi")
(nr 141)
(ref "Article 5 a (new)")
(old (ML mY1 " "))
(nov (ML rcW "Article 5a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Member States shall ensure that the distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent.
       </entry>

(lang en)
(just (ML Wsi "Freedom of publication, as stipulated in Art 10 ECHR, can be limited by copyright but not by patents. Patent rights are broad and unsuited for information goods. This amendment does not make any patents invalid, rather it limits the ways in which a patent owner can enforce his patents. Such a provision should be complemented by other provisions which make sure that information patents are not granted in the first place.") (ML aef2 "This amendment is a simplified and reduced version of article 7 paragraph 3 in the consolidated text of the EP’s first reading."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 142)
(ref "Article 5 b (new)")
(old (ML xmv " "))
(nov (ML rcW2 "Article 5b")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Limitation of the effects of a patent
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
The rights conferred by patents for inventions within the scope of this Directive shall not extend to:
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
a) acts done privately and for non-commercial  purposes,
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
b) acts done for experimental purposes relating to the subject-matter of the patented invention, including non-commercial academic and research use.
       </entry>

(lang en)
(just (ML WtL "This proposal establishes that certain acts can be carried out safely with the peace of mind that they do not constitute patent infringement.  In particular this applies to acts done privately or for experimental purposes, as long as they are not done commercially.  Likewise academic and research use is immune if non-commercial."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 143)
(ref "Artículo 5 bis (nuevo)")
(old (ML Fir " "))
(nov (ML tlW2 "Artículo 5 bis")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Los Estados miembros garantizarán que la distribución y publicación de información, cualquiera que sea la forma, nunca constituya una violación directa o indirecta de una patente.
       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 Los términos de "distribución y "publicación" tienen más en cuenta los casos de demandas de patentes para métodos comerciales (de hecho el tratamiento de la información) que existen en los Estados Unidos y que no deberían existir en la Unión Europea.
  </para>

  <para>
<emphasis>Esta enmienda corresponde a una versión más restrictiva del artículo 7 párrafo 3 en el texto consolidado de la primera lectura del PE con el objetivo de alcanzar un compromiso con el Consejo.</emphasis>
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 144)
(ref "Artículo 5 ter (nuevo)")
(old (ML kSY " "))
(nov (ML tlW3 "Artículo 5 ter")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Los Estados miembros garantizarán que siempre que una reclamación de patentes indique características que impliquen el uso de un programa de ordenador, un buen funcionamiento y bien documentada referencia de aplicación de tal programa se publicará como parte de la descripción sin ninguna restricción a sus condiciones de licencia.
       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 Estas enmiendas, a pesar de lo que se pueda pensar, no sirven para promover los programas de red abierta sino para asegurar que la obligación de publicidad, que es inherente al sistema de patentes, se tome en serio y para que un programa informático esté, como cualquier otro objeto de información, en el lado revelado de la patente en vez de en su lado de exclusión /monopolio.
  </para>

  <para>
<emphasis>Esto hace un poco más difícil impedir que alguien haga cosas que ni siquiera ha hecho él mismo, pero que son obviamente posibles desde que el modelo de computación se definió perfectamente y se sabe siempre lo que se puede hacer con un ordenador.</emphasis>
  </para>

  <para>
<emphasis>  Obsérvese que esta enmienda no requiere que el código de fuentes para todos los programas del titular de la que usa estas características se revelen. Solamente tiene que proporcionar una única y simple aplicación de la funcionalidad que está monopolizando.</emphasis>
  </para>

  <para>
<emphasis>Esta enmienda corresponde al artículo 7 párrafo 5 del texto consolidado de la primera lectura del PE.</emphasis>
  </para>

(am
(aut "Michel Rocard")
(nr 145)
(ref "Article 6, paragraphe 1 bis (nouveau)")
(old (ML ts8 " "))
       <entry>
 1 bis.Les Etats membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire afin d'assurer l'interopérabilité entre deux systèmes ou réseaux informatiques différents, cela dans le cas où il n'existe pas d'alternative technique non brevetée aussi efficace permettant d'obtenir l'interopérabilité entre les deux systèmes, ni ce recours, ni le developpement, l'expérimentation, la fabrication, la vente, la cession de licences, ou l'importation de programmes mettant
en oeuvre cette technique brevetée ne soient considérés comme une
contrefaçon de brevet.
       </entry>

(lang fr)
(just (ML pt1 " La préservation de l'interopérabilité suppose la capacité, non seulement de pouvoir le cas échéant effectuer des opérations de rétro-ingéniérie pour déterminer les caractéristiques des protocoles et interfaces de communication avec lesquelles il s'agira de communiquer, mais également de pouvoir réaliser et commercialiser effectivement de tels produits interopérables.") (ML eut "L'article 6.2, autorisé par l'article 30 de l'accord ADPIC, est nécessaire pour empêcher de possibles graves distorsions de la concurrence sur le marché intérieur du fait que la mise sur le marché de produits interopérables constituerait toujours une contrefaçon des revendications d'un brevet."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 146)
(ref "Article 6 second subparagraph (new)")
(old (ML fll " "))
(nov (ML cWe " Member States shall ensure that, wherever the use of a patented technique is necessary in order to ensure interoperability between two different computer systems or networks, in the sense that no equally efficient and equally effective alternative non-patented means of achieving such interoperability between them is available, such use is not considered to be a patent infringement, nor is the development, testing, making, offering for sale or license, or importation of programs making such use of a patented technique to be considered a patent infringement.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Article 6 of the Council only refers to the exemption provided for by the copyright directive; this means that a developer is allowed to use reverse engineering to make his software interoperable with that of a competitor but afterwards he needs to be able to distribute, sell and use the interoperable software he developed.
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 147)
(ref "Artykul 6, second subparagraph (new)")
(old (ML Uqk " "))
(nov (ML udW2 " Państwa członkowskie zapewnią, że ilekroć użycie opatentowanej techniki jest niezbędne jedynie dla zapewnienia konwersji standardów używanych w dwóch różnych systemach przetwarzania danych tak by zapewnić komunikowanie się i wymianę danych między nimi, takie użycie nie jest uważane za naruszenie patentu.")
(lang pl)
(just (ML Whh " ****"))
(am
(aut "Toine Manders")
(nr 148)
(ref "Article 6, paragraph 2 (new)")
(old (ML bqF " "))
(nov (ML emW "2. a) Member States shall ensure that a patented computer-implemented invention that is essential for enabling interoperability between programmable devices can be used on reasonable and non-discriminatory terms and conditions by third parties to enable interoperability between programmable devices. ")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
b) If a voluntary license on reasonable commercial terms and conditions cannot be obtained within a reasonable period of time, Member States shall apply Article 31 TRIPS to such a patented invention.
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
c) It shall not be deemed reasonable if a potential licensee is forced to license its own technology that is essential for interoperability without any compensation or to agree to abstain from enforcing his own rights on such technology.

       </entry>

(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis> A compulsory license subject to Art. 31 TRIPs ensures access to interoperability technology, an  incentive to innovate because the patentee receives an adequate remuneration, an incentive to early publication of new technology because patenting still makes sense and an adequate remuneration for the patentee thereby balancing the public interests above with the patentee's private interest.</emphasis>
  </para>

  <para>
<emphasis>Paragraph 3 ensures that a dominant player cannot force somebody else to waive his patent rights</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Othmar Karas")
(nr 149)
(ref "Article 6 a (new)")
(old (ML 4t3 " "))
(nov (ML rcW3 "Article 6a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Member States shall ensure that, wherever the use of a patented technique is necessary in order to ensure interoperability between two different computer systems or networks, in the sense that no equally efficient and equally effective alternative non-patented means of achieving such interoperability between them is available, such use is not considered to be a patent infringement, nor is the development, testing, making, offering for sale or license, or importation of programs making such use of a patented technique to be considered a patent infringement.
       </entry>

(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>It is necessary to have the interoperability exception in articles.</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 150)
(ref "Article 6 a (new)")
(old (ML B1G " "))
(nov (ML rcW4 "Article 6a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
1. Member States shall ensure that the non-commercial use of an interface for the sole purpose of ensuring interoperability with an otherwise non-infringing product, system, network, or service does not constitute a patent infringement.
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
2. Any person requesting a licence for such use on a commercial basis may require the patent owner to grant a licence to the patented interface for such use on reasonable and non-discriminatory terms and on adequate conditions.
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
3. This article applies without prejudice to the TRIPs agreement.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
This amendment aims at keeping  interoperability as  one of the cornerstones of the information and communications technology. Electronic products need to be able to communicate and interoperate.
  </para>

  <para>
<emphasis>To strike the proper balance between the rights of the patent owner to enjoy the full benefits of the patent, the third party´s interest to develop interoperating products, as well as the public interest to prevent unjustified monopolies on standards.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 151)
(ref "Article 6 a (new)")
(old (ML OET " "))
(nov (ML rcW5 "Article 6a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Member States shall ensure that, where the use of an interface, which is protected by a patent for a computer-implemented invention, is indispensable for the sole purpose of ensuring interoperability, such as to ensure conversion of the conventions used in two different computer systems or network in order to allow communication and exchange of data content between them, this use of the interface is not considered to be a patent infringement.
       </entry>

(lang en)
(just (ML tla "Where an interface protected by a patent is necessary to allow interoperability, the use of the interface cannot be regarded as a matter of patent infringement."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 152)
(ref "Article 6 a (new)")
(old (ML BVV " "))
(nov (ML rcW6 "Article 6a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Member States shall ensure that wherever an interface which is protected by a patent for a computer-implemented invention is indispensable for the sole purpose of ensuring interoperability such as to ensure conversion of the conventions used in two different computer systems or network in order to allow communication and exchange of data content between them, the use of the interface is not considered to be a patent infringement.
       </entry>

(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>Where an interface protected by a patent is necessary to allow interoperability, the use of the interface cannot be regarded as a matter of patent infringement.</emphasis>&lt;/OptDelPrev&gt;
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 153)
(ref "Article 6 a (new)")
(old (ML X9n " "))
(nov (ML re6 "Arikel 6 a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Die Mitgliedstaaten stellen sicher, dass in allen Fällen, in denen der Einsatz einer patentierten Technik für die Konvertierung der in mindestens zwei verschiedenen Computersystemen verwendeten Konventionen unverzichtbar ist, um die Kommunikation und den Austausch von Dateninhalten zwischen den Computersystemen zu ermöglichen, einem Lizenzsucher dieser patentierten Technik ein Anspruch auf Einräumung einer Lizenz zu angemessenen Bedingungen (Zwangslizenz) gegenüber dem Rechteinhaber zusteht. Die Regelungen des TRIPS-Übereinkommens bleiben unberührt.
       </entry>

(lang de)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>************</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>


(am
(aut "Piia-Noora Kauppi")
(nr 154)
(ref "Article 6 a (new)")
(old (ML lrc " "))
(nov (ML rcW7 "Article 6a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring interoperability of two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement. Member States must ensure that the court may require a patent owner to grant a licence for such use having regard to the public interest in permitting access to the patented technique, provided that a licence is not otherwise available for such use on reasonable and non-discriminatory terms and conditions.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
Interoperability must be ensured in articles and it should not be considered as patent infringement.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Arlene McCarthy")
(nr 155)
(ref "Article 6 a (new)")
(old (ML 2b6 ""))
(nov (ML 6Wa "6 a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Interoperability exception
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
The developing, testing, making, using, offering for sale or license, selling, licensing, or importing of a patented computer-implemented invention shall not require the authorisation of the patent owner, to the extent that use of the patented computer-implemented invention is indispensable to achieve the interoperability of the computer program with one or more other computer programs, in the sense that no equally efficient and equally effective alternative non-patented means of achieving such interoperability between them is available.
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
The exceptions set out in this Article  may not be interpreted in such a way as to allow its application to be used in a manner which unreasonably prejudices the right holders legitimate interests or unreasonably conflicts with a normal exploitation of the computer implemented invention, taking account of the legitimate interests of third party software developers to achieve interoperability and of end-users  to have access to interoperable programs systems and networks and the need to use data on different computer systems.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 Ensures compatibility with the software copyright directive and ensures users have access to interoperable programme systems and networks.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>


(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 156)
(ref "Article 6 a (new)")
(old (ML lfo " "))
(nov (ML rcW8 "Article 6 a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
 Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
       </entry>

(lang en)
(just (ML Wno " Interoperability of data processing systems (e.g. computers) lies at the foundation of the information economy and allows for fair competition by all players large and small.") (ML pou "Article 6 of the Council only refers to the exemption provided for by the Copyright directive. This means that a software developer is allowed to find out how to make his data processing system interoperable with that of a competitor, but afterwards he cannot necessarily use his gained knowledge, since that could be covered by patents.") (ML tsW "This amendment makes sure that patents also cannot be used to prevent interoperability. It was passed in an almost identical form by ITRE and JURI prior to the first reading (”data processing systems” read " computer systems or networks "). In first reading, a more sweeping version of this amendment was passed, which appeared as Article 9 in the consolidated version.") (ML eWe "The expression ”for the sole purpose” reverts to the spirit of the original ITRE/JURI version of the interoperability exemption (which is more limited), which was also supported by Luxembourg and several others in the Council (but didn’t make it)."))
(am
(aut "Fausto Bertinotti")
(nr 157)
(ref "Article 6 a (new)")
(old (ML XXF " "))
(nov (ML rcW9 "Article 6 a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
 Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
       </entry>

(lang en)
(just (ML Wno2 " Interoperability of data processing systems (e.g. computers) lies at the foundation of the information economy and allows for fair competition by all players large and small.") (ML pou2 "Article 6 of the Council only refers to the exemption provided for by the Copyright directive. This means that a software developer is allowed to find out how to make his data processing system interoperable with that of a competitor, but afterwards he cannot necessarily use his gained knowledge, since that could be covered by patents.") (ML tsW2 "This amendment makes sure that patents also cannot be used to prevent interoperability. It was passed in an almost identical form by ITRE and JURI prior to the first reading (”data processing systems” read " computer systems or networks "). In first reading, a more sweeping version of this amendment was passed, which appeared as Article 9 in the consolidated version.") (ML eWe2 "The expression ”for the sole purpose” reverts to the spirit of the original ITRE/JURI version of the interoperability exemption (which is more limited), which was also supported by Luxembourg and several others in the Council (but didn’t make it)."))
(am
(aut "Giuseppe Gargani")
  <para>
Emendamento &lt;NumAm&gt;158&lt;/NumAm&gt;
  </para>

(ref "Articolo 6 bis (nuovo)")
(old (ML 1I9 ""))
       <entry>
Articolo 6 bis
Pubblicità
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
I contenuti del contratto di brevetto dovranno essere condivisi attraverso adeguati strumenti di pubblicità.
       </entry>

(lang it)
  <para>
<emphasis>Motivazione</emphasis>
  </para>

  <para>
Il riconoscimento della tutela in cambio della condivisione della conoscenza, principio fondante del “contratto” di brevetto, per essere realizzato necessita di strumenti adeguati di pubblicità/condivisione, che nel caso del software oggi non esistono e non sono previsti nella proposta di Direttiva. Ciò tenderà inevitabilmente a svantaggiare le PMI del settore e a creare un ulteriore ampliamento del contenzioso.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 159)
(ref "Artículo 6 bis (nuevo)")
(old (ML cwb " "))
(nov (ML tlW4 "Artículo 6 bis")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
Los Estados miembros garantizarán que, donde quiera que el uso de una técnica patentada sea necesaria, con el único propósito de asegurar la conversión de los convenios utilizados en dos sistemas diferentes de procesamiento de datos a fin de permitir la comunicación y el intercambio entre ellas del contenido de sus datos, tal uso no sea considerado una violación de patente.
       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 La interoperabilidad de los sistemas de procesamiento de datos (como, por ejemplo, los ordenadores) subyace a la economía de la información y permite la competencia leal por todos los operadores grandes y pequeños.
  </para>

  <para>
<emphasis>El artículo 6 del texto del  Consejo solamente hace referencia a la exención prevista por la directiva de los derechos reservados. Esto significa que se permite a un creador de programas informáticos descubrir cómo hacer interoperable su sistema de procesamiento de datos con el de un competidor, pero no puede más tarde utilizar el conocimiento adquirido, puesto que éste podría estar cubierto por patentes.</emphasis>
  </para>

  <para>
<emphasis>Esta enmienda asegura que las patentes tampoco puedan ser utilizadas para impedir la interoperabilidad. Esto se  aprobó de una forma casi idéntica por ITRE y JURI antes de la primera lectura(la segunda parte subrayada decía " sistemas de ordenador o redes"). En primera lectura, una versión más liberal de esta enmienda fue aprobada y aparece en el  artículo 9 en la versión consolidada.</emphasis>
  </para>

  <para>
<emphasis>La mención "el único propósito" hace referencia al espíritu de la versión original ITRE/JURI de esta enmienda.</emphasis>
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 160)
(ref "Artículo 6 ter (nuevo)")
(old (ML PpW ""))
(nov (ML tlW5 "Artículo 6 ter")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
1.  El  desarrollo,  examen,  creación, utilización, oferta para venta o licencia  o  importación  de  un  programa  de  ordenador que incorpore una invención  aplicada a través de ordenador no requerirá la autorización del dueño de la patente en caso de que:
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
 (a)  la invención implantada en ordenador sea indispensable para conseguir la  interoperabilidad  de  dicho  programa  del computador con otro u otros programas  de  ordenador   y siempre que no esté disponible una alternativa igualmente  eficiente  e  igualmente efectiva no patentada de conseguir tal interoperabilidad entre dichos programas;
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
(b) el  programa  de  ordenador  utilice  la  invención implementada en el ordenador para conseguir tal interoperabilidad.
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
2. Para lo relativo a este artículo “interoperabilidad” se define como la capacidad de un programa de ordenador para comunicar e intercambiar información con otro programa de ordenador y utilizar mutuamente la información intercambiada, incluyendo la capacidad  de usar, convertir o intercambiar formatos de ficheros, protocolos, esquemas o información de interconexión o convenciones, de forma que permita a este programa de ordenador trabajar con otro programa de ordenador y con los usuarios en todas las formas en que esta previsto su funcionamiento.
       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 Esta enmienda intenta resolver el problema que puede presentarse cuando sea necesario el acceso al sistema lógico de interconexión de una invención aplicada a través de ordenador  para permitir la interoperabilidad.
  </para>

  <para>
<emphasis>Debe garantizarse el acceso cuando el uso de la invención aplicada a través de ordenador es indispensable para la interoperabilidad pero solo si no hay otra alternativa disponible. En caso de interconexión patentada es necesario conseguir un equilibrio entre el derecho del dueño de la patente a disfrutar del beneficio completo de la patente y los intereses de terceros  de utilizar la invención para producir productos interoperables.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Francesco Enrico Speroni")
(nr 161)
(ref "Articulo 7")
(old  La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, compreso il commercio elettronico.

       </entry>

       <entry>
 La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, in particolare sulle Piccole e Medie Imprese, compreso il commercio elettronico.
       </entry>

(lang it)
  <para>
Justification
  </para>

  <para>
 Visto e considerato che l’economia europea si basa, in maniera particolare, sulla rete di piccole e medie imprese, che fanno della qualità dei propri prodotti un vantaggio competitivo, e che le stesse potrebbero subire negativamente l’attuazione della direttiva in oggetto, appare corretto intervenire per controllare i possibili effetti sfavorevoli che si avranno nel tessuto economico e produttivo degli Stati membri.
  </para>

(am
(aut " Katalin Lévai")
(nr 162)
(ref "Article 7")
(old (ML tiW " The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, on Community businesses, especially small and medium-sized enterprises, on the open-source community and on electronic commerce "))
(nov (ML rjW " The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, on Community businesses, especially small and medium-sized enterprises, on the open-source community and on electronic commerce, in particular from the aspect of employment in small and medium-sized enterprises.")
(lang en)
  <para>
Justification
  </para>

  <para>
 The Commission has to monitor the impact of computer-implemented inventions not only on the aspect of innovation and competition but from the aspect of employment, especially in small and medium-sized enterprises which could be affected negatively, and which take a very important part in the employment situation of the EU, in connection with one of the EU's main priorities, the Lisbon Strategy.
  </para>

(am
(aut "Alexander Nuno Alvaro, Diana Wallis, Toine Manders, Janelly Fourtou")
(nr 163)
(ref "Article 7 a (new)")
(old (ML AJD " "))
(nov (ML lnW " 1.  To assist in the monitoring obligation set forth in Article 7 of this Directive, a Committee on Technological Innovation in the Small- and Medium-sized Enterprise Sector, hereinafter referred to as “the Committee”, shall hereby be established.")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
2.  The Committee shall in particular:
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
a) examine the impact of patents for computer-implemented inventions on small- and medium-sized enterprises and highlight any difficulties;
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
b) monitor participation of small- and medium-sized enterprises in the patent system, with particular regard to patents for computer-implemented inventions, and  consider and recommend any legislative or other EU-level initiatives related thereto; and
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
c) facilitate the exchange of information with regard to relevant developments in the area of patents for computer-implemented inventions that might affect the interests of small- and medium-sized enterprises.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 This amendment relates to Article 10 (Monitoring) adopted by the European Parliament during First Reading.
  </para>

  <para>
<emphasis>Currently, SMEs participate actively in Europe’s CII patents system.  Indeed, SMEs represent the majority of applicants for CII patents.  To ensure ongoing and active participation by SMEs—and to provide opportunities to enhance their involvement—this amendment proposes the creation of a committee focused on SME-related issues, with a mandate to recommend necessary reforms.</emphasis>
  </para>

(am
(aut "József Szájer")
  <para>
Amendment &lt;NumAm&gt;164&lt;/NumAm&gt;
  </para>

(ref "Article 7 bis (new)")
(old (ML dkX ""))
(nov (ML tlt "The Commission shall conduct a feasibility study looking to the establishment of a Fund for small and medium-sized enterprises to provide financial, technical and administrative support to small and medium-sized enterprises dealing with issues related to the patentability of computer-implemented inventions.")
(lang en)
(just (ML Wat " This amendment proposes that the European Commission studies the possibility of an “SME Fund” to assist SMEs in fully participating in, and benefiting from, the computer-implemented invention patent regime."))

(am
(aut "Klaus-Heiner Lehne")
(nr 165)
(ref "Article 8, introductory sentence")
(old (ML oei "The Commission shall report to the European Parliament and the Council by*...... on:"))
(nov (ML oei2 "The Commission shall report to the European Parliament and the Council by*......  on:")
      </row>

      <row>

       <entry>
_____________________
5 years after the date of entry into force of this Directive
       </entry>

       <entry>
_______________________
3 years after the date of entry into force of this Directive
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
It is necessary to set a clear deadline for the Commission report, but also for the first review of the Directive in accordance with article 9. The timeframe of 5 years should be split into two so that the Commiccion effectively reports to the European Parliament and the Council by three years and reviews the Directive by five years after entry into force.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>


(am
(aut "József Szájer")
  <para>
Amendment &lt;NumAm&gt;166&lt;/NumAm&gt;
  </para>

(ref "Article 8, phrase introductive")
(old (ML iou "The Commission shall report to the European Parliament and the Council by*... on:"))
(nov (ML rWt3 "The Commission shall report to the European Parliament and the Council within three years from the date specified in Article 9(1) at the latest on:")
      </row>

      <row>

       <entry>
* 5 years after the date of entry into force of this Directive.
       </entry>

       <entry>

       </entry>

(lang en)
  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
<emphasis>The foreseen possibility of monitoring and reporting on the effects of the directive seems to be absolutely ineffective on the software market. To the three years of monitoring, in fact, two additional years have to be added for the transposition in the Member states, and one minimum year necessary for a revision of the act. This timetable seems to be incompatible with the dynamicity of this market.</emphasis>
  </para>

  <para>
&lt;/Amend&gt;
  </para>

(am
(aut "Alexander Nuno Alvaro, Diana Wallis, Toine Manders, Janelly Fourtou ")
(nr 167)
(ref "Article 8, point (a) a (new)")
(old (ML lYf " "))
(nov (ML ire "(a a) participation by small- and medium-sized enterprises  in the patent system for computer-implemented inventions.  Such report shall include data, to the extent available, regarding applicants for and recipients of patents for computer-implemented inventions;")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>This amendment relates to Article 10 (Monitoring) adopted by the European Parliament during First Reading.</emphasis>
  </para>

  <para>
<emphasis>Existing statistics demonstrate fairly broad participation in the CII patents process by SMEs.  However, there is consensus among all interested parties that additional and more comprehensive statistical data on CII patents would be welcomed.  The above amendment would ensure that such data is compiled.</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Giuseppe Gargani")
  <para>
Emendamento &lt;NumAm&gt;168&lt;/NumAmB&gt;
  </para>

(ref "Article 8, point a a) (new)")
(old (ML 9Ik " "))
(nov (ML ire2 "(a a) participation by small- and medium-sized enterprises  in the patent system for computer-implemented inventions.  Such report shall include data, to the extent available, regarding applicants for and recipients of patents for computer-implemented inventions;")
(lang en)
  <para>
Justification
  </para>

  <para>
 This amendment relates to Article 10 (Monitoring) adopted by the European Parliament during First Reading.
  </para>

  <para>
<emphasis>Existing statistics demonstrate fairly broad participation in the CII patents process by SMEs.  However, there is consensus among all interested parties that additional and more comprehensive statistical data on CII patents would be welcomed.  The above amendment would ensure that such data is compiled</emphasis>
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 169)
(ref "Article 8, point (b)")
(old (ML trn "(b) whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims are adequate, and whether it would be desirable and legally possible having regard to the Community's international obligations to make modifications to such rules;"))
(nov (ML nyh "(b) whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims are adequate; ")
(lang en)
(just (ML rot "Last part of the Common position text is not necessary."))
(am
(aut "Piia-Noora Kauppi")
(nr 170)
(ref "Article 8, point f)")
(old (ML rrE "(f) the aspects in respect of which it may be necessary to prepare for a diplomatic conference to revise the European Patent Convention;"))
(nov (ML etW "deleted  ")
(lang en)
(just (ML nit "Not necessary in this article."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 171)
(ref "Article 8, point f)")
(old (ML eeu "f) the aspects in respect of which it may be necessary to prepare for a diplomatic conference to revise the European Patent Convention; "))
(nov (ML eee2 "deleted")
(lang en)
(just (ML cla " The aim of the present Directive is not to modify the European Patent Convention neither to legalize patents on software, therefore there is no need to envisage a modification of this Convention."))
(am
(aut "Klaus-Heiner Lehne")
(nr 172)
(ref "Article 8, point (g) a (new)")
(old (ML NEp " "))
(nov (ML pno "(ga) developments in the interpretation of the terms ”technical contribution” and " inventive step " by patent offices and patent courts in the light of the future evolution of technology.")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>Parliament and the Council should be informed about the practice of granting patents under this Directive. Special attention should be given to the interpretation of the most relevant legal definintions.</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 173)
(ref "Article 8, point (g) a (new)")
(old (ML qN2 " "))
(nov (ML uiy "(ga) whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate; ")
(lang en)
  <para>
Justification
  </para>

  <para>
Self explanatory<emphasis>.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "József Szájer")
  <para>
Amendment &lt;NumAm&gt;174&lt;/NumAm&gt;
  </para>

(ref "Article 8, letter (g a) new")
(old (ML ryB ""))
(nov (ML ihm "(ga) the feasibility study looking to the establishment of a Fund for small and medium-sized enterprises.")
(lang en)
(just (ML Wat2 " This amendment proposes that the European Commission studies the possibility of an “SME Fund” to assist SMEs in fully participating in, and benefiting from, the computer-implemented invention patent regime."))
  <para>
&lt;/Amend&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 175)
(ref "Article 8, point (g) b (new)")
(old (ML yTy " "))
(nov (ML eii "(gb) Whether difficulties have been experienced arising from the grant of patents for computer-implemented inventions which do not comply with the statutory requirements for patentability both in terms of whether the invention")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
(1) involves an inventive step and
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
(2) makes a technical contribution
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
in accordance with Article 4.1 above, and as such should not have legitimately been granted.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>This amendment addresses the concerns that have been expressed about the grant of trivial, or undeserving, patents.  It provides a new initiative for the Commission to report to the European Parliament and the Council on whether difficulties have been found in practice caused by patents that should not have legitimately been granted.   This will encourage the European Patent Office and national Patent Offices to maintain the highest standards for examining patent applications, thus minimising the risk of undeserving patents being granted.</emphasis>
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 176)
(ref "Article 8, point (g) b (new)")
(old (ML lAG " "))
(nov (ML Wdo "(gb) whether this Directive has performed the desired effects in terms of harmonisation and clarification of the legal rules governing the patentability of computer-implemented inventions. ")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>To provide an assessment whether the aims leading to the adoption of this Directive have been achieved.</emphasis>&lt;/OptDelPrev&gt;
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 177)
(ref "Article 8, point (g) c (new)")
(old (ML 50X " "))
(nov (ML WpW2 "(gc) the developments of the world-wide patent systems in the area of computer-implemented inventions in terms of the aspects mentioned in this article (a to d and f to gb).")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>The evolution of the patent systems in other major jurisdictions, especially the possibility to have a world-wide patent system, should be closely monitored.</emphasis>&lt;/OptDelPrev&gt;
  </para>

(am
(aut "Toine Manders")
(nr 178)
(ref "Article 8, second subparagraph (new)")
(old (ML wNW " "))
(nov (ML WWt " The Commission shall come forward within a year with a proposal for an effective European Community Patent there by allowing a democratic control by the European Parliament on the European Patent Office and the European Patent Convention.")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis> With a view to legal certainty and reaching the Lisbon objectives it is desirable that there is one single patent system across the European Union.</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>



(am
(aut "Piia-Noora Kauppi")
(nr 179)
(ref "Article 8 a (new)")
(old (ML XkL " "))
(nov (ML rcW10 "Article 8a")
      </row>

      <row>

       <entry>

       </entry>

       <entry>
1. Member States shall ensure that its representatives in the Administrative Council of the European Patent Organisation take such measures within their authority to ensure that the European Patent Office only grants European patents when the requirements of the European Patent Convention have been met, in particular with respect to  inventive step and technical contribution as defined in Article 2(b).
       </entry>

      </row>

      <row>

       <entry>

       </entry>

       <entry>
2. The Council shall provide a yearly report to the European Parliament on the activities of representatives of Member States that are Contracting States to the European Patent Convention in the Administrative Council of the European Patent Organisation, and the progress that has been made to achieving the objectives set out in Article 8A.1 above.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
This amendment recognises that the Member States are also Contracting States of the European Patent Convention and that Member States have some influence the practice of the European Patent Office, specifically with respect to maintaining high standards of examining patent applications in particular with respect to inventive step and “technical contribution” as defined in this directive.
  </para>

  <para>
<emphasis>Furthermore, this amendment requires Member States (in Council) to report to the European Parliament each year on what they have actually done to influence the EPO in this regard and on the progress that has been made towards the goal of minimising the grant of undeserving patents.</emphasis>
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 180)
(ref "Article 9")
(old (ML Wir3 "In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall review the impact of this Directive and, where necessary, submit amending proposals to the European Parliament and the Council."))
(nov (ML rma "In the light of the monitoring carried out pursuant to Article 7 and the report to be drawn up pursuant to Article 8, the Commission shall review the impact of this Directive at latest 2 years after having submitted the report, and, where necessary, submit amending proposals to the European Parliament and the Council.")
(lang en)
  <para>
&lt;OptDel&gt;&lt;/OptDel&gt;
  </para>

  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>Self-explanatory.</emphasis>&lt;/OptDelPrev&gt;
  </para>

  <para>
&lt;/AmendB&gt;
  </para>


(am
(aut "Evelin Lichtenberger, Monica Frassoni ")
(nr 181)
(ref "Recital 1")
(old  The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

       </entry>

       <entry>
 The realisation of the internal market implies the elimination of restrictions to free circulation and of unjustified distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is one of the elements contributing to the success of the internal market. Appropriate, effective, transparent and harmonised protection of computer-assisted inventions throughout the Member States is essential in order to maintain and encourage investment in all technical fields involving the use of information technology.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 Distortions in competition are harmful only when they are unjustified. States may, within their competencies, make use of these, which is something that the Directive cannot prejudge.
  </para>

  <para>
<emphasis>The Directive covers the patentability of technical inventions assisted by information technology.</emphasis>
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 182)
(ref "Recital 2")
(old (ML WwW "(2) Differences exist in the protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market."))
(nov (ML Wwi "(2) Differences exist in the protection of computer-assisted inventions resulting from the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market.")
(lang en)
  <para>
Justification
  </para>

  <para>
 See justification amendment on article 1.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 183)
(ref "Recital 5")
(old  (5) Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation. Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may, and national courts of last instance must, seek a ruling from the Court of Justice.

       </entry>

       <entry>
 (5) Therefore, the legal rules governing the patentability of computer-assisted inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation. Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may, and national courts of last instance must, seek a ruling from the Court of Justice
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
See justification article 1.
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 184)
(ref "Recital 5 a (new)")
(old (ML 7Cb " "))
(nov (ML oWi "(5a) The rules of the Convention on the Grant of European Patents signed in Munich on 5 October 1973, and in particular Article 52 thereof concerning the limits to patentability, should be confirmed and clarified.")
(lang en)
  <para>
Justification
  </para>


  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 185)
(ref "Recital 6")
(old (ML fWe "The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions."))
(nov (ML gee "The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). ")
(lang en)
  <para>
Justification
  </para>

  <para>
&lt;OptDelPrev&gt;<emphasis>Slef-explanatory.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 186)
(ref "Recital 6")
(old (ML sWa2 "(6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions."))
(nov (ML fst4 "(6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. This means that patentability must be effectively limited in terms of general concepts such as " invention ", " technology " and " industry ", so as to avoid both unsystematic exceptions and uncontrollable extensions, both of which would act as barriers to free trade. Thus inventions in all fields of applied natural science are patentable, whereas innovations in fields such as mathematics, data processing and organisational logic, are not patentable, regardless of whether a computer is used for their implementation or not.")
(lang en)
(just (ML WWs " It must be made clear that there are limits as to what can be subsumed under " fields of technology " according to Art 27 TRIPS and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability. This interpretation of TRIPS is indirectly confirmed by lobbying of the US government last year against Art 27 TRIPS, on the account that it excludes business method patents, which the US government wants to mandate by the new Substantive Patent Law Treaty draft.") (ML dnt "In its first reading, Parliament deleted this recital, and therefore the amendment that proposed the above change was not voted upon. Deletion is better than keeping the original, but clarification regarding the applicability and interpretation of the TRIPs agreement is better."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 187)
(ref "Recital 6")
(old (ML sWa3 "(6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions."))
(nov (ML fst5 "(6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. This means that patentability must be effectively limited in terms of general concepts such as " invention ", " technology " and " industry ", so as to avoid both unsystematic exceptions and uncontrollable extensions, both of which would act as barriers to free trade. Thus inventions in all fields of applied natural science are patentable, whereas innovations in fields such as mathematics, data processing and organisational logic, are not patentable, regardless of whether a computer is used for their implementation or not.")
(lang en)
  <para>
Justification
  </para>

  <para>
 It must be made clear that there are limits as to what can be subsumed under "fields of technology" according to Art 27 TRIPS and that this article is not designed to mandate unlimited patentability but rather to avoid frictions in free trade, which can be caused by undue exceptions as well as by undue extensions to patentability. This interpretation of TRIPS is indirectly confirmed by lobbying of the US government last year against Art 27 TRIPS, on the account that it excludes business method patents, which the US government wants to mandate by the new Substantive Patent Law Treaty draft.
  </para>

  <para>
<emphasis>In its first reading, Parliament deleted this recital, and therefore the amendment that proposed the above change was not voted upon. Deletion is better than keeping the original, but clarification regarding the applicability and interpretation of the TRIPs agreement is better.</emphasis>
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 188)
(ref "Considerando 6")
(old (6) La Comunidad y sus Estados miembros están obligados por el Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual relacionados con el Comercio (ADPIC), aprobado mediante la Decisión 94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la celebración en nombre de la Comunidad Europea, por lo que respecta a los temas de su competencia, de los acuerdos resultantes de las negociaciones multilaterales de la Ronda Uruguay (1986-1994) 1. El apartado 1 del artículo 27 del acuerdo ADPIC establece que las patentes podrán obtenerse por todas las invenciones, sean de productos o de procedimientos, en todos los campos de la tecnología, siempre que sean nuevas, entrañen una actividad inventiva y sean susceptibles de aplicación industrial. Además, según este mismo artículo, las  patentes se podrán obtener y los derechos de patente se podrán ejercer sin discriminación por el campo de la tecnología. En consecuencia, estos principios deben aplicarse a las invenciones implementadas en ordenador.

       </entry>

       <entry>
 (6) La Comunidad y sus Estados miembros están obligados por el Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual relacionados con el Comercio (ADPIC), aprobado mediante la Decisión 94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la celebración en nombre de la Comunidad Europea, por lo que respecta a los temas de su competencia, de los acuerdos resultantes de las negociaciones multilaterales de la Ronda Uruguay (1986-1994) 1. El apartado 1 del artículo 27 del acuerdo ADPIC establece que las patentes podrán obtenerse por todas las invenciones, sean de productos o de procedimientos, en todos los campos de la tecnología, siempre que sean nuevas, entrañen una actividad inventiva y sean susceptibles de aplicación industrial. Además, según este mismo artículo, las
patentes se podrán obtener y los derechos de patente se podrán ejercer sin discriminación por el campo de la tecnología. Esto significa que esa patentabilidad debe limitarse efectivamente a términos de conceptos generales tales como "invención", "tecnología" e "industria", con el objetivo de evitar tanto las excepciones no sistemáticas como extensiones incontrolables, que actuarían como barreras al libre cambio. Las invenciones en cualquier ámbito de aplicación de las ciencias naturales son patentables, mientras que las innovaciones en campos tales como las matemáticas, el proceso de datos y la lógica de organización, no son patentables, independientemente de si se utiliza un ordenador para su aplicación o no.
       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 <emphasis>Debe dejarse claro que hay límites en cuanto a lo que puede incluirse bajo "campos de tecnología" según el artículo 27 ADPIC y que este artículo no pretende crear una patentabilidad ilimitada sino evitar las fricciones en el libre comercio que podrían causarse mediante excepciones indebidas así como por extensiones indebidas respecto a la patentabilidad.</emphasis>
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 189)
(ref "Recital 6")
(old (6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

       </entry>

       <entry>
 (6) The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to that Article, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-assisted inventions. Nevertheless, the field of software is not considered to be a field of technology.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 Patents are intended for inventions of a material nature. Copyright is intended for works of a conceptual nature, and this includes software. Software, like books, music or intellectual methods, is not therefore a field of technology within the meaning of TRIPS.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 190)
(ref "Considerando 7")
(old (ML WeW2 " (7) Con arreglo al Convenio sobre la concesión de patentes europeas, firmado en Munich el 5 de octubre de 1973, (" Convenio sobre la Patente Europea "), y las legislaciones sobre patentes de los Estados miembros, no se consideran invenciones, y quedan por tanto excluidos de la patentabilidad, los programas de ordenadores, así como los descubrimientos, las teorías científicas, los métodos matemáticos, las creaciones estéticas, los planes, principios y métodos para el ejercicio de actividades intelectuales, para juegos o para actividades económicas, y las formas de presentar informaciones. No obstante, esta excepción se aplica y se justifica únicamente en la medida en que la solicitud de patente o la patente se refiera a uno de esos elementos o actividades considerados como tales, porque dichos elementos y actividades como tales no pertenecen al campo de la tecnología."))
       <entry>
 (7)Con arreglo al Convenio sobre la concesión de patentes europeas, firmado en Munich el 5 de octubre de 1973, ("Convenio sobre la Patente Europea"), y las legislaciones sobre patentes de los Estados miembros, no se consideran invenciones, y quedan por tanto excluidos de la patentabilidad, los programas de ordenadores, así como los descubrimientos, las teorías científicas, los métodos matemáticos, las creaciones estéticas, los planes, principios y métodos para el ejercicio de actividades intelectuales, para juegos o para actividades económicas, y las formas de presentar informaciones. Esta excepción se aplica porque dichos elementos y actividades no pertenecen al campo de la tecnología.

       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 El artículo 52 EPC dice que los programas de ordenadores no son invenciones en el sentido de la legislación sobre patentes, es decir, que un sistema que consiste en un equipo informático genérico de  ordenador y una cierta combinación de normas de cálculo que funcionan en él no son patentables. No dice que tales sistemas puedan patentarse declarándolos para ser "no como tal" o "técnico". La exclusión de programas para los ordenadores no es una excepción, es parte de la regla para definir qué es una "invención"<emphasis>.</emphasis>
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 191)
(ref "Recital 7")
(old (ML egW "(7) Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 (European Patent Convention) and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to the above subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology."))
(nov (ML cth "(7) Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.")
(lang en)
(just (ML teW2 " Art 52 EPC says that programs for computers etc are not inventions in the sense of patent law, i.e. that a system consisting of generic computing hardware and some combination of calculation rules operating on it can not form the object of a patent. It does not say that such systems can be patented by declaring them to be " not as such " or " technical ". This amendment reconfirms Art 52 EPC. Note that the exclusion of programs for computers is not an exception, it is part of the rule for defining what an " invention " is.") (ML ttP3 "This amendment corresponds to recital 7 in the consolidated text of the EP’s first reading."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 192)
(ref "Recital 7")
(old (ML egW2 "(7) Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 (European Patent Convention) and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to the above subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology."))
(nov (ML cth2 "(7) Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Art 52 EPC says that programs for computers etc are not inventions in the sense of patent law, i.e. that a system consisting of generic computing hardware and some combination of calculation rules operating on it can not form the object of a patent. It does not say that such systems can be patented by declaring them to be "not as such" or "technical". This amendment reconfirms Art 52 EPC. Note that the exclusion of programs for computers is not an exception, it is part of the rule for defining what an "invention" is.
  </para>

  <para>
<emphasis>This amendment corresponds to recital 7 in the consolidated text of the EP’s first reading.</emphasis>
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 193)
(ref "Recital 8")
(old (ML nle2 "(8) The aim of this Directive is to prevent different interpretations of the provisions of the European Patent Convention concerning the limits to patentability. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software."))
(nov (ML iao " (8) The aim of this Directive is to prevent different interpretations of the provisions of the European Patent Convention concerning the limits to patentability. The consequent legal certainty should help to foster a climate conducive to investment and innovation in fields of technology as well as in the field of software.")
(lang en)
  <para>
Justification
  </para>

  <para>
 The aim is not to legislate on the patentability of software, but on that of computer-controlled inventions.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 194)
(ref "Recital 8 a (new)")
(old (ML C7S ""))
(nov (ML Wtr "(8a) Member states shall respect the provisions of this directive when acting in the framework of the European Patent Convention.")
(lang en)
  <para>
Justification
  </para>

  <para>
This amendment recognises that the Member States are also Contracting States of the European Patent Convention and that Member States have some influence on the practice of the European Patent Office, specifically with respect to ensuring that the European Patent Office complies with this directive<emphasis>.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 195)
(ref "Recital 8 a (new)")
(old (ML BiI " "))
(nov (ML vir "(8a) The European Patent Convention provides that the European Patent Office is supervised by the Administrative Council of the European Patent Organisation, and that the President of the European Patent Office is responsible for its activities to the Administrative Council. The Administrative Council is composed of representatives of the Contracting States of the European Patent Convention, a clear majority of which is formed by Member States.  These representatives shall exercise such measures within their authority to achieve compliance by the European Patent Office with this directive.")
(lang en)
(just (ML sto "This amendment recognises that the Member States are also Contracting States of the European Patent Convention and that Member States have some influence the practice of the European Patent Office, specifically with respect to maintaining high standards of examining patent applications in particular with respect to inventive step and “technical contribution” as defined in this directive."))
  <para>
Furthermore, this amendment requires Member States (in Council) to report to the European Parliament each year on what they have actually done to influence the EPO in this regard and on the progress that has been made towards the goal of minimising the grant of undeserving patents.
  </para>


  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 196)
(ref "Considerando 9")
(old (ML sed "(9) La protección que otorgan las patentes permite a los innovadores beneficiarse de su creatividad. Los derechos de patente protegen la innovación en interés de toda la sociedad y no deberían utilizarse de forma anticompetitiva."))
(nov (ML taW " (9) Las patentes son derechos temporales de exclusión concedidos por el Estado a los inventores para estimular progresos técnicos. Para asegurarse de que los trabajos del sistema pretendidos, las condiciones para conceder patentes y las modalidades para hacerlas cumplir deben ser delimitadas cuidadosamente, y, en especial, los corolarios inevitables del sistema de patentes tales como la restricción de la libertad creativa, legal, la inseguridad y los efectos contra la competencia deben ser mantenidos en límites razonables. ")
(lang es)
  <para>
Justification
  </para>

  <para>
 Los creadores de innovaciones pueden beneficiarse de su creatividad sin patentes. Si los derechos de patentes "protegen" o sofocan la innovación, y si actúan en interés de la sociedad en conjunto, esa es una cuestión que solamente puede ser contestada por el estudio empírico, no por afirmaciones dogmáticas en la legislación.
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 197)
(ref "Recital 9")
(old (ML nWs "(9) Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive."))
(nov (ML nsg "(9) Patents are temporary exclusion rights granted by the state to inventors in order to stimulate technical progress. In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed. In particular, inevitable corollaries of the patent system such as restriction of creative freedom, users´ rights or legal insecurity and anti-competitive effects must be kept within reasonable limits.")
(lang en)
(just (ML cie " Innovators can benefit from their creativity without patents. Whether patent rights " protect " or stifle innovation and whether they act in the interests of society as a whole is a question that can only be answered by empirical study, not by statements in legislation."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 198)
(ref "Recital 9")
(old (ML nWs2 "(9) Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive."))
(nov (ML nsg2 "(9) Patents are temporary exclusion rights granted by the state to inventors in order to stimulate technical progress. In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed. In particular, inevitable corollaries of the patent system such as restriction of creative freedom, users´ rights or legal insecurity and anti-competitive effects must be kept within reasonable limits.")
(lang en)
(just (ML cie2 " Innovators can benefit from their creativity without patents. Whether patent rights " protect " or stifle innovation and whether they act in the interests of society as a whole is a question that can only be answered by empirical study, not by statements in legislation."))
(am
(aut "Piia-Noora Kauppi")
(nr 199)
(ref "Recital 9")
(old (ML ind "(9) Patent protection allows innovators to benefit from their creativity. Patent rights protect innovation in the interests of society as a whole and should not be used in a manner which is anti-competitive."))
(nov (ML rti "(9) Patents are temporary exclusion rights granted by the state to inventors in order to benefit from their creativity and to stimulate technical progress. In order to ensure that the patent rights protect innovation in the interest of society as a whole and the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed. In particular, inevitable corollaries of the patent system such as restriction of creative freedom, users' rights or legal insecurity and anti-competitive effects must be kept within reasonable limits.")
(lang en)
(just (ML ott "It is important to specify the temporary nature of patents and the system needed for enforcing them in a proper manner."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 200)
(ref "Recital 9")
(old (ML Wne " (9) Patent protection allows innovators to benefit from their creativity. Patent rights protect innovation in the interests of society as a whole and should not be used in a manner which is anti-competitive."))
(nov (ML soe " (9) Patent protection allows inventors to benefit from their creativity. Patent rights protect innovation in the interests of society as a whole and should not be used in a manner which is anti-competitive or excessively detrimental to the innovation derived there from.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Self explanatory.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 201)
(ref "Considerando 10")
(old (ML Wgl " (10) De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo de 1991, sobre la protección jurídica de los programas de ordenador, cualquier forma de expresión de un programa de ordenador original estará protegida por los derechos de autor como obra literaria. No obstante, las ideas y principios en los que se basa cualquiera de los elementos de un programa de ordenador no están protegidos por los derechos de autor."))
       <entry>
(10) De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo de 1991, sobre la protección jurídica de los programas de ordenador, la propiedad en los programas de ordenador se adquiere mediante derechos reservados. Las ideas y los  principios generales sobre las que se basa un programa de ordenador deben ser utilizables libremente, de modo que los diferentes creadores puedan obtener simultáneamente la propiedad en creaciones individuales basadas en ellas.

       </entry>

(lang es)
  <para>
Justification
  </para>

  <para>
 Los derechos reservados no sólo se aplican a los trabajos literarios, sino también a los libros de texto, manuales de operación, programas de ordenador y todas las clases de estructuras de información. Los derechos reservados son el sistema de "propiedad intelectual" para programas de ordenador y no son solamente un sistema para un aspecto lateral o "literario" de los programas de ordenador. Si los derechos reservados no cubren la "idea subyacente" de un libro o de un programa, no se trata de una indicación de una insuficiencia de derechos reservados sino de la necesidad de mantener libres las "ideas subyacentes"  o conceptos generales, de modo que diversos creadores tengan ocasión de obtener la propiedad mediante trabajos individuales basados en éstos conceptos generales.
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 202)
(ref "Recital 10")
(old (ML 0cn "(10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright."))
(nov (ML CnW "(10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, property in computer programs is acquired by copyright. General ideas and principles which underlie a computer program must stay freely usable, so that many different creators may simultaneously obtain property in individual creations based thereon.")
(lang en)
(just (ML aac " Copyright does not only apply to literary works, but also to textbooks, operation manuals, computer programs and all kinds of information structures. Copyright is the system of " intellectual property " for computer programs, not only a system for a " literary " side aspect of computer programs.") (ML oov "If copyright does not cover the " underlying idea " of a book or a program then that is not an indication of an insufficiency of copyright but rather an indication of the need to keep " underlying ideas " (general concepts) free, so that many different creators have a chance to obtain property in individual works based on these general concepts."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 203)
(ref "Recital 10")
(old (ML 0cn2 "(10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright."))
(nov (ML CnW2 "(10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, property in computer programs is acquired by copyright. General ideas and principles which underlie a computer program must stay freely usable, so that many different creators may simultaneously obtain property in individual creations based thereon.")
(lang en)
(just (ML aac2 " Copyright does not only apply to literary works, but also to textbooks, operation manuals, computer programs and all kinds of information structures. Copyright is the system of " intellectual property " for computer programs, not only a system for a " literary " side aspect of computer programs.") (ML oov2 "If copyright does not cover the " underlying idea " of a book or a program then that is not an indication of an insufficiency of copyright but rather an indication of the need to keep " underlying ideas " (general concepts) free, so that many different creators have a chance to obtain property in individual works based on these general concepts."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 204)
(ref "Recital 10")
(old (ML 5cy " (10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright."))
(nov (ML Was " (10) In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright, because they are algorithms which are comparable to mathematical methods or methods of presenting information. ")
(lang en)
  <para>
Justification
  </para>

  <para>
 Rules for designing programs cannot be patentable as they are comparable to mathematical proofs.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 205)
(ref "Article 10 a (new)")
(old (ML 26k " "))
(nov (ML tal "(10a)A technical contribution is present if technical considerations contribute to the solution of a technical problem. A technical contribution is not present if the subject matter claimed in the patent solely consists of discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, programs for computers, or  presentations of information, without limitation to new, non-obvious and technical subject matter that can be made or used in any kind of industry.")
(lang en)
(just (ML Woe4 "Clarification of “technical contribution”. Whereas the positive definition of technical contribution is rather difficult and is necessarily open to interpretation, it is nevertheless important to make clear which interpretations of this term as not envisaged in the framework of this Directive."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 206)
(ref "Recital 11")
(old (ML yoW "(11) In order for any invention to be considered as patentable it should have a technical character, and thus belong to the field of technology."))
(nov (ML apc "(11) In order for any innovation to be considered a patentable invention it should have a technical character, and thus belong to a field of technology. In order to be patentable, inventions in general and inventions which can be realized by a computer program in particular must be susceptible of industrial application, new and involve an inventive step.")
(lang en)
(just (ML ies2 "The references to industrial application and inventiveness are necessary for the technical aspect to be sufficiently highlighted."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 207)
(ref "Recital 11")
(old (ML gna "  (11) Aby wynalazek mógł być uznany za posiadający zdolność patentową, powinien on mieć charakter techniczny i w związku z tym należeć do dziedziny technologii."))
(nov (ML WaW " (11) Aby wynalazek mógł być uznany za posiadający zdolność patentową, powinien on mieć charakter techniczny, tzn. dotyczyć układów materialnych, takich jak konstrukcje i tworzywa, a także materiały i substancje oraz energia i procesy ich wytwarzania i przetwarzania.")
(lang pl)
  <para>
Justification
  </para>

  <para>
 Należy koniecznie zdefiniować "charakter techniczny", dla którego to pojęcia wszystkie pozostałe zawierające określenia nawiązujące do techniki lub technologii są wtórne. Pojęcie "technologia" jest węższe od pojęcia "techniki" i w tym ostatnim się mieści. Zdolność patentowa w dotychczasowym prawie patentowym zawsze dotyczyła układów materialnych oraz procesów zachodzących w ramach tych układów. Odwoływanie się do " czterech sił natury" jest archaiczne i mało precyzyjne.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 208)
(ref "Recital 11")
(old  (11) In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology.

       </entry>

       <entry>
(11) In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology. It must also be capable of industrial application, be new, and involve an inventive step.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
This amendment is a reminder of the conditions of patentability.
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 209)
(ref "Recital 11")
(old (ML yhn "(11) In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology."))
(nov (ML Wne2 "(11) In order for any innovation to be considered a patentable invention it should have a technical character, and thus belong to a field of technology.")
(lang en)
(just (ML Wts " The Council text is not in line with Art 52 EPC. Art 52(2) EPC lists examples of non-inventions. It is not permissible to subsume these under " inventions " and then test their technical character. Moreover, while it can not be inferred from Art 52 EPC that all technical innovations are inventions, it can, based on a unanimous tradition of patent law, be assumed that all inventions have technical character."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 210)
(ref "Recital 11")
(old (ML yhn2 "(11) In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology."))
(nov (ML Wne3 "(11) In order for any innovation to be considered a patentable invention it should have a technical character, and thus belong to a field of technology.")
(lang en)
(just (ML Wts2 " The Council text is not in line with Art 52 EPC. Art 52(2) EPC lists examples of non-inventions. It is not permissible to subsume these under " inventions " and then test their technical character. Moreover, while it can not be inferred from Art 52 EPC that all technical innovations are inventions, it can, based on a unanimous tradition of patent law, be assumed that all inventions have technical character."))
(am
(aut "Manuel Medina Ortega")
(nr 211)
(ref "Considerando 11")
(old (ML nrW "(11) Para que una invención se considere patentable, deberá tener carácter técnico y pertenecer, por tanto, a un campo de la tecnología."))
(nov (ML ieW " (11) Para que una innovación se considere patentable, deberá tener carácter técnico y pertenecer, por tanto, a un campo de la tecnología.")
(lang es)
  <para>
Justification
  </para>

  <para>
 El texto del Consejo no coincide con el artículo 52 CEP. El artículo 52 (2) CEP enumera ejemplos de no invenciones. No procede incluir éstos bajo el concepto de "invenciones" para probar así su carácter técnico. Por otra parte, mientras que no puede deducirse del artículo 52 CEP que cualquier innovación técnica sea invención, sí se puede asumir, basándonos en una tradición unánime del derecho de patentes, que todas las invenciones tienen carácter técnico.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 212)
(ref "Considerando 12")
(old (ML WWt2 " (12) Condición de las invenciones en general es que, para que entrañen una actividad inventiva, deben aportar una contribución técnica al estado de la técnica."))
(nov (ML uid "suprimido")
(lang es)
  <para>
Justification
  </para>

  <para>
 Esta enmienda fue incorporada de su propia cosecha  por el Consejo. Intenta ahondar aún más en el concepto de la OEP " contribución técnica del escalón de invención".
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 213)
(ref "Recital 12")
(old (ML hzn "  (12) Dla wszelkich wynalazków warunkiem spełnienia kryterium poziomu wynalazczego jest wniesienie wkładu technicznego do stanu techniki."))
(nov (ML Weo " (12) Wszelkie wynalazki muszą spełniać wymóg wnoszenia wkładu technicznego do stanu techniki. Wkład techniczny musi być nowy i nieoczywisty dla specjalisty w danej dziedzinie techniki. Bez wnoszenia wkładu technicznego rozwiązanie jest niepatentowalne, bo nie ma wynalazku.")
(lang pl)
  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
 Charakter techniczny odzwierciedla zazwyczaj stan układu materialnego w danym momencie. Wniesienie zmian w charakter techniczny powoduje zmiany parametrów stanu tego układu materialnego.
  </para>

(am
(aut "Malcolm Harbour")
(nr 214)
(ref "Recital 12")
(old (ML WWt3 "(12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of art."))
(nov (ML aim "(12)  In order to be patentable, inventions in general and inventions which can be realised by a computer program (computer implemented inventions) in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer implemented inventions must in addition make a new technical contribution to the state of the art.")
(lang en)
  <para>
Justification
  </para>

  <para>
Clarification of the text
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 215)
(ref "Recital 12")
(old (ML Wai "(12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art."))
(nov (ML noo2 "(12) It is a condition for inventions in general that they must make a technical contribution to the state of the art. The technical contribution must be new and not obvious to the person skilled in the art. If there is no technical contribution, there is no patentable subject matter and no invention.")
(lang en)
(just (ML ivt " This amendments was newly inserted by the Council. It attempts to further codify the EPO’s “technical contribution in the inventive step” doctrine. What one invents is his contribution to the state of the art, and for this contribution to be patentable it has to (among other things) involve an inventive step. Not the other way round.") (ML tWW "The justification for the replacement text is the same as the one for article 2 (b) (amendment 4)"))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 216)
(ref "Recital 12")
(old (12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art.

       </entry>

       <entry>
(12) It is a condition for inventions in general that, in order to involve an inventive step, they should show a significant difference between the overall technical characteristics in the patent claim and the state of the art.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 This definition of an inventive step is tautological, as any technical contribution already involves an inventive step.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 217)
(ref "Recital 12")
(old (ML Wai2 "(12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art."))
(nov (ML noo3 "(12) It is a condition for inventions in general that they must make a technical contribution to the state of the art. The technical contribution must be new and not obvious to the person skilled in the art. If there is no technical contribution, there is no patentable subject matter and no invention.")
(lang en)
(just (ML ivt2 " This amendments was newly inserted by the Council. It attempts to further codify the EPO’s “technical contribution in the inventive step” doctrine. What one invents is his contribution to the state of the art, and for this contribution to be patentable it has to (among other things) involve an inventive step. Not the other way round.") (ML it2 "The justification for the replacement text is the same as the one for article 2 (b) (amendment 4)."))
  <para>
<emphasis>&lt;/AmendB&gt;</emphasis>&lt;AmendB&gt;Amendement déposé par &lt;Members&gt;Klaus-Heiner Lehne&lt;/Members&gt;
  </para>

(nr 218)
(ref "Recital 12")
(old (ML fnb "(12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a technical contribution to the state of the art.."))
(nov (ML fWr "(12) It is a condition for inventions in general that, in order to involve an inventive step, they should make a new technical contribution to the state of the art.")
(lang en)
  <para>
Justification
  </para>

  <para>
Self-evident<emphasis>.</emphasis>
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 219)
(ref "Recital 13")
(old (ML yiw " (13) Odpowiednio, mimo że wynalazek realizowany przy pomocy komputera należy do dziedziny technologii, jeżeli nie wnosi wkładu technicznego do stanu techniki, ponieważ np. jego szczególny wkład nie ma charakteru technicznego, wówczas nie spełni on kryterium poziomu wynalazczego i dlatego nie będzie posiadał zdolności patentowej."))
(nov (ML iWz " (13) Jeżeli wynalazek realizowany przy pomocy komputera nie ma charakteru technicznego, to wówczas nie spełnia on kryterium wynalazczego i dlatego nie będzie on posiadał zdolności patentowej.")
(lang pl)
  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
 Zachodzi tu wyraźna spójność pomiędzy ustępem 13 a ustępami 11 i 12.
  </para>

(am
(aut "Malcolm Harbour")
(nr 220)
(ref "Recital 13")
(old (ML tet "(13) Accordingly, although a computer implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable."))
(nov (ML itn "(13) Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of the patent law.")
(lang en)
  <para>
Justification
  </para>

  <para>
Clarification of the text. The Council proposal si misleading.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 221)
(ref "Recital 13")
(old  (13) Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable.

       </entry>

       <entry>
 (13) Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention within the meaning of patent law.

       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 When judging the patentability of an invention, patent offices have always made a clear distinction between criteria of technical expertise and inventive step. Absence of technical expertise will mean that the patent will not be granted, regardless of any inventive step criterion. If this were not the case, any purely new invention would pass the test of inventiveness, which could lead to a considerable reduction in the quality of patents granted. The amendment is a reminder of the method used to assess technical expertise.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 222)
(ref "Considerando 13")
(old (ML ele "(13) En consecuencia, si bien una invención implementada en ordenador pertenece a un campo de la tecnología, si no aporta una contribución técnica al estado de la técnica, como sería el caso, por ejemplo, si su contribución específica careciera de carácter técnico, la invención no implicará actividad inventiva y no podrá ser patentable."))
(nov (ML nsl " (13) En consecuencia, una innovación que no aporte una contribución técnica al estado de la técnica, no se considerará una invención en el sentido del derecho de patentes.")
(lang es)
  <para>
Justification
  </para>

  <para>
 El texto del Consejo define los requisitos de los programas de ordenador para ser invenciones técnicas. Retira el requisito independiente de la invención ("contribución técnica") y lo fusiona en el carácter no obvio de "paso inventivo". Esto lleva a incoherencias  y consecuencias prácticas indeseables.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 223)
(ref "Recital 13")
(old (ML tWW2 "(13) Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable."))
       <entry>
(13) Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of the patent law.”

       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
Slef-explanatory.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 224)
(ref "Recital 13")
(old (ML nWi " (13) Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable."))
(nov (ML iih " (13) Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention within the meaning of patent law.")
(lang en)
(just (ML dWq " The Council text declares computer programs to be technical inventions. It removes the independent requirement of invention (" technical contribution ") and merges it into the requirement of non-obviousness (" inventive step ").  This leads to theoretical inconsistency and undesirable practical consequences, as explained in detail in the justification of the amendment to article 4.") (ML tWE "This amendment corresponds to recital 14 in the consolidated text of the EP’s first reading."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 225)
(ref "Recital 13")
(old (ML nWi2 " (13) Accordingly, although a computer-implemented invention belongs to a field of technology, where it does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, it will lack an inventive step and thus will not be patentable."))
(nov (ML iih2 " (13) Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention within the meaning of patent law.")
(lang en)
(just (ML dWq2 " The Council text declares computer programs to be technical inventions. It removes the independent requirement of invention (" technical contribution ") and merges it into the requirement of non-obviousness (" inventive step ").  This leads to theoretical inconsistency and undesirable practical consequences, as explained in detail in the justification of the amendment to article 4.") (ML tWE2 "This amendment corresponds to recital 14 in the consolidated text of the EP’s first reading."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 226)
(ref "Recital 14")
(old (ML lne2 " (14) The mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method data processing method or other method, in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention."))
       <entry>
 (14) Accordingly, whilst computer-controlled inventions belong to a technical field, because their technical contribution lies outside the software that controls them, implementation on an apparatus such as a computer of an otherwise unpatentable method, such as a business method, data-processing method or any other method, in which the contribution to the state of the art is not technical in nature, cannot under any circumstances be considered a technical contribution. Accordingly, such an implementation cannot under any circumstances constitute a patentable invention.

       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 The initial wording is open to misunderstanding, as it implies that there could be a contribution to the state of the art that is not technical. It is important to distinguish what is technical from what is not.
  </para>

(am
(aut "Andrzej Jan Szejna")
(nr 227)
(ref "Recital 14")
(old (ML odW " (14)  Samo tylko wdrożenie nieposiadającej z innych względów zdolności patentowej metody do urządzenia takiego jak komputer nie jest wystarczającą podstawą ustalenia istnienia wkładu technicznego. Odpowiednio, metoda prowadzenia działalności gospodarczej realizowana przy użyciu komputera, metoda przetwarzania danych lub inna metoda, w której jedyny wkład do stanu techniki nie ma charakteru technicznego, nie może stanowić wynalazku posiadającego zdolność patentową."))
(nov (ML Aot " (14)  Samo tylko  zastosowanie nieposiadającej z innych względów zdolności patentowej metody do urządzenia takiego jak komputer nie jest wystarczającą podstawą ustalenia istnienia wkładu technicznego. Odpowiednio, metoda prowadzenia działalności gospodarczej realizowana przy użyciu komputera, metoda przetwarzania danych lub inna metoda,  o charakterze nietechnicznym  nie może stanowić wynalazku posiadającego zdolność patentową.")
(lang pl)
  <para>
Justification
  </para>

  <para>
 Stwierdzenie to jest spójne w stosunku do tych, które zawierają ustępy 11, 12 i 13 i stanowi swoiste powtórzenie.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 228)
(ref "Recital 14 a (new)")
(old (ML rsM " "))
(nov (ML neW "(14a) Data processing in the sense of the directive does not cover the identification of physical effects and their conversion into data.")
(lang en)
  <para>
Justification
  </para>

  <para>
The method of data processing does not cover the interfaces referred to in the Recital which belong to a field of technology.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 229)
(ref "Recital 15")
(old (ML att " (15) If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims."))
(nov (ML ate "(15) If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claim.")
(lang en)
  <para>
Justification
  </para>

  <para>
 A contribution to the state of the art must by definition be technical in nature.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 230)
(ref "Considerando 16")
(old (ML qme "(16) Además, un algoritmo es esencialmente no técnico, por lo que no puede constituir una invención técnica. No obstante, un método que comprenda el uso de un algoritmo puede ser patentable siempre que dicho método se utilice para resolver un problema técnico. Sin embargo, una patente concedida por un método de estas características no debe permitir que se monopolice el propio algoritmo o su uso en contextos no previstos en la patente."))
(nov (ML uid2 "suprimido")
(lang es)
  <para>
Justification
  </para>

  <para>
 La naturaleza del problema resuelto  debería ser irrelevante para la patentabilidad. Lo que cuenta es la naturaleza de la solución. No se inventan los problemas sino las soluciones. Y la invención debe ser técnica o bien tener carácter técnico.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 231)
(ref "Recital 16")
(old (16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method should not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

       </entry>

       <entry>
 (16) Thus, an algorithm or computer program, which are inherently non-technical, can never be regarded as inventions. A computer-controlled technical procedure might be patentable to the extent that this process has characteristics which make it a technical contribution. However, any patent granted for such a process may not establish a monopoly on the algorithm or the program itself, as programs as such cannot be patentable, as stated in particular in Article 52(2)(c) of the European Patent Convention.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 The original wording is incomplete, as it does not state that the method in question must be a technical process. It should not be concluded from this that non-technical methods, such as business or mathematical methods, could be patentable.
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 232)
(ref "Recital 16")
(old (ML abl "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method should not monopolise the algorithm itself or its use in contexts not foreseen in the patent."))
(nov (ML jnW "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.")
(lang en)
(just (ML htu "The nature of the problem solved should be irrelevant to patentability. It’s the nature of the solution that counts. Problems are not invented, but solutions are, and it’s the invention that must be technical (or have technical character)."))
(am
(aut "Andrzej Jan Szejna")
(nr 233)
(ref "Recital 16")
(old (ML npo " (16) Ponadto, algorytm sam w sobie nie posiada charakteru technicznego i dlatego też nie może stanowić wynalazku o charakterze technicznym. Pomimo to, metoda wykorzystująca użycie algorytmu może posiadać zdolność patentową pod warunkiem, że jest ona wykorzystywana jest do rozwiązania problemu technicznego. Jednakże żaden patent udzielony na taką metodę nie powinien spowodować monopolizacji samego algorytmu ani jego wykorzystania w kontekście nieprzewidzianym patentem."))
       <entry>
 (16) Ponadto, algorytm sam w sobie nie posiada charakteru technicznego i dlatego też nie może stanowić wynalazku o charakterze technicznym.

       </entry>

(lang pl)
  <para>
Justification
  </para>

  <para>
 Programy komputerowe stanowią swoiste algorytmy, wskazujące kolejność poszczególnych kroków w przekazywaniu informacji, a więc są częścią teorii i jako takie bez stosownego odniesienia do układu materialnego, ale nie komputera, nie mogą być przedmiotem patentowania. Istotą stwierdzenia zawartego w ustępie 16 jest podkreślenie intencji, iż jedynie nowe rozwiązanie teoretyczne w formie programu w skojarzeniu z urządzeniem użytkowym (nie komputerem) lub procesem materialnym mogą stanowić nową jakość łącznie podlegającym ewentualnemu patentowaniu.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Barbara Kudrycka, Tadeusz Zwiefka")
(nr 234)
(ref "Recital 16")
(old (ML abl2 "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method should not monopolise the algorithm itself or its use in contexts not foreseen in the patent."))
(nov (ML jnW2 "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.")
(lang en)
  <para>
Justification
  </para>

  <para>
 <emphasis>The nature of the problem solved should be irrelevant to patentability. It’s the nature of the solution that counts. Problems are not invented, but solutions are, and it’s the invention that must be technical (or have technical character).</emphasis>
  </para>

  <para>
<emphasis>This was a new recital from the Council.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 235)
(ref "Recital 16")
(old (ML abl3 "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method should not monopolise the algorithm itself or its use in contexts not foreseen in the patent."))
(nov (ML jnW3 "(16) Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.")
(lang en)
  <para>
Justification
  </para>

  <para>
 <emphasis>The nature of the problem solved should be irrelevant to patentability. It’s the nature of the solution that counts. Problems are not invented, but solutions are, and it’s the invention that must be technical (or have technical character).</emphasis>
  </para>

  <para>
<emphasis>This was a new recital from the Council.</emphasis>
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 236)
(ref "Recital 16 a (new)")
(old (ML gYx " "))
(nov (ML dfs "(16 a) Methods for processing data represented in digital form are by their very nature algorithms and are therefore inherently non-technical. On the other hand, if information from the physical world is not captured in order to be represented digitally, a physical process for processing such information in hardware could have a technical character. ")
(lang en)
  <para>
<emphasis>Justification</emphasis>
  </para>

  <para>
 The dividing line between the material and immaterial world, and hence between what is patentable and what is not, can be defined with legal certainty. Once a physical signal is digitised, it becomes symbolic information, which can be manipulated in an abstract fashion in software, with no possible technical effect. A technical effect can be observed only in the case of signals of a nature capable of producing the desired effect, whilst a digital signal may be stored and processed independently of the technologies used to represent it.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 237)
(ref "Recital 17")
(old (ML yha " (17) The scope of the exclusive rights conferred by any patent is defined by the claims, as interpreted with reference to the description and any drawings. Computer-implemented inventions should be claimed at least with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement."))
(nov (ML dma "(17) The scope of the exclusive rights conferred by any patent is defined by the claims, as interpreted with reference to the description and any drawings. Computer-assisted inventions should be claimed only with reference to either a product such as a programmed apparatus, or to a technical process carried out in such an apparatus.")
(lang en)
  <para>
Justification
  </para>

  <para>
 As software is not patentable, a patent cannot be infringed by the use of software under any circumstances. Patent claims may refer only to technical devices or processes, regardless of whether software is used to control the process claimed.
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 238)
(ref "Recital 17")
(old (ML WWa5 "(17) The scope of the exclusive rights conferred by any patent is defined by the claims, as interpreted with reference to the description and any drawing. Computer-implemented inventions should be claimed at least with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such sue will not constitute patent infringement. "))
(nov (ML eWe3 "(17) The scope of the exclusive rights conferred by any patent is defined by the claims, as interpreted with reference to the description and any drawing.")
(lang en)
  <para>
Justification
  </para>

  <para>
Self-explanatory<emphasis>.</emphasis>
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 239)
(ref "Recital 17 a (new)")
(old (ML dRh " "))
(nov (ML heW "(17a) Member States shall ensure that the description shall disclose the invention as claimed in such terms that the technical solution can be understood, and state any advantageous effects of the invention with reference to the background art.")
(lang en)
  <para>
Justification
  </para>

  <para>
This amendment further clarifies what has to be disclosed in a patent application.  In particular, the patent application has to explain the technical problem that the invention is seeking to overcome, and its solution, in a way that can be understood. It also has to describe any advantages, if there are any, that the invention brings over and above what has been done before.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 240)
(ref "Recital 17 a (new)")
(old (ML oxA " "))
(nov (ML sia "(17a) Member States shall ensure that the description shall disclose the invention as claimed in such terms that the technical problem and its solution as well as the inventive step can be understood.")
(lang en)
  <para>
Justification
  </para>

  <para>
This amendment further clarifies what has to be disclosed in a patent application.  In particular, the patent application has to explain the technical problem that the invention is seeking to overcome, and its solution, in a way that can be understood.
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 241)
(ref "Recital 17 b (new)")
(old (ML UXh " "))
(nov (ML cpn "(17b) It would aid in the diffusion of information and the establishment of a comprehensive database of prior art, if patent applicants could, where feasible, but independently of the need for the purposes of sufficiency of disclosure to do so, file with each patent application relating to a computer-implemented invention a well-functioning and well documented reference implementation of a program suitable for use in implementing the invention, which can be made available to the public at the same time as the publication of the description.")
(lang en)
(just (ML rer "Transparent reference implementation, where feasible, helps the information diffusion."))
(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 242)
(ref "Recital 18")
(old (ML els "(18) The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as obvious or non-technical procedures and business methods."))
(nov (ML osr "(18) The legal protection of computer-assisted inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-assisted inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods, in particular inherently non-technical methods such as algorithms, software, data processing methods or teaching or business methods.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Self-explanatory.
  </para>

(am
(aut "Manuel Medina Ortega")
(nr 243)
(ref "Considerando 18 a (new)")
(old (ML 1hF " "))
(nov (ML dlc " (18 a) De acuerdo con las previsiones del ADPIC, cualquier excepción al disfrute de los derechos de patente no debería ser interpretada de forma que permita su aplicación para ser utilizado de manera que  perjudique de forma no razonable al propietario de los derechos o que de forma no razonable entre en conflicto con la explotación normal de la invención aplicada a través de ordenador, teniendo en cuenta los legítimos intereses de otros creadores de programas informáticos para conseguir la interoperabilidad y la de los usuarios finales para tener acceso a sistemas y redes con programas interoperables y a ser capaces de utilizar los datos sobre diferentes sistemas de ordenadores.")
(lang es)
  <para>
Justification
  </para>

  <para>
 Esta enmienda supone que es el dueño de la patente quien tiene que probar que la excepción para el pleno disfrute de los derechos de su patente.  El riesgo de que cualquier persona pueda simplemente ignorar una patente reclamando que es de aplicación esta excepción queda mitigado por la capacidad del dueño de la patente de iniciar una acción convencional por infracción si cree que las estrictas condiciones de esta excepción no son aplicables.  El tribunal competente podría considerar entonces la aplicación de la excepción a las circunstancias del caso, de acuerdo con las previsiones del ADPIC, incluso si la excepción entra en conflicto, de forma no razonable, con la explotación normal de la invención.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Tadeusz Zwiefka, Barbara Kudrycka")
(nr 244)
(ref "Recital 19")
(old (ML Wct "(19) This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not susceptible of protection."))
(nov (ML eee3 "deleted")
(lang en)
(just (ML cao " Similarly to Council recital 13, this amendment claims that there are non-technical inventions. See the justification under the amendment to recital 13 for more information.") (ML wiW "This was a new recital from the Council."))
  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Fausto Bertinotti")
(nr 245)
(ref "Recital 19")
(old (ML Wct2 "(19) This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not susceptible of protection."))
(nov (ML eee4 "deleted")
(lang en)
(just (ML cao2 " Similarly to Council recital 13, this amendment claims that there are non-technical inventions. See the justification under the amendment to recital 13 for more information.") (ML wiW2 "This was a new recital from the Council."))
(am
(aut "Manuel Medina Ortega")
(nr 246)
(ref "Considerando 19")
(old (ML tnn " (19) La presente Directiva debe limitarse al establecimiento de determinados principios aplicables a la patentabilidad de este tipo de invenciones. Dichos principios pretenden garantizar que las invenciones que pertenezcan a un campo de la tecnología y aporten una contribución técnica puedan ser objeto de protección y que, por el contrario, aquellas invenciones que no aporten una contribución técnica no lo sean."))
(nov (ML uid3 "suprimido")
(lang es)
  <para>
Justification
  </para>

  <para>
 Del mismo modo que la enmienda al considerando 13, esta enmienda reivindica que hay invenciones no técnicas.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 247)
(ref "Recital 20")
(old (20) The competitive position of Community industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the Community, the importance of intellectual property protection and in particular patent protection is self-evident.

       </entry>

       <entry>
 (20) The competitive position of Community industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-assisted inventions are eliminated and the legal situation is transparent. The present trend for
traditional manufacturing industry to shift their operations to low-cost economies outside the Community, as well as the requirements for sustainable and balanced development, are factors to be taken into consideration when choosing an appropriate level of intellectual property protection and in particular patent protection for technical inventions and copyright protection for software. The level of this protection, as well as the monopolistic effects it might create, should be determined in a manner that competition and cross-fertilisation which are the key to the development of innovative small and medium-sized enterprises in the European Union with easy market access;  such enterprises will be the guarantors of the future competitiveness of the Community.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 Self-explanatory.
  </para>

(am
(aut "Klaus-Heiner Lehne")
(nr 248)
(ref "Recital 20")
(old (ML str "(20) The competitive position of Community industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the Community, the importance of intellectual property protection and in particular patent protection is self-evident."))
(nov (ML mce "(20) The competitive position of Community industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent.")
(lang en)
  <para>
Justification
  </para>

  <para>
 Self-evident.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Alexander Nuno Alvaro, Diana Wallis, Toine Manders, Janelly Fourtou")
(nr 249)
(ref "Recital 20 a (new)")
(old (ML 0Uk " "))
(nov (ML nWi3 "(20 a).  Small- and medium-sized enterprises are essential to the economic success and global competitiveness of the European Union and its Member States.  Intellectual property rights benefit small and medium-sized enterprises just as they do larger entities.  To ensure that this Directive advances the interests of SMEs, a Committee on Technological Innovation in the Small- and Medium-sized Enterprise Sector should be formed.  This Committee should focus on patent-related issues relevant to such enterprises and should bring these issues to the attention of the Commission as necessary.")
(lang en)
  <para>
Justification
  </para>

  <para>
 This amendment relates to Article 10 (Monitoring) adopted by the European Parliament during First Reading.
  </para>

  <para>
<emphasis>Currently, SMEs participate actively in Europe’s CII patents system.  Indeed, SMEs represent the majority of applicants for CII patents.  To ensure ongoing and active participation by SMEs—and to provide opportunities to enhance their involvement—this amendment proposes the creation of a committee focused on SME-related issues, with a mandate to recommend necessary reforms.</emphasis>
  </para>

(am
(aut "Malcolm Harbour")
(nr 250)
(ref "Recital 21")
(old (ML aio " (21) This Directive should be without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computers systems or networks so as to allow communication and exchange of data content between them."))
(nov (ML ufe " This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.")
(lang en)
  <para>
Justification
  </para>

  <para>
 More concise drafting appropriate to selectively define the purpose of Articles 81 and 82.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 251)
(ref "Recital 21")
(old (21) This Directive should be without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them.

       </entry>

       <entry>
(21) The provisions of this Directive are without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed to ensure conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 Interoperability, i.e. the possibility of conversion of convention between 2 systems is a key element to prevent monopoles.
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 252)
(ref "Recital 21")
(old (ML pno2 "(21) This Directive should be without prejudice to the Application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them."))
(nov (ML ou1 "(21) The provisions of this Directive are without prejudice to the application of Articles 81 and 82 of the Treaty.")
(lang en)
  <para>
Justification
  </para>

  <para>
Slef-explanatory<emphasis>.</emphasis>
  </para>

  <para>
&lt;/AmendB&gt;
  </para>

(am
(aut "Alexander Nuno Alvaro, Diana Wallis, Toine Manders, Janelly Fourtou")
(nr 253)
(ref "Recital 21 a (new)")
(old (ML 4b6 " "))
(nov (ML eiW "(21a)  Patents play an important role in European innovation.  To ensure effective functioning of the patent system, it is important to monitor developments in this sector, including developments involving patents on computer-implemented inventions.  To this end, relevant data should be gathered and appropriate reports produced.  Such reports should include information pertaining specifically to participation by small- and medium-sized enterprises in the system of patents for computer-implemented inventions.")
(lang en)
  <para>
Justification
  </para>

  <para>
 This amendment relates to Article 10 (Monitoring) adopted by the European Parliament during First Reading.
  </para>

  <para>
<emphasis>Existing statistics demonstrate fairly broad participation in the CII patents process by SMEs.  However, there is consensus among all interested parties that additional and more comprehensive statistical data on CII patents would be welcomed.  The above amendment would ensure that such data is compiled.</emphasis>
  </para>

(am
(aut "Piia-Noora Kauppi")
(nr 254)
(ref "Recital 21 a (new)")
(old (ML FoY " "))
(nov (ML WWm "(21a) The dominant supplier shall not be able to refuse to allow the use of a patented technique which is needed for the sole purpose of ensuring interoperability of two different computer systems or networks so as to allow communication and exchange of data content between them.")
(lang en)
  <para>
Justification
  </para>

  <para>
Self-explanatory.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 255)
(ref "Recital 22")
(old (22) The rights conferred by patents granted for inventions within the scope of this Directive should not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for those Articles, would require such authorisation, should not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

       </entry>

       <entry>
(22) The rights conferred by patents granted for inventions within the scope of this Directive should not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for those Articles, would require such authorisation, should not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program. Moreover, where it is necessary to make use of a patented technique to ensure conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use should not be considered as an infringement of patent.
       </entry>

(lang en)
  <para>
Justification
  </para>

  <para>
 This addition makes it possible to maintain interoperability.
  </para>

(am
(aut "Evelin Lichtenberger, Monica Frassoni")
(nr 256)
(ref "Recital 23")
(old (23) Since the objective of this Directive, namely to harmonise national rules on the patentability of computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty.
In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve that objective,

       </entry>

       <entry>
 (23) Since the objective of the proposed action,, namely to harmonise national rules on the patentability computer-assisted inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of this action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve objectives,
       </entry>

(lang en)
(just (ML Wai3 "Idem justification artile 1."))
)
