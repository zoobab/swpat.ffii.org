BEGIN {
	RECSEP="|";
}
#go == 0 {
#	next;
#}
#$0 ~ /Projet/ {
#	go=1;
#}
/<P>Amendment by/ {
	if (just == 1)
		print "";
	just=0;
	am=1
	sub(/<P>Amendment by /,"");
	sub(/<.P>/,"");
	printf "\n%s%s", $0,RECSEP;
	next
}
am == 0 {
	next;
}
/<P>Amendment [1-9]/ {
	sub(/<P>Amendment /,"");
	sub(/<.P><P>/, RECSEP);
	sub(/<.P>/,"");
	printf "%s" RECSEP, $0;
	next
}
/<P>1/ {
	sub(/<P>/,"");
	sub(/<.P>/,"");
	printf RECSEP $0
	getline
	sub(/<.P>/,"");
	printf $0 RECSEP
	next
}
/TABLE/ {
	td=0;
	next
}
/<TD>/ {
	td++;
	next
}
/<.TD>/ {
	if (td <2)
		printf RECSEP
	getline
	next
}
/<.TR>/ {
	#print ""
	next
}
td > 0 {
	sub(/<P>/,"");
	sub(/<BR>/,"");
	sub(/<.P>/,"");
	sub(/^	*/,"");
	if (length($0) > 0) {
		printf " %s", $0
	}
	next
}
/<P>Or\./ {
	sub(/<P>Or./,"");
	lang=$0
	sub(/<.P>.*/,"", lang);
	printf "%s" RECSEP, lang;
}
/<I>Justification<.I>/ {
	just=1
	next
}
just == 1 {
	if ($0 ~ /<.P>/) {
		just=0;
	}
	sub(/<P>/,"");
	sub(/<.P>/,"");
	printf " %s", $0
	if (just == 0)
		print "";
}
