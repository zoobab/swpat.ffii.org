while sleep 1
do
	if test html2data.awk -nt .h2d
	then
		touch .h2d --reference html2data.awk
		mv data .data.old
		awk -f html2data.awk 566052XM-polished.html >data
		diff -u .data.old data
		:xterm -132 -e view 566052XM-polished.html &
	fi
done
