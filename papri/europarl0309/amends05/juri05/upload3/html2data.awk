BEGIN {
	R="|";
}
/TABLE/ || /COL WIDTH/ {
	next;
}
/&lt;\/Amend[AB]?&gt;/ {
	print "";
	td=0;
	just=0;
}
/&lt;Members&gt;/ {
	sub(/.*&lt;Members&gt;/, "");
	sub(/&lt;\/Members&gt;.*/, "");
	sub(/<I>/, "");
	sub(/<\/I>/, "");
	printf $0 R
}
/&lt;NumAm[AB]?&gt;/ {
	t=$0
	sub(/.*&lt;NumAm[AB]?&gt;/, "",t);
	sub(/&lt;\/NumAm[AB]?&gt;.*/, "",t);
	printf t R
}
/&lt;Article&gt;/ {
	sub(/.*&lt;Article&gt;/, "");
	sub(/&lt;\/Article&gt;.*/, "");
	printf $0 R
	td++;
	next;
}
/&lt;Original&gt;/ {
	sub(/.*&lt;Original&gt;{..}/, "");
	sub(/&lt;\/Original&gt;.*/, "");
	printf R $0 R
	just++;
	next;
}
td != 0 {
	sub(/<BR>/,"");
	if (just != 0) {
		sub(/<P>/,"");
		sub(/<.P>/,"");
		sub(/<I>/,"");
		sub(/<.I>/,"");
	}
	#sub(/<.P>/,"");
	sub(/^	*/,"");
	if (length($0) > 0) {
		if ($0 !~ /^</)
			printf " ";
		#printf "[TD] %s", $0
		printf "%s", $0
	}
	next
}
#	if (just == 1)
#		print "";
#	just=0;
#	am=1
#	sub(/<P>Amendment by /,"");
#	sub(/<.P>/,"");
#	printf "\n%s%s", $0,RECSEP;
#	next
#}
am == 0 {
	next;
}
/TABLE/ {
	td=0;
	next
}
/<TD>/ {
	td++;
	next
}
/<.TD>/ {
	#if (td <3)
		printf RECSEP
	#getline
	next
}
/<TR / {
	next
}
/<.TR>/ {
	next
}
/<.TABLE>/ {
	#printf "|"
	td=0
	next
}
/<P>1/ {
	sub(/<P>/,"");
	sub(/<.P>/,"");
	printf RECSEP $0
	if ($5 ~ /rejette la$/) {
		getline
		sub(/<.P>/,"");
		printf $0 RECSEP
#print "========================================================="
	}
		next
}
#/<P>/ {
#	sub (/<P>/, "");
#}
/<P>Or\. / {
	sub(/<P>/,"");
	sub(/ /,"");
	sub(/Or./,"");
	lang=$0;
	sub(/<\/P>.*/,"", lang);
	printf "%s" RECSEP, lang;
}
/<I>Justification<.I>/ {
	#	printf RECSEP
	just=1
	next
}
just == 1 {
	if ($0 ~ /<.P>/) {
		just=0;
	}
	sub(/<P> /,"");
	sub(/<P>/,"");
	sub(/<.P>/,"");
	printf " %s", $0
	if (just == 0)
		print "";
	next
}
