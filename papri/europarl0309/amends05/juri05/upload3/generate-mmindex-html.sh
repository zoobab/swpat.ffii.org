#!/bin/sh
awk -F'|' '{if (NF!=76)print $3}' rawdata | sort  -u | sort +1 -n >articles.txt
outdir=mmindex
mkdir -p $outdir
(
echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"
echo "<HTML><HEAD>"
echo "<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">"
echo "<TITLE>Amendments by Signatures</TITLE></HEAD><BODY><TABLE>";
#while read article
#for number in `seq 40 256`
for number in 1
do
	#count=`grep -c "|$article|" rawdata`
	#count=`grep -c "|$number|" rawdata`
	#file=${article// /_}.html
	#filehref="<A HREF=\"$file\">"
	#echo "<TR><TD>${filehref}$article</A></TD><TD>${filehref}$count amendments</A></TD></TR>"|
		#sed 's/>1 amendments/>1 amendment/'
	#echo -n "%-50s" "$article"
	#grep -c "$article" rawdata
	awk -F'|' 'BEGIN {
}
{
		relnum++;
		sig=$1;
		num=$2;
		art=$3;
		txt=$4;
		lng=$5;
		jus=$6;
		sigarr[relnum]=sig;
		numarr[relnum]=num;
		artarr[relnum]=art;
		txtarr[relnum]=txt;
		lngarr[relnum]=lng;
		jusarr[relnum]=jus;
}
END {
		for (idx=1; idx<=relnum; idx++) {
			if (txtarr[idx]=="")
				continue;
			relidx++;
			lng=lngarr[idx];
			if (lng=="de")
				lngtxt=" (in German)";
			if (lng=="en")
				lngtxt="";
			if (lng=="es")
				lngtxt=" (in Spanish)";
			if (lng=="fr")
				lngtxt=" (in French)";
			if (lng=="it")
				lngtxt=" (in Italian)";
			if (lng=="pl")
				lngtxt=" (in Polish)";
			
			delete upd;
			num=numarr[idx];
			for (id2=idx+1; id2<=relnum; id2++) {
				if (txtarr[idx] == txtarr[id2]) {
					upd[id2]=1;
					numarr[idx]=numarr[idx] "=" numarr[id2]
					sigarr[idx]=sigarr[idx] "/" sigarr[id2]
					txtarr[id2]="";
				}
			}
			for (val in upd) {
				numarr[val]=numarr[idx]
				sigarr[val]=sigarr[idx]
			}
			mmlink="<A HREF=\"http://mm.ffii.org/JURI/Am/Num/" numarr[idx] "\">"; 
			numsigs=split(sigarr[idx], sigs, /\//);
			siglink=""
			for(sigidx=1; sigidx <= numsigs;sigidx++) {
				sigc=sigs[sigidx];
				gsub(/ /, "_", sigc);
				siglink=siglink "<A HREF=\"../signatures/" sigc ".html#" num "\">"; 
				siglink=siglink sigs[sigidx] "</A><BR>"; 
			}
			artc=artarr[idx];
			gsub(/ /, "_", artc);
			artlink="<A HREF=\"../articles/" artc ".html#" numarr[idx] "\">" artarr[idx] "</A>"; 
			numsigs=split(sigarr[idx], sigs, /\//);
			print "<TR><TD>" mmlink numarr[idx]; 
			#print "</TD><TD>" sigarr[idx];
			print "</TD><TD>" siglink;
			#print "</TD><TD>" artarr[idx];
			print "</TD><TD>" artlink;
			print "</TD><TD>" lng;
			print "</TR>";
		}

		print "</BODY></HTML>";
}' rawdata 
	#echo $file
done
echo "</TABLE></BODY></HTML>"
) >$outdir/index.html
: '{
	#print $1 >/dev/stderr
}'
