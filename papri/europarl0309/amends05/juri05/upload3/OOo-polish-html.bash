#name=juri05 admendments
name=566052XM
src=$name.html
dest=$name-polished.html
#sed 's/<FONT COLOR="#000080">.*<FONT COLOR="#000000">//g
#s/table width=[/TABLE ALIGN="center"/;
sed '#s/&lt;[^&]*&gt;//g;
/<META NAME=/d
s/TABLE WIDTH=[0-9]*/TABLE ALIGN="center"/;
s/COL WIDTH=[0-9]*/COL WIDTH=50%/;
s/TD WIDTH=[0-9]*/TD/;
s/ CLASS="western"//;
s/ FACE="Times New Roman, serif"//;
s/ STYLE="[^"]*"//;
s/.SPAN LANG="[A-z-]*"...SPAN.//g;
s/.SPAN LANG="[A-z-]*"...SPAN.//g;
s/.FONT SIZE=....FONT.//;
s/.FONT FACE="[A-z, -]*"...FONT.//;
s/.FONT COLOR="#[0-9]*"...FONT.//;
s/.FONT SIZE=....FONT.//;
s/.FONT FACE="[A-z, -]*"...FONT.//;
s/.FONT COLOR="#[0-9]*"...FONT.//g;
s/<FONT[^>]*>//g;
s/ COLOR="#.0.0.0"//;
s/ CELLSPACING=0//;
s/ FRAME=VOID//;
s/ BORDERCOLOR="#ffffff"//;
s/ RULES=COLS//;
s/<FONT>\([^<>]*\)<\/FONT>/\1/;
s/<FONT>//g;
s/<FONT SIZE=.>//g;
s/<FONT FACE="[^"]*">//g;
s/<\/FONT>//g;
s/ ALIGN=CENTER//;
s/ ALIGN=LEFT//;
s/ ALIGN=JUSTIFY//;
s/ ALIGN=RIGHT//;
s/P LANG="..-.."/P/;
s/<SPAN LANG="..-..">\([^<>]*\)<\/SPAN>/\1/g;
s/<\/SPAN><SPAN LANG="..-..">//g;
s/<SPAN>//
s/<CENTER>//
s/<\/CENTER>//
s/<STRONG>/<B>/
s/<\/STRONG>/<\/B>/
s/&lt;OptDel&gt;//;
s/&lt;\/OptDel&gt;//;
s/&lt;OptDelPrev&gt;//;
s/&lt;\/OptDelPrev&gt;//;
s/<B> <\/B>/ /
s/&quot;/"/g;
s/&laquo;&nbsp;/«/g;
s/&laquo;/«/g;
s/&raquo;/»/g;
s/« /«/g;
s/ »/»/g;
s/&oacute;/ó/g;
s/&aacute;/á/g;
s/&eacute;/é/g;
s/&iacute;/í/g;
s/&egrave;/è/g;
#s/&aacute;/Mojca DrčaI/g;
' $src |
awk -F '>' '
BEGIN {
	noprint="0"
}
/<B>Position commune/ {
	noprint="1";
}
/\/TABLE>/ {
	print
	noprint="0"
	next
}
noprint=="0"{
	if ($0 ~ /^[ 	]*$/)
		next;
#print "^"
	gsub(/Amendement/, "Amendment");
	if ($0 ~ /Emendamento presentado da/) {
		print
		getline
		prinf $0
		next
	}
	gsub(/Emendamento/, "\nAmendment");
	gsub(/presentato da/, "by");
	gsub(/Articel/, "Article");
	gsub(/Articolo/, "Article");
	gsub(/Article2,/, "Article 2,");
        gsub(/\> Amendement/, ">Amendment");
        gsub(/\<P\> /, "<P>");
        gsub(/[ 	]*$/, "");
        gsub(/	 /, "	");
        gsub(/^ *</, "<");
        gsub(/[^ 	]\([ 	]*\)/, " ");
	if ($0 ~ /^62/) {
		#print "TEST:" $0
		#getline
		#print "TEST:" $0 "--"
		#if ($0 ~ /<P>$/) print "HEY" else print "BOO"
	}
	if (strip == 1)
		gsub(/^[ 	]*/, "");
	strip=0;
	if ($0 ~ /TABLE/)
		print "";
	if ($1=="<P" || $0 ~ /^\<P\>$/) {
		printf "%s", $0;
		strip=1;
		#printf "FOO:%s--", $0;
	} else {
		#test=lastline $0;
		#if (test ~/Amendment/ || test ~/Or\./ || test ~/Paragraphe$/ || test ~/Recital$/ || test ~/Considerando/ || test ~/Artykul/ || test ~/Art.*culo/ || test ~/Artikel/ || test ~/Article$/ || test ~/Article 2,$/ || $1 ~/^.SPAN LANG=/) {
		if ($0 ~ /&lt;Article&gt;/ || $0 ~ /&lt;Members&gt;/ || $0 ~/Or\./) {
			last=$0;
			getline
			$0=last " " $0;
			sub(/Artículo/, "Article");
			sub(/Articulo/, "Article");
			sub(/Art\)iculo/, "Article");
			sub(/Artykul/, "Article");
			sub(/Artikel/, "Article");
			sub(/Apartado/, "Paragraph");
			sub(/apartado/, "paragraph");
			sub(/Considerando/, "Recital");
			sub(/Paragraphe/, "Paragraph");
			sub(/phrase introductive/, "introductory sentence");
			gsub(/punto/, "point");
#			gsub(/point b ter\)/, "point bb");
			gsub(/letter \(/, "point ");
			gsub(/point \(/, "point ");
			gsub(/point g\(/, "point g");
#			gsub(/point quinquies/, "point ba");
			sub(/nuevo/, "new");
			sub(/nouveau/, "new");
			gsub(/\)\(new/, ") (new");
		}
		print;
	}
	lastline=$0
} ' |
sed 's/<P><P>/<P>/;
s/<SPAN LANG="..-..">\([^<>]*\)<\/SPAN>/\1/g;
s/<SPAN LANG="..-..">//g;
s/<\/SPAN>//g;
s/<I><\/I>//g;
s/<SPAN LANG="">//g;
/AmendB/s/<I>//g;
/AmendB/s/<.I>//g;
s/Tadeusz Zwiefka, Barbara Kudrycka/Barbara Kudrycka, Tadeusz Zwiefka/g;
s/\(Article .\), /\1 /;
s/)(new/) (new/g;
s/\(point .\)) \(.\)/\1\2/g;
s/\(point .\) \(.\))/\1\2/g;
s/ga new/ga (new)/;
s/ <P> <I>/<P><I>/g;
s/<\/P> <P> /<\/P><P>/g;
s/<\/P> <P>/<\/P><P>/g;
s/lt;Members&gt; /lt;Members\&gt;/g;
s/lt;Article&gt; /lt;Article\&gt;/g;
s/ &lt;\/Members&gt;/\&lt;\/Members\&gt;/g;
s/ *&lt;\/Article&gt;/\&lt;\/Article\&gt;/g;
s/<A NAME=".*"><\/A>//g;
s/Amendment d&eacute;pos&eacute; par/Amendment by/;
' >$dest
exit
s/.FONT...FONT.//;
s/ COLOR="#000080"//;
s/.SPAN LANG="[A-z-]*"...SPAN.//;
s/.FONT SIZE=...FONT.//;
s/.FONT FACE="[A-z-]*"...FONT.//;
: '
s/ RULES=COLS//g
s/TABLE WIDTH=... /TABLE /;
s/<CENTER>//g
s/<\/CENTER>/<br> <br>/g
s/<\/center>/&<\/FONT>/g
s/^<center>/&<FONT COLOR="#0000ff">/
'
ls -l $src $dest
mozilla $dest
