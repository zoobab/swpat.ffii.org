#!/bin/sh
awk -F'|' '{if (NF!=76)print $3}' rawdata | sort  -u | sort +1 -n >articles.txt
outdir=articles
mkdir -p $outdir
(
echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"
echo "<HTML><HEAD>"
echo "<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">"
echo "<TITLE>Amendments by Signatures</TITLE></HEAD><BODY><TABLE>";
while read article
do
	count=`grep -c "|$article|" rawdata`
	file=${article// /_}.html
	filehref="<A HREF=\"$file\">"
	echo "<TR><TD>${filehref}$article</A></TD><TD>${filehref}$count amendments</A></TD></TR>"|
		sed 's/>1 amendments/>1 amendment/'
	#echo -n "%-50s" "$article"
	#grep -c "$article" rawdata
	awk -F'|' 'BEGIN {
		print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"
		print "<HTML><HEAD>"
		print "<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">"
		print "<TITLE>'"$article"'</TITLE></HEAD><BODY>";
		amstyl="H4";
}
$3 == "'"$article"'" {
		relnum++;
		sig=$1;
		num=$2;
		art=$3;
		txt=$4;
		lng=$5;
		jus=$6;
		sigarr[relnum]=sig;
		numarr[relnum]=num;
		artarr[relnum]=art;
		txtarr[relnum]=txt;
		lngarr[relnum]=lng;
		jusarr[relnum]=jus;
}
END {
		for (idx=1; idx<=relnum; idx++) {
			if (txtarr[idx]=="")
				continue;
			relidx++;
			lng=lngarr[idx];
			if (lng=="de")
				lngtxt=" (in German)";
			if (lng=="en")
				lngtxt="";
			if (lng=="es")
				lngtxt=" (in Spanish)";
			if (lng=="fr")
				lngtxt=" (in French)";
			if (lng=="it")
				lngtxt=" (in Italian)";
			if (lng=="pl")
				lngtxt=" (in Polish)";
			s="#" relidx ", Amendment " numarr[idx]; 
			s=s " from " sigarr[idx]
			
			for (id2=idx+1; id2<=relnum; id2++) {
				if (txtarr[idx] == txtarr[id2]) {
					s=s " and Amendment " numarr[id2]; 
					s=s " from " sigarr[id2]; 
					numarr[idx]=numarr[idx] "=" numarr[id2]
					txtarr[id2]="";
				}
			}
			printf "<" amstyl ">"
			printf "<A NAME=\"" numarr[idx] "\" ID=\"" numarr[idx] "\"></A>";
			printf s;
			print " " lngtxt ":</" amstyl ">"; 
			print "<TABLE ALIGN="center" BORDER=1 CELLPADDING=23>"
			print "<COL WIDTH=50%>"
			print "<COL WIDTH=50%>"
			print txtarr[idx];
			print "</TABLE>"
			#print "<P align=CENTER><I>Justification</I></CENTER>"
			print "<P align=CENTER><I>Justification</I></P>"
			print "<P><I>" jusarr[idx] "</I></P>";
		}

		print "</BODY></HTML>";
}' sig="$article" rawdata >$outdir/$file
	#echo $file
done < articles.txt
echo "</TABLE></BODY></HTML>"
) >$outdir/index.html
: '{
	#print $1 >/dev/stderr
}'
