# -*- mode: makefile -*-

amends05.en.lstex:
	lstex amends05.en | sort -u > amends05.en.lstex
amends05.en.mk:	amends05.en.lstex
	vcat /ul/prg/RC/texmake > amends05.en.mk


amends05.en.dvi:	amends05.en.mk
	rm -f amends05.en.lta
	if latex amends05.en;then test -f amends05.en.lta && latex amends05.en;while tail -n 20 amends05.en.log | grep -w references && latex amends05.en;do eval;done;fi
	if test -r amends05.en.idx;then makeindex amends05.en && latex amends05.en;fi

amends05.en.pdf:	amends05.en.ps
	if grep -w '\(CJK\|epsfig\)' amends05.en.tex;then ps2pdf amends05.en.ps;else rm -f amends05.en.lta;if pdflatex amends05.en;then test -f amends05.en.lta && pdflatex amends05.en;while tail -n 20 amends05.en.log | grep -w references && pdflatex amends05.en;do eval;done;fi;fi
	if test -r amends05.en.idx;then makeindex amends05.en && pdflatex amends05.en;fi
amends05.en.tty:	amends05.en.dvi
	dvi2tty -q amends05.en > amends05.en.tty
amends05.en.ps:	amends05.en.dvi	
	dvips  amends05.en
amends05.en.001:	amends05.en.dvi
	rm -f amends05.en.[0-9][0-9][0-9]
	dvips -i -S 1  amends05.en
amends05.en.pbm:	amends05.en.ps
	pstopbm amends05.en.ps
amends05.en.gif:	amends05.en.ps
	pstogif amends05.en.ps
amends05.en.eps:	amends05.en.dvi
	dvips -E -f amends05.en > amends05.en.eps
amends05.en-01.g3n:	amends05.en.ps
	ps2fax n amends05.en.ps
amends05.en-01.g3f:	amends05.en.ps
	ps2fax f amends05.en.ps
amends05.en.ps.gz:	amends05.en.ps
	gzip < amends05.en.ps > amends05.en.ps.gz
amends05.en.ps.gz.uue:	amends05.en.ps.gz
	uuencode amends05.en.ps.gz amends05.en.ps.gz > amends05.en.ps.gz.uue
amends05.en.faxsnd:	amends05.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo amends05.en-??.g3n`;source faxsnd main
amends05.en_tex.ps:	
	cat amends05.en.tex | splitlong | coco | m2ps > amends05.en_tex.ps
amends05.en_tex.ps.gz:	amends05.en_tex.ps
	gzip < amends05.en_tex.ps > amends05.en_tex.ps.gz
amends05.en-01.pgm:
	cat amends05.en.ps | gs -q -sDEVICE=pgm -sOutputFile=amends05.en-%02d.pgm -
amends05.en/amends05.en.html:	amends05.en.dvi
	rm -fR amends05.en;latex2html amends05.en.tex
xview:	amends05.en.dvi
	xdvi -s 8 amends05.en &
tview:	amends05.en.tty
	browse amends05.en.tty 
gview:	amends05.en.ps
	ghostview  amends05.en.ps &
print:	amends05.en.ps
	lpr -s -h amends05.en.ps 
sprint:	amends05.en.001
	for F in amends05.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	amends05.en.faxsnd
	faxsndjob view amends05.en &
fsend:	amends05.en.faxsnd
	faxsndjob jobs amends05.en
viewgif:	amends05.en.gif
	xv amends05.en.gif &
viewpbm:	amends05.en.pbm
	xv amends05.en-??.pbm &
vieweps:	amends05.en.eps
	ghostview amends05.en.eps &	
clean:	amends05.en.ps
	rm -f  amends05.en-*.tex amends05.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} amends05.en-??.* amends05.en_tex.* amends05.en*~
amends05.en.tgz:	clean
	set +f;LSFILES=`cat amends05.en.ls???`;FILES=`ls amends05.en.* $$LSFILES | sort -u`;tar czvf amends05.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
