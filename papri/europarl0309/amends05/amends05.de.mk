# -*- mode: makefile -*-

amends05.de.lstex:
	lstex amends05.de | sort -u > amends05.de.lstex
amends05.de.mk:	amends05.de.lstex
	vcat /ul/prg/RC/texmake > amends05.de.mk


amends05.de.dvi:	amends05.de.mk
	rm -f amends05.de.lta
	if latex amends05.de;then test -f amends05.de.lta && latex amends05.de;while tail -n 20 amends05.de.log | grep -w references && latex amends05.de;do eval;done;fi
	if test -r amends05.de.idx;then makeindex amends05.de && latex amends05.de;fi

amends05.de.pdf:	amends05.de.ps
	if grep -w '\(CJK\|epsfig\)' amends05.de.tex;then ps2pdf amends05.de.ps;else rm -f amends05.de.lta;if pdflatex amends05.de;then test -f amends05.de.lta && pdflatex amends05.de;while tail -n 20 amends05.de.log | grep -w references && pdflatex amends05.de;do eval;done;fi;fi
	if test -r amends05.de.idx;then makeindex amends05.de && pdflatex amends05.de;fi
amends05.de.tty:	amends05.de.dvi
	dvi2tty -q amends05.de > amends05.de.tty
amends05.de.ps:	amends05.de.dvi	
	dvips  amends05.de
amends05.de.001:	amends05.de.dvi
	rm -f amends05.de.[0-9][0-9][0-9]
	dvips -i -S 1  amends05.de
amends05.de.pbm:	amends05.de.ps
	pstopbm amends05.de.ps
amends05.de.gif:	amends05.de.ps
	pstogif amends05.de.ps
amends05.de.eps:	amends05.de.dvi
	dvips -E -f amends05.de > amends05.de.eps
amends05.de-01.g3n:	amends05.de.ps
	ps2fax n amends05.de.ps
amends05.de-01.g3f:	amends05.de.ps
	ps2fax f amends05.de.ps
amends05.de.ps.gz:	amends05.de.ps
	gzip < amends05.de.ps > amends05.de.ps.gz
amends05.de.ps.gz.uue:	amends05.de.ps.gz
	uuencode amends05.de.ps.gz amends05.de.ps.gz > amends05.de.ps.gz.uue
amends05.de.faxsnd:	amends05.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo amends05.de-??.g3n`;source faxsnd main
amends05.de_tex.ps:	
	cat amends05.de.tex | splitlong | coco | m2ps > amends05.de_tex.ps
amends05.de_tex.ps.gz:	amends05.de_tex.ps
	gzip < amends05.de_tex.ps > amends05.de_tex.ps.gz
amends05.de-01.pgm:
	cat amends05.de.ps | gs -q -sDEVICE=pgm -sOutputFile=amends05.de-%02d.pgm -
amends05.de/amends05.de.html:	amends05.de.dvi
	rm -fR amends05.de;latex2html amends05.de.tex
xview:	amends05.de.dvi
	xdvi -s 8 amends05.de &
tview:	amends05.de.tty
	browse amends05.de.tty 
gview:	amends05.de.ps
	ghostview  amends05.de.ps &
print:	amends05.de.ps
	lpr -s -h amends05.de.ps 
sprint:	amends05.de.001
	for F in amends05.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	amends05.de.faxsnd
	faxsndjob view amends05.de &
fsend:	amends05.de.faxsnd
	faxsndjob jobs amends05.de
viewgif:	amends05.de.gif
	xv amends05.de.gif &
viewpbm:	amends05.de.pbm
	xv amends05.de-??.pbm &
vieweps:	amends05.de.eps
	ghostview amends05.de.eps &	
clean:	amends05.de.ps
	rm -f  amends05.de-*.tex amends05.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} amends05.de-??.* amends05.de_tex.* amends05.de*~
amends05.de.tgz:	clean
	set +f;LSFILES=`cat amends05.de.ls???`;FILES=`ls amends05.de.* $$LSFILES | sort -u`;tar czvf amends05.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
