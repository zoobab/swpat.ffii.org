Objet : soutien pour des amendements de compromis

Cher collègue,

Le 5 juillet, notre Parlement votera sur la directive concernant les
brevets logiciels.

Vous êtes probablement au fait de cette directive, car elle a été
sujette à un intense lobbying. De nombreux collègues ont reçu une
moyenne d'environ trois demandes de rendez-vous chaque jour. Un groupe
d'aproximativement 20 grandes entreprises possédant un important
département des brevets et un budget de lobbying non moins important a
créé l'illusion d'un support unilatéral de la soi-disant « position
commune » sur les « inventions mises en œuvre par ordinateur ».

Vous vous souvenez sans doute également de la demande unanime du
Parlement pour un redémarrage (saisine répétée), qui a été rejetée par
la Commission européenne le 28 février de cette année. Il y avait un
large consensus au Parlement sur le fait que la Commission n'avait pas
fait ses devoirs et que la « position commune » était un ensemble
trompeur qui ne reflétait pas la volonté des États membres.
Finalement, cet ensemble a été adopté par la Présidence du Conseil
« pour de pures raisons procédurales », afin d'éviter la création d'un
« précédent dommageable » qui aurait pu donner aux parlements
nationaux un peu plus d'influence sur le processus de prise de
décision du Conseil.

Cependant, les efforts des lobbies pro-brevets semblent avoir payé.
Lors du vote de la semaine dernière en commission des affaires
juridiques (JURI), les tentatives du rapporteur Michel Rocard et de la
rapporteuse fictive Piia-Noora Kauppi pour clarifier les limites à la
brevetabilité ont été pour la plupart déjouées. En session plénière,
il est devenu encore plus difficile de passer des amendements
apportant des limites, car, selon les règles spécifiques à la seconde
lecture dans la procédure de codécision, toute absence sera comptée
comme une voix en faveur du Conseil.

Nous trouvons que cette évolution est plutôt alarmante.

Les faits restent les mêmes qu'ils étaient en février, lorsque nous
avons demandé une saisine répétée :

 - La « position commune » promet d'exclure de la brevetabilité les
   logiciels purs et les méthodes d'affaire, mais n'arrive pas à
   satisfaire cette promesse. Depuis 1997, lorsque les objectifs
   stipulés dans le Livre vert de la Commission étaient d'harmoniser
   les pratiques européennes en matière de brevets avec celles des
   États-Unis et du Japon, les moyens proposés pour accomplir ces
   objectifs n'ont pas changé sur le fond. Seules les formulations
   sont devenues de plus en plus alambiquées et trompeuses.
   
 - Si le Parlement européen n'arrive pas à rejeter ou à amender
   sérieusement la proposition du Conseil le 5 juillet prochain,
   l'exercice de larges monopoles sur des règles de calcul triviales
   et sur des méthodes d'affaires commencera immédiatement et sera
   irréversible dans les années à venir. Il existe une crainte très
   répandue chez les professionnels du logiciel et les économistes que
   ceci pourrait dévaster le paysage informatique européen, dans
   lequel 70 % de l'emploi est créé par quelques 100 000 PME qui
   détiennent ensemble moins de 10 % des brevets logiciels accordés et
   qui sont incapables de jouer le jeu du portefeuille de brevets.

 - Les citoyens européens et les parlements nationaux ont besoin d'un
   Parlement européen qui puisse dire Non. Lorsque la qualité de la
   législation provenant des organes exécutifs plonge en dessous d'un
   certain seuil, nous pouvons tirer l'arrêt d'urgence. Si nous disons
   Non plus souvent, peut-être que les citoyens que nous représentons
   le diront moins dans les futurs référendums.

Pour ces raisons, nous vous demandons expressément de signer et de
soutenir l'ensemble d'amendements de compromis ci-joint.

Ces amendements de compromis représentent un ensemble prudent de
règles simples, qui sont basées sur des pratiques éprouvées et
largement acceptées par les tribunaux nationaux.

Les règles essentielles sont formulées dans les amendements suivants :

 - N° 8 : exclusion effective des méthodes d'affaires
 - N° 3 : la « technologie»  est une « science naturelle appliquée »
 - N° 4 : la contribution doit satisfaire les critères de brevetabilité
 - N° 9 : interdiction d'une monopolisation directe des objets
          informationnels
 - N° 1 : remplacement de « mise en œuvre par ordinateur »  par
          « assistée par ordinateur »

Selon cette proposition, les inventions assistées par ordinateur dans
des secteurs tels que l'imagerie médicale par tomographie, le contrôle
de moteur automobile, les appareils ménagers, etc. sont
indiscutablement brevetables. Toute inquiétude légitime sur les
formulations peut-être extrêmes de la première lecture n'a plus de
raison d'être dans notre ensemble de compromis.

Pour tout à fait comprendre le raisonnement de ces amendements,
veuillez vous reporter au document de discussion ci-joint de la FFII,
qui, comme nous avons pu le constater, donne des arguments
convainquants en faveur de telles règles.

Dans l'intérêt de l'avenir de l'Union européenne et de la
compétitivité de notre économie de la connaissance, nous espérons
beaucoup que vous pourrez rapidement accepter de signer notre ensemble
de compromis, afin qu'il puisse être déposé mercredi 29 juin.

Cordialement,
