<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Freedom of Publication

#descr: Publication and distribution of information should never be a patent
infringement.  Freedom of publication, as stipulated in Art 10 of the
European Convention for Human Rights (ECHR), can be limited by
copyright but not by patents.

#lga: Information related rights have become increasingly important and need
reaffirmation.

#fcr: Patent rights are broad and unsuited for limiting a basic right such
as the right of publication.  By affirming the precedence of freedomof
publication over patent rights, we do not limit patentability but
rather limit the ways in which patents can be enforced.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriPubl0505 ;
# txtlang: xx ;
# End: ;

