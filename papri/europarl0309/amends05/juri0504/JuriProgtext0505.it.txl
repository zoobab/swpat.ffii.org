<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rivelazione di un semplice programma

#descr: Un programma è una descrizione di un processo fatta con un linguaggio
formale. Scrivere un programma che funziona significa descrivere un
processo in modo coerente e completamente verificato. Non vuol dire
implementare il processo. Non può essere richiesto ad un detentore di
brevetto la sottomissione di una implementazione funzionante si un
processo brevettabile. Ma può essere richiesta la sottomissione di una
descrizione completamente verificata come parte dell'obbligo di
pubblicazione, e la pratica ha dimostrato che ciò può essere utile.
Emendamenti che evitano il termine %(q:implementazione di riferimento)
sono quindi da preferirsi, anche se è OK richiedere il %(e:programma),
che dopo tutto non è l'invenzione, che deve essere %(q:implementato)
nel senso di %(q:realizzato e verificato).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriProgtext0505 ;
# txtlang: xx ;
# End: ;

