# -*- mode: makefile -*-

ffiiepp050615.de.lstex:
	lstex ffiiepp050615.de | sort -u > ffiiepp050615.de.lstex
ffiiepp050615.de.mk:	ffiiepp050615.de.lstex
	vcat /ul/prg/RC/texmake > ffiiepp050615.de.mk


ffiiepp050615.de.dvi:	ffiiepp050615.de.mk
	rm -f ffiiepp050615.de.lta
	if latex ffiiepp050615.de;then test -f ffiiepp050615.de.lta && latex ffiiepp050615.de;while tail -n 20 ffiiepp050615.de.log | grep -w references && latex ffiiepp050615.de;do eval;done;fi
	if test -r ffiiepp050615.de.idx;then makeindex ffiiepp050615.de && latex ffiiepp050615.de;fi

ffiiepp050615.de.pdf:	ffiiepp050615.de.ps
	if grep -w '\(CJK\|epsfig\)' ffiiepp050615.de.tex;then ps2pdf ffiiepp050615.de.ps;else rm -f ffiiepp050615.de.lta;if pdflatex ffiiepp050615.de;then test -f ffiiepp050615.de.lta && pdflatex ffiiepp050615.de;while tail -n 20 ffiiepp050615.de.log | grep -w references && pdflatex ffiiepp050615.de;do eval;done;fi;fi
	if test -r ffiiepp050615.de.idx;then makeindex ffiiepp050615.de && pdflatex ffiiepp050615.de;fi
ffiiepp050615.de.tty:	ffiiepp050615.de.dvi
	dvi2tty -q ffiiepp050615.de > ffiiepp050615.de.tty
ffiiepp050615.de.ps:	ffiiepp050615.de.dvi	
	dvips  ffiiepp050615.de
ffiiepp050615.de.001:	ffiiepp050615.de.dvi
	rm -f ffiiepp050615.de.[0-9][0-9][0-9]
	dvips -i -S 1  ffiiepp050615.de
ffiiepp050615.de.pbm:	ffiiepp050615.de.ps
	pstopbm ffiiepp050615.de.ps
ffiiepp050615.de.gif:	ffiiepp050615.de.ps
	pstogif ffiiepp050615.de.ps
ffiiepp050615.de.eps:	ffiiepp050615.de.dvi
	dvips -E -f ffiiepp050615.de > ffiiepp050615.de.eps
ffiiepp050615.de-01.g3n:	ffiiepp050615.de.ps
	ps2fax n ffiiepp050615.de.ps
ffiiepp050615.de-01.g3f:	ffiiepp050615.de.ps
	ps2fax f ffiiepp050615.de.ps
ffiiepp050615.de.ps.gz:	ffiiepp050615.de.ps
	gzip < ffiiepp050615.de.ps > ffiiepp050615.de.ps.gz
ffiiepp050615.de.ps.gz.uue:	ffiiepp050615.de.ps.gz
	uuencode ffiiepp050615.de.ps.gz ffiiepp050615.de.ps.gz > ffiiepp050615.de.ps.gz.uue
ffiiepp050615.de.faxsnd:	ffiiepp050615.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo ffiiepp050615.de-??.g3n`;source faxsnd main
ffiiepp050615.de_tex.ps:	
	cat ffiiepp050615.de.tex | splitlong | coco | m2ps > ffiiepp050615.de_tex.ps
ffiiepp050615.de_tex.ps.gz:	ffiiepp050615.de_tex.ps
	gzip < ffiiepp050615.de_tex.ps > ffiiepp050615.de_tex.ps.gz
ffiiepp050615.de-01.pgm:
	cat ffiiepp050615.de.ps | gs -q -sDEVICE=pgm -sOutputFile=ffiiepp050615.de-%02d.pgm -
ffiiepp050615.de/ffiiepp050615.de.html:	ffiiepp050615.de.dvi
	rm -fR ffiiepp050615.de;latex2html ffiiepp050615.de.tex
xview:	ffiiepp050615.de.dvi
	xdvi -s 8 ffiiepp050615.de &
tview:	ffiiepp050615.de.tty
	browse ffiiepp050615.de.tty 
gview:	ffiiepp050615.de.ps
	ghostview  ffiiepp050615.de.ps &
print:	ffiiepp050615.de.ps
	lpr -s -h ffiiepp050615.de.ps 
sprint:	ffiiepp050615.de.001
	for F in ffiiepp050615.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	ffiiepp050615.de.faxsnd
	faxsndjob view ffiiepp050615.de &
fsend:	ffiiepp050615.de.faxsnd
	faxsndjob jobs ffiiepp050615.de
viewgif:	ffiiepp050615.de.gif
	xv ffiiepp050615.de.gif &
viewpbm:	ffiiepp050615.de.pbm
	xv ffiiepp050615.de-??.pbm &
vieweps:	ffiiepp050615.de.eps
	ghostview ffiiepp050615.de.eps &	
clean:	ffiiepp050615.de.ps
	rm -f  ffiiepp050615.de-*.tex ffiiepp050615.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} ffiiepp050615.de-??.* ffiiepp050615.de_tex.* ffiiepp050615.de*~
ffiiepp050615.de.tgz:	clean
	set +f;LSFILES=`cat ffiiepp050615.de.ls???`;FILES=`ls ffiiepp050615.de.* $$LSFILES | sort -u`;tar czvf ffiiepp050615.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
