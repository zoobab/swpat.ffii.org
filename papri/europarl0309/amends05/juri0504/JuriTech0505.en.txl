<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Defining %(q:Technical) and %(q:Field of Technology)

#descr: The TRIPs Treaty, concluded in 1994, stipulates that patents must be
available for %(q:inventions in all fields of technology), thus
turning the question of limitation of patentability largely into one
of defining the terms %(q:field of technology) and %(q:technical).

#udW: The directive as proposed makes patentability depend on the TRIPs
terms %(q:technical) and %(q:technical field) but fails to concretise
these terms.  Or worse, it concretises them by stating that data
processing is a field of technology, thereby using TRIPs to lock
Europe into a regime of software patentability.

#ttr: There is a choice between two meanings for the term %(q:technology) to
be taken:

#ltc: applied natural science

#yig: advocated by the German Federal Court of Justice in decisions from
%(dp:1976) to %(kl:2004).

#lae: applied exact science

#emS: advocated by EICTA in their comment on the JURI amendments and by EPO
judge Mark Schar in an %(ms:article of 1998).

#nWt: Natural science is the subset of exact science in which knowledge is
gained by experimentation with %(e:controllable forces of nature). 
The %(q:four forces of nature) are a basic concept of physics, see
e.g. the recent explanation by %(NASA). Since we are dealing with
%(q:exact science), these four forces must be %(e:controllable).  The
formula of the Federal Court is thus very much to the point.

#dai: Lichtenberger et al have proposed to split the clause that defines
%(q:field of technology) and %(q:technical) into two.  This may be a
good idea and should be clarified before decisions about which version
to go for are taken.

#edl: It can be inferred from the tabled amendments that %(q:technology is
applied natural science).  This very simple phrase is also worth
tabling on its own.

#ctt: Other phrases, such as %(q:a field of technology is a discipline of
applied science in which knowledge is gained by experimenting with
controllable forces of nature) or %(q:a field of technology is an
application domain requiring the use of controllable forces of nature
to achieve predictable results) can not achieve anything more than to
reduce %(q:natural science) to more elementary terms.  The first of
these two alternatives is to be preferred, because it corresponds to
the normal understanding of %(q:natural science) as a discipline in
which experiments matter.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriTech0505 ;
# txtlang: xx ;
# End: ;

