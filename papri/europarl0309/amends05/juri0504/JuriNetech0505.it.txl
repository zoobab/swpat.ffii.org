<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: definizione Negativa di %(q:Tecnico) e %(q:Campo Tecnologico)

#descr: I TRIPs trasformano il concetto di %(q:settore tecnologico) nel
criterio chiave per limitare la brevettabilità. È quindi richiesta la
concretizzazione di questo concetto a livello nazionale. Il primo
passo per la concretizzazione è una definizione negativa. Le
definizioni negative sono più semplici di quelle positive.
L'elaborazione dati è un'attività astratta: tutte le innovazioni
nell'elaborazione dati sono nel campo dell'astrazione logica. Nel
contesto dell'elaborazione dati, %(q:tecnico) è quindi al più un
sinonimo di %(q:banale). Non riuscire ad asserire che l'elaborazione
dati non è un settore tecnologico nei termini dei TRIPs significa non
riuscire a portare a termine il compito basilare di un legislatore.
Ogni tentativo di fornire una definizione positiva che non è basata
sul riconoscimento di questa iniziale e semplice definizione negativa,
sarà confuso e incerto.

#negdef: Le definizioni negative sono molto comuni nella redazione delle leggi,
poiché esse sono meno pretenziose e durano molto di più nel tempo
delle definizioni positive. L'articolo 52 della convenzione Europea
dei Brevetti del 1973 usa lo stesso approccio: definisce che cosa
%(e:non) è un invenzione ai sensi della legge. Dopo l'introduzione di
un nuovo linguaggio da parte del %(tr:Treaty on Trade-Related Aspects
of Intellectual Property) (TRIPs) del 1994, il legislatore deve
affrontare il compito di tradurre questo approccio nel nuovo
linguaggio, in modo da chiarire la relazione tra l'articolo 52 della
CEB e l'articolo 27 dei TRIPs. Questo può essere anche considerato
essere il problema principale da risolvere per la direttiva. La
soluzione si trova in una semplice frase: l'elaborazione dati non è un
settore tecnologico.

#datproc: L'elaborazione dati è un'attività astratta: tutte le innovazioni
nell'elaborazione dati sono nel campo dell'astrazione logica. Nel
contesto dell'elaborazione dati, %(q:tecnico) è quindi al più un
sinonimo di %(q:banale). Non riuscire ad asserire che l'elaborazione
dati non è un settore tecnologico nei termini dei TRIPs significa non
riuscire a portare a termine il compito basilare di un legislatore.
Ogni tentativo di fornire una definizione positiva che non è basata
sul riconoscimento di questa iniziale e semplice definizione negativa,
sarà confuso e incerto.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriNetech0505 ;
# txtlang: xx ;
# End: ;

