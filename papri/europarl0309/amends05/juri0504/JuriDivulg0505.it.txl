<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rivelazione dell'invenzione

#descr: Diversi emendamenti richiedono una rivelazione dell'invenzione in modo
vago, non collegato alla natura informativa di innovazioni
nell'elaborazione dati. Tali emendamenti non aderiscono agli scopi
della direttiva e gonfiano solamente il testo. Alcuni ripetono leggi
esistenti, altri finiscono senza ragione col parlare di metodologie
esaminative. Il requisito di Gargani che i contratti relativi a
brevetti andrebbero resi pubblici corrisponde ad una necessità sentita
ampiamente, ma sembra anche aprire un vaso di Pandora e ha poca
relazione con l'oggetto della direttiva.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriDivulg0505 ;
# txtlang: xx ;
# End: ;

