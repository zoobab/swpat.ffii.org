<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definizioni positive e negative di %(q:Industria)

#descr: Secondo quanto riportato nell'articolo 27 dei TRIPs, %(q:invenzioni in
ogni campo della tecnologia) sono brevettabili, a patto che ... siano
%(q:suscettibili di applicazione industriale). La rivendicazione di
universalità del sistema brevettuale è limitata solo da tre termini,
tra i quali %(q:industria). È quindi necessario definire questi 3
termini, sia in maniera positiva che negativa se possibile. Il testo
della direttiva utilizza il termine %(q:industria) in molti posti per
creare l'impressione che la brevettabilità sia limitata. Il fatto che
tradizionalmente si intenda il concetto di %(q:industria) come divisa
in tre settori, quello primario, quello secondario e quello terziario
è importante e non si dovrebbe mancare la possibilità di inserire
questa distinzione nella legge anche se questo non è centrale quanto
le definizioni di %(q:tecnico) e %(q:invenzione).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriIndustr0505 ;
# txtlang: xx ;
# End: ;

