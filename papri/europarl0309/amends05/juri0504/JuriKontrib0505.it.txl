<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definizione di Contributo Tecnico

#descr: Una invenzione è un contributo allo stato dell'arte. Il contributo è
l'insieme di caratteristiche per le quali l'oggetto rivendicato si
presuppone differisca dall'esistente(prior art). L'insieme di
caratteristiche deve allo stesso tempo soddisfare a pieno i criteri di
brevettabilità. Ciò è intuitivo ed è il modo in cui tradizionalmente
sono stati usati questi termini giuridicamente e normalmente non
dovrebbe essere necessario riconfermarlo nella legge. Ma l'UBE ne ha
confuso il significato e sta cercando di imporre queste nozioni
confuse. Il consiglio ha seguito l'UBE in modo incompleto, anche
perché difficilmente si riescono ad intendere i significati dati
dall'UBE. Misure quali l'articolo 2 comma b forniscono l'opportunità
di mettere le cose nel modo giusto. Non è necessario definire il
termine %(q:tecnico) nell'articolo 2 comma b, né positivamente né
negativamente.

#eoW

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriKontrib0505 ;
# txtlang: xx ;
# End: ;

