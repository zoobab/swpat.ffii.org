<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Non è un corpo legislativo separato

#descr: Lo scopo della direttiva è di concretizzare alcune regole generali
della legislazione brevettuale in modo che sia chiarita e resa
adeguata alle innovazioni correlate ai programmi per elaboratore. Non
serve per creare un legge sui generis per i brevetti software. La
proposta della lobby a favore della brevettazione del software sembra
indicare questo intento in un considerando ma non riesce a raggiungere
lo scopo nel testo della direttiva. Inoltre il considerando
suggerisce, tra le righe, che il ruolo del Parlamento debba essere
limitato a controfirmare la pratica corrente dell'Ufficio Brevetti
Europeo (macchinosamente indicata come %(q:posizione legale
corrente)). I Verdi e Lehne stanno provando a risolvere il problema.
Entrambi gli approcci sono OK.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriSuigen0505 ;
# txtlang: xx ;
# End: ;

