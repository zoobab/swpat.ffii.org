<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Non codifichiamo la dottrina UBE sull'%(q:Ulteriore Effetto Tecnico) !

#descr: Qual è la %(q:normale interazione tra un cuoco e una ricetta)? Quali
effetti vanno oltre questa interazione? L'UBE non conosce la risposta,
e nessuno la sa. L'UBE stesso ha dichiarato pubblicamente che questa
dottrina non ha senso ed è stata introdotto per aggirare la legge. Il
Consiglio ha proposto di usarla per aggirare le chiare limitazioni
votate dal Parlamento. In generale, solo chiare, semplici regole
dovrebbero diventare legge. Concetti metodologici per l'esame dei
brevetti non dovrebbero essere codificati anche quando migliori di
questo. La domanda che sorge è se le misure proposte dal Consiglio
vadano cancellate completamente o rimpiazzate da un emendamento che le
corregga. Esse sono così sbagliate che un emendamento correttivo
risulterebbe molto diverso dall'originale. Probabilmente gli
emendamenti correttivi esistenti andrebbero spaccati in una
cancellazione e una inserzione.

#aWr: La %(q:normale interazione tra programmi ed elaboratori) è definita
tanto bene quanto la %(q:normale interazione tra un cuoco e una
ricetta). È una formula legale che l'UBE ha inventato nel 1998 per
aggirare l'articolo 52 della CEB. Solo due anni dopo, lo stesso UBE
%(a6:ha commentato) questa formula in questo modo:

#sWW: Non c'è alcun bisogno di considerare il concetto di %(q:ulteriore
effetto tecnico) durante l'esame, e si preferisce non farlo per le
seguenti ragioni: innanzitutto perché crea confusione sia per gli
esaminatori che per gli applicanti; inoltre l'unica ragione apparente
per distingure un %(q:effetto tecnico) da un %(q:ulteriore effetto
tecnico) nella decisione fu adottata per la presenza di %(q: programmi
per elaboratori) nella lista di esclusione sotto l'articolo 52 comma 2
della CEB. Se, come c'è da aspettarsi, questo elemento viene scartato
dalla lista dalla Conferenza Diplomatica, non ci sarà più alcuna base
per tale distinzione.Se ne deve dedurre che la CdA avrebbe preferito
poter dire che nessuna invenzione implementata per mezzo di
elaboratore elettronico sia esclusa dalla brevettabilità dalle
clausole degli Articoli 52 comma 2 e 3 della CEB.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriPlutech0505 ;
# txtlang: xx ;
# End: ;

