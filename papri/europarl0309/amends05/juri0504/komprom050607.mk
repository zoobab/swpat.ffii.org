# -*- mode: makefile -*-

komprom050607.lstex:
	lstex komprom050607 | sort -u > komprom050607.lstex
komprom050607.mk:	komprom050607.lstex
	vcat /ul/prg/RC/texmake > komprom050607.mk


komprom050607.dvi:	komprom050607.mk komprom050607.tex
	rm -f komprom050607.lta
	if latex komprom050607;then test -f komprom050607.lta && latex komprom050607;while tail -n 20 komprom050607.log | grep -w references && latex komprom050607;do eval;done;fi
	if test -r komprom050607.idx;then makeindex komprom050607 && latex komprom050607;fi

komprom050607.pdf:	komprom050607.ps
	if grep -w '\(CJK\|epsfig\)' komprom050607.tex;then ps2pdf komprom050607.ps;else rm -f komprom050607.lta;if pdflatex komprom050607;then test -f komprom050607.lta && pdflatex komprom050607;while tail -n 20 komprom050607.log | grep -w references && pdflatex komprom050607;do eval;done;fi;fi
	if test -r komprom050607.idx;then makeindex komprom050607 && pdflatex komprom050607;fi
komprom050607.tty:	komprom050607.dvi
	dvi2tty -q komprom050607 > komprom050607.tty
komprom050607.ps:	komprom050607.dvi	
	dvips  komprom050607
komprom050607.001:	komprom050607.dvi
	rm -f komprom050607.[0-9][0-9][0-9]
	dvips -i -S 1  komprom050607
komprom050607.pbm:	komprom050607.ps
	pstopbm komprom050607.ps
komprom050607.gif:	komprom050607.ps
	pstogif komprom050607.ps
komprom050607.eps:	komprom050607.dvi
	dvips -E -f komprom050607 > komprom050607.eps
komprom050607-01.g3n:	komprom050607.ps
	ps2fax n komprom050607.ps
komprom050607-01.g3f:	komprom050607.ps
	ps2fax f komprom050607.ps
komprom050607.ps.gz:	komprom050607.ps
	gzip < komprom050607.ps > komprom050607.ps.gz
komprom050607.ps.gz.uue:	komprom050607.ps.gz
	uuencode komprom050607.ps.gz komprom050607.ps.gz > komprom050607.ps.gz.uue
komprom050607.faxsnd:	komprom050607-01.g3n
	set -a;FAXRES=n;FILELIST=`echo komprom050607-??.g3n`;source faxsnd main
komprom050607_tex.ps:	
	cat komprom050607.tex | splitlong | coco | m2ps > komprom050607_tex.ps
komprom050607_tex.ps.gz:	komprom050607_tex.ps
	gzip < komprom050607_tex.ps > komprom050607_tex.ps.gz
komprom050607-01.pgm:
	cat komprom050607.ps | gs -q -sDEVICE=pgm -sOutputFile=komprom050607-%02d.pgm -
komprom050607/komprom050607.html:	komprom050607.dvi
	rm -fR komprom050607;latex2html komprom050607.tex
xview:	komprom050607.dvi
	xdvi -s 8 komprom050607 &
tview:	komprom050607.tty
	browse komprom050607.tty 
gview:	komprom050607.ps
	ghostview  komprom050607.ps &
print:	komprom050607.ps
	lpr -s -h komprom050607.ps 
sprint:	komprom050607.001
	for F in komprom050607.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	komprom050607.faxsnd
	faxsndjob view komprom050607 &
fsend:	komprom050607.faxsnd
	faxsndjob jobs komprom050607
viewgif:	komprom050607.gif
	xv komprom050607.gif &
viewpbm:	komprom050607.pbm
	xv komprom050607-??.pbm &
vieweps:	komprom050607.eps
	ghostview komprom050607.eps &	
clean:	komprom050607.ps
	rm -f  komprom050607-*.tex komprom050607.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} komprom050607-??.* komprom050607_tex.* komprom050607*~
komprom050607.tgz:	clean
	set +f;LSFILES=`cat komprom050607.ls???`;FILES=`ls komprom050607.* $$LSFILES | sort -u`;tar czvf komprom050607.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
