# -*- mode: makefile -*-

ffiiepp050615.lstex:
	lstex ffiiepp050615 | sort -u > ffiiepp050615.lstex
ffiiepp050615.mk:	ffiiepp050615.lstex
	vcat /ul/prg/RC/texmake > ffiiepp050615.mk


ffiiepp050615.dvi:	ffiiepp050615.mk ffiiepp050615.tex
	rm -f ffiiepp050615.lta
	if latex ffiiepp050615;then test -f ffiiepp050615.lta && latex ffiiepp050615;while tail -n 20 ffiiepp050615.log | grep -w references && latex ffiiepp050615;do eval;done;fi
	if test -r ffiiepp050615.idx;then makeindex ffiiepp050615 && latex ffiiepp050615;fi

ffiiepp050615.pdf:	ffiiepp050615.ps
	if grep -w '\(CJK\|epsfig\)' ffiiepp050615.tex;then ps2pdf ffiiepp050615.ps;else rm -f ffiiepp050615.lta;if pdflatex ffiiepp050615;then test -f ffiiepp050615.lta && pdflatex ffiiepp050615;while tail -n 20 ffiiepp050615.log | grep -w references && pdflatex ffiiepp050615;do eval;done;fi;fi
	if test -r ffiiepp050615.idx;then makeindex ffiiepp050615 && pdflatex ffiiepp050615;fi
ffiiepp050615.tty:	ffiiepp050615.dvi
	dvi2tty -q ffiiepp050615 > ffiiepp050615.tty
ffiiepp050615.ps:	ffiiepp050615.dvi	
	dvips  ffiiepp050615
ffiiepp050615.001:	ffiiepp050615.dvi
	rm -f ffiiepp050615.[0-9][0-9][0-9]
	dvips -i -S 1  ffiiepp050615
ffiiepp050615.pbm:	ffiiepp050615.ps
	pstopbm ffiiepp050615.ps
ffiiepp050615.gif:	ffiiepp050615.ps
	pstogif ffiiepp050615.ps
ffiiepp050615.eps:	ffiiepp050615.dvi
	dvips -E -f ffiiepp050615 > ffiiepp050615.eps
ffiiepp050615-01.g3n:	ffiiepp050615.ps
	ps2fax n ffiiepp050615.ps
ffiiepp050615-01.g3f:	ffiiepp050615.ps
	ps2fax f ffiiepp050615.ps
ffiiepp050615.ps.gz:	ffiiepp050615.ps
	gzip < ffiiepp050615.ps > ffiiepp050615.ps.gz
ffiiepp050615.ps.gz.uue:	ffiiepp050615.ps.gz
	uuencode ffiiepp050615.ps.gz ffiiepp050615.ps.gz > ffiiepp050615.ps.gz.uue
ffiiepp050615.faxsnd:	ffiiepp050615-01.g3n
	set -a;FAXRES=n;FILELIST=`echo ffiiepp050615-??.g3n`;source faxsnd main
ffiiepp050615_tex.ps:	
	cat ffiiepp050615.tex | splitlong | coco | m2ps > ffiiepp050615_tex.ps
ffiiepp050615_tex.ps.gz:	ffiiepp050615_tex.ps
	gzip < ffiiepp050615_tex.ps > ffiiepp050615_tex.ps.gz
ffiiepp050615-01.pgm:
	cat ffiiepp050615.ps | gs -q -sDEVICE=pgm -sOutputFile=ffiiepp050615-%02d.pgm -
ffiiepp050615/ffiiepp050615.html:	ffiiepp050615.dvi
	rm -fR ffiiepp050615;latex2html ffiiepp050615.tex
xview:	ffiiepp050615.dvi
	xdvi -s 8 ffiiepp050615 &
tview:	ffiiepp050615.tty
	browse ffiiepp050615.tty 
gview:	ffiiepp050615.ps
	ghostview  ffiiepp050615.ps &
print:	ffiiepp050615.ps
	lpr -s -h ffiiepp050615.ps 
sprint:	ffiiepp050615.001
	for F in ffiiepp050615.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	ffiiepp050615.faxsnd
	faxsndjob view ffiiepp050615 &
fsend:	ffiiepp050615.faxsnd
	faxsndjob jobs ffiiepp050615
viewgif:	ffiiepp050615.gif
	xv ffiiepp050615.gif &
viewpbm:	ffiiepp050615.pbm
	xv ffiiepp050615-??.pbm &
vieweps:	ffiiepp050615.eps
	ghostview ffiiepp050615.eps &	
clean:	ffiiepp050615.ps
	rm -f  ffiiepp050615-*.tex ffiiepp050615.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} ffiiepp050615-??.* ffiiepp050615_tex.* ffiiepp050615*~
ffiiepp050615.tgz:	clean
	set +f;LSFILES=`cat ffiiepp050615.ls???`;FILES=`ls ffiiepp050615.* $$LSFILES | sort -u`;tar czvf ffiiepp050615.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
