<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2005-05-04 JURI Emendamenti alla Direttiva sui Brevetti Software

#descr: Presentazione strutturata di emendamenti sottoposti alla decisione
sulla direttiva sui brevetti software in JURI il 4 Maggio 2005

#submit: sottoposto da

#artrec: misura

#lang: lingua

#subdok: argomento

#rekom: raccomandazione

#kritik: critiche

#vot: voto

#amnr: num.

#votlist: Lista di Voto

#amdetal: Emendamenti in dettaglio

#AWp: Le Chiarificazioni Chiave da apportare alla Direttiva

#doT: Emendamenti raggruppati per Argomento

#CxidefT: Definizione di %(q:invenzione assistita da elaboratori elettronici)

#cxidef: Una %(q:invenzione assistita da elaboratori elettronici)[, anche
inappropriatamente chiamata %(q:invenzione implementata per mezzo di
elaboratori elettronici),] è una invenzione per la legislazione
brevettuale il cui funzionamento coinvolge l'uso di un apparato
programmabile.

#ProgT: Definizione di %(q:programma per elaboratore)

#prog: Un  %(q:elaboratore) è la realizzazione di una macchina astratta che
consiste di entità quali sistemi di ingresso/uscita, processore,
memoria, memoria di massa e interfacce per scambiare informazioni con
sistemi esterni e utenti umani.  L'%(q:elaborazione dati) è il calcolo
realizzato con componenti astratti di un elaboratore. Un %(q:programma
per elaboratore) è una sistema di elaborazione dati che può, se
correttamente descritto in un linguaggio adatto, essere eseguito
immediatamente da un elaboratore.

#ProdprocT: Oggetti di Prodotto e Rivendicazioni di Processo

#prodproc: Una invenzione assistita da elaboratori elettronici può essere
rivendicata come prodotto, cioè un apparato programmato, o un processo
svolto da tale apparato.

#ProgklmT: Esclusione di Rivendicazioni su Programmi

#progklm: Una rivendicazione di brevetto su un programma per computer, di per se
o su un supporto, non è permessa.

#PublT: Libertà di Stampa

#publ: La pubblicazione o distribuzione di informazioni non può mai
costituire una violazione di brevetto.

#NetechT: Definizione Negativa di %(q:Settore Tecnologico)

#netech: L'elaborazione dati non è un settore tecnologico.

#TechT: Definizione Positiva di %(q:Tecnico) e  %(q:Settore Tecnologico)

#tech: %(q:Tecnologia) è definito come scienze naturali applicate.  Un
settore tecnologico è una disciplina delle scienze applicate nella
quale la conoscenza è ottenuta da esperimenti con forze controllabili
della natura. %(q:Tecnico) significa %(q:appartenente ad un settore
tecnologico).

#NekontribT: Definizione Negativa di %(q:Contributo)

#nekontrib: Un miglioramento nell'efficienza dell'elaborazione dati non è un
contributo tecnico.

#KontribT: Definizione Positiva di %(q:Contributo) e %(q:Invenzione)

#kontrib: Una %(q:invenzione) è un contributo allo stato dell'arte in un settore
tecnologico. Il contributo è il gruppo di caratteristiche per le quali
l'oggetto della rivendicazione di brevetto nel suo insieme si
presuppone differisca dall'esistente (prior art). Il contributo deve
essere di tipo tecnico, ovvero, deve contenere caratteristiche
tecniche e appartenere ad un settore tecnologico. Senza un contributo
tecnico, non vi è materia brevettabile ne invenzione. Il contributo
tecnico deve soddisfare a pieno le condizioni per la brevettabilità. 
In particolare, il contributo tecnico deve essere nuovo e non ovvio ad
una persona competente nella materia.

#InteropT: Libertà di interoperazione

#interop: Quando l'uso di una tecnica brevettata è necessario per poter
assicurare l'interoperabilità, tale uso non è considerato un
violazione di brevetto.

#tTf: Dieci Chiarificazioni Chiave

#HxW: lucidi della presentazione fatta da Hartmut Pilch alla %(bk:Conferenza
di Bruxelles) del 2005-06-01

#WgW: per stampa su singola pagina

#vnp: Come Raggiungere gli Obiettivi della %(q:Posizione Comune): Opzioni e
Potenziale Compromesso

#rci: Articolo per la discussione nel PPE

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/juri0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: juri0505 ;
# txtlang: xx ;
# End: ;

