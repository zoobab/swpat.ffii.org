\contentsline {chapter}{\numberline {1}Why do we want to exclude patents on software and business methods?}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}What's wrong with the ``Common Position''?}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}The thoughts as such are free\dots }{5}{section.2.1}
\contentsline {section}{\numberline {2.2}A program claim shall not be allowed, unless\dots }{6}{section.2.2}
\contentsline {chapter}{\numberline {3}The Fundamental Choice: Natural vs Exact Science}{8}{chapter.3}
\contentsline {chapter}{\numberline {4}Patenting computer tomography under the ``Ten Clarifications''}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Process Claim}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Program Claim}{10}{section.4.2}
\contentsline {chapter}{\numberline {A}Ten Core Clarifications for the Software Patent Directive}{11}{appendix.A}
