<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definire %(q:Tecnico) e %(q:Settore Tecnologico)

#descr: Il Trattato TRIPs, concluso nel 1994, stipula che i brevetti devono
essere disponibili per %(q:invenzioni in ogni settore tecnologico), e
quindi trasformando il problema di limitare la brevettabilità
praticamente in quello di definire i termini %(q:settore tecnologico)
e %(q:tecnico).

#udW: La direttiva per come è proposta rende la brevettabilità dipendente
dai termini dei TRIPs %(q:tecnico) e %(q:campo tecnico) ma non
definisce correttamente questi termini. O peggio, li definisce
dichiarando che l'elaborazione dati è un settore tecnologico, e quindi
utilizzando i TRIPs per bloccare l'Europa in un regime in cui il
software è brevettabile.

#ttr: Si deve scegliere tra due significati per il termine ''tecnologia'':

#ltc: scienze naturali applicate

#yig: sostenuto dalla Corte Federale Tedesca di Giustizia nelle sue
decisioni dal i%(dp:1976) al %(kl:2004).

#lae: scienze esatte applicate

#emS: sostenuto da EICTA nel proprio commento sugli emendamenti presentati
in JURI e dal giudice Mark Schar dell'UBE in un %(ms:articolo del
1998).

#nWt: Le scienze naturali sono il sottoinsieme di scienze esatte nel quale
la conoscenza è ottenuta attraverso la sperimentazione con %(e:forze
controllabili della natura). Le %(q:quattro forze della natura) sono
un concetto basilare della fisica, vedere ad esempio le recenti
spiegazioni fornite dalla %(NASA). Siccome abbiamo a che fare con la
%(q:scienza esatta), queste quattro forze devono essere
%(e:controllabili). La formula della Corte Federale è quindi molto
azzeccata.

#dai: Lichtenberger e altri hanno proposto di spaccare in due la clausola
che definisce %(q: settore tecnologico) e %(q:tecnico). Può essere una
buona idea e andrebbe chiarita prima di prendere decisioni su quale
versione supportare.

#edl: Dagli emendamenti proposti si può dedurre che le %(q:tecnologie sono
scienze naturali applicate). Questa semplicissima frase meriterebbe di
essere proposta come emendamento a se stante.

#ctt: Altre frasi, quali %(q:un settore tecnologico è una disciplina delle
scienze applicate nel quale la conoscenza è ottenuta per mezzo di
esperimenti con forze controllabili della natura) o %(q:un settore
tecnologico è un dominio di applicazione che richiede l'uso di forze
controllabili della natura per ottenere risultati predicibili) non
possono ottenere niente di più che ridurre le %(q:scienze naturali) a
termini più elementari. LA prima di queste due alternative è da
preferirsi, perché corrisponde alla normale definizione di %(q:scienze
naturali) come di una disciplina in cui contano gli esperimenti.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriTech0505 ;
# txtlang: xx ;
# End: ;

