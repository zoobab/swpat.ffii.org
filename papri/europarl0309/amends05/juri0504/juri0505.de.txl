<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2005-05-04 JURI Änderungen an der Softwarepatentrichtlinie

#descr: Strukturierte Übersicht der am 4. Mai 2005 im Rechtsausschuss des
Europäischen Parlaments eingereichten Anträge zur Änderung der
Softwarepatent-Richtlinie.

#submit: eingereicht von

#artrec: Bestimmung

#lang: Sprache

#subdok: Thema

#rekom: Empfehlung

#kritik: Kritik

#vot: Stimme

#amnr: Nr.

#votlist: Abstimmliste

#amdetal: Änderungen im Detail

#AWp: die zu leistenden Zentralen Klarstellungen

#CxidefT: Definition der %(q:computergestützten Erfindung)

#cxidef: Eine %(q:Computergestützte Erfindung) [, bisweilen irreführenderweise
%(q:computer-implementierte Erfindung) genannt,] ist eine Erfindung im
Sinne des Patentrechts, bei deren Ausführung ein programmierbarer
Apparat zum Einsatz kommt.

#ProgT: Definition von %(q:Computerprogramm)

#prog: Eine %(q:Datenverarbeitungsanlage) ist eine Realisierung einer
abstrakten Maschine, die aus Bestandteilen wie Ein- und Ausgabe,
Prozessor, Arbeitsspeicher, Dauerspeicher und Schnittstellen für den
Datenaustausch mit externen Systemen und menschlichen Anwendern
besteht.  %(q:Datenverarbeitung) ist Rechnen mit Bestandteilen
abstrakter Maschinen.  Ein %(q:Datenverarbeitungsprogramm) oder
%(q:Computerprogramm) ist eine Lösung eines Problems mit Mitteln der
Datenverarbeitung, die, sobald sie in einer passenden Sprache korrekt
beschrieben worden ist, von einer Datenverarbeitungsanlage ausgeführt
werden kann.

#ProdprocT: Gegenstände von Erzeugnis- und Verfahrensansprüchen

#prodproc: Eine computergestützte Erfindung kann beansprucht werden als
Erzeugnis, d.h. als ein programmierter Apparat, oder als ein von einem
solchen Apparat ausgeführter Vorgang.

#ProgklmT: Ausschluss von Programmansprüchen

#progklm: Ein Patentanspruch auf ein Computerprogramm, sei es für sich allein
oder auf einem Datenträger, ist nicht zulässig.

#PublT: Veröffentlichungsfreiheit

#publ: Die Veröffentlichung oder Verbreitung von Computerprogrammen in
jedweder Form kann niemals eine Patentverletzung darstellen.

#NetechT: Negative Definition von %(q:Gebiet der Technik)

#netech: Die Datenverarbeitung ist kein Gebiet der Technik.

#TechT: Positive Definition von %(q:Technisch) und %(q:Gebiet der Technik)

#tech: %(q:Technik) ist angewandte Naturwissenschaft.  Ein Gebiet der Technik
ist eine Disziplin der angewandten Wissenschaft, in der Wissen durch
Versuche mit beherrschbaren Naturkräften gewonnen wird. 
%(q:Technisch) bedeutet %(q:zu einem Gebiet der Technik gehörend).

#NekontribT: Negative Definition von %(q:Beitrag)

#nekontrib: Eine Steigerung der Datenverarbeitungseffizienz ist kein technischer
Beitrag.

#KontribT: Positive Definition von %(q:Beitrag) und %(q:Erfindung)

#kontrib: Eine %(q:Erfindung) ist ein Beitrag zum Stand der Technik. Der Beitrag
ist die Menge der Merkmale durch die sich der Umfang des
Patentanspruchs vom bisherigen Wissensstand abhebt.  Dieser Beitrag
muss ein technischer sein, d.h. er muss technische Merkmale umfassen
und in einem technischen Gebiet liegen.  Ohne einen technischen
Beitrag gibt es keinen patentfähigen Gegenstand und keine Erfindung.
Der technische Beitrag muss die Bedingungen der Patentierbarkeit
erfüllen.  Insbesondere muss der technische Beitrag neu und für den
Fachmann nicht naheliegend sein.

#InteropT: Interoperationsfreiheit

#interop: Wenn der Gebrauch einer patentierten Lösung notwendig ist, um die
Interoperabilität zu gewährleisten, handelt es sich bei diesem
Gebrauch nicht um eine Patentverletzung.

#doT: nach Themen gruppierte Änderungsvorschläge

#tTf: Zehn Zentrale Klarstellungen

#HxW: Seiten einer Präsentation, die Hartmut Pilch auf der %(bk:Brüsseler
Konferenz) vom 1. Juni 2006 gab.

#WgW: zum Drucken auf einer Seite

#vnp: Wie sich die Ziele des %(q:Gemeinsamen Standpunktes) erreichen lassen
-- Optionen und Kompromisspotential

#rci: Diskussionspapier zur Werbung für die Zehn Zentralen Klarstellungen in
der EVP

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/juri0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: juri0505 ;
# txtlang: xx ;
# End: ;

