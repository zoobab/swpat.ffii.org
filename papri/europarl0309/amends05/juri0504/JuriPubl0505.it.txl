<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Libertà di Stampa

#descr: La pubblicazione e la distribuzione di informazioni non dovrebbe mai
costituire una violazione di brevetto. La libertà di stampa, come
stipulato nell'articolo 10  della Convenzione Europea sui Diritti
Umani (EHCR), può essere limitata dal diritto d'autore ma non dai
brevetti.

#lga: Diritti legati all'informazione sono diventati sempre più importanti
ed è necessario riaffermarli.

#fcr: I diritti concessi dai brevetti sono ampi e inadeguati a limitare
diritti basilari come il diritto di stampa. Affermando la precedenza
del diritto di stampa sui diritti concessi dai brevetti non si limita
la brevettabilità ma piuttosto si limita il modo in cui i brevetti
possono essere fatti valere.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriPubl0505 ;
# txtlang: xx ;
# End: ;

