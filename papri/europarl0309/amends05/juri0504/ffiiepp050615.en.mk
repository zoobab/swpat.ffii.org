# -*- mode: makefile -*-

ffiiepp050615.en.lstex:
	lstex ffiiepp050615.en | sort -u > ffiiepp050615.en.lstex
ffiiepp050615.en.mk:	ffiiepp050615.en.lstex
	vcat /ul/prg/RC/texmake > ffiiepp050615.en.mk


ffiiepp050615.en.dvi:	ffiiepp050615.en.mk
	rm -f ffiiepp050615.en.lta
	if latex ffiiepp050615.en;then test -f ffiiepp050615.en.lta && latex ffiiepp050615.en;while tail -n 20 ffiiepp050615.en.log | grep -w references && latex ffiiepp050615.en;do eval;done;fi
	if test -r ffiiepp050615.en.idx;then makeindex ffiiepp050615.en && latex ffiiepp050615.en;fi

ffiiepp050615.en.pdf:	ffiiepp050615.en.ps
	if grep -w '\(CJK\|epsfig\)' ffiiepp050615.en.tex;then ps2pdf ffiiepp050615.en.ps;else rm -f ffiiepp050615.en.lta;if pdflatex ffiiepp050615.en;then test -f ffiiepp050615.en.lta && pdflatex ffiiepp050615.en;while tail -n 20 ffiiepp050615.en.log | grep -w references && pdflatex ffiiepp050615.en;do eval;done;fi;fi
	if test -r ffiiepp050615.en.idx;then makeindex ffiiepp050615.en && pdflatex ffiiepp050615.en;fi
ffiiepp050615.en.tty:	ffiiepp050615.en.dvi
	dvi2tty -q ffiiepp050615.en > ffiiepp050615.en.tty
ffiiepp050615.en.ps:	ffiiepp050615.en.dvi	
	dvips  ffiiepp050615.en
ffiiepp050615.en.001:	ffiiepp050615.en.dvi
	rm -f ffiiepp050615.en.[0-9][0-9][0-9]
	dvips -i -S 1  ffiiepp050615.en
ffiiepp050615.en.pbm:	ffiiepp050615.en.ps
	pstopbm ffiiepp050615.en.ps
ffiiepp050615.en.gif:	ffiiepp050615.en.ps
	pstogif ffiiepp050615.en.ps
ffiiepp050615.en.eps:	ffiiepp050615.en.dvi
	dvips -E -f ffiiepp050615.en > ffiiepp050615.en.eps
ffiiepp050615.en-01.g3n:	ffiiepp050615.en.ps
	ps2fax n ffiiepp050615.en.ps
ffiiepp050615.en-01.g3f:	ffiiepp050615.en.ps
	ps2fax f ffiiepp050615.en.ps
ffiiepp050615.en.ps.gz:	ffiiepp050615.en.ps
	gzip < ffiiepp050615.en.ps > ffiiepp050615.en.ps.gz
ffiiepp050615.en.ps.gz.uue:	ffiiepp050615.en.ps.gz
	uuencode ffiiepp050615.en.ps.gz ffiiepp050615.en.ps.gz > ffiiepp050615.en.ps.gz.uue
ffiiepp050615.en.faxsnd:	ffiiepp050615.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo ffiiepp050615.en-??.g3n`;source faxsnd main
ffiiepp050615.en_tex.ps:	
	cat ffiiepp050615.en.tex | splitlong | coco | m2ps > ffiiepp050615.en_tex.ps
ffiiepp050615.en_tex.ps.gz:	ffiiepp050615.en_tex.ps
	gzip < ffiiepp050615.en_tex.ps > ffiiepp050615.en_tex.ps.gz
ffiiepp050615.en-01.pgm:
	cat ffiiepp050615.en.ps | gs -q -sDEVICE=pgm -sOutputFile=ffiiepp050615.en-%02d.pgm -
ffiiepp050615.en/ffiiepp050615.en.html:	ffiiepp050615.en.dvi
	rm -fR ffiiepp050615.en;latex2html ffiiepp050615.en.tex
xview:	ffiiepp050615.en.dvi
	xdvi -s 8 ffiiepp050615.en &
tview:	ffiiepp050615.en.tty
	browse ffiiepp050615.en.tty 
gview:	ffiiepp050615.en.ps
	ghostview  ffiiepp050615.en.ps &
print:	ffiiepp050615.en.ps
	lpr -s -h ffiiepp050615.en.ps 
sprint:	ffiiepp050615.en.001
	for F in ffiiepp050615.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	ffiiepp050615.en.faxsnd
	faxsndjob view ffiiepp050615.en &
fsend:	ffiiepp050615.en.faxsnd
	faxsndjob jobs ffiiepp050615.en
viewgif:	ffiiepp050615.en.gif
	xv ffiiepp050615.en.gif &
viewpbm:	ffiiepp050615.en.pbm
	xv ffiiepp050615.en-??.pbm &
vieweps:	ffiiepp050615.en.eps
	ghostview ffiiepp050615.en.eps &	
clean:	ffiiepp050615.en.ps
	rm -f  ffiiepp050615.en-*.tex ffiiepp050615.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} ffiiepp050615.en-??.* ffiiepp050615.en_tex.* ffiiepp050615.en*~
ffiiepp050615.en.tgz:	clean
	set +f;LSFILES=`cat ffiiepp050615.en.ls???`;FILES=`ls ffiiepp050615.en.* $$LSFILES | sort -u`;tar czvf ffiiepp050615.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
