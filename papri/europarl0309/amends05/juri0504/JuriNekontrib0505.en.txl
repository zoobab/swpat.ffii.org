<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Negative Definitions of %(q:Contribution) / %(q:Invention)

#descr: An improvements in data processing efficiency, i.e. economisation of
time or memory space, should not constitute a %(q:technical
contribution).  If it does, all new data processing and business
methods become patentable.  Various amendments have been submitted
relating to this.

#Wht: The simplest form to put forward the needed clarification would be
%(q:An improvement in data processing efficiency is not a technical
contribution).  Possibly in finding a compromise amendment between
Ortega and Kudrycka et al, this can still be achieved.  It is a bit
unfortunate if the principle has to be limited to either %(q:solutions
to technical problems) or %(q:data processing solutions).  Rather it
should apply to all solutions.  Moreover, although the term
%(q:invention) can and should be treated as a synonym of %(q:technical
contribution), in case anyone wants to make a distinction between the
two, it should be clear that this provision is about limiting the
%(q:contribution), which is the more unequivocal term.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriNekontrib0505 ;
# txtlang: xx ;
# End: ;

