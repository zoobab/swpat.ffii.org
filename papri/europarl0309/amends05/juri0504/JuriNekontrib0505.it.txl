<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definizione Negativa di %(q:Contributo) / %(q:Invenzione)

#descr: Un miglioramento nell'efficienza dell'elaborazione dati, quale il
risparmio di tempo o spazio di memoria, non dovrebbe costituire un
%(q:contributo tecnico). Se lo fosse, ogni nuovo metodo di
elaborazione dati e metodo commerciale diventerebbe brevettabile.
Questo codifica sia la prassi (caselaw) Tedesca che quella Britannica.
Sono stati proposti vari emendamenti in merito a questo punto.

#Wht: La forma più semplice per avanzare la necessaria chiarificazione
sarebbe %(q:Un miglioramento nell'efficienza dell'elaborazione dati
non è un contributo tecnico). Probabilmente ciò può ancora essere
ottenuto trovando un compromesso tra gli emendamenti di Ortega e
Kudrycka e altri. Sarebbe un po' problematico se il principio fosse da
limitarsi a %(q:soluzioni a problemi tecnici) o %(q:soluzioni di
elaborazioni di dati). Invece dovrebbe applicarsi a tutte le
soluzioni. Inoltre, benché il termine %(q:invenzione) può e dovrebbe
normalmente essere considerato un sinonimo di %(q:contributo tecnico),
in caso qualcuno volesse fare una distinzione tra i due, dovrebbe
essere chiaro che questa misura si applica a limitare il
%(q:contributo), che il termine più inequivocabile.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriNekontrib0505 ;
# txtlang: xx ;
# End: ;

