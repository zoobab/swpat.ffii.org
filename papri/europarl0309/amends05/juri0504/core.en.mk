# -*- mode: makefile -*-

core.en.lstex:
	lstex core.en | sort -u > core.en.lstex
core.en.mk:	core.en.lstex
	vcat /ul/prg/RC/texmake > core.en.mk


core.en.dvi:	core.en.mk
	rm -f core.en.lta
	if latex core.en;then test -f core.en.lta && latex core.en;while tail -n 20 core.en.log | grep -w references && latex core.en;do eval;done;fi
	if test -r core.en.idx;then makeindex core.en && latex core.en;fi

core.en.pdf:	core.en.ps
	if grep -w '\(CJK\|epsfig\)' core.en.tex;then ps2pdf core.en.ps;else rm -f core.en.lta;if pdflatex core.en;then test -f core.en.lta && pdflatex core.en;while tail -n 20 core.en.log | grep -w references && pdflatex core.en;do eval;done;fi;fi
	if test -r core.en.idx;then makeindex core.en && pdflatex core.en;fi
core.en.tty:	core.en.dvi
	dvi2tty -q core.en > core.en.tty
core.en.ps:	core.en.dvi	
	dvips  core.en
core.en.001:	core.en.dvi
	rm -f core.en.[0-9][0-9][0-9]
	dvips -i -S 1  core.en
core.en.pbm:	core.en.ps
	pstopbm core.en.ps
core.en.gif:	core.en.ps
	pstogif core.en.ps
core.en.eps:	core.en.dvi
	dvips -E -f core.en > core.en.eps
core.en-01.g3n:	core.en.ps
	ps2fax n core.en.ps
core.en-01.g3f:	core.en.ps
	ps2fax f core.en.ps
core.en.ps.gz:	core.en.ps
	gzip < core.en.ps > core.en.ps.gz
core.en.ps.gz.uue:	core.en.ps.gz
	uuencode core.en.ps.gz core.en.ps.gz > core.en.ps.gz.uue
core.en.faxsnd:	core.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo core.en-??.g3n`;source faxsnd main
core.en_tex.ps:	
	cat core.en.tex | splitlong | coco | m2ps > core.en_tex.ps
core.en_tex.ps.gz:	core.en_tex.ps
	gzip < core.en_tex.ps > core.en_tex.ps.gz
core.en-01.pgm:
	cat core.en.ps | gs -q -sDEVICE=pgm -sOutputFile=core.en-%02d.pgm -
core.en/core.en.html:	core.en.dvi
	rm -fR core.en;latex2html core.en.tex
xview:	core.en.dvi
	xdvi -s 8 core.en &
tview:	core.en.tty
	browse core.en.tty 
gview:	core.en.ps
	ghostview  core.en.ps &
print:	core.en.ps
	lpr -s -h core.en.ps 
sprint:	core.en.001
	for F in core.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	core.en.faxsnd
	faxsndjob view core.en &
fsend:	core.en.faxsnd
	faxsndjob jobs core.en
viewgif:	core.en.gif
	xv core.en.gif &
viewpbm:	core.en.pbm
	xv core.en-??.pbm &
vieweps:	core.en.eps
	ghostview core.en.eps &	
clean:	core.en.ps
	rm -f  core.en-*.tex core.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} core.en-??.* core.en_tex.* core.en*~
core.en.tgz:	clean
	set +f;LSFILES=`cat core.en.ls???`;FILES=`ls core.en.* $$LSFILES | sort -u`;tar czvf core.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
