<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Importanza delle PMI

#descr: Molti emendamenti evidenziano quanto le piccole e medie imprese (PMI)
siano importanti per l'autore dell'emendamento, ma non fanno nulla
eccetto gonfiare la direttiva e aggiungere nuovi balzelli. Le proposte
ignorano anche il fatto che la Commissione Europea è già coinvolta
nella evangelizzazione del sistema brevettuale presso le PMI
attraverso vari programmi. Alcuni degli emendamenti esprimono falsità
a proposito dell'importanza dei brevetti per le PMI nelle proprie
giustificazioni. Sembra che il processo di emendamento sia utilizzato
per scopi non-legislativi. La risposta comune a tali emendamenti
dovrebbe essere: No. Il Parlamento si deve proteggere dai tentativi di
rubargli tempo.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriSme0505 ;
# txtlang: xx ;
# End: ;

