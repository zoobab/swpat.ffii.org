<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definizione di  %(q:Invenzione assistita da/implementata per mezzo di
Elaboratore Elettronico)

#descr: Una invenzione assistita da un elaboratore elettronico è una
innovazione brevettabile la cui realizzazione coinvolge l'uso di un
apparato programmabile. Una %(q:invenzione attuata per mezzo di
elaboratore elettronico) non è nient'altro che un programma per
computer nel contesto delle rivendicazioni di brevetto e quindi non
una invenzione secondo la legislazione brevettuale. Comunque, a scopo
di transizione, il termine %(q:invenzione attuata per mezzo di
elaboratore elettronico) andrebbe ridefinito come sinonimo di
%(q:invenzione assistita da elaboratore elettronico).

#qhd: L'Ufficio Brevetti Europeo (UBE) ha cominciato a dichiarare come
brevettabile materie non brevettabili semplicemente definendole come
%(q:invenzioni). Una soluzione informatica generica, chiamata
%(q:programma per elaboratore) nel linguaggio della Convenzione
Europea sui Brevetti ed esclusa dalla brevettabilità, è implicitamente
dichiarata brevettabile col nome di %(q:invenzione attuata per mezzo
di elaboratore elettronico).  Nelle comunicazioni pubbliche, i
sostenitori di questa posizione comunque sostengono che %(q:CII) si
riferisce a sistemi %(q:assistiti da elaboratori elettronici), come
lavatrici o caricabatteria che funzionano sotto il controllo di un
elaboratore.

#ene: Molti gruppi hanno proposto modi di risolvere il conflitto. È
impossibile non usare sia la locuzione più appropriata %(q:invenzione
assistita da elaboratore elettronico) che la precedente locuzione
%(q:invenzione attuata per mezzo di elaboratore elettronico) e
definirle come ciò che i sostenitori dell'UBE ci vogliono far credere
intendano aver definito.

#cho: PR del Consiglio sulla Posizione Comune

#Wfo: Il Consiglio sostiene di rendere brevettabili solo le %(q:invenzioni
assistite da elaboratore elettronico).

#dsp: La locuzione %(q:assistita da elaboratore elettronico) è usata
comunemente nell'industria ed è stata usata nella letteratura
brevettuale e in comunicati stampa che sostengono l'approccio della
Commissione/Consiglio. Hanno utilizzato questa locuzione perché
veicola l'intenzione del legislatore nel linguaggio più naturale. La
locuzione %(q:aiutata da elaboratore elettronico) sembra una variante
di %(q:assistita da elaboratore elettronico), probabilmente originata
da una versione francese della stessa locuzione.

#dnc: La locuzione %(q:controllata da elaboratore elettronico) specifica il
modo in cui l'elaboratore interagisce con l'invenzione e lascia
intendere che l'invenzione sia sempre un processo e che l'elaboratore
controlla sempre tale processo. Non è mai stato usato nella
letteratura brevettuale ne dalla retorica di quelli che promuovono la
direttiva. Questi fattori sembrano renderla leggermente meno elegante
e più facile da attaccare di %(q:assistita da elaboratore elettronico)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriCxidef0505 ;
# txtlang: xx ;
# End: ;

