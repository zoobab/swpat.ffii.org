<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Product and Process claims

#descr: Computer-aided inventions can be claimed as products, i.e.
apparatusses, and as processes executed on such apparatusses.  This
provision of the draft software patents directive should concretise
the TRIPs terms %(q:products and processes) in the context of
computer-aided inventions.  It should make as few assumptions as
possible about other matters, such as patentability criteria.

#EWn: Art 27 TRIPs requires the patentability of %(q:products and
processes).  This deserves concretisation in EU law. Computer-aided
inventions can, like other inventions, be claimed as products, i.e.
apparatusses, and as processes executed on such apparatusses.  This
should be stated in the simplest possible terms.  Statements as to
whether software is patentable or not should not be piggybacked onto
this provision.  This provision's importance regarding software lies
in the fact that the %(q:product) in the sense of patent law is an
appratus running a program, not the program as such.  It can be
inferred (and made explicit by an appropriate article 5(2)) that there
is no such thing as a %(q:computer program product) in patent law.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriProdproc0505 ;
# txtlang: xx ;
# End: ;

