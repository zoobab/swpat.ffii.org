<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rafforzare l'UE nella Competizione Internazionale

#descr: La paura di sbilanciamenti regionali e della competizione di paesi
industrializzati emergenti sono sempre stati citati come argomenti a
favore della brevettabilità. Argomenti di questo tipo generalmente non
hanno alcuna giustificazione o sono bilanciati da argomenti contrari
ugualmente validi. Il considerando numero 20 in particolare consiste
di falsi argomenti e sembra essere pensato per mobilitare istinti
protezionistici. Questo considerando è stato introdotto da Arlene
McCarthy e approvato in JURI nel Giugno 2003. I Verdi e Lehne hanno
proposto due differenti metodi per aggiustarlo. Ognuno ha i propri
vantaggi, entrambe sono meritevoli di essere votati favorevolmente.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriProtekt0505 ;
# txtlang: xx ;
# End: ;

