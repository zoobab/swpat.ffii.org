<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Do not codify the EPO's %(q:Further Technical Effect) doctrine!

#descr: What is the %(q:normal interaction between a cook and a recipe)? 
Which effects go beyond this interaction?  The EPO does not know the
answer, and nobody does.  The EPO itself has stated publicly that this
doctrine is meaningless and was only introduced in order to circumvent
the law.  The Council has proposed to use it for circumventing the
Parliament's clear limitation.  In general, only clear and simple
general rules should become law.  Methodological concepts from patent
examination should not be codified even when they are of better
quality than this one.  A remaining question is whether the concerned
Council provisions should be deleted entirely or replaced with a
correcting amendment.  They are so thouroughly flawed that a
correcting amendment will to look very different from the original. 
Possibly existing correction amendments should be subdivided into a
deletion and an insertion.

#aWr: The %(q:normal interaction between programs and computers) is about as
well defined as the %(q:normal interaction between the cook and the
recipe).  It is a legal formula which the EPO invented in 1998 in
order to circumvent Art 52 EPC.  Only two years later, the EPO itself
%(a6:commented) on this formula as follows:

#sWW: There is no need to consider the concept of %(q:further technical
effect) in examination, and it is preferred not to do so for the
following reasons: firstly, it is confusing to both examiners and
applicants; secondly, the only apparent reason for distinguishing
%(q:technical effect) from %(q:further technical effect) in the
decision was because of the presence of %(q:programs for computers) in
the list of exclusions under Article 52(2) EPC. If, as is to be
anticipated, this element is dropped from the list by the Diplomatic
Conference, there will no longer be any basis for such a distinction.
It is to be inferred that the BoA would have preferred to be able to
say that no computer-implemented invention is excluded from
patentability by the provisions of Articles 52(2) and (3) EPC.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriPlutech0505 ;
# txtlang: xx ;
# End: ;

