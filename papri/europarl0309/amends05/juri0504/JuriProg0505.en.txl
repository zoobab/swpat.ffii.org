<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definition of %(q:Data Processing) and %(q:Computer Program)

#descr: In the context of patent claims, %(q:programs for computers) are
achievements in data processing.  This needs to be clarified, since it
is a major source of confusion.  Moreover, it would be a good idea to
clarify what %(q:data processing) and, by inference, %(q:data
processing program) or %(q:computer program) comprises and what not.

#reh: The Council has decided to write wordings from Art 52 EPC into the
directive, together with interpretations which make the same article
meaningless.  The central term in Art 52 EPC is %(q:programs for
computers) or %(q:computer program), or %(q:program for data
processing equipment) (german version %(q:Programm für
Datenverarbeitungsanlagen)).  As an object of patent claims, a
%(q:computer program, characterised by ...) is, like any other claim
object, a potential invention, a solution of a problem, in this case
by means of general purpose data processing equipment.  Once this is
understood, the obscurity that some have created around Art 52 EPC
goes away and clarification is achieved.

#aWW: The term %(q:computer program) has mostly been used in this sense in
the legal literature.  For example, recent decisions of the Federal
Court of Justice interpret %(q:program for computers) as a
%(q:teaching directed toward data processing by means of a suitable
computer. The %(e2:EPO Examination Guidelines of 2000) make the matter
even clearer:

#auf: Programs for computers are a form of %(q:computer-implemented
invention), an expression intended to cover claims which involve
computers, computer networks or other conventional programmable
apparatus whereby prima facie the novel features of the claimed
invention are realised by means of a program or programs. Such claims
may e.g. take the form of a method of operating said conventional
apparatus, the apparatus set up to execute the method, or, following
%(et:T 1173/97), the program itself.

#tir: Unfortunately this fact has been obfuscated by the ministerial patent
officials of the Council, who are trying to interpret %(q:computer
program) as an individual instance of source or binary code which
%(q:can not constitute a patentable invention) simply because it
doesn't fit into the context of patent claims.

#Wat: Some amendments propose to introduce a clarification in this sense as
a new element in the directive, others propose to replace the
Council's Art 4(2) with it.  Art 4(2) represents the EPO's doctrine of
%(pt:further technical effect)  by which Art 52 was made meaningless
in 1998.  This is broken beyond repair and it is logically correct to
replace it with an explanation of %(q:program for computers) in the
sense of the EPO's Guidelines.  Whether this replacement occurs
directly (as proposed by Kudrycka) or by deletion and insertion (as
proposed by Kauppi) is a procedural technicality about which we have
no particular preference.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriProg0505 ;
# txtlang: xx ;
# End: ;

