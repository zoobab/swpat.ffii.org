<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Replace %(q:invention) with %(q:innovation) where patentability is not
established

#descr: An %(q:invention) in the sense of patent law is an innovation (a
solution, a teaching) which fulfills the requirements of statutory
subject matter as laid down (for example) in Art 52 EPC.  The text of
the EPO supporters often uses the term %(q:invention) in contexts
where patentability has yet to be tested, thus contributing to the
confusion of patentability requirements which has progressed in EPO
caselaw since the 1990s, especially since IBM 1 & 2 and Controlling
Pension Benefits System.  This must be corrected rather than codified.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriPatinv0505 ;
# txtlang: xx ;
# End: ;

