<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Positive and negative definition of %(q:Industry)

#descr: According to Art 27 TRIPs, %(q:inventions in all fields of technology)
are patentable, provided they .. are %(q:susceptible of industrial
application).  The claim of universality of the patent system is
limited only by three terms, among them %(q:industry).  This
necessitates definitions of these three terms, in positive and
negative ways if possible.  The directive text uses the term
%(q:industry) in several places to create an impression that
patentability is being limited.  In fact the traditional concept of
distinguishing the primary, secondary and tertiary sector, which is at
the basis of the %(q:industry) concept, is meaningful in this context,
and the chance to write this into law should not be missed, although
it is not as central as the definitions of %(q:technical) and
%(q:invention).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriIndustr0505 ;
# txtlang: xx ;
# End: ;

