\begin{subdocument}{JuriPlutech0505}{Do not codify the EPO's ``Further Technical Effect'' doctrine!}{http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/plutech.en.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2005-03-20 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{What is the ``normal interaction between a cook and a recipe''?  Which effects go beyond this interaction?  The EPO does not know the answer, and nobody does.  The EPO itself has stated publicly that this doctrine is meaningless and was only introduced in order to circumvent the law.  The Council has proposed to use it for circumventing the Parliament's clear limitation.  In general, only clear and simple general rules should become law.  Methodological concepts from patent examination should not be codified even when they are of better quality than this one.  A remaining question is whether the concerned Council provisions should be deleted entirely or replaced with a correcting amendment.  They are so thouroughly flawed that a correcting amendment will to look very different from the original.  Possibly existing correction amendments should be subdivided into a deletion and an insertion.}
\input{67.en}

\input{107.en}

\input{108.en}

\input{109.en}

\input{226.en}

\input{227.en}

The ``normal interaction between programs and computers'' is about as well defined as the ``normal interaction between the cook and the recipe''.  It is a legal formula which the EPO invented in 1998 in order to circumvent Art 52 EPC.  Only two years later, the EPO itself commented\footnote{\url{http://swpat.ffii.org/papers/epo-tws-app6/index.en.html}} on this formula as follows:

\begin{quote}
{\it ``There is no need to consider the concept of ``further technical effect'' in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing ``technical effect'' from ``further technical effect'' in the decision was because of the presence of ``programs for computers'' in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.''}
\end{quote}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
% mode: latex ;
% End: ;

