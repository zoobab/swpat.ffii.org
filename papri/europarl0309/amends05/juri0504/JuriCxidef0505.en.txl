<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Defining %(q:Computer Aided/Implemented Invention)

#descr: A computer-aided invention is a patentable innovation the performance
of which involves the use of a programmable apparatus.  A
%(q:computer-implemented invention) is nothing but a program for
computers in the context of patent claims and therefore not an
invention within the meaning of patent law.  Yet, for transition
purposes, the term %(q:computer-implemented invention) must be
redefined as a synonym of %(q:computer-aided invention).

#qhd: The European Patent Office (EPO) began to declare unpatentable subject
matter as patentable by simply defining it to be an %(q:invention).  
A generic computing solution, called %(q:program for computers) in the
language of the European Patent Convention and excluded from
patentability, is implicitely declared patentable by calling it
%(q:computer-implemented invention).  In public communication, the
defenders of this position however pretend that %(q:CII) refers to
%(q:computer-aided) systems, such as washing machines or battery
chargers operating running under computer control.

#ene: Several groups have proposed ways to resolve the conflict.  It is
unavoidable to use both the more appropriate term %(q:computer-aided
invention) and the legacy term %(q:computer-implemented invention) and
to define them in the way in which the EPO supporters want to make us
believe they are defined.

#cho: Council PR on the Common Position

#Wfo: The Council says it is making only %(q:computer-aided inventions)
patentable.

#dsp: The term %(q:computer-aided) is commonly used in the industry and has
been used in the patent literature and in press releases which argue
in favor of the Commission/Council approach.  They have used this term
because it conveys the legislators intention in the most natural
language. The term %(q:computer-assisted) seems to be a variant of
%(q:computer-aided), possibly originating from a French version of the
same term.

#dnc: The term %(q:computer-controlled) specifies the way in which the
computer interacts with the invention and lets people think that the
invention is always a process and that the computer always controls
the whole process.  It has not been used either in the patent
literature or in the rhetoric of those who promote the directive. 
These factors seem to make it slightly less elegant and easier to
attack than %(q:computer-aided).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriCxidef0505 ;
# txtlang: xx ;
# End: ;

