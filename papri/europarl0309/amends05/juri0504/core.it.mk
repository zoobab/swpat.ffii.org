# -*- mode: makefile -*-

core.it.lstex:
	lstex core.it | sort -u > core.it.lstex
core.it.mk:	core.it.lstex
	vcat /ul/prg/RC/texmake > core.it.mk


core.it.dvi:	core.it.mk
	rm -f core.it.lta
	if latex core.it;then test -f core.it.lta && latex core.it;while tail -n 20 core.it.log | grep -w references && latex core.it;do eval;done;fi
	if test -r core.it.idx;then makeindex core.it && latex core.it;fi

core.it.pdf:	core.it.ps
	if grep -w '\(CJK\|epsfig\)' core.it.tex;then ps2pdf core.it.ps;else rm -f core.it.lta;if pdflatex core.it;then test -f core.it.lta && pdflatex core.it;while tail -n 20 core.it.log | grep -w references && pdflatex core.it;do eval;done;fi;fi
	if test -r core.it.idx;then makeindex core.it && pdflatex core.it;fi
core.it.tty:	core.it.dvi
	dvi2tty -q core.it > core.it.tty
core.it.ps:	core.it.dvi	
	dvips  core.it
core.it.001:	core.it.dvi
	rm -f core.it.[0-9][0-9][0-9]
	dvips -i -S 1  core.it
core.it.pbm:	core.it.ps
	pstopbm core.it.ps
core.it.gif:	core.it.ps
	pstogif core.it.ps
core.it.eps:	core.it.dvi
	dvips -E -f core.it > core.it.eps
core.it-01.g3n:	core.it.ps
	ps2fax n core.it.ps
core.it-01.g3f:	core.it.ps
	ps2fax f core.it.ps
core.it.ps.gz:	core.it.ps
	gzip < core.it.ps > core.it.ps.gz
core.it.ps.gz.uue:	core.it.ps.gz
	uuencode core.it.ps.gz core.it.ps.gz > core.it.ps.gz.uue
core.it.faxsnd:	core.it-01.g3n
	set -a;FAXRES=n;FILELIST=`echo core.it-??.g3n`;source faxsnd main
core.it_tex.ps:	
	cat core.it.tex | splitlong | coco | m2ps > core.it_tex.ps
core.it_tex.ps.gz:	core.it_tex.ps
	gzip < core.it_tex.ps > core.it_tex.ps.gz
core.it-01.pgm:
	cat core.it.ps | gs -q -sDEVICE=pgm -sOutputFile=core.it-%02d.pgm -
core.it/core.it.html:	core.it.dvi
	rm -fR core.it;latex2html core.it.tex
xview:	core.it.dvi
	xdvi -s 8 core.it &
tview:	core.it.tty
	browse core.it.tty 
gview:	core.it.ps
	ghostview  core.it.ps &
print:	core.it.ps
	lpr -s -h core.it.ps 
sprint:	core.it.001
	for F in core.it.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	core.it.faxsnd
	faxsndjob view core.it &
fsend:	core.it.faxsnd
	faxsndjob jobs core.it
viewgif:	core.it.gif
	xv core.it.gif &
viewpbm:	core.it.pbm
	xv core.it-??.pbm &
vieweps:	core.it.eps
	ghostview core.it.eps &	
clean:	core.it.ps
	rm -f  core.it-*.tex core.it.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} core.it-??.* core.it_tex.* core.it*~
core.it.tgz:	clean
	set +f;LSFILES=`cat core.it.ls???`;FILES=`ls core.it.* $$LSFILES | sort -u`;tar czvf core.it.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
