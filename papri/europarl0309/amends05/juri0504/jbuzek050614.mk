# -*- mode: makefile -*-

jbuzek050614.lstex:
	lstex jbuzek050614 | sort -u > jbuzek050614.lstex
jbuzek050614.mk:	jbuzek050614.lstex
	vcat /ul/prg/RC/texmake > jbuzek050614.mk


jbuzek050614.dvi:	jbuzek050614.mk jbuzek050614.tex
	rm -f jbuzek050614.lta
	if latex jbuzek050614;then test -f jbuzek050614.lta && latex jbuzek050614;while tail -n 20 jbuzek050614.log | grep -w references && latex jbuzek050614;do eval;done;fi
	if test -r jbuzek050614.idx;then makeindex jbuzek050614 && latex jbuzek050614;fi

jbuzek050614.pdf:	jbuzek050614.ps
	if grep -w '\(CJK\|epsfig\)' jbuzek050614.tex;then ps2pdf jbuzek050614.ps;else rm -f jbuzek050614.lta;if pdflatex jbuzek050614;then test -f jbuzek050614.lta && pdflatex jbuzek050614;while tail -n 20 jbuzek050614.log | grep -w references && pdflatex jbuzek050614;do eval;done;fi;fi
	if test -r jbuzek050614.idx;then makeindex jbuzek050614 && pdflatex jbuzek050614;fi
jbuzek050614.tty:	jbuzek050614.dvi
	dvi2tty -q jbuzek050614 > jbuzek050614.tty
jbuzek050614.ps:	jbuzek050614.dvi	
	dvips  jbuzek050614
jbuzek050614.001:	jbuzek050614.dvi
	rm -f jbuzek050614.[0-9][0-9][0-9]
	dvips -i -S 1  jbuzek050614
jbuzek050614.pbm:	jbuzek050614.ps
	pstopbm jbuzek050614.ps
jbuzek050614.gif:	jbuzek050614.ps
	pstogif jbuzek050614.ps
jbuzek050614.eps:	jbuzek050614.dvi
	dvips -E -f jbuzek050614 > jbuzek050614.eps
jbuzek050614-01.g3n:	jbuzek050614.ps
	ps2fax n jbuzek050614.ps
jbuzek050614-01.g3f:	jbuzek050614.ps
	ps2fax f jbuzek050614.ps
jbuzek050614.ps.gz:	jbuzek050614.ps
	gzip < jbuzek050614.ps > jbuzek050614.ps.gz
jbuzek050614.ps.gz.uue:	jbuzek050614.ps.gz
	uuencode jbuzek050614.ps.gz jbuzek050614.ps.gz > jbuzek050614.ps.gz.uue
jbuzek050614.faxsnd:	jbuzek050614-01.g3n
	set -a;FAXRES=n;FILELIST=`echo jbuzek050614-??.g3n`;source faxsnd main
jbuzek050614_tex.ps:	
	cat jbuzek050614.tex | splitlong | coco | m2ps > jbuzek050614_tex.ps
jbuzek050614_tex.ps.gz:	jbuzek050614_tex.ps
	gzip < jbuzek050614_tex.ps > jbuzek050614_tex.ps.gz
jbuzek050614-01.pgm:
	cat jbuzek050614.ps | gs -q -sDEVICE=pgm -sOutputFile=jbuzek050614-%02d.pgm -
jbuzek050614/jbuzek050614.html:	jbuzek050614.dvi
	rm -fR jbuzek050614;latex2html jbuzek050614.tex
xview:	jbuzek050614.dvi
	xdvi -s 8 jbuzek050614 &
tview:	jbuzek050614.tty
	browse jbuzek050614.tty 
gview:	jbuzek050614.ps
	ghostview  jbuzek050614.ps &
print:	jbuzek050614.ps
	lpr -s -h jbuzek050614.ps 
sprint:	jbuzek050614.001
	for F in jbuzek050614.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	jbuzek050614.faxsnd
	faxsndjob view jbuzek050614 &
fsend:	jbuzek050614.faxsnd
	faxsndjob jobs jbuzek050614
viewgif:	jbuzek050614.gif
	xv jbuzek050614.gif &
viewpbm:	jbuzek050614.pbm
	xv jbuzek050614-??.pbm &
vieweps:	jbuzek050614.eps
	ghostview jbuzek050614.eps &	
clean:	jbuzek050614.ps
	rm -f  jbuzek050614-*.tex jbuzek050614.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} jbuzek050614-??.* jbuzek050614_tex.* jbuzek050614*~
jbuzek050614.tgz:	clean
	set +f;LSFILES=`cat jbuzek050614.ls???`;FILES=`ls jbuzek050614.* $$LSFILES | sort -u`;tar czvf jbuzek050614.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
