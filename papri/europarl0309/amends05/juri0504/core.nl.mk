# -*- mode: makefile -*-

core.nl.lstex:
	lstex core.nl | sort -u > core.nl.lstex
core.nl.mk:	core.nl.lstex
	vcat /ul/prg/RC/texmake > core.nl.mk


core.nl.dvi:	core.nl.mk
	rm -f core.nl.lta
	if latex core.nl;then test -f core.nl.lta && latex core.nl;while tail -n 20 core.nl.log | grep -w references && latex core.nl;do eval;done;fi
	if test -r core.nl.idx;then makeindex core.nl && latex core.nl;fi

core.nl.pdf:	core.nl.ps
	if grep -w '\(CJK\|epsfig\)' core.nl.tex;then ps2pdf core.nl.ps;else rm -f core.nl.lta;if pdflatex core.nl;then test -f core.nl.lta && pdflatex core.nl;while tail -n 20 core.nl.log | grep -w references && pdflatex core.nl;do eval;done;fi;fi
	if test -r core.nl.idx;then makeindex core.nl && pdflatex core.nl;fi
core.nl.tty:	core.nl.dvi
	dvi2tty -q core.nl > core.nl.tty
core.nl.ps:	core.nl.dvi	
	dvips  core.nl
core.nl.001:	core.nl.dvi
	rm -f core.nl.[0-9][0-9][0-9]
	dvips -i -S 1  core.nl
core.nl.pbm:	core.nl.ps
	pstopbm core.nl.ps
core.nl.gif:	core.nl.ps
	pstogif core.nl.ps
core.nl.eps:	core.nl.dvi
	dvips -E -f core.nl > core.nl.eps
core.nl-01.g3n:	core.nl.ps
	ps2fax n core.nl.ps
core.nl-01.g3f:	core.nl.ps
	ps2fax f core.nl.ps
core.nl.ps.gz:	core.nl.ps
	gzip < core.nl.ps > core.nl.ps.gz
core.nl.ps.gz.uue:	core.nl.ps.gz
	uuencode core.nl.ps.gz core.nl.ps.gz > core.nl.ps.gz.uue
core.nl.faxsnd:	core.nl-01.g3n
	set -a;FAXRES=n;FILELIST=`echo core.nl-??.g3n`;source faxsnd main
core.nl_tex.ps:	
	cat core.nl.tex | splitlong | coco | m2ps > core.nl_tex.ps
core.nl_tex.ps.gz:	core.nl_tex.ps
	gzip < core.nl_tex.ps > core.nl_tex.ps.gz
core.nl-01.pgm:
	cat core.nl.ps | gs -q -sDEVICE=pgm -sOutputFile=core.nl-%02d.pgm -
core.nl/core.nl.html:	core.nl.dvi
	rm -fR core.nl;latex2html core.nl.tex
xview:	core.nl.dvi
	xdvi -s 8 core.nl &
tview:	core.nl.tty
	browse core.nl.tty 
gview:	core.nl.ps
	ghostview  core.nl.ps &
print:	core.nl.ps
	lpr -s -h core.nl.ps 
sprint:	core.nl.001
	for F in core.nl.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	core.nl.faxsnd
	faxsndjob view core.nl &
fsend:	core.nl.faxsnd
	faxsndjob jobs core.nl
viewgif:	core.nl.gif
	xv core.nl.gif &
viewpbm:	core.nl.pbm
	xv core.nl-??.pbm &
vieweps:	core.nl.eps
	ghostview core.nl.eps &	
clean:	core.nl.ps
	rm -f  core.nl-*.tex core.nl.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} core.nl-??.* core.nl_tex.* core.nl*~
core.nl.tgz:	clean
	set +f;LSFILES=`cat core.nl.ls???`;FILES=`ls core.nl.* $$LSFILES | sort -u`;tar czvf core.nl.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
