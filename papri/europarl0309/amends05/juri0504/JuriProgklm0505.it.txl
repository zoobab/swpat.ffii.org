<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rivendicazioni su Programmi

#descr: L'articolo 5 comma 2 del Consiglio dice che rivendicazioni su
programmi non possono essere ammessi %(q:a meno che ... [condizione
non chiara, che sotto attento esame, si rivela essere sempre vera]).
L'unica risposta corretta è di affermare chiaramente che le
rivendicazioni su programmi non sono ammesse.  Lehne e altri
propongono di permettere le rivendicazioni su programmi limitandole
con una cosmesi di emendamenti di facciata che pretendono che esse non
abbiano il significato e l'effetto che invece necessariamente hanno,
creando una legislazione opaca e paludosa che serve solo a scopi
temporanei di persuasione politica.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriProgklm0505 ;
# txtlang: xx ;
# End: ;

