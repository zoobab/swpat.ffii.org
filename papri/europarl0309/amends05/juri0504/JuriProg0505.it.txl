<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Definizione di %(q:Elaborazione Dati) e %(q:Programma per elaboratore)

#descr: Nel contesto delle rivendicazioni di brevetto, i %(q:programmi per
elaboratore) sono risultati raggiunti nell'elaborazione di dati.
Questo va chiarito, in quanto è una evidente sorgente di confusione.
Inoltre, sarebbe un'ottima idea chiarire che cosa comprenda o non
comprenda l'%(q:elaborazione dati) e, di conseguenza, %(q:programma
per elaborazione dati) o %(q:programma per elaboratore).

#reh: Il Consiglio ha deciso di riportare frasi dall'articolo della 52 EPC
nella direttiva, insieme ad interpretazioni che rendono l'articolo
privo di significato. Il termine centrale nell'articolo 52 della EPC è
%(q:programmi per elaboratore) o %(q:programmi per dispositivi di
elaborazione dati) (la versione tedesca è %(q:Programm für
Datenverarbeitungsanlagen)). Come oggetto di rivendicazione di
brevetto, un %(q: programma per elaboratore, caratterizzato da ...) è,
come per ogni altra rivendicazione, una potenziale invenzione, una
soluzione ad un problema, in questo caso attraverso dispositivi
generici di elaborazione dati. Una volta che si capisce questo, la
confusione creata ad arte attorno all'articolo 52 della EPC di
dissolve e si riesce a chiarire il concetto.

#aWW: Il termine %(q:programma per elaboratore) è stato prevalentemente
usato con questo significato nella letteratura legale. Per esempio,
recenti decisioni della Corte federale di Giustizia interpretano
%(q:programmi per elaboratore) come un %(q:insegnamento
sull'elaborazione dati per mezzo di un elaboratore appropriato). Le
%(e2: Linee Guide per l'Esame del 2000 dell'UBE)) rendono questa cosa
ancora più chiara:

#auf: I programmi per computer sono una forma di %(q:invenzioni attuate per
mezzo di elaboratori elettronici), una espressione intesa a coprire
rivendicazioni che coinvolgono elaboratori, reti di elaboratori o
altri apparati programmabili convenzionali nei quali prima facie le
caratteristiche di novità della invenzione rivendicata sono realizzate
attraverso uno o più programmi. Tali rivendicazioni possono per
esempio prendere la forma di un metodo per utilizzare tali apparati
convenzionali, l'apparato è impostato per eseguire il metodo o,
seguendo %(et:T 1173/97), il programma stesso

#tir: Sfortunatamente questo fatto è stato offuscato dai delegati,
incaricati dai ministeri per la materia brevettuale, del Consiglio che
stanno cercando di interpretare %(q:programma per elaboratore) come
una istanza individuale di codice sorgente o binario che %(q:non può
costituire una invenzione brevettabile) semplicemente perché non trova
posto nel contesto delle rivendicazioni di brevetto.

#Wat: Alcuni emendamenti propongono di introdurre chiarimenti in merito
attraverso un nuovo elemento della direttiva, altri propongono di
rimpiazzare l'articolo 4 comma 2 del Consiglio. L'articolo 4 comma 2
rappresenta la dottrina dell'UBE sull'%(q:ulteriore effetto tecnico)
attraverso la quale l'articolo 52 fu reso privo di senso nel 1998.
Questo articolo è irrecuperabile ed è logicamente corretto sostituirlo
con una spiegazione di %(q:programma per elaboratore) ai sensi delle
linee Guida dell'UBE. Che questo articolo venga rimpiazzato
direttamente (come proposto da Kudrycka) o per cancellazione e
inserzione (come proposto da Kauppi) è un tecnicismo procedurale sul
quale non abbiamo particolari preferenze.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriProg0505 ;
# txtlang: xx ;
# End: ;

