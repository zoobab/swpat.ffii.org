# -*- mode: makefile -*-

ffiiepp050615.it.lstex:
	lstex ffiiepp050615.it | sort -u > ffiiepp050615.it.lstex
ffiiepp050615.it.mk:	ffiiepp050615.it.lstex
	vcat /ul/prg/RC/texmake > ffiiepp050615.it.mk


ffiiepp050615.it.dvi:	ffiiepp050615.it.mk
	rm -f ffiiepp050615.it.lta
	if latex ffiiepp050615.it;then test -f ffiiepp050615.it.lta && latex ffiiepp050615.it;while tail -n 20 ffiiepp050615.it.log | grep -w references && latex ffiiepp050615.it;do eval;done;fi
	if test -r ffiiepp050615.it.idx;then makeindex ffiiepp050615.it && latex ffiiepp050615.it;fi

ffiiepp050615.it.pdf:	ffiiepp050615.it.ps
	if grep -w '\(CJK\|epsfig\)' ffiiepp050615.it.tex;then ps2pdf ffiiepp050615.it.ps;else rm -f ffiiepp050615.it.lta;if pdflatex ffiiepp050615.it;then test -f ffiiepp050615.it.lta && pdflatex ffiiepp050615.it;while tail -n 20 ffiiepp050615.it.log | grep -w references && pdflatex ffiiepp050615.it;do eval;done;fi;fi
	if test -r ffiiepp050615.it.idx;then makeindex ffiiepp050615.it && pdflatex ffiiepp050615.it;fi
ffiiepp050615.it.tty:	ffiiepp050615.it.dvi
	dvi2tty -q ffiiepp050615.it > ffiiepp050615.it.tty
ffiiepp050615.it.ps:	ffiiepp050615.it.dvi	
	dvips  ffiiepp050615.it
ffiiepp050615.it.001:	ffiiepp050615.it.dvi
	rm -f ffiiepp050615.it.[0-9][0-9][0-9]
	dvips -i -S 1  ffiiepp050615.it
ffiiepp050615.it.pbm:	ffiiepp050615.it.ps
	pstopbm ffiiepp050615.it.ps
ffiiepp050615.it.gif:	ffiiepp050615.it.ps
	pstogif ffiiepp050615.it.ps
ffiiepp050615.it.eps:	ffiiepp050615.it.dvi
	dvips -E -f ffiiepp050615.it > ffiiepp050615.it.eps
ffiiepp050615.it-01.g3n:	ffiiepp050615.it.ps
	ps2fax n ffiiepp050615.it.ps
ffiiepp050615.it-01.g3f:	ffiiepp050615.it.ps
	ps2fax f ffiiepp050615.it.ps
ffiiepp050615.it.ps.gz:	ffiiepp050615.it.ps
	gzip < ffiiepp050615.it.ps > ffiiepp050615.it.ps.gz
ffiiepp050615.it.ps.gz.uue:	ffiiepp050615.it.ps.gz
	uuencode ffiiepp050615.it.ps.gz ffiiepp050615.it.ps.gz > ffiiepp050615.it.ps.gz.uue
ffiiepp050615.it.faxsnd:	ffiiepp050615.it-01.g3n
	set -a;FAXRES=n;FILELIST=`echo ffiiepp050615.it-??.g3n`;source faxsnd main
ffiiepp050615.it_tex.ps:	
	cat ffiiepp050615.it.tex | splitlong | coco | m2ps > ffiiepp050615.it_tex.ps
ffiiepp050615.it_tex.ps.gz:	ffiiepp050615.it_tex.ps
	gzip < ffiiepp050615.it_tex.ps > ffiiepp050615.it_tex.ps.gz
ffiiepp050615.it-01.pgm:
	cat ffiiepp050615.it.ps | gs -q -sDEVICE=pgm -sOutputFile=ffiiepp050615.it-%02d.pgm -
ffiiepp050615.it/ffiiepp050615.it.html:	ffiiepp050615.it.dvi
	rm -fR ffiiepp050615.it;latex2html ffiiepp050615.it.tex
xview:	ffiiepp050615.it.dvi
	xdvi -s 8 ffiiepp050615.it &
tview:	ffiiepp050615.it.tty
	browse ffiiepp050615.it.tty 
gview:	ffiiepp050615.it.ps
	ghostview  ffiiepp050615.it.ps &
print:	ffiiepp050615.it.ps
	lpr -s -h ffiiepp050615.it.ps 
sprint:	ffiiepp050615.it.001
	for F in ffiiepp050615.it.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	ffiiepp050615.it.faxsnd
	faxsndjob view ffiiepp050615.it &
fsend:	ffiiepp050615.it.faxsnd
	faxsndjob jobs ffiiepp050615.it
viewgif:	ffiiepp050615.it.gif
	xv ffiiepp050615.it.gif &
viewpbm:	ffiiepp050615.it.pbm
	xv ffiiepp050615.it-??.pbm &
vieweps:	ffiiepp050615.it.eps
	ghostview ffiiepp050615.it.eps &	
clean:	ffiiepp050615.it.ps
	rm -f  ffiiepp050615.it-*.tex ffiiepp050615.it.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} ffiiepp050615.it-??.* ffiiepp050615.it_tex.* ffiiepp050615.it*~
ffiiepp050615.it.tgz:	clean
	set +f;LSFILES=`cat ffiiepp050615.it.ls???`;FILES=`ls ffiiepp050615.it.* $$LSFILES | sort -u`;tar czvf ffiiepp050615.it.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
