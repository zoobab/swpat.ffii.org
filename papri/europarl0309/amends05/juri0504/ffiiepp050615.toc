\contentsline {chapter}{\numberline {1}Why do we want to exclude patents on software and business methods?}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}What's wrong with the ``Common Position''?}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}The basic choice: Natural Science vs Exact Science}{6}{chapter.3}
\contentsline {chapter}{\numberline {4}Patenting computer tomography inventions under the ``Ten Clarifications''}{7}{chapter.4}
\contentsline {chapter}{\numberline {A}Ten Core Clarifications for the Software Patent Directive}{9}{appendix.A}
