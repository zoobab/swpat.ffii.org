<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Negative Definitions of %(q:Technical) and %(q:Field of Technology)

#descr: TRIPs turns the concepts of %(q:field of technology) into the main key
for limiting patentability.  Concretisation of this concept is
therefore required at the national level.  The first step to
concretisation is a negative definition: list examples of what is
%(e:not) %(q:technical) and what is %(e:not) a %(q:field of
technology).

#negdef: Negative definitions are very popular in lawmaking, because they are
less pretentious and more time-enduring than positive definitions. 
Art 52 of the European Patent Conventions of 1973 uses the same
approach: it defines what is %(e:not) an invention in the sense of the
law.  After the introduction of new language by the %(tr:Treaty on
Trade-Related Aspects of Intellectual Property) of 1994, the
legislator now faces the task of translating this approach into new
language, so as to clarify the relation between Art 52 EPC and Art 27
TRIPs.  This can be understood to be the single most central problem
to be solved by the present directive.  The solution is to be found in
one simple sentence: data processing is not a field of technology.

#datproc: Data processing is an abstract endeavour: all data processing
innovation is in the realm of logical abstraction.  In the context of
data processing, %(q:technial) is therefore at best a synonym of
%(q:trivial).  Failure to assert that data processing is not a field
of technology in the sense of TRIPs means failure to do the
legislator's most basic homework.  Any attempt at positive definition
that is not based on the recognition of this first simple negative
definition will be muddled and half-hearted.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriNetech0505 ;
# txtlang: xx ;
# End: ;

