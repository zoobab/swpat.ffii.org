<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Esenzione dalla Protezione Brevettuale per l'Interoperabilità

#descr: Molti gruppi hanno proposto emendamenti che escludono o pretendono di
escludere dalla protezione brevettuale l'interoperazione tra due
sistemi. Visto che l'obiettivo principale della direttiva è di
chiarire le regole per la concessione piuttosto che per la protezione
questa è veramente una questione marginale, e la mancanza di tempo
prima della seconda lettura non permettono al Parlamento di studiarla
a fondo. Non c'è alcun motivo valido per non mantenere la versione
della prima lettura. Molte delle varianti sono ugualmente accettabili.
Una definizione separata di interoperabilità è comunque utile. Ad ogni
modo, molte delle varianti proposte servono a stemperare questa
esclusione per esempio rendendola dipendente da nozioni vaghe quali
%(q:soggetto dominante), la cui applicazione dipende da costosi
ricorsi sulla mancanza di competizione, o escludendo il software
libero/open source (con formule di licenza di tipo apparentemente
%(q:ragionevole e non discriminatorio  )), o introducendo i termini
astratti dell'articolo 30 dei TRIPs direttamente nella legislazione
Europea come condizione limitante.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriInterop0505 ;
# txtlang: xx ;
# End: ;

