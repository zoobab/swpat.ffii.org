<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2005-05-04 JURI Amendementen op de Softwareoctrooirichtlijn

#descr: Gestructureerde presentatie van amendementen opgesteld voor het 
besluit over de softwareoctrooirichtlijn in de Commissie juridische 
zaken (JURI) op 4 mei 2005

#submit: ingediend door

#artrec: bestemming

#lang: taal

#subdok: onderwerp

#rekom: aanbeveling

#kritik: kritiek

#vot: stem

#amnr: nr.

#votlist: Stemlijst

#amdetal: Amendementen in Detail

#AWp: De Kernverduidelijkingen die gemaakt moeten worden door de Richtlijn

#doT: Amendementen Gegroepeerd naar Onderwerp

#CxidefT: Definitie van %(q:door Computers Ondersteunde Uitvinding)

#cxidef: Een %(q:door computers ondersteunde uitvinding) [, ook wel 
ontoepasselijk %(q:in computers geïmplementeerde uitvinding) genoemd,]
 is een uitvinding in octrooirechtelijke zin waar in de werking ervan 
gebruik wordt gemaakt van een programmeerbaar apparaat.

#ProgT: Definitie van %(q:computerprogramma)

#prog: Een %(q:computer) is een realisatie van een abstracte machine die 
bestaat uit bestanddelen zoals in- en uitvoer, processor, geheugen, 
opslagruimte en interfaces voor informatieuitwisseling met externe 
systemen en menselijke gebruikers. %(q:Gegevensverwerking) is rekenen 
met abstracte bestanddelen van computers. Een %(q:computerprogramma)
is  een oplossing van een probleem door middel van gegevensverwerking,
die,  zodra het correct beschreven is in een passende taal, door een
computer  uitgevoerd kan worden.

#ProdprocT: Objecten van Product- en Procesaanspraken

#prodproc: Op een door computers ondersteunde uitvinding mag aanspraak  worden
gemaakt als een product, d.w.z. als een geprogrammeerd apparaat,  of
als een proces uitgevoerd door een dergelijk apparaat.

#ProgklmT: Uitsluiting van Programmaaanspraken

#progklm: Een octrooiaanspraak op een computerprogramma, hetzij als  zodanig of
op een drager, zal niet worden toegestaan.

#PublT: Publicatievrijheid

#publ: Het publiceren of verspreiden van informatie kan nooit een inbreuk 
maken op een octrooi.

#NetechT: Negatieve definitie van %(q:Gebied van de Technologie)

#netech: Gegevensverwerking is niet een gebied van de technologie.

#TechT: Positieve Definitie van %(q:Technisch) en %(q:Gebied van de 
Technologie)

#tech: %(q:Technologie) is toegepaste natuurwetenschap. Een gebied van de 
technologie is een discipline van natuurwetenschap waarin kennis wordt
 verkregen door het experimenteren met controleerbare natuurkrachten. 
%(q:Technisch) betekent %(q:behorend tot een gebied van de
technologie).

#NekontribT: Negatieve Definitie van %(q:Bijdrage)

#nekontrib: Een verbetering in efficiëntie van gegevensverwerking is  geen
technische bijdrage.

#KontribT: Positieve Definitie van %(q:Bijdrage) en %(q:Uitvinding)

#kontrib: Een %(q:uitvinding) is een bijdrage aan de stand van de  techniek op
een gebied van de technologie. De bijdrage is het geheel aan 
kenmerken waarin de omvang van de octrooiaanspraak als geheel wordt 
aangenomen te verschillen met de stand van de techniek. De bijdrage
moet  een technische zijn, d.w.z. het moet technische kenmerken
bevatten en  behoren tot een gebied van de technologie. Zonder een
technische  bijdrage, is er geen octrooieerbare onderwerpmaterie en
geen uitvinding.  De technische bijdrage moet voldoen aan de eisen van
octrooieerbaarheid.  In bijzonder moet de technische bijdrage nieuw
zijn en niet voor de hand  liggen voor een deskundige.

#InteropT: Interoperatievrijheid

#interop: Wanneer het gebruik van een geoctrooieerde techniek  noodzakelijk is
om interoperabiliteit te waarborgen, zal zulk gebruik  niet beschouwd
worden als een inbreuk op een octrooi.

#tTf: Tien Kernverduidelijkingen

#HxW: slides van de presentatie gegeven door Hartmut Pilch op de 
%(bk:Brusselse Conferentie) van 2005-06-01

#WgW: om af te drukken op één pagina

#vnp: Hoe de Doelen te Bereiken van het %(q:Gemeenschappelijke  Standpunt):
Opties en Compromispotentieel

#rci: Paper voor discussies in de EVP

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/juri0505.el ;
# mailto: jdbuiso@cs.vu.nl ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: juri0505 ;
# txtlang: xx ;
# End: ;

