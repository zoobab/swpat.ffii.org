\begin{subdocument}{JuriProg0505}{Definition of ``Data Processing'' and ``Computer Program''}{http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/prog.en.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2005-03-20 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{In the context of patent claims, ``programs for computers'' are achievements in data processing.  This needs to be clarified, since it is a major source of confusion.  Moreover, it would be a good idea to clarify what ``data processing'' and, by inference, ``data processing program'' or ``computer program'' comprises and what not.}
\begin{sect}{intro}{Introduction}
The Council has decided to write wordings from Art 52 EPC into the directive, together with interpretations which make the same article meaningless.  The central term in Art 52 EPC is ``programs for computers'' or ``computer program'', or ``program for data processing equipment'' (german version ``Programm f\"{u}r Datenverarbeitungsanlagen'').  As an object of patent claims, a ``computer program, characterised by ...'' is, like any other claim object, a potential invention, a solution of a problem, in this case by means of general purpose data processing equipment.  Once this is understood, the obscurity that some have created around Art 52 EPC goes away and clarification is achieved.

The term ``computer program'' has mostly been used in this sense in the legal literature.  For example, recent decisions of the Federal Court of Justice interpret ``program for computers'' as a ``teaching directed toward data processing by means of a suitable computer. The EPO Examination Guidelines of 2000\footnote{\url{http://www.european-patent-office.org/legal/gui_lines/e/c_iv_2_3_6.htm}} make the matter even clearer''

\begin{quote}
{\it Programs for computers are a form of ``computer-implemented invention'', an expression intended to cover claims which involve computers, computer networks or other conventional programmable apparatus whereby prima facie the novel features of the claimed invention are realised by means of a program or programs. Such claims may e.g. take the form of a method of operating said conventional apparatus, the apparatus set up to execute the method, or, following T 1173/97\footnote{\url{http://swpat.ffii.org/papers/epo-t971173/index.en.html}}, the program itself.}
\end{quote}

Unfortunately this fact has been obfuscated by the ministerial patent officials of the Council, who are trying to interpret ``computer program'' as an individual instance of source or binary code which ``can not constitute a patentable invention'' simply because it doesn't fit into the context of patent claims.

see also Art 52 EPC: Interpretation and Revision\footnote{\url{http://swpat.ffii.org/analysis/epc52/index.en.html}}

Some amendments propose to introduce a clarification in this sense as a new element in the directive, others propose to replace the Council's Art 4(2) with it.  Art 4(2) represents the EPO's doctrine of further technical effect\footnote{\url{http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/plutech.en.html}}  by which Art 52 was made meaningless in 1998.  This is broken beyond repair and it is logically correct to replace it with an explanation of ``program for computers'' in the sense of the EPO's Guidelines.  Whether this replacement occurs directly (as proposed by Kudrycka) or by deletion and insertion (as proposed by Kauppi) is a procedural technicality about which we have no particular preference.
\end{sect}

\begin{sect}{votlist}{Voting List}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
no. & submitted by & vote & critique\\\hline
31\footnote{\url{am31JuriProg0505}} & Rocard & o & \input{31-krit.en}\\\hline
97\footnote{\url{am97JuriProg0505}} & Ortega & + & \input{97-krit.en}\\\hline
99\footnote{\url{am99JuriProg0505}} 100\footnote{\url{am100JuriProg0505}} 101\footnote{\url{am101JuriProg0505}} & Kudrycka, Zwiefka; Bertinotti; Kauppi & ++ & \input{99-krit.en}\\\hline
102\footnote{\url{am102JuriProg0505}} & Manders & - & \input{102-krit.en}\\\hline
103\footnote{\url{am103JuriProg0505}} & Lichtenberger, Frassoni & o & \input{103-krit.en}\\\hline
104\footnote{\url{am104JuriProg0505}} 105\footnote{\url{am105JuriProg0505}} 106\footnote{\url{am106JuriProg0505}} & Kauppi; Kudrycka, Zwiefka; Bertinotti & ++ & \input{104-krit.en}\\\hline
107\footnote{\url{am107JuriProg0505}} & Szejna & o & \input{107-krit.en}\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{amends}{Amendments in Detail}
\input{31.en}

\input{97.en}

\input{99.en}

\input{100.en}

\input{101.en}

\input{102.en}

\input{103.en}

\input{104.en}

\input{105.en}

\input{106.en}

\input{107.en}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
% mode: latex ;
% End: ;

