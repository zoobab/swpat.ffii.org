# -*- mode: makefile -*-

core.de.lstex:
	lstex core.de | sort -u > core.de.lstex
core.de.mk:	core.de.lstex
	vcat /ul/prg/RC/texmake > core.de.mk


core.de.dvi:	core.de.mk
	rm -f core.de.lta
	if latex core.de;then test -f core.de.lta && latex core.de;while tail -n 20 core.de.log | grep -w references && latex core.de;do eval;done;fi
	if test -r core.de.idx;then makeindex core.de && latex core.de;fi

core.de.pdf:	core.de.ps
	if grep -w '\(CJK\|epsfig\)' core.de.tex;then ps2pdf core.de.ps;else rm -f core.de.lta;if pdflatex core.de;then test -f core.de.lta && pdflatex core.de;while tail -n 20 core.de.log | grep -w references && pdflatex core.de;do eval;done;fi;fi
	if test -r core.de.idx;then makeindex core.de && pdflatex core.de;fi
core.de.tty:	core.de.dvi
	dvi2tty -q core.de > core.de.tty
core.de.ps:	core.de.dvi	
	dvips  core.de
core.de.001:	core.de.dvi
	rm -f core.de.[0-9][0-9][0-9]
	dvips -i -S 1  core.de
core.de.pbm:	core.de.ps
	pstopbm core.de.ps
core.de.gif:	core.de.ps
	pstogif core.de.ps
core.de.eps:	core.de.dvi
	dvips -E -f core.de > core.de.eps
core.de-01.g3n:	core.de.ps
	ps2fax n core.de.ps
core.de-01.g3f:	core.de.ps
	ps2fax f core.de.ps
core.de.ps.gz:	core.de.ps
	gzip < core.de.ps > core.de.ps.gz
core.de.ps.gz.uue:	core.de.ps.gz
	uuencode core.de.ps.gz core.de.ps.gz > core.de.ps.gz.uue
core.de.faxsnd:	core.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo core.de-??.g3n`;source faxsnd main
core.de_tex.ps:	
	cat core.de.tex | splitlong | coco | m2ps > core.de_tex.ps
core.de_tex.ps.gz:	core.de_tex.ps
	gzip < core.de_tex.ps > core.de_tex.ps.gz
core.de-01.pgm:
	cat core.de.ps | gs -q -sDEVICE=pgm -sOutputFile=core.de-%02d.pgm -
core.de/core.de.html:	core.de.dvi
	rm -fR core.de;latex2html core.de.tex
xview:	core.de.dvi
	xdvi -s 8 core.de &
tview:	core.de.tty
	browse core.de.tty 
gview:	core.de.ps
	ghostview  core.de.ps &
print:	core.de.ps
	lpr -s -h core.de.ps 
sprint:	core.de.001
	for F in core.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	core.de.faxsnd
	faxsndjob view core.de &
fsend:	core.de.faxsnd
	faxsndjob jobs core.de
viewgif:	core.de.gif
	xv core.de.gif &
viewpbm:	core.de.pbm
	xv core.de-??.pbm &
vieweps:	core.de.eps
	ghostview core.de.eps &	
clean:	core.de.ps
	rm -f  core.de-*.tex core.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} core.de-??.* core.de_tex.* core.de*~
core.de.tgz:	clean
	set +f;LSFILES=`cat core.de.ls???`;FILES=`ls core.de.* $$LSFILES | sort -u`;tar czvf core.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
