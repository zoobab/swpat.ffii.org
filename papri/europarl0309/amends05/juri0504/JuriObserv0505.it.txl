<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Monitoraggio, Creazione di Comitati, un altro balzello

#descr: Molti Parlamentari Europei hanno sottoposto emendamenti che chiedono
alla Commissione di monitorare questo o quello, creando un nuovo
balzello e fornendo ulteriore potere ad una agenzia che ha già
eseguito precedenti investigazioni e lavori di monitoraggio in modo
inadeguato e in cattiva fede. Gli emendamenti sul monitoraggio possono
avere senso solo fin tanto che danno al Parlamento Europeo
l'opportunità di un successivo coinvolgimento nella revisione
dell'applicazione della direttiva. La maggior parte degli emendamenti
sono probabilmente solo una perdita del poco tempo prezioso
disponibile in seconda lettura. Alcuni emendamenti, specialmente
quelli di Toine Manders e colleghi, sembrano servire al solo scopo di
veicolare nella parte giustificativa disinformazione sugli interessi
delle Piccole e Medie Imprese (PMI). Alcuni degli emendamenti di
monitoraggio di Kauppi hanno un senso in quanto correggono
implicazioni inaccettabili nel testo originale. Una buona strategia
sarebbe quella di eliminare del tutto dalla direttiva gli articoli sul
monitoraggio.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriObserv0505 ;
# txtlang: xx ;
# End: ;

