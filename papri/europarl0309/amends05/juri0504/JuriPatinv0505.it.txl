<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rimpiazzare %(q:invenzione) con %(q:innovazione) quando non è
stabilita la brevettabilità

#descr: Una %(q:invenzione) nella giurisprudenza brevettuale è una innovazione
(una soluzione, un insegnamento) che soddisfa pienamente i criteri di
brevettabilità come descritti (per esempio) nell'articolo. 52 della
CEB. Il testo dei sostenitori dell'UBE spesso usa il termine
%(q:invenzione) in contesti in cui la brevettabilità deve ancora
essere accertata, contribuendo quindi alla confusione sui requisiti
per la brevettabilità che è progredita nella prassi legale dell'UBE
fin dal 1990, specialmente da IBM 1 & 2 and Controlling Pension
Benefits System. Questo deve essere corretto invece che codificato.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriPatinv0505 ;
# txtlang: xx ;
# End: ;

