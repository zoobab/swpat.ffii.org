<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Significato degli accordi TRIPs

#descr: Il trattato TRIPs imposta delle regole allo scopo di evitare frizioni
e assicurare il libero scambio. Queste regole sono necessariamente di
natura astratta. Devono essere concretizzate a livello di legislazione
comunitaria. L'UBE e i suoi sostenitori non lo fanno e invece
propongono di integrare direttamente la terminologia stratta dei TRIPs
in modo da aggirare l'ordinamento giudiziario e impostare le regole
come preferiscono. Inoltre interpretano i TRIPs in modi che rendono
impossibili per le corti di giustizia ritornare ad una sana
interpretazione della Convenzione Europea sui Brevetti.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriTrips0505 ;
# txtlang: xx ;
# End: ;

