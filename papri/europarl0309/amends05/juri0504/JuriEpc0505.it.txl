<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interpretazione della Convenzione Europea sui Brevetti

#descr: Abbiamo a che fare con una direttiva interpretativa. L'obiettivo della
direttiva è di interpretare (chiarire e concretizzare) le regole che
sono già state codificate in leggi europee, e nel caso specifico nella
Convenzione Europea sui Brevetti (CEB), e che finora è stata
interpretata in modi enormemente differenti. La lobby a favore della
brevettazione illimitata cerca di imporre una interpretazione che
toglie significato l'articolo 52 della CEB e che porta alla fine alla
sua revisione, mentre il Parlamento Europeo in prima lettura ha optato
per una interpretazione significativa che era dominante negli anni '70
e '80. Rivendicazioni su cosa la CEB significhi sono quindi di
primaria importanza per il progetto di direttiva.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriEpc0505 ;
# txtlang: xx ;
# End: ;

