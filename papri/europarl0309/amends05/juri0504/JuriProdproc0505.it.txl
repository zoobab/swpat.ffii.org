<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Prodotti e Rivendicazioni su Processi

#descr: Invenzioni assistite da elaboratore elettronico possono essere
rivendicate come prodotti, ovvero apparati e come processi su tali
apparati. Questa misura della direttiva sui brevetti software dovrebbe
concretizzare i termini dei TRIPs: %(q:prodotti e processi) nel
contesto delle invenzioni assistite dal calcolatore. Dovrebbe fare
solo le assunzioni strettamente indispensabili su altre materie quali
i criteri di brevettabilità.

#EWn: L'articolo 27 dei TRIPs richiede la brevettabilità di %(q:prodotti e
processi). Questo va reso esplicito nella legge dell'Unione. Le
invenzioni assistite da elaboratore elettronico, come altre
invenzioni, possono essere rivendicate come prodotti, cioè apparati e
come processi eseguiti su tali apparati. Questo andrebbe spiegato nel
modo più semplice possibile. Dichiarazioni sul fatto che il software
sia brevettabile o meno non dovrebbero essere infilati di straforo in
questa misura. L'importanza di questa misura riguardo al software sta
nel fatto che il (q:prodotto) ai sensi delle leggi brevettuali è un
apparato che esegue un programma, non il programma in quanto tale. Ciò
può esserne derivato (e reso esplicito in un articolo appropriato
5(2)) che non esiste una cosa come %(q:prodotto costituito da un
programma per elaboratore) nelle leggi brevettuali.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/JuriInvstep0505.el ;
# mailto: simo.sorce@xsec.it ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: JuriProdproc0505 ;
# txtlang: xx ;
# End: ;

