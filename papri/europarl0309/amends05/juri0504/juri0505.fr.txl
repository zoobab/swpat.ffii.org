<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 04/05/2005 Amendements de JURI à la directive sur les brevets
logiciels

#descr: Présentation structurée des amendements déposés le 4 mai 2005 pour la
décision en commission parlementaire des affaires juridiques (JURI)
concernant la directive sur les brevets logiciels.

#submit: soumis par

#artrec: disposition

#lang: langue

#subdok: thème

#rekom: recommandation

#kritik: critique

#vot: vote

#amnr: n°

#votlist: Liste de vote

#amdetal: Détails des amendements

#AWp: Les clarifications clés que la directive doit apporter

#CxidefT: Définition d'%(q:invention assistée par ordinateur)

#cxidef: Une %(q:invention assistée par ordinateur)[, également appelée de
manière inappropriée %(q:invention mise en œuvre par ordinateur),] est
une invention au sens du droit des brevets dont l'exécution implique
l'utilisation d'un appareil programmable.

#ProgT: Définition de %(q:programme d'ordinateur)

#prog: Un %(q:ordinateur) est une réalisation d'une machine abstraite
composée d'entités telles que des entrées/sorties, un processeur, de
la mémoire, de l'espace de stockage et des interfaces pour échanger
avec des systèmes externes et des utilisateurs humains.  Le
%(q:traitement de données) est le calcul sur des entités composants
des systèmes de traitement de données. Un programme d'ordinateur est
une solution d'un problème par moyen de traitement de données, qui,
dés qu'elle à été correctement décrite dans une langue prédéfinie,
peut être executée par un ordinateur.

#ProdprocT: Objets des revendications de produit et de procédé

#prodproc: Une invention assistée par ordinateur peut être revendiquée en tant
que produit, c'est-à-dire en tant qu'appareil programmé, ou en tant
que procédé réalisé par un tel appareil.

#ProgklmT: Exclusion des revendications de programme

#progklm: Une revendication de brevet pour un programme d'ordinateur, seul ou
sur support, ne doit pas être autorisée.

#PublT: Liberté de publication

#publ: La publication ou la distribution d'informations ne peut jamais
constituer une contrefaçon de brevet.

#NetechT: Définition négative de %(q:domaine technique)

#netech: Le traitement de données n'est pas un domaine technique.

#TechT: Définition positive de %(q:technique) et %(q:domaine technique)

#tech: %(q:technologie) signifie %(q:science de nature appliquée).  Un
domaine technique est une discipline des sciences appliquées dans
laquelle une connaissance est acquise par expérimentation sur les
forces contrôlables de la nature. %(q:Technique) signifie
%(q:appartenant à un domaine technique).

#NekontribT: Définition négative de %(q:contribution)

#nekontrib: Une amélioration de l'efficacité d'un traitement de données ne
constitue pas une contribution technique.

#KontribT: Définition positive de %(q:contribution) et %(q:invention)

#kontrib: Une %(q:invention) est une contribution à l'état de l'art dans un
domaine technique. La contribution est l'ensemble de caractéristiques
par lequel l'objet de la revendication de brevet dans son ensemble est
prétendue différer de l'état de l'art antérieur. La contribution doit
être de nature technique, i.e. elle doit comprendre des
caractéristiques techniques et appartenir à un domaine technique. Sans
contribution technique, il n'existe pas d'objet brevetable et pas
d'invention. La contribution technique doit remplir les conditions de
brevetabilité. En particulier, la contribution technique doit être
nouvelle et non évidente pour une personne du métier.

#InteropT: Liberté d'interopérer

#interop: Chaque fois que le recours à une technique brevetée est nécessaire
pour l'interopérabilité, ce recours n'est pas considéré comme une
contrefaçon de brevet.

#doT: Amendements regroupés par thème

#tTf: Dix clarifications clés

#HxW: transparents de la présentation donnée par Hartmut Pilch à la
%(bk:conférence du 1er juin à Bruxelles)

#WgW: pour imprimer sur un page

#vnp: Comment achever les Buts de la %(q:Position Commune) -- Options et
Potentiel de Compromis

#rci: Papier des discussions pour promotion des Dix Clarifications Centrales
au sein du PPE

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/juri0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: juri0505 ;
# txtlang: xx ;
# End: ;

