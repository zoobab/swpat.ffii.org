\begin{subdocument}{JuriProdproc0505}{Product and Process claims}{http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/prodproc.en.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2005-03-20 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Computer-aided inventions can be claimed as products, i.e. apparatusses, and as processes executed on such apparatusses.  This provision of the draft software patents directive should concretise the TRIPs terms ``products and processes'' in the context of computer-aided inventions.  It should make as few assumptions as possible about other matters, such as patentability criteria.}
\begin{sect}{intro}{Introduction}
Art 27 TRIPs requires the patentability of ``products and processes''.  This deserves concretisation in EU law. Computer-aided inventions can, like other inventions, be claimed as products, i.e. apparatusses, and as processes executed on such apparatusses.  This should be stated in the simplest possible terms.  Statements as to whether software is patentable or not should not be piggybacked onto this provision.  This provision's importance regarding software lies in the fact that the ``product'' in the sense of patent law is an appratus running a program, not the program as such.  It can be inferred (and made explicit by an appropriate article 5(2)) that there is no such thing as a ``computer program product'' in patent law.
\end{sect}

\begin{sect}{votlst}{Votes}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
no. & submitted by & vote & critique\\\hline
119\footnote{\url{am119JuriProdproc0505}} 120\footnote{\url{am120JuriProdproc0505}} 121\footnote{\url{am121JuriProdproc0505}} & Kudrycka, Zwiefka; Bertinotti; Kauppi & ++ & \input{119-krit.en}\\\hline
33\footnote{\url{am33JuriProdproc0505}} & Rocard & + & \input{33-krit.en}\\\hline
118\footnote{\url{am118JuriProdproc0505}} & Ortega & + & \input{118-krit.en}\\\hline
117\footnote{\url{am117JuriProdproc0505}} & Lichtenberger, Frassoni & + & \input{117-krit.en}\\\hline
116\footnote{\url{am116JuriProdproc0505}} & Szejna & + & \input{116-krit.en}\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{amends}{Amendments in Detail}
\input{33.en}

\input{116.en}

\input{117.en}

\input{118.en}

\input{119.en}

\input{120.en}

\input{121.en}

\input{237.en}

\input{238.en}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
% mode: latex ;
% End: ;

