# -*- mode: makefile -*-

core.fr.lstex:
	lstex core.fr | sort -u > core.fr.lstex
core.fr.mk:	core.fr.lstex
	vcat /ul/prg/RC/texmake > core.fr.mk


core.fr.dvi:	core.fr.mk
	rm -f core.fr.lta
	if latex core.fr;then test -f core.fr.lta && latex core.fr;while tail -n 20 core.fr.log | grep -w references && latex core.fr;do eval;done;fi
	if test -r core.fr.idx;then makeindex core.fr && latex core.fr;fi

core.fr.pdf:	core.fr.ps
	if grep -w '\(CJK\|epsfig\)' core.fr.tex;then ps2pdf core.fr.ps;else rm -f core.fr.lta;if pdflatex core.fr;then test -f core.fr.lta && pdflatex core.fr;while tail -n 20 core.fr.log | grep -w references && pdflatex core.fr;do eval;done;fi;fi
	if test -r core.fr.idx;then makeindex core.fr && pdflatex core.fr;fi
core.fr.tty:	core.fr.dvi
	dvi2tty -q core.fr > core.fr.tty
core.fr.ps:	core.fr.dvi	
	dvips  core.fr
core.fr.001:	core.fr.dvi
	rm -f core.fr.[0-9][0-9][0-9]
	dvips -i -S 1  core.fr
core.fr.pbm:	core.fr.ps
	pstopbm core.fr.ps
core.fr.gif:	core.fr.ps
	pstogif core.fr.ps
core.fr.eps:	core.fr.dvi
	dvips -E -f core.fr > core.fr.eps
core.fr-01.g3n:	core.fr.ps
	ps2fax n core.fr.ps
core.fr-01.g3f:	core.fr.ps
	ps2fax f core.fr.ps
core.fr.ps.gz:	core.fr.ps
	gzip < core.fr.ps > core.fr.ps.gz
core.fr.ps.gz.uue:	core.fr.ps.gz
	uuencode core.fr.ps.gz core.fr.ps.gz > core.fr.ps.gz.uue
core.fr.faxsnd:	core.fr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo core.fr-??.g3n`;source faxsnd main
core.fr_tex.ps:	
	cat core.fr.tex | splitlong | coco | m2ps > core.fr_tex.ps
core.fr_tex.ps.gz:	core.fr_tex.ps
	gzip < core.fr_tex.ps > core.fr_tex.ps.gz
core.fr-01.pgm:
	cat core.fr.ps | gs -q -sDEVICE=pgm -sOutputFile=core.fr-%02d.pgm -
core.fr/core.fr.html:	core.fr.dvi
	rm -fR core.fr;latex2html core.fr.tex
xview:	core.fr.dvi
	xdvi -s 8 core.fr &
tview:	core.fr.tty
	browse core.fr.tty 
gview:	core.fr.ps
	ghostview  core.fr.ps &
print:	core.fr.ps
	lpr -s -h core.fr.ps 
sprint:	core.fr.001
	for F in core.fr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	core.fr.faxsnd
	faxsndjob view core.fr &
fsend:	core.fr.faxsnd
	faxsndjob jobs core.fr
viewgif:	core.fr.gif
	xv core.fr.gif &
viewpbm:	core.fr.pbm
	xv core.fr-??.pbm &
vieweps:	core.fr.eps
	ghostview core.fr.eps &	
clean:	core.fr.ps
	rm -f  core.fr-*.tex core.fr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} core.fr-??.* core.fr_tex.* core.fr*~
core.fr.tgz:	clean
	set +f;LSFILES=`cat core.fr.ls???`;FILES=`ls core.fr.* $$LSFILES | sort -u`;tar czvf core.fr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
