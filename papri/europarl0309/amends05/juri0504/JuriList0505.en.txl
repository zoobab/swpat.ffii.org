<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: JURI Amendments 1-256

#descr: Sequential List of Amendments tabled for decision on the software
patent directive in JURI on May 4th 2005

#otit: DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on the
patentability of computerimplemented invent ions DIRECTIVE OF THE
EUROPEAN PARLIAMENT AND OF THE COUNCIL on the patentability of
computer-controlled inventions

#am1n: DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on the
patentability of computerimplemented invent ions DIRECTIVE OF THE
EUROPEAN PARLIAMENT AND OF THE COUNCIL on the patentability of
computer-controlled inventions

#am1j: The term %(q:implemented) is misleading here, as it could give the
impression that an invention can be wholly realised by a mere
computer, which would mean that software could be patentable. As both
the Commission and the Council have come out against the patentability
of software, the scope of the directive needs to be defined so as to
exclude this eventuality. What the directive should therefore cover is
the patenting of inventive material devices which controlled by
software.

#am1k: Only software solutions can be %(q:computer-implemented), and software
solutions are not inventions in the sense of the law.  This amendment
corrects this grave error.

#cre1: The realisation of the internal market implies the elimination of
restrictions to free circulation and of distortions in competition,
while creating an environment which is favourable to innovation and
investment. In this context the protection of inventions by means of
patents is an essential element for the success of the internal
market. Effective, transparent and harmonised protection of
computerimplemented inventions throughout the Member States is
essential in order to maintain and encourage investment in this field.

#am2n: The realisation of the internal market implies the elimination of
restrictions to free circulation and of unjustified distortions in
competition, while creating an environment which is favourable to
innovation and investment. In this context the protection of
inventions by means of patents is one of the elements contributing to
the success of the internal market. Appropriate effective, transparent
and harmonised protection of computer-controlled inventions throughout
the Member States is essential in order to maintain and encourage
investment in all technical fields involving the use of information
technology.

#am2j: Distortions in competition are harmful only when they are unjustified.
States may, within their competencies, make use of these, which is
something that the directive cannot prejudge. The directive covers the
patentability of technical inventions controlled by information
technology.

#oea: The original implies that the directive aims to make %(q:inventions)
in the field of data processing patentable.  The amendment corrects
this.

#cre22: Differences exist in the protection of computer-implemented inventions
offered by the administrative practices and the case law of the
different Member States. Such differences could create barriers to
trade and hence impede the proper functioning of the internal market.

#am3n: Differences exist in the protection of computer-controlled inventions
offered by the administrative practices and the case law of the
different Member States. Such differences could create barriers to
trade and hence impede the proper functioning of the internal market.

#am3j: To be consistent with Article 1.

#am3k: %(q:Computer-implemented) solutions are not %(q:inventions) within the
meaning of patent law.  This amendment corrects the error changing
%(q:implemented) to %(q:controlled).

#cre5: Therefore, the legal rules governing the patentability of
computer-implemented invent ions should be harmonised so as to ensure
that the resulting legal certainty and the level of requirements
demanded for patentability enable innovative enterprises to derive the
maximum advantage from their inventive process and provide an
incentive for investment and innovation. Legal certainty will also be
secured by the fact that, in case of doubt as to the interpretation of
this Directive, national courts may, and national courts of last
instance must, seek a ruling from the Court of Justice.

#am4n: Therefore, the legal rules governing the patentability of
computer-controlled invent ions should be harmonised so as to ensure
that the resulting legal certainty and the level of requirements
demanded for patentability enable innovative enterprises to derive the
maximum advantage from their invent ive process and provide an
incentive for investment and innovation. Legal certainty will also be
secured by the fact that, in case of doubt as to the interpretation of
this Directive, national courts may, and national courts of last
instance must, seek a ruling from the Court of Justice.

#cre6: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994) . Article 27(1) of TRIPS
provides that patents shall be available for any inventions, whether
products or processes, in all fields of technology, provided that they
are new, involve an inventive step and are capable of industrial
application. Moreover, according to that Article, patent rights should
be available and patent rights enjoyable without discrimination as to
the field of technology. These principles should accordingly apply to
computerimplemented inventions.

#am5n: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to that Article, patent rights should be available
and enjoyable without discrimination as to the field of technology.
These principles should accordingly apply to computer-controlled
inventions, without prejudice however to the legitimate interests of
software authors as regards exploitation of their work, as stipulated
by Article 13 of TRIPS, since computer programs are protected under
copyright pursuant to Article 10 of this agreement.

#am5j: The need to concretise the %(tr:TRIPs treaty) is one of the chief
reasons for legislative action at the EU level.  This amendment
rectifies abusive interpretations of the treaty that have been used to
promote software patentability in Europe.

#cre7: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 (European Patent Convention) and the patent laws of
the Member States, programs for computers together with discoveries,
scientific theories, mathematical methods, aesthetic creations,
schemes, rules and methods for performing mental acts, playing games
or doing business, and presentations of information are expressly not
regarded as inventions and are therefore excluded from patentability.
This exception, however, applies and is justified only to the extent
that a patent application or patent relates to the above
subject-matter or activities as such, because the said subject-matter
and activities as such do not belong to a field of technology.)

#am6n: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 (European Patent Convention) and the patent laws of
the Member States, programs for computers together with discoveries,
scientific theories, mathematical methods, aesthetic creations,
schemes, rules and methods for performing mental acts, playing games
or doing business, and presentations of information are expressly not
regarded as inventions and are therefore excluded from patentability.
This exception applies because the said subject matter and activities
do not belong to a field of technology.

#am6j: Computer programmes are not inventions within the meaning of patent
law, because software is not a field of technology.

#am6k: The EPO's practise of explaining %(q:as such) as an equivalent of
%(q:as far as it is not technical) does not conform to the %(le:normal
rules for interpretation of laws).  The amendment corrects the
misinterpretation and safeguards the integrity of Art 52 EPC as a
meaningful piece of law.

#cre8: The aim of this Directive is to prevent different interpretations of
the provisions of the European Patent Convention concerning the limits
to patentability. The consequent legal certainty should help to foster
a climate conducive to investment and innovation in the field of
software.

#am7n: The aim of this Directive is to prevent different interpretations of
the provisions of the European Patent Convention concerning the limits
to patentability. The consequent legal certainty should help to foster
a climate conducive to investment and innovation in fields of
technology and in the field of software.

#am7j: The aim of this directive is not to legislate on software
patentability but on the patentability of computer-controlled
inventions.

#am7k: Experience show that patents are not a stimulating factor in software
innovation.  The aim of the directive can not be to achieve something
contradictory.  The amendment corrects the error.

#cre9: Patent protection allows innovators to benefit from their creativity.
Patent rights protect innovation in the interests of society as a
whole and should not be used in a manner which is anti-competitive.

#am8n: Patent protection may allow inventors to benefit from their
creativity. Patent rights protect innovation in the interests of
society as a whole and should not be used in a manner which is
anti-competitive or excessively detrimental to the innovation derived
therefrom.

#am8j: Patents are not the only way of enabling innovators to benefit from
their creations. Restrictions on the freedom of enterprise imposed by
the patent system must be taken into account in assessing the
relevance of the patent system as regards new potential areas of
application.

#am8k: Patents are not intrinsically good or bad for innovation, and problems
do not only arise only in the %(q:use) of the (intrinsically good)
system.  The amendment partially corrects the error of an overly
religious law drafter.

#cr10: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright.

#am9n: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright, because they are
algorithms which are comparable to mathematical methods or methods of
presenting information.

#am9j: Programme design principles cannot be patentable as they are
comparable to mathematical proofs.

#am9k: The Commission suggests that patents might be needed to fill a void
that is left by software copyright.  The amendment clarifies that the
void is there for good reasons.

#cr11: In order for any invention to be considered as patentable it should
have a technical character, and thus belong to the field of
technology.

#a10: In order for any innovation to be considered as patentable it should
have a technical character, and thus belong to a field of technology.
It must also be capable of industrial application, be new and involve
an inventive step.

#a10j: Reminder of the conditions for patentability.

#a10k: %(q:Inventions) are always patentable.  Only %(q:innovations) can be
tested for patentability.  This amendment corrects the error, but not
clearly enough.  It would have been better to say: %(q:In order for
any innovation to be considered an %(e:invention), it should ... .
%(e:Inventions) must moreover be ...)

#cr12: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of the art.

#a11n: It is a condition for inventions in general that, in order to involve
an inventive step, they should show a significant difference between
the overall technical features in the patent claim and the state of
the art.

#a11j: This definition of an inventive step is tautological, as any technical
contribution already involves an inventive step. The initial wording
creates confusion between the criteria for technical expertise and
inventiveness. If there is no technical expertise, a patent cannot be
granted, regardless of any inventive criterion. Otherwise an invention
would only need to be new in order to pass the inventiveness test,
which could lead to a serious decline in the quality of patents
granted and result in patent offices being overwhelmed by patent
applications for trivial inventions.

#a11k: Non-obviousness (= inventive step) and patentability (= technical
contribution) are separate requirements.  The amendment corrects the
error, but at the price of unnecessarily meddling in the meaning of
%(q:inventive step).

#cr13: Accordingly, although a computer-implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of the art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#a12n: Accordingly, an innovation which does not make a technical
contribution to the state of the art is not an invention within the
meaning of patent law.

#a12j: For the purposes of patent law, inventions must entail a technical
contribution.

#a12k: The patent lobby is trying to weaken the invention test by mixing it
with the non-obviousness test in obscure ways.  The amendment corrects
the error and restores clarity in a perfectly elegant way.

#cr14: The mere implementation of an otherwise unpatentable method on an
apparatus such as a computer is not in itself sufficient to warrant a
finding that a technical contribution is present. Accordingly, a
computer-implemented business method, data processing method or other
method, in which the only contribution to the state of the art is
nontechnical, cannot constitute a patentable invention.

#a13n: Accordingly, whilst computer-controlled inventions belong to a field
of technology, because their technical contribution lies outside the
software that controls them, implementation on an apparatus such as a
computer of an otherwise unpatentable method such as a business
method, data processing method or other method, in which the only
contribution to the state of the art is non-technical, cannot under
any circumstances be considered a technical contribution. Accordingly,
an implementation of this kind can under no circumstances constitute a
patentable invention.

#a13j: The initial wording is open to misunderstanding, since it implies that
there could be a contribution to the state of the art that is not
technical. The new wording draws a distinction between what is
technical and what is not.

#a13k: The main problem of the original proposal is that, depending on the
understanding of the bloated syntactic construction, it implies that a
business method could make technical contribution and thereby become
patentable.  Unfortunately the amendment does not solve these
problems.  It can still be recommended because it corrects the
misleading term %(q:computer-implemented) and stresses that the
contribution must lie outside the software.

#cr15: Si la contribution à l'état de la technique porte uniquement sur un
objet non brevetable, il ne peut y avoir invention brevetable,
indépendamment de la façon dont l'objet est présenté dans les
revendications. Ainsi, l'exigence d'une contribution technique ne peut
être contournée simplement en spécifiant des moyens techniques dans
les revendications du brevet.

#a14n: Si la contribution à l'état de l'art porte uniquement sur un objet non
brevetable, il ne peut y avoir invention brevetable, indépendamment de
la façon dont l'objet est présenté dans les revendications. Ainsi,
l'exigence d'une contribution technique ne peut être contournée
uniquement en spécifiant des moyens techniques dans la revendication
de brevet.

#a14j: Il ne peut y avoir de contribution à l'état de la technique provenant
d'un objet non brevetable parce que non technique. On peut en revanche
parler d'état de l'art pour les domaines non techniques.

#a14k: No comment

#cr16: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention. Nonetheless, a method
involving the use of an algorithm might be patentable provided that
the method is used to solve a technical problem. However, any patent
granted for such a method should not monopolise the algorithm itself
or its use in contexts not foreseen in the patent.

#a15n: Thus, an algorithm or computer program are inherently non-technical
and cannot constitute a technical invention. Nonetheless, a technical
procedure controlled by a computer programme may be patentable, if the
process has characteristics which make it a technical contribution,
besides the normal interaction between the program and the computer.
However, any patent granted for such a technical process may not
establish a monopoly on the algorithm or the program itself, as
programs as such cannot be patentable, as stated in particular in
Article 52(2)(c) of the European Patent Convention.

#a15j: The initial wording is unhelpful as it fails to stipulate that the
method in question must be a technical process. It should not be
possible to draw the conclusion that methods other than technical
methods can be patentable.

#a15k: The original recital says that a non-technical solution of a technical
problem can be patentable.  Moreover it promises that such a patent
would not monopolise the solution, as it would %(q:only) apply in the
context of the problem.  The amendment fills the promise with some
substance.  Unfortunately it contains allusions the EPO doctrine of
%(q:further technical effect beyond the normal interaction between
program and computer).  %(q:Procedure) is a mistranslation, should be
corrected to %(q:process).

#a16n: Methods for processing data represented in digital form are by their
very nature algorithms and are therefore inherently non-technical.
However, if information from the physical world is not captured in
order to be represented digitally, a physical process for processing
such information in hardware could have a technical character.

#a16j: This definition illustrates the nature of digital data-processing
performed by computer programmes, which may in no case be patented. It
also makes it possible to maintain the scope for patenting inventive
technical processes for which the nature of the signals used is
significant as regards the desired result: for example, electrical
voltage to power an engine; pressure changes to power a hydraulic
piston, etc, since what matters in these cases is the result of
controllable physical interactions not the processing of information
regardless of the physical carrier used.

#a16k: Good intentions, poor means.  Physical processes always have a
technical character.  It would have been more understandable to
explain in a recital what data processing is, so as to support and
article which declares that it is not a field of technology.

#cr17: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawings. Computer-implemented inventions should be claimed at
least with reference to either a product such as a programmed
apparatus, or to a process carried out in such an apparatus.
Accordingly, where individual elements of software are used in
contexts which do not involve the realisation of any validly claimed
product or process, such use will not constitute patent infringement.

#a17n: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawings. Computer-controlled inventions should be claimed solely
with reference to either a product such as a programmed apparatus, or
to a technical process carried out in such an apparatus. Accordingly,
where individual elements of software are used in contexts which do
not involve the realisation of any validly claimed product or
technical process, such use will not constitute patent infringement.

#a17j: Software on a carrier cannot be patentable.

#a17k: It goes without saying that a patent monopoly is defined by the
claims, and that software patents contain process claims.  Some
legislators have been misled to believe that this somehow limits the
scope of software patent claims to complete software products rather
than features thereof, and that the %(q:fears) of all those ignorant
programmers out there are unfounded.  This amendment does not
completely correct the error.  However it does replace the erroneous
term %(q:computer-implemented invention), and it shows an intention to
limit patentability to inventions in applied natural sciences.

#cr18: The legal protection of computer-implemented inventions does not
necessitate the creation of a separate body of law in place of the
rules of national patent law. The rules of national patent law remain
the essential basis for the legal protection of computer-implemented
invent ions. This Directive simply clarifies the present legal
position with a view to securing legal certainty, transparency, and
clarity o f the law and avoiding any drift towards the patentability
of unpatentable methods such as obvious or non-technical procedures
and business methods.

#a18n: The legal protection of computer-controlled inventions does not
necessitate the creation of a separate body of law in place of the
rules of national patent law. The rules of national patent law remain
the essential basis for the legal protection of computer-controlled
inventions. This Directive simply clarifies the present legal position
with a view to securing legal certainty, transparency, and clarity of
the legislation and avoiding any drift towards the patentability of
unpatentable methods in particular inherently non-technical methods
such as algorithms, software, data processing methods or educational
or business methods.

#a18j: Correcting the terminology.

#a18k: Computer-implemented solutions are not inventions in the sense of
patent law.  This amendment replaces CII with CCI, enlarges the list
of %(q:inherently non-technical methods), and cleans out the talk
about obviousness which does not belong into this context.

#cr19: This Directive should be limited to laying down certain principles as
they apply to the patentability of such inventions, such principles
being intended in particular to ensure that inventions which belong to
a field of technology and make a technical contribution are
susceptible of protection, and conversely to ensure that those
inventions which do not make a technical contribution are not
susceptible of protection.

#a19j: The Commission suggests that there may be such a thing as
non-technical inventions and that the legislator should limit himself
to a task of rubberstamping decisions taken by administrative and
judiciary bodies.  Since the amendment only repeats errors that were
already voiced elsewhere, deletion is a good choice.

#cr20: The competitive position of Communit y industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-implemented invent ions are
eliminated and the legal situation is transparent. With the present
trend for traditional manufacturing industry to shift their operations
to low-cost economies outside the Community, the importance of
intellectual property protection and in particular patent protection
is self-evident.

#a20n: The competitive position of Community industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-controlled inventions are eliminated
and the legal situation is transparent. The present trend for
traditional manufacturing industry to shift their operations to
low-cost economies outside the Community, as well as the requirements
for sustainable and balanced development, are factors to be taken into
account when determining an appropriate level of intellectual property
protection and in particular patent protection for technical
inventions and copyright protection for software. The level of this
protection, as well as the monopolistic effects it might create should
be determined in a manner that will not prejudice the dynamics of
competition and cross-fertilisation which are the key to the
development of innovative small and medium-sized enterprises in the
European Union with easy market access, which will serve to ensure the
Community's future competitiveness.

#a20j: Recalling the Lisbon objectives and the methods needing to be applied
in this connection.

#a20k: This recital deals with the intended effects of the directive on the
competitiveness of the EU as a region.  The original provision
insinuates that software patents are needed for to protect the EU's
industry against competition from Eastern Europe and China and
postulates that the conclusions to be drawn from protectionist fears
are %(q:self-evident).  The new version takes a more sensible and
balanced view, based on the Lisbon Agenda.

#cr22: The rights conferred by patents granted for inventions within the
scope of this Directive should not affect acts permitted under
Articles 5 and 6 of Direct ive 91/250/EEC, in particular under the
provisions thereof in respect of decompilation and interoperability.
In particular, acts which, under Articles 5 and 6 of Directive
91/250/EEC, do not require authorisation of the rightholder with
respect to the rightholder's copyrights in or pertaining to a computer
program, and which, but for those Articles, would require such
authorisation, should not require authorisation of the rightholder
with respect to the rightholder's patent rights in or pertaining to
the computer program.

#a21n: The rights conferred by patents granted for inventions within the
scope of this Directive should not affect acts permitted under
Articles 5 and 6 of Direct ive 91/250/EEC, in particular under the
provisions thereof in respect of decompilation and interoperability.
In particular, acts which, under Articles 5 and 6 of Directive
91/250/EEC, do not require authorisation of the rightholder with
respect to the rightholder's copyrights in or pertaining to a computer
program, and which, but for those Articles, would require such
authorisation, should not require authorisation of the rightholder
with respect to the rightholder's patent rights in or pertaining to
the computer program. Furthermore, wherever the use of a patented
technique is needed for the conversion of conventions used in two
different computer systems or networks so as to allow communication
and exchange of data between them, such use should not be considered
to be a patent infringement.

#a21j: To ensure that interoperability can be maintained.

#a21k: The original provision wanted to allow only reverse engineering of a
patented-encumbered interface, but not the use of the resulting
program.  The amendment turns the fake exemption into a real one, as
voted for by more than 90% of MEPs in 2003.

#cr23: Since the objective of this Directive, namely to harmonise national
rules on the patentabilit y of computer-implemented inventions, cannot
be sufficiently achieved by the Member States and can therefore be
better achieved at Community level, the Community may adopt measures,
in accordance with the principle of subsidiarity as set out in Article
5 of the Treaty. In accordance with the principle of proportionality,
as set out in that Article, this Directive does not go beyond what is
necessary to achieve that objective.

#a22n: Since the objective of this Directive, namely to harmonise national
rules on the patentabilit y of computer-controlled inventions, cannot
be sufficiently achieved by the Member States and can therefore be
better achieved at Community level, the Community may adopt measures,
in accordance with the principle of subsidiarity as set out in Article
5 of the Treaty. In accordance with the principle of proportionality,
as set out in that Article, this Directive does not go beyond what is
necessary to achieve that objective.

#a22j: Consistency with Article 1.

#a22k: %(q:Computer-implemented) solutions are not inventions.  This
amendmenment replaces CII with CCI.

#car1: This Directive lays down rules for the patentability of
computer-implemented inventions.

#a23n: This Directive lays down rules for the patentability of
computer-controlled inventions, sometimes also known as
computer-implemented inventions.

#a23j: As it is not the aim of this directive to amend the European Patent
Convention or to call into question the non-patentability of software,
as is explicitly stated in Recital 8 of the text adopted by the
Council, it is quite inappropriate to call for the European Patent
Convention to be revised.

#a23k: No opinion

#ca2a: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#ita: %(q:computer-controlled invention) means any invention the performance
of which involves the use of a computer, computer network or other
programmable apparatus, the invention having one or more nontechnical
features realised wholly or partly by means of a computer program or
computer programs, besides the technical features which any invention
must possess;

#iWg: Similar to an amendment adopted in first reading, except that
%(q:implemented) was replaced with %(q:controlled) and that the
clarification that %(q:invention) is an %(q:invention in the sense of
patent law), i.e. a patentable innovation, has been dropped.

#ca2b: %(q:technical contribution) means a contribution to the state of the
art in a field of technology which is new and not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether or not these are
accompanied by non-technical features.

#a25n: %(q:technical contribution), also called %(q:invention), means a
contribution to a field of technology.

#a25j: Article 3 is a more appropriate place for a definition of
patentability conditions, since it deals specifically with that
question. An invention is equivalent to a technical contribution,
which, besides its technical nature, must therefore be new,
non-obvious, and capable of industrial application, as specified in
Article 3.

#a25k: Disentangles %(q:contribution) from inventive step.

#a26n: %(q:Technical) means %(q:belonging to a field of technology). A new
teaching about the use of controllable forces of nature, under the
control of a computer program and beyond the technical devices
required to implement the program, is technical. The processing,
handling, representation and presentation of information by computer
program are not technical, even where technical devices are employed
for such purposes;

#a26j: A number of definitions need to be provided if the directive is to
achieve its goal of legal clarification. The second paragraph
reaffirms the patentability of innovatory material devices controlled
by software, such as washing machines, ABS braking systems, etc. The
third paragraph, which is consistent with the provisions of the TRIPS
Treaty, will avoid patents being requested for software, even if this
is in conjunction with technical devices. Clearly, if the technical
devices used are themselves innovatory, a patent application may be
submitted. This amendment takes up and refines the ideas contained in
Article 2(a) and (b) as adopted at first reading.

#a26k: explains that what happens within the data processing system is not
technical.

#a27n: %(q:Field of technology) means an industrial field of application
requiring the use of controllable forces of nature to obtain
predictable results in the physical world;

#a27j: This provides a positive definition of the concept of %(q:field of
technology) for the purposes of patent law. Fields of technology
belong to the physical world and are characterised by the need to use
physical interaction to achieve the desired result, such as the
movement of a vehicle, the emission of a laser beam, etc. This
amendment reintroduces and supplements the amendment to Article 2(c)
adopted at first reading.

#a27k: Defines %(q:field of technology) by physical causality.

#a28n: %(q:Information processing method) means any processing method
handling digitally represented information, whatever the nature or
origin of what it represents. These methods include digital
information processing as such, but also the handling, representation
or presentation of such information.

#a28j: This definition illustrates the nature of digital data processing
performed by computer programs, none of which may be patentable. It
also enables inventive technical processes, where the nature of the
signals used is significant as regards the desired result, to remain
patentable: e.g. electrical voltage to drive an engine, pressure
differences to drive a hydraulic piston, etc., since here what matters
is the result of controllable physical interaction and not the
processing of information regardless of the physical carrier used.

#a28k: It's a good idea to explain terms like %(q:data processing) or, in
this case %(q:information processing) in the directive.

#a29n: Member States shall ensure that inventions are patentable irrespective
of whether or not they use computerised means and that, conversely, no
one may patent algorithms, software or information-processing methods,
whether or not they are combined with technical mechanisms.

#a29j: The amendment stipulates that the scope of patentability cannot be
altered by the presence or absence of data-processing devices in the
proposed technical solution. Legitimately patentable inventions will
remain so, such as a new ABS system producing better braking than
previous generations. This amendment reproduces and refines Article 5
as adopted at first reading.

#a29k: a useful statement of the general intention of the legislator.

#car3: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and must involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution.

#a30n: In order to be patentable, a computer-controlled invention must, in
addition to being technical in nature, be new, susceptible of
industrial application and involve an inventive step. The inventive
step shall be assessed by consideration of the difference between the
overall technical features in the patent claim and the state of the
art, irrespective of whether or not such features are accompanied by
non-technical features.

#a30j: The wording used by the Council is tautological, in that it defines as
a condition for the inventive step the fact that the invention should
entail a technical contribution, in other words that it should belong
to a field of technology, be new and involve an inventive step. In
order to escape this chain of reasoning, an explicit definition is
needed of the concept of an inventive step, with reference to the
contribution the invention makes. This contribution must derive solely
from its technical features, otherwise a patent application referring
to the use of inventive software to control technical but not
inventive devices could result in the issuing of a patent which would
in fact be solely for software. This article reintroduces and refines
the substance of Articles 2(b) and 4 adopted at first reading by
Parliament, where this type of reasoning was also used.

#a30k: The intention seems to be to disentangle the %(q:contribution) from
the %(q:inventive step), but the amendment augments the confusion by
attempting to define %(q:inventive step) (= non-obviousness) as the
set of features by which the claim object differs from the prior art,
i.e. as the %(q:contribution).  Also, it implies that there could be
unpatentable inventions.

#ca41: A computer program as such cannot constitute a patentable invention.

#a31n: A computer program as such, on any carrier or in the form of a signal,
cannot constitute a patentable invention.

#a31j: The second paragraph of the Council's text is ambiguous, since it
authorises software patentability provided the software produces
%(q:technical effects), an expression which in EPO practice means the
capacity to resolve a specific problem, which is precisely what any
software is intended to do. Accordingly, a computer program alone or
on any carrier may not be claimed as an invention, as this would be
tantamount to authorising software patentability on the grounds that
the software itself possessed patentable technical features, which
cannot be the case. Only claims for a computer-controlled invention as
a process or as a software-controlled device are therefore legitimate.

#a31k: The Council's text quotes much-abused wording from the EPC without
concretising it.  The amendment makes the situation even less clear by
opposing %(q:program as such) to %(q:program on a carrier) and
%(q:program in the form of a signal), as if these were somehow
different entities.  On the other hand the intention to resolve the
conflict with Council Art 5(2) and prevent program claims deserves
support.

#ca42: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus. Accordingly,
inventions involving computer programs, whether expressed as source
code, as object code or in any other form, which implement business,
mathematical or other methods and do not produce any technical effects
beyond the normal physical interactions between a program and the
computer, network or other programmable apparatus in which it is run
shall not be patentable.

#a32n: Member States shall ensure that data processing is not considered to
be a field of technology within the meaning of patent law, and that
innovations in the field of data processing are not considered to be
inventions within the meaning of patent law.

#a32j: The second paragraph of this article is identical to Article 3 as
adopted by the European Parliament at first reading. It ensures that
the directive is consistent with the provisions of the TRIPS Treaty by
clearly stipulating that software is not a field of technology within
the meaning of patent law. However, the material components and
devices which make up computers will of course remain patentable when
they are innovatory.

#a32k: The replacement text is very much worth supporting and the original is
very much worth deleting.  One might however wonder whether the
replacement text really belongs into this article, which is about
explaining %(q:programs for computers) in the context of patent
claims.

#ca51: Member States shall ensure that a computer-implemented invent ion may
be claimed as a product, that is as a programmed computer, a
programmed computer network or other programmed apparatus, or as a
process carried out by such a computer, computer network or apparatus
through the execution of software.

#a33n: Member States shall ensure that a computer-controlled invention may be
claimed only as a product, that is as a device controlled by a
programmed computer, a programmed computer network or other programmed
apparatus, or as a technical process controlled by such a computer,
computer network or apparatus through the execution of software.

#a33j: A computer programme on its own or on any carrier may not be claimed
as an invention, as this would be tantamount to authorising software
patentability on the grounds that the software itself possessed
patentable technical features, which cannot be the case. Only claims
for a computer controlled invention as a process or as a
software-controlled device are therefore legitimate. It is preferable
to talk of a computer-controlled device rather than simply a
programmed computer as this would again give the impression that
software on its own may constitute an invention. The first paragraph
is similar to Article 7(1) adopted by Parliament at first reading.

#a33k: The original provision defines the TRIPs term %(q:products and
processes) almost perfectly well.  The only problem is that it,
unnecessarily for this context, insinuates patentability of software. 
The amendment corrects this error and makes it excruciatingly clear
that software is not meant.

#ca52: A claim to a computer program, either on its own or on a carrier,
shall not be allowed unless that program would, when loaded and
executed in a programmable computer, programmable computer network or
other programmable apparatus, put into force a product or process
claimed in the same patent application in accordance with paragraph 1.

#a34n: In accordance with Article 3, Member States shall ensure that the use
of information processing methods can never constitute a direct or
indirect patent infringement.

#a34j: This second paragraph, which is not an additional restriction but
rather a consequence of the definition of technicity introduced in
Article 2, guarantees freedom of information. It reproduces and
refines the meaning of Article 7(3) adopted by Parliament at first
reading, drawing on the definition of information processing method
included in the amendment to Article 2(e) new.

#a34k: Art 5(2) deserves to be deleted or negated.  A positive statement of
freedom of publication would also be appropriate here.  Freedom to use
information processing methods, as stipulated here, goes too far in
the other direction for this context.

#a35n: Member States shall ensure that, wherever the use of a patented
technique is needed for ensuring conversion of the conventions used in
two different computer systems or networks so as to allow
communication and exchange of data content between them, such use is
not considered to be a patent infringement.

#a35j: Unimpeded interoperability implies not only that it has to be possible
to carry out any reverse engineering operations needed in order to
establish the specifications of the communication protocols and
interfaces with which communication is to take place, but also that it
must in fact be permissible to manufacture and market interoperable
products. The second paragraph of Article 6, permitted under Article
30 of the TRIPS Agreement, is necessary to avert the serious
distortions of competition on the internal market which might arise if
the marketing of interoperable products invariably constituted an
infringement of patent claims. The text of paragraph 6(2) corresponds
exactly to ITRE Amendment 15, which became JURI Amendment 20 and was
adopted at first reading in slightly amended form as Article 9.

#a35k: The interoperability article 6a as accepted by CULT, ITRE, JURI and
proposed by the Luxemburg delegetation in the Council.

#ca8a: the impact of patents for computerimplemented inventions on the
factors referred to in Article 7;

#a36n: the impact of patents for computer-controlled inventions on the
factors referred to in Article 7;

#a36j: Justification To be consistent with Article 1.

#a36k: Computer-implemented solutions are not inventions within the meaning
of patent law.  Replaces CII with CCI.  CAI would be better.

#ca8d: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-implemented
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-implemented
inventions;

#a37n: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-controlled
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-controlled
inventions;

#a37j: To be consistent with Article 1.

#ca8f: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the European Patent Convention

#ca8g: the impact of patents for computer-implemented inventions on the
development and commercialisation of interoperable computer programs
and systems.

#a39n: the impact of patents for computer-controlled inventions on the
development and commercialisation of interoperable computer programs
and systems.

#a39j: To be consistent with Article 1.

#eoo: rejette la position commune

#Wqi: En proposant la brevetabilité des inventions mises en oeuvre par
ordinateur, la Commission ouvre la voie à la brevetabilité du savoir
humain. De plus, cette directive ne répond pas aux enjeux économiques,
scientifiques et culturels du secteur du logiciel ainsi qu'à la
nécessité de promouvoir l'innovation. Pour toutes ces raisons et pour
répondre à la forte opposition de scientifiques, d'éditeurs de
logiciels, la position commune doit être rejetée

#a40k: The Council has in multiple ways failed to present the Parliament with
a workable basis for a second reading.  For the sake of the sanity of
the legislative process, the Parliament should say No.

#eCs: Rejects the Common Position

#etu: The Common Position was questioned by several Member states in the
Council, even when the political agreement was finally adopted earlier
this year. This means that there are strong doubts about the content
of the Common Position.

#a42o: Proposal for a directive of the European Parliament and of the Council
on the patentability of computer-implemented inventions

#a42n: Proposal for a directive of the European Parliament and of the Council
on the patentability of computer-aided inventions

#a42j1: This replacement is to be performed at all places in the text where
the expression %(q:computer-implemented invention) is used.

#a42j2: In their press release upon adoption of the %(q:Common Position), the
Council says that that their text did not allow patening of software
as such but only of washing machines, mobile phones etc, which they
called %(q:computer-aided inventions).

#a42j3: When a solution is %(q:aided) by a computer, such as is the case e.g.
in %(q:computer-aided design) and %(q:computer-aided manufacturing)
(CAD/CAM), the claim is usually not directed to the software as such,
but to an industrial engineering process. A computer can aid such a
process but not implement it. A computer alone can only implement a
software solution, and software together with a computer is nothing
more than software as such and thus not an %(q:invention) in the sense
of patent law. The Council has good reasons to avoid this misleading
term in its press release. The same reasons apply to the whole
directive text.

#a42k: a %(q:computer-implemented solution) is nothing but a %(q:program for
computers) in the context of patent claims and therefore not an
invention.  General replacement to %(q:computer-aided) corrects the
error.  The term is intuitively understandable to the person skilled
in the art, and it makes no unneeded assumptions about the invention.

#lCr: Directive 2004/../EC of the European Parliament of the Council on the
patentability of computer implemented inventions

#lWt: Directive 2004/../EC of the European Parliament of the Council on the
patentability of computer operated inventions

#WeW: The title of the directive should undoubtedly express its scope, which
is dealing with computer-operated devices, not with software. The word
%(q:implemented) might suggest that patenting of software is under
circumstances possible.

#apo: %(q:computer-operated) is slightly better than
%(q:computer-implemented), because, while %(q:computer-implemented
solutions) are always nothing but programs for computers, the term
%(q:computer-operated inventions) %(q:might suggest that patenting of
non-software is under circumstances covered by this directive).

#a45o: La presente Directiva establece normas para la patentabilidad de las
invenciones implementadas en ordenador.

#a45n: La presente Directiva establece normas relativas a los límites de la
patentabilidad y a la posibilidad de hacer valer las patentes en
relación con los programas informáticos.

#a45j: El término %(q:invención aplicada a través de ordenador) no es
utilizado por los informáticos. De hecho, no se utiliza en absoluto.
Fue introducido por el OEP en mayo de 2000 para legitimar las patentes
de métodos empresariales, al igual que para equiparar la práctica de
OEP a la de Japón y EEUU.  El término

#Wla: Esta implicación está en contradicción con el artículo 52 CEP, según
el cual los algoritmos, los métodos y los programas empresariales para
los ordenadores no son invenciones en el sentido de la ley de
patentes. No puede ser objetivo de la presente directiva declarar
todas las clases de ideas

#a45k: corrects the title to express the purpose of the directive in plain
language.

#a46o: This Directive lays down rules for the patentability of
computer-implemented inventions.

#a46n: This directive lays down limiting rules for the patentability of
computer-aided inventions.

#a46j: Only software solutions can be %(q:computer-implemented). The aim of
the directive is not to state that software solutions are inventions
in the sense of patent law.

#a46k: Only software solutions can be %(q:computer-implemented). The aim of
the directive is not to state that software solutions are inventions
in the sense of patent law, but to set out the limiting rules for the
patentability of computer-aided inventions.

#vtm3: This Directive lays down rules for the patentability of
computer-implemented inventions.

#ehs: This Directive lays down rules concerning the patentability of
computer-assisted inventions.

#WWW: The term %(q:implemented) is not suitable, as computer-implemented
software is not an invention, as software is not patentable. The
computer and its program are used only to control a hardware
invention, hence the change of word. Moreover, the expression computer
implemented invention is not in use among specialist, on the contrary
of the expression %(q:computer-assisted) such as in the software
%(q:computer-assisted design / computer-assisted manufacturing).

#a48k: corrects %(q:computer-implemented) to %(q:computer-assisted) with
reference to CAD/CAM, thus seems to mean %(q:computer-aided).

#wWf: Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen

#wWf2: Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen

#eai: %(q:Computerimplementierte Erfindung) ist eine Erfindung, zu deren
Ausführung ein Computer, ein Computernetz oder eine sonstige
programmierbare Vorrichtung eingesetzt wird und die mindestens ein
Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren
Computerprogrammen realisiert wird.

#eai2: %(q:Computerimplementierte Erfindung) ist eine Erfindung, zu deren
Ausführung ein Computer, ein Computernetz oder eine sonstige
programmierbare Vorrichtung eingesetzt wird und die mindestens ein
Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren
Computerprogrammen realisiert wird.

#ide: %(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf
einem Gebiet der Technik, der neu und für eine fachkundige Person
nicht nahe liegend ist. Zur Ermittlung des technischen Beitrags wird
beurteilt, inwieweit sich der Gegenstand des Patentanspruchs, der
technische Merkmale umfassen muss, die ihrerseits mit nichttechnischen
Merkmalen versehen sein können, in seiner Gesamtheit vom Stand der
Technik abhebt.

#geP: %(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf
einem Gebiet der Technik, der neu und für eine fachkundige Person
nicht nahe liegend ist.

#eee: Software leistet einen technischen Beitrag, soweit sie bei der
digitalen Verwaltung, Verarbeitung und Darstellung von Daten den
Einsatz beherrschbarer Naturkräfte zur unmittelbaren Herbeiführung des
Ergebnisses direkt benutzt.

#uzv: Der hier vorgelegte Änderungsantrag soll den Bereich, in dem Software
vom Patentschutz im Rahmen einer technischen Entwicklung mit umfasst
ist, näher fixieren und genauer abgrenzen.

#WWa: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#gns: %(q:Computer-aided invention), also called %(q:computer-implemented
invention), means an invention in the sense of patent law the
performance of which involves the use of a programmable apparatus;

#Wur: In their press release upon adoption of the %(q:Common Position), the
Council says that that their text did not allow patening of software
as such but only of washing machines, mobile phones etc, which they
called %(q:computer-aided inventions). When a solution is %(q:aided)
by a computer, such as is the case e.g. in %(q:computer-aided design)
and %(q:computer-aided manufacturing) (CAD/CAM), the claim is usually
not directed to the software as such, but to an industrial engineering
process. A computer can aid such a process but not implement it. A
computer alone can only %(q:implement) (= run) a software solution,
and software running on a computer is nothing more than software as
such and thus not an %(q:invention) in the sense of patent law.

#tse: As long as the old term is still in use, it has to be defined here as
a synonym.

#Wro: This amendment removes wordings from the Council text which are
unclear and redundant and whose only purpose seems to be to suggest
that the claimed invention can consist in nothing but software running
on a computer.

#eni: This amendment is a simplified version of an amendment adopted in 1st
reading.

#a50k: minimalist definition, rectifies the Council's implication that
computing solutions are patentable inventions, simplifies wording,
removes superfluous implications about software patentability, avoids
direct reference to non-EU law which had been criticised by the
Commission.

#WWa2: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#gns2: %(q:Computer-aided invention), also called %(q:computer-implemented
invention), means an invention in the sense of patent law the
performance of which involves the use of a programmable apparatus;

#nwh3: In their press release upon adoption of the %(q:Common Position), the
Council says that that their text did not allow patening of software
as such but only of washing machines, mobile phones etc, which they
called %(q:computer-aided inventions).

#WpW: When a solution is %(q:aided) by a computer, such as is the case e.g.
in %(q:computer-aided design) and %(q:computer-aided manufacturing)
(CAD/CAM), the claim is usually not directed to the software as such,
but to an industrial engineering process. A computer can aid such a
process but not implement it. A computer alone can only %(q:implement)
(= run) a software solution, and software running on a computer is
nothing more than software as such and thus not an %(q:invention) in
the sense of patent law.

#tse2: As long as the old term is still in use, it has to be defined here as
a synonym.

#Wro2: This amendment removes wordings from the Council text which are
unclear and redundant and whose only purpose seems to be to suggest
that the claimed invention can consist in nothing but software running
on a computer.

#eni2: This amendment is a simplified version of an amendment adopted in 1st
reading.

#WWa3: %(q:computer-implemented invention) means %(s:any) invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, %(s:the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs);

#noo: %(q:computer-implemented invention) means %(s:an) invention %(s:within
the meaning of the European Patent Convention), the performance of
which involves the use of a computer, computer network or programmable
apparatus.

#Wtn: Reference to the European Patent Convention definition is clearer.

#a52k: makes it clear that %(q:invention) does not refer to %(q:any) claim
object that involves computers but rather to inventions in the sense
of patent law.

#a53n: %(q:computer-implemented invention) means any invention within the
sense of patent law the application of which involves the additional
use of a computer, computer network or other programmable apparatus as
a control apparatus, which changes one or more features of the state
of the art in a given field;

#a53j: This statement shows that a 'computer-implemented invention' is equal
to 'involving the additional use of a computer'.

#a53k: makes it clear that a %(q:computer-implemented invention) is not
%(q:any) claim object involving computers but an invention in the
sense of patent law.  Similar to 54, but removal of %(q:technical) and
weakening of %(q:must) to %(q:should) is slightly disadvantageous. 
Also, the Commission has criticised the reference to non-EU law, and
in fact there may be more elegant ways to inherit the concepts of the
EPC than to refer directly to the EPC.

#WWa4: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#Wnm: %(q:computer-assisted invention) means any invention the performance
of which involves the use of a computer, a computer network or other
programmable apparatus and having one or more non-technical features
which are realised wholly or partly by means of a computer programme
or computer programmes besides the technical features that any
invention must contribute.

#rrt: This is very close to EP 1st reading article 2a ; this amendment aims
at ensuring that software cannot be patentable, but rather only
computer-assisted hardware inventions, software needs to be excluded
from the %(q:technical) field, as Parliament did at first reading.

#fet: similar to the amendment adopted in first reading, except that
%(q:implemented) was replaced with %(q:assisted) and the reference to
the EPC has been dropped, so it compromises with Commission criticism
that the EPC cannot be embedded in EU law like that.

#jua: %(q:invenciones implementadas en ordenador), toda invención para cuya
ejecución se requiera la utilización de un ordenador, una red
informática u otro aparato programable, teniendo la invención una o
más características que se realicen total o parcialmente mediante un
programa o programas de ordenador;

#loa: %(q:invenciones asistidas por ordenador), toda invención en el sentido
del Convenio de Patente Europea para cuya ejecución se requiera la
utilización de un ordenador, una red informática u otro aparato
programable y teniendo en su aplicación una o más características no
técnicas que se realicen total o parcialmente mediante un programa o
programas de ordenador, además de las características que cualquier
invención deba aportar.

#Wee: El artículo 52 del CEP establece claramente que un programa de
ordenador independiente (o un programa de ordenador como tal) no puede
constituir una invención patentable. Esta enmienda aclara que una
innovación es solamente patentable si se ajusta al artículo 52 del
EPC, independientemente de si un programa de ordenador forma parte de
su aplicación o no.

#ado: Esta enmienda se adoptó en una forma ligeramente diferente en la
primera lectura del PE. Se establecía %(q:invención aplicada a través
de ordenador) en vez de %(q:invención asistida por ordenador). La
razón para este cambio de la terminología se encuentra en la
justificación para el artículo 1.

#WWr: %(q:computer implemented inventions) means any invention, the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#vro: %(q:computer implemented inventions) means any invention within the
meaning of the European Patent Convention, the performance of which
involves the use of a computer, computer network or other programmable
apparatus, the invention having one or more features which are
realised wholly or partly by means of a computer program or computer
programs;

#ePt: Reference to the EPC clarifies the text.

#tth: This amendment makes it clear that a %(q:computer-implemented
invention) is not just any claim object involving computers but an
innovation that has passed the test of patentable subject matter. 
However unwarranted insinuations about patentability of software
solutions are retained, and the direct reference to an external body
of law may not be the most elegant way to solve the problem.  At least
the Commission has said that they find such a reference unacceptable.

#a57n: An %(q:invention) in the sense of patent law is a contribution to the
state of the art in a field of technology.  The contribution is the
set of features by which the scope of the patent claim as a whole is
claimed to differ from the prior art.  The contribution must be a
technical one, i.e. it must comprise technical features and belong to
a field of technology.  Without a technical contribution, there is no
patentable subject matter and no invention.  The technical
contribution must fulfill the conditions for patentability.  In
particular, the technical contribution must be novel and not obvious
to a person skilled in the art.

#a57j1: Upon close reading, it appears that according to the Council's text
the %(q:technical contribution) may consist solely of non-technical
features.  The text is full of redundant statements and misleading
ambiguities, but does contain some usable elements.

#a57j2: The concept of %(q:technical contribution) has pervaded the discussion
about the directive and generated great confusion and therefore to
some extent deserves to be clarified.  While intuitively and in the
subjective belief of most discutants the %(q:technical contribution)
appears to be related to the question of patentable subject matter
(Art 52 EPC), the EPO used the term as a means of abolishing the
subject matter test by mixing it into the non-obviousness test (Art 56
EPC) in obscure ways, which national courts and ministerial patent
officials have found difficult to follow.  It is thus particularly
important that, as far as the written law uses this term, it is
understood to be connected to the concept of %(q:invention)
(patentable subject matter) and dissociated from all other conditions
of patentability.

#a57j3: A similar amendment that was adopted in first reading by the EP. This
amendment adds some ideas of the Council such as that of subtracting
the prior art from the claimed object.  If worded carefully like here,
this can help provide further clarification.

#a57k: gets an essential clarification right: contribution is the new part
and that this part must fulfill the requirements of patentability.

#a58n: %(q:technical contribution) means an activity which changes the state
of the art, which is essentially new and not obvious. The technical
contribution shall be assessed by consideration of the difference
between the state of the art and the state after consideration of the
scope of the patent claim, which must comprise technical features,
that is, applied to material systems such as structures and materials,
substances and energy, as well as their manufacture and processing,
irrespective of whether or not these are accompanied by non-technical
features.

#a58j: If the parameters of the state of the material system undergo changes,
we are dealing with change to the technical state. If, on the other
hand, this new state leads to economic or other benefits then we are
speaking of a technical contribution.

#a58k: Corrects an error of the Council (according to which the
%(q:contribution) may consist entirely of non-technical features), but
fails to clarify the position of the contribution in the system of
patentability examination.

#a59o: %(q:technical contribution) means a contribution to the state of the
art in a field of technology which is new and not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether or not these are
accompanied by non-technical features.

#a59n: %(q:technical contribution) means a new way and non obvious for a
person skilled in the state of the art to use forces of nature to
solve a problem in a technical field ;

#a59j: This is very close to EP 1st reading article 2a ; In order to ensure
that software cannot be patentable, but rather only
computer-controlled hardware inventions, software needs to be excluded
from the %(q:technical) field, as Parliament did at first reading.

#a59k: The intention to define %(q:technical) by %(q:forces of nature) and to
exclude software is commendable, but this amendment fails to clarify
the function of the %(q:contribution) in the system of patentability
criteria.  It would be better to define %(q:technical) somewhere else.

#a60n: %(q:contribución técnica), también llamada %(q:invención), significa
una contribución en un campo de la tecnología. El carácter técnico de
la contribución es uno de los cuatro requisitos para la
patentabilidad. Además, para merecer una patente, la contribución
técnica tiene que ser nueva, no obvia, y susceptible de aplicación
industrial. El uso de fuerzas naturales para controlar los efectos
físicos más allá de la representación digital de la información
pertenece a un campo de la tecnología. El tratamiento, manejo, y
presentación de la información no pertenece a un campo de la
tecnología, ni siquiera donde los dispositivos técnicos son empleados
para tales fines. En todo caso, las características no técnicas de la
invención no serán tomadas en consideración en la evaluación de la
contribución técnica.

#a60j: El texto del Consejo combina la prueba de la invención (contribución
técnica) con otras pruebas no obvias (escalón de invención) y nuevas,
debilitando, por ello, todas las pruebas, desviándose del artículo 52
CPE, y creando problemas prácticos.

#iaW: Las %(q:cuatro fuerzas de la naturaleza) son un concepto reconocido de
epistemología (teoría de la ciencia). Mientras que las matemáticas y
el procesamiento de datos son abstractos y sin relación al que se
relaciona con las fuerzas de la naturaleza, podría, sin embargo,
sostenerse que algunos métodos empresariales dependen de la química de
las células cerebrales del cliente. Éstas  son fuerzas no
controlables, es decir, sujetas a la libre voluntad.

#etl: Por tanto el término %(q:fuerzas controlables de la naturaleza)
excluye claramente lo que necesita ser excluido y sin embargo
proporciona bastante flexibilidad para la inclusión de posibles campos
futuros de ciencia natural aplicada más allá de las actualmente
reconocidas %(q:4 fuerzas de la naturaleza).

#ica: Este concepto se ha formulado en la mayor parte de las jurisdicciones
e incluso se ha incorporado a  la legislación de países como Japón y
Polonia. La justificación clásica para el carácter técnico de las
%(q:invenciones aplicadas a través de ordenador) no es que el
significado de %(q:técnico) haya cambiado, sino  que el ordenador
efectivamente consume la energía de una manera controlada, y que la
%(q:invención) debe %(q:considerarse como un todo).

#eon: Los críticos de esta opinión, por ejemplo el Tribunal Federal Alemán
de Patentes, argumentan que la solución es completada mediante un
cálculo anterior abstracto, y solamente durante la aplicación no
inventiva sobre un sistema de procesamiento de datos, entran en juego
las fuerzas de la naturaleza.

#a60k: makes it clear that forces of nature must be part of the contribution
and that the contribution should be construed narrowly from the novel
features.  Based on 1st reading, leaves room for possible compromise
in Conciliation.

#a61n: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in a technical field. The
technical character of the contribution is one of the four
requirements for patentability. Additionally, to deserve a patent, the
technical contribution has to be new, non-obvious, and susceptible of
industrial application. The use of natural forces to control physical
effects beyond the digital representation of information belongs to a
technical field.  The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes. The method of data processing
by using a computer, network or other programmable apparatus is not
considered to belong to a field of technology.

#a61j: The definition of %(q:technical contribution) has to be clear in order
to determine what is patentable and what is not. Reference to
%(q:technical features) is too vague.

#a64n0: %(q:technical contribution) means the application of a new process
using physical forces in an inventive and non-obvious way, subject to
the following provisos:

#a64n1: The process may require a computation using a computer, but the normal
physical processes of a computer cannot be part of the %(q:technical
contribution): a new process using physical forces separate from all
computation is required.

#a64n2: The process may perform communication with computers or people, but
the physical processes of a pre-existing communication apparatus
cannot be part of the %(q:technical contribution): a new process using
physical forces separate from any pre-existing communication apparatus
is required.

#a64j: Ensures compatibility with the software copyright directive and
ensures users have access to interoperable programme systems and
networks.

#a65n: %(q:contribution technique) désigne une solution à un problème dans un
domaine technique.

#a65j: L’article 2(b), combiné avec l’article 2(c) suggéré, propose une
définition non-tautologique de l’expression %(q:contribution
technique).  Il est clair qu’une  contribution technique correspond à
une solution technique :  une solution à un problème dans un domaine
technique.  L’évaluation mentionnée à la seconde phrase de l’article
2(b) est relative à une approche largement utilisée pour la
détermination de l’activité inventive.  La seconde phrase est par
conséquent supprimée de l’article pour se référer seulement à une
contribution technique.

#a65k: Rectifies EPO error, correctly disentangles contribution from
non-obviousness, but apart from that does not clarify much.

#oeW: %(q:Interoperability) means the ability of a computer program to
communicate and exchange information with other computer programs and
mutually to use the information which has been exchanged, including
the ability to use, convert, or exchange file formats, protocols,
schemas, interface information or conventions, so as to permit such a
computer program to work with other computer programs and with users
in all the ways in which they are intended to function.

#cho: It is necessary to design the meaning of interoperability.

#amm: %(q:Technical) means the identification of a physical effect which
goes beyond the digital representation of information and the normal
physical interaction between software and hardware of a computer,
network or other programmable apparatus.

#fno: It is necessary to make clear that the term %(q:technical) demands the
identification of a physical effect. Such an effect must be produced
in addition to effects with regard to digital representation of
information and such caused merely by the interaction between software
and hardware with regard to operability.

#nsi: meaningless definition, resulting from flawed transformation of a
flawed EPO doctrine.

#Wui: It could have been a good idea to define %(q:technical) simply as
%(q:physical).  In its current convoluted state, the amendment is
meaningless.  It reads like an erroneous term transformation of the
EPO's erroneous %(pt:doctrine of %(q:further technical effect)).

#a68n: %(q:field of technology) means an industrial application domain
requiring the use of controllable forces of nature to achieve
predictable results. %(q:Technical) means %(q:belonging to a field of
technology).

#tle: These definitions are essential, especially to clarify the term
%(q:field of technology).

#a68k: provides the needed definition of %(q:technical) by reference to
forces of nature.

#oeW2: %(q:Interoperability) means the ability of a computer program to
communicate and exchange information with other computer programs and
mutually to use the information which has been exchanged, including
the ability to use, convert, or exchange file formats, protocols,
schemas, interface information or conventions, so as to permit such a
computer program to work with other computer programs and with users
in all the ways in which they are intended to function.

#lpe: This amendment clarifies that the physical use of a computer cannot be
considered as the %(q:technical contribution).

#a70n: A %(q:field of technology) is a discipline of applied sciences in
which new knowledge is gained by experimentation with controllable
forces of nature. %(q:Technical) means %(q:belonging to a field of
technology);

#a70j1: This amendment clarifies the term %(q:field of technology) from Art 27
TRIPs.

#a70j2: It is an improved version of the Parliament's first reading article
2(c).

#a70j3: A discipline is normally characterised not by its domain of
application but by the way in which it gains knowledge. For patent
granting, what matters is where the achievement lies, not to which
domain it is applied. Also, %(q:industrial applicability) is a
separate requirement of patentability. Patentability requirements
should stand on their own, relying on each other as little as
possible.

#a70k: provides a clear and adequate definition of %(q:technology), with
minimal reliance on other patentability criteria and thus minimal
circularity.

#noW: %(q:Technical) means %(q:belonging to a field of technology);

#Woh2: This is very close to EP 1st reading article 2a ; In order to ensure
that software cannot be patentable, but rather only
computer-controlled hardware inventions, software needs to be excluded
from the %(q:technical) field, as Parliament did at first reading.

#a72k: The question of splitting must be clarified before this amendment can
be recommended.

#a73n: %(q:domaine technique) signifie toute activité utilisant directement
ou indirectement des forces de la nature contrôlables afin d’obtenir
des résultats prévisibles dans le monde physique tels que des signaux
électriques, radios ou lumineux. Le traitement de l’information dans
le but d’exécuter ou de soutenir une telle activité doit être
considéré comme appartenant à un domaine technique, alors que le
traitement de l’information à des fins de calcul, de gestion de
valeurs financières  ou de traitement de texte ne doit pas être
considéré comme appartenant à un domaine technique.

#a73j1: La première phrase codifie essentiellement une doctrine utilisée dans
la jurisprudence actuelle allemande (« Logikverifikation » de 1999,
BGHZ 143, 255) et propose quelques exemples de types de résultats à
considérer. Les signaux électriques, radios et lumineux, qui ont fait
l’objet d’une mention dans le document de travail 2002/0047 (COD),
sont caractéristiques de la technologie moderne.

#a72j2: La seconde phrase clarifie que le traitement de l’information pour
exécuter ou soutenir des activités tels que mentionnées à la première
phrase est considéré comme technique, alors que le traitement à
d’autre fins (par exemple : méthode de gestion) ne peut être considéré
comme technique. Cette limite reflète la pratique actuelle en
Allemagne et en Europe.

#a73k1: Symbols are never %(q:technical).

#a73k2: The operation of a machine is technical, but an operation manual is
not.  The use of the machine is technical, but the instruction manual,
computer program or algorithm that describes it is not.

#faq: %(q:campo de la tecnología) significa ámbito de aplicación industrial
requiriendo el uso de fuerzas de la naturaleza controlables para
alcanzar resultados fiables. %(q:Técnico) significa %(q:perteneciente
a un campo de la tecnología);

#als: El hecho de que los aparatos programables, tales como un ordenador
genérico, hagan uso de efectos físicos para procesar la información no
debe utilizarse para permitir la protección de patentes a programas
que funcionan por medio de tales instrumentos.

#anp: Esta enmienda también aclara el término no definido de ADPIC %(q:campo
de tecnología)

#rWW: Esta enmienda corresponde a la letra c del artículo 2 del texto
consolidado de la primera lectura del Parlamento Europeo.

#eWd: %(q:industria) en el sentido de la ley de patentes significa
producción automatizada de bienes materiales;

#sfa: Se quiere evitar que las innovaciones en la %(q:industria de la
música) o la %(q:industria de los servicios jurídicos) se ajusten con
los requisitos de ADPIC relativos a la %(q:aplicabilidad industrial).
El concepto de %(q:industria) abarca amplios significados y es
utilizado frecuentemente en la actualidad de manera inapropiada en el
contexto de la ley de patentes.

#oli: Esta enmienda corresponde a la letra d) del artículo 2 del texto
consolidado de la primera lectura del PE.

#oio: %(q:Field of technology) means an industrial application domain
requiring the use of controllable forces of nature to achieve
predictable results.

#Wcn: This is very close to EP 1st reading article 2a ; In order to ensure
that software cannot be patentable, but rather only
computer-controlled hardware inventions, software needs to be excluded
from the ‘technical’ field, as Parliament did at first reading.

#ded: Provides the needed definition of %(q:technical) by reference to
forces of nature.  Splitting the amendment may be a good idea.

#Woe: The production and distribution of information goods is not an
%(q:industry) in the sense of patent law.

#fce: Information goods can be reproduced on millions of computers within
seconds at near to zero cost. More than material goods, information
goods are suitable for production by freelancers. The economics
differ, and the business models for information goods tend to be
closer to those of the service sector than of the classical
%(q:industry) sector.

#idr: This amendment clarifies, using a negative definition, a central term
of Art 27 TRIPs which has been used in several provisions and
amendments within this directive. If the term is to retain any
limiting meaning at all, production of information goods can not fall
within it.

#Woe2: The production and distribution of information goods is not an
%(q:industry) in the sense of patent law.

#fce2: Information goods can be reproduced on millions of computers within
seconds at near to zero cost. More than material goods, information
goods are suitable for production by freelancers. The economics
differ, and the business models for information goods tend to be
closer to those of the service sector than of the classical
%(q:industry) sector.

#taj: This amendment clarifies, using a negative definition, a central term
of Art 27 TRIPs which has been used in several provisions and
amendments within this directive. If the term is to retain any
limiting meaning at all, production information goods can not fall
within it.

#Woe3: The production and distribution of information goods is not an
%(q:industry) in the sense of patent law.

#fce3: Information goods can be reproduced on millions of computers within
seconds at near to zero cost. More than material goods, information
goods are suitable for production by freelancers. The economics
differ, and the business models for information goods tend to be
closer to those of the service sector than of the classical
%(q:industry) sector.

#idr2: This amendment clarifies, using a negative definition, a central term
of Art 27 TRIPs which has been used in several provisions and
amendments within this directive. If the term is to retain any
limiting meaning at all, production of information goods can not fall
within it.

#oeW3: %(q:Interoperability) means the ability of a computer program to
communicate and exchange information with other computer programs and
mutually to use the information which has been exchanged, including
the ability to use, convert, or exchange file formats, protocols,
schemas, interface information or conventions, so as to permit such a
computer program to work with other computer programs and with users
in all the ways in which they are intended to function.

#sWn: It is necessary to define the term %(q:interoperability) in the
articles.

#icW: %(q:Industry) in the sense of patent law means commercially organised
production of material goods;

#e7m: This amendments clarifies, using a positive defintion, a central term
of Art 27 TRIPs which has been used in several provisions and
amendments within this directive.

#glc: Innovations in the %(q:music industry) or %(q:legal services industry)
should not meet the TRIPS requirement of %(q:industrial
applicability). The word %(q:industry) is nowadays often used in
extended meanings which are not appropriate in the context of patent
law.

#ess: In the tradition of patent law, %(q:industry) refers to the primary
and secondary sector, i.e. it includes agriculture. The distinction
between these sectors and the tertiary (software and service) sector
is economically meaningful. E.g. in the anti-trust procedings against
IBM, the company was split into two along these lines.

#ltn: This amendment corresponds to article 2(d) in the consolidated text of
the EP’s first reading, except that %(q:automated) was replaced with
%(q:commercially organised), so as to approximate the original meaning
in the legal tradition more closely.

#rMW: It should be noted that the requirement of industrial applicability in
itself has very little excluding force. Most advances in the area of
mathematics or business methods are applicable to industry, no matter
how the term is defined.

#icW2: %(q:Industry) in the sense of patent law means commercially organised
production of material goods;

#e7m2: This amendments clarifies, using a positive defintion, a central term
of Art 27 TRIPs which has been used in several provisions and
amendments within this directive.

#glc2: Innovations in the %(q:music industry) or %(q:legal services industry)
should not meet the TRIPS requirement of %(q:industrial
applicability). The word %(q:industry) is nowadays often used in
extended meanings which are not appropriate in the context of patent
law.

#ess2: In the tradition of patent law, %(q:industry) refers to the primary
and secondary sector, i.e. it includes agriculture. The distinction
between these sectors and the tertiary (software and service) sector
is economically meaningful. E.g. in the anti-trust procedings against
IBM, the company was split into two along these lines.

#ltn2: This amendment corresponds to article 2(d) in the consolidated text of
the EP’s first reading, except that %(q:automated) was replaced with
%(q:commercially organised), so as to approximate the original meaning
in the legal tradition more closely.

#rMW2: It should be noted that the requirement of industrial applicability in
itself has very little excluding force. Most advances in the area of
mathematics or business methods are applicable to industry, no matter
how the term is defined.

#Wsr: %(q:information processing method) means any processing method
handling digitally represented information.

#ese: To garantee that pure  software cannot be patented, but only material
inventions controlled by computers, we must exclude the field of
software from the field of what is %(q:technical_) as did the
Parliament in its 1st reading. The world of software is the world of
digital information.

#ime: %(q:interopérabilité) signifie la capacité pour un programme
informatique de communiquer, échanger des informations avec d'autres
programmes informatiques et utiliser mutuellement l'information
échangée, ce qui comprend la capacité d'utiliser, de convertir et
d'échanger des formats de fichiers, des protocoles, des schémas et des
informations relatives à des interfaces ou des conventions, afin de
permettre à ce programme informatique de communiquer et de coopérer
avec d'autres programmes informatiques et avec des utilisateurs de
toutes les façons dont ils sont destinés à interagir.

#trW: Il est essentiel, d'autre part, dans le cadre de cette directive, de
fournir une définition précise de l'interopérabilité et des actes qui
lui sont nécessaires.

#eun: Para ser patentable, una invención implementada en ordenador deberá
ser susceptible de aplicación industrial, ser nueva y suponer una
actividad inventiva. Para entrañar una actividad inventiva, la
invención implementada en ordenador deberá aportar una contribución
técnica.

#pso: La primera frase fue ya incorporada  en el artículo 2 b) de la
resolución aprobada por el PE. La segunda frase mezcla la
%(q:invención) o %(q:contribución técnica) con la condición de escalón
de invención.

#But: El Consejo saca esta %(q:contribución técnica en el escalón de
invención) de la Oficina Europea de Patentes, donde se utiliza para
permitir que ordenadores genéricos que funcionan con programas
informáticos sean considerados invenciones patentables.

#lne: Con las definiciones del Parlamento, no es necesaria ninguna condición
adicional para la patentabilidad aparte de los mencionados en el
artículo 52 del CEP (según lo mencionado en el artículo2 b))

#rte: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and must involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution.

#iie: Member States shall ensure that inventions are patentable irrespective
of whether or not they use computerised means and that, vice versa, no
one may patent algorithms, software or information processing methods,
whether or not they are combined with technical mechanisms.

#fIt: The definition of inventive step supplied by the Council is not
suitable, as it refers to the definition of a technical contribution,
which is not itself explicitly defined, but which does, however, make
reference, by means of the word ‘contribution’, to an inventive step.
In order to break this vicious circle, inventive step needs to be
defined in relation to itself. In order for a new computer program
executed on a technical device not to be a patentable invention, the
inventive step has to be evaluated solely in relation to the technical
features of the patent claim.

#kae: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and must make involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution.

#oas: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and make a technical
contribution. The technical contribution must be new and involve an
inventive step.

#etn: The inventive step should be in the technical contribution for the
computer-implemented invention to be patentable.

#rWt: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and must involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution.

#tsi: In order to be patentable, an [computer-aided] invention must make a
technical contribution. The technical contribution must be new and
involve an inventive step. If there is no technical contribution,
there is no patentable subject matter, and no invention.

#eWn: Even the Council's Art 2(b) agrees that the %(q:technical
contribution) must be new and involve an inventive step, and not vice
versa. The second sentence makes it absolutely clear that the
%(q:technical contribution) requirement is closely connected to the
requirement of patentable subject matter and dissociated from that of
non-obviousness.

#ecr: Moreover, this amendment deletes the attribute
%(q:computer-implemented), since the above logic applies to all
patentable inventions. There is no advantage in creating sui generis
software patent law.

#rWt2: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and must involve an
inventive step. In order to involve an inventive step, a
computer-implemented invention must make a technical contribution.

#rWs: In order to be patentable, a computer-aided invention must make a
technical contribution. The technical contribution must be new and
involve an inventive step. If there is no technical contribution,
there is no patentable subject matter, and no invention.

#Wve: Even the Council's Art 2(b) agrees that the %(q:technical
contribution) must be new and involve an inventive step, and not vice
versa. The second sentence makes it absolutely clear that the

#ecr2: Moreover, this amendment deletes the attribute
%(q:computer-implemented), since the above logic applies to all
patentable inventions. There is no advantage in creating sui generis
software patent law.

#a90n: In order to be patentable, a computer-implemented invention must make
a technical contribution and be suitable for practical, including
industrial, application, it must be new and change the current state
of the art.

#a90j: This article refers to the practical significance of the invention in
question, with the new element relating to both software and hardware
and changing the state of the art.

#a90k: Good intention, but uses the term %(q:invention) to refer to
unpatentable subject matter and fails to correct those errors of the
Council which need to be corrected here.

#sis: In order to be patentable, a computer-controlled invention must be
new, susceptible of industrial application and involve an inventive
step. The inventive step shall be assessed by consideration of the
difference between all of the technical features included in the scope
of the patent claim considered as a whole and the state of the art,
irrespective of whether or not such features are accompanied by
non-technical features.

#i44: This first sentence of this amendment restates the former EP 1st
reading amendment 4.1 first part and the second sentence restates
former EP 4.3 which were deleted by Council.

#sin: The definition of the inventive step of the CCP is insufficient
because it refers to the definition of what a technical contribution
is, but this is not defined precisely in the text although there is a
reference (with the word %(q:contribution)) to an inventive activity.
to break this vicious circle we must define the inventive activity as
such. So that a new computer programme executed on a technical
apparatus can not constitute a patentable invention, the inventive
step must be assessed in relation to the sole technical
characteristics of the patent claim.

#ato: Member States shall ensure that data processing is not considered to
be a field of technology within the meaning of patent law, and that
innovations in the field of data processing are not considered to be
inventions within the meaning of patent law.

#a92j1: This amendment clarifies Art 27 TRIPs by a negative definition of
%(q:fields of technology).

#a92j2: Data processing is a branch of mathematics, a mental activity whose
innovative advances lie in the area of abstraction, and whose
technical aspects, if existent at all, are known and trivial. This
amendment in no way affects the patentability of the computers
themselves, or of any processes involved in implementing the abstract
data processing machine into silicon, wood or DNA.

#a92k: Needed translation of Art 52 EPC into the language of Art 27 TRIPs. 
If the directive had to consist in one phrase, this would be it.

#ato2: Member States shall ensure that data processing is not considered to
be a field of technology within the meaning of patent law, and that
innovations in the field of data processing are not considered to be
inventions within the meaning of patent law.

#a93j3: Strictly speaking, the amendment does not even exclude software from
patentability. Rather, it forbids certain extensive interpretations of
Art 27 TRIPs which have been used to circumvent Article 52 of the
European Patent Convention and to reduce the freedom of the judiciary
to interpreting this article in meaningful ways (i.e., this amendment
makes sure that one cannot interpret TRIPs in a way which makes it
require software patents, but does not say anything about whether or
not it allows them).

#a93j4: This amendment corresponds to article 3 in the consolidated text of
the EP’s first reading.

#pcb: The application for a patent must disclose the invention in a manner
sufficiently clear and complete for it to be carried out by a person
skilled in the art.

#akh: This amendment clarifies expressly that a patent application has to
disclose an invention clearly and comprehensively, so that it can be
implemented by someone working in the field.   The expression %(q:a
person skilled in the art) is a well-established term of patent law
which means someone of ordinary skill in the relevant technical field.

#ato3: Member States shall ensure that data processing is not considered to
be a field of technology within the meaning of patent law, and that
innovations in the field of data processing are not considered to be
inventions within the meaning of patent law.

#tnl3: This amendment clarifies Art 27 TRIPs by a negative definition of
%(q:fields of technology).

#cnp3: Data processing is a branch of mathematics, a mental activity whose
innovative advances lie in the area of abstraction, and whose
technical aspects, if existent at all, are known and trivial. This
amendment in no way affects the patentability of the computers
themselves, or of any processes involved in implementing the abstract
data processing machine into silicon, wood or DNA.

#toa2: Strictly speaking, the amendment does not even exclude software from
patentability. Rather, it forbids certain extensive interpretations of
Art 27 TRIPs which have been used to circumvent Article 52 of the
European Patent Convention and to reduce the freedom of the judiciary
to interpreting this article in meaningful ways (i.e., this amendment
makes sure that one cannot interpret TRIPs in a way which makes it
require software patents, but does not say anything about whether or
not it allows them).

#ttP2: This amendment corresponds to article 3 in the consolidated text of
the EP’s first reading.

#pcb2: The application for a patent must disclose the invention in a manner
sufficiently clear and complete for it to be carried out by a person
skilled in the art.

#akh2: This amendment clarifies expressly that a patent application has to
disclose an invention clearly and comprehensively, so that it can be
implemented by someone working in the field.   The expression %(q:a
person skilled in the art) is a well-established term of patent law
which means someone of ordinary skill in the relevant technical field.

#Wpc: Un programa de ordenador como tal no podrá constituir una invención
patentable.

#Bat: Las últimas enmiendas de la Comisión incorporadas en la reunión del
Consejo 18 de mayo de 2004 redefinen un programa de ordenador como tal
para hacer referencia al código de fuentes o al código automático de
un programa individual de ordenador.

#lih: Nadie está interesado en las patentes de tales programas de ordenador
individuales. Estas inserciones no responden a ningún propósito
regulador sino que más bien imponen una interpretación del artículo 52
CEP que priva de sentido a la ley y es rechazada por el Tribunal
Federal Alemán e incluso por la OEP.

#WsW: La doctrina de los %(q:efectos técnicos más allá de la interacción
física normal) es un formalismo introducido con la única intención de
convertir los métodos empresariales aplicados por ordenador en
patentables, según lo explicado en el apéndice 6 de un informe enviado
por la OEP a las oficinas de patentes de EEUU y Japón:
http://www.european-patente-office.org/tws/appendix6.pdf

#iWx: El cambio de %(q:invención) a %(q:innovación) es necesario porque
%(q:invención) es sinónimo de %(q:materia legal) en el CEP.

#qnj: La redacción original sugiere que todo lo que se puede hacer en un
ordenador es por definición una invención (y por lo tanto llega a ser
patentable si es igualmente nuevo, no obvio e industrialmente
aplicable).

#a97k: The directive should clarify wordings such as %(q:as such), rather
than reiterate them in an unclarified form.

#a98n: No se considerará que una innovación asistida por ordenador aporta una
contribución técnica meramente porque implique el uso de un ordenador,
red u otro aparato programable.

#a98j: Ver justificación de la enmienda al artículo 4 apartado 1 de Manuel
Medina Ortega.

#a98k: this does not exclude anything: the argument for contribution of
programs and business methods will always be that they make something
faster or more efficient than before, not that they use known data
processing equipment.  The latter will always lack novelty and
inventive step anyway.

#a99n: Programs for computers are not inventions in the sense of patent law.

#a99j: Art 52(2) EPC states that programs for computers are not inventions in
the sense of patent law. It is a good idea to transfer this provision
into EU law. The additional provision of Art 52(3) (exclusion only
pertains to computer programs as such) should be reflected in an
additional clause (amendment to Art 4.2), which also clarifies the
above provision. The EU law should be clearer, not less clear, than
Art 52 EPC.

#a99k: It is a good idea to restate Art 52 EPC in the directive.  As in the
original, the %(q:as such) phrase (Art 52.3) should be kept separate
from the %(q:programs are not inventions) phrase (Art 52.2), and it
should be concretised separately.

#a102n: A computer program as such cannot constitute a patentable invention.
Computer programs as such are effectively protected by copyright. The
objective of the directive is to provide a proper legal certainty as
an incentive for innovation for inventors and does not affect
computer-programmers.

#a102j: Although for the legal point of view it is clear that the directive
will not affect the open-source, it must be clearly pointed out that
the objective of the directive is to make the computer-implemented
inventions patentable and not computer programmes as such. The target
group it concerns are inventors and not computer programmers.

#a102k: The statement that computer programmers are unaffected serves no
regulatory purpose.  The appositive %(q:as such) is used in a vague,
misleading way.  The only discernible meaning of the amendment seems
to be to insult programmers as either uninnovative or law-ignorant or
both.

#a103n: The content of a computer program cannot constitute a patentable
invention. Software shall remain under the protection of copyright
according to Directive 91/250 CEE.

#a103j: A patent cannot claim a monopole on software, whatever carrier the
software is on because this would mean that software is patentable.

#a103k: An attempt to avoid the wordplays based on %(q:computer program as
such), but %(q:content of computer program) is difficult to interpret
in the context of patent claims, and the wording %(q:cannot constitute
an invention) still leaves open the interpretation that nothing is
excluded because the excluded object belongs to a category that
%(q:can not) fit into the context of patent claims.

#a104n: A computer program is a solution of a problem by calculation with the
abstract entities of a generic data processing machine, such as input,
output, processor, memory, storage as well as interfaces for
information exchange with external systems and human users. A computer
program may take various forms, e.g. a computing process, an
algorithm, or a text recorded on a medium. If the contribution to the
known art resides solely in a computer program then the subject matter
is not patentable in whatever manner it may be presented in the
claims.

#a104j: Definition of the computer program is important for determining the
patentability.

#a104k: definition of %(q:computer program) is essential for clarification of
Art 52 EPC and for delimiting the scope of exclusion of related terms
such as %(q:data processing).  This provision could either be a new
insertion (Kauppi) or a replacement of the Council's Art 4 par 2
(Kudrycka, Zwiefka; Bertinotti).

#a105j1: This amendment proposes to replace the Council's amendment with a text
which concretises the meaning of Art 52(2) and 52(3) EPC. This
proposal is based on the explanation given in the original EPO
Examination Guidelines of 1978 and subsequent caselaw.

#a105j2: The Commission’s last minute amendments inserted at the Council 18 May
2004 meeting redefine a %(q:computer program as such) to referring to
the %(q:source code or machine code) of an individual computer
program, as defined by copyright. This is meaningless in the context
of patent law. The effect of the Council's proposal can only be to
make Art 52 EPC meaningless.

#a105j3: The %(q:normal interaction between programs and computers) is about as
well defined as the %(q:normal interaction between the cook and the
recipe). It is a legal formula which the EPO invented in 1998 in order
to circumvent Art 52 EPC. Only two years later, the EPO itself
commented this formula as follows:

#a105j4: There is no need to consider the concept of %(q:further technical
effect) in examination, and it is preferred not to do so for the
following reasons: firstly, it is confusing to both examiners and
applicants; secondly, the only apparent reason for distinguishing
%(q:technical effect) from %(q:further technical effect) in the
decision was because of the presence of %(q:programs for computers) in
the list of exclusions under Article 52(2) EPC.

#a105j5: If, as is to be anticipated, this element is dropped from the list by
the Diplomatic Conference, there will no longer be any basis for such
a distinction. It is to be inferred that the Board of Appeals would
have preferred to be able to say that no computer-implemented
invention is excluded from patentability by the provisions of Articles
52(2) and (3) EPC.

#a106n: A computer program is a solution of a problem by calculation with the
abstract entities of a generic data processing machine, such as input,
output, processor, memory, storage as well as interfaces for
information exchange with external systems and human users. A computer
program may take various forms, e.g. a computing process, an
algorithm, or a text recorded on a medium. If the contribution to the
known art resides solely in a computer program then the subject matter
is not patentable in whatever manner it may be presented in the
claims.

#a107n: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use only of a
computer, network or other programmable apparatus, with no practical
possibility of its application in the starting and controlling of
material systems. Accordingly, inventions involving exclusively
computer programs, whether expressed as source code, as object code or
in any other form, and those which implement business, as well as
mathematical computational methods, texts recorded on a carrier and
algorithms themselves shall not be patentable.

#a107j: Two statements are included in this point: firstly, it removes the
possibility of using a computer or network or other control apparatus
to produce programs that will be submitted for patent, and secondly it
excludes internal computer programs as well as all mathematical
methods and different algorithms as well as business methods from
being patentable solutions.

#a107k: Good intentions, removes the Council's empty talk about %(q:further
technical effects), but retains some of its other key errors:
%(ol|uses the term %(q:invention) to refer to unpatentable subject
matter|talks about the expression side of a computer program which is
offtopic and designed to be abused for voiding the exclusion.|There is
always a %(q:possibility) that a program be used for manufacturing
etc, and %(q:inventions) can always be said to not %(q:involve
exclusively computer programs), so this limits nothing)

#tWo3: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus. Accordingly,
inventions involving computer programs, whether expressed as source
code, as object code or in any other form, which implement business,
mathematical or other methods and do not produce any technical effects
beyond the normal physical interactions between a program and the
computer, network or other programmable apparatus in which it is run
shall not be patentable.

#tep: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus. Accordingly,
computer programs, which implement business, mathematical or other
methods and do not produce any technical effects beyond the normal
physical interactions between a program and the computer, network or
other programmable apparatus in which it is run, shall not be
patentable.

#fIt2: The definition of inventive step supplied by the Council is not
suitable, as it refers to the definition of a technical contribution,
which is not itself explicitly defined, but which does, however, make
reference, by means of the word ‘contribution’, to an inventive step.
In order to break this vicious circle, inventive step needs to be
defined in relation to itself. In order for a new computer program
executed on a technical device not to be a patentable invention, the
inventive step has to be evaluated solely in relation to the technical
features of the patent claim.

#tjr: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus.  Accordingly,
inventions involving computer programs, whether expressed as source
code, as object code or in any other form, which implement business,
mathematical or other methods and do not produce any technical effects
beyond the normal physical interactions between a program and the
computer, network or other programmable apparatus in which it is run
shall not be patentable.

#vtr: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus.

#ent: Inventions involving computer programs, whether expressed as source
code, as object code or in any other form, which implement business,
mathematical or other methods and do not produce any technical effects
beyond the normal physical interactions between a program and the
computer, network or other programmable apparatus in which it is run
shall not be patentable.

#tpo: A technical amendment to define more clear in the text that inventions
involving computer programs, which implement business, mathematical or
other methods and do not produce any technical effects beyond the
normal physical interactions between a program and the computer,
network or other programmable apparatus in which it is run are not
patentable.

#tri: The Member States shall ensure that data processing is not considered
to be a field of technology as defined under patent law, and that
innovations in the field of data processing are not considered
inventions as defined under patent law.

#iWt: This is EP 1st reading article 3. This ensures that software is not
considered technical, which is important for compliance to TRIPS.

#a111n: Member States shall ensure that data processing solutions are not
considered to be patentable inventions merely because they improve
efficiency in the use of resources within data processing systems.

#a111j1: Nobody ever writes software without trying to optimise the use of
computing resources.

#a111j2: This amendment makes sure that this fact does not justify the granting
of a patent. This codifies both UK case law (Gale's application) and
Germany’s case law (BpatG’s ruling in the Error Search case). As the
German court found: if an improvement of efficiency in the use of
computing ressources, such as time or data space, is deemed to be a
technical contribution, then all computer-implemented business methods
become patentable.

#a111j3: This amendment corresponds to article 6 in the consolidated text of
the EP’s first reading, except that

#a111k: These amendments codify both UK case law (Gale's application) and
Germany's case law (German High Patent Court's ruling in the
%(bs:Error Search) case). As the German court found: if an improvement
of efficiency in the use of computing resources, such as time or data
space, is deemed to be a technical contribution, then all
computer-implemented business methods become patentable.

#ler: Los Estados miembros garantizarán que el procesamiento de datos no se
considere un ámbito de tecnología en el sentido del derecho de
patentes y que las innovaciones en el ámbito del procesamiento de
datos tampoco se considere invenciones en el sentido del derecho de
patentes.

#Dtp: Se garantiza el cumplimiento del ADPIC asegurándose que los programas
informáticos no pertenezcan al ámbito de la tecnología. Obsérvese que
esto no excluye a los dispositivos utilizados para el procesamiento de
los datos de la patentabilidad.

#tlc: Un ordenador puede solamente llevar a cabo dicho procesamiento, pero
crear  una nueva clase de ordenadores es un progreso en la ingeniería
electrónica y no en el procesamiento de datos.

#Wee2: Los Estados miembros garantizarán que las soluciones a los problemas
técnicos aplicadas a través de ordenador no sean consideradas como
invenciones patentables cuando solamente mejoren la eficacia en el uso
de recursos en los sistemas de procesamiento de datos.

#Wea: Se garantiza que hacer que un programa funcione con mayor rapidez o
que use menos memoria no pueda ser utilizado como justificación para
conceder una patente.

#aer: Esta enmienda corresponde al artículo 6 del texto consolidado en
primera lectura del PE.

#car51: Member States shall ensure that a computer-implemented invention may
be claimed as a product, that is as a programmed computer, a
programmed computer network or other programmed apparatus, or as a
process carried out by such a computer, computer network or apparatus
through the execution of software.

#a116n: Member States shall ensure that a computer-implemented invention may
be claimed as a product in the form of a machine or technical
apparatus with the addition of a computer, computer network or other
programmable apparatus or as a technical process which is started and
controlled by means of sensors,  circuit breakers and commutators
through such a computer, computer network or other control apparatus.

#a116j: It is extremely important to regard the combination of machine or
apparatus with computer, another network or programmable apparatus,
together with the appropriate programs, as a patentable 'new article'
. The criterion for such a 'new article' should include new, hitherto
unknown, useful features in these combinations of apparatus plus
computer plus program.

#a116k: is better than the Council version, but is overly specific (the
inclusion of sensors, circuit breakers etc is just as unnecessary in
this context as the Council's specification of computers, networks
etc).

#a117n: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is a programmed device, or as a
technical production process.

#a117j: This is 1st reading article 7.1; it guarantees that only technical
inventions can be claimed.

#a117k: removes Council insinuations that software is patentable, but uses the
term %(q:computer-implemented invention), which itself implies
patentability of software, and specifies more than needed by limiting
claims to %(q:production processes).

#a118n: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is a programmed device, or as a
technical production process.

#a118j1: By definition, a process carried out by a computer corresponds to the
computer programs run on that computer. It must be ensured that claims
will apply only to technical inventions that can be implemented under
computer control and not simply to the computer programs used for such
control (or when they are run on an ordinary desktop computer not
being used to control a technical invention).

#a118j2: This amendment corresponds to Article 7(1) of the consolidated text
resulting from the EP first reading.

#a118k: completely removes Council insinuations that software is patentable. 
specifies more than needed by limiting claims to %(q:production
processes).

#a119n: Member States shall ensure that a computer-aided invention may be
claimed as a product, that is as a programmed apparatus, or as a
process carried out by such an apparatus.

#a119j1: Software in combination with generic computing equipment is still not
more than software (as such).  Suggestions that software can be
patentable are outside the scope of this article and should be
avoided.

#a119j2: This amendment roughly corresponds to article 7.1 in the consolidated
text of the EP’s first reading (except that %(q:implemented) has been
replaced with %(q:aided), %(q:device) with %(q:apparatus) and
%(q:technical production) has been deleted from %(q:technical
production process).

#a119k: removes unneeded insinuations about patentability of software from the
otherwise correct approach of the Commission; refrains from adding new
insinutations to the contrary: this article is only about the
allowable claim types, not about patentability criteria.

#eid: Member States shall ensure that the distribution and publication of
information, in whatever form, can never constitute direct or indirect
infringement of a patent.

#ets: Freedom of publication, as stipulated in Art 10 ECHR, can be limited
by copyright but not by patents. Patent rights are broad and unsuited
for information goods. This amendment does not make any patents
invalid, rather it limits the ways in which a patent owner can enforce
his patents. Such a provision should be complemented by other
provisions which make sure that information patents are not granted in
the first place.

#aef: This amendment is a simplified and reduced version of article 7
paragraph 3 in the consolidated text of the EP’s first reading.

#a122k: affirms right of publication as a limit to patent rights.

#a123n: Member States shall ensure that the distribution and publication of
information, in whatever form, can never constitute direct or indirect
infringement of a patent.

#wke: Member States shall ensure that whenever a patent claim names features
that imply the use of a computer program, an well-functioning and
well-documented program text shall be published as part of the patent
description without any restricting licensing terms.

#hne: A program listing is an excellent means of describing to a skilled
person what a computer-aided process does. This amendment ensures that
the obligation of disclosure is taken seriously, and that software is
treated as a means of describing the invention, rather than as an
invention in itself. The Commission's objection that patent law does
not normally require the disclosure of a full reference implementation
does not apply, because this amendment does not ask for a reference
implementation but only for an accurate description.

#oad: This requirement makes it a little more difficult to block people from
doing things you even haven't done yourself, but which are obviously
possible since the computing model is perfectly defined and you always
know in advance what is theoretically possible with a computer. When
you publish working source code you at least offer some real knowledge
on how to solve the problem, unlike when you say in the claims
language that a

#qrn: Note that this amendment does not require that the source code for all
programs of the patent owner which use these features be disclosed. He
only has to provide a single, simple text which describes the
monopolised functionality in a programming language.

#rWo: This amendment roughly corresponds to article 7 paragraph 5 in the
consolidated text of the EP’s first reading (it’s been made more clear
that only an example must be provided).

#wke2: Member States shall ensure that whenever a patent claim names features
that imply the use of a computer program, an well-functioning and
well-documented program text shall be published as part of the patent
description without any restricting licensing terms.

#hne2: A program listing is an excellent means of describing to a skilled
person what a computer-aided process does. This amendment ensures that
the obligation of disclosure is taken seriously, and that software is
treated as a means of describing the invention, rather than as an
invention in itself. The Commission's objection that patent law does
not normally require the disclosure of a full reference implementation
does not apply, because this amendment does not ask for a reference
implementation but only for an accurate description.

#oad2: This requirement makes it a little more difficult to block people from
doing things you even haven't done yourself, but which are obviously
possible since the computing model is perfectly defined and you always
know in advance what is theoretically possible with a computer. When
you publish working source code you at least offer some real knowledge
on how to solve the problem, unlike when you say in the claims
language that a %(q:processor means coupled to input output means so
that they compute a function such that the result of said function
when output through said output means solves the problem the user
wanted to solve).

#qrn2: Note that this amendment does not require that the source code for all
programs of the patent owner which use these features be disclosed. He
only has to provide a single, simple text which describes the
monopolised functionality in a programming language.

#rWo2: This amendment roughly corresponds to article 7 paragraph 5 in the
consolidated text of the EP’s first reading (it’s been made more clear
that only an example must be provided).

#a126n: A claim to a computer program, as the major provision of the
invention, cannot apply to a computer programme on its own or on a
carrier. Member States shall ensure that a patent claim for a
computer-implemented invention shall lead to a change in the state of
the art.

#a126k: excludes program claims.  The second sentence is unclear and offtopic.

#sWa: A claim to a computer program product, on its own or on a carrier
shall be allowed only if the invention realised by a computer program
would, when loaded or run on a computer, computer network or other
programmable apparatus, have an antecedent main claim in the same
patent with a product or process claim as described in Article 5.1.

#omi: An inventive product which can be realised by a computer program must
be able to be distributed by carrier.

#a127k: authorises program claims under a limiting condition which is always
true.

#a128n: A patent claim to a computer program, either on its own or on a
carrier, shall not be allowed.

#a128j1: It is contradictory to say that computer programs at the same time
cannot be inventions, and saying that they nevertheless can be claimed
in a patent. Additionally, the condition after the %(q:unless) in the
Council version can always be fulfilled.

#a128j2: The Commission purposefully did not include these so-called
%(q:program claims) in its original proposal, as allowing patent
monopolies on programs on their own is hard to defend if you at the
same time want to maintain that %(q:program as such) are not
patentable.

#a128j3: Getting rid of this Council amendment is one of the most basic
requirements. In first reading, the EP rejected a similar amendment,
and the replacement is part of an amendment which was adopted (article
7 paragraph 2 of the consolidated version).

#a128k: briefly and clearly states that program claims (= claims like
%(q:computer program, characterised by that ...)) are not allowed.

#sexp: Self-explanatory.

#a131j: This is 1st reading article 7.1 ; it specifies that a computer program
cannot be claimed either on its own or on any carrier : that would be
tantamount to allowing software patentability on the basis of
considering that software can possess patentable technical features.
The programme claims are considered as patents on software as such,
therefore these should not be included in this Directive.

#a133n: A claim to a computer program, either on its own or on a carrier,
shall not be allowed unless that program would, when loaded and
executed in a computer, programmed computer network or other
programmable apparatus, put into %(s:effect) a product or process
claimed %(s:for the invention) in accordance with paragraph 1.

#a133j: authorises claims to computer programs on their own, under a condition
which is always true.  The reasoning is self contradictory: the only
way to %(q:make clear that the program on its own cannot be the object
of claims) would be to disallow such claims.  Lehne can't have his
cake and eat it.

#a134n: Member States shall ensure that the production, processing,
dissemination and publication of information in any form cannot be the
basis of direct or indirect breach of patent law, even if patented
technical apparatus is used to this end.

#a134j: Restricting the exchange of information could impede the development
of fundamental research.

#a134k: makes it clear that publication is not an infringment.  The mentioning
of the possibility that a patented apparatus could be used for
dissemination complicates the matter unnecessarily.

#a135n: Member States shall ensure that the production, handling, processing,
distribution and publication of information in any form can never
constitute a patent infringement, either direct or indirect, even if a
technical device is used for this purpose

#a135j: This is 1st reading article 7.3 ; it aims at ensuring freedom of
information.

#a135k: clearly states the precedence of freedom of publication over patent
rights.

#a136n: Where individual elements of software are used in contexts which do
not involve the realisation of any validly claimed product or process,
such use will not constitute patent infringement.

#a136j: Only when software elements are used in the context of realising the
computer-implemented invention the claims raised in accordance with
paragraph 1 extend to the software and infringements may take place.
This should not only be mentioned in Recital 17 but also in Article 5.

#a136k: Lehne apparently wants to make us believe that a program claim extends
to something other than what is claimed.  Given its opaque wording,
this provision can in any case not serve any regulatory purpose.  It
apparently serves only to misinform the legislator about what program
claims do.

#rtW: A claim described in Article 5.2 only gives protection for the use
which is described in the respective patent.

#eeW: Complements the clearer wording of 5.2.

#a137k: Lehne apparently wants to make us believe that a program claim extends
to something other than what is claimed.  Given its opaque wording,
this provision can in any case not serve any regulatory purpose.  It
apparently serves only to misinform the legislator about what program
claims do.

#a138n: Member States shall ensure that using computer programs for purposes
not contained in the scope of a patent claim cannot be taken as direct
or indirect breach of patent law.

#a138k: either tautological or useless limitation, detracts from core issues. 
The misunderstanding seems to be based on a widespread misconception
that patents could be narrowly tied to individual products.  This may
be desirable, but it can not be done within the framework of the
patent system, as laid down in TRIPs, and any attempt to do it would
exceed the scope of the present directive.

#a139n: Member States shall ensure that the use of a computer program for
purposes that do not belong to the scope of the patent cannot
constitute a direct or indirect patent infringement.

#Wsa: Only inventions can be patented.

#a139j: A meaningless provision.  This directive can not regulate how courts
should interpret patent claims, and interpretation of patent claims is
not a problem that needs to be regulated.

#a140n: Member States shall ensure that in the even of a patent claim relating
to technical features which install computer program applications,
full documentation of such program applications shall require that
they be presented as part of the patent description and in the form of
specific examples, not in the form of separate, main or even
additional patent claims.

#a140j: On the one hand, we must ensure that writers of computer programs are
given their author's rights and, on the other, prevent
pseudo-invention by ensuring the disclosure of specific computer
programs in patent submissions.

#a140k: says that informatic elements of an invention belong to the disclosure
side rather than the monopoly side of the patent monopoly.  Wording
not very clear.

#uon: Member States shall ensure that the distribution and publication of
information, in whatever form, can never constitute direct or indirect
infringement of a patent.

#Wsi: Freedom of publication, as stipulated in Art 10 ECHR, can be limited
by copyright but not by patents. Patent rights are broad and unsuited
for information goods. This amendment does not make any patents
invalid, rather it limits the ways in which a patent owner can enforce
his patents. Such a provision should be complemented by other
provisions which make sure that information patents are not granted in
the first place.

#aef2: This amendment is a simplified and reduced version of article 7
paragraph 3 in the consolidated text of the EP’s first reading.

#aWa: Limitation of the effects of a patent

#fis: The rights conferred by patents for inventions within the scope of
this Directive shall not extend to:

#nfl: acts done privately and for non-commercial purposes,

#mfa: acts done for experimental purposes relating to the subject-matter of
the patented invention, including non-commercial academic and research
use.

#WtL: This proposal establishes that certain acts can be carried out safely
with the peace of mind that they do not constitute patent
infringement.  In particular this applies to acts done privately or
for experimental purposes, as long as they are not done commercially. 
Likewise academic and research use is immune if non-commercial.

#ncW: Los Estados miembros garantizarán que la distribución y publicación de
información, cualquiera que sea la forma, nunca constituya una
violación directa o indirecta de una patente.

#cci: Los términos de %(q:distribución) y %(q:publicación) tienen más en
cuenta los casos de demandas de patentes para métodos comerciales (de
hecho el tratamiento de la información) que existen en los Estados
Unidos y que no deberían existir en la Unión Europea.

#Wta: Esta enmienda corresponde a una versión más restrictiva del artículo 7
párrafo 3 en el texto consolidado de la primera lectura del PE con el
objetivo de alcanzar un compromiso con el Consejo.

#a144n: Los Estados miembros garantizarán que siempre que una reclamación de
patentes indique características que impliquen el uso de un programa
de ordenador, un buen funcionamiento y bien documentada referencia de
aplicación de tal programa se publicará como parte de la descripción
sin ninguna restricción a sus condiciones de licencia.

#a144j1: Estas enmiendas, a pesar de lo que se pueda pensar, no sirven para
promover los programas de red abierta sino para asegurar que la
obligación de publicidad, que es inherente al sistema de patentes, se
tome en serio y para que un programa informático esté, como cualquier
otro objeto de información, en el lado revelado de la patente en vez
de en su lado de exclusión /monopolio.

#a144j2: Esto hace un poco más difícil impedir que alguien haga cosas que ni
siquiera ha hecho él mismo, pero que son obviamente posibles desde que
el modelo de computación se definió perfectamente y se sabe siempre lo
que se puede hacer con un ordenador.

#a144j3: Obsérvese que esta enmienda no requiere que el código de fuentes para
todos los programas del titular de la que usa estas características se
revelen. Solamente tiene que proporcionar una única y simple
aplicación de la funcionalidad que está monopolizando.

#a144j4: Esta enmienda corresponde al artículo 7 párrafo 5 del texto
consolidado de la primera lectura del PE.

#eaW: Les Etats membres veillent à ce que, lorsque le recours à une
technique brevetée est nécessaire afin d'assurer l'interopérabilité
entre deux systèmes ou réseaux informatiques différents, cela dans le
cas où il n'existe pas d'alternative technique non brevetée aussi
efficace permettant d'obtenir l'interopérabilité entre les deux
systèmes, ni ce recours, ni le developpement, l'expérimentation, la
fabrication, la vente, la cession de licences, ou l'importation de
programmes mettant en oeuvre cette technique brevetée ne soient
considérés comme une contrefaçon de brevet.

#pt1: La préservation de l'interopérabilité suppose la capacité, non
seulement de pouvoir le cas échéant effectuer des opérations de
rétro-ingéniérie pour déterminer les caractéristiques des protocoles
et interfaces de communication avec lesquelles il s'agira de
communiquer, mais également de pouvoir réaliser et commercialiser
effectivement de tels produits interopérables.

#eut: L'article 6.2, autorisé par l'article 30 de l'accord ADPIC, est
nécessaire pour empêcher de possibles graves distorsions de la
concurrence sur le marché intérieur du fait que la mise sur le marché
de produits interopérables constituerait toujours une contrefaçon des
revendications d'un brevet.

#cWe: Member States shall ensure that, wherever the use of a patented
technique is necessary in order to ensure interoperability between two
different computer systems or networks, in the sense that no equally
efficient and equally effective alternative non-patented means of
achieving such interoperability between them is available, such use is
not considered to be a patent infringement, nor is the development,
testing, making, offering for sale or license, or importation of
programs making such use of a patented technique to be considered a
patent infringement.

#ees: Article 6 of the Council only refers to the exemption provided for by
the copyright directive; this means that a developer is allowed to use
reverse engineering to make his software interoperable with that of a
competitor but afterwards he needs to be able to distribute, sell and
use the interoperable software he developed.

#a147n: Member States shall ensure that whenever it is necessary to use a
patented technology solely to ensure the conversion of standards used
in two different data processing systems in order to ensure
communication and data exchange, such use is not considered breach of
patent.

#ar147k: well-worded version of the interoperability privilege.

#emW: Member States shall ensure that a patented computer-implemented
invention that is essential for enabling interoperability between
programmable devices can be used on reasonable and non-discriminatory
terms and conditions by third parties to enable interoperability
between programmable devices.

#an3: If a voluntary license on reasonable commercial terms and conditions
cannot be obtained within a reasonable period of time, Member States
shall apply Article 31 TRIPS to such a patented invention.

#WtW: It shall not be deemed reasonable if a potential licensee is forced to
license its own technology that is essential for interoperability
without any compensation or to agree to abstain from enforcing his own
rights on such technology.

#cvy: A compulsory license subject to Art. 31 TRIPs ensures access to
interoperability technology, an  incentive to innovate because the
patentee receives an adequate remuneration, an incentive to early
publication of new technology because patenting still makes sense and
an adequate remuneration for the patentee thereby balancing the public
interests above with the patentee's private interest.

#nne: Paragraph 3 ensures that a dominant player cannot force somebody else
to waive his patent rights.

#eoj: Member States shall ensure that, wherever the use of a patented
technique is necessary in order to ensure interoperability between two
different computer systems or networks, in the sense that no equally
efficient and equally effective alternative non-patented means of
achieving such interoperability between them is available, such use is
not considered to be a patent infringement, nor is the development,
testing, making, offering for sale or license, or importation of
programs making such use of a patented technique to be considered a
patent infringement.

#een2: It is necessary to have the interoperability exception in articles.

#hpi: Member States shall ensure that the non-commercial use of an interface
for the sole purpose of ensuring interoperability with an otherwise
non-infringing product, system, network, or service does not
constitute a patent infringement.

#fWr2: Any person requesting a licence for such use on a commercial basis may
require the patent owner to grant a licence to the patented interface
for such use on reasonable and non-discriminatory terms and on
adequate conditions.

#iWI: This article applies without prejudice to the TRIPs agreement.

#iiW: This amendment aims at keeping  interoperability as  one of the
cornerstones of the information and communications technology.
Electronic products need to be able to communicate and interoperate.

#nds: To strike the proper balance between the rights of the patent owner to
enjoy the full benefits of the patent, the third party´s interest to
develop interoperating products, as well as the public interest to
prevent unjustified monopolies on standards.

#WWW3: Member States shall ensure that, where the use of an interface, which
is protected by a patent for a computer-implemented invention, is
indispensable for the sole purpose of ensuring interoperability, such
as to ensure conversion of the conventions used in two different
computer systems or network in order to allow communication and
exchange of data content between them, this use of the interface is
not considered to be a patent infringement.

#tla: Where an interface protected by a patent is necessary to allow
interoperability, the use of the interface cannot be regarded as a
matter of patent infringement.

#see: Member States shall ensure that wherever an interface which is
protected by a patent for a computer-implemented invention is
indispensable for the sole purpose of ensuring interoperability such
as to ensure conversion of the conventions used in two different
computer systems or network in order to allow communication and
exchange of data content between them, the use of the interface is not
considered to be a patent infringement.

#tla2: Where an interface protected by a patent is necessary to allow
interoperability, the use of the interface cannot be regarded as a
matter of patent infringement.

#eWn2: Die Mitgliedstaaten stellen sicher, dass in allen Fällen, in denen der
Einsatz einer patentierten Technik für die Konvertierung der in
mindestens zwei verschiedenen Computersystemen verwendeten
Konventionen unverzichtbar ist, um die Kommunikation und den Austausch
von Dateninhalten zwischen den Computersystemen zu ermöglichen, einem
Lizenzsucher dieser patentierten Technik ein Anspruch auf Einräumung
einer Lizenz zu angemessenen Bedingungen (Zwangslizenz) gegenüber dem
Rechteinhaber zusteht. Die Regelungen des TRIPS-Übereinkommens bleiben
unberührt.

#ttv: Member States shall ensure that wherever the use of a patented
technique is needed for the sole purpose of ensuring interoperability
of two different computer systems or networks so as to allow
communication and exchange of data content between them, such use is
not considered to be a patent infringement. Member States must ensure
that the court may require a patent owner to grant a licence for such
use having regard to the public interest in permitting access to the
patented technique, provided that a licence is not otherwise available
for such use on reasonable and non-discriminatory terms and
conditions.

#tdp: Interoperability must be ensured in articles and it should not be
considered as patent infringement.

#eic: Interoperability exception

#jmy: The developing, testing, making, using, offering for sale or license,
selling, licensing, or importing of a patented computer-implemented
invention shall not require the authorisation of the patent owner, to
the extent that use of the patented computer-implemented invention is
indispensable to achieve the interoperability of the computer program
with one or more other computer programs, in the sense that no equally
efficient and equally effective alternative non-patented means of
achieving such interoperability between them is available.

#yfW: The exceptions set out in this Article  may not be interpreted in such
a way as to allow its application to be used in a manner which
unreasonably prejudices the right holders legitimate interests or
unreasonably conflicts with a normal exploitation of the computer
implemented invention, taking account of the legitimate interests of
third party software developers to achieve interoperability and of
end-users  to have access to interoperable programs systems and
networks and the need to use data on different computer systems.

#tnr2: Ensures compatibility with the software copyright directive and
ensures users have access to interoperable programme systems and
networks.

#WWn3: Member States shall ensure that, wherever the use of a patented
technique is needed for the sole purpose of ensuring conversion of the
conventions used in two different data processing systems so as to
allow communication and exchange of data content between them, such
use is not considered to be a patent infringement.

#Wno: Interoperability of data processing systems (e.g. computers) lies at
the foundation of the information economy and allows for fair
competition by all players large and small.

#pou: Article 6 of the Council only refers to the exemption provided for by
the Copyright directive. This means that a software developer is
allowed to find out how to make his data processing system
interoperable with that of a competitor, but afterwards he cannot
necessarily use his gained knowledge, since that could be covered by
patents.

#tsW: This amendment makes sure that patents also cannot be used to prevent
interoperability. It was passed in an almost identical form by ITRE
and JURI prior to the first reading (%(q:data processing systems) read
%(q:computer systems or networks)). In first reading, a more sweeping
version of this amendment was passed, which appeared as Article 9 in
the consolidated version.

#eWe: The expression %(q:for the sole purpose) reverts to the spirit of the
original ITRE/JURI version of the interoperability exemption (which is
more limited), which was also supported by Luxembourg and several
others in the Council (but didn’t make it).

#WWn4: Member States shall ensure that, wherever the use of a patented
technique is needed for the sole purpose of ensuring conversion of the
conventions used in two different data processing systems so as to
allow communication and exchange of data content between them, such
use is not considered to be a patent infringement.

#uli: Pubblicità

#Wru: I contenuti del contratto di brevetto dovranno essere condivisi
attraverso adeguati strumenti di pubblicità.

#Won: Il riconoscimento della tutela in cambio della condivisione della
conoscenza, principio fondante del %(q:contratto) di brevetto, per
essere realizzato necessita di strumenti adeguati di
pubblicità/condivisione, che nel caso del software oggi non esistono e
non sono previsti nella proposta di Direttiva. Ciò tenderà
inevitabilmente a svantaggiare le PMI del settore e a creare un
ulteriore ampliamento del contenzioso.

#ust: Los Estados miembros garantizarán que, donde quiera que el uso de una
técnica patentada sea necesaria, con el único propósito de asegurar la
conversión de los convenios utilizados en dos sistemas diferentes de
procesamiento de datos a fin de permitir la comunicación y el
intercambio entre ellas del contenido de sus datos, tal uso no sea
considerado una violación de patente.

#sur: La interoperabilidad de los sistemas de procesamiento de datos (como,
por ejemplo, los ordenadores) subyace a la economía de la información
y permite la competencia leal por todos los operadores grandes y
pequeños.

#nte: El artículo 6 del texto del  Consejo solamente hace referencia a la
exención prevista por la directiva de los derechos reservados. Esto
significa que se permite a un creador de programas informáticos
descubrir cómo hacer interoperable su sistema de procesamiento de
datos con el de un competidor, pero no puede más tarde utilizar el
conocimiento adquirido, puesto que éste podría estar cubierto por
patentes.

#ttr: Esta enmienda asegura que las patentes tampoco puedan ser utilizadas
para impedir la interoperabilidad. Esto se  aprobó de una forma casi
idéntica por ITRE y JURI antes de la primera lectura(la segunda parte
subrayada decía

#lWm: La mención %(q:el único propósito) hace referencia al espíritu de la
versión original ITRE/JURI de esta enmienda.

#uno: El  desarrollo,  examen,  creación, utilización, oferta para venta o
licencia  o  importación  de  un  programa  de  ordenador que
incorpore una invención  aplicada a través de ordenador no requerirá
la autorización del dueño de la patente en caso de que:

#noe: la invención implantada en ordenador sea indispensable para conseguir
la interoperabilidad de dicho programa del computador con otro u otros
programas  de  ordenador   y siempre que no esté disponible una
alternativa igualmente  eficiente  e  igualmente efectiva no patentada
de conseguir tal interoperabilidad entre dichos programas;

#Wlr: el  programa  de  ordenador  utilice  la  invención implementada en el
ordenador para conseguir tal interoperabilidad.

#ien: Para lo relativo a este artículo %(q:interoperabilidad) se define como
la capacidad de un programa de ordenador para comunicar e intercambiar
información con otro programa de ordenador y utilizar mutuamente la
información intercambiada, incluyendo la capacidad  de usar, convertir
o intercambiar formatos de ficheros, protocolos, esquemas o
información de interconexión o convenciones, de forma que permita a
este programa de ordenador trabajar con otro programa de ordenador y
con los usuarios en todas las formas en que esta previsto su
funcionamiento.

#Wse: Esta enmienda intenta resolver el problema que puede presentarse
cuando sea necesario el acceso al sistema lógico de interconexión de
una invención aplicada a través de ordenador  para permitir la
interoperabilidad.

#cae: Debe garantizarse el acceso cuando el uso de la invención aplicada a
través de ordenador es indispensable para la interoperabilidad pero
solo si no hay otra alternativa disponible. En caso de interconexión
patentada es necesario conseguir un equilibrio entre el derecho del
dueño de la patente a disfrutar del beneficio completo de la patente y
los intereses de terceros  de utilizar la invención para producir
productos interoperables.

#ioe: La Commissione osserva gli effetti delle invenzioni attuate per mezzo
di elaboratori elettronici sull'innovazione e sulla concorrenza, in
Europa e sul piano internazionale, e sulle imprese europee, compreso
il commercio elettronico.

#ezo: La Commissione osserva gli effetti delle invenzioni attuate per mezzo
di elaboratori elettronici sull'innovazione e sulla concorrenza, in
Europa e sul piano internazionale, e sulle imprese europee, in
particolare sulle Piccole e Medie Imprese, compreso il commercio
elettronico.

#esW: Visto e considerato che l’economia europea si basa, in maniera
particolare, sulla rete di piccole e medie imprese, che fanno della
qualità dei propri prodotti un vantaggio competitivo, e che le stesse
potrebbero subire negativamente l’attuazione della direttiva in
oggetto, appare corretto intervenire per controllare i possibili
effetti sfavorevoli che si avranno nel tessuto economico e produttivo
degli Stati membri.

#tiW: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, on Community businesses, especially small and
medium-sized enterprises, on the open-source community and on
electronic commerce.

#rjW: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, on Community businesses, especially small and
medium-sized enterprises, on the open-source community and on
electronic commerce, in particular from the aspect of employment in
small and medium-sized enterprises.

#lmW: The Commission has to monitor the impact of computer-implemented
inventions not only on the aspect of innovation and competition but
from the aspect of employment, especially in small and medium-sized
enterprises which could be affected negatively, and which take a very
important part in the employment situation of the EU, in connection
with one of the EU's main priorities, the Lisbon Strategy.

#lnW: To assist in the monitoring obligation set forth in Article 7 of this
Directive, a Committee on Technological Innovation in the Small- and
Medium-sized Enterprise Sector, hereinafter referred to as %(q:the
Committee), shall hereby be established.

#Cht: The Committee shall in particular:

#osh: examine the impact of patents for computer-implemented inventions on
small- and medium-sized enterprises and highlight any difficulties;

#Wfr: monitor participation of small- and medium-sized enterprises in the
patent system, with particular regard to patents for
computer-implemented inventions, and  consider and recommend any
legislative or other EU-level initiatives related thereto; and

#fss: facilitate the exchange of information with regard to relevant
developments in the area of patents for computer-implemented
inventions that might affect the interests of small- and medium-sized
enterprises.

#rad: This amendment relates to Article 10 (Monitoring) adopted by the
European Parliament during First Reading.

#pos: Currently, SMEs participate actively in Europe’s CII patents system. 
Indeed, SMEs represent the majority of applicants for CII patents.  To
ensure ongoing and active participation by SMEs -- and to provide
opportunities to enhance their involvement -- this amendment proposes
the creation of a committee focused on SME-related issues, with a
mandate to recommend necessary reforms.

#tlt: The Commission shall conduct a feasibility study looking to the
establishment of a Fund for small and medium-sized enterprises to
provide financial, technical and administrative support to small and
medium-sized enterprises dealing with issues related to the
patentability of computer-implemented inventions.

#Wat: This amendment proposes that the European Commission studies the
possibility of an %(q:SME Fund) to assist SMEs in fully participating
in, and benefiting from, the computer-implemented invention patent
regime.

#oei: The Commission shall report to the European Parliament and the Council
by*...... on:

#ati: 5 years after the date of entry into force of this Directive

#oei2: The Commission shall report to the European Parliament and the Council
by*......  on:

#ati2: 3 years after the date of entry into force of this Directive

#mhy: It is necessary to set a clear deadline for the Commission report, but
also for the first review of the Directive in accordance with article
9. The timeframe of 5 years should be split into two so that the
Commiccion effectively reports to the European Parliament and the
Council by three years and reviews the Directive by five years after
entry into force.

#iou: The Commission shall report to the European Parliament and the Council
by*... on:

#rWt3: The Commission shall report to the European Parliament and the Council
within three years from the date specified in Article 9(1) at the
latest on:

#hal: The foreseen possibility of monitoring and reporting on the effects of
the directive seems to be absolutely ineffective on the software
market. To the three years of monitoring, in fact, two additional
years have to be added for the transposition in the Member states, and
one minimum year necessary for a revision of the act. This timetable
seems to be incompatible with the dynamicity of this market.

#ire: participation by small- and medium-sized enterprises in the patent
system for computer-implemented inventions.  Such report shall include
data, to the extent available, regarding applicants for and recipients
of patents for computer-implemented inventions;

#rad2: This amendment relates to Article 10 (Monitoring) adopted by the
European Parliament during First Reading.

#dWh: Existing statistics demonstrate fairly broad participation in the CII
patents process by SMEs.  However, there is consensus among all
interested parties that additional and more comprehensive statistical
data on CII patents would be welcomed.  The above amendment would
ensure that such data is compiled.

#ire2: participation by small- and medium-sized enterprises  in the patent
system for computer-implemented inventions.  Such report shall include
data, to the extent available, regarding applicants for and recipients
of patents for computer-implemented inventions;

#rad3: This amendment relates to Article 10 (Monitoring) adopted by the
European Parliament during First Reading.

#dWh2: Existing statistics demonstrate fairly broad participation in the CII
patents process by SMEs.  However, there is consensus among all
interested parties that additional and more comprehensive statistical
data on CII patents would be welcomed.  The above amendment would
ensure that such data is compiled.

#trn: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims are adequate,
and whether it would be desirable and legally possible having regard
to the Community's international obligations to make modifications to
such rules;

#nyh: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims are adequate;

#rot: Last part of the Common position text is not necessary.

#rrE: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the European Patent Convention;

#nit: Not necessary in this article.

#eeu: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the European Patent Convention;

#cla: The aim of the present Directive is not to modify the European Patent
Convention neither to legalize patents on software, therefore there is
no need to envisage a modification of this Convention.

#pno: developments in the interpretation of the terms %(q:technical
contribution) and %(q:inventive step) by patent offices and patent
courts in the light of the future evolution of technology.

#oDo: Parliament and the Council should be informed about the practice of
granting patents under this Directive. Special attention should be
given to the interpretation of the most relevant legal definintions.

#uiy: whether the option outlined in the Directive concerning the use of a
patented invention for the sole purpose of ensuring interoperability
between two systems is adequate;

#lpt3: Self explanatory

#ihm: the feasibility study looking to the establishment of a Fund for small
and medium-sized enterprises.

#Wat2: This amendment proposes that the European Commission studies the
possibility of an “SME Fund” to assist SMEs in fully participating
in, and benefiting from, the computer-implemented invention patent
regime.

#eii: Whether difficulties have been experienced arising from the grant of
patents for computer-implemented inventions which do not comply with
the statutory requirements for patentability both in terms of whether
the invention

#lve: involves an inventive step and

#scb: makes a technical contribution

#Wsa2: in accordance with Article 4.1 above, and as such should not have
legitimately been granted.

#tia: This amendment addresses the concerns that have been expressed about
the grant of trivial, or undeserving, patents.  It provides a new
initiative for the Commission to report to the European Parliament and
the Council on whether difficulties have been found in practice caused
by patents that should not have legitimately been granted.   This will
encourage the European Patent Office and national Patent Offices to
maintain the highest standards for examining patent applications, thus
minimising the risk of undeserving patents being granted.

#Wdo: whether this Directive has performed the desired effects in terms of
harmonisation and clarification of the legal rules governing the
patentability of computer-implemented inventions.

#age: To provide an assessment whether the aims leading to the adoption of
this Directive have been achieved.

#WpW2: the developments of the world-wide patent systems in the area of
computer-implemented inventions in terms of the aspects mentioned in
this article (a to d and f to gb).

#alm: The evolution of the patent systems in other major jurisdictions,
especially the possibility to have a world-wide patent system, should
be closely monitored.

#WWt: The Commission shall come forward within a year with a proposal for an
effective European Community Patent there by allowing a democratic
control by the European Parliament on the European Patent Office and
the European Patent Convention.

#csm: With a view to legal certainty and reaching the Lisbon objectives it
is desirable that there is one single patent system across the
European Union.

#eee2: Member States shall ensure that its representatives in the
Administrative Council of the European Patent Organisation take such
measures within their authority to ensure that the European Patent
Office only grants European patents when the requirements of the
European Patent Convention have been met, in particular with respect
to  inventive step and technical contribution as defined in Article
2(b).

#Eae: The Council shall provide a yearly report to the European Parliament
on the activities of representatives of Member States that are
Contracting States to the European Patent Convention in the
Administrative Council of the European Patent Organisation, and the
progress that has been made to achieving the objectives set out in
Article 8A.1 above.

#onn: This amendment recognises that the Member States are also Contracting
States of the European Patent Convention and that Member States have
some influence the practice of the European Patent Office,
specifically with respect to maintaining high standards of examining
patent applications in particular with respect to inventive step and
%(q:technical contribution) as defined in this directive.

#ble: Furthermore, this amendment requires Member States (in Council) to
report to the European Parliament each year on what they have actually
done to influence the EPO in this regard and on the progress that has
been made towards the goal of minimising the grant of undeserving
patents.

#Wir3: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
review the impact of this Directive and, where necessary, submit
amending proposals to the European Parliament and the Council.

#rma: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
review the impact of this Directive at latest 2 years after having
submitted the report, and, where necessary, submit amending proposals
to the European Parliament and the Council.

#lpt4: Self-explanatory.

#oet: The realisation of the internal market implies the elimination of
restrictions to free circulation and of distortions in competition,
while creating an environment which is favourable to innovation and
investment. In this context the protection of inventions by means of
patents is an essential element for the success of the internal
market. Effective, transparent and harmonised protection of
computer-implemented inventions throughout the Member States is
essential in order to maintain and encourage investment in this field.

#iWi: The realisation of the internal market implies the elimination of
restrictions to free circulation and of unjustified distortions in
competition, while creating an environment which is favourable to
innovation and investment. In this context the protection of
inventions by means of patents is one of the elements contributing to
the success of the internal market. Appropriate, effective,
transparent and harmonised protection of computer-assisted inventions
throughout the Member States is essential in order to maintain and
encourage investment in all technical fields involving the use of
information technology.

#Wit: Distortions in competition are harmful only when they are unjustified.
States may, within their competencies, make use of these, which is
something that the Directive cannot prejudge.

#cno: The Directive covers the patentability of technical inventions
assisted by information technology.

#WwW: Differences exist in the protection of computer-implemented inventions
offered by the administrative practices and the case law of the
different Member States. Such differences could create barriers to
trade and hence impede the proper functioning of the internal market.

#Wwi: Differences exist in the protection of computer-assisted inventions
resulting from the administrative practices and the case law of the
different Member States. Such differences could create barriers to
trade and hence impede the proper functioning of the internal market.

#uaa: See justification amendment on article 1.

#ngr: Therefore, the legal rules governing the patentability of
computer-implemented inventions should be harmonised so as to ensure
that the resulting legal certainty and the level of requirements
demanded for patentability enable innovative enterprises to derive the
maximum advantage from their inventive process and provide an
incentive for investment and innovation. Legal certainty will also be
secured by the fact that, in case of doubt as to the interpretation of
this Directive, national courts may, and national courts of last
instance must, seek a ruling from the Court of Justice.

#oWe2: Therefore, the legal rules governing the patentability of
computer-assisted inventions should be harmonised so as to ensure that
the resulting legal certainty and the level of requirements demanded
for patentability enable innovative enterprises to derive the maximum
advantage from their inventive process and provide an incentive for
investment and innovation. Legal certainty will also be secured by the
fact that, in case of doubt as to the interpretation of this
Directive, national courts may, and national courts of last instance
must, seek a ruling from the Court of Justice.

#jtc: See justification article 1.

#oWi: The rules of the Convention on the Grant of European Patents signed in
Munich on 5 October 1973, and in particular Article 52 thereof
concerning the limits to patentability, should be confirmed and
clarified.

#fWe: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to TRIPS, patent rights should be available and
patent rights enjoyable without discrimination as to the field of
technology. These principles should accordingly apply to
computer-implemented inventions.

#gee: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994).

#lpt5: Self-explanatory.

#sWa2: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to that Article, patent rights should be available
and patent rights enjoyable without discrimination as to the field of
technology. These principles should accordingly apply to
computer-implemented inventions.

#fst4: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to that Article, patent rights should be available
and patent rights enjoyable without discrimination as to the field of
technology. This means that patentability must be effectively limited
in terms of general concepts such as

#WWs: It must be made clear that there are limits as to what can be subsumed
under %(q:fields of technology) according to Art 27 TRIPS and that
this article is not designed to mandate unlimited patentability but
rather to avoid frictions in free trade, which can be caused by undue
exceptions as well as by undue extensions to patentability. This
interpretation of TRIPS is indirectly confirmed by lobbying of the US
government last year against Art 27 TRIPS, on the account that it
excludes business method patents, which the US government wants to
mandate by the new Substantive Patent Law Treaty draft.

#dnt: In its first reading, Parliament deleted this recital, and therefore
the amendment that proposed the above change was not voted upon.
Deletion is better than keeping the original, but clarification
regarding the applicability and interpretation of the TRIPs agreement
is better.

#sWa3: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to that Article, patent rights should be available
and patent rights enjoyable without discrimination as to the field of
technology. These principles should accordingly apply to
computer-implemented inventions.

#fst5: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS
provides that patents shall be available for any inventions, whether
products or processes, in all fields of technology, provided that they
are new, involve an inventive step and are capable of industrial
application. Moreover, according to that Article, patent rights should
be available and patent rights enjoyable without discrimination as to
the field of technology. This means that patentability must be
effectively limited in terms of general concepts such as

#CWc: La Comunidad y sus Estados miembros están obligados por el Acuerdo
sobre los Aspectos de los Derechos de Propiedad Intelectual
relacionados con el Comercio (ADPIC), aprobado mediante la Decisión
94/800/CE del Consejo, de 22 de diciembre de 1994, relativa a la
celebración en nombre de la Comunidad Europea, por lo que respecta a
los temas de su competencia, de los acuerdos resultantes de las
negociaciones multilaterales de la Ronda Uruguay (1986-1994) 1. El
apartado 1 del artículo 27 del acuerdo ADPIC establece que las
patentes podrán obtenerse por todas las invenciones, sean de productos
o de procedimientos, en todos los campos de la tecnología, siempre que
sean nuevas, entrañen una actividad inventiva y sean susceptibles de
aplicación industrial. Además, según este mismo artículo, las patentes
se podrán obtener y los derechos de patente se podrán ejercer sin
discriminación por el campo de la tecnología. Esto significa que esa
patentabilidad debe limitarse efectivamente a términos de conceptos
generales tales como

#sei: Debe dejarse claro que hay límites en cuanto a lo que puede incluirse
bajo

#Trb: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994) 1. Article 27(1) of TRIPS
provides that patents shall be available for any inventions, whether
products or processes, in all fields of technology, provided that they
are new, involve an inventive step and are capable of industrial
application. Moreover, according to that Article, patent rights should
be available and patent rights enjoyable without discrimination as to
the field of technology. These principles should accordingly apply to
computer-implemented inventions.

#rWf: The Community and its Member States are bound by the Agreement on
trade-related aspects of intellectual property rights (TRIPS),
approved by Council Decision 94/800/EC of 22 December 1994 concerning
the conclusion on behalf of the European Community, as regards matters
within its competence, of the agreements reached in the Uruguay Round
multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides
that patents shall be available for any inventions, whether products
or processes, in all fields of technology, provided that they are new,
involve an inventive step and are capable of industrial application.
Moreover, according to that Article, patent rights should be available
and patent rights enjoyable without discrimination as to the field of
technology. These principles should accordingly apply to
computer-assisted inventions. Nevertheless, the field of software is
not considered to be a field of technology.

#fnW: Patents are intended for inventions of a material nature. Copyright is
intended for works of a conceptual nature, and this includes software.
Software, like books, music or intellectual methods, is not therefore
a field of technology within the meaning of TRIPS.

#WeW2: Con arreglo al Convenio sobre la concesión de patentes europeas,
firmado en Munich el 5 de octubre de 1973, (%(q:Convenio sobre la
Patente Europea)), y las legislaciones sobre patentes de los Estados
miembros, no se consideran invenciones, y quedan por tanto excluidos
de la patentabilidad, los programas de ordenadores, así como los
descubrimientos, las teorías científicas, los métodos matemáticos, las
creaciones estéticas, los planes, principios y métodos para el
ejercicio de actividades intelectuales, para juegos o para actividades
económicas, y las formas de presentar informaciones. No obstante, esta
excepción se aplica y se justifica únicamente en la medida en que la
solicitud de patente o la patente se refiera a uno de esos elementos o
actividades considerados como tales, porque dichos elementos y
actividades como tales no pertenecen al campo de la tecnología.

#CtW: Con arreglo al Convenio sobre la concesión de patentes europeas,
firmado en Munich el 5 de octubre de 1973, (%(q:Convenio sobre la
Patente Europea)), y las legislaciones sobre patentes de los Estados
miembros, no se consideran invenciones, y quedan por tanto excluidos
de la patentabilidad, los programas de ordenadores, así como los
descubrimientos, las teorías científicas, los métodos matemáticos, las
creaciones estéticas, los planes, principios y métodos para el
ejercicio de actividades intelectuales, para juegos o para actividades
económicas, y las formas de presentar informaciones. Esta excepción se
aplica porque dichos elementos y actividades no pertenecen al campo de
la tecnología.

#Wlr2: El artículo 52 EPC dice que los programas de ordenadores no son
invenciones en el sentido de la legislación sobre patentes, es decir,
que un sistema que consiste en un equipo informático genérico de 
ordenador y una cierta combinación de normas de cálculo que funcionan
en él no son patentables. No dice que tales sistemas puedan patentarse
declarándolos para ser %(q:no como tal) o %(q:técnico). La exclusión
de programas para los ordenadores no es una excepción, es parte de la
regla para definir qué es una %(q:invención).

#egW: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 (European Patent Convention) and the patent laws of
the Member States, programs for computers together with discoveries,
scientific theories, mathematical methods, aesthetic creations,
schemes, rules and methods for performing mental acts, playing games
or doing business, and presentations of information are expressly not
regarded as inventions and are therefore excluded from patentability.
This exception, however, applies and is justified only to the extent
that a patent application or patent relates to the above
subject-matter or activities as such, because the said subject-matter
and activities as such do not belong to a field of technology.

#cth: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 and the patent laws of the Member States, programs
for computers together with discoveries, scientific theories,
mathematical methods, aesthetic creations, schemes, rules and methods
for performing mental acts, playing games or doing business, and
presentations of information are expressly not regarded as inventions
and are therefore excluded from patentability. This exception applies
because the said subject-matter and activities do not belong to a
field of technology.

#teW2: Art 52 EPC says that programs for computers etc are not inventions in
the sense of patent law, i.e. that a system consisting of generic
computing hardware and some combination of calculation rules operating
on it can not form the object of a patent. It does not say that such
systems can be patented by declaring them to be

#ttP3: This amendment corresponds to recital 7 in the consolidated text of
the EP’s first reading.

#egW2: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 (European Patent Convention) and the patent laws of
the Member States, programs for computers together with discoveries,
scientific theories, mathematical methods, aesthetic creations,
schemes, rules and methods for performing mental acts, playing games
or doing business, and presentations of information are expressly not
regarded as inventions and are therefore excluded from patentability.
This exception, however, applies and is justified only to the extent
that a patent application or patent relates to the above
subject-matter or activities as such, because the said subject-matter
and activities as such do not belong to a field of technology.

#cth2: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 and the patent laws of the Member States, programs
for computers together with discoveries, scientific theories,
mathematical methods, aesthetic creations, schemes, rules and methods
for performing mental acts, playing games or doing business, and
presentations of information are expressly not regarded as inventions
and are therefore excluded from patentability. This exception applies
because the said subject-matter and activities do not belong to a
field of technology.

#eWr: Art 52 EPC says that programs for computers etc are not inventions in
the sense of patent law, i.e. that a system consisting of generic
computing hardware and some combination of calculation rules operating
on it can not form the object of a patent. It does not say that such
systems can be patented by declaring them to be %(q:not as such) or
%(q:technical). This amendment reconfirms Art 52 EPC. Note that the
exclusion of programs for computers is not an exception, it is part of
the rule for defining what an %(q:invention) is.

#ttP4: This amendment corresponds to recital 7 in the consolidated text of
the EP’s first reading.

#nle2: The aim of this Directive is to prevent different interpretations of
the provisions of the European Patent Convention concerning the limits
to patentability. The consequent legal certainty should help to foster
a climate conducive to investment and innovation in the field of
software.

#iao: The aim of this Directive is to prevent different interpretations of
the provisions of the European Patent Convention concerning the limits
to patentability. The consequent legal certainty should help to foster
a climate conducive to investment and innovation in fields of
technology as well as in the field of software.

#tWo4: The aim is not to legislate on the patentability of software, but on
that of computer-controlled inventions.

#irW: a useful clarification which recapitulates the important distinction
between technology and data processing.

#Wtr: Member states shall respect the provisions of this directive when
acting in the framework of the European Patent Convention.

#Set: This amendment recognises that the Member States are also Contracting
States of the European Patent Convention and that Member States have
some influence on the practice of the European Patent Office,
specifically with respect to ensuring that the European Patent Office
complies with this directive.

#vir: The European Patent Convention provides that the European Patent
Office is supervised by the Administrative Council of the European
Patent Organisation, and that the President of the European Patent
Office is responsible for its activities to the Administrative
Council. The Administrative Council is composed of representatives of
the Contracting States of the European Patent Convention, a clear
majority of which is formed by Member States.  These representatives
shall exercise such measures within their authority to achieve
compliance by the European Patent Office with this directive.

#sto: This amendment recognises that the Member States are also Contracting
States of the European Patent Convention and that Member States have
some influence the practice of the European Patent Office,
specifically with respect to maintaining high standards of examining
patent applications in particular with respect to inventive step and
%(q:technical contribution) as defined in this directive.

#ble2: Furthermore, this amendment requires Member States (in Council) to
report to the European Parliament each year on what they have actually
done to influence the EPO in this regard and on the progress that has
been made towards the goal of minimising the grant of undeserving
patents.

#sed: La protección que otorgan las patentes permite a los innovadores
beneficiarse de su creatividad. Los derechos de patente protegen la
innovación en interés de toda la sociedad y no deberían utilizarse de
forma anticompetitiva.

#taW: Las patentes son derechos temporales de exclusión concedidos por el
Estado a los inventores para estimular progresos técnicos. Para
asegurarse de que los trabajos del sistema pretendidos, las
condiciones para conceder patentes y las modalidades para hacerlas
cumplir deben ser delimitadas cuidadosamente, y, en especial, los
corolarios inevitables del sistema de patentes tales como la
restricción de la libertad creativa, legal, la inseguridad y los
efectos contra la competencia deben ser mantenidos en límites
razonables.

#ees2: Los creadores de innovaciones pueden beneficiarse de su creatividad
sin patentes. Si los derechos de patentes

#nWs: Patent protection allows innovators to benefit from their creativity.
Whereas patent rights protect innovation in the interests of society
as a whole; they should not be used in a manner which is
anti-competitive.

#nsg: Patents are temporary exclusion rights granted by the state to
inventors in order to stimulate technical progress. In order to ensure
that the system works as intended, the conditions for granting patents
and the modalities for enforcing them must be carefully designed. In
particular, inevitable corollaries of the patent system such as
restriction of creative freedom, users´rights or legal insecurity and
anti-competitive effects must be kept within reasonable limits.

#cie: Innovators can benefit from their creativity without patents. Whether
patent rights %(q:protect) or stifle innovation and whether they act
in the interests of society as a whole is a question that can only be
answered by empirical study, not by statements in legislation.

#nWs2: Patent protection allows innovators to benefit from their creativity.
Whereas patent rights protect innovation in the interests of society
as a whole; they should not be used in a manner which is
anti-competitive.

#nsg2: Patents are temporary exclusion rights granted by the state to
inventors in order to stimulate technical progress. In order to ensure
that the system works as intended, the conditions for granting patents
and the modalities for enforcing them must be carefully designed. In
particular, inevitable corollaries of the patent system such as
restriction of creative freedom, users´ rights or legal insecurity and
anti-competitive effects must be kept within reasonable limits.

#cie2: Innovators can benefit from their creativity without patents. Whether
patent rights

#ind: Patent protection allows innovators to benefit from their creativity.
Patent rights protect innovation in the interests of society as a
whole and should not be used in a manner which is anti-competitive.

#rti: Patents are temporary exclusion rights granted by the state to
inventors in order to benefit from their creativity and to stimulate
technical progress. In order to ensure that the patent rights protect
innovation in the interest of society as a whole and the system works
as intended, the conditions for granting patents and the modalities
for enforcing them must be carefully designed. In particular,
inevitable corollaries of the patent system such as restriction of
creative freedom, users' rights or legal insecurity and
anti-competitive effects must be kept within reasonable limits.

#ott: It is important to specify the temporary nature of patents and the
system needed for enforcing them in a proper manner.

#Wne: Patent protection allows innovators to benefit from their creativity.
Patent rights protect innovation in the interests of society as a
whole and should not be used in a manner which is anti-competitive.

#soe: Patent protection allows inventors to benefit from their creativity.
Patent rights protect innovation in the interests of society as a
whole and should not be used in a manner which is anti-competitive or
excessively detrimental to the innovation derived there from.

#lpt6: Self explanatory.

#Wgl: De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo
de 1991, sobre la protección jurídica de los programas de ordenador,
cualquier forma de expresión de un programa de ordenador original
estará protegida por los derechos de autor como obra literaria. No
obstante, las ideas y principios en los que se basa cualquiera de los
elementos de un programa de ordenador no están protegidos por los
derechos de autor.

#als2: De conformidad con la Directiva 91/250/CEE del Consejo, de 14 de mayo
de 1991, sobre la protección jurídica de los programas de ordenador,
la propiedad en los programas de ordenador se adquiere mediante
derechos reservados. Las ideas y los  principios generales sobre las
que se basa un programa de ordenador deben ser utilizables libremente,
de modo que los diferentes creadores puedan obtener simultáneamente la
propiedad en creaciones individuales basadas en ellas.

#spt: Los derechos reservados no sólo se aplican a los trabajos literarios,
sino también a los libros de texto, manuales de operación, programas
de ordenador y todas las clases de estructuras de información. Los
derechos reservados son el sistema de %(q:propiedad intelectual) para
programas de ordenador y no son solamente un sistema para un aspecto
lateral o %(q:literario) de los programas de ordenador. Si los
derechos reservados no cubren la %(q:idea subyacente) de un libro o de
un programa, no se trata de una indicación de una insuficiencia de
derechos reservados sino de la necesidad de mantener libres las
%(q:ideas subyacentes) o conceptos generales, de modo que diversos
creadores tengan ocasión de obtener la propiedad mediante trabajos
individuales basados en éstos conceptos generales.

#0cn: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright.

#CnW: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, property in computer programs
is acquired by copyright. General ideas and principles which underlie
a computer program must stay freely usable, so that many different
creators may simultaneously obtain property in individual creations
based thereon.

#aac: Copyright does not only apply to literary works, but also to
textbooks, operation manuals, computer programs and all kinds of
information structures. Copyright is the system of %(q:intellectual
property) for computer programs, not only a system for a %(q:literary)
side aspect of computer programs.

#oov: If copyright does not cover the %(q:underlying idea) of a book or a
program then that is not an indication of an insufficiency of
copyright but rather an indication of the need to keep %(q:underlying
ideas) (general concepts) free, so that many different creators have a
chance to obtain property in individual works based on these general
concepts.

#0cn2: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright.

#CnW2: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, property in computer programs
is acquired by copyright. General ideas and principles which underlie
a computer program must stay freely usable, so that many different
creators may simultaneously obtain property in individual creations
based thereon.

#5cy: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright.

#Was: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the
legal protection of computer programs, the expression in any form of
an original computer program is protected by copyright as a literary
work. However, ideas and principles which underlie any element of a
computer program are not protected by copyright, because they are
algorithms which are comparable to mathematical methods or methods of
presenting information.

#iaa: Rules for designing programs cannot be patentable as they are
comparable to mathematical proofs.

#a205n: A technical contribution is present if technical considerations
contribute to the solution of a technical problem. A technical
contribution is not present if the subject matter claimed in the
patent solely consists of discoveries, scientific theories,
mathematical methods, aesthetic creations, schemes, rules and methods
for performing mental acts, playing games or doing business, programs
for computers, or  presentations of information, without limitation to
new, non-obvious and technical subject matter that can be made or used
in any kind of industry.

#a205j: Clarification of %(q:technical contribution). Whereas the positive
definition of technical contribution is rather difficult and is
necessarily open to interpretation, it is nevertheless important to
make clear which interpretations of this term as not envisaged in the
framework of this Directive.

#a205k: even less helpful than the Council version: long sentence with
syntactic ambiguities, refers to new undefined concepts, fails to
answer question of how contribution relates to claim object as a whole
and which conditions of patentability it must meet.

#apc: In order for any innovation to be considered a patentable invention it
should have a technical character, and thus belong to a field of
technology. In order to be patentable, inventions in general and
inventions which can be realized by a computer program in particular
must be susceptible of industrial application, new and involve an
inventive step.

#ies2: The references to industrial application and inventiveness are
necessary for the technical aspect to be sufficiently highlighted.

#a207n: In order for any invention to be considered as patentable it should
have a technical character, that is, it should apply to material
systems such as structures and materials, as well as materials,
substances and energy, and their manufacture and processing.

#a207j: It is essential to define %(q:technical character), as this concept is
central to all the other technical or technological terms. The concept
of %(q:technology) is narrower than that of %(q:technics) and is
subsumed in the latter. Patentability under current patent law is
concerned with material systems and the processes falling under the
framework of these systems. Referring to a %(q:field of technology) is
obsolete and imprecise.

#a207k: Good intentions but poor means:%(ol|Uses the term %(q:invention) for
unpatentable subject matter.|Reference to %(q:field of technology)
helps concretise TRIPs.|%(q:Structures) are immaterial and therefore
not technical.  %(q:forces of nature) terminology is more exact as
based in physics as well as (old and new) caselaw|Even pure
mathematics such as that of linear optimisation is designed for
application to material objects.  Inventions can not be distinguished
from non-inventions by the domain of application.)

#vun: In order for any invention to be considered as patentable it should
have a technical character, and thus belong to a field of technology.

#Wla2: In order for any invention to be considered as patentable it should
have a technical character, and thus belong to a field of technology.
It must also be capable of industrial application, be new, and involve
an inventive step.

#dfa: This amendment is a reminder of the conditions of patentability.

#yhn: In order for any invention to be considered as patentable it should
have a technical character, and thus belong to a field of technology.

#Wne2: In order for any innovation to be considered a patentable invention it
should have a technical character, and thus belong to a field of
technology.

#Wts: The Council text is not in line with Art 52 EPC. Art 52(2) EPC lists
examples of non-inventions. It is not permissible to subsume these
under

#yhn2: In order for any invention to be considered as patentable it should
have a technical character, and thus belong to a field of technology.

#Wne3: In order for any innovation to be considered a patentable invention it
should have a technical character, and thus belong to a field of
technology.

#Wts2: The Council text is not in line with Art 52 EPC. Art 52(2) EPC lists
examples of non-inventions. It is not permissible to subsume these
under %(q:inventions) and then test their technical character.
Moreover, while it can not be inferred from Art 52 EPC that all
technical innovations are inventions, it can, based on a unanimous
tradition of patent law, be assumed that all inventions have technical
character.

#nrW: Para que una invención se considere patentable, deberá tener carácter
técnico y pertenecer, por tanto, a un campo de la tecnología.

#ieW: Para que una innovación se considere patentable, deberá tener carácter
técnico y pertenecer, por tanto, a un campo de la tecnología.

#ltc: El texto del Consejo no coincide con el artículo 52 CEP. El artículo
52 (2) CEP enumera ejemplos de no invenciones. No procede incluir
éstos bajo el concepto de %(q:invenciones) para probar así su carácter
técnico. Por otra parte, mientras que no puede deducirse del artículo
52 CEP que cualquier innovación técnica sea invención, sí se puede
asumir, basándonos en una tradición unánime del derecho de patentes,
que todas las invenciones tienen carácter técnico.

#pnt: Esta enmienda fue incorporada de su propia cosecha por el Consejo.
Intenta ahondar aún más en el concepto de la OEP %(q:contribución
técnica del escalón de invención).

#a213n: All inventions must meet the requirement of making a technical
contribution to the state of the art. The technical contribution must
be new and not obvious to specialists in the given technical field. If
it makes no technical contribution, the solution is not patentable,
because there is no inventive step.

#a231j: All inventions must meet the requirement of making a technical
contribution to the state of the art. The technical contribution must
be new and not obvious to specialists in the given technical field. If
it makes no technical contribution, the solution is not patentable,
because there is no inventive step.

#a231k: The Council mingles the test of %(q:contribution) (patentable subject
matter) with that of %(q:inventive step) (non-obviousness).  The
amendment fails to correct the error.

#WWt3: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of art.

#aim: In order to be patentable, inventions in general and inventions which
can be realised by a computer program (computer implemented
inventions) in particular must be susceptible of industrial
application, new and involve an inventive step. In order to involve an
inventive step, computer implemented inventions must in addition make
a new technical contribution to the state of the art.

#roe: Clarification of the text

#Wai: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of the art.

#noo2: It is a condition for inventions in general that they must make a
technical contribution to the state of the art. The technical
contribution must be new and not obvious to the person skilled in the
art. If there is no technical contribution, there is no patentable
subject matter and no invention.

#ivt: This amendments was newly inserted by the Council. It attempts to
further codify the EPO’s %(q:technical contribution in the inventive
step) doctrine. What one invents is his contribution to the state of
the art, and for this contribution to be patentable it has to (among
other things) involve an inventive step. Not the other way round.

#tWW: The justification for the replacement text is the same as the one for
article 2 (b) (amendment 4)

#iib: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of the art.

#sWW: It is a condition for inventions in general that, in order to involve
an inventive step, they should show a significant difference between
the overall technical characteristics in the patent claim and the
state of the art.

#fWo: This definition of an inventive step is tautological, as any technical
contribution already involves an inventive step.

#Wai2: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of the art.

#noo3: It is a condition for inventions in general that they must make a
technical contribution to the state of the art. The technical
contribution must be new and not obvious to the person skilled in the
art. If there is no technical contribution, there is no patentable
subject matter and no invention.

#ivt2: This amendments was newly inserted by the Council. It attempts to
further codify the EPO’s %(q:technical contribution in the inventive
step) doctrine. What one invents is his contribution to the state of
the art, and for this contribution to be patentable it has to (among
other things) involve an inventive step. Not the other way round.

#it2: The justification for the replacement text is the same as the one for
article 2 (b) (amendment 4).

#fnb: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a technical contribution to the
state of the art.

#fWr: It is a condition for inventions in general that, in order to involve
an inventive step, they should make a %(s:new) technical contribution
to the state of the art.

#eee3: Self-evident

#a219n: If a computer-implemented invention is not technical in nature, then
it does not satisfy the criterion of being an inventive step and
therefore shall not be patentable.

#a219j: It is necessary to make clear the cohesion between recital 13 and
recitals 11 and 12.

#a219k: unsuccessful attempt to dissociate the subject matter test from the
inventive step test, introduces an unusual and unexplained new usage
of the term %(q:inventive step).

#tet: Accordingly, although a computer implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#itn: Accordingly, an innovation that does not make a technical contribution
to the state of the art is not an invention in the sense of the patent
law.

#ahs: Clarification of the text. The Council proposal is misleading.

#iWt2: Accordingly, although a computer-implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of the art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#aWi: Accordingly, an innovation that does not make a technical contribution
to the state of the art is not an invention within the meaning of
patent law.

#mWl: When judging the patentability of an invention, patent offices have
always made a clear distinction between criteria of technical
expertise and inventive step. Absence of technical expertise will mean
that the patent will not be granted, regardless of any inventive step
criterion. If this were not the case, any purely new invention would
pass the test of inventiveness, which could lead to a considerable
reduction in the quality of patents granted. The amendment is a
reminder of the method used to assess technical expertise.

#ele: En consecuencia, si bien una invención implementada en ordenador
pertenece a un campo de la tecnología, si no aporta una contribución
técnica al estado de la técnica, como sería el caso, por ejemplo, si
su contribución específica careciera de carácter técnico, la invención
no implicará actividad inventiva y no podrá ser patentable.

#nsl: En consecuencia, una innovación que no aporte una contribución técnica
al estado de la técnica, no se considerará una invención en el sentido
del derecho de patentes.

#eee4: El texto del Consejo define los requisitos de los programas de
ordenador para ser invenciones técnicas. Retira el requisito
independiente de la invención (%(q:contribución técnica)) y lo fusiona
en el carácter no obvio de %(q:paso inventivo). Esto lleva a
incoherencias  y consecuencias prácticas indeseables.

#tWW2: Accordingly, although a computer-implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of the art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#von: Accordingly, an innovation that does not make a technical contribution
to the state of the art is not an invention in the sense of the patent
law.

#lpt7: Self-explanatory.

#nWi: Accordingly, although a computer-implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of the art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#iih: Accordingly, an innovation that does not make a technical contribution
to the state of the art is not an invention within the meaning of
patent law.

#dWq: The Council text declares computer programs to be technical
inventions. It removes the independent requirement of invention
(%(q:technical contribution)) and merges it into the requirement of
non-obviousness (%(q:inventive step)).  This leads to theoretical
inconsistency and undesirable practical consequences, as explained in
detail in the justification of the amendment to article 4.

#tWE: This amendment corresponds to recital 14 in the consolidated text of
the EP’s first reading.

#nWi2: Accordingly, although a computer-implemented invention belongs to a
field of technology, where it does not make a technical contribution
to the state of the art, as would be the case, for example, where its
specific contribution lacks a technical character, it will lack an
inventive step and thus will not be patentable.

#iih2: Accordingly, an innovation that does not make a technical contribution
to the state of the art is not an invention within the meaning of
patent law.

#lne2: The mere implementation of an otherwise unpatentable method on an
apparatus such as a computer is not in itself sufficient to warrant a
finding that a technical contribution is present. Accordingly, a
computer-implemented business method, data processing method or other
method, in which the only contribution to the state of the art is
non-technical, cannot constitute a patentable invention.

#eti: Accordingly, whilst computer-controlled inventions belong to a
technical field, because their technical contribution lies outside the
software that controls them, implementation on an apparatus such as a
computer of an otherwise unpatentable method, such as a business
method, data-processing method or any other method, in which the
contribution to the state of the art is not technical in nature,
cannot under any circumstances be considered a technical contribution.
Accordingly, such an implementation cannot under any circumstances
constitute a patentable invention.

#WWg: The initial wording is open to misunderstanding, as it implies that
there could be a contribution to the state of the art that is not
technical. It is important to distinguish what is technical from what
is not.

#a227n: The mere application of an otherwise unpatentable method on an
apparatus such as a computer is not in itself sufficient to warrant a
finding that a technical contribution is present. Accordingly, a
computer-implemented business method, data processing method or other
method of a non-technical nature cannot constitute a patentable
invention.

#a227j: This statement is cohesive in relation to those included in recitals
11,12 and 13 and is a specific repetition.

#a227k: The Council implied that a business method or other non-invention
could %(q:make a technical contribution) and thereby become
patentable.  The amendment removes this error.

#neW: Data processing in the sense of the directive does not cover the
identification of physical effects and their conversion into data.

#asW: The method of data processing does not cover the interfaces referred
to in the Recital which belong to a field of technology.

#a228k1: Physical phenomena and data are not mutually convertible, and
%(q:identification) is a mental activity.

#a228k2: Data processing does indeed not cover the physical phenomena that are
represented by data and, since data processing has become an important
antonym to %(q:technology), it would be appropriate to say in the
directive what it covers and what not.  However the approach chosen
here is unclear and inadequate.

#cre15: If the contribution to the state of the art relates solely to
unpatentable matter, there can be no patentable invention irrespective
of how the matter is presented in the claims. For example, the
requirement for technical contribution cannot be circumvented merely
by specifying technical means in the patent claims.

#a229n: If the contribution to the state of the art relates solely to
unpatentable matter, there can be no patentable invention irrespective
of how the matter is presented in the claims. For example, the
requirement for technical contribution cannot be circumvented merely
by specifying technical means in the patent claim.

#a229j: A contribution to the state of the art must by definition be technical
in nature.

#a229k: The recital is a meaningless apologetic text which tries to make
existing tautological limitations on patentability appear significant.
 The amended text seems indistinguishable from the original. 
Something must have gone wrong during submission of this amendment.

#qme: Además, un algoritmo es esencialmente no técnico, por lo que no puede
constituir una invención técnica. No obstante, un método que comprenda
el uso de un algoritmo puede ser patentable siempre que dicho método
se utilice para resolver un problema técnico. Sin embargo, una patente
concedida por un método de estas características no debe permitir que
se monopolice el propio algoritmo o su uso en contextos no previstos
en la patente.

#oWW: La naturaleza del problema resuelto  debería ser irrelevante para la
patentabilidad. Lo que cuenta es la naturaleza de la solución. No se
inventan los problemas sino las soluciones. Y la invención debe ser
técnica o bien tener carácter técnico.

#nWs3: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention. Nonetheless, a method
involving the use of an algorithm might be patentable provided that
the method is used to solve a technical problem. However, any patent
granted for such a method should not monopolise the algorithm itself
or its use in contexts not foreseen in the patent.

#cin: Thus, an algorithm or computer program, which are inherently
non-technical, can never be regarded as inventions. A
computer-controlled technical procedure might be patentable to the
extent that this process has characteristics which make it a technical
contribution. However, any patent granted for such a process may not
establish a monopoly on the algorithm or the program itself, as
programs as such cannot be patentable, as stated in particular in
Article 52(2)(c) of the European Patent Convention.

#jIs: The original wording is incomplete, as it does not state that the
method in question must be a technical process. It should not be
concluded from this that non-technical methods, such as business or
mathematical methods, could be patentable.

#abl: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention. Nonetheless, a method
involving the use of an algorithm might be patentable provided that
the method is used to solve a technical problem. However, any patent
granted for such a method should not monopolise the algorithm itself
or its use in contexts not foreseen in the patent.

#jnW: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention.

#htu: The nature of the problem solved should be irrelevant to
patentability. It’s the nature of the solution that counts. Problems
are not invented, but solutions are, and it’s the invention that must
be technical (or have technical character).

#a233n: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention.

#a233j: Computer programs have their own algorithms indicating the sequence of
individual steps in the presentation of information, that is, they are
part of the theory and, as such, without appropriate reference to the
material system, but not the computer, they cannot be submitted for
patent. The nature of the statement included in recital 16 is to
stress the intention that only new theoretical solutions in the form
of programs combined with useful apparatus (not a computer) or
material process can constitute a new article linked to a possible
patent.

#a233k: Removes an erroneous part of the recital which would make algorithms
patentable.  The amended recital does not achieve everything that the
justification says it should achieve.

#abl2: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention. Nonetheless, a method
involving the use of an algorithm might be patentable provided that
the method is used to solve a technical problem. However, any patent
granted for such a method should not monopolise the algorithm itself
or its use in contexts not foreseen in the patent.

#jnW2: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention.

#htu2: The nature of the problem solved should be irrelevant to
patentability. It’s the nature of the solution that counts. Problems
are not invented, but solutions are, and it’s the invention that must
be technical (or have technical character).

#wiW3: This was a new recital from the Council.

#abl3: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention. Nonetheless, a method
involving the use of an algorithm might be patentable provided that
the method is used to solve a technical problem. However, any patent
granted for such a method should not monopolise the algorithm itself
or its use in contexts not foreseen in the patent.

#jnW3: Furthermore, an algorithm is inherently non-technical and therefore
cannot constitute a technical invention.

#htu3: The nature of the problem solved should be irrelevant to
patentability. It’s the nature of the solution that counts. Problems
are not invented, but solutions are, and it’s the invention that must
be technical (or have technical character).

#wiW4: This was a new recital from the Council.

#dfs: Methods for processing data represented in digital form are by their
very nature algorithms and are therefore inherently non-technical. On
the other hand, if information from the physical world is not captured
in order to be represented digitally, a physical process for
processing such information in hardware could have a technical
character.

#nrl: The dividing line between the material and immaterial world, and hence
between what is patentable and what is not, can be defined with legal
certainty. Once a physical signal is digitised, it becomes symbolic
information, which can be manipulated in an abstract fashion in
software, with no possible technical effect. A technical effect can be
observed only in the case of signals of a nature capable of producing
the desired effect, whilst a digital signal may be stored and
processed independently of the technologies used to represent it.

#yha: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawings. Computer-implemented inventions should be claimed at
least with reference to either a product such as a programmed
apparatus, or to a process carried out in such an apparatus.
Accordingly, where individual elements of software are used in
contexts which do not involve the realisation of any validly claimed
product or process, such use will not constitute patent infringement.

#dma: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawings. Computer-assisted inventions should be claimed only with
reference to either a product such as a programmed apparatus, or to a
%(s:technical) process carried out in such an apparatus.

#aWs: As software is not patentable, a patent cannot be infringed by the use
of software under any circumstances. Patent claims may refer only to
technical devices or processes, regardless of whether software is used
to control the process claimed.

#WWa5: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawing. Computer-implemented inventions should be claimed at
least with reference to either a product such as a programmed
apparatus, or to a process carried out in such an apparatus.
Accordingly, where individual elements of software are used in
contexts which do not involve the realisation of any validly claimed
product or process, such use will not constitute patent infringement.

#eWe3: The scope of the exclusive rights conferred by any patent is defined
by the claims, as interpreted with reference to the description and
any drawing.

#lpt8: Self-explanatory

#heW: Member States shall ensure that the description shall disclose the
invention as claimed in such terms that the technical solution can be
understood, and state any advantageous effects of the invention with
reference to the background art.

#sWa4: This amendment further clarifies what has to be disclosed in a patent
application.  In particular, the patent application has to explain the
technical problem that the invention is seeking to overcome, and its
solution, in a way that can be understood. It also has to describe any
advantages, if there are any, that the invention brings over and above
what has been done before.

#sia: Member States shall ensure that the description shall disclose the
invention as claimed in such terms that the technical problem and its
solution as well as the inventive step can be understood.

#tnd2: This amendment further clarifies what has to be disclosed in a patent
application.  In particular, the patent application has to explain the
technical problem that the invention is seeking to overcome, and its
solution, in a way that can be understood.

#cpn: It would aid in the diffusion of information and the establishment of
a comprehensive database of prior art, if patent applicants could,
where feasible, but independently of the need for the purposes of
sufficiency of disclosure to do so, file with each patent application
relating to a computer-implemented invention a well-functioning and
well documented reference implementation of a program suitable for use
in implementing the invention, which can be made available to the
public at the same time as the publication of the description.

#rer: Transparent reference implementation, where feasible, helps the
information diffusion.

#els: The legal protection of computer-implemented inventions does not
necessitate the creation of a separate body of law in place of the
rules of national patent law. The rules of national patent law remain
the essential basis for the legal protection of computer-implemented
inventions. This Directive simply clarifies the present legal position
with a view to securing legal certainty, transparency, and clarity of
the law and avoiding any drift towards the patentability of
unpatentable methods such as obvious or non-technical procedures and
business methods.

#osr: The legal protection of computer-assisted inventions does not
necessitate the creation of a separate body of law in place of the
rules of national patent law. The rules of national patent law remain
the essential basis for the legal protection of computer-assisted
inventions. This Directive simply clarifies the present legal position
with a view to securing legal certainty, transparency, and clarity of
the law and avoiding any drift towards the patentability of
unpatentable methods, in particular inherently non-technical methods
such as algorithms, software, data processing methods or teaching or
business methods.

#lpt9: Self-explanatory.

#dlc: De acuerdo con las previsiones del ADPIC, cualquier excepción al
disfrute de los derechos de patente no debería ser interpretada de
forma que permita su aplicación para ser utilizado de manera que
perjudique de forma no razonable al propietario de los derechos o que
de forma no razonable entre en conflicto con la explotación normal de
la invención aplicada a través de ordenador, teniendo en cuenta los
legítimos intereses de otros creadores de programas informáticos para
conseguir la interoperabilidad y la de los usuarios finales para tener
acceso a sistemas y redes con programas interoperables y a ser capaces
de utilizar los datos sobre diferentes sistemas de ordenadores.

#eaW2: Esta enmienda supone que es el dueño de la patente quien tiene que
probar que la excepción para el pleno disfrute de los derechos de su
patente.  El riesgo de que cualquier persona pueda simplemente ignorar
una patente reclamando que es de aplicación esta excepción queda
mitigado por la capacidad del dueño de la patente de iniciar una
acción convencional por infracción si cree que las estrictas
condiciones de esta excepción no son aplicables.  El tribunal
competente podría considerar entonces la aplicación de la excepción a
las circunstancias del caso, de acuerdo con las previsiones del ADPIC,
incluso si la excepción entra en conflicto, de forma no razonable, con
la explotación normal de la invención.

#Wct: This Directive should be limited to laying down certain principles as
they apply to the patentability of such inventions, such principles
being intended in particular to ensure that inventions which belong to
a field of technology and make a technical contribution are
susceptible of protection, and conversely to ensure that those
inventions which do not make a technical contribution are not
susceptible of protection.

#cao: Similarly to Council recital 13, this amendment claims that there are
non-technical inventions. See the justification under the amendment to
recital 13 for more information.

#wiW: This was a new recital from the Council.

#Wct2: This Directive should be limited to laying down certain principles as
they apply to the patentability of such inventions, such principles
being intended in particular to ensure that inventions which belong to
a field of technology and make a technical contribution are
susceptible of protection, and conversely to ensure that those
inventions which do not make a technical contribution are not
susceptible of protection.

#cao2: Similarly to Council recital 13, this amendment claims that there are
non-technical inventions. See the justification under the amendment to
recital 13 for more information.

#wiW2: This was a new recital from the Council.

#tnn: La presente Directiva debe limitarse al establecimiento de
determinados principios aplicables a la patentabilidad de este tipo de
invenciones. Dichos principios pretenden garantizar que las
invenciones que pertenezcan a un campo de la tecnología y aporten una
contribución técnica puedan ser objeto de protección y que, por el
contrario, aquellas invenciones que no aporten una contribución
técnica no lo sean.

#qee: Del mismo modo que la enmienda al considerando 13, esta enmienda
reivindica que hay invenciones no técnicas.

#oWn: The competitive position of Community industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-implemented inventions are eliminated
and the legal situation is transparent. With the present trend for
traditional manufacturing industry to shift their operations to
low-cost economies outside the Community, the importance of
intellectual property protection and in particular patent protection
is self-evident.

#rhh: The competitive position of Community industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-assisted inventions are eliminated
and the legal situation is transparent. The present trend for
traditional manufacturing industry to shift their operations to
low-cost economies outside the Community, as well as the requirements
for sustainable and balanced development, are factors to be taken into
consideration when choosing an appropriate level of intellectual
property protection and in particular patent protection for technical
inventions and copyright protection for software. The level of this
protection, as well as the monopolistic effects it might create,
should be determined in a manner that competition and
cross-fertilisation which are the key to the development of innovative
small and medium-sized enterprises in the European Union with easy
market access;  such enterprises will be the guarantors of the future
competitiveness of the Community.

#lpt10: Self-explanatory.

#str: The competitive position of Community industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-implemented inventions are eliminated
and the legal situation is transparent. With the present trend for
traditional manufacturing industry to shift their operations to
low-cost economies outside the Community, the importance of
intellectual property protection and in particular patent protection
is self-evident.

#mce: The competitive position of Community industry in relation to its
major trading partners will be improved if the current differences in
the legal protection of computer-implemented inventions are eliminated
and the legal situation is transparent.

#eee5: Self-evident.

#nWi3: Small- and medium-sized enterprises are essential to the economic
success and global competitiveness of the European Union and its
Member States.  Intellectual property rights benefit small and
medium-sized enterprises just as they do larger entities.  To ensure
that this Directive advances the interests of SMEs, a Committee on
Technological Innovation in the Small- and Medium-sized Enterprise
Sector should be formed.  This Committee should focus on
patent-related issues relevant to such enterprises and should bring
these issues to the attention of the Commission as necessary.

#rad4: This amendment relates to Article 10 (Monitoring) adopted by the
European Parliament during First Reading.

#pos2: Currently, SMEs participate actively in Europe’s CII patents system. 
Indeed, SMEs represent the majority of applicants for CII patents.  To
ensure ongoing and active participation by SMEs—and to provide
opportunities to enhance their involvement—this amendment proposes
the creation of a committee focused on SME-related issues, with a
mandate to recommend necessary reforms.

#aio: This Directive should be without prejudice to the application of
Articles 81 and 82 of the Treaty, in particular where a dominant
supplier refuses to allow the use of patented technique which is
needed for the sole purpose of ensuring conversion of the conventions
used in two different computers systems or networks so as to allow
communication and exchange of data content between them.

#ufe: This Directive should be without prejudice to the application of the
competition rules, in particular Articles 81 and 82 of the Treaty.

#Wtr2: More concise drafting appropriate to selectively define the purpose of
Articles 81 and 82.

#cqo: This Directive should be without prejudice to the application of
Articles 81 and 82 of the Treaty, in particular where a dominant
supplier refuses to allow the use of a patented technique which is
needed for the sole purpose of ensuring conversion of the conventions
used in two different computer systems or networks so as to allow
communication and exchange of data content between them.

#WWW4: The provisions of this Directive are without prejudice to the
application of Articles 81 and 82 of the Treaty, in particular where a
dominant supplier refuses to allow the use of a patented technique
which is needed to ensure conversion of the conventions used in two
different computer systems or networks so as to allow communication
and exchange of data content between them.

#Wnn: Interoperability, i.e. the possibility of conversion of convention
between 2 systems is a key element to prevent monopoles.

#pno2: This Directive should be without prejudice to the Application of
Articles 81 and 82 of the Treaty, in particular where a dominant
supplier refuses to allow the use of a patented technique which is
needed for the sole purpose of ensuring conversion of the conventions
used in two different computer systems or networks so as to allow
communication and exchange of data content between them.

#ou1: The provisions of this Directive are without prejudice to the
application of Articles 81 and 82 of the Treaty.

#lpt11: Self-explanatory

#eiW: Patents play an important role in European innovation.  To ensure
effective functioning of the patent system, it is important to monitor
developments in this sector, including developments involving patents
on computer-implemented inventions.  To this end, relevant data should
be gathered and appropriate reports produced.  Such reports should
include information pertaining specifically to participation by small-
and medium-sized enterprises in the system of patents for
computer-implemented inventions.

#rad5: This amendment relates to Article 10 (Monitoring) adopted by the
European Parliament during First Reading.

#dWh3: Existing statistics demonstrate fairly broad participation in the CII
patents process by SMEs.  However, there is consensus among all
interested parties that additional and more comprehensive statistical
data on CII patents would be welcomed.  The above amendment would
ensure that such data is compiled.

#WWm: The dominant supplier shall not be able to refuse to allow the use of
a patented technique which is needed for the sole purpose of ensuring
interoperability of two different computer systems or networks so as
to allow communication and exchange of data content between them.

#lpt12: Self-explanatory.

#nvW: The rights conferred by patents granted for inventions within the
scope of this Directive should not affect acts permitted under
Articles 5 and 6 of Directive 91/250/EEC, in particular under the
provisions thereof in respect of decompilation and interoperability.
In particular, acts which, under Articles 5 and 6 of Directive
91/250/EEC, do not require authorisation of the rightholder with
respect to the rightholder's copyrights in or pertaining to a computer
program, and which, but for those Articles, would require such
authorisation, should not require authorisation of the rightholder
with respect to the rightholder's patent rights in or pertaining to
the computer program.

#Wrs: The rights conferred by patents granted for inventions within the
scope of this Directive should not affect acts permitted under
Articles 5 and 6 of Directive 91/250/EEC, in particular under the
provisions thereof in respect of decompilation and interoperability.
In particular, acts which, under Articles 5 and 6 of Directive
91/250/EEC, do not require authorisation of the rightholder with
respect to the rightholder's copyrights in or pertaining to a computer
program, and which, but for those Articles, would require such
authorisation, should not require authorisation of the rightholder
with respect to the rightholder's patent rights in or pertaining to
the computer program. Moreover, where it is necessary to make use of a
patented technique to ensure conversion of the conventions used in two
different computer systems or networks so as to allow communication
and exchange of data content between them, such use should not be
considered as an infringement of patent.

#ibr: This addition makes it possible to maintain interoperability.

#ret: Since the objective of this Directive, namely to harmonise national
rules on the patentability of computer-implemented inventions, cannot
be sufficiently achieved by the Member States and can therefore be
better achieved at Community level, the Community may adopt measures,
in accordance with the principle of subsidiarity as set out in Article
5 of the Treaty.

#oer: Since the objective of the proposed action, namely to harmonise
national rules on the patentability computer-assisted inventions,
cannot be sufficiently achieved by the Member States and can
therefore, by reason of the scale or effects of this action, be better
achieved at Community level, the Community may adopt measures, in
accordance with the principle of subsidiarity as set out in Article 5
of the Treaty.

#Wai3: Idem justification article 1.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/juri0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriList0505 ;
# txtlang: xx ;
# End: ;

