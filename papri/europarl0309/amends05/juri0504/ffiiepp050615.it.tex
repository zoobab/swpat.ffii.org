\documentclass[12pt]{scrreprt}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{hyperref}
\begin{document}
\title{Brevetti Software\\Come raggiungere i propositi della ``Posizione Comune''\\Opzioni e Potenziale Compromesso}
\author{Proposta FFII a PPE}
\maketitle
\tableofcontents
\chapter{Perché vogliamo escludere i brevetti sul software e i metodi commerciali?}

La maggior parte di noi converrà che le persone creative, inclusi i
programmatori e i creatori di metodi commerciali, dovrebbero poter
ottenere un ritorno dalla propria attività e che leggi sulla proprietà
intellettuale dovrebbero essere usate quando ciò sia utile a questo fine.

Bisogna ricordare che i brevetti sono un tipo molto speciale di
``proprietà intellettuale''. Un brevetto è un monopolio su un nuovo
concetto. L'estensione delle azioni conseguentemente monopolizzante 
può essere molto ampio. Può diventare molto difficile riconciliare 
questo tipo di monopolio con la libertà di azione e la libera 
competizione, cose che sono il cuore delle moderne democrazie e 
dell'economia di mercato.

Perciò quando si permette di brevettare in un qualsiasi settore,
è necessario esaminare accuratamente il problema.

Tradizionalmente il sistema brevettuale è stato sempre limitato al
campo delle scienze applicate. La soluzione ad un problema non deve
essere solo nuova, ma anche concreta e fisica. Questo ha aiutato a
limitare il raggio di azione delle rivendicazioni brevettuali.

La Convenzione Europea sui Brevetti del 1973 esclude software,
matematica e metodi commerciali dalla brevettabilità. Nelle Linee
Guida all'Esaminazione del 1978, si dice che la materia esclusa è di
natura ``astratta''. In effetti l'elaborazione dati è un campo nel
quale la parte ingegnosa è di tipo astratto. Se c'è qualcosa di
``tecnico'' (ovvero concreto e fisico, relativo a forze controllabili
della natura) nell'elaborazione dati, esso è al più qualche aspetto
già ben conosciuto e banale come il fatto che si utilizza un computer.

L'Ufficio Brevetti Europeo (UBE) ha cominciato a rilasciare brevetti
software fin dal 1986 (decisione Vicom) e ha gradualmente allargato
il campo di concessione dei brevetti attraverso decisioni conseguenti,
specialmente quelle del 1998, che hanno premesso rivendicazioni
sui programmi, e quelle del 2000 e successive, che hanno essenzialmente
rimosso il test dell'appartenenza ad un ambito brevettabile e
trasferito quanto ne restava nel test sulla non-ovvietà (passo inventivo).

Il risultato di questa pratica è stato il rilascio di più di 30000
brevetti incredibilmente ampi e banali. L'esperienza ha provato
ancora una volta ciò che già si sapeva: aprire il sistema brevettuale
al campo astratto porta a risultati indesiderabili. L'innovazione in
questa sfera deve essere ricompensata attraverso sistemi di diritti di
proprietà più precisi, come il diritto d'autore, ed i meccanismi
informali già ottimamente funzionanti oggi nell'industria del software.
E' abitualmente possibile per i creatori di software avere un guadagno 
dal proprio lavoro. L'imitazione non è per nulla facile. Non è proibita, 
ma è molto costosa sotto l'attuale regime del diritto d'autore e della 
segretezza del codice sorgente.

Oggi, dopo quasi 10 anni di discussione pubblica sul progetto di
direttiva, sembra esserci un ampio consenso sul fatto che i brevetti
nei campi del ``puro software'' (ovvero quello nel quale operano
Microsoft e SAP) e dei metodi commerciali sono indesiderabili.

\chapter{Che cosa c'è di sbagliato nella ``Posizione Comune''?}

La ``Posizione Comune'' del Maggio 2004 è spesso presentata come un
modo per prevenire brevetti sul ``software in quanto tale'' e sui
metodi commerciali.

Noi concordiamo con l'obiettivo espresso dalla ``Posizione Comune'',
ma ci è chiaro che il contenuto è radicalmente diverso dal
contenitore. Con Microsoft e SAP che fanno opera di lobbying a favore
della ``Posizione Comune'', non dovrebbe essere necessario dare
ulteriori spiegazioni, ma siccome il sig. Lehne, nel suo discorso di
chiusura all'incontro del 2 Giugno, ha citato alcune rassicuranti
frasi della ``Posizione Comune'', a proposito del ``software in quanto
tale'' e ha sfidato le persone a spiegargli perché queste non siano
sufficienti ad escludere il software puro, cerchiamo di entrare un
po' più nel dettaglio su questo punto.

Il progetto di direttiva è iniziato nel 1997 con il ``Foglio Verde''
della Commissione, che aspirava esplicitamente ad un'armonizzazione
delle pratiche europee con quelle in vigore negli USA. L'Ufficio
Brevetti Europeo da allora ha adottate praticamente la stessa
politica degli USA riguardo la brevettabilità del software. Il
Trilateral Project (una iniziativa comune degli uffici brevetti
statunitense, giapponese ed europeo) del 2000 lo esprime molto chiaramente
nei suoi rapporti, ed introduce il termine ``invenzione attuata per
mezzo di elaboratori elettronici'' in modo da giustificare la
brevettazione di ``metodi commerciali attuati per mezzo di elaboratori
elettronici'', praticata attualmente dall'Ufficio Brevetti
Europeo e ratificata dalla ``Posizione Comune''.

La ``Posizione Comune'' contiene alcune dichiarazioni rassicuranti del
tipo

\begin{quote}
Art 4.1

Un programma per elaboratore in quanto tale non può costituire
un'invenzione brevettabile.
\end{quote}

che sembrano andare contro questa intenzione. Tuttavia questa frase,
in combinazione con le clausole seguenti, è equivalente a dire:

\begin{quote}
I pensieri in quanto tali sono liberi. Imprigioniamo solo chi li
pensa.
\end{quote}

Questo tipo di dichiarazioni non serve ad alcuno scopo regolatorio
evidente, sembra essere poco più che un tentativo di confondere il
legislatore. 

Ad un'audizione del Ministro dell'Informazione polacco del Novembre 2004
tutti i relatori, inclusi quelli di Microsoft e dell'associazione
dei giuristi specializzati in brevetti, hanno convenuto che nel campo
delle rivendicazioni di brevetto le ``invenzione attuata per mezzo di
elaboratori elettronici'' sono davvero semplicemente soluzioni software. 
Anche l'Ufficio Brevetti Europeo lo riconosce nelle sue Linee Guide per 
l'Esame del 2001, dove l'Art 52 CBE viene spiegato in questo modo:

\begin{quote}
   I programmi per computer sono una forma di ``invenzioni attuate per
   mezzo di elaboratori elettronici'', un'espressione intesa a coprire
   rivendicazioni che coinvolgono elaboratori, reti di elaboratori o
   altri apparati programmabili convenzionali nei quali \textit{prima facie} le
   caratteristiche di novità della invenzione rivendicata sono
   realizzate attraverso uno o più programmi. Tali rivendicazioni
   possono per esempio prendere la forma di un metodo per utilizzare
   suddetti apparati convenzionali, l'apparato impostato per eseguire il
   metodo o, seguendo T 1173/97, il programma stesso
\end{quote}

La Commissione e il Consiglio hanno infatti usato un linguaggio molto
simile nella loro proposta di definizione per ``invenzione attuata per
mezzo di elaboratori elettronici'', eccetto che essi pretendono che un
``programma per computer'' sia qualcosa di radicalmente differente da una
``invenzione attuata per mezzo di elaboratori elettronici''.

L'Art 5.2 è un esempio dello stile in cui la ``Posizione Comune'' è scritta: 

\begin{quote}
        Una rivendicazione su un programma in sé o su un supporto non
        dovrebbe essere permessa a meno che .... [ un requisito lungo
        e complicato che, sotto attenta analisi, si rivela essere
        sempre soddisfatto, eliminando quindi ogni limitazione ].
\end{quote}

Questi tentativi di confondere il legislatore sono in sé uno
scandalo. E il modo in cui queste posizioni sono state spinte nel
Consiglio, contro la volontà dei parlamenti nazionali, contro le
richieste di rinegoziazione di tre paesi e contro le regole
procedurali del Consiglio stesso, è stato un altro scandalo, senza
menzionare il rifiuto secco della Commissione alla
richiesta di febbraio del Parlamento di ripartire da zero e
l'irragionevole campagna-bulldozer di grandi gruppi industriali
camuffati da PMI. Stiamo assistendo al tentativo di imporre con 
tutti i mezzi all'Europa il regime dell'Ufficio Brevetti Europeo, 
sfruttando al massimo le debolezze dei processi democratici dell'Unione 
Europea, in modo da evitare un'onesta discussione sugli interessi in gioco.

È chiaro che se vogliamo creare le condizioni per raggiungere gli
intenti dichiarati nella ``Posizione Comune'', dobbiamo emendare
la ``Posizione Comune'' in modo decisivo.

Il risultato finale dovrebbe essere un gruppo ben chiaro di regole che
escluda i brevetti sull'elaborazione dati permettendo al contempo
brevetti su invenzioni nel campo automobilistico o delle macchine
medicali, dei dispositivi per la domotica ecc, indipendentemente dal
fatto che nella loro implementazione siano utilizzati mezzi per 
elaborare dati.

Queste regole dovrebbero essere il più possibile semplici e dovrebbero
essere ben integrate con Art 52 CBE e Art 27 TRIPs.

Esse non devono necessariamente regolare ogni dettaglio della
legislazione sui brevetti. Alcune parti saranno inevitabilmente
lasciate agli atti giudiziari. Ma termini quali ``tecnologia'',
che sono utilizzati dai TRIPs, devono essere concretizzati, se si
vuole raggiungere una ``armonizzazione e chiarificazione''.

In seguito spieghiamo alcuni punti base necessari per raggiungere
questo scopo.

Un eventuale insuccesso del Parlamento nel votare in favore di un
gruppo di emendamenti più o meno in linea con i principi sotto
esposti significherà che nel Luglio 2005 comincerà il Medio Evo di
cause legali per violazione di brevetti software in stile USA.

\chapter{La scelta basilare: Scienze Naturali vs Science Esatte}

Generalmente vi sono due modi di definire la ``tecnologia''.

\begin{itemize}
\item scienze esatte applicate
\item scienze naturali applicate
\end{itemize}


Queste possono essere analizzate ulteriormente, ma non sembra esserci
alcuna terza opzione. O si considera la matematica come ``un campo
della tecnologia'', oppure no. Se si scopre un nuovo modo per
ottimizzare il calcolo della via più corta possibile per il commesso
viaggiatore, questo è una invenzione tecnica? Di sicuro è una
innovazione in una scienza esatta. L'EICTA ha proposto ``scienza esatta''
nei propri commenti agli emendamenti in JURI. Un articolo interessante
scritto sul tema da un giudice dell'UBE molto influente può essere
trovato presso \url{http://swpat.ffii.org/papers/jwip-schar98/}.
L'autore opta per la scienza esatta e da questa scelta ne deriva
correttamente che ``tutte le soluzioni pratiche sono invenzioni
tecniche''.

Se concordiamo che la tecnologia debba essere ``scienze naturali
applicate'', allora questioni quali l'uso o meno della locuzione ``forze
controllabili della natura'', o se indicare esplicitamente che
l'elaborazione dati non è parte delle scienze naturali, e come
spiegarlo esattamente, sono questioni di dettaglio sulle quali
i giusti compromessi verrano trovati, presto o tardi. Se non nella seconda
lettura del Parlamento, accadrà nei negoziati per la Conciliazione con il
Consiglio.

La pratica legislativa Tedesca (includendo ``Kommunikationsl\"osung''
e altri casi del 2004) usa la definizione di ``tecnologia'' basata
sulle ``forze controllabili della natura'' ed il giudice che presiede
la più alta corte, Dr. Klaus-J\"urgen Melullis, ha recentemente
evidenziato ancora una volta che senza questa definizione le corti al
giorno d'oggi non hanno alcun modo sicuro di escludere brevetti su
concetti astratti e metodi commerciali. Allo stesso tempo Melullis ha
evidenziato che tale definizione da sola non esclude poi molto. Essa è solo
la base di partenza. Su questa sola base, praticamente ogni brevetto può essere
concesso. Per escludere veramente i brevetti software ed i metodi
commerciali, è necessario prendere in esame altri elementi, e specificamente
i concetti di ``invenzione'' e ``contributo''.

\chapter{Brevettare invenzioni di tomografia computerizzata sotto le ``Dieci Chiarificazioni''}

Se vogliamo raggiungere un consenso sulle nostre intenzioni, allora
dobbiamo essere anche in grado di risolvere la maggior parte degli altri
problemi. I principi legali necessari per escludere il software e i
metodi commerciali dalla brevettabilità sono molto chiari. FFII ha
cercato di riassumerli \footnote{vedere le \ref{nukleus} ``Dieci
Chiarificazioni Chiave'' a pagina \pageref{nukleus}}.

Questi principi sono praticamente gli stessi usati nella famosa
decisione della Corte Federale Tedesca di Giustizia (BGH) sul sistema
anti-bloccaggio-freni (ABS) nel 1980, dove sono stati usati per
giustificare la concessione di brevetto su un sistema anti
bloccaggio dei freni assistito da elaboratore, e sono
approssimativamente gli stessi che la BGH e l'autorità polacca per i
brevetti usano oggi.

Spieghiamo brevemente come si possa brevettare un sistema anti-
bloccaggio-freni o un tomografia computerizzata con queste regole.

Sistemi frenanti per auto e tomografia computerizzata sono campi
della tecnologia.

Una rivendicazione di brevetto del tipo

\begin{description}
\item[Rivendicazione 1]
processo in esecuzione su un apparato di tomografia computerizzata,
caratterizzato dal fatto che
\begin{itemize}
\item il corpo umano è analizzato con il modello X,
\item questo modello è analizzato in accordo alla regola Y,
\item il risutato è emesso da un dispositivo.
\end{itemize}
\end{description}

è  \textit{prima facie} una rivendicazione di un oggetto che risiede nel campo
della tomografia, non nel campo dell'elaborazione dati. Questo diventa
chiaro dalle definizioni proposte di ``programma per elaboratore'' e
``elaborazione dati''.

Comunque non è abbastanza trovare che il processo come insieme è nel
campo della tomografia. Deve anche essere esaminato per capire se
c'è un ``contributo'' nel campo della tomografia, ovvero se la nuova
conoscenza insita in questa soluzione sia conoscenza nel campo medico
o biologico o solamente conoscenza legata all'elaborazione dati. La
domanda da porsi sarebbe: il presunto ``inventore'' ha trovato
qualcosa di nuovo su come funziona il corpo? L' ``inventore'' è uno
specialista in medicina o biologia? O è semplicemente un programmatore
che ha utilizzato una conoscenza medica scolastica come base per
scrivere un programma più efficace con una migliore gestione della
memoria, in modo che la velocità di elaborazione aumentasse?

In alcuni casi a questa domanda può non esserci una facile risposta.
Ma c'è comunque spazio per i giudici per trovare decisioni adatte.

Dato per assunto che la precedente rivendicazione fosse in effetti
un'invenzione tecnica, il successivo quesito potrebbe essere per
una rivendicazione su programma, del tipo

\begin{description}
\item[Riendicazione 20]
programma per elaboratore su un supporto che, quando caricato in
memoria ed eseguito su un elaboratore, mette in azione il processo
descritto nella rivendicazione 1.
\end{description}

Questo tipo di rivendicazione sarebbe rigettata sotto le Dieci
Chiarificazioni Chiave. Inoltre, la libertà di distribuire programmi
che eseguono processi brevettati sarebbe garantita allo stesso modo
della libertà di pubblicare manuali di istruzioni per l'uso della
macchina per la tomografia computerizzata. Tuttavia, per eseguire il
processo che è descritto nel manuale o sul dischetto, l'operatore
della macchina per la tomografia computerizzata dovrebbe ottenere
una licenza dal detentore del brevetto. Quindi, in pratica, il
fornitore di software dovrebbe informare i propri clienti della
necessità di ottenere una licenza, e potrebbe anche essere indotto
a cooperare col detentore del brevetto nel vendere le licenze al
cliente.

La scelta è giustificata sia per ragioni economiche (competizione),
sia per ragioni di chiarezza della legge.

\begin{description}
\item[ragioni economiche]: Il mercato del software è un mercato
indipendente che sta a valle. Come per il caso dei pezzi di ricambio
per veicoli, non c'è nessuna buona ragione per dare ai produttori di
pezzi troppo controllo su questo mercato indipendente. I produttori
di software, come gli editori di manuali, sono serviti meglio dal
regime di proprietà intellettuale che attualmente vige per il
software.
\item[ragioni legali]: quando l'innovazione inserita in un programma
per computer è in un campo quale la biologia/medicina (fuori dal
campo dell'elaborazione dati), allora l'invenzione non risiede nel
lavoro di programmazione. Solo ciò che è stato inventato andrebbe
rivendicato. Rompere con questo principio porta ad una collisione con
altri importanti valori legali, quali la libertà di stampa, che
funziona come uno dei pochi limiti affidabili del sistema brevettuale.
\end{description}

Se questo è il risultato che abbiamo in mente, allora sicuramente
si potrà trovare un compromesso sulle parole da usare.

\appendix
\chapter{Dieci Chiarificazioni Chiave per la Direttiva sui Brevetti
Software}
\label{nukleus}

I primi passi sono, come descritto sopra: definizione di ``tecnologia''
(7) come ``scienze naturali applicate'', esclusioni di rivendicazioni
su programmi (4) e correzione di ``implementate per mezzo di elaboratori
elettronici'' in ``assistite da elaboratori elettronici'' (1).

{\renewcommand{\footnote}[1]{}
\input{nukleus.it}
}
\end{document}
