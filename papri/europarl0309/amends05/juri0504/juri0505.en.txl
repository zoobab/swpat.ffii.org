<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2005-05-04 JURI Amendments to the Software Patent Directive

#descr: Structured presentation of amendments tabled for decision on the
software patent directive in JURI on May 4th 2005

#submit: submitted by

#artrec: provision

#lang: language

#subdok: topic

#rekom: recommendation

#kritik: critique

#vot: vote

#amnr: no.

#votlist: Voting List

#amdetal: Amendments in Detail

#AWp: The Core Clarifications to be made by the Directive

#CxidefT: Definition of %(q:Computer-Aided Invention)

#cxidef: A %(q:Computer-aided invention)[, also inappropriately called
%(q:computer-implemented invention),] is an invention in the sense of
patent law the performance of which involves the use of a programmable
apparatus.

#ProgT: Definition of %(q:computer program)

#prog: A %(q:computer) is a realisation of an abstract machine consisting of
entities such as input/output, processor, memory, storage space and
interfaces for information exchange with external systems and human
users.  %(q:Data processing) is calculation with abstract component
entities of computers. A %(q:computer program) is a solution of a
problem by means of data processing which can, as soon as it has been
correctly described in a suitable language, be executed by a computer.

#ProdprocT: Objects of Product and Process Claims

#prodproc: A computer-aided invention may be claimed as a product, that is as a
programmed apparatus, or as a process carried out by such an
apparatus.

#ProgklmT: Exclusion of Program Claims

#progklm: A patent claim to a computer program, either on its own or on a
carrier, shall not be allowed.

#PublT: Freedom of Publication

#publ: The publication or distribution of information can never constitute a
patent infringment.

#NetechT: Negative Definition of %(q:Field of Technology)

#netech: Data processing is not a field of technology.

#TechT: Positive Definition of %(q:Technical) and %(q:Field of Technology)

#tech: %(q:Technology) is applied natural science.  A field of technology is
a discipline of applied science in which knowledge is gained by
experimentation with controllable forces of nature.  %(q:Technical)
means %(q:belonging to a field of technology).

#NekontribT: Negative Definition of %(q:Contribution)

#nekontrib: An improvement in data processing efficiency is not a technical
contribution.

#KontribT: Positive Definition of %(q:Contribution) and %(q:Invention)

#kontrib: An %(q:invention) is a contribution to the state of the art in a field
of technology.  The contribution is the set of features by which the
scope of the patent claim as a whole is presumed to differ from the
prior art.  The contribution must be a technical one, i.e. it must
comprise technical features and belong to a field of technology. 
Without a technical contribution, there is no patentable subject
matter and no invention.  The technical contribution must fulfill the
conditions for patentability.  In particular, the technical
contribution must be novel and not obvious to a person skilled in the
art.

#InteropT: Freedom of Interoperation

#interop: Wherever the use of a patented solution is necessary in order to
ensure interoperability, such use is not considered to be a patent
infringement.

#doT: Amendments Grouped by Topics

#tTf: Ten Core Clarifications

#HxW: slides of presentation given by Hartmut Pilch at %(bk:Brussels
Conference) of 2005-06-01

#WgW: for printing on one page

#vnp: How to Achieve the Aims of the %(q:Common Position): Options and
Compromise Potential

#rci: Paper for promoting the Ten Core Clarifications in EPP

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/juri0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: juri0505rc ;
# dok: juri0505 ;
# txtlang: xx ;
# End: ;

