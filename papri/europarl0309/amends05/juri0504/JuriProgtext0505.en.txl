<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Disclosure of a sample program

#descr: A program is a process description in a formal language.  Writing a
working program means describing a process in a coherent and fully
verified way.  It does not mean implementing the process.  The
subission of a working implementation of a patentable process can not
be demanded from a patentee.  But a fully verified description can be
demanded as part of his disclosure duty, and practise has shown that
it would be helpful.  Amendments which avoid the term %(q:reference
implementation) are therefore to be preferred, altough it is OK to
demand the %(e:program), which is after all not the invention, to be
%(q:implemented) in the sense of %(q:worked out and verified).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriProgtext0505 ;
# txtlang: xx ;
# End: ;

