<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interpretation of the European Patent Convention

#descr: We are dealing with an interpretatory directive.  The purpose of the
directive is to interpret (clarify and concretise) rules that have
already been laid down in a European law, namely the European Patent
Convention (EPC), and that have so far been interpreted in vastly
different ways.  The patent lobby tries to impose an interpretation
which renders Art 52 EPC meaningless and ultimately leads to its
revision, whereas the EP in its first reading opted for the meaningful
interpretation that was dominant in the 1970s and 80s.  Claims about
what the EPC means are thus of central importance for the directive
project.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriEpc0505 ;
# txtlang: xx ;
# End: ;

