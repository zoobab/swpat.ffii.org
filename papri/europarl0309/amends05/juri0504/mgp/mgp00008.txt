
Negative Definition of "Field of Technology"

Data processing is not a field of technology.

Art 52(2)c "programs for computer" = "Programme f�r Datenverarbeitungsanlagen" = "programs for data processing systems"
Edsger Dijkstra: "Computer science is no more about computers than astronomy is about telescopes."
Computers and processes running thereon are technical, but data processing is not.
data processing system = brain of a machine.
DP innovation lies in abstraction.  "Technical" is antonym to "abstract".  In DP, "technical" means "trivial".

