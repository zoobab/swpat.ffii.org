%% -*- coding: iso-8859-1 -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% This default.mgp is "TrueType fonts" oriented.
%% First, you should create "~/.mgprc" whose contents are:
%%	tfdir "/path/to/truetype/fonts"
%%
%% To visualize English, install "times.ttf", "arial.ttf", and "cour.ttf"
%% into the "tfdir" directory above:
%%	http://microsoft.com/typography/fontpack/default.htm
%%
%% To visualize Japanese, install "MSMINCHO.ttf" and 
%% "watanabenabe-mincho.ttf" into the "tfdir" directory above:
%%	http://www.mew.org/mgp/xtt-fonts_0.19981020-3.tar.gz
%%
%deffont "standard" xfont "helvetica-medium-r", tfont "standard.ttf", tmfont "hoso6.ttf"
%deffont "thick" xfont "helvetica-bold-r", tfont "thick.ttf", tmfont "hoso6.ttf"
%deffont "typewriter" xfont "courier-medium-r", tfont "typewriter.ttf", tmfont "hoso6.ttf"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Default settings per each line numbers.
%%
%default 1 area 90 90, leftfill, size 2, fore "gray20", back "white", font "standard", hgap 0
%default 2 size 7, vgap 10, prefix " ", ccolor "black"
%default 3 size 2, bar "gray70", vgap 10
%default 4 size 5, fore "gray20", vgap 30, prefix " ", font "standard"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Default settings that are applied to TAB-indented lines.
%%
%tab 1 size 5, vgap 40, prefix "  ", icon box "green" 50
%tab 2 size 4, vgap 40, prefix "      ", icon arc "yellow" 50
%tab 3 size 3, vgap 40, prefix "            ", icon delta3 "white" 40
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%nodefault
%size 6.5, font "standard", back "white", ccolor "black"

%center, fore "blue", font "standard", hgap 60, size 6.5
%image "konf0506_728x80.en.png"
CORE CLARIFICATIONS 
to be achieved by a
Directive on the Limiting Rules for the 
Patentability of Computer-Aided Inventions
%bar "skyblue" 6 15 70
%font "standard", hgap 0


%size 5, fore "darkblue"
Hartmut PILCH
%size 4.5
phm@ffii.org


%size 3, fore "black", hgap 20
http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/
http://swpat.ffii.org/papers/europarl0309/amends05/juri0504/mgp/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "cyan" "white"

Definition of "Computer-Aided Invention"

A "computer-aided invention", also [inappropriately] called "computer-implemented invention", is a patentable innovation the performance of which involves the use of a programmable apparatus.

	only data processing solutions are "computer-implemented"
	"computer-implemented invention" = "patentable computer program"
	"CII" proponents advertise patentability of "washing machines", "household appliances" etc, and use term "computer-aided" in PR.
	"control" is a special case of "aid".
	"computer-aided" is the common term used by the person skilled in the art.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "pink" "white"

Definition of "computer program"

A "computer" is an abstract machine consisting of entities such as input/output, processor, memory, storage space and interfaces for information exchange with external systems and human users.  
A "data processing system" is a computer or a network of computers.  
"Data processing" is calculation with component entities of a data processing system.  
A "computer program" is a solution of a problem by means of data processing.
A computer program may take various forms, e.g. a computing process, an algorithm, or a text recorded on a medium.
	EPO 1978 Examination Guidelines http://swpat.ffii.org/papers/epo-gl78/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "cyan" "white"

Objects of Product and Process Claims

A computer-aided invention may be claimed as a product, that is as a programmed apparatus, or as a process carried out by such an apparatus.

	Art 27 TRIPs requires patentability of "products and processes in all fields of technology".
	Need to concretise what "products" and "processes" are in software.
	Information goods are not "products".  Production of information goods is not an "industry".  Reject EPO "computer program products".
	Commission got it right, except for insinuations of software patentability. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "pink" "white"

Exclusion of Program Claims

A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.

	Council says: "shall not be allowed, unless [condition that is always true]".
	Program claim implies that the invention lies in a program. Any program claim to a non-program is undefined in scope (http://wiki.ffii.org/DilabProg04En). This reality can not be changed by declaratory amendments.
	Program claims directly restrict freedom of publication, kill shareware and free software at the very source.
	Program claims create an incentive to offshore software development.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "orange" "white"

Freedom of Publication

The publication or distribution of information can never constitute a patent infringment.

	Freedom of Publication (Art 10 ECHR) needs affirmation
	Limitable by copyright, not by patents (Art 10.2 ECHR)
	Implies No to Program Claims (McCarthy voting order 2003-06-17)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "cyan" "white"

Positive Definition of "Field of Technology" and "Technical"

A field of technology is a discipline of applied science in which knowledge is gained by experimentation with controllable forces of nature.
"Technical" means "belonging to a field of technology".

	EICTA 2005: "applied exact science"
	BGH Kommunikationsloesung 2004: "The problem is not technical, because it does not require use of controllable forces of nature to achieve a causally overseeable success."
	BGH ABS 1980: the computer-aided anti-lock braking system is a technical solution, because it involves "use of controllable forces of nature for the immediate achievement of a causally overseeable success".

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "pink" "white"

Negative Definition of "Field of Technology"

Data processing is not a field of technology.

	Art 52(2)c "programs for computer" = "Programme f�r Datenverarbeitungsanlagen" = "programs for data processing systems"
	Edsger Dijkstra: "Computer science is no more about computers than astronomy is about telescopes."
	Computers and processes running thereon are technical, but data processing is not.
	data processing system = brain of a machine.
	DP innovation lies in abstraction.  "Technical" is antonym to "abstract".  In DP, "technical" means "trivial".

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "cyan" "white"

Positive Definition of "Contribution" and "Invention"

An "invention" is a patentable innovation.
An "invention" is a contribution to the state of the art in a field of technology.
The contribution is the set of features by which the scope of the patent claim as a whole is presumed to differ from the prior art.
The contribution must be a technical one, i.e. it must comprise technical features and belong to a field of technology.  
Without a technical contribution, there is no patentable subject matter and no invention.
The technical contribution must fulfill the conditions for patentability.  In particular, the technical contribution must be novel and not obvious to a person skilled in the art.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "pink" "white"

Negative Definition of "Contribution" / "Invention"

An improvement in data processing efficiency is not a technical contribution.

%size 4, fore "black", hgap 20
	BPatG 2002 "Fehlersuche": "If computer implementations of non-technical processes were to be attributed a technical character simply based on the fact that they show special characteristics such as requiring less computing time or less memory space, then this would mean that any computer implementation would have to be attributed a technical character."
%size 4, fore "black", hgap 20
	Basinski 2001 "Business Method Patents in Europe": "In the background section, one should describe the "technical problem" addressed by the invention, in terms of how the system/method makes the computer function "more efficiently" "faster" "in a less costly way" "more of something" in doing what the invention is doing (such that you have a "further technical effect" rather than just the interaction of computer and its instructions).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%bgrad 0 0 256 90 0 "orange" "white"

Freedom of Interoperation

Wherever the use of a patented technique is necessary in order to ensure interoperability, such use is not considered to be a patent infringement.

	right to interoperate needs affirmation.
	need concretisation of 30 TRIPs.
	only a theoretical concern if core clarifications are achieved.
	90% of MEPs were in favor during 1st reading.
	not worth spending limited time of 2nd reading on.
	2nd reading question is Reaffirm Core Clarifications Yes or No

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

Summary: CORE of the CORE: 

	Clarify Art 52 EPC: 
		computer program = computer-implemented solution.
		The contribution must meet the requirements of patentability.
	Translate EPC to TRIPS: 
		Data processing is not a field of technology.
		Technology is applied natural science.
	Resulting 2nd reading proposition
		is clear
			corrects EPO errors, puts common sense back on track
			regulates only what can be regulated at this level
		is adequate
			serves Europe's economy
			does what everybody wants: patents for "ABS" only
		puts Council under pressure, keeps the debate going.
				
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page
%nodefault
%size 6.5, font "standard", back "white", ccolor "black"

%center, fore "blue", font "standard", hgap 60, size 6.5
%image "konf0506_728x80.en.png"
Don't Panic!

%font "standard", hgap 0

%size 5, fore "darkblue"

Visit

%font "typewriter", fore "blue"
       http://ffii.org/amend/
%fore "gray20", font "standard"

    for upcoming information.