<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interoperability Exemption from Patent Enforcement

#descr: Many groups have tabled amendments that exempt or pretend to exempt
interoperation between two systems from patent enforcement.  Given
that the primary purpose of the directive is to clarify the rules for
granting rather than enforcement, this is really a side issue, and the
time constraints on the 2nd reading do not allow the Parliament to
study it in depth.  There is no good reason not to stick to the
version from the 1st reading.  Several of the variants are equally
acceptable.  An separate interoperability definition is worthwhile. 
However many of the submitted variants serve to make the
interoperability exemption toothless, e.g. by making it dependent on
vague notions such as %(q:dominant player), whose application depends
on costly competition procedings, or by excluding free/opensource
software (under the guise of %(q:reasonable and non-discriminatory)
licensing terms), or by introducing the abstract terms of Art 30 TRIPs
directly into European law as a limiting condition.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/JuriInvstep0505.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JuriInterop0505 ;
# txtlang: xx ;
# End: ;

