\contentsline {section}{\numberline {1}Implemented, Controlled, Aided}{2}
\contentsline {section}{\numberline {2}Data processing vs information processing}{3}
\contentsline {section}{\numberline {3}Interoperability Definition}{4}
\contentsline {section}{\numberline {4}Definition of CAI}{4}
\contentsline {section}{\numberline {5}Contribution}{4}
\contentsline {section}{\numberline {6}Non-Technical Processes?}{4}
\contentsline {section}{\numberline {7}Positive Definition of ``Technical''}{5}
\contentsline {section}{\numberline {8}Information Processing Method}{6}
\contentsline {section}{\numberline {9}Interoperability Definition}{6}
\contentsline {section}{\numberline {10}Inventions vs Algorithms}{6}
\contentsline {section}{\numberline {11}Disentangling Inventive Step from Contribution}{7}
\contentsline {section}{\numberline {12}Definitions of ``Data Processing'' and ``Computer Program''}{7}
\contentsline {section}{\numberline {13}Negative Definition of Field of Technology}{9}
\contentsline {section}{\numberline {14}Further Technical Effect}{9}
\contentsline {section}{\numberline {15}Data Processing Efficiency is not Technical Contribution}{10}
\contentsline {section}{\numberline {16}Claim Types}{10}
\contentsline {section}{\numberline {17}Freedom of Publication}{11}
