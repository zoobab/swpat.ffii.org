Assunto: Apoio às emendas de compromisso

Caro colega,

A 5 de Julho, vamos votar em 2a leitura a directiva a directiva de patentes
de software. Necessitamos da sua assinatura para um conjunto de emendas de
compromisso que foram preparadas para resolver os conflitos entre os grupos
políticos e permitir que o Parlamento atinja uma votação de maioria.

Provavelmente está bem a par desta directiva, dado que tem sido sujeita
a um lobby extraordinariamente intensivo. Muitos colegas têm recebido
em médias cerca de 3 pedidos de agendamento de reunião todos os dias.
Um grupo de aproximadamente 20 grandes companhias com grandes departamentos
de patentes e grandes orçamentos de lobby tem estado a criar uma
aparência de apoio universal para a suposta "Posição Comum" sobre
"inventos implementados com recurso a computador".

Poderá ainda lembrar-se do pedido unânime do Parlamento para que o projecto
fosse reiniciado (reconduzir para primeira leitura), que foi rejeitado
pela Comissão Europeia em 28 de Fevereiro deste ano. Havia uma vasto
consenso no Parlamento de que a Comissão não tinha feito o seu trabalho
de casa e que a "Posição Comum" do Conselho era um pacote enganador, que não
reflectia a vontade dos estados membros. No final, este pacote foi adoptado
pela presidência do Conselho "por motivos puramente procedimentais", para
evitar a criação de um "mau precedente" que poderia ter dado aos
parlamentos nacionais um pouco mais de influência sobre o processo de
decisão do Conselho.

Entretanto, os esforços de lobby pró-patentes parecem ter resultado.
Pelo menos a votação da semana passada no Comité dos Assuntos Jurídicos
(JURI), as tentativas do relator Michel Rocard e da sub-relatora Piia-Noora
Kauppi para clarificar os limites à patentabilidade foram essencialmente
frustradas. No plenário, poderá tornar-se ainda mais difícil passar emendas
limitadoras porque, devido às regras especiais para a 2a leitura no processo
de co-decisão, qualquer ausência será contada como um voto a favor do Conselho.

Achamos este desenvolvimento bastante preocupante.

Os factos mantém-se desde Fevereiro, quando solicitamos o reinício do
processo:

 - A "Posição Comum" do Conselho promete excluir da patentabilidade
   o software puro e os modelos de negócio, mas falha no cumprimento
   desta promessa. Desde 1997, quando o objectivo declarado no Greenpaper
   da Comissão era harmonizar a prática de patentes Europeia com a dos
   EUA e Japão, os meios propostos para atingir este objectivo não
   mudaram substancialmente. Apenas o fraseado se tornou ainda mais
   convulso e enganador.
   
 - Se o Parlamento Europeu falhar a rejeição ou seriamente emendar a
   proposta do Conselho em 5 de Julho, a sujeição a monopólios sobre
   regras de cálculo triviais e modelos de negócio irá começar
   imediatamente e será irreversível durante vários anos. Existe um
   medo consensual entre profissionais do software e economistas que
   isto poderá ser devastador para o cenário do software Europeu, no
   qual 70% do emprego é criado por umas poucas 100,000 PMEs que no
   seu total detém menos de 10% das patentes de software concedidas
   e não incapazes de jogar o jogo dos portfólios de patentes.

 - Os cidadãos Europeus e os parlamentos nacionais necessitam de um
   Parlamento Europeu capaz de dizer Não. Quando a qualidade da
   legislação que surge dos órgãos executivos afunda abaixo de determinado
   nível, temos de puxar o travão de emergência. Se dissermos Não mais
   vezes, talvez os cidadãos que representamos o digam menos vezes
   em futuros referendos.

Por estes motivos, apelamos-lhe que subscreva e apoie o conjunto de
emendas de compromisso que se encontram em anexo.

Estas emendas de compromisso representam um conjunto conservador de
regras simples, que se baseiam em práticas provadas e largamente aceites
de tribunais nacionais.

As regras mais essenciais são exprimidas nas seguintes emendas:

 Nr. 8: exclusão efectiva de modelos de negócio
 Nr. 3: "tecnologia" é "ciência natural aplicada"
 Nr. 4: a contribuição tem de satisfazer os critérios de patentabilidade
 Nr. 9: proíbe monopolização directa de objectos informacionais
 Nr. 1: muda "implementado com recurso a computador" para "auxiliado por
        computador"

Sob esta proposta, as invenções auxiliadas por computador em áreas tal como
a tomografia computorizada, controlo de motores automóveis, aparelhos
caseiros, etc... serão sem qualquer dúvida patenteáveis. Nenhuma
preocupação legítima sobre possíveis fraseados demasiado exigentes da
1a leitura poderá ter qualquer base neste conjunto de compromisso.

Para compreender completamente a motivação destas emendas, por favor veja
o estuda da FFII anexado que, em nosso parecer, argumenta de forma
convincente a favor de regras semelhantes.

Em nome do futuro da União Europeia e da competitividade da nossa economia
do conhecimento, esperamos mesmo muito que consiga concordar rapidamente
em subscrever o nosso conjunto de compromisso, para que possa ser agendado
já na próxima quarta-feira, 29 de Junho.

Saudações cordiais,
