\contentsline {section}{\numberline {1}Directive on the patentability of computer-implemented inventions}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Artikel 1: Werkingssfeer}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Artikel 2: Definities}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Artikel 3a: In computers ge\"{\i }mplementeerde uitvindingen als een gebied van de technologie}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Artikel 4: Voorwaarden voor octrooieerbaarheid}{2}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Artikel 4a: Uitsluitingen van octrooieerbaarheid}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Artikel 5: Vorm van de conclusies; en verdere voorzieningen}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Artikel 6: Interoperabiliteit}{4}{subsection.1.7}
\contentsline {section}{\numberline {2}koppelingen met voetnoten}{4}{section.2}
