<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#ctd: Document Header

#aha: Demande d'habilitation

#WWo: Current package on form of claims/interoperability

#SWC: 2. State of Play:  Council

#tll: 3. State of Play - Parliament

#tWt: 4. Details of the submission to GrAC

#spW: 5. Possible Developments in the Council

#EEU: PROPOSITION DE DIRECTIVE DU PARLEMENT EUROPEEN ET DU CONSEIL
CONCERNANT LA BREVETABILITE DES INVENTIONS MISES EN OeVRE PAR
ORDINATEUR (SEC(2004) 525 ; SI(2004) 320)

#dib: Les chefs de cabinet recommandent au Collège d'autoriser M. BOLKESTEIN
à soutenir le compromis de la présidence dans la perspective d'une
prochaine décision du Conseil, conformément à la ligne indiquée au
document SEC(2004) 525 et dans la mesure où l'équilibre de la
proposition initiale de la Commission est préservé.

#ate: Il est pris acte que M. BOLKESTEIN, en accord avec M. MONTI, veillera
au contenu du considérant n° 17, aux termes duquel les dispositions de
la directive, en particulier concernant la question de
l'interopérabilité, ne préjugent pas l'application des articles 81 et
82 du Traité.

#ive: Le Chef de cabinet de M. LIIKANEN émet une réserve d'attente
concernant l'aspect de l'interopérabilité.

#CfA: FICHE %(q:GrAC)

#hr4: Updated version of fiche %(q:GrAC) in the light of the Intellectual
Property Working Party (Patents) meeting of 30 March 2004 and COREPER
on 21 April 2004

#ujc: Subject:

#ehr: Proposal for a Directive of the European Parliament and of the Council
on the patentability of computer-implemented inventions.

#CWp: The objective of this fiche is to request a mandate for Commissioner
Bolkestein to accept the Presidency's compromise package along the
lines indicated in this fiche to facilitate adoption of the Council's
common position. The habilitation would allow Commissioner Bolkestein
to go along with the Presidency compromise, providing that the balance
of the Commission's original proposal is preserved.

#eWc: Elements of package

#ura: Computer programs on carriers

#tel: Interoperability

#Wul: The package previously agreed at GAP/hebdo (August 2003) balanced an
acceptance of claims for computer programs on carriers (Article 5(2))
in exchange for an additional interoperability exception (Article 6a).

#oie: The current Presidency compromise is based on this package except that
the intention behind Article 6a is covered by a new Recital 17 agreed
by MSs in Council working group.

#inW: In our view, the new Recital 17 provides adequate provision to deal
with abuses hindering interoperability. This is rightly dealt with
under competition rules and the Commission has shown that it is not
afraid to defend third parties by robustly applying these rules (e.g.
Microsoft). Interoperability in the context of acts permitted under
copyright is already covered by existing Article 6.

#hin: Moreover, any fear that Article 5(2) may appear to allow the patenting
of computer programs as such is addressed in several places as
follows:

#aop: Recital 7a clearly states that %(q:a computer program as such cannot
constitute a patentable invention)

#tic: Article 4 sets out the conditions for patentability and stipulates
that an a computer program as such cannot be patentable;

#ect: Article 4a provides clarification that the simple specification of
technical means is not enough to imply patentability.

#iie: Additionally, other aspects serve to reinforce the overall balance of
the text, such as Recital 12 on technical contribution and Articles 7,
8(ca) 8(cd) and 8a on reporting. Therefore, as a whole we feel that
this text provides a compromise with which the Commission could agree.

#rWn: On 14 November 2002 the Council (Competitiveness) reached broad
agreement on a common approach, pending adoption of the European
Parliament�s opinion at first reading, noting a reservation by the
Commission, a general scrutiny reservation by FR and a parliamentary
scrutiny reservation by ES, as well as statements made by BE
(supported by IT) and FR.

#rsd: The Intellectual Property Working Party (Patents) discussed the
amendments proposed by the European Parliament on several occasions,
the latest being 30 March 2004 and has agreed to take on board a
number thereof. The Presidency�s compromise text reflects the text of
the Council�s common approach mentioned above, as amended in order to
accommodate some of the amendments proposed by the European
Parliament. Furthermore, the Presidency�s text generally reflects the
position taken by Commissioner Bolkestein at the EP plenary session.

#We4: As a result of COREPER on 21 April, the Presidency's text (8731/04)
appears to be

#WWj: acceptable without change for UK, IE, NL, AT, FI, SE, PT, LU, IT and
EL

#asi: subject to a general scrutiny reservation by FR, BE, ES at this stage
although FR indicated a positive attitude on a technical level

#WWt: subject to parliamentary scrutiny reservation by DK

#eWW: subject to a general reservation by DE as well as specific
reservations on the definition of technical contribution in Article
2(b) and on recitals 7a and 13.

#tyn: Article 2 (b) contains a definition of %(q:technical contribution).
DE, with some support from BE and DK for the intention, has proposed
an expanded definition which draws on amendments 107 and 69 (slightly
modified) of the European Parliament, which were rejected by the
Commission. The other delegations prefer the wording contained in the
Presidency text. At COREPER the Presidency was not minded to take this
amendment on board.

#arl: In relation to the issue of interoperability, the competition aspect
is now covered by recital 17. Some MS were interested in a more
extensive provision in the direction of Parliament�s Article 6a,
which was unacceptable to the Commission because the wording went
considerably beyond the context of interoperability and would
introduce exceptions to the scope of patent protection which are
incompatible with our international obligations under the WTO TRIPs
Agreement. However most agreed that that the issue could be dealt with
through competition rules as in recital 17, and any further provision
should be kept in hand for the second reading.

#pyt: The EP plenary of 24 September 2003 approved some 50 amendments to the
Commission's proposal. Many of these amendments are completely
unacceptable to the Commission and have not been taken on board in the
Presidency text. They create large discrepancies between patent
practice for computer-implemented compared to other types of invention
which defeat the object of a harmonisation directive. Exceptions for
patentability are created which could remove patent protection from
almost all computer-implemented inventions in the EU, particularly in
the telecommunications field. For example, the form of
interoperability exception contemplated by Parliament would severely
damage incentives for innovation in the computer-related area.
Furthermore, it would be in breach of the Community's obligations
under the TRIPs Agreement and would invite retaliation by our major
trading partners.

#Tkt: The main elements of the Presidency�s compromise are spelt out below.
This compromise safeguards the overall balance between interests of
right holders and other parties that was contained in the original
proposal. Adoption of this package would require a modification of the
Commission�s position to accept new wording in Recital 17 to deal
with abuses hindering interoperability rather than an additional
Article 6a as previously contemplated. Further aspects of the package
are outlined below.

#tro: Claims to a computer program on its own or on a carrier

#rir: The compromise text states in Article 5, paragraph 2 that:

#oji: A claim to a computer program, either on its own or on a carrier,
shall not be allowed unless that program would, when loaded and
executed in a computer, programmed computer network or other
programmable apparatus, put into force a product or process claimed in
the same patent application in accordance with paragraph 1.

#ecr: All delegations accept this Article.

#cpw: The requirement that claims which are consistent with paragraph 1 be
included in the patent application if the form of claim in paragraph 2
is used ensures that the substance of all claims in a patent is in
conformity with what was originally intended by the Commission.

#tel2: Interoperability

#sti: New Recital 17 clarifies the role of competition law in cases where a
dominant supplier limits the use of a technique required for
interoperability. It states that:

#Wtt: The provisions of this Directive are without prejudice to the
application of Articles 81 and 82 of the Treaty, in particular where a
dominant supplier refuses to allow the use of a patented technique
which is needed for the sole purpose of ensuring conversion of the
conventions used in two different computer systems or networks so as
to allow communication and exchange of data content between them.

#isr: Most delegations feel that this is the best way to deal with any
potential difficulties in this area. FR has a reservation because they
feel that the particular wording may be broader than necessary to
ensure interoperability.

#dom: In light of the recent decision in the Microsoft case, the Commission
has shown itself capable of dealing with this issue in the manner
foreseen by this recital.

#shW: Commission report on the effect of the directive

#olv: Articles 7, 8(ca) 8(cd) and 8a require that the Commission monitor and
report on the effect of the directive on the development and
commercialisation of interoperable computer programs. This requirement
will allow the Commission to propose amendments to this directive if
the provisions relating to interoperability are found to be
insufficient.

#hee: The adoption by the Commission of the above provisions would allow
adoption of the directive in Council by a qualified majority rather
than unanimity. The latter seems unlikely in any case given the
current reservations of certain Member States.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cec0405 ;
# txtlang: en ;
# multlin: t ;
# End: ;

