\begin{subdocument}{europarl-cpedu}{Call for Action II}{http://swpat.ffii.org/papers/europarl0309/demands/europarl-cpedu.en.html}{Workgroup\\swpatag@ffii.org\\english version 2003/10/21 by PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{The European Parliament has voted for legislation that would effectively exclude software and business methods from patentability.  However European patent legislation is still largely in the hands of ministerial patent experts, many of whom have for years been pushing for unlimited patentability.  This situation calls for close attention and resolute action by national parliamentarians and concerned citizens.}
\begin{sect}{prob}{We are concerned that}
\begin{enumerate}
\item
The European Patent Office\footnote{http://swpat.ffii.org/players/epo/swpatepo.en.html} (EPO) has, in contradiction to the letter and spirit of the written law, granted tens of thousands of patents on rules for computing with conventional data processing equipment, below termed ``software patents''.

\item
The European Comission has proposed to legalise these patents and make them uniformly enforcable in Europe.  In doing so, it has disregarded the manifest will and well-argued reasoning of the vast majority of software professionals, software companies, computer scientists and economists.

\item
Prominent proponents have tried to deceive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/eubsa-tech.en.html\#cecjuri} and intimidate\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/deba/plendeba0309.en.html\#bolk} the European Parliament.  They have presented the proposal as a means of excluding software and business methods from patentability and have threatened that the European Parliament would lose its chance of participation if it voted for a real limitation of patentability.

\item
Since the European Parliament has refused to be deceived or intimidated, influential patent professionals in various governments and organisations are now trying to use the EU Council of Ministers in order to sidestep parliamentary democracy in the European Union.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{For these reasons we recommend the following:}
\begin{enumerate}
\item
We urge the European Patent Office as well as national patent offices to immediately stop granting patents on business methods and data processing methods in whatever verbal clothing, and to apply Article 52 of the European Patent Convention\footnote{http://swpat.ffii.org/analysis/epc52/epue52.en.html} correctly according to conventional methods of interpretation of law\footnote{http://swpat.ffii.org/analysis/epc52/exeg/epue52exeg.de.html}.

\item
We urge the members of the EU Council of Ministers to refrain from any counter-proposals to the European Parliament's version of the draft, unless such counter-proposals have been explicitely endorsed by a majority decision of the member's national parliament.

\item
We urge members of national parliaments to formulate clear national policies on the limits of patentability and to make sure that their government's representatives in the European Council are faithfully implementing these policies.

\item
We demand that all legislative proposals, including those from the European Parliament and the member states, be rigorously tested against a test suite\footnote{http://swpat.ffii.org/analysis/testsuite/swpatmanri.en.html} of sample patent applications to see whether they would beyond any doubt lead to the desired results and would not leave room for any more misinterpretations.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Signatories}
\begin{center}
See http://aktiv.ffii.org/euparl/en
\end{center}

Bent Hindrup Andersen\footnote{http://www.hindrup.dk} (MEP, EDD, Denmark, June Movement\footnote{http://www.j.dk})\\
Jens-Peter Bonde\footnote{http://www.bonde.com/} (MEP, EDD (chairman), Denmark, June Movement\footnote{http://www.j.dk})\\
Johanna Boogerd\footnote{http://www.johannaboogerd.nl/} (MEP, ELDR, Netherlands)\\
Hiltrud Breyer\footnote{http://www.hiltrud-breyer.de} (MEP, VERD, Germany)\\
Daniel Cohn-Bendit\footnote{http://www.cohn-bendit.com/} (MEP, VERD, France)\\
Jan Dhaene\footnote{http://www.jandhaene.be} (MEP, Belgium)\\
Raina Mercedes Echerer\footnote{http://www.mercedes-echerer.at/} (MEP, VERD, Austria)\\
Pernille Frahm\footnote{http://www.frahm.dk/} (MEP, GUE/NGL, Denmark)\\
Pierre Jonckheer\footnote{http://www.pierrejonckheer.be/} (MEP, VERD, Belgium)\\
Piia-Noora Kauppi\footnote{http://www.kauppi.net/} (MEP, PPE-DE, Finnland)\\
Wolfgang Kreissl-Dörfler\footnote{http://www.kreissl-doerfler.de/} (MEP, PSE, Germany)\\
Jean Lambert\footnote{http://http://www.jeanlambertmep.org.uk/} (MEP, VERD, United Kingdom)\\
Donald Neil (Professor) MacCormick\footnote{http://wwwdb.europarl.eu.int/ep5/owa/whos\_mep.data?ipid=0\&ilg=EN\&iucd=4548\&ipolgrp=.\&ictry=GB\&itempl=\&ireturn=\&imode=} (MEP, VERD, United Kingdom)\\
Caroline Lucas\footnote{http://www.carolinelucasmep.org.uk/} (MEP, VERD, United Kingdom)\\
Heide Rühle\footnote{http://www.heide-ruehle.de} (MEP, VERD, Germany)\\
Ulla Sandbæk\footnote{http://www.ullasandbaek.dk/} (MEP, EDD, Denmark, June Movement\footnote{http://www.j.dk})\\
Olle Schmidt\footnote{http://www.olles.nu/} (MEP, ELDR, Sweden)\\
Bart Staes\footnote{http://www.bartstaes.be/} (MEP, VERD, Belgium, Groen!\footnote{http://www.groen.be})\\
Claude Turmes\footnote{http://www.greng.lu/site/index.php?ref3=111\&ref2=7\&navtype=3\&typebloc=2} (MEP, VERD, Luxemburg, DÉI GRÉNG\footnote{http://www.greng.lu})\\
Anders Wijkman\footnote{http://www.wijkman.kristdemokrat.se/default.asp} (MEP, PPE-DE, Sweden)\\
Matti Wuori\footnote{http://www.mattiwuori.net/} (MEP, VERD, Finnland)\\
Olga Zrihen\footnote{http://www.dpsb.be/oz/oz.html} (MEP, PSE, Belgium, PS\footnote{http://www.ps.be})\\
Jens Holm\footnote{http://www.jensholm.se/} (MEP candidate, Sweden)\\
Ellen Trane Nørby\footnote{http://www.ellenieu.dk/} (MEP candidate, Denmark)\\
Dirk Van Der Maelen (Leader parliamentary faction of Flemish Social Democrats in Belgian Parliament, Belgium, SP.be\footnote{http://www.sp.be})\\
Grietje Bettin\footnote{http://www.sh.gruene.de:8080/MdB/Bettin/ein\_text?datum2=2002/05/13\percent{}2020\percent{}3A00\percent{}3A19\percent{}20GMT\percent{}2B2} (Member of the German Federal Parliament, Media Policy Speaker of the Green Party)\\
Gustav Fridolin\footnote{http://www.mp.se/gustavfridolin.asp} (MP, Sweden)\\
Zo\'{e} Genot (MP, Belgium, ecolo.be\footnote{http://www.ecolo.be/})\\
Arseni Gibert (Senator of the Girona region for Entesa Catalana de Progrés\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})\\
Ulrich Kelber\footnote{http://www.ulrich-kelber.de/} (Member of the German Federal Parliament (German Social Democratic Party\footnote{http://www.spd.de/}))\\
Monica Lochner-Fischer\footnote{http://www.lochner-fischer.de/} (member of Bavarian Parliament, SPD)\\
Dr. Jan Van Duppen\footnote{http://www.janvanduppen.be} (Belgian Senator, Member of the Flemish Parliament, SP.a spirit\footnote{http://www.sp.be/})\\
CEA/PME\footnote{http://www.ceapme.org} (European Confederation of Associations of Small and Medium Enterprises) (contact: Walter Grupp)\\
Danish Association of IT Professionals\footnote{http://www.prosa.dk/} (PROSA.dk) (contact: Peter Ussing)\\
VOV@SPD\footnote{http://www.vov.de/} (Internet Association of the German Social Democratic Party) (contact: Axel Schudak, Arne Brand and Boris Piwinger)\\
temPS r\'{e}els\footnote{http://www.temps-reels.net} (Internet Branch of the French Socialist Party) (contact: Thierry Noisette)\\
Opera Software\footnote{http://www.opera.com/} (contact: H\aa{}kon Wium Lie, CTO)\\
Magix AG\footnote{http://www.magix.com/} (contact: Oliver Lorenz, Head of Legal Department)\\
ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat} (contact: Stefan Pollmeier, Managing Director)\\
FSG-IT\footnote{http://www.fsg-it.de} (contact: Lars Noschinski, Developer)\\
ObjectWeb\footnote{http://wiki.objectweb.org/Wiki.jsp?page=CWP\_SoftwarePatents\_French} (contact: Francois Letellier)\\
TFDesign s.r.o.\footnote{http://www.tfdesign.cz} (contact: Michal Zajic, Owner, Head Designer - civil engineering)\\
ashampoo Technology GmbH \& Co. KG\footnote{http://tech.ashampoo.com/} (contact: Hauke Duden)\\
Intelligent Firmware Ltd\footnote{http://www.intelligentfirm.co.uk/} (contact: Michael Krech, Managing Director)\\
PETE SOFTWARE GmbH\footnote{http://www.pete-software.de/} (contact: Thies Reinhold, CEO)\\
Phaidros AG\footnote{http://www.phaidros.com/german/phaidros/engagement/standpunkt\_patente.htm} (contact: Matthias Schlegel, CEO)\\
Power Media Sp. z o.o.\footnote{http://www.power.com.pl/} (contact: Narczynski, Wojtek Jakub, CTO, VP \& co-founder)\\
Myriad Software\footnote{http://www.myriad-online.com/} (contact: Guillion Didier, managing director)\\
MySQL\footnote{http://www.mysql.com/} (contact: Michael Widenius, Florian Mueller)\\
NEOlabs\footnote{http://www.neolabs.be/} (contact: Jo De Baer, consultant)\\
Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, founder and owner)\\
Ulf Dunkel (invers Software Vertrieb\footnote{http://www.calamus.net/} and Dunkel Software Distribution\footnote{http://www.dsd.net/}, Managing Director)\\
Mind\footnote{http://www.mind.be/} (contact: Dr. ir. Peter Vandenabeele, Managing Director)\\
Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Managing Director)\\
Intevation GmbH\footnote{http://www.intevation.de/} (contact: Bernhard Reiter and Jan-Oliver Wagner)\\
RDG Software di 'Roberto Della Grotta'\footnote{http://www.rdg-software.it/} (contact: Roberto Della Grotta)\\
S.K.I. GmbH\footnote{http://www.ski-gmbh.com/} (contact: Michael Schlegel)\\
Bureau d'Etudes en Génie Informatique, Hasgard\footnote{http://www.hasgard.net/} (contact: Bruno Berthelet)\\
Nexedi\footnote{http://www.nexedi.com/} (contact: Jean-Paul Smets)\\
TRI-EDRE and TRI-EDRE Developments\footnote{http://www.tri-edre.fr/} (contact: Thierry Rolland, managing director)\\
Stephan K\"{o}rner (Managing Director, Pilum Technology GmbH)\\
Marco Schulze (Managing Director, Nightlabs GmbH\footnote{http://www.nightlabs.com/})\\
Python Business Forum\footnote{http://www.python-in-business.org/} (contact: Jacob Hall\'{e}n, president)\\
National Computer Helpdesk\footnote{http://NationalComputerHelpdesk.net/} (contact: John Harris - Manager Support Services, Christopher Thompson - Director)\\
ROHOST\footnote{http://www.rohost.com/} (contact: Marius David)\\
AB STRAKT\footnote{http://www.strakt.com/} (contact: Laura Creighton, Venture Capitalist)\\
Prof. Martin Kretschmer (Centre for Intellectual Property Policy \& Management\footnote{http://www.cippm.org.uk/}, Joint Director)\\
ROSPOT\footnote{http://www.rospot.com/} (contact: Cosmin Neagu)\\
Dr. Karl-Friedrich Lenz\footnote{http://k.lenz.name/LB/archives/000617.html\#000617} (Professor for German and European Law, Aoyama Gakuin Daigaku, Japan)\\
Dr. Jean-Philippe Rennard (economist, informatician, Managing Director, Eponyme SA\footnote{http://www.eponyme.com/})\\
Anders Skovsted Buch (Lector in mathematics, Aarhus University, Denmark)\\
Prof. Dr. Koen De Bosschere (professor of informatics, Ghent University, Belgium)\\
Prof. Dr. Wilfried Philips (professor of informatics, Ghent University, Belgium)\\
Dr. Michiel Ronsse (researcher, Ghent University)\\
Student Union Akku (University of Nijmegen, The Netherlands)\footnote{http://www.kun.nl/akku/} (contact: Dieter Van Uytvanck)\\
Institut Biotecnologia i Biomedicina (Universitat Aut\'{o}noma Barcelona, Spain)\footnote{http://www.uab.es/investigacio/ibf.htm} (contact: Dr. Enrique Querol, Director)\\
Austrian University Students Union\footnote{http://www.oeh.ac.at/} (contact: Leonard Dobusch)\\
ANSOL - Associa\c{c}\~{a}o Nacional para o Software Livre\footnote{http://www.ansol.org/} (contact: Jaime Villate, President)\\
APRIL\footnote{http://www.april.org/} (contact: Olivier Berger)\\
Catalan Linux User Group\footnote{http://patents.caliu.info/} (CALIU)\\
Electronic Frontier Finnand\footnote{http://www.effi.org/patentit/index.en.html} (contact: Ville Oksanen, Tapani Tarvainen)\\
Europe Shareware\footnote{http://www.europe-shareware.org/} (contact: Pascal Ricard and Sylvain Perchaud)\\
Association for Free Software\footnote{http://www.affs.org.uk/} (contact: Marc Eberhard)\\
Association of Free Software Professionals\footnote{http://www.afsp.org.uk/} (contact: Neil Darlow)\\
Bürgernetzverband e.V.\footnote{http://www.buerger.net/} (contact: Eberhard Mittag)\\
Free Software Foundation Europe\footnote{http://www.fsfeurope.org/} (contact: Georg Greve, president)\\
FSF France\footnote{http://www.fsffrance.org/} (contact: Fr\'{e}d\'{e}ric Couchet)\\
German Unix User Group\footnote{http://www.guug.de/} (GUUG.de, contact: Christian Lademann, vice president)\\
Internet Society Belgium\footnote{http://www.isoc.be/} (contact: Rudi Vansnick, president)\\
Internet Society Poland\footnote{http://www.isoc.org.pl/} (contact: Wladek Majewski)\\
LSVB\footnote{http://www.lsvb.nl} (Dutch National Student Union) (contact: Jurjen van den Bergh)\\
Patentverein.de\footnote{http://www.patentverein.de/} (defense association of german SMEs against trivial patents) (contact: Heiner Flocke)\\
LSVB (The Dutch National Student Union)\footnote{http://www.lsvb.nl} (contact: Jurjen van den Bergh)\\
Liberale Hochschulgruppe Ulm\footnote{http://www.uni-ulm.de/lhg/} (contact: Markus Schaber)\\
SSLUG.dk\footnote{http://www.sslug.dk} (contact: Erik Josefsson)\\
WINA-Leuven (Student Association of Students in Maths, Computer Sciences and Physics of the KU Leuven)\footnote{http://www.wina.be/} (contact: Dominique Devriese)\\
VVS (Union of Flemish Students)\footnote{http://www.vvs.ac/} (official standpoint\footnote{http://www.vvs.ac/standpunt/pdf/2004\_05\_01\_patenten\_op\_software\_en\_zakenmethodes.pdf})\\
Zeus WPI (Student Workgroup on Informatics of Ghent University)\footnote{http://zeus.ugent.be/} (contact: Jonas Maebe)\\
Foundation for a Free Information Infrastructure\footnote{http://www.ffii.org/ffii.en.html} (contact, Hartmut Pilch, Alex Macfie and Holger Blasum)\\
\dots (see Signators of the previous version.\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/eubsa-cpedu.en.html\#sign}, We are currently putting together a new list)\\
more signatures\footnote{euparl-sign.html} (16482 persons have so far signed this appeal via the FFII participation system.)
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf European Software Patents: Assorted Examples\footnote{http://swpat.ffii.org/patents/samples/swpikmupli.en.html}}}

\begin{quote}
We came across the following impressive examples at first reading through our tabular listings of software patents.  They were almost randomly chosen and thus approximately represent the technicity and inventivity standards of the European Patent Office.  If anything distinguishes them from other patents it is the relative ease with which they can be understood at first sight by a fairly large public.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente in Aktion\footnote{http://swpat.ffii.org/patents/effects/swpikxrani.en.html}}}

\begin{quote}
In den letzten Jahren sind einige Streitf\"{a}lle um Softwarepatente durch die Medien bekannt geworden.  Es handelt sich hierbei um die Spitze des Eisbergs. Die meisten Entwickler und Firmen werden nur au{\ss}erhalb der Gerichte mit Patentforderungen konfrontiert, und Schweigen liegt im beiderseitigen Interesse.  Viele Projekte werden zur\"{u}ckgeschraubt oder aufgegeben.  Es ist schwer, verhinderte Entwicklungen zu dokumentieren.  Hier wollen wir diesen Versuch unternehmen.
\end{quote}
\filbreak

\item
{\bf {\bf Quotations on Software Patents\footnote{http://swpat.ffii.org/archive/quotes/swpatcusku.en.html}}}

\begin{quote}
Salient quotations from law texts, economic analyses, political documents as well as statements by programmers, politicians and other parties interested in the debate about software patents.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analysis/testsuite/swpatmanri.en.html}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{http://swpat.ffii.org/papers/europarl0309/europarl0309.en.html}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papers/europarl0309/reag/europarl-reag0309.en.html}}}

\begin{quote}
Frits Bolkestein, Malcolm Harbour and other politicians have joined in a choir of patent lawyers who are saying that ``the European Parliament ruined its chances of democratic participation'' by failing to vote as the patent establishment had asked it to vote.  Preparations are under way to withdraw the directive and legalise software patents by means of an agreement between national patent officials, who are used to deciding patent policies among themselves.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council 2004/01/29 ``Presidency Compromise Proposal'' on Software Patents\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/cons0401.en.html}}}

\begin{quote}
The Irish EU Council presidency has circulated a paper among governental ministries which contains alternative suggestions to the amendments on the directive ``on the patentability of computer-implemented inventions'' passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, ``computer-implemented'' algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/plen0309.en.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf How you can help us stop Software Patents\footnote{http://swpat.ffii.org/group/todo/swpatgunka.en.html}}}
\filbreak

\item
{\bf {\bf Help gain supporters for this Call!}}

\begin{quote}
Contact cpedu-help@ffii.org for information on how to procede.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
% mode: latex ;
% End: ;

