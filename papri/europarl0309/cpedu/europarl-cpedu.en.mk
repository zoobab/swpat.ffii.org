# -*- mode: makefile -*-

europarl-cpedu.en.lstex:
	lstex europarl-cpedu.en | sort -u > europarl-cpedu.en.lstex
europarl-cpedu.en.mk:	europarl-cpedu.en.lstex
	vcat /ul/prg/RC/texmake > europarl-cpedu.en.mk


europarl-cpedu.en.dvi:	europarl-cpedu.en.mk
	rm -f europarl-cpedu.en.lta
	if latex europarl-cpedu.en;then test -f europarl-cpedu.en.lta && latex europarl-cpedu.en;while tail -n 20 europarl-cpedu.en.log | grep -w references && latex europarl-cpedu.en;do eval;done;fi
	if test -r europarl-cpedu.en.idx;then makeindex europarl-cpedu.en && latex europarl-cpedu.en;fi

europarl-cpedu.en.pdf:	europarl-cpedu.en.ps
	if grep -w '\(CJK\|epsfig\)' europarl-cpedu.en.tex;then ps2pdf europarl-cpedu.en.ps;else rm -f europarl-cpedu.en.lta;if pdflatex europarl-cpedu.en;then test -f europarl-cpedu.en.lta && pdflatex europarl-cpedu.en;while tail -n 20 europarl-cpedu.en.log | grep -w references && pdflatex europarl-cpedu.en;do eval;done;fi;fi
	if test -r europarl-cpedu.en.idx;then makeindex europarl-cpedu.en && pdflatex europarl-cpedu.en;fi
europarl-cpedu.en.tty:	europarl-cpedu.en.dvi
	dvi2tty -q europarl-cpedu.en > europarl-cpedu.en.tty
europarl-cpedu.en.ps:	europarl-cpedu.en.dvi	
	dvips  europarl-cpedu.en
europarl-cpedu.en.001:	europarl-cpedu.en.dvi
	rm -f europarl-cpedu.en.[0-9][0-9][0-9]
	dvips -i -S 1  europarl-cpedu.en
europarl-cpedu.en.pbm:	europarl-cpedu.en.ps
	pstopbm europarl-cpedu.en.ps
europarl-cpedu.en.gif:	europarl-cpedu.en.ps
	pstogif europarl-cpedu.en.ps
europarl-cpedu.en.eps:	europarl-cpedu.en.dvi
	dvips -E -f europarl-cpedu.en > europarl-cpedu.en.eps
europarl-cpedu.en-01.g3n:	europarl-cpedu.en.ps
	ps2fax n europarl-cpedu.en.ps
europarl-cpedu.en-01.g3f:	europarl-cpedu.en.ps
	ps2fax f europarl-cpedu.en.ps
europarl-cpedu.en.ps.gz:	europarl-cpedu.en.ps
	gzip < europarl-cpedu.en.ps > europarl-cpedu.en.ps.gz
europarl-cpedu.en.ps.gz.uue:	europarl-cpedu.en.ps.gz
	uuencode europarl-cpedu.en.ps.gz europarl-cpedu.en.ps.gz > europarl-cpedu.en.ps.gz.uue
europarl-cpedu.en.faxsnd:	europarl-cpedu.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo europarl-cpedu.en-??.g3n`;source faxsnd main
europarl-cpedu.en_tex.ps:	
	cat europarl-cpedu.en.tex | splitlong | coco | m2ps > europarl-cpedu.en_tex.ps
europarl-cpedu.en_tex.ps.gz:	europarl-cpedu.en_tex.ps
	gzip < europarl-cpedu.en_tex.ps > europarl-cpedu.en_tex.ps.gz
europarl-cpedu.en-01.pgm:
	cat europarl-cpedu.en.ps | gs -q -sDEVICE=pgm -sOutputFile=europarl-cpedu.en-%02d.pgm -
europarl-cpedu.en/europarl-cpedu.en.html:	europarl-cpedu.en.dvi
	rm -fR europarl-cpedu.en;latex2html europarl-cpedu.en.tex
xview:	europarl-cpedu.en.dvi
	xdvi -s 8 europarl-cpedu.en &
tview:	europarl-cpedu.en.tty
	browse europarl-cpedu.en.tty 
gview:	europarl-cpedu.en.ps
	ghostview  europarl-cpedu.en.ps &
print:	europarl-cpedu.en.ps
	lpr -s -h europarl-cpedu.en.ps 
sprint:	europarl-cpedu.en.001
	for F in europarl-cpedu.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	europarl-cpedu.en.faxsnd
	faxsndjob view europarl-cpedu.en &
fsend:	europarl-cpedu.en.faxsnd
	faxsndjob jobs europarl-cpedu.en
viewgif:	europarl-cpedu.en.gif
	xv europarl-cpedu.en.gif &
viewpbm:	europarl-cpedu.en.pbm
	xv europarl-cpedu.en-??.pbm &
vieweps:	europarl-cpedu.en.eps
	ghostview europarl-cpedu.en.eps &	
clean:	europarl-cpedu.en.ps
	rm -f  europarl-cpedu.en-*.tex europarl-cpedu.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} europarl-cpedu.en-??.* europarl-cpedu.en_tex.* europarl-cpedu.en*~
europarl-cpedu.en.tgz:	clean
	set +f;LSFILES=`cat europarl-cpedu.en.ls???`;FILES=`ls europarl-cpedu.en.* $$LSFILES | sort -u`;tar czvf europarl-cpedu.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
