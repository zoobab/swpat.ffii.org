<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wezwanie do działania

#descr: Parlament Europejski przegłosował dyrektywę, która w sposób skuteczny wykluczyłaby z patentowania oprogramowanie i metody prowadzenia działalności gospodarczej. Jednakże los prawa patentowego pozostaje w dalszym ciągu w rękach ministerialnych urzędników patentowych, z których wielu od lat naciska na zniesienie ograniczeń w patentowaniu. Ta sytuacja wywołuje potrzebę uwagi i zdecydowanego działania parlamentarzystów i obywateli krajów członkowskich.

#Wce: Jesteśmy zaniepokojeni tym, że

#Fea: Z tych powodów opowiadamy się za następującymi działaniami

#Sno: Sygnotariusze

#tef: %(ep:Europejski Urząd Patentowy) %(pe:EPO) wbrew literze i duchowi obowiązującego prawa udzielił dziesiątków tysięcy patentów na rozwiązania programistyczne i metody prowadzenia działalności gospodarczej, które są niżej nazywane %(q:patentami na oprogramowanie komputerowe).

#toi: Komisja Europejska %(cp:wywiera nacisk), by zalegalizować patenty na oprogramowanie komputerowe i egzekwować ich przestrzeganie w całej Europie. W swym dążeniu Komisja Europejska ignoruje wyraźnie wyrażaną wolę i dobrze uzasadnione racje zdecydowanej większości informatyków: twórców oprogramowania, przedsiębiorców, naukowców, a także ekonomistów.

#clt: Zwolennicy patentów na oprogramowanie próbują %(dp:zmylić) i %(ip:zastraszyć) Parlament Europejski. Przedstawiaja oni swoją propozycję jako drogę do wyłączenia rozwiązań programistycznych i metod prowadzenia działalności gospodarczej z patentowania i grożą, że Parlament Europejski straci swoją szansę uczestniczenia w stanowieniu prawa patentowego gdyby zagłosował za określeniem faktycznych granic patentowalności.

#qrW: Ponieważ Parlament Europejski nie dał się wprowadzić w błąd i nie uległ szantażowi, posiadający wpływy w różnych rządach i organizacjach zwolennicy patentów na oprogramowanie próbują użyć Rady Ministrów Unii Europejskiej dla odstąpienia od parlamentarnego procesu demokratycznego.

#ltt: Wzywamy Europejski Urząd Patentowy i narodowe urzędy patentowe by niezwłocznie zaprzestały udzielania patentów na metody prowadzenia działalności gospodarczej i rozwiązania programistyczne bez względu na sposób w jaki są one formułowane oraz stosowały %(ar:artykuł 52 Europejskiej Konwencji Patentowej) zgodnie ze %(ee:zwykłymi metodami interpretacji prawa).

#ivt: Wzywamy członków Rady Ministrów UE do powstrzymania się od jakichkolwiek kontr-propozycji w stosunku do wersji zaproponowanej przez Parlament Europejski, chyba że miałyby one poparcie wyrażone w formalnych decyzjach ich narodowego parlamentu.

#fWl: Wzywamy parlamentarzystów w krajach członkowskich do sformułowania jasnych narodowych polityk co do granic patentowalności i dopilnowania by ich rządowi przedstawiciele w Radzie Europejskiej sumiennie prezentowali te polityki.

#Dsl: Żądamy, by wszelkie akty prawodawcze %(pe:zarówno propozycje europejskie jak i państw członkowskich) dotyczące patentowalności podlegały wpierw %(ts:testowaniu) na dobranych losowo zbiorach zgłoszeń, w celu stwierdzenia ponad wszelką wątpliwość, czy prowadzą one do pożądanych skutków i czy nie stwarzają niebezpieczeństwa niewłaściwych interpretacji.

#lsl: Leader parliamentary faction of Flemish Social Democrats in Belgian Parliament

#MSe: Media Policy Speaker of the Green Party

#Sna: Senator of the Girona region for %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: European Confederation of Associations of Small and Medium Enterprises

#aru: Walter Grupp

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-founder

#fed: founder and owner

#gbb: Nightlabs GmbH

#iDc: Joint Director

#smp: Professor for German and European Law

#cos: economist

#nmc: informatician

#caliu: Catalan Linux User Group

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Sygnotariusze poprzedniej wersji.

#Wta: We are currently putting together a new list

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII participation system).

#gti: Help gain supporters for this Call!

#tro: Contact %(MT) for information on how to procede.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@a2e.de ;
# login: ciniol ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl-cpedu ;
# txtlang: pl ;
# multlin: t ;
# End: ;

