<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Oppfordring til handling II

#descr: Europaparlamentet har stemt for en lovgivning som vil utelukke 
programmer og forretningsidéer fra patentering.  Men den europeiske 
lovgivningen ligger fortsatt hovedsaklig i hendene på departementenes 
patenteksperter.  Mange av dem har presset på for ubegrenset 
patenterbarhet i årevis.  Denne situasjonen bør få høy oppmerksomhet
og  krever resolutt handling fra nasjonale parlamentarikere og
engasjerte  borgere.

#Wce: Hva vi er bekymret for

#tef: %(ep:Det europeiske patentkontoret) (EPO) har, i motstrid med  lovens
bokstav og ånd, innvilget titusener av patenter på regler for 
databehandling ved hjelp av vanlig datautstyr, heretter omtalt som 
%(q:programvarepatenter).

#toi: Europakommisjonen har %(cp:foreslått) å gjøre disse patentene  lovlige
og gjøre det mulig å håndheve dem enhetlig over hele Europa.  Ved å
gjøre dette, har kommisjonen sett bort fra den klare og vel 
argumenterte tankegangen til et klart flertall blant profesjonelle 
programvareutviklere, programvareselskaper, informatikere og økonomer.

#clt: Fremtredende tilhengere har forsøkt å %(dp:villede) og  %(ip:skremme)
europaparlamentet.  De har fremstilt forslaget som et  middel for å
utelukke programvare og forretningsidéer fra patentering,  og har
truet med at europaparlamentet ville miste sin mulighet til å  delta i
beslutningsprosessen hvis det stemte for en virkelig  innskrenkning av
patenterbarheten.

#qrW: Siden europaparlamentet ikke har villet la seg villede og skremme, 
forsøker innflytelsesrike patenteksperter i flere regjeringer og 
organisasjoner nå å bruke EUs ministerråd for å sette til side det 
parlamentariske demokratiet i den europeiske union.

#Fea: Derfor anbefaler vi følgende

#ltt: Vi oppfordrer det europeiske patentkontoret, så vel som nasjonale 
patentkontorer på det sterkeste til å umiddelbart stoppe med å
innvilge  patenter på forretningsidéer og databehandlingsmetoder i
enhver ordlyd,  og å anvende %(ar:artikkel 52 i den europeiske
patentkonvensjonen)  riktig i henhold til %(ee:vanlig lovfortolkning).

#ivt: Vi oppfordrer på det sterkeste ministerrådets medlemmer til å avstå
fra å fremme motforslag til europaparlamentets utgave av utkastet, med
mindre slike motforslag har blitt uttrykkelig velsignet ved en
flertallsavgjøresle i medlemmets nasjonale parlament.

#fWl: Vi oppfordrer de lokale parlamentene til å utforme klare nasjonale
retningslinjer for grensene for patenterbarhet, og til å forsikre seg
om at regjeringens representanter i europarådet lojalt følger disse
retningslinjene.

#Dsl: Vi krever at alle lovforslag, også de fra europaparlamentet og 
medlemslandene, blir testet strengt mot en %(ts:testserie) av utvalgte
 patentsøknader, for å se om de, hinsides enhver tvil, vil ha den
ønskede  effekt og ikke gi rom for noen feiltolkninger.

#Sno: Underskrivere

#mandir: managing director

#proedr: chairman

#presid: president

#infprof: professor of informatics

#sci: professor of electrical engineering

#serchist: researcher

#konsultist: consultant

#junmov: Junibevægelsen

#SPD: Sozialdemokratische Partei Deutschlands

#lsl: Leader parliamentary faction of Flemish Social Democrats in Belgian
Parliament

#MSe: Media Policy Speaker of the Green Party

#Sna: Senator of the Girona region for %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: European Confederation of Associations of Small and Medium Enterprises

#aru: Walter Grupp

#DiW: Forbundet af IT-professionelle

#eei: Internet Branch of the French Socialist Party

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-founder

#fed: founder and owner

#gbb: Nightlabs GmbH

#iDc: Joint Director

#smp: Professor for German and European Law

#cos: economist

#nmc: informatician

#caliu: Catalan Linux User Group

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Signators of the previous version.

#Wta: We are currently putting together a new list

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#gti: Help gain supporters for this Call!

#tro: Contact %(MT) for information on how to procede.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

