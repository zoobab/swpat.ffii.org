<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Oproep tot Handelen II

#descr: Het Europees Parlement heeft gestemd voor een wetgeving die software
en methoden voor bedrijfsvoering expliciet van octrooieerbaarheid zou
uitsluiten.  Niettemin is de octrooiwetgeving nog steeds grotendeels
in handen van ministeriële octrooiexperten, waarvan velen jarenlang
voor onbeperkte octrooieerbaarheid hebben geijverd.  Deze situatie
vraagt om een nauwgezette opvolging en een resolute actie door
parlementairen en bezorgde burgers.

#Wce: We zijn bezorgd omdat

#tef: Het %(ep:Europees Octrooibureau) (EOB) heeft, in tegenspraak met de
letter en de geest van de geschreven wetgeving, tienduizenden
softwarepatenten toegekend. De term %(q:softwarepatenten) wordt hier
gebruikt in de zin van octrooien op regels voor het verrichten van
geestelijke arbeid en wiskunde, geformuleerd in termen van
conventionele computerapparatuur.

#toi: De Europese Commissie heeft %(cp:voorgesteld) deze octrooien geldig te
verklaren en ze afdwingbaar te maken over heel Europa.  Hierdoor heeft
ze de duidelijke wil en goed-onderbouwde redeneringen van de overgrote
meerderheid van professionele softwareontwikkelaars,
softwarebedrijven, computerwetenschappers en economen genegeerd.

#clt: Sommige voorstanders hebben getracht om het Europees Parlement te
%(dp:misleiden) en te %(ip:intimideren).  Zij hebben het voorstel
omschreven als een manier om software en methoden voor bedrijfsvoering
uit te sluiten van octrooieerbaarheid en hebben gedreigd dat het
Europees Parlement zijn kans tot deelname aan het debat zou verspelen
als het zou stemmen voor echte beperkingen wat betreft de
octrooieerbaarheid.

#qrW: Nu het Europees Parlement geweigerd heeft zich te laten misleiden of
te laten intimideren, proberen invloedrijke octrooispecialisten de EU
Raad van Ministers te gebruiken om de parlementaire democratie in de
Europese Unie te omzeilen.

#Fea: Om deze redenen bevelen wij het volgende aan

#ltt: Wij dringen bij zowel het Europees Octrooibureau als bij de nationale
octrooibureaus aan om onmiddellijk te stoppen met het toekennen van
octrooien op methoden voor bedrijfsvoering en methoden voor
gegevensverwerking en om %(ar:Artikel 52 van het Europees
Octrooiverdrag) toe te passen op een manier die overeenkomt met de
%(ee:conventionele methoden tot interpretatie van de wet).

#ivt: Wij dringen bij de leden van de EU Raad van Ministers aan om zich te
onthouden van het formuleren van enige tegenvoorstellen t.o.v. de
versie van het Europees Parlement, tenzij een dergelijk tegenvoorstel
de uitdrukkelijke goedkeuring heeft gekregen van een meerderheid in
het nationaal parlement van de lidstaat.

#fWl: Wij dringen bij de leden van de nationale parlementen aan om een
duidelijk nationaal beleid te formuleren wat betreft de beperkingen
van octrooieerbaarheid en om te verzekeren dat de vertegenwoordigers
van hun regeringen in de Europese Raad dit beleid terdege
implementeren.

#Dsl: Wij eisen dat alle wetsvoorstellen, inclusief deze van het Europees
Parlement en van de lidstaten, uiterst grondig getoetst worden aan de
hand van een %(ts:testreeks) van uitgekozen octrooiaanvragen om te
zien of ze boven alle twijfel verheven zouden leiden tot de gewenste
resultaten en geen ruimte laten voor verdere verkeerde interpretaties.

#Sno: Ondertekenaars

#mandir: bestuurder

#proedr: Voorzitter

#presid: Voorzitter

#infprof: professor in de informatica

#sci: professor in de elektrotechniek

#serchist: onderzoeker

#konsultist: consultant

#junmov: JuniBeweging

#SPD: Duitse Sociaal-Democratische Partij

#lsl: Fractievoorzitter sp.a in de Kamer

#MSe: Media Policy Speaker of the Green Party

#Sna: Senator van de Girona-regio voor %(ECP)

#eal: lid van het het Beiers Parlement

#lWt: Belgische Senator

#ian: Europese Confederatie van Verenigingen van Kleine en Middelgrote
Bedrijven)

#aru: Walter Grupp

#DiW: Forbundet af IT-professionelle

#eei: Internet Branch of the French Socialist Party

#dat: Hoofd van de Juridische Afdeling

#Mic: Matthias Schlegel

#Odo: CTO, VP & mede-oprichter

#fed: Oprichter en eigenaar

#gbb: Nightlabs GmbH

#iDc: Medebestuurder

#smp: Professor in Duits en Europees Recht

#cos: econoom

#nmc: informaticus

#caliu: Catalaanse Linuxgebruikers

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Ondertekenaars van de vorige versie

#Wta: wij zijn momenteel bezig een nieuwe lijst samen te stellen.

#rgr: Meer handtekeningen

#gcr: %(N) personen hebben tot nu toe deze oproep getekend via het %(ps:FFII
deelnemerssysteem).

#gti: Help ons steunbetuigers vinden voor deze Oproep.

#tro: Contacteer %(MT) voor informatie over hoe tewerk te gaan.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: dietvu ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

