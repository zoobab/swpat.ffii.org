\begin{subdocument}{Cpedu0410}{Oproep tot Handelen II}{http://swpat.ffii.org/papri/europarl0309/oproep/0410/index.nl.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\Nederlandse versie 2003/10/27 door Dieter VAN UYTVANCK\footnote{\url{http://www.student.kun.nl/dieter.vanuytvanck/}}}{Het Europees Parlement heeft gestemd voor een wetgeving die software en methoden voor bedrijfsvoering expliciet van octrooieerbaarheid zou uitsluiten.  Niettemin is de octrooiwetgeving nog steeds grotendeels in handen van ministeri\"{e}le octrooiexperten, waarvan velen jarenlang voor onbeperkte octrooieerbaarheid hebben geijverd.  Deze situatie vraagt om een nauwgezette opvolging en een resolute actie door parlementairen en bezorgde burgers.}
\begin{sect}{prob}{We zijn bezorgd omdat}
\begin{enumerate}
\item
Het Europees Octrooibureau\footnote{\url{http://swpat.ffii.org/gasnu/epo/index.en.html}} (EOB) heeft, in tegenspraak met de letter en de geest van de geschreven wetgeving, tienduizenden softwarepatenten toegekend. De term ``softwarepatenten'' wordt hier gebruikt in de zin van octrooien op regels voor het verrichten van geestelijke arbeid en wiskunde, geformuleerd in termen van conventionele computerapparatuur.

\item
De Europese Commissie heeft voorgesteld deze octrooien geldig te verklaren en ze afdwingbaar te maken over heel Europa.  Hierdoor heeft ze de duidelijke wil en goed-onderbouwde redeneringen van de overgrote meerderheid van professionele softwareontwikkelaars, softwarebedrijven, computerwetenschappers en economen genegeerd.

\item
Sommige voorstanders hebben getracht om het Europees Parlement te misleiden\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html\#cecjuri}} en te intimideren\footnote{\url{http://wiki.ffii.org/AnchNl}}.  Zij hebben het voorstel omschreven als een manier om software en methoden voor bedrijfsvoering uit te sluiten van octrooieerbaarheid en hebben gedreigd dat het Europees Parlement zijn kans tot deelname aan het debat zou verspelen als het zou stemmen voor echte beperkingen wat betreft de octrooieerbaarheid.

\item
Nu het Europees Parlement geweigerd heeft zich te laten misleiden of te laten intimideren, proberen invloedrijke octrooispecialisten de EU Raad van Ministers te gebruiken om de parlementaire democratie in de Europese Unie te omzeilen.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{Om deze redenen bevelen wij het volgende aan}
\begin{enumerate}
\item
Wij dringen bij zowel het Europees Octrooibureau als bij de nationale octrooibureaus aan om onmiddellijk te stoppen met het toekennen van octrooien op methoden voor bedrijfsvoering en methoden voor gegevensverwerking en om Artikel 52 van het Europees Octrooiverdrag\footnote{\url{http://swpat.ffii.org/stidi/epc52/index.en.html}} toe te passen op een manier die overeenkomt met de conventionele methoden tot interpretatie van de wet\footnote{\url{http://swpat.ffii.org/stidi/epc52/exeg/index.de.html}}.

\item
Wij dringen bij de leden van de EU Raad van Ministers aan om zich te onthouden van het formuleren van enige tegenvoorstellen t.o.v. de versie van het Europees Parlement, tenzij een dergelijk tegenvoorstel de uitdrukkelijke goedkeuring heeft gekregen van een meerderheid in het nationaal parlement van de lidstaat.

\item
Wij dringen bij de leden van de nationale parlementen aan om een duidelijk nationaal beleid te formuleren wat betreft de beperkingen van octrooieerbaarheid en om te verzekeren dat de vertegenwoordigers van hun regeringen in de Europese Raad dit beleid terdege implementeren.

\item
Wij eisen dat alle wetsvoorstellen, inclusief deze van het Europees Parlement en van de lidstaten, uiterst grondig getoetst worden aan de hand van een testreeks\footnote{\url{http://swpat.ffii.org/stidi/manri/index.en.html}} van uitgekozen octrooiaanvragen om te zien of ze boven alle twijfel verheven zouden leiden tot de gewenste resultaten en geen ruimte laten voor verdere verkeerde interpretaties.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Ondertekenaars}
\begin{center}
Zie http://aktiv.ffii.org/euparl/nl
\end{center}

Bent Hindrup Andersen\footnote{\url{http://www.hindrup.dk}} (MEP, EDD, Denemarken, JuniBeweging\footnote{\url{http://www.j.dk}})\\
Jens-Peter Bonde\footnote{\url{http://www.bonde.com/}} (MEP, EDD (Voorzitter), Denemarken, JuniBeweging\footnote{\url{http://www.j.dk}})\\
Johanna Boogerd\footnote{\url{http://www.johannaboogerd.nl/}} (MEP, ELDR, Nederland)\\
Hiltrud Breyer\footnote{\url{http://www.hiltrud-breyer.de}} (MEP, VERD, Duitsland)\\
Daniel Cohn-Bendit\footnote{\url{http://www.cohn-bendit.com/}} (MEP, VERD, Frankrijk)\\
Jan Dhaene\footnote{\url{http://www.jandhaene.be}} (MEP, Belgi\"{e})\\
Raina Mercedes Echerer\footnote{\url{http://www.mercedes-echerer.at/}} (MEP, VERD, Oostenrijk)\\
Pernille Frahm\footnote{\url{http://www.frahm.dk/}} (MEP, GUE/NGL, Denemarken)\\
Pierre Jonckheer\footnote{\url{http://www.pierrejonckheer.be/}} (MEP, VERD, Belgi\"{e})\\
Piia-Noora Kauppi\footnote{\url{http://www.kauppi.net/}} (MEP, PPE-DE, Finland)\\
Wolfgang Kreissl-Dörfler\footnote{\url{http://www.kreissl-doerfler.de/}} (MEP, PSE, Duitsland)\\
Jean Lambert\footnote{\url{http://http://www.jeanlambertmep.org.uk/}} (MEP, VERD, Verenigd Koninkrijk)\\
Donald Neil (Professor) MacCormick\footnote{\url{http://wwwdb.europarl.eu.int/ep5/owa/whos_mep.data?ipid=0&ilg=EN&iucd=4548&ipolgrp=.&ictry=GB&itempl=&ireturn=&imode=}} (MEP, VERD, Verenigd Koninkrijk)\\
Caroline Lucas\footnote{\url{http://www.carolinelucasmep.org.uk/}} (MEP, VERD, Verenigd Koninkrijk)\\
Heide Rühle\footnote{\url{http://www.heide-ruehle.de}} (MEP, VERD, Duitsland)\\
Ulla Sandbæk\footnote{\url{http://www.ullasandbaek.dk/}} (MEP, EDD, Denemarken, JuniBeweging\footnote{\url{http://www.j.dk}})\\
Olle Schmidt\footnote{\url{http://www.olles.nu/}} (MEP, ELDR, Zweden)\\
Bart Staes\footnote{\url{http://www.bartstaes.be/}} (MEP, VERD, Belgi\"{e}, Groen!\footnote{\url{http://www.groen.be}})\\
Claude Turmes\footnote{\url{http://www.greng.lu/site/index.php?ref3=111&ref2=7&navtype=3&typebloc=2}} (MEP, VERD, Luxemburg, DÉI GRÉNG\footnote{\url{http://www.greng.lu}})\\
Anders Wijkman\footnote{\url{http://www.wijkman.kristdemokrat.se/default.asp}} (MEP, PPE-DE, Zweden)\\
Matti Wuori\footnote{\url{http://www.mattiwuori.net/}} (MEP, VERD, Finland)\\
Olga Zrihen\footnote{\url{http://www.dpsb.be/oz/oz.html}} (MEP, PSE, Belgi\"{e}, PS\footnote{\url{http://www.ps.be}})\\
Jens Holm\footnote{\url{http://www.jensholm.se/}} (MEP candidate, Zweden)\\
Ellen Trane Nørby\footnote{\url{http://www.ellenieu.dk/}} (MEP candidate, Denemarken)\\
Dirk Van Der Maelen (Fractievoorzitter sp.a in de Kamer, Belgi\"{e}, SP.be\footnote{\url{http://www.sp.be}})\\
Grietje Bettin\footnote{\url{http://www.sh.gruene.de:8080/MdB/Bettin/ein_text?datum2=2002/05/13%2020%3A00%3A19%20GMT%2B2}} (Lid van het Duitse federale parlement, Media Policy Speaker of the Green Party)\\
Gustav Fridolin\footnote{\url{http://www.mp.se/gustavfridolin.asp}} (kamerlid, Zweden)\\
Zo\'{e} Genot (kamerlid, Belgi\"{e}, ecolo.be\footnote{\url{http://www.ecolo.be/}})\\
Arseni Gibert (Senator van de Girona-regio voor Entesa Catalana de Progrés\footnote{\url{http://www.senado.es/solotexto/legis7/grupos/GRECP.html}})\\
Ulrich Kelber\footnote{\url{http://www.ulrich-kelber.de/}} (Lid van het Duitse federale parlement (Duitse Sociaal-Democratische Partij\footnote{\url{http://www.spd.de/}}))\\
Monica Lochner-Fischer\footnote{\url{http://www.lochner-fischer.de/}} (lid van het het Beiers Parlement, SPD)\\
Dr. Jan Van Duppen\footnote{\url{http://www.janvanduppen.be}} (Belgische Senator, Member of the Flemish Parliament, SP.a spirit\footnote{\url{http://www.sp.be/}})\\
CEA/PME\footnote{\url{http://www.ceapme.org}} (Europese Confederatie van Verenigingen van Kleine en Middelgrote Bedrijven)) (Contacten: Walter Grupp)\\
Forbundet af IT-professionelle\footnote{\url{http://www.prosa.dk/}} (PROSA.dk) (Contacten: Peter Ussing)\\
VOV@SPD\footnote{\url{http://www.vov.de/}} (Internet Association of the German Social Democratic Party) (Contacten: Axel Schudak, Arne Brand en Boris Piwinger)\\
temPS r\'{e}els\footnote{\url{http://www.temps-reels.net}} (Internet Branch of the French Socialist Party) (Contacten: Thierry Noisette)\\
Opera Software\footnote{\url{http://www.opera.com/}} (Contacten: H\aa{}kon Wium Lie, CTO)\\
Magix AG\footnote{\url{http://www.magix.com/}} (Contacten: Oliver Lorenz, Hoofd van de Juridische Afdeling)\\
ESR Pollmeier GmbH\footnote{\url{http://www.esr-pollmeier.de/swpat}} (Contacten: Stefan Pollmeier, Directeur Beheer)\\
FSG IT\footnote{\url{http://www.fsg-it.de}} (Contacten: Lars Noschinski, Developer)\\
ObjectWeb\footnote{\url{http://wiki.objectweb.org/Wiki.jsp?page=CWP_SoftwarePatents_French}} (Contacten: Francois Letellier)\\
TFDesign s.r.o.\footnote{\url{http://www.tfdesign.cz}} (Contacten: Michal Zajic, Owner, Head Designer - civil engineering)\\
ashampoo Technology GmbH \& Co. KG\footnote{\url{http://tech.ashampoo.com/}} (Contacten: Hauke Duden)\\
Intelligent Firmware Ltd\footnote{\url{http://www.intelligentfirm.co.uk/}} (Contacten: Michael Krech, Directeur Beheer)\\
PETE SOFTWARE GmbH\footnote{\url{http://www.pete-software.de/}} (Contacten: Thies Reinhold, CEO)\\
Phaidros AG\footnote{\url{http://www.phaidros.com/german/phaidros/engagement/standpunkt_patente.htm}} (Contacten: Matthias Schlegel, CEO)\\
Power Media Sp. z o.o.\footnote{\url{http://www.power.com.pl/}} (Contacten: Narczynski, Wojtek Jakub, CTO, VP \& mede-oprichter)\\
Myriad Software\footnote{\url{http://www.myriad-online.com/}} (Contacten: Guillion Didier, bestuurder)\\
MySQL\footnote{\url{http://www.mysql.com/}} (Contacten: Michael Widenius, Florian Mueller)\\
NEOlabs\footnote{\url{http://www.neolabs.be/}} (Contacten: Jo De Baer, consultant)\\
Thorsten Lemke (Lemke Software GmbH\footnote{\url{http://www.lemkesoft.com/}}, Oprichter en eigenaar)\\
Ulf Dunkel (invers Software Vertrieb\footnote{\url{http://www.calamus.net/}} en Dunkel Software Distribution\footnote{\url{http://www.dsd.net/}}, Directeur Beheer)\\
Mind\footnote{\url{http://www.mind.be/}} (Contacten: Dr. ir. Peter Vandenabeele, Directeur Beheer)\\
Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Directeur Beheer)\\
Intevation GmbH\footnote{\url{http://www.intevation.de/}} (Contacten: Bernhard Reiter en Jan-Oliver Wagner)\\
RDG Software di 'Roberto Della Grotta'\footnote{\url{http://www.rdg-software.it/}} (Contacten: Roberto Della Grotta)\\
S.K.I. GmbH\footnote{\url{http://www.ski-gmbh.com/}} (Contacten: Michael Schlegel)\\
Bureau d'Etudes en Génie Informatique, Hasgard\footnote{\url{http://www.hasgard.net/}} (Contacten: Bruno Berthelet)\\
Nexedi\footnote{\url{http://www.nexedi.com/}} (Contacten: Jean-Paul Smets)\\
TRI-EDRE and TRI-EDRE Developments\footnote{\url{http://www.tri-edre.fr/}} (Contacten: Thierry Rolland, bestuurder)\\
Stephan K\"{o}rner (Directeur Beheer, Pilum Technology GmbH)\\
Marco Schulze (Directeur Beheer, Nightlabs GmbH\footnote{\url{http://www.nightlabs.com/}})\\
Python Business Forum\footnote{\url{http://www.python-in-business.org/}} (Contacten: Jacob Hall\'{e}n, Voorzitter)\\
National Computer Helpdesk\footnote{\url{http://NationalComputerHelpdesk.net/}} (Contacten: John Harris - Manager Support Services, Christopher Thompson - Director)\\
ROHOST\footnote{\url{http://www.rohost.com/}} (Contacten: Marius David)\\
AB STRAKT\footnote{\url{http://www.strakt.com/}} (Contacten: Laura Creighton, Venture Capitalist)\\
Prof. Martin Kretschmer (Centre for Intellectual Property Policy \& Management\footnote{\url{http://www.cippm.org.uk/}}, Medebestuurder)\\
ROSPOT\footnote{\url{http://www.rospot.com/}} (Contacten: Cosmin Neagu)\\
Dr. Karl-Friedrich Lenz\footnote{\url{http://k.lenz.name/LB/archives/000617.html\#000617}} (Professor in Duits en Europees Recht, Aoyama Gakuin Daigaku, Japan)\\
Dr. Jean-Philippe Rennard (econoom, informaticus, Directeur Beheer, Eponyme SA\footnote{\url{http://www.eponyme.com/}})\\
Anders Skovsted Buch (Lector in mathematics, Universiteit van Aarhus, Denemarken)\\
Prof. Dr. Koen De Bosschere (professor in de informatica, Universiteit van Ghent, Belgi\"{e})\\
Prof. Dr. Wilfried Philips (professor in de informatica, Universiteit van Ghent, Belgi\"{e})\\
Dr. Michiel Ronsse (onderzoeker, Universiteit van Ghent)\\
Student Union Akku (University of Nijmegen, The Netherlands)\footnote{\url{http://www.kun.nl/akku/}} (Contacten: Dieter Van Uytvanck)\\
Institut Biotecnologia i Biomedicina (Universitat Aut\'{o}noma Barcelona, Spain)\footnote{\url{http://www.uab.es/investigacio/ibf.htm}} (Contacten: Dr. Enrique Querol, Director)\\
\"{O}sterreichische Hochsch\"{u}lerInnenschaft -- Bundesvertretung\footnote{\url{http://www.oeh.ac.at/}} (Contacten: Leonard Dobusch)\\
ANSOL - Associa\c{c}\~{a}o Nacional para o Software Livre\footnote{\url{http://www.ansol.org/}} (Contacten: Jaime Villate, President)\\
APRIL\footnote{\url{http://www.april.org/}} (Contacten: Olivier Berger)\\
Catalaanse Linuxgebruikers\footnote{\url{http://patents.caliu.info/}} (CALIU)\\
Electronic Frontier Finnand\footnote{\url{http://www.effi.org/patentit/index.en.html}} (Contacten: Ville Oksanen, Tapani Tarvainen)\\
Europe Shareware\footnote{\url{http://www.europe-shareware.org/}} (Contacten: Pascal Ricard en Sylvain Perchaud)\\
Association for Free Software\footnote{\url{http://www.affs.org.uk/}} (Contacten: Marc Eberhard)\\
Association of Free Software Professionals\footnote{\url{http://www.afsp.org.uk/}} (Contacten: Neil Darlow)\\
Bürgernetzverband e.V.\footnote{\url{http://www.buerger.net/}} (Contacten: Eberhard Mittag)\\
Free Software Foundation Europe\footnote{\url{http://www.fsfeurope.org/}} (Contacten: Georg Greve, voorzitter)\\
FSF France\footnote{\url{http://www.fsffrance.org/}} (Contacten: Fr\'{e}d\'{e}ric Couchet)\\
German Unix User Group\footnote{\url{http://www.guug.de/}} (GUUG.de, Contacten: Christian Lademann, vice-voorzitter)\\
Internet Society Belgium\footnote{\url{http://www.isoc.be/}} (Contacten: Rudi Vansnick, voorzitter)\\
Internet Society Poland\footnote{\url{http://www.isoc.org.pl/}} (Contacten: Wladek Majewski)\\
LSVB\footnote{\url{http://www.lsvb.nl}} (Dutch National Student Union) (Contacten: Jurjen van den Bergh)\\
Patentverein.de\footnote{\url{http://www.patentverein.de/}} (defense association of german SMEs against trivial patents) (Contacten: Heiner Flocke)\\
LSVB (The Dutch National Student Union)\footnote{\url{http://www.lsvb.nl}} (Contacten: Jurjen van den Bergh)\\
Liberale Hochschulgruppe Ulm\footnote{\url{http://www.uni-ulm.de/lhg/}} (Contacten: Markus Schaber)\\
SSLUG.dk\footnote{\url{http://www.sslug.dk}} (Contacten: Erik Josefsson)\\
WINA-Leuven (Student Association of Students in Maths, Computer Sciences and Physics of the KU Leuven)\footnote{\url{http://www.wina.be/}} (Contacten: Dominique Devriese)\\
VVS (Union of Flemish Students)\footnote{\url{http://www.vvs.ac/}} (official standpoint\footnote{\url{http://www.vvs.ac/standpunt/pdf/2004_05_01_patenten_op_software_en_zakenmethodes.pdf}})\\
Zeus WPI (Student Workgroup on Informatics of Ghent University)\footnote{\url{http://zeus.ugent.be/}} (Contacten: Jonas Maebe)\\
Foundation for a Free Information Infrastructure\footnote{\url{http://www.ffii.org/index.nl.html}} (Contacten, Hartmut Pilch, Alex Macfie en Holger Blasum)\\
\dots (zie Ondertekenaars van de vorige versie\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/cpedu/index.en.html\#sign}}, wij zijn momenteel bezig een nieuwe lijst samen te stellen.)\\
Meer handtekeningen\footnote{\url{euparl-sign.html}} (29790 personen hebben tot nu toe deze oproep getekend via het FFII deelnemerssysteem\footnote{\url{http://aktiv.ffii.org/}}.)
\end{sect}

\begin{sect}{links}{koppelingen met voetnoten}
\begin{itemize}
\item
{\bf {\bf European Software Patents: Assorted Examples\footnote{\url{http://swpat.ffii.org/pikta/mupli/index.en.html}}}}

\begin{quote}
We came across the following impressive examples at first reading through our tabular listings of software patents.  They were almost randomly chosen and thus approximately represent the technicity and inventivity standards of the European Patent Office.  If anything distinguishes them from other patents it is the relative ease with which they can be understood at first sight by a fairly large public.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents in Action\footnote{\url{http://swpat.ffii.org/pikta/xrani/index.de.html}}}}

\begin{quote}
In recent years many disputes concerning software patents which have been publicized in the media.  Yet these were only the peak of the iceberg. Most of the developers and companys are confronted with patent claims  outside of courtrooms and maintaining silence is in the interest of both partys. Many projects are degraded or abandoned. It is difficult to document prevented innovation. Here we will undertake efforts for that attempt.
\end{quote}
\filbreak

\item
{\bf {\bf Quotations on Software Patents\footnote{\url{http://swpat.ffii.org/vreji/cusku/index.en.html}}}}

\begin{quote}
Salient quotations from law texts, economic analyses, political documents as well as statements by programmers, politicians and other parties interested in the debate about software patents.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{\url{http://swpat.ffii.org/stidi/manri/index.en.html}}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Geamendeerde softwareoctrooi-richtlijn\footnote{\url{http://swpat.ffii.org/papri/europarl0309/index.nl.html}}}}

\begin{quote}
Samengestelde versie van de geamendeerde richtlijn ``betreffende de octrooieerbaarheid van in computers ge\"{\i}mplementeerde uitvindingen'' waarvoor het Europees Parlement stemde op 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf Reactions to the EU Parliament's Vote of 2003/09/24\footnote{\url{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html}}}}

\begin{quote}
Frits Bolkestein, Malcolm Harbour and other politicians have joined in a choir of patent lawyers who are saying that ``the European Parliament ruined its chances of democratic participation'' by failing to vote as the patent establishment had asked it to vote.  Preparations are under way to withdraw the directive and legalise software patents by means of an agreement between national patent officials, who are used to deciding patent policies among themselves.
\end{quote}
\filbreak

\item
{\bf {\bf Werkgroep EU-Raad van Ministers 2004 Werkdocument Softwarepatenten\footnote{\url{http://swpat.ffii.org/papri/europarl0309/cons0401/index.nl.html}}}}

\begin{quote}
Het Ierse voorzitterschap van de EU-Raad heeft een document verspreid onder de verschillende ministeries, dat alternatieven suggereert voor de amendementen die het Europees Parlement (EP) gestemd heeft voor de richtlijn ``over de octrooieerbaarheid van in computers ge\"{\i}mplementeerde uitvindingen''. De versie van de Raad legt, in sterk contrast met de EP-versie, absoluut geen beperkingen op wat betreft octooieerbaarheid en het afdwingen van octrooien. Volgens de huidige versie zouden ``in computers ge\"{\i}mplementeerde'' algoritmen en methoden voor bedrijfsvoering uitvindingen worden in de zin van het octrooirecht, en de publicatie van een functionele beschrijving van een gepatenteerd idee zou een patentinbreuk vormen. Communicatieprotocols en gegevensformaten zouden kunnen gepatenteerd worden en zouden dan niet vrijelijk te gebruiken zijn, zelfs niet voor interoperabiliteitsdoeleinden. Deze implicaties zijn echter misschien niet onmiddellijk duidelijk voor de onbedachtzame lezer. Hier proberen wij de misleidende taal van het voorstel te ontcijferen en haar implicaties uit te leggen.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Softwarepatent Richtlijn Amendementen: Echte vs Valse Limitering\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/index.nl.html}}}}

\begin{quote}
Het Europees Parlement zal volgens de planning op 24 September over de Softwarepatent-richtlijn stemmen. De richtlijn zoals voorgesteld door de Europese Commissie vernietigt de basisstructuur van de huidige wet (Art. 52 van de Europese Patent Conventie) en vervangt het door de Trilaterale Standaard die uitgewerkt werd door de VS, Europese en Japanse Patentbureaus in 2000, volgens welke alle ``computer-ge\"{\i}mplementeerde'' probleemoplossingen patenteerbare uitvindingen zijn. Sommige lede van het Parlement hebben amendementen voorgesteld met als doel het striktere uitvindingsconcept van de Europese Patent Conventie te bewaren, terwijl anderen kiezen voor ongelimiteerde patenteerbaarheid volgens de Trilaterale Standaard, maar wel in een rethorische vorm van beperkingen. We proberen een vergelijkende analyse te maken van alle voorgestelde amendementen om beslissingmakers te helpen herkennen of ze stemmen voor echte of valse limitering van patenteerbaarheid.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Vragen, Wat moet er nog gebeuren, Hoe kun je helpen}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Help ons steunbetuigers vinden voor deze Oproep.}}

\begin{quote}
Contacteer cpedu-help@ffii.org voor informatie over hoe tewerk te gaan.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
% mode: latex ;
% End: ;

