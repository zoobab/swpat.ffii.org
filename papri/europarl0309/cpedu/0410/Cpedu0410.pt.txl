<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposta de medidas II

#descr: O Parlamento Europeu votou a favor de legislação que exclui,
efectivamente, a patenteabilidade de software e de métodos de negócio.
No entanto a legislação europeia de patentes está, na sua maior parte,
nas mãos de especialistas de patentes dos ministérios, alguns dos
quais têem defendido patenteabilidade sem limites ao longo dos últimos
anos. Esta situação exige uma atenção redobrada e acção decisiva por
parte dos parlamentares nacionais e cidadãos preocupados.

#Wce: Preocupa-nos que

#tef: O %(ep: Gabinete Europeu de Patentes) (GEP), em contradição com a
letra e o espírito da lei, registou dezenas de milhar de patentes
sobre regras de negócio e métodos computacionais, doravante designadas
por %(q:patentes de software)

#toi: A Comissão Europeia %(cp:propõs) legalisar estas patentes e torná-las
legais em toda a União Europeia. Ao fazê-lo, ignorou a vontade e as
razões fundamentado da maioria dos profissionais da informática,
empresas de software, cientistas informáticos e ecnomistas.

#clt: Os proponentes da proposta na Comissão, no %(sc:Conselho) e no
%(ep:Parlamento) tentaram %(dp:iludir) e %(ip:intimidar) o Parlamento
Europeu. Apresentaram a proposta como uma forma de excluir a
patenteabilidade de software e métodos de negócio e ameaçaram que o
Parlamento Europeu perderia a hipótese de participar no processo se se
atrevesse a modificar a proposta.

#qrW: Como o Parlamento Europeu se recusou a ser iludido ou intimidado,
vários profissionais de patentes em vários governos e organizações
estão a tentar usar o Conselho de Ministros da União Europeia para
anular o resultado da democracia parlamentar na União Europeia.

#Fea: Por estas razões recomendamos o seguinte:

#ltt: Apelamos ao Gabinete Europeu de Patentes bem como aos gabinetes
nacionais de patentes para que parem imediatamente de conceder
patentes sobre métodos de negócio e métodos de processamento de dados
seja sob que disfarce verbal for, e para aplicarem o %(ar:Artigo 52º
da Convenção Europeia de Patentes) correctamente, de acordo com
%(ee:os métodos convencionais de interpretação de leis).

#ivt: Requeremos aos membro do Conselho de Ministros da União Europeia para
evitarem qualquer contra-proposta à versão do Parlamento Europeu, a
não ser que tal proposta seja suportada por uma maioria parlamentar de
um país membro.

#fWl: Requeremos aos membros dos parlamentos nacionais para que formulem
políticas nacionais claras sobre os limites da patenteabilidade e a
garantirem que os seus representantes no Conselho Europeu as
implementaram fielmente.

#Dsl: Requeremos que todas as propostas legislativas, incluindo as que vêem
do Parlamento Europeu e dos estados membros, sejam %(ts:testadas
rigorosamente) em relação a um conjunto de pedidos de patentes de
forma a identificar se a proposta em causa leva aos resultados
desejados, sem deixar lugar a dúvidas nem a reinterpretações.

#Sno: Subscritores

#mandir: managing director

#proedr: chairman

#presid: president

#infprof: professor de informática

#sci: professor de engenharia electrónica

#serchist: investigador

#konsultist: consultor

#junmov: Junibevægelsen

#SPD: Sozialdemokratische Partei Deutschlands

#lsl: Leader parliamentary group of Flemish Social Democrats in Belgian
Parliament

#MSe: Media Policy Speaker of the Green Party

#Sna: Senador da Região de Girona para %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: Confédération Européenne des Associations de Petites et Moyennes
Entreprises

#aru: Walter Grupp

#DiW: Forbundet af IT-professionelle

#eei: Internet Branch of the French Socialist Party

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-fundador

#fed: founder and owner

#gbb: Nightlabs GmbH

#iDc: Director Adjunto

#smp: Professor for German and European Law

#cos: economista

#nmc: informático

#caliu: Catalan Linux Users

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Subscritores da versão anterior.

#Wta: estamos de momento a compilar uma nova lista

#rgr: Mais subscritores

#gcr: %(N) pessoas já assinaram esta Proposta de Medidas através do
%(ei:sistema de participação da FFII).

#gti: Ajude a angariar apoiantes para esta Proposta!

#tro: Contacte %(MT) para informações sobre como proceder.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

