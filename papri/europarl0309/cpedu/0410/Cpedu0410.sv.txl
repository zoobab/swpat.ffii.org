<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Handlingsförslag (II)

#descr: Europaparlamentet har röstat för en lagstiftning som verkningsfullt
undantar datorprogram och metoder för affärsverksamhet från det
patenterbara området. Likväl är europeisk lagstiftning fortfarande i
händerna på verkställande patentexperter som i många år förordat en
obegränsad utvidgning av det patenterbara området. Denna situation
erfordrar särskild uppmärksamhet och handfast ingripande av nationella
parlamentariker och berörda medborgare.

#Wce: Vi är oroade över att

#tef: %(ep:Europeiska patentverket) (EPO) har, i strid med lagens ord och
mening, beviljat tiotusentals patent på regler för beräkning med
konventionell databehandlingsutrustning, nedan benämnda
%(q:mjukvarupatent).

#toi: Europeiska kommissionen har %(cp:föreslagit) att legalisera dessa
patent och göra det möjligt att enhetligt upprätthålla dem i hela
Europa. Därigenom ignorerar kommissionen den uttryckta viljan och en
välgrundad argumentation hos den överväldigande majoriteten av
professionella programmerare, programvaruföretag, dataloger och
ekonomer.

#clt: Inflytelserika förespråkare har försökt %(dp:vilseleda) och
%(ip:skrämma) Europaparlamentet. De presenterade kommissionens förslag
som om undantog det datorprogram och metoder för affärsverksamhet från
det patenterbara området, och hotade med att Europaparlamentet skulle
förlora sin chans till deltagande om det röstade för en verklig
begränsning av det patenterbara området.

#qrW: Eftersom Europaparlamentet inte lät sig vilseledas eller skrämmas,
försöker nu inflytelserika patentexperter från olika regeringar och
organisationer använda sig av rådet för att åsidosätta den
parlamentariska demokratin inom EU.

#Fea: Därför rekommenderar vi följande åtgärder

#ltt: Vi uppmanar det europeiska patentverket och de nationella patentverken
att omedelbart upphöra bevilja patent på metoder för affärsverksamhet
och metoder för databehandling - oavsett hur de presenteras, samt
tillämpa %(ar:europeiska patentkonventionens artikel 52) på ett
korrekt sätt i enlighet med %(ee:konventionella metoder för
lagtolkning).

#ivt: Vi uppmanar ministerrådets medlemmar att avstå från att lägga
motförslag till Europaparlamentets förslag, om inte sådana
uttryckligen godkänts genom ett majoritetsbeslut i medlemslandets
parlament.

#fWl: Vi uppmanar nationella parlamentsledamöter att formulera tydliga
nationella riktlinjer för gränsdragandet kring det patenterbara
området, samt att försäkra sig om att deras regeringars representanter
i rådet troget följer dessa riktlinjer.

#Dsl: Vi kräver att alla lagförslag, inklusive de från Europaparlamentet och
medlemsländerna, grundligt prövas gentemot en %(ts:testserie) av
utvalda patentansökningar för att säkerställa att de bortom allt
tvivel leder till önskat resultat, och att de inte lämnar rum för
fortsatt vantolkning.

#Sno: Undertecknare

#mandir: managing director

#proedr: chairman

#presid: president

#infprof: professor of informatics

#sci: professor of electrical engineering

#serchist: researcher

#konsultist: consultant

#junmov: Junibevægelsen

#SPD: Sozialdemokratische Partei Deutschlands

#lsl: Leader parliamentary faction of Flemish Social Democrats in Belgian
Parliament

#MSe: Media Policy Speaker of the Green Party

#Sna: Senator of the Girona region for %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: Confédération Européenne des Associations de Petites et Moyennes
Entreprises

#aru: Walter Grupp

#DiW: Forbundet af IT-professionelle

#eei: Internet Branch of the French Socialist Party

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-founder

#fed: founder and owner

#gbb: Nightlabs GmbH

#iDc: Joint Director

#smp: Professor i Tysk och Europeisk lag

#cos: ekonom

#nmc: informatiker

#caliu: Catalan Linux User Group

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Undertecknare av föregående version.

#Wta: En ny lista håller på att sammanställas

#rgr: fler underskrfter

#gcr: %(N) personer har hittils skrivit på det här handlingsförslaget via
%(ps:FFIIs medverkanssystem).

#gti: Hjälp hitta personer som vill stödja det här förslaget

#tro: Skriv till %(MT) för information om hur du går tillväga.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

