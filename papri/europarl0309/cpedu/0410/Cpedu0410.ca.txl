<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Peticions d'acció II

#descr: El Parlament Europeu ha votat a favor d'una legislació que exclou, de
manera efectiva, els mètodes de negoci i programari del sistema de
patents. De tota manera, la legislació Europea sobre patents es troba
encara en mans dels experts ministerials en patents, molts del quals,
i per anys, han intentat aconseguir un marc jurisdiccional que
garanteixi una patentibilitat il·limitada. Aquesta situació exigeix
una atenció especial i una resolució clara per part dels diferents
parlaments nacionals, així com dels ciutadans dels diferents estats.

#Wce: Ens preocupa que

#tef: L'%(ep:Oficina Europea de Patents) (EPO), en contradicció al redactat
i esperit de la llei, ha garantit decenes de milers de patents en
mètodes i mecanismes per a la computació de sistemes convencionals de
processament de dades, anomenades també %(q:patents de programari).

#toi: La Comissió Europea ha %(cp:proposat) legalitzar i uniformitzar
aquestes patents per tot Europa. En fer això ha despreciat els
desitjos i els arguments de la majoria de la comunitat de
professionals del programari, empreses desenvolupadores de programari,
enginyers informàtics i economistes.

#clt: Importants desenvolupadors han intentat %(dp:enganyar) i
%(ip:intimidar) el Parlament Europeu. Han provat de presentar aquesta
proposta com un mitjà per excloure la patentabilitat dels mètodes de
negoci i programari i han amenaçat el Parlament Europeu amb una pèrdua
de poder si votaven a favor d'una limitació real de les patents.

#qrW: Des de l'emissió de la proposta del Parlament Europeu, els influents
professionals de patents de diversos governs i institucions han provat
d'utilitzar el Consell de Ministres de la Unió Europea per tal
d'avortar la proposta del Parlament. Atemptant d'aquesta manera contra
la democràcia a la Unió Europea.

#Fea: Per aquestes raons recomanem el següent:

#ltt: Demanem a l'Oficina Europea de Patents així com a les diferents
oficines nacionals de patents que aturi immediatament la concessió de
patents basades en mètodes de negoci i processament de dades de
qualsevol mena i que compleixi escrupulosament amb allò que estableix
l'%(ar:Article 52 de la Convenció Europea de Patents) seguint %(ee:de
manera estricta la interpretació de la llei).

#ivt: Demanem als membres del Consell de ministres de la Unió Europea que
s'abstinguin de donar suport a qualsevol mesura contraria a la
proposta del Parlament Europeu, a menys que aquestes mesures
provinguin de la majoria de membres dels respectius parlaments
nacionals.

#fWl: Demanem als membres dels diferents parlaments nacionals, que formulin
polítiques que marquin clarament els límits del que és i no és
patentable i que s'assegurin que els seus representats al Consell de
la Unió Europea facin tot el possible per dur a terme aquestes
normatives.

#Dsl: Demanem que totes les propostes legislatives, incloses aquelles del
mateix Parlament Europeu i la resta d'estats membres siguin
rigorosament testades contra la següent %(ts:bateria) d'exemples
d'aplicació de patents per tal de veure sense cap mena de dubte que
n'obtenim els resultats desitjats i que no donen marge a qualsevol
mena de confusió.

#Sno: Signatures

#mandir: mànager

#proedr: chairman

#presid: president

#infprof: professor of informatics

#sci: professor of electrical engineering

#serchist: researcher

#konsultist: consultant

#junmov: Junibevægelsen

#SPD: Sozialdemokratische Partei Deutschlands

#lsl: Líder del grup Social Demòcrata Flamenc al parlament Belga

#MSe: Media Policy Speaker of the Green Party

#Sna: Parlamentari per Girona de %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: European Confederation of Associations of Small and Medium Enterprises

#aru: Walter Grupp

#DiW: Forbundet af IT-professionelle

#eei: Internet Branch of the French Socialist Party

#dat: Cap del departament jurídic

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-fundador

#fed: fundador i propietari

#gbb: Nightlabs GmbH

#iDc: Director

#smp: Professor for German and European Law

#cos: economista

#nmc: informàtic

#caliu: Catalan GNU/Linux Users

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Signatures de la versió anterior.

#Wta: Actualment estem creant una nova llista

#rgr: més signatures

#gcr: %(N) han signat aquest document usant %(ps:el sistema de votació de la
FFII).

#gti: Ajuda'ns a guanyar suport per aquesta crida!

#tro: Contacte amb %(MT) si vols informació sobre com actuar.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

