<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Appel à l'action II

#descr: La proposition de directive de la Commission Européenne sur la
Brevetabilité des Innovations Informatiques nécessite une réponse du
Parlement Européen, des gouvernements des États membres et autres
acteurs politiques. Voici nos propositions.

#Wce: Nous sommes préoccupés du fait que :

#tef: L'%(ep:Office Européen des Brevets) %(pe:OEB) a, en contradiction  
avec la lettre et l'esprit de la loi, accordé des dizaines de milliers
  de brevets sur des règles de calcul avec machine conventionnelle de 
traitement de données, ci-après désignés sous le  terme de %(q:brevets
 logiciels).

#toi: La Commission Européenne a %(cp:proposé) de considérer que ces 
brevets sont valides et de les rendre uniformément applicables dans 
toute l'Europe. Ce faisant, elle est  passée outre à la volonté 
manifeste et aux arguments solides de la  grande majorité des 
concepteurs de logiciels, SSII, chercheurs en  informatique et 
économistes.

#clt: D'éminents partisans de la Proposition ont tenté de %(dp:tromper) et
d'%(ip:intimider) le Parlement Européen. Ils ont présenté la
Proposition comme un moyen d'exclure du champ de la brevetabilité les
logiciels et les méthodes d'affaires et ont averti le Parlement
Européen qu'il %(q:perdrait toute chance de participation
démocratique) s'il votait pour une réelle limitation de la
brevetabilité.

#qrW: Comme le Parlement Européen a refusé de se laisser tromper ou
intimider, des experts influents en matière de brevets auprès de
différents gouvernements et organisations tentent maintenant
d'utiliser le Conseil des Ministres de l'Union Européenne pour se
soustraire àla démocratie parlementaire au sein de l'Union.

#Fea: C'est pourquoi nous recommandons les mesures suivantes :

#ltt: Nous appelons l'Office Européen des Brevets ainsi que les offices 
nationaux de brevets à arrêter immédiatement d'accorder des brevets
sur  des méthodes économiques et des méthodes de traitement de
données, quel  qu'en soit l'habillage, et d'appliquer correctement
l'%(ar:Article 52  de la Convention Européenne sur les Brevets)
suivant les %(ee:méthodes  conventionnelles d'interprétation des
lois).

#ivt: Nous appelons les Membres du Conseil des Ministres de l'Union  
Européenne à s'abstenir de présenter toute contre-proposition à la  
version du projet du Parlement Européen, à moins que cette  
contre-proposition n'ait été soutenue par un vote à la majorité du  
Parlement de l'État membre concerné.

#fWl: Nous appelons les membres des parlements nationaux à formuler une 
politique nationale claire sur les limites de la brevetabilité et à 
s'assurer que les représentants de leurs gouvernements mettent 
fidèlement en oeuvre cette politique.

#Dsl: Nous demandons que tous les projets de lois, y compris ceux du
Parlement Européen et des États membres, soient soumis  à  une
%(ts:batterie de tests) sur un jeu de dépôts de brevets, pour  
vérifier s'ils conduisent, sans aucun doute possible, aux résultats  
voulus et ne laissent aucune place à une interprétation erronée.

#Sno: Signatures

#lsl: Président du groupe des socialdémocrates flamands dans le parlament
belge

#MSe: Porte-Parole Média et Informatique du Parti Vert

#Sna: sénateur pour la région Girona de l'%(ECP)

#eal: Membre du Parlament Bavarois

#lWt: Sénateur Belge

#ian: Confédération Européenne des Associations de Petites et Moyennes
Entreprises

#aru: Walter Grupp

#DiW: Association Danoise des Professionels de l'Informatique

#eei: Section Internet du Parti Socialiste Français

#dat: Département Juridique

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-founder

#fed: fondateur et propriétaire

#gbb: Nightlabs GmbH

#iDc: Joint Director

#smp: professeur du droit allemand et européen

#cos: economist

#nmc: informatician

#caliu: Association des Utilisateurs de GNU/Linux en Langue Catalonienne

#hlW: Dutch National Student Union

#aav: association de PMEs allemandes pour la défense contre brevets triviaux

#seW: Signataires de la version précédente.

#Wta: Nous sommes en train d'établir une nouvelle liste

#rgr: Autres signataires

#gcr: %(N) personnes ont d'ores et déjà signé cet appel via le  
%(ps:système de participation de la FFII).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: pelegrin ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0410 ;
# txtlang: xx ;
# End: ;

