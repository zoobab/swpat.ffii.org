<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Call for Action II

#descr: The European Parliament has voted for legislation that would
effectively exclude software and business methods from patentability. 
However European patent legislation is still largely in the hands of
ministerial patent experts, many of whom have for years been pushing
for unlimited patentability.  This situation calls for close attention
and resolute action by national parliamentarians and concerned
citizens.

#Wce: We are concerned that

#Fea: For these reasons we recommend the following:

#Sno: Signatories

#tef: The %(ep:European Patent Office) (EPO) has, in contradiction to the
letter and spirit of the written law, granted tens of thousands of
patents on rules for computing with conventional data processing
equipment, below termed %(q:software patents).

#toi: The European Comission has %(cp:proposed) to legalise these patents
and make them uniformly enforcable in Europe.  In doing so, it has
disregarded the manifest will and well-argued reasoning of the vast
majority of software professionals, software companies, computer
scientists and economists.

#clt: Prominent proponents have tried to %(dp:deceive) and %(ip:intimidate)
the European Parliament.  They have presented the proposal as a means
of excluding software and business methods from patentability and have
threatened that the European Parliament would lose its chance of
participation if it voted for a real limitation of patentability.

#qrW: Since the European Parliament has refused to be deceived or
intimidated, influential patent professionals in various governments
and organisations are now trying to use the EU Council of Ministers in
order to sidestep parliamentary democracy in the European Union.

#ltt: We urge the European Patent Office as well as national patent offices
to immediately stop granting patents on business methods and data
processing methods in whatever verbal clothing, and to apply
%(ar:Article 52 of the European Patent Convention) correctly according
to %(ee:conventional methods of interpretation of law).

#ivt: We urge the members of the EU Council of Ministers to refrain from any
counter-proposals to the European Parliament's version of the draft,
unless such counter-proposals have been explicitely endorsed by a
majority decision of the member's national parliament.

#fWl: We urge members of national parliaments to formulate clear national
policies on the limits of patentability and to make sure that their
government's representatives in the European Council are faithfully
implementing these policies.

#Dsl: We demand that all legislative proposals, including those from the
European Parliament and the member states, be rigorously tested
against a %(ts:test suite) of sample patent applications to see
whether they would beyond any doubt lead to the desired results and
would not leave room for any more misinterpretations.

#mandir: managing director

#proedr: chairman

#presid: president

#infprof: professor of informatics

#sci: professor of electrical engineering

#serchist: researcher

#konsultist: consultant

#junmov: June Movement

#SPD: German Social Democratic Party

#lsl: Leader parliamentary faction of Flemish Social Democrats in Belgian
Parliament

#MSe: Media Policy Speaker of the Green Party

#Sna: Senator of the Girona region for %(ECP)

#eal: member of Bavarian Parliament

#lWt: Belgian Senator

#ian: European Confederation of Associations of Small and Medium Enterprises

#aru: Walter Grupp

#DiW: Danish Association of IT Professionals

#eei: Internet Branch of the French Socialist Party

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#Odo: CTO, VP & co-founder

#fed: founder and owner

#gbb: Nightlabs GmbH

#iDc: Joint Director

#smp: Professor for German and European Law

#cos: economist

#nmc: informatician

#caliu: Catalan Linux User Group

#hlW: Dutch National Student Union

#aav: defense association of german SMEs against trivial patents

#seW: Signators of the previous version.

#Wta: We are currently putting together a new list

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#gti: Help gain supporters for this Call!

#tro: Contact %(MT) for information on how to procede.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0310 ;
# txtlang: xx ;
# End: ;

