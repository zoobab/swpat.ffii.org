\begin{subdocument}{europarl-cpedu}{Peticions d'acci\'{o} II}{http://swpat.ffii.org/papri/europarl0309/cpedu/europarl-cpedu.ca.html}{Grup de treball\\swpatag@ffii.org\\versi\'{o} catalana 2004/01/14 per Francesc Genove MuÃ±oz\footnote{http://lists.ffii.org/mailman/listinfo/traduk}}{El Parlament Europeu ha votat a favor d'una legislaci\'{o} que exclou, de manera efectiva, els m\`{e}todes de negoci i programari del sistema de patents. De tota manera, la legislaci\'{o} Europea sobre patents es troba encara en mans dels experts ministerials en patents, molts del quals, i per anys, han intentat aconseguir un marc jurisdiccional que garanteixi una patentibilitat il$\cdot$limitada. Aquesta situaci\'{o} exigeix una atenci\'{o} especial i una resoluci\'{o} clara per part dels diferents parlaments nacionals, aix\'{\i} com dels ciutadans dels diferents estats.}
\begin{sect}{prob}{Ens preocupa que}
\begin{enumerate}
\item
L'Oficina Europea de Patents\footnote{http://swpat.ffii.org/gasnu/epo/swpatepo.en.html} (EPO), en contradicci\'{o} al redactat i esperit de la llei, ha garantit decenes de milers de patents en m\`{e}todes i mecanismes per a la computaci\'{o} de sistemes convencionals de processament de dades, anomenades tamb\'{e} ``patents de programari''.

\item
La Comissi\'{o} Europea ha proposat legalitzar i uniformitzar aquestes patents per tot Europa. En fer aix\`{o} ha despreciat els desitjos i els arguments de la majoria de la comunitat de professionals del programari, empreses desenvolupadores de programari, enginyers inform\`{a}tics i economistes.

\item
Importants desenvolupadors han intentat enganyar\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/eubsa-tech.en.html\#cecjuri} i intimidar\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/plendeba0309.en.html\#bolk} el Parlament Europeu. Han provat de presentar aquesta proposta com un mitj\`{a} per excloure la patentabilitat dels m\`{e}todes de negoci i programari i han amena\c{c}at el Parlament Europeu amb una p\`{e}rdua de poder si votaven a favor d'una limitaci\'{o} real de les patents.

\item
Des de l'emissi\'{o} de la proposta del Parlament Europeu, els influents professionals de patents de diversos governs i institucions han provat d'utilitzar el Consell de Ministres de la Uni\'{o} Europea per tal d'avortar la proposta del Parlament. Atemptant d'aquesta manera contra la democr\`{a}cia a la Uni\'{o} Europea.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{Per aquestes raons recomanem el seg\"{u}ent:}
\begin{enumerate}
\item
Demanem a l'Oficina Europea de Patents aix\'{\i} com a les diferents oficines nacionals de patents que aturi immediatament la concessi\'{o} de patents basades en m\`{e}todes de negoci i processament de dades de qualsevol mena i que compleixi escrupulosament amb all\`{o} que estableix l'Article 52 de la Convenci\'{o} Europea de Patents\footnote{http://swpat.ffii.org/stidi/cpe52/epue52.ca.html} seguint de manera estricta la interpretaci\'{o} de la llei\footnote{http://swpat.ffii.org/stidi/cpe52/exeg/epue52exeg.de.html}.

\item
Demanem als membres del Consell de ministres de la Uni\'{o} Europea que s'abstinguin de donar suport a qualsevol mesura contraria a la proposta del Parlament Europeu, a menys que aquestes mesures provinguin de la majoria de membres dels respectius parlaments nacionals.

\item
Demanem als membres dels diferents parlaments nacionals, que formulin pol\'{\i}tiques que marquin clarament els l\'{\i}mits del que \'{e}s i no \'{e}s patentable i que s'assegurin que els seus representats al Consell de la Uni\'{o} Europea facin tot el possible per dur a terme aquestes normatives.

\item
Demanem que totes les propostes legislatives, incloses aquelles del mateix Parlament Europeu i la resta d'estats membres siguin rigorosament testades contra la seg\"{u}ent bateria\footnote{http://swpat.ffii.org/stidi/testsuite/swpatmanri.ca.html} d'exemples d'aplicaci\'{o} de patents per tal de veure sense cap mena de dubte que n'obtenim els resultats desitjats i que no donen marge a qualsevol mena de confusi\'{o}.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Signatures}
\begin{center}
Veieu http://aktiv.ffii.org/euparl/ca
\end{center}

Bent Hindrup Andersen\footnote{http://www.hindrup.dk} (MPE, EDD, Dinamarca, Junibev\ae{}gelsen\footnote{http://www.j.dk})\\
Jens-Peter Bonde\footnote{http://www.bonde.com/} (MPE, EDD (chairman), Dinamarca, Junibev\ae{}gelsen\footnote{http://www.j.dk})\\
Johanna Boogerd\footnote{http://www.johannaboogerd.nl/} (MPE, ELDR, Holanda)\\
Hiltrud Breyer\footnote{http://www.hiltrud-breyer.de} (MPE, VERD, Alemanya)\\
Daniel Cohn-Bendit\footnote{http://www.cohn-bendit.com/} (MPE, VERD, Fran\c{c}a)\\
Jan Dhaene\footnote{http://www.jandhaene.be} (MPE, B\`{e}lgica)\\
Raina Mercedes Echerer\footnote{http://www.mercedes-echerer.at/} (MPE, VERD, \`{A}ustria)\\
Pernille Frahm\footnote{http://www.frahm.dk/} (MPE, GUE/NGL, Dinamarca)\\
Pierre Jonckheer\footnote{http://www.pierrejonckheer.be/} (MPE, VERD, B\`{e}lgica)\\
Piia-Noora Kauppi\footnote{http://www.kauppi.net/} (MPE, PPE-DE, Finl\`{a}ndia)\\
Wolfgang Kreissl-Dörfler\footnote{http://www.kreissl-doerfler.de/} (MPE, PSE, Alemanya)\\
Jean Lambert\footnote{http://http://www.jeanlambertmep.org.uk/} (MPE, VERD, Regne Unit)\\
Donald Neil (Professor) MacCormick\footnote{http://wwwdb.europarl.eu.int/ep5/owa/whos\_mep.data?ipid=0\&ilg=EN\&iucd=4548\&ipolgrp=.\&ictry=GB\&itempl=\&ireturn=\&imode=} (MPE, VERD, Regne Unit)\\
Caroline Lucas\footnote{http://www.carolinelucasmep.org.uk/} (MPE, VERD, Regne Unit)\\
Heide Rühle\footnote{http://www.heide-ruehle.de} (MPE, VERD, Alemanya)\\
Ulla Sandbæk\footnote{http://www.ullasandbaek.dk/} (MPE, EDD, Dinamarca, Junibev\ae{}gelsen\footnote{http://www.j.dk})\\
Olle Schmidt\footnote{http://www.olles.nu/} (MPE, ELDR, Su\`{e}cia)\\
Bart Staes\footnote{http://www.bartstaes.be/} (MPE, VERD, B\`{e}lgica, Groen!\footnote{http://www.groen.be})\\
Claude Turmes\footnote{http://www.greng.lu/site/index.php?ref3=111\&ref2=7\&navtype=3\&typebloc=2} (MPE, VERD, Luxemburg, DÉI GRÉNG\footnote{http://www.greng.lu})\\
Anders Wijkman\footnote{http://www.wijkman.kristdemokrat.se/default.asp} (MPE, PPE-DE, Su\`{e}cia)\\
Matti Wuori\footnote{http://www.mattiwuori.net/} (MPE, VERD, Finl\`{a}ndia)\\
Olga Zrihen\footnote{http://www.dpsb.be/oz/oz.html} (MPE, PSE, B\`{e}lgica, PS\footnote{http://www.ps.be})\\
Jens Holm\footnote{http://www.jensholm.se/} (MEP candidate, Su\`{e}cia)\\
Ellen Trane Nørby\footnote{http://www.ellenieu.dk/} (MEP candidate, Dinamarca)\\
Dirk Van Der Maelen (L\'{\i}der del grup Social Dem\`{o}crata Flamenc al parlament Belga, B\`{e}lgica, SP.be\footnote{http://www.sp.be})\\
Grietje Bettin\footnote{http://www.sh.gruene.de:8080/MdB/Bettin/ein\_text?datum2=2002/05/13\percent{}2020\percent{}3A00\percent{}3A19\percent{}20GMT\percent{}2B2} (Membre del Parlament Federal Alemany, Media Policy Speaker of the Green Party)\\
Gustav Fridolin\footnote{http://www.mp.se/gustavfridolin.asp} (Membre del Parlament, Su\`{e}cia)\\
Zo\'{e} Genot (Membre del Parlament, B\`{e}lgica, ecolo.be\footnote{http://www.ecolo.be/})\\
Arseni Gibert (Parlamentari per Girona de Entesa Catalana de Progrés\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})\\
Ulrich Kelber\footnote{http://www.ulrich-kelber.de/} (Membre del Parlament Federal Alemany (Sozialdemokratische Partei Deutschlands\footnote{http://www.spd.de/}))\\
Monica Lochner-Fischer\footnote{http://www.lochner-fischer.de/} (member of Bavarian Parliament, SPD)\\
Dr. Jan Van Duppen\footnote{http://www.janvanduppen.be} (Belgian Senator, Member of the Flemish Parliament, SP.a spirit\footnote{http://www.sp.be/})\\
CEA/PME\footnote{http://www.ceapme.org} (Conf\'{e}d\'{e}ration Europ\'{e}enne des Associations de Petites et Moyennes Entreprises) (contacte: Walter Grupp)\\
Forbundet af IT-professionelle\footnote{http://www.prosa.dk/} (PROSA.dk) (contacte: Peter Ussing)\\
VOV@SPD\footnote{http://www.vov.de/} (Internet Association of the German Social Democratic Party) (contacte: Axel Schudak, Arne Brand i Boris Piwinger)\\
temPS r\'{e}els\footnote{http://www.temps-reels.net} (Internet Branch of the French Socialist Party) (contacte: Thierry Noisette)\\
Opera Software\footnote{http://www.opera.com/} (contacte: H\aa{}kon Wium Lie, CTO)\\
Magix AG\footnote{http://www.magix.com/} (contacte: Oliver Lorenz, Cap del departament jur\'{\i}dic)\\
ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat} (contacte: Stefan Pollmeier, Director Administratiu)\\
FSG-IT\footnote{http://www.fsg-it.de} (contacte: Lars Noschinski, Developer)\\
ObjectWeb\footnote{http://wiki.objectweb.org/Wiki.jsp?page=CWP\_SoftwarePatents\_French} (contacte: Francois Letellier)\\
TFDesign s.r.o.\footnote{http://www.tfdesign.cz} (contacte: Michal Zajic, Owner, Head Designer - civil engineering)\\
ashampoo Technology GmbH \& Co. KG\footnote{http://tech.ashampoo.com/} (contacte: Hauke Duden)\\
Intelligent Firmware Ltd\footnote{http://www.intelligentfirm.co.uk/} (contacte: Michael Krech, Director Administratiu)\\
PETE SOFTWARE GmbH\footnote{http://www.pete-software.de/} (contacte: Thies Reinhold, CEO)\\
Phaidros AG\footnote{http://www.phaidros.com/german/phaidros/engagement/standpunkt\_patente.htm} (contacte: Matthias Schlegel, CEO)\\
Power Media Sp. z o.o.\footnote{http://www.power.com.pl/} (contacte: Narczynski, Wojtek Jakub, CTO, VP \& co-fundador)\\
Myriad Software\footnote{http://www.myriad-online.com/} (contacte: Guillion Didier, m\`{a}nager)\\
MySQL\footnote{http://www.mysql.com/} (contacte: Michael Widenius, Florian Mueller)\\
NEOlabs\footnote{http://www.neolabs.be/} (contacte: Jo De Baer, consultant)\\
Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, fundador i propietari)\\
Ulf Dunkel (invers Software Vertrieb\footnote{http://www.calamus.net/} i Dunkel Software Distribution\footnote{http://www.dsd.net/}, Director Administratiu)\\
Mind\footnote{http://www.mind.be/} (contacte: Dr. ir. Peter Vandenabeele, Director Administratiu)\\
Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Director Administratiu)\\
Intevation GmbH\footnote{http://www.intevation.de/} (contacte: Bernhard Reiter i Jan-Oliver Wagner)\\
RDG Software di 'Roberto Della Grotta'\footnote{http://www.rdg-software.it/} (contacte: Roberto Della Grotta)\\
S.K.I. GmbH\footnote{http://www.ski-gmbh.com/} (contacte: Michael Schlegel)\\
Bureau d'Etudes en Génie Informatique, Hasgard\footnote{http://www.hasgard.net/} (contacte: Bruno Berthelet)\\
Nexedi\footnote{http://www.nexedi.com/} (contacte: Jean-Paul Smets)\\
TRI-EDRE and TRI-EDRE Developments\footnote{http://www.tri-edre.fr/} (contacte: Thierry Rolland, m\`{a}nager)\\
Stephan K\"{o}rner (Director Administratiu, Pilum Technology GmbH)\\
Marco Schulze (Director Administratiu, Nightlabs GmbH\footnote{http://www.nightlabs.com/})\\
Python Business Forum\footnote{http://www.python-in-business.org/} (contacte: Jacob Hall\'{e}n, president)\\
National Computer Helpdesk\footnote{http://NationalComputerHelpdesk.net/} (contacte: John Harris - Manager Support Services, Christopher Thompson - Director)\\
ROHOST\footnote{http://www.rohost.com/} (contacte: Marius David)\\
AB STRAKT\footnote{http://www.strakt.com/} (contacte: Laura Creighton, Venture Capitalist)\\
Prof. Martin Kretschmer (Centre for Intellectual Property Policy \& Management\footnote{http://www.cippm.org.uk/}, Director)\\
ROSPOT\footnote{http://www.rospot.com/} (contacte: Cosmin Neagu)\\
Dr. Karl-Friedrich Lenz\footnote{http://k.lenz.name/LB/archives/000617.html\#000617} (Professor for German and European Law, Aoyama Gakuin Daigaku, Jap\'{o})\\
Dr. Jean-Philippe Rennard (economista, inform\`{a}tic, Director Administratiu, Eponyme SA\footnote{http://www.eponyme.com/})\\
Anders Skovsted Buch (Lector in mathematics, Universitat of Aarhus, Dinamarca)\\
Prof. Dr. Koen De Bosschere (professor of informatics, Universitat of Ghent, B\`{e}lgica)\\
Prof. Dr. Wilfried Philips (professor of informatics, Universitat of Ghent, B\`{e}lgica)\\
Dr. Michiel Ronsse (researcher, Universitat of Ghent)\\
Student Union Akku (University of Nijmegen, The Netherlands)\footnote{http://www.kun.nl/akku/} (contacte: Dieter Van Uytvanck)\\
Institut Biotecnologia i Biomedicina (Universitat Aut\'{o}noma Barcelona, Spain)\footnote{http://www.uab.es/investigacio/ibf.htm} (contacte: Dr. Enrique Querol, Director)\\
\"{O}sterreichische Hochsch\"{u}lerInnenschaft -- Bundesvertretung\footnote{http://www.oeh.ac.at/} (contacte: Leonard Dobusch)\\
ANSOL - Associa\c{c}\~{a}o Nacional para o Software Livre\footnote{http://www.ansol.org/} (contacte: Jaime Villate, President)\\
APRIL\footnote{http://www.april.org/} (contacte: Olivier Berger)\\
Catalan GNU/Linux Users\footnote{http://patents.caliu.info/} (CALIU)\\
Electronic Frontier Finnand\footnote{http://www.effi.org/patentit/index.en.html} (contacte: Ville Oksanen, Tapani Tarvainen)\\
Europe Shareware\footnote{http://www.europe-shareware.org/} (contacte: Pascal Ricard i Sylvain Perchaud)\\
Association for Free Software\footnote{http://www.affs.org.uk/} (contacte: Marc Eberhard)\\
Association of Free Software Professionals\footnote{http://www.afsp.org.uk/} (contacte: Neil Darlow)\\
Bürgernetzverband e.V.\footnote{http://www.buerger.net/} (contacte: Eberhard Mittag)\\
Free Software Foundation Europe\footnote{http://www.fsfeurope.org/} (contacte: Georg Greve, president)\\
FSF France\footnote{http://www.fsffrance.org/} (contacte: Fr\'{e}d\'{e}ric Couchet)\\
German Unix User Group\footnote{http://www.guug.de/} (GUUG.de, contacte: Christian Lademann, vicepresident)\\
Internet Society Belgium\footnote{http://www.isoc.be/} (contacte: Rudi Vansnick, president)\\
Internet Society Poland\footnote{http://www.isoc.org.pl/} (contacte: Wladek Majewski)\\
LSVB\footnote{http://www.lsvb.nl} (Dutch National Student Union) (contacte: Jurjen van den Bergh)\\
Patentverein.de\footnote{http://www.patentverein.de/} (defense association of german SMEs against trivial patents) (contacte: Heiner Flocke)\\
LSVB (The Dutch National Student Union)\footnote{http://www.lsvb.nl} (contacte: Jurjen van den Bergh)\\
Liberale Hochschulgruppe Ulm\footnote{http://www.uni-ulm.de/lhg/} (contacte: Markus Schaber)\\
SSLUG.dk\footnote{http://www.sslug.dk} (contacte: Erik Josefsson)\\
WINA-Leuven (Student Association of Students in Maths, Computer Sciences and Physics of the KU Leuven)\footnote{http://www.wina.be/} (contacte: Dominique Devriese)\\
VVS (Union of Flemish Students)\footnote{http://www.vvs.ac/} (official standpoint\footnote{http://www.vvs.ac/standpunt/pdf/2004\_05\_01\_patenten\_op\_software\_en\_zakenmethodes.pdf})\\
Zeus WPI (Student Workgroup on Informatics of Ghent University)\footnote{http://zeus.ugent.be/} (contacte: Jonas Maebe)\\
Foundation for a Free Information Infrastructure\footnote{http://www.ffii.org/ffii.en.html} (contacte, Hartmut Pilch, Alex Macfie i Holger Blasum)\\
\dots (veieu Signatures de la versi\'{o} anterior.\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/peticions/eubsa-cpedu.ca.html\#sign}, Actualment estem creant una nova llista)\\
m\'{e}s signatures\footnote{euparl-sign.html} (16482 han signat aquest document usant el sistema de votaci\'{o} de la FFII.)
\end{sect}

\begin{sect}{links}{Enlla\c{c}os anotats}
\begin{itemize}
\item
{\bf {\bf Patents Europees de Programari: Exemples Escollits\footnote{http://swpat.ffii.org/pikta/mupli/swpikmupli.ca.html}}}

\begin{quote}
Vam arribar als seg\"{u}ents impressionants exemples mitjan\c{c}ant la primera lectura de les nostres llistes tabulades de patents de programari.  Estan escollides gaireb\'{e} a l'atzar i representen aproximadament l'est\`{a}ndar de t\'{e}cnica i inventiva de l'Oficina Europea de Patents (EPO).  Si alguna cosa les distingueix d'altres patents \'{e}s la relativa facilitat d'entendre-les a primera vista per una gran quantitat de gent.
\end{quote}
\filbreak

\item
{\bf {\bf Patents de Programari en Acci\'{o}\footnote{http://swpat.ffii.org/pikta/xrani/swpikxrani.ca.html}}}

\begin{quote}
Aquests \'{u}ltims anys, m\'{e}s i m\'{e}s casos de litigi de patents de programari es coneixen pels mitjans de comunicaci\'{o}.  Per\`{o} aix\`{o} \'{e}s nom\'{e}s la punta de l'iceberg.  La major part de les empreses de programari i els desenvolupadors de programari s\'{o}n amena\c{c}ats fora dels jutjats i el silenci \'{e}s part obligat\`{o}ria d'aquest \emph{arranjament fora del jutjat}.  Molts projectes s'aturen o no comencen perqu\`{e} el camp est\`{a} abarrotat de patents.  \'{E}s dif\'{\i}cil documentar camins de desenvolupament bloquejats.  Aqu\'{\i} intentarem fer el millor que puguem.
\end{quote}
\filbreak

\item
{\bf {\bf Quotations on Software Patents\footnote{http://swpat.ffii.org/vreji/cusku/swpatcusku.en.html}}}

\begin{quote}
Salient quotations from law texts, economic analyses, political documents as well as statements by programmers, politicians and other parties interested in the debate about software patents.
\end{quote}
\filbreak

\item
{\bf {\bf Banc de proves de s\`{e}ries d'assaigs de legislaci\'{o} de patentabilitat\footnote{http://swpat.ffii.org/stidi/testsuite/swpatmanri.ca.html}}}

\begin{quote}
Per tal d'assajar una proposta de llei, la provem amb un conjunt de mostres d'innovacions.  Per cada innovaci\'{o}, donem una descripci\'{o} de l'art previ, una descripci\'{o} de l'invent declarat i un petit conjunt de reivindicacions.  Donat que les descripcions s\'{o}n correctes, assagem la nostra legislaci\'{o} proposada en ell.  El focus est\`{a} en la claredat i l'adequaci\'{o}:  condueix la regla proposada cap a un veredicte predecible?  Quina de les reivindicacions acceptarem, si n'acceptem alguna?  Aquest resultat \'{e}s el que vol\'{\i}em?  Provem d'altres propostes de llei amb la mateixa s\`{e}rie d'assaigs i veiem quina obt\'{e} la millor puntuaci\'{o}.  Els professionals del programari creuen que hauries de ``primer fixar les errades, despr\'{e}s alliberar el codi''.  Les s\`{e}ries d'assaigs s\'{o}n una manera habitual d'aconseguir-ho.  La legislaci\'{o} \'{e}s un camp de la tecnologia anomenat ``enginyeria social'', oi?  Tecnologia o no, ja \'{e}s hora d'encarar la legislaci\'{o} amb el mateix rigor metodol\`{o}gic aplicable on sigui que decisions mal dissenyades puguin afectar significantment la vida de la gent.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{http://swpat.ffii.org/papri/europarl0309/europarl0309.en.html}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/europarl-reag0309.en.html}}}

\begin{quote}
Frits Bolkestein, Malcolm Harbour and other politicians have joined in a choir of patent lawyers who are saying that ``the European Parliament ruined its chances of democratic participation'' by failing to vote as the patent establishment had asked it to vote.  Preparations are under way to withdraw the directive and legalise software patents by means of an agreement between national patent officials, who are used to deciding patent policies among themselves.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council 2004/01/29 ``Presidency Compromise Proposal'' on Software Patents\footnote{http://swpat.ffii.org/papri/europarl0309/cons0401/cons0401.de.html}}}

\begin{quote}
The Irish EU Council presidency has circulated a paper among governental ministries which contains alternative suggestions to the amendments on the directive ``on the patentability of computer-implemented inventions'' passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, ``computer-implemented'' algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/plen0309.en.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Preguntes, Coses a fer, Com podeu ajudar}
\begin{itemize}
\item
{\bf {\bf How you can help us stop Software Patents\footnote{http://swpat.ffii.org/girzu/gunka/swpatgunka.en.html}}}
\filbreak

\item
{\bf {\bf Ajuda'ns a guanyar suport per aquesta crida!}}

\begin{quote}
Contacte amb cpedu-help@ffii.org si vols informaci\'{o} sobre com actuar.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
% mode: latex ;
% End: ;

