\begin{subdocument}{europarl-cpedu}{Aufruf zum Handeln II}{http://swpat.ffii.org/papiere/europarl0309/appell/europarl-cpedu.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/10/22 von Christian Cornelssen\footnote{http://www.cs.tu-berlin.de/~ccorn}}{Das Europ\"{a}ische Parlament hat f\"{u}r einen Gesetzentwurf gestimmt, der Programm- und Gesch\"{a}ftslogik wirklich von der Patentierbarkeit ausschlie{\ss}en w\"{u}rde.  Die Europ\"{a}ische Patentgesetzgebung liegt jedoch nach wie vor weitgehend in den H\"{a}nden ministerieller Patentexperten, von denen viele seit Jahren auf grenzenlose Patentierbarkeit hinwirken.  Diese Situation erfordert genaue Aufmerksamkeit und entschlossenes Handeln seitens nationaler Parlamentarier und betroffener B\"{u}rger.}
\begin{sect}{prob}{Folgendes bereitet uns Sorge}
\begin{enumerate}
\item
Das Europ\"{a}ische Patenamt (EPA) hat im Widerspruch zum Buchstaben und Geist des geltenden Gesetzes zehntausende Patente auf Regeln zum Rechnen mit herk\"{o}mmlichen Datenverarbeitungsanlagen erteilt, die wir im folgenden ``Softwarepatente'' nennen.

\item
Die Europ\"{a}ische Kommission hat vorgeschlagen, diese Patente als rechtm\"{a}{\ss}ig anzuerkennen und in ganz Europa einheitlich durchsetzbar zu machen.  Sie hat dabei den ausdr\"{u}cklichen Willen und die wohlbegr\"{u}ndeten Argumente der gro{\ss}en Mehrheit von Software-Fachleuten, Software-Firmen, Informatikern und Wirtschaftswissenschaftlern missachtet.

\item
Prominente Bef\"{u}rworter des Vorschlages haben versucht, das Europ\"{a}ische Parlament zu t\"{a}uschen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/eubsa-tech.en.html\#cecjuri} und einzusch\"{u}chtern\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/plen0309/deba/plendeba0309.en.html\#bolk}. Sie haben den Vorschlag als Mittel dargestellt, Software und Gesch\"{a}ftsmethoden von der Patentierbarkeit auszuschlie{\ss}en, und haben damit gedroht, dass das Europ\"{a}ische Parlament seine Chance zur Mitwirkung verspielen w\"{u}rde, wenn es f\"{u}r eine echte Begrenzung der Patentierbarkeit stimmen sollte.

\item
Da das Europ\"{a}ische Parlament sich nicht t\"{a}uschen oder einsch\"{u}chtern lie{\ss}, versuchen einflu{\ss}reiche Patentfachleute in verschiedenen Regierungen und Organisationen nun, den EU-Ministerrat zu benutzen, um an der parlamentarischen Demokratie in der Europ\"{a}ischen Union vorbei zu kommen.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{Aus diesen Gr\"{u}nden empfehlen wir folgende Ma{\ss}nahmen}
\begin{enumerate}
\item
Wir rufen das Europ\"{a}ische Patentamt und nationale Patent\"{a}mter dazu auf, sofort die Erteilung von Patenten auf Gesch\"{a}fts- und Datenverarbeitungsmethoden in jedweder sprachlicher Einkleidung einzustellen und Artikel 52 des Europ\"{a}ischen Patent\"{u}bereinkommens\footnote{http://swpat.ffii.org/analyse/epue52/epue52.de.html} korrekt gem\"{a}{\ss} den \"{u}blichen Methoden der Gesetzesauslegung\footnote{http://swpat.ffii.org/analyse/epue52/exeg/epue52exeg.de.html} anzuwenden.

\item
Wir rufen die Mitglieder des EU-Ministerrates auf, von jeglichem Gegenvorschlag zu der Version des Europ\"{a}ischen Parlaments Abstand zu nehmen, solange solch ein Gegenvorschlag keine ausdr\"{u}ckliche Unterst\"{u}tzung des Parlaments ihres Mitgliedsstaates gefunden hat.

\item
Wir rufen die Abgeordneten der nationalen Parlamente auf, ihre nationalen Interessen hinsichtlich der Grenzen der Patentierbarkeit klar zu formulieren und daf\"{u}r zu sorgen, dass die Ministerialbeamten ihrer Regierung diese nationalen Interessen im Europ\"{a}ischen Rat loyal vertreten.

\item
Wir fordern, dass alle Gesetzesvorschl\"{a}ge, einschlie{\ss}lich derjenigen des Europ\"{a}ischen Parlaments und der Mitgliedsstaaten, anhand einer Testreihe\footnote{http://swpat.ffii.org/analyse/testsuite/swpatmanri.de.html} von Beispiel-Patentantr\"{a}gen eingehend gepr\"{u}ft werden, um zu sehen, ob sie zweifelsfrei zu den gew\"{u}nschten Resultaten f\"{u}hren und keinen Raum f\"{u}r weitere Fehlauslegungen lassen w\"{u}rden.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Unterschriften}
\begin{center}
Siehe http://aktiv.ffii.org/euparl/de
\end{center}

Bent Hindrup Andersen\footnote{http://www.hindrup.dk} (MdEP, EDD, D\"{a}nemark, Junibewegung\footnote{http://www.j.dk})\\
Jens-Peter Bonde\footnote{http://www.bonde.com/} (MdEP, EDD (Vorsitzender), D\"{a}nemark, Junibewegung\footnote{http://www.j.dk})\\
Johanna Boogerd\footnote{http://www.johannaboogerd.nl/} (MdEP, ELDR, Niederlande)\\
Hiltrud Breyer\footnote{http://www.hiltrud-breyer.de} (MdEP, VERD, Deutschland)\\
Daniel Cohn-Bendit\footnote{http://www.cohn-bendit.com/} (MdEP, VERD, Frankreich)\\
Jan Dhaene\footnote{http://www.jandhaene.be} (MdEP, Belgien)\\
Raina Mercedes Echerer\footnote{http://www.mercedes-echerer.at/} (MdEP, VERD, \"{O}sterreich)\\
Pernille Frahm\footnote{http://www.frahm.dk/} (MdEP, GUE/NGL, D\"{a}nemark)\\
Pierre Jonckheer\footnote{http://www.pierrejonckheer.be/} (MdEP, VERD, Belgien)\\
Piia-Noora Kauppi\footnote{http://www.kauppi.net/} (MdEP, PPE-DE, Finnland)\\
Wolfgang Kreissl-Dörfler\footnote{http://www.kreissl-doerfler.de/} (MdEP, PSE, Deutschland)\\
Jean Lambert\footnote{http://http://www.jeanlambertmep.org.uk/} (MdEP, VERD, Vereinigtes K\"{o}nigreich)\\
Donald Neil (Professor) MacCormick\footnote{http://wwwdb.europarl.eu.int/ep5/owa/whos\_mep.data?ipid=0\&ilg=EN\&iucd=4548\&ipolgrp=.\&ictry=GB\&itempl=\&ireturn=\&imode=} (MdEP, VERD, Vereinigtes K\"{o}nigreich)\\
Caroline Lucas\footnote{http://www.carolinelucasmep.org.uk/} (MdEP, VERD, Vereinigtes K\"{o}nigreich)\\
Heide Rühle\footnote{http://www.heide-ruehle.de} (MdEP, VERD, Deutschland)\\
Ulla Sandbæk\footnote{http://www.ullasandbaek.dk/} (MdEP, EDD, D\"{a}nemark, Junibewegung\footnote{http://www.j.dk})\\
Olle Schmidt\footnote{http://www.olles.nu/} (MdEP, ELDR, Schweden)\\
Bart Staes\footnote{http://www.bartstaes.be/} (MdEP, VERD, Belgien, Groen!\footnote{http://www.groen.be})\\
Claude Turmes\footnote{http://www.greng.lu/site/index.php?ref3=111\&ref2=7\&navtype=3\&typebloc=2} (MdEP, VERD, Luxemburg, DÉI GRÉNG\footnote{http://www.greng.lu})\\
Anders Wijkman\footnote{http://www.wijkman.kristdemokrat.se/default.asp} (MdEP, PPE-DE, Schweden)\\
Matti Wuori\footnote{http://www.mattiwuori.net/} (MdEP, VERD, Finnland)\\
Olga Zrihen\footnote{http://www.dpsb.be/oz/oz.html} (MdEP, PSE, Belgien, PS\footnote{http://www.ps.be})\\
Jens Holm\footnote{http://www.jensholm.se/} (MEP candidate, Schweden)\\
Ellen Trane Nørby\footnote{http://www.ellenieu.dk/} (MEP candidate, D\"{a}nemark)\\
Dirk Van Der Maelen (Fraktionsvorsitzender der Fl\"{a}mischen Sozialdemokraten im Belgischen Parlament, Belgien, SP.be\footnote{http://www.sp.be})\\
Grietje Bettin\footnote{http://www.sh.gruene.de:8080/MdB/Bettin/ein\_text?datum2=2002/05/13\percent{}2020\percent{}3A00\percent{}3A19\percent{}20GMT\percent{}2B2} (MdB, Sprecherin f\"{u}r Medienpolitik der Fraktion B\"{u}ndnis 90 / Die Gr\"{u}nen)\\
Gustav Fridolin\footnote{http://www.mp.se/gustavfridolin.asp} (Parlamentsabgeordneter, Schweden)\\
Zo\'{e} Genot (Parlamentsabgeordneter, Belgien, ecolo.be\footnote{http://www.ecolo.be/})\\
Arseni Gibert (Senator der Entesa Catalana de Progrés\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html} f\"{u}r die Region Girona)\\
Ulrich Kelber\footnote{http://www.ulrich-kelber.de/} (MdB (Sozialdemokratische Partei Deutschlands\footnote{http://www.spd.de/}))\\
Monica Lochner-Fischer\footnote{http://www.lochner-fischer.de/} (Mitglied des Bayerischen Landtags, SPD)\\
Dr. Jan Van Duppen\footnote{http://www.janvanduppen.be} (Belgischer Senator, Mitglied des Fl\"{a}mischen Parlaments, SP.a spirit\footnote{http://www.sp.be/})\\
CEA/PME\footnote{http://www.ceapme.org} (Europ\"{a}ischer Bund der Verb\"{a}nde Kleiner und Mittelst\"{a}ndischer Unternehmen) (Kontakt: Walter Grupp)\\
D\"{a}nischer Verband der IT-Fachleute\footnote{http://www.prosa.dk/} (PROSA.dk) (Kontakt: Peter Ussing)\\
VOV@SPD\footnote{http://www.vov.de/} (Virtueller Ortsverein der SPD) (Kontakt: Axel Schudak, Arne Brand und Boris Piwinger)\\
temPS r\'{e}els\footnote{http://www.temps-reels.net} (Internet-Sektion der Sozialistischen Partei Frankreichs) (Kontakt: Thierry Noisette)\\
Opera Software\footnote{http://www.opera.com/} (Kontakt: H\aa{}kon Wium Lie, Leiter F\&E)\\
Magix AG\footnote{http://www.magix.com/} (Kontakt: Oliver Lorenz, Leiter der Rechtsabteilung)\\
ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat} (Kontakt: Stefan Pollmeier, Gesch\"{a}ftsf\"{u}hrer)\\
FSG-IT\footnote{http://www.fsg-it.de} (Kontakt: Lars Noschinski, Developer)\\
ObjectWeb\footnote{http://wiki.objectweb.org/Wiki.jsp?page=CWP\_SoftwarePatents\_French} (Kontakt: Francois Letellier)\\
TFDesign s.r.o.\footnote{http://www.tfdesign.cz} (Kontakt: Michal Zajic, Owner, Head Designer - civil engineering)\\
ashampoo Technology GmbH \& Co. KG\footnote{http://tech.ashampoo.com/} (Kontakt: Hauke Duden)\\
Intelligent Firmware Ltd\footnote{http://www.intelligentfirm.co.uk/} (Kontakt: Michael Krech, Gesch\"{a}ftsf\"{u}hrer)\\
PETE SOFTWARE GmbH\footnote{http://www.pete-software.de/} (Kontakt: Thies Reinhold, Vorstand)\\
Phaidros AG\footnote{http://www.phaidros.com/german/phaidros/engagement/standpunkt\_patente.htm} (Kontakt: Matthias Schlegel, Vorstand)\\
Power Media Sp. z o.o.\footnote{http://www.power.com.pl/} (Kontakt: Narczynski, Wojtek Jakub, CTO, VP \& co-founder)\\
Myriad Software\footnote{http://www.myriad-online.com/} (Kontakt: Guillion Didier, Gesch\"{a}ftsf\"{u}hrer)\\
MySQL\footnote{http://www.mysql.com/} (Kontakt: Michael Widenius, Florian Mueller)\\
NEOlabs\footnote{http://www.neolabs.be/} (Kontakt: Jo De Baer, Berater)\\
Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, Gr\"{u}nder und Besitzer)\\
Ulf Dunkel (invers Software Vertrieb\footnote{http://www.calamus.net/} und Dunkel Software Distribution\footnote{http://www.dsd.net/}, Gesch\"{a}ftsf\"{u}hrer)\\
Mind\footnote{http://www.mind.be/} (Kontakt: Dr. ir. Peter Vandenabeele, Gesch\"{a}ftsf\"{u}hrer)\\
Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Gesch\"{a}ftsf\"{u}hrer)\\
Intevation GmbH\footnote{http://www.intevation.de/} (Kontakt: Bernhard Reiter und Jan-Oliver Wagner)\\
RDG Software di 'Roberto Della Grotta'\footnote{http://www.rdg-software.it/} (Kontakt: Roberto Della Grotta)\\
S.K.I. GmbH\footnote{http://www.ski-gmbh.com/} (Kontakt: Michael Schlegel)\\
Bureau d'Etudes en Génie Informatique, Hasgard\footnote{http://www.hasgard.net/} (Kontakt: Bruno Berthelet)\\
Nexedi\footnote{http://www.nexedi.com/} (Kontakt: Jean-Paul Smets)\\
TRI-EDRE and TRI-EDRE Developments\footnote{http://www.tri-edre.fr/} (Kontakt: Thierry Rolland, Gesch\"{a}ftsf\"{u}hrer)\\
Stephan K\"{o}rner (Gesch\"{a}ftsf\"{u}hrer, Pilum Technology GmbH)\\
Marco Schulze (Gesch\"{a}ftsf\"{u}hrer, Nightlabs GmbH\footnote{http://www.nightlabs.com/})\\
Python Business Forum\footnote{http://www.python-in-business.org/} (Kontakt: Jacob Hall\'{e}n, Vorsitzender)\\
National Computer Helpdesk\footnote{http://NationalComputerHelpdesk.net/} (Kontakt: John Harris - Manager Support Services, Christopher Thompson - Director)\\
ROHOST\footnote{http://www.rohost.com/} (Kontakt: Marius David)\\
AB STRAKT\footnote{http://www.strakt.com/} (Kontakt: Laura Creighton, Venture Capitalist)\\
Prof. Martin Kretschmer (Centre for Intellectual Property Policy \& Management\footnote{http://www.cippm.org.uk/}, Joint Director)\\
ROSPOT\footnote{http://www.rospot.com/} (Kontakt: Cosmin Neagu)\\
Dr. Karl-Friedrich Lenz\footnote{http://k.lenz.name/LB/archives/000617.html\#000617} (professor (ky\^{o}ju) f\"{u}r Deutsches und Europ\"{a}isches Recht, Aoyama Gakuin Daigaku, Japan)\\
Dr. Jean-Philippe Rennard (Wirtschaftswissenschaftler, Informatiker, Gesch\"{a}ftsf\"{u}hrer, Eponyme SA\footnote{http://www.eponyme.com/})\\
Anders Skovsted Buch (Lector in mathematics, Universit\"{a}t Aarhus, D\"{a}nemark)\\
Prof. Dr. Koen De Bosschere (Professor der Informatik, Universit\"{a}t Ghent, Belgien)\\
Prof. Dr. Wilfried Philips (Professor der Informatik, Universit\"{a}t Ghent, Belgien)\\
Dr. Michiel Ronsse (Forscher, Universit\"{a}t Ghent)\\
Student Union Akku (University of Nijmegen, The Netherlands)\footnote{http://www.kun.nl/akku/} (Kontakt: Dieter Van Uytvanck)\\
Institut Biotecnologia i Biomedicina (Universitat Aut\'{o}noma Barcelona, Spain)\footnote{http://www.uab.es/investigacio/ibf.htm} (Kontakt: Dr. Enrique Querol, Director)\\
\"{O}sterreichische Hochsch\"{u}lerInnenschaft -- Bundesvertretung\footnote{http://www.oeh.ac.at/} (Kontakt: Leonard Dobusch)\\
ANSOL - Associa\c{c}\~{a}o Nacional para o Software Livre\footnote{http://www.ansol.org/} (Kontakt: Jaime Villate, President)\\
APRIL\footnote{http://www.april.org/} (Kontakt: Olivier Berger)\\
Verband der Katalonischsprachigen Anwender von GNU/Linux\footnote{http://patents.caliu.info/} (CALIU)\\
Electronic Frontier Finnand\footnote{http://www.effi.org/patentit/index.en.html} (Kontakt: Ville Oksanen, Tapani Tarvainen)\\
Europe Shareware\footnote{http://www.europe-shareware.org/} (Kontakt: Pascal Ricard und Sylvain Perchaud)\\
Association for Free Software\footnote{http://www.affs.org.uk/} (Kontakt: Marc Eberhard)\\
Association of Free Software Professionals\footnote{http://www.afsp.org.uk/} (Kontakt: Neil Darlow)\\
Bürgernetzverband e.V.\footnote{http://www.buerger.net/} (Kontakt: Eberhard Mittag)\\
Free Software Foundation Europe\footnote{http://www.fsfeurope.org/} (Kontakt: Georg Greve, Pr\"{a}sident)\\
FSF France\footnote{http://www.fsffrance.org/} (Kontakt: Fr\'{e}d\'{e}ric Couchet)\\
German Unix User Group\footnote{http://www.guug.de/} (GUUG.de, Kontakt: Christian Lademann, stellvertretender Vorsitzender)\\
Internet Society Belgium\footnote{http://www.isoc.be/} (Kontakt: Rudi Vansnick, Pr\"{a}sident)\\
Internet Society Poland\footnote{http://www.isoc.org.pl/} (Kontakt: Wladek Majewski)\\
LSVB\footnote{http://www.lsvb.nl} (Dutch National Student Union) (Kontakt: Jurjen van den Bergh)\\
Patentverein.de\footnote{http://www.patentverein.de/} (Verteidigungsbund deutscher KMU gegen Trivialpatente) (Kontakt: Heiner Flocke)\\
LSVB (The Dutch National Student Union)\footnote{http://www.lsvb.nl} (Kontakt: Jurjen van den Bergh)\\
Liberale Hochschulgruppe Ulm\footnote{http://www.uni-ulm.de/lhg/} (Kontakt: Markus Schaber)\\
SSLUG.dk\footnote{http://www.sslug.dk} (Kontakt: Erik Josefsson)\\
WINA-Leuven (Student Association of Students in Maths, Computer Sciences and Physics of the KU Leuven)\footnote{http://www.wina.be/} (Kontakt: Dominique Devriese)\\
VVS (Union of Flemish Students)\footnote{http://www.vvs.ac/} (official standpoint\footnote{http://www.vvs.ac/standpunt/pdf/2004\_05\_01\_patenten\_op\_software\_en\_zakenmethodes.pdf})\\
Zeus WPI (Student Workgroup on Informatics of Ghent University)\footnote{http://zeus.ugent.be/} (Kontakt: Jonas Maebe)\\
F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V.\footnote{http://www.ffii.org/ffii.de.html} (Kontakt, Hartmut Pilch, Alex Macfie und Holger Blasum)\\
\dots (siehe Unterzeichner der fr\"{u}heren Version\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/appell/eubsa-cpedu.de.html\#sign}, Wir stellen gerade die neue Liste zusammen)\\
weitere Unterzeichner\footnote{euparl-sign.html} (16482 Personen haben diesen Appell \"{u}ber das Mitwirkungssystem des FFII unterzeichnet.)
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Europ\"{a}ische Softwarepatente: Einige Musterexemplare\footnote{http://swpat.ffii.org/patente/muster/swpikmupli.de.html}}}

\begin{quote}
Auf folgende eing\"{a}ngige Beispiele stie{\ss}en wir beim ersten Durchst\"{o}bern unserer Softwarepatente-Tabellen.  Sie wurden fast zuf\"{a}llig ausgesucht und k\"{o}nnen somit als repr\"{a}sentativ f\"{u}r die Ma{\ss}st\"{a}be des Europ\"{a}ischen Patentamtes hinsichtlich Technizit\"{a}t und Erfindungsh\"{o}he gelten.  Wenn sie in irgendeinem Punkt vom Mittelma{\ss} abweichen, dann darin, dass sie relativ leicht einem breiten Publikum verst\"{a}ndlich gemacht werden k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente in Aktion\footnote{http://swpat.ffii.org/patente/wirkungen/swpikxrani.de.html}}}

\begin{quote}
In den letzten Jahren sind einige Streitf\"{a}lle um Softwarepatente durch die Medien bekannt geworden.  Es handelt sich hierbei um die Spitze des Eisbergs. Die meisten Entwickler und Firmen werden nur au{\ss}erhalb der Gerichte mit Patentforderungen konfrontiert, und Schweigen liegt im beiderseitigen Interesse.  Viele Projekte werden zur\"{u}ckgeschraubt oder aufgegeben.  Es ist schwer, verhinderte Entwicklungen zu dokumentieren.  Hier wollen wir diesen Versuch unternehmen.
\end{quote}
\filbreak

\item
{\bf {\bf Zitate zu Softwarepatenten\footnote{http://swpat.ffii.org/archiv/zitate/swpatcusku.de.html}}}

\begin{quote}
Einschl\"{a}gige Zitate aus Rechtstexten, wirtschaftwissenschaftlichen Analysen, politischen Beschl\"{u}ssen und Aussagen von Programmierern, Informatikern, Wissenschaftlern, Politikern und anderen Pers\"{o}nlichkeiten des \"{o}ffentlichen Lebens.
\end{quote}
\filbreak

\item
{\bf {\bf Testsuite f\"{u}r die Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit\footnote{http://swpat.ffii.org/analyse/testsuite/swpatmanri.de.html}}}

\begin{quote}
Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu pr\"{u}fen, sollten wir sie an Beispiel-Innovationen ausprobieren.  F\"{u}r jedes Beispiel gibt es einen Stand der Technik, eine technische Lehre und eine Reihe von Anspr\"{u}chen.  In der Annahme, dass die Beispiele zutreffend beschrieben wurden, probieren wir dann unsere neue Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2) Angemessenheit:  f\"{u}hrt die vorgeschlagene Regelung zu einem vorhersagbaren Urteil?  Welche der Anspr\"{u}che w\"{u}rden erteilt?  Entspricht dieses Ergebnis unseren W\"{u}nschen?  Wir probieren verschiedene Gesetzesvorschl\"{a}ge an der gleichen Beispielserie (Testsuite) aus und vergleichen, welches am besten abschneidet.  F\"{u}r Programmierer ist es Ehrensache, dass man ``die Fehler beseitigt, bevor man das Programm freigibt'' (first fix the bugs, then release the code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses Ziels.  Gem\"{a}{\ss} Art. 27 TRIPS geh\"{o}rt die Gesetzgebung zu einem ``Gebiet der Technik'' namens ``Sozialtechnik'' (social engineering), nicht wahr?  Technizit\"{a}t hin oder her, es ist Zeit an die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen, die \"{u}berall dort angesagt ist, wo schlechte Konstruktionsentscheidungen das Leben der Menschen stark beeintr\"{a}chtigen k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/europarl0309/europarl0309.de.html}}}

\begin{quote}
Konsolidierte Version der wesentlichen Bestimmungen (Art 1-6) der ge\"{a}nderten Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'', f\"{u}r die das Europ\"{a}ische Parlament am 24. September 2003 gestimmt hat.
\end{quote}
\filbreak

\item
{\bf {\bf Reaktionen auf die Entscheidung des EU-Parlaments von 03-09-24\footnote{http://swpat.ffii.org/papiere/europarl0309/reag/europarl-reag0309.de.html}}}

\begin{quote}
Frits Bolkestein, Malcolm Harbour und andere Politiker haben sich zu einem Chor von Patentanw\"{a}lten zusammengeschlossen, der sagt, dass ``das Europ\"{a}ische Parlament seine Chance auf demokratische Teilnahme zunichte gemacht hat'' weil es fataler Weise nicht so abgestimmt hat wie dies vom Patent-Establishment gew\"{u}nscht worden war.  Vorbereitungen sind im Gange die Richtline zur\"{u}ck zu ziehen und Software-Patente mittels Vereinbarenung zwischen den nationalen Patentbeh\"{o}rden zu legalisieren, die es gewohnt sind Patentanpolitik unter sich zu entscheiden.
\end{quote}
\filbreak

\item
{\bf {\bf EU Rat 2004/01/29: ``Kompromissvorschlag der Pr\"{a}sidentschaft'' zu Softwarepatenten\footnote{http://swpat.ffii.org/papiere/europarl0309/cons0401/cons0401.de.html}}}

\begin{quote}
Die Irische Ratspr\"{a}sidentschaft der Europ\"{a}ischen Union hat auf ministerialer Arbeitsebene ein Papier herausgebracht, welches Gegenvorschl\"{a}ge zu den \"{A}nderungsvorschl\"{a}gen des Europ\"{a}ischen Parlamentes zur Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'' enth\"{a}lt.  Im Gegensatz zur Version des Europ\"{a}ischen Parlamentes erlaubt die Version des Rates grenzenlose Patentierbarkeit und Patent-Durchsetzbarkeit.  Gem\"{a}{\ss} der Version des Rates gelten ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmethoden von vorneherein als Erfindungen im Sinne des Patentrechts.   Schon die Ver\"{o}ffentlichung einer funktionsf\"{a}higen Beschreibung einer patentierten Idee stellt laut Ratsvorschlag eine Patentverletzung dar.  Protokolle und Dateiformate k\"{o}nnen direkt patentiert werden und d\"{u}rfen dann ohne eine Lizenz des Patentinhabers nicht verwendet werden.  F\"{u}r den unge\"{u}bten Leser sind diese Implikationen nicht unbedingt sofort ersichtlich.  Wir erkl\"{a}ren, woraus sie sich ergeben.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Softwarepatent-\"{A}nderungsantr\"{a}ge: Echte und Scheinbare Begrenzungen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/plen0309/plen0309.de.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 24th.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\begin{itemize}
\item
{\bf {\bf Wie Sie helfen k\"{o}nnen, die Invasion des Patentwesens abzuwehren\footnote{http://swpat.ffii.org/gruppe/aufgaben/swpatgunka.de.html}}}
\filbreak

\item
{\bf {\bf Helfen Sie, Unterst\"{u}tzer f\"{u}r diesen Aufruf zu gewinnen.}}

\begin{quote}
Schreiben Sie an cpedu-help@ffii.org, um zu erfahren, wie Sie weiter helfen k\"{o}nnen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
% mode: latex ;
% End: ;

