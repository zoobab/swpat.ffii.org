<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Appel à l'action III

#descr: En septembre 2003, le Parlement européen a voté pour réaffirmer que le
logiciel était exclu de la brevetabilité. Depuis, la Commission et le
Conseil ont ignoré ce vote et établi de nouveaux précédents dans
l'élaboration non démocratique des lois. Les signataires appellent les
différents acteurs à jouer leur rôle pour remédier à cette situation.

#Wce: Nous sommes préoccupés du fait que :

#tef: Ces dernières années, %(tp|l'Office européen des brevets|OEB), en
contradiction avec la lettre et l'esprit de la loi, a accordé plus de
30 000 brevets sur des règles de calcul avec une machine
conventionnelle de traitement de données, ci-après désignés sous le
terme de %(q:brevets logiciels). Ces brevets sont aussi étendus,
triviaux et préjudiciables que leurs équivalents aux États-Unis.

#nle: En septembre 2003, le Parlement européen a voté pour réaffirmer que le
logiciel était exclu de la brevetabilité. Le vote a suivi 19 mois de
délibérations au sein de 3 commissions parlementaires et était basé
sur une large participation et des recherches bibliographiques
approfondies. Malgré tout, la Commission et le Conseil ont refusé de
seulement discuter des problèmes abordés par le Parlement. Au lieu de
cela, ils ont tenté de faire passer en force les pratiques de l'OEB
pour la seconde lecture dans un Parlement renouvelé, avec des
contraintes de délai plus serrées et des exigences de majorité plus
fortes.

#uoe: Le texte du Conseil prétend exlure les brevets logiciels mais rend en
fait les exclusions dénuées de sens et empêche toute limitation
effective de la brevetabilité. La plupart des formulations utilisées
n'ont aucun but, si ce n'est d'amadouer les consciences des ministres
et des parlementaires.

#WWn: La prise de décision du Conseil a été dominée par les mêmes
fonctionnaires qui dirigent l'Office européen des brevets et qui ont
poussé le texte contre l'avis des parlements nationaux, à l'aide de
manœuvres abusives. Durant la procédure, tant le Conseil que la
Commission ont établi de nouveaux précédents problématiques dans
l'élaboration non démocratique des lois au sein de l'Union européenne.

#Fea: C'est pourquoi nous recommandons les mesures suivantes :

#eon: Nous demandons expressément au Conseil de l'UE d'expliquer, en
répondant aux %(kv:23 questions) de la FFII, comment il est parvenu à
sa prétendue %(q:position commune) sur les brevets logiciels le 7 mars
2005.

#ltt: Nous demandons expressément à l'Office européen des brevets d'arrêter
immédiatement d'accorder des brevets sur des méthodes d'affaires et
des méthodes de traitement de données, quelle que soit la formulation
habillant ces brevets, et d'appliquer correctement %(ar:l'article 52
de la Convention sur les brevets européens) selon les %(ee:méthodes
conventionnelles d'interprétation du droit).

#fWl: Nous demandons expressément aux parlements nationaux en Europe de
prendre en main les sujets concernant la législation sur les brevets.
À court terme, un signal clair doit être envoyé pour empêcher les
offices de brevets nationaux et les tribunaux statuant sur les brevets
d'accorder des revendications de programmes.

#lep: Nous demandons expressément aux députés des parlements nationaux en
Europe de ramener le Conseil de l'UE sous contrôle démocratique. Tout
au moins, les accords politiques doivent être sujets à une
ratification parlementaire, particulièrement lorsqu'ils incorporent
des amendements de dernière minute.

#aWa: Nous demandons expressément aux membres du Parlement européen de
réaffirmer la position claire du Parlement de septembre 2003 ou de
rejeter entièrement la directive.

#Sno: Signataires

#agr: Principales signatures :

#Wfm: doivent encore être recueillies, voir les appels de %(LST) pour vous
faire une idée approximative de ce à quoi cela devrait ressembler. Si
vous désirés être affiché en tant que signatire principal, merci de
contacter %(CH). Si vous souhaitez nous aider à solliciter des
signataires principaux, merci de vous abonner à la %(cp:liste du
projet).

#rgr: Autres signataires

#gcr: %(N) personnes ont d'ores et déjà signé cet appel via le  
%(ps:système de participation de la FFII).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
# mailto: phm@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

