\begin{subdocument}{cpedu0504}{V\'{y}zva k akcii III}{http://swpat.ffii.org/papri/europarl0309/cpedu/0504/index.sk.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2005-04-29 by Slovak Environment Agency\footnote{\url{http://www.sazp.sk}}}{V septembri 2003 hlasoval Eur\'{o}psky parlament, aby zaistil vy\v{n}atie softwaru z patentovania. Medzit\'{y}m Komisia a Rada ignorovala toto hlasovanie a stanovila tak novy precedens v tvorbe nedemokratick\'{e}ho pr\'{a}va. Nasleduje podpisova akcia na n\'{a}pravu tejto situ\'{a}cie.}
\begin{sect}{prob}{Sme znepokojen\'{\i} t\'{y}m, \v{z}e}
\begin{enumerate}
\item
V posledn\'{y}ch rokoch Eur\'{o}psky patentov\'{y} \'{u}rad (EPO) udelil viac ako 30,000 patentov na po\v{c}\'{\i}ta\v{c}ov\'{e} spracovanie s konven\v{c}nou v\'{y}po\v{c}tovou technikou vr\'{a}mci kateg\'{o}rie ``softwarov\'{e} patenty'', \v{c}o je v rozpore s literou a duchom platn\'{e}ho z\'{a}kona. Tieto patenty s\'{u} tak \v{s}irok\'{e}, trivi\'{a}lne a po\v{s}kodzuj\'{u}ce ako ich ekvivalenty v USA.

\item
V septembri 2003 hlasoval Eur\'{o}psky parlament, aby zaistil vy\v{n}atie softwaru z patentovania, pri\v{c}om kodifikoval p\^{o}vodn\'{e} znenie z\'{a}kona\footnote{\url{http://swpat.ffii.org/papri/epo-gl78/index.en.html}} a objasnil terminol\'{o}giu konvencie TRIP vysvet\v{l}uj\'{u}c tradi\v{c}n\'{u} te\'{o}riu technick\'{e}ho objavu, tak ako je to uveden\'{e} v Nemeckom z\'{a}konodarstve od 1976\footnote{\url{http://swpat.ffii.org/papri/bgh-dispo76/index.de.html}} do 2002\footnote{\url{http://swpat.ffii.org/papri/bpatg17-suche02/index.de.html}}. Hlasovanie prebehlo po 19 mesiacoch pr\'{a}ce 3 komisi\'{\i} a bolo podlo\v{z}en\'{e} \v{s}irokou \'{u}\v{c}as\v{t}ou a rozsiahlym \v{s}t\'{u}diom literat\'{u}ry. Napriek tomu Komisia a Rada odmietli diskutova\v{t} o probl\'{e}me, na ktor\'{y} poukazoval Parlament. Namiesto toho sa sna\v{z}ili o n\'{a}siln\'{e} presadenie zdiskreditovan\'{y}ch postupov EPO do druh\'{e}ho \v{c}\'{\i}tania v novom parlamente, ktor\'{e} bolo v \v{c}asovom strese a potrebovalo v\"{a}\v{c}\v{s}iu majoritu.

\item
Text Rady len predstiera\footnote{\url{http://swpat.ffii.org/xatra/cons0406/text/index.en.html}} vyl\'{u}\v{c}enie softwarov\'{y}ch patentov, v skuto\v{c}nosti znejas\v{n}uje s\'{u}\v{c}asn\'{e} znenie direkt\'{\i}vy a zabra\v{n}uje ak\'{y}mko\v{l}vek \'{u}\v{c}inn\'{y}m obmedzeniam ich patentovania. V\"{a}\v{c}\v{s}ina znen\'{\i} pou\v{z}it\'{y}ch v direkt\'{\i}ve nesl\'{u}\v{z}i na \v{z}iadny \'{u}\v{c}el, len ober\'{a} ministrov a \v{c}lenov parlamentu o ich rozum.

\item
V rozhodovan\'{\i} Rady dominoval vplyv slu\v{z}obn\'{y}ch zamestnancov, ktor\'{\i} riadia EPO a robia n\'{a}tlak proti v\^{o}li n\'{a}rodn\'{y}ch parlamentov. Po\v{c}as tohto procesu sa Rada aj Komisia stali nosite\v{l}mi bezprecedentn\'{e}ho nedemokratick\'{e}ho pr\'{a}vneho postupu v EU.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{Z t\'{y}chto d\^{o}vodov vyz\'{y}vame zodpovedn\'{y}ch \'{u}\v{c}astn\'{\i}kov legislat\'{\i}vneho procesu, aby vykonali nasledovn\'{e}.}
\begin{enumerate}
\item
\v{Z}iadame Radu Eur\'{o}py, aby vysvetlila svoj postoj v ot\'{a}zke softwarov\'{y}ch patentov zo d\v{n}a 7.marca 2005, formou zodpovedania 23 ot\'{a}zok\footnote{\url{http://swpat.ffii.org/xatra/cons050308/index.en.html}} FFII.

\item
\v{Z}iadame Eur\'{o}psky patentov\'{y} \'{u}rad, aby okam\v{z}ite zastavil schva\v{l}ovanie patentov a vr\'{a}til sa ku korektnej interpret\'{a}cii \v{C}l\'{a}nku 52 Eur\'{o}pskej Patentovej Konvencie\footnote{\url{http://swpat.ffii.org/stidi/epc52/index.en.html}}, tak ako sa v\v{s}eobecne aplikovala v EPO pred\footnote{\url{http://swpat.ffii.org/papri/epo-t850022/index.en.html}} 1986\footnote{\url{http://swpat.ffii.org/papri/epo-t840208/index.en.html}}.

\item
\v{Z}iadame \v{c}lenov n\'{a}rodn\'{y}ch parlamentov, aby zobrali rozhodovanie o legislat\'{\i}ve patentovania do vlastn\'{y}ch r\'{u}k. V kr\'{a}tkej dobe, musia vysla\v{t} jasn\'{e} sign\'{a}ly, ktor\'{e} zabr\'{a}nia n\'{a}rodn\'{y}m patentov\'{y}m \'{u}radom a s\'{u}dom schva\v{l}ova\v{t} patenty na programy\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/prog/index.en.html}}.

\item
\v{Z}iadame \v{c}lenov n\'{a}rodn\'{y}ch parlamentov, aby kontrolovali Radu Eur\'{o}py. Aspo\v{n} politick\'{e} dohody musia podlieha\v{t} ratifik\'{a}cii parlamentov a to  \v{s}peci\'{a}lne v pr\'{\i}padoch, ke\v{d} obsahuj\'{u} dodatky na posledn\'{u} chv\'{\i}\v{l}u.

\item
\v{Z}iadame \v{c}lenov Eur\'{o}pskeho parlamentu, aby upevnili jasn\'{y} postoj Parlamentu zo Septembra 2003\footnote{\url{http://swpat.ffii.org/papri/europarl0309/plen05/index.en.html}}, vr\'{a}tane v\v{s}etk\'{e}ho, \v{c}o je potrebn\'{e} na ukon\v{c}enie vyh\'{y}baniu sa dohovoru\footnote{\url{http://swpat.ffii.org/lisri/03/epet0929/aigrain/index.fr.html}}, pr\'{\i}padne zlyhanie dohovoru, zamietnut\'{\i}m celej direkt\'{\i}vy.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{signat\'{a}ri}
\begin{center}
See http://aktiv.ffii.org/parl54/sk
\end{center}

FFII\footnote{\url{http://www.ffii.org/}} (contact: Hartmut Pilch,Jonas Maebe,G\'{e}rald S\'{e}drati-Dinet,Erik Josefsson\ etc)\\
Professional Contractors Group, UK\footnote{\url{http://www.pcg.org.uk/}} (contact: John Kell, Political Researcher)\\
Slovak Environmental Agency\footnote{\url{http://www.sazp.sk/}} (contact: Nada Machkova)\\
Redhat Inc\footnote{\url{http://www.redhat.com/}} (contact: Mark Webbink)\\
MySQL Inc\footnote{\url{http://www.mysql.com/}} (contact: Kaj Arn\"{o})\\
Coop Sange\footnote{\url{http://www.sange.fi/}} (contact: Osma Suominen)\\
Attac France\footnote{\url{http://france.attac.org/}} (contact: Rosaire Amore)\\
hlavn\'{\i} signat\'{a}ri (sa e\v{s}te m\'{a}j\'{u} vyzbiera\v{t}, a ako to m\'{a} pribli\v{z}ne vyzera\v{t} sa pozrite vo v\'{y}zvach 2002-03\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/cpedu/index.en.html}}, 2003-10\footnote{\url{http://swpat.ffii.org/papri/europarl0309/cpedu/0310/index.en.html}} and 2004-06\footnote{\url{http://swpat.ffii.org/xatra/cons0406/index.en.html}} Ak chcete by\v{t} zverejnen\'{y} ako hlavn\'{y} signat\'{a}r, pros\'{\i}m kontaktujte \mbox{\includegraphics{/img/text/cpedu-help.ffii.org}}. Ak n\'{a}m chcete pom\^{o}c\v{t} pri v\'{y}bere hlavn\'{y}ch signat\'{a}rov, pros\'{\i}m prihl\'{a}ste sa do project forum\footnote{\url{http://lists.ffii.org/mailman/listinfo/cpedu-parl/}}.)\\
\v{d}al\v{s}ie podpisy\footnote{\url{parl54-sign.html}} (1174 os\^{o}b doteraz podp\'{\i}salo t\'{u}to v\'{y}zvu cez FFII system\footnote{\url{http://aktiv.ffii.org/}}.)
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
% mode: latex ;
% End: ;

