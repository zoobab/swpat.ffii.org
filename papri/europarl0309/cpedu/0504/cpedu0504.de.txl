<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Aufruf zum Handeln III

#descr: Im September 2003 beschloss das Europäische Parlament, die
Nichtpatentierbarkeit von Software zu bekräftigen.  Die Europäische
Kommission und der Rat der EU haben diesen Beschluss missachtet und
weitere Präzedenzfälle undemokratischer Gesetzgebung geschaffen.  Die
Unterzeichner rufen die Beteiligten des Gesetzgebungsprozesses auf,
ihren Teil beizutragen, um Abhilfe zu schaffen.

#Wce: Folgendes bereitet uns Sorge

#tef: Das %(tp|Europäische Patentamt|EPA) hat in den letzten Jahren gegen
Wortlaut und Geist des geschriebenen Gesetzes mehr als 30.000 Patente
auf Rechenregeln für herkömmliche Datenverarbeitungsanlagen, im
Folgenden %(q:Softwarepatente) genannt, erteilt.  Diese Patente sind
so breit, trivial und schädlich wie ihre US-Gegenstücke.

#nle: Im September 2003 beschloss das Europäische Parlament, die
Nichtpatentierbarkeit von Software zu bekräftigen, indem es die
%(ep:ursprüngliche Interpretation des Gesetzes) festschrieb, und die
Bedeutung der Schlüsselbegriffe des %(tr:TRIPs-Abkommens) zu klären,
indem es auf die traditionelle Lehre der %(ti:technischen Erfindung)
zurückgriff, wie sie insbesondere im deutschen Fallrecht von %(DP) bis
%(FS) auftritt.  Die Abstimmung erfolgte nach 19 Monaten der Beratung
in drei Ausschüssen und gründete sich auf breite Beteiligung und
umfassende Forschungsarbeiten. Dennoch haben die Europäische
Kommission und der Rat der EU sich geweigert, die vom Parlament
erkannten und behandelten Probleme auch nur zu diskutieren.
Stattdessen haben sie sich auf den Versuch verlegt, eine Absegnung der
verrufenen Erteilungspraxis des EPA durch eine zweite Lesung in einem
neuen Parlament zu drücken, wobei ein enger Zeitrahmen und höhere
Mehrheitsanforderungen für Änderungen gelten.

#uoe: Der Entwurf des Rates der EU %(ct:täuscht vor), Softwarepatente
auszuschließen, macht aber tatsächlich die bestehenden
Ausschlussbestimmungen bedeutungslos und verhindert jede wirksame
Begrenzung der Patentierbarkeit. Die meisten Formulierungen des
Ratstextes dienen keinem weiteren Zweck als dem, das Gewissen der
Minister und Parlamentarier zu besänftigen.

#WWn: Die Entscheidungsfindung des Rates wurde von den gleichen
Ministerialbeamten beherrscht, die das EPA leiten, und sie wurde gegen
den Willen nationaler Parlamente durchgedrückt.  Während dieses
Vorgangs haben sowohl der Rat der EU als auch die Europäische
Kommission bestürzende neue Präzendenzfälle undemokratischer
Gesetzgebung in der EU geschaffen.

#Fea: Deswegen rufen wir die Verantwortlichen zu folgenden Maßnahmen auf

#eon: Wir rufen den EU-Rat auf, zu erklären, wie sein angeblich
%(q:Gemeinsamer Standpunkt) vm 7. März 2005 über Softwarepatente
zustande kam, indem er die diesbezüglichen %(kv:23 Fragen) des FFII
beantwortet.

#ltt: Wir rufen das Europäische Patentamt auf, die Erteilung von Patenten
auf Geschäftsmethoden und Rechenregeln unverzüglich einzustellen und
zu einer %(ee:korrekten Auslegung) von %(ar:Artikel 52 des
Europäischen Patentübereinkommens) zurück zu kehren, wie sie beim EPA
%(eb:vor) %(vc:1986) allgemein ausgeübt wurde.

#fWl: Wir rufen die Mitglieder der nationalen Parlamente Europas auf, die
Gesetzgebung des Patentwesens in ihre eigenen Hände zu nehmen. 
Kurzfristig müssen klare Signale ausgegeben werden, um nationale
Patentämter und -gerichte davon abzuhalten, %(pk:Programmansprüche)
anzuerkennen.

#lep: Wir rufen die Mitglieder der nationalen Parlamente Europas auf, den
Rat der EU unter Kontrolle zu bringen.  Es ist dringend nötig, dass
politische Übereinkünfte über Gemeinsame Standpunkte des Rates einer
Ratifizierung durch die nationalen Parlaente unterworfen werden,
insbesondere wenn sie Änderungen enthalten, die erst in letzter Minute
eingebracht wurden.

#aWa: Wir rufen die Mitglieder des Europäischen Parlaments auf, %(p5: den
klaren Standpunkt des Parlaments vom September 2003 zu bekräftigen)
oder anderenfalls die Richtline vollständig abzulehnen.

#Sno: Unterzeichner

#agr: Führende Unterschriften

#Wfm: sind noch zu sammeln, siehe Aufrufe von %(LST), um zu erfahren, wie
das ungefähr aussehen sollte.  Wenn Sie gerne als führender
Unterzeichner aufgelistet werden wollen, wenden Sie sich bitte an
%(CH).  Wenn Sie uns helfen wollen, führende Unterzeichner zu
gewinnen, tragen Sie sich bitte im Verteiler des %(cp:Projektforums)
ein.

#rgr: weitere Unterzeichner

#gcr: %(N) Personen haben diesen Appell über das %(ps:Mitwirkungssystem des
FFII) unterzeichnet.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
# mailto: phm@a2e.de ;
# login: nlmarco ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

