<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Richiesta d'azione III

#descr: Nel settembre 2003 il Parlamento Europeo votò per riaffermare
l'esclusione del software dall'ambito di quanto è brevettabile. Nel
frattempo, la Commissione ed il Consiglio dei Ministri hanno ignorato
questa votazione e stabilito nuovi precedenti di un modo
antidemocratico di legiferare. I sottoscritti chiedono ai vari
protagonisti di fare la loro parte per rimediare a questa situazione.

#Wce: Siamo preoccupati dal fatto che

#tef: Negli ultimi anni, l'%(tp|Ufficio per i Brevetti Europeo|EPO), in
contraddizione con la lettera e lo spirito della legge vigente, ha
concesso più di 30,000 brevetti per computer normalmente attrezzati
per processare dati (es. i personal computer), sotto indicati come
“brevetti software”. Questi brevetti sono estremamente ampi, banali
e dannosi come le loro controparti USA.

#nle: Nel settembre 2003 il Parlamento Europeo votò per riaffermare
l'esclusione del software dall'ambito di quanto è brevettabile,
codificando così l'%(ep:interpretazione originale della legge), e per
chiarire la terminologia del %(tr:trattato TRIPS) ricorrendo alla
teoria tradizionale dell'%(ti:invenzione tecnica), come si trova
specialmente nella giurisprudenza tedesca dal %(DP) al %(FS). Il voto
si ebbe dopo 19 mesi di deliberazioni in 3 commissioni e si basò su
una vasta partecipazione e un'approfondita documentazione. Ciò
nonostante la Commissione e il Consiglio si sono rifiutati di anche
solo discutere le problematiche risolte dal Parlamento. Hanno invece
tentato di imporre la screditata pratica dell'EPO eliminando gli
emendamenti e rimandando la direttiva ad una seconda lettura nel nuovo
parlamento, con una tempistica molto stretta ed il requisito di una
maggioranza assai più vasta per reintrodurre le modifiche.

#uoe: Il testo del Consiglio %(ct:pretende) di escludere i brevetti
software, ma nella realtà svuota poi di significato tali esclusioni ed
impedisce ogni effettiva limitazione della brevettabilità in questo
campo. Molte delle espressioni in esso usate non servono a null'altro
che a tranquillizzare le coscienze dei ministri e dei parlamentari.

#WWn: La decisione del Consiglio è stata dominata da quegli stessi che
governano l'Ufficio Brevetti Europeo (EPO), ed è stata forzata contro
la volontà dei parlamenti nazionali. Durante il processo, sia il
Consiglio sia la Commissione hanno stabilito nuovi preoccupanti
precedenti di giurisprudenza antidemocratica nell'Unione Europea.

#Fea: Per queste ragioni, raccomandiamo ciò che segue:

#eon: E' urgente che il Consiglio dei Ministri Europeo spieghi, rispondendo
alle %(kv:23 domande) della FFII, in che modo sia scaturita ciò che
chiama %(q:posizione comune) sui brevetti software del 7 marzo 2005.

#ltt: E' urgente che l'Ufficio Brevetti Europeo (EPO) smetta immediatamente
di concedere brevetti sui metodi commerciali ("business methods” e
regole per l'elaborazione dei dati, e che ritorni su una %(ee:corretta
interpretazione) dell'%(ar:Articolo 52 della Convenzione sui Brevetti
Europea), come generalmente avveniva nell'EPO %(eb:prima) del
%(vc:1986).

#fWl: E' urgente che i membri dei parlamenti nazionali degli stati europei
prendano nelle loro mani la legislazione in materia di brevetti. Nel
breve periodo, chiari segnali devono essere inviati per impedire che
gli uffici nazionali dei brevetti e le corti chiamate a decidere in
materia permettano %(pk:rivendicazioni su programmi).

#lep: E' urgente che i membri dei parlamenti nazionali degli stati europei
mettano il Consiglio dei Ministri Europeo sotto controllo. Come
requisito minimo, gli accordi politici devono essere soggetti alla
ratifica parlamentare, in special modo quando contengano emendamenti
dell'ultimo minuto.

#aWa: E' urgente che i membri del Parlamento Europeo %(p5:riaffermino la
chiara posizione del Parlamento del settembre 2003), compreso
%(pa:tutto quanto è necessario per eliminare scappatoie ed ambiguità),
o, se ciò fallisse, rigettino interamente la direttiva.

#Sno: Firmatari

#agr: firme di testa

#Wfm: devono ancora essere raccolte; vedere richieste del %(LST) per un'idea
di come appaiano. Se vuoi essere visualizzato come uno dei firmatari
di testa, per cortesia contatta %(CH). Se ci vuoi aiutare a
raccogliere firmatari di testa, per cortesia iscriviti al %(cp:forum
del progetto).

#rgr: ulteriori firme

#gcr: %(N) persone hanno sottoscritto questo appello tramite il %(ps:sistema
di partecipazione della FFII).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
# mailto: phm@a2e.de ;
# login: simos ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

