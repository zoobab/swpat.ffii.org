<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Call for Action III

#descr: In September 2003 the European Parliament voted to reaffirm the
exclusion of software from patentability.  Meanwhile, the Commission
and Council have ignored this vote and set new precedents in
undemocratic lawmaking.  The undersigned call on the various players
to do their part in remedying the situation.

#Wce: We are concerned that

#Fea: For these reasons we recommend the following:

#Sno: Signatories

#tef: In recent years, the %(tp|European Patent Office|EPO) has, in
contradiction to the letter and spirit of the written law, granted
more than 30,000 patents rules for computing with conventional data
processing equipment, below termed %(q:software patents).  These
patents are as broad, trivial and damaging as their US counterparts.

#uoe: The Council's text pretends to exclude software patents, but in fact
only makes existing exclusions meaningless and prevents any effective
limitation of patentability.  Most of the wordings used therein do not
serve any purpose apart from soothing the consciences of ministers and
parliamentarians.

#WWn: The Council's decisionmaking was dominated by the very civil servants
who run the European Patent Office, and pushed through against the
will of national parliaments, with help of abusive maneuvers.  During
the process, both the Council and the Commission have set troubling
new precedents for undemocratic lawmaking in the EU.

#eon: We urge the EU Council to explain, by answering the FFII's %(kv:23
questions), how its claimed %(q:common position) on software patents
of 7th of March 2005 came about.

#ltt: We urge the European Patent Office to immediately stop granting
patents on business methods and data processing rules in whatever
verbal clothing, and to apply %(ar:Article 52 of the European Patent
Convention) correctly according to %(ee:conventional methods of
interpretation of law).

#fWl: We urge the members of Europe's national parliaments to take
legislation on patent matters into their hands.  In the short term,
clear signals must be sent to prevent national patent offices and
patent courts from allowing program claims.

#lep: We urge the members of Europe's national parliaments to bring the EU
Council under control.  At the very least, political agreements must
be subject to parliamentary ratification, especially when they contain
last-minute amendments.

#aWa: We urge the members of the European Parliament to either reject the
directive entirely or reaffirm the Parliament's clear position of
September 2003 without much change.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

