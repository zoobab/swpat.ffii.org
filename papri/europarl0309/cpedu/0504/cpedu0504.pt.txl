<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposta de medidas III

#descr: Em Setembro de 2003, o Parlamento Europeu votou por reafirmar a
exclusão do software da patentabilidade. Entretanto, a Comissão e o
Conselho têm ignorado este voto e criado novos precedentes de ausência
democrática na criação de leis. Os subscritores apelam aos diversos
participantes que contribuam para remediar a situação.

#Wce: Preocupa-nos que

#tef: Nos últimos anos, o %(tp|Gabinete Europeu de Patentes|EPO) tem, em
contradição com a letra e espírito da lei escrita, concedido mais de
30,000 patentes sobre regras de computaçãop com equipamento
convencional de processamento de dados, doravante referidos como
%(q:patentes de software). Estas patentes são tão abrangentes,
triviais e danosas como as suas equivalentes nos EUA.

#nle: Em Setembro de 2003, o Parlamento Europeu votou por reafirmar a
exclusão do software da patentabilidade codificando a
%(ep:interpretação original da lei), e para clarificar a terminologia
do %(tr:tratado TRIPs) recorrendo à teoria tradicional da
%(ti:invenção técnica), como encontrada em especial na jurisprudência
alemã de %(DP) a %(FS). A decisão surgiu após 19 meses de deliberação
em 3 comités e foi baseado numa ampla particupação e extensiva
literatura de investigação. Apesar disso, a Comissão e o Conselho
têm-se recusado sequer a discutir os problemas endereçados pelo
Parlamento. Pelo contrário, tentaram forçar a prática desacreditada do
EPO através de uma segunda leitura num novo Parlamento, com mais
apertadas restrições temporais e maiores exigências quanto à maioria.

#uoe: O texto do Conselho %(ct:finge) excluir patentes de software, mas na
realidade apenas tira qualquer significado às exclusões existentes e
impede qualquer limitação efectiva à patentabilidade. A maioria do
conteúdo utilizado não serve propósito algum para além de confortar as
consciências de ministros e parlamentares.

#WWn: O processo de decisão do Conselho foi dominado pelos mesmos
funcionários públicos que gerem o EPO, e pressionado contra a vontade
de parlamentos nacionais. Durante o processo, tanto o Conselho como a
Comissão criaram preocupantes precedentes de ausência democrática na
escrita de leis Europeias.

#Fea: Por estas razões recomendamos o seguinte:

#eon: Apelamos ao Conselho da UE que explique, respondendo às %(kv:23
perguntas) da FFII, como é que surgiu a sua %(q:posição comum) sobre
patentes de software de 7 de Março de 2005.

#ltt: Apelamos ao Gabinete Europeu de Patentes que cesse imediatamente de
conceder patentes sobre modelos de negócio e regras de processamento
de dados, e a reverter para uma %(ee:interpretação correcta) do
%(ar:Artigo 52 da Convenção Europeia de Patentes), tal como praticada
na generalidade pelo EPO %(eb:até) %(vc:1986).

#fWl: Apelamos aos deputados dos parlamentos nacionais Europeus que tomem
nas suas próprias mãos a legislaçào sobre patentes. A curto prazo têm
de ser enviados sinais claros para prevenir que os gabinetes nacionais
de patentes e tribunais de patentes concedam mais %(pk:reivindicações
sobre programas).

#lep: Apelamos aos membros dos parlamentos nacionais da Europa que exerçam
controlo sobre o Conselho. No mínimos dos mínimos, os acordos
políticos deverão ser sujeitos a ratificação parlamentar,
especialmente quando contiverem emendas apresentadas nos últimos
momentos.

#aWa: Apelamos aos membros do Parlamento Europeu que %(p5:reafirmem a sua
clara posição de Setembro de 2003), incluindo %(pa:tudo o que for
necessário para fechar as portas do cavalo), ou, não o conseguindo
fazer, que rejeitem a directiva por completo.

#Sno: Subscritores

#agr: principais assinaturas

#Wfm: ainda não foram obtidas, ver apelos de %(LST) para ver aproximadamente
o aspecto desejado. Se desejar ser listado como um subscritor
principal, por favor contacte %(CH). Se pretende ajudar a obter
assinaturas principais, por favor subscreva of %(cp:fórum project).

#rgr: Mais subscritores

#gcr: %(N) pessoas já assinaram esta Proposta de Medidas através do
%(ei:sistema de participação da FFII).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
# mailto: phm@a2e.de ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

