<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Call for Action III

#descr: In September 2003 the European Parliament voted to reaffirm the
exclusion of software from patentability.  The vote came after 19
months of deliberation in 3 committees and was based on broad
participation and an extensive research literature.  Yet the
Commission and Council have refused to even discuss the problems
addressed by the Parliament. Instead they have attempted to force the
EPO practise through another reading in a new parliament, with much
tighter time constraints and higher majority requirements.

#Wce: We are concerned that

#tef: In recent years, the %(tp|European Patent Office|EPO) has, in
contradiction to the letter and spirit of the written law, granted
more than 30,000 patents rules for computing with conventional data
processing equipment, below termed %(q:software patents).  These
patents are as broad, trivial and damaging as their US counterparts.

#uoe: The Council's text pretends to exclude software patents, but in fact
only makes existing exclusions meaningless and prevents any effective
limitation of patentability.  Most of the wordings used therein do not
serve any purpose apart from soothing the consciences of ministers and
parliamentarians.

#WWn: The Council's decisionmaking was dominated by the very civil servants
who run the European Patent Office, and pushed through against the
will of national parliaments, with help of abusive maneuvers.  During
the process, both the Council and the Commission have set new
precedents for undemocratic lawmaking in the EU.

#Fea: For these reasons we recommend the following:

#ltt: We urge the European Patent Office to immediately stop granting
patents on business methods and data processing methods in whatever
verbal clothing, and to apply %(ar:Article 52 of the European Patent
Convention) correctly according to %(ee:conventional methods of
interpretation of law).

#fWl: We urge the members of our national parliaments to take charge of the
national patent systems and to prepare legislation.  In particular,
clear signals must be sent to prevent national patent offices and
patent courts from allowing program claims.

#eon: We urge the European Council to explain, by answering the FFII's
%(kv:23 questions), how its claimed %(q:common position) on software
patents of 7th of March 2005 came about.

#lep: We urge the members of national parliaments to take control over their
country's lawmaking in the EU Council.  As a preliminary minimum,
political agreements must be subject to parliamentary review,
especially when they contain last-minute amendments.  Consent to the
EU Constitution should also be made dependent on such reform.

#aWa: We urge the members of the European Parliament to either reject the
Council's %(q:Common Position) or prepare for a strong stand in the
next round by reaffirming the amendments of September 2003 without
much change.

#Sno: Signatories

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/europarl-cpedu.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: Cpedu0504 ;
# txtlang: xx ;
# End: ;

