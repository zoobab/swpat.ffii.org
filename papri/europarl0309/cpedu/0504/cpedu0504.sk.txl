<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Výzva k akcii III

#descr: V septembri 2003 hlasoval Európsky parlament, aby zaistil vyňatie
softwaru z patentovania. Medzitým Komisia a Rada ignorovala toto
hlasovanie a stanovila tak novy precedens v tvorbe nedemokratického
práva. Nasleduje podpisova akcia na nápravu tejto situácie.

#Wce: Sme znepokojení tým, že

#tef: V posledných rokoch %(tp|Európsky patentový úrad|EPO) udelil viac ako
30,000 patentov na počítačové spracovanie s konvenčnou výpočtovou
technikou vrámci kategórie %(q:softwarové patenty), čo je v rozpore s
literou a duchom platného zákona. Tieto patenty sú tak široké,
triviálne a poškodzujúce ako ich ekvivalenty v USA.

#nle: V septembri 2003 hlasoval Európsky parlament, aby zaistil vyňatie
softwaru z patentovania, pričom kodifikoval %(ep:pôvodné znenie
zákona) a objasnil terminológiu %(tr:konvencie TRIP) vysvetľujúc
tradičnú teóriu %(ti:technického objavu), tak ako je to uvedené v
Nemeckom zákonodarstve od %(DP) do %(FS). Hlasovanie prebehlo po 19
mesiacoch práce 3 komisií a bolo podložené širokou účasťou a
rozsiahlym štúdiom literatúry. Napriek tomu Komisia a Rada odmietli
diskutovať o probléme, na ktorý poukazoval Parlament. Namiesto toho sa
snažili o násilné presadenie zdiskreditovaných postupov EPO do druhého
čítania v novom parlamente, ktoré bolo v časovom strese a potrebovalo
väčšiu majoritu.

#uoe: Text Rady len %(ct:predstiera) vylúčenie softwarových patentov, v
skutočnosti znejasňuje súčasné znenie direktívy a zabraňuje akýmkoľvek
účinným obmedzeniam ich patentovania. Väčšina znení použitých v
direktíve neslúži na žiadny účel, len oberá ministrov a členov
parlamentu o ich rozum.

#WWn: V rozhodovaní Rady dominoval vplyv služobných zamestnancov, ktorí
riadia EPO a robia nátlak proti vôli národných parlamentov. Počas
tohto procesu sa Rada aj Komisia stali nositeľmi bezprecedentného
nedemokratického právneho postupu v EU.

#Fea: Z týchto dôvodov vyzývame zodpovedných účastníkov legislatívneho
procesu, aby vykonali nasledovné.

#eon: Žiadame Radu Európy, aby vysvetlila svoj postoj v otázke softwarových
patentov zo dňa 7.marca 2005, formou zodpovedania %(kv:23 otázok)
FFII.

#ltt: Žiadame Európsky patentový úrad, aby okamžite zastavil schvaľovanie
patentov a vrátil sa ku korektnej interpretácii %(ar:Článku 52
Európskej Patentovej Konvencie), tak ako sa všeobecne aplikovala v EPO
%(eb:pred) %(vc:1986).

#fWl: Žiadame členov národných parlamentov, aby zobrali rozhodovanie o
legislatíve patentovania do vlastných rúk. V krátkej dobe, musia
vyslať jasné signály, ktoré zabránia národným patentovým úradom a
súdom schvaľovať %(pk:patenty na programy).

#lep: Žiadame členov národných parlamentov, aby kontrolovali Radu Európy.
Aspoň politické dohody musia podliehať ratifikácii parlamentov a to 
špeciálne v prípadoch, keď obsahujú dodatky na poslednú chvíľu.

#aWa: Žiadame členov Európskeho parlamentu, aby %(p5:upevnili jasný postoj
Parlamentu zo Septembra 2003), vrátane %(pa:všetkého, čo je potrebné
na ukončenie vyhýbaniu sa dohovoru), prípadne zlyhanie dohovoru,
zamietnutím celej direktívy.

#Sno: signatári

#agr: hlavní signatári

#Wfm: sa ešte májú vyzbierať, a ako to má približne vyzerať sa pozrite vo
výzvach %(LST) Ak chcete byť zverejnený ako hlavný signatár, prosím
kontaktujte %(CH). Ak nám chcete pomôcť pri výbere hlavných
signatárov, prosím prihláste sa do %(cp:project forum).

#rgr: ďalšie podpisy

#gcr: %(N) osôb doteraz podpísalo túto výzvu cez %(ps:FFII system).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl-cpedu.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cpedu0504 ;
# txtlang: xx ;
# End: ;

