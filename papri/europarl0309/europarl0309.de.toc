\contentsline {section}{\numberline {1}Richtlinie \"{u}ber die Patentierbarkeit computer-implementierter Erfindungen}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Artikel 1: Zweck}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Artikel 2: Definitionen}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Artikel 3a: Gebiete der Technik}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Artikel 4: Regeln der Patentierbarkeit}{2}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Artikel 4a: Ausschluss von der Patentierbarkeit}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Artikel 5: Anspruchsformen und weitere Bestimmungen}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Artikel 6: Interoperabilit\"{a}t}{4}{subsection.1.7}
\contentsline {section}{\numberline {2}Annotated Links}{4}{section.2}
