<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Nokias "Aufruf zum Handeln" %(pe:Call for Action), 4. April 2004

#descr: Nokias Patentabteilung sammelt aktuell Unterschriften von technischen Führungskräften zur Unterstützung des %(q:Arbeitspapiers) der %(q:Arbeitsgruppe zu Geistigem Eigentum) des EU-Rats. Dabei handelt es sich um eine Reihe von Patentbeamten, die das Europäische Patentamt leiten. Der Brief beschreibt das Arbeitspapier als einen %(q:ausgewogenen Kompromiss), der %(q:die Bedenken des Parlaments berücksichtigt) und sieht es ansonsten für nötig an, um die Patentierbarkeit von Haushaltsgeräten, Medizintechnik und ähnlichem zu erhalten. Damit täuscht der Brief sowohl seine Unterzeichner, Technische Manager, die üblicherweise die Direktivenvorschläge nicht gelesen haben, als auch seine Leser.

#LRI: AUFRUF ZUM HANDELN

#WWn: %(nl|Vorschlag zu einer Direktive über die Patentierbarkeit von computer-implementierten Erfindungen|Gemeinsame Verlautbarung)

#iWn: Als wichtige innovative Technologiefirmen in Europa fordern wir die Minister auf, auf der Ratstagung im Mai eine gemeinsame Position zum Vorschlag einer europäischen Direktive über die Patentierbarkeit von computer-implementierten Erfindungen zu verabschieden. Der Vorschlag der irischen Ratspräsidentschaft vom 17. März 2004 verdient die Unterstützung aller Minister des Rates.

#WeW: Wir begrüßen, dass die irische Präsidentschaft einen ausgewogenen Text vorgelegt hat. Es wird sowohl der Anreiz für Innovationen in Europa auf so unterschiedlichen Gebieten wie der Telekommunikation, der Informationstechnologie, Konsumgütern, Haushaltsgegenständen oder medizinischen Gerätschaften aufrecht erhalten als auch der Forderung des Europäischen Parlaments Rechnung getragen, eine Ausweitung der Patentierbarkeit auf nicht-technische Bereiche oder übermäßige Behinderung des Datenaustausches in unserer zunehmend vernetzten Welt zu vermeiden.

#isW: Ohne eine ausgewogene gemeinsame Position, wie sie von der irischen Ratspräsidentschaft im März 2004 vorgeschlagen wurde, wird die Innovation in Europa schwer getroffen werden. Weit reichende Ausnahmen für Datenverarbeitungstechnologien, Informationsverwaltung oder für Techniken der sogenannten %(q:Konvertierung von Konventionen) [gemeint ist wohl das Interoperabilitätsprivileg, Anm.d.Ü.] würden sehr viele bereits erteilte Patente in Europa nicht durchsetzbar machen. Dies würde Lizenzabkommen entwerten, die wir mit anderen Firmen abgeschlossen haben. Viele dieser Firmen sitzen dabei außerhalb der Europäischen Union und unterliegen einer Rechtssprechung, die die Patentierbarkeit von computer-implementierten Erfindungen zulässt. Dies verschiebt die Balance von Europa weg, macht unser Geschäft risikoreicher und reduziert das Potential für Gewinne aus Lizenzabgaben, die momentan europäischen Firmen zu Gute kommen und helfen, fortlaufend Forschung und Entwicklung zu finanzieren.

#ien2: Alle innovativen Kräfte in Europa, seien es einzelne Erfinder, kleine oder mittelständische Unternehmen %(pe:KMUs) oder auch große multinationale Konzerne, benötigen Patente um ihre Erfindungen zu schützen, einen Anreiz für Forschung und Entwicklung in Europa zu haben und Lizenzierungsmodelle und Technologietransfer zu befördern.

#rhe: Ihr Einsatz ist nötig, damit Innovation in Europa gesichert ist, sodass die Gesellschaft auch morgen noch vom technologischen Fortschritt profitieren kann, sei es in Handys oder sei es in Autos, die immer mehr auf computer-implementierte Erfindungen angewiesen sind. Gefährden Sie nicht das ambitionierte Ziel, Europa zur %(q:stärksten und dynamischsten wissensbasierten Wirtschaftskraft) im Jahr 2010 zu machen.

#ONO: Firmenlogos

#dsu: und Unterschriften der Technischen Geschäftsführer und Manager (CTOs)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl0309.el ;
# mailto: mlhtimport@a2e.de ;
# login: dhillbre ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: nokia0404 ;
# txtlang: de ;
# multlin: t ;
# End: ;

