<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#LRI: CALL FOR ACTION

#WWn: %(nl|Proposed Directive on Patentability of Computer-Implemented
Inventions|Joint Statement)

#iWn: As some of Europe's most innovative technology companies, we call upon
ministers at the May Competitiveness Council to adopt a Common
Position on the proposed EU directive on the patentability of
computer-implemented inventions.  The 17 March 2004 Consolidated text
proposed by the Irish Council Presidency deserves the support of all
Ministers in the Competitiveness Council.

#WeW: We commend the Irish Presidency for presenting a balanced text which
preserves the incentives for European innovation in sectors as diverse
as telecommunications, information technology, consumer electronics,
household appliances, transportation and medical instruments while
responding to the European Parliament's call for limitations to ensure
that patentability does not extend into non-technical areas or unduly
hinder interoperability in our increasingly networked society.

#isW: Failure to adopt a balanced Common Position as proposed by the Irish
Presidency in March 2004 will undermine innovation in Europe. 
Broad-based exclusions for data processing technologies, information
handling, or techniques used for the so-called %(q:conversion of
conventions) would make very many patents already granted in Europe
unenforceable.  This would undermine licence agreements we have with
other companies, many of whom are based outside the European Union in
jurisdictions where computer-implemented inventions will remain
patentable, shifting the balance away from European companies, making
our businesses more risky, and reducing the potential for return on
investment from royalty revenues which currently flow to European
companies and which help fund continuing research and development
activities.

#ien2: All of Europe's innovators, including individual inventors, small and
medium size enterprises (SMEs), as well as large multinational
companies, require patents to protect their inventions, provide
incentives to undertake research and development in Europe, and to
promote licensing and technology transfer.

#rhe: Your action is required to safeguard innovation in Europe, so that
society may continue to benefit from technology advances in products
as diverse as mobile telephones and cars which increasingly rely on
computer-implemented inventions and to avoid jeopardising the
ambitious Lisbon goal of making Europe the %(q:most competitive and
dynamic knowledge based economy) by 2010.

#ONO: COMPANY LOGOS

#dsu: and CTO signatures

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: nokia0404 ;
# txtlang: en ;
# multlin: t ;
# End: ;

