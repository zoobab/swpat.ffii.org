<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#bed: Absender

#WAl: MAG. OTHMAR KARAS  M.B.L.HSG

#mve: Empfänger

#ife: %(nl|Dr. Martin Bartenstein|Bundesminister für Wirtschaft und
Arbeit|Stubenring 1A1010 Wien)

#riW: Gleichlautender Brief erging an Wettbewerbskommissar Mario Monti und
Botschafter Dr. Gregor Woschnagg

#rne: Ort und Zeit

#sWl: Brüssel, den 20. April 2004

#erf: Betreff

#nkt: EU-Richtlinie über die Patentierbarkeit computerimplementierter
Erfindungen

#Wei: Sehr geehrter Herr Bundesminister,

#lnW: Ich teile die Sorge vieler Entwickler, daß durch eine Patentierbarkeit
von Software Codes die Interessen der kleinen und mittleren
ITUnternehmen nachhaltig geschädigt werden könnten. Nicht zuletzt auf
Grund der zahlreichen engagierten Schreiben von Betroffenen aus der
Branche bin ich mir der Problematik im Zusammenhang mit einer
möglichen Erteilung von Patenten auf computerimplementierte
Erfindungen durchaus bewusst. Ich habe jeden Brief und jedes Gespräch
sehr ernst genommen. Grundsätzlich ist eine Richtlinie, die die
einheitliche Rechtsanwendung durch Patentämter und Patentgerichte
regelt, im Interesse eines funktionierenden Binnenmarktes, zur
Schaffung von Rechtssicherheit und zur Vermeidung von
Wettbewerbsverzerrungen zu begrüßen. Dies ist schon alleine deshalb
ein wichtiger Schritt, um der gegenwärtigen Praxis des Europäischen
Patentamts, welches bereits entgegen des Wortlauts des Artikels 52 des
Europäischen Patentübereinkommens nahezu 30.000 Patente auf reine
Software erteilt hat, einen Riegel vorzuschieben. Die bisherige Praxis
in den USA und des Europäischen Patentamtes macht es tatsächlich
notwendig, zu vernünftigen Regelungen zu kommen, die es auch den KMUs
ermöglichen, an der Innovation und Entwicklung teilzunehmen, ohne
durch hohe Gebühren oder duch ungerechtfertigte Monopolstellungen
gehindert zu werden.

#enW: In der Praxis führen Softwarepatente zu unüberwindbaren bürokratischen
Hindernissen. Es ist weder einsichtig noch praktizierbar, dass ein nur
technisch, nicht jedoch rechtlich geschulter Programmierer die
Patentdatenbanken der Mitgliedstaaten durchforsten muss, um  ausfindig
zu machen, ob ein von ihm entwickeltes Verfahren bereits von Dritten
geschützt worden ist. Der Urheberschutz von Software bietet genügend
Rechtssicherheit und Schutz, um die missbräuchliche Verwendung der
Kreativität von Unternehmen zu verhindern. Daher lehne ich die
Patentierbarkeit von Code grundsätzlich ab, wie dies leider in vielen
Fällen mangels konkreter Regelungen und vermutlich mangelnder
Überprüfung der Patentanmeldungen bereits geschehen ist. Äußerst
problematisch erscheint mir in diesem Zusammenhang die Finanzierung
des Patentwesens, welche Gebühren hauptsächlich für erteilte Patente
vorsieht. Aufgrund dieser Tatsache gibt es einen massiven Anreiz für
Patentämter, Anträge auf Patente positiv zu beurteilen, um die eigene
Finanzierung sichern zu können. Dies öffnet der Erteilung von
Trivialpatenten Tür und Tor. Folglich wäre eine neutrale Finanzierung
des Patentwesens anzustreben. Die Quantität der erteilten Patente sagt
nichts über die Innovations und Wettbewerbsfähigkeit eines
Wirtschaftsraumes aus  diese Aussage in den Lissabon Zielen ist stark
anzuzweifeln. Alleine die Qualität der individuellen Leistung sollte
im Vordergrund stehen. Informationsgesellschaft bedeutet nicht
Informationsmonopol. Da das Patentwesen auf territorialen Rechten
beruht, ist die Argumentation des Wettbewerbsnachteils gegenüber
dritten Staaten, allen voran den USA oder Asien, nicht
nachzuvollziehen. Gäbe es im Territorium der EU keine Softwarepatente,
könnten folglich auch keine Unternehmen aus Drittstaaten
Softwarepatenten für den EURaum erlangen. Auf der anderen Seite sind
europäische Unternehmen bereits jetzt nicht daran gehindert, Patente
für Software in Drittstaaten zu beantragen und zu bekommen. Ich trete
im Interesse der europäischen Wirtschaft für eine klare und leicht
verständliche Regelung im Rahmen einer neuen Direktive ein, die einer
allgemeinen Patentierung von Software entgegen tritt. Deshalb bitte
ich dich, dich für eine entsprechende Regelung im Gemeinsamen
Standpunkt stark zu machen!

#tes: Mit besten Grüssen,

#mrn: Bundesministerium für Wirtschaft und Arbeit

#tde: untersteht dem Hauptaddressaten dieses Briefes

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: okaras0404 ;
# txtlang: de ;
# multlin: t ;
# End: ;

