<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mpW: The amendments approved, from the EP.

#sdn: consolidated version by EP, changes were still being applied by the
secretariat during the days after the vote.  E.g. %(q:contribute) in
%(q:technical features that any invention must contribute) was changed
to %(q:posess).

#WMW: Europarl 03-09-24: Arlene McCarthy's summary of the vote

#fth: Mentions mainly the parts of the vote that are congruent with her own
proposals and unimportant aspects, such as the novelty grace period,
but omits everything that really limits patentability.

#purp: Purpose

#defi: Definitions

#efn: Fields of Technology

#eai: Rules of Patentability

#uoa: Causas de exclusión de la patentabilidad

#ono: Form of Claims; and further provisions

#tel: Interoperability

#mon: Monitoring

#rep: Report on the effects of the Directive

#ass: Impact assessment

#titl: Directive on the patentability of computer-implemented inventions

#Art1: This Directive lays down rules for the patentability of
computer-implemented inventions.

#Am36: %(q:invención implementada en ordenador), toda invención en el sentido
del Convenio sobre la Patente Europea para cuya ejecución se requiera
la utilización de un ordenador, una red informática u otro aparato
programable y que tenga en sus aplicaciones una o más características
no técnicas que se realicen total o parcialmente mediante un programa
o programas de ordenador, sin perjuicio de las características
técnicas que debe presentar toda invención;

#Am107: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in technical field. The technical
character of the contribution is one of the four requirements for
patentability. Additionally, to deserve a patent, the technical
contribution has to be new, non-obvious, and susceptible of industrial
application. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes.

#Am97: %(q:technical field) means an industrial application domain requiring
the use of controllable forces of nature to achieve predictable
results. %(q:Technical) means %(q:belonging to a technical field).

#Am38: %(q:industria) en el sentido del Derecho de patentes, %(q:producción
automática de bienes materiales);

#Am45: Member states shall ensure that data processing is not considered to
be a field of technology in the sense of patent law, and that
innovations in the field of data processing are not considered to be
inventions in the sense of patent law.

#Am16N1: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and involve an inventive
step. In order to involve an inventive step, a computer-implemented
invention must make a technical contribution.

#Am16N2: Member States shall ensure that a computer-implemented invention
making a technical contribution constitutes a necessary condition of
involving an inventive step.

#Am100P1: The significant extent of the technical contribution shall be assessed
by consideration of the difference between the technical elements
included in the scope of the patent claim considered as a whole and
the state of the art, irrespective of whether or not such features are
accompanied by non-technical features.

#Am70: In determining whether a given computer-implemented invention makes a
technical contribution, the following test shall be used: whether it
constitutes a new teaching on cause-effect relations in the use of
controllable forces of natures and has an industrial application in
the strict sense of the expression, in terms of both method and
result.

#Am17: No se considerará que una invención implementada en ordenador aporta
una contribución técnica meramente porque implique el uso de un
ordenador, red u otro aparato programable. En consecuencia, no serán
patentables las invenciones que utilizan programas informáticos que
aplican métodos comerciales, matemáticos o de otro tipo y no producen
efectos técnicos, aparte de la normal interacción física entre un
programa y el ordenador, red o aparato programable de otro tipo en que
se ejecute.

#Am60: Los Estados miembros garantizarán que las soluciones a problemas
técnicos implementadas en ordenador no se consideren como invenciones
patentables simplemente por el hecho de que mejoran la eficiencia en
la utilización de los recursos dentro del sistema de tratamiento de
datos.

#Am101: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is as a programmed device, or as a
technical production process.

#Wce: Member States shall ensure that patent claims granted in respect of
computer-implemented inventions include only the technical
contribution which justifies the patent claim. A patent claim to a
computer program, either on its own or on a carrier, shall not be
allowed.

#Am103: Member States shall ensure that the production, handling, processing,
distribution and publication of information, in whatever form, can
never constitute direct or indirect infringement of a patent, even
when a technical apparatus is used for that purpose.

#Am104N1: Member States shall ensure that the use of a computer program for
purposes that do not belong to the scope of the patent cannot
constitute a direct or indirect patent infringement.

#Am104N2: Member States shall ensure that whenever a patent claim names features
that imply the use of a computer program, a well-functioning and well
documented reference implementation of such a program is published as
part of the patent description without any restricting licensing
terms.

#Am19: The rights conferred by patents granted for inventions within the
scope of this Directive shall not affect acts permitted under Articles
5 and 6 of Directive 91/250/EEC on the legal protection of computer
programs by copyright, in particular under the provisions thereof in
respect of decompilation and interoperability.

#Am76P1: Los Estados miembros garantizarán que, en cualquier caso en que sea
necesaria la utilización de una técnica patentada con un propósito
significativo, como es asegurar la conversión de las convenciones
utilizadas en dos sistemas de ordenadores o redes diferentes a fin de
hacer posible la comunicación y el intercambio recíproco del contenido
de datos, esta utilización no se considere una violación de la
patente.

#Am71: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, and on European businesses, especially small and
medium-sized enterprises and the open source community, and electronic
commerce.

#Art81: The Commission shall report to the European Parliament and the Council
by [DATE (three years from the date specified in Article 9(1))] at the
latest on

#Art81a: the impact of patents for computer-implemented inventions on the
factors referred to in Article 7;

#Am92: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims, are adequate;
and

#Art82c: whether difficulties have been experienced in respect of Member States
where the requirements of novelty and inventive step are not examined
prior to issuance of a patent, and if so, whether any steps are
desirable to address such difficulties.

#Am23: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-implemented
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-implemented
inventions;

#Am24: whether it would be desirable and legally possible having regard to
the Community's international obligations to introduce a 'grace
period' in respect of elements of a patent application for any type of
invention disclosed prior to the date of the application;

#Am25: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the Convention on the Grant of
European Patents, also in the light of the advent of the Community
patent;

#Am26: how the requirements of this Directive have been taken into account in
the practice of the European Patent Office and in its examination
guidelines.

#Am81: whether the powers delegated to the EPO are compatible with the need
to harmonise Community legislation, and with the principles of
transparency and accountability.

#Am89: the impact on the conversion of the conventions used in two different
computer systems to allow communication and exchange of data;

#Am93: whether the option outlined in the Directive concerning the use of a
patented invention for the sole purpose of ensuring interoperability
between two systems is adequate;

#Am94: In this report the Commission shall justify why it believes an
amendment of the Directive in question necessary or not and, if
required, will list the points which it intends to propose an
amendment to.

#Am27: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
assess the impact of this Directive and, where necessary, submit
proposals for amending legislation to the European Parliament and the
Council.

#rFr: CEC proposal and FFII counter-proposal.

#nin: Amendment proposals on which the EP voted on 2003/09/24

#ent: Amendment Proposals on which JURI voted in June

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl0309 ;
# txtlang: es ;
# multlin: t ;
# End: ;

