# -*- mode: makefile -*-

aipla0310.en.lstex:
	lstex aipla0310.en | sort -u > aipla0310.en.lstex
aipla0310.en.mk:	aipla0310.en.lstex
	vcat /ul/prg/RC/texmake > aipla0310.en.mk


aipla0310.en.dvi:	aipla0310.en.mk
	rm -f aipla0310.en.lta
	if latex aipla0310.en;then test -f aipla0310.en.lta && latex aipla0310.en;while tail -n 20 aipla0310.en.log | grep -w references && latex aipla0310.en;do eval;done;fi
	if test -r aipla0310.en.idx;then makeindex aipla0310.en && latex aipla0310.en;fi

aipla0310.en.pdf:	aipla0310.en.ps
	if grep -w '\(CJK\|epsfig\)' aipla0310.en.tex;then ps2pdf aipla0310.en.ps;else rm -f aipla0310.en.lta;if pdflatex aipla0310.en;then test -f aipla0310.en.lta && pdflatex aipla0310.en;while tail -n 20 aipla0310.en.log | grep -w references && pdflatex aipla0310.en;do eval;done;fi;fi
	if test -r aipla0310.en.idx;then makeindex aipla0310.en && pdflatex aipla0310.en;fi
aipla0310.en.tty:	aipla0310.en.dvi
	dvi2tty -q aipla0310.en > aipla0310.en.tty
aipla0310.en.ps:	aipla0310.en.dvi	
	dvips  aipla0310.en
aipla0310.en.001:	aipla0310.en.dvi
	rm -f aipla0310.en.[0-9][0-9][0-9]
	dvips -i -S 1  aipla0310.en
aipla0310.en.pbm:	aipla0310.en.ps
	pstopbm aipla0310.en.ps
aipla0310.en.gif:	aipla0310.en.ps
	pstogif aipla0310.en.ps
aipla0310.en.eps:	aipla0310.en.dvi
	dvips -E -f aipla0310.en > aipla0310.en.eps
aipla0310.en-01.g3n:	aipla0310.en.ps
	ps2fax n aipla0310.en.ps
aipla0310.en-01.g3f:	aipla0310.en.ps
	ps2fax f aipla0310.en.ps
aipla0310.en.ps.gz:	aipla0310.en.ps
	gzip < aipla0310.en.ps > aipla0310.en.ps.gz
aipla0310.en.ps.gz.uue:	aipla0310.en.ps.gz
	uuencode aipla0310.en.ps.gz aipla0310.en.ps.gz > aipla0310.en.ps.gz.uue
aipla0310.en.faxsnd:	aipla0310.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo aipla0310.en-??.g3n`;source faxsnd main
aipla0310.en_tex.ps:	
	cat aipla0310.en.tex | splitlong | coco | m2ps > aipla0310.en_tex.ps
aipla0310.en_tex.ps.gz:	aipla0310.en_tex.ps
	gzip < aipla0310.en_tex.ps > aipla0310.en_tex.ps.gz
aipla0310.en-01.pgm:
	cat aipla0310.en.ps | gs -q -sDEVICE=pgm -sOutputFile=aipla0310.en-%02d.pgm -
aipla0310.en/aipla0310.en.html:	aipla0310.en.dvi
	rm -fR aipla0310.en;latex2html aipla0310.en.tex
xview:	aipla0310.en.dvi
	xdvi -s 8 aipla0310.en &
tview:	aipla0310.en.tty
	browse aipla0310.en.tty 
gview:	aipla0310.en.ps
	ghostview  aipla0310.en.ps &
print:	aipla0310.en.ps
	lpr -s -h aipla0310.en.ps 
sprint:	aipla0310.en.001
	for F in aipla0310.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	aipla0310.en.faxsnd
	faxsndjob view aipla0310.en &
fsend:	aipla0310.en.faxsnd
	faxsndjob jobs aipla0310.en
viewgif:	aipla0310.en.gif
	xv aipla0310.en.gif &
viewpbm:	aipla0310.en.pbm
	xv aipla0310.en-??.pbm &
vieweps:	aipla0310.en.eps
	ghostview aipla0310.en.eps &	
clean:	aipla0310.en.ps
	rm -f  aipla0310.en-*.tex aipla0310.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} aipla0310.en-??.* aipla0310.en_tex.* aipla0310.en*~
aipla0310.en.tgz:	clean
	set +f;LSFILES=`cat aipla0310.en.ls???`;FILES=`ls aipla0310.en.* $$LSFILES | sort -u`;tar czvf aipla0310.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
