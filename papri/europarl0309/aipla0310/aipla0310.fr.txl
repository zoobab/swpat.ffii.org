<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#tsa: Le Parlement européen a récemment voté la directive proposée sur  les
brevets logiciels. Le texte, tel qu'amendé (une copie est jointe),  
apparaît complètement éliminer tout brevetage logiciel et rendre  
inapplicable la plupart des brevets existants.

#iuW: La formulation de ce projet de directive implique une future  brèche 
dans le traité GATT/ADPIC puisque la technologie logicielle  serait 
exclue de la protection par brevet. Un considérant confirmant le 
traité  GATT/ADPIC (%(r6:le considérant 6 original)) a été supprimé
par  le  Parlement.

#eWr: La directive amendée n'implique aucune brèche dans l'ADPIC.  Au  
contraire, elle fonde l'exclusion des brevets logiciels sur la
doctrine   que l'invention technique est étroitement liée à l'usage
des forces de  la  nature et que %(e:le traitement de données n'est
pas un domaine de   technologie au sens de l'article 27 de l'ADPIC). 
Cette doctrine   imprègne la directive telle qu'amendée par le
Parlement.  Elle est   déclarée explicitement dans plusieurs de ses
articles et considérants.    En conséquence, le considérant 6 original
est devenu redondant.

#mWf: L'influence apparente de la communauté open source sur les membres  
du Parlement et l'apparent manque de compréhension général du
Parlement   sur les avancées commerciales et technologiques résultant
de   l'utilisation actuelle et prédite des inventions relatives aux  
ordinateurs, sont vraiment remarquables et illustrent la nature  
politique des problèmes. Comme le remarquait récemment l'un de mes
amis   européens : %(q:la question de la brevetabilité des inventions 
relatives  au logiciel et des méthodes pour l'exercice d'activités 
économiques  mises en oeuvre par logiciel ne doit pas être décidée en 
fonction  des lois existantes, mais en fonction des besoins sociaux. 
Ceci est un  problème social - ou politique - avant d'être un problème
 juridique.)

#c43: Comme les amendements eux-mêmes, les plus onéreux pour moi sont  les 
amendements %(am:32) (qui est juste incorrect); %(am:95) (qui  ferait 
fermer l'OEB); %(am:84); %(a2:tous les amendements de l'article  2)
(qui  réfuterait toute invention relative au logiciel); %(am:45) (n'a 
pas de  sens si l'on considère les milliards de dollars/euro investis 
dans le  marché des finance/banque/action et les industries liées à
ces   services); %(am:70); %(am:60); %(am:102) et %(am:111); %(am:72);
  %(am:103) et %(am:119); %(am:104) et %(am:120); %(am:76) (l'article
6a   reformulé est toujours en soi, destructeur de la plupart des
brevets   logiciels existants).

#dtW: Mes sources à la Commission essaient de déterminer ce qu'il faut  
faire.  Elles pensent que le Conseil rejettera cette version et la  
renverra au Parlement en seconde lecture (vote) mais la question est
de   savoir si elle sera même sauvée .  Je pense que notre espoir
vient de  ce  qu'elle sera simplement enterrée ou retirée par la
Commission.

#Woi: Traite d'un rapport plus récent de E. Basinski dans lequel il  essaie 
de mobiliser l'argument peur-incertitude-doute de l'ADPIC  contre 
l'article 6a (amendement sur l'interopérabilité).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: aipla0310 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

