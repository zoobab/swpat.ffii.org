<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#tsa: The EU Parliament recently voted on the Proposed Software Directive.
The text as amended (a copy is attached) appears to completely
eliminate any software patent and make unenforceable most existing
patents.

#iuW: The wording of this draft further implies a breach of the
GATT/TRIPS-Treaty since software technology would be excluded from
patent protection. A recital confirming the GATT/TRIPS-Treaty
(%(r6:Original Recital 6)) has been deleted by the Parliament.

#eWr: The amended directive implies no breach of TRIPs.  On the contrary, it
bases the exclusion of software patents on the doctrine that technical
inventing is closely related to the use of forces of nature and that
%(e:data processing is not a field of technology in the sense of Art
27 TRIPs).  This doctrine pervades the directive as amended by the
Parliament.  It is stated explicitely in several of its articles and
recitals.  Thus the original recital 6 has been rendered redundant.

#mWf: The apparent influence of the open source community on the members of
Parliament and the Parliament's general apparent lack of understanding
of the technological and business advances resulting from the current
and predicted use of computer related inventions, are truly remarkable
and illustrate the political nature of the problems. As one of my
European friends remarked recently:  %(q:the question of the
patentability of software related inventions and software implemented
business methods does not have to be decided as a function of the
existing laws, but as a function of social needs. This is a social -
or political - problem before being a legal problem.)

#c43: As to the amendments themselves, to me the most onerous are %(am:32)
(which is just incorrect); %(am:95) (which would put the EPO out of
business); %(am:84); %(a2:all of the amendments to Article 2) (which
would negate any software related invention); %(am:45) (makes no sense
when you consider the billions of dollars/euros invested in the
financial/banking/stock market and related industries to make those
services function); %(am:70); %(am:60); %(am:102) and %(am:111);
%(am:72); %(am:103) and %(am:119); %(am:104) and %(am:120); %(am:76)
(reworded Article 6a is still destructive of most existing software
patents by itself).

#dtW: My sources on the Commission are trying to determine what to do.  They
believe the Council will reject this version and send it back to
Parliament for a second reading (vote) but the question is whether it
can be saved at all.  I think our hope is it will either just die or
be withdrawn by the Commission.

#Woi: Discusses an earlier report from E. Basinski in which he tries to
mobilise the TRIPs FUD argument against Art 6a (interoperability
amendment).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: aipla0310 ;
# txtlang: en ;
# multlin: t ;
# End: ;

