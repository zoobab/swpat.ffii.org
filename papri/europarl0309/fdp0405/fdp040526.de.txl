<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FDP fordert Bundestagsentscheidung gegen Softwarepatente, für Europäisches Parlament

#descr: Die Fraktion der Freien Demokratischen Partei (FDP) fordert in einem Entschließungsantrag die Abgeordneten des Deutschen Bundestages auf, die Entscheidung des Europäischen Parlamentes zur Begrenzung der Patentierbarkeit im Hinblick auf %(q:computer-implementierte) Algorithmen und Geschäftsmodelle zu unterstützen und die Bundesregierung aufzufordern, ihre Fehlentscheidung im EU-Rat zu revidieren.  Es liegt nun an den Regierungsfraktionen, den Antrag der FDP unterstützen und die wackelnde %(q:qualifizierte Mehrheit) im Rat endgültig zu kippen.

#W2W: Mit Schreiben vom 22. Februar 2002 hat die Kommission dem Europäischen Parlament gemäß Art. 251 Abs. 2 und Art. 95 des EG-Vertrages den Vorschlag für eine %(q:Richtlinie des Europäischen Parlaments und des Rates über die Patentierbarkeit computerimplementierter Erfindungen) (KOM(2002)92 2002/0047 (COD)) unterbreitet. Dieser Entwurf wies gravierende Mängel auf, denn er hätte im Wesentlichen die rechtlich fragwürdige Praxis des Europäischen Patentamtes kodifiziert, auf deren Grundlage in den letzten Jahren zahlreiche triviale und breite Patente auf Algorithmen und Geschäftsmethoden erteilt worden sind.

#rnW: Software soll laut Art. 10 des %(q:Agreement on Trade-Related Aspects of Intellectual Property Rights) (TRIPs) nach den Regeln des Urheberrechts geschützt werden. Der urheberrechtliche Schutz von Computerprogrammen wird durch die Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz von Computerprogrammen (ABl. Nr. L 122, S. 42 ff.)  gewährleistet, deren Vorgaben in Deutschland durch die §§ 69 a ff. des Urheberrechtsgesetzes umgesetzt worden sind. Ein patentrechtlicher Ideenschutz ist mit dem durch das Urheberrechtsgesetz gewährten Formenschutz schwer vereinbar. Computerprogramme sind dementsprechend nach den geltenden Bestimmungen des Europäischen Patentübereinkommens (EPÜ) %(q:als solche) (ebenso wie Geschäftsmodelle) von der Patentierbarkeit ausgenommen. Das deutsche Recht enthält in Übereinstimmung mit dem EPÜ eine entsprechende Bestimmung in § 1 Abs. 2 und 3 des Patentgesetzes (PatG). An diesen Grundsätzen ist unbedingt festzuhalten.

#Wes: Das Europäische Parlament hat nach intensiven und kontroversen Beratungen im September 2003 einen Richtlinienentwurf in erster Lesung gebilligt, der aufgrund zahlreicher Änderungen gegenüber dem ursprünglichen Entwurf der Kommission einen sachgerechten Kompromiss darstellt. Die Änderungsanträge des Europäischen Parlaments betreffen insbesondere die Bekräftigung der Publikationsfreiheit, die Absicherung der Interoperationsfreiheit, die Definition der von Art. 27 TRIPs vorgegebenen Schlüsselbegriffe %(q:Technik) und %(q:Industrie), sowie die Klarstellung, dass die Datenverarbeitung kein %(q:Gebiet der Technik) im Sinne von Art. 27 TRIPs ist. Außerdem ist in der Richtlinie nach dem Willen des Europäischen Parlaments zu definieren, dass mit dem Begriff %(q:computerimplementierte Erfindung) Erfindungen im Sinne des Patentrechts gemeint sind, bei denen der Computer nur ein Mittel zur Implementierung ist und die eigentliche Leistung auf dem Gebiet der Technik (d.h. der angewandten Naturwissenschaft) liegt.

#alo: Die Arbeitsgruppe des Rates hat im Januar 2004 unter der Bezeichnung %(q:Kompromissvorschlag der Präsidentschaft) einen eigenen Entwurf vorgelegt, der das Votum des Europäischen Parlaments zurückweist und auf die genannten notwendigen Elemente verzichtet. Im Gegensatz zur Version des Europäischen Parlaments erlaubt die Version des Rates deshalb eine unbegrenzte Patentierbarkeit und Patentdurchsetzbarkeit in Bezug auf Software. Der Entwurf des Rates fällt damit weit hinter den vom Parlament gebilligten Kompromiss zurück und wird den Anforderungen, die an den Regelungsgehalt der Richtlinie zu stellen sind, deshalb nicht gerecht.

#iit: Die Bundesregierung hat sich in der Sitzung des Ministerrates am 18. Mai 2004 entgegen ihrer ursprünglichen Ankündigungen gegen den Entwurf des Europäischen Parlaments ausgesprochen und den Vorschlag der Ratspräsidentschaft unterstützt. Dieser Meinungswechsel hat maßgeblich dazu beigetragen, dass der Vorschlag der Ratspräsidentschaft im Ministerrat beschlossen wurde. Das Votum der Bundesregierung stellt eine wettbewerbs- und wirtschaftspolitische Fehlentscheidung dar, die der Bundestag missbilligt.

#zdi: Der Deutsche Bundestag fordert die Bundesregierung auf, im weiteren Verfahren zur Verabschiedung der %(q:Richtlinie des Europäischen Parlaments und des Rates über die Patentierbarkeit computerimplementierter Erfindungen) ihre Entscheidung vom 18. Mai 2004 für den Vorschlag der Ratspräsidentschaft zu revidieren und in der Frage der Grenzen der Patentierbarkeit von Computerprogrammen statt dessen die Position des Europäischen Parlamentes, wie sie sich aus dessen Abstimmungsergebnis vom 24. September 2003 ergibt, vollinhaltlich zu unterstützen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/fdp040526.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: fdp040526 ;
# txtlang: de ;
# multlin: t ;
# End: ;

