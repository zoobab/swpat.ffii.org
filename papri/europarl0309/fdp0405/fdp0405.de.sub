\begin{subdocument}{fdp0405}{FDP beantragt Bundestagsvotum gegen Softwarepatente}{http://swpat.ffii.org/papiere/europarl0309/fdp0405/fdp0405.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2004/05/27 von FFII\footnote{http://lists.ffii.org/mailman/listinfo/traduk}}{Die Fraktion der Freien Demokratischen Partei (FDP) fordert in einem Entschlie{\ss}ungsantrag die Abgeordneten des Deutschen Bundestages auf, die Entscheidung des Europ\"{a}ischen Parlamentes zur Begrenzung der Patentierbarkeit im Hinblick auf ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmodelle zu unterst\"{u}tzen und die Bundesregierung aufzufordern, ihre Fehlentscheidung im EU-Rat zu revidieren.  Es liegt nun an den Regierungsfraktionen, den Antrag der FDP unterst\"{u}tzen und die wackelnde ``qualifizierte Mehrheit'' im Rat endg\"{u}ltig zu kippen.}
\begin{sect}{text}{Der Text}
Mit Schreiben vom 22. Februar 2002 hat die Kommission dem Europ\"{a}ischen Parlament gem\"{a}{\ss} Art. 251 Abs. 2 und Art. 95 des EG-Vertrages den Vorschlag f\"{u}r eine ``Richtlinie des Europ\"{a}ischen Parlaments und des Rates \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen'' (KOM(2002)92 2002/0047 (COD)) unterbreitet. Dieser Entwurf wies gravierende M\"{a}ngel auf, denn er h\"{a}tte im Wesentlichen die rechtlich fragw\"{u}rdige Praxis des Europ\"{a}ischen Patentamtes kodifiziert, auf deren Grundlage in den letzten Jahren zahlreiche triviale und breite Patente auf Algorithmen und Gesch\"{a}ftsmethoden erteilt worden sind.

Software soll laut Art. 10 des ``Agreement on Trade-Related Aspects of Intellectual Property Rights'' (TRIPs) nach den Regeln des Urheberrechts gesch\"{u}tzt werden. Der urheberrechtliche Schutz von Computerprogrammen wird durch die Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 \"{u}ber den Rechtsschutz von Computerprogrammen (ABl. Nr. L 122, S. 42 ff.)  gew\"{a}hrleistet, deren Vorgaben in Deutschland durch die \S{}\S{} 69 a ff. des Urheberrechtsgesetzes umgesetzt worden sind. Ein patentrechtlicher Ideenschutz ist mit dem durch das Urheberrechtsgesetz gew\"{a}hrten Formenschutz schwer vereinbar. Computerprogramme sind dementsprechend nach den geltenden Bestimmungen des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) ``als solche'' (ebenso wie Gesch\"{a}ftsmodelle) von der Patentierbarkeit ausgenommen. Das deutsche Recht enth\"{a}lt in \"{U}bereinstimmung mit dem EP\"{U} eine entsprechende Bestimmung in \S{} 1 Abs. 2 und 3 des Patentgesetzes (PatG). An diesen Grunds\"{a}tzen ist unbedingt festzuhalten.

Das Europ\"{a}ische Parlament hat nach intensiven und kontroversen Beratungen im September 2003 einen Richtlinienentwurf in erster Lesung gebilligt, der aufgrund zahlreicher \"{A}nderungen gegen\"{u}ber dem urspr\"{u}nglichen Entwurf der Kommission einen sachgerechten Kompromiss darstellt. Die \"{A}nderungsantr\"{a}ge des Europ\"{a}ischen Parlaments betreffen insbesondere die Bekr\"{a}ftigung der Publikationsfreiheit, die Absicherung der Interoperationsfreiheit, die Definition der von Art. 27 TRIPs vorgegebenen Schl\"{u}sselbegriffe ``Technik'' und ``Industrie'', sowie die Klarstellung, dass die Datenverarbeitung kein ``Gebiet der Technik'' im Sinne von Art. 27 TRIPs ist. Au{\ss}erdem ist in der Richtlinie nach dem Willen des Europ\"{a}ischen Parlaments zu definieren, dass mit dem Begriff ``computerimplementierte Erfindung'' Erfindungen im Sinne des Patentrechts gemeint sind, bei denen der Computer nur ein Mittel zur Implementierung ist und die eigentliche Leistung auf dem Gebiet der Technik (d.h. der angewandten Naturwissenschaft) liegt.

Die Arbeitsgruppe des Rates hat im Januar 2004 unter der Bezeichnung ``Kompromissvorschlag der Pr\"{a}sidentschaft'' einen eigenen Entwurf vorgelegt, der das Votum des Europ\"{a}ischen Parlaments zur\"{u}ckweist und auf die genannten notwendigen Elemente verzichtet. Im Gegensatz zur Version des Europ\"{a}ischen Parlaments erlaubt die Version des Rates deshalb eine unbegrenzte Patentierbarkeit und Patentdurchsetzbarkeit in Bezug auf Software. Der Entwurf des Rates f\"{a}llt damit weit hinter den vom Parlament gebilligten Kompromiss zur\"{u}ck und wird den Anforderungen, die an den Regelungsgehalt der Richtlinie zu stellen sind, deshalb nicht gerecht.

Die Bundesregierung hat sich in der Sitzung des Ministerrates am 18. Mai 2004 entgegen ihrer urspr\"{u}nglichen Ank\"{u}ndigungen gegen den Entwurf des Europ\"{a}ischen Parlaments ausgesprochen und den Vorschlag der Ratspr\"{a}sidentschaft unterst\"{u}tzt. Dieser Meinungswechsel hat ma{\ss}geblich dazu beigetragen, dass der Vorschlag der Ratspr\"{a}sidentschaft im Ministerrat beschlossen wurde. Das Votum der Bundesregierung stellt eine wettbewerbs- und wirtschaftspolitische Fehlentscheidung dar, die der Bundestag missbilligt.

Der Deutsche Bundestag fordert die Bundesregierung auf, im weiteren Verfahren zur Verabschiedung der ``Richtlinie des Europ\"{a}ischen Parlaments und des Rates \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen'' ihre Entscheidung vom 18. Mai 2004 f\"{u}r den Vorschlag der Ratspr\"{a}sidentschaft zu revidieren und in der Frage der Grenzen der Patentierbarkeit von Computerprogrammen statt dessen die Position des Europ\"{a}ischen Parlamentes, wie sie sich aus dessen Abstimmungsergebnis vom 24. September 2003 ergibt, vollinhaltlich zu unterst\"{u}tzen.
\end{sect}

\begin{sect}{bakgr}{Hintergr\"{u}nde}
\begin{itemize}
\item
{\bf {\bf FDP fordert Bundestagsentscheidung gegen Softwarepatente, f\"{u}r Europ\"{a}isches Parlament\footnote{http://swpat.ffii.org/log/04/fdp0527/fdp040527.de.html}}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Original der FDP\footnote{fdp040525.html}}}

\begin{quote}
converted from MsWordDoc to HTML, navigation bar added at the top
\end{quote}
\filbreak

\item
{\bf {\bf Thin Majority of Ministers for Unlimited Patentability, Swayed by Bogus Compromise\footnote{http://swpat.ffii.org/log/04/cons0518/cons040518.en.html}}}

\begin{quote}
The Irish presidency has secured political approval for a new draft of the controversial software patents directive in a meeting of the Council of Ministers on Tuesday, 2004-05-18.  The draft and accompanying press releases are full of statements of good intentions to avoid patents on software and business methods as such, whereas the provisions in the text assure that such items are without any doubt to be treated as patentable inventions in Europe.  The draft owes its majority to a maneuver by the German delegation, which had collected the opposition under its flag, to settle for a bogus amendment in the last minute and take the Poles and Latvians with it.  Representatives of the Netherlands, Hungary, Denmark and France apparently acted in breach of promises given to their parliaments or governments.
\end{quote}
\filbreak

\item
{\bf {\bf Germany, the Trojan Horse of Bolkestein in the Council\footnote{http://swpat.ffii.org/log/04/cons0518/de/ConsDe040518.en.html}}}

\begin{quote}
Had Germany kept its promise to at least abstain, the 37 votes needed to block it would have been achieved, and surpassed by votes from countries like Poland who had apparently been instructed to follow the Germans.
\end{quote}
\filbreak

\item
{\bf {\bf http://kwiki.ffii.org/Fdp040518De}}
\filbreak

\item
{\bf {\bf EU Rat 2004/01/29: ``Kompromissvorschlag der Pr\"{a}sidentschaft'' zu Softwarepatenten\footnote{http://swpat.ffii.org/papiere/europarl0309/cons0401/cons0401.de.html}}}

\begin{quote}
Die Irische Ratspr\"{a}sidentschaft der Europ\"{a}ischen Union hat auf ministerialer Arbeitsebene ein Papier herausgebracht, welches Gegenvorschl\"{a}ge zu den \"{A}nderungsvorschl\"{a}gen des Europ\"{a}ischen Parlamentes zur Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'' enth\"{a}lt.  Im Gegensatz zur Version des Europ\"{a}ischen Parlamentes erlaubt die Version des Rates grenzenlose Patentierbarkeit und Patent-Durchsetzbarkeit.  Gem\"{a}{\ss} der Version des Rates gelten ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmethoden von vorneherein als Erfindungen im Sinne des Patentrechts.   Schon die Ver\"{o}ffentlichung einer funktionsf\"{a}higen Beschreibung einer patentierten Idee stellt laut Ratsvorschlag eine Patentverletzung dar.  Protokolle und Dateiformate k\"{o}nnen direkt patentiert werden und d\"{u}rfen dann ohne eine Lizenz des Patentinhabers nicht verwendet werden.  F\"{u}r den unge\"{u}bten Leser sind diese Implikationen nicht unbedingt sofort ersichtlich.  Wir erkl\"{a}ren, woraus sie sich ergeben.
\end{quote}
\filbreak

\item
{\bf {\bf Freie Demokratische Partei (FDP.de) und Softwarepatente\footnote{http://swpat.ffii.org/akteure/fdp/swpatfdp.de.html}}}

\begin{quote}
Am 25. Mai 2004 stellte die FDP einen Entschlie{\ss}ungsantrag im Bundestag, der sehr klar gegen die Patentierbarkeit von Software und f\"{u}r die Position des Euorp\"{a}ischen Parlamentes in dieser Frage Stellung nimmt.  Die Meinungsbildung in der FDP reflektiert weitgehend die Meinung in Verb\"{a}nden von KMU und Freiberuflern.   In Zeiten geringer Besch\"{a}ftigung mit dem Thema haben Patentanw\"{a}lte (auch eine Freiberuflergruppe) einigen Einfluss.  Auf ihrem D\"{u}sseldorfer Parteitag vom Juni 2001 spricht sich die FDP daf\"{u}r aus, das geistige Eigentum an Programlogik auch k\"{u}nftig ausschlie{\ss}lich \"{u}ber das Urheberrecht zu regeln und kritisiert Bestrebungen, Programme als solche patentierbar zu machen.  Zugleich verspricht sie sich viel von Biopatenten und setzt auf fl\"{a}chendeckenden Einsatz von Kopierschutztechniken an Stelle der GEMA-Geb\"{u}hren.  Die FDP ist neben den Gr\"{u}nen die einzige Partei, die zum Thema Softwarepatente eine Position beschlossen hat.  W\"{a}hrend sich der D\"{u}sseldorfer Beschluss noch als Formelkompromiss deuten l\"{a}sst, fand die FDP im September 2000 und im Fr\"{u}hjahr 2003 unmissverst\"{a}ndliche Worte gegen die Patentierung von Software.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/fdp0405.el ;
% mode: latex ;
% End: ;

