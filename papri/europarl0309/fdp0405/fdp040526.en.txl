<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: DE Liberals 2004-05-26 Ask for Bundestag Vote Against Software Patents, for European Parliament

#descr: The German Liberal Democrats (FDP) have submitted a resolution proposal to the German Federal Parliament (Bundestag) in which they explain in detail why software patents as granted by the European Patent Office are undesirable and incompatible with the written law and how this malpractise should be rectified.  The FDP finds that the European Parliament found an %(q:adequate compromise) in its vote of 2003-09-24, and that this compromise deserves the German government's full support, whereas the Council of ministers agreed on a %(q:misguided economic policy decision) last week.  The resolution calls on the government to withdraw its support from the Council's decision and to declare support for the European Parliament's vote instead.  It is now up to the governing fractions to supports the FDP's resolution and make the Irish presidency lose the qualified majority that is required for the adoption of its patent-maximalist proposal by the heads of state at their next meeting in June.

#W2W: On Sept., 22nd, 2002, the Commission has submitted a proposal for a %(q:Directive of the European Parliament and the Council about patentability of computer-implemented inventions) to the EU Parliament in accordance with art. 251, sec. 2 and art. 95 of the EC treaty (KOM(2002)92 2002/0047 (COD)). This proposal had serious shortcomings, as it mainly just codified the European Patent Office's questionable practice of the last years to grant numerous patents on trivial or broad algorithms and business methods.

#rnW: According to art. 10 of the %(q:Agreement on Trade-Related Aspects of Intellectual Property Rights) (TRIPs), software shell be protected by the means of copyright. This possibility is given by the EU Council's directive 91/250/EWG from May, 14th, 1991 about the legal protection of computer programs (ABl. no. L122, pp. 42 et sqq.), which has been adopted though §§ 69a et sqq. of German copyright law. A patent-based idea protection is hardly compatible with this copyright-based work protection.. Therefore, computer programs (and business methods) %(q:as such) are defined as non patentable in the European Patent Convention (EPC). German patent law contains certain clauses in §1, sec. 2 and 3 PatG in accordance with this regulation. This must continue to be an absolutely definite ruling.

#Wes: After intensive and controverse discussions, the European Parliament has passed a directive proposal in the first reading in September 2003. This proposal includes numerous changes to the original Commission's version leading to an appropriate compromise. The Parliament's amendments endorse publication freedom, ensure interoperability, define the TRIPS' keywords %(q:Technic) and %(q:Industry), and clearify that data processing is not a field of technology. Additionally, the European Parliament demands the directive to define %(q:computer-implemented inventions) as inventions in terms of patent law, i.e. the computer is only a tool for implementation and the real invention is in a field of technology (e.g. applied natural science).

#alo: In January 2004, the EU Council's working party has delivered a new proposal under the title %(q:Compromise of the Presidency). This version rejects the Parliament's vote and abandons the essential parts described above. Contrary to the European Parliament's position, the Council's version allows unlimited patentability and patent enforcement with regard to software. The Council's proposal falls therefore far behind the version approved by the Parliament. It does not comply with the needed regulation requirements in any way.

#iit: In the Ministry Council's meeting on May, 18th, 2004, the German gouvernment has voted against the Parliament's directive proposal and supported the Council's version, contrary to its announcements before. This position change had significant impact on the voting as a whole and lead to the approvement of the Council's proposal. The German gouvernment's vote is a wrong decision, with regard to both economy and competition. The German Parliament disapproves it.

#zdi: The German Parliament requests the government to revide its vote in favor of the Council from May, 18th, 2004. For the rest of process about the %(q:Directive of the European Parliament and the Council about the patentability of computer-implemented inventions),  it should follow the European Parliament's position about patentability of computer programs, as it results from the vote of Sept, 24th, 2003, instead.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/fdp040526.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: fdp040526 ;
# txtlang: en ;
# multlin: t ;
# End: ;

