<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-12-01 Bundestag: Resolution on Software Patents

#descr: All Groups of the German Federal Parliament have approved a motion
that criticises the EU Council's Software Patent Agreement of May 18th
and calls for stronger limits on patentability in the spirit of the
European Parliament's Amendments of May 18th.

#ure: German Federal Parliament (Bundestag)

#uhW: Document Nr. %(N)

#Wm2: 1st December 2004

#llo: 15. Election Period

#rag: Motion by the members of parliament %(SPD) and the parliamentary group
of the Social Democratic Party of Germany (SPD); by the members of
parliament %(CDU) and the parliamentary group of the Christian
Democratic Union / Christian Social Union (CDU/CSU); by the members of
parliament %(VERD) and the parliamentary group of Alliance 90 / The
Greens; and by the members of parliament %(FDP) and the parliamentary
group of the Liberal Democratic Party (FDP).

#ncm: Safeguarding competition and innovation dynamics in the field of
software ­ Effectively confining the patenting of computer programs

#Bwh: May the Bundestag resolve as follows:

#edl: The German Federal Parliament finds:

#isd: In a global knowledge and information society and an increasingly
knowledge-based world economy, information technology solutions are
gaining more importance.

#nln: The regulatory framework for the development of powerful,
cost-efficient, dependable and, not least, safe computer programs and
software is becoming a critical factor for the German innovation
system.

#asi: The dynamic evolution of the German and the European software industry
is, in particular, also based upon small and medium-sized enterprises.

#euc: On 20 February 2002, the EU Comission put forward its proposal for a
directive of the European Parliament and of the Council on the
patentability of computer-implemented inventions (COM(2002)92 final).

#lop: On 24 September [2003], the European Parliament passed fundamental
amendments; on 18 May 2004, the Council of the European Union, with
the consent of the [German] federal government, was able to agree on a
common position (Council document no. 9713/04).

#beo: In principle, the German Bundestag welcomes the initiative for a
European harmonization of the patent granting practice with respect to
computer-implemented inventions.

#heh: It reaffirms its conviction that the sufficient protection of
intellectual property is indispensable for the sustaining and the
development of creative potentials in society, in the interest of the
creative, the consumers, as well as culture, economy and society as a
whole.

#Wje: Moreover, the innovation dynamics in many segments of the economy ­
increasingly also in traditional economic segments such as, for
instance, the machine construction, automotive and electrical
industries ­ depend upon the growing capabilities and successful
integration of information technology components.

#ija: The German Bundestag shares the conviction that technical inventions,
even if they contain software components, must be amenable to
protection by patent law.

#nWg: Nevertheless, the German Bundestag has arrived at the conclusion that
the present state of opinions concerning the draft directive at the
European level does, thus far, not provide sufficient solutions to
central questions.

#nsr: The definition of the "technical contribution" of a
computer-implemented invention as a requirement for its patentability
represents a central item of the proposed directive. For reasons of
legal certainty, the definition of the technical contribution
therefore has to be shaped as precisely as possible in order to
achieve sufficient quality control in the patent granting practice and
to prevent the patenting of so-called trivial patents.

#ifi: Computerimplemented inventions have to make such a technical
contribution in order to become patentable. The European Parliament
and the Council start from basically the same definition, according to
which a "technical contribution" is a contribution to the state of the
art in a field of technology, which contribution is not obvious to a
person skilled in the art. However, the European Parliament
additionally stipulated in its amendments to article 2b that the use
of forces of nature to control physical effects only belongs to a
field of technology if it transcends the numerical representation of
information.

#Wrc: The presentation, handling and processing of information should,
however, not represent a technical contribution, according to the
position of the [European] Parliament, even if technical devices are
used for such purposes.

#rWR: Such a restrictive definition is missing from the Council's proposal.

#Wen: The directive as proposed by the Council only contains such reasons
for exclusion [from patentability] in its article 4(2) that make
reference to the technical contribution of a computerimplemented
invention. Thereby, the Council has only partially adopted the reasons
for exclusion from the position of the European Parliament.

#ade: Also, the common position of the Council lacks a concrete definition
of the term "technical", which could be helpful to achieve the stated
objectives.

#hae: For this purpose, the jurisdictional practice of the Federal Supreme
Court [of Germany] has developed a practical definition: "Technical"
is a teaching for an action according to plan by using controllable
forces of nature for the achievement of a causally surveyable result,
which result is, without intermediary activity by the human mind, the
direct outcome of the use of controllable forces of nature.

#eeh: With such a definition, the individual elements of the technical
contribution would be easier to understand.

#eWr: At the same time, this would make an important contribution to
ensuring the continued interoperability of different computer systems.

#onW: All in all, the proposal by the Council does not meet these
requirements.

#eie: A too far-reaching patentability of computer programs threatens to
adversely affect innovation dynamics and to lead to new legal
uncertainties, particularly for open source concepts.

#mee: An important role must be ascribed to these and to open standards with
an eye to the growing requirements in terms of interopability and IT
security.

#dIW: From a technical point of view, copyright-related privileges
concerning decompilation and interoperability do not meet those
requirements by themselves and are to be complemented by a regulation
within patent law.

#trr: In particular, small and medium-sized enterprises fear that a too
far-reaching patentability of computer programs requires a high degree
of efforts in terms of human resources, legal costs and financial
expenditures, and creates substantial economic and legal risks.

#oid: Excessively broad patent claims or trivial patents furthermore bear
the danger of undermining the societal acceptance of the patent system
as an effective instrument for innovation and progress.

#hee2: In this regard, an independent evaluation of the controversial patent
granting practice of the European Patent Office in recent times is to
be ensured.

#tog: According to article 10 of the Agreement on Trade-Related Aspects of
Intellectual Property Rights (TRIPS) of the World Trade Organization
(WTO), computer programs shall be protected according to the rules of
copyright law.

#esW: The copyright protection of computer programs is guaranteed by Council
Directive 91/250/EEC of 14 May 1991, which guideline has been
incorporated into German law by articles 69 et seq. of the [German]
Copyright Law.

#neW: Correspondingly, computer programs "as such" (as well as business
models) are, according to the statutes of the European Patent
Convention that are in force and effect, excluded from patentability.

#etu: This principle must be upheld.

#eng: Computer-implemented technical inventions must therefore be construed
as narrowly as possible.

#urv: Hence, the German Bundestag welcomes the objectives of the decisions
taken by the European Parliament.

#ise: It requests the European Parliament to further advance these
objectives in the upcoming consultations of the draft directive.

#tin: It welcomes the recent initiative by the [German] federal government
with respect to a "roundtable" for sounding out possible paths to a
compromise in a dialog with stakeholders and the potentially affected.

#trg: The German Federal Parliament calls upon the Federal Government:

#mWs: to take into account, to an increased extent, aspects of industrial
location policy, competition policy and innovation policy as well as
the particular parameters of the development and the specific
characteristics of computer programs, in forthcoming debates and
measures for reforming the protection of intellectual property in
computer programs and in the field of information technology;

#WGn: to further and intensify the commenced dialog with small and
medium-sized software enterprises, science and other representatives
of civil society;

#rta: to further its efforts for an improved information of small and
medium-sized enterprises about the opportunities of an active patent
policy.

#iut: Regarding the further consultation of the draft directive at the
European level, the German Federal Parliament calls upon the Federal
Government:

#ahu: to use its influence so that, in further consultations, the draft
directive be changed to the effect that the definition of the
technical contribution in article 2b be stated more concretely, and
that a definition of the term "technical", which is oriented towards
the definition of "technical" by the [German] Federal Supreme Court,
be included. The very definition has to ensure that computer programs
as such, business methods and algorithms cannot be patented;

#ige: Schon durch die Definition muss sichergestellt werden, dass
Computerprogramme als solche, Geschäftsmethoden, Algorithmen und
Erfindungen, deren technischer Beitrag allein in der Datenverarbeitung
liegt, nicht patentiert werden können.

#jhr: to use its influence so that an interoperability privilege under
patent law, to the fullest extent possible, be incorporated as a
statute;

#AAd: to use its influence so that, in article 5 of the draft directive, the
scope of permissible patent claims be restricted to product and
process claims by excluding autonomous program claims;

#sdn: to use its influence at the European level so that an independent
evaluation of the decision-making practice of patent offices,
particularly the EPO, be performed.

#log: This could, for example, be an integral element of the envisioned
report on the effects of the directive;

#c4h: to take into account to a greater extent, in the further quest for a
compromise, the objectives of the decisions taken by the European
Parliament in the spirit of this motion, and the future results of the
roundtable of the Federal Ministry of Justice;

#Kon: to use its influence, with determination, so that alternative
development concepts such as, in particular, open source projects not
be impaired.

#ont: Bundestag Unanimously Criticises Council Software Patent Agreement,
Calls for Effective Limitation of Patentability

#wWt: Nosoftwarepatents.com: English Translation & FAQ

#tta: Übersetzungen und Erklärungen von Florian Müller

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: (de flomysql 2004-12-01) ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bundestag0411 ;
# txtlang: en ;
# multlin: t ;
# End: ;

