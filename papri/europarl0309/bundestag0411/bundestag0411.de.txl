<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-11 Bundestag: Interfraktioneller Entschließungsantrag zu
Softwarepatenten

#descr: Die Fraktionen des Deutschen Bundestages wollen mit einem
Entschließungsantrag für eine Begrenzung des Patentwesens auf die
angewandten Naturwissenschaften eintreten und dem europäischen
Parlament in seiner Auseinandersetzung mit dem Rat (d.h. den jenseits
wirksamer parlamentarischer Kontrolle agierenden Patentbeamten des
Bundesministeriums der Justiz) den Rücken stärken.  Der Antrag fasst
unterschiedliche Anträge der FDP, CDU-CSU und der Koalitionsfraktionen
zu einem gemeinsamen Antrag zusammen.

#ure: Deutscher Bundestag

#uhW: Drucksache Nr. %(N)

#Wm2: 1. Dezember 2004

#llo: 15. Wahlperiode

#rag: Antrag der Abgeordneten %(SPD) und der Fraktion der SPD, der
Abgeordneten %(CDU) und der Fraktion der CDU/CSU, der Abgeordneten
%(VERD) und der Fraktion BÜNDNIS 90/DIE GRÜNEN, sowie der Abgeordneten
%(FDP) und der Fraktion der FDP

#ncm: Wettbewerb und Innovationsdynamik im Softwarebereich sichern ­
Patentierung von Computerprogrammen effektiv begrenzen

#Bwh: Der Bundestag wolle beschließen:

#edl: Der Deutsche Bundestag stellt fest:

#isd: In einer globalen Wissens- und Informationsgesellschaft und einer
zunehmend wissensbasierten Weltwirtschaft gewinnen
informationstechnische Lösungen zunehmend an Bedeutung.

#nln: Die Rahmenbedingungen für die Entwicklung leistungsfähiger,
kostengünstiger, verlässlicher und nicht zuletzt sicherer
Computerprogramme oder Software werden zu einem kritischen Faktor des
deutschen Innovationssystems.

#asi: Getragen wird die dynamische Entwicklung der deutschen wie der
europäischen Softwarebranche insbesondere auch durch kleine und
mittlere Unternehmen.

#euc: Die EU-Kommission hat am 20. Februar 2002 ihren Vorschlag für die
Richtlinie des Europäischen Parlamentes und des Rates über die
Patentierbarkeit computerimplementierter Erfindungen vorgelegt
(KOM(2002) 92 endgültig).

#lop: Das Europäische Parlament hat am 24. September wesentliche Änderungen
beschlossen, am 18. Mai 2004 hat der Rat der Europäischen Union sich
mit Zustimmung der Bundesregierung auf einen gemeinsamen Standpunkt
einigen können (Ratsdokument Nr. 9713/04).

#beo: Der Deutsche Bundestag begrüßt grundsätzlich die Initiative zur
europäischen Vereinheitlichung der Patentierungspraxis in Bezug auf
computerimplementierte Erfindungen.

#heh: Er bekräftigt seine Überzeugung, dass der hinreichende Schutz des
geistigen Eigentums unverzichtbar ist zum Erhalt und zur Entwicklung
der kreativen gesellschaftlichen Potenziale im Interesse der
Kreativen, der Verbraucherinnen und Verbraucher wie der Kultur,
Wirtschaft und Gesellschaft insgesamt.

#Wje: Zudem hängt die Innovationsdynamik in vielen Wirtschaftsbereichen ­
zunehmend auch in klassischen Wirtschaftsbereichen wie beispielsweise
Maschinenbau-, Automobil- sowie Elektroindustrie ­ in wachsendem Maße
von der steigenden Leistungsfähigkeit und erfolgreichen Integration
von informationstechnischen Komponenten ab.

#ija: Der Deutsche Bundestag teilt die Überzeugung, dass technische
Erfindungen auch dann, wenn sie Softwarekomponenten enthalten, dem
Schutz des Patentrechts zugänglich sein müssen.

#nWg: Gleichwohl ist der Deutsche Bundestag zu der Auffassung gelangt, dass
der gegenwärtige Meinungsstand zum Richtlinienentwurf auf Europäischer
Ebene bisher für zentrale Fragen keine hinreichenden Lösungen
aufweist.

#nsr: Die Definition des %(q:technischen Beitrags) einer
computerimplementierten Erfindung als Voraussetzung ihrer
Patentierbarkeit stellt einen zentralen Punkt des
Richtlinienvorschlags dar. Aus Gründen der Rechtssicherheit muss daher
die Definition des technischen Beitrages so genau wie möglich gefasst
werden, um eine genügende Qualitätskontrolle in der
Patentierungspraxis zu erreichen und insbesondere die Patentierung von
so genannten Trivialpatenten zu verhindern.

#ifi: Computerimplementierte Erfindungen müssen einen solchen technischen
Beitrag leisten, um Patentfähigkeit zu erreichen. Das Europäische
Parlament und der Rat gehen grundsätzlich von derselben Definition
aus, wonach ein %(q:technischer Beitrag) ein Beitrag zum Stand der
Technik auf einem Gebiet der Technik ist, der für eine fachkundige
Person nicht nahe liegend ist. Das Europäische Parlament hat aber
weiterhin in seinen Abänderungen in Art. 2 lit. (b) des
Richtlinienvorschlags definiert, dass eine Nutzung der Naturkräfte zur
Beherrschung von physikalischen Wirkungen nur dann zum Gebiet der
Technik gehört, wenn sie über die numerische Darstellung von
Informationen hinausgeht.

#Wrc: Die Darstellung, Bearbeitung und Verarbeitung von Informationen sollen
nach dem Parlamentsentwurf aber keinen technischen Beitrag darstellen,
selbst wenn dafür technische Vorrichtungen verwendet werden.

#rWR: Eine derartige  einschränkende Definition fehlt im Ratsvorschlag.

#Wen: Der Richtlinienvorschlag des Rates enthält in Art. 4 a Abs. 2 nur
Ausschlussgründe, die sich auf den technischen Beitrag einer
computerimplementierten Erfindung beziehen. Damit hat der Rat die
Ausschlussgründe inhaltlich von der Entschließung des Europäischen
Parlamentes nur teilweise übernommen.

#ade: Auch fehlt dem gemeinsamen Standpunkt des Rates eine konkrete
Definition des Technikbegriffs, die zur Erreichung der genannten Ziele
hilfreich sein könnte.

#hae: Die ständige Rechtsprechung des Bundesgerichtshofes hat hierzu eine
praxisnahe Bestimmung entwickelt: Technisch ist eine Lehre zum
planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur
Erreichung eines kausal übersehbaren Erfolgs, der ohne
Zwischenschaltung menschlicher Verstandestätigkeit die unmittelbare
Folge des Einsatzes beherrschbarer Naturkräfte ist.

#eeh: Mit einer solchen Definition wären die einzelnen Elemente des
technischen Beitrags leichter nachvollziehbar.

#eWr: Gleichzeitig würde dies einen wichtigen Beitrag dazu leisten, dass die
Interoperabilität zwischen verschiedenen Computersysteme gewährleistet
bleibt.

#onW: Der Ratsvorschlag wird diesen Anforderungen insgesamt nicht gerecht.

#eie: Eine zu weit gehende Patentierbarkeit von Computerprogrammen droht
sich negativ auf die Innovationsdynamik auszuwirken und zu neuen
Rechtsunsicherheiten insbesondere für Open-Source-Konzepte zu führen.

#mee: Diesen kommt gemeinsam mit offenen Standards hinsichtlich der
steigenden Anforderungen an Interoperabilität und IT-Sicherheit eine
wichtige Rolle zu.

#dIW: Aus technischer Sicht genügen die urheberrechtlichen Privilegien
hinsichtlich der Dekompilierung und Interoperabilität diesen
Anforderungen allein nicht und sind durch eine patentrechtliche
Vorschrift zu ergänzen.

#trr: Insbesondere kleine und mittlere Unternehmen befürchten zudem von
einer zu weit gehenden Patentierbarkeit von Computerprogrammen einen
hohen personellen, juristischen und finanziellen Aufwand sowie
erhebliche wirtschaftliche und rechtliche Risiken.

#oid: Ausufernde Patentansprüche oder Trivialpatente bergen ferner die
Gefahr, die gesellschaftliche Akzeptanz des Patentsystems als
effektives Innovations- und Fortschrittsinstrument auszuhöhlen.

#hee2: Hier ist eine unabhängige Evaluierung der umstrittenen jüngeren
Patentierungspraxis des Europäischen Patentamtes sicherzustellen.

#tog: Computerprogramme sollen laut Art. 10 des Agreement on Trade-Related
Aspects of Intellectual Property Rights (TRIPS) der
Welthandelsorganisation (WTO) nach den Regeln des Urheberrechts
geschützt werden.

#esW: Der urheberrechtliche Schutz von Computerprogrammen wird durch die
Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz
von Computerprogrammen gewährleistet, deren Vorgaben in Deutschland
durch die §§ 69 a ff. des Urheberrechtsgesetzes umgesetzt worden sind.

#neW: Computerprogramme sind dementsprechend nach den geltenden Bestimmungen
des Europäischen Patentübereinkommens %(q:als  solche) (ebenso wie
Geschäftsmodelle) von der Patentierbarkeit ausgenommen.

#etu: An diesem Grundsatz ist festzuhalten.

#eng: Computerimplementierte technische Erfindungen sind daher so eng wie
möglich auszulegen.

#urv: Der Deutsche Bundestag begrüßt deshalb die Zielrichtung der Beschlüsse
des Europäischen Parlaments.

#ise: Er bittet das Europäische Parlament, in den kommenden Beratungen des
Richtlinienentwurfes dieser Zielrichtung weiterhin Geltung zu
verschaffen.

#tin: Er begrüßt die jüngste Initiative der Bundesregierung zu einem
%(q:Runden Tisch), um im Dialog mit Beteiligten und Betroffenen
mögliche Kompromisswege auszuloten.

#trg: Der Deutsche Bundestag fordert die Bundesregierung auf:

#mWs: bei kommenden Debatten und Maßnahmen zur Reform des Schutzes geistigen
Eigentums bei Computerprogrammen sowie im informationstechnischen
Bereich verstärkt standort-, wettbewerbs- und innovationspolitische
Aspekte sowie die besonderen Entwicklungsbedingungen und spezifischen
Merkmale von Computerprogrammen zu berücksichtigen;

#WGn: den begonnenen Dialog mit kleinen und mittleren Softwareunternehmen,
der Open Source-Gemeinde sowie mit anderen zivilgesellschaftlichen
Vertretern fortzusetzen und zu intensivieren;

#rta: ihre Bemühungen zur verbesserten Information insbesondere kleiner und
mittlerer Unternehmen über die Chancen einer aktiven Patentpolitik
weiter zu führen.

#iut: Hinsichtlich der weiteren Beratung des Richtlinienentwurfs auf
europäischer Ebene fordert der Deutsche Bundestag die Bundesregierung
auf:

#ahu: darauf hinzuwirken, dass in den weiteren Beratungen der
Richtlinienentwurf dahingehend geändert wird, dass die Definition des
technischen Beitrags in Art. 2 lit. b) konkreter gefasst und eine
Definition des Begriffs %(q:Technik) aufgenommen wird, die sich an der
Technikdefinition des BGH orientiert.

#ige: Schon durch die Definition muss sichergestellt werden, dass
Computerprogramme als solche, Geschäftsmethoden, Algorithmen und
Erfindungen, deren technischer Beitrag allein in der Datenverarbeitung
liegt, nicht patentiert werden können.

#jhr: darauf hinzuwirken, dass ein möglichst umfassendes patentrechtliches
Interoperabilitätsprivileg als Vorschrift aufgenommen wird.

#AAd: sich dafür einzusetzen, dass in Art. 5 des Richtlinienentwurfs der
Umfang der zulässigen patentrechtlichen Ansprüche auf Erzeugnis- und
Verfahrensansprüche begrenzt wird und selbständige Programmansprüche
ausgeschlossen werden;

#sdn: sich auf europäischer Ebene dafür einzusetzen, eine unabhängige
Evaluierung der Entscheidungspraxis der Patentämter, insbesondere des
EPA, durchzuführen.

#log: Dies kann beispielsweise ein integraler Bestandteil des vorgesehenen
Berichtes über die Auswirkungen der Richtlinie sein;

#c4h: bei der weiteren Kompromisssuche die Zielrichtung der Beschlüsse des
Europäischen Parlaments vom 24. September 2003 und auch die
zukünftigen Ergebnisse des Runden Tisches beim BMJ stärker zu
berücksichtigen;

#Kon: sich entschieden dafür einzusetzen, dass alternative
Entwicklungskonzepte wie insbesondere Open Source-Projekte nicht
beeinträchtigt werden;

#ont: Bundestag Unanimously Criticises Council Software Patent Agreement,
Calls for Effective Limitation of Patentability

#wWt: Nosoftwarepatents.com: English Translation & FAQ

#tta: Übersetzungen und Erklärungen von Florian Müller

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: flomysql ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bundestag0411 ;
# txtlang: de ;
# multlin: t ;
# End: ;

