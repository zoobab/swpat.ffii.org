
   #[1]RSS

[2]Lenz Blog

December 19, 2004

Ms. van Gennip's Weird Theory

   Ms. van Gennip is the Dutch Secretary of State of Economical Affairs.
   According to [3]this EDRI report, she declared to the Dutch Parliament
   that

     Since there will be no vote on the A-item the Netherlands don't
     have to abstain from voting.

   If there will be no vote, how exactly is the proposal supposed to
   receive "approval without discussion" as stated in Article 3 Paragraph
   6 as the meaning of "Part A" items?

   If there is no vote, there is no approval. "Part A" means "without
   discussion", not "without vote". And using this as an excuse for
   acting against the declared will of the Dutch Parliament is rather
   weird.

   Then again, at this level of contempt for basic values of democracy,
   maybe one should not expect anything else.

   Of course this hotly contested issue needs to be voted on, even if
   there is "no discussion" on the vote. If it is not, then there is no
   basis whatsoever for assuming that the Council has "formally adopted"
   anything.

   And it takes only one Member State to force a vote anyway (Article 3
   Paragraph 8).

   So even if "formal adoption" means "adoption while nobody actually
   votes on the proposal" in Ms. Gennip's world, we might very well see a
   vote anyway next week.

   That should happen independent of any merit of this proposal, and even
   if there is actually a majority for it. "Approval without vote" is a
   deeply disturbing perversion of democratic principles, and logically
   impossible as well.

   Bonus quote from the [4]December 7 speech by Ambassador of the
   European Commission to the United States:

     "We need local democracy. We need national democracy. But if we are
     to manage the forces of globalization, we also now need
     supranational democracy. The European Union is not perfect, but it
     is one of the best attempts yet at supranational democracy,
     anywhere in the world."

   Yes. We need supranational democracy. But that is impossible without
   actually voting on proposals if you want to adopt them.
   Posted by Karl-Friedrich Lenz to [5]Patents at [6]08:15 PM |
   [7]TrackBack (0)

Political Agreement

   The [8]EU Council Rules of Procedure provide for a two-stage decision
   process.

   Member States can find "political agreement" on a position in one
   Council meeting and then "formally adopt" it as a so-called A-item at
   a later meeting.

   The Rules of Procedure say in Article 3, Paragraph 8 that any Member
   State can force a vote at the later meeting. See also [9]this earlier
   post.

   However, the enemies of democracy running the Council now seem to
   think that there is an unwritten rule that prevents Member States from
   actually doing that if they are opposed to a proposal at the time of
   the later meeting.

   This is clearly the exact opposite of what the text of the Rules of
   Procedure says.

   And if that position was true, there would be no point in having any
   "formal adoption" in a later meeting in the first place. All
   "political agreements" would be final. That would mean a major shift
   in the whole construction of the Rules of Procedure.

   For example, that would mean that in turn all safeguards against
   hastily adopted decisions no one has had a chance to read or discuss
   (like all requirements on translations or minimum time frames for
   agenda decisions) would need to be cleared at the earlier time of the
   "political agreement".

   So this is a test case. Either Article 3 Paragraph 8 has some meaning
   or not. We will see in a couple of days what happens.
   Posted by Karl-Friedrich Lenz to [10]European Union Law at [11]10:01
   AM | [12]TrackBack (0)

EU Democracy

   The most important legislator in the EU is the Council. Parliament has
   some rights, but much less than in any Member State. The legislative
   center of power is in the Council.

   That is somewhat problematic. Where is the democratic legitimacy of
   Council decisions?

   The only answer possible is that the representatives of the Member
   States in the Council are elected in their respective national
   Parliaments.

   That means there is zero democratic legitimacy for any Council
   decision that is taken against the expressly stated will of Member
   States' Parliaments.

   This should really be only a theoretical consideration. Surely, one
   should assume, Member States delegates at the Council will respect
   their respective Parliament's decisions.

   However, in the case of the [13]harmful and obnoxious proposal for a
   software patent Directive there is a real chance that next Tuesday the
   [14]Council will take a decision that is clearly contradicting Member
   States' Parliaments positions.

   This is an outrage. The democratic legitimacy of legislation by
   Council beuraucrats is shaky to begin with. But adopting positions in
   clear opposition to the declared will of the democratic
   representatives of the Member States' people would be a new low point.

   This kind of scandal is the last thing the EU needs in this critical
   time frame where [15]referendums on the EU Constitution are scheduled
   in multiple Member States.

   So what excuse have the enemies of democracy for this kind of
   behaviour?

   It seems they are hiding behind a rule that does not exist and that
   would make the whole [16]Rules of Procedure of the Council meaningless
   if it did.

   Addressing this requires a [17]seperate post.
   Posted by Karl-Friedrich Lenz to [18]Patents at [19]09:46 AM |
   [20]TrackBack (0)

December 15, 2004

Threats against Germany?

   [21]Juan Cole points [22]to this article about the Pentagon reaction
   to the [23]recent criminal complaint against Rumsfeld and others.

   Pentagon spokesman Lawrence DiRita said that

     "I think every government in the world, particularly a NATO ally,
     understands the potential effect on relations with the United
     States if these kinds of frivolous lawsuits were ever to see the
     light of day."

   Cole reports this under the title " Pentagon Threatens Germany over
   Rumsfeld Suit".

   I am not sure if that DiRita comment should be described as a threat.
   I would rather want to understand it as a warning, and as one that
   only states rather obvious things. No German prosecutor needs this
   statement by DiRita to know that the American government will not be
   pleased if he decides to start a criminal investigation of the
   complaint.

   And surely the Pentagon can't possibly assume that any threat might
   help influence the decision about what to do with that criminal
   complaint in a way favorable to Rumsfeld. Of course, if anything, a
   real threat could only damage his position. If German prosecutors are
   influenced at all by vague threats, it is probably only in a
   backfiring way.

   And, as I remarked in my [24]first post about this complaint, under
   German criminal procedure, there is no way to avoid that this case is
   going to court, short of having the Center for Constitutional Rights
   and the four Iraqi citizens back down. I don't know how much "light of
   day" the complaint will see, but it will certainly see some judges
   sooner or later.
   Posted by Karl-Friedrich Lenz to [25]Illegal War at [26]10:07 PM |
   [27]TrackBack (0)

   The Author
   Dr. Karl-Friedrich Lenz
   Professor,
   [28]Aoyama Gakuin University
   in Tokyo
   [29]Homepage
   Mail: kf02 at k.lenz.name
   Postal Address:
   150-8366 Tokyo Shibuya-ku
   Shibuya 4-4-25
   Aoyama Gakuin University,
   Law School, Japan
   My Books (in German)
   [30]Elementare Grundlagen des Go-Spiels
   [31]Grenzen des Patentwesens
   [32]Lernstrategie Jura
   [33]Zuk�nftiges Recht
   Links
   Lenz Blog is listed at [34]Daily Whirl.
   [35]LenzBlog German
   [36]LenzBlog Japanese
   [37]Wiki (in Japanese)
   Blogs in English
   [38]Bag and Baggage
   [39]Jack Balkin
   [40]John Battelle
   [41]Stefan Bechtold
   [42]Berkeley IP
   [43]Boing Boing
   [44]Andreas Bovens
   [45]Commons-blog
   [46]Copyfight
   [47]Creative Commons
   [48]Dennis Crouch
   [49]DM Europe
   [50]Digital Consumer
   [51]Maximillian Dornseif (1)
   [52]Maximillian Dornseif (2)
   [53]EFF Deeplinks
   [54]Eldred Act Blog
   [55]Emerging Technologies
   [56]Europunditry
   [57]Eurosavant
   [58]Seth Finkelstein
   [59]Frank Field
   [60]Freedom To Tinker
   [61]Michael Froomkin
   [62]German American Law Journal
   [63]Dan Gillmor
   [64]Google Weblog
   [65]GrepLaw
   [66]GrokLaw
   [67]Kevin J. Heller
   [68]Axel H. Horns
   [69]IPKAT
   [70]Joi Ito
   [71]Andis Kaulins
   [72]Dennis Kennedy
   [73]Jerry Lawson
   [74]Legal Theory Blog
   [75]Lessig Blog
   [76]Matt
   [77]Ernest Miller
   [78]Stephen M. Nipper
   [79]Open Access News
   [80]Sabrina I. Pacifici
   [81]John Palfrey
   [82]Paper Chase
   [83]PFF IPCentral
   [84]Bruce Schneier
   [85]Seth Schoen
   [86]Walter Simon
   [87]Derek Slater
   [88]Richard Stallman
   [89]Stanford CIS
   [90]M. Claire Stewart
   [91]Aaron Swartz
   [92]Trademark Blog
   [93]Transblawg
   [94]Siva Vaidhyanathan
   [95]The Volokh Conspiracy
   [96]Kim Weatherall
   [97]Dave Winer
   [98]Yale LawMeme
   Blogs in German
   [99]FFII Swpat Mailinglist
   [100]Alexander Hartmann
   [101]Michael Heng
   [102]Institut f�r Urheber- und Medienrecht
   [103]Sascha Kremer
   [104]Rainer Langenhan
   [105]Ostermaier/Trautmann
   [106]LAWgical
   [107]Schockwellenreiter
   [108]Udo
   Blogs in Japanese
   [109]Joi Ito
   [110]Larry Lessig
   [111]Makina
   [112]Kiichi Okaguchi (1)
   [113]Kiichi Okaguchi (2)
   [114]Hisamichi Okamura
   [115]Patent DJ
   [116]Patent Salon
   [117]The Trembling of a Leaf
   EU Law
   [118]Eur-Lex
   [119]Official Journal
   [120]Council
   [121]Commission
   [122]Court of Justice
   [123]Parliament
   [124]EDRI
   [125]US Delegation
   [126]EUbusiness.com
   [127]EUobserver.com
   [128]Statewatch
   [129]Google News
   [130]Berkeley
   German Law
   [131]Federal Official Journal
   [132]Federal Constitutional Court
   [133]Federal Court of Justice
   [134]Federal Government
   [135]Federal Ministry of Justice
   [136]Parliament
   [137]JIPS
   [138]Jurawiki
   [139]Beck
   [140]Spiegel
   [141]FAZ
   [142]ZEIT
   [143]FITUG Debate
   IP Links
   [144]Slashdot
   [145]Wired
   [146]MPI Links
   Categories
   [147]Copyright 249
   [148]E-Gold 4
   [149]European Union Law 88
   [150]Everything else 45
   [151]German Law 16
   [152]Ideas 1
   [153]Illegal War 82
   [154]Internet Freedom 50
   [155]Patents 170
   [156]Trademarks 4
   Archives
   [157]December 2004
   [158]November 2004
   [159]October 2004
   [160]September 2004
   [161]August 2004
   [162]July 2004
   [163]June 2004
   [164]May 2004
   [165]April 2004
   [166]March 2004
   [167]February 2004
   [168]January 2004
   [169]December 2003
   [170]November 2003
   [171]October 2003
   [172]September 2003
   [173]August 2003
   [174]July 2003
   [175]June 2003
   [176]May 2003
   [177]April 2003
   [178]March 2003
   [179]February 2003
   [180]January 2003
   Recent Entries
   [181]Ms. van Gennip's Weird Theory
   [182]Political Agreement
   [183]EU Democracy
   [184]Threats against Germany?
   [185]Vote America Out of Iraq
   [186]Universal Surveillance
   [187]Optimal War
   [188]EU Council on Data Retention
   [189]Wikinews Launch
   [190]Brazil AIDS Patents Violations
   Trackbacks
   [191]Richard Stallman on the EU-US Economic Partnership
   [192]Software Patents = = WMD
   [193]German Abu Ghraib Complaint
   RSS feed full text:
   [194]RSS 1.0
   [195]RSS 2.0
   RSS feed entry body only:
   [196]RSS 1.0
   [197]RSS 2.0
   Search
   Search this site:
   ____________________
   Search
   Copyright
   All rights reserved.
   Comments are owned
   by their authors.
   Please feel free to
   forward copies of this
   work to others,
   mirror it on your
   homepage or blog,
   or post it on
   bulletin boards
   or P2P networks.
   However, please leave
   my name attached
   and don't edit
   the work.
   Commercial use
   requires separate
   permission.
   Powered by
   [198]Movable Type 2.51
   RSS 2.0 template by
   [199]RSS Validator

Verweise

   1. http://k.lenz.name/LB/index.rdf
   2. http://k.lenz.name/LB/
   3. http://www.edri.org/edrigram/number2.24/softpats
   4. http://www.eurunion.org/News/speeches/2004/041207jb.htm
   5. http://k.lenz.name/LB/archives/cat_patents.html
   6. http://k.lenz.name/LB/archives/000968.html
   7. http://k.lenz.name/LB/mt-tb.cgi?__mode=view&entry_id=968
   8. http://europa.eu.int/smartapi/cgi/sga_doc?smartapi!celexapi!prod!CELEXnumdoc&lg=en&numdoc=32004D0338&model=guichett
   9. http://k.lenz.name/LB/archives/000951.html#000951
  10. http://k.lenz.name/LB/archives/cat_european_union_law.html
  11. http://k.lenz.name/LB/archives/000967.html
  12. http://k.lenz.name/LB/mt-tb.cgi?__mode=view&entry_id=967
  13. http://k.lenz.name/LB/archives/000264.html#000264
  14. http://kwiki.ffii.org/Cons041217En
  15. http://news.google.com/news?q=EU+constitution+referendum&hl=en&lr=&sa=N&tab=nn&oi=newsr
  16. http://europa.eu.int/smartapi/cgi/sga_doc?smartapi!celexapi!prod!CELEXnumdoc&lg=en&numdoc=32004D0338&model=guichett
  17. http://k.lenz.name/LB/archives/000967.html
  18. http://k.lenz.name/LB/archives/cat_patents.html
  19. http://k.lenz.name/LB/archives/000966.html
  20. http://k.lenz.name/LB/mt-tb.cgi?__mode=view&entry_id=966
  21. http://www.juancole.com/2004/12/pentagon-threatens-germany-over.html
  22. http://www.spacewar.com/2004/041213183522.7t8ko4bz.html
  23. http://k.lenz.name/LB/archives/000958.html
  24. http://k.lenz.name/LB/archives/000958.html
  25. http://k.lenz.name/LB/archives/cat_illegal_war.html
  26. http://k.lenz.name/LB/archives/000965.html
  27. http://k.lenz.name/LB/mt-tb.cgi?__mode=view&entry_id=965
  28. http://www.law.aoyama.ac.jp/
  29. http://k.lenz.name/
  30. http://k.lenz.name/d/v/index.html
  31. http://k.lenz.name/d/v/index.html
  32. http://k.lenz.name/d/v/index.html
  33. http://k.lenz.name/d/v/index.html
  34. http://www.dailywhirl.com/
  35. http://k.lenz.name/LB/d/
  36. http://k.lenz.name/weblog/
  37. http://k.lenz.name/lw
  38. http://bgbg.blogspot.com/
  39. http://balkin.blogspot.com/
  40. http://battellemedia.com/
  41. http://cyberlaw.stanford.edu/blogs/bechtold
  42. http://journalism.berkeley.edu/projects/biplog/
  43. http://boingboing.net/
  44. http://akira.arts.kuleuven.ac.be/andreas/blog/
  45. http://www.info-commons.org/blog/
  46. http://www.corante.com/copyfight/
  47. http://creativecommons.org/weblog/
  48. http://patentlaw.typepad.com/patent/
  49. http://www.dmeurope.com/
  50. http://www.digitalconsumer.org/news.html
  51. http://blogs.23.nu/disLEXia/
  52. http://blogs.23.nu/cybercrime/
  53. http://blogs.eff.org/deeplinks/
  54. http://www.eldred.cc/
  55. http://miladus.typepad.com/etech/
  56. http://www.europundit.com/
  57. http://www.eurosavant.com/
  58. http://sethf.com/infothought/blog/
  59. http://msl1.mit.edu/furdlog/index.php
  60. http://www.freedom-to-tinker.com/
  61. http://www.discourse.net/
  62. http://63.142.46.199/cgi-bin/amlaw.cgi/
  63. http://weblog.siliconvalley.com/column/dangillmor/
  64. http://google.blogspace.com/
  65. http://grep.law.harvard.edu/
  66. http://www.groklaw.net/index.php
  67. http://techlawadvisor.com/blog/
  68. http://www.ipjur.com/03.php3
  69. http://ipkitten.blogspot.com/
  70. http://joi.ito.com/
  71. http://www.lawpundit.com/blog/lawpundit.htm
  72. http://www.denniskennedy.com/blog/
  73. http://www.netlawblog.com/
  74. http://lsolum.blogspot.com/
  75. http://lessig.org/blog/
  76. http://mattrolls.blogspot.com/
  77. http://www.corante.com/importance/
  78. http://nip.blogs.com/patent/
  79. http://www.earlham.edu/~peters/fos/fosblog.html
  80. http://www.bespacific.com/index.html
  81. http://blogs.law.harvard.edu/palfrey/
  82. http://jurist.law.pitt.edu/paperchase/
  83. http://ipcentral.info/blog/index.shtml
  84. http://www.schneier.com/blog/
  85. http://vitanuova.loyalty.org/latest.html
  86. http://blat.antville.org/
  87. http://blogs.law.harvard.edu/cmusings/
  88. http://www.stallman.org/#notes
  89. http://cyberlaw.stanford.edu/index.shtml#blog
  90. http://copyrightreadings.blogspot.com/
  91. http://www.aaronsw.com/weblog/
  92. http://trademark.blog.us/blog/
  93. http://www.margaret-marks.com/Transblawg/
  94. http://www.sivacracy.net/
  95. http://volokh.com/
  96. http://weatherall.blogspot.com/
  97. http://www.scripting.com/
  98. http://research.yale.edu/lawmeme/
  99. http://www.ffii.org/archive/mails/swpat/
 100. http://jurabilis.blogspot.com/
 101. http://www.advocatus.de/heng/weblog.php
 102. http://www.urheberrecht.org/news/
 103. http://weblawg.saschakremer.de/
 104. http://log.handakte.de/
 105. http://www.law-blog.de/
 106. http://lawgical.jura.uni-sb.de/
 107. http://schockwellenreiter.server-wg.de/blog
 108. http://udoslive.blogspot.com/
 109. http://joi.ito.com/jp/
 110. http://blog.cnetnetworks.jp/lessig/
 111. http://homepage3.nifty.com/machina/
 112. http://okaguchi.tripod.co.jp/top.htm
 113. http://d.hatena.ne.jp/okaguchik/
 114. http://www.law.co.jp/okamura/nikki.htm
 115. http://d.hatena.ne.jp/patentdj/
 116. http://www.patentsalon.com/
 117. http://blog.melma.com/00089025/
 118. http://europa.eu.int/eur-lex/en/index.html
 119. http://europa.eu.int/eur-lex/en/oj/index.html
 120. http://ue.eu.int/newsroom/Latest.ASP?BID=999&LANG=1&Version=fromHomePage
 121. http://europa.eu.int/geninfo/whatsnew.htm
 122. http://curia.eu.int/en/actu/communiques/index.htm
 123. http://www.europarl.eu.int/press/index_en.htm
 124. http://www.edri.org/
 125. http://www.eurunion.org/
 126. http://www.eubusiness.com/
 127. http://www.euobserver.com/
 128. http://www.statewatch.org/
 129. http://news.google.com/news?hl=en&q=eu&btnG=Search+News
 130. http://www.lib.berkeley.edu/GSSI/eu.html
 131. http://bgbl.makrolog.de/
 132. http://www.bverfg.de/cgi-bin/link.pl?presse
 133. http://juris.bundesgerichtshof.de/cgi-bin/rechtsprechung/list.py?Gericht=bgh&Sort=3&Art=pm&client=2
 134. http://www.bundesregierung.de/
 135. http://www.bmj.bund.de/
 136. http://www.bundestag.de/presse/hib/index.html
 137. http://www.jura.uni-sb.de/nachrichten.html/
 138. http://www.jurawiki.de/
 139. http://rsw.beck.de/rsw/shop/default.asp?sessionid=00961393840646DEA4F35AEA16474083&toc=HP.0110
 140. http://www.spiegel.de/
 141. http://www.faz.net/s/homepage.html
 142. http://www.zeit.de/
 143. http://www.fitug.de/debate/index.html
 144. http://www.slashdot.org/
 145. http://www.wired.com/
 146. http://www.ip.mpg.de/Enhanced/Deutsch/externeLinks/auslandd.cfm
 147. http://k.lenz.name/LB/archives/cat_copyright.html
 148. http://k.lenz.name/LB/archives/cat_egold.html
 149. http://k.lenz.name/LB/archives/cat_european_union_law.html
 150. http://k.lenz.name/LB/archives/cat_everything_else.html
 151. http://k.lenz.name/LB/archives/cat_german_law.html
 152. http://k.lenz.name/LB/archives/cat_ideas.html
 153. http://k.lenz.name/LB/archives/cat_illegal_war.html
 154. http://k.lenz.name/LB/archives/cat_internet_freedom.html
 155. http://k.lenz.name/LB/archives/cat_patents.html
 156. http://k.lenz.name/LB/archives/cat_trademarks.html
 157. http://k.lenz.name/LB/archives/2004_12.html
 158. http://k.lenz.name/LB/archives/2004_11.html
 159. http://k.lenz.name/LB/archives/2004_10.html
 160. http://k.lenz.name/LB/archives/2004_09.html
 161. http://k.lenz.name/LB/archives/2004_08.html
 162. http://k.lenz.name/LB/archives/2004_07.html
 163. http://k.lenz.name/LB/archives/2004_06.html
 164. http://k.lenz.name/LB/archives/2004_05.html
 165. http://k.lenz.name/LB/archives/2004_04.html
 166. http://k.lenz.name/LB/archives/2004_03.html
 167. http://k.lenz.name/LB/archives/2004_02.html
 168. http://k.lenz.name/LB/archives/2004_01.html
 169. http://k.lenz.name/LB/archives/2003_12.html
 170. http://k.lenz.name/LB/archives/2003_11.html
 171. http://k.lenz.name/LB/archives/2003_10.html
 172. http://k.lenz.name/LB/archives/2003_09.html
 173. http://k.lenz.name/LB/archives/2003_08.html
 174. http://k.lenz.name/LB/archives/2003_07.html
 175. http://k.lenz.name/LB/archives/2003_06.html
 176. http://k.lenz.name/LB/archives/2003_05.html
 177. http://k.lenz.name/LB/archives/2003_04.html
 178. http://k.lenz.name/LB/archives/2003_03.html
 179. http://k.lenz.name/LB/archives/2003_02.html
 180. http://k.lenz.name/LB/archives/2003_01.html
 181. http://k.lenz.name/LB/archives/000968.html
 182. http://k.lenz.name/LB/archives/000967.html
 183. http://k.lenz.name/LB/archives/000966.html
 184. http://k.lenz.name/LB/archives/000965.html
 185. http://k.lenz.name/LB/archives/000964.html
 186. http://k.lenz.name/LB/archives/000963.html
 187. http://k.lenz.name/LB/archives/000962.html
 188. http://k.lenz.name/LB/archives/000961.html
 189. http://k.lenz.name/LB/archives/000960.html
 190. http://k.lenz.name/LB/archives/000959.html
 191. http://www.freecherrypy.org/asbradbury/archive/0434101
 192. http://www.corante.com/copyfight/archives/031322.php
 193. http://www.discourse.net/archives/2004/12/german_abu_ghraib_complaint.html
 194. file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/europarl0309/lenz0412/index.rdf
 195. file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/europarl0309/lenz0412/index.xml
 196. file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/europarl0309/lenz0412/indexshort.rdf
 197. file://localhost/ul/sig/srv/res/www/ffii/swpat/papri/europarl0309/lenz0412/indexshort.xml
 198. http://www.movabletype.org/
 199. http://feeds.archive.org/validator/docs/howto/MovableType.html
