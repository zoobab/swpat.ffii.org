\begin{subdocument}{europarl0309}{Europarl 2003-09-24: Amended Software Patent Directive}{http://swpat.ffii.org/papri/europarl0309/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.}
\begin{sect}{text}{Directive on the patentability of computer-implemented inventions}
\begin{sect}{art1}{Article 1: Purpose}
This Directive lays down rules for the patentability of computer-implemented inventions.
\end{sect}

\begin{sect}{art2}{Article 2: Definitions}
2a. ``computer-implemented invention'' means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

2b. ``technical contribution'', also called ``invention'', means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

2c. ``technical field'' means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. ``Technical'' means ``belonging to a technical field''.

2d. ``industry'' in the sense of patent law means ``automated production of material goods'';
\end{sect}

\begin{sect}{art3a}{Article 3a: Fields of Technology}
3a. Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.
\end{sect}

\begin{sect}{art4}{Article 4: Rules of Patentability}
4.1. In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

4.2. Member States shall ensure that a computer-implemented invention making a technical contribution constitutes a necessary condition of involving an inventive step.

4.3. The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art, irrespective of whether or not such features are accompanied by non-technical features.

4.3a. In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of natures and has an industrial application in the strict sense of the expression, in terms of both method and result.
\end{sect}

\begin{sect}{art4a}{Article 4a: Exclusions from patentability}
4a.1. A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

4a.2. Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.
\end{sect}

\begin{sect}{art5}{Article 5: Form of Claims; and further provisions}
5. Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

5a. Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

5b. Member States shall ensure that patent claims granted in respect of computer-implemented inventions include only the technical contribution which justifies the patent claim. A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.

5c. Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

5d. Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.
\end{sect}

\begin{sect}{art6}{Article 6: Interoperability}
6. The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

6a. Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.
\end{sect}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf The amendments approved, from the EP.\footnote{\url{http://www3.europarl.eu.int/omk/omnsapir.so/pv2?PRG=DOCPV&APP=PV2&LANGUE=EN&SDOCTA=2&TXTLST=1&POS=1&Type_Doc=RESOL&TPV=PROV&DATE=240903&PrgPrev=PRG@TITRE|APP@PV2|TYPEF@TITRE|YEAR@03|Find@\%2a\%69\%6e\%76\%65\%6e\%74\%69\%6f\%6e\%73|FILE@BIBLIO03|PLAGE@1&TYPEF=TITRE&NUMB=1&DATEF=030924}}}}

\begin{quote}
consolidated version by EP, changes were still being applied by the secretariat during the days after the vote.  E.g. ``contribute'' in ``technical features that any invention must contribute'' was changed to ``posess''.
\end{quote}
\filbreak

\item
{\bf {\bf EU Parliament Votes for Real Limits on Patentability\footnote{\url{http://localhost/swpat/lisri/03/plen0924/index.en.html}}}}

\begin{quote}
In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on ``patentability of computer-implemented inventions'' with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{\url{}}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Analysis of the European Parliament's Vote of 2003/09/24\footnote{\url{}}}}

\begin{quote}
Who voted for innovation, who for litigation?  Comparison of the voting behaviour of all MEPs with the FFII's voting list
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

