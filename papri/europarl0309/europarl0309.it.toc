\select@language {italian}
\contentsline {section}{\numberline {1}Directive on the patentability of computer-implemented inventions}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}\v {C}l\'{a}nek 1: Purpose}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}\v {C}l\'{a}nek 2: Definitions}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}\v {C}l\'{a}nek 3a: Fields of Technology}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}\v {C}l\'{a}nek 4: Rules of Patentability}{2}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}\v {C}l\'{a}nek 4a: Cause di esclusione dalla brevettabilit\`{a}}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}\v {C}l\'{a}nek 5: Form of Claims; and further provisions}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}\v {C}l\'{a}nek 6: Interoperability}{4}{subsection.1.7}
\contentsline {section}{\numberline {2}Annotated Links}{4}{section.2}
