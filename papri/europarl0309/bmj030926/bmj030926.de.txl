<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#bmj030926T: BMJ-PE 2003-09-26: Nur ABS, nicht Software als solche

#BmjOrig030926D: Original der ausweichenden PE des BMJ

#egn: Patente für computergestützte Erfindungen?

#enn: Eine Frage, die niemand verneint.  Die Streitfrage ist vielmehr, ob
eine Anweisung zum Betrieb von Universalrechnern eine %(q:Erfindung)
im Sinne des Patentrechts darstellen kann.

#ufx: Das Europäische Parlament hat in seiner Sitzung am 24. September 2003
in erster Lesung über Änderungsvorschläge zum Richtlinienvorschlag der
Europäischen Kommission über die Patentierbarkeit
computerimplementierter Erfindungen abgestimmt und den Text in
geänderter Fassung mit deutlicher Mehrheit angenommen.

#ste: Bei der Patentierbarkeit computerimplementierter Erfindungen handelt
es sich um ein bereits seit längerem kontrovers diskutiertes Thema,
das besonders seit Vorlage des Richtlinienvorschlags der Europäischen
Kommission im Februar 2002 große Aufmerksamkeit erregt. Der häufig
benutzte Begriff der %(q:Softwarepatente) ist dabei irreführend, da
bereits nach heute geltendem Recht keine Patente auf reine Software,
sondern nur auf mit Hilfe von Software realisierte technische
Erfindungen erteilt werden dürfen (beispielsweise die
Programmsteuerung eines ABS-Bremssystems). Reine Quellcodes können nur
urheberrechtlichem Schutz unterfallen.

#tWn: Nach dem heute geltenden Europäischen Patentübereinkommen stellen
Abläufe auf Datenverarbeitungsanlagen keine %(q:Erfindungen) im Sinne
des Patentrechts dar.  Daher hat das Europäische Patentamt bis 1986
korrekterweise die Erteilung von Patenten u.a. auf Ansprüche der
folgenden Art %(vw:verweigert):

#urW: System und Methode zur Verwaltung digitaler Dokumente mithilfe eines
Computer-Netzwerks, dadurch gekennzeichnet, dass in Speicherblock A
der Wert V1 eingespeichert ...

#pez: Doch einem Teil der heutigen Rechtsprechung zufolge handelt es sich
bereits hierbei (und nicht erst bei einem Antiblockiersystem, bei dem
Reifen und Bremskräfte im Vordergrund stehen) um eine %(e:technische
Erfindung).

#rsr: Das BMJ weicht nicht nur der entscheidenden Frage aus, sondern
suggeriert auch, dass mit dem Begriff %(q:Programme für
Datenverarbeitungsanlagen) nichts von der Patentierbarkeit
ausgeschlossen wird, was irgend jemanden interessieren könnte.  Denn
die Beanspruchung von %(q:reinen Quellcodes), was auch immer damit
gemeint sein könnte, steht und stand nie zur Debatte.

#Wtc: Das BMJ redet hiermit einem Gesetzesverständnis des Wort, das nicht
dem der deutschen Rechtsprechung entspricht und sich mit den üblichen
Methoden der Rechtsauslegung nicht vereinbaren lässt.

#ien: Durch Propagierung des EPA-Neusprech-Begriffs
%(q:computer-implementierte Erfindungen) führt das BMJ seine Leser in
die Irre.  Unter %(q:computer-implementierte Erfindung) versteht man
normalerweise eben gerade nicht das Antiblockiersystem, bei dessen
Implementierung Autoreifen und Bremskräfte im Mittelpunkt stehen,
sondern ein System zur Verarbeitung von Daten sowie Standard-Elementen
abstrakter Datenverarbeitungsmaschinen wie z.B. der
Von-Neumann-Maschine.  Mit %(q:computer-implementierte Erfindung)
werden genau jene der Gesetzeslage nach nicht patentfähigen
Anspruchskonstrukte bezeichnet, für die sich der Begriff
%(q:Softwarepatente) eingebürgert hat.

#JdW: Wieder einmal besteht der einzige bisherige Beitrag des BMJ zur
Debatte in einem Versuch, bisherige funktionierende Kommunikation
stören und an ihrer Stelle doppeldeutige Begriffe zu etablieren, mit
denen sich jede beliebige Rechtsprechungspraxis rechtfertigen lässt. 
Eine bessere ständische Interessenvertretung für die Patentjustiz als
das BMJ sie leistet, könnte es nicht geben.  Von gesetzgebeischer
Verantwortlichkeit und Verbindlichkeit fehlt jedoch jede Spur.

#Wsm: Eine Konkretisierung der Patentierungsvoraussetzungen und damit eine
Harmonisierung des Patentrechts und der Patentpraxis in den
EU-Mitgliedstaaten und der Europäischen Patentorganisation ist Ziel
des Richtlinienentwurf. Dabei soll kein Sonderrecht für
computerimplementierte Erfindungen geschaffen werden, sondern die
allgemeinen Patentierungsvoraussetzungen sollen für die speziellen
Bedürfnisse dieses Anwendungsfeldes definiert werden.  Die
Patentierbarkeit reiner Software soll weiterhin ausgeschlossen sein,
ebenso soll durch klare Regeln die Erteilung von Trivialpatenten, z.B.
Patente auf reine Geschäftsmethoden, vermieden werden. Erwünscht ist
also keineswegs eine Ausweitung der derzeitigen Patentierungspraxis,
eher eine Einschränkung derselben.

#irW: Zwischen %(q:Trivialpatenten) und %(q:Patenten auf reine
Geschäftsmethoden) gibt es zwar Überlappungen, aber die Begriffe haben
nichts miteinander zu tun.  Ferner kommt es nicht darauf an, was
erwünscht ist, sondern was mit den Vorschlägen in Wirklichkeit
erreicht wird.  Nur der Vorschlag des europäischen Parlaments ist
geeignet, die genannten Ziele zu erreichen.  Die Kommission und das
BMJ hingegen vertreten %(mp:Mogelpackungen).  Sie haben bisher
keinerlei Bereitschaft gezeigt, deren Inhalt an den vorgeblichen
Zielsetzungen messen zu lassen.

#ico: Durch die genannte EU-Richtlinie soll ein Ausgleich geschaffen werden
zwischen den Zielen, einerseits %(q:klassische) Erfindungen mit
Computerbezug, die einen technischen Beitrag leisten, weiterhin durch
Patente zu schützen und andererseits die Entstehung einer
Software-Monokultur durch eine zu umfassende Patentierungspraxis zu
verhindern.

#eDo: In einem nächsten Verfahrensschritt wird der Rat der Europäischen
Union sich mit den durch das Parlament vorgenommenen Änderungen
befassen. Der abgeänderte Text wird zunächst auf Fachebene in der
zuständigen Ratsarbeitsgruppe geprüft, bevor sich der EU-Ministerrat
mit den Änderungen beschäftigt.

#irs: Leider hat das BMJ bisher keinerlei fachliche Kompetenz bei der
Überprüfung der Wirkungen von Vorschlägen gezeigt.  Seine Kompetenz
beschränkt sich bisher auf das Jonglieren mit patentrechtlichen
Begriffen im Dienste nicht näher erläuterter Interessen.  Es ist zu
befürchten, dass in der deutschen Delegation in der Ratsarbeitsgruppe
weiterhin nur Patentjuristen anwesend sein werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bmj030926 ;
# txtlang: de ;
# multlin: t ;
# End: ;

