<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mpW: The amendments approved, from the EP.

#sdn: consolidated version by EP, changes were still being applied by the
secretariat during the days after the vote.  E.g. %(q:contribute) in
%(q:technical features that any invention must contribute) was changed
to %(q:posess).

#WMW: Europarl 03-09-24: Arlene McCarthy's summary of the vote

#fth: Mentions mainly the parts of the vote that are congruent with her own
proposals and unimportant aspects, such as the novelty grace period,
but omits everything that really limits patentability.

#purp: Purpose

#defi: Definitions

#efn: Fields of Technology

#eai: Rules of Patentability

#uoa: Cause di esclusione dalla brevettabilità

#ono: Form of Claims; and further provisions

#tel: Interoperability

#mon: Monitoring

#rep: Report on the effects of the Directive

#ass: Impact assessment

#titl: Directive on the patentability of computer-implemented inventions

#Art1: This Directive lays down rules for the patentability of
computer-implemented inventions.

#Am36: %(q:invenzione attuata per mezzo di elaboratori elettronici),
un'invenzione ai sensi della Convenzione per il brevetto europeo la
cui esecuzione implica l'uso di un elaboratore, di una rete di
elaboratori o di un altro apparecchio programmabile e che presenta
nelle sue applicazioni una o più caratteristiche non tecniche che sono
realizzate in tutto o in parte per mezzo di uno o più programmi per
elaboratore, oltre al contributo tecnico che ogni invenzione deve
arrecare;

#Am107: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in technical field. The technical
character of the contribution is one of the four requirements for
patentability. Additionally, to deserve a patent, the technical
contribution has to be new, non-obvious, and susceptible of industrial
application. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes.

#Am97: %(q:technical field) means an industrial application domain requiring
the use of controllable forces of nature to achieve predictable
results. %(q:Technical) means %(q:belonging to a technical field).

#Am38: %(q:industria), ai sensi del diritto dei brevetti, produzione
automatizzata di beni materiali;

#Am45: Gli Stati membri assicurano che l'elaborazione dei dati non venga
considerata un settore della tecnologia ai sensi del diritto dei
brevetti e che le innovazioni nel settore dell'elaborazione di dati
non vengano considerate invenzioni ai sensi del diritto dei brevetti.

#Am16N1: In order to be patentable, a computer-implemented invention must be
susceptible of industrial application and new and involve an inventive
step. In order to involve an inventive step, a computer-implemented
invention must make a technical contribution.

#Am16N2: Member States shall ensure that a computer-implemented invention
making a technical contribution constitutes a necessary condition of
involving an inventive step.

#Am100P1: The significant extent of the technical contribution shall be assessed
by consideration of the difference between the technical elements
included in the scope of the patent claim considered as a whole and
the state of the art, irrespective of whether or not such features are
accompanied by non-technical features.

#Am70: Per determinare se una data invenzione attuata per mezzo di
elaboratore elettronico arreca un contributo tecnico, si applica il
seguente criterio: si valuta se essa costituisce un nuovo insegnamento
sulle relazioni di causa-effetto nell'impiego delle forze
controllabili della natura e se ha un'applicazione industriale nel
senso stretto dell'espressione, in termini sia di metodo che di
risultato.

#Am17: Un'invenzione attuata per mezzo di elaboratori elettronici non è
considerata arrecante un contributo tecnico semplicemente perchè
implica luso di un elaboratore, di una rete o di un altro apparecchio
programmabile. Pertanto, non sono brevettabili le invenzioni
implicanti programmi per elaboratori che applicano metodi per attività
commerciali, metodi matematici o di altro tipo e non producono alcun
effetto tecnico oltre a quello delle normali interazioni fisiche tra
un programma e lelaboratore, la rete o un altro apparecchio
programmabile in cui viene eseguito.

#Am60: Gli Stati membri garantiscono che le soluzioni attuate mediante
elaboratore elettronico rispetto a problemi tecnici non siano
considerate invenzioni brevettabili solo perchè migliorano l'efficacia
nell'impiego delle risorse del sistema di trattamento dei dati.

#Am101: Member States shall ensure that a computer-implemented invention may
be claimed only as a product, that is as a programmed device, or as a
technical production process.

#Wce: Member States shall ensure that patent claims granted in respect of
computer-implemented inventions include only the technical
contribution which justifies the patent claim. A patent claim to a
computer program, either on its own or on a carrier, shall not be
allowed.

#Am103: Gli Stati membri assicurano che la produzione, la manipolazione, il
trattamento, la distribuzione e la pubblicazione di informazioni, in
qualsiasi forma, non possano mai costituire una violazione di
brevetto, diretta o indiretta, anche se a tale fine sono stati
utilizzati dispositivi tecnici.

#Am104N1: Gli Stati membri assicurano che l'uso di un programma per elaboratore
per scopi che non riguardano l'oggetto del brevetto non possa
costituire una violazione di brevetto diretta o indiretta.

#Am104N2: Gli Stati membri assicurano che, qualora una rivendicazione di
brevetto menzioni caratteristiche che implicano l'uso di un programma
per elaboratore, un'applicazione di riferimento, ben funzionante e ben
documentata, di tale programma sia pubblicata come parte della
descrizione senza condizioni di licenza restrittive.

#Am19: La protezione conferita dai brevetti per le invenzioni che rientrano
nel campo d'applicazione della presente direttiva lascia
impregiudicate le facoltà riconosciute dalla direttiva 91/250/CEE
relativa alla tutela giuridica dei programmi per elaboratore mediante
il diritto d'autore, in particolare le disposizioni relative alla
decompilazione e all'interoperabilità o le disposizioni relative alle
topografie dei semiconduttori o ai marchi commerciali.

#Am76P1: Gli Stati membri assicurano che, in ogni caso in cui l'uso di una
tecnica brevettata sia necessario per un fine importante quale ad
esempio garantire la conversione delle convenzioni utilizzate in due
diversi sistemi o reti di elaboratori elettronici, così da consentire
la comunicazione e lo scambio dei dati fra di essi, detto uso non sia
considerato una violazione di brevetto.

#Am71: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, and on European businesses, especially small and
medium-sized enterprises and the open source community, and electronic
commerce.

#Art81: The Commission shall report to the European Parliament and the Council
by [DATE (three years from the date specified in Article 9(1))] at the
latest on

#Art81a: the impact of patents for computer-implemented inventions on the
factors referred to in Article 7;

#Am92: whether the rules governing the term of the patent and the
determination of the patentability requirements, and more specifically
novelty, inventive step and the proper scope of claims, are adequate;
and

#Art82c: whether difficulties have been experienced in respect of Member States
where the requirements of novelty and inventive step are not examined
prior to issuance of a patent, and if so, whether any steps are
desirable to address such difficulties.

#Am23: whether difficulties have been experienced in respect of the
relationship between the protection by patent of computer-implemented
inventions and the protection by copyright of computer programs as
provided for in Directive 91/250/EEC and whether any abuse of the
patent system has occurred in relation to computer-implemented
inventions;

#Am24: whether it would be desirable and legally possible having regard to
the Community's international obligations to introduce a 'grace
period' in respect of elements of a patent application for any type of
invention disclosed prior to the date of the application;

#Am25: the aspects in respect of which it may be necessary to prepare for a
diplomatic conference to revise the Convention on the Grant of
European Patents, also in the light of the advent of the Community
patent;

#Am26: how the requirements of this Directive have been taken into account in
the practice of the European Patent Office and in its examination
guidelines.

#Am81: whether the powers delegated to the EPO are compatible with the need
to harmonise Community legislation, and with the principles of
transparency and accountability.

#Am89: the impact on the conversion of the conventions used in two different
computer systems to allow communication and exchange of data;

#Am93: whether the option outlined in the Directive concerning the use of a
patented invention for the sole purpose of ensuring interoperability
between two systems is adequate;

#Am94: In this report the Commission shall justify why it believes an
amendment of the Directive in question necessary or not and, if
required, will list the points which it intends to propose an
amendment to.

#Am27: In the light of the monitoring carried out pursuant to Article 7 and
the report to be drawn up pursuant to Article 8, the Commission shall
assess the impact of this Directive and, where necessary, submit
proposals for amending legislation to the European Parliament and the
Council.

#rFr: CEC proposal and FFII counter-proposal.

#nin: Amendment proposals on which the EP voted on 2003/09/24

#ent: Amendment Proposals on which JURI voted in June

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl0309 ;
# txtlang: it ;
# multlin: t ;
# End: ;

