<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#EuroparlFaq0309T: FAQ: The EU Vote on Software Patents

#EuroparlFaq0309D: Answers to Questions about the Vote of the European Parliament of 2003/09/24, its Implications and the Road Ahead.

#tWp: What was it that happened?

#Wgr: Is this good or bad?

#Ico: As a layman, I really can't see much difference between the directive %(q:before) and %(q:after)!

#Wes: Why does %(q:the other side) also claim it as a victory?

#hwe: Is this the law in Europe now?

#tWh: What can I do to help?

#ee7: On September 24, 2003, the European Parliament adopted a directive titled %(q:Patentability of computer-implemented inventions). After having been extensively amended by the parliament, the directive was passed with a large majority (361 for, 157 against, 28 abstained) during a 40-minute voting session.

#hae: In the parliamentary debate the day before the vote, the directive was presented by Member of the European Parliament (MEP) Arlene McCarthy. She stated that it was not about introducing a new patent law, not about patenting software and not about extending the scope of patents. She continued to point out that:

#ssf: In the USA, and increasingly in Japan, patents have unfortunately been granted for what is essentially pure software. An EU directive, by setting limits to patentability in this area, could stop the drift in Europe towards a US-liberal style of patenting software as such, and indeed of patenting pure business methods.

#oeX: One oft-quoted example of such a bad patent is Amazon's 'one-click' shopping method. Clearly this technology is not new, nor is it unique, and patenting of software business methods such as this is not good for innovation and competition. It is unfortunate that the [European Patent Office] EPO has granted it a patent: this is an example of bad EPO practice.

#ucq: While most other parliament members who subsequently spoke in the debate were in agreement with these general remarks, many expressed concern that the directive as drafted failed to achieve the stated objectives, and might lead to %(q:leakage resulting in the patenting of pure software).

#tan: The FFII, as well as other organizations, enterprises, and individuals that are opposed to the introduction of software patents, share this latter opinion.

#eat: It would appear that a majority of the Members of the European Parliament also do, since, in the vote that followed the debate the day after, the parliament choose to incorporate a large number of amendments to the directive before adopting the it, so that, in its opinion, the effects of the directive would be more in line with the stated objectives of Ms. McCarthy and other proponents of the original proposal.

#rnt: Some have described these amendments as fundamentally reversing the effects of the directive.

#slW: It's actually quite good.

#tth: We know, of course, that despite their %(pc:protestations to the contrary), what the rapporteur %(AM) and other directive proponents were really trying to do is to legalize the current EPO practice and sanction the introduction of software patents in Europe.

#eaa: If the directive had passed in its %(of:original form), without all the amendments that the parliament made, this would have created a situation in Europe that was just as bad as the one in America. The original directive would have turned %(ep:article 52 of the European Patent Convention EPC), which clearly and unambiguously states that computer programs are not inventions and therefore cannot be patented, into a meaningless formality.

#ehW: As is bound to happen when 78 amendments are voted in 40 minutes, there are still some inconsistencies in the directive that need to be corrected, but there is nothing wrong with it that cannot be fixed when the European Parliament reviews the directive in a second reading.

#orP: Either the Council reverts to the original version which introduces unlimited patentability while claiming to do the opposite, or it continues on the path the Parliament is pointing at, to keep Europe free of software patents.

#sWp: That's because you were not supposed to.

#eia: Throughout this affair, Arlene McCarthy and other proponents of the directive's initial version have been using an fraudulent communication strategy.  In all of their public representations to the parliament and to the media, they have maintained that this is only a technical issue of harmonizing judicial practice, and that the only effect will be to restrict the scope of software patents.  However, while the original proposal may appear to a casual observer to be doing just this, it is %(et:filled with barn door sized loopholes) that any competent patent lawyer can immediately spot and take advantage of, so that he will always be able to draft a patent application that fulfills the formal criteria for any given piece of software.

#oyW: For example, some of the key elements in drawing the line between computer-controlled washing machines (which should be patentable if they use heat and humidity in an inventive way) and pure computing logic, such as Amazon's one-click scheme, which McCarthy claims would not be patentable under her proposal, are certain provisions about %(q:industrial) application and %(q:technical) effects. Provisions like these can indeed be found in the original proposal, and will quite likely cause an unsuspecting reader to think that all is well.

#iei: Is it? Well, actually not, because by conveniently omitting to properly define the terms %(q:industry) and %(q:technical), the original proposal leaves the door wide open for patent lawyers to claim that the one-click patent is applicable to the %(q:book-selling industry), and that it has the %(q:technical effect) of reducing the number of mouse clicks when you order a book.

#usW: Think this sounds paranoid? You have obviously never been involved in any patent litigation.

#toW: Think it was merely an accidental omission? You are probably not aware of how vigorously the patent lobby opposed the insertion of the definitions as they stand today.

#tWt: The relevant part of Article 2 in the amended and adopted directive now reads:

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field).

#Am38: %(q:industry) in the sense of patent law means %(q:automated production of material goods);

#itW: To a patent examiner, this means that the one-click patent can be rejected outright, since it teaches nothing new about how to use forces of nature to produce material goods.

#dct: McCarthy voted against these amendments, as did most of those who had fought for the Commission's original proposal by claiming that their aim was to %(q:close loopholes) and %(q:avoid a drift toward unlimited patentability).

#Wey: It doesn't, anymore.

#Whh: When it was first reported that the directive had been passed, many assumed that it was a directive with essentially the same contents as the original one. As it turned out, this was not really a warranted assumption.

#ilW: It seems now that the patent friendly forces are focusing on %(er:questioning the competence of the parliament to handle such a complicated issue), or to %(ai:suggest that the Commission should withdraw the proposal).

#WWW: No, not by a long shot.

#vxp: The directive has now gone through %(e:first reading) in the European Parliament.

#ouj: The next step is that it gets sent to the Council of Ministers, which will amend the directive as it sees fit, before it gets sent back to the European Parliament for %(e:second reading), where the parliament will be asked to either accept or reject the directive proposal more or less as it then stands.

#Wit: If the proposal is rejected it will be passed on to a Reconciliation Committee, which consists of representatives from both the parliament and the Council, and will try to put together a compromise proposal that is acceptable to all parties. This proposal will then be sent to the parliament once more, where it is either rejected or passed.

#eoe: Assuming that the directive is passed, the member states then have 18 months to incorporate the directive into their respective national legislations.

#sWt: The fight against software patents in Europe is not yet won, and there are many things that need to be done.

#Wcr: Please consult %(sg:this page) to see what you can do to help keep Europe free from software patents.

#pst: A major improvement would be to remove the term %(ki:computer-implemented invention) from the directive.  Although this term has been re-defined by the Parliament's %(Am:amendment 117), so that it now really refers to washing machine inventions rather than software innovations, the term remains misleading.  The term %(q:computer-implemented invention) was %(ep:launched by the EPO in 2000) as part of a fraudulent communication strategy and becomes a misnomer when redefined outside of this context of this strategy.  Patent practitioners who do not have Amendment 117 in mind will still try to refer to programs for computers as %(q:computer-implemented inventions), since this is what the term literally suggests when read by a person skilled in the art of patenting.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/europarl0309.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: europarl-faq0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

