
   [Draft no. 3]

    FAQ: The EU vote on software patents

    What was it that happened?

   On September 24, 2003, the European Parliament adopted a directive
   titled "Patentability of computer-implemented inventions". After
   having been extensively amended by the parliament, the directive was
   passed with a large majority (361 for, 157 against, 28 abstained)
   during a 40-minute voting session.

   In the parliamentary debate the day before the vote, the directive was
   presented by Member of the European Parliament (MEP) Arlene McCarthy.
   She stated that it was not about introducing a new patent law, not
   about patenting software and not about extending the scope of patents.
   She continued to point out that:

   "In the USA, and increasingly in Japan, patents have unfortunately
   been granted for what is essentially pure software. An EU directive,
   by setting limits to patentability in this area, could stop the drift
   in Europe towards a US-liberal style of patenting software as such,
   and indeed of patenting pure business methods.

   One oft-quoted example of such a bad patent is Amazon's 'one-click'
   shopping method. Clearly this technology is not new, nor is it unique,
   and patenting of software business methods such as this is not good
   for innovation and competition. It is unfortunate that the [European
   Patent Office] EPO has granted it a patent: this is an example of bad
   EPO practice." 

   While most other parliament members who subsequently spoke in the
   debate were in agreement with these general remarks, many expressed
   concern that the directive as drafted failed to achieve the stated
   objectives, and might lead to "leakage resulting in the patenting of
   pure software".

   The FFII, as well as other organizations, enterprises, and individuals
   that are opposed to the introduction of software patents, share this
   latter opinion.

   It would appear that a majority of the Members of the European
   Parliament also do, since, in the vote that followed the debate the
   day after, the parliament choose to incorporate a large number of
   amendments to the directive before adopting the it, so that, in its
   opinion, the effects of the directive would be more in line with the
   stated objectives of Ms. McCarthy and other proponents of the original
   proposal.

   Some have described these amendments as "fundamentally reversing" the
   effects of the directive.

   You can view the EU:s own plain language summary of the adopted
   directive [1]here, and the FFII:s analysis of the amendments [2]here.

    Is this good or bad?

   It's actually quite good.

   We know, of course, that despite her protestations to the contrary,
   what Ms. McCarthy is really trying to do is to legalize the current
   EPO practice and sanction the introduction of software patents in
   Europe, while pretending that this is merely a technical issue of
   harmonization.

   If the directive had passed in its original form, without all the
   amendments that the parliament made, this would have been very bad
   indeed, and would have created a situation in Europe that was just as
   bad as the one in America. The original directive was filled with
   loopholes that would have turned [3]article 52 of the European Patent
   Convention EPC, which clearly and unambiguously states that computer
   programs are not inventions and therefore cannot be patented, into a
   meaningless formality that any patent lawyer could circumvent by
   adding a couple of phrases to the patent claims.

   The directive as it is written now, before it gets to the next stage
   of modifications by the Council of Ministers, partially reintroduces
   the fundamental EPC concept of "technical invention" and possibly
   excludes patents on programs for computers. Inventions where software
   is a part of some technical device remain patentable. Examples where
   software might be a part of such inventions are anti-lock braking
   systems (that require computer control of its operation), or an
   innovative washing machine.

   This is fine, as long as the line between genuine inventions that
   happen to contain software, and software as such, is kept well defined
   in watertight language.

   Now, many small and medium sized companies that do have the resources
   to build innovative software, but cannot afford to get drawn into a
   costly patent litigation battle with a major corporation, are
   hopefully safe, depending on in which direction the Council will
   further amend the text.

   As is bound to happen when 78 amendments are voted in 40 minutes,
   there are still some glitches and technical inconsistencies in the
   directive that need to be corrected, but there is nothing wrong with
   it that cannot be fixed before the European Parliament has to review
   the directive in second reading.

   Everybody agrees that the directive must now be amended to be wholly
   consistent. Either the Council reverts to the original version which
   introduces unlimited patentability while claiming to do the opposite,
   or it continues on the path the Parliament is pointing at, to keep
   Europe one-click free.

    As a layman, I really can't see much difference between the directive
    "before" and "after"!

   That's because you were not supposed to.

   Throughout this affair, Arlene McCarthy and other proponents of the
   software patent have been using an argumentation strategy that can
   only be described as dishonest. In all of her public representations
   to the parliament and to the media, she has maintained that this is
   only a technical issue of harmonizing practice, and that the only
   effect will be to restrict the scope of software patents. The truth
   is, that while the original proposal may appear to a casual observer
   to be doing just this, it is deliberately filled with barn door sized
   loopholes that any competent patent lawyer can immediately spot and
   take advantage of, so that he will always be able to draft a patent
   application that fulfills the formal criteria for any given piece of
   software.

   For example, some of the key elements in drawing the line industrial
   products like washing machines containing software, which should be
   patentable, and features realized by ordinary software, like Amazon's
   one-click patent, which McCarthy claims would not be patentable, are
   certain provisions about "industrial" application and "technical"
   effects. Provisions like these can indeed be found in the original
   proposal, and will quite likely cause an unsuspecting reader to think
   that all is well.

   Is it? Well, actually not, because by conveniently omitting to
   properly define the terms "industry" and "technical", the original
   proposal leaves the door wide open for patent lawyers to claim that
   the one-click patent is applicable to the "book-selling industry", and
   that it has the "technical effect" of reducing the number of mouse
   clicks when you order a book.

   Think this sounds paranoid? You have obviously never been involved in
   any patent litigation.

   Think it was merely an accidental omission? You are probably not aware
   of how vigorously the patent lobby opposed the insertion of the
   definitions as they stand today. Read about it [4]here if you are
   interested.

   The relevant part of Article 2 in the amended and adopted directive
   now reads:

   2c. "technical field" means an industrial application domain requiring
   the use of controllable forces of nature to achieve predictable
   results. "Technical" means "belonging to a technical field". The use
   of forces of nature to control physical effects beyond the digital
   representation of information belongs to a technical domain. The
   production, handling, processing, distribution and presentation of
   information do not belong to a technical field, even when technical
   devices are employed for such purposes. 

   2d. "industry" in the sense of patent law means "automated production
   of material goods"; 

   To a patent examiner, this means that the one-click patent can be
   rejected outright, since the single click does not control the forces
   of nature except to represent information, and, furthermore, the
   "book-selling industry" does not deal with the automated production of
   material goods.

   If McCarthy had really wanted to avoid the one-click patent, this is
   how to do it. She voted against.

    Why does "the other side" also claim it as a victory?

   It doesn't, anymore.

   When it was first reported that the directive had been passed, many
   assumed that it was a directive with essentially the same contents as
   the original one. As it turned out, this was not really a warranted
   assumption.

   It seems now that the patent friendly forces are focusing on
   [5]questioning the competence of the parliament to handle such a
   complicated issue, or to suggest that the Commission should
   [6]withdraw the proposal.

    Is this the law in Europe now?

   No, not by a long shot.

   The directive has now gone through "first reading" in the European
   Parliament.

   The next step is that it gets sent to the Council of Ministers, which
   will amend the directive as it sees fit, before it gets sent back to
   the European Parliament for "second reading", where the parliament
   will be asked to either accept or reject the directive proposal more
   or less as it then stands.

   If the proposal is rejected it will be passed on to a Reconciliation
   Committee, which consists of representatives from both the parliament
   and the Council, and will try to put together a compromise proposal
   that is acceptable to all parties. This proposal will then be sent to
   the parliament once more, where it is either rejected or passed.

   Assuming that the directive is passed, the member states then have 18
   months to incorporate the directive into their respective national
   legislations.

    What can I do to help?

   The fight against software patents in Europe is not yet won, and there
   are many things that need to be done.

   Please consult [7]this page to see what you can do to help keep Europe
   one-click free.

Verweise

   1. http://www2.europarl.eu.int/omk/sipade2?SAME_LEVEL=1&LEVEL=3&NAV=S&PUBREF=-//EP//TEXT+PRESS+DN-20030924-1+0+DOC+XML+V0//EN#SECTION2
   2. http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/
   3. http://www.european-patent-office.org/legal/epc/e/ar52.html#A52
   4. http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html#newspeak
   5. http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm
   6. http://www.computing.co.uk/News/1144100
   7. http://swpat.ffii.org/group/todo/index.en.html
