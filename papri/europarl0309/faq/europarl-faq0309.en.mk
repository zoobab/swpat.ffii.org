# -*- mode: makefile -*-

europarl-faq0309.en.lstex:
	lstex europarl-faq0309.en | sort -u > europarl-faq0309.en.lstex
europarl-faq0309.en.mk:	europarl-faq0309.en.lstex
	vcat /ul/prg/RC/texmake > europarl-faq0309.en.mk


europarl-faq0309.en.dvi:	europarl-faq0309.en.mk
	rm -f europarl-faq0309.en.lta
	if latex europarl-faq0309.en;then test -f europarl-faq0309.en.lta && latex europarl-faq0309.en;while tail -n 20 europarl-faq0309.en.log | grep -w references && latex europarl-faq0309.en;do eval;done;fi
	if test -r europarl-faq0309.en.idx;then makeindex europarl-faq0309.en && latex europarl-faq0309.en;fi

europarl-faq0309.en.pdf:	europarl-faq0309.en.ps
	if grep -w '\(CJK\|epsfig\)' europarl-faq0309.en.tex;then ps2pdf europarl-faq0309.en.ps;else rm -f europarl-faq0309.en.lta;if pdflatex europarl-faq0309.en;then test -f europarl-faq0309.en.lta && pdflatex europarl-faq0309.en;while tail -n 20 europarl-faq0309.en.log | grep -w references && pdflatex europarl-faq0309.en;do eval;done;fi;fi
	if test -r europarl-faq0309.en.idx;then makeindex europarl-faq0309.en && pdflatex europarl-faq0309.en;fi
europarl-faq0309.en.tty:	europarl-faq0309.en.dvi
	dvi2tty -q europarl-faq0309.en > europarl-faq0309.en.tty
europarl-faq0309.en.ps:	europarl-faq0309.en.dvi	
	dvips  europarl-faq0309.en
europarl-faq0309.en.001:	europarl-faq0309.en.dvi
	rm -f europarl-faq0309.en.[0-9][0-9][0-9]
	dvips -i -S 1  europarl-faq0309.en
europarl-faq0309.en.pbm:	europarl-faq0309.en.ps
	pstopbm europarl-faq0309.en.ps
europarl-faq0309.en.gif:	europarl-faq0309.en.ps
	pstogif europarl-faq0309.en.ps
europarl-faq0309.en.eps:	europarl-faq0309.en.dvi
	dvips -E -f europarl-faq0309.en > europarl-faq0309.en.eps
europarl-faq0309.en-01.g3n:	europarl-faq0309.en.ps
	ps2fax n europarl-faq0309.en.ps
europarl-faq0309.en-01.g3f:	europarl-faq0309.en.ps
	ps2fax f europarl-faq0309.en.ps
europarl-faq0309.en.ps.gz:	europarl-faq0309.en.ps
	gzip < europarl-faq0309.en.ps > europarl-faq0309.en.ps.gz
europarl-faq0309.en.ps.gz.uue:	europarl-faq0309.en.ps.gz
	uuencode europarl-faq0309.en.ps.gz europarl-faq0309.en.ps.gz > europarl-faq0309.en.ps.gz.uue
europarl-faq0309.en.faxsnd:	europarl-faq0309.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo europarl-faq0309.en-??.g3n`;source faxsnd main
europarl-faq0309.en_tex.ps:	
	cat europarl-faq0309.en.tex | splitlong | coco | m2ps > europarl-faq0309.en_tex.ps
europarl-faq0309.en_tex.ps.gz:	europarl-faq0309.en_tex.ps
	gzip < europarl-faq0309.en_tex.ps > europarl-faq0309.en_tex.ps.gz
europarl-faq0309.en-01.pgm:
	cat europarl-faq0309.en.ps | gs -q -sDEVICE=pgm -sOutputFile=europarl-faq0309.en-%02d.pgm -
europarl-faq0309.en/europarl-faq0309.en.html:	europarl-faq0309.en.dvi
	rm -fR europarl-faq0309.en;latex2html europarl-faq0309.en.tex
xview:	europarl-faq0309.en.dvi
	xdvi -s 8 europarl-faq0309.en &
tview:	europarl-faq0309.en.tty
	browse europarl-faq0309.en.tty 
gview:	europarl-faq0309.en.ps
	ghostview  europarl-faq0309.en.ps &
print:	europarl-faq0309.en.ps
	lpr -s -h europarl-faq0309.en.ps 
sprint:	europarl-faq0309.en.001
	for F in europarl-faq0309.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	europarl-faq0309.en.faxsnd
	faxsndjob view europarl-faq0309.en &
fsend:	europarl-faq0309.en.faxsnd
	faxsndjob jobs europarl-faq0309.en
viewgif:	europarl-faq0309.en.gif
	xv europarl-faq0309.en.gif &
viewpbm:	europarl-faq0309.en.pbm
	xv europarl-faq0309.en-??.pbm &
vieweps:	europarl-faq0309.en.eps
	ghostview europarl-faq0309.en.eps &	
clean:	europarl-faq0309.en.ps
	rm -f  europarl-faq0309.en-*.tex europarl-faq0309.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} europarl-faq0309.en-??.* europarl-faq0309.en_tex.* europarl-faq0309.en*~
europarl-faq0309.en.tgz:	clean
	set +f;LSFILES=`cat europarl-faq0309.en.ls???`;FILES=`ls europarl-faq0309.en.* $$LSFILES | sort -u`;tar czvf europarl-faq0309.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
