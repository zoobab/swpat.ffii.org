<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#WfS: Zur heutigen abschließenden Beratung im Europäischen Parlament zum
Vorschlag für eine Richtlinie zu %(q:computer-implementierte
Erfindungen) erklären der Bundestagsabgeordnete Ulrich Kelber, MdB,
und der medienpolitische Sprecher der SPD-Bundestagsfraktion, Jörg
Tauss, MdB:

#Wmr: Das Europäische Parlament hat mit seiner heutigen Entscheidung Schaden
von der europäischen Softwareindustrie und von den kleinen und
mittleren Unternehmen abgewendet. Die beschlossenen Änderungen des
Kommissionsvorschlags für eine Richtlinie zu
%(q:computer-implementierten Erfindungen) ­ allgemein als
Softwarepatente-Richtlinie bezeichnet ­ sind nicht nur angemessen und
zielführend, vor allem geben sie der Richtlinie eine auch tatsächlich
innovationsorientierte und Trivialpatente effektiv begrenzende
Fassung. Mit den eingefügten Bestimmungen werden die befürchtete
Inflation trivialer, allein wettbewerbsverzerrender Softwarepatente
wohl ausbleiben und auch die ebenso innovationsleeren wie
volkswirtschaftlich kostspieligen Patentstreitigkeiten verringert.

#tee: Besonders ist zu begrüßen, dass mit dem Bezug auf kontrollierbare
Naturkräfte ein belastbares Technizitätskriterium Eingang gefunden
hat, um patentfähige und nicht patentierbare softwarebasierte Lösungen
trennscharf differenzieren zu können. Für die Open
Source-Entwicklungsprojekte ist zudem insbesondere das eingefügte so
genannte Interoperabilitätsprivileg sowie der Ausschluss von
Patentansprüchen auf Computerprogramme als solche bzw. auf einem
Datenträger von hoher Bedeutung.

#nnW: Dennoch erhalten Unternehmen und Investoren hinreichend Anreize für
eine aktive Patentpolitik zur Sicherung der Investitionskosten gerade
im wachsenden %(q:Embedded-Markt). In der nun vorliegenden Form wird
die Richtlinie nicht nur eine europäische Vereinheitlichung der
Patentpraxis befördern, darüber hinaus wird sie Trittbrettfahrern, die
weniger an Fortschritt als an Wettbewerbsverzerrungen zu ihren Gunsten
interessiert sind, den Zugang zu patentrechtlichen Schutzansprüchen
erschweren.

#hmn: Doch selbst dann blieben offene Fragen bestehen, die
gemeinschaftsrechtlich (noch) nicht lösbar sind, wie etwa das
Verhältnis der Richtlinie zum ­ eigentlich richtigeren Gegenstand für
die Softwarepatente- Diskussion ­ Europäischen Patent-Übereinkommen
oder auch zu den Bemühungen um das so genannte Gemeinschaftspatent.
Dessen ungeachtet wird es nun darauf ankommen, dafür zu sorgen, dass
diese Beschlüsse des Europäischen Parlaments auch in dem weiteren
Verfahren sowohl bei der EU-Kommission wie beim Rat Bestand haben.

#msn: Erklärt den Zusammenhang zwischen Technizität (Erfordernis, dass
Naturkräfte Teil der Problemlösung sind) und Trivialität.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: spdmdb030924 ;
# txtlang: de ;
# multlin: t ;
# End: ;

