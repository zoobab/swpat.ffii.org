<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CEC 2003/11: Secret Nitpicking on European Parliament's Amendments

#descr: The Industrial Property Unit of the Commission of the European Communities (CEC) had stated in October 2003 that it finds the European Parliament's Amendments to its software patent directive proposal mostly inacceptable.  In a confidential document distributed to EU member state governments in November 2003, the Commission's patent officials added some critical notes about each of the amendments of the European Parliament.  The Commision points out that the text deviates from the practise of the European Patent Office in its use of the terminology and in its reasoning.  This is enough for the Commission to find the Parliament's text inacceptable.  Rather than examine the the merits of the Parliament's versus the EPO's approach, the Commission treats the EPO's approach as the absolute authority that must be followed and tries to find fault in the Parliament's legal logic, mostly by misunderstanding this logic or claiming that it is unclear or that it is at odds with some established practise.  Some of these claims are provably untrue.  The Commission's own proposal has been heavily criticised by prominent patent law experts for its incoherence and lack of clarity.

#WKl: Dr. Klaus-Jürgen Melullis

#net: presiding judge

#Sno: Patent Senate

#aWJ: German Federal Court of Justice

#iln: Article published in 2002

#nWp: In particular, it seems to me that the Pension Benefits decision significantly shifted the interface between
the assessment of statutory subject-matter and of inventive step without any consideration of whether such a shift was compatible with existing case law. I find it regrettable therefore that the rationale of Pension Benefits has been so rapidly adopted in both the EPO Guidelines and also the proposed Directive on software patents.

#lov: Dr. Simon Davies

#Wnw: UK patent lawyer

#hvn: Chair of the Forum on Software Inventions at the European Patent Office

#WWi: The Commission is unable to cope with this new expression of technical novelty, and to fit that in with its existing structure of three pillars, either under the heading novelty, or inventiveness, or industrial application. And for this reason, the Commission's text requires clarification, or indeed, correction. I can only describe the Commission's text, moreover, as miserable.

#loy: Willy Rothley MEP

#dsp: vice president of the European Parliament's Legal Affairs Commission (JURI)

#e0k: speech 2003-06-16)

#nme: number

#EUPL: EP

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cec0311.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cec0311 ;
# txtlang: cs ;
# multlin: t ;
# End: ;

