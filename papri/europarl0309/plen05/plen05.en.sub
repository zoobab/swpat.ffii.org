\begin{subdocument}{plen05}{The European Parliament's Work on Software Patents in 2005}{http://swpat.ffii.org/papers/europarl0309/plen05/index.en.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{The European Parliament is facing the challenge of reaffirming the effective exclusion of software patentability for which it had voted on 24th September 2003, so as to be able to force the Council and Commission to conduct a discussion on substance.  Here we try to collect pointers to documents that may be worth consulting by members of the European Parliament when amending the Council's Uncommon Position.}
\begin{sect}{amend}{Amendments}
\begin{itemize}
\item
Draft set of amendments (based on selected articles from the first reading)
\begin{itemize}
\item
Amendments in MS Word doc format\footnote{\url{http://www.ffii.org/\~jmaebe/ep2r/ffiiam050328en.doc}} (last update: 2005/03/31, 10:00 am)

\item
Amendments in Portable Document Format (PDF)\footnote{\url{http://www.ffii.org/\~jmaebe/ep2r/ffiiam050328en.pdf}}
\end{itemize}

\item
The general outline of steps we deem appropriate to start fixing the Council text\footnote{\url{http://swpat.ffii.org/letters/cons0406/step/index.en.html}}
\end{itemize}
\end{sect}

\begin{sect}{news}{News and Chronology}
\begin{center}
\begin{center}
\begin{tabular}{|C{18}|C{63}|}
\hline
2005-04-07 & phm proposes 5-step Questionnaire/Checklist for Software Patent Legislators\footnote{\url{http://wiki.ffii.org/MepsQuest05En}}\\\hline
2005-03-28 & FFII distributes a letter with appendices on paper to all MEPs\footnote{\url{http://wiki.ffii.org/LtrFfiiMeps050329EnEn}}\\\hline
\end{tabular}
\end{center}
\end{center}
\end{sect}

\begin{sect}{ltr0503}{The letter and appendices distributed to MEP mail boxes on 2005-03-28}
\begin{itemize}
\item
Letter with Position about the EP's second reading\footnote{\url{http://www.ffii.org/\~jmaebe/ep2r/ffiiltr050328en.pdf}}

\item
Appendices to the letter\footnote{\url{http://www.ffii.org/\~jmaebe/ep2r/ffiiapp050328en.pdf}}
\end{itemize}
\end{sect}

\begin{sect}{etc}{More information}
\begin{itemize}
\item
Tabular Comparison of Council vs Parliament text\footnote{\url{http://swpat.ffii.org/papers/europarl0309/cons0401/tab/index.en.html}}

\item
FFII interests and the EU Software Patent Directive\footnote{\url{http://swpat.ffii.org/analysis/needs/index.en.html}}

\item
Overview of Proposed Amendments of 2003-09-24\footnote{\url{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html}}

\item
Urgent Appeal to the Council\footnote{\url{http://swpat.ffii.org/letters/cons0406/index.en.html}} with informative appendices such as a short overview of what's wrong with the Council text\footnote{\url{http://swpat.ffii.org/letters/cons0406/text/index.en.html}} and a summary of studies\footnote{\url{http://swpat.ffii.org/letters/cons0406/parl/index.en.html}}

\item
FFII Counter-Proposal of 2002-2003\footnote{\url{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}}
\begin{itemize}
\item
Minimal Proposal\footnote{\url{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/mini/index.en.html}}
\end{itemize}

\item
Polish Government's Unilateral Declaration\footnote{\url{http://wiki.ffii.org/ConsUniPl0502En}}

\item
23 Questions to the Council\footnote{\url{http://swpat.ffii.org/letters/cons050308/index.en.html}}

\item
Short Introduction to the Software Patent Directive Project\footnote{\url{http://swpat.ffii.org/log/intro/index.en.html}}
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/plen05.el ;
% mode: latex ;
% End: ;

