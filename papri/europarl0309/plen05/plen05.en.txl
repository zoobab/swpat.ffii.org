<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The European Parliament's Work on Software Patents in 2005

#descr: The European Parliament is facing the challenge of reaffirming the
effective exclusion of software patentability for which it had voted
on 24th September 2003, so as to be able to force the Council and
Commission to conduct a discussion on substance.  Here we try to
collect pointers to documents that may be worth consulting by members
of the European Parliament when amending the Council's Uncommon
Position.

#mdn: Amendments

#wWo: News and Chronology

#rbW: The letter and appendices distributed to MEP mail boxes on 2005-03-28

#rft: More information

#oeW: Draft set of amendments (based on selected articles from the first
reading)

#dMc: Amendments in MS Word doc format

#lupd: last update: 2005/03/31, 10:00 am

#elr: Amendments in Portable Document Format (PDF)

#Wpt: The general outline of steps we deem appropriate to start fixing the
Council text

#sme: phm proposes %(rs:5-step Questionnaire/Checklist for Software Patent
Legislators)

#tte: FFII distributes a letter with appendices on paper to all MEPs

#woo: Letter with Position about the EP's second reading

#ete: Appendices to the letter

#rCa: Tabular Comparison of Council vs Parliament text

#eA0: Overview of Proposed Amendments of 2003-09-24

#htW: %(ua:Urgent Appeal to the Council) with informative appendices such as
a short overview of %(ct:what's wrong with the Council text) and a
%(cp:summary of studies)

#Wo0: FFII Counter-Proposal of 2002-2003

#nWo: Minimal Proposal

#WUl: Polish Government's Unilateral Declaration

#QWo: 23 Questions to the Council

#toc: Short Introduction to the Software Patent Directive Project

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/plen05.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: plen05 ;
# txtlang: xx ;
# End: ;

