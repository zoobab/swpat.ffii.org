<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lutterbeck & Gehring 2003/09 zur Swpat-Problematik

#descr: Den neuen Text von Lutterbeck und Gehring, 3 Jahre nach ihrem von der
Bundesregierung bestellten %(q:Kurzgutachten) geschrieben, durchzieht
erneut eine nicht unbedingt realistische Verehrung des Reellen (oder
bessergesagt eines f|r autoritdtsbeladenen Ausschnittes desselben). 
Der naturalistischen Fehlschluss von der Praxis der Patentjustiz auf
die zu etablierenden Regeln steht diesmal in merkw|rdigem Kontrast
nicht nur zu zahlreichen von Lutterbeck und Gehring offenbar nicht
gesichteten Gerichtsurteilen, sondern auch zu Beschl|ssen und
inzwischen verabschiedeten Gesetzesentw|rfen des Europdischen
Parlaments.   Neben diesem grundlegenden Fehler zeit der Text einige
recht originelle Ansdtze.

#riz: Lutterbeck & Gehring 2003-09 zu Swpat

#Drn: PDF-Original

#dst: Sie bekräftigen die Hornssche Diktion und den naturalistischen
Fehlschluss aus ihrem %(bm:Gutachten vom Jahr 2000):

#sWr: Unbeschadet des Wortlauts des deutschen und amerikanischen
Patentrechts haben sich Gerichte und Patentämter in Europa und den USA
nicht gescheut, %(q:Software-Patente) rechtlich anzuerkennen. Das Ob
von %(q:Software-Patenten) ist nicht mehr ernsthaft bestritten. Die
Frage %(q:Ist Software patentierbar?) ist praktisch längst beantwortet
%(q:a matter for the history books).

#jtr: Darüberhinaus bestand in der europäischen Literatur weitgehende
Einigkeit insofern,als daß die Patentierungspraxis in Europa
restriktiver ausfallen müsse als die des vielkritisierten
amerikanischen Patentamtes. Insbesondere bestand so etwas wie ein
Konsens, daß reine Geschäftsmethoden nicht patentierbar sein sollten.
Auch diese Sicht der Literatur scheint gefallen zu sein. Das
Europäische Patentamt in München hat kürzlich der Firma Amazon ein
Patent erteilt, das praktisch alle computer-basierten Verfahren der
bestellten Lieferung von Geschenken an Dritte umfaßt (und mehr).

#cku: Das wissenschaftliche Literaturmeinungen durch die Amtspraxis
korrigiert werden, scheint wohl nur in der Juristerei möglich zu sein.

#nme: Lutterbeck und Gehring scheinen die Vorgänge im Europäischen
Parlament, die ja nicht erst mit dem Plenar-Votum begannen, übersehen
zu haben.

#ggf: Lutterbeck und Gehring üben auch Selbstkritik an dem zuvor der
Bundesregierung als Lösung angebotenen und von niemandem
aufgegriffenen %(q:Quelltextprivileg):

#agW: Aber auch bei diesem Vorschlag handelt es sich um eine
Symptombehandlung, die allenfalls einige, wenige Schwächen des
Patentsystems lindern hilft. Gesund wird der Patient dadurch noch
lange nicht.

#shn: Der Quelltextprivileg-Vorschlag widerspricht auch dem sonstigen
Fatalismus von Lutterbeck und Gehring.  Es setzt die Bereitschaft
voraus, Datenverarbeitung als etwas besonderes zu sehen und damit
gegen breite Interpretationen von Art 27ff, insbesondere Art 30ff
TRIPs einzutreten.  Ist man hierzu bereit, so kann man jedoch gleich
auch die Technizität der Datenverarbeitung verneinen.

#eun: Die Wahl, die eine Realität als übermächtig und unumkehrbar
einzustufen und an der anderen zu rütteln, erscheint unbegründet und
willkürlich.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: luge0309 ;
# txtlang: xx ;
# End: ;

