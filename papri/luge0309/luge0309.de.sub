\begin{subdocument}{luge0309}{Lutterbeck \& Gehring 2003/09 zur Swpat-Problematik}{http://swpat.ffii.org/papiere/luge0309/index.de.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\deutsche Version 2005/03/28 von Andreas RUDERT\footnote{\url{http://www.rudert-home.de}}}{Den neuen Text von Lutterbeck und Gehring, 3 Jahre nach ihrem von der Bundesregierung bestellten ``Kurzgutachten'' geschrieben, durchzieht erneut eine nicht unbedingt realistische Verehrung des Reellen (oder bessergesagt eines f|r autoritdtsbeladenen Ausschnittes desselben).  Der naturalistischen Fehlschluss von der Praxis der Patentjustiz auf die zu etablierenden Regeln steht diesmal in merkw|rdigem Kontrast nicht nur zu zahlreichen von Lutterbeck und Gehring offenbar nicht gesichteten Gerichtsurteilen, sondern auch zu Beschl|ssen und inzwischen verabschiedeten Gesetzesentw|rfen des Europdischen Parlaments.   Neben diesem grundlegenden Fehler zeit der Text einige recht originelle Ansdtze.}
Sie bekr\"{a}ftigen die Hornssche Diktion und den naturalistischen Fehlschluss aus ihrem Gutachten vom Jahr 2000\footnote{\url{http://swpat.ffii.org/papiere/bmwi-luhoge00/index.de.html}}:

\begin{quote}
{\it Unbeschadet des Wortlauts des deutschen und amerikanischen Patentrechts haben sich Gerichte und Patent\"{a}mter in Europa und den USA nicht gescheut, ``Software-Patente'' rechtlich anzuerkennen. Das Ob von ``Software-Patenten'' ist nicht mehr ernsthaft bestritten. Die Frage ``Ist Software patentierbar?'' ist praktisch l\"{a}ngst beantwortet ``a matter for the history books''.}

{\it [\dots]}

{\it Dar\"{u}berhinaus bestand in der europ\"{a}ischen Literatur weitgehende Einigkeit insofern,als da{\ss} die Patentierungspraxis in Europa restriktiver ausfallen m\"{u}sse als die des vielkritisierten amerikanischen Patentamtes. Insbesondere bestand so etwas wie ein Konsens, da{\ss} reine Gesch\"{a}ftsmethoden nicht patentierbar sein sollten. Auch diese Sicht der Literatur scheint gefallen zu sein. Das Europ\"{a}ische Patentamt in M\"{u}nchen hat k\"{u}rzlich der Firma Amazon ein Patent erteilt, das praktisch alle computer-basierten Verfahren der bestellten Lieferung von Geschenken an Dritte umfa{\ss}t (und mehr).}
\end{quote}

Das wissenschaftliche Literaturmeinungen durch die Amtspraxis korrigiert werden, scheint wohl nur in der Juristerei m\"{o}glich zu sein.

Lutterbeck und Gehring scheinen die Vorg\"{a}nge im Europ\"{a}ischen Parlament, die ja nicht erst mit dem Plenar-Votum begannen, \"{u}bersehen zu haben.

Lutterbeck und Gehring \"{u}ben auch Selbstkritik an dem zuvor der Bundesregierung als L\"{o}sung angebotenen und von niemandem aufgegriffenen ``Quelltextprivileg'':

\begin{quote}
{\it Aber auch bei diesem Vorschlag handelt es sich um eine Symptombehandlung, die allenfalls einige, wenige Schw\"{a}chen des Patentsystems lindern hilft. Gesund wird der Patient dadurch noch lange nicht.}
\end{quote}

Der Quelltextprivileg-Vorschlag widerspricht auch dem sonstigen Fatalismus von Lutterbeck und Gehring.  Es setzt die Bereitschaft voraus, Datenverarbeitung als etwas besonderes zu sehen und damit gegen breite Interpretationen von Art 27ff, insbesondere Art 30ff TRIPs einzutreten.  Ist man hierzu bereit, so kann man jedoch gleich auch die Technizit\"{a}t der Datenverarbeitung verneinen.

Die Wahl, die eine Realit\"{a}t als \"{u}berm\"{a}chtig und unumkehrbar einzustufen und an der anderen zu r\"{u}tteln, erscheint unbegr\"{u}ndet und willk\"{u}rlich.

\begin{itemize}
\item
{\bf {\bf Lutterbeck \& Gehring 2003-09 zu Swpat\footnote{\url{http://ig.cs.tu-berlin.de/ap/rg/2003-x/GehringLutterbeck-SWPat-092003.pdf}}}}

\begin{quote}
PDF-Original
\end{quote}
\filbreak

\item
{\bf {\bf Sicherheit in der Informationstechnologie und Patentschutz f\"{u}r Software-Produkte - Kurzgutachten von Lutterbeck et al im Auftrag des BMWi\footnote{\url{http://swpat.ffii.org/papiere/bmwi-luhoge00/index.de.html}}}}

\begin{quote}
Prof. Dr.iur. Bernd Lutterbeck von der TU-Berlin, sein Assistent Robert Gehring und der M\"{u}nchener Patentanwalt Axel Horns nahmen im Sp\"{a}tsommer 2000 unter dem Namen ``Forschergruppe Internet Governance'' einen Auftrag des BMWi f\"{u}r dieses 166 Seiten lange ``Kurzgutachten'' an, das im Dezember 2000 ver\"{o}ffentlicht wurde.   Darin vertreten sie eine bereits h\"{a}ufig zuvor ver\"{o}ffentlichte Rechtsauffassung des M\"{u}nchener Patentanwalts Axel H. Horns zur Frage der Patentierbarkeit von Computerprogrammen in Europa.  PA Horns kann dem Art 52 EP\"{U} keine klare Bedeutung abgewinnen.  Er erkl\"{a}rt den traditionellen Technikbegriff des Patentwesens f\"{u}r nicht mehr zeitgem\"{a}{\ss} und h\"{a}lt die grenzenlose Patentierbarkeit, wie das EPA sie in der Theorie anstrebt und in der Praxis bereits weitgehend verwirklicht hat, f\"{u}r unvermeidbar.  Gleichzeitig wird aber vor diversen negativen Folgen des Patentwesens f\"{u}r Wirtschaft und Gesellschaft gewarnt und es werden diverse Ma{\ss}nahmen vorgeschlagen, mit denen die Sperrwirkung der Patente im Bereich der Informationsverarbeitung abgeschw\"{a}cht werden k\"{o}nnte, so dass zumindest in Deutschland ein Reservat \"{u}brig bleiben k\"{o}nnte, in dem Software-Quelltexte ungehindert ver\"{o}ffentlicht aber nicht gewerblich genutzt werden d\"{u}rfen.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
% mode: latex ;
% End: ;

