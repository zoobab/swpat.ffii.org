<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Jan Busche: Softwarebezogene Erfindungen in der Entscheidungspraxis
des Bundespatentgerichts und des Bundesgerichtshofs

#descr: erklärt, laut EPÜ Art 52 sei die Patentierbarkeit von softwarbezogenen
Ideen nur als Ausnahme möglich, aber es habe ein wirtschaftliches
Interesse daran gegeben, diese Ausnahme zur Regel zu machen, nämlich
den %(q:Wunsch der Softwareentwickler nach regulärem Zugang zum
Patentrecht) und die Bedürftnisse der Unternehmensfinanzierung. 
Listet Urteile seit 1976 auf, erteilt denen gute Noten, die zur
schrittweisen Umkehrung des Ausnahme-Regel-Verhältnisses beitrugen,
schmäht die anderen als rückständig.  Frohlockt zum Schluss, dass die
Algorithmen nunmehr entgegen Kolle 1977 nicht mehr ins
immaterialgüterrechtliche Niemandsland verbannt würden und behauptet
fälschlicherweise, Kolle habe dies %(q:befürchtet).

#Zcr: Zur Verdeutlichung sei der Rechtsrahmen noch einmal kurz skizziert. 
Sowohl das europäische als auch das deutsche Patentschutz stellen die
Regel auf, dass Patentschutz insoweit ausgeschlossen ist, als er für
Programme für Datenverarbeitungsanlagen %(q:als solche) begehrt wird. 
Dieser Regelungsansatz harmoniert -- freilich nur vom gedanklichen
Ansatz her -- mit dem in urheberrechtlichen Vorschriften (vgl Art 4
WCT; Art 10 TRIPs; Art 1 EG-Software-RL, §§69 a ff UrhG) zum Ausdruck
kommenden Anliegen, den Schutz von Computersoftware im wesentlichen
durch das Urheberrecht zu gewährleisten.  Damit scheint
softwarebezogener Patentschutz nur in Ausnahmefällen erreichbar zu
sein. ... Das beschriebene Regel-Ausnahmeverhältnis hat daher, wie
nicht anders zu erwarten, die Phantasie der Juristen beflügelt: Suchen
wir doch gerne nach den Ausnahmefällen, nach den vermeintlich
außergewöhnlichen Fallgestaltungen, die es uns erlauben, die
Rechtsordnung auf die Probe zu stellen, was Regel und was Ausnahme
ist.  Dies gilt insbesondere dann, wenn es wirtschaftlich vernünftige
Gründe gibt, die Ausnahme und nicht die Regel anzustreben, die uns der
Gesetzgeber ans Herz zu legen scheint.

#Det: Derartige wirtschaftliche Gründe sind im Falle softwarebezogener
Leistungen in der Tat erkennbar: In dem beschriebenen
Spannungsverhältnis zwischen Urheberrecht und Patentrecht geht es um
nicht mehr und nicht weniger als um einen möglichst effektiven
wirtschaftlichen Schutz von Software vor einer Ausbeutung durch
Dritte, der nach allgemeiner Erkenntnis durch das Urheberrecht nicht
gerade vermittelt wird. ... Dies erklärt den Wunsch von
Softwareentwicklern nach einem %(q:regulären) Zugang zum Patentschutz.
 Davon hängt häufig nicht zuletzt auch die Unternehmensfinanzierung
ab.  Das Patent eignet sich im Gegensatz zum Urheberrecht wegen seiner
freien Übertragbarkeit (§15 PatG) als Kreditgrundlage; schließlich
kanndie Innehabung eines Patents bei der Einwerbung von Fremdkapital
über die Börse oder durch Venture-Kapital-Gesellschaften eine durchaus
positivee Rolle spielen. ...

#Zec: Zum Schluss des Artikels schreibt Busche:

#Sus: Sieht man einmal von dem Sonderproblem der Geschäftsmethoden ab,
lassen sich mit dem in der Jüngeren BGH-Rechtsprechung sichtbaren
%(q:neuen) Technikverständnis, das in seinen Grundlinien mit der
Entscheidungspraxis des Europäischen Patentamtes korrespondiert,
durchaus praxisgerechte Lösungen für die Patentierung
softwarebezogener Leistungen finden.  Jedenfalls ist mittlerweile
nicht mehr zu befürchten, dass die Informatik als janusköpfige
Disziplin zwischen Geist und Materie %(q:ins immaterialgüterrechtliche
Niemandsland verbannt) wird, wie Kolle im Jahre 1977, seinerzeit nicht
zu Unrecht, %(gk:befürchtete).

#Mle: Man vergleiche hiermit Kolles Orginal-Artikel und bedenke auch, dass
Busche diesen Artikel zu einem Zeitpunkt veröffentlichte, wo der
Widerstand zehntausender von Softwareentwicklern gegen die gelobte
Praxis bereits politische Veränderungen ausgelöst hatte.  Busche war
auf diese Tatsachen auch spätestens im Herbst 2000 von Journalisten
angesprochen worden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: mdp-busche01 ;
# txtlang: de ;
# multlin: t ;
# End: ;

