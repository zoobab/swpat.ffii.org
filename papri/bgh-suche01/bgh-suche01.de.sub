\begin{subdocument}{bgh-suche01}{BGH 2001: Patentanspr\"{u}che auf ``Computerprogrammprodukt'' etc zul\"{a}ssig}{http://swpat.ffii.org/papiere/bgh-suche01/index.de.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\deutsche Version 2005/03/25 von Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Beschluss des 10. Senates des Bundesgerichtshofes (BGH/10) in der Rechtsbeschwerdesache betr die Patentanmeldung P 4323241.8-53 (IBM: Suche fehlerhafter Zeichenketten in einem Text) vom Oktober 2000.  Der 17. Senat des Bundespatentgerichtes (BPatG/17) hatte im August 2000 erkldrt, dass ein Computerprogramm nicht beansprucht werden kann, und dass ein ``Computerprogramm als solches'' nichts anderes als ein Computerprogramm in beliebiger Entwurfsstufe ist, und dabei diverse Lehrsdtze des BGH und EPA analysiert und zur|ckgewiesen.  BGH/10 hebt den BPatG/17-Beschluss auf und deutet an, dass Programme mvglicherweise beansprucht werden kvnnten, wenn der Gegenstand des Hauptanspruchs sich als eine Erfindung gemd\_ g1 PatG erweist, deutet aber an, dass dies im vorliegenden Falle nicht gegeben sein d|rfte und verweist den Fall zum BPatG zur erneuten Pr|fung zur|ck.}
\begin{description}
\item[Titel:]\ BGH 2001: Patentanspr\"{u}che auf ``Computerprogrammprodukt'' etc zul\"{a}ssig
\item[Quelle:]\ BGH/10 X ZB 16/00
\item[English Version:]\ \begin{itemize}
\item
{\bf {\bf Betten on BGH and program claims\footnote{\url{http://lists.ffii.org/archive/mails/news/2002/Nov/0001.html}}}}

\begin{quote}
Comments by Hartmut Pilch on comments by PA J\texmath{\backslash}``{u}rgen Betten about an ambivalent decision of the Patent Senate of the German Federal Court of Justice of Nov 2001 on whether claims to a program with the ``technical effect'' of correcting errors in text strings are permissible.  In this exchange of opinions some of the speakers of the EuroParl Hearing of 2002-11-07 are mentioned and some backgrounds of this hearing and of the European Councils Patent Workgroup's decisions are explained.
\end{quote}
\filbreak
\end{itemize}
\end{description}

\begin{description}
\item[Aktenzeichen:]\ BGH, Beschl. v. 17. Oktober 2001 - X ZB 16/00 - Bundespatentgericht
\item[Gesetze:]\ PatG 1981 \S{}1 Abs. 2 Nr. 3, Abs. 3
\item[Leits\"{a}tze:]\ \begin{enumerate}
\item
Das Patentierungsverbot f\"{u}r Computerprogramme als solche verbietet, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten, wenn sie nur - irgendwie - \"{u}ber die Bereitstellung der Mittel hinausgeht, welche die Nutzung als Programm f\"{u}r Datenverarbeitungsanlagen erlauben.  Die pr\"{a}genden Anweisungen der beanspruchten Lehre m\"{u}ssen vielmehr insoweit der L\"{o}sung eines konkreten technischen Problems dienen.

\item
Eine vom Patentierungsverbot erfasste Lehre (Computerprogramm als solches) wird nicht schon dadurch patentierbar, dass sie in einer auf einem herk\"{o}mmlichen Datentr\"{a}ger gespeicherten Form zum Patentschutz angemeldet wird.
\end{enumerate}
\end{description}

Der 10. Zivilsenat des Bundesgerichtshofes hat durch den Vorsitzenden Richter Rogge und die Richter Prof. Dr. Jestaedt, Dr. Melullis, Scharen und Dr. Meier-Beck am 17. Oktober 2001 beschlossen:

\begin{quote}
Auf die Rechtsbeschwerde der Anmelderin wird der Beschluss des 17. Senats (Technischen Beschwerdesenats) des Bundespatentgerichts vom 28. Juli 2000\footnote{\url{http://swpat.ffii.org/papiere/bpatg17-suche00/index.de.html}} aufgehoben.

Die Sache wird zur anderweiten Verhandlung auch \"{u}ber die Kosten der Beschwerde an das Bundespatentbericht zur\"{u}ckverwiesen.

Der Wert des Gegenstandes der Rechtsbeschwerde wird auf 50000 DM festgesetzt.
\end{quote}

Gr\"{u}nde:

...

Patentanspruch 22: ``Digitales Speichermedium, insbesondere Diskette, mit elektronisch auslesbaren Steuersignalen, die so mit einem programmierbaren Computersystem zusammenwirken k\"{o}nnen, dass ein Verfahren nach einem der Anspr\"{u}che 1 bis 17 ausgef\"{u}hrt wird.''

...

Die ... Rechtsbeschwerde f\"{u}hrt zur Zur\"{u}ckweisung der Sache an das Bundespatentgericht, weil sich anhand der von diesem getroffenen tats\"{a}chlichen Feststellungen nicht abschlie{\ss}end beurteilen l\"{a}sst, ob dem Patentanspruch 22 die Patentierbarkeit fehlt, wie das Bundespatentgericht angenommen hat, oder ob dieser Anspruch einen patentierbaren Gegenstand hat.

I. Das Bundespatentgericht hat der Anmeldung im Wege der Auslegung entnommen, der noch streitige Patentanspruch 22 solle sich auf ein \"{u}bliches Speichermedium beziehen, das sich von anderen maschinenlesbaren Speichermedien dadurch unterscheide, dass es eine Aufzeichnung trage, die im Zusammenwirken mit einem geeigneten Computersystem eine Ausf\"{u}hrung des Verfahrens nach einem der in Bezug genommenen Patentanspr\"{u}che bewirken k\"{o}nne.

Das begegnet keinen rechtlichen Bedenken und wird im Ergebnis auch von der Rechtsbeschwerde nicht angegriffen.  Sie stellt darauf ab, es handele sich um einen Datentr/aeger, der lediglich das Steuerelement mit einer technischen Schnittstelle zum Computersystem dergestalt darstelle, dass elektronisch auslesbare Steuersignale vorhanden seien, die so mit dem Computersystem zusammenwirkten, dass die Schritte des erfindungsgem\"{a}{\ss}en Verfahrens ausgef\"{u}hrt werden.

II. Das Bundespatentgericht hat den f\"{u}r Anspruch 22 begehrten Patentschutz schon deshalb versagt, weil dieser Patentanspruch keine Lehre angebe, die wenigstens die wesentlichen L\"{o}sungsmittel umfasse.  Eine Erfindung im Sinne einer Lehre zum technischen Handeln bestehe in der L\"{o}sung eines technischen Problems.  Nach Satz 2 Abs. 3 der Beschreibung liege dem Patentbegehren die Aufgabe zugrunde, ein verbessertes Verfahren und Computersystem zur Suche und/oder Korrektur einer fehlerhaften Zeichenkette in einem Text zu schaffen.  Das k\"{o}nne jedoch allein durch ein digitales Speichermedium nicht gelingen, auf dem, wie Patentanspruch 22 lediglich angebe, eine Aufzeichnung angebracht sei.  Eine Ausf\"{u}hrung des Verfahrens gelinge nur mit einem Computersystem, das in der Lage sei, die einzelnen Teile der Aufzeichnung quasi vollst\"{a}ndig zu interpretieren und dadurch eine Durchf\"{u}hrung der gew\"{u}nschten Verfahrensschritte zu bewirken.

Das beanstandet die Rechtsbeschwerde zu Recht.

Die Feststellung des Bundespatentgerichts, Patentanspruch 22 lehre zur L\"{o}sung der in der Anmeldung genannten Aufgabe lediglich das Aufbringen einer Aufzeichnung von durch ein Computersystem erst noch zu interpretierenden Daten, die selbst nicht die zur Durchf\"{u}hrung von Verfahrensschritten repr\"{a}sentativen Steuersignale darstellten, greift zu kurz.  Sie l\"{a}sst unber\"{u}cksichtigt, dass das beanspruchte Speichermedium \"{u}ber die auf ihm aufgebrachten auslesbaren Daten nach dem Wortlaut des Patentanspruchs derart mit einem programmierbaren Computersystem zusammenwirken k\"{o}nnen muss, dass das insbesondere in dem Anspruch 1 beanspruchte Verfahren ausgef\"{u}hrt wird.  Die Anweisung nach Patentanspruch 22 dient danach zur Realisierung eines bestimmten Computerprogramms.  Das vorgeschlagene digitale Speichermedium selbst ist ein gegenst\"{a}ndliches Mittel zur Ausf\"{u}hrung des in dem nachgesuchten Patent ferner vorgeschlagenen Verfahrens; sein bestimmungsgem\"{a}{\ss}er Einsatz f\"{u}hrt - die Ausf\"{u}hrbarkeit und technische Brauchbarkeit der angemeldeten Anweisung vorausgesetzt - zu dem gew\"{u}nschten Ergebnis.  Das reicht f\"{u}r eine im Rahmen der die Anmeldung pr\"{a}genden Problemstellung liegende L\"{o}sung aus.

III. Das Bundespatentgericht hat das Speichermedium mit einer Aufzeichnung gem\"{a}{\ss} dem Anspruch 22 als ein ``Programm f\"{u}r eine Datenverarbeitungsanlage als solches'' angesehen und gemeint, dass dieser Anspruch deshalb auch nach \S{}1 Abs. 2 Nr. 3 und Abs. 3 PatG vom Patentschutz ausgenommen sei.

1. Zu dieser Bewertung ist es gelangt, weil der Computerfachmann den mehrdeutigen Begriff ``Programm'' bei enger Sicht lediglich f\"{u}r den Programmcode und dessen Aufzeichnungen (gleichg\"{u}ltig welche Entwurfsstufe) verwende.  Da \S{}1 Abs. 2 Nr. 3 in Verbindung mit Abs. 3 PatG nach einer engen Auslegung verlange, umfasse der Begriff ``Programm f\"{u}r eine Datenverarbeitungsanlage als solches'' eine Programmcodedarstellung oder -aufzeichnung auf einem Klarschriftdatentr\"{a}ger wie Papier oder einem maschinenlesbaren Speichermedium.

Auch diese Auffassung bek\"{a}mpft die Rechtsbeschwerde zu Recht.

(a) Allerdings wird sie in der Literatur verschiedentlich bef\"{u}rwortet (Tauchert, GRUR 1997, 149, 154; ...).  Es gibt aber auch ma{\ss}gebliche Gegenstimmen.  Vor allem ist auf die Spruchpraxis des Europ\"{a}ischen Patentamtes zu dem nahezu wortgleichen Art. 52 Abs. 2 Buchst. c, Abs. 3 EP\"{U} zu verweisen, wonach ein -- Datenverarbeitung mittels eines geeigneten Computers betreffender -- Gegenstand nicht als ``Programm als solches'' im Sinne dieser Regelung zu verstehen ist, wenn er -- hinreichend qualifizierten -- technischen Charakter hat (EPO T 1173/97: IBM Computer Program Product\footnote{\url{http://swpat.ffii.org/papiere/epo-t971173/index.de.html}}; ...).  Zu \"{a}hnlichen Ergebnissen f\"{u}hrt eine in der Literatur vertretene Auffassung, wonach unter ``Programm als solches'' lediglich der zugrundeliegende, von einer technischen Funktion noch freie Programminhalt zu verstehen ist (Mellulis (BGH) 1998: Zur Patentierbarkeit von Programmen fuer DV-Anlagen\footnote{\url{http://swpat.ffii.org/papiere/grur-mellu98/index.de.html}}; Anders, GRUR 1990, 498, 499).

(b) Der Auffassung des Bundespatentgerichts kann aus Rechtsgr\"{u}nden nicht beigetreten werden.

Bei der Bestimmung, was als Programm f\"{u}r Datenverarbeitungsanlagen vom Patentschutz ausgenommen ist, weil es ein Programm als solches ist, kann nicht allein auf das Verst\"{a}ndnis von Computerfachleuten zur\"{u}ckgegriffen werden.  Die Bestimmung hat vielmehr -- wie auch sonst bei der Gesetzesauslegung -- ausgehend vom Wortlaut sachbezogen nach Sinn und Zweck der gesetzlichen Regelung zu fragen.

(aa) Die gesetzliche Regelung ergibt schon nach ihrem Wortlaut zun\"{a}chst, dass weder Programme f\"{u}r Datenverarbeitungsanlagen schlechthin vom Patentschutz ausgeschlossen sind, noch dass bei Vorliegen der weiteren Voraussetzungen des Gesetzes f\"{u}r jedes Computerprogramm Patentschutz erlangt werden kann.  Letzteres f\"{u}hrt zu der Erkenntnis, dass eine beanspruchte Lehre nicht schon deshalb als patentierbar angesehen werden kann, weil sie bestimmungsgem\"{a}{\ss} den Einsatz eines Computers erfordert.  Es muss vielmehr bei einer Lehre, die bei ihrer Befolgung dazu beitr\"{a}gt, dass eine geeignete Datenverarbeitungsanlage bestimmte Anweisungen abarbeitet, eine hier\"{u}ber hinausgehende Eigenheit bestehen.  Da Datenverarbeitung geeignet erscheint, in nahezu allen Bereichen des menschlichen Lebens n\"{u}tzlich zu sein, kann im Hinblick auf diese Notwendigkeit au{\ss}erdem nicht unber\"{u}cksichtigt bleiben, dass das Patentrecht geschaffen wurde, um durch Gew\"{a}hrung eines zeitlich beschr\"{a}nkten Ausschlie{\ss}lichkeitsschutzes neue, nicht nahegelegte und gewerblich anwendbare Probleml\"{o}sungen auf dem Gebiet der Technik zu f\"{o}rdern.  Das wiederum verbietet, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten, wenn sie nur -- irgendwie -- \"{u}ber die Bereitstellung der Mittel hinausgeht, welche die Nutzung als Programm f\"{u}r Datenverarbeitungsanlagen erlauben.  Die pr\"{a}genden Anweisungen der beanspruchten Lehre m\"{u}ssen vielmehr insoweit der L\"{o}sung eines konkreten technischen Problems dienen.  Unter diesen Voraussetzungen ist die beanspruchte Lehre dem Patentschutz auch dann zug\"{a}nglich, wenn sie als Computerprogramm oder in einer sonstigen Erscheinungsform gesch\"{u}tzt werden soll, die eine Datenverarbeitungsanlage nutzt.

(bb) Diese Abgrenzung der f\"{u}r Datenverarbeitungsanlagen bestimmten Programme, f\"{u}r die als solche Schutz begehrt wird, von computerbezogenen Gegenst\"{a}nden, die \S{}1 Abs 2 Nr 3 PatG nicht unterfallen, f\"{u}hrt dazu, dass Anspr\"{u}che, die zur L\"{o}sung eines Problems, das auf den herk\"{o}mmlichen Gebieten der Technik, also der Ingenieurwissenschaften, der Physik, der Chemie oder der Biologie besteht, die Abarbeitung bestimmter Verfahrensschritte durch einen Computer vorschlagen, grunds\"{a}tzlich patentierbar sind.  Ansonsten bedarf es hingegen einer Pr\"{u}fung, ob die auf Datenverarbeitung mittels eines geeigneten Computers gerichtete Lehre sich gerade durch eine Eigenheit auszeichnet, die unter Ber\"{u}cksichtigung der Zielsetzung patentrechtlichen Schutzes eine Patentierbarkeit rechtfertigt.

Hiervon ist der Senat bereits bisher im Rahmen seiner neueren Rechtsprechung zu computerbezogenen Patentanmeldungen ausgegangen.  So hat er -- wenn auch im Hinblick auf die f\"{u}r eine Erfindung i.S.d. \S{}1 Abs. 1 PatG erforderliche Technizit\"{a}t -- eine Gesamtbetrachtung dar\"{u}ber gefordert, was nach der beanspruchten Lehre im Vordergrund steht (BGH 1999 Logikverifikation\footnote{\url{http://swpat.ffii.org/papiere/bgh-logik99/index.de.html}}).  Das erlaubt in dem hier interessierenden Zusammenhang ebenfalls eine sachgerechte Wertung und Abgrenzung. ...

(cc) Das vom Senat f\"{u}r ma{\ss}geblich gehaltene Verst\"{a}ndnis von \S{}1 Abs 2 Nr 3 PatG wird durch die Gesetzessystematik best\"{a}tigt.  Die dargelegte Tragweite des Patentierungsverbots f\"{u}r Computerprogramme entspricht derjenigen von anderen Tatbest\"{a}nden des \S{}1 Abs 2 PatG.  Sowohl die dort in Nr. 1 genannten wissenschaftlichen Theorien und mathematischen Methoden als auch die in Nr. 3 genannten Pl\"{a}ne, Regeln und Verfahren f\"{u}r gedankliche T\"{a}tigkeiten sind nur insoweit vom Patentschutz ausgeschlossen, als sie losgel\"{o}st von einer konkreten Umsetzung beansprucht werden.  Soweit sie hingegen zur L\"{o}sung eines konkreten technischen Problems Verwendung finden, sind sie -- in diesem Kontext -- grunds\"{a}tzlich patentf\"{a}hig (BGH 1976 Disposition\footnote{\url{http://swpat.ffii.org/papiere/bgh-dispo76/index.de.html}}; EPA T27/97).

(dd) \S{}1 Abs 2 Nr 3, Abs 3 PatG ist bewusst an die europ\"{a}ische Regelung in Art 52 Abs 32 Buchstabe c, Abs. 3 EP\"{U} angeglichen worden, um sicherzustellen, dass der Kreis der patentf\"{a}higen Erfindungen nach nationalem Recht derselbe ist wie nach dem Europ\"{a}ischen Patent\"{u}bereinkommen (BT-Drucks. 7/3712, S. 27).  Bei der Entstehung des Europ\"{a}ischen Patent\"{u}bereinkommens herrschte zwar im Hinblick auf die Patentierung von computerbezogenen Lehren keine klare Vorstellung dar\"{u}ber, welche Definition gew\"{a}hlt werden soll.  W\"{a}hrend der diplomatischen Konferenz zum Abschluss des \"{U}bereinkommens wurde ausdr\"{u}cklich darauf hingewiesen, dass vergeblich versucht worden sei, die Begrifflichkeiten auszuf\"{u}llen; die Auslegung m\"{u}sse der Rechtspraxis \"{u}berlassen bleiben (Dokument M/PR/I S.28 Tz. 18, Berichte der M\"{u}nchener Diplomatischen Konferenz \"{u}ber die Einf\"{u}hrung eines Europ\"{a}ischen Patenterteilungsverfahrens, herausgegeben von der Regierung der Bundesrepublik Deutschland, in: Materialien zum Europ\"{a}ischen Patent\"{u}bereinkommen: Anl Bd. 3).

Die in das Europ\"{a}ische Patent\"{u}bereinkommen und das PatG \"{u}bernommene Wortwahl tr\"{a}gt jedoch dem Anliegen Rechnung, die Entwicklung auf dem damals immer noch relativ neuen Gebiet der Computertechnik nicht durch eine uferlose Ausdehnung des Patentschutzes zu behindern.  Dies legt es nahe, Lehren aus Gebieten, die nach traditionellem Verst\"{a}ndnis nicht zur Technik geh\"{o}ren, nicht allein deshalb dem Patentschutz zug\"{a}nglich zu erachten, weil sie mit hilfe eines Computers angewendet werden sollen.  Andererseits w\"{u}rde es \"{u}ber das genannte Ziel hinausgehen, einer Lehre, deren Eigenart durch technische Vorg\"{a}nge oder \"{U}berlegungen gepr\"{a}gt ist, den Patentschutz zu versagen, weil sie auf einem Computer zur Ausf\"{u}hrung kommen soll und/oder von einem Teil der Computerfachleute in einem engeren Sinne als Programm f\"{u}r Datenverarbeitungsanlagen angesehen wird.

2 Ob Anspruch 22 hiernach von dem Patentierungsausschluss nach \S{}1 Abs 2 Nr 3 PatG erfasst wird, kann der Senat nicht abschlie{\ss}end beurteilen.

(a) Die Anmeldung betrifft die Suche und/oder Korrektur einer fehlerhaften Zeichenkette in einem Text.  Das liegt nicht auf technischem Gebiet, auch wenn der zu pr\"{u}fende Text mit einem computergest\"{u}tzten Textverarbeitungssystem erstellt worden ist.  Im vorliegenden Fall ist deshalb -- wie ausgef\"{u}hrt -- eine Bewertung n\"{o}tig, ob Anspruch 22 Anweisungen enth\"{a}lt, die den erforderlichen Bezug zur Technik herstellen.  Das erfordert eine tatrichterliche Analyse sowie die Feststellung der ma{\ss}geblichen Umst\"{a}nde, die das Bundespatentgericht -- in Konsequenz seines rechtlichen Ausgangspunktes -- nicht getroffen hat.  Das wird daher nachzuholen sein.

(b) Die neuerliche Pr\"{u}fung ist nicht etwa deshalb entbehrlich, weil mit Anspruch 22 ein Verfahren nicht unmittelbar beansprucht wird.  Die in Anspruch 22 enthaltene Lehre kann nicht schon deshalb patentiert werden, weil dieser Anspruch insbesondere auf eine Diskette und damit auf einen k\"{o}rperlichen Gegenstand (Vorrichtung) gerichtet ist.

Nach der Beschreibung in der Patentanmeldung wird bei bekannten Textverarbeitungssystemen auf ein sogenantes Lexikon zur\"{u}ckgegriffen.  Dieses enth\"{a}lt eine Liste von bekannten W\"{o}rtern.  Zur Fehlersuche werden die W\"{o}rter eines eingegebenen Textes mit den Eintr\"{a}gen des Lexikons verglichen.  Die Verwendung des Lexikons erfordert einen relativ gro{\ss}en Speicherplatz.  Ferner kann es seinerseits Fehleintr\"{a}ge enthalten.  Es muss dar\"{u}ber hinaus st\"{a}ndig aktualisiert werden, was zu weiteren Fehleintr\"{a}gen f\"{u}hren kann.

Zur \"{U}berwindung der hiernach bestehenden nachteile kommt der durch Anspruch 22 gemachte L\"{o}sungsvorschlag nicht ohne Ausf\"{u}hrung des insbesondere nach dem erteilten Patentanspruch 1 beanspruchten Verfahrens aus.  \"{A}hnlich einem Blatt Papier, das anderweitig ben\"{o}tigte Informationen enth\"{a}lt, kommt dem Speichermedium, das durch Anspruch 22 gesch\"{u}tzt werden soll, nur die Funktion eines Informationstr\"{a}gers zu, der eingesetzt werden kann, wenn die Ausf\"{u}hrung des Verfahrens durch einen Computer gew\"{u}nscht wird.  Auch die Rechtsbeschwerde erkennt an, dass der Datentr\"{a}ger als solcher im vorliegenden Fall nicht zur Begr\"{u}ndung der Patentf\"{a}higkeit beitr\"{a}gt.  Wie die Anmelderin in der Rechtsbeschwerde noch einmal geltend gemacht hat, ist Anspruch 22 auf eine Lehre f\"{u}r einen solchen Gegenstand nur deshalb gerichtet, um ohne besonderen Nachweis den Vorwurf der Patentverletzung nicht erst bei Ausf\"{u}hrung des Verfahrens erheben zu k\"{o}nnen, sondern Dritte als Patentverletzer bereits dann belangen zu k\"{o}nnen, wenn Gegenst\"{a}nde gehandelt werden, mit deren Hilfe die Ausf\"{u}hrung des Verfahrens gelingt bzw in Gang gesetzt werden kann.  Diesem Wunsch mag zwar die \"{U}berlegung zugrunde liegen, dass es Sache des Anmelders ist, den in Frage kommenden Patentschutz durch entsprechende Anspruchsformulierung auszusch\"{o}pfen.  Das bietet jedoch keinen Grund, die Frage, ob ein angemeldeter Patentanspruch die erforderliche Patentf\"{a}higkeit aufweist, allein nach der Kategorie dieses Anspruchs und unabh\"{a}ngig davon zu beantworten, was nach der beanspruchten Lehre im Vordergrund steht.

Der vorstehenden Bewertung der Kategorie des Anspruchs 22 steht auch nicht entgegen, dass der Senat in der .. Entscheidung .. Sprachanalyseeinrichtung\footnote{\url{http://swpat.ffii.org/papiere/bgh-sprach00/index.de.html}} .. bei einer Datenverarbeitungsanlage, auf welcher die Bearbeitung von Texten vorgenommen wird, technischen Charakter angenommen hat, weil der Patentanspruch eine industriell herstellbare und gewerblich einsetzbare Vorrichtung betrifft.  Denn damals waren es die vorrichtungsgem\"{a}{\ss} gekennzeichneten Merkmale des zu beurteilenden Patentanspruchs, dier der L\"{o}sung des Problems dienten, das dem damaligen Schutzbegehren zugrunde lag.

(c) Bei der erneuten Befassung wird das Bundespatentgericht daher vor allem die verfahrensm\"{a}{\ss}igen Anweisungen der in Anspruch 22 in Bezug genommenen Anspr\"{u}che 1 bis 17 zu bewerten haben.  Diesen Anweisungen liegen ausweislich der Beschreibung der Patentanmeldung Erkenntnisse zugrunde, die durch statistische Erhebung gewonnen werden k\"{o}nnen.  Sollten sie (auch) die Lehre nach Anspruch 22 pr\"{a}gen, m\"{u}sste diesem nach dem Vorgesagten die Patentierbarkeit abgesprochen werden.  Allerdings erscheint auch die gegenteilige Bewertung nicht g\"{a}nzlich ausgeschlossen.

...

\begin{itemize}
\item
{\bf {\bf \url{bgh-suche01.pdf}}}

\begin{quote}
Papierfassung
\end{quote}
\filbreak

\item
{\bf {\bf phm: BGH er\"{o}ffnet Weg f\"{u}r direkte Patentierung von Informationswerken\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Dec/0010.html}}}}

\begin{quote}
s. auch folgende Diskussion
\end{quote}
\filbreak

\item
{\bf {\bf Volltext\footnote{\url{http://lenz.als.aoyama.ac.jp/Stellungnahmen/bgh_fehlersuche.htm}}}}

\begin{quote}
Prof. Lenz hat den Text vollst\"{a}ndig verf\"{u}gbar gemacht.
\end{quote}
\filbreak

\item
{\bf {\bf drlenz: Fragen\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Dec/0048.html}}}}

\begin{quote}
Prof Lenz formuliert 4 Fragen nach der Bedeutung dieser Entscheidung und k\"{u}ndigt Stellungnahme an.
\end{quote}
\filbreak

\item
{\bf {\bf drlenz: Schritt in Richtung Gesetzlichkeit\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Dec/0051.html}}}}

\begin{quote}
Die Entscheidung ist ein wesentlicher Schritt in Richtung auf eine ernsthafte Anwendung des Gesetzes, auch wenn manches daran sich so nicht aus dem Gesetz herleiten l\"{a}sst.
\end{quote}
\filbreak

\item
{\bf {\bf phm: BGH authorises claims to computer programs\footnote{\url{http://aful.org/wws/arc/patents/2001-12/msg02502.html}}}}

\begin{quote}
s. auch Antworten und folgende Beitr\"{a}ge
\end{quote}
\filbreak

\item
{\bf {\bf Bgh\footnote{\url{http://www.uni-karlsruhe.de/\~BGH/}}}}

\begin{quote}
Netzheimat des BGH: sp\"{a}rliche Informationen, keine Erkl\"{a}rung zum Beschluss.

\url{http://www.uni-karlsruhe.de/\~BGH/gvpl00.htm\#zs10}
\end{quote}
\filbreak

\item
{\bf {\bf Bpatg17w6998\footnote{\url{http://swpat.ffii.org/papiere/bpatg17-suche00/index.de.html}}}}
\filbreak

\item
{\bf {\bf EPO T 1173/97: IBM Computer Program Product\footnote{\url{http://swpat.ffii.org/papiere/epo-t971173/index.de.html}}}}

\begin{quote}
EPA Entscheidung T 1173/97: IBM-Computerprogrammprodukt
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{\url{http://swpat.ffii.org/stidi/erfindung/index.de.html}}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
% mode: latex ;
% End: ;

