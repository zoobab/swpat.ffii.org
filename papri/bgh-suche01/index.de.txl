<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: BGH 2001: Patentansprüche auf %(q:Computerprogrammprodukt) etc
zulässig

#descr: Beschluss des 10. Senates des Bundesgerichtshofes (BGH/10) in der
Rechtsbeschwerdesache betr die Patentanmeldung P 4323241.8-53 (IBM:
Suche fehlerhafter Zeichenketten in einem Text) vom Oktober 2000.  Der
17. Senat des Bundespatentgerichtes (BPatG/17) hatte im August 2000
erkldrt, dass ein Computerprogramm nicht beansprucht werden kann, und
dass ein %(q:Computerprogramm als solches) nichts anderes als ein
Computerprogramm in beliebiger Entwurfsstufe ist, und dabei diverse
Lehrsdtze des BGH und EPA analysiert und zur|ckgewiesen.  BGH/10 hebt
den BPatG/17-Beschluss auf und deutet an, dass Programme
mvglicherweise beansprucht werden kvnnten, wenn der Gegenstand des
Hauptanspruchs sich als eine Erfindung gemd_ g1 PatG erweist, deutet
aber an, dass dies im vorliegenden Falle nicht gegeben sein d|rfte und
verweist den Fall zum BPatG zur erneuten Pr|fung zur|ck.

#tWn: Comments by Hartmut Pilch on comments by PA Jürgen Betten about an
ambivalent decision of the Patent Senate of the German Federal Court
of Justice of Nov 2001 on whether claims to a program with the
%(q:technical effect) of correcting errors in text strings are
permissible.  In this exchange of opinions some of the speakers of the
EuroParl Hearing of 2002-11-07 are mentioned and some backgrounds of
this hearing and of the European Councils Patent Workgroup's decisions
are explained.

#gWi: English Version

#Anc: Aktenzeichen

#Bt0: BGH, Beschl. v. 17. Oktober 2001 - X ZB 16/00 - Bundespatentgericht

#Gst: Gesetze

#Ltt: Leitsätze

#DWe: Das Patentierungsverbot für Computerprogramme als solche verbietet,
jedwede in computergerechte Anweisungen gekleidete Lehre als
patentierbar zu erachten, wenn sie nur - irgendwie - über die
Bereitstellung der Mittel hinausgeht, welche die Nutzung als Programm
für Datenverarbeitungsanlagen erlauben.  Die prägenden Anweisungen der
beanspruchten Lehre müssen vielmehr insoweit der Lösung eines
konkreten technischen Problems dienen.

#EgE: Eine vom Patentierungsverbot erfasste Lehre (Computerprogramm als
solches) wird nicht schon dadurch patentierbar, dass sie in einer auf
einem herkömmlichen Datenträger gespeicherten Form zum Patentschutz
angemeldet wird.

#Del: Der 10. Zivilsenat des Bundesgerichtshofes hat durch den Vorsitzenden
Richter Rogge und die Richter Prof. Dr. Jestaedt, Dr. Melullis,
Scharen und Dr. Meier-Beck am 17. Oktober 2001 beschlossen:

#Ass: Auf die Rechtsbeschwerde der Anmelderin wird der %(bp:Beschluss des
%(tpe:17. Senats:Technischen Beschwerdesenats) des
Bundespatentgerichts vom 28. Juli 2000) aufgehoben.

#Dnn: Die Sache wird zur anderweiten Verhandlung auch über die Kosten der
Beschwerde an das Bundespatentbericht zurückverwiesen.

#DWd: Der Wert des Gegenstandes der Rechtsbeschwerde wird auf 50000 DM
festgesetzt.

#GKd: Gründe:

#AWW: Die ... Rechtsbeschwerde führt zur Zurückweisung der Sache an das
Bundespatentgericht, weil sich anhand der von diesem getroffenen
tatsächlichen Feststellungen nicht abschließend beurteilen lässt, ob
dem Patentanspruch 22 die Patentierbarkeit fehlt, wie das
Bundespatentgericht angenommen hat, oder ob dieser Anspruch einen
patentierbaren Gegenstand hat.

#AWW2: Das Bundespatentgericht hat der Anmeldung im Wege der Auslegung
entnommen, der noch streitige Patentanspruch 22 solle sich auf ein
übliches Speichermedium beziehen, das sich von anderen
maschinenlesbaren Speichermedien dadurch unterscheide, dass es eine
Aufzeichnung trage, die im Zusammenwirken mit einem geeigneten
Computersystem eine Ausführung des Verfahrens nach einem der in Bezug
genommenen Patentansprüche bewirken könne.

#AWW3: Das begegnet keinen rechtlichen Bedenken und wird im Ergebnis auch von
der Rechtsbeschwerde nicht angegriffen.  Sie stellt darauf ab, es
handele sich um einen Datentr/aeger, der lediglich das Steuerelement
mit einer technischen Schnittstelle zum Computersystem dergestalt
darstelle, dass elektronisch auslesbare Steuersignale vorhanden seien,
die so mit dem Computersystem zusammenwirkten, dass die Schritte des
erfindungsgemäßen Verfahrens ausgeführt werden.

#AWW4: Das Bundespatentgericht hat den für Anspruch 22 begehrten Patentschutz
schon deshalb versagt, weil dieser Patentanspruch keine Lehre angebe,
die wenigstens die wesentlichen Lösungsmittel umfasse.  Eine Erfindung
im Sinne einer Lehre zum technischen Handeln bestehe in der Lösung
eines technischen Problems.  Nach Satz 2 Abs. 3 der Beschreibung liege
dem Patentbegehren die Aufgabe zugrunde, ein verbessertes Verfahren
und Computersystem zur Suche und/oder Korrektur einer fehlerhaften
Zeichenkette in einem Text zu schaffen.  Das könne jedoch allein durch
ein digitales Speichermedium nicht gelingen, auf dem, wie
Patentanspruch 22 lediglich angebe, eine Aufzeichnung angebracht sei. 
Eine Ausführung des Verfahrens gelinge nur mit einem Computersystem,
das in der Lage sei, die einzelnen Teile der Aufzeichnung quasi
vollständig zu interpretieren und dadurch eine Durchführung der
gewünschten Verfahrensschritte zu bewirken.

#AWW5: Das beanstandet die Rechtsbeschwerde zu Recht.

#AWW6: Die Feststellung des Bundespatentgerichts, Patentanspruch 22 lehre zur
Lösung der in der Anmeldung genannten Aufgabe lediglich das Aufbringen
einer Aufzeichnung von durch ein Computersystem erst noch zu
interpretierenden Daten, die selbst nicht die zur Durchführung von
Verfahrensschritten repräsentativen Steuersignale darstellten, greift
zu kurz.  Sie lässt unberücksichtigt, dass das beanspruchte
Speichermedium über die auf ihm aufgebrachten auslesbaren Daten nach
dem Wortlaut des Patentanspruchs derart mit einem programmierbaren
Computersystem zusammenwirken können muss, dass das insbesondere in
dem Anspruch 1 beanspruchte Verfahren ausgeführt wird.  Die Anweisung
nach Patentanspruch 22 dient danach zur Realisierung eines bestimmten
Computerprogramms.  Das vorgeschlagene digitale Speichermedium selbst
ist ein gegenständliches Mittel zur Ausführung des in dem
nachgesuchten Patent ferner vorgeschlagenen Verfahrens; sein
bestimmungsgemäßer Einsatz führt - die Ausführbarkeit und technische
Brauchbarkeit der angemeldeten Anweisung vorausgesetzt - zu dem
gewünschten Ergebnis.  Das reicht für eine im Rahmen der die Anmeldung
prägenden Problemstellung liegende Lösung aus.

#AWW7: Das Bundespatentgericht hat das Speichermedium mit einer Aufzeichnung
gemäß dem Anspruch 22 als ein %(q:Programm für eine
Datenverarbeitungsanlage als solches) angesehen und gemeint, dass
dieser Anspruch deshalb auch nach §1 Abs. 2 Nr. 3 und Abs. 3 PatG vom
Patentschutz ausgenommen sei.

#AWW8: Zu dieser Bewertung ist es gelangt, weil der Computerfachmann den
mehrdeutigen Begriff %(q:Programm) bei enger Sicht lediglich für den
Programmcode und dessen Aufzeichnungen (gleichgültig welche
Entwurfsstufe) verwende.  Da §1 Abs. 2 Nr. 3 in Verbindung mit Abs. 3
PatG nach einer engen Auslegung verlange, umfasse der Begriff
%(q:Programm für eine Datenverarbeitungsanlage als solches) eine
Programmcodedarstellung oder -aufzeichnung auf einem
Klarschriftdatenträger wie Papier oder einem maschinenlesbaren
Speichermedium.

#AWW9: Auch diese Auffassung bekämpft die Rechtsbeschwerde zu Recht.

#AWW10: Allerdings wird sie in der Literatur verschiedentlich
%(bf:befürwortet).  Es gibt aber auch maßgebliche Gegenstimmen.  Vor
allem ist auf die Spruchpraxis des Europäischen Patentamtes zu dem
nahezu wortgleichen Art. 52 Abs. 2 Buchst. c, Abs. 3 EPÜ zu verweisen,
%(ep:wonach ein -- Datenverarbeitung mittels eines geeigneten
Computers betreffender -- Gegenstand nicht als %(q:Programm als
solches) im Sinne dieser Regelung zu verstehen ist, wenn er --
hinreichend qualifizierten -- technischen Charakter hat).  Zu
ähnlichen Ergebnissen führt eine in der Literatur vertretene
Auffassung, %(me:wonach unter %(q:Programm als solches) lediglich der
zugrundeliegende, von einer technischen Funktion noch freie
Programminhalt zu verstehen ist).

#AWW11: Der Auffassung des Bundespatentgerichts kann aus Rechtsgründen nicht
beigetreten werden.

#AWW12: Bei der Bestimmung, was als Programm für Datenverarbeitungsanlagen vom
Patentschutz ausgenommen ist, weil es ein Programm als solches ist,
kann nicht allein auf das Verständnis von Computerfachleuten
zurückgegriffen werden.  Die Bestimmung hat vielmehr -- wie auch sonst
bei der Gesetzesauslegung -- ausgehend vom Wortlaut sachbezogen nach
Sinn und Zweck der gesetzlichen Regelung zu fragen.

#AWW13: Die gesetzliche Regelung ergibt schon nach ihrem Wortlaut zunächst,
dass weder Programme für Datenverarbeitungsanlagen schlechthin vom
Patentschutz ausgeschlossen sind, noch dass bei Vorliegen der weiteren
Voraussetzungen des Gesetzes für jedes Computerprogramm Patentschutz
erlangt werden kann.  Letzteres führt zu der Erkenntnis, dass eine
beanspruchte Lehre nicht schon deshalb als patentierbar angesehen
werden kann, weil sie bestimmungsgemäß den Einsatz eines Computers
erfordert.  Es muss vielmehr bei einer Lehre, die bei ihrer Befolgung
dazu beiträgt, dass eine geeignete Datenverarbeitungsanlage bestimmte
Anweisungen abarbeitet, eine hierüber hinausgehende Eigenheit
bestehen.  Da Datenverarbeitung geeignet erscheint, in nahezu allen
Bereichen des menschlichen Lebens nützlich zu sein, kann im Hinblick
auf diese Notwendigkeit außerdem nicht unberücksichtigt bleiben, dass
das Patentrecht geschaffen wurde, um durch Gewährung eines zeitlich
beschränkten Ausschließlichkeitsschutzes neue, nicht nahegelegte und
gewerblich anwendbare Problemlösungen auf dem Gebiet der Technik zu
fördern.  Das wiederum verbietet, jedwede in computergerechte
Anweisungen gekleidete Lehre als patentierbar zu erachten, wenn sie
nur -- irgendwie -- über die Bereitstellung der Mittel hinausgeht,
welche die Nutzung als Programm für Datenverarbeitungsanlagen
erlauben.  Die prägenden Anweisungen der beanspruchten Lehre müssen
vielmehr insoweit der Lösung eines konkreten technischen Problems
dienen.  Unter diesen Voraussetzungen ist die beanspruchte Lehre dem
Patentschutz auch dann zugänglich, wenn sie als Computerprogramm oder
in einer sonstigen Erscheinungsform geschützt werden soll, die eine
Datenverarbeitungsanlage nutzt.

#AWW14: Diese Abgrenzung der für Datenverarbeitungsanlagen bestimmten
Programme, für die als solche Schutz begehrt wird, von
computerbezogenen Gegenständen, die §1 Abs 2 Nr 3 PatG nicht
unterfallen, führt dazu, dass Ansprüche, die zur Lösung eines
Problems, das auf den herkömmlichen Gebieten der Technik, also der
Ingenieurwissenschaften, der Physik, der Chemie oder der Biologie
besteht, die Abarbeitung bestimmter Verfahrensschritte durch einen
Computer vorschlagen, grundsätzlich patentierbar sind.  Ansonsten
bedarf es hingegen einer Prüfung, ob die auf Datenverarbeitung mittels
eines geeigneten Computers gerichtete Lehre sich gerade durch eine
Eigenheit auszeichnet, die unter Berücksichtigung der Zielsetzung
patentrechtlichen Schutzes eine Patentierbarkeit rechtfertigt.

#AWW15: Hiervon ist der Senat bereits bisher im Rahmen seiner neueren
Rechtsprechung zu computerbezogenen Patentanmeldungen ausgegangen.  So
hat er -- wenn auch im Hinblick auf die für eine Erfindung i.S.d. §1
Abs. 1 PatG erforderliche Technizität -- %(lv:eine Gesamtbetrachtung
darüber gefordert, was nach der beanspruchten Lehre im Vordergrund
steht).  Das erlaubt in dem hier interessierenden Zusammenhang
ebenfalls eine sachgerechte Wertung und Abgrenzung. ...

#AWW16: Das vom Senat für maßgeblich gehaltene Verständnis von §1 Abs 2 Nr 3
PatG wird durch die Gesetzessystematik bestätigt.  Die dargelegte
Tragweite des Patentierungsverbots für Computerprogramme entspricht
derjenigen von anderen Tatbeständen des §1 Abs 2 PatG.  Sowohl die
dort in Nr. 1 genannten wissenschaftlichen Theorien und mathematischen
Methoden als auch die in Nr. 3 genannten Pläne, Regeln und Verfahren
für gedankliche Tätigkeiten sind nur insoweit vom Patentschutz
ausgeschlossen, als sie losgelöst von einer konkreten Umsetzung
beansprucht werden.  Soweit sie hingegen zur Lösung eines konkreten
technischen Problems Verwendung finden, sind sie -- in diesem Kontext
-- grundsätzlich %(pf:patentfähig).

#AWW17: §1 Abs 2 Nr 3, Abs 3 PatG ist bewusst an die europäische Regelung in
Art 52 Abs 32 Buchstabe c, Abs. 3 EPÜ angeglichen worden, um
sicherzustellen, dass der Kreis der patentfähigen Erfindungen
%(bt:nach nationalem Recht derselbe ist wie nach dem Europäischen
Patentübereinkommen).  Bei der Entstehung des Europäischen
Patentübereinkommens herrschte zwar im Hinblick auf die Patentierung
von computerbezogenen Lehren keine klare Vorstellung darüber, welche
Definition gewählt werden soll.  %(rr:Während der diplomatischen
Konferenz zum Abschluss des Übereinkommens wurde ausdrücklich darauf
hingewiesen, dass vergeblich versucht worden sei, die
Begrifflichkeiten auszufüllen; die Auslegung müsse der Rechtspraxis
überlassen bleiben).

#AWW18: Die in das Europäische Patentübereinkommen und das PatG übernommene
Wortwahl trägt jedoch dem Anliegen Rechnung, die Entwicklung auf dem
damals immer noch relativ neuen Gebiet der Computertechnik nicht durch
eine uferlose Ausdehnung des Patentschutzes zu behindern.  Dies legt
es nahe, Lehren aus Gebieten, die nach traditionellem Verständnis
nicht zur Technik gehören, nicht allein deshalb dem Patentschutz
zugänglich zu erachten, weil sie mit hilfe eines Computers angewendet
werden sollen.  Andererseits würde es über das genannte Ziel
hinausgehen, einer Lehre, deren Eigenart durch technische Vorgänge
oder Überlegungen geprägt ist, den Patentschutz zu versagen, weil sie
auf einem Computer zur Ausführung kommen soll und/oder von einem Teil
der Computerfachleute in einem engeren Sinne als Programm für
Datenverarbeitungsanlagen angesehen wird.

#AWW19: Ob Anspruch 22 hiernach von dem Patentierungsausschluss nach §1 Abs 2
Nr 3 PatG erfasst wird, kann der Senat nicht abschließend beurteilen.

#AWW20: Die Anmeldung betrifft die Suche und/oder Korrektur einer fehlerhaften
Zeichenkette in einem Text.  Das liegt nicht auf technischem Gebiet,
auch wenn der zu prüfende Text mit einem computergestützten
Textverarbeitungssystem erstellt worden ist.  Im vorliegenden Fall ist
deshalb -- wie ausgeführt -- eine Bewertung nötig, ob Anspruch 22
Anweisungen enthält, die den erforderlichen Bezug zur Technik
herstellen.  Das erfordert eine tatrichterliche Analyse sowie die
Feststellung der maßgeblichen Umstände, die das Bundespatentgericht --
in Konsequenz seines rechtlichen Ausgangspunktes -- nicht getroffen
hat.  Das wird daher nachzuholen sein.

#AWW21: Die neuerliche Prüfung ist nicht etwa deshalb entbehrlich, weil mit
Anspruch 22 ein Verfahren nicht unmittelbar beansprucht wird.  Die in
Anspruch 22 enthaltene Lehre kann nicht schon deshalb patentiert
werden, weil dieser Anspruch insbesondere auf eine Diskette und damit
auf einen körperlichen Gegenstand (Vorrichtung) gerichtet ist.

#AWW22: Nach der Beschreibung in der Patentanmeldung wird bei bekannten
Textverarbeitungssystemen auf ein sogenantes Lexikon zurückgegriffen. 
Dieses enthält eine Liste von bekannten Wörtern.  Zur Fehlersuche
werden die Wörter eines eingegebenen Textes mit den Einträgen des
Lexikons verglichen.  Die Verwendung des Lexikons erfordert einen
relativ großen Speicherplatz.  Ferner kann es seinerseits Fehleinträge
enthalten.  Es muss darüber hinaus ständig aktualisiert werden, was zu
weiteren Fehleinträgen führen kann.

#AWW23: Zur Überwindung der hiernach bestehenden nachteile kommt der durch
Anspruch 22 gemachte Lösungsvorschlag nicht ohne Ausführung des
insbesondere nach dem erteilten Patentanspruch 1 beanspruchten
Verfahrens aus.  Ähnlich einem Blatt Papier, das anderweitig benötigte
Informationen enthält, kommt dem Speichermedium, das durch Anspruch 22
geschützt werden soll, nur die Funktion eines Informationsträgers zu,
der eingesetzt werden kann, wenn die Ausführung des Verfahrens durch
einen Computer gewünscht wird.  Auch die Rechtsbeschwerde erkennt an,
dass der Datenträger als solcher im vorliegenden Fall nicht zur
Begründung der Patentfähigkeit beiträgt.  Wie die Anmelderin in der
Rechtsbeschwerde noch einmal geltend gemacht hat, ist Anspruch 22 auf
eine Lehre für einen solchen Gegenstand nur deshalb gerichtet, um ohne
besonderen Nachweis den Vorwurf der Patentverletzung nicht erst bei
Ausführung des Verfahrens erheben zu können, sondern Dritte als
Patentverletzer bereits dann belangen zu können, wenn Gegenstände
gehandelt werden, mit deren Hilfe die Ausführung des Verfahrens
gelingt bzw in Gang gesetzt werden kann.  Diesem Wunsch mag zwar die
Überlegung zugrunde liegen, dass es Sache des Anmelders ist, den in
Frage kommenden Patentschutz durch entsprechende Anspruchsformulierung
auszuschöpfen.  Das bietet jedoch keinen Grund, die Frage, ob ein
angemeldeter Patentanspruch die erforderliche Patentfähigkeit
aufweist, allein nach der Kategorie dieses Anspruchs und unabhängig
davon zu beantworten, was nach der beanspruchten Lehre im Vordergrund
steht.

#AWW24: Der vorstehenden Bewertung der Kategorie des Anspruchs 22 steht auch
nicht entgegen, dass der Senat in der .. Entscheidung ..
%(sa:Sprachanalyseeinrichtung) .. bei einer Datenverarbeitungsanlage,
auf welcher die Bearbeitung von Texten vorgenommen wird, technischen
Charakter angenommen hat, weil der Patentanspruch eine industriell
herstellbare und gewerblich einsetzbare Vorrichtung betrifft.  Denn
damals waren es die vorrichtungsgemäß gekennzeichneten Merkmale des zu
beurteilenden Patentanspruchs, dier der Lösung des Problems dienten,
das dem damaligen Schutzbegehren zugrunde lag.

#AWW25: Bei der erneuten Befassung wird das Bundespatentgericht daher vor
allem die verfahrensmäßigen Anweisungen der in Anspruch 22 in Bezug
genommenen Ansprüche 1 bis 17 zu bewerten haben.  Diesen Anweisungen
liegen ausweislich der Beschreibung der Patentanmeldung Erkenntnisse
zugrunde, die durch statistische Erhebung gewonnen werden können. 
Sollten sie (auch) die Lehre nach Anspruch 22 prägen, müsste diesem
nach dem Vorgesagten die Patentierbarkeit abgesprochen werden. 
Allerdings erscheint auch die gegenteilige Bewertung nicht gänzlich
ausgeschlossen.

#Bdv: BGH eröffnet Weg für direkte Patentierung von Informationswerken

#AWW26: s. auch folgende Diskussion

#DWn: Die Entscheidung ist ein wesentlicher Schritt in Richtung auf eine
ernsthafte Anwendung des Gesetzes, auch wenn manches daran sich so
nicht aus dem Gesetz herleiten lässt.

#BWp: BGH authorises claims to computer programs

#sre: s. auch Antworten und folgende Beiträge

#NiW: Netzheimat des BGH: spärliche Informationen, keine Erklärung zum
Beschluss.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bgh-suche01 ;
# txtlang: xx ;
# End: ;

