<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Paul B. Laat, philosopher from Groningen, argues that mathematical
concepts should be patentable without any reference to practical
applications.  He points out that such references lead to
inconsistencies and that it would be better to instead limit the range
of objects against which patents can be enforced.  Unfortunately he
does not engage in a serious discussions about the latter or about the
economic implications of unlimited patentability.  Instead he points
out that some important mathematical inventions in renaissance times
were kept secret for decades and might have been known earlier had
there been a patent system on mathematical ideas in place.

#TsW: The abstract says:

#TtW: The patenting of software-related inventions is on the increase,
especially in the United States.  Mathematical formulas and
algorithms, though, are still sacrosanct.  Only under special
conditions may algorithms qualify as statutory matter: if they are not
solely a mathematical exercise, but if they are somehow linked with
physical reality.  In this article, the best results are obtained if
formulas and algorithms are only protected in combination with a proof
that supports them.  This argument is developed by conducting a
thought experiment.  After describing the development of algebra from
the 16th century up to the 20th century (in particular, the solution
of the cubic equation), the likely effects on the development of
mathematics as a science are analyzed in the context of postulating a
patent regime that would actually have been in force protecting
mathematical inventions.

#Lte: Laat points out the inconsistencies brought about in the US by
limiting patent claims to applications of mathematics, citing the
Karmakar and RSA examples.  He then cites examples of advances in
mathematical knowledge in renaissance times that were kept secret and
that, according to Laat, would probably have been published earlier if
there had been a patent system in place.

#LWc: Laat concludes

#WkW: What lessons can be drawn from this thought experiment?  If the
results are any guide to present-day developments, they would argue
for a BLANKET ACCEPTANCE of mathematical formula and algorithms as
statutory matter for patent applications.  Let us finally acknowledge
that mathematics belongs not only to the 'liberal arts' but also the
'useful arts'.  Let us abolish the 'pure-number condition'; even if it
is all mathematical abstraction, this is acceptable.  There is no need
to specify links with 'physical elements' or 'process steps', with
'pre-computer' or 'post-computer process activity'.  Even if nothing
of the sort is specified, the claim is acceptable.  Let us not bother
that the claim 'wholly preempts' the use of the algorithm; let all
possible commercial applications effectively become protectable.  It
should be stressed that noncommercial, in particular academic, use is
to remain possible (experimental use exception).  Actually, it is the
development of both (pure) science and (applied) technology that the
system is designed to foster.  Such unconditional acceptance, however,
should be carefully formulated as to what is protected and what is
not.  While the protection of formulas tout court is acceptal in
societal terms, the best results seem to be obtained when algorithms
and formulas are only patentable in combination with a supporting
proof.  That arrangements keeps avenues open for inventive
mathematicians to develop alternative methods of proof.

#Let: Laat suggests that patent monopolies could have helped to incite pure
mathematical research activity as long as that activity itself is
patent-free.  However he does not attempt to weigh the possible burden
that these monopolies place on the overall economy.

#Avt: Although he correctly states that such a general patentability shifts
the focus to the question of what kind of activities can constitute
infringement and advocates that there should be clear restrictions
concerning this, he does not propose any helpful criteria that could
serve to clarify these restrictions in a consistent and meaningful
manner.

#Het: He does however issue some warnings about the possible consequences of
his approach, saying e.g. that 16th century examples may not be
adequate for analysing today's problems and that there is a danger
that too burdensome monopolies could be imposed on society by this
approach.

#Tcd: The full text can be downloaded in PDF form.

#GiW: Greg Aharonian reports enthusiastically about a European philosopher
who seems to share his opinion and is therefore a true scientific
thinker.  Contains a copy of the abstract.

#Eea: Erik Josefsson quotes some more text pieces from Laat to relativise
Aharonian's enthusiasm.

#Hfa: Hartmut Pilch argues that %(tm:Tamai's article) pointed out the same
inconsistency, i.e. that of allowing the patenting of abstract
mathematics only in combination with an irrelevant practical
application.  But Laat seems to be overlooking some important
questions that arise from this.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: irle-laat00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

