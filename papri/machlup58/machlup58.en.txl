<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Machlup 1958: An Economic Review of the Patent System

#descr: Report to the US congress from 1958, which also extensively narrates
the history of the patent movement and of earlier economic research on
this subject.  Machlup, a renowned American economist of Austrian
origin, is the first author of a large treatise on knowledge economics
and other treatises which belong to the teaching repertoire of
economics departments in universities.  His report cites a wealth of
historical and economic evidence to refute most of the reasoning used
by lawyers to legitimate the patent system.

#Gae: German version

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: machlup58 ;
# txtlang: en ;
# multlin: t ;
# End: ;

