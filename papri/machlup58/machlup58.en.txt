<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Report to the US congress from 1958, which extensively narrates the history of the patent movement and of earlier economic research on this subject.  Machlup, a renowned American economist of Austrian origin, is the first author of a large treatise on knowledge economics and other treatises which belong to the teaching repertoire of economics departments in universities.  His report concludes that the patent system on the whole fails to achieve its professed aims and does more harm than good.  Through his detailed account, Machlup comes to the conclusion that the patent system was a %(q:victorious movement of lawyers against economists).  The list of patent critics reads like a Who Is Who of economics.  A must read for anyone interested in the patent system.
title: Fritz Machlup 1958: The economic foundations of patent law
Gae: German version

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: machlup58 ;
# txtlang: en ;
# End: ;

