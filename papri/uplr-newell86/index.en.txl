<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Newell 1986: The Models are Broken

#descr: Paul Newell, professor of computer science, in response to law
professor Donald Chisum's proposal to affirm the patentability of
algorithms, does not directly say whether algorithms should be
patentable, but rather describes how both affirmation and negation of
this proposal would clash with the underlying assumptions of the
patent system and how this will inevitably challenge the foundations
of the patent system.  As more and more problems are solved by purely
mathematical means, the patent system will either have to become less
relevant for innovation, or it will have to completely review its
model of what an invention is and how it should be appropriated.  In
particular, Newell explains some basic concepts of informatics and
points out that, with algorithms, there can be no meaningful
differentiation between discovery and invention, between application
and theory, between abstract and concrete, between numeric and
symbolic etc.  Moreover he explains by a model of game theory that
sharing algorithms, as currently practised by programmers, may lead to
more innovation than making them appropriatable, so that a crude
application of the patent system to algorithms could very well stifle
rather than stimulate innovation.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: uplr-newell86 ;
# txtlang: xx ;
# End: ;

