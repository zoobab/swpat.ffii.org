<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Paul Newell, professor of computer science, in response to law professor Donald Chisum's proposal to affirm the patentability of algorithms, does not directly say whether algorithms should be patentable, but rather describes how both affirmation and negation of this proposal would clash with the underlying assumptions of the patent system and how this will inevitably challenge the foundations of the patent system.  As more and more problems are solved by purely mathematical means, the patent system will either have to become less relevant for innovation, or it will have to completely review its model of what an invention is and how it should be appropriated.  In particular, Newell explains some basic concepts of informatics and points out that, with algorithms, there can be no meaningful differentiation between discovery and invention, between application and theory, between abstract and concrete, between numeric and symbolic etc.  Moreover he explains by a model of game theory that sharing algorithms, as currently practised by programmers, may lead to more innovation than making them appropriatable, so that a crude application of the patent system to algorithms could very well stifle rather than stimulate innovation.
title: Newell 1986: The models are broken, the models are broken!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: uplr-newell86 ;
# txtlang: en ;
# End: ;

