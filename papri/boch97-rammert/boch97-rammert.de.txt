<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Der Autor, Prof. Dr. Werner Rammert, geb. 1949, lehrt Soziologie an der FU Berlin.  Er stellt fest, dass der Innovationsmotor in Deutschland ins Stottern geraten sei und möglicherweise nicht mit den konventionellen Fördermechanismen wieder in Gang gebracht werden kann.  Ein Problem dabei ist die Organisation der Innovation.  Heute sind weder Einzelerfinder noch Betriebe, weder der Einzelerfinder noch der Großkonzern, weder der kreative Wissenschaftler noch die staatliche Großforschung maßgeblich sondern sog. Innovationsnetzwerke.  Das sind informelle halboffene Gruppierungen aus Institutionen und Mitspielern unterschiedlicher Art.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: boch97-rammert ;
# txtlang: de ;
# End: ;

