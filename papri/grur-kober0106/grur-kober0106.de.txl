<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kober 2001: The Role of the European Patent Office

#descr: Dr. h.c. Ingo Kober, Präsident des Europäischen Patentamtes (EPA),
erklärt in dieser überarbeiteten Fassung eines am 15. November 2000 
im Rahmen des Patent- und Marken-Forums gehaltenen Vortrages, welche
Meilensteine auf dem Weg zum Ziel eines weltweit einheitlichen
Patentschutzes auf allen wirtschaftlich interessanten Gebieten schon
zurückgelegt wurden und noch zurückzulegen sind.  Er erklärt dabei,
dass die soeben abgehaltene Diplomatische Konferenz zwar nicht den
Wünschen des EPA entsprechend Art 52 EPÜ geändert hat, aber dass das
EPA dennoch mit voller Unterstützung der Regierungen weiterhin Patente
auf %(e:Computerprogramme und computer-gestützte geschäftliche
Methoden) (sic!) erteilen wird.

#Sir: Steigende Anmeldezahlen und zunehmendes Patentbewußtsein auf
nationaler, regionaler und globaler Ebene zeigen, daß der effektive
Schutz von Erfindung und Innovation in unserer vernetzten Welt für
immer wichtiger erachtet wird.  Fachleute überrascht dies nicht.  Als
Instrument des Wettbewerbs- und Wirtschaftsrechts haben sie das
Patentwesen schon immer zu den Faktoren gerechnet, die für den Ausbau
und die Sicherung von Marktpositionen von grundlegender Bedeutung
sind.  Das Patent macht die Erfindung verkehrsfähig und wirtschaftlich
verwertbar.  Dies ist besonders wichtig für eine wissensbasierte
Wirtschaft, wie die sogenannte Neue Ökonomie.  Wirksamer Patentschutz
ist hier die zentrale Voraussetzung für Investitionen und die
Bereitstellung von Risikokapital.

#Doi: Dies wird inzwischen auch von den Entscheidungsträgern in Politik und
Wirtschaft so gesehen. Förderung und Schutz von Innovationen gehören
zu den Top-Prioritäten der modernen Wirtschaftspolitik.  Die
europäische Kommission, nationale Regierungen und Parlamente befassen
sich heute mit Detailfragen des Patentrechts, die über Jahre nur in
Expertenkreisen behandelt worden sind.  Auch die allgemeine
Öffentlichkeit hat inzwischen das Patentrecht entdeckt.  Die
technologische Revolution im Bereich der Bio-Techniken und der
Informationstechnologie ist im Alltag zunehmend wahrnehmbar und wird
nicht zuletzt durhc die patentamtlichen Veröffentlichungen zum
Gegenstand breiter Diskussion.

#Rie: Revision des EPÜ

#DWK: Der Verwaltungsrat der Europäischen Patentorganisation hat daher auf
meinen Vorschlag Anfang 1998 die Initiative ergriffen und eine erste
Liste von rechtlichen und technischen Punkten für eine Revision des
EPÜ zusammengestellt und an seinen Ausschuss %(q:Patentrecht) zur
Prüfung überwiesen.  Damals hat wohl kaum jemand damit gerechnet, dass
dieses Vorhaben in nur knapp 2 Jahren erfolgreich abgeschlossen würde.

#Zsd: Zunächst möchte ich einen Punkt hervorheben, bei dem vorerst alles
beim alten bleiben wird.  Er betrifft die Schutzfähigkeit von
softwarebezogenen Erfindungen.  Die Konferenz hat sich gegen die vom
Verwaltungsrat vorgeschlagene Streichung der Computerprogramme aus der
Liste der nicht patentfähigen Gegenstände in Art. 52(2) EPÜ
ausgesprochen.  Damit bleibt die bestehende Rechtslage vorerst
unverändert.  Mit ihrer Entscheidung hat die Konferenz dem von der
Europäischen Kommission kürzlich eingeleiteten Konsultationsprozess
über die zukünftige Rechtsordnung auf diesem Gebiet Rechnung getragen.
 Die bisherige Praxis des Amts und seiner Beschwerdekammern, der
nationalen Patentbehörden und Gerichte wird damit jedoch in keiner
Weise in Frage gestellt.  Wie bisher können computer-implementierte
Erfindungen patentiert werden, wenn damit ein neuer und erfinderischer
technischer Beitrag zum vorbekannten Stand der Technik verbunden ist. 
Patentierbar sind also weiterhin technische Verfahren auf dem Gebiet
der Datenverarbeitung oder zur computergestützten Durchführung
geschäftlicher Methoden.  Dies ergibt sich aus dem der europäischen
Patentpraxis zu Grunde liegenden Erfindungsbegriff, der zwischen
technischen Lösungen und nichttechnischen Methoden klar unterscheidet.
 Die Richtlinien für die Prüfung im EPA werden derzeit überarbeitet,
um der neueren Rechtsprechung der Beschwerdekammern auf diesem Gebiet
Rechnung zu tragen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-kober0106 ;
# txtlang: de ;
# multlin: t ;
# End: ;

