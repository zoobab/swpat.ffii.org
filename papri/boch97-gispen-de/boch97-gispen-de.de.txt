<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Der Autor Prof. Kees Gispen lehrt in den USA, University of Mississippi, Geschichte.  Er gilt als profundester Kenner der Geschichte des deutschen Erfindungsschutzes.  Laut Gispen setzte sich das deutsche Patentwesen setzte 1877 gegen eine starke Ablehnungsfront durch, als die Wirtschaftspolitik sich vom Liberalismus zum staatlichen Interventionismus hin wendete.  Dabei wurden einige Argumente der Patentkritiker aufgenommen, so u.a. deren skeptische Haltung gegenüber der Erfinder-Romantik.  Nicht der einzelne Erfinderf sondern die Volkswirtschaft und der Erfindungen fördernde Betrieb stand im Mittelpunkt.  Die Patentrechte gehörten nicht dem Erfinder sondern dem Anmelder.  Dies begünstigte eine spezifisch deutsche Kultur des %(q:konservativen Erfindens), die sowohl Erfolge zeitigt als auch Unzufriedenheit hervorrief, die insbesondere von Kapitalismuskritikern unter den Sozialisten und Nationalsozialisten aufgegriffen wurde.  Viele der Patentreformer setzten schließlich auf Hitler, der in %(q:Mein Kampf) ihre Argumenation bekräftigt hatte.  Tatsächlich reformierten die Nationalsozialisten 1936 gegen den Willen der Großindustrie das Patentgesetz und legten die Grundlage für das spätere Arbeitnehmer-Erfindergesetz der Bundesrepublik.  Auf dem Umweg einer eigenartigen Dialektik der Geschichte entstand unverhofft ein modellhaftes System.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: boch97-gispen-de ;
# txtlang: de ;
# End: ;

