<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Arbeitsgruppe">
<link href="/girzu/index.de.html" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/01/06">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="F&ouml;rderverein f&uuml;r eine Freie Informationelle Infrastruktur, Logikpatente, Geistiges Eigentum, Gewerblicher Rechtschutz, GE, GE &amp; GR, immaterielle Verm&ouml;gensgegenst&auml;nde, Immaterialg&uuml;terrecht, mathematische Methoden, Gesch&auml;ftsverfahren, Organisations- und Rechenregeln, Erfindung, Nicht-Erfindungen, computer-implementierte Erfindungen, computer-implementierbare Erfindungen, software-bezogene Erfindungen, software-implementierte Erfindungen, Softwarepatent, Computer-Patente, Informationspatente, technische Erfindung, technischer Charakter, Technizit&auml;t, Technik, Datentechnik, gewerbliche Anwendbarkeit, patentability, materielles Patentrecht, right to use, Patentinflation, quelloffen, Standardisierung, Innovation, Wettbewerb, Patentamt, Generaldirektion Binnenmarkt, Patentbewegung, Patentfamilie, Patentwesen, Patentrecht, Patentjuristen, Lobby">
<meta name="title" content="Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens">
<meta name="description" content="Lindenmaier ist einer der Theoretiker des patentrechtlichen Erfindungsbegriffes.  In den 50er Jahren wurde die &quot;Lindenmaiersche Formel&quot; zur Definition der Technischen Erfindung verwendet.  Spdter baute der BGH darauf seine bekannte Technikdefinition auf.">
<title>Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.de.html"-->
<div id="doktit">
<h1>Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens<br>
<!--#include virtual="../../banner0.de.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.de.html"-->
</div>
</div>
<div id="dokmid">
<div id="papri">
<dl><dt>Titel:</dt>
<dd>Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens</dd>
<dt>Quelle:</dt>
<dd><a href="/papri/index.de.html#GRUR">GRUR</a> GRUR 1953.01 p12-15</dd></dl>
</div>
Zum Schluss fasst Lindenmaier seine &Uuml;berlegungen zu der folgenden Definition zusammen:
<blockquote>
Eine technische Erfindung ist eine Anweisung zur Benutzung von Kr&auml;ften oder Stoffen der unbelebten oder belebten Natur oder aus diesen Stoffen gewonnener Stoffe oder einer Kombination solcher Kr&auml;fte und Stoffe, mit dem erstrebten, erreichten und beliebig wiederholbaren Erfolge eines in der realen Erscheinungswelt liegenden und als solches unmittelbar verwertbaren Ergebnisses, die nach Weg oder Ergebnis oder beiden zum bisherigen Inhalt des durch Wort, Schrift oder zug&auml;ngliches technisches Handeln dokumentierten Standes der Technik nicht geh&ouml;rt und im Rahmen des durchschnittlichen Fachk&ouml;nnens nicht zu erwarten war.
</blockquote>
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/papri/grur-kolle77/index.de.html">Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs</a></b></dt>
<dd>Gert Kolle, heute im Europ&auml;ischen Patentamt f&uuml;r Internationales Patentrecht zust&auml;ndig, war bis Mitte der 80er Jahre der meistzitierte Rechtstheoretiker in Fragen der Technizit&auml;t und der Patentierbarkeit von Computerprogrammen.  Er agierte als wissenschaftlicher Referent und Berichterstatter der deutschen Delegation bei verschiedenen Patentgesetzgebungskonferenzen der 70er Jahre, bem&uuml;hte sich stets um einen unparteiischen wissenschaftlichen Standpunkt fernab jeglicher &quot;ideologischer Versteinerung&quot;, in der die beiden Fronten schon damals aufeinanderprallten.  Im vorliegenden GRUR-Artikel von 1977 erkl&auml;rt er, warum Computerprogramme nicht als &quot;technisch&quot; im Sinne des Patentrechts gelten k&ouml;nnen und warum eine &quot;naiv oder bewusst&quot; herbeigef&uuml;hrte &quot;Lockerung des Technikbegriffs&quot; zu unverantwortbaren Sperrwirkungen f&uuml;hren w&uuml;rde.  Es m&uuml;sse daher ein &quot;Niemandsland des Geistigen Eigentums&quot; geben, und Algorithmen sollten &quot;vergesellschaftet&quot; werden.  Ein wegen seiner Tiefe und Klarheit sehr empfehlenswerter Artikel, der nach &uuml;ber 20 Jahren kaum etwas von seiner Aktualit&auml;t verloren hat.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/stidi/korcu/index.de.html">Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs</a></b></dt>
<dd>Bisher geh&ouml;ren Computerprogramme ebenso wie andere <em>Organisations- und Rechenregeln</em> in Europa nicht zu den <em>patentf&auml;higen Erfindungen</em>, was nicht ausschlie&szlig;t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ&auml;ische Patentamt und einige nationale Gerichte haben diese zun&auml;chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf&uuml;hrung in die Thematik und einen &Uuml;berblick &uuml;ber die rechtswissenschaftliche Fachliteratur.</dd></dl>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.de.html"-->
</div>
<!--#include virtual="../../valid.de.html"-->
<div id="dokadr">
http://swpat.ffii.org/papri/grur-lindenm53/index.de.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/01/06 (2004/08/24)
<a href="/girzu/index.de.html">Arbeitsgruppe</a><br>
deutsche Version 2003/12/17 von <a href="http://www.a2e.de">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el -->
<!-- mode: html -->
<!-- End: -->

