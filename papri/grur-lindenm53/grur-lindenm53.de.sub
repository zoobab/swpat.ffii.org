\begin{subdocument}{grur-lindenm53}{Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens}{http://swpat.ffii.org/papri/grur-lindenm53/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/12/17 von Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Lindenmaier ist einer der Theoretiker des patentrechtlichen Erfindungsbegriffes.  In den 50er Jahren wurde die ``Lindenmaiersche Formel'' zur Definition der Technischen Erfindung verwendet.  Spdter baute der BGH darauf seine bekannte Technikdefinition auf.}
\begin{description}
\item[Titel:]\ Machlup 1958: Die wirtschaftlichen Grundlagen des Patentwesens
\item[Quelle:]\ GRUR\footnote{\url{http://localhost/swpat/papri/index.de.html\#GRUR}} GRUR 1953.01 p12-15
\end{description}

Zum Schluss fasst Lindenmaier seine \"{U}berlegungen zu der folgenden Definition zusammen:

\begin{quote}
Eine technische Erfindung ist eine Anweisung zur Benutzung von Kr\"{a}ften oder Stoffen der unbelebten oder belebten Natur oder aus diesen Stoffen gewonnener Stoffe oder einer Kombination solcher Kr\"{a}fte und Stoffe, mit dem erstrebten, erreichten und beliebig wiederholbaren Erfolge eines in der realen Erscheinungswelt liegenden und als solches unmittelbar verwertbaren Ergebnisses, die nach Weg oder Ergebnis oder beiden zum bisherigen Inhalt des durch Wort, Schrift oder zug\"{a}ngliches technisches Handeln dokumentierten Standes der Technik nicht geh\"{o}rt und im Rahmen des durchschnittlichen Fachk\"{o}nnens nicht zu erwarten war.
\end{quote}

\begin{itemize}
\item
{\bf {\bf Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs\footnote{\url{http://localhost/swpat/papri/grur-kolle77/index.de.html}}}}

\begin{quote}
Gert Kolle, heute im Europ\"{a}ischen Patentamt f\"{u}r Internationales Patentrecht zust\"{a}ndig, war bis Mitte der 80er Jahre der meistzitierte Rechtstheoretiker in Fragen der Technizit\"{a}t und der Patentierbarkeit von Computerprogrammen.  Er agierte als wissenschaftlicher Referent und Berichterstatter der deutschen Delegation bei verschiedenen Patentgesetzgebungskonferenzen der 70er Jahre, bem\"{u}hte sich stets um einen unparteiischen wissenschaftlichen Standpunkt fernab jeglicher ``ideologischer Versteinerung'', in der die beiden Fronten schon damals aufeinanderprallten.  Im vorliegenden GRUR-Artikel von 1977 erkl\"{a}rt er, warum Computerprogramme nicht als ``technisch'' im Sinne des Patentrechts gelten k\"{o}nnen und warum eine ``naiv oder bewusst'' herbeigef\"{u}hrte ``Lockerung des Technikbegriffs'' zu unverantwortbaren Sperrwirkungen f\"{u}hren w\"{u}rde.  Es m\"{u}sse daher ein ``Niemandsland des Geistigen Eigentums'' geben, und Algorithmen sollten ``vergesellschaftet'' werden.  Ein wegen seiner Tiefe und Klarheit sehr empfehlenswerter Artikel, der nach \"{u}ber 20 Jahren kaum etwas von seiner Aktualit\"{a}t verloren hat.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.de.html}}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

