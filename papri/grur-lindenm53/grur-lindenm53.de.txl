<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Lindenmaier ist einer der Theoretiker des patentrechtlichen
Erfindungsbegriffes.  In den 50er Jahren wurde die %(q:Lindenmaiersche
Formel) zur Definition der Technischen Erfindung verwendet.  Spdter
baute der BGH darauf seine bekannte Technikdefinition auf.

#Zrf: Zum Schluss fasst Lindenmaier seine Überlegungen zu der folgenden
Definition zusammen:

#Efe: Eine technische Erfindung ist eine Anweisung zur Benutzung von Kräften
oder Stoffen der unbelebten oder belebten Natur oder aus diesen
Stoffen gewonnener Stoffe oder einer Kombination solcher Kräfte und
Stoffe, mit dem erstrebten, erreichten und beliebig wiederholbaren
Erfolge eines in der realen Erscheinungswelt liegenden und als solches
unmittelbar verwertbaren Ergebnisses, die nach Weg oder Ergebnis oder
beiden zum bisherigen Inhalt des durch Wort, Schrift oder zugängliches
technisches Handeln dokumentierten Standes der Technik nicht gehört
und im Rahmen des durchschnittlichen Fachkönnens nicht zu erwarten
war.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-lindenm53 ;
# txtlang: de ;
# multlin: t ;
# End: ;

