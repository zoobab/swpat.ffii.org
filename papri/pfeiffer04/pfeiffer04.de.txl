<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#iWe: Weitere Verweise

#fWE: %(em:Empfehlung) durch %(ae:Endres)

#tal: Ich möchte alle an dem Thema SW-Patente interessierten GI-Mitglieder
(und andere) auf den Beitrag von Herrn Axel Pfeiffer im aktuellen Heft
von %(q:Informatik - Forschung und Entwicklung) (IFE Heft 19,1)
hinweisen. Interessenten mit Online-Zugang finden ihn unter:

#ncW: Für Nicht-Abonnenten ist er auch online zu beziehen, und zwar gegen
die übliche Gebühr pro Artikel. Wie Sie sehen werden, räumt hier ein
erfahrener Patent-Praktiker mit den vielen Scheinargumenten der
Patentgegner auf. Der Beitrag zeigt auf, welche widersprüchlichen
Argumente herhalten müssen, wenn es darum geht, die Motive von
Erfindern und Patentanmeldern in Zweifel zu ziehen.

#eei: Argumente, Sichtweisen, Paradigmen (I)

#eim: Argumente, Sichtweisen, Paradigmen (II)

#i4l: Seite 14 Zeile 19

#hae: Nachfolgend sollen in den Punkten 3.1 bis 3.15 diejenigen Argumente,
Paradigmen, Sichtweisen und Insinuationen dargelegt werden, die
seitens der Gegner von Patenten auf softwareimplementierbare
Erfindungen zur Unterstützung ihrer Forderungen und Wünsche verwendet
werden. Die Tatsache, dass die ?Argumente? hier aufgeführt werden,
heißt nicht, dass der Autor ihnen Richtigkeit beimessen würde. Sie
werden hier genannt um zu zeigen, wie argumentiert wird, um abschätzen
zu können, in welcher Weise und auf welcher Ebene Beeinflussungen
stattfinden und mit was auch zukünftig zu rechnen ist.

#reh: Die Formulierung hier spricht für sich und zeigt deutlich, dass dem
Autor nicht wirklich an einer ernsthaften Beschäftigung mit
Gegenargumenten gelegen ist. Sie dienen aus seiner Sicht ja nur der
%(q:Beeinflussung) (wessen?).

#seh: Angemerkt sei an dieser Stelle noch, dass Pfeiffers Vorgehen sowohl
wissenschaftlich als auch journalistisch sehr fragwürdig ist. Er
zitiert hier mitnichten echte Argumente der Softwarepatentgegner oder
belegt diese gar mit Quellenangaben. Vielmehr formuliert er selbst
mögliche Argumente - was eine Gegenargumentation natürlich viel
einfacher macht. Dass ihm aber auch das nicht wirklich gelingt wird
die folgende Analyse zeigen.

#nte: 3.1 %(q:Patente passen auf Software nicht, denn Software ist etwas
Besonders, weil?)

#Wea: ... ja, weil ? damit in Europa 400.000 Leute befasst seien, ...
Software Abbild menschlichen Denkens sei, ... Software und Sprache und
Denken und Kreativität dasselbe seien, ... Software ein Kulturgut
geworden sei, ... Software sehr viele Schnittstellen und Standards
habe, die nicht patentiert werden dürften, ? Softwarefirmen
Basisökonomie kleiner Firmen seien, ... die Programmierer sonst
Patentrecherchen machen müssten, ... Softwarepatente zu
Überamortisationen führen könnten, ... Patente auf Software immer
wieder die einzige mögliche Lösung blockieren würden, ... Software im
Internet ganz einfach verteilt werden könne, ... Software eine ganz
andere Vertriebs- und Kostenstruktur habe, ... Software ganz
außergewöhnlich innovativ und deshalb viele Jahre Patentschutz
hinderlich seien, ... Software das Open-Source-Geschäftsmodell
erlaube, usw..

#Wau: Teilweise spricht PA Pfeiffer hier sehr gewichtige Argumente an,
verkürzt sie aber sehr stark. An gegebener Stelle werde ich noch
darauf eingehen. Andere Argumente sind für sich genommen lächerlich
und sicher nie von ernstzunehmenden Softwarepatentgegnern so verwendet
worden.

#WWs: Bei vielen dieser Statements lässt sich das Wort %(q:Software)
schlicht durch %(q:Hardware) oder durch %(q:Eisen) ersetzen, und das
Statement ist genauso richtig oder halbrichtig wie vorher. Dies ist
ein Indiz dafür, dass die Aussage an sich zwar (halb-) richtig sein
mag, es sich dabei aber eben nicht um eine Softwarebesonderheit
handelt.

#Phu: Die Pfeiffersche Substitution.

#e3w: Führen wir sie doch einfach einmal durch: Eisen ist ein Abbild
menschlichen Denkens und ein Kulturgut? Eisen verfügt über
Schnittstellen und Standards? Eisen kann ganz einfach im Internet
verteilt werden und erlaubt das Open-Source-Geschäftsmodell? ...
Gewisse Unterschiede kann man wohl doch nicht ganz verleugnen.

#tde: Und auch das selbstverständlich unterstellte Paradigma
%(q:Unterschiedliches muss unterschiedlich behandelt werden) sei in
Abrede gestellt: Ein Fahrrad ist sicher etwas anderes als ein
Lastkraftwagen. Aus guten Gründen aber gilt eine Ampel für beide in
gleicher Weise.

#erd: Spinnt man den Gedanken weiter, müsste z.B. das Urheberrecht auch auf
alle technischen Erfindungen Anwendung finden.

#Wnl: Wenn auf mögliche %(q:Überamortisationen) (heißt: wegen des
Patentschutzes erhält der Inhaber mehr, als er in seine Erfindung
investieren musste) hingewiesen und dies als verwerflich
charakterisiert wird, sei daran erinnert, dass es Ziel jedweder
Investition und allgemein jedweden Wirtschaftens ist, mehr
herauszuholen, als man hineinsteckt. Nicht Nullsummenspiel oder das
Draufzahlen stimulieren, sondern Gewinnerwartung. Wenn diese
Zielsetzung diffamiert wird, dann wird die Mehrwertschaffung insgesamt
in Frage gestellt.

#cGo: Hier wäre eine Quellenangabe nun wirklich hilfreich. Ich beschäftige
mich schon seit Jahren intensiv mit dem Thema, diese Argumentation ist
mir aber noch nicht untergekommen. Auch Google kann hier nicht
weiterhelfen, es werden weder Seiten mit dem Begriff %(q:
Überamortisation) noch mit %(q:Überamortisationen) - schon gar nicht
in Verbindung mit %(q:Softwarepatente) - gefunden.

#pzn: Wenn die einfache Verteilbarkeit von Software im Internet als
patentunzuträgliche Besonderheit bemüht wird, wird -- patentrechtlich
betrachtet -- die faktische Einfachheit der Rechtsverletzung als
Begründung zur Abschaffung des Rechts verwendet. Man möge sich
vorstellen, was analog geschehen würde, wenn der Chirurgenverband
forderte, Chirurgen seien von Kunstfehlerhaftung freizustellen, weil
ein Kunstfehler sehr leicht vorkommen könne.

#Wte: An dieser Stelle verwechselt PA Pfeiffer etwas grundlegend. Dieses
Argument wurde nicht von Softwarepatentgegnern verwendet, sondern von
-befürwortern, den Münchner PAen Esslinger & Betten. Siehe %(URL)

#ice: Warum die leichte Verteilbarkeit von Software allerdings für
Softwarepatente sprechen soll ist auch nicht ersichtlich. Wie sollte
das Patentrecht einem möglichen Missbrauch besser als das Urheberrecht
entgegenstehen?

#eee: Die unübertroffene Innovationskraft von Software wird
selbstverständlich behauptet, aber in seiner Pauschalität sei das
Statement hier trotzdem in Frage gestellt. Auch Hardware ist
außerordentlich innovativ, was jeder daran feststellen kann, dass er
sich spätestens alle vier oder fünf Jahre nicht nur neue Software
kaufen muss, sondern eben auch neue Hardware. Abgesehen davon ist
Software nicht zwingend immer außerordentlich schnell und innovativ.
Das Paradebeispiel für nicht innovative Software war das
Jahr-2000-Problem Ende 1999, verursacht durch immer noch laufende
Altsoftware, die vierstellige Jahresangaben nicht handhaben konnte.
Ein anderes Beispiel: Ein bekanntes Betriebssystem verwendete über
weit mehr als ein Jahrzehnt hinweg zuletzt jedenfalls noch intern
konstante Formate, beispielsweise das 8.3-Dateinamensformat, obwohl es
längst obsolet geworden war.

#ksr: Dass sich die Innovationszyklen auch bei Hardware immer mehr verkürzen
ist bestimmt kein Argument für eine Ausweitung des Patentrechts auf
Programmlogik. Fakt ist, dass kurze Innovations- und lange
Patentlaufzeiten nicht zusammenpassen. Trifft dies mittlerweile auch
auf den %(q:klassischen) Patentbereich zu, sollte man sich evtl.
überlegen, das Patentrecht an sich zu reformieren (abschaffen oder
zumindest die Laufzeiten anpassen - wobei mir klar ist, dass dies
theoretische Vorschläge sind, die von %(q:vorausschauenden Fachleuten)
durch das %(tr:TRIPS-Abkommen) weitgehend verbaut wurden).

#tna: Der groteske Versuch Pfeiffers, Software %(q:Innovationskraft)
abzusprechen um sie doch irgendwie kompatibel zum Patentrecht zu
reden, zeigt aber sehr schön die Hilflosigkeit der Softwarepatentlobby
auf - ganz offensichtlich fehlen echte Argumente.

#rBW: Und im Übrigen vernachlässigen die Verweise auf vermeintliche
Softwarebesonderheiten die Tatsache, dass Software ja regelmäßig nicht
die Erfindung ist. Software wird nur zum %(q:Bau) der Erfindung
benötigt, so wie man Eisen zum Bau eines erfundenen Zahnrades braucht,
siehe auch oben 2.1. Es gehen dann Verweise auf Softwarebesonderheiten
der Erfindungen schon dem betrachteten Gegenstand nach ins Leere, man
redet an der Sache, nämlich an den real vorliegenden Erfindungen,
vorbei.

#re2: Wer Ansprüche auf abstrakte Ideen erhebt, erhebt auch Ansprüche auf
mögliche Implementierungen. Dr. Kiesewetter-Köbinger hat es 2001 sehr
schön %(kk:definiert):

#gkm: Zentrale Bedeutung meiner Definition %(q:Softwarepatent) kommt hier
dem Verletzungsfall zu. Nach meiner Ansicht ist ein

#aed: ... für Software, wird behauptet, und Patentschutz für
softwareimplementierbare Erfindungen bräuchte man deshalb nicht.
Soweit Software alleine zu betrachten ist, wird man dem obigen
Statement sicher zustimmen können. Nur ist es eben so, dass das
Patentrecht erfinderische Konzepte schützen will und diese Erfindungen
regelmäßig ? wie schon gesagt ? nicht Software sind. Zur Vermeidung
von Wiederholungen wird insoweit auf das Ende des vorherigen Punktes
verwiesen. Zum Schutz von Konzepten ist das Urheberrecht aber völlig
untauglich.

#Wei: Und? Warum sollte man so einen Logik-Konzeptschutz wollen? Schutz um
des Schutzes willen kann es ja wohl nicht sein. Leider wieder keine
Argumente.

#hnr: Das sehen die Kritiker offenbar ganz ähnlich, eine Google-Recherche
bringt genau null Fundstellen für dieses %(q:Zitat) (gemeint ist
natürlich %(q:Software ist untechnisch), eine Gegenprobe mit %(q:Alle
Iren haben rote Haare) ergab tatsächlich mehrere Treffer ;-) ).

#eli: Es mag ja durchaus sein, dass softwareimplementierbare Erfindungen mit
untechnischen Gegenständen stärker korrelieren als
metallimplementierbare Erfindungen dies tun. Dies ändert aber nichts
daran, dass die Fragen nach Technizität einerseits und nach Software
andererseits voneinander unabhängig und jeweils in Ansehen einer
konkreten Erfindung getrennt voneinander zu beantworten sind. Und
abermals gilt das Gleiche wie am Ende von 3.1 und 3.2 gesagt: Auch die
Frage der Technizität ist nicht in Ansehen einer Software zu
beurteilen, sondern in Ansehen einer Erfindung, die regelmäßig
jenseits der implementierenden Software liegt. Die Frage nach
Technizität von Software ist im patentrechtlichen Kontext somit
eigentlich irrelevant. Ob sich in anderen Sphären jemand dafür
interessiert, weiß der Autor nicht.

#ems: Ein Anspruch auf eine Idee, die in reine Software umgesetzt werden
kann, ist ein Anspruch auf Software als solche. Er bezieht sich somit
nicht auf eine %(q:Erfindung) im Sinne des Art 52 EPÜ.
%(q:Technizität) war dabei in der Vergangenheit ein anerkanntes
Abgrenzungsmerkmal (siehe z.B. die %(dp:BGH-Entscheidung
%(q:Dispositionsprogramm))). In letzter Zeit wird dieser Begriff aber
von den Patentämtern so inflationär ausgelegt, dass die begrenzende
Funktion faktisch nicht mehr vorhanden ist (was rechtlich den
Ausschluss von Operations- und Rechenregeln aus dem Patentschutz aber
nicht tangiert - die Begriffe %(q:Technik), %(q:technisch) oder
%(q:Technizität) findet man in Art. 52 EPÜ überhaupt nicht).

#tsi: Die Forderung der Softwarepatentgegner lautet deshalb: Wenn
%(q:Technizität) als Abgrenzungsmerkmal in den europäische Normen
enthalten sein soll, dann auch mit Sinngehalt, sprich mit einer klaren
Bereichsdefinition, wie sie z.B. der %(ep:Richtlinienvorschlag des
Europäischen Parlaments) leistet.

#WlW: Irgendwie kam die Zahl in die Welt, dass das EPA bisher 30.000 Patente
auf softwareimplementierbare Erfindungen erteilt habe.

#mth: Diese Zahl kam nicht %(q:irgendwie in die Welt) sondern geht auf
%(sn:Recherchen des FFII zurück)

#tit: BTW: Den Ausdruck %(q:softwareimplementierbare Erfindungen) gibt es
natürlich nicht wirklich, das ist eine Pfeiffersche Schöpfung, die
(wieder mit Google überprüft) nur er benutzt.

#hW6: Aus der Tatsache, dass Software ein Patent verletzen kann, wird
angesichts des Softwareausschlusses in Art. 52 EPÜ hergeleitet, dass
diese Patente illegal seien. Selbstverständlich wird bei diesem
Argument vorausgesetzt, dass Art. 52 auf den Schutzbereich eines
Patents zu lesen ist, obwohl Art. 52 doch den Erfindungsbegriff
definiert und demgegenüber der Schutzbereich ganz woanderes, nämlich
in Art. 64 EPÜ8 (Verweis) bzw. §§ 9ff. PatG definiert ist. Eine
Softwareausnahme ist dort nicht gemacht.

#Ksc: Das ist einfach hanebüchener Unsinn. Wenn etwas vom Patentschutz
ausgeschlossen ist, muss man sich über den Schutzbereich keine
Gedanken mehr machen.

#hAW: Gleichwohl wird ein Prima-facie-Widerspruch dazu benützt, die
Amtspraxis von EPA und DPMA als contra legem darzustellen. Nach
Auffassung des Autors verfängt dieses %(q:Argument) in hohem Maße und
trägt stark zu Skepsis und Vertrauensverlust gegenüber der bisherigen
Handhabung und den Akteuren bei.

#nnl: Die Skepsis und der Vertrauensverlust sind offenbar begründet, vor
allem wenn man sich die %(q:bisherige Handhabung) ansieht. Verboten
die Prüfungsrichtlinien der Patentämter anfangs noch klar die
Patentierung von Software, erlauben sie heute den Schutz trivialster
Algorithmen und Geschäftsmethoden - bei unveränderter Rechtslage.

#5tK: 3.5 Ängste schüren

#ekd: Das hört sich so an: %(q:Mozart hätte Stehgeiger werden müssen, wenn
Haydn ein Patent auf seine Musik bekommen hätte).
%(q:Softwareunternehmen werden in einen Rüstungswettlauf gedrängt,
bald haben wir amerikanische Verhältnisse). %(q:Siehe
patenting-art.com). Das Verfügungsverfahren von amazon.com gegen
Barnes & Noble in der Weihnachtszeit 2000 (?) aus dem One-klick-Patent
heraus war ein gefundenes Fressen insoweit. Genüsslich wird Buch
geführt darüber, wie sich in Amerika welche Unternehmen mit
Patentrechtsklagen überziehen. Routiniert und zuverlässig werden diese
Aspekte vorgebracht, angeführt und immer wieder betont, und trotz des
offenkundigen Widersinns wird dies gemäß der Strategie %(q:aliquid
semper haeret) sicher in bestimmtem Umfang verfangen.

#tgt: Worin soll dieser %(q:Widersinn) begründet sein? Mit der Unterstellung
einer nicht vorhandenen Offensichtlichkeit kann man leider keine
Argumente ersetzen. Diese substanzlose und polemische Kritik legt aber
nahe, dass es PA Pfeiffer ist, der darauf spekuliert, dass irgend
etwas hängen bleibt.

#ess: Dass die Gegner der grenzenlosen Patentierbarkeit recht professionell
arbeiten mag den Befürwortern Angst machen (nein, ich glaube nicht,
dass der Autor das mit %(q:Ängste schüren) gemeint hat ;-) ), spricht
jedoch bestimmt nicht gegen sie (die Gegner). Die Verwendung auch
provokativer Aussagen ist in meinen Augen angesichts der
Verharmlosungsrhetorik der Patentlobby legitim und nötig, um
Öffentlichkeit herzustellen.

#WWf: Ansonsten fehlen hier wieder einmal die Quellenangaben, soweit möglich
hole ich das aber gern für Herrn Pfeiffer nach:

#oat: Mozart:

#ssl: Rüstungswettlauf

#Wdl: keine passende Fundstelle, evtl. %(URL)

#nns: keine passende Fundstelle

#ecv: One-Click: evtl.

#Wtc: 3.6 Vermengte Betrachtung

#ern: Bei der Argumentation gegen die Patentierung softwareimplementierbarer
Erfindungen kommt es regelmäßig dazu, dass die unterschiedlichsten
Aspekte in einen Topf geworfen und zusammen betrachtet werden. In
einem Atemzug erfolgt die Betrachtung von Erfindung, Software,
Implementierung, Klarheit, Schutzbereich, Technizität und Trivialität,
und wenn in diesem Gebräu irgendetwas eigenartig erscheint, wird dies
zur Begründung dafür, dass Patente auf softwareimplementierbare
Erfindungen unpassend seien, verwendet. Auf diese Weise entstehen
unterschiedliche Permutationen von doch eigentlich immer wieder den
gleichen %(q:Argumenten). Unterschiedliche Rechtsfragen werden
vermengt dargestellt, die in handwerklich ordentlicher Weise
eigentlich getrennt behandelt werden müssten und dann auch
übersichtlich werden würden.

#lre: Ohne konkrete Beispiele und Zitate sind das nun auch wieder bloße
Unterstellungen, auf die ich ab hier nur noch hinweisen, nicht aber
eingehen werde.

#eta: Wie sich aber schon mehrfach gezeigt hat, behandelt PA Pfeiffer gerne
Dinge getrennt, die eigentlich zusammengehören. So will er - auch hier
wieder - eine abstrakte %(q:Erfindung) losgelöst von der (trotzdem
beanspruchbaren) Software sehen. Das ist unzulässig, da man so
überhaupt keinen Bereich mehr von der Patentierung ausschließen
könnte.

#ess2: 3.7 Übergehen der sonstigen Ausschlusskriterien

#2Wt: Es wird so getan, als ob der Softwareausschluss des Art. 52 EPÜ
%(q:die letzte Barriere vor der grenzenlosen Patentierbarkeit) sei,
und wenn die fiele, dann sei der Patentierung der menschlichen Denke
keine Grenze mehr gesetzt. Übersehen oder absichtlich übergangen wird
dabei zweierlei, nämlich erstens, dass der existierende
Softwareausschluss faktisch ins Leere läuft und somit keine
%(q:Filterwirkung) hat,

#Ksl: Das gebetsmühlenartige Wiederholen dieses Scheinarguments langweilt
nun doch etwas.

#asW: und zweitens, dass es wesentlich mächtigere Schranken als den
Softwareausschluss gibt, nämlich insbesondere die Beschränkung des
Patentfähigen auf Technisches und auf Erfinderisches. Diese Aspekte
gehen regelmäßig unter oder werden verschwiegen, und stattdessen wird
der schale Eindruck vermittelt, dass es gewisse Kreise gäbe, deren
Ziel die Patentierung von allem und jedem sei.

#eze: Wie wenig mächtig diese Schranken in der Realität sind, kann man ganz
deutlich an den schon erteilten Softwarepatenten des EPA und DPMA
erkennen.

#dnh: Falsch ist auch, dass diese Aspekte von den Gegnern der grenzenlosen
Patentierbarkeit verschwiegen werden, sie werden vielmehr äußerst
lebhaft diskutiert:

#Wni: Man sieht hier, dass dieser Umstand auch Herrn Pfeiffer sehr wohl
bekannt ist, er beteiligt sich ja selbst des Öfteren an den
Diskussionen. Was ihn ärgert ist wohl mehr das regelmäßige Fazit
dieser Gespräche: dass diese Schranken ihre Wirkung im Logikbereich
nur sehr bedingt entfalten.

#WEe: 3.8 Verschwörungstheorien

#PWE: Denjenigen, die zugunsten eines vernünftigen Patentschutzes auch für
softwareimplementierbare Erfindungen reden, werden entweder mangelndes
Verständnis oder finstere eigene Interessen unterstellt. Patentanwälte
wollen sich mit der EPÜ-Revision bzw. mit der EU-Richtlinie angeblich
ein neues Geschäftsfeld erschließen,

#LWM: Selbstverständlich setzen sich Leute wie PA Pfeiffer aus rein
altruistischen Motiven so aufopferungsvoll für etwas ein, das nach
allen (mir) bekannten Umfragen die überwiegende Mehrheit der
Betroffenen nicht haben will.

#ues: Amerika will Europa patentrechtlich so usurpieren, wie es das mit
Japan schon geschafft habe, usw..

#sWn: Ich weiß nicht, ob das wirklich jemand so formuliert hat; es gibt aber
Fakten, die die Grundaussage dieser These stützen (hier nur die
wichtigsten):

#ein: Die USA halten den ganz überwiegenden Teil der in Europa angemeldeten
Softwarepatente

#nWe: Der ursprüngliche Richtlinienentwurf der EU-Kommission stammt -
zumindest zu weiten Teilen - aus der Feder der BSA, einer Organisation
der amerikanischen Software-Großkonzerne

#oge: Die USA und vor allem ihre Handelsvereinigung USTR arbeiten ganz offen
gezielt in diese Richtun

#ssW: An sich sind diese Darstellungsarten subsidiär zu der unter 3.4
dargestellten vermeintlichen Unrechtmäßigkeit von Patenten auf
softwareimplementierbare Erfindungen, denn die dargestellten Szenarien
%(q:funktionieren) ja nur, wenn es tatsächlich richtig wäre, dass
Patente auf softwareimplementierbare Erfindungen bisher unzulässig
seien, was aber ja nicht der Fall ist. Zur Diskreditierung des
Standpunktes zugunsten eines vernünftigen Patentschutzes taugen solche
Szenarien aber allemal.

#riH: Was PA Pfeiffer nicht in den Kram passt existiert einfach nicht, was
er gerne haben möchte ist automatisch vernünftig, Gegenargumente und
entgegenstehende Meinungen sind von Haus aus Verunglimpfungen seiner
Position.

#20g: 3.9 %(q:270.000 Stimmen, 2.000 Firmen gegen Logikpatente)

#hvW: Die Richtliniengegner berufen sich auf eine hohe Zahl von
Unterstützern der eigenen Position. Dies wird regelmäßig angeführt und
zur Legitimation der eigenen Forderungen verwendet. Verifizierbar sind
diese Zahlen nicht. Verwendet werden sie aber gleichwohl, und es mag
sein, dass die hohen Zahlen durchaus den einen oder anderen
Parlamentarier beeindrucken.

#Uft: Ich kann nicht für den FFII sprechen, da die Unterstützer aber ihre
E-Mail-Adresse angeben müssen sind die Zahlen wohl durchaus
verifizierbar. PA Pfeiffer hat offensichtlich keinerlei Anhaltspunkte
für eine %(q:Schummelei); so ist es zumindest als schlechter Stil zu
betrachten, diese Zahlen öffentlich anzuzweifeln.

#ciW: Angemerkt sei noch, dass der Begriff %(q:Richtliniengegner) nicht
zutreffend ist. Die Gegner der unbegrenzten Patentierbarkeit sind
durchaus für eine Präzisierung der europäischen Rechtslage und
unterstützen den Richtlinienvorschlag des Europäischen Parlaments.

#iil: Beeindruckend in die entgegengesetzte Richtung sollten dann aber
zumindest auch die angeblich 30.000 erteilten Patente auf
softwareimplementierbare Erfindungen sein. Sieht man sie als
%(q:Abstimmung mit den Füßen) an, dann ist den genannten hohen Zahlen
eine durchaus gewichtige Zahl entgegenzusetzen, denn diese 30.000
%(q:Meinungsäußerungen) sind nicht durch billig abzuschickende E-Mails
oder effizient auf Messeständen (Cebit, Systems) in der
Open-Source-Halle eingesammelte Unterschriften zustande gekommen,
sondern durch ein viele Tausend Euro kostendes Verhalten.

#rcn: Das ist tatsächlich eine beeindruckende Zahl. Im angegebenen
Zusammenhang relativiert sie sich aber sehr schnell, wenn man sich die
Herkunft ansieht. Zum einen kommt der allergrößte Teil der Anmeldungen
aus den USA sowie aus Japan (die Vermutung liegt nahe, dass viele
davon auch noch Parallelanmeldungen sind). Zum anderen sind es
natürlich vor allem Konzerne, die diese teuren Patente anmelden. So
entfallen z.B. auf deutsche Anmelder nur etwa 2500 dieser Patente,
davon wiederum über 700 alleine auf die Siemens AG (gestützt auf die
vom FFII veröffentlichten Zahlen, Stand 2003).

#him: Und vermerkt sei hier auch, dass sich große IT-Verbände, z. B. in
Deutschland die Gesellschaft für Informatik (GI), für einen
vernünftigen Patentschutz und für eine Einordnung der
Softwareindustrie in herkömmliche ingenieursmäßige Sichtweisen
aussprechen.

#neM: Nicht verschweigen sollte man allerdings, dass diese Verbände nicht
unbedingt im Sinne ihrer Mitglieder sprechen. Bei der repräsentativen
Umfrage der Gesellschaft für Informatik stimmten fast zwei Drittel der
Mitglieder gegen die Patentierbarkeit von Software.

#1tK: 3.10 Mythos KMU

#nWe2: %(q:KMU) steht für %(q:kleine und mittlere Unternehmen). Seitens der
Gegner von Patenten auf softwareimplementierbare Erfindungen wird
angeführt, dass sich Patente angeblich besonders gegen kleine und
mittlere Unternehmen auswirken würden. Hier will man mit der
Davids-Position im David-gegen-Goliath-Szenario Sympathien sammeln.
Nach Auffassung des Autors ist es aber nüchtern betrachtet gerade
anders herum richtig: Lässt man sich auf die Sichtweise
David-gegen-Goliath ein, so hätte ein innovativer David mit einem oder
mehreren Patenten in der Hand gegen einen Goliath eine bessere Chance,
als wenn man sowohl dem David als auch dem Goliath die Patente aus dem
Repertoire streicht. Dann bliebe es rein auf dem Niveau des
Armdrückens am Markt, und da dürfte Goliath in der Regel die
wesentlich besseren Chancen, weil in jedem Fall den längeren Atem,
haben.

#iai: Es ist beeindruckend, mit welcher Mühelosigkeit PA Pfeiffer die
niederen Beweggründe der Gegenseite aufdeckt und mit welch treffenden
Metaphern er ihr klarmacht, wie falsch sie liegt. Ob es aber wirklich
so schlau wäre, mir einer Steinschleuder auf eine potentielle
Gerölllawine zu zielen?

#eqa: Im Übrigen unterscheiden die Gegner der Richtlinie durchaus zwischen
%(q:guten) KMUs und %(q:unguten) KMUs, was zeigt, dass es um die
Interessen von KMU's als solchen eigentlich gar nicht geht.

#Wle: Diese Logik würde wohl nicht einmal das EPA patentieren. Interessiert
hätte mich aber, von welcher Gruppe sich KMU %(q:als solche) aus
Pfeiffers Sicht abgrenzen, von den guten oder von den unguten?

#Wee: Die %(q:Guten) sind diejenigen, die selber programmieren, Software
schreiben, vielleicht auch innovativ sind, aber nichts patentieren.
Die %(q:Unguten) sind diejenigen, die zwar auch kleine Unternehmen
sind, softwareimplementierbare Erfindungen machen, diese aber
patentieren lassen und dann Lizenzen einfordern. Wenn solch ein KMU
als David gegen Goliath antritt und vielleicht auch noch einen Erfolg
errungen hat, wird das nicht als ein dem Patentsystem zu verdankender
Erfolg eines KMU gesehen, sondern das KMU ist ein ungutes KMU, und die
Sympathien liegen zwar nicht auf Seiten Goliaths, aber man hat doch
viel Einfühlungsvermögen dafür, dass Goliath sich über solche
wadelbeißenden KMUs ärgert ... wobei, um die Sache rund zu machen,
dieser Ärger nicht auf Goliaths Rechtsverletzung zurückgeführt wird,
sondern auf %(q:Softwarepatente), verbunden mit dem Hinweis, dass
demnächst sich wohl auch KMUs in dieser Weise über
%(q:Softwarepatente) würden ärgern müssen.

#vWt: Um das einmal in verständliche Worte zu fassen: Pfeiffer spricht hier
das Problem der so genannten Patentfreibeuter an:

#iur: Ein Patentfreibeuter ist ein Unternehmen, das sich des Patentrechtes
bedient, um mit den allgemeinen Geschäftssitten zuwiderlaufenden aber
formell legalen Mitteln Lizenzgebühren zu erpressen. Dabei handelt es
sich häufig um Unternehmen, die keine eigenen Produkte herstellen und
deren Belegschaft sich nur aus Anwälten rekrutiert. Da viele normale
Unternehmen sich aus rein defensiven Gründen Patente mit sehr breiten
Ansprüchen zulegen, schlägt die Stunde der Patentfreibeuter, wenn
solche Unternehmen insolvent werden. Deren Trivialpatente werden dann
aufgekauft und verwertet.

#Wsk: Dass ein solches Gebaren von den Gegnern der unbegrenzten
Patentierbarkeit abgelehnt wird und dass diese Ablehnung unabhängig
davon ist, ob KMU oder Konzerne betroffen sind, ist
selbstverständlich. Daraus aber einen Interessenkonflikt konstruieren
zu wollen ist einfach lächerlich.

#egp: Der Autor hat Software-Kleinunternehmen als Mandanten, deren eine
zeitkonsumierende Tätigkeit es ist, Geld von Investoren aufzutreiben.
Regelmäßig fordern diese, das im Betrieb entstehende %(q:Intellectual
Property), also auch die softwareimplementierbaren Erfindungen, in
vernünftigem Umfang abzusichern, und so geschieht es dann auch. Wenn
das nicht möglich wäre, wären Investoren zurückhaltender, denn warum
sollten Sie Geld in ein Unternehmen stecken, dessen erfinderische
Entwicklungen (weil sie das %(q:Pech) haben, softwareimplementierbar
anstatt z.B. metallimplementierbar zu sein) nicht gegen Übernahme
durch die Konkurrenz geschützt werden könnten?

#hnh: Software steht auch ohne Patente nicht ohne Schutz da. Erwähnt seien
hier nur Urheberrecht, Betriebsgeheimnis und Zeitvorsprung.

#eto: Das Argument, dass Patente Investoren ansprechen, ist trotzdem nicht
von der Hand zu weisen. Im Logikbereich ist es aber wahrscheinlicher,
dass potentielle Investoren durch ausufernde Patentvergabe eher
abgeschreckt werden. Wer engagiert sich schon gerne in einem Gebiet,
in dem die Investitionen ständig durch U-Boot-Patente bedroht werden?
Nutznießer einer Legalisierung von Softwarepatenten wären in diesem
Zusammenhang wohl hauptsächlich die oben angesprochenen
Patentfreibeuter. Da sie keine eigenen Produkte vertreiben, ist das
Risiko für Investoren überschaubar. Die Patente würden so zur
Wettbewerbsverzerrung beitragen.

#uKn: Nun, so ist es ja nicht, denn softwareimplementierbare Erfindungen
sind nicht a priori vom Patentschutz ausgeschlossen, siehe oben 2.1.
Aber wenn es so wäre, dann würde sich dies auch im Hinblick auf die
Kapitalversorgung von Kleinunternehmen gegen KMU's auswirken. Man tut
also auch unter diesem Aspekt den Software-KMU's keinen Gefallen, wenn
man sich pauschal gegen Patente auf softwareimplementierbare
Erfindungen ausspricht.

#pfc: Man würde den KMU erhebliche Kosten, die durch legale Softwarepatente
auf sie zukämen, ersparen: Recherchekosten, Lizenzkosten,
Rücklagenbildung für Rechtsstreitigkeiten, Erhöhter Aufwand, um
patentierte Verfahren zu umgehen... Auch mit stark sinkender Nachfage
nach ihren Produkten müßten sie rechnen, da kleine Unternehmen ihre
gewerblichen Kunden nicht ausreichend gegen Verletzungsansprüche
(%(q:Mitstörerhaftung)) absichern können.

#1he: 3.11 %(q:Ich wandere aus)

#nPs: Es wird eine Situation beschworen, in der man vor lauten Patenten
angeblich nicht mehr programmieren könne. Pathetisch wird dann der
Arbeitsplatzverlust angedroht und das Auswandern als die einzig
mögliche Alternative beschworen.

#elc: Über solche Aussagen sollte sich Herr Pfeiffer nicht herablassend
äußern, sondern einmal ernsthaft über die Ursachen nachdenken. Sie
sind Ausdruck echter Sorge um die eigene Existenz.

#oWW: Abgesehen davon, dass man damit dem sich territorial erstreckenden
Schutz von Patenten nicht entkommt und weiter abgesehen davon, dass
sich der Patentschutz ja gerade nicht auf Forschung und Entwicklung
erstreckt, ist es dann umso erstaunlicher, dass man bei der Frage an
ein Auditorium von vielen Hundert Rezipienten dahingehend, wer denn
schon einmal wegen eines Patentes etwas unterlassen haben müsse oder
wer auch nur ein einziges Mal wenigstens vorsichtshalber eine
Patentrecherche gemacht habe, eine Null-Antwort bekommt.

#mni: Das zeigt nur, dass kaum jemand PA Pfeiffers Meinung teilt, Software
wäre schon heute in Europa rechtmäßig patentierbar - offensichtlich
nicht einmal die Patenthalter.

#wWg: Man erhält dann nur Antworten dahingehend, dass man sich Sorgen mache
und dass man ja wohl (insbesondere amerikanischen Verhältnissen)
vorbeugend tätig werden dürfe.

#cgh: In meinen Augen eine realistische Sicht der Lage. Es geht jetzt darum,
die nachträgliche Legalisierung zehntausender trivialer
Softwarepatente zu verhindern und die Neugewährung für die Zukunft
unmöglich zu machen.

#swW: 3.12 Konsequenzen in Europa wegen amerikanischer Verhältnisse

#egW: Mit großer Selbstverständlichkeit werden seitens der Richtliniengegner
tatsächliche oder vermeintliche Verhältnisse in Amerika zur Begründung
ihrer Anliegen in Europa verwendet. Dies geschieht entweder
unmittelbar, oder etwas ausgefeilter im Hinblick auf Prävention
amerikanischer Verhältnisse.

#naw: Das ist nicht besonders verwunderlich, angesichts der Tatsache, dass
die Forderung nach Softwarepatenten auch mit einer angeblich durch die
Globalisierung notwendig werdenden Harmonisierung begründet wird.

#eae: Der Ansatz ist aber nach Auffassung des Autors insoweit falsch, als
%(q:amerikanische Verhältnisse), soweit sie denn tatsächlich
bedenklich sein sollten, sicher nicht in der Tatsache liegen, dass das
US-Recht keinen Software-Ausschluss in seinen Patentregelungen hat.
Wenn man dem amerikanischen Rechtssystem skeptisch gegenübersteht,
dann ist das häufig aus der zu beobachtenden Prozessfreude und gerne
berichteten %(q:realen) Absurditäten herzuleiten, hat aber mit den
Softwareregelungen nichts zu tun. Insofern sind dann auch
Schlussfolgerungen zu europäischen Softwarediskussionen abwegig.

#WdA: Hier stellt sich natürlich die Frage, inwieweit die Möglichkeiten, die
das amerikanische Rechtssystem bietet, für die Prozessfreudigkeit und
die uns absurd anmutenden Entscheidungen verantwortlich sind. Das
Fehlen eines Software-Ausschlusses in den US-Patentgesetzen machte es
ja erst möglich, dass Eolas über 500 Millionen Dollar für die
Verletzung eines Trivialpatentes zugesprochen bekam. Mit einer
wirksamen Ausschlussregelung hätte es diesen Prozess und das absurde
Urteil nie gegeben. Die von PA Pfeiffer diagnostizierte Abwegigkeit
ergibt sich, wenn überhaupt, nur aus seiner Verwechslung von Ursache
und Wirkung.

#nEn: 3.13 %(q:Linux hätte nicht entstehen können, wenn es Softwarepatente
gegeben hätte)

#jal: Das %(q:Argument) ist grundfalsch, denn seine Prämisse ist falsch: Es
gibt Patente auf softwareimplementierbare Erfindungen, angeblich sogar
30.000 davon in Europa (siehe 3.4), und Linux und andere
Open-Source-Produkte existieren trotzdem sehr unbeschwert. Und nicht
nur existieren sie in Europa, sondern auch in den USA, die den
angeblichen %(q:Sündenfall) zu Softwarepatenten schon hinter sich
haben. Für Europa wird der Widerspruch quasi-juristisch klingend, aber
fachlich völlig falsch damit erklärt, dass die Rechtslage,
insbesondere der Softwareausschluss in Art. 52 EPÜ, die
Durchsetzbarkeit der fraglichen Patente unmöglich mache, so dass die
Patentinhaber dieser Tag (noch) alle kollektiv stillhielten. Eine
Antwort auf die Frage, wie sich der Widerspruch für die USA löse, wo
es ja keinen Softwareausschluss entsprechend Art. 52 gibt, erhält man
nicht mehr.

#lzn: Dann sollte PA Pfeiffer z.B. einmal hier nachsehen:

#dnu: 3.14 Reduktion von Patenten auf Verbietungsrechte

#WWl: Seitens der Gegner der Richtlinie wird die durch das Patentrecht
vorgenommene Interessensgestaltung darauf reduziert, dass
ausschließlich der Verbietungsaspekt dargestellt wird. Sicherlich ist
dieser vorhanden, und er wird durchaus gelegentlich Unternehmen in
ihren gewerblichen Vorhaben behindern.

#nrg: Von einer Reduktion auf diesen Punkt kann natürlich keine Rede sein,
er ist aber angesichts des sequentiellen Charakters von Software
besonders gravierend. Neue Innovationen bauen hier immer auf
vorausgegangene auf. Schutzrechte auf grundlegende Ideen können so den
Wettbewerb großflächig aushebeln.

#lui: Patente haben aber auch Anreiz-, Belohnungs- und Sicherungsaspekte und
damit eine markante entwicklungsfördernde Wirkung, wie dies vor 3.1
weiter oben dargestellt wurde. Diese Aspekte werden regelmäßig nicht
erwähnt, sie werden schlicht übergangen.

#rre: Diese Aspekte werden nicht übergangen, ihre innovationsfördernde
Wirkung wird in Frage gestellt, da es für die Richtigkeit dieser These
keinerlei empirische Belege gibt. Gerade im Softwarebereich sind aber
eher negative ökonomischen Wirkungen auszumachen.  Einigen
%(ss:Studien) zufolge hat sich in den USA die F&E-Intensität nach der
Einführung von Softwarepatenten erheblich verringert.

#cpW: 3.15 %(q:Softwarepatente sind trivial)

#hGi: Die Nennung dieses Arguments erfolgt mit Absicht am Ende der
bisherigen Aufzählung, denn der Autor misst ihm noch am ehesten einen
Kern möglicher Relevanz bei. In jedwedem der oben angeführten Kontexte
wird ausdrücklich oder implizit deutlich, dass man den Gegenstand von
Patenten auf softwareimplementierbare Erfindungen jedenfalls in den
allermeisten Fällen für trivial und das Patent deshalb für zu Unrecht
erteilt hält. Diese Sichtweise schürt einen wesentlichen Teil der
Empörung gegen Patente auf softwareimplementierbare Erfindungen.

#Wrd: PA Pfeiffer vergisst, dass er in dieser Diskussion fast durchweg
Fachleuten und Praktikern gegenübersteht, welche die Innovationshöhe
dieser %(q:Erfindungen) sehr wohl einschätzen können.

#tef: Ein Teil dieser Auffassung ist sicherlich Ex-post-Betrachtungen
zuzuschreiben. In Ansehen eines bestimmten Patents wird dessen Gehalt
ausgehend von heutigem Wissen als trivial angesehen. Gelegentlich
handelt es sich aber bei den betrachteten Patenten um US-Patente mit
frühen Anmeldetagen, die wegen der ehemals anderen
Laufzeitbestimmungen in den USA heute noch in Kraft sind, nicht aber
in Europa.

#swt: Man muss sich ja nur ansehen, was auch in Europa patentiert wird:

#zee: Die meisten dieser Patente beziehen sich auf trivialste und
altbekannte Abläufe und Methoden, die Neuheit liegt nur in der
Umsetzung in Software (Adobes Karteikartenreiter-Patent ist ein
Musterbeispiel).

#enn: Damit lassen sich aber nicht alle Trivialitätswahrnehmungen erklären.
Klassischer Fall hier war abermals das One-klick-Patent von Amazon (US
5 960 411), das -- verkürzt dargestellt -- den Gedanken schützt, dass
man bei Käufen im Internet ausgehend von einer Produktdarstellung auf
dem Bildschirm einen Kauf durch lediglich einen einzigen Mausklick
bewirken kann anstatt mehrerer, wie es vorher üblich war.

#ncG: Genau für diesen Zweck wurden Cookies entwickelt - allerdings nicht
von Amazon, sondern von Lou Montulli (damals Entwickler bei Netscape)
als Teil der HTML-Spezifikation. Amazon hat sich also praktisch den
bestimmungsgemäßen Gebrauch einer freien Standardtechnik schützen
lassen.

#lte: Das zitierte One-klick-Patent ist aber beileibe nicht das einzige, das
Bauchgrimmen verursacht. Fast jedes Patent wird als trivial
bezeichnet, und man kann sogar Bemühungen dahingehend beobachten, für
softwareimplementierbare Erfindungen eine inhärente, systemnotwendige
Trivialität zu begründen.

#oet: In seinen blumigen Auswüchsen kann man diese Argumentationslinie
sicherlich nicht ernst nehmen, zumal sie sich ja offensichtlich mit
dem auch sehr routiniert vorgetragenen, weiter oben bei 3.1 schon
gestreiften Statement beißt, wonach die Softwarebranche angeblich ganz
außerordentlich innovativ sei, was sich mit notorischer Trivialität
der dabei entstehenden Erfindungen nicht gut verträgt.

#end: Das passt sehr wohl zusammen. Bei Software gibt es keine abrupten
Innovationssprünge, die oben festgestellte Dynamik begründet sich aus
den ausgesprochen kurzen Zeiträumen zwischen den einzelnen
Entwicklungsschritten. Ein Umstand, dem das schwerfällige Patentwesen
niemals gerecht werden kann.

#eno: Zudem liegt es natürlich im Interesse der Patentanmelder, sich
möglichst breite Ansprüche auf möglichst elementare Ideen zu sichern.
So existiert z.B. nicht etwa ein Patent auf das wirklich innovative
MP3-Verfahren, die Entwickler haben sich eine Vielzahl von Patenten
auf einzelne Rechenregeln gesichert. Siehe %(URL)

#Tcu: Gleichwohl aber soll hier gefragt werden, ob der Vorwurf der
Trivialität nicht einen wunden Punkt berührt. Wenn man angesichts des
Vorwurfs auf die Bestimmungen im Patentgesetz dahingehend, dass
erfinderische Tätigkeit eine der Patentfähigkeitsvoraussetzungen sei,
hinweist, erntet man Hohngelächter, verbunden mit der Feststellung,
dass das Kriterium ausgedient habe und es letztendlich nicht oder
nicht ordentlich angewendet werde.

#KdW: Nach Auffassung des Autors kann letzteres nicht in jedem Fall verneint
werden. Wenn die Richtliniengegner sich als in einem %(q:Gestrüpp von
Trivialpatenten stehend) darstellen, dann ist dies sicherlich Rhetorik
und übertrieben. Wenn aber festgestellt wird, dass durchaus auch
Banales oder Obskures patentiert wird, dann ist dies nicht in jedem
Fall zu verneinen, im Übrigen auch auf Feldern jenseits
softwareimplementierbarer Erfindungen. An dieser Stelle ist nach
Auffassung des Autors durchaus Aufmerksamkeit angebracht: Es dürfte
Common Sense sein, dass die Patentierung von Trivialitäten nicht
erwünscht ist, denn ein Filz von Patenten auf einfach abgewandelte
Gegenstände würde nicht mehr unter die Rechtfertigung des
Patentschutzes im Allgemeinen fallen, ja würde sogar das Gegenteil
bewirken, nämlich ökonomisch nicht wünschenswerte und
ungerechtfertigte Behinderungen und eine Verrechtlichung des
wirtschaftlichen Bereichs.

#sWW: Triviale Softwarepatente sind schon heute nicht die Ausnahme, sondern
die Regel. Dass das nicht von ungefähr kommt, zeigt z.B. Peter
Mühlbauer in einem %(ta:Telepolis-Artikel) auf:

#aeW: Wie kam es zu dieser Inflation? In Firmen wie Hewlett Packard und
Siemens finden %(q:Erweckungs-Sitzungen) statt, bei denen Entwickler
%(q:lernen) dass Ideen, die sie für trivial halten, vom Patentamt als
Erfindungen angenommen werden. Auf solche Schulungen folgen regelmäßig
Hunderte von Patentanmeldungen. Auf diese Weise konnte HP seinen
Patentausstoß in kurzer Zeit auf 5000 Anmeldungen pro Jahr steigern.

#Snr: Bedacht werden muss auch, dass in einer Umgebung, in der
Softwarepatente erlaubt sind, bereits die Anmeldung von trivialen
Ideen ausreicht, um als Drohpotential gegen kleinere Wettbewerber
eingesetzt zu werden. Das Risiko, dass sie nach dem langwierigen Prüf-
und Erteilungsverfahren (das auch noch sehr leicht verschleppt werden
kann) tatsächlich erteilt werden, ist für die meisten KMU einfach zu
groß.

#oev: Die Legalisierung von Softwarepatenten könnte so eine destruktive
Wirkung auch dann entfalten, wenn es tatsächlich gelingen sollte,
wirksame Qualitätsprüfungen im Erteilungsverfahren zu etablieren.
Davon ist aber auch nicht auszugehen, Lippenbekenntnissen der
Exekutive in diese Richtung stehen keinerlei konkrete Ansätze
gegenüber.

#Leh: Auf der Jahrestagung der %(q:American Intellectual Property Law
Association) (AIPLA) Ende Oktober 2003 redete der Vorsitzende der
%(q:Federal Trade Commission) (FTC), T. J. Muris, sehr direkt zu genau
dieser Problematik, die er mit %(q:questionable patents) umschrieb.
Seitens der FTC werden Abhilfen als notwendig angesehen, und insoweit
machte sie auch bestimmte Vorschläge, die man aber tendenziell als
eher verfahrenstechnisch charakterisieren kann. Gleichwohl ist
festzuhalten, dass in den USA Regierungsstellen insoweit
Handlungsbedarf sehen und Vorstöße machen.

#sen: In den USA können kurzfristig wohl nur schadensbegrenzende Maßnahmen
Anwendung finden, in Europa haben wir aber noch die Möglichkeit, den
grundlegenden Fehler nicht zu begehen.

#obn: Prof. Albert Endres

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: pfeiffer04 ;
# txtlang: de ;
# multlin: t ;
# End: ;

