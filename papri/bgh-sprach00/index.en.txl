<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: BGH 2000-05-11: Sprachanalyseeinrichtung

#descr: Language Analysis is patentable if something tangible is mentioned in
the claims.  Two years after the Street Decision, the Federal Court's
Patent Senate declares exclusions of statutory subject matter
meaningless, decrees that if not anything under the sun then at least
any idea that is claimed in connection with a programmable device is
patentable:  %(q:A device that is program-technically configured in a
certain way has a technical character ... For the evaluation of the
technical character of such a device it is not relevant whether a
(further) technical effect is achieved, whether the technical field is
enriched by it or whether it makes a contribution to the state of
technology.

#B0K: Beschl. v. 11.5.2000

#Eru: Einer Vorrichtung (Datenverarbeitungsanlage), die in bestimmter Weise
programmtechnisch eingerichtet ist, kommt technischer Charakter zu.
Das gilt auch dann, wenn auf der Anlage eine Bearbeitung von Texten
vorgenommen wird.

#Fhi: Für die Beurteilung des technischen Charakters einer solchen
Vorrichtung kommt es nicht darauf an, ob mit ihr ein (weiterer)
technischer Effekt erzielt wird, ob die Technik durch sie bereichert
wird oder ob sie einen Beitrag zum Stand der Technik leistet.

#Dtu: Dem technischen Charakter der Vorrichtung steht es nicht entgegen, daß
ein Eingreifen des Menschen in den Ablauf des auf dem Rechner
durchzuführenden Programms in Betracht kommt.

#Iid: I. Die Anmelderin hat am 17. Mai 1990 unter Inanspruchnahme der
Priorität einer Voranmeldung in Japan beim Deutschen Patentamt unter
der Bezeichnung »Dialog-.Analyseeinrichtung für natürliche Sprache«
ein Patent angemeldet. Die Prüfungsstelle G 1 OL des Deutschen
Patentamts hat die Anmeldung zurückgewiesen. Hiergegen hat die
Anmelderin Beschwerde eingelegt. Im Laut des Beschwerdeverfahrens hat
die Anmelderin zuletzt beantragt, ein Patent mit folgender Fassung des
Patentanspruchs 1 zu erteilen:

#SiW: Sprachanalyseeinrichtung vom Dialogtyp mit:

#eWi: einer Satzeingabeeinrichtung, die der Eingabe eines zu analysierenden
Textes in einer Sprache dient, wobei ein Satz des Textes aus
syntaktischen Einheiten besteht,

#eib: einer Wörterbucheinrichtung (41, in der syntaktische Einheiten
gespeichert sind, und der Attribute für syntaktische Einheiten
entnehmbar sind,

#eWn: einer Grammatikeinrichtung (5), die die für die Sprache des Textes
möglichen linguistischen Beziehungen zwischen syntaktischen Einheiten,
denen jeweils ein Attribut zugeordnet ist, bereitstellt, wobei der
Inhalt der Wörterbucheinrichtung (4) und der Grammatikeinrichtung (5)
in einem Speicher gespeichert ist,

#erl: einer Feststelleinrichtung (2), die mittels der Wörterbucheinrichtung
den Satz in syntaktische Einheiten aufteilt und für jede syntaktische
Einheit mögliche Attribute feststellt und mittels der
Grammatikeinrichtung anhand der als möglich erkannten Attribute alle
möglichen linguistischen Beziehungen zwischen den Attributen, die
jeweils einer syntaktischen Einheit zugeordnet sind, feststellt, wobei
jede auf diese Weise festgestellte mögliche linguistische Beziehung
zwischen den syntaktischen Einheiten des Satzes eine
Kandidatenbeziehung darstellt, die möglicherweise korrekt ist, und

#ehe: eine Dialog-Auswahleinrichtung (9), mit der im Dialog mit einem
Benutzer, wenn für eine syntaktische Einheit mehr als eine
Kandidatenbeziehung möglich ist, eine korrekte Beziehung aus den
Kandidatenbeziehungen basierend auf einer Befehlseingabe von einer
Betriebseinheit ausgewählt werden kann,

#gze: gekennzeichnet durch

#enr: einen Bewertungsblock (8), der die Kandidatenbeziehungen dahingehend
bewertet, ob sie eine höhere oder geringere Wahrscheinlichkeit haben,
korrekt zu sein, und durch

#eeB: eine Bevorzugungs-Analyseeinrichtung (10), die, wenn für mehrere
Kandidatenbeziehungen keine klärende Auswahl über die
Dialog-Auswahleinrichtung getroffen wurde, die durch den
Bewertungsblock als wahrscheinlichste bewertete Kandidatenbeziehung
als korrekt auswählt.

#WrB: Wegen der weiteren Patentansprüche wird auf die Akten des
Beschwerdeverfahrens verwiesen.

#DWA: Das Bundespatentgericht hat die Beschwerde zurückgewiesen; die
Entscheidung ist in BPatGE 40, 62 und Mitt. 1998, 473 veröffentlicht.
Mit ihrer zugelassenen Rechtsbeschwerde begehrt die Anmelderin die
Aufhebung des angefochtenen Beschlusses und die Zurückverweisung der
Sache an das Beschwerdegericht.

#Deg: Die kraft Zulassung statthafte und auch im übrigen zulässige
Rechtsbeschwerde hat Erfolg.

#Dzi: Das Bundespatentgericht hat die Patentfähigkeit des Gegenstands des
zur Entscheidung gestellten Patentanspruchs 1 verneint, weil dieser
nicht auf einer technischen Leistung beruhe. Aus dem Umstand, daß der
Patentanspruch auf eine Einrichtung (Vorrichtung) bezogen sei, ergebe
sich nicht schon, daß ein Gegenstand dem Kreis der patentfähigen
Erfindungen zuzurechnen sei. Ein Gegenstand, der technische und
nichttechnische Aspekte umfasse, gebe jedenfalls dann eine
patentfähige Erfindung an, wenn er einen Beitrag zum Stand der Technik
enthalte und dieser Beitrag auch die weiteren
Patentierungsvoraussetzungen erfülle. Der Beitrag, mit dem sich der
Gegenstand des Patentanspruchs 1 von bekannten
Sprachanalyseeinrichtungen unterscheide, bestehe in der Lehre, eine
Satzkonstruktion durch einen Bewertungsblock auf möglicherweise
korrekte Kandidatenbeziehungen zu bewerten und durch eine
Bevorzugungs-Analyseeinrichtung die Kandidatenbeziehung mit der
höchsten Wahrscheinlichkeit auswählen zu lassen, sofern die Auswahl
nicht vom Benutzer selbst vorgenommen werde. Hierzu habe zunächst ein
Sprachwissenschaftler die Wahrscheinlichkeit für die einzelnen
Interpretationsmöglichkeiten festlegen müssen; dabei handle es sich um
eine nichttechnische Leistung.   Die Anmelderin begehre allerdings
Schutz für eine Einrichtung, die selbsttätig nach bestimmten
grammatikalischen Erkenntnissen arbeite. Die dazu erforderliche
Umsetzung der nichttechnischen Erkenntnisse in eine technische
Einrichtung sei Sache eines Datenverarbeitungsfachmanns. Die von
diesem zu erbringende Leistung habe lediglich in der Erstellung eines
Grammatikanalyseprogramms bestanden, das sodann in eine übliche
Datenverarbeitungseinrichtung zu laden und von dieser auszuführen
gewesen sei.  Dies bewege sich im Rahmen üblichen fachmännischen
Handelns und bereichere den Stand der Technik nicht. Eine andere
technische Leistung sei bei der Umsetzung der nichttechnischen
Erkenntnisse in eine technische Einrichtung mit entsprechender
Arbeitsweise weder erkennbar noch erforderlich.  In der
erfindungsgemäßen Satzanalyse und Zuordnung von Wahrscheinlichkeiten
zu bestimmten Satzkonstruktionen Liege auch keine Aufeinanderfolge von
technischen Einzelmaßnahmen, die das zugrundeliegende Programm zu
einem patentfähigen %(q:technischen Programm), mache.  Eine
Komprimierung des Texts erfolge allenfalls nach grammatikalischen und
damit untechnischen Gesichtspunkten. Die beanspruchte
Sprachanalyseeinrichtung lehre schließlich keine neue Brauchbarkeit
einer Datenverarbeitungseinrichtung.

#Dse: Die Rechtsbeschwerde greift die Auffassung des Bundespatentgerichts
an, daß der Gegenstand des Patentanspruchs 1 der im Streit stehenden
Anmeldung nicht technisch sei. Zwar habe das Bundespatentgericht
zutreffend gesehen, daß dieser Gegenstand als Vorrichtung nicht von
den Patentierungsausschlüssen nach § 1 -,? bs. 2 PatG erfaßt werde.
Die mit der Lehre des Streitpatents verbundene Informationsreduktion
(Entropieverminderung) erfordere auch eine aktive, energieaufwendige
Verarbeitung durch die Einrichtung, z. B. einen Computer. Eine
selbständig arbeitende, zur Umsetzung der technischen Lehre
energieverbrauchende Einrichtung sei technisch.

#Dee: Die Rechtsbeschwerde beanstandet weiter die Auffassung des
Bundespatentgerichts, daß zunächst die nicht technische Leistung eines
Sprachwissenschaftlers erforderlich sei, dem es bekannt sei, bei
Fehlen einer eindeutigen Analyse des Sinngehalts auf statistische
Wahrscheinlichkeit zurückzugreifen, und dessen Erkenntnisse in eine
technische Einrichtung umgesetzt werden müßten, Ein unsinniger Satz
habe keine statistische Wahrscheinlichkeit der Richtigkeit. Selbst
wenn dem Sprachwissenschaftler die Wahrscheinlichkeitsberechnung
naheliege, fehle jede Anregung, diese nur dann zur automatischen
Auswahl zu verwenden, wenn keine Auswahl mittels der
Dialog-Auswahleinrichtung getroffen werde.

#Sil: Schließlich meint die Rechtsbeschwerde, das Bundespatentgericht habe
in unzulässiger Weise Argumente aus dem Bereich der erfinderischen
Tätigkeit mit solchen aus dem Bereich der Technizität vermischt sowie
die technische Umsetzung verkannt, die einen technischen Kompromiß
zwischen einer optimalen Textanalyse durch reinen Dialogvorgang und
einer vollautomatischen Textanalyse verkörpere und damit zu einem
suboptimalen Arbeitsergebnis führe.

#Dbn: Die auf eine Verletzung der Bestimmung des § 1 PatG gestützten Rügen
der Rechtsbeschwerde erweisen sich im Ergebnis als begründet. Die
Beurteilung des Bundespatentgerichts, mit der dieses das Vorliegen
einer Lehre zum technischen Handeln als Element des Erfindungsbegriffs
verneint hat, hält einer rechtlichen Überprüfung nicht stand.

#Dee2: Die im Streit stehende Patentanmeldung betrifft nach ihrem
Patentanspruch eine Sprachanalyseeinrichtung mit bestimmten
Bestandteilen, die in diesem Patentanspruch im einzelnen aufgeführt
sind.  Wie das Bundespatentgericht festgestellt hat, handelt es sich
um eine Einrichtung, die unter Versendung einer üblichen
Datenverarbeitungsanlage verwirklicht werden kann, wobei der
Bewertungsblock und die Bevorzugungsanalyseeinrichtung sowohl durch
Hardware als auch durch Software realisiert werden können.  Diese
Anlage benötigt weiter eine Eingabeeinrichtung und eine
Anzeigeeinrichtung. Die übrigen Merkmale beschreiben die funktionellen
Mittel, mit denen die Textbearbeitung zu realisieren ist, wobei auf
einer bestimmten Ebene von einer Bedienperson eine Auswahl zwischen
verschiedenen Möglichkeiten erfolgen kann und andernfalls die Auswahl
nach einem bestimmten Algorithmus ohne Zutun der Bedienperson
getroffen wird. Dies zieht auch die Rechtsbeschwerde nicht in Zweifel.

#NbW: Nach den Feststellungen des Bundespatentgerichts ist das
Patentbegehren mithin auf eine Vorrichtung (Datenverarbeitungsanlage,
die in bestimmter, näher definierter Weise programmtechnisch
eingerichtet ist, und nicht auf ein Verfahren oder ein Programm
gerichtet. Einer derartigen Vorrichtung kommt entgegen der Auffassung
des Bundespatentgerichts der erforderliche technische Charakter ohne
weiteres zu.

#Ela: Es entspricht gefestigter Rechtsprechung des Senats zum geltenden
nationalen wie europäischen Recht, daß Patentschutz nur für
Erfindungen auf dem Gebiet der Technik gewährt wird (BGH Z 115, 23i,
30-Chinesische Schriftzeichen; Sen.Beschl. v. 13. 12. 1999' - X ZB
11/98 - Logikverifikation, Umdruck S.10 f., zur Veröffentlichung in
BGHZ vorgesehen; vgl. BGHZ 117, 144', 148 t. -Tauchcomputer). Der
Begriff der Technik im patentrechtlichen Sinn ist im Gesetz nicht
näher definiert und entzieht sich als der Abgrenzung des durch die
technischen Schutzrechte Schutzfähigen dienender Rechtsbegriff einer
eindeutigen und abschließenden Festlegung. Er hat vielmehr eine
Wertung (vgl. hierzu Sen.Beschl. Logikverifikation, a. a. O., S. 12
f.) bezüglich dessen zur Voraussetzung, was technisch und deshalb dem
Patentschutz zugänglich sein soll. Damit knüpft er jedenfalls auch an
dem Verständnis an, das den Begriff der Technik herkömmlich ausfüllt.
Hierzu rechnet ohne weiteres eine industriell herstellbare und
gewerblich einsetzbare Vorrichtung, zu deren Betrieb Energie
eingesetzt (:,verbraucht«) wird und innerhalb derer unterschiedliche
Schaltzustände auftreten, wie dies bei einem Universalrechner, aber
ebenso bei einer besonders konfigurierten Datenverarbeitungsanlage der
Fall ist (vgl. zum technischen Charakter einer solchen Anlage schon
BGHZ 67, 225, 27 f. -Dispositionsprogramm: BGHZ 11 7, 144, 149 -
Tauchcomputer; BPatG GRUR 1999, 1078 ff. = Mitt. 2000, 33 ff. -
%(q:automatische Absatzsteuerung),: weiter Melullis; GRUR 1998. 843,
848, 850). Daß der Rechner in bestimmter Weise programmtechnisch
eingerichtet ist, nimmt ihm nicht seinen technischen Charakter,
sondern fügt ihm als technischem Gegenstand lediglich weitere
Eigenschaften hinzu, auf deren eigenen technischen Charakter es für
die Beurteilung des technischen Charakters der Anlage als solcher
nicht ankommt. Daß eine Datenverarbeitungsanlage als solche
technischen Charakter aufweist, ist zudem soweit ersichtlich ernstlich
nirgends in Zweifel gezogen worden. Die Diskussion um die Technizität
bezieht sich im wesentlichen auf Programme, die auf solchen Anlagen
ablaufen und auf Verfahren die mit ihnen durchgeführt werden. Darum
geht es hier nicht.

#Awa: Aus diesem Grund und weil es nicht uni die Anwendung des
Patentierungsausschlusses für Programme für Datenverarbeitungsanlagen
als solche (§ 1 Abs. 2 Nr. 3, Abs. 3 PatG) geht, kommt es im
vorliegenden Fall auch nicht darauf an ob, wie es das Europäische
Patentamt bei programmbezogenen Erfindungen als erforderlich ansieht
(EPA T 1173/97 ABl. EPA 1999, 609, 620 t. = GRUR Int. 1999, 1053 -
Computerprogrammprodukt/IBM), ein weiterer technischer Effekt erzielt
wird, der über eine %(q:normale) physikalische Wechselwirkung zwischen
dem Programm und dem Computer hinausgeht (vgl. Melullis, a. a. O., S.
850), Erst recht kann es für die Beurteilung des technischen
Charakters der beanspruchten Anlage nicht darauf ankommen, ob diese
die Technik bereichert oder ob sie einen Beitrag zum Stand der Technik
leistet. Auch einer bekannten Vorrichtung, die an sich technisch ist,
kann deswegen, weil sie der Technik nichts hinzufügt, nicht der
technische Charakter abgesprochen werden. Eine Prüfung, ob eine
Bereicherung der Technik eintritt oder ob ein Beitrag zum Stand der
Technik geleistet wird ist allenfalls und erst bei der Prüfung der
Schutzfähigkeit am Platz, soweit der Wegfall des
Patentierungserfordernisses des technischen Fortschritts für sie
überhaupt noch Raum läßt.

#EeS: Es bedarf keiner Prüfung der Frage, ob die der Anmeldung
zugrundeliegende Lehre dann als nichttechnisch oder wegen Verstoßes
gegen das Patentierungsverbot in § 1 Abs. 2 Nr. 3 PatG vom Schutz
ausgeschlossen wäre, wenn sie als Verfahrensanspruch oder in Form
eines Programms beansprucht wäre.  Ein generelles Verbot der
Patentierung von Lehren, die von Programmen für
Datenverarbeitungsanlagen Gebrauch machen, besteht, wie sich schon im
Umkehrschluß aus der Regelung in §1 Abs. 2 Nr. 3. Abs. 3 PatG und der
parallelen Regelung in Art. 52 EPÜ ergibt, nach denn Gesetz jedenfalls
nicht; dies wird nunmehr durch Artikel 27 des Abkommens über
handelsrelevante Aspekte des geistigen Eigentums (TRIPs) bestätigt und
entspricht soweit ersichtlich auch allgemeiner Auffassung in
Rechtsprechung und Schrifttum (vgl. nur Sen.Beschl. v. 7. 6. 1977 - X
ZB 20/14, GRUR 1978. 102' f. -Prüfverfahren; v. 13. 5. 1980 - X ZB
19/78, GRUR 1980, 849 ff, -Antiblockiersystem; BGHZ 115, 11. ff. -
Seitenpuffer; EPA T 1173/97 Abt, EPA 1999, 609 619 ff. -
Computerprogrammprodukt/IBM; Benkard, PatG/GebrMG 9. Aufl. § 1 PatG
Rdn. 104; Busse, PatG, 5. Aufl. § 1 PatG Rdn. 45; Schulte, PatG, 5.
Aufl., § 1 PatG Rdn. 77; Mes. PatG § 1 Rdn. 57).

#Egj: Einer Patentierung stände es unter dem Gesichtspunkt fehlenden
technischen Charakters weiter nicht entgegen, wenn die der Anmeldung
zugrundeliegende Lehre Elemente aufweisen sollte, die der inhaltlichen
Überarbeitung von Sprachtexten zuzuordnen sein sollten. Eine solche
Überarbeitung (Textredaktion) wäre als solche allerdings nicht ohne
weiteres dem Bereich des Technischen zuzuordnen (vgl. hierzu etwa EPA
T 38/86 ABI. EPA 1990, 384 = GRUR tut. 1991, 118 - Textverarbeitung).
Auch eine Datenverarbeitungsanlage. auf der eine (redaktionelle)
Bearbeitung von Texten vorgenommen wird, bleibt aber als solche ein
technischer Gegenstand. Zweifelhaft kann insoweit allenfalls sein,
wieweit solche Elemente bei der Prüfung der Schutzfähigkeit zu
berücksichtigen sind, denen für sich ein technischer Charakter nicht
zukommt. Der Senat hat hierzu bei anderer Gelegenheit ausgeführt, daß
bei der Prüfung einer Erfindung, die technische und nichttechnische
Merkmale enthält, auf erfinderische Tätigkeit der gesamte
Erfindungsgegenstand unter Einschluß einer etwaigen Rechenregel zu
berücksichtigen ist (BGHZ 117, 144, 150 - Tauchcomputer). Die Frage,
ob dabei auch der Inhalt der zu verarbeitenden Information. zu
berücksichtigen sein kann wogegen Bedenken geltend gemacht worden sind
f. Melullis, a. a. O., S. 846 li. Sp.) und was auch nicht der
herkömmlichen .Auffassung zur Nichtberücksichtigung von %(q:geistigen
Anweisungen) entspräche (vgl. z. B. Sen.Beschl. v. 18. 3. 1975 - X ZB
9/74, GRUB 197 5, 549 f. Buchungsblatt), stellt sich im vorliegenden
Verfahren ersichtlich schon deshalb nicht, weil sich die Anmeldung
jedenfalls nicht auf solche Inhalte beschränkt.

#SWg: Schließlich steht es dem technischen Charakter der Vorrichtung nicht
entgegen, daß nach Merkmal (e) des Patentanspruchs 1 ein Eingreifen
des Menschen in den Ablauf des auf dem Rechner durchzuführenden
Programms in Betracht kommt.. Daß vom menschlichen Verstand Gebrauch
gemacht werden kann ... daß allein dadurch der Bereich des Technischen
bereits verlassen wird, ergibt sich schon daraus daß dem Patentschutz
Lehren zum planmäßigen Handeln unter Einsalz beherrschbarer
Naturkräfte zur Erreichung eines kausal übersehbaren Erfolgs
zugänglich sind (BGHZ 52, 74, 79 - Rote Taube.  Auch aus anderen
Entscheidungen des Senats läßt sich nicht entnehmen, daß bereits ein
menschliches Eingreifen für sich und auch für Fälle einer wie hier -
im Bereich der elektronischen Datenverarbeitung üblichen - im
Dialogbetrieb arbeitenden Einrichtung oder eines Dialogverfahrens dem
technischen Charakter der Lehre entgegensteht.

#Dej: Die angegriffene Entscheidung erweist sich auf der Grundlage der im
Beschwerdeverfahren getroffenen Feststellungen auch nicht aus anderen
Gründen als zutreffend, Zwar setzt sie sich (Gründe unter II. 2. b und
c) am Rande auch mit der Frage auseinander, ob der
Anmeldungsgegenstand auf erfinderischer Tätigkeit beruht. Das
Bundespatentgericht geht bei seiner Würdigung von der Beurteilung der
Schutzfähigkeit von Programmen aus.  Diese Frage stellt sich im
vorliegenden Fall indessen nicht, weil in Patentanspruch 1 eine
Vorrichtung und nicht ein Programm beansprucht ist. Schon aus diesem
Grund genügt die Entscheidung nicht den Anforderungen, die an die
Prüfung des Vorliegens erfinderischer Tätigkeit zu stellen sind. Das
Bundespatentgericht legt seiner Betrachtung zudem nicht, wie es
geboten gewesen wäre, den Gegenstand des Patentanspruchs in seiner
Gesamtheit zugrunde. Im übrigen widerspricht auch die völlige
Nichtberücksichtigung der %(q:nichttechnischen Erkenntnisse), die dem
Anmeldungsgegenstand zugrundeliegen, den von der Rechtsprechung zur
Beurteilung der erfinderischen Tätigkeit bei Erfindungen auf dem
Gebiet der Datenverarbeitung entwickelten Grundsätzen (vgl. Sen.Urt.
Tauchcomputer, a. a, O.).

#Ngt: Nach alledem kann die angefochtene Entscheidung keinen Bestand haben.
Sie ist deshalb aufzuheben und die Sache ist zu anderweiter
Verhandlung und Entscheidung an das Bundespatentgericht
zurückzuverweisen (§ 108 Abs. 1 PatG). Dieses wird zu bestimmen haben,
wer der hier maßgebliche Fachmann ist und welche Kenntnisse und
Fähigkeiten ihm zuzurechnen sind, und auf dieser Grundlatte zu
beurteilen haben, ob der Gegenstand der mit dem Erteilungsantrag
verfolgten Anmeldung gegenüber dem maßgeblichen Stand der Technik neu
ist und auf erfinderischer Tätigkeit beruht, wobei das Vorliegen einer
technischen Lehre weder indiziell noch präjudiziell herangezogen
werden kann.

#EsB: Eine mündliche Verhandlung hat der Senat schon deshalb nicht als
erforderlich angesehen, weil dem Begehren der Rechtsbeschwerde zu
entsprechen ist.

#Htn: Harrison: EPO not to search %(q:Business Method Patents)

#spe: British patent attorney says German courts are the most progressive,
quotes %(q:Sprachanalyse) and %(q:Automatische Absatzsteuerung), asks
british lawcourts to emulate this practise.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bgh-sprach00 ;
# txtlang: xx ;
# End: ;

