\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}The Annotated Text}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}``EXPLANATORY MEMORANDUM''}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}``The approach adopted''}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}``The legal basis for harmonisation''}{10}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}``Explanation of the Directive article by article''}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}The Proposal}{12}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}``FINANCIAL STATEMENT''}{16}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}``The Proposal''}{17}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Footnotes}{22}{subsection.2.8}
\contentsline {section}{\numberline {3}Further Reading}{25}{section.3}
