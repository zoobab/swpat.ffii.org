<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FTC 2003 Report on Patents and Competition

#descr: The Federal Trade Commission FTC.gov, and independent agency within
the US government, published this final report in 2003 after
conducting extensive hearings on the effects of patents and other
exclusivity rights on competition.  FTC conducted the hearings due to
what some hearing participants described as the %(q:widespread
animosity against patents in the software industry) and the impression
that competition law in its current state is incapable of preventing
anti-competitive effects in particular of patents in the software
fields, but also of exclusion rights in general on various industries.
  The final report comes to the conclusion that the patent system
stimulates competition and productivity in some fields (pharma is
cited as an example), whereas it tends to harm both in others,
especially where software and business methods are concerned.  The
report expresses doubts as to the wisdom of past court decisions to
admit patentability in these areas and proposes a series of measures
for repairing some of the damage.

#Wcr: FTC introduces the report.

#Tro: The full text of the report on patents and competition

#Ten: FTC overview page on the hearings of 2002

#nfW: Salient extracts from the FTC report, overview of quotations of
passages relevant to the software patent debate, in exhaustive detail.

#WiF: German ZDNet news article about the FTC report.  Contains some
misinformations, such as that the European Parliament legalised
software patents by its vote a month earlier.

#atr: Many panelists and participants expressed the view that software and
Internet patents are impeding innovation

#cno: Recommendation 6

#evm: Executive Summary

#umr: Summary

#hej: Chapter 3, I

#coW: Introduction -- Conclusions about various industries

#pWg: Chapter 3, V.A page 153

#aiP: The U.S. Federal Trade Commission was asked to research the interface
between patent policy and competition policy. After extensive
hearings, its report %(q:To Promote Innovation: The Proper Balance of
Competition and Patent Law and Policy) was published in October 2003.

#cee: The FTC looked especially closely at the different effects of patents
on the competitive environment in four key high-tech industries:
pharmaceuticals, biotech, computer hardware/semiconductors, and
computer software/internet. In its quietly-stated, deeply-researched
governmental way, its conclusions are fascinating and devastating:
positive effects of software patents are questionable at best; but
software patents have serious negative effects on competition -- and
competition, not patentability, is what drives innovation in this
sector.

#ese: Recommendation: Consider Possible Harm to Competition  Along with
Other Possible Benefits and Costs  Before Extending the Scope of
Patentable Subject Matter.

#eWc: ... Decisionmakers should ask whether granting patents on certain
subject matter in fact will promote such progress or instead will
hinder competition that can effectively spur innovation. Such
consideration is consistent with the historical interpretation of
patentable subject matter, which implicitly recognizes that granting
patent protection to certain things, such as phenomena of nature and
abstract intellectual concepts, would not advance the progress of
science and the useful arts. For future issues, it will be highly
desirable to consider possible harms to competition that spurs
innovation  as well as other possible benefits and costs before
extending the scope of patentable subject matter.

#laa: By contrast, computer hardware and software industry representatives
generally emphasized competition to develop more advanced technologies
as a driver of innovation in these rapidly changing industries. These
representatives, particularly those from the software industry,
described an innovation process that is generally significantly less
costly than in the pharmaceutical and biotech industries, and they
spoke of a product life cycle that is generally much shorter. Some
software representatives observed that copyrights or open source code
policies facilitate the incremental and dynamic nature of software
innovation. They discounted the value of patent disclosures, because
they do not require the disclosure of a software product's underlying
source code...

#ton: Representatives from both the computer hardware and software
industries observed that firms in their industries are obtaining
patents for defensive purposes at rapidly increasing rates. They
explained that the increased likelihood of firms holding overlapping
intellectual property rights creates a %(q:patent thicket) that they
must clear away to commercialize new technology. They discussed how
patent thickets divert funds away from R&D, make it difficult to
commercialize new products, and raise uncertainty and investment
risks. Some computer hardware and software representatives highlighted
their growing concern that companies operating in a patent thicket are
increasingly vulnerable to threats to enjoin their production from
non-practicing entities (NPEs) that hold patents necessary to make the
manufacturer's product.

#ret: In the software and Internet industries, innovation generally occurs
on an incremental basis, with participation possible at the design
level by individual programmers and small firms. Panelists
consistently emphasized that competition is an important driver of
innovation in these industries. Although some panelists stated that
software and business method patents foster innovation, many
disagreed, asserting that such patents are often questionable and are
actually stifling innovation by increasing entry barriers and creating
pervasive uncertainty. Some panelists questioned whether it was
necessary to have patent protection on software, given the
availability of copyrights. Others reported that defensive patenting
has accelerated the development of a patent thicket, which, in turn,
has increased the likelihood of patentees holding up their rivals. 
Panelists generally agreed that too many questionable patents are
issued; they attributed this to the difficulty patent examiners can
have in considering all the relevant prior art in the field and
staying informed about the rapid advance of computer science.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ftc03 ;
# txtlang: en ;
# multlin: t ;
# End: ;

