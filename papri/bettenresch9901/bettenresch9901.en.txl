<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#tao: the author(s)

#TCu: The Circular

#Fea: Further Reading

#Jdh: Jürgen Betten is a leading activist for the patentability of software,
including software-based business methods, in Europe.  He has given
expert opinion to the EPO in some landmark cases that lead to a
widening of software patentability.  He has been the chairman of the
%(un:UNION) working group on this subject.  You can find his opinions
and some career details on the %(br:Betten & Resch law office
website).  Also we have here commented on an %(be:article) co-authored
by him in a German law journal in 2000, where he tries to refute some
of the alleged arguments of software patent critics.  In March 2001,
Betten reported to %(ai:AIPPI) about the European situation,
explaining that the field of informatics has not yet been fully
absorbed by the patent system here, which is why it seems that the
time has not yet come for a resolution on the general patentability of
business methods.

#Tpu: The following circular which can be found on the law office website in
a similar form, was apparently sent out to interested clients and
other parties at the beginning of 1999, a time when the critical
voices hat not penetrated very far in Europe yet.  Thus this paper is
rather euphoric, and it pronounces some undoubtable truths about the
current caselaw situation in Europe in a rather frank manner.

#Tcc: The Technical Board of Appeal 3.5.1 of the European Patent Office
(EPO) has decided that, in principle, media claims (covering the
computer program on a storage medium ...) and Internet claims
(covering the transmission or electronic distribution of the computer
program) are admissible. ...

#Aoc: According to the US-CAFC (%(q:In re Lowry)) and the Guidelines of the
Japanese and the Korean Patent Offices, %(s:data structures) are
protectable by a patent claim.  This question has not yet been decided
by the Technical Boards of Appeal of the EPO.  In view of the EPO
decision %(q:BBC / Colour Television Signal) we are, however, quite
confident that the EPO will grant, in the long run, such claims as
well.

#IWo: In view of the practice of the last two to five years it can be said
that, in principle, a patent will be granted for %(s:all computer
programs) (including business methods) which are new and inventive.
This is at least valid for the EPO and the German Patent and Trademark
Office, but not yet for the UK Patent Office.  In connection with this
we refer to the %(q:SOHEI) case (EP 209907 for a computer managment
system), and EP patents for a trade warrant system (EP 762304), a
stateless shopping cart for the web (EP 784279), and an interactive
information selection apparatus (for selecting the items for a meal)
(EP 756731). Thus the practice of the EPO seems to be quite similar to
that of the USPTO, even if the wording of the claims differs somewhat.

#Ieh: In this connection it may be interesting to know that in 1997 the
number of European patent applications in the field of data
processing, most of them relating to computer programs, had the
highest growth of 28%{pc} compared with 1996, and that the EPO has
started to establish a second division of examiners dealing with
software applications.

#Itr: In General the practise of the German PTO is quite similar to that of
the EPO.  The UNION Round Table Convference on %(q:Patenting of
Computer Software) in December 1997 obviously had a good impact not
only on the European, but also on the German situation.  In 1998 the
17th Senate of the German Patents Court, who had a rather restrictive
practice as to patenting of computer programs in the past,
surprisingly admitted in 1998 in two cases the appeal on points of law
to the German Federal Supreme Court (FSC).  Such an admission had been
denied all the years before.  This will give the FSC the possibility
to consider the discussion of the last years and to bring its case law
of 1991/1992 in line with that of the Technical Boards of Appeal of
the EPO.

#OsW: One judge of the FSC, who is the expert in the FSC for computer
programs, has just published an %(ma:article) showing his %(q:personal
opinion), according to which, in principle, computer programs should
be considered technical.  However only the conversion of the logical
concept into the operation of the computer or the realization of the
logical concept (program) by the computer, but not the logical concept
itself should be protected by patents.  This approach seems to be
quite similar to what is known as %(q:technical application) of the
computer program in the USA.

#Tae: The latest state of the discussion on the amendment of the European
Patent Convention (EPC) is that there seems to be a great consensus
within the deciding bodies of the EPO and the European Commission that
Art 52(2) and (3) EPC (including the exclusion of computer programs as
such) should be cancelled and that Art. 52(1) EPC should be brought in
line with Art 27(1) TRIPs Agreement.  Although such an amendment could
take years, the efforts towards such an amendment may have an impact
on the general practice of the EPO so that the exclusion of computer
programs as such from patentability in Art. 52(2) EPC will be
interpreted in a very narrow way.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bettenresch9901 ;
# txtlang: en ;
# multlin: t ;
# End: ;

