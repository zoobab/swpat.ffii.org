\begin{subdocument}{uplj-chisum86}{Bronwyn H. Hall & Rose Marie Ham: The Patent Paradox Revisited}{http://swpat.ffii.org/papri/uplj-chisum86/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{A leading US scholar of patent law who is said to have exerted an overwhelming influence on patent jurisdiction of major US patent law courts such as the CAFC, explains that it is not consistent to grant patents on practical applications of mathematical algorithms but not on the algorithms themselves.  Chisum argues that mathematical methods are useful inventions just like any other methods and should be granted independently of any specific tangible application.  Any future tangible application that uses the mathematical method should fall under the claim.}
\begin{description}
\item[title:]\ Bronwyn H. Hall & Rose Marie Ham: The Patent Paradox Revisited
\item[source:]\ University of Pittsburgh Law Journal, 47 (1986) 959-1022
\end{description}

\begin{itemize}
\item
{\bf {\bf Newell 1986: The models are broken, the models are broken!\footnote{\url{http://localhost/swpat/papri/uplr-newell86/index.en.html}}}}

\begin{quote}
Paul Newell, professor of computer science, in response to law professor Donald Chisum's proposal to affirm the patentability of algorithms, does not directly say whether algorithms should be patentable, but rather describes how both affirmation and negation of this proposal would clash with the underlying assumptions of the patent system and how this will inevitably challenge the foundations of the patent system.  As more and more problems are solved by purely mathematical means, the patent system will either have to become less relevant for innovation, or it will have to completely review its model of what an invention is and how it should be appropriated.  In particular, Newell explains some basic concepts of informatics and points out that, with algorithms, there can be no meaningful differentiation between discovery and invention, between application and theory, between abstract and concrete, between numeric and symbolic etc.  Moreover he explains by a model of game theory that sharing algorithms, as currently practised by programmers, may lead to more innovation than making them appropriatable, so that a crude application of the patent system to algorithms could very well stifle rather than stimulate innovation.
\end{quote}
\filbreak

\item
{\bf {\bf Bronwyn H. Hall & Rose Marie Ham: The Patent Paradox Revisited\footnote{\url{http://localhost/swpat/papri/irle-laat00/index.en.html}}}}

\begin{quote}
Paul B. Laat, philosopher from Groningen, argues that mathematical concepts should be patentable without any reference to practical applications.  He points out that such references lead to inconsistencies and that it would be better to instead limit the range of objects against which patents can be enforced.  Unfortunately he does not engage in a serious discussions about the latter or about the economic implications of unlimited patentability.  Instead he points out that some important mathematical inventions in renaissance times were kept secret for decades and might have been known earlier had there been a patent system on mathematical ideas in place.
\end{quote}
\filbreak

\item
{\bf {\bf Mark Schar 1998: What is Technical?\footnote{\url{http://localhost/swpat/papri/jwip-schar98/index.en.html}}}}

\begin{quote}
An EPO judge proposes a consistent definition of the term ``technical character'' that has sinc 1986 been changed by the EPO's Technical Board of Appeal in an unsystematic manner, case by case.   Schar proposes that that any practical and repeatable solution should be considered to be a technical and therefore patentable.  This is well in line with current EPO practise, and it means that all computer programs as well as all programmed or otherwise practically applied mathematical methods, business methods, games and data structures are patentable.  Only the problem solving event in the human mind is non-patentable according to this doctrine.  As soon as the solution is objectivated, i.e. transformed into its practically applicable and repeatable form, it is no longer the solution as such and therefore patentable.  Schar explicitely rejects previous doctrines such as those of the German Federal Court (and the EPO examination guidelines of 1978) which distinguished between the invention and its embodiment (objectivation) and demanded that not only the emodiment but also the invention be technical, i.e. contain natural forces as a constitutive element.  The chief reason he gives for this rejection is that certain books from 1900 contain some outdated or politically incorrect wording and that currently monism seems to be more fashionable than mind-matter dualism.  This reasoning may be more than sufficient to convince the readers of the Journal of World Intellectual Property.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

