<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wolfgang Wodarg: EU-Patentrichtlinie: Schwammige Definitionen,
moralische Lyrik

#descr: Der Bundestagsabgeordnete und Arzt schreibt im Deutschen Drzteblatt
|ber die Manvver der Br|sseler Patentbewegung im Gen-Bereich

#Wna: Wodarg hat die %(ep:Eurolinux-Petition) wohl nicht zuletzt deshalb
unterzeichnet, weil er auf seinem Gebiet mit den Entgrenzungs-Taktiken
der Patentgesetzgeber aufs genaueste vertraut ist.  Im %(q:Deutschen
Ärzteblatt) vom Juli 2000 seziert er unter der Überschrift
``Schwammige Definitionen, moralische Lyrik'' die EU-Richtlinie
``Rechtlicher Schutz biotechnologischer Erfindungen''.  Darin schreibt
er u.a.:

#DWn: Der Text der EU-Richtlinie enthält vor allem im Vorspann und in den
Erwägungsgründen viel moralische Lyrik, mit der Kritiker
abgeschmettert und Parlamentariergewissen beschwichtigt werden können.
 Klare Grenzen und Definitionen fehlen.  Schwammige oder in sich
widersprüchliche Bestimmungen sorgen dafür, dass Rechtsnormen, wie sie
das 1977 in Kraft getretene Europäische Patentübereinkommen vorgibt,
systematisch ausgehöhlt werden können -- zum Beispiel das zentrale
Prinzip der ärztlichen Therapiefreiheit.  Nach Artikel 52,4 des
Übereinkommens dürfen diagnostische, therapeutische und chirurgische
Verfahren am menschlichen und tierischen Koerper nicht patentiert
werden.  Diesen Grundsatz beizubehalten, verspricht die Richtlinie in
Erwägungsgrund 35, jedenfalls im Hinblick auf die Verfahren als
ganzes.  Teilschritte von Verfahren können jedoch sehr wohl patentiert
werden.  Nach dem Motto: Keine Tür darf dem Arzt zum Wohle der
Patienten verschlossen sein, über die Nutzung der Türgriffe
entscheidet aber der Patentinhaber.

#Ele: Eine ähnliche juristische Spitzfindigkeit findet sich in dem für die
EU-Richtlinie zentralen Artikel 5.  Im ersten Abschnitt ist
festgelegt, dass der menschliche Körper in allen Entwicklungsstadien
nicht patentierbar ist, ebenso wenig wie die bloße Entdeckung eines
seiner Bestandteile (Beispiel Gene).  Im zweiten Abschnitt heisst es
jedoch, dass Teile, die mit Hilfe eines technischen Verfahrens
isoliert wurden, sehr wohl patentierbar sind.  Wer sich mit der
Regelung näher befasst, wie der Nationale Ethikrat der Dänen oder
Kritiker in Frankreich, kommt nicht umhin festzustellen, dass bei
Genen und Teilsequenzen von Genen die vermeintliche Ausnahme die Regel
ist.  Gene liegen immer isoliert vor, wenn sie entschlüsselt werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: daeb-wodarg00 ;
# txtlang: de ;
# multlin: t ;
# End: ;

