<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: UK High Court 2002-03-15: patent infringement doesn't depend on server
location

#descr: According to this decsision (Menashe v. Hill) of the High Court of
England & Wales, a game played between a foreign server and UK clients
would still infringe on a UK (EP) patent for a computer-implemented
game system, if that patent is valid and the game falls within the
claims. The fact that the server is placed outside of the territory,
so that the system as claimed does not physically occur within the UK
jurisdiction, does not protect the defendant from liability.

#TlT: The judge adopts a somewhat unbalanced teleological (purpose-oriented)
law interpretation, according to which it is the purpose of patent law
to protect the inventor and claim-wordplays, which are commonplace and
even the basis of software patentability in Europe, may be played by
the patentee but not by those who try to try to stay out of harmsway. 
The only reason why in this case it was possible to dodge was that an
illegal software patent had been granted.  To use such a patent as a
basis for creating a precedent that blurs the territoriality principle
of patent law seems quite bold.

#Ieu: It is also unfortunate that the defendant did not fight on the basis
that he was using only a game and program as such, with no extra
inventive element, and that therefore, even if the patent could be
valid, he could not have been infringing on it, since the invention,
according to art 52 EPC, must have been in something beyond a mere
game or computer program.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ewhc-mh020315 ;
# txtlang: en ;
# multlin: t ;
# End: ;

