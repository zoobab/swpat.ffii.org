<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Security in Information Technology and Patent Protection for Software
Products -- Expert Opinion by Lutterbeck et al written at the order of
the German Ministery of Economics and Technology

#descr: Prof. Lutterbeck of Berlin Technical University, his assistant Robert
Gehring and Axel Horns, patent lawyer in Munich, figuring under the
name of %(q:Internet Governance Research Group), received an order
from the German Ministery of Economics and Technology in late summer
of 2000 to work out this %(q:short expert opinion) which was published
in December 2000.  A large part of the 166 pages is dedicated to
reiterating the well-known legal opinion of Horns.  Horns states that
Art 52 EPC was a misconception from the beginning, and that patent law
will be seriously impaired unless any innovation that is implemented
through a computer is patentable.  However he warns that software
patents can have a very negative impact on open source software and
proposes that patent law at least in Germany should be amended in such
a way that the publication and transmission of source code does not
violate the law, even if the excecution of object code on a computer
does.

#Dwn: This Expert Opinion, ordered by the German Ministery of Economics,
largely states the opinion of Axel Horns, patent lawyer in Munich,
which he had alread exposed in almost identical form in previous
articles and discussions.  Like many of his colleagues, Horns
interprets Art 52 EPC as an obnoxious misconception of the legislator.
 He eloquently avoids to understand its underlying systematic teaching
about the technical invention.  The software patent critics, who base
much of their legal opinion on this systematic understanding of the
current law, are labeled as %(q:ideologues).  Even the legislator is,
against all historical facts, presented as a moron:

#Shg: Thus the introduction of the term %(q:computer programs as such) into
German and European patent law was no more than an act of symbolic
legislations without practical meaning, born from a lack of technical
understanding. ... It was by no means understood to serve as a dam
against the patentability of software inventions.

#Dkt: %(ar:Art 52(2)(3) EPC) does not either mention or imply a syntactic or
semantic unit called %(q:computer programs as such).  Just as
mathematical methods, construction plans, %(q:programs for computers)
etc are %(e:not patentable as such), the historical myths of Horns are
%(e:as such hardly worthy of any serious discussion).  However they
could aspire to certain honours in the context of a %(ms:satire).

#Eds: Instead of analysing the details of this expert opinion, we will first
present here a few extracts from our discussions with Axel Horns.  In
order to understand more deeply the background of this discussion, you
may want to browse through the %(lt:legal literure on the notion of
technical invention).  Horns keeps silent on this literature, mentions
at best helpful fragments of the %(ab:ABS decision) and occasionally
insinuates that once upon a time there was an %(q:outdated) concept of
technical invention and and an %(q:old core theory) (i.e. the doctrine
that a claim must be stripped of its %(q:linguistic clothing) and that
not the claim wording but the %(q:inventive core), i.e. the problem
solution in return for whose disclosure a monopoly is granted, must be
an application of physical causality).  Unfortunately the reasoning of
Horns itself could be seen as %(q:outdated) compared to the concept of
technical invention and the %(q:core theory).  The Horns teaching
lacks novelty compared to the reasoning of a whole range of theorists
and court decisions of the 50s, 60s and 70s, that were, after a long
argumenative back and forth, overruled by the landmark decisions in
1973, %(dp:1976) in Germany as well as parallel decisions in France
and other European countries.  The European Patent Convention (EPC)
was born in this context, and its underlying concept of technical
invention received a forceful theoretical underpinning by %(gk:Gert
Kolle) and other scholars of the time, which again found its way back
into BGH caselaw, which speaks a clear a language against software
patents, just as the examination guidelines of the EPO.

#DWn: The LuHoGe expert opinion uses a lot of space on known personal
opinions that have been forcefully rejected.  One of these is Robert
Gehrings statement in favor of a novelty grace period, which echoes,
which echoes a similar demand of the %(bm:universitarian patent
lobby).  Another is Prof. Lutterbecks frequently reiterated claim that
copyright is adequate to Goethe's Faust but not to software.  This
claim is no longer state of the art, because copyright has been
adapted to software through several rounds of revision in the 80s and
90s, and it contradicts the daily practise and widespread acceptance
of copyright in all parts of the software community.  The fact that
software functionalities cannot be appropriated by means of coypright
does not indicate that they need to be appropriated by means of
patents.  It could just as well indicate that they should not be
appropriatable at all, as has been frequently pointed out by the
patent law literature.  Lutterbeck on the one hand tries to justify
the expansion of the patent system into the field of abstract
functional ideas and on the other hand cites Machlup and others to
give a negative judgement on the patent system as a whole, including
the field of concrete physical causality, where it has caused much
less trouble.  This way Lutterbeck manages to appear as a critic of
the patent system while at the same time condoning patent inflation
and evading the questions he was asked by the ministery of economics.

#Iba: On the whole the experience with this and other %(q:expert opinions)
leads to the question, whether such opinions really justify the
privilege of a governmental study order.   Wouldn't it be better if
the responsible governmental agencies could actively participate in a
well organised public discussion through means such as mailing lists
or newsgroups, and within that framework sponsor conferences, pay for
more systematic presentations of information sources as well as
research on specific factual questions?  The question of this study
%(q:patents vs open source, a contradiction?) was apparently not worth
asking.  Our %(kf:consultation questions of the Federal Parliament)
could provide some inspiration as to what might be.

#Bbk: On 2000-08-03 H. Soquat announces that the Federal Ministery of
Economics and Technology (BMWi) has ordered a study on the impact of
patents on network security and open source software

#Oru: The BMWi website announces the publication of the study in Dec 2000
and explains backgrounds

#DoW: Similar arguments from Axel Horns on his website

#Eeh: Another similar article appeared in GRUR 2001-01

#Tve: Telepolis report

#Mbd: Telepois reports about the LuHoGe report and about dissenting voices
from FFII, SuSE and others

#zes: reports about the report and the criticism

#DiE: Discussions on Mailing Lists

#Rhu: Special treatment for OpenSource not recommendable

#Srt: Website of the group of researchers around Prof. Lutterbeck at Berlin
Technical University

#Aen: Article by %(WT)

#DWr: The software patent chief ideologue of the German Patent Office
applauds the expert opinion's view that patentability can not be
limited to technical inventions in the traditional sense, but
criticises their demand for special treatment of opensource software
as the reflection of particularist interests of people who want free
access to other's intellectual property for themselves and at least in
part non-conformant to %(q:international legal order) which must take
precedence over considerations of whether such an order serves the
goals of public policy, given that the latter is often influenced by
particularist interests such as that of the open source lobby, due to
whose pressure the overdue deletion of %(q:programs for computers)
from the European Patent Convention was once again postponed.

#Het: Hebt die ökonomischen Überlegungen hervor, lässt die juristische
Argumentation links liegen.

#wiK: wiederholt im wesentlichen Standpunkte der Studie und weist ein
englischsprachiges Publikum auf deren Bedeutung hin.  Erhielt auf
einer Konferenz einen von SAP u.a. ausgelobten Preis als %(q:bestes
Papier).

#Efs: %{EB}, inventor and proprietor of some valuable patents in the field
of measurement technology, is concerned about an %(q:exhaustion of
patentworthy ideas).  He finds that the stock market and vested
interests have, in their relentless drive to develop ever new
goldrushes fewelled by patent monopolies, inflated the patent system
to a degree where most patents are trivial and the system itself is
close to a point of collapse, like an economy after a long period of
hyper-inflation.  He applauds the ministery of economics for its
decision to take the issue out of the hands of patent law gurus of
international organisations and openly question the efficiency of the
system.  However he criticizes that the authors of this study seem to
be more concerned about continuing to extend the system and conserving
unjustified proprietary structures than about actually analysing the
problems and proposing solutions.  Here are some quotations:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bmwi-luhoge00 ;
# txtlang: xx ;
# End: ;

