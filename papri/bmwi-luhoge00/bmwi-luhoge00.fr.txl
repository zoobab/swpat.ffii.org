<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Efs: %{EB}, selber Inhaber einiger Patente im Bereich der Messtechnik, konstatiert eine %(q:Erschöpfung der patentierbaren Ressourcen).  M.a.W. es wurde immer mehr und immer trivialeres patentiert, in der Hoffnung, dadurch neue Wertschöpfungspotentiale für monopolbasierte Börsen-Strohfeuer erschließen zu können, aber diese Inflation muss irgendwann zusammenbrechen.  Dem BMWi kommt der Verdienst zu, das Thema aus den Hinterzimmern patentjuristischer Gurus in die Sphäre der öffentlichen wirtschaftspolitischen Diskussion geholt zu haben, aber die Gutachter enttäuschen.  Sie scheinen mehr darum besorgt zu sein, die proprietären Strukturen des Patentwesens zu retten.

#wiK: wiederholt im wesentlichen Standpunkte der Studie und weist ein englischsprachiges Publikum auf deren Bedeutung hin.  Erhielt auf einer Konferenz einen von SAP u.a. ausgelobten Preis als %(q:bestes Papier).

#Het: Hebt kunstvoll die ökonomischen Überlegungen hervor, lässt die juristische Argumentation links liegen.

#DWr: Der Softwarepatent-Abteilungsleiter und -chefideologe des Deutschen Patentamtes applaudiert dem LuHoGe-Gutachten, in soweit es für die grenzenlose Patentierbarkeit plädiert, hält aber wenig von der vorgeschlagenen Privilegierung von Quelltexten.  Dabei bezieht er sich auf seine Privattheorie, wonach unter %(q:Programmen als solchen) der Quelltext zu verstehen ist.

#Aen: Artikel von %{WT}

#Srt: Selbstdarstellung der Forschergruppe um Prof. Lutterbeck an der TU Berlin

#Rhu: Rechtliche Sonderbehandlung von OpenSource nicht sinnvoll

#DiE: Diskussion in E-Foren

#zes: zu Meinungsverschiedenheiten über das LuHoGe-Gutachten

#Mbd: Meinungsverschiedenheiten über das LuHoGe-Gutachten und Kritik von FFII, SuSE u.a.

#Tve: Telepolis-Bericht

#Eeh: Ein weiterer Artikel dieser Art erschien in GRUR 2001-01

#DoW: Die Argumentation von Horns auf seiner eigenen Webestätte

#Oru: Die BMWi-Webseiten verkünden im Dez 2000 die Veröffentlichung des Gutachtens und erklären Hintergründe

#Bbk: Bekanntmachung von Hubertus Soquat 2000-08-03 auf FFII-Verteiler

#Iba: Insgesamt stellt sich die Frage, ob die persönlichen Meinungen ausgesuchter %(q:Experten) wirklich eine Auftragsvergabe durch die Bundesregierung rechtfertigen.  Wäre es nicht viel besser, wenn sich die Regierungsstellen aktiv an vorhandenen Foren beteiligen und in deren Rahmen Bemühungen um eine umfassendere Bereitstellung von Informationen sowie um Beantwortung klar definierter Tatsachen-Fragen hinwirken würde?  Die Fragestellung dieser Studie %(q:Patente vs Opensource, ein Widerspruch) war offenbar nicht besonders fruchtbar.  Die %(kf:Konsultationsfragen des Bundestages) könnten als ein erster Wegweiser für ein künftiges zielgerichteteres Vorgehen dienen.

#DWn: Das LuHoGe-Gutachten verwendet sehr viel Raum auf persönliche Meinungen, die längst kraftvoll widerlegt wurden.  Hierzu gehören auch die Vorstellungen von Robert Gehring zur Neuheitsschonfrist, mit denen eine Forderung der %(bm:Hochschul-Patentbewegung) weitere Unterstützung erhalten soll.  Hinzu kommt %(lu:Prof. Lutterbeck)s häufig wiederholte Behauptung, das Urheberrecht passe auf Goethes Faust aber nicht auf Computerprogramme.  Dieses Vorurteil der 70er Jahre ist nicht erst seit den Urheberrechtsnovellen der 80er und 90er Jahre offensichtlich veraltet.  Tatsächlich stehten Software-Autoren auf der Sonnenseite der Eigentumsordnung.  Das Urheberrecht entfaltet im Softwarebereich eine besonders starke Schutzwirkung und genießt die einhellige Akzeptanz der Branche.  Schon früher hatte sich das Urheberrecht weit über %(q:Goethes Faust) hinaus in Bereichen wie Musik, Pantomime, Skulptur, Architektur, wissenschaftliche Darstellungen, Landkarten, Konstruktionszeichnungen, Gebrauchsanweisungen bewährt.  Die funktionale Natur dieser Gegenstände war nie ein Argument gegen die Anwendbarkeit des Urheberrechts.  Die Tatsache, dass in all diesen Fällen logische Funktionalitäten vom Urheberrecht explizit freigestellt werden, kann erst recht nicht eine Forderung begründen, sie über die Hintertür des Patentrechtes doch wieder monopolisierbar zu machen.  Im Gegenteil lässt sich daraus die Einsicht ableiten, dass allgemeine Ideen frei bleiben sollten, wie es von der einschlägigen urheberrechtlichen (und z.T. patentrechtlichen) Literatur immer wieder erklärt worden ist.  Lutterbeck rechtfertigt einerseits die Expansion des Patentwesens in das Reich der logischen Funktionalitäten, andererseits zitiert er Wirtschaftswissenschaftler wie Machlup, um das ganze Patentwesen, einschließlich der die Patentierung technischer Erfindungen zu diskreditieren.  So gelingt es ihm, sich einerseits als Patentkritiker und Freund der %(q:Opensource-Bewegung) darzustellen, andererseits aber der Patentexpansionsbewegung die Richtigkeit all ihrer wesentlichen Glaubenssätze zu bescheinigen und zugleich den heiklen Fragen, um deretwillen das BMWi ihn um Rat fragte, aus dem Wege zu gehen.

#Eds: Es folgen anstelle einer detaillierten Würdigung des Gutachtens zunächst ein paar Auszüge aus unserer Diskussion mit Axel Horns.  Zur Einstimmung auf das Thema blättere man in der %(lt:juristischen Literatur über den Begriff der technischen Erfindung).  Horns verschweigt diese Literatur, erwähnt von der Rechtsprechung bis 1986 allenfalls ihm genehme Bruchstücke aus dem %(ab:ABS-Beschluss), erwähnt ohne nähere Erläuterung einen %(q:veralteten) Technikbegriff und eine %(q:alte Kerntheorie) (d.h. die Forderung des BGH, dass nicht der Anspruchswortlaut sondern die Leistung des Erfinders im Kern technischer Natur sein muss).  Man könnte jedoch ohne weiteres gerade die Hornssche Rechtsauffassung als %(q:veraltet) bezeichnen.  Sie weist nämlich im Vergleich zu den Argumenten der Softwarepatentbewegung der 60er Jahre keinerlei Neuheit auf.  Diese Standpunkte wurden nach reifer Diskussion durch die Grundsatzurteile von 1973 und %(dp:1976) in Deutschland und parallele Urteile in FR, NL, UK und anderen Ländern gründlich widerlegt.  In diesem Zusammenhang entstand auch das Europäische Patentübereinkommen.  %(gk:Gert Kolle) und zahlreichen andere Theoretiker lieferten dafür eine rechtsdogmatische Begründung, und die %(gl:Prüfungsrichtlinien des EPA von 1978) sprechen ebenso wie die Gerichtsentscheidungen jeder Zeit eine klare Sprache.

#Dkt: In %(ar:Art 52(2)(3) EPÜ) ist weder explizit noch implizit von einer Einheit namens %(q:Datenverarbeitungsprogramm als solches) die Rede.  Ebenso wie mathematische Methoden, Baupläne, %(e:Programme für Datenverarbeitungsanlagen) usw nicht %(e:als solche patentierbar sind), ist auch die Hornsche Geschichtsauslegung kaum %(e:als solche eine seriöse Diskussion wert).   Im Rahmen einer %(ms:Satire) kann sie jedoch sehr wohl zu gewissen Ehren gelangen.

#Shg: Somit war die Einführung des Begriffs %(q:Datenverarbeitungsprogramm als solches) in das deutsche und europäische Patentrecht lediglich ein aus Mangel an technischem Verständnis geborener Akt symbolischer, aber praktisch wirkungsloser Gesetzgebung. ... Keinesfalls sollte in den Bestimmungen ein Bollwerk gegen die Patentfähigkeit von Software-Erfindungen gesehen werden.

#Dwn: Das Gutachten erläutert eine altbekannten Rechtsauffassung des Münchener Patentanwalts %(ho:Axel H. Horns), die er bereits früheren Artikeln und umfangreichen Diskussionen dargelegt hatte.  Horns kann dem Art 52 EPÜ keine klare Bedeutung abgewinnen.  Wie viele seiner Kollegen interpretiert Horns den Art 52 EPÜ als einen ärgerlichen Missgriff des Gesetzgebers und bemüht sich, diesem Artikel keine vernünftige Bedeutung abzugewinnen.   Er vermeidet es eloquent, die ihm zugrundeliegende %(kr:Systematik der technischen Erfindung) zur Kenntnis zu nehmen.  Die Softwarepatentgegner, die sich heute auf diese Systematik berufen, bezeichnet er als %(q:Ideologen).  Selbst der Gesetzgeber wird, wider alle historischen Fakten, als Dummerchen hingestellt:

#descr: Prof. Dr.iur. Bernd Lutterbeck von der TU-Berlin, sein Assistent Robert Gehring und der Münchener Patentanwalt Axel Horns nahmen im Spätsommer 2000 unter dem Namen %(q:Forschergruppe Internet Governance) einen Auftrag des BMWi für dieses 166 Seiten lange %(q:Kurzgutachten) an, das im Dezember 2000 veröffentlicht wurde.   Darin vertreten sie eine bereits häufig zuvor veröffentlichte Rechtsauffassung des Münchener Patentanwalts Axel H. Horns zur Frage der Patentierbarkeit von Computerprogrammen in Europa.  PA Horns kann dem Art 52 EPÜ keine klare Bedeutung abgewinnen.  Er erklärt den traditionellen Technikbegriff des Patentwesens für nicht mehr zeitgemäß und hält die grenzenlose Patentierbarkeit, wie das EPA sie in der Theorie anstrebt und in der Praxis bereits weitgehend verwirklicht hat, für unvermeidbar.  Gleichzeitig wird aber vor diversen negativen Folgen des Patentwesens für Wirtschaft und Gesellschaft gewarnt und es werden diverse Maßnahmen vorgeschlagen, mit denen die Sperrwirkung der Patente im Bereich der Informationsverarbeitung abgeschwächt werden könnte, so dass zumindest in Deutschland ein Reservat übrig bleiben könnte, in dem Software-Quelltexte ungehindert veröffentlicht aber nicht gewerblich genutzt werden dürfen.

#title: La Sécurité dans la Technologie d'Information et la Protection de Brevet pour les Produits Logiciels -- Opinion d'Expert de Lutterbeck et autres a l'ordre du Ministère d'Économie et Technologie Allemand

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bmwi-luhoge00 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

