<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Aborted Whitehouse Study on Software Patents 1998

#descr: In summer 1998 a policy study division of the Whitehouse under
President Clinton attempted to study the problem of failures of the
patent system in the software and business method field but then was
successfully pressured by IBM lobbying to desist from the project. 
The outlines of the project became public later.

#pnN: Report in GregNews

#iry: Overview of the Proposed Study Project

#giW: This report must be taken with a grain of salt.  %(GA) has been
selling his patent busting services as the solution to all software
patent problems and been attacking %(q:both sides) of the software
patent discussion for not believing in this solution.  However,
Aharonian is not the only one who says that the planed Whitehouse
report was given up due to lobbying by IBM.

#vhE: Innovating in the Digital Economy:

#nas: Patent Quality and Business Effect

#ecW: A Project of the Critical Technologies Institute

#Won: for the White House Office of Science and Technology Policy

#agu: Background

#Wor: The patenting system is designed to protect and foster the fruits of
intellectual labor.  In making the environment more secure for
inventors, it increases the incentives to invent by making the
potential rewards for doing so easier to capture.  The presumption is
that this will benefit society as a whole by making it valuable to
seek applications for the invention, thereby ensuring its wide
diffusion and dissemination, and by enhancing the capture of those
aspects of the invention that possess the characteristics of a public
good. The transaction therefore involves issuing a patent granting a
limited monopoly to the patent-holder in exchange for full disclosure
of the inventor's art.

#hlW: It is by now a truism to note that rapid technological change is one
of the hallmarks of our time. Do patents and the patenting process
work as well-for both inventors and society -- in current areas of
innovation as they are perceived to have worked for the products and
processes of the industrial revolution?

#oWu: This issue has become critical in the burgeoning %(q:digital economy)
that depends on advances in software and electronic commerce,
including new business models that must of necessity be implemented on
computers and networks.  These fields have become economically
important in their own right while their products provide the motive
force to the digital revolution which has transformed so many
traditional industries and give birth to new ones.

#afW: Software and %(q:business methods) pose challenges to the patent
system in several respects. Innovation in these areas is often more
abstract than for traditional industrial products and processes, which
may make examination of applications (and subsequent litigation) more
difficult.  The pace of change is so rapid that the patent system is
stressed to respond in a timely and accurate fashion to the
requirements of these fields.  The difficulty of understanding prior
art is exacerbated by the wide distribution of effort and the
under-documentation of state-of-the-art that are characteristic of the
industry.  An inability to keep up with the pace and to ensure the
quality of the patents that do issue may prove detrimental to the
fields' development and to the interests of inventors, investors, and
society.

#isW: Typically, in the early days of an applied technology the patents will
tend to be fairly broad in scope, becoming narrower as the field
develops.  Scope, therefore, is a major dimension contributing to a
perception of patent 'quality'.  A quality patent is one that is
considered to be appropriate in scope for the art being claimed within
the context of developments in its field.  It will provide sufficient
disclosure to be enabling, and the claims will be seen as a
development not obvious to one sufficiently practiced in the art. 
Such a patent will be viewed as being defensible without dominating
the possibilities for other instantiations of the art from being
developed.  It will be forthcoming in a timely fashion given the needs
of that branch of technology.

#alt: In the software field, the issue of patent quality is a pressing one. 
Concerns have been raised about quality in terms of patent scope, but
also in terms of defensibility, perceived nonobviousness, degree of
enabling disclosure, potentially chilling effects on both innovation
and technological uptake, and of cost and timeliness. There appears to
have been no systematic analysis of software patent quality to date. 
It is the purpose of the research outlined here to begin an initial
exploration, to discover what may be known or knowable about software
patent quality, and to indicate areas where further effort might be
required to come to a fuller understanding of the issues.

#hSd: The Study

#ien: The fact that concerns are expressed by some does not mean that there
is a public policy case to be made for making adjustments in the
present approach to software patenting.  An assessment needs to be
made about the systematic nature of complaint.  How ubiquitous are the
concems? What points of commonality exist and in what areas are
concerns most frequently raised? To what extent can different
perceptions be attributed to the strategic and tactical positions held
by the complainants?

#aye: This project has been conceived as a preliminary study of a
fact-gathering nature. It is intended to

#dll: canvass informed opinion on the issue of patent quality in the realms
of software and electronic commerce; and

#nWr: perform an exploratory analysis of empirical data sources available.

#Wai: The approach will emphasize taking a wide cut, providing an overview,
as opposed to a more intensive case study approach.

#ioe: The principal questions to be addressed are:

#cqn: What is the perception within the industry of the quality of patents
that have been and are being issued?

#Wet: What has been and is likely to be the economic cost to the private and
public sectors of deficient or problematic patents?

#Wgt: The project will seek to gain an understanding of the perceptions of
industry practitioners and those intimately engaged in the patenting
process.  At the same time, objective data sources will be analyzed to
shed light on the responses received. The intent is to assess effects
of patents, for good or ill, on both the environment for invention as
well as on the post-invention conditions for exploiting the inherent
promise of the technologies involved.

#Wtn: The software patent chapter of this NAS report of 2000 is reminiscent
of the aborted Whitehouse report of 2000, and both were slammed as
%(q:idiotic) or similar by Aharonian..  Perhaps the same people were
involved.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: whitehouse98 ;
# txtlang: en ;
# multlin: t ;
# End: ;

