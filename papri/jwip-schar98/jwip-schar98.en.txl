<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Mark Schar 1998: What is Technical?

#descr: An EPO judge proposes a consistent definition of the term
%(q:technical character) that has sinc 1986 been changed by the EPO's
Technical Board of Appeal in an unsystematic manner, case by case.  
Schar proposes that that any practical and repeatable solution should
be considered to be a technical and therefore patentable.  This is
well in line with current EPO practise, and it means that all computer
programs as well as all programmed or otherwise practically applied
mathematical methods, business methods, games and data structures are
patentable.  Only the problem solving event in the human mind is
non-patentable according to this doctrine.  As soon as the solution is
objectivated, i.e. transformed into its practically applicable and
repeatable form, it is no longer the solution as such and therefore
patentable.  Schar explicitely rejects previous doctrines such as
those of the German Federal Court (and the EPO examination guidelines
of 1978) which distinguished between the invention and its embodiment
(objectivation) and demanded that not only the emodiment but also the
invention be technical, i.e. contain natural forces as a constitutive
element.  The chief reason he gives for this rejection is that certain
books from 1900 contain some outdated or politically incorrect wording
and that currently monism seems to be more fashionable than
mind-matter dualism.  This reasoning may be more than sufficient to
convince the readers of the Journal of World Intellectual Property.

#cxr: %(q:Technicality) (german Technizität, latin technicitas) is not
related to %(q:technicalities) (= implementational details).  A less
confusing term would be %(q:technicalness), or, conforming to
anglo-latin word-derivation rules, %(q:technicity). %(q:Technique)
stands here for german %(q:Technik), and to my knowledge it is not
used in the English version of the EPC.  %(q:Technik) can refer to
%(q:violin technique) (Violintechnik) and other %(q:non-technical)
techniques, but when standing alone, it is understood to mean
%(q:applied natural science) or %(q:engineering), seen in words such
as %(q:Technische Hochschule) (engineering college), %(q:technische
Berufe) (engineering professions).  In German, this is understood as
not referring to the %(q:technicalities) of mathematics or business
administration but only to applied natural science.  In the words of
the Federal Court (BGH), patents hat to contain a %(q:technical
teaching), valuable to the specialist of applied natural science,
providing him with new insights on how to %(q:use natural forces to
directly achieve a causally overseeable useful effect).  This doctrine
was written into the examination guidelines of the EPO and the German
patent office in the 1980s.  However the EPO has since then
successively dismantled this doctrine by a series of decisions of the
Technical Board of Appeal (TBA).  The author Mark Schar was a member
of the TBA during this time.  The TBA's decisions aroused heated
discussions, and although most patent lawyers agreed with the
direction taken by the TBA, many have pointed out a lack of
consistency in those decisions.  In this article, Schar endeavors to
systematize the decisions taken by the TBA into a new rationale for
delimiting what should be patentable.  His carefully crafted doctrine
seems to achieve this aim quite successfully.  It was reviewed by
colleagues still on the TBA, and it is perfectly in line with the
EPO's draft of a %(bp:Basic Proposal for the Revision of the European
Patent Convention) of July 2000, which proposed to remove the entire
list of patentability exclusions from Art 52 of the EPC.  Although the
doctrine may be consistent in itself, it conflicts with external
values such as freedom of written expression and economic policy goals
such as innovation and competition.  These goals seem better served by
the old %(q:natural forces) doctrine, which seems at least equally
consistent in itself and, in contrast to the new EPO doctrine,
perfectly in line with the written law.  Therefore Mark Schar needs to
find a way to refute the old natural-forces doctrine.  While direct
refutation may be difficult, for the purpose of convincing a
%(pm:congregation of believers), less powerful argumentative devices
might suffice.  Let's see how Mark Schar tackles the job:

#tap: The %(q:solution of a problem) belongs to present-day EPO doctrine. 
It was hardly ever explicitely mentioned in earlier writings. Instead
the %(q:use of natural forces to directly achieve a causally
overseeable effect) has been the dominant concept until changes were
introduced by the EPO's Technical Board of Appeal, in which the author
participated.  The author fails to properly explain this earlier
doctrine and to cite the relevant literature that explains it
adequately.  Instead he attacks some older writings that are less
authoritative and less well argued.

#Tea: This means that any practical application of pure thought (i.e.
applying the theorem of Pythagoras to architecture) is in principle
patentable.)

#IaW: If %(q:pure thought) is not technical, then the %(q:technical) sphere
is again limited to the realm of applied natural science, and the
%(q:natural forces) theory sneaks back in.

#Iat: If, on the other hand, the non-inventive objectivaton of %(q:pure
thought) by means of well-known embodiments such as the operation of a
computer is to be patentable, then pure thought becomes patentable. 
Not only is a basic priniciple of patent law that the claim should
reflect the invention, in this case pure thought.  Also, even if we
deviate from this principle and don't allow a claim to a thought
process but only to its non-inventive objectivisation as a computer
process, we have thereby in fact claimed the thought process, because
the universal computer is the standard logical device of modern man.

#wWs: we have cited and discussed the complete section iii.B here, and we
didn't find a single argument, let alone a valid one, for not
requiring that an invention should provide new insights into a
physical causality between means and effect.

#Irf: Interestingly enough, Schar also fails at this point so summarise any
arguments.  He can apparently build on a consensus of his readers, who
evidently are adherents of the %(pm:patent movement).

#Pat: Patent lawyers have largely agreed that they want to discard this
important definition, which provides the only way to stay in line with
art 52 EPC and, as Kolle pointed out in his %(gk:analysis of the
Dispositionsprogramm decision) in 1977, the only way to delimit
patentability and define technicity in a consistent and meaningful
manner.  They have discarded the concept of technicity, but, for PR
reasons, they still need to pretend that there is some continuity.

#Itt: In the elided sections, Schar restates the well-known EPO fallacies
about %(q:computer programs as such) vs %(q:computer programs with a
technical effect), which allow the EPO to decide about patentability
based on its definition of %(q:technical character), thus in effect
rendering the list of exclusions in art 52.2 meaningless.  Whether an
item is on this list or not has no effect on the EPO's patenting
practise.  A %(er:consistent and meaningful interpretation of art
52(2)) would not be difficult.

#Spl: Schar procedes to make clear what everybody can guess:  his
%(q:objective technicality concept) leads to the patentability of
anything man-made under the sun, including mathematical theorems,
computer programs, business methods, the presentation of information
and even data structures, provided that they can be put to some kind
of concrete use.  The latter is usually possible, thanks to the
universal computer.  Anything that runs on a universal computer is,
according to Schar's systematisation of recent (unsystematic) EPO
doctrine, patentable.  This corresponds to the %(gk:well-established
facts).

#TmW: This seems to mean that the great achievement rewarded by patent is
not the invention of mathematical methods as such, which is not
patentable, but only the ability to run an already invented
unpatentable mathematical method on a universal computer and the
ability to formulate it in terms of patent claims.

#Tot: The last subphrase seems redundant, because, according to Schar, the
restrictive clauses themeselves are construed as referring to mental
processes or metaphysical entities outside of the scope of
%(q:practical and repeatable problem solutions).  Whenever a
restriction seems to refer to something of practical relevance, the
EPO simply decides that the patent is not being granted on the
questionable object as such (which, in EPO jargon, has come to mean
%(q:a metaphysical entity related to that object)), but on a practical
solution related to it.  Usually this is all the EPO's customers will
ever want.  They are not coming to apply for ownership of metaphysical
entities but for monopolies on practical solutions).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: jwip-schar98 ;
# txtlang: en ;
# multlin: t ;
# End: ;

