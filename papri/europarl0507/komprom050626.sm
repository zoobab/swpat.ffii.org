To: Erik Josefsson <ehj@cleosan.com>, Jonas Maebe <jmaebe@ffii.org>, Jozef Halbersztadt <jothal@o2.pl>,	philippe.aigrain@wanadoo.fr, pelegrin@labri.fr, j.heald@ucl.ac.uk, aptiko@ffii.org, florian.mueller@nosoftwarepatents.com
Subject: advertising letter from MEP to MEP
From: PILCH Hartmut <phm@a2e.de>
--text follows this line--
Subject: urgent request to sign new software patent compromise amendments

Dear colleague,

On July 5th our Parliament will vote on the software patent directive
in 2nd reading.

You are probably well aware of this directive, as it has been subject
to extraordinarily intensive lobbying.  Many colleagues have been
receiving an average of about 3 requests for appointments every day.
A group of approximately 20 large companies with big patent
departments and big lobbying budgets has been creating an appearance
of universal support for the so-called "Common Position" on
"computer-implemented inventions".

You may also still remember the Parliament's unanimous request for a
restart (renewed referral), which was rejected by the European
Commission on Feburary 28th of this year.  There was a widespread
consensus in the Parliament that the Commission had not done its
homework and that the Council's "Common Position" was a deceptive
package, drafted by ministerial patent officials behind closed doors,
that did not reflect the will of the member states.  In the end, this
package was adopted by the Council presidency "for purely procedural
reasons", so as to avoid the creation of a "harmful precedent" that
could have giving national parliaments a bit more influence over the
Council's decisionmaking.

Meanwhile, the pro-patent lobbying efforts of seem to have paid off.
At last week's vote in the Legal Affairs Committee (JURI), the
attempts of the rapporteur Michel Rocard and the EPP shadow rapporteur
Piia-Noora Kauppi to limit patentability were mostly frustrated.  In
the plenary, it may become even more difficult to pass limiting
amendments, because, due to the special rules for 2nd reading in the
co-decision procedure, any absence will be counted as a vote in favour
of the Council.

We find this development rather alarming.

The facts remain as they were in February, when we asked for a renewed
referral: 

 - The Council's "Common Position" promises to exclude pure software
   and business methods from patentability, but fails to deliver on
   this promise.  Since 1997, when the objective stated in the
   Commission's Greenpaper was to harmonise European patenting
   practise with that of the United States and Japan, the proposed
   means for achieving this objective have not substantially changed.
   Only the wordings became ever more convoluted and deceptive.
   
 - If the European Parliament fails to reject or seriously amend the
   Coucil's proposal on the 5th of July, the enforcement of broad
   monopolies on trivial calculation rules and business methods will
   start immediately and be irreversible for years to come.  There is
   a widespread fear among software professionals and economists that
   this could be devastating to Europe's software landscape, in which
   70% of the employment is created by a few 100,000 SMEs who together
   own less than 10% of the granted software patents and are unable to
   play the patent portfolio game.

 - Europe's citizens and national parliaments need a European
   parliament that can say No.  When the quality of legislation that
   comes from the executive organs sinks below a certain threshold, we
   must pull the emergency brake.  If we say No more often, perhaps
   the citizens whom we represent will say so less often in future
   referenda about proposed constitutions.

For these reasons we urge you to sign and support the attached set of
compromise amendments.

These compromise amendments represent a conservative set of simple
rules, which are based on proven and widely accepted practises of
national courts.

The most essential rules are expressed in the following amendments:

 Nr. 8: effective exclusion of business methods
 Nr. 3: "technology" is "applied natural science"
 Nr. 4: the contribution must meet the requirements of patentability
 Nr. 9: forbid direct monopolisation of information objects
 Nr. 1: change "computer-implemented" to "computer-aided"

Under this proposal, computer-aided inventions in areas such as
computer tomography, automobile engine control, household appliances
etc are without any doubt patentable.  Any legitimate concerns about
possibly far-reaching wordings in the 1st reading can no longer have a
basis in our compromise set.

To fully understand the rationale of these amendments, please refer to
the attached discussion paper from the FFII, which, as we find, argues
rather convincingly for similar rules.

For the sake of the future of the European Union and the competitivity
of our knowledge economy, we hope very much that you can quickly agree
to sign our compromise set, so that it can be tabled on wednesday, the
29th of June.

Yours sincerely
