Subject: Support for Compromise Amendments

Dear colleague,

On July 6th we will vote on the software patent directive in 2nd
reading. We need your signature for a set of compromise amendments
which are designed to resolve the conflicts between the political
groups and enable the Parliament to reach a majority vote.

You may still remember the Parliament's unanimous request for a
restart (renewed referral), which was rejected by the European
Commission on Feburary 28th of this year.  There was a widespread
consensus in the Parliament that the Commission had not done its
homework and that the Council's "Common Position" did not reflect the
will of the member states.

Notwithstanding 3 months of confusion by intense lobbying, the facts
remain now what they were in February:

- The Council's "Common Position" promises to exclude pure software
   and business methods from patentability, but fails to deliver on
   this promise.  Since 1997, when the objective stated in the
   Commission's Greenpaper was to harmonise European patenting
   practise with that of the United States and Japan, the proposed
   means for achieving this objective have not substantially changed.
   Only the wordings became ever more convoluted and deceptive.
   
 - If the Council's proposal goes through without serious amendments,
   the enforcement of tens of thousands of broad and trivial software
   and business method patents will begin to escalate in Europe, as
   it has already in the USA, but with even more severe consequences.
   There will be no way back for many years to come. Each time an SME
   is hit or killed by a frivolous patent, they will know very
   well whom they have to thank.   
    
For these reasons we urge you to sign and support the attached set of
compromise amendments.

These compromise amendments represent a conservative set of simple
rules, which are based on proven and widely accepted practises of
national courts.

The most essential rules are expressed in the following amendments:

 Nr. 8: effective exclusion of business methods
 Nr. 3: "technology" is "applied natural science"
 Nr. 4: the contribution must meet the requirements of patentability
 Nr. 9: forbid direct monopolisation of information objects
 Nr. 1: change "computer-implemented" to "computer-aided"

Under this proposal, computer-aided inventions in areas such as
computer tomography, automobile engine control, household appliances
etc are without any doubt patentable.  Any legitimate concerns about
possibly far-reaching wordings in the 1st reading can no longer have a
basis in our compromise set.

To fully understand the rationale of these amendments, please refer to
the attached discussion paper from the FFII, which, as we find, argues
rather convincingly for similar rules.

For the sake of the future of the European Union and the competitivity
of our knowledge economy, we hope very much that you can quickly agree
to sign our compromise set, so that it can be tabled on wednesday, the
29th of June.

Yours sincerely
