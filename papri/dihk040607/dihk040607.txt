-----------------------------

= DIHK-Thema:
= EU-Richtlinie zum Patentschutz von computerimplementierten Erfindungen ("Softwarepatente")

Stand: 2004-06-07

== Wie ist es ?  Problemstellung 

Anfang der 70er Jahre wurde erstmals der Rechtsschutz von Software
diskutiert. Software war damals technisch und rechtlich gesehen etwas
Neues. Bald wurde Software ,,als solche" dem Schutz des Urheberrechts
unterstellt. Die Computerprogrammrichtlinie der EU stellte klar, dass
EU weit geringe Anforderungen an den Gehalt der Software
(Sch�pfungsh�he) gen�gen m�ssen, um Urheberrechtsschutz zu haben. Die
Richtlinie wurde in allen Mitgliedsstaaten umgesetzt (f�r Deutschland
siehe �� 2 Abs. 1 Ziff. 1 und 69 a ff. UrhG). Die Diskussion um
Softwareschutz geht aber weiter. Hintergrund ist, dass Software viele
Bereiche "revolutioniert". Was fr�her z. B. �ber mechanische Mittel
gesteuert wurde, wird heute mit Hilfe eines Chips mit integrierter
Software erledigt. Die Frage stellt sich daher, inwieweit derartige
Softwarel�sungen auch in den Patentschutz eingebunden werden k�nnen.

== Unterschied zwischen Urheber- und Patentschutz 

Das Urheberrecht sch�tzt die Software, (die Sch�pfung), als
solche. Dabei geht es nicht um das dabei erreichte Ziel oder
Ergebnis. Nur, wenn ein anderer die Software (und die
dahinterstehenden Programmierbefehle) genauso �bernimmt und kopiert,
wird eine Urheberrechtsverletzung bejaht. Werden andere
Programmbefehle und damit eine andere Software geschrieben, mit der
das gleiche Ergebnis erzielt wird z. B. Verbuchungen vorgenommen,
liegt keine Urheberrechtsverletzung der ersten daf�r entwickelten
Software vor. Anders beim Patent. Dieses sch�tzt die Erfindung in
ihrem Ergebnis. Kommt ein zweiter selbst mit Hilfe einer anderen
Programmbefehlskette zu dem gleichen Ergebnis und liegt Patentschutz
vor, ist das urspr�ngliche Patent verletzt. Aus diesem Grunde wird von
vielen Softwareentwicklern und Herstellern die Debatte um den Schutz
computerimplementierter Erfindungen �u�erst kritisch gesehen.


== Wie es sein sollte 

Zielsetzung des DIHK Der Schutz von Computerimplementierten
Erfindungen darf nicht die Software als solchen und niemals
Gesch�ftsmethoden oder -ideen und Trivialerfindungen
umfassen. Rechenabl�ufe und Algorithmen sind nicht
patentf�hig. Verh�ltnisse wie in den USA, wo auch Gesch�ftsmethoden
und Trivialerfindungen Patente erhalten mit erheblich negativen Folgen
f�r die dortige Wirtschaft, darf es in Europa nicht geben. Der
Technikbegriff muss daher f�r diesen Bereich den klaren Rahmen
vorgeben. Das System der gewerblichen Schutzrechte mit Ihren
verschiedenen Schutzzwecken macht Sinn und darf nicht ausgeh�hlt
werden. Patente sch�tzen technische Erfindungen, die eine
entsprechende Erfindungsh�he aufweisen. Diese Vorraussetzungen sind
daher zu bejahen, bevor in weitere Pr�fungen einzusteigen
ist. Computerprogramme als solche sollen nicht patentierbar sein. Die
Tatsache, dass sie auf dem Computer (Maschine) installiert und
angewendet werden, reicht nicht, ihnen "Technizit�t" zu verleihen!
F�r Computerprogramme als solche steht das Urheberrecht (siehe oben)
zur Verf�gung. Andererseits sollte f�r eine computergesteuerte
Werkzeugmaschine, die insgesamt patentierbar ist, Patentschutz m�glich
sein. Die Richtlinie muss daher ausgewogen sein und einerseits den
Wettbewerb sichern, andererseits im wirklich technischen
Erfindungsbereich einen sinnvollen Patentschutz gew�hrleisten. Dies
auch vor dem Hintergrund, dass Patentverletzungsprozesse teuer und
langwierig sind und daher der Qualit�t der patentanwaltlichen
Pr�fungen von Amts wegen eine hohe Verl�sslichkeit zukommen muss. Das
neueste Ratsdokument datiert vom 24. Mai 2004 (Interinstitutional File
9713/04) und gibt den derzeitigen gemeinsamen Standpunkt des Rates
wieder. Dabei sind Verbesserungen in Richtung der von uns pr�ferierten
L�sung erkennbar. 

== Was getan werden muss � Aktivit�ten 

* Stellungnahme des DIHK zum urspr�nglichen Entwurf v. 20.04.2002 
* Schreiben DIHK an Europaparlamentarier v. 18.09.2003
* Schreiben DIHK an BMJ v. 30.09.2003 
* Aktive Begleitung der weiteren Entwicklung 

== Wer macht mit � Meinungsbildner und Entscheidungstr�ger 

* Ansprechpartner im BMJ: Herr MR Dr. Welp 
* Ansprechpartner im BMWA: Frau Swantje Weber-Cludius, Frau Ortemeyer
* Ansprechpartner im DIHK: RA Doris M�ller, Fachbereich Recht, Tel.: 030/2 03 08 27 04


