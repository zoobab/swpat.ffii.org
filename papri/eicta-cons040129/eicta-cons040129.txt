Brussels, 16 February 2004

Proposal for a Directive on the patentability of computerimplemented inventions EICTA Response on the Irish Presidency Compromise text 5570/04 of 29 January 2004

EICTA1  commends   the   Irish   Presidency   in   proposing   a   compromise   text   which   incorporates   a significant number (24) of the amendments adopted by the European Parliament at the plenary vote on 24 September 2004, while avoiding the provisions which would be most harmful to innovative organisations including large companies and SMEs, as well as individual innovators, in many different industrial sectors.  Such companies are increasingly reliant on softwarerelated innovation in a broad range of products including mobile phones, medical instruments, cars, and domestic appliances etc. EICTA believes that the Irish Presidency's compromise proposal, subject to an important improvement in Article 5 discussed below, represents an acceptable basis on which to build the Council's  Common Position, although in many respects the newlyintroduced amendments make it less clear than the earlier " Common Approach" text endorsed by the Council in November 2002. The Annex includes a more detailed analysis of the individual amendments proposed by the Irish Presidency. As mentioned, however, there is one important respect in which the Irish Presidency's  compromise proposal leaves room for improvement, namely in Article 5 regarding " program product claims".   The current wording of Article 5(2) would change the law as it stands and would place unnecessary constraints on how patent claims are drafted which would be unique in the field of Computer implemented Inventions (CIIs).  These constraints would complicate and increase the cost of drafting patents particularly for small and medium size enterprises and individual inventors, and would render enforcement more difficult and uncertain. 

1  EICTA  European Information, Communications and Consumer Electronics Technology Industry Association  combines

46 major multinational companies as direct members and 31 national associations from 21 European countries. EICTA altogether represents more than 10,000 companies all over Europe with more than 1.5 million employees and revenues of over 190 billion Euro.

D i a m a n t 8 0 B l d .

B u i l d i n g A . R e y e r s ( B e l g i u m )

7 0 6 7 0 6 7 0 6 8 4 8 4 8 4 7 0 8 0 7 9

A . R e y e r s l a a n

B - 1 0 3 0

1

B r u s s e l s

T : T : F : + 3 2 + 3 2 + 3 2 2

2 2

i n f o @ e i c t a . o r g w w w . e i c t a . o r g


Ideally, the Council should adopt language for Article 5(2) similar to Amendment 18 in the McCarthy Report from the Legal Affairs Committee of the European Parliament, which was unfortunately not adopted by Parliament, as follows: " Article 5.2.   A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer,   computer   network   or   other   programmable   apparatus,   implement   a product or carry out a process patentable under Articles 4 and 4a." A Council Common Position based on the based on the Common Approach or, failing that, on the Irish Presidency compromise proposal, but incorporating the above language on program product claims would best reflect existing practice in Europe.   EICTA is very conscious that the current legal framework for CIIs in Europe is serving all stakeholders well, including not only large companies, but also SMEs, and individual inventors. And, the Open Source community has certainly flourished in this environment.   EICTA does not want to see the delicate balance reflected in the status quo disturbed, i.e. the scope of what is currently patentable should neither be increased nor reduced.  EICTA believes that it would be better to have no directive at all than a directive which would damage European industry. o Recital 7b.   This implies that the current legal climate may not be conducive to investment and innovation in the field of software. Furthermore it is not clear what is meant by different interpretations of the provisions of the EPC. Strictly speaking, it is only open for the EPO to interpret its provisions.  The Patent Offices and courts in the Member States do not interpret the EPC as such, but their own respective national versions of it.

o

Recital 13a.   This introduces a new term " data processing method",  but without any definition of what that term means.     It would be clearer to revert to the original Common Approach text by deleting this term.  

o Recital  13b.    To  the   extent  that  this  means  that  you  cannot  save  an   otherwise unpatentable invention by reciting a technical means, EICTA agrees. However, the language seems to point away from the practice established in current jurisprudence of analysing patentability by looking at the claim as whole.  EICTA cannot support any approach to assessing patentability based on dividing claims into novel and known features. o Recital 13c. However the word "foreseen" should be replaced by "claimed". o Recital 13d.  This seems to be inconsistent with Article 5.2.

D i a m a n t 8 0 B l d .

B u i l d i n g A . R e y e r s ( B e l g i u m )

7 0 6 7 0 6 7 0 6 8 4 8 4 8 4 7 0 8 0 7 9

A . R e y e r s l a a n

B - 1 0 3 0

2

B r u s s e l s

T : T : F : + 3 2 + 3 2 + 3 2 2

2 2

i n f o @ e i c t a . o r g w w w . e i c t a . o r g


o Recital 14. But the word "currently" should be inserted before "unpatentable" in the last sentence.  EICTA would strongly prefer " trivial" to be replaced with "non technical"  in line with COM, E, S, P, IRL. o Recital 16.  The logic and purpose of the second sentence is unclear. 

o Recital 17. The original wording conveyed more legal certainty, i.e. "shall"  instead of " should".

o

Articles 4.1 and 4.2.  Seem to be duplicative. The version of Article 4 in the Common Approach seems more elegant.

o Article 7.  The word "especially" should be replaced by "including".  The impact of the directive should be monitored for all portions of the European business and user communities.  While small and medium enterprises and the open source community are important groups to include in the scope of monitoring activity, the monitoring of the directive's  impact should avoid focus on certain interest groups and not others. o Article 8(b). It is not appropriate to consider the term of the patent only in the context of CIIs. The term of he patent has to be the same for all technologies.

o

o

Article 8(ca). It is not clear what this provision really means and what the Commission is being called to report on.  Articles 8(cb) and 8(cc). The value of these provisions is questionable. Even if the Commission reported on such aspects it would not have the competence to follow through.

D i a m a n t 8 0 B l d .

B u i l d i n g A . R e y e r s ( B e l g i u m )

7 0 6 7 0 6 7 0 6 8 4 8 4 8 4 7 0 8 0 7 9

A . R e y e r s l a a n

B - 1 0 3 0

3

B r u s s e l s

T : T : F : + 3 2 + 3 2 + 3 2 2

2 2

i n f o @ e i c t a . o r g w w w . e i c t a . o r g


