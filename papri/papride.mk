papride.lstex:
	lstex papride | sort -u > papride.lstex
papride.mk:	papride.lstex
	vcat /ul/prg/RC/texmake > papride.mk


papride.dvi:	papride.mk papride.tex
	latex papride && while tail -n 20 papride.log | grep references && latex papride;do eval;done
	if test -r papride.idx;then makeindex papride && latex papride;fi
papride.pdf:	papride.mk papride.tex
	pdflatex papride && while tail -n 20 papride.log | grep references && pdflatex papride;do eval;done
	if test -r papride.idx;then makeindex papride && pdflatex papride;fi
papride.tty:	papride.dvi
	dvi2tty -q papride > papride.tty
papride.ps:	papride.dvi	
	dvips  papride
papride.001:	papride.dvi
	rm -f papride.[0-9][0-9][0-9]
	dvips -i -S 1  papride
papride.pbm:	papride.ps
	pstopbm papride.ps
papride.gif:	papride.ps
	pstogif papride.ps
papride.eps:	papride.dvi
	dvips -E -f papride > papride.eps
papride-01.g3n:	papride.ps
	ps2fax n papride.ps
papride-01.g3f:	papride.ps
	ps2fax f papride.ps
papride.ps.gz:	papride.ps
	gzip < papride.ps > papride.ps.gz
papride.ps.gz.uue:	papride.ps.gz
	uuencode papride.ps.gz papride.ps.gz > papride.ps.gz.uue
papride.faxsnd:	papride-01.g3n
	set -a;FAXRES=n;FILELIST=`echo papride-??.g3n`;source faxsnd main
papride_tex.ps:	
	cat papride.tex | splitlong | coco | m2ps > papride_tex.ps
papride_tex.ps.gz:	papride_tex.ps
	gzip < papride_tex.ps > papride_tex.ps.gz
papride-01.pgm:
	cat papride.ps | gs -q -sDEVICE=pgm -sOutputFile=papride-%02d.pgm -
papride/papride.html:	papride.dvi
	rm -fR papride;latex2html papride.tex
xview:	papride.dvi
	xdvi -s 8 papride &
tview:	papride.tty
	browse papride.tty 
gview:	papride.ps
	ghostview  papride.ps &
print:	papride.ps
	lpr -s -h papride.ps 
sprint:	papride.001
	for F in papride.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	papride.faxsnd
	faxsndjob view papride &
fsend:	papride.faxsnd
	faxsndjob jobs papride
viewgif:	papride.gif
	xv papride.gif &
viewpbm:	papride.pbm
	xv papride-??.pbm &
vieweps:	papride.eps
	ghostview papride.eps &	
clean:	papride.ps
	rm -f  papride-*.tex papride.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} papride-??.* papride_tex.* papride*~
tgz:	clean
	set +f;LSFILES=`cat papride.ls???`;FILES=`ls papride.* $$LSFILES | sort -u`;tar czvf papride.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
