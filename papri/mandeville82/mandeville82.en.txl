<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Mandeville et al 1982: Economic Effects of the Australian Patent
System

#descr: A Commissioned Report to the Industrial Property Advisory Committee. 
Contains statistics about the use of the patent system as a source of
information and as a source of revenues.  Its general reasoning and
conclusions are similar to those of most economists, especially Fritz
Machlup.  Some quotations: %(bc|Since the benefits of the patent
system are so tenuous and subtle and the overall benefit/cost ratio is
considered to be negative, there is no economic justification for
extending patent monopolies by lengthening the term, or by widening
the grounds for either infringement, or %(tepe|patentability|for
example, plant variety rights or computer programs).  However, in the
light of our findings, there is considerable economic justification
for policy action to reduce the negative effects of the patent system
by stricter examination, by reducing the length of term and the scope
of patent monopolies, and by action to deal with undesirable
restrictive practices in patent licensing.|An historical awareness of
the political economy of patent reform suggests that this task is not
easy at the domestic policy level. This is basically because those who
perceive they would lose by such reform are concentrated, powerful and
active defenders of their interests.  In contrast, those who would
gain by patent reform are diffuse and hardly aware of their interest
in the matter.)  Apparently for the latter reason, this report shys
away from officially recommending what the facts really suggest: 
abolishing the patent system.  Only a few years later, the Australian
Patent Office decided to make %(q:software-related inventions)
patentable.

#sno: second source

#MWv: Main Sources of Technological Information for Engineers Involved with
the Patent System

#Sea: Statistics on Value of Patent Publications to Industry

#Het: History: Controversy over Innovators' Privileges

#Osi: Patent System's Negative Impact, Scope must be Restricted

#Lta: Lamberton Statement: Recommendations Softened by Political Pressure

#TWe: Technical and trade journals

#Iar: Informal contact with other organizations

#Ccs: Conferences and seminars

#Iau: Informal contact within your organization

#Vuu: Visits outside Australia

#Nra: Newspapers and magazines

#Gea: Government departments

#Cns: Courses in edu. institutions

#Cea: Computer data bases

#IWg: Internal training courses

#PWi: Patents specifications

#Tna: TV and radio

#FW2: First ranked source given a weighting of 3, second ranked source a
weighting of 2, third ranked source a weighting of 1.

#Mog: Main Reasons for Companies Consulting Patent Information

#Cni: Check on potential patent infringements

#Chi: Consider new products or processes which could be manufactured or used
with or without a licensing agreement

#Atk: Assess the state of the art before embarking on a R&D project

#Aoh: Assess the novelty of an invention with a view to patenting

#Cao: Check on what competitors are doing

#Scp: Solve technical problems

#Irt: In England by the late 16th century, patents had regularly been
granted by the Crown for the encouragement of invention and
innovation.  But Royal prerogative in this as in other matters soon
degenerated to near total misuse with patent monopolies being granted
to reward favourites and to help consolidate the power of the Crown. 
As restrictive patent monopolies began to cover such daily necessities
as salt, oils, vinegar, starch and saltpeter, public outcry became
great.  Eventually pressures exerted via the House of Commons and the
Courts led to the next major development in the world history of the
patent system: the English Statute of Monopolies of 1623.  This
Statute which declared monopolies to be void under common law, made an
exception for invention patents.  Patent law was thus contradictory
from the beginning.

#Bhu: By the end of the 18th century, the U.S. and France had established
patent law based on the English Statute of Monopolies.  In the early
decades of the 19th century many European countries adopted formal
patent laws.  However, controversy and debate on the patent system
heightened as the 19th century progressed.  Indeed for a few years it
appeared as if the patent abolitionist movement, which was linked to
the free trade movement would prevail.

#Pgc: Parliamentary committees and royal commissions investigated the
operation of the British patent system in 1851-52, in 1862-65, and
again in 1869-72.  The findings of these reviews lent considerable
weight to the arguments of the patent abolitionists: %(q:Some of the
testimony before these commissions was so damaging to the repute of
the patent system that leading statesmen in the two Houses of
Parliament proposed the complete abolition of patent protection.)
However, compromise prevailed and the eventual Patent Reform Bill that
arose out of the 1872 Commission's Report advocated changes to patent
law that would significantly weaken the harmful effects of patent
monopolies: %(q:... a reduction of patent protection [from fourteen
years] to seven years, strictest examination of patent applications,
forfeit of patents not worked after two years, and compulsory
licensing of all patents.)  This Bill was passed by the House of
Lords.

#Wos: While ultimately a matter of judgement, this study leaves little room
for doubt that the benefit/cost ratio of the patent system in
Australia is negative, or at the very best, in balance.  However, this
conclusion does not necessarily imply an economic justification for
abolishing the patent system.  The costs and benefits of an
institution need to be distinguished from the costs and benefits of
abolishing that institution.  In the perspective of the national
economy, the economic effects - both costs and benefits - of the
patent system in Australia are quite modest.  However, the costs of
the unilateral abolition of the patent system to Australia's
international commercial relations could possibly be much larger; the
Swiss experience outlined in the historical section of Chapter 2 is
pertinent.

#SbW: Since the benefits of the patent system are so tenuous and subtle and
the overall benefit/cost ratio is considered to be negative, there is
no economic justification for extending patent monopolies by
lengthening the term, or by widening the grounds for either
infringement, or patentability (for example, Plant VAriety Rights or
computer programs).  However, in the light of our findings, there is
considerable economic justification for policy action to reduce the
negative effects of the patent system by stricter examination, by
reducing the length of term and the scope of patent monopolies, and by
action to deal with undesirable restrictive practices in patent
licensing.

#Acn: An historical awareness of the political economy of patent reform
suggests that this task is not easy at the domestic policy level. This
is basically because those who perceive they would lose by such reform
are concentrated, powerful and active defenders of their interests. 
In contrast, those who would gain by patent reform are diffuse and
hardly aware of their interest in the matter.  Again a pertinent
parallel could be drawn with the tariff issue.  Furthermore, since the
patent system's costs and benefits cannot be measured precisely,
%(q:the optimum limits of the patent system, whether with respect to
time, space, patentability or restrictions on the use of the grant
must always remain a subject of controversy.  There is no doubt,
however, that the costs have been underestimated).  For these reasons,
as well as the patent system's intrinsic international nature, patent
reform is best pursued in international forums - such as the
conferences for the revision of the Paris Convention.  This need not
preclude unilateral action by Australia whenever such action is deemed
practically appropriate or feasible.  There is now ample economic
justification for measures which might be taken to reduce the costs of
the patent system in Australia.

#Prt: Prof Lamberton was the only economist working on this report.  He
criticised that the report politically compromised its findings by
shying away from the drastic recommendations that could have been the
only intellectually honest consequence of its findings:

#Tcc: This report does not live up to its claim to have adopted economic
perspectives and to have applied economic criteria.  It has not
consistently applied economic criteria; it has not made use of the
available empirical evidence; and the concept of social cost, so
frequently mentioned, has never really been fully grasped.

#Ttz: The sensible objective is rightly declared to be %(q:to modify the
Australian patent laws, adjusting the length, strength and breath of
patent rights) to maximize the net benefit.  It is unfortunate that
the Report soon strays from this path.

#NWs: No amount of talk about individual patent successes nor about a future
in which the Australian economy has magically become progressive,
innovation-oriented, and competitive on the world scene, can hide the
fact that Australia exports little in the way of manufactured goods
and has few innovations for sale.  Most patents are granted to
overseas firms.  To make the most of this situation, Australia needs
to reduce the social costs to the extent possible without inhibiting
innovation and without provoking international retaliation.  As a
small nation, there is scope for such action.  The constraints of the
[Paris] Convention are largely a myth.

#TWW: To acknowledge the circumstances of the Australian economy and to seek
such a balancing of social costs and dynamic benefits is to reject
much of this Report.  In particular, it points to:

#rar: reduction of standard patent term [from 16] to 10 years;

#smc: some freeing of import competition from the restrictions patents
permit ...

#maa: making sure that provisions such as compulsory licensing and
re-examination can function effectively;

#ase: avoiding the restrictive consequences and additional social costs that
can arise if the scope of the patent system is extended unnecessarily
in the development of the information economy;

#Oho: Only a few years after this report was published, the Australian
Patent Office spontaneously decided to allow patents on %(q:software
related innovation).

#wno: weakening the professional patent attorney monopoly of costly advice;

#stk: significantly improving the educational requirements for those working
within the patent system; and

#cwt: clarifying the extent to which Patent Office operations are to be
subsidized.

#Tnt: This report is not an imaginative one.  It is constrained by the very
%(q:haze of assumptions about rights and rewards for inventors,
special pleading by those directly involved, and a plethora of legal
procedures and criteria in the Patents act) that it deplores.  Many of
its recommendations are for no change; and when change is implemented
it is all too often merely procedural or has little prospect of being
effective.  A good opportunity to adjust an ancient institution to the
current needs of the Australian economy has been missed.

#Aec: A governmental study that finds no benefit and quite a lot of harm in
software patents and then jumps to recommend introducing them.  This
and other studies of this type show how political pressures have
continuously been compromising governmental study reports and caused
their recommendations to be inconsistend with their findings.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: mandeville82 ;
# txtlang: en ;
# multlin: t ;
# End: ;

