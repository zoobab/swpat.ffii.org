<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FTC 2002: Hearings on Anti-Competitive Effects of Patents

#descr: In the USA, recent trends toward extensive patentability and rigid IP
enforcement laws have given rise to complaints about chilling effects
on innovationn and competition.  In particular, concerns were raised
about standardisation being disrupted by patents. Some people proposed
to mandate by law that that standards be freely available and that any
relevant submarine patents be disclosed early.  In spring of 2002 the
Federal Trade Commission conducted an expert hearing on this subject. 
Representatives of standardisation bodies explained that the proposed
legal requirements could backfire, because they would deter companies
from participating in standardisation.  Some economists and
programmers asked for restrictions on what can be patented or how
patents can be used.  Representatives of the USPTO and patent lawyers
argued that everything was just fine and competition law should not
interfere with patent rights.  FTC has published many testimonies and
reports as PDF files on its website.  We are presenting an overview of
these texts and what can be learnt for them for the patentability
discussion.

#22e: 2002-02-28 session

#_201: 2002-03-20 and 04-11 sessions

#VWr: Various speakers at the US Government's Federal Trade Commission
dramatically point out the harm done by the patent system and in
particular software patents in the US.

#Cen: Complains that the patenting consumes ressources of CISCO and
innovative companies in software-related fields without promoting
innovation, and in fact penalises innovators, asks for restriction of
patentability to fields where it can be shown that patents benefit
society.

#Wto: We have not yet evaluated most of the speeches although they look very
interesting.  Just by chance we stumbled on a very statement of the
IPR section head of CISCO, which shows how software patents are
working.

#Tti: The session transcripts we have read so far are the afternoon of March
20th 2002 and the morning of 11th April 2002. Here are the notes for
some random documents.

#TWy: Their patent policy is optional disclosure and Uniform-Fee-Only
(UFO/RAND) commitment, following ANSI.

#Toa: They don't want the government to force standards to be free or to
make full disclosure of pending patents required.  Such requirements
would be ineffective and place a further burden on standardisation
participants.

#Asu: Against life (gene) patenting and DMCA. Patents may disincentive
further reasearch and discourage exchange of information among
scientists.

#Ruo: RedHat against software and biz patents. Complains of difficulty of
search for prior art and too long term of patents. Does not want
patents to be made stronger than competition law.

#Tee: Texas Instruments feels very comfortable with patent thickets, broad
patents, licensing, etc, thinks patentees should not be forced to
disclose essential patents for standards. The USPTO works well but
needs more funding/staff.

#SaK: Speech at Cambridge (UK) 2002-03-12

#fdc: (I've only read the conclusion). Worried about narrowing of claims to
protect competition. Proposes very strong patents

#fgu: (I've only read introduction). Builds a model showing patents for 
drugs benefit consumers in the long run. Suggests the model could be
applied to software.

#HWU: Head of US Patent Office

#IaW: Impressive collection of common patent myths. Patents are not
monopolies, so antitrust laws should not apply. Patents are more
important than competition laws, since patents are in the US
constitution.  Expansion of subject matter is good because the US
leads new subject matter worldwide. Expansion of subject matter was a
great success, as can be seen from the soaring number of patent
applications.

#Suo: Short text criticizing patent quality (many patents not new, not
useful, not reworkable etc).

#Wee: Worried about differences between USA and Europe in patent and
competition law.  Thinks european directive proposal is more
restrictive than US law.  Tells about ETSI patent policy for standards
changed due to US corporations pressure, says a balance must be found,
no opinion on how it should be done.

#Goa: Game-thoeretical paper

#sil: shows preliminary injunctions in trials for patent infringement cause
patent inflation by incentivating trivial patenting as a menas to
obtain temporary monopoly while the trial lasts, even if it is lost
eventually. With preliminary injuctions, both companies in trial may
get some benefit at the expense of consumers.

#JnD: John T. Mitchell and Video Software Dealers Association.

#PsC: Possibly interesting piece on Copyright abuse.

#AIP: AIPLA

#Aty: American Intellectual property Lawyers association.

#Uls: Unsurprinsing. Patent scope is adequate, antitrust laws should not
%(q:artificially) restrict patent rights, the courts should be free to
decide, PTO should be given more money, etc.

#Afn: Antitrust lawyer asks for guidelines for standards setting bodies.

#PFw: Proposes uniform-fee-only (UFO) style guidelines with few restraints
on participants.

#IjW: Independent programmer complains about trivial patents, giving a
couple of examples he's come accross.  asks for abolition of software
and business method patents.

#Pth: Proposes a criteria to decide when to apply antitrust law or patent
law, according to the role patents or competition have in each
different indsustry. For software it agrees than patents do not
incentivate innovation, and competition does, but it says the patent
system should not be changed, instead antitrust law should be applied
to software patents (and other). Although it is full of citations and
arguments, it seems to give no reason for not tuning the patent system
so that software patents settlement and litigation problems are
avoided upfront.

#Apo: Antitrust lawyer propses to see whether patent pools harm competition
or not by imagining whether there would be competition or not without
the patent pool.  Does not talk of software.

#Ntn: Nice piece on intellectual property difference with material property,
and the abuses of copyright. Also explains software is information and
math, and patents are unsuitable and dangerous.  Points to harms of
monopolism in software and music area, which are induced or aggravated
by IP laws.

#Rir: Review of European caselaw on essential facilities doctrine and
compulsory licensing in the area of copyright.

#Ptt: Proposes abolishing patent examination and shifting to a registration
system with penalties for patents which are invalidated in court.
Opposes adapting patent law to different subject matter, or investing
more money on patent examination.

#Dld: Defend current practice (ANSI like) in standards settings and opposes
proposed requirements

#Ynn: You can patent that?

#Ani: Analyses extension of patentable subject matter in the US, concludes
there are no benefits to software and biz patents, asks for stronger
obviousness requirements, more funding for the PTO, less burden to
invalidate, etc.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ftc02 ;
# txtlang: en ;
# multlin: t ;
# End: ;

