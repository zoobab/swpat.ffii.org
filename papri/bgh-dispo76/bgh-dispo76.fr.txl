<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vlt: Texte Intégral

#DmD: Le 28 octobre 1971, l'Office Allemand des Brevets, à la suite d'un
recours, a décidé d'accorder un brevet qui avait été déposé le 20
décembre 1960 pour un procédé de détermination des variations d'une
grande quantité de variables à l'aide d'un ordinateur - par exemple
pour la résolution de tâches de présentation de l'information. Voici
la revendication numéro 1 de ce brevet:

#VEt: Un procédé permettant la détermination des modifications apportées à
un grand nombre de variables pricipales et de variables secondaires,
au cours duquel les variables principales se mettent en place
progressivement à l'aide d'un ordinateur comportant au moins deux
unités de sauvegarde en entrée et autant en sortie, dans lequel ...,
caractérisé par le fait que ...

#Deu: Les prétentions de 2 à 5 concernent d'autres aspects caractéristiques
du procédé candidat.

#AWs: A la suite de la plainte exprimée par le requérant, la Cour Fédérale
des Brevets a annulé la décision précédente et refusé le brevet.

#Deb: Un nouveau recours fut admis auprès de la Cour mais conduisit à un
échec.

#Dii: La Cour Fédérale des Brevets motiva son refus par le fait que pour
l'essentiel le procédé candidat n'était qu'un plan d'organisation qui
en tant qu'(q:instruction à l'esprit humain) ne comporte pas de
caractère technique. Cette règle d'organisation était ainsi faite
qu'elle n'utilisait que les possibilités des ordinateurs conformes à
l'état de l'art. Le poids de l'invention ne repose donc pas sur le
domaine technique : pour sa mise au point aucune considération d'ordre
technique n'est nécessaire.

#Dee: Les attaques des plaignants demeurent sans succès.

#Dbh: Présenté comme nouveau et créatif, le corpus faisant l'objet de la
plainte n'appartient pas au domaine technique.

#EpW: Il s'agit là d'une règle qui si on la suit d'une manière mécanique
permet la résolution de tâches, c'est exactement ce que l'on appelle
un algorithme dans le vocabulaire mathématique. Le fait que cette
règle de calcul soit formulée différemment - en l'occurrence dans un
discours qui mêle étroitement les considérations techniques sur
l'installation et le déroulement du procédé - n'y change rien. Ce
n'est pas en effet l'enveloppe discursive qui peut faire la différence
entre un corpus de nature technique et un corpus de nature non
technique, mais bien son contenu professionnel. Par conséquent,
l'objection soulevée n'est pas fondée, quand bien même la décision
contestée aurait mal tenu compte des termes exacts des prétentions du
candidat.

#Dts: Considérée en tant que telle à travers l'argumentation exposée
oralement par la plaignante au cours des débats devant la Chambre
%(q:senat) des Brevets, cette règle d'organisation et de calcul n'a
pas de caractère technique. Et ce concept de technicité, que la
jurisprudence et la littérature ont jusqu'à présent considéré comme le
critère essentiel de démarcation permettant de distinguer les
prestations brevetables de celles qui ne peuvent obtenir cette forme
de protection, a vu récemment son contenu explicité par le Tribunal
compétent dans sa décision publique BGHZ 52,74 = GRUR 1969,672, ou
%(q:Rote Taube). D'après ce texte, un corpus doit être considéré comme
brevetable s'il constitue un schéma industriel capable d'atteindre un
résultat par des liens prévisibles de causalité et grâce à
l'intervention des forces de la nature que l'on peut diriger. Il n'est
pas douteux qu'une simple règle d'organisation ou de calcul, comme
celle qui est ici candidate au brevet, peut elle aussi constituer un
enselble d'instructions pour un procédé industriel, et que si l'on
suit ces instructions il est possible d'arriver à un résultat par des
liens prévisibles de causalité. Simplement, il manque l'intervention
des forces de la nature dans l'obtention du résultat. La règle
d'organisation ou de calcul représente déjà la résolution du problème.
Son application ne nécessite pas l'utilisation des lois physiques.
Elle démontre de quelle manière, par quels processus d'ordonnement et
de calcul, certains problèmes, comme par exemple des problèmes de
disposition, peuvent être résolus, sans pour cela qu'il y ait
nécessité d'intervention de moyens techniques pour son application.
D'après cette règle, toute personne ayant à sa disposition les
connaissances professionnelles et les connaissances mathématiques
nécessaires peut également s'avancer et venir à bout des tâches de
disposition d'une manière tout aussi certaine.

#Drn: Mais la la faculté d'entendement de l'homme ne constitue pas une loi
de la nature que l'on peut contrôler, relevant du concept énoncé dans
le texte cité. Il s'agit là uniquement des lois physiques qui se
situent en-dehors de l'entendement humain et qui peuvent être
contrôlées à l'aide de cette faculté d'entendement. Si ce n'était pas
le cas, c'est l'ensemble de la pensée humaine qui devrait être
qualifiée de technique, et le concept de technicité perdrait
complètement sa signification spécifique et discriminante. Le fait que
l'inventeur ait proposé d'installer certains moyens techniques en tant
que moyen pratique de mise en oeuvre de sa règle d'organisation et de
calcul et que pour cette raison il énonce cette règle à travers son
déroulement confié à la structure et à l'efficacité d'un ordinateur,
ne change rien au caractère non technique de la règle en elle-même. Le
fait que des moyens techniques puissent s'avérer nécessaires, ou
simplement utiles, au traitement approprié de la règle, est isi sans
incidence particulière. Car la règle constitue en elle-même une
instruction abstraite et logique qui ne peut nullement devenir
technique parce que son application nécessite des moyens techniques,
qu'il s'agisse là de la machine à écrire ou d'un ordinateur. Que des
moyens techniques soient utilisés à l'occasion de l'application d'un
corpus non-technique, cela ne saurait suffire à le rendre technique;
l'utilisation de moyen technique doit pour cela être partie intégrante
de la solution du problème, elle doit concourir directement au
résultat escompté et ne saurait faire défaut sans que le résultat
escompté fasse défaut lui aussi.

#Dmc: La Cour Fédérale des Brevets a énoncé le caractère non technique de la
règle de calcul par le fait qu'aucune %(q:considération relevant du
domaine technique) n'a été nécessaire pour sa mise au point.
Manifestement, on ne parle pas ici du chemin subjectif parcouru par la
pensée de l'inventeur, mais des sauts logiques nécessaires à l'exposé
du corpus. Même si la partie plaignante a, à tort, combattu cette
argumentation qu'elle qualifie de considérations historisantes, elle
n'en conduit pas moins à la conclusion que les règles d'organisation
et de calcul ne sont pas en elles-mêmes d'une nature technique.

#Dde: La jurisprudence a caractérisé le concept de technicité par sa
relation au monde sensible par opposition au monde de l'esprit (RG
GRUR 1933, 289, 290 --- Table de multiplication). D'après la décision
%(q:Wettschein) de la Cour Fédérale de Justice (GRUR 1958, 602), une
invention technique est établie si elle fournit une méthode pour
résoudre un problème technique par des moyens techniques déterminés,
et ainsi obtenir un résultat de nature technique. D'après la décision
%(q:Rote Taube), le tribunal signataire a exprimé cette définition
comme pouvant comprendre l'utilisation d'autres lois de la nature
comme par exemple celles de la biologie, à côté de celles de la
physique et de la chimie (voir également RG GRUR 1933, 289, 290).

#Sai: Cependant, dans tous ces textes, l'utilisation planifiée de forces de
la nature contrôlables est considérée comme une condition nécessaire
pour que l'agrément soit donné au caractère technique d'une invention.
Ainsi que nous l'avons exposé plus haut, l'inclusion des forces de la
raison humaine en tant que telles dans le domaine des forces de la
nature dont l'utilisation pour la création d'une innovation fonde son
caractère technique, aurait pour conséquence directe l'attribution
d'une signification technique à toutes les activités de la pensée qui
en tant que série d'instructions sont susceptibles de causer un
résultat d'une manière prévisible. A partir de là, le concept de
technicité perdrait son rôle de critère, l'ensemble des réalisations
de l'intelligence humaine - dont les l'envergure et les limites sont
inconnues et imprévisibles - se verraient ouvrir les portes du droit
des brevets.

#Eii: De plus, on peut avoir de bonnes raisons de conclure que, étant donné
l'unanimité avec laquelle la jurisprudence et la littérature ont
constamment insisté sur les limitations de la brevetabilité aux
inventions de nature technique, on peut parler d'un droit coutumier à
ce sujet.

#Dnu: Mais peu importe finalement. En effet le concept de technicité
apparaît comme le seul qui puisse servir à distinguer clairement ce
qui est brevetable de ce qui relève plutôt d'une autre forme de
production intellectuelle de l'homme pour laquelle le brevet n'est ni
approprié ni envisagé. Si l'on devait renoncer à cette ligne de
partage, il n'y aurait plus, par exemple, de possibilité certaine de
faire la différence entre les les prestations brevetables et celles
pour lesquelles le législateur a attribué d'autres formes de
protection, en particulier le droit d'auteur. Le système allemand de
propriété industrielle et droit d'auteur repose essentiellement sur le
fait que certaines formes de protection sont valables pour certaines
catégories précises de prestations intellectuelles et qu'il faut
éviter autant que possible les recoupements entre ces différents
droits de protection. La loi sur les brevets n'a tout de même pas été
conçue comme une loi à tout faire au sein de laquelle les catégories
de prestations intellectuelles pour lesquelles rien n'a été prévu par
la loi pourraient trouver une protection, mais comme une loi
spécialisée, visante à protéger un ensemble clairement délimité de
prestations intellectuelles : les prestations techniques, et elle a
toujour été comprise et appliquée comme telle.

#Ene: Par conséquent, il faut empêcher le système de protection des biens
intellectuels d'obtenir une extension des limites de la technicité,
concept qui serait alors détourné de son rôle. Il doit rester clair,
bien au contraire, qu'une règle d'organisation ou de calcul en
elle-même ne mérite pas de se voir protégée par un brevet si sa
relation au domaine technique ne repose que sur l'emploi d'un
ordinateur dans l'usage commercial qui en est fait. Il ne nous
appartient pas de débattre ici de l'éventuelle protection qui peut lui
être accordée soit par le droit d'auteur, soit par le droit de la
concurrence.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: obenassy ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh-dispo76 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

