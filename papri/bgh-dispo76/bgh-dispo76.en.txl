<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vlt: Full Text

#DmD: The German Patent Office has on 1971-10-28, after an opposition
proceding, decided to grant a patent for an application from
1960-12-20 concerning a process for determining changes in a
multiplicity of entities -- e.g. for solving disposition problems --
with help of a computer.  Claim 1 reads:

#VEt: Process for determining changes in a multiplicity of chief entities
and partial entities from which the chief entities are composed
stepwise, with help of a computer comprising at least two external
memory units on the input side and two such units on the output side,
such that ..., characterised by the fact that ...

#Deu: Claims 2 to 5 concern further elaborations of the applied-for process.

#AWs: On complaint by the opponent, the Federal Patent Court (BPatG) has
revoked the granting decision and denied the patent.

#Deb: Against this decision a legal complaint was admitted but remained
without success.

#Dii: The Federal Patent Court gave as a reason for denying the patent that
the applied-for process was essentially a pure organisation plan which
as a mere %(q:instruction to the human mind) has no technical
character.  This organisation rule was constructed in such a way that
it utilised the normal possibilities of a computer as known from prior
art.  Therefore the weight of the invention did not lie in the field
of technology and could on the whole be found without any need of
considerations that belong to the technical realm.

#Dee: The attacks of the legal complaint remain without success.

#Dbh: The teaching that is claimed to be new and inventive does not belong
to the realm of technology.

#EpW: It pertains thus to a rule, by whose schematic application analogous
problems can be solved, i.e. what in mathematical terminology is
called an algorithm.  Claiming this rule in connection with technical
features of an apparatus and a processing sequence does not make any
difference: whether a teaching is of technical nature or not does not
depend on the verbal clothing of that teaching but on its material
content.  Therefore the legal complaint's objection that the disputed
court decision did not strictly adhere to the claim wording is
mistaken.

#Dts: This rule of organisation and calculation, which, according to the
oral explanations given by the plaintiff before the court, is not by
itself the object of the patent application, has no technical
character.  How the concept of technology, which the caselaw and the
literature have always regarded as the decisive criterion for
distinguishing patentable achievements from non-patentable ones, is to
be defined in detail, has been most recently explained by this court
in the Rote Taube (red dove) decision.  According to this, a
patentable invention is a teaching for plan-conformant action
utilising controllable forces of nature for achieving a causally
overseeable result.   It is beyond doubt that a rule of organisation
and calculation, as described in this patent application, constitutes
an instruction for plan-conformant action and that the execution of
this instruction leads to a causally overseeable result.  However this
success (result) is not achieved by use of controllable forces of
nature.  It teaches, by which processes of ordering and calculating
certain problems such as disposition problems can be solved, where
this solution does not require the use of technical means.  A human
being equipped with the necessary commercial and mathematical
knowledge can use this rule to reliably solve the disposition problem.

#Drn: However human mental activity does not belong to the controllable
forces of nature in the sense explained above.  This concept refers
only to those forces of nature which lie outside the activity of the
human mind and are controlled by man with help of the human mind. 
Otherwise human thinking in its entirety would be subsumed under the
concept of technology, with the result that this concept would lose
its specific and distinctive meaning.  The fact that the inventor has
proposed to use technical means for practically executing the rule of
organisation and calculation and that for this reason he has
formulated the rule with the peculiarities and capabilities of the
computer in mind does not change the untechnical character of the rule
itself:  it is of no meaning that in order to put the completed rule
to practical application technical means can be used or should
preferably be used:  The rule, which by itself constitutes a
mental-logical instruction does not become technical by the fact that
during its application technical means -- be that the writing device
or the computer of the person who applies this rule -- are used.  It
is not enough that technical means are occasionally used for applying
an untechnical teaching; the use of these technical means must be an
integral part of the problem solution itself: it must serve to achieve
the causally overseeable result, and its non-use must inevitably cause
the aimed-for success to remain unachieved.

#Dmc: The Federal Patent Court has described the untechnical character of
the calculation rule by the formulation that for its finding no
%(q:considerations located in the technical realm) were necessary. 
Thereby it evidently did not mean the subjective mental path which the
inventors happened to have gone but the mental steps which, according
to the laws of logic, are objectively required.  This reasoning, which
the plaintiff unjustifiedly accuses of being a %(q:historicist
method), also leads to the result that the rule of organisation and
calculation is by itself not technical.

#Dde: The caselaw has characterised the notion of technology by its relation
to the world of phenomena, contrasting with the world of
reasoning/mind.  According to the Wettschein (betting certificate)
decision of the Federal Court, a technical invention is present, if an
instruction is given to solve a technical problem by using specific
technical means to achieve a technical result.  In the Rote Taube (red
dove) decision, this court generalised this definition so as to
accomodate other forces of nature than those of physics and chemistry,
e.g. those of biology.

#Sai: However in all cases the plan-conformant utilisation of controllable
forces of nature has been named as an essential precondition for
asserting the technical character of an invention.  As shown above,
the inclusion of human mental forces as such into the realm of the
forces of nature, on whose utilisation in creating an innovation the
technical character of that innovation is founded, would lead to the
consequence that virtually all results of human mental activity, as
far as they constitute an instruction for plan-conformant action and
are causally overseeable, would have to be attributed a technical
meaning.  In doing so, we would however de facto give up the concept
of the technical invention and extend the patent system to a vast
field of achievements of the human mind whose essence and limits can
neither be recognized nor overseen.

#Eii: It can furthermore be argued with good reasons that, given the
unanimity with which the jurisdiction and the legal literature have
always insisted on limiting the patent system to technical inventions,
the above reasoning constitutes a theorem of customary patent law.

#Dnu: Whether we want to postulate such a theorem is however not essential
for this discussion, because also from a purely objective point of
view the concept of technical character seems to be the only usable
criterion for delimiting inventions against other human mental
achievements, for which patent protection is neither intended nor
appropriate.  If we gave up this delimitation, there would for example
no longer be a secure possibility of distinguishing patentable
achievements from achievements, for which the legislator has provided
other means of protection, especially copyright protection.  The
system of German industrial property and copyright protection is
however founded upon the basic assumption that for specific kinds of
mental achievements different specially adapted protection regulations
are in force, and that overlappings between these different protection
rights need to be excluded as far as possible.  The patent system is
also not conceived as a reception basin, in which all otherwise not
legally privileged mental achievements should find protection.  It was
on the contrary conceived as a special law for the protection of a
delimited sphere of mental achievements, namely the technical ones,
and it has always been understood and applied in this way.

#Ene: Any attempt to attain the protection of mental achievements by means
of extending the limits of the technical invention -- and thereby in
fact giving up this concept -- leads onto a forbidden path.  We must
therefore insist that a pure rule of organisation and calculation,
whose sole relation to the realm of technology consists in its
usability for the normal operation of a known computer, does not
deserve patent protection.  Whether it can be awarded protection under
some other regime, e.g. copyright or competition law, is outside the
scope of our discussion.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh-dispo76 ;
# txtlang: en ;
# multlin: t ;
# End: ;

