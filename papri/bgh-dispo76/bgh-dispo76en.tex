\documentclass[11pt]{scrartcl}
\usepackage{hyperref}
\usepackage{epsfig}
\usepackage{array}
\sloppy
\pagestyle{plain}
\input{el2tex}
\input{coldef}
\begin{document}
\title{BGH 1976-06-22: Dispositionsprogramm\\
{\tt\normalsize http://swpat.ffii.org/vreji/papri/bgh-dispo76/indexen.html}}
\author{PILCH Hartmut}
\maketitle
\begin{abstract}
A landmark decision of the German Federal Court (BGH): 'organisation and calculation programs for computing machines used for disposition tasks, during whose execution a computing machine of known structure is used in the prescribed way, are not patentable.'  This is the first and most often quoted of a series of decisions of the BGH's 10th Civil Senate, which explain why computer-implementable rules of organisation and calculation (programs for computers) are not technical inventions, and elaborates a methodology for analysing whether a patent application pertains to a technical invention or to a computer program.  The Dispositionsprogramm verdict is especially famous for its final passages, in which it explains in general and almost prophetic terms that patent law is a specialised tool for rewarding innovators in an overseeable field, namely that of technical inventions, i.e. solving problems by phyisical causality rather than logical functionality.  Any attempt to loosen and thereby effectively remove this borderline would, even if not explicitely forbidden by law, lead onto an adventurous path from which any responsible judiciary must shy back.
\end{abstract}
\tableofcontents
\section*{}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
\begin{description}
\item[{\bf title}:]\ BGH-Beschluss 'Dispositionsprogramm' 1976-06-22
\item[{\bf source}:]\ BGHZiv\footnote{http://swpat.ffii.org/vreji/papri/indexen.html\#BGHZiv}\ 1977\ Bd\ 67\ p22ff;\ BGHZ\ 67,\ 22;\ Beschluss\ des\ X.\ Zivilsenats\ des\ BGH\ in\ der\ Rechtsbeschwerdesache\ X\ ZB\ 23/74
\end{description}\\\hline
\end{tabular}
\end{center}

The German Patent Office has on 1971-10-28, after an opposition proceding, decided to grant a patent for an application from 1960-12-20 concerning a process for determining changes in a multiplicity of entities -- e.g. for solving disposition problems -- with help of a computer.  Claim 1 reads:

\begin{quote}
Process for determining changes in a multiplicity of chief entities and partial entities from which the chief entities are composed stepwise, with help of a computer comprising at least two external memory units on the input side and two such units on the output side, such that ..., characterised by the fact that ... 
\end{quote}

Claims 2 to 5 concern further elaborations of the applied-for process.

On complaint by the opponent, the Federal Patent Court (BPatG) has revoked the granting decision and denied the patent.

Against this decision a legal complaint was admitted but remained without success.

The Federal Patent Court gave as a reason for denying the patent that the applied-for process was essentially a pure organisation plan which as a mere ``instruction to the human mind'' has no technical character.  This organisation rule was constructed in such a way that it utilised the normal possibilities of a computer as known from prior art.  Therefore the weight of the invention did not lie in the field of technology and could on the whole be found without any need of considerations that belong to the technical realm.

2. ...

The attacks of the legal complaint remain without success.

...

The teaching that is claimed to be new and inventive does not belong to the realm of technology.

...

It pertains thus to a rule, by whose schematic application analogous problems can be solved, i.e. what in mathematical terminology is called an algorithm.  Claiming this rule in connection with technical features of an apparatus and a processing sequence does not make any difference: whether a teaching is of technical nature or not does not depend on the verbal clothing of that teaching but on its material content.  Therefore the legal complaint's objection that the disputed court decision did not strictly adhere to the claim wording is mistaken.

This rule of organisation and calculation, which, according to the oral explanations given by the plaintiff before the court, is not by itself the object of the patent application, has no technical character.  How the concept of technology, which the caselaw and the literature have always regarded as the decisive criterion for distinguishing patentable achievements from non-patentable ones, is to be defined in detail, has been most recently explained by this court in the Rote Taube (red dove) decision.  According to this, a patentable invention is a teaching for plan-conformant action utilising controllable natural forces for achieving a causally overseeable result.   It is beyond doubt that a rule of organisation and calculation, as described in this patent application, constitutes an instruction for plan-conformant action and that the execution of this instruction leads to a causally overseeable result.  However this success (result) is not achieved by use of controllable natural forces.  It teaches, by which processes of ordering and calculating certain problems such as disposition problems can be solved, where this solution does not require the use of technical means.  A human being equipped with the necessary commercial and mathematical knowledge can use this rule to reliably solve the disposition problem.

However human mental activity does not belong to the controllable natural forces in the sense explained above.  This concept refers only to those natural forces which lie outside the activity of the human mind and are controlled by man with help of the human mind.  Otherwise human thinking in its entirety would be subsumed under the concept of technology, with the result that this concept would lose its specific and distinctive meaning.  The fact that the inventor has proposed to use technical means for practically executing the rule of organisation and calculation and that for this reason he has formulated the rule with the peculiarities and capabilities of the computer in mind does not change the untechnical character of the rule itself:  it is of no meaning that in order to put the completed rule to practical application technical means can be used or should preferably be used:  The rule, which by itself constitutes a mental-logical instruction does not become technical by the fact that during its application technical means -- be that the writing device or the computer of the person who applies this rule -- are used.  It is not enough that technical means are occasionally used for applying an untechnical teaching; the use of these technical means must be an integral part of the problem solution itself: it must serve to achieve the causally overseeable result, and its non-use must inevitably cause the aimed-for success to remain unachieved.

The Federal Patent Court has described the untechnical character of the calculation rule by the formulation that for its finding no ``considerations located in the technical realm'' were necessary.  Thereby it evidently did not mean the subjective mental path which the inventors happened to have gone but the mental steps which, according to the laws of logic, are objectively required.  This reasoning, which the plaintiff unjustifiedly accuses of being a ``historicist method'', also leads to the result that the rule of organisation and calculation is by itself not technical.

...

The caselaw has characterised the notion of technology by its relation to the world of phenomena, contrasting with the world of reasoning/mind.  According to the Wettschein (betting certificate) decision of the Federal Court, a technical invention is present, if an instruction is given to solve a technical problem by using specific technical means to achieve a technical result.  In the Rote Taube (red dove) decision, this court generalised this definition so as to accomodate other natural forces than those of physics and chemistry, e.g. those of biology.
However in all cases the plan-conformant utilisation of controllable natural forces has been named as an essential precondition for asserting the technical character of an invention.  As shown above, the inclusion of human mental forces as such into the realm of the natural forces, on whose utilisation in creating an innovation the technical character of that innovation is founded, would lead to the consequence that virtually all results of human mental activity, as far as they constitute an instruction for plan-conformant action and are causally overseeable, would have to be attributed a technical meaning.  In doing so, we would however de facto give up the concept of the technical invention and extend the patent system to a vast field of achievements of the human mind whose essence and limits can neither be recognized nor overseen.

...

It can furthermore be argued with good reasons that, given the unanimity with which the jurisdiction and the legal literature have always insisted on limiting the patent system to technical inventions, the above reasoning constitutes a theorem of customary patent law. 

Whether we want to postulate such a theorem is however not essential for this discussion, because also from a purely objective point of view the concept of technical character seems to be the only usable criterion for delimiting inventions against other human mental achievements, for which patent protection is neither intended nor appropriate.  If we gave up this delimitation, there would for example no longer be a secure possibility of distinguishing patentable achievements from achievements, for which the legislator has provided other means of protection, especially copyright protection.  The system of German industrial property and copyright protection is however founded upon the basic assumption that for specific kinds of mental achievements different specially adapted protection regulations are in force, and that overlappings between these different protection rights need to be excluded as far as possible.  The patent system is also not conceived as a reception basin, in which all otherwise not legally privileged mental achievements should find protection.  It was on the contrary conceived as a special law for the protection of a delimited sphere of mental achievements, namely the technical ones, and it has always been understood and applied in this way.

Any attempt to attain the protection of mental achievements by means of extending the limits of the technical invention -- and thereby in fact giving up this concept -- leads onto a forbidden path.  We must therefore insist that a pure rule of organisation and calculation, whose sole relation to the realm of technology consists in its usability for the normal operation of a known computer, does not deserve patent protection.  Whether it can be awarded protection under some other regime, e.g. copyright or competition law, is outside the scope of our discussion.

\section{Further Reading}

\begin{itemize}
\item
{\bf {\bf Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs\footnote{http://swpat.ffii.org/vreji/papri/grur-kolle77/indexde.html}}}

\begin{quote}
A comprehensive treatise on the traditional notion of technical invention, i.e. the requirement that a patentable invention provide a new way of directly using natural forces.   Kolle applauds the theoretical consistency of the Dispositionsprogramm verdict of the German Federal Court (BGH) of 1976, which systematically confirmed the legislator's decision of excluding all software from patentability.  Kolle remarks that it could be possible to find ways to reinterpret the law so as to make computer programs patentable nonetheless, but warns that such a reinterpretation would be irresponsible and beyond the legitimate rights of a lawcourt, as it would open the possibilities of monpolising the intellectual sphere, thus leading to a dangerous growth of power in the hands of a few economic actors, which some of the people who advocate software patentability may be ``naively or ignorantly`` aiming for.  According to the BGH, there must be a sphere of non-patentable innovations, which Kolle also calls ``nobody's land of intellectual property''.  Algorithms should be ``socialised''.  Kolle was one of the opinion leaders of the debate in the 70s, which led to the explicit non-patentability of computer programs.
\end{quote}
\filbreak

\item
{\bf {\bf Bernhardt \& Kra\ss{}er: Lehrbuch des Patentrechts -- Recht der Bundesrepublik Deutschland, Europ\"aisches und Internationales Patentrecht\footnote{http://swpat.ffii.org/vreji/papri/krasser86/indexde.html}}}

\begin{quote}
Prof Krasser of Munich Technical University rewrote this famous introduction to patent law of his older colleague Prof. Bernhardt in 1986 to adapt it to the new situation after the entry into force of the European Patent Convention (EPC).   Krasser's explanations of the concepts of invention and technical character are closely founded on the German Federal Court (BGH) caselaw and extensively quote legal literature, especially of Gert Kolle.   Krasser argues among other points:  The invention concept of the EPC is based on the concept of 'technical teaching', which is an unquestioned part of customary patent law worldwide and has been made explicit in detail by German courts.  The ``invention'' is defined as a technical teaching, i.e. instructions on how to use natural (physical, material) forces to directly cause a physical/material effect.  This can refer to dead or living nature and may extend to new laws of nature as they are discovered, but not to the laws of human reasoning (logics, mathematics etc).  'Rules of Organisation or Calculation' are not technical even when they are executed on a computer, because the problem solution is concluded and verified within the realm of reason before the realm of engineering (the technical field) is entered during the practical application.  The catalogue of patentability exclusions in Art 52 EPC confirms this old German and international customary law and codifies it.  This means that many important intellectual achievements of mankind must be denied patent protection.  Recently many writers of legal literature have demanded that this 'discrimination against the non-technical' be loosened or discarded, especially in view of the growing importance and protection needs of the software industry.  However the BGH consistently rejects this demand.  According to the BGH, it is not permissible to work toward such an extension of patentability by means of caselaw.  Not every important intellectual achievement with great economic potential needs to be protected by some means.  The patent system is a system for a specific area of intellectual achievement.  A lack of legal protection in another area of intellectual achievement does not indicate that patents are called for in that area.  It may just as well indicate that the concerned area has a special need of keeping ideas freely available.  In the non-technical areas, such a need arises from the fact that everybody can immediately apply and develop the new ideas in question without any investment in new equipment.  If patents were to be granted for software or other non-technical achievements, this decision would have to be made by the legislator, and it would require careful consideration.  However insufficient recent software copyright decisions of the BGH may appear to be, there are many reasons to doubt whether patents would really be a solution to the insufficiencies of a given copyright regime.  It seems more likely that solutions are to be sought in a better adaptation of copyright to software, such as proposed by WIPO in 1977.
\end{quote}
\filbreak

\item
{\bf {\bf BGH 1980-09-16: Walzstabteilung Decision\footnote{http://swpat.ffii.org/vreji/papri/bgh-walzst80/indexen.html}}}

\begin{quote}
One of a series of landmark decisions of the German Federal Court (BGH) about the non-patentability of computer programs: 'Calculating programs for electronic data processing hardware, during whose execution a machine of known structure is used in the preconceived way, are not patentable.  This holds even in cases where the computer is used for directly influencing a manufacturing process using known control means.'   
\end{quote}
\filbreak

\item
{\bf {\bf 'Anti Blocking System' decision of 1980 puts brakes on 'witch chase against untechnical patent claims'\footnote{http://swpat.ffii.org/vreji/papri/bgh-abs80/indexde.html}}}

\begin{quote}
The Federal Patent Court (BPatG) had rejected the claims to an anti blocking system, because they pertained to an organisational or computational rule that solved an abstract problem within a known model, without any necessity of recourse to experimenting with natural forces.  The Federal Court (BGH) now overruled this rejection and upheld the patent, saying that it was enough that the claims were directed to a braking process and therefore technical.  Thereby the Federal Court legalised function claims.  The commentator, a patent attorney, hails this as groundbreaking good news that will send a sigh of relief through the rounds of patent lawyers, who had been struck by sorrow and anguish after reading the earlier BPatG verdict, which seemed like a 'witch hunt against all things non-technical'.  In fact the new BGH verdict, although still in line with the technicity doctrine and even based on Gert Kolle's interpretation of this doctrine, opened up the patent system to an adventurous development toward function claims, erosion of the technicity concept and eventual de facto patentability of software.
\end{quote}
\filbreak

\item
{\bf {\bf BGH-Beschluss 'Flugkostenminimierung' 1981\footnote{http://swpat.ffii.org/vreji/papri/bgh-flug81/indexde.html}}}

\begin{quote}
Ein Rechenprogramm ist auch dann untechnisch, wenn die Rechenergebnisse direkt f\"ur technische Zwecke angewendet werden, vgl Straken 1977 und Walzstabteilung 1980
\end{quote}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/stidi/korcu/indexen.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant law science literature.
\end{quote}
\filbreak
\end{itemize}
\end{document}