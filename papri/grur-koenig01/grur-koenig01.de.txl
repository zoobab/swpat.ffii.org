<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: König 2001: Patentfähige Datenverarbeitungsprogramme - ein Widerspruch
in sich

#descr: Der Düsseldorfer Patentanwalt Dr. König zeigt allerlei Ungereimtheiten
in der Softwarepatent-Rechtsprechung des BGH und EPA auf, kritisiert
%(q:Zirkelschlüsse) und argumentiert, das EPA habe Art 52 EPÜ
%(q:Gewalt angetan).  Durch eine %(q:grammatische Auslegung des
Begriffs %(q:Datenverarbeitungsprogramme als solche)) gelangt er zu
der Erkenntnis, dass damit nur alle Datenverarbeitungsprogramme ohne
Ausnahme gemeint sein können, allerdings nur insoweit sie alleine
Gegenstand des Patentbegehrens seien.  Seit der Umsetzung des EPÜ in
die PatG-Novelle von 1978 gibt es keine Grundlage mehr für eine
Unterscheidung zwischen technischen und untechnischen Programmen. 
Beim Verständnis des Begriffes %(q:DV-Programm) habe sich die
Rechtsprechung nach allgemeinen außerjuristischen Definitionen zu
richten, wie sie etwa von DIN-Fachleuten geleistet wurden.  Danach ist
ein Programm die Gesamtheit aus Sprachdefinitionen und Text und nicht
etwa nur der urheberrechtlich schützbare Text.  Das EPÜ/PatG erlaube
es aber, %(q:Kombinationserfindungen) zu patentieren, die als ganzes
auf Technizität, Neuheit, Nichtnaheliegen und gewerbliche
Anwendbarkeit zu prüfen seien.  Gerichte hätten schon immer
Kreativität entfaltet, wenn es darum ging, %(q:sich über
Patentierbarkeitsausschlüsse hinwegzuhelfen). 
Datenverarbeitungsprogrammen (als solchen) könne auf dem Wege über ein
Kombinationspatent mittelbar der volle Schutz eines Sachpatents
zukommen.  Datenverarbeitungsprogramme seien ebenso wie etwa
naturwissenschaftliche Entdeckungen (als solche) dem Verwendungsschutz
zugänglich.

#Ssl: Schrifttum wie Rechtsprechung nehmen das Technizitätsgebot von jeher
als gegeben hin, obgleich das Patent- wie Gebrauchsmustergesetz und
bislang auch das EPÜ -- nichts dergleichen enthalten.  Erst Art. 52(1)
EPÜ in der anlässlich der Revisionskonferenz verabschiedeten, aber
noch nicht in Kraft getretenen Fassung verankert das
Technizitätsgebot.  Dies berührt jedoch die unverändert bleibenden
Ausnahmetatbestände der Abs. 2 und 3 nicht, sonder lässt nur den
Schluss zu, dass der europäische Gesetzgeber offensichtlich von einem
umfassenden Technikbegriff ausgeht, der auch die Ausnahmetatbestände
der Abs. 2 und 3 ausschlösse, wenn sie nicht qua lege ausgenommen
wären.

#Vge: Verfolgte das Patentgesetz mit § 1 II, III das Ziel
Datenverarbeitungsprogramme in patentfähige und nicht patentfähige zu
scheiden, dann müsste dies mit hinreichender Bestimmtheit im Gesetz
tatbestandlich vorgezeichnet und verfassungskonform konkretisiert sein
(Art. 14 I, 3 I GG).

#Ien: In der Nominalgruppe %(q:Datenverarbeitungsprogramm als solches)
bewirkt %(q:als) in Verbindung mit %(q:solche) eine Gleichstellung der
Basis (Datenverarbeitungsprogramm), und zwar mit all ihren
begriffsbestimmenden Merkmalen, mit sich selbst; diese Gleichstellung
ist kontextunabhängig und verweist ohne Wenn und Aber auf die
fachlexikalische Bedeutung der Basis.  Insoweit kommt dem Appositiv
%(q:als solche) hier gleichsam die Qualität einer Verweisung zu. 
Kontextunabhängig bedeutet im vorliegenden Zusammenhang: jenseits der
Kategorien und Kriterien des Patentrechts.  Demgemäß meint %(q:als
solche) (nur) soviel wie %(q:jeweils für sich) im Sinne einer
%(q:Alleinstellung).  Das Appositiv %(q:als solche) enthält auch
keinerlei Unschärfe und ist erst recht nicht %(q:irreführend) (fn:
UNION Round Table .. zitiert nach Betten); es ist semantisch
eindeutig.  Datenverarbeitungsprogramme sind nicht patentfähig, eben
und gerade weil sie Datenverarbeitungsprogramme sind.

#Kll: König scheint bei der grammatischen Analyse noch zu übersehen, dass es
sich bei %(q:[basis] als solche) nicht um eine geschlossene
Nominalgruppe handelt.  Vielmehr ist die Konstruktion unvollständig. 
Es gehört noch ein Prädikat wie etwa %(q:beanspruchen),
%(q:patentieren) o.ä. dazu, und dieses Prädikat reißt oft die Gruppe
sichtbar auseinander:  %(bq:Programme dürfen nicht als solche
beansprucht werden).  Daher erscheint angesichts Art 52 EPÜ die Frage
%(bq:Was ist ein %(q:Programme als solches)?) ähnlich verfehlt wie
angesichts des Satzes %(q:Es regnet) die rage %(q:Was ist ein
%(q:es)?).  Richtig wäre die Frage %(bq:Was bedeutet %(q:ein XXX als
solches zum Patent anmelden)?)  Die Antwort dürfte dennoch ähnlich wie
bei König ausfallen, denn auch König zeigt auf, dass %(q:als solches)
kein Attribut ist und somit nichts zu einer irgendwie definierbaren
Einschränkung der Bedeutung der Basis XXX beitragen kann.

#DpW: Das Differenzierungskriterium %(q:als solche) fragt demanch nicht nach
Inhalt, Art, Wirkung und Beitrag zum Stand der Technik oder nach der
Zweckbestimmung des Datenverarbeitungsprogramms.  Daher sind auch
Datenverarbeitungsprogramme, die auf die Arbeitsweise des Computers
Einfluss nehmen (Betriebsprogramme) oder die Messergebnisse
verarbeiten, Programme als solche, weil %(q:als solche)
Datenverarbeitungsprogramme schlechthin vom Patentschutz ausschließen
will, allerdings nur solange sie allein Gegenstand des Patentbegehrens
sind.

#Ene: Erstaunlicherweise wendet sich keine der veröffentlichten
Entscheidungen der grammatischen Auslegung bzw Wortsinnermittlung des
Begriffs %(q:Datenverarbeitungsprogramm als solches) zu. 
Rechtsprechung und Fachliteratur zum neuen Recht sind sich vielmehr
weitgehend einig in dem Bestreben, die am Technizitätsgebot
orientierte Rechtsprechung zum alten Recht mit Hilfe der %(q:als
solche)-Interpretation möglichst weitgehend über die Gesetzesänderung
von 1978 hinwegzuretten; sie begreifen %(q:als solche) als eine andere
Bezeichnung für %(q:nicht technisch) und verlassen dabei die Vorgabe
des Gesetzes.  Hier könnte ein klärendes Wort des BGH hilfreich sein
und ist in Sonderheit eine Entscheidung der Großen Beschwerdekammer
des EPA gefordert; denn wenn es sich bei der Auslegung des
Rechtsbegriffs %(q:Datenverarbeitungsprogramm als solches) nicht um
eine Rechtsfrage von grundsätzlicher Bedeutung handelt (Art. 112 EPÜ),
dann fragt sich, was denn sonst.

#Wne: Was der Begriff %(q:Datenverarbeitungsprogramm) beinhaltet, ist keine
Frage einer abstrakten Definition oder einer an
Zweckmäßigkeitserwägungen orientierten Grenzziehung; sie kann ihre
Antwort nach dem oben Gesagten nur in einer allgemein anerkannten
Begriffsdefinition außerhalb des Patentgesetzes finden.

#Ert: Eine solche liefert die DIN 44 300 Teil 4, die
%(q:Datenverarbeitungsprogramm) als eine %(bq:nach den Regeln der
verwendeten Sprache festgelegte syntaktische Einheit aus Anweisungen
und Vereinbarungen, welche die zur Lösung einer Aufgabe notwendigen
Elemente umfasst,) definiert.  Die Anwendung dieser DIN besitzt den
unschätzbaren Vorteil, dass sie in einem geregelten Verfahren (DIN
820) mit einer 5/6-Mehrheit der sachkundigen Mitglieder der
zuständigen Fachkommission des Deutschen Normenausschusses (DNA) zu
Stande gekommen ist.  Diese Norm nicht zur Konkretisierung des
Begriffs %(q:Datenverarbeitungsprogramm) zu benutzen, bedürfte einer
überzeugenden Begründung; denn sie gibt schließlich die allgemein
anerkannte Auffassung der Fachwelt wieder und enthält damit eine
sachverständige Aussage über den Begriff
%(q:Datenverarbeitungsprogramm).  Solche Normen werden daher
hinsichtlich ihrer rechtlichen Wirkung häufig auch als antizipierte
Sachverständigengutachten bezeichnet; sie bieten ein hohes Maß an
rechtsstaatlicher Bestimmtheit und sind daher auch in großem Umfang
Gegenstand gesetzlicher Verweisungen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-koenig01 ;
# txtlang: de ;
# multlin: t ;
# End: ;

