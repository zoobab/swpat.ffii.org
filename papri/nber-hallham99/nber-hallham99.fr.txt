<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Travail de recherche fait à l'Université de Californie, Berkeley, publié en 1999 par National Bureau of Economic Research Inc. Il trouve que la poussée d'activité remous sur les brevets dans l'industrie des semi-conducteurs dans les années 80-90 ne reflète pas une poussée d'activité en Recherche et Développement.

#title: Bronwyn H. Hall et Rose Marie Ham: Le Paradoxe des Brevets Revisité

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: nber-hallham99 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

