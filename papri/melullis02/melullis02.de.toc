\contentsline {section}{\numberline {1}Ausz\"{u}ge}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Urheberrecht im wesentlichen ausreichend}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Politische Interessenlage}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}EPA-Rechtsprechung sprengt Grenzen der Auslegung des Art 52 EP\"{U} und f\"{u}hrt zu Rechtsunsicherheit}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Schutzbegehren nicht f\"{u}r Technik sondern f\"{u}r Ideen und Aufgabenstellungen}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}EU-Richtlinie bejaht Softwarepatente, verl\"{a}sst Boden des EP\"{U}, l\"{a}sst Interessenabw\"{a}gung vermissen}{5}{subsection.1.5}
\contentsline {section}{\numberline {2}Kommentierte Verweise}{6}{section.2}
