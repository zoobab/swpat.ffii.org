<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#uzg: Auszüge

#ess: Urheberrecht im wesentlichen ausreichend

#iIe: Politische Interessenlage

#cgu: EPA-Rechtsprechung sprengt Grenzen der Auslegung des Art 52 EPÜ und
führt zu Rechtsunsicherheit

#hog: Schutzbegehren nicht für Technik sondern für Ideen und
Aufgabenstellungen

#Wtn: EU-Richtlinie bejaht Softwarepatente, verlässt Boden des EPÜ, lässt
Interessenabwägung vermissen

#uhe: Dem im Hinblick auf den - wenn auch mit Blick auf die Beschränkung auf
Proramme als solche - in seinen Konturen unscharfen Ausschluss der
Software verbleibenden Schutzbedürfnis der Hersteller von Software hat
die Rechtsordnung nach In-Kraft-Treten des EPÜ durch ihre Einbeziehung
in das Urheberrecht nachzukommen versucht, was heute im Anschluss an
die Richtlinie 91/250/EWG des Rates über den Schutz von
Computerprogrammen vom 14.5.1991z in der Europäischen Union zu einem
in seinem Inhalt im Wesentlichen übereinstimmenden Schutz geführt hat.
Dieser sieht entsprechend den Vorgaben der Richtlinie im Wesentlichen
ein Verbot des Kopierens, der Verbreitung und der Bearbeitung
einschließlich des Dekompilierens ohne Erlaubnis des Rechtsinhabers
vor.

#enc: Für das konkrete Programm scheint damit auf den ersten Blick ein
umfassender und hinreichender Schutz jedenfalls dann gegeben, wenn man
mit der wohl überwiegenden Meinung zum deutschen Urheberrecht davon
ausgeht, dass ein Kopieren im Sinne des Gesetzes auch das Aufspielen
des Programms auf den Rechner ist.  Eine unbefugte Benutzung des
konkreten Programms ist danach über das Kopierverbots ebenso
ausgeschlossen wie seine Bearbeitung ohne Erlaubnis; in Verbindung mit
der jedenfalls im Bereich der klassischen Softwareindustrie üblichen
Geheimhaltung des Sourcecodes ist damit zugleich auch eine
Nachschöpfung auf dessen Grundlage praktisch unmöglich. Das
Dekompilierungsverbot stellt in diesem Zusammenhang eher eine
flankierende Maßnahme dar, die einen schon aus tatsächlichen Gründen
bestehenden Schutz weiter absichert. Die Rückverwandlung in Code führt
auch bei eher kleinen Programmen zu einem Wust von unstrukturierten
Programmzeilen, die eine Eigenentwicklung ohne Rücksicht auf diesen
Code als sinnvoller erscheinen lassen. Eine Schutzlücke ist hier
allenfalls dort zu erkennen, wo sich Dritte den Code auf unlautere
Weise verschaffen oder dieser von ausscheidenden Mitarbeitern
mitgenommen wird. Auch diesen Widrigkeiten ist indessen, soweit sie
überhaupt eine Rolle spielen sollten, mit den vorhandenen Mitteln der
Rechtsordnung zu begegnen.

#nke: Von den Vertretern beider Lager wird der jeweilige Standpunkt mit
vielfältigen und zum Teil gewichtigen Argumenten begründet. So wird
die Ablehnung der Patentierung von Software neben anderen Vorschriften
der Verfassung auch durch den grundrechtlichen Schutz der
Informations- und Meinungsfreiheit gerechtfertigt, der durch eine
Monopolisierung von Software, Algorithmen und Regeln gefährdet oder in
Frage gestellt werden könne. Weiter wird u.a. geltend gemacht, dass
Programmierer auf die freie Zugänglichkeit und Verwertbarkeit des nur
begrenzten Vorrats an Algorithmen und Regeln angewiesen seien; auch
bei dem Gegenstand der Programme und dem was mit ihnen bewirkt werden
könne, seien Überschncidungen kaum zu vermeiden. Dort, wo wie in den
USA Patentschutz für Software seit längerer Zeit gewährt werde,
könnten neue Programme ohne Inanspruchnahme geschützter Gegenstände
nicht mehr programmiert werden; in diesem Zusammenhang wird davon
gesprochen, dass bei der Entwicklung einer durchschnittlichen
Anwendung schon heute 200 Patente und mehr benutzt würden. Soweit die
Diskussion juristisch determiniert ist, wird auf den Inhalt der
gesetzlichen Regelung verwiesen, nach der Programme als solche nicht
als Erfindung anzusehen seien. In diesem Zusammenhang wird ferner auf
einen angeblich fehlenden technischen Charakter von Software
verwiesen, der eine Patentierung nach dem System des Gesetzes ohnehin
ausschließe.

#Pfn: Die Gegenmeinung geht demgegenüber von einem solchen Charakter aus und
macht geltend, dass nach Sinn und Zweck des Patentrechtes dessen
Schutz jeder menschlichen technischen Schöpfung zur Verfügung stehen
müsse, was folgerichtig daher grundsätzlich auch für Software zu
gelten habe. Das gesetzliche Patentierungsverbot beschränkt der
überwiegende Teil der Vertreter dieser Ansicht auf nicht-technische
Elemente, wobei mit Blick auf die gesetzliche Regelung von der
Mehrzahl ihrer Vertreter konzediert wird, dass das übliche
Zusammenwirken eines Programms mit einem Universalrechner zur
Begründung der erforderlichen Technizität nicht genügen könne, sondern
es darüber hinaus eines technischen Überschusses bedürfe, an den
allerdings im Verlauf der Entwicklung immer geringere Anforderungen
gestellt werden.

#Png: In das Europäische Patentübereinkommen, dem die meisten nationalen
Patentgesetze im Vertragsgebiet nachgebildet sind, haben dessen Väter
lediglich ein durch seine Einschränkung im folgenden Absatz allenfalls
in seiner Reichweite unklares Patentierungsverbot aufgenommen. Daraus
ist zunächst nur herzuleiten, dass Software an sich nicht patentiert
werden soll, Ausnahmen aber möglich und denkbar sind. Die Annahme
einer generellen Schutzfähigkeit würde das gesetzliche
Regel-Ausnahmeverhältnis umkehren, wie immer sie im Einzelnen auch
begründet und inhaltlich ausgefüllt wird. Zudem bleibt bei der ihr zu
Grunde liegenden Differenzierung unberücksichtigt, dass sich die auf
den ersten Blick einleuchtende Unterscheidung zwischen einem üblichen
und einem darüber hinausgehenden Zusammenwirken von Programm und
Rechner sauber kaum treffen lässt, wie auch an den Entscheidungen des
Europäischen Patentamts und seiner Beschwerdekammern deutlich wird,
die - obwohl von diesem Grundsatz verbal ausgehend - eine klare Linie
kaum erkennen lassen. Letztlich ist es eine in ihrem Ergebnis nicht
mit Sicherheit vorherzusagende persönliche Wertung, ob ein Programm
zur Herbeiführung des gewünschten Arbeitserfolges nur von den üblichen
Möglichkeiten eines Rechners Gebrauch macht und damit über das übliche
Zusammenwirken nicht hinausgeht oder ob es einen weitergehenden Effekt
auflöst. Verlässliche Entscheidungen sind daher mit dieser
Differenzierung nicht zu erreichen; auch das macht sie für die Zwecke
des Patentrechtes, das schon wegen seines Verbotscharakters im
Interesse der unverzichtbaren Rechtssicherbeit auf eben diese
Verlässlichkeit angewiesen ist, eher ungeeignet.

#Wle: Gegenstand und Inhalt der programmbezogenen Anmeldungen legen die
Vermutung nahe, dass in einer Reihe der von der Problematik
betroffenen Anmeldungen nicht der Schutz für die Technik der Software
im Vordergrund steht, sondern von dieser weitgehend unabhängige Ideen
unter Schutz gestellt werden sollen. Betrachtet man etwa die
Anmeldungen beim Europäischen Patentamt und beim Deutschen Patent- und
Markenamt, die Gegenstand veröffentlichter Entscheidungen geworden
sind, so fällt auf, dass in ihmen nicht die Lösung eines konkreten
technischen oder außertechnischen Problems durch Software unter Schutz
gestellt werden soll, sondern beansprucht wird, ein solches Problem
durch Einsatz eines Computers abstrakt zu lösen, ohne dass diese
Lösung in einer konkreten und einem Fachmann ohne weiteres
zugänglichen Fonn Gegenstand der Schutzansprüche geworden ist. Ein
solcher Schutz betrifft nicht eine konkrete Programmierung zur
Bewältigung des Problems mit Hilfe des Computers, sondern es wird nur
abstrakt Schutz begehrt für die Lösung eines technischen oder
außertechnischen Problems mit Hilfe eines Computers; der Anspruch
beschränkt sich danach im Wesentlichen auf eine mehr aufgabenhaft
beschriebene Lehre als den Anforderungen des Patentrechts
entsprechende praktische Lösung.

#grn: Ein so breit gefächerter Schutz lässt die Befürchtung der Gegner
jeglichen Patentschutzes für Software verständlich erscheinen, dass
mit ihm auf Dauer eine vernünftige Programmierung von Software ohne
die Notwendigkeit permanenter Lizenzierung nicht mehr möglich sein
wird. Dass einzelne Unternehmen wie etwa IBM eine vergleichsweise
großzügige Lizenzierungspolitik hinsichtlich der zu ihren Gunsten
geschützten Software betreiben, verringert diese Besorgnis nicht. Zum
einen muss es nicht bci dieser Politik verbleiben; zum anderen werden
auch bei ihr Lizenzen regelmäßig nur gegen Vergütung erteilt. Das wird
die Kosten für die Entwicklung weiterer Software in die Höhe treiben
und insbesondere für die freien Programmierer, die ihre Programme
vielfach ohne gesonderte Vergütung angeben, eine nicht zu
finanzierende wirtschaftliche Belastung darstellen. Hinzu kommt, dass
sie in der Regel nicht über die wirtschaftlichen Mittel verfügen, um
die für die Klärung der Patentrechtslage erforderlichen Recherchen
durchzuführen, zumal diese wegen der schon jetzt nicht zu übersehenden
Zahl von Computerprogrammen einen erheblichen Aufwand erforderlich
machen wird.

#eei: Zusammenfassend ist festzustellen, dass auf der Basis des aktuellen
Rechts eine Grundlage für einen umfassenden Patentschutz für Software
nicht zu erkennen ist. Eine Auslegung, die zu einem solchen Schutz
führt, beruht auf Unterstellungen, die im EPÜ und den ihm folgenden
nationalen Patentgesetzen gerade wegen des technischen Charakters von
Software keine tragfähige Grundlage finden; sie ist darüber hinaus
auch deshalb abzulehnen, weil sie ohne hinreichende Rechtfertigung die
Lösung eines gesellschaftlichen Konfliktes zum Nachteil einer der
beteiligten Gruppen bewirkt, ohne deren mangelnde Schutzfähigkeit
feststellen zu können. Zur Lösung dieses Konfliktes bedarf es einer
Entscheidung des Gesetzgebers, der sich dabei allerdings erst dieses
Konfliktes und der ihm zu Grunde liegenden Interessengegensätze gewahr
werden und die Grundlagen seiner Entscheidung dieses Konfliktes klären
muss. Die %(rl:Richtlinie der Kommission der Europäischen
Gemeinschaft), die erst nach Abschluss des vorliegenden Manuskripts
zugänglich geworden ist, genügt diesem Anspruch nicht; sie ist zudem
mit ihrer weitgehenden Bejahung einer Patentierung von Software so
nicht in das System des EPÜ einzuordnen.

#dis: Ein Text des Festschrift-Jubiliars, auf den Melullis Bezug nimmt.

#ter: GRUR-Artikel eines Patentprüfers, auf den Melullis Bezug nimmt und der
seinerseits auf einen vielbeachteten früheren GRUR-Artikel von
Melullis antwortet.

#Pnn: Dr. Karl-Friedrich Lenz zeigt auf, wie das EPA durch die Erteilung von
Softwarepatenten und insbesondere durch zwei bahnbrechende
Entscheidungen von 1998 nach allen Regeln der Gesetzesauslegungskunst
das geltende Recht bricht.  Melullis zitiert diesen Artikel und misst
seiner Argumentation einiges Gewicht bei.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: melullis02 ;
# txtlang: xx ;
# End: ;

