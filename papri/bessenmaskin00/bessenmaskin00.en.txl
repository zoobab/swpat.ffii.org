<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen & Maskin 2000: Sequential Innovation

#descr: This article is written by two researcher from MIT and concludes,
after giving mathematical models and experimental evidence, that in a
dynamic world such as the software industry or consulting industry,
firms may have plenty of incentive to innovate without patents and
patents may constrict complementary innovation. It concludes that
copyright protection for software programs (which has gone through its
own evolution over the last decade) may have achieved a better balance
than patent protection. This new model suggests another, different
rationale for narrow patent breadth than the recent economic
literature on this subject.

#Whe: some of the most innovative industries today -- software, computers
and semiconductors -- have historically had weak patent protection and
have experienced rapid imitation of their products.

#yrd: Far from unleashing a flurry of new innovative activity, these
stronger property rights ushered in a period of stagnant, if not
declining, R&D among those industries and firms that patented most.

#knl: Clement 2003-03: Creation Myths -- Does innovation require
intellectual property rights?

#eil: An article that introduces recent economic literature which is
sceptical about %(q:intellectual property rights) in general and about
software patents in particular.  This article also cites the Bessen &
Maskin study.

#ArW: An anonymous patent lawyer speaking in the name of the European
software and telecom industry lobby group EICTA claims that this paper
is built on old data and inadequate models, but doesn't say exactly
what is wrong and how it should be done better.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bessenmaskin00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

