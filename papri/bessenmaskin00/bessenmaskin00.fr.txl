<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen et Maskin 2000: Innovation Continue

#descr: Cet article est icrit par deux chercheurs du MIT et conclut, aprhs
avoir donni des modhles mathimatiques et une preuve expirimentale, que
dans un monde dynamique comme celui de l'industrie du logiciel et des
cabinets de consultants, les entreprises peuvent avoir beaucoup de
rendement ` innover sans brevet et les brevets peuvent itrangler
l'innovation complimentaire. Il conclut que la protection par
copyright des programmes logiciels (qui a suivi sa propre ivolution
depuis les dix derniires annies) pourrait avoir atteint un meilleur
iquilibre que la protection par brevet. Ce nouveau modhle sugghre un
autre raisonnement, diffirent pour l'itroitesse d'itendue du brevet
que le littirature iconomique ricente sur ce sujet.

#Whe: quelques unes des industries les plus innovantes aujourd'hui --
logiciel, ordinateurs et semi-conducteurs -- ont eu historiquement de
faibles protections par brevets et ont rapidement fait l'expérience de
l'imitation de leurs produits par d'autres.

#yrd: Loin de déchaîner un tourbillon d'activité innovatrice, ces droits de
propriétés plus forts ont annoncé une période de stagnation, si ce
n'est de déclin, de Recherche et Développement parmi ces industries et
entreprises qui brevetaient le plus.

#knl: Clement 2003-03: Les Mythes de la Création -- L'innovation
requière-t-elle des droits de propriété intellectuelle?

#eil: Un article qui est apparu dans la littérature économique récente qui
est sceptique à propos %(q:des droits de propriété intellectuelle) en
général et sur les brevets logiciels en particulier.  Cet article cite
aussi l'étude de Bessen et Maskin.

#ArW: Un juriste anonyme sur les brevets parlant au nom du lobby du groupe
des industries de télécommunications et de logiciels européen EICTA
prétend que ce document est fondé sur des données anciennes et sur des
modèles inadéquats, mais ne dit pas exactement ce qui est faux et
comment cela pourrait être mieux fait.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bessenmaskin00 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

