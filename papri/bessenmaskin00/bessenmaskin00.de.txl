<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen & Maskin 2000: Sequentielle Innovation

#descr: Dieser Artikel wurde von zwei Forschern des MIT verfasst und folgert
unter Anführung mathematische Modelle und experimenteller  Belege,
dass in einer dynamischen Welt wie der Software- oder 
Beratungsbranche, Unternehmen ohne Patente genug Anreize zur
Innovation  haben und dass Patente komplementäre Innovationen
begrenzen können. Der  Artikel kommt zu dem Schluss, dass der
Urheberrechtsschutz für Software  (der seine eigene Entwicklung im
letzten Jahrzehnt durchlaufen hat) eine  bessere Balance bewirkt hat
als der Patentschutz. Dieses neue Modell  legt eine weitere Begründung
für eine enge Begrenzung von Patenten nahe.

#Whe: Einige der derzeit innovativsten Branchen - Software, Computer und
Halbleiterindustrie - haben in der Vergangenheit schwachen
Patentschutz gehabt und eine rasante Nachahmung ihrer Produkte
erfahren.

#yrd: Weit davon entfernt, rege Aktivitäten für Innovationen zu  entfachen,
führten diese stärkeren Eigentumsrechte in eine Zeit  stagnierender
oder sogar abnehmender F.u.E. bei den Branchen und  Unternehmen, die
am häufigsten patentieren.

#knl: Clement 2003-03: Creation Myths -- Erfordert Innovation geistige 
Eigentumsrechte?

#eil: Ein Artikel, der aktuelle, allgemein gegenüber (%q:geistigen 
Eigentumsrechten) und speziell Softwarepatenten skeptische 
Wirtschaftsliteratur vorstellt. Dieser Artikel zitiert auch die Bessen
&  Maskin Studie.

#ArW: Ein anonymer Patentanwalt behauptet im Namen der europäischen 
Software- und Telekommunikations-Lobbyistengruppe EICTA, dass diese 
Studie auf veralteten Daten und inaäquaten Modellen beruht, er sagt
aber  nicht genauer, was er für falsch hät und wie es besser gemacht
werden  könnte.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/swpatpapri.el ;
# mailto: andreas_ffii_20030226@rudert-home.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bessenmaskin00 ;
# txtlang: xx ;
# End: ;

