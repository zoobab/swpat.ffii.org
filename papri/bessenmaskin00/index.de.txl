<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bessen & Maskin 2000: Sequentielle Innovation

#descr: Dieser Artikel wurde von zwei Forschern des MIT verfasst und folgert
unter Anf=C3=BChrung mathematische Modelle und experimenteller=20
Belege, dass in einer dynamischen Welt wie der Software- oder=20
Beratungsbranche, Unternehmen ohne Patente genug Anreize zur
Innovation=20 haben und dass Patente komplement=C3=A4re Innovationen
begrenzen k=C3=B6n= nen. Der=20 Artikel kommt zu dem Schluss, dass der
Urheberrechtsschutz f=C3=BCr Softw= are=20 (der seine eigene
Entwicklung im letzten Jahrzehnt durchlaufen hat) eine=20 bessere
Balance bewirkt hat als der Patentschutz. Dieses neue Modell=20 legt
eine weitere Begr=C3=BCndung f=C3=BCr eine enge Begrenzung von Paten=
ten nahe.

#Whe: Einige der derzeit innovativsten Branchen - Software, Computer und
Halbleiterindustrie - haben in der Vergangenheit schwachen
Patentschutz gehabt und eine rasante Nachahmung ihrer Produkte
erfahren.

#yrd: Weit davon entfernt, rege Aktivit=C3=A4ten f=C3=BCr Innovationen zu=20
entfachen, f=C3=BChrten diese st=C3=A4rkeren Eigentumsrechte in eine
Zeit= =20 stagnierender oder sogar abnehmender F.u.E. bei den Branchen
und=20 Unternehmen, die am h=C3=A4ufigsten patentieren.

#knl: Clement 2003-03: Creation Myths -- Erfordert Innovation geistige=20
Eigentumsrechte?

#eil: Ein Artikel, der aktuelle, allgemein gegen=C3=BCber (%q:geistigen=20
Eigentumsrechten) und speziell Softwarepatenten skeptische=20
Wirtschaftsliteratur vorstellt. Dieser Artikel zitiert auch die Bessen
&=20 Maskin Studie.

#ArW: Ein anonymer Patentanwalt behauptet im Namen der europ=C3=A4ischen=20
Software- und Telekommunikations-Lobbyistengruppe EICTA, dass diese=20
Studie auf veralteten Daten und ina=C3=A4quaten Modellen beruht, er
sagt = aber=20 nicht genauer, was er f=C3=BCr falsch h=C3=A4t und wie
es besser gemacht = werden=20 k=C3=B6nnte.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/swpatpapri.el ;
# mailto: andreas_ffii_20030226@rudert-home.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: bessenmaskin00 ;
# txtlang: xx ;
# End: ;

