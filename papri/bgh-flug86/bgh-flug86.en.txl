<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Uhz: Uns lag nur ein Auszug vor, den wir hier wiedergegeben haben.  Es fehlt z.B. der Wortlaut des Patentanspruchs.

#Dkr: Die hiesige Dokumentation vervollständigen

#DeF: Diesen Text in andere Sprachen (EN, FR etc) übersetzen

#Deu: Der technische Charakter der beanspruchten Lehre kann entgegen der Ansicht des Beklagten weder daraus hergeleitet werden, daß es sich im Kern um eine besondere (neue) Betriebsweise (Verwendung) eines Flugzeuges handele noch daraus, daß die im Flugzeug vorhandenen Meßgeräte für den Treibstoffdurchsatz und die Geschwindigkeit eine neue Verwendung erführen. Diese Ansicht des Beklagten richtet ihren Blick einseitig nur auf Teilaspekte der beanspruchten Lehre und läßt die Einbeziehung der markt- und betriebswirtschaftlichen Werte und der Berechnungsregel in die beanspruchte Lehre, insbesondere aber deren überwiegende und entscheidende Bedeutung für die Erreichung des erstrebten Erfolges, der auch wirtschaftlicher Art ist, außer Betracht.

#BeW: Bei dieser Sachlage rechtfertigt es die Mitursächlichkeit der eingesetzten Naturkräfte nicht, der Gesamtheit der Lehre einen technischen Charakter im Sinne der zitierten Senatsrechtsprechung zuzubilligen.

#Edq: Eine Gewichtung der Maßnahmen, derer sich die beanspruchte Lehre bedient, um das Ziel der erstrebten Kostenminimierung zu erreichen, daß markt- und betriebswirtschaftliche Aspekte unter Einschluß der hier in Rede stehenden Berechnungsregel gegenüber den eingesetzten Naturkräften im Vordergrund stehen. Zunächst ist der erstrebte Erfolg betriebswirtschaftlicher Art. Sodann liefern die markt- und betriebswirtschaftlichen Faktoren den wesentlichen Beitrag zu der Ermittlung des Steuerkriteriums. Dies zeigt sich daTan, daß gleiche Naturkräfte und damit gleiche Meßwerte von Treibstoffdurchsatz und Geschwindigkeitje nach dem relativen Übergewicht von Teibstoflkosten oder Flugzeitkosten zu verschiedenen Flugzuständen als Ergebnis der Regelung führen können. Allein die betriebswirtschaftliche Bewertung der technischen Meßwerte bestimmt die Steuerung nach Größe und Richtung. Demnach sind die Auswahl der Berechnungskriterien und die Art der rechnerischen Ermittlung des Steuerkriteriums die entscheidenden Mittel für die Erreichung des erstrebten Erfolges und für dessen Zuverlässigkeit. Sie sind zur Problemlösung unerläßlich und bilden den Kern der beanspruchten Lehre. Demgegenüber treten die eingesetzten Naturkräfte bei der Erreichung des angestrebten Erfolges an Bedeutung zurück. Die Verwendung des Rechners mit Speicher und Vergleicher ist zwar sinnvoll, jedoch nicht denkgesetzlich notwendig, um den Erfolg zu erreichen. Außerdem sind diese Mittel üblicher Art. Bedeutung im Rahmen der Gewichtung kommt erst der für den Erfolg entscheidenden Rechenoperation zu. Diese beruht jedoch auf einer im wesentlichen von markt- und betriebswirtschaftlichen Aspekten beeinflußten gedanklich-logischen Lehre. Die Mittel zur automatischen Datenübermittlung an den Rechner und den dort zur Treibstoffdurchsatzsteuetung sind zwar technischer, aber ebenfalls üblicher Art. Außerdem haben sie im Rahmen der Problemlösung nur eine dienende Funktion. Was endlich die Änderungen des Treibstoffdurchsatzes und der Geschwindigkeit angeht, so führen diese nicht unmittelbar den erstrebten Erfolg der Kostenminimierung herbei; sie sind allein keine vollständige Problemlösung, sondern liefern im Rahmen des hier streitigen Verfahrens zwar wichtige, aber letztlich für den erstrebten Erfolg nicht allein entscheidende Kriterien einer Berechnung, deren Art und Weise nur unter Berücksichtigung markt- und betriebswirtschaftlicher Kriterien den entscheidenden Wert erbringt, der für die Erreichung des Erfolges ausschlaggebend ist.

#Bcn: Bei der Lehre ... in der verteidigten Fassung werden beherrschbare Naturkräfte eingesetzt und gleichzeitig betriebswirtschaftliche Faktoren herangezogen, um den Erfolg zu erreichen, ein Flugzeug automatisch im günstigsten Kostenbereich zu fliegen, mit anderen Worten, um die Gesamtflugkosten auf einem Flug zwischen zwei Flughäfen auf einen Minimalwert zu bringen. Es werden nicht allein die jeweiligen Änderungen des Treibstoffdurchsatzes und der Geschwindigkeit des Flugzeuges für zwei Teilstrecken automatisch ermittelt und für sich allein automatisch als Steuerungssignal für den Treibstoffdurchsatz eingesetzt, um das Ziel der Kostenminimierung zu erreichen, was %(ab:als technische Lehre anzusehen wäre), sondern es werden auch die betriebswirtschaftlichen Faktoren %(q:Treibstoffpreis) und %(q:Flugzeitkosten) herangezogen, um dieses Ziel zu erreichen. Abgesehen davon, daß der %(q:Treibstoffpreis) für sich allein betrachtet nach den Angaben der Streitpatentschrift kein maßgebendes Kriterium für die Bewältigung des Problems der Kostenminimierung ist, handelt es sich weder bei diesem noch bei den %(q:Flugzeitkosten) um Naturkräfte, die eingesetzt werden, um den hier in Rede stehenden Erfolg zu erreichen. Mögen diese Faktoren auch von technischen Gegebenheiten beeinflußt werden, so richten sie sich in ihrem Wesen nach den Marktverhältnissen und betriebswirtschaftlichen Gesichtspunkten, die nicht den Naturkräften zugerechnet werden können, deren Beherrschung zur Lösung von Problemen dem Patentschutz zugänglich ist. Die Naturkräfte, wie die automatisch ermittelten Werte der Änderungen von Treibstoffdurchsatz und Geschwindigkeit, werden vielmehr zu den markt- und betriebswirtschaftlichen Faktoren in bestimmte rechnerische Beziehungen gesetzt, wie das oben geschildert ist.

#Vrn: Vielmehr muß das Ergebnis, der kausal übersehbare Erfolg, die unmittelbare Folge des Einsatzes beherrschbarer Naturkräfte sein (...), d. h. die Verwendung technischer Mittel muß nicht nur Bestandteil der Problemlösung selbst sein, sondern die beanspruchte Lehre muß in ihrem technischen Aspekt auch eine vollständige Problemlösung bieten (...).

#Dnc: Dabei kommt es für den technischen Charakter und damit die Patentierbarkeit einer Lehre nicht auf deren sprachliche Einkleidung an, insbesondere nicht darauf, ob die Lehre in den Patentansprüchen unter Verknüpfung mit den zu ihrer Ausführung zweckmäßig oder notwendig heranzuziehenden technischen Einrichtungen formuliert worden ist. Entscheidend ist vielmehr, welches der sachliche Gehalt der beanspruchten Lehre ist, auf welchem Gebiet ihr Kern liegt. Ist Kern der Lehre die Auffindung einer Regel, deren Befolgung den Einsatz beherrschbarer Naturkräfte außerhalb des menschlichen Verstandes nicht gebietet, dann ist sie nicht technisch, auch wenn zu ihrer Ausführung der Einsatz technischer Mittel zweckmäßig oder gar allein sinnvoll, d. h. notwendig erscheint und auf den Einsatz dieser technischen Mittel in den Ansprüchen oder in der Patentschrift hingewiesen ist

#Vih: Voraussetzung für die Patentierbarkeit einer Lehre ist deren technischer Charakter. Nach der Rechtsprechung des Senats gehört eine Lehre zum planmäßigen I-iandeln dem Bereich der Technik nur dann an, wenn sie sich zuc Erreichung eines kausal übersehbaren Erfolges des Einsatzes beherrschbaret Naturkräfte außerhalb der menschlichen Verstandestätigkeit bedient

#DwW: Das BPatG hat die Klage abgewiesen. Die Berufung der Kl. hat Erfolg. Aus den Gründen:

#UR1: Urt. v. 11. 3. 1986 - X ZR 65/85 (BPatG) - NJW RR 1986, 994 = MDR 1986, 754

#Wen: Werden bei einem Verfahren (hier: Verfahren zur Minimierung von Flugkosten) sowohl von Naturkräften abgeleitete Meßwerte als auch betriebswirtschaftliche Faktoren rechnerisch in der Weise miteinander verknüpft, daß das Ergebnis der Rechnung einen Steuervorgang auslöst (hier: Änderung des Treibstoffdurchsatzes), so ist das Verfahren dann keine der Patentierung zugängliche technische Lehre, wenn die markt- und betriebswirtschaftlichen Faktoren den entscheidenden Beitrag zur Erreichung des erstrebten Erfolges liefern und die eingesetzten Naturkräfte demgegenüber an Bedeutung zurücktreten.

#mAO: Az

#descr: A teaching of how to control an airplane engine so as to diminish use of fuel is not necessarily a technical teaching.  In the present case, the examined teaching was not primarily about harnessing the forces of nature but about the use of economic and mathematical knowledge.  Therefore the Federal Court of Justice declared this patent invalid.

#title: BGH 1986-03-11: Beschluss Flugkostenminimierung

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bgh-flug86 ;
# txtlang: en ;
# multlin: t ;
# End: ;

