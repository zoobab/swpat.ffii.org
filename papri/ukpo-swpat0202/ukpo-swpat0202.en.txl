<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: UKPO 2002-02-20: proposal to allow patents to programs as such

#descr: The UK PTO conducted its own consultation, which showed an
overwhelming wish of software professionals to be free of software
patents.  But the UK PTO, speaking in the name of the UK government,
reinterprets this as a legitimation to remove all limits on
patentability by modifying Art 52 EPC at the Diplomatic Conference in
June 2002.  The %(pr:proposal) would render Art 52 tautological. 
Given that an %(q:invention) in the meaning of Art 52 is the same as a
%(q:technical contribution to the state of the art), the UKPO proposal
is tautological: %(orig:the following are not inventions, unless in
their inventive step they make a technological contribution to the
state of the art) just means %(orig:the following are not inventions
unless in their inventive step they are inventions) or, after removing
the misplaced %(q:inventive step) requirement, which is dealt with in
Art 56 EPC and not in Art 52 EPC, the UKPO's proposal boils down to:
%(bc:The following are not inventions unless they are inventions).  In
order to arrive at this recommendation, the UKPO conducted a
consultation, it says.  The purpose of this UKPO proposal was to help
CEC Commissioner Bolkestein persuade the grudging European Commission
to adopt their directive proposal in February 2002, a proposal written
by UK patent office people together with BSA in Brussels.  %(q:If the
European Commission doesn't adopt the proposal, we will sidestep the
EU by pressing ahead in the European Patent Organisation), was the
UKPO's (and thereby the UK government's) message.

#UkpoSwpat0202RefT: %(q:UK Position) on Software Patents

#UkpoSwpat0202RefD: This text contained a proposal to rewrite Art 52(3) EPC, replacing
%(q:this exclusion applies only to the extent that the claims are
directed to the excluded objects as such) with %(q:the following are
not inventions, unless in their inventive step they make a
technological contribution to the state of the art).  The UKPO has
removed this article, but it is continuing to misinterpret Art 52 EPC
in the same sense:  %(q:Software should not be patentable when there
is no technological innovation, and technological innovations should
not cease to be patentable merely because the innovation lies in
software), thereby stating indirectly that software innovations are
patentable inventions, whereas Art 52 EPC clearly says that
mathematical methods, algorithms and programs for computers are not
inventions in the sense of patent law.  The UKPO bases this conclusion
on a consultation conducted by the UKPO in 2000, in which most of the
participants, especially nearly all the software professionals, said
they did not want software to be covered by patents.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ukpo-swpat0202 ;
# txtlang: en ;
# multlin: t ;
# End: ;

