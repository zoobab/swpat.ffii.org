\begin{subdocument}{mdp-unice02}{UNICE 2002/09: CEC/BSA does not go far enough}{http://swpat.ffii.org/papers/mdp-unice02/index.en.html}{Workgroup\\swpatag@ffii.org}{This UNICE position paper was first published in ``Mitteilungen der Patentanw\"{a}lte'' (MDP), a monthly bulletin of a german patent lawyer association.  MDP does not say who the authors are and when the text was drafted or adopted.  The text is evidently written from a patent lawyer perspective on the unquestioned presupposition that the software industry needs ``patent protection''.  It complains about alleged insufficiencies of the CEC proposal in awarding this protection and in ensuring that any software innovation is beyond doubt patentable.}
UNICE -- Proposal for a directive on the patentability of computer-implemented inventions COM (2002) 92 FINAL

\begin{sect}{genkmt}{GENERAL COMMENTS}
\begin{quote}
{\it UNICE welcomes the release of the Commission's proposal for a Directive on the patentability of computer-implemented inventions (``the Directive'') since Industry has been waiting for such an initiative for a long time.}

{\it As UNICE has repeatedly pointed out, the legal landscape surrounding patentability of software-related inventions has changed dramatically over the last few years, requiring the protection of technology and technological advances to follow this progress by adapting.}

{\it UNICE comments on the consultation paper by the European Commission on computer-implemented inventions (18 December 200O)}

{\it The digital environment is one of the most promising in terms of innovation and economic development. The ability for Europe to position itse(f on this market will impact substantially on its economy and wealth.}

{\it Europe needs to remove the current ambiguity and legal uncertainty, which surrounds patentability of software-related inventions if it wants to support innovation in this field and not to debar European companies from access to those markets. If there is no rapid action, this market will be dominated by Europe's main trading partners, in particular Japan and the USA where there are no restrictions on patenting software-related inventions that meet the normal standards of novelty and obviousness.}

{\it In this context, UNICE supports the broad intention of the Directive as proposed by the Commission on the patentability of computer-implemented inventions (COM (2002) 92 final) as a means to harmonise the different interpretations given by the European Paptent Office Boards of Appeal and national courts on the relevant questions.}

{\it Building upon this proposal, UNICE has high expectations that the European institutions will discuss the Commission document in a constructive and effective way, ensuring that the foreseen harmonisation fully meets users' needs without jeopardising the quality of the patent system in Europe or putting at stake the well-functioning of the Internal Market.}

{\it However UNICE has some cause for concern. First of all, UNICE believes that the current Commission's proposal does not fully keep with its declared purpose of ``avoiding any sudden change in the legal position'', as stated in the Explanatory Memorandum.}

{\it In fact, at the least, the rules concerning the form of claims would represent a major step backwards from the EPC, as interpreced by the EPO Board of Appeal, as well as by the Courts of the two EU countries, UK and Germany, that have so far developed the majority of national level jurisprudence in the field of computer-implemented inventions, as acknowledged by the Commission'. This may represent a thorny problem in the future. Moreover, UNICE considers that the proposed Directive contains a series of ambiguities that should be removed.}

{\it Against this background, UNICE would like to submit the following comments.}
\end{quote}
\end{sect}

\begin{sect}{prgklm}{PROGRAM CLAIMS}
In one respect the draft makes a policv choice that UNICE cannot agree with and which it believes should be reversed during the detailed consideration of the Directive.

Article 5 limits the forms of claims, stepping back from the two favourable decisions of the Technical Board of Appeal (T 1173/97 and T 0935/9/), which allowed ``computer program products''. This drawback is also bound to create a difference as compared with Japanese and US legislation where isolated softmare is patentable.

In UNICE's view, the exclusion of claims directed to program products, even when they relate to a perfectly patentable invention having a technica( character, takes away a significant part of the economic value of the patent. In addition, the exclusion of claims for products in a field of technology is directly contrary to Article 27(1) TRIPS.

Besides, it is acknowledged that patent and copyright protection are complementary and that the exercise of a patent covering a computer-implemented invention should not interfere with the freedoms granted under copyright law by Directive 91/250/EEC. However, copyright protection for isolated software is not enough because it would mean that the use and distribution of a piece of software stored as a file (e.g. on a disk or down loaded from the Internet) in other than the original expression could not be prevented as a direct infringement. This can only be achieved by patent protection of isolated sofeware.

UNICE believes that this provision should also include the protection of a program stored as a file on a data carrier (e.g. disk) or transmitted as a signal. Therefore, it is necessary to obtain patent protection also for an isolated computer program stored as a file or transmitted as a signal (e.g. in the form of a ``computer program product'').

%3

See

Explanatory

Memorandum,

pp.

9-11.

%350

%Mitteilungen

der

deutschen

Patentanwälte

The departure from the current practice of the EPO would set up a problematical and undesirable divergence between mhat the EPO permits and the laws of a large number of its Member States. It would also give rise to doubts as to the status of the many existing granted rights of this kind.

Given that contributory infringement only exists when infringement is carried out completely in the same territory, contributory infringement is not a useful remedy to repair the damage caused by the exclusion of technical computer programs when the supply is from one member state and results in infringement in another.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

