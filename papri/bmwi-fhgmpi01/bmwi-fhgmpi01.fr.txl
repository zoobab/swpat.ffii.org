<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Adr: Auch diese französischen Artikel werten die Studie als eine Stellungnahme %(q:für Opensource und gegen Softwarepatente).

#iaW: interpretiert die Studie als eine Stellungnahme der deutschen Regierung %(q:für Opensource und gegen Softwarepatente), meint aber, die EU-Kommission würde sich davon nicht beeinflussen lassen und Softwarepatente in jedem Falle durchdrücken.  Diese Deutung erscheint sachlich unbegründet.  Über die Hintergründe kann man spekulieren.

#Att: Noch ein Internet-Nachrichtendienst findet, dass sich mit der BMWi-Presseerklärung trefflich Nachrichtenspalten füllen lassen.

#BWs: Bundeswirtschaftsministerium wendet sich gegen breite Patentierbarkeit von Software, Auftragsstudie kommt jedoch zu keinem klaren Resultat

#Eav: Ein Heise-Journalist hat die BMWi-Presseerklärung entdeckt und abgeschrieben.

#Sei: Softwarepatente -- Alles bleibt wie es ist?

#Bae: Die für die Studie zuständige BMWi-Referentin zeigt auf Powerpoint-Folien die Problematik der Softwarepatente auf. Vortrag vor Industrieverband im Mai 2001. Zeigt zurückhaltend sachlich Streitfragen auf, berichtet über BMWi-Studienprojekte.

#Dee: Dieser Artikel nimmt den MPI-Teil des Gutachtens, wie es am 10. Juli vorgestellt wurde, bereits vorweg.

#Sry: Sitzungsbericht von PA Axel Horns mit Ergänzungen und Kritik von Hartmut Pilch; daran anschließend weitere Diskussionen

#saW: sieht einiges positive an der Studie.  S. auch anschließende Debatte.

#kWW: kritisiert den Vorschlag der Studie, Entscheidungen über grundlegende Freiheiten auf der Ebene von EPA-Prüfungsrichtlinien zu treffen, als Verstoß gegen das Demokratieprinzip.  S. auch anschließende Debatten.

#Eaz: Einer der Fraunhofer-ISI-Autoren protestiert gegen verbale Schärfen von Hartmut Pilch, der mit Blick auf die Studie und ihre unerwartete Rezeption durch das französische Publikum %(sm:gemeint) hatte %(q:Lügen haben kurze Beine).  In einer Antwort listet Pilch irreführende Argumentationen der Studie auf und hält damit in der Sache am Vorwurf der Unehrlichkeit fest.  Friedewald setzt eine weitere persönliche Attacke gegen die Person Pilch drauf, geht aber auf keines der Sachargumente ein.  Dieser Auftritt steht in der %(mb:MPI-Tradition des Mobbings gegen Kritiker).  Kein einziger Mitarbeiter der Gruppe hat bisher zu irgendwelchen Kritikpunkten Stellung genommen.

#Brt: Beide Institute empfehlen, die Praxis des EPA, die in allen für die Fragestellung wesentlichen Punkten der des US-Patentamtes gleicht, durch Gesetzgebungsakte anzuerkennen.  Keines der Institute zog eine Kritik an dieser Praxis überhaupt als Option in Betracht. Keines stellte die Frage, ob die Praxis des EPA dem Vorgaben des Gesetzgebers und den aktuellen oder damaligen volkswirtschaftlichen Zielen entspricht.

#pcW: BMWi veröffentlicht FHG/MPI-Gutachten -- erste Stellungnahme

#EWl: Eine Gegenüberstellung der Hauptthesen im zusammenfassenden Vorspann der Studie mit Gegenargumenten und relativierenden Zitaten aus dem Hauptteil

#Enu: Empfehlungen nicht von Befunden gestützt

#wWz: wirft der Studie methodische Beliebigkeit vor und weist zahlreiche Widersprüche nach.

#Btw: BMWi setzt auf umfassenden gewerblichen Rechtschutz für %(q:Produkte, Verfahren und Dienstleistungen) aller Art und beteiligt sich an der %(q:Diskussion über computer-implementierbare Erfindungen).

#Den: Die Gutachter plädieren für eine zügige Rechtsharmonisierung und -klarstellung im Rahmen der Europäischen Union, möglichst auch auf Ebene der Welthandelsorganisation.

#Blu: BMWi-Erklärung zur Studie

#The: Text ohne Leerzeilen

#TaW: Text mit zahlreichen Lehrzeilen

#Hre: Hieraus wurden unter Verlusten folgende Versionen gewonnen

#Ere: Entsperrte Version

#Dcr: Diese Datei verwendet patentierte Algorithmen und Sperrmechanismen, weshalb sie mit Ghostscript 5.50 (Grundlage freier/quelloffener Betriebssysteme) nicht darstellbar ist.

#DWc: Die vom BMWi veröffentlichte PDF-Datei

#DjW: Das MPI hat in den letzten Jahren in theologischer Manier stimmige wissenschaftliche Gebäude um einen morschen Kern herum aufgebaut.  Es ist an der Zeit, sich hiervon zu verabschieden und zu moderner Rechtswissenschaft im Sinne Gert Kolles voranzuschreiten.  Die Weisheit menschlicher Machwerke, egal von welcher %(q:höchstrichterlicher) Autorität, muss in Frage gestellt werden.

#PWn: Prof. Straus irrt nur in einem Punkt, nämlich dort, wo er unterstellt, bestehende rechtssystematische Überlegungen erforderten eine Patentierbarkeit von computer-implementierbaren Organisations- und Rechenregeln.  Das Gegenteil ist der Fall: %(q:Es verbietet sich, ...) und diejenigen, welche die Warnungen des BGH von 1976 nicht wahrhaben wollten, haben den Karren auch in rechtssystematischer Hinsicht in den Dreck gefahren.  Nur ein wirklich akuter Bedarf der Softwarebranche nach Patentierbarkeit hätte einen rechtssystematisch so halsbrecherischen Kurs rechtfertigen können.  Der ist aber, wie die ISI-Studie auch bestätigt hat, nicht auszumachen.

#Srl: Sowohl rein ökonomische als auch rein rechtliche Überlegungen verfehlen das Thema.  Insofern hatte Professor Straus recht, als er forderte, dass das Rechtssystem nicht unsystematischen kurzfristigen Brancheninteressen folgen kann.  Ökonomie im Sinne des hier von Straus passend zitierten Friedrich Hayek ist angewandte Matmematik, die der Verifizierung philosophischer Gebäude dient, bei denen erst einmal und vor allem Denkarbeit außerhalb der Empirie zu leisten ist.  Diese philosophischen Gebäude können wiederum in Verbindung mit dem Rechtssystem in Normen des Handelns umgesetzt werden.  Rechtsbegriffe wie etwa der der technischen Erfindung sind auch nach %(gk:Gert Kolle) (den Straus in seiner Rede ebenfalls würdigte) vorzugsweise auf fachübergreifende philosophische Begriffe zu stützen.  Eine Umsetzung von empirischen ökonomischen Erkenntnissen in juristische Empfehlungen ist nur über dieses philosophische Bindeglied möglich.  In der bisherigen ISI-MPI-Studie vermisst man dieses Bindeglied völlig.

#OWW: Ob der BGH hier eine zeitlose Wahrheit formuliert hat, mag letztlich dahinstehen.  Denn der Begriff der Technik erscheint auch sachlich als das einzige brauchbare Bindeglied zwischen dem ökonomischen und dem juristischen Teil der ISI/MPI-Studie.

#Ene: Par conséquent, il faut empêcher le système de protection des biens intellectuels d'obtenir une extension des limites de la technicité, concept qui serait alors détourné de son rôle. Il doit rester clair, bien au contraire, qu'une règle d'organisation ou de calcul en elle-même ne mérite pas de se voir protégée par un brevet si sa relation au domaine technique ne repose que sur l'emploi d'un ordinateur dans l'usage commercial qui en est fait. Il ne nous appartient pas de débattre ici de l'éventuelle protection qui peut lui être accordée soit par le droit d'auteur, soit par le droit de la concurrence.

#Dnu: Mais peu importe finalement. En effet le concept de technicité apparaît comme le seul qui puisse servir à distinguer clairement ce qui est brevetable de ce qui relève plutôt d'une autre forme de production intellectuelle de l'homme pour laquelle le brevet n'est ni approprié ni envisagé. Si l'on devait renoncer à cette ligne de partage, il n'y aurait plus, par exemple, de possibilité certaine de faire la différence entre les les prestations brevetables et celles pour lesquelles le législateur a attribué d'autres formes de protection, en particulier le droit d'auteur. Le système allemand de propriété industrielle et droit d'auteur repose essentiellement sur le fait que certaines formes de protection sont valables pour certaines catégories précises de prestations intellectuelles et qu'il faut éviter autant que possible les recoupements entre ces différents droits de protection. La loi sur les brevets n'a tout de même pas été conçue comme une loi à tout faire au sein de laquelle les catégories de prestations intellectuelles pour lesquelles rien n'a été prévu par la loi pourraient trouver une protection, mais comme une loi spécialisée, visante à protéger un ensemble clairement délimité de prestations intellectuelles : les prestations techniques, et elle a toujour été comprise et appliquée comme telle.

#Eii: De plus, on peut avoir de bonnes raisons de conclure que, étant donné l'unanimité avec laquelle la jurisprudence et la littérature ont constamment insisté sur les limitations de la brevetabilité aux inventions de nature technique, on peut parler d'un droit coutumier à ce sujet.

#Sai: Cependant, dans tous ces textes, l'utilisation planifiée de forces de la nature contrôlables est considérée comme une condition nécessaire pour que l'agrément soit donné au caractère technique d'une invention. Ainsi que nous l'avons exposé plus haut, l'inclusion des forces de la raison humaine en tant que telles dans le domaine des forces de la nature dont l'utilisation pour la création d'une innovation fonde son caractère technique, aurait pour conséquence directe l'attribution d'une signification technique à toutes les activités de la pensée qui en tant que série d'instructions sont susceptibles de causer un résultat d'une manière prévisible. A partir de là, le concept de technicité perdrait son rôle de critère, l'ensemble des réalisations de l'intelligence humaine - dont les l'envergure et les limites sont inconnues et imprévisibles - se verraient ouvrir les portes du droit des brevets.

#DWr: Die Entscheidung, ob diese oder andere Regeln sinnvoll und sachgerecht sind, muss letztlich der Politik überlassen bleiben, aber Ihr Gutachten legt in vieler Hinsischt nahe, dass nach wie vor die Weisheit der BGH-Richter gilt, die im %(dp:Dispositionsprogramm-Beschluss) 1976 schrieben:

#Ers: Eine besonders stringente Interpretation, die ich gerne explizit kritisiert hätte, wird von uns in %{URL} vorgeschlagen.  Dieser Text stützt sich auf die BGH-Beschlüsse %(dp:Dispositionsprogramm) (1976), %(wt:Walzstabteilung) (1980) u.a., auf die Prüfungsrichtlinien des Europäischen Patentamtes von 1978 und 1985 und auf das %(lp:Lehrbuch des Patentrechts) von Prof. Kraßer (1986).

#EeX: Erklären Sie bitte, warum Prof. Lenz, Dr. Kiesewetter-Köbinger und all diejenigen irren, die dem Gesetz eine klare Abgrenzungsregel zu entnehmen imstande sind!  Beziehen Sie sich dabei bitte auf die Einführung und Literaturliste in %{URL}!

#BwW: Bedenken Sie, dass der Fortbestand des Patentwesens auf Dauer von dieser Frage abhängt.  Wenn es wirklich nicht möglich sein sollte, diese Frage in befriedigender Weise zu beantworten, wird früher oder später das ganze Patentwesen untragbar werden.  In diesem Zusammenhang darf ich auch auf die %(sl:Stellungnahme von Prof. Dr. iur. Karl Friedrich Lenz an den Bundestag) hinweisen, der im übrigen auch in einem %(gl:Gutachten) erklärt hat, dass die jetzige Gesetzesregel sehr wohl klar ist, dass aber das EPA gegen sie verstößt.

#NtS: Nehmen Sie bitte zu den bekannten sinnvollen Interpretationen des Patentierungsausschlusses auf Programme für Datenverarbeitungsanlagen Stellung und erklären Sie bitte, warum diese %(q:nicht möglich) sind!

#ERe: Erklären Sie bitte unabhängig von der jetzigen Rechtslage, wie patentierbare von nicht patentierbaren Gegenständen unterschieden werden können!

#Ide: Ich schlage vor, dass Sie sich stattdessen bemühen, jetzt selber %(q:eine Aussage in der Sache) zu treffen.

#EeW: Es ist daher ein Gebot der .. Rechtsklarheit, auf die baldige Streichung .. hinzuwirken.  Gleichzeitig ist jedoch darauf hinzuweisen, dass in der Sache keine Aussage getroffen wird.

#Dqt: Das gesetzliche Patentierungsverbot für %(q:Computerprogramme als solche) lässt nicht nur keine sinnvolle Interpretation zu, ...

#Ien: In Ihrer Vorläufigen Zusammenfassung zu Ihrem Gutachten schreiben Sie zum Schluss

#Aam: Am 10. Juli 2001 fand eine Vorab-Präsentation der noch unvollendeten Studie im BMWi statt.  Den Teilnehmern, zu denen auch Vertreter von FFII, Linux-Verband u.a. gehörten, sollte Gelegenheit gegeben werden, ihre Kritikpunkte einzubringen.  Für die Autoren der Studie sprachen u.a. Prof. Josef Straus und Ralf Nack.  Der Vortrag von Straus blieb bei grundsätzlichem stehen, nämlich bei der Forderung, eine %(q:patentrechtliche Extrawurst für die Softwarebranche) müsse vermieden werden.  Folgenden offenen Brief sandte Hartmut Pilch (FFII/Eurolinux) am 18. Juli an Straus und den Verteiler der Teilnehmer.  Als Antwort erhielt er bislang nur eine Aufforderung der Sekretärin von Straus, unerwünschte Zusendungen dieser Art künftig zu unterlassen.  Im nachhinein zeigt sich, dass die MPI-Autoren weder auf diese noch auf sonstige auf der Veranstaltung geäußerte Kritik reagiert haben.

#Drs: Die Autoren betonen zwar, dass freie/quelloffene Software besonderen Schutz vor den Gefahren übermäßiger Patentierung verdiene, schlagen aber vor, hierzu vorerst keinerlei Maßnahmen zu ergreifen, da der TRIPS-Vertrag dies nicht gestatte.  Stattdessen solle man die Situation beobachten und notfalls eine TRIPS-Änderung im Rahmen der WTO anstreben, m.a.W. das Thema auf unabsehbare Zeit verschieben und zugleich vorab durch Aufgabe des Erfindung- und Technikbegriffs für ein volles Durchschlagen des TRIPS-Vertrages auf das europäische Patentrecht sorgen.  Somit empfiehlt die Studie letztlich eine Unterordnung der Freiheits- und Wohlstands-Interessen der Öffentlichkeit und der Softwarebranche (einschließlich Opensource, Shareware, KMU etc) unter höhere Notwendigkeiten des Patentwesens.  Während für die Autoren die Praxis des EPA einen unhinterfragbaren Kultstatus hat, ist die freie/quelloffene Software immerhin inzwischen auch zu einer mittelgroßen heiligen Kuh avanciert, die ebenfalls einen gewissen Tribut erfordert.  Erst dieses politische Dilemma schuf überhaupt die Gelegenheit zu einer Regierungsstudie.  Im Ergebnis wird aber doch nur die erste heilige Kuh genährt, während die zweite sich mit virtuellem Futter zu begnügen und auf eine (reelle) Opferung gefasst zu machen hat.

#DrW: Die Autoren schlagen vor, der Staat möge viel Geld ausgeben, um die Qualität der Patentprüfungen zu verbessern und die strukturelle Benachteiligung kleiner und mittlerer Unternehmen auszugleichen.  Wie viel Geld dies sein soll, wird nicht beziffert.  Eine %(as:andere Studie) geht jedoch von 3-5 Milliarden EUR pro Jahr aus.  Dieses Geld soll z.B. durch Institutionen der Patentbewegung wie z.B. die %(po:Patentoffensive des BMBF) ausgegeben werden.  Ferner werden auch weitere langjährige Anliegen der Patentbewegung wie z.B. die Wiedereinführung einer Neuheitsschonfrist unter dem Vorwand dieser Studie propagiert, obwohl gerade diese Forderung für die Softwarebranche erhebliche Gefahren birgt.  Auf diese Gefahren wird nicht eingegangen.

#Dtw: Die Autoren empfehlen, den Ausschluss von Programmen für Datenverarbeitungsanlagen von der Patentierbarkeit zu streichen und die Beschränkung des Patentwesens auf technische Erfindungen (Lehren zum unmittelbaren Einsatz beherrschbarer Naturkräfte) aufzuheben.  Künftig soll die Patentierbarkeit nur noch auf der Ebene von Prüfungsrichtlinien des EPA geregelt werden.  Hierbei handelt es sich jedoch um Verwaltungsvorschriften, die vom Präsidenten des EPA nach freiem Ermessen erlassen werden (sofern nach Umsetzung der Empfehlungen dieser Studie in Anbetracht von Art 27 TRIPS überhaupt noch ein Ermessensspielraum verbleibt, s. oben).  Wer dem demokratisch legitmierten Gesetzgeber die Entscheidungsbefugnis in grundrechtsrelevanten Fragen entzieht, handelt jedoch gegen das grundgesetzliche Demokratieprinzip.

#EpW: Die MPI-Patentjuristen fragen nicht, in wieweit die Einschränkung der Allgemeinen Handlungsfreiheit (Art 2 GG) und anderer von der Verfassung garantierter Freiheiten und Werte von Softwarepatenten berührt wird.  Nicht einmal die offensichtlichen Widersprüche zwischen Art 5 GG (Freiheit von Meinung, Information, Forschung, Lehre, Kunst etc) und der Erteilung von Ansprüchen auf Informationsgüter (Computerprogrammprodukt, Computerprogramm, Datenstruktur), wie das EPA sie seit 1998 praktiziert, werden untersucht.  Für das MPI genießt die %(q:autonome) (= US-zentristische) Auslegung einiger TRIPS-Klauseln erdrückenden Vorrang vor der freiheitlichen Grundordnung unseres Gemeinwesens.  Deshalb kommt es auch nicht zu der entscheidenden Frage, ob die genannten Freiheitseinschränkungen im Hinblick auf etwaige gesamtwirtschaftliche Vorteile der Patentierung zu rechtfertigen sind.  Angesichts der Ergebnisse des volkswirtschaftlichen Teils hätte diese Frage nur mit Nein beantwortet werden können.

#Dvi: Der vom MPI (Nack unter Anleitung von Prof Straus) erstellte juristische Teil verwendet bez Art 52 EPÜ keine der üblichen Methoden der Gesetzesauslegung (grammatisch-lexikalisch, systematisch, historisch, teleologisch, verfassungskonform) sondern versucht stattdessen aus zahlreichen Urteilen der EPA-Beschwerdekammern und anderer Spruchkörper verborgene Tendenzen herauszulesen und hieraus ein neues Wertgefüge zu konstruieren.  Nack vermischt dabei Urteile aus 3 Jahrzehnten zu einem Gesamtbild zusammen und verschweigt, dass sich während dieser Zeit die zugrundeliegenden Normen grundlegend änderten.  Z.B. wird der BGH-Beschluss %(cs:Chinesische Schriftzeichen) von 1992 als Beleg dafür herangezogen, dass heute beim EPA %(q:Textverarbeitungssoftware tendenziell nicht patentierbar) sei.  Nack weiß sehr wohl, dass der BGH sich inzwischen von seiner damaligen %(q:restriktiven) Auffassung abgewandt hat.  Doch nur duch Verrührung von Urteilen zu einem Datenbrei lässt sich jene höhere objektive Wirklichkeit hinter der Rechtsprechung konstruieren, hinter der Nack die Subjektivität seiner Empfehlungen verbergen möchte.  Da wesentliche Abgrenzungskriterien im Datenbrei mit verrührt werden, bleiben im Ergebnis einerseits keinerlei Beschränkungen der Patentierbarkeit bestehen, andererseits lässt sich auch die beschränkende Praxis früherere Zeiten mitsamt ihren heutzutage politisch erwünschten %(q:restriktiven) Ergebnissen auf das Konto der empfohlenen heutigen EPA-Praxis verbuchen.  An die Stelle einer Begrenzung der Patentierbarkeit tritt schließlich die Unterscheidung nach %(q:Softwarearten, die in einer Ingenieurstradition stehen) und solchen, für die dies nicht gilt (wie z.B. Textverarbeitung gemäß %(q:chinesische Schriftzeichen)).  Da jedoch der Universalrechner selber zweifellos %(q:in einer Ingenieurstradition steht), genügt letztlich doch wieder die Nennung von Anspruchsmerkmalen wie %(q:Prozessor) und %(q:Anzeigegerät) in Kombination mit etwas informatischem Jargon, um alle Organisations- und Rechenregeln auf allen Gebieten der Patentierung zugänglich zu machen.  Für ein übriges sorgt Art 27 TRIPS:  wenn die Technizität von Textverarbeitungs-Rechenregeln nicht mehr verneint werden kann, ist eine patentrechtliche %(q:Diskriminierung) gegen solche Regeln ohnehin verboten.

#Dl2: Die Autoren bilden aus %(ar:Art 52.2,3 EPÜ) die Wortgruppe %(q:Programm für Datenverarbeitungsanlagen als solche), setzen diese mit %(q:Software als solche) gleich und definieren dies wiederum entgegen allen %(ep:bewährten Praktiken der Gesetzesauslegung) völlig willkürlich als %(q:von ihrer technischen Basis losgelöste Software).  Hierbei handelt es sich um einen unverständlichen Pleonasmus, denn Software (Logikalie) ist ja gerade durch ihren Gegensatz zu Hardware (Physikalie = technische Basis) definiert.  Der Neumannsche Universalrechner hat diese Loslösung (Abstraktion) des Logischen vom Physischen ermöglicht.   Die Autoren ziehen aber alte, in den 70er Jahren %(gk:kraftvoll widerlegte) Scheinargumente heran, um auch noch den Technikbegriff zu entstellen.  Dabei verweisen sie nicht auf die einschlägigen Debatten der 70er Jahre und erwähnen diese nicht einmal in ihrem umfangreichen Literaturverzeichnis.  So kann es nicht verwundern, dass sie, ähnlich wie das EPA heute, dem Art 52 EPÜ %(q:keine klare Bedeutung abgewinnen können).  Dennoch hantieren sie mit den von ihnen eigens entstellten Begriffen und befragen sogar hilflose Softwareentwickler und -unternehmer, ob %(q:Software als solche) aka %(q:von ihrer technischen Basis losgelöste Software) patentierbar sein soll oder nicht.

#Etr: Ein Angelpunkt der Argumentation ist der %(q:Status Quo).  Die Rechtslage in Europa wird mit der neuesten Rechtsprechung des Europäischen Patentamtes (EPA) gleichgesetzt.  %(sk:Die Möglichkeit, dass das EPA gesetzeswidrig agieren könnte), wird nicht in Betracht gezogen.   Obwohl sich die Praxis des EPA in bezug auf Software seit 1998 nicht mehr erkennbar von der des US-Patentamtes unterscheidet, fingiert die Studie noch immer zwei angeblich wesentliche Unterschiede: (1) Beim EPA sei %(q:Software als solche) nicht patentierbar; (2) Das EPA richte sich bezüglich Geschäftsmethoden nicht nach der State-Street-Entscheidung des amerikanischen Bundesgerichtes für Patentsachen (CAFC).  Beide Unterscheidungen sind für das Thema der Befragung irrelevant, denn (1) wie die Studie selbst feststellt hat Art 52.2,3 EPÜ für die heutige EPA-Rechtsprechung keine Bedeutung (2) bei State-Street geht es gerade nicht um Software sondern um die Möglichkeit, Geschäftsmethoden ohne Umweg über informatische Merkmale zu beanspruchen.  Die Studie baut offenbar auf der Tatsache auf, dass sich der Umbruch im EPA bei den meisten Softwarefirmen noch nicht herumgesprochen hat.  Wenn die meisten befragten Softwarefirmen %(q:den europäischen Status Quo beibehalten und amerikanische Verhältnisse meiden wollen), meinen sie damit höchstwahrscheinlich ihre gewohnte von Softwarepatenten weitgehend unbehelligte Arbeitsweise.  Die Autoren legen dies jedoch als eine Befürwortung der bereits amerikanisierten Praxis des EPA aus.  Um hiervon abzulenken, pochen sie auf fiktiven/irrelevanten Unterschieden zwischen EPA und USPTO.

#Dsr: Die Studie versucht, von den Ergebnissen der Befragung ausgehend politische Empfehlungen zu formulieren und mit dem Qualitätsprädikat %(q:wissenschaftlich) zu versehen.  Dies mag zwar dem Wunsch von Politikern nach Abnahme von Verantwortung entsprechen, ist aber für einen Wissenschaftler bei gutem Gewissen nicht zu leisten.  Aus einer Befragung von Unternehmen lassen sich eigentlich noch nicht einmal volkswirtschaftliche Schlussfolgerungen ziehen.  Somit beruhen die Empfehlungen des Gutachtens nicht auf dessen eigentlichem wissenschaftlichem Beitrag, sondern auf davon völlig unabhängigen rein gedanklichen Überlegungen.  Indem die Autoren beides mischen, verringern sie den Wert ihrer eigentlichen Leistung.  Besonders nachteilhaft ist, dass schillernde Begriffe aus patentjuristischen (Schein)debatten unbekümmert in den Fragebogen ebenso wie dessen Auswertung übernommen wurde, s. unten.  Dabei ist offensichtlich, dass die Leser diese Begriffe nicht oder nur in unvorhersagbarer Weise verstehen.

#KKh: Kernstück der Studie ist ein Fragebogen, der von einigen 100 Unternehmern und Entwicklern beantwortet wurde.  Untersuchungen dieser Art wurden bisher in Europa noch nicht durchgeführt.  Die Ergebnisse stimmen im wesentlichen mit %(ss:bisher bekannten Studien) überein.  Sie zeigen zum einen, dass die Softwarebranche an Patenten wenig Interesse zeigt und größtenteils skeptisch bis ablehnend gestimmt ist, zum anderen, dass sich positive Impulse für die gesamtwirtschaftliche Entwicklung schwer nachweisen lassen und dass mittelfristig eher nachteilige Auswirkungen auf öffentliche Güter wie Standards und freie/quelloffene Software zu erwarten sind.

#cvu: Eiertanz zwischen zwei heiligen Kühen: Opensource darf nicht ohne besorgte Mine geopfert werden

#Mrr: Nebenbei noch ein paar Bonbons für die Patentbewegung

#Mee: Verletzung des Demokratieprinzips

#VWi: Verfassungskonforme Auslegung fehlt, empfohlene Freiheitsbeschränkungen nicht legitimiert

#nss: naturalistischer Fehlschluss: Normbegründung aus (chaotischem) Ist-Zustand der Rechtsprechung

#Zek: Zirkelschluss: MPI kann entstellten Rechtsbegriffen keine Bedeutung abgewinnen

#Dce: Doppelbödige Begriffe: %(q:Status Quo), %(q:erweiterte Patentierbarkeit amerikanischen Stils)

#Pps: Politischer Anspruch trübt Wissenschaftlichkeit

#Wre: Wertvoller Kern: Repräsentative Befragung

#Wrk: Weitere Lektüre

#S0A: Schreiben vom Juli an die Autoren der Studie

#Gze: Grundsätzliche Bewertung

#descr: In 2001-01, the German Federal Ministery of Economy and Technology (BMWi) ordered a study on the economic effects of software patentability from well known think tanks with close affinity to the German patent establishment:  the Fraunhofer Institute for Innovation Research (ISI.fhg.de), the Fraunhofer Patent Agency (PST.fhg.de) and the Max Planck Institute for Foreign and International Patent, Copyright and Competition Law (MPI = intellecprop.mpg.de).  The study was largely concluded in 2001-06 and preliminary results were presented to a selected audience.  The final report was published by the BMWi on 2001-11-15.  The study is based on an opinion poll answered by several hundred software company representatives and independent software developpers, conducted by Fraunhofer ISI.  Most respondents have had little experience with software patents and don't want software patents to become a daily reality like in the US.  The poll also investigated the significance of open source software for these companies and found it to be of substantial importance as a common infrastructure.  Based on these findings, the Fraunhofer authors predict that an increase in the use of software patents will put many software companies out of business and slow down innovation in the software field.  The study then jumps to conclude that software patents must be legalised and SMEs must be better informed about them.  This surprising conclusion is drawn by the patent law scholars from MPI.  The MPI's legal study does not explore any ways to redraw the borders between patents and copyright but just takes the EPO and USPTO practise as an inevitable reality.  They find that the EPO's caselaw is contradictory and chaotic and blame this on Art 52.2c EPC, which they say has failed to provide clear guidance and should therefore be deleted.  Business related algorithms are, they say, less likely to be patented at the EPO than algorithms that %(q:stand in a tradition of engineering).  The MPI writers however do not try to provide a clear rule for distinguishing the two, and they oppose the idea of drawing a line between the physical and the logical (%(q:technical inventions) vs %(q:rules of organisation and calculation)) as done by lawcourts in the 70s and 80s, asserting that information is also a physical phenomenon.  They propose that all legislative power concerning the limits of patentability be handed over to the EPO, which should then, at its discretion and as far as Art 27 TRIPS allows, consult experts of interested parties for regular rewriting of its Examination Guidelines.  Art 27 TRIPS demands that patents be %(q:available in all fields of technology), and the MPI understands %(q:technology) as %(q:the useful arts) and is careful not to mention Kolle and other European theoreticians of the concept of technical invention.  Summarily the study can be summarised as %(q|Fraunhofer: software patents are unpopular in the software industry and dangerous to innovation and competition.  MPI:  Fine, so let's legalise them quickly.)

#title: Fraunhofer/MPI 2001: Étude Économique/Légale sur les Brevets Logiciels

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bmwi-fhgmpi01 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

