\contentsline {section}{\numberline {1}Basic Evaluation}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Valuable Core: Representative Enquete}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Political claims undermine scientificity}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Double-talk about ``status quo'' and ``american-type extensive patentability''}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Cirucular reasoning: MPI unable to understand self-obfuscated legal terms}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Naturalist fallacy: MPI attempts to derive normative statements from (chaotic) factual state of jurisdiction}{5}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Constitution-conformant interpretation missing, recommended freedom restrictions not legitimised}{5}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Violation of Democracy Principle}{6}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Wishlist of goodies for the patent movement}{6}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}``Without appropriate ritual weeping, opensource may not be sacrificed''}{6}{subsection.1.9}
\contentsline {section}{\numberline {2}Letter of 2001-07-18 to theauthors of the study}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Please explain independently of the current legal situation how patentable ideas can be reliably distinguished from non patentable ones!}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Please comment some well known meaningful interpretations of the exclusion of programs for comoputers and explain, why they are ``not possible''!}{8}{subsection.2.2}
\contentsline {section}{\numberline {3}Further Reading}{10}{section.3}
