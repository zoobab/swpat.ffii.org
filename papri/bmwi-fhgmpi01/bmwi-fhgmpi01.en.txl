<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fraunhofer/MPI 2001: Economic/Legal Study about Software Patents

#descr: In 2001-01, the German Federal Ministery of Economy and Technology
(BMWi) ordered a study on the economic effects of software
patentability from well known think tanks with close affinity to the
German patent establishment:  the Fraunhofer Institute for Innovation
Research (ISI.fhg.de), the Fraunhofer Patent Agency (PST.fhg.de) and
the Max Planck Institute for Foreign and International Patent,
Copyright and Competition Law (MPI = intellecprop.mpg.de).  The study
was largely concluded in 2001-06 and preliminary results were
presented to a selected audience.  The final report was published by
the BMWi on 2001-11-15.  The study is based on an opinion poll
answered by several hundred software company representatives and
independent software developpers, conducted by Fraunhofer ISI.  Most
respondents have had little experience with software patents and don't
want software patents to become a daily reality like in the US.  The
poll also investigated the significance of open source software for
these companies and found it to be of substantial importance as a
common infrastructure.  Based on these findings, the Fraunhofer
authors predict that an increase in the use of software patents will
put many software companies out of business and slow down innovation
in the software field.  The study then jumps to conclude that software
patents must be legalised and SMEs must be better informed about them.
 This surprising conclusion is drawn by the patent law scholars from
MPI.  The MPI's legal study does not explore any ways to redraw the
borders between patents and copyright but just takes the EPO and USPTO
practise as an inevitable reality.  They find that the EPO's caselaw
is contradictory and chaotic and blame this on Art 52.2c EPC, which
they say has failed to provide clear guidance and should therefore be
deleted.  Business related algorithms are, they say, less likely to be
patented at the EPO than algorithms that %(q:stand in a tradition of
engineering).  The MPI writers however do not try to provide a clear
rule for distinguishing the two, and they oppose the idea of drawing a
line between the physical and the logical (%(q:technical inventions)
vs %(q:rules of organisation and calculation)) as done by lawcourts in
the 70s and 80s, asserting that information is also a physical
phenomenon.  They propose that all legislative power concerning the
limits of patentability be handed over to the EPO, which should then,
at its discretion and as far as Art 27 TRIPs allows, consult experts
of interested parties for regular rewriting of its Examination
Guidelines.  Art 27 TRIPs demands that patents be %(q:available in all
fields of technology), and the MPI understands %(q:technology) as
%(q:the useful arts) and is careful not to mention Kolle and other
European theoreticians of the concept of technical invention. 
Summarily the study can be summarised as %(q|Fraunhofer: software
patents are unpopular in the software industry and dangerous to
innovation and competition.  MPI:  Fine, so let's legalise them
quickly.)

#Gze: Basic Evaluation

#S0A: Letter of 2001-07-18 to theauthors of the study

#Wrk: Further Reading

#Wre: Valuable Core: Representative Enquete

#Pps: Political claims undermine scientificity

#Dce: Double-talk about %(q:status quo) and %(q:american-type extensive
patentability)

#Zek: cirucular reasoning:  MPI unable to understand self-obfuscated legal
terms

#nss: naturalist fallacy: MPI attempts to derive normative statements from
(chaotic) factual state of jurisdiction

#VWi: constitution-conformant interpretation missing, recommended freedom
restrictions not legitimised

#Mee: Violation of Democracy Principle

#Mrr: Wishlist of goodies for the patent movement

#cvu: %(q:Without appropriate ritual weeping, opensource may not be
sacrificed)

#KKh: The core piece of the study consists in an opinion poll which was
answered by several 100 software creators and company representatives,
selected for representativity.  Investigations of this kind have so
far not been conducted in Europe.  The results largely conform to
%(ss:studies known so far).  They show that the software companies,
including young companies and even many large companies, tend to show
little interest in patents and would prefer that their field stay
largely free of patents.  Moreover, the study finds that software
companies are already depending to an unexpectedly large extent on
free standards and free/opensource software.  From these findings they
conclude that an %(q:extension of software patentability) would
probably lead to a reduction of growth and innovation in the software
industry.

#Dsr: Die Studie versucht, von den Ergebnissen der Befragung ausgehend
politische Empfehlungen zu formulieren und mit dem Qualitätsprädikat
%(q:wissenschaftlich) zu versehen.  Dies mag zwar dem Wunsch von
Politikern nach Abnahme von Verantwortung entsprechen, ist aber für
einen Wissenschaftler bei gutem Gewissen nicht zu leisten.  Aus einer
Befragung von Unternehmen lassen sich eigentlich noch nicht einmal
volkswirtschaftliche Schlussfolgerungen ziehen.  Somit beruhen die
Empfehlungen des Gutachtens nicht auf dessen eigentlichem
wissenschaftlichem Beitrag, sondern auf davon völlig unabhängigen rein
gedanklichen Überlegungen.  Indem die Autoren beides mischen,
verringern sie den Wert ihrer eigentlichen Leistung.  Besonders
nachteilhaft ist, dass schillernde Begriffe aus patentjuristischen
(Schein)debatten unbekümmert in den Fragebogen ebenso wie dessen
Auswertung übernommen wurde, s. unten.  Dabei ist offensichtlich, dass
die Leser diese Begriffe nicht oder nur in unvorhersagbarer Weise
verstehen.

#Etr: Ein Angelpunkt der Argumentation ist der %(q:Status Quo).  Die
Rechtslage in Europa wird mit der neuesten Rechtsprechung des
Europäischen Patentamtes (EPA) gleichgesetzt.  %(sk:Die Möglichkeit,
dass das EPA gesetzeswidrig agieren könnte), wird nicht in Betracht
gezogen.   Obwohl sich die Praxis des EPA in bezug auf Software seit
1998 nicht mehr erkennbar von der des US-Patentamtes unterscheidet,
fingiert die Studie noch immer zwei angeblich wesentliche
Unterschiede: (1) Beim EPA sei %(q:Software als solche) nicht
patentierbar; (2) Das EPA richte sich bezüglich Geschäftsmethoden
nicht nach der State-Street-Entscheidung des amerikanischen
Bundesgerichtes für Patentsachen (CAFC).  Beide Unterscheidungen sind
für das Thema der Befragung irrelevant, denn (1) wie die Studie selbst
feststellt hat Art 52.2,3 EPÜ für die heutige EPA-Rechtsprechung keine
Bedeutung (2) bei State-Street geht es gerade nicht um Software
sondern um die Möglichkeit, Geschäftsmethoden ohne Umweg über
informatische Merkmale zu beanspruchen.  Die Studie baut offenbar auf
der Tatsache auf, dass sich der Umbruch im EPA bei den meisten
Softwarefirmen noch nicht herumgesprochen hat.  Wenn die meisten
befragten Softwarefirmen %(q:den europäischen Status Quo beibehalten
und amerikanische Verhältnisse meiden wollen), meinen sie damit
höchstwahrscheinlich ihre gewohnte von Softwarepatenten weitgehend
unbehelligte Arbeitsweise.  Die Autoren legen dies jedoch als eine
Befürwortung der bereits amerikanisierten Praxis des EPA aus.  Um
hiervon abzulenken, pochen sie auf fiktiven/irrelevanten Unterschieden
zwischen EPA und USPTO.

#Dl2: Die Autoren bilden aus %(ar:Art 52.2,3 EPÜ) die Wortgruppe
%(q:Programm für Datenverarbeitungsanlagen als solche), setzen diese
mit %(q:Software als solche) gleich und definieren dies wiederum
entgegen allen %(ep:bewährten Praktiken der Gesetzesauslegung) völlig
willkürlich als %(q:von ihrer technischen Basis losgelöste Software). 
Hierbei handelt es sich um einen unverständlichen Pleonasmus, denn
Software (Logikalie) ist ja gerade durch ihren Gegensatz zu Hardware
(Physikalie = technische Basis) definiert.  Der Neumannsche
Universalrechner hat diese Loslösung (Abstraktion) des Logischen vom
Physischen ermöglicht.   Die Autoren ziehen aber alte, in den 70er
Jahren %(gk:kraftvoll widerlegte) Scheinargumente heran, um auch noch
den Technikbegriff zu entstellen.  Dabei verweisen sie nicht auf die
einschlägigen Debatten der 70er Jahre und erwähnen diese nicht einmal
in ihrem umfangreichen Literaturverzeichnis.  So kann es nicht
verwundern, dass sie, ähnlich wie das EPA heute, dem Art 52 EPÜ
%(q:keine klare Bedeutung abgewinnen können).  Dennoch hantieren sie
mit den von ihnen eigens entstellten Begriffen und befragen sogar
hilflose Softwareentwickler und -unternehmer, ob %(q:Software als
solche) aka %(q:von ihrer technischen Basis losgelöste Software)
patentierbar sein soll oder nicht.

#Dvi: Der vom MPI (Nack unter Anleitung von Prof Straus) erstellte
juristische Teil verwendet bez Art 52 EPÜ keine der üblichen Methoden
der Gesetzesauslegung (grammatisch-lexikalisch, systematisch,
historisch, teleologisch, verfassungskonform) sondern versucht
stattdessen aus zahlreichen Urteilen der EPA-Beschwerdekammern und
anderer Spruchkörper verborgene Tendenzen herauszulesen und hieraus
ein neues Wertgefüge zu konstruieren.  Nack vermischt dabei Urteile
aus 3 Jahrzehnten zu einem Gesamtbild zusammen und verschweigt, dass
sich während dieser Zeit die zugrundeliegenden Normen grundlegend
änderten.  Z.B. wird der BGH-Beschluss %(cs:Chinesische
Schriftzeichen) von 1992 als Beleg dafür herangezogen, dass heute beim
EPA %(q:Textverarbeitungssoftware tendenziell nicht patentierbar) sei.
 Nack weiß sehr wohl, dass der BGH sich inzwischen von seiner
damaligen %(q:restriktiven) Auffassung abgewandt hat.  Doch nur duch
Verrührung von Urteilen zu einem Datenbrei lässt sich jene höhere
objektive Wirklichkeit hinter der Rechtsprechung konstruieren, hinter
der Nack die Subjektivität seiner Empfehlungen verbergen möchte.  Da
wesentliche Abgrenzungskriterien im Datenbrei mit verrührt werden,
bleiben im Ergebnis einerseits keinerlei Beschränkungen der
Patentierbarkeit bestehen, andererseits lässt sich auch die
beschränkende Praxis früherere Zeiten mitsamt ihren heutzutage
politisch erwünschten %(q:restriktiven) Ergebnissen auf das Konto der
empfohlenen heutigen EPA-Praxis verbuchen.  An die Stelle einer
Begrenzung der Patentierbarkeit tritt schließlich die Unterscheidung
nach %(q:Softwarearten, die in einer Ingenieurstradition stehen) und
solchen, für die dies nicht gilt (wie z.B. Textverarbeitung gemäß
%(q:chinesische Schriftzeichen)).  Da jedoch der Universalrechner
selber zweifellos %(q:in einer Ingenieurstradition steht), genügt
letztlich doch wieder die Nennung von Anspruchsmerkmalen wie
%(q:Prozessor) und %(q:Anzeigegerät) in Kombination mit etwas
informatischem Jargon, um alle Organisations- und Rechenregeln auf
allen Gebieten der Patentierung zugänglich zu machen.  Für ein übriges
sorgt Art 27 TRIPs:  wenn die Technizität von
Textverarbeitungs-Rechenregeln nicht mehr verneint werden kann, ist
eine patentrechtliche %(q:Diskriminierung) gegen solche Regeln ohnehin
verboten.

#EpW: Die MPI-Patentjuristen fragen nicht, in wieweit die Einschränkung der
Allgemeinen Handlungsfreiheit (Art 2 GG) und anderer von der
Verfassung garantierter Freiheiten und Werte von Softwarepatenten
berührt wird.  Nicht einmal die offensichtlichen Widersprüche zwischen
Art 5 GG (Freiheit von Meinung, Information, Forschung, Lehre, Kunst
etc) und der Erteilung von Ansprüchen auf Informationsgüter
(Computerprogrammprodukt, Computerprogramm, Datenstruktur), wie das
EPA sie seit 1998 praktiziert, werden untersucht.  Für das MPI genießt
die %(q:autonome) (= US-zentristische) Auslegung einiger
TRIPs-Klauseln erdrückenden Vorrang vor der freiheitlichen
Grundordnung unseres Gemeinwesens.  Deshalb kommt es auch nicht zu der
entscheidenden Frage, ob die genannten Freiheitseinschränkungen im
Hinblick auf etwaige gesamtwirtschaftliche Vorteile der Patentierung
zu rechtfertigen sind.  Angesichts der Ergebnisse des
volkswirtschaftlichen Teils hätte diese Frage nur mit Nein beantwortet
werden können.

#Dtw: Die Autoren empfehlen, den Ausschluss von Programmen für
Datenverarbeitungsanlagen von der Patentierbarkeit zu streichen und
die Beschränkung des Patentwesens auf technische Erfindungen (Lehren
zum unmittelbaren Einsatz beherrschbarer Naturkräfte) aufzuheben. 
Künftig soll die Patentierbarkeit nur noch auf der Ebene von
Prüfungsrichtlinien des EPA geregelt werden.  Hierbei handelt es sich
jedoch um Verwaltungsvorschriften, die vom Präsidenten des EPA nach
freiem Ermessen erlassen werden (sofern nach Umsetzung der
Empfehlungen dieser Studie in Anbetracht von Art 27 TRIPs überhaupt
noch ein Ermessensspielraum verbleibt, s. oben).  Wer dem demokratisch
legitmierten Gesetzgeber die Entscheidungsbefugnis in
grundrechtsrelevanten Fragen entzieht, handelt jedoch gegen das
grundgesetzliche Demokratieprinzip.

#DrW: Die Autoren schlagen vor, der Staat möge viel Geld ausgeben, um die
Qualität der Patentprüfungen zu verbessern und die strukturelle
Benachteiligung kleiner und mittlerer Unternehmen auszugleichen.  Wie
viel Geld dies sein soll, wird nicht beziffert.  Eine %(as:andere
Studie) geht jedoch von 3-5 Milliarden EUR pro Jahr aus.  Dieses Geld
soll z.B. durch Institutionen der Patentbewegung wie z.B. die
%(po:Patentoffensive des BMBF) ausgegeben werden.  Ferner werden auch
weitere langjährige Anliegen der Patentbewegung wie z.B. die
Wiedereinführung einer Neuheitsschonfrist unter dem Vorwand dieser
Studie propagiert, obwohl gerade diese Forderung für die
Softwarebranche erhebliche Gefahren birgt.  Auf diese Gefahren wird
nicht eingegangen.

#Drs: Die Autoren betonen zwar, dass freie/quelloffene Software besonderen
Schutz vor den Gefahren übermäßiger Patentierung verdiene, schlagen
aber vor, hierzu vorerst keinerlei Maßnahmen zu ergreifen, da der
TRIPs-Vertrag dies nicht gestatte.  Stattdessen solle man die
Situation beobachten und notfalls eine TRIPs-Änderung im Rahmen der
WTO anstreben, m.a.W. das Thema auf unabsehbare Zeit verschieben und
zugleich vorab durch Aufgabe des Erfindung- und Technikbegriffs für
ein volles Durchschlagen des TRIPs-Vertrages auf das europäische
Patentrecht sorgen.  Somit empfiehlt die Studie letztlich eine
Unterordnung der Freiheits- und Wohlstands-Interessen der
Öffentlichkeit und der Softwarebranche (einschließlich Opensource,
Shareware, KMU etc) unter höhere Notwendigkeiten des Patentwesens. 
Während für die Autoren die Praxis des EPA einen unhinterfragbaren
Kultstatus hat, ist die freie/quelloffene Software immerhin inzwischen
auch zu einer mittelgroßen heiligen Kuh avanciert, die ebenfalls einen
gewissen Tribut erfordert.  Erst dieses politische Dilemma schuf
überhaupt die Gelegenheit zu einer Regierungsstudie.  Im Ergebnis wird
aber doch nur die erste heilige Kuh genährt, während die zweite sich
mit virtuellem Futter zu begnügen und auf eine (reelle) Opferung
gefasst zu machen hat.

#Aam: On 2001-10-07 a pre-presentation of the unfinished study took place in
 the BMWi.  The participants, which included vocal patent critics from
FFII and other organisations, had an opportunity to raise issues for
the purpose of possibly including them in the study, it was said. 
Prof. Josef Straus, Ralf Nack from MPI and two ISI people spoke on
behalf of the study authors.  Straus limited his speech to questions
of principle, namely the demand that the consistency of patent law
needs to be defended against any %(q:special wishes of the software
industry or other industries).  Hartmut Pilch from FFII/Eurolinux sent
the following letter to Straus and other participants of the meeting
on 2001-07-18.  The only response we have received so far was a
request from a secretary of prof. Straus to stop sending them
unsollicited email.  Meanwhile it has become clear that the MPI
authors not only did not react to this letter but also failed to
incorporate any of the many critical suggestions raised at the
pre-presentation meeting.

#Ien: In your preliminary summary of the expert opinion you write at the end

#Dqt: The legal exclusion of %(q:computer programs as such) does not allow a
meaningful interpretation, ...

#EeW: It is therefore necessary in the interest of clarity of law to press
for a rapid deletion.  At the same time it should be made clear, that
this does not imply any factual statement.

#Ide: I propose that you endeavor to make this needed %(q:factual statement)
within your study.

#ERe: Please explain independently of the current legal situation how
patentable ideas can be reliably distinguished from non patentable
ones!

#NtS: Please comment some well known meaningful interpretations of the
exclusion of programs for comoputers and explain, why they are %(q:not
possible)!

#BwW: Bedenken Sie, dass der Fortbestand des Patentwesens auf Dauer von
dieser Frage abhängt.  Wenn es wirklich nicht möglich sein sollte,
diese Frage in befriedigender Weise zu beantworten, wird früher oder
später das ganze Patentwesen untragbar werden.  In diesem Zusammenhang
darf ich auch auf die %(sl:Stellungnahme von Prof. Dr. iur. Karl
Friedrich Lenz an den Bundestag) hinweisen, der im übrigen auch in
einem %(gl:Gutachten) erklärt hat, dass die jetzige Gesetzesregel sehr
wohl klar ist, dass aber das EPA gegen sie verstößt.

#EeX: Erklären Sie bitte, warum Prof. Lenz, Dr. Kiesewetter-Köbinger und all
diejenigen irren, die dem Gesetz eine klare Abgrenzungsregel zu
entnehmen imstande sind!  Beziehen Sie sich dabei bitte auf die
Einführung und Literaturliste in %{URL}!

#Ers: Eine besonders stringente Interpretation, die ich gerne explizit
kritisiert hätte, wird von uns in %{URL} vorgeschlagen.  Dieser Text
stützt sich auf die BGH-Beschlüsse %(dp:Dispositionsprogramm) (1976),
%(wt:Walzstabteilung) (1980) u.a., auf die Prüfungsrichtlinien des
Europäischen Patentamtes von 1978 und 1985 und auf das %(lp:Lehrbuch
des Patentrechts) von Prof. Kraßer (1986).

#DWr: The decision, whether this or other rules are meaningfule and
adequate, must ultimately be left to the political level, but your
study seems to demonstrate in many respects, that the wisdom of the
BGH judges is still valid, who wrote in the %(dp:Dispositionsprogramm
decision of 1976):

#Sai: However in all cases the plan-conformant utilisation of controllable
natural forces has been named as an essential precondition for
asserting the technical character of an invention.  As shown above,
the inclusion of human mental forces as such into the realm of the
natural forces, on whose utilisation in creating an innovation the
technical character of that innovation is founded, would lead to the
consequence that virtually all results of human mental activity, as
far as they constitute an instruction for plan-conformant action and
are causally overseeable, would have to be attributed a technical
meaning.  In doing so, we would however de facto give up the concept
of the technical invention and extend the patent system to a vast
field of achievements of the human mind whose essence and limits can
neither be recognized nor overseen.

#Eii: It can furthermore be argued with good reasons that, given the
unanimity with which the jurisdiction and the legal literature have
always insisted on limiting the patent system to technical inventions,
the above reasoning constitutes a theorem of customary patent law.

#Dnu: Whether we want to postulate such a theorem is however not essential
for this discussion, because also from a purely objective point of
view the concept of technical character seems to be the only usable
criterion for delimiting inventions against other human mental
achievements, for which patent protection is neither intended nor
appropriate.  If we gave up this delimitation, there would for example
no longer be a secure possibility of distinguishing patentable
achievements from achievements, for which the legislator has provided
other means of protection, especially copyright protection.  The
system of German industrial property and copyright protection is
however founded upon the basic assumption that for specific kinds of
mental achievements different specially adapted protection regulations
are in force, and that overlappings between these different protection
rights need to be excluded as far as possible.  The patent system is
also not conceived as a reception basin, in which all otherwise not
legally privileged mental achievements should find protection.  It was
on the contrary conceived as a special law for the protection of a
delimited sphere of mental achievements, namely the technical ones,
and it has always been understood and applied in this way.

#Ene: Any attempt to attain the protection of mental achievements by means
of extending the limits of the technical invention -- and thereby in
fact giving up this concept -- leads onto a forbidden path.  We must
therefore insist that a pure rule of organisation and calculation,
whose sole relation to the realm of technology consists in its
usability for the normal operation of a known computer, does not
deserve patent protection.  Whether it can be awarded protection under
some other regime, e.g. copyright or competition law, is outside the
scope of our discussion.

#OWW: Whether the BGH has here forulated a timeless truth is however not
essential for this discussion, because the concept of technical
invention appears also from a factual point of view to be the only
usable connecting element between the economic and the legal part of
the ISI/MPI study.

#Srl: Sowohl rein ökonomische als auch rein rechtliche Überlegungen
verfehlen das Thema.  Insofern hatte Professor Straus recht, als er
forderte, dass das Rechtssystem nicht unsystematischen kurzfristigen
Brancheninteressen folgen kann.  Ökonomie im Sinne des hier von Straus
passend zitierten Friedrich Hayek ist angewandte Matmematik, die der
Verifizierung philosophischer Gebäude dient, bei denen erst einmal und
vor allem Denkarbeit außerhalb der Empirie zu leisten ist.  Diese
philosophischen Gebäude können wiederum in Verbindung mit dem
Rechtssystem in Normen des Handelns umgesetzt werden.  Rechtsbegriffe
wie etwa der der technischen Erfindung sind auch nach %(gk:Gert Kolle)
(den Straus in seiner Rede ebenfalls würdigte) vorzugsweise auf
fachübergreifende philosophische Begriffe zu stützen.  Eine Umsetzung
von empirischen ökonomischen Erkenntnissen in juristische Empfehlungen
ist nur über dieses philosophische Bindeglied möglich.  In der
bisherigen ISI-MPI-Studie vermisst man dieses Bindeglied völlig.

#PWn: Prof. Straus irrt nur in einem Punkt, nämlich dort, wo er unterstellt,
bestehende rechtssystematische Überlegungen erforderten eine
Patentierbarkeit von computer-implementierbaren Organisations- und
Rechenregeln.  Das Gegenteil ist der Fall: %(q:Es verbietet sich, ...)
und diejenigen, welche die Warnungen des BGH von 1976 nicht wahrhaben
wollten, haben den Karren auch in rechtssystematischer Hinsicht in den
Dreck gefahren.  Nur ein wirklich akuter Bedarf der Softwarebranche
nach Patentierbarkeit hätte einen rechtssystematisch so
halsbrecherischen Kurs rechtfertigen können.  Der ist aber, wie die
ISI-Studie auch bestätigt hat, nicht auszumachen.

#DjW: Das MPI hat in den letzten Jahren in theologischer Manier stimmige
wissenschaftliche Gebäude um einen morschen Kern herum aufgebaut.  Es
ist an der Zeit, sich hiervon zu verabschieden und zu moderner
Rechtswissenschaft im Sinne Gert Kolles voranzuschreiten.  Die
Weisheit menschlicher Machwerke, egal von welcher
%(q:höchstrichterlicher) Autorität, muss in Frage gestellt werden.

#DWc: The PDF file published by BMWi

#Dcr: This file uses patent algorithms and blocking mechanisms, which is why
it is not readable using Ghostscript 5.50 (basis of free/opensource
operating systems).

#Ere: Unblocked Version

#Hre: From this the following versions were derived, with losses

#TaW: text with many empty lines

#The: Text without empty lines

#Blu: BMWi-Erklärung zur Studie

#Den: The experts demand a swift harmonisation and clarification at the EU
level and, if possible, also at the WTO level.

#Btw: The BMWi advocates comprehensive industrial property protection for
all kinds of %(q:products, processes and services) and participates in
the %(q:discussion about computer-implementable inventions)

#wWz: A professor of German and European law accuses the study of shoddy and
arbitrary legal reasoning and points out many contradictions.

#Enu: recommendations not supported by findings

#EWl: A juxtaposition of the main theses of the study with counter-arguments
and relativising quotations from the main part

#pcW: BMWi veröffentlicht FHG/MPI-Gutachten -- erste Stellungnahme

#Brt: The economic part of the study contains some valuable insights, but
the legal part is hardly more than a highly deceptive patent movement
FUD pamphlet.

#Eaz: One of the Fraunhofer ISI authors protests against Pilch's verbal
harshness and attacks Pilch's personal integrity.  Pilch elaborates
his criticism, but the response is again only a personal attack.  This
method conforms to the %(mb:MPI tradition of bullying tactics against
critics).

#kWW: criticises the study's proposal to decide matters of limiting
patentability, which touch upon questions of constitutionally
guaranteed freedoms, by means of administrative regulations of the
EPO, says that this is contrary to the democracy principle of the
German Basic Law.

#saW: A software entrepreneur stresses the positive achievements found in
the economic part of the study, suggests that we should ignore the
legal junktalk and instead draw our own conclusions.

#Sry: report by PA Axel Horns with supplements and criticism from Hartmut
Pilch and ensuing further discussions.

#Dee: This article already contains all the characterising features of the
legal reasoning found in this study.

#Bae: The person in charge of the study at the ministery explains the
problems of software patenting and the approach of the study in a
ms-powerpoint presentation before software industry representatives.

#Sei: Softwarepatente -- Alles bleibt wie es ist?

#Eav: A Heise journalist has discovered and copied the BMWi press release.

#BWs: Bundeswirtschaftsministerium wendet sich gegen breite Patentierbarkeit
von Software, Auftragsstudie kommt jedoch zu keinem klaren Resultat

#Att: Another Internet news service finds the BMWi press release useful for
filling its column.

#iaW: A journalist interprets the study as an attempt by the German
government to fight %(q:for open source and against software patents)
but says that the European Commission will push a pro software patent
directive in any case.  This interpretation looks unfounded.  We may
speculate about the background of this strange article.

#Adr: These french articles also treat the study as a move of the German
government %(q:against software patents and in favor of free
software).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bmwi-fhgmpi01 ;
# txtlang: en ;
# multlin: t ;
# End: ;

