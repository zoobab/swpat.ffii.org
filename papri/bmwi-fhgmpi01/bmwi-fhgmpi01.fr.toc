\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}Grunds\"{a}tzliche Bewertung}{2}{section.0.1}
\contentsline {subsection}{\numberline {1.1}Wertvoller Kern\@DP Repr\"{a}sentative Befragung}{2}{subsection.0.1.1}
\contentsline {subsection}{\numberline {1.2}Politischer Anspruch tr\"{u}bt Wissenschaftlichkeit}{3}{subsection.0.1.2}
\contentsline {subsection}{\numberline {1.3}Doppelb\"{o}dige Begriffe\@DP ``Status Quo'', ``erweiterte Patentierbarkeit amerikanischen Stils''}{3}{subsection.0.1.3}
\contentsline {subsection}{\numberline {1.4}Zirkelschluss\@DP MPI kann entstellten Rechtsbegriffen keine Bedeutung abgewinnen}{4}{subsection.0.1.4}
\contentsline {subsection}{\numberline {1.5}naturalistischer Fehlschluss\@DP Normbegr\"{u}ndung aus (chaotischem) Ist-Zustand der Rechtsprechung}{4}{subsection.0.1.5}
\contentsline {subsection}{\numberline {1.6}Verfassungskonforme Auslegung fehlt, empfohlene Freiheitsbeschr\"{a}nkungen nicht legitimiert}{5}{subsection.0.1.6}
\contentsline {subsection}{\numberline {1.7}Verletzung des Demokratieprinzips}{5}{subsection.0.1.7}
\contentsline {subsection}{\numberline {1.8}Nebenbei noch ein paar Bonbons f\"{u}r die Patentbewegung}{6}{subsection.0.1.8}
\contentsline {subsection}{\numberline {1.9}Eiertanz zwischen zwei heiligen K\"{u}hen\@DP Opensource darf nicht ohne besorgte Mine geopfert werden}{6}{subsection.0.1.9}
\contentsline {section}{\numberline {2}Schreiben vom Juli an die Autoren der Studie}{6}{section.0.2}
\contentsline {subsection}{\numberline {2.1}Erkl\"{a}ren Sie bitte unabh\"{a}ngig von der jetzigen Rechtslage, wie patentierbare von nicht patentierbaren Gegenst\"{a}nden unterschieden werden k\"{o}nnen\@PE }{7}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Nehmen Sie bitte zu den bekannten sinnvollen Interpretationen des Patentierungsausschlusses auf Programme f\"{u}r Datenverarbeitungsanlagen Stellung und erkl\"{a}ren Sie bitte, warum diese ``nicht m\"{o}glich'' sind\@PE }{7}{subsection.0.2.2}
\contentsline {section}{\numberline {3}Weitere Lekt\"{u}re}{10}{section.0.3}
