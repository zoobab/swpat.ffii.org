\contentsline {section}{\numberline {1}Grunds\"atzliche Bewertung}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Wertvoller Kern: Repr\"asentative Befragung}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Politischer Anspruch tr\"ubt Wissenschaftlichkeit}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Doppelb\"odige Begriffe: ``Status Quo'', ``erweiterte Patentierbarkeit amerikanischen Stils''}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Zirkelschluss: MPI kann entstellten Rechtsbegriffen keine Bedeutung abgewinnen}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Naturalistischer Fehlschluss: Normbegr\"undung aus (chaotischem) Ist-Zustand der Rechtsprechung}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Verfassungskonforme Auslegung fehlt, empfohlene Freiheitsbeschr\"ankungen nicht legitimiert}{5}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Verletzung des Demokratieprinzips}{5}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Nebenbei noch ein paar Bonbons f\"ur die Patentbewegung}{6}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Eiertanz zwischen zwei heiligen K\"uhen: Opensource darf nicht ohne besorgte Mine geopfert werden}{6}{subsection.1.9}
\contentsline {section}{\numberline {2}Schreiben vom Juli an die Autoren der Studie}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Erkl\"aren Sie bitte unabh\"angig von der jetzigen Rechtslage, wie patentierbare von nicht patentierbaren Gegenst\"anden unterschieden werden k\"onnen!}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Nehmen Sie bitte zu den bekannten sinnvollen Interpretationen des Patentierungsausschlusses auf Programme f\"ur Datenverarbeitungsanlagen Stellung und erkl\"aren Sie bitte, warum diese ``nicht m\"oglich'' sind!}{7}{subsection.2.2}
\contentsline {section}{\numberline {3}Weitere Lekt\"ure}{10}{section.3}
