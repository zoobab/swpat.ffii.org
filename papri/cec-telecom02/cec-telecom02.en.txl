<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Technology policy in the telecommunication sector -- Market responses
and economic impacts

#descr: A report issued by the European Commission, prepared by Heli Koski,
ETLA, the Research Institute of the Finnish Economy, and the London
School of Economics.  It studies the current situation of the European
telecommunication enterprises and concludes with a warning against
harmful effects of recent extensions of patentability on this sector.

#ssa: The European Commission (CEC)'s Directorate General for Enterprises
(DG Enterprise) has published a study which dwells extensively on the
effects of software patents in particular in the telecommunication
sector

#vri: Overview

#rum: A report issued by the European Commission, prepared by Heli Koski,
ETLA, the Research Institute of the Finnish Economy, and the London
School of Economics.  It studies the current situation of the European
telecommunication enterprises and concludes with a warning against
harmful effects of recent extensions of patentability on this sector.

#rum2: A report issued by the European Commission, prepared by Heli Koski,
ETLA, the Research Institute of the Finnish Economy, and the London
School of Economics.  It studies the current situation of the European
telecommunication enterprises and concludes with a warning against
harmful effects of recent extensions of patentability on this sector.

#wAr: The Study was written by a scientist from %(ETLA) and %(LSE) and is
available for download at %(URL).

#etw: On pages 28/29 the study discusses patents and copyrights.  It finds
that patents are generally granted for technical inventions and
criticises the extension of patentability to areas where previously
only copyright was available.

#WlW: Thus the study almost incidentally hits a central point of the debate
which we can summarise as follows

#aor: Many patent claims cover very general ideas

#Why: Many unsuspicious debatants such as %(ZVEI) have found that the
concept of invention has been softened and have expressed concern
about the %(cb:CEC/BSA directive proposal)'s tendency to codify and
reinforce this development.

#iie: The debate is suffering from the fact that terms such as %(q:technical
contribution) are understood in very different ways by the debatants.

#Wix: The study ends on page 33/34 with the following %(c:Conclusions)

#aiw: Highly tentative conclusions that can be made based on the literature
   suggest that stronger patent rights may create substantial problems
   in the telecommunication sector.  First, strong patent rights may
   cause %(q:patent portfolio race). In other words, companies may use
   patents primarily not to protect their technological invention
   itself but as instruments with which to trade in order to be able
   to negotiate access to external technologies. Given the observed
   entry deterrence strategies of the incumbents, stronger patent
rights    might provide with them new powerful weapons to defend
monopolistic    market positions. Thus, stronger patent rights may
hinder the    development of effective competition in the
telecommunication    markets. Patentability of principles or ideas
might further result in    strategic patenting against compatibility.
This could be particularly    lethal to the content industry and
further to the markets of the    future generations of cellular mobile
telephones and services.    Currently available empirical evidence
does not allow us to make    definite conclusions. However, it
suggests that strengthening of    patent rights in the communication
sector or extending patent    protection to cover intellectual
property currently protected by    copyright involves great potential
risks.

#ter: The study also criticses the Community Patent and tendencies for a
centralised European patent system as measures which are conducive to
the worrisome %(q:stronger patent rights).

#yWW: The risks caused by an excessively broad patenting of software are
reinforce by a future Community Patent.  One could also count the
efforts at building a %(up:Universitarian Patent System) and Basel II
among the strengthening factors.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cec-telecom02 ;
# txtlang: xx ;
# End: ;

