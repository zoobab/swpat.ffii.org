<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Hartmut PILCH und FFII">
<link href="http://localhost/phm" rev="made">
<link href="http://www.ffii.org/verein/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/04/10">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Reply-To" content="phm@a2e.de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="F&ouml;rderverein f&uuml;r eine Freie Informationelle Infrastruktur, Logikpatente, Geistiges Eigentum, Gewerblicher Rechtschutz, GE, GE &amp; GR, immaterielle Verm&ouml;gensgegenst&auml;nde, Immaterialg&uuml;terrecht, mathematische Methoden, Gesch&auml;ftsverfahren, Organisations- und Rechenregeln, invention, Nicht-Erfindungen, computer-implementierte Erfindungen, computer-implementierbare Erfindungen, software-bezogene Erfindungen, software-implementierte Erfindungen, Softwarepatent, Computer-Patente, Informationspatente, technische Erfindung, technischer Charakter, Technizit&auml;t, Technik, Datentechnik, gewerbliche Anwendbarkeit, Patentierbarkeit, materielles Patentrecht, Nutzungsrecht, Patentinflation, OpenSource, Standardisierung, Innovation, competition, Patentamt, Generaldirektion Binnenmarkt, Patentbewegung, Patentfamilie, Patentwesen, Patentrecht, Patentjuristen, Lobby">
<meta name="title" content="CEC/ETLA 2002: Technology policy in the telecommunication sector -- Market responses and economic impacts">
<meta name="description" content="Ein im Auftrag der Europ&auml;ischen Kommission geschriebener Forschungsbericht &uuml;ber den Zustand der Telekommunikationsunternehmen in Europa berichtet besort &uuml;ber diverse Tendenzen der Verst&auml;rkung des Patentwesens und insbesondere seine Ausdehnung auf Software, die zu unproduktivem Wettr&uuml;sten innerhalb der Telekom-Branche f&uuml;hren.">
<title>CEC/ETLA 2002</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.de.html"-->
<div id="doktit">
<h1>CEC/ETLA 2002: Technology policy in the telecommunication sector -- Market responses and economic impacts<br>
<!--#include virtual="../../banner0.de.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.de.html"-->
</div>
</div>
<div id="dokmid">
Die Europ&auml;ische Kommission, Generaldirektion Unternehmen, hat im Jahr 2002 eine Studie ver&ouml;ffentlicht, die sich auch stark mit Software-Patenten besch&auml;ftigt
<div class="sectmenu">
<ol type="1"><li><a href="#study">&Uuml;berblick</a></li>
<li><a href="#links">Kommentierte Verweise</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h2>1. <a name="study">&Uuml;berblick</a></h2>
</div>

<div class="sectbody">
<div class=cit>
Enterprise Papers No 8, 2002<p><div align="center">Technology policy in the telecommunication sector

Market responses and economic impacts</div>
</div><p>Die Studie wurde erstellt von einem Wissenschaftler des ETLA (Research Institute of the Finnish Economy) und der London School of Economics und steht zum Herunterladen unter <a href="http://europa.eu.int/comm/enterprise/library/enterprise-papers/paper8.htm">CEC/ETLA 2002: Technology policy in the telecommunication sector -- Market responses and economic impacts: Introduction</a> bereit.<p>Auf Seite 28/29 (Seite 31/32 der PDF-Datei) diskutiert die Studie Urheberrecht und Patente. Dabei stellt sie zuerst fest, dass Patente &uuml;blicherweise f&uuml;r technische Erfindungen (und die dahinter stehenden Ideen und Prinzipien) erteilt werden. Dann geht sie auf die Patentierbarkeit von Software und computer-implementierten Erfindungen ein. Anschlie&szlig;end kritisiert sie in diesem Zusammenhang die Ausweitung der Patentierbarkeit auf (reine) Ideen und Prinzipien in Bereichen, in denen fr&uuml;her nur das Copyright zum Schutz eines Werks zur Anwendung kam.<p>Damit arbeitet die Studie beinahe beil&auml;ufig einen Kernpunkt der Debatte heraus, den man wie folgt zusammenfassen k&ouml;nnte:
<ul><li>Die Patent-Anspr&uuml;che vieler erteilter und beantragter Software-Patente beanspruchen reine Ideen und Prinzipien (z. B. Algorithmen, sehr allgemein gehaltenen Anforderungen an eine Software). Das Erfordernis des &quot;Technischen Beitrags&quot; soll das theoretisch verhindern.</li>
<li>Nicht unwesentliche Teilnehmer der Debatte haben festgestellt, dass eine Aufweichung des Technik-Begriffs stattgefunden habe, die durch den Richtlinienvorschlag eher noch verst&auml;rkt wird und Ihre Besorgnis dar&uuml;ber ge&auml;u&szlig;ert (z. B. ein <a href="/akteure/zvei/index.de.html">ZVEI und Softwarepatente</a>-Arbeitskreis).</li>
<li>Die Debatte krankt oft daran, dass die Diskussion des &quot;Technischen Beitrags&quot; (teils in Kategorien abstrakter Begriffe, teils anhand konkreter Erfahrungen) wegen unterschiedlicher Interpretation oft aneinander vorbeil&auml;uft.</li></ul><p>Die Studie endet auf Seite 33/34 (Seite 36/37 der PDF-Datei) im Abschnitt <cite>Conclusions</cite><p><div class=cit>
Highly tentative conclusions that can be made based on the literature    suggest that stronger patent rights may create substantial problems    in the telecommunication sector.  First, strong patent rights may    cause &quot;patent portfolio race&quot;. In other words, companies may use    patents primarily not to protect their technological invention    itself but as instruments with which to trade in order to be able    to negotiate access to external technologies. Given the observed    entry deterrence strategies of the incumbents, stronger patent rights    might provide with them new powerful weapons to defend monopolistic    market positions. Thus, stronger patent rights may hinder the    development of effective competition in the telecommunication    markets. Patentability of principles or ideas might further result in    strategic patenting against compatibility. This could be particularly    lethal to the content industry and further to the markets of the    future generations of cellular mobile telephones and services.    Currently available empirical evidence does not allow us to make    definite conclusions. However, it suggests that strengthening of    patent rights in the communication sector or extending patent    protection to cover intellectual property currently protected by    copyright involves great potential risks.
</div><p>Interessant ist in diesem Zusammenhang auch, dass die Studie das in der EU angestrebte Gemeinschaftspatent und die zugeh&ouml;rige Gerichtsbarkeit als Tendenzen in Richtung der kritisierten &quot;stronger patent rights&quot; sieht (Seite 30, PDF-Datei Seite 33):<p><div class=cit>
The creation of a Community patent and the proposed establishment    of a Community intellectual property court suggest that the EU    countries are moving towards stronger patent rights.
</div><p>D. h. die Risiken, die bei einer zu weitgehenden Patentierung von Software auftreten, werden durch ein k&uuml;nftiges Gemeinschaftspatent nach Ansicht der Studie noch verst&auml;rkt. Basel II und Hochschulpatente k&ouml;nnte man ebenfalls noch als verst&auml;rkende Faktoren anf&uuml;hren.
</div>

<div class="secthead">
<h2>2. <a name="links">Kommentierte Verweise</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/archiv/spiegel/wirkung/index.de.html">Forschungsarbeiten &uuml;ber die Volkswirtschaftlichen Auswirkungen von Patenten</a></b></dt>
<dd>Seit Fritz Machlups Bericht an den US-Kongress von 1958 hat sich eine Reihe von Studien &uuml;ber die Wirkungen des Patentwesens auf verschiedene Bereiche der Volkswirtschaft angesammelt.  Einige besch&auml;ftigen sich mit besonderen Typen von Innovation (sequentiell, in komplexen Systemen) oder mit besonderen Branchen (Halbleiter, Software, Genetik).  Alle Studien weisen darauf hin, dass Patente zumindest an dem Ende der typologischen Skala, zu dem Software geh&ouml;rt, die Innovation eher hemmen als f&ouml;rdern.  Einige regierungsgef&ouml;rderte Studien aus dem Umfeld der Patentbewegung (Institute f&uuml;r Geistiges Eigentum etc) kombinieren negative Befunde &uuml;ber die volkswirtschaftlichen Wirkungen von Softwarepatenten mit der Forderung nach Legalisierung von Softwarepatenten.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/akteure/nokia/index.en.html">Nokia und Software-Patente</a></b></dt>
<dd>Tim Frain, head of Nokia's patent department, is a &quot;permanent resident&quot; of the European parliament and has used every opportunity to ask politicians in Brussels and in Finland to support the European Commission's software patentability directive.  He is present at conferences everywhere.  He argues that small companies badly need software patents because otherwise their ideas might be stolen by large companies.  Interestingly, most of the software which Nokia uses in its mobile phones is written by Opera, a relatively small (120 employees) company which has actively supported the Eurolinux campaign against software patents.  Frain's department is one of the most active producers of software patents in Europe.  Here you find an overview of their applications at the European Patent Office.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/akteure/alcatel/index.en.html">Alcatel and Software Patents</a></b></dt>
<dd>In 2002, the french telecom giant published a paper which complains in harsh words about negative effects of patents such as legal insecurity and diversion of funds from R&amp;D to litigation.  On the other hand, Alcatel, as a global telecom player, owns a large software patent portfolio, and Alcatel's patent department is actively lobbying politicians to assure that Alcatel's patenting efforts will pay off in Europe in the same way as in the US.  Jonas Heitto, Alcatel patent lawyer trained at MPI in Munich, seems to be present at every related meeting in Brussels and some national capitals.  He is not the only one.  Alcatel participated in the European Commission's &quot;Consultation Exercise&quot; of 2000 in its own name as well as in the name of IT associations such as ANIEL from Spain.  Alcatel has recently been pursuing a strategy &quot;fabless company&quot;, i.e. outsource as much production as possible to partners in countries where production is cheap (e.g. China) while keeping a lean headquarter (financial and legal services and some R&amp;D) in France and/or the Bahamas/Bermudas, depending on where taxes are lower.  Patents are said to be helpful in implementing this strategy and in transferring money to tax havens.  In the rationale for her software patentability directive proposal, MEP Arlene McCarthy cites Alcatel as an example and stresses that patents are needed to protect European businesses at a time where production costs are high here.  Here we try to take a closer look at Alcatel's patent-related strategies and in particular their software patent applications at the European Patent Office.  Most of Alcatel's developments are in the area of software, and recently Alcatel is more than ever focussing on &quot;next generation Internet&quot; communication standards and applications.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/akteure/siemens/index.de.html">Siemens und Logikpatente</a></b></dt>
<dd>Siemens war insbesondere in Deutschland schon immer an der Spitze der Patentbewegung.  Dennoch behaupten die Vertreter der Siemens-Patentabteilung, die in Verb&auml;nden und regierungsnahen Organisationen f&uuml;r die Ausweitung der Patentierbarkeit eintreten, nicht ernsthaft, dass die Patentflut dem technischen Fortschritt diene oder f&uuml;r das Gesch&auml;ftsmodell von Siemens notwendig sei.  Sie betonen lediglich, dass Europa den von den USA geschaffenen globalen Regeln zu folgen habe.  Nicht wenige Verantwortungstr&auml;ger im technischen und gesch&auml;ftlichen Bereich von Siemens st&ouml;hnen derweil &uuml;ber die bremsende Wirkung, welche von der Patentflut ausgeht und die Branche ebenso wie die gesch&auml;ftlichen Aktivit&auml;ten von Siemens oft schmerzhaft beeintr&auml;chtigt.  Aber diese Leute sind nicht berechtigt, im Namen von Siemens &uuml;ber &quot;Patentfragen&quot; sprechen.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/akteure/ericsson/index.en.html">Ericsson and Software Patents</a></b></dt>
<dd>A report by the Swedish telecommunication giant Ericsson estimated in 2001 that &quot;Like other companies operating in the telecommunications industry, because our products comprise complex technology, we experience litigation regarding patent and other intellectual property rights.  Third parties have asserted, and in the future may assert, claims against us alleging that we infringe their intellectual property rights.  If we do not succeed in any such litigation, we could be required to expend significant resources to pay damages, develop non-infringing technology or to obtain licenses to the technology which is the subject of such litigation. However, we cannot be certain that any such licenses, if available at all, will be available to us on commercially reasonable terms.&quot;  Ericsson is currently (2003) facing software patent infringment charges in a US court where the plaintiff is demanding 1 billion USD in damages.  The patent activities of the swedish telecommunication giant Ericsson are cited in MEP Arlene McCarthy's software patentability directive proposal as a reason for legalising software patents in Europe.  Here we take a close look at the software patent applications of Ericsson at the European Patent Office.  Although Ericsson's patent portfolio is gigantic, we cannot find the &quot;1000 patents per year&quot; which McCarthy talks about, nor does it seem likely that Ericsson's R&amp;D is dependent on the availability of software patents.  The Ericsson paper itself writes:  &quot;We rely upon a combination of trade secrets, confidentiality policies, nondisclosure and other contractual arrangements, and patent, copyright and trademark laws to protect our intellectual property rights...&quot;.  In order to defent itself against litigation threats and earn itself the freedom to develop products, Ericsson it is patenting masses of ideas and concepts which any normal engineer or software developper would hardly consider as &quot;inventions&quot;.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.de.html"-->
</div>
<!--#include virtual="../../valid.de.html"-->
<div id="dokadr">
http://swpat.ffii.org/papiere/cec-telecom02/index.de.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/04/10
<a href="http://www.a2e.de">Hartmut PILCH</a><br>
deutsche Version 2005/03/28 von <a href="http://www.rudert-home.de">Andreas RUDERT</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el -->
<!-- mode: html -->
<!-- End: -->

