<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Ein im Auftrag der Europäischen Kommission geschriebener Forschungsbericht über den Zustand der Telekommunikationsunternehmen in Europa berichtet besort über diverse Tendenzen der Verstärkung des Patentwesens und insbesondere seine Ausdehnung auf Software, die zu unproduktivem Wettrüsten innerhalb der Telekom-Branche führen.
title: CEC/ETLA 2002: Technology policy in the telecommunication sector -- Market responses and economic impacts
ssa: Die Europäische Kommission, Generaldirektion Unternehmen, hat im Jahr 2002 eine Studie veröffentlicht, die sich auch stark mit Software-Patenten beschäftigt
vri: Überblick
rum: Ein im Auftrag der Europäischen Kommission geschriebener Forschungsbericht über den Zustand der Telekommunikationsunternehmen in Europa berichtet besort über diverse Tendenzen der Verstärkung des Patentwesens und insbesondere seine Ausdehnung auf Software, die zu unproduktivem Wettrüsten innerhalb der Telekom-Branche führen.
rum2: Ein im Auftrag der Europäischen Kommission geschriebener Forschungsbericht über den Zustand der Telekommunikationsunternehmen in Europa berichtet besorgt über diverse Tendenzen der Verstärkung des Patentwesens und insbesondere seine Ausdehnung auf Software, die zu unproduktivem Wettrüsten innerhalb der Telekom-Branche führen.
wAr: Die Studie wurde erstellt von einem Wissenschaftler des %(ETLA) und der London School of Economics und steht zum Herunterladen unter %(URL) bereit.
etw: Auf Seite 28/29 (Seite 31/32 der PDF-Datei) diskutiert die Studie Urheberrecht und Patente. Dabei stellt sie zuerst fest, dass Patente üblicherweise für technische Erfindungen (und die dahinter stehenden Ideen und Prinzipien) erteilt werden. Dann geht sie auf die Patentierbarkeit von Software und computer-implementierten Erfindungen ein. Anschließend kritisiert sie in diesem Zusammenhang die Ausweitung der Patentierbarkeit auf (reine) Ideen und Prinzipien in Bereichen, in denen früher nur das Copyright zum Schutz eines Werks zur Anwendung kam.
WlW: Damit arbeitet die Studie beinahe beiläufig einen Kernpunkt der Debatte heraus, den man wie folgt zusammenfassen könnte:
aor: Die Patent-Ansprüche vieler erteilter und beantragter Software-Patente beanspruchen reine Ideen und Prinzipien (z. B. Algorithmen, sehr allgemein gehaltenen Anforderungen an eine Software). Das Erfordernis des %(q:Technischen Beitrags) soll das theoretisch verhindern.
Why: Nicht unwesentliche Teilnehmer der Debatte haben festgestellt, dass eine Aufweichung des Technik-Begriffs stattgefunden habe, die durch den Richtlinienvorschlag eher noch verstärkt wird und Ihre Besorgnis darüber geäußert (z. B. ein %(ZVEI)-Arbeitskreis).
iie: Die Debatte krankt oft daran, dass die Diskussion des %(q:Technischen Beitrags) (teils in Kategorien abstrakter Begriffe, teils anhand konkreter Erfahrungen) wegen unterschiedlicher Interpretation oft aneinander vorbeiläuft.
Wix: Die Studie endet auf Seite 33/34 (Seite 36/37 der PDF-Datei) im Abschnitt %(c:Conclusions)
aiw: Highly tentative conclusions that can be made based on the literature    suggest that stronger patent rights may create substantial problems    in the telecommunication sector.  First, strong patent rights may    cause %(q:patent portfolio race). In other words, companies may use    patents primarily not to protect their technological invention    itself but as instruments with which to trade in order to be able    to negotiate access to external technologies. Given the observed    entry deterrence strategies of the incumbents, stronger patent rights    might provide with them new powerful weapons to defend monopolistic    market positions. Thus, stronger patent rights may hinder the    development of effective competition in the telecommunication    markets. Patentability of principles or ideas might further result in    strategic patenting against compatibility. This could be particularly    lethal to the content industry and further to the markets of the    future generations of cellular mobile telephones and services.    Currently available empirical evidence does not allow us to make    definite conclusions. However, it suggests that strengthening of    patent rights in the communication sector or extending patent    protection to cover intellectual property currently protected by    copyright involves great potential risks.
ter: Interessant ist in diesem Zusammenhang auch, dass die Studie das in der EU angestrebte Gemeinschaftspatent und die zugehörige Gerichtsbarkeit als Tendenzen in Richtung der kritisierten %(q:stronger patent rights) sieht (Seite 30, PDF-Datei Seite 33):
yWW: D. h. die Risiken, die bei einer zu weitgehenden Patentierung von Software auftreten, werden durch ein künftiges Gemeinschaftspatent nach Ansicht der Studie noch verstärkt. Basel II und Hochschulpatente könnte man ebenfalls noch als verstärkende Faktoren anführen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: cec-telecom02 ;
# txtlang: de ;
# End: ;

