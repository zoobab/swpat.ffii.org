<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kai Brandt 2003: Patent Protection in Europe in Danger

#descr: In an internal journal of Siemens, Dr. Kai Brandt, an independent patent attorney residing in Munich and member of the Siemens patent department, writes that the European Parliament voted to ban patenting of all innovative industrial processes that make use of software, that there is no R&D without patents, that the European Commission's original proposal was well balanced, that the EP voted for amendments because it was misled to believe that patents and opensource software are incompatible, and that Siemens boss Heinrich von Pierer has teamed up with other CEOs and associations to prevent this disaster, which is only in the interest of a few software distributors and against the interests of all innovative SMEs.  Brandt fails to give his audience any usable pointers that could allow them to inform themselves about the other side's arguments.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: siemens-brandt03 ;
# txtlang: de ;
# multlin: t ;
# End: ;

