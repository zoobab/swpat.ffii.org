<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kai Brandt 2003: Patent Protection in Europe in Danger

#descr: In an internal journal of Siemens, Dr. Kai Brandt, an independent
patent attorney residing in Munich and member of the Siemens patent
department, writes that the European Parliament voted to ban patenting
of all innovative industrial processes that make use of software, that
there is no R&D without patents, that the European Commission's
original proposal was well balanced, that the EP voted for amendments
because it was misled to believe that patents and opensource software
are incompatible, and that Siemens boss Heinrich von Pierer has teamed
up with other CEOs and associations to prevent this disaster, which is
only in the interest of a few software distributors and against the
interests of all innovative SMEs.  Brandt fails to give his audience
any usable pointers that could allow them to inform themselves about
the other side's arguments.

#yae: Due to copyright restraints, we can not publish the whole article
here.

#aob: Here are some quotes and rebuttals:

#wtr: New technologies depend on innovative software developments, whether
these be, for example, in the information, automobile, medical or
industrial domains.  An appropriate patent protection is essential. 
But if the new directive of the European Parliament becomes law,
exactly this is endangered.

#tnW: On Sepetember 24th, 2003, the European Parliament approved on its
first reading the Directive on the Patentability of
Computer-Implemented Inventions (software patents).  If this directive
were to take effect across the EU in this form, it would have
catastrophic implications for both industry and the entire R&D
community in Europe.   It would essentially make it virtually
impossible to obtain such a patent or to enforce patents of this type
already granted.  The term %(q:computer-implemented invention) in this
context is conceivably open to broad interpretation: it covers all
inventions, the performance of which involves the use of a computer or
other programmable apparatus.

#tlm: The European Parliament's version does not make it impossible or
difficult to obtain patents on %(q:computer-implemented inventions). 
Rather, it defines the term %(q:computer-implemented invention) more
narrowly, so that %(q:new technologies ... in the .. automobile,
medical or industrial domains) are patentable, independently of
whether they %(q:depend on innovative software developments). 
According to the EP-amended directive, the work of the process
engineer is patentable, but that of the programmer is not.

#eol: The result would be that more than 90% of all patents relating to
information technology, telecommunications and consumer technologies
would be invalidated without compensation.  Many other areas would
also be affected since there are very few inventions nowadays that can
be executed without the use of computers.  Some examples of
technologies that would no longer be patentable include:

#ert: software-assisted image capture, processing and representation -- an
essential component of medical diagnosis

#WWd: data compression and data conversion techniques, such as MP3, which
was awarded the German Prize for Technology and Innovation in the year
2000

#scr: electronic control systems for injection processes in motor vehicles
as well as brake control, stability control and navigation systems

#asw: process automation or building management systems and power managment
in power supply networks.

#tso: The European Parliament's directive indeed forbids patents on data
processing rules, precisely because such rules can be applied to all
engineering domains.   But again in all engineering domains solutions
that directly involve forces of nature (the work of the process
engineer) remain patentable, and whether or not a computer is used is
irrelevant to patentability.

#det: In the future, R&D results in all these fields in Europe could be
copied and imitated to a virtually unlimited extent.

#nre: Untrue.

#hfc: Software is covered by copyright.  Even when the source code is
available, the imitator must spend nearly the same amount of man hours
of skillful work as the first mover.  In addition normally source code
is neither available nor can any useful source code be obtained by
decompilation.

#rao: Above all, this would affect European companies, who would no longer
have any efficient protection mechanisms in their domestic markets,
and who would be mercilessly exposed to product dumping from low-wage
countries.

#the: As explained above, efficient protection is available without patents.
 With software patents in place, European companies would face no less
competition from low-wage countries.  Siemens has been doing well at
moving its R&D facilities to low-wage countries and %(q:dumping)
low-cost goods from those countries to Europe.  Nothing helps against
competition from a well-educated low-wage workforce.

#rWi: And that's not all: in the medium term, there would also be serious
implications for the European R&D community.  If it is no longer
worthwhile to run research and developmentin Europe because the
results can no longer be protected, then this is bound to lead to
offshoring to locations outside Europe.

#nre2: Untrue.

#efi: Patents are used at the location of sale, not at the location of
research.

#rtl: Currently Siemens, SAP and many others are offshoring to locations
such as India.   One might assume that they are doing this because
India has a tradition of skepticism toward the patent system and
explicitely banning software patents.  But it is more likely that the
availability or not of patents at the site of R&D plays no role
whatsoever.

#noo: How did a directive of this kind ever come into being?  The original
aim was to harmonize patent law for software inventions within Europe
and to put a sto to a trend that originated in the USA of patenting
mere business methods and non-technical software.  After an intensive
exploratory phase, the European Commission developped a well-balanced
draft directive, which largely served to secure the status quo in
Europe.  It was essentially a compromise, on the one hand helping open
source business models to flourish, while, on the other hand, also
guaranteeing efficient protection of R&D results.

#ctf: %(q:Balancing between R&D and open-source business models) was not an
issue in the original draft, and there is nothing in the draft that is
related to open-source business models, nor has this been a major
subject in the %(q:intensive exploratory phase) or at any time later.

#hWo: Unfortunatley, certain interest groups then pushed this compromise
towards a format that would allow R&D results to be used for free. 
This was combined with an attempt to stok up fears, e.g. by suggesting
that the open source concept and the patent system are mutually
incompatible.

#utw: Instead of naming the %(q:interest groups) and pointing to their
publications, Brandt makes up interest groups which don't exist or
were never heard of during the debate.  Clearly he finds it easier to
refute the views of fictitious opponents than those of real ones.

#uem: More counter-arguments against fictitious arguments of the software
patent critics.  E.g. %(q:opensource and the patent system have
peacefully coexisted for a long time), %(q:patents favor SMEs more
than large companies), %(q:trivial patents are no problem in Europe
because they can always be invalidated in court).

#tae: In all probability, the remaining stages of the European Union's
legislative process will stretch into the autumn of 2004 In its
current form, the directive one-sidedly reflects the interest of a few
large software distributors, while ignoring the interests of
universities, R&D institutions, innovative software developpers, small
and medium-sized companies, and many of Europe's core industries.  In
the meantime, companies, organisations and associations are jointly
attempting to prevent Europe sliding back to the level of a
developping country in terms of its protection of intellectual
property, while, ironically enough, other countries, such as the USA
and China, are taking steps to provide improved safeguards for R&D
results.  Heinrich von Pierer has collaborated with the CEOs of
Philipps, Alcatel, Ericsson and Nokia to submit a petition to the
European Union, while countless associations ... have also swollen the
ranks of protesters against this draft directive.  Thus, the hope
still exists that the directive may not take effect in its current
form.

#eWt: We do not know to which initiatives in %(q:the USA and China) Brandt
may be referring, but certainly not to the %(ft:Federal Trade
Commission's recent hearings and summary report on competition
problems engendered by software patents).

#sdu: The message is to continue R&D activities and continue to deliver
reports of inventions, but it is important to also keep a careful and
critical eye on this subject.  Research and development are the only
ways of ensuring that our company retains its technological edge in
the future.

#trW: With these final words Brandt implies that software patents and
%(q:research and development) are synonyms and that R&D must be kept
up for the time being, because not all hope is lost yet.  However
Brandt stops short of revealing to his readers that these exhortations
come from their patent lawyer.  Indeed for the patent department at
Siemens there is a real danger that the Parliament's directive could
deal their organisation a severe blow and stop the continuous flow of
%(q:reports of inventions) from coming into their department.  The
fact that Brandt, contrary to normal practise of the magazine, does
not disclose his membership in the patent department but rather poses
as an R&D person, illustrates the tension that exists between the
patent department and other departments at Siemens, which often
perceive the patent department as a brake on their activities.

#krp: The message is to continue to keep an eye on the activities of the
Siemens patent department.  When it comes to their organisational
survival, they are not picky about the means.  They are writing FUD
articles in internal journals at Siemens to mislead the employees of
their own company, and they have probably done the same to their boss.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: siemens-brandt03 ;
# txtlang: en ;
# multlin: t ;
# End: ;

