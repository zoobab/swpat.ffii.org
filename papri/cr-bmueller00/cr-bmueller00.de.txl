<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Pldne f|r eine Softwarepatent-Richtlinie

#descr: Der Autor ist federf|hrend beim Entwurf der neuen Richtlinie tdtig. 
Er meint, Europa solle sich in Richtung auf amerikanische Verhdltnisse
bewegen.  Als Begr|ndung f|hrt er die Meinung von 44 zu Fragen der
Rechtssystematik befragten Patentjuristen an.

#Ato: Author

#Agr: Anmerkung des Herausgebers

#DsW: Der Beitrag gibt seine persönliche Auffassung wieder.

#Dnw: Die Rechtslage zur Patentierbarkeit von Computerprogrammen in Europa
ist schwer nachvollziehbar und uneinheitlich.

#AiU: Ausgehend von der Notwendigkeit einer europäischen Gesetzesinitiative
vergleicht der Beitrag die Rechtslage in Europa und den USA und zeigt
die künftigen Ansatzpunkte der EG-Richtlinie auf.

#NWe: Notwendigkeit einer EU-Gesetzesinitiative

#Dho: Derzeitige Rechtslage in Europa und den USA

#Afi: Ansätze für EG-Richtlinie

#DeW: Die Dienststellen der Europäischen Kommission bereiten derzeit den
Vorschlag einer Richtlinie über die Patentierbarkeit von
Computerprogrammen vor.

#Grr: Gesetzgeberisches Handeln in diesem Bereich war in der mit dem
%(gb:Grünbuch über das Gemeinschaftspatent und das Patentschutzsystem
in Europa) eingeleiteten Konsultation %(fm:als vorrangig eingestuft
worden).

#Nnf: Nach der derzeitigen Regelung ist ein Programm %(q:als solches) nicht
patentfähig, ein Patent kann jedoch für eine technische Erfindung, die
sich auf ein Computerprogramm stützt, erteilt werden.

#DMa: Diese Regelung weist einen Mangel an Transparenz und damit an
Rechtssicherheit auf.

#AcW: Außerdem sind Entscheidungspraxis nationaler Gerichte und des
Europäischen Patentamtes nicht immer einheitlich, was dem Binnenmarkt
abträglich ist.

#EWe: Eine auf §95 EG-Vertrag gestützte Richtlinie zur Harmonisierung der
nationalen Patentgesetze in diesem Bereich soll Abhilfe schaffen.

#Dmu: Das Europäische Parlament sprach sich für die Patentfähigkeit von
Computerprogrammen aus, welche die an eine technische Erfindung
gestellten Anforderungen an Neuheit und Anwendbarkeit erfüllen, so wie
dies in den USA und Japan der Fall ist.

#LKr: Lange Ausführungen über gerichtliche Prozeduren in den USA.

#Fre: Für den Vorschlag der EG-Richtlinie der Kommission stellt sich die
grundsätzliche Frage, ob die harmonisierung auf der Grundlage des
status quo, so wie ihn die Rechtsprechung in Europa definiert,
erfolgen oder ob sie weiter und dabei insbesondere in Richtung der
US-Rechtsprechung, gehen sollte.

#Btw: Bei der Klärung dieser Frage sollten insbesondere die Auswirkungen der
Richtlinie auf Innovation und Wettbewerb (namentlich für KMU), die
Rolle und Interessen unabhängiger Software-Entwickler sowie die
Auswirkungen auf den elektronischen Geschäftsverkehr berücksichtigt
werden.

#Dii: Die Komission hat den beteiligten Kreisen Gelegenheit gegeben, sich zu
dem konkreten Inhalt der Richtlinie durch Beantwortung eines
Fragebogens zu äußern.

#des: der Artikel enthält keinen weiteren Hinweis zu diesem Fragebogen

#Dut: Dabei wurde im Zusammenhang mit der Frage der Patentierbarkeit von
Computerprogrammen eine Definition des Erfindungsbegriffs im Sinne von
%(q:technische Lösung) nur von einer Minderheit der Antwortenden
befürwortet.

#Taw: Teilweise wurde vorgeschlagen, statt dessen auf einen Beitrag zum
Stand der Technik abzustellen oder sich auf den Nachweis einer
technischen Wirkung der Erfindung zu beschränken.

#Ema: Einer anderen Stellungnahme zufolge sollte der technische Charakter
eines Computerprogramms allgemein anerkannt und die gewerbliche
Anwendbarkeit weit ausgelegt werden, so dass das Kriterium eines
nützlichen praktischen Ergebnisses maßgebliche Bedeutung erlangen
würde.

#EpW: Eine weitere Frage ging dahin, ob ein Unterschied zwischen
verschiedenen Kategorien von Computerprogrammen gemacht werden sollte,
um bestimmte von ihnen von der Patentirbarkeit auszuschließen,
insbesondere Computerprogramme für Spiele oder für Tätigkeiten im
Geschäfts- oder Finanzbereich.

#Due: Diese Frage wurde überwiegend verneint.

#Iez: In einzelnen Beiträgen wurde gefordert, jedes Computerprogramm,
welches die allgemeinen Bedingungen der Patentierbarkeit erfüllt, als
patentfähig anzusehen, ohne zwischen verschiedenen Arten von
Programmen zu unterscheiden.

#Iiw: In einer anderen Stellungnahme hieß es, die technische Natur einer
Erfindung hänge nicht von dem Zweck des Computerprogramms ab, das
diese beinhalte.

#TaE: Teilweise wurde aber auch in diesem Zusammenhang ausdrücklich ein
Beitrag der Erfindung zum Stand der Technik verlangt.

#Dln: Die Anregungen der beteiligten Kreise sollten bei der Fertigstellung
des Richtlinienvorschlags in Betracht gezogen werden.

#Dez: Der Vorschlag sollte auf den allgemeinen Grundsätzen des Patentrechts
beruhen, so wie diese sich historisch entwickelt haben, und dabei
gleichzeitig den Notwendigkeiten der Informationsgesellschaft Rechnung
tragen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cr-bmueller00 ;
# txtlang: de ;
# multlin: t ;
# End: ;

