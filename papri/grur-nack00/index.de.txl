<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Nack 2000 zu BGH

#descr: Der BGH erkldrte im Fr|hjahr 2000 in einer spektakuldren Kehrtwendung,
der technische Charakter einer %(q:programmtechnischen Vorrichtung)
sei unabhdngig davon zu betrachten, ob diese Vorrichtung den Stand der
Technik bereichert.  Damit schien der Weg f|r die Patentierbarkeit
aller Geschdftsmethoden ervffnet zu sein.  Nack fragt, ob dies nun
tatsdchlich der Fall ist.  Er f|hrt in einige Fragestellungen ein,
zeigt Ungereimtheiten der Rechtsprechung sowohl des BGH als auch des
EPA auf (%(q:Dogmatisch gesehen ist das Chaos jedoch kaum zu
|berbieten)), weist auf die Mvglichkeit weiterer |berraschender
Kehrtwendungen der Rechtsprechung hin, ldsst die gesetzlichen
Patentierbarkeitsausschl|sse als nichtssagend erscheinen, und fordert
zum Schluss, man solle das Thema nicht nur auf rechtsdogmatischer
sondern auch auf politischer Ebene angehen.  Wie 1877 gehe es heute
darum, das Patentwesen auf neue Wirtschaftszweige (Handel, Banken,
Dienstleistungen) auszudehnen.  %(q:Damals war man mutig und im
Ergebnis |beraus erfolgreich; was spricht dagegen, heute wiederum
mutig zu sein?)

#Min: Max-Planck-Institut für Ausländisches und Internationales Patent,
Urheber- und Wettbewerbsrecht

#Rwi: Ralph Nack ist ein junger wissenschaftlicher Mitarbeiter von Prof.
Straus am %{MPI} in München, der durch zahlreiche Artikel und
öffentliche Auftritte sein starkes Engagement für die Ausweitung der
Patentierbarkeit unter Beweis gestellt hat.

#Age: Auch in diesem Artikel macht Nack zunächst kräftig Stimmung für
Softwarepatente und versucht den Eindruck zu erwecken, die
Gesetzesregel biete %(q:keine Hilfestellung), m.a.W. bedeute nichts
und werde daher von EPA und BGH auch nicht verletzt.  Einige
Kostproben:

#WeW2: .. Art 52 EPÜ .. Dort stehen vor allem die %(q:Klassiker) der nicht
patentfähigen Gegenstände, wie Geschäftsmethoden, Regeln für
Gedankliche Tätigkeiten, aber auch Computerprogramme.  Nimmt man den
Wortlaut der Regelung ernst, lautet ihr Auftrag, den Gegenstand
daraufhin zu untersuchen, ob er vom %(e:äußeren Erscheinungsbild),
also vom %(q:Phänotyp) her gesehen keine Geschäftsmethode, Regel für
gedankliche Tätigkeit usw. ist.  GEnau eine solche Prüfung führt
jedoch bei Computerprogrammen komplett in die Irre, da dort -- je nach
Fassung des Patentanspruchs -- %(q:formal) eine Maschine oder ein
Verfahren Gegenstand der Patentanmeldung ist.

#Dlr: Eben.  U.a. aus diesem Grunde lautet der Auftrag auch nicht, den
Phänotyp zu untersuchen, sondern die offenbarte Lehre daraufhin zu
prüfen, ob es sich um eine technische Erfindung handelt.  Diese Lehre
ist vom Phänotyp (sprachliche Einkleidung) unabhängig.  Eine
%(q:Programm für Datenverarbeitungsanlagen) ist das, was der BGH etwa
in Entscheidungen wie %(dp:Dispositionsprogramm) und
%(wt:Walzstabteilung) auch als %(q:Organisations- und Rechenregel)
oder %(q:Rechenprogramm für Datenverarbeitungsanlagen) bezeichnet,
also eine abstrakte Anweisung, ein Algorithmus in beliebiger
Entwurfsstufe oder Erscheinungsform.  Dies entspricht dem Verständnis
des Informatikers ebenso wie der patentrechtlichen Literatur zumindest
der damaligen Zeit, wie auch die %(ep:Prüfungsrichtlinien des EPA von
1978) belegen.

#ZWu: Zu Beginn der Computer-Rechtsprechung in der 70er Jahren entwickelte
der Bundesgerichtshof die %(q:Kerntheorie).

#Dls: Dieser Satz enthält zwei tendenziöse Ungenauigkeiten:

#DlW: Die Software-Rechtsprechung begann in den 50er Jahren.  Zunächst
wurden Softwarepatente häufig gewährt. Nach breiter Diskussion setzte
sich 1973/76 eine ablehnende Haltung durch.  Nack möchte aber, wie man
im folgenden erkennt, diese Haltung gerne auf
Anfänger-Missverständnisse zurückführen.

#DWn: Der Begriff %(q:Kerntheorie) stammt nicht vom BGH sondern von den
zahlreichen Patentjuristen, deren Trommelfeuer in Zeitschriften wir
GRUR, Mitteilungen der Deutschen Patentanwälte etc den BGH schließlich
verunsicherte.  Mit der Bezeichnung %(q:Kerntheorie) wird eine
naheliegende Betrachtungsweise als abgehobene %(q:Theorie)
dargestellt.  Eine passendere Bezeichung ist
%(q:Differenzbetrachtung).  Sie steht im Gegensatz zu der von den
meisten Patentjuristen bevorzugten (und daher auch so benannten)
%(q:Ganzheitsbetrachtung).

#Nuz: Nack erläutert die ungeliebte Differenzbetrachtung korrekt:

#NsL: Nach dieser Lehre kommt es darauf an, %(q:in welchen Anweisungen der
als neu und erfinderisch beanspruchte Kern der Lehre zu sehen ist). 
Dieser %(q:Kern) muss %(q:technisch) sein.  ... Nach diesem
Prüfungsschema muss also zunächst ermittelt werden, worin die
%(e:Leistung) des Erfinders liegt.  Als zweites muss dann der
%(e:Charakter) der Leistung beurteilt werden, sie muss %(q:technisch)
sein.

#DoW: D.h. es wird nicht der %(q:Anspruch in seiner Ganzheit) sondern nur
die Differenz zum Stand der Technik bewertet.   Das, was neu ist, muss
auch technisch sein und umgekehrt.

#Dip: Dispositionsprogramm

#Wti: Walzstabteilung

#SWs: Soweit gut.  Im folgenden offenbart Nack eine erstaunliche
Begriffsstutzigkeit gegenüber der BGH-Doktrin von der technischen
Erfindung, wie er etwa in %{LST} zum Ausdruck kommt.  Statt diese
Doktrin erst einmal richtig darzustellen, beurteilt er sie sofort
aufgrund seiner eigenen Lieblingskategorien, nämlich der
Unterschiedung nach %(q:herkömmlichen Maschinen) und traditionell
nicht patentierbaren Gegenständen.  Dabei vergisst er auch schon, dass
es nicht auf Anspruchsgegenstände sondern auf
%(e:Erfindugnsleistungen) ankommt.

#Hea: Hier begann jedoch stets ein argumentativer Dschungel: Bei
Computerprogrammen besteht nämlich die %(e:Leistung) naturgemäß in der
Erstellung einer Folge von Arbeitsanweisungen (einem Algorithmus im
weiteren Sinne); darin unterscheidet sihc zwar ein Computerprogramm
nicht wesentlich von einem herkömmlichen Verfahren oder einer
herkömmlichen Maschine, da jeder Vorrichtung und jedem Verfahren eine
Algorithmus zugrunde liegt; allerdings wird bei Computerprogrammen
dieser Algorithmus nicht in Form einer Interaktion mechanischer oder
elektronischer Bauteile, also in %(q:maschinisierter) Form formuliert,
sondern in Form von Arbeitsanweisungen an einen Computer.  In
Anbetracht dieser fehlenden %(q:Maschinisierung) fiel es nun
%(e:intuitiv besonders schwer), den %(q:technischen Charakter) der
Leistung zu bejahen, da insofern eine gewisse Verwandtschaft zwischen
Computerprogrammen und den sog. nicht patentfähigen %(q:Anweisungen an
den menschlichen Geist) besteht.

#Eri: Einer Maschine, die sich nur durch einen neuen Algorithmus von
bekannten Maschinen unterschied, wurde auch schon vor der Zeit der
Computerprogramme regelmäßig die Patentierbarkeit abgesprochen.  Ein
Algorithmus wird insbesondere laut %(e:Walzstabteilung) nicht dadurch
zu etwas patentfähigem, dass er in %(q:maschinisierter) Form
beansprucht wird.  Nack missversteht wohl auch den oft und gerne
missverstandenen Ausdruck %(q:Anweisung an den menschlichen Geist). 
Hierbei geht es um rechnerische Problemlösungen, also solche, deren
Gültigkeit durch logischen Beweis, unabhängig von einer etwaigen
physikalischen Umsetzung, nachzuweisen ist.  Zwischen solchen
Problemlösungen und reinen Denkregeln besteht nicht nur eine
%(q:gewisse Verwandschaft), sondern es handelt sich um die gleiche Art
von Leistung.  Und um Leistungsbeurteilung sollte es bei der
Patentprüfung gehen.  Das Patent ist der Lohn für eine Leistung, nicht
für einen Anspruch auf eine %(q:maschinisierte) Einkleidung dieser
Leistung.  Offenbar fällt es Nack %(e:intuitiv besonders schwer),
zwischen logischer Funktionalität und physischer Kausalität zu
unterscheiden.  Dem BGH fiel dies in den 70er und 80er Jahren nicht
schwer.

#Iof: Im Ergebnis scheiterten daher in dieser Zeit praktisch %(e:alle)
Computerprogramme am Bundesgerichtshof, insbesondere auch solche, die
eine geradezu %(q:lineare) Fortführung einer %(q:technischen
Wissenstradition) darstellten.

#NeW: Nack zeigt eigentlich deutlich, dass der Aspekt der
%(q:Wissenstradition) eigentlich vor allem ihn selbst aber nicht
unbedingt den BGH interessierte.

#Aee: Ab 1980 setzte sich beim Bundesgerichtshof die Erkenntnis durch, dass
man computerbezogene Erfindungen nicht pauschal vom Patentschutz
ausnehmen kann.

#Ntc: Nack führt mit diesem Satz seine Leser in mehrfacher Hinsicht in die
Irre:

#Eew: Es hat noch nie jemand behauptet, dass man dies könnte.  In der Tat
wird man z.B. in der Blaulicht-Diode, die für DVD-Laufwerke eine große
Rolle spielt, eine technische Erfindung sehen.

#Nee: Nack spielt offenbar auf die schillernde BGH-Entscheidung
%(ab:Antiblockiersystem) an.  Diese bewegte sich jedoch noch immer im
Rahmen der bisherigen Doktrin und wurde zudem durch weitere Beschlüsse
wie %(fm:Flugkostenminimierung) zurechtgerückt.  Von einer
%(q:Erkenntnis) oder Entwicklung kann keine Rede sein.  %(bk:Benkard
1988) und %(bs:BGH Betriebssystem 1990) sowie zahlreiche
unangefochtene BPatG-Urteile der späten 80er Jahre wenden noch immer
die alles andere als %(q:pauschalen) Kriterien (s. %(gk:Kolle)) an und
verhinderten somit in Deutschland die Patentierung von Programmen. 
Ein nennenswerter Einbruch kam erst 1991.

#Mnh: Man beachte Wertungen wie %(q:Erkenntnis) und anachronistisch
verwendeten neuesten apologetischen Jargon wie %(q:computerbezogene
Erfindungen).

#Wer: Wesentlich interessanter als obige tendenziöse Aussagen sind Nacks
Analysen der neuen BGH-Entscheidung %(q:Sprachanalyse) und des darin
zur Aufweichung verwendeten Begriffes %(q:technische Überlegungen). 
Dabei stellt er u.a. fest:

#Dmr: Das Erfordernis %(q:technischer Überlegungen) ist also dahingehend zu
verstehen, dass gerade %(e:der erfinderische Schritt) nur dadurch
möglich war, dass der Erfinder ein ingenieur- oder
naturwissenschaftliches %(e:Vorwissen) hatte.  Damit wären zumindest
solche Computerprogramme %(e:nicht patentfähig), bei denen die
erfinderische Leistung z.B. auf dem Gebiet der Finanz- und
Versicherungswirtschaft liegt.

#Dec: Diese Erklärung dürfte zwar in etwa dem Denken der BGH-Richter
entsprechen, aber in der Praxis ist Skepsis angebracht.  Man sollte
korrigieren:  %(q:Damit würden zumindest solche Patentansprüche auf
Schwierigkeiten stoßen, bei denen der Bezug zur Finanz- und
Versicherungswirtschaft unnötig in den Vordergrund gestellt wird). 
Weder bei Amazons %(q:One-Click)-Verfahren noch bei der
betriebswirtschaftlich höchst bedeutenden Karmarkar-Methode der
linearen Programmierung dürften an dem weichen Erfordernis der
%(q:technischen Überlegungen) anecken.  Andererseits geht Nack dieses
Erfordernis schon wieder zu weit:

#Aez: Allerdings werden durch dieses Prüfungsschema auch Erfindungen wie die
folgende von der Patentfähigkeit %(e:ausgeschlossen): Ein
elektronisches Signalfilter wird durch einen Computer ersetzt, der das
digitalisierte Signal mittels eines speziellen Algorithmus
%(q:filtert).  Hier ist praktisch %(e:keine) Kenntnis der
herkömmlichen Technologie mehr notwendig; die Leistung liegt allein in
der Entwicklung eines zweckentsprechenden mathematischen Algorithmus. 
Denoch liegt diese Erfindung innerhalb einer Wissenstradition, die
unstreitig dem Patentschutz zugänglich ist.  Die %(q:mechanische)
Anwendung des Erfordernisses %(q:technischer Überlegungen) könnte hier
zur Verneinung der Patentfähigkeit führen, ein unsinniges Ergebnis. 
Auf der anderen Seite würde die Bejahrung %(q:technischer
Überlegungen) etwas gekünstelt wirken.

#Ost: Offenbar geht es weniger dem BGH als Nack um die Wahrung der
Besitzstände des Patentwesens an bestimmten %(q:Wissenstraditionen). 
Der BGH hat sogar in einigen Entscheidungen dargelegt, dass es
unerheblich ist, ob eine heute durch Rechenregeln gelöste Aufgabe
früher mit technischen Mitteln gelöst wurde.  Auf die Leistung kommt
es an.

#Dso: Die Rechtsprechung des EPA kommt vom Ergebnis her Nacks Wünschen
besser entgegen, erreicht dieses Ergebnis aber nur um den Preis
rechtsdogmatische Verrenkungen, die Nack bemerkenswert klarsichtig
aufzeigt:

#Dsd: Dieses Konzept meidet die Nachteile, die oben in bezug auf das
Erfordernis %(q:technischer Überlegungen) beschrieben worden sind: 
Der oben beispielhaft genannte digitale Signalfilter wäre nämlich
hiernach patentfähig, da ihm eine %(q:technische Aufgabenstellung)
zugrunde liegt.  Allerdings ist die systematische Einordnung bei der
Erfindungshöhe völlig verfehlt: Die %(e:inhaltliche) Prüfung des
Gegenstands ist Aufgabe des %(e:Erfindungsbegriffs); die Neuheit und
Erfindungshöhe dienen dagegen der %(e:qualitativen) Prüfung.  Ein
Bruch dieses Konzeptes würde einen Rückfall in das Jahr 1877 bedeuten,
als man noch nicht zwischen den %(e:inahltlichen) und den
%(e:qualitativen) Anforderungen an eine Erfindung unterschied.

#Ahh: Als %(q:Zwischenergebnis) hält Nack fest:

#HWt: Hier findet sich der Grundgedanke wieder, den Nack auch im MPI-Beitrag
zu einer %(bm:vom BMWi in auftrag gegebenen Studie) ausarbeitet. 
Dagegen ist u.a. folgendes einzuwenden:

#Glu: Gerade vom Ergebnis der Patenterteilung her betrachtet ist der
Rechtsprechung seit ca 1986 keine Begrenzung mehr gelungen.   Patente
auf Geschäftsverfahren in weitestem Sinne (aufgabenhafte
Funktionslogik) sind die Regel.  Auch im engsten Sinne gibt es sehr
viele Geschäftsverfahrenspatente.  Vor 1986 gab es einen klaren
Begriff der technischen Erfindung, der nichts mit Nacks
%(q:Traditionen) zu tun hatte.  Die Einteilung nach Traditionen ist
Nacks normative Idee.  Vielleicht liegt diese Idee in der Luft und
wirkt somit auch auf das Denken mancher Richter ein.  Zur Beschreibung
der Tatsachen wird diese Dichotomie jedoch erst dann einigermaßen
brauchbar, wenn man großzügig die Rechtsprechung mehrerer Jahrzehnte
durcheinanderrührt und dann anhand der Urteilswortlaute fragt, welche
Anträge %(q:tendenziell) von Gerichten gebilligt und welche
zurückgewiesen wurden.

#DnW: Der Begriff %(q:Computerprogramme) wird hier mehrdeutig verwendet. 
Gemeint kann einerseits ein technischer (Naturkräfte anwendender)
Vorgang sein, der durch ein Computerprogramm gesteuert wird.  In
diesem Sinne war etwa in der %(ab:Antiblockiersystem-Entscheidung) von
einem %(q:technischen Programm) die Rede, wobei die Patentansprüche
sich jedoch nur auf die Naturkräfteanwendung und nicht auf das
Programm beziehen.  Noch 1990 bestätigte die
%(bs:Betriebssystem)-Entscheidung diese Sicht, wonach Patente nur
physische Kausalitäten betreffen können, die jenseits des
Programmierens liegen und daher ein Betriebssystem gerade kein
%(q:technisches Programm) ist.  Seit 1991 gibt es BGH-Entscheidungen,
welche den Begriff der technischen Lehre aufweichen und somit die
Aufrechterhaltung der Trennung zwischen technischen Prozessen
(irreführend %(q:technische Programme) genannt) und Programmen als
solchen zunehmend künstlich erscheinen lassen.  Es gibt jedoch noch
keine BGH-Entscheidung, welche ein Programm als solches patentierbar
gemacht hätte.  Das BPatG hat dieser Möglichkeit im August 2000
vorerst einen Riegel vorgeschoben.

#Ehq: Es folgt eine recht scharfsichtige Analyse zahlreicher Unklarheiten in
der Entscheidung %(q:Sprachanalyse), an deren Ende resümiert wird:

#Ift: Insgesamt bleibt daher etwas unklar, welche Auffassung der
Bundesgerichtshof %(e:gegenwärtig) vertritt; die %(e:bisherige)
Rechtsprechung %(e:verbietet) jedenfalls eine %(q:gespaltene) Prüfung
der Erfindungshöhe; hinzu kommt, dass eine solche Prüfung systematisch
völlig verfehlt wäre, da die Erfindungshöhe nicht der Platz für eine
%(e:inhaltliche), sondern für eine %(e:qualitative) Prüfung der
Erfindung ist (s.o.).

#Grn: Geht man nun davon aus, dass der Bundesgerichtshof an seiner
bisherigen -- systematisch schlüssigen -- Rechtsprechung festhält und
keine %(q:gespaltene) Prüfung der Erfindungshöhe vornimmt, gelangt man
zwangsläufig zu dem Ergebnis, dass von jetzt an %(e:keine
substantiellen Schranken mehr für die Patentfähigkeit von
Softwareerfindungen) bestehen: Solange das Programm als
%(q:programmierte Datenverarbeitungsanlage) beansprucht wird, sind die
formalen Anforderungen erfüllt; der Schutzbereich eines solchen
Patents erfährt gegenüber einem %(q:echten) Anspruch auf ein
Computerprogrammprodukt auch keine wesentliche Einschränkung, da der
gewerbliche Vertrieb der Software regelmäßig als mittelbare
Patentverletzung miterfasst sein wird.  Darüber hinaus lässt sich eine
Begrenzung dieser Rechtsprechung auf %(q:Vorrichtungsansprüche) kaum
aufrechterhalten, da die Wahl der Anspruchskategorie bei gleichem
Anspruchsinhalt an sich keine Auswirkungen auf die Patentfähigkeit
haben darf, was auch der US-amerikanische CAFC in %(e:AT&T Corp v.
Excel Communications, Inc.) zutreffend hervorgehoben hat.

#ZLe: Zum Schluss regt Nack die Leser dazu an, über die Konsequenzen auch
auf wirtschaftspolitischer Ebene nachzudenken:

#Icn: Im letzten Satz wurde die Wortgruppe %(q:und die Öffentlichkeit)
offenbar nachträglich eingeschoben.  Kurz vor der Veröffentlichung
dieses Artikels zeichnete sich tatsächlich eine Politisierung des
Themas ab.  Auch Nack war auf einer %(bw:Konferenz im BMWi) zugegen
und warf dort den zahlreich versammelten Logikpatentkritikern vor, sie
stellten erneut eine Frage zur Debatte, die eigentlich 1877 schon ein
paar Häuser weiter im Berliner Reichstag entschieden worden sei.  Bis
zum Frühjahr 2000 sah es so aus, als wäre der Rest tatsächlich Sache
des Bundesgerichtshofes, dessen Rechtsdogmatik letztlich wohl nur
deshalb chaotische Bahnen beschreiten konnte, weil diese durch
wirtschaftspolitischen Konsens der maßgeblichen Patentjuristenkreise
abgesichert zu sein schien.

#Nne: Naheliegende Einwände gegen die nun endlich ausgesprochenen und zur
allgemeinen Diskussion gestellten Ziele sind:

#DeW: Das Patentwesen war keineswegs %(ue:überaus erfolgreich), man lese ein
wenig %(el:volkswirtschaftliche Literatur).

#Wue: Wesentlich für die Akzeptanz des Patentwesens war die Beschränkung auf
%(te:technische Erfindungen), welche Nack nicht verstehen will (und
daher vielleicht auch tatsächlich nicht versteht).

#WeW: Wenn technische Erfindungen heute nicht mehr so im Mittelpunkt der
Wirtschaftsentwicklung stehen und das Patentwesen folglich eine
weniger zentrale Rolle spielt als früher, so mag das Patentrechtlern
Sorgen machen oder gar in frevelhafter Weise dem Selbstverständnis der
%(pm:Patentbewegung) widersprechen, wie er in zahllosen Festreden
verkündet wird.  Für andere besteht aber kein Grund zur Sorge.  Und
auch Patentrechtler dürften sich nach einem ersten Schock schnell
trösten:  echte technische Erfindungen der 90er Jahre etwa im Bereich
der blauen Leuchtdiode, des Wasserstoffmotors oder besonders starker
Magneten usw sind heute alles andere als unbedeutend, und die völlige
Mathematisierung des Kosmos, die allein uns wirklich von den perversen
Schwierigkeiten des Umgangs mit Naturkräften befreien und damit auch
das Patentwesen überflüssig machen würde, liegt leider in weiter
Ferne.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: grur-nack00 ;
# txtlang: xx ;
# End: ;

