\begin{subdocument}{grur-nack00}{Nack 2000: Sind jetzt computer-implementierte Gesch\"{a}ftsmethoden patentierbar?}{http://swpat.ffii.org//papiers/grur-nack00/index.fr.html}{Groupe de travail\\swpatag@ffii.org\\version fran\c{c}aise 2003/11/18 par Michèle Garoche\footnote{http://micmacfr.homeunix.org}}{En printemps 2000, la Court F\'{e}d\'{e}rale Supr\`{e}me (BGH) d\'{e}clare dans un changement d'opinion spectaculaire que le caract\`{e}re technique d'un ``appareil de technique programmatrice'' doit \^{e}tre consid\'{e}r\'{e} ind\'{e}pendemment de la question est-ce qu cet ``appareil'' s'accompagne d'une contribution a la technique.  Ainsi il semble que la route a la brevetabilit\'{e} sans limites de toute m\'{e}thode de gestion est ouverte.  Nack demande est-ce que c'est vraiment comme ca.  Il introduit quelques questions fondamentales, montre des inconsistences de la jurisdiction autant de la BGH que de l'OEB (``du point de vue dogmatique le chaos ne saurait \^{e}tre plus grand''), donne l'impression que la loi \'{e}crite ne signifie rien et doit donc \^{e}tre chang\'{e} au sense de brevetabilit\'{e} du logiciel, explique que il est vraisemblable que la BGH changera encore d'avis est il est difficile de pr\'{e}dire dans quelle direction.  Finalement Nack demande que le suj\`{e}t soit trait\'{e} non seulement sur in niveau juridique mais aussi sur un niveau politique:  en 1877, quand l'Allemagne introduisa sa loi de brevet, elle \'{e}tait courageuse et c'a \'{e}t\'{e} un grand succ\`{e}s.  Pour quoi au jour d'hui, dans l'\'{e}poque post-industrielle, on ne montre pas au jour d'hui la m\^{e}me courage en introduisant le brevet aux secteurs de finances, commerce et services, demande-t-il.}
Ralph Nack ist ein junger wissenschaftlicher Mitarbeiter von Prof. Straus am Max-Planck-Institut f\"{u}r Ausl\"{a}ndisches und Internationales Patent, Urheber- und Wettbewerbsrecht\footnote{http://swpat.ffii.org//acteurs/mpi/index.de.html} in M\"{u}nchen, der durch zahlreiche Artikel und \"{o}ffentliche Auftritte sein starkes Engagement f\"{u}r die Ausweitung der Patentierbarkeit unter Beweis gestellt hat.

Auch in diesem Artikel macht Nack zun\"{a}chst kr\"{a}ftig Stimmung f\"{u}r Softwarepatente und versucht den Eindruck zu erwecken, die Gesetzesregel biete ``keine Hilfestellung'', m.a.W. bedeute nichts und werde daher von EPA und BGH auch nicht verletzt.  Einige Kostproben:

\begin{quote}
{\it .. Art 52 EP\"{U} .. Dort stehen vor allem die ``Klassiker'' der nicht patentf\"{a}higen Gegenst\"{a}nde, wie Gesch\"{a}ftsmethoden, Regeln f\"{u}r Gedankliche T\"{a}tigkeiten, aber auch Computerprogramme.  Nimmt man den Wortlaut der Regelung ernst, lautet ihr Auftrag, den Gegenstand daraufhin zu untersuchen, ob er vom \emph{\"{a}u{\ss}eren Erscheinungsbild}, also vom ``Ph\"{a}notyp'' her gesehen keine Gesch\"{a}ftsmethode, Regel f\"{u}r gedankliche T\"{a}tigkeit usw. ist.  GEnau eine solche Pr\"{u}fung f\"{u}hrt jedoch bei Computerprogrammen komplett in die Irre, da dort -- je nach Fassung des Patentanspruchs -- ``formal'' eine Maschine oder ein Verfahren Gegenstand der Patentanmeldung ist.}
\end{quote}

Eben.  U.a. aus diesem Grunde lautet der Auftrag auch nicht, den Ph\"{a}notyp zu untersuchen, sondern die offenbarte Lehre daraufhin zu pr\"{u}fen, ob es sich um eine technische Erfindung handelt.  Diese Lehre ist vom Ph\"{a}notyp (sprachliche Einkleidung) unabh\"{a}ngig.  Eine ``Programm f\"{u}r Datenverarbeitungsanlagen'' ist das, was der BGH etwa in Entscheidungen wie Dispositionsprogramm\footnote{http://swpat.ffii.org//papiers/bgh-dispo76/index.fr.html} und Walzstabteilung\footnote{http://swpat.ffii.org//papiers/bgh-walzst80/index.de.html} auch als ``Organisations- und Rechenregel'' oder ``Rechenprogramm f\"{u}r Datenverarbeitungsanlagen'' bezeichnet, also eine abstrakte Anweisung, ein Algorithmus in beliebiger Entwurfsstufe oder Erscheinungsform.  Dies entspricht dem Verst\"{a}ndnis des Informatikers ebenso wie der patentrechtlichen Literatur zumindest der damaligen Zeit, wie auch die Pr\"{u}fungsrichtlinien des EPA von 1978\footnote{http://swpat.ffii.org//papiers/epo-gl78/index.en.html} belegen.

\begin{quote}
{\it Zu Beginn der Computer-Rechtsprechung in der 70er Jahren entwickelte der Bundesgerichtshof die ``Kerntheorie''.}
\end{quote}

Dieser Satz enth\"{a}lt zwei tendenzi\"{o}se Ungenauigkeiten:
\begin{itemize}
\item
Die Software-Rechtsprechung begann in den 50er Jahren.  Zun\"{a}chst wurden Softwarepatente h\"{a}ufig gew\"{a}hrt. Nach breiter Diskussion setzte sich 1973/76 eine ablehnende Haltung durch.  Nack m\"{o}chte aber, wie man im folgenden erkennt, diese Haltung gerne auf Anf\"{a}nger-Missverst\"{a}ndnisse zur\"{u}ckf\"{u}hren.

\item
Der Begriff ``Kerntheorie'' stammt nicht vom BGH sondern von den zahlreichen Patentjuristen, deren Trommelfeuer in Zeitschriften wir GRUR, Mitteilungen der Deutschen Patentanw\"{a}lte etc den BGH schlie{\ss}lich verunsicherte.  Mit der Bezeichnung ``Kerntheorie'' wird eine naheliegende Betrachtungsweise als abgehobene ``Theorie'' dargestellt.  Eine passendere Bezeichung ist ``Differenzbetrachtung''.  Sie steht im Gegensatz zu der von den meisten Patentjuristen bevorzugten (und daher auch so benannten) ``Ganzheitsbetrachtung''.
\end{itemize}

Nack erl\"{a}utert die ungeliebte Differenzbetrachtung korrekt:

\begin{quote}
{\it Nach dieser Lehre kommt es darauf an, ``in welchen Anweisungen der als neu und erfinderisch beanspruchte Kern der Lehre zu sehen ist''.  Dieser ``Kern'' muss ``technisch'' sein.  ... Nach diesem Pr\"{u}fungsschema muss also zun\"{a}chst ermittelt werden, worin die \emph{Leistung} des Erfinders liegt.  Als zweites muss dann der \emph{Charakter} der Leistung beurteilt werden, sie muss ``technisch'' sein.}
\end{quote}

D.h. es wird nicht der ``Anspruch in seiner Ganzheit'' sondern nur die Differenz zum Stand der Technik bewertet.   Das, was neu ist, muss auch technisch sein und umgekehrt.

Soweit gut.  Im folgenden offenbart Nack eine erstaunliche Begriffsstutzigkeit gegen\"{u}ber der BGH-Doktrin von der technischen Erfindung, wie er etwa in Dispositionsprogramm\footnote{http://swpat.ffii.org//papiers/bgh-dispo76/index.fr.html},Walzstabteilung\footnote{http://swpat.ffii.org//papiers/bgh-walzst80/index.de.html},Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs\footnote{http://swpat.ffii.org//papiers/grur-kolle77/index.de.html} et Bernhardt \& Kra{\ss}er 1986: Lehrbuch des Patentrechts -- Recht der Bundesrepublik Deutschland, Europ\"{a}isches und Internationales Patentrecht\footnote{http://swpat.ffii.org//papiers/krasser86/index.de.html} zum Ausdruck kommt.  Statt diese Doktrin erst einmal richtig darzustellen, beurteilt er sie sofort aufgrund seiner eigenen Lieblingskategorien, n\"{a}mlich der Unterschiedung nach ``herk\"{o}mmlichen Maschinen'' und traditionell nicht patentierbaren Gegenst\"{a}nden.  Dabei vergisst er auch schon, dass es nicht auf Anspruchsgegenst\"{a}nde sondern auf \emph{Erfindugnsleistungen} ankommt.

\begin{quote}
{\it Hier begann jedoch stets ein argumentativer Dschungel: Bei Computerprogrammen besteht n\"{a}mlich die \emph{Leistung} naturgem\"{a}{\ss} in der Erstellung einer Folge von Arbeitsanweisungen (einem Algorithmus im weiteren Sinne); darin unterscheidet sihc zwar ein Computerprogramm nicht wesentlich von einem herk\"{o}mmlichen Verfahren oder einer herk\"{o}mmlichen Maschine, da jeder Vorrichtung und jedem Verfahren eine Algorithmus zugrunde liegt; allerdings wird bei Computerprogrammen dieser Algorithmus nicht in Form einer Interaktion mechanischer oder elektronischer Bauteile, also in ``maschinisierter'' Form formuliert, sondern in Form von Arbeitsanweisungen an einen Computer.  In Anbetracht dieser fehlenden ``Maschinisierung'' fiel es nun \emph{intuitiv besonders schwer}, den ``technischen Charakter'' der Leistung zu bejahen, da insofern eine gewisse Verwandtschaft zwischen Computerprogrammen und den sog. nicht patentf\"{a}higen ``Anweisungen an den menschlichen Geist'' besteht.}
\end{quote}

Einer Maschine, die sich nur durch einen neuen Algorithmus von bekannten Maschinen unterschied, wurde auch schon vor der Zeit der Computerprogramme regelm\"{a}{\ss}ig die Patentierbarkeit abgesprochen.  Ein Algorithmus wird insbesondere laut \emph{Walzstabteilung} nicht dadurch zu etwas patentf\"{a}higem, dass er in ``maschinisierter'' Form beansprucht wird.  Nack missversteht wohl auch den oft und gerne missverstandenen Ausdruck ``Anweisung an den menschlichen Geist''.  Hierbei geht es um rechnerische Probleml\"{o}sungen, also solche, deren G\"{u}ltigkeit durch logischen Beweis, unabh\"{a}ngig von einer etwaigen physikalischen Umsetzung, nachzuweisen ist.  Zwischen solchen Probleml\"{o}sungen und reinen Denkregeln besteht nicht nur eine ``gewisse Verwandschaft'', sondern es handelt sich um die gleiche Art von Leistung.  Und um Leistungsbeurteilung sollte es bei der Patentpr\"{u}fung gehen.  Das Patent ist der Lohn f\"{u}r eine Leistung, nicht f\"{u}r einen Anspruch auf eine ``maschinisierte'' Einkleidung dieser Leistung.  Offenbar f\"{a}llt es Nack \emph{intuitiv besonders schwer}, zwischen logischer Funktionalit\"{a}t und physischer Kausalit\"{a}t zu unterscheiden.  Dem BGH fiel dies in den 70er und 80er Jahren nicht schwer.

\begin{quote}
{\it Im Ergebnis scheiterten daher in dieser Zeit praktisch \emph{alle} Computerprogramme am Bundesgerichtshof, insbesondere auch solche, die eine geradezu ``lineare'' Fortf\"{u}hrung einer ``technischen Wissenstradition'' darstellten.}
\end{quote}

Nack zeigt eigentlich deutlich, dass der Aspekt der ``Wissenstradition'' eigentlich vor allem ihn selbst aber nicht unbedingt den BGH interessierte.

\begin{quote}
{\it Ab 1980 setzte sich beim Bundesgerichtshof die Erkenntnis durch, dass man computerbezogene Erfindungen nicht pauschal vom Patentschutz ausnehmen kann.}
\end{quote}

Nack f\"{u}hrt mit diesem Satz seine Leser in mehrfacher Hinsicht in die Irre:
\begin{itemize}
\item
Es hat noch nie jemand behauptet, dass man dies k\"{o}nnte.  In der Tat wird man z.B. in der Blaulicht-Diode, die f\"{u}r DVD-Laufwerke eine gro{\ss}e Rolle spielt, eine technische Erfindung sehen.

\item
Nack spielt offenbar auf die schillernde BGH-Entscheidung Antiblockiersystem\footnote{http://swpat.ffii.org//papiers/bgh-abs80/index.de.html} an.  Diese bewegte sich jedoch noch immer im Rahmen der bisherigen Doktrin und wurde zudem durch weitere Beschl\"{u}sse wie Flugkostenminimierung zurechtger\"{u}ckt.  Von einer ``Erkenntnis'' oder Entwicklung kann keine Rede sein.  Benkard 1988\footnote{http://swpat.ffii.org//papiers/benkard88/index.de.html} und BGH Betriebssystem 1990\footnote{http://swpat.ffii.org//papiers/bgh1-bs90/index.de.html} sowie zahlreiche unangefochtene BPatG-Urteile der sp\"{a}ten 80er Jahre wenden noch immer die alles andere als ``pauschalen'' Kriterien (s. Kolle\footnote{http://swpat.ffii.org//papiers/grur-kolle77/index.de.html}) an und verhinderten somit in Deutschland die Patentierung von Programmen.  Ein nennenswerter Einbruch kam erst 1991.

\item
Man beachte Wertungen wie ``Erkenntnis'' und anachronistisch verwendeten neuesten apologetischen Jargon wie ``computerbezogene Erfindungen''.
\end{itemize}

Wesentlich interessanter als obige tendenzi\"{o}se Aussagen sind Nacks Analysen der neuen BGH-Entscheidung ``Sprachanalyse'' und des darin zur Aufweichung verwendeten Begriffes ``technische \"{U}berlegungen''.  Dabei stellt er u.a. fest:

\begin{quote}
{\it Das Erfordernis ``technischer \"{U}berlegungen'' ist also dahingehend zu verstehen, dass gerade \emph{der erfinderische Schritt} nur dadurch m\"{o}glich war, dass der Erfinder ein ingenieur- oder naturwissenschaftliches \emph{Vorwissen} hatte.  Damit w\"{a}ren zumindest solche Computerprogramme \emph{nicht patentf\"{a}hig}, bei denen die erfinderische Leistung z.B. auf dem Gebiet der Finanz- und Versicherungswirtschaft liegt.}
\end{quote}

Diese Erkl\"{a}rung d\"{u}rfte zwar in etwa dem Denken der BGH-Richter entsprechen, aber in der Praxis ist Skepsis angebracht.  Man sollte korrigieren:  ``Damit w\"{u}rden zumindest solche Patentanspr\"{u}che auf Schwierigkeiten sto{\ss}en, bei denen der Bezug zur Finanz- und Versicherungswirtschaft unn\"{o}tig in den Vordergrund gestellt wird''.  Weder bei Amazons ``One-Click''-Verfahren noch bei der betriebswirtschaftlich h\"{o}chst bedeutenden Karmarkar-Methode der linearen Programmierung d\"{u}rften an dem weichen Erfordernis der ``technischen \"{U}berlegungen'' anecken.  Andererseits geht Nack dieses Erfordernis schon wieder zu weit:

\begin{quote}
{\it Allerdings werden durch dieses Pr\"{u}fungsschema auch Erfindungen wie die folgende von der Patentf\"{a}higkeit \emph{ausgeschlossen}: Ein elektronisches Signalfilter wird durch einen Computer ersetzt, der das digitalisierte Signal mittels eines speziellen Algorithmus ``filtert''.  Hier ist praktisch \emph{keine} Kenntnis der herk\"{o}mmlichen Technologie mehr notwendig; die Leistung liegt allein in der Entwicklung eines zweckentsprechenden mathematischen Algorithmus.  Denoch liegt diese Erfindung innerhalb einer Wissenstradition, die unstreitig dem Patentschutz zug\"{a}nglich ist.  Die ``mechanische'' Anwendung des Erfordernisses ``technischer \"{U}berlegungen'' k\"{o}nnte hier zur Verneinung der Patentf\"{a}higkeit f\"{u}hren, ein unsinniges Ergebnis.  Auf der anderen Seite w\"{u}rde die Bejahrung ``technischer \"{U}berlegungen'' etwas gek\"{u}nstelt wirken.}
\end{quote}

Offenbar geht es weniger dem BGH als Nack um die Wahrung der Besitzst\"{a}nde des Patentwesens an bestimmten ``Wissenstraditionen''.  Der BGH hat sogar in einigen Entscheidungen dargelegt, dass es unerheblich ist, ob eine heute durch Rechenregeln gel\"{o}ste Aufgabe fr\"{u}her mit technischen Mitteln gel\"{o}st wurde.  Auf die Leistung kommt es an.

Die Rechtsprechung des EPA kommt vom Ergebnis her Nacks W\"{u}nschen besser entgegen, erreicht dieses Ergebnis aber nur um den Preis rechtsdogmatische Verrenkungen, die Nack bemerkenswert klarsichtig aufzeigt:

\begin{quote}
{\it Dieses Konzept meidet die Nachteile, die oben in bezug auf das Erfordernis ``technischer \"{U}berlegungen'' beschrieben worden sind:  Der oben beispielhaft genannte digitale Signalfilter w\"{a}re n\"{a}mlich hiernach patentf\"{a}hig, da ihm eine ``technische Aufgabenstellung'' zugrunde liegt.  Allerdings ist die systematische Einordnung bei der Erfindungsh\"{o}he v\"{o}llig verfehlt: Die \emph{inhaltliche} Pr\"{u}fung des Gegenstands ist Aufgabe des \emph{Erfindungsbegriffs}; die Neuheit und Erfindungsh\"{o}he dienen dagegen der \emph{qualitativen} Pr\"{u}fung.  Ein Bruch dieses Konzeptes w\"{u}rde einen R\"{u}ckfall in das Jahr 1877 bedeuten, als man noch nicht zwischen den \emph{inahltlichen} und den \emph{qualitativen} Anforderungen an eine Erfindung unterschied.}
\end{quote}

Als ``Zwischenergebnis'' h\"{a}lt Nack fest:

\begin{quote}
{\it Bislang ist der Rechtsprechung \emph{vom Ergebnis her betrachtet} eine relativ klare Begrenzung der patentf\"{a}higen Computerprogramme gelungen: Computerprogramme, die in einer natur- oder ingenieurwissenschaftlichen Wissenstradition stehen, wurden tendenziell f\"{u}r patentf\"{a}hig erkl\"{a}rt; dagegen wurde die Patentf\"{a}higkeit tendenziell f\"{u}r solche Computerprogramme verneint, die \emph{nicht} in dieser Tradition stehen. \emph{Dogmatisch gesehen} ist das Chaos jedoch kaum zu \"{u}berbieten; allein der j\"{u}ngste Ansatz des EPA scheint eine hinreichende Zuverl\"{a}ssigkeit zu bieten, auch wenn er systematisch vollkommen verfehlt bei der Erfindungsh\"{o}he verortet wurde.}
\end{quote}

Hier findet sich der Grundgedanke wieder, den Nack auch im MPI-Beitrag zu einer vom BMWi in auftrag gegebenen Studie\footnote{http://swpat.ffii.org//papiers/bmwi-fhgmpi01/index.fr.html} ausarbeitet.  Dagegen ist u.a. folgendes einzuwenden:
\begin{itemize}
\item
Gerade vom Ergebnis der Patenterteilung her betrachtet ist der Rechtsprechung seit ca 1986 keine Begrenzung mehr gelungen.   Patente auf Gesch\"{a}ftsverfahren in weitestem Sinne (aufgabenhafte Funktionslogik) sind die Regel.  Auch im engsten Sinne gibt es sehr viele Gesch\"{a}ftsverfahrenspatente.  Vor 1986 gab es einen klaren Begriff der technischen Erfindung, der nichts mit Nacks ``Traditionen'' zu tun hatte.  Die Einteilung nach Traditionen ist Nacks normative Idee.  Vielleicht liegt diese Idee in der Luft und wirkt somit auch auf das Denken mancher Richter ein.  Zur Beschreibung der Tatsachen wird diese Dichotomie jedoch erst dann einigerma{\ss}en brauchbar, wenn man gro{\ss}z\"{u}gig die Rechtsprechung mehrerer Jahrzehnte durcheinanderr\"{u}hrt und dann anhand der Urteilswortlaute fragt, welche Antr\"{a}ge ``tendenziell'' von Gerichten gebilligt und welche zur\"{u}ckgewiesen wurden.

\item
Der Begriff ``Computerprogramme'' wird hier mehrdeutig verwendet.  Gemeint kann einerseits ein technischer (Naturkr\"{a}fte anwendender) Vorgang sein, der durch ein Computerprogramm gesteuert wird.  In diesem Sinne war etwa in der Antiblockiersystem-Entscheidung von einem ``technischen Programm'' die Rede, wobei die Patentanspr\"{u}che sich jedoch nur auf die Naturkr\"{a}fteanwendung und nicht auf das Programm beziehen.  Noch 1990 best\"{a}tigte die Betriebssystem-Entscheidung diese Sicht, wonach Patente nur physische Kausalit\"{a}ten betreffen k\"{o}nnen, die jenseits des Programmierens liegen und daher ein Betriebssystem gerade kein ``technisches Programm'' ist.  Seit 1991 gibt es BGH-Entscheidungen, welche den Begriff der technischen Lehre aufweichen und somit die Aufrechterhaltung der Trennung zwischen technischen Prozessen (irref\"{u}hrend ``technische Programme'' genannt) und Programmen als solchen zunehmend k\"{u}nstlich erscheinen lassen.  Es gibt jedoch noch keine BGH-Entscheidung, welche ein Programm als solches patentierbar gemacht h\"{a}tte.  Das BPatG hat dieser M\"{o}glichkeit im August 2000 vorerst einen Riegel vorgeschoben.
\end{itemize}

Es folgt eine recht scharfsichtige Analyse zahlreicher Unklarheiten in der Entscheidung ``Sprachanalyse'', an deren Ende res\"{u}miert wird:

\begin{quote}
{\it Insgesamt bleibt daher etwas unklar, welche Auffassung der Bundesgerichtshof \emph{gegenw\"{a}rtig} vertritt; die \emph{bisherige} Rechtsprechung \emph{verbietet} jedenfalls eine ``gespaltene'' Pr\"{u}fung der Erfindungsh\"{o}he; hinzu kommt, dass eine solche Pr\"{u}fung systematisch v\"{o}llig verfehlt w\"{a}re, da die Erfindungsh\"{o}he nicht der Platz f\"{u}r eine \emph{inhaltliche}, sondern f\"{u}r eine \emph{qualitative} Pr\"{u}fung der Erfindung ist (s.o.).}

{\it Geht man nun davon aus, dass der Bundesgerichtshof an seiner bisherigen -- systematisch schl\"{u}ssigen -- Rechtsprechung festh\"{a}lt und keine ``gespaltene'' Pr\"{u}fung der Erfindungsh\"{o}he vornimmt, gelangt man zwangsl\"{a}ufig zu dem Ergebnis, dass von jetzt an \emph{keine substantiellen Schranken mehr f\"{u}r die Patentf\"{a}higkeit von Softwareerfindungen} bestehen: Solange das Programm als ``programmierte Datenverarbeitungsanlage'' beansprucht wird, sind die formalen Anforderungen erf\"{u}llt; der Schutzbereich eines solchen Patents erf\"{a}hrt gegen\"{u}ber einem ``echten'' Anspruch auf ein Computerprogrammprodukt auch keine wesentliche Einschr\"{a}nkung, da der gewerbliche Vertrieb der Software regelm\"{a}{\ss}ig als mittelbare Patentverletzung miterfasst sein wird.  Dar\"{u}ber hinaus l\"{a}sst sich eine Begrenzung dieser Rechtsprechung auf ``Vorrichtungsanspr\"{u}che'' kaum aufrechterhalten, da die Wahl der Anspruchskategorie bei gleichem Anspruchsinhalt an sich keine Auswirkungen auf die Patentf\"{a}higkeit haben darf, was auch der US-amerikanische CAFC in \emph{AT\&T Corp v. Excel Communications, Inc.} zutreffend hervorgehoben hat.}
\end{quote}

Zum Schluss regt Nack die Leser dazu an, \"{u}ber die Konsequenzen auch auf wirtschaftspolitischer Ebene nachzudenken:

\begin{quote}
{\it Wenn der Bundesgerichtshof an dieser Rechtsprechung festh\"{a}lt --- es hat schon mehrere \"{u}berraschende Wendungen gegeben ---, dann m\"{u}ssen sich ganze Wirtschaftszweige auf eine v\"{o}llig neue Form des Wettbewerbs einstellen: ... Diese Wirtschaftsbereiche liegen in Deutschland in einem patentrechtlichen Dornr\"{o}schenschlaf, der sich sehr schnell zu einer Existenzbedrohung entwickeln k\"{o}nnte.}

{\it Ist diese Entwicklung zu begr\"{u}{\ss}en?  Motor des wirtschaftlichen Fortschritts ist heutzutage schhon lange nicht mehr allein die produzierende Industrie; Deutschland war bereits 1993 wieder bei einem Anteil der Industriebesch\"{a}ftigung angekommen, der schon vor einem Jahrhundert erreicht war, was als ``Deindustrialisierung'' bezeichnet wird.  Insofern liegt es nahe, \"{u}ber eine Ausweitung des Patentschutzes in den Bereich der Dienstleistungsindustrie nachzudenken.  Eine Prognose, ob das Patentrecht in diesen neuen Bereichen eine fruchtbare Entwicklung belebt, kann man allerdings kaum treffen; im Grunde stehen wir heute wieder wie 1877 da, als es darum ging, ob der Patentschutz \emph{\"{u}berhaupt} eingef\"{u}hrt werden soll: Damals war man mutig und im Ergebnis \"{u}beraus erfolgreich; was spricht dagegen, heute wiederum mutig zu sein?  W\"{u}nschenswert w\"{a}re, dass der Bundesgerichtshof und die \"{O}ffentlichkeit die Diskussion auch auf dieser Ebene f\"{u}hrt und sich nicht auf rein ``juristische'' Argumente beschr\"{a}nkt.}
\end{quote}

Im letzten Satz wurde die Wortgruppe ``und die \"{O}ffentlichkeit'' offenbar nachtr\"{a}glich eingeschoben.  Kurz vor der Ver\"{o}ffentlichung dieses Artikels zeichnete sich tats\"{a}chlich eine Politisierung des Themas ab.  Auch Nack war auf einer Konferenz im BMWi\footnote{http://swpat.ffii.org//dates/2000/bmwi0518/index.fr.html} zugegen und warf dort den zahlreich versammelten Logikpatentkritikern vor, sie stellten erneut eine Frage zur Debatte, die eigentlich 1877 schon ein paar H\"{a}user weiter im Berliner Reichstag entschieden worden sei.  Bis zum Fr\"{u}hjahr 2000 sah es so aus, als w\"{a}re der Rest tats\"{a}chlich Sache des Bundesgerichtshofes, dessen Rechtsdogmatik letztlich wohl nur deshalb chaotische Bahnen beschreiten konnte, weil diese durch wirtschaftspolitischen Konsens der ma{\ss}geblichen Patentjuristenkreise abgesichert zu sein schien.

Naheliegende Einw\"{a}nde gegen die nun endlich ausgesprochenen und zur allgemeinen Diskussion gestellten Ziele sind:
\begin{itemize}
\item
Das Patentwesen war keineswegs \"{u}beraus erfolgreich, man lese ein wenig volkswirtschaftliche Literatur.

\item
Wesentlich f\"{u}r die Akzeptanz des Patentwesens war die Beschr\"{a}nkung auf technische Erfindungen\footnote{http://swpat.ffii.org//analyse/invention/index.fr.html}, welche Nack nicht verstehen will (und daher vielleicht auch tats\"{a}chlich nicht versteht).

\item
Wenn technische Erfindungen heute nicht mehr so im Mittelpunkt der Wirtschaftsentwicklung stehen und das Patentwesen folglich eine weniger zentrale Rolle spielt als fr\"{u}her, so mag das Patentrechtlern Sorgen machen oder gar in frevelhafter Weise dem Selbstverst\"{a}ndnis der Patentbewegung\footnote{http://swpat.ffii.org//analyse/lijda/index.de.html} widersprechen, wie er in zahllosen Festreden verk\"{u}ndet wird.  F\"{u}r andere besteht aber kein Grund zur Sorge.  Und auch Patentrechtler d\"{u}rften sich nach einem ersten Schock schnell tr\"{o}sten:  echte technische Erfindungen der 90er Jahre etwa im Bereich der blauen Leuchtdiode, des Wasserstoffmotors oder besonders starker Magneten usw sind heute alles andere als unbedeutend, und die v\"{o}llige Mathematisierung des Kosmos, die allein uns wirklich von den perversen Schwierigkeiten des Umgangs mit Naturkr\"{a}ften befreien und damit auch das Patentwesen \"{u}berfl\"{u}ssig machen w\"{u}rde, liegt leider in weiter Ferne.
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
% mode: latex ;
% End: ;

