<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Der Autor, Arno Kvrber, geb 1934, war bis Oktober 1997 Leiter der
Patentabteilung der Siemens AG und ist seitdem Reprdsentant des Hauses
Siemens im gewerblichen Rechtschutz gegen|ber staatlichen und
nichtstaatlichen Einrichtungen und Organisationen.  Er nimmt
Vorstandsfunktionen in zahlreichen Verbandsgremien wahr.  Kvrber
blickt auf die letzten Jahrzehnte zur|ck. Bis vor kurzem entfalteten
Patente ihre Wirkung vor allem in forschungsintensiven Gebieten wie
der Chemie und Pharmazie.  In diesen Gebieten ist der Ausschluss von
Wettbewerbern sowohl wichtig als auch, dank Stoffpatenten, leicht
durchf|hrbar.  In Elektronik und Informationstechnik herrschte
Patentfrieden.  U.a. im Interesse der Interoperabilitdt wurden Patente
nur defensiv eingesetzt.  Mit der Umorientierung des amerikanischen
Patentsystems nach Einrichtung des CAFC dnderte sich dies jedoch
zusehends.  Heute ist es unerldsslich f|r Unternehmen, die beider
Verabschiedung von informatischen Standards ein Wvrtchen mitreden
wollen, |ber ein geeignetes Patentportfolio zu verf|gen.  Um 1990
stagnierte die Zahl der Patentanmeldungen der Siemens-Mitarbeiter bei
ca 2000 pro Jahr.  Das lag u.a. daran, dass die Entwickler glaubten,
Softwarelvsungen seien in Europa nicht patentierbar und in diesem
Glauben von konservativen Patentgerichten und Patentdmtern bestdrkt
wurden.  Daher leistete Siemens einen aktiven Beitrag zur
Rechtsfortbildung.  Ferner wurden Prdmiensysteme f|r Patentanmelder
eingef|hrt.  In wenigen Jahren verdoppelten sich die Anmeldezahlen.

#PBs: Patentfrieden im IT-Bereich 1980 durch USA aufgekündigt

#Ilr: IT-Normierung ein globaler Kampfschauplatz, für den Siemens rüsten
muss

#Snn: Siemens treibt Amerikanisierung der deutschen Erteilungspraxis voran

#KnF: Körber beginnt mit einem Rückblick auf gute alte Zeiten, in denen
seine Branche, die der Elektrontechnik, Elektronik und
Informationstechnik, noch in Frieden arbeiten konnte und Patente nur
die ihnen gebührende Rolle spielten:

#BHs: Bei der Nutzung der verschiedenen Möglichkeiten des Patentschutzes
durch Großunternehmen gab und gibt es jedoch branchenspezifisch
unterschiedliche Schwerpunkte.  So spielt für die chemische und
insbesondere die pharmazeutische Industrie die Sicherung der
Alleinstellung des eigenen Unternehmens durch Ausschluss von
Wettbewerbern mit Hilfe des Patentschutzes nach wie vor eine
bedeutende Rolle.  Dies erklärt sich daraus, dass auf breiter Basis
betriebene Forschungsarbeiten in der Regel nur bei verhältnismäßig
wenigen Produkten zum schließlichen Erfolg führen und der hohe
Forschungsaufwand beim Vertrieb dieser Produkte über ihren Preis
wieder eingespielt werden muss.  Der Patentschutz ist daher
Voraussetzung für die Investition in ein Produkt.  Die betreffenden
Unternehmen melden daher ihre Erfindungen in der Regel auch in
wesentlich mehr Ländern zum Patent an, als dies in anderen Branchen
üblich ist.  Dazu kommt, dass Sachpatente auf chemische Substanzen in
vielen Fällen kaum zu umgehen sind.

#IWp: Im Vergleich hierzu hat der Ausschluss von Wettbewerbern in den
Schutzrechtsstrategien von Großunternehmen der Elektrotechnik- und
Elektronikindustrie in den letzten Jahrzehnten nur eine
verhältnismäßig geringe Rolle gespielt.  Wenn man sich mit
Wettbewerbern über die Hauptmotive für die Anmeldung von Patenten
unterhielt, bekam man in aller Regel die Antwort, es gehe in erster
Linie um die Sicherung der nötigen Freiräume für die eigene
Geschäftstätigkeit, bei der man durch Patente von Wettbewerbern
möglichst nicht behindert werden wolle.  Patente wurden also
vorwiegend dazu eingestzt, einerseits die eigenen Entwicklungsprojekte
durch rechtzeitige Schutzrechtsanmeldungen von später entstehenden
Schutzrechten Dritter freizuhaltenund, soweit dies nicht möglich war,
im Austausch gegen Lizenzen an den eigenen Schutzrechten
Benutzungsrechte an den Schutzrechten der Wettbewerber zu erwerben. 
Der Ausschluss eines Wettbewerbers vom Markt spielte und spielt auch
heute noch dem gegenüber praktisch keine wesentliche Rolle.

#Udu: Unter anderem erklärt sich diese Situation daraus, dass auf den
entsprechenden technischen Gebieten im Verlauf der normalen
Entwicklung gegenseitige Überschneidungen und daraus entstehende
schutzrechtliche Abhängigkeiten oft nicht zu vermeiden sind.  Auf
verschiedenen Gebieten, beispielsweise in der Kommunikations- und
Informationstechnik, ist die sogenannte Interoperabilität, also das
Zusammenwirken verschiedener Systeme, sogar eine wesentliche
Voraussetzung für den Markterfolg.  Dazu kommt, dass in der
Elektrotechnick und Elektronik Möglichkeiten zur Umgehung von
Schutzrechten, wenn auch mit einigem Aufwand, häufig leichter gefunden
werden können, als dies beispielsweise beim chemischen Stoffschutz der
Fall ist. ... Gerichtliche Auseinandersetzungen waren verhältnismäßig
selten.

#DiW: Die Zeiten des relativen Patentfriedens sind indessen auch in unserer
Branche weitgehend zu Ende gegangen.  Zunächst haben gesetzliche
Maßnahmen in den USA, wie die Erstreckung des Schutzes von
Verfahrenspatenten auf das unmittelbare Verfahrensprodukt und die
Gründung des Court of Appeal for the Federal Circuit, zu einer
Stärkung des Patentschutzes geführt.  Dies hat einige Unternehmen dazu
veranlasst, ihre Schutzrechte wesentlich aggressiver als bisher
einzusetzen. ...

#KeW: Körber erläutert im folgenden, warum nun für Siemens und andere
Großunternehmen die Patentstrategie zu einer Überlebensfrage und die
Patentabteilung zu einer zentralen unternehmerischen Abteilung
geworden ist.  Er geht dabei gar nicht auf die Frage ein, ob die von
den USA eingeführten neuen Regeln gut für die Branche oder auch nur
gut für Siemens sind.  Diese Frage stellt sich selbst für einen
Weltkonzern nicht.  Die Regeln sind als Sachzwänge vorgegeben. 
Aufgabe von Körbers Abteilung ist es, unter den gegebenen Umständen
nach der besten Überlebensstrategie zu suchen.  Wer über keine
schlagkräftige Patentabteilung verfügt, kann sich selbst mit viel Geld
kaum den Zutritt zum Markt erkaufen.

#Bul: Beim Erwerb und bei der Vergabe von Patentlizenzen spielt in dieser
Situation der %(q:Preis) eine wesentlich bedeutender Rolle als dies
vordem der Fall war.  Vor dem Abschluss von Patentlizenzverträgen
werden heute üblicherweise die beiderseitigen Patentbestände in
aufwendigen Verfahren im einzelnen inhaltlich bewertet.  Bei
aussichtsreichen, erfolgversprechenden Zukunftstechnologien kann es
für ein Unternehmen, das selbst keine Gegenlizenzen an wertvollen
eigenen Schutzrechten anbieten kann, sogar schwierig werden, gegen
Geld allein Patentlizenzen zu Bedingungen zu erhalten, die es selbst
noch als angemessen betrachten kann.

#EWs: Eine wichtige Rolle spielen Patente inzwischen auch bei
internationalen und nationalen Standards.  Besonders auf technischen
Gebieten, wie der Kommunikations- und Informationstechnik, bei denen
es wesentlich auf die Interoperabilität ankommt, gehen technische
Standards inzwischen sehr ins Detail.  Dadurch steigt die
Wahrscheinlichkeit, dass für den Standard wesentliche Patente bestehen
oder angemeldet werden, also solche Patente, die bei Benutzung des
Standards nicht umgangen werden können.  Bei der Vereinbarung von
Standards durch die Standardisierungsorganisationen ist es zwar
üblich, dass die beteiligten Patentinhaber sich bereit erklären
müssen, an ihren Patenten Lizenzen zu angemessenen, nicht
diskriminierenden Bedingungen zu erteilen.  Dennoch ist ein
Unternehmen, das selbst keine wesentlichen Patente besitzt, im
Nachteil.  Es muss nicht nur von allen Patentinhabern Lizenz nehmen,
sondern kann auch die Durchsetzung eigenenr Entwicklungen als
Standards nicht durch entsprechende Lizenzpolitik fördern.  Bei
sogenannten De-facto-Standards, die sich ohne Vereinbarung im Markt
entwickeln, ist die Situation gegebenenfalls noch kritischer.  Hier
besteht für die Schutzrechtsinhaber, die den Standard etabliert haben,
keinerlei Verpflichtung, jedem interessierten Wettbewerber eine Lizenz
einzuräumen.  Eigene wesentliche Patente können für ein Unternehmen
die einzige Möglichkeit sein, nicht aus dem Markt gedrängt zu werden.

#Zni: Zusammenfassend lässt sich somit feststellen, Patente sind für
Großunternehmen - und nicht nur für diese - heute wichtiger denn je.

#Fnw: Für die Patentabteilung eines Unternehmens ergeben sich unter diesen
Aspekten über die üblichen Dienstleistungsfunktionen hinaus
unternehmerische Aufgaben.  In unserem Unternehmen sehen wir diese in
der %(q:Stärkung der Wettbewerbsfähigkeit des Unternehmens durch
Entwicklung und Umsetzung von Strategien für den gewerblichen
Rechtsschutz).

#Khi: Körber erläutert, dass die Patentabteilung von Siemens in Deutschland
seit ca 1990 auf die Änderung der Erteilungskriterien zugunsten von
Softwarepatenten gedrängt hat.  Das tat sie nicht deshalb, weil ihr
die neuen Regeln für die Branche oder auch nur für Siemens besonders
vorteilhaft erschienen.  Im Gegenteil, für die Mitarbeiter von Siemens
bedeutete die Anmeldung von Softwarepatenten eine unverhältnismäßig
schwierige und zeitraubende Aufgabe.  Aber um Siemens für den Kampf im
amerikanische dominierten Weltgeschehen zu rüsten, war es nötig, sich
dieser Aufgabe auch zu Hause in Deutschland zu stellen:

#Bis: Basis für Patente und damit auch für Patentstrategien sind
Erfindungen, und zwar in unserem Haus wie in anderen Großunternehmen
in erster Linie die Erfindungen der eigenen Mitarbeiter.  Die
möglichst systematische und vollständige Erfassung der entstehenden
Erfindungen ist daher von besonderer Bedeutung.

#HWu: Hier hatten wir Anfang der 90er Jahre ein gewisses Problem.  Die Zahl
der jährlich bei der deutschen Patentabteilung eingehenden
Erfindungsmeldungen lag über mehr als zehn Jahre jeweils um die 2000,
und dies trotz steigenden F&E-Aufkommens, trotz eines zunehmenden
Anteils neuer Produkte und Systeme an unserem Jahresumsatz und trotz
ständiger Bemühungen der Mitarbeiter der Patentabteilung,
Erfindungsmeldungen anzuregen.  Wir fanden für dieses Phänomen
schließlich zwei Hauptursachen.

#Dnt: Die erste Ursache war, dass speziell auf dem Gebiet der Informations-,
Kommunikations- und Automatisierungstechnik zunehmend Hardwarelösungen
durch Softwarelösungen ersetzt wurden und dass viele
Softwareentwickler dem Missverständnis unterlagen, Softwareerfindungen
seien nicht patentfähig.  Zudem waren anfänglich auch nur wenige
Mitarbeiter der Patentabteilung hinreichend erfahren in der Abfassung
von Patentanmeldungen auf softwarebezogene Erfindungen und in der
Führung von entsprechenden Verfahren vor den Patentämtern, ganz
abgesehen von der zunächst keineswegs softwarepatentfreundlichen
Praxis einiger Ämter.  Zur Lösung dieser Probleme beauftragten wir
eine Arbeitsgruppe erfahrener Patentingenieure, weitere ihrer Kollegen
zu trainieren und Informationsveranstaltungen für die entsprechenden
Entwicklungsabteilungen durchzuführen.  Dennoch bleibt die
Patentierung softwarebezogener Erfindungen eine zeitraubende Aufgabe
für Erfinder und Patentingenieure sowohl wegen der üblicherweise hohen
Komplexität der Materie, als auch wegen der noch uneinheitlichen
Praxis von Patentämtern und Gerichten.  Andererseits würde der
Verzicht auf die Patentierung von softwarebezogenen Erfindungen gerade
auf den technischen Gebieten mit den meistversprechenden
Zukunftsaussichten zu erheblichen wettbewerblichen Nachteilen führen. 
Wir sind daher entschlossen, die Patentierung softwarebezogener
Erfindungen weiter voranzutreiben und wie bisher unseren Beitrag zur
Weiterentwicklung von Patentamtspraxis und Rechtsprechung zu leisten.

#Esg: Ein weiterer Grund für die Stagnation bei Erfindungsmeldungen war der,
dass manche Entwickler unter dem ständig steigenden Termindruck nicht
mehr die Zeit fanden, während der Arbeitszeit eine Erfindungsmeldung
abzufassen oder sich wenigstens mit dem für sie zuständigen
Patentingenieur in Verbindung zu setzen. ...

#Hnu: Hier sei daran erinnert, dass Siemens mit IBM zu den wenigen
Unternehmen gehört, die vor dem BPatG und BGH anstrengende
Musterprozesse geführt haben, um die Patenterteilungskriterien auf dem
Wege der Rechtsprechung zu ändern.  Z.B. wurde das BGH-Urteil
%(q:Chinesische Schriftzeichen) durch eine Siemens-Klage
herbeigeführt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: boch97-koerber ;
# txtlang: de ;
# multlin: t ;
# End: ;

