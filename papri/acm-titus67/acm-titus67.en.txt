<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: In 1967 programmers and computing companies almost uniformly resisted the idea of software patentability, proposed to them by the US Patent Office.  Senator Brooks expressed alarm at the PO's move to introduce software patents through a set of guidelines and demanded that these should be %(q:set aside until ... responsible officials at the policy-making levels of the executive and legislative branches of government have had an opportunity to take whatever action might be necessary to protect the public interest).  Patent lawyers and representatives of patent-experienced companies such as Bell Laboratories argued in favor of the PO's move.  BEMA and IBM argued that programs %(q:are not within the present patent statutes and are not suitable for patent protection).  The chairman of the Association for Computing Machinery (ACM) published a poll, according to which most programmers opposed to the idea of software patenting and stressed that %(q:the vital issue of computer program patents should not be left to the deliberation of patent attorneys in government and industry).  This is a fairly detailed account of the struggle and the various positions at the time.  It shows how little has changed in the 35 years since then.
title: Titus 1967: Pros and Cons of Patenting Computer Programs
Tst: Titus Article
Zra: Zachary Statement
UIr: US ACM on %(q:Intellectual Property) today
Tae: This section deals only with copyright, and it sides with free speech against the DMCA and other attempts of %(q:outlawing technology).  Unfortunately, 35 years after Dr. Zachary's emphatic appeal, ACM and its members still seem to turn a blind eye on the much more pervasive %(q:outlawing of technology) and suppression of free speech which is made possible by patents, imposed on the US computing community by a group of %(q:patent attorneys in government and industry).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: acm-titus67 ;
# txtlang: en ;
# End: ;

