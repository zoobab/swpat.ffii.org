<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SPn: Salient Points in English

#I1D: In its %(fs:Error Search Decision of 2001), the patent senate of the
German Federal Court of Justice (BGH) refers to this documentation in
order to justify the great liberty which it is taking with the
non-patentability of programs for computers, mental rules,
mathematical methods etc.

#Dat: The Patent Senate writes

#Brc: Bei der Entstehung des Europäischen Patentübereinkommens herrschte
zwar im Hinblick auf die Patentierung von computerbezogenen Lehren
keine klare Vorstellung darüber, welche Definition gewählt werden
soll.  Während der diplomatischen Konferenz zum Abschluss des
Übereinkommens wurde ausdrücklich darauf hingewiesen, dass vergeblich
versucht worden sei, die Begrifflichkeiten auszufüllen; die Auslegung
müsse der Rechtspraxis überlassen bleiben (Dokument M/PR/I S.28 Tz.
18, Berichte der Münchener Diplomatischen Konferenz über die
Einführung eines Europäischen Patenterteilungsverfahrens,
herausgegeben von der Regierung der Bundesrepublik Deutschland, in:
Materialien zum Europäischen Patentübereinkommen: Anl Bd. 3).

#WsW: We have meanwhile gone long ways to get hold of this text.  What it
reveals is very interesting:

#Btr: Bem: %{FICPI} ist der Interessenverband der Patentanwälte, der auch
heute auf extensive Patentierbarkeit von Programmen und
Geschäftsverfahren weltweit %(vr:drängt).

#Env: Es bestehe die Gefahr, dass auch die solchen Programmen zugrunde
liegenden Strukturen oder Algorithmen nicht als Erfindung angesehen
werden.  Dies könne Probleme für große Industriezweige aufwerfen, die
auf dem Gebiet der Datenverarbeitung speziell oder auch der
Nachrichtentechnik im allgemeinen tätig seien.  Man sollte sich
jedenfalls hüten, bestimmte Techniken, an die heute noch gar nicht
gedacht werde, von der Patentierbarkeit auszuschließen.

#Mrd: M.a.W. FICPI opponierte gegen den Ausschluss von Programmen von der
Patentierbarkeit und wandte bei der Verfolgung dieses Zieles zweierlei
Taktiken an:

#SsW: Stiftung von Verwirrung um %(q:Programm vs Algorithmus) -- der
FICPI-Vertreter unterstellt, mit %(q:Programm) sei etwas anderes als
%(q:Algorithmus) oder eine %(q:zugrundeliegende Struktur) gemeint und
letztere sollten patentierbar sein.  Während die %(q:zugrundeliegende
Struktur) etwas programmfremdes (z.B. eine chemische Reaktion) sein
kann, ist ein %(q:Algorithmus) nach allgemeinem informatischem
Verständnis lediglich das unter einem bestimmten Blickwinkel
beschriebene Programm.  Nach dem Sprachgebrauch der
Patentrechtsprechung und -literatur von damals und heute
(einschließlich %(fs:BGH Fehlersuche)) sind die %(e:Programme für
Datenverarbeitungsanlagen) den %(q:Plänen und Regeln für gedankliche
Tätigkeit) eng verwandt und werden häufig als %(q:Rechenregeln für
Datenverarbeitungsanlagen), %(q:Lehren zum Betrieb von DV-Anlagen) u
ä. bezeichnet.

#Wea: Weckung von Sorge um unvorhersehbare Auswirkungen der Regelung auf
unbekannte Bereiche der %(q:Technik).  Vgl hierzu die heutigen
Argumentationen über %(q:neuronale Netze) und allerlei %(q:hochmoderne
Technologien) von %(ds:Daniele Schiuma) oder %(gi:PA Springorum).

#Dmt: Dieses Argumentationskalkül war auch 1973 verbreitet, und es geht
selbstverständlich in einer Umgebung von Diplomaten auf, die noch
anderes zu tun haben, als sich mit jedem einzelnen Punkt eines von
Rechtsexperten vorgeschlagenen Abkommens genau auseinanderzusetzen. 
Heute wird solches Argumentieren manchmal %(tpe:FUD:fear, uncertainty,
distrust) genannt, und ist nach wie vor weit über die FICPI hinaus
verbreitet, taugt aber dennoch kaum als Grundlage für eine historische
Rechtsauslegung durch hohe Gerichte.

#ZsW: Zu dieser Bemerkung ruft der Vorsitzende in Erinnerung, dass bereits
die Luxemburger Regierungskonferenz vergeblich versucht habe, den
Begriff %(e:Programme für Datenverarbeitungsanlagen) zu definieren.

#MjW: Man müsse hier einfach darauf vertrauen, dass das Europäische
Patentamt diesen Begriff später eindeutig auslegen werde.

#DAr: Der letzte Satz eignet sich, als Ermächtigung des EPA zur freimütigen
Gesetzesauslegung verstanden zu werden.  Gegen solches
(Miss)verständnis spricht jedoch:

#Dcl: Dem EPA wird eine %(q:eindeutige Auslegung) aufgetragen.

#Dve: Der Vorsitzende geht davon aus, dass diese ohne weiteres möglich ist.

#DiW: Dieser Sachverhalt erklärt sich leicht, wenn man bedenkt, dass
%(q:Programm) ein weithin intuitiv verständlicher Elementarbegriff
ist.  Elementarbegriffe zu definieren ist immer schwer, aber dennoch
lassen sie sich eindeutig auslegen.

#Dfl: Der BGH-Patentsenat stellt den Sachverhalt irreführend dar, wenn er
wie oben zitiert behauptet, die Konferenz habe %(q:vergeblich
versucht, die Begrifflichkeiten auszufüllen).  Es gab lediglich einen
Hinweis des Vorsitzenden auf vorangegangene vergebliche
Definitionsversuche.

#Eoe: Es folgen einige Absätze um die unterschiedlichen Nuancen der Wörter
%(q:computer), %(q:Datenverarbeitungsanlage) und %(q:ordinateur) in
den verschiedenen Sprachen.  Offenbar bestand auch hier reichlich
Anlass für Definitionsdebatten, die sich letztlich als überflüssig
erwiesen:

#DsW: Die italienische Delegation ist der Ansicht, der englische Ausdruck
%(q:computer) bezeichne ein viel komplexeres System als der deutsche
Ausdruck %(q:Datenverarbeitungsanlage) und der französische Ausdruck
%(q:ordinateur).  Es sei daher vielleicht angezeigt, in der englischen
Fassung den Ausdruck %(q:data handling systems) zu wählen.

#Dgg: Die britische Delegation führt hierzu aus, ihrer Ansicht nach solle im
Englischen der Ausdruck %(q:computer) beibehalten werden, wenn er
sprachlich auch mehr als eine bloße Rechenanlage bedeuten könne. Die
Auslegung derartiger Begriffe möge der künftigen Praxis der Organe des
Europäischen Patentamtes vorbehalten bleiben.

#Wna: Die österreichische Delegation regt an, die deutsche Fassung daraufhin
zu überprüfen, ob der Ausdruck %(q:Datenverarbeitungsanlage) im
Vergleich mit dem englischen Ausdruck %(q:computer) und dem
französischen Ausdruck %(q:ordinateur) nicht zu weit gefasst ist.  Es
bestehe sonst vielleicht die Gefahr, dass diese Bestimmung anhand der
deutschen Fassung zu weit ausgelegt werde.

#Eih: Es zeigen sich hier unterschiedliche Tendenzen der nationalen
Delegationen.  Die einen wollen klare Patentierbarkeitsausschlüsse,
die anderen sind davon eher beunruhigt.  Dabei zeigen sich nationale
Traditionen, die wohl auch heute noch fortleben.

#Dxu: Der Hauptausschuss kommt überein, den englischen Ausdruck
%(q:computer) als zutreffend im Text zu belassen.  Er beauftragt
ferner den Redaktionsausschuss, zu prüfen, ob sich für den deutschen
Ausdruck %(q:Datenverarbeitungsanlagen) vielleicht ein engerer
Ausdruck finden läßt.

#Vgh: Vermutlich wirkte der englische Ausdruck schon deshalb überzeugend,
weil er gebräuchlich ist und deshalb weniger als bei %(q:DV-Anlage)
die Versuchung besteht, die Fantasie zu bemühen und alles mögliche,
was irgendwie an Informationsverarbeitung erinnert, unter
%(q:Programme für Datenverarbeitungsanlagen) einzuordnen. 
Andererseits war das Ergebnis bekanntlich, dass
%(q:Datenverarbeitungsanlagen) (und nicht etwa %(q:Rechner) oder
%(q:Rechenanlagen) oder gar %(q:Computer)) für zutreffend gehalten
wurde.  Vielleicht entdeckte man in der Beibehaltung der
unterschiedlich nuancierten deutschen, englischen und französischen
Begriffe einen Vorteil: sie ist geeignet, ein Abrutschen des Begriffes
sowohl in allzu umgangssprachlich-konkrete als auch in allzu
abstrakt-philosophische Sphären zu verhindern.  Gemeint ist offenbar
der Neumannsche Universalrechner in allen seinen Ausprägungen.

#EWn: Es folgen einige Absätze, bei denen es um Art 52(4) geht.  Darin kommt
zum Ausdruck, dass %(e:therapeutische Verfahren) zwar zu den
%(e:Erfindungen) gehören aber nicht %(e:gewerblich anwendbar) sind. 
D.h. der zugrunde liegende Begriff der %(e:Gewerblichen Anwendung)
(application industrielle, industrial application) ist enger zu
verstehen als das EPA es heute wahr haben möchte.  Wohl deshalb wurde
Art 52(4) auch im November 2000 gestrichen.

#Dba: Das Ansinnen des EPA-Basisvorschlages vom August 2000, die
Patentierbarkeitsausschlüsse ganz oder weitgehend zu streichen und
ihre Handhabung dem EPA anheimzustellen, findet auch schon 1973 einen
Vorläufer:

#Dic: Die Delegation der CNIPA, unterstützt von der britischen und irischen
Delegation, tritt dafür ein, die Buchstaben c, d, und e in die
Ausführungsordnung zu übernehmen, damit derwissenschaftlichen und
technologischen Entwicklung besser Rechnung getragen werden könne (vgl
Dok. M/20 Nr.10).

#Dts: Die britische Delegation macht hierzu geltend, dass es sich bei den
hier geregelten Fragen auch und in erster Linie um rechtspolitische
Fragen handele, deren Lösung dem Verwaltungsrat als dem politischen
Organ der Patentorganisation zukomme.

#DWg: Die niederländische Organisation gibt, ohne zu dem Problem
hinsichtlich des Grundes Stellung nehmen zu wollen, zu bedenken, dass
sich ein derartiges Ergebnis auch erreichen lasse, indem man Artikel
31 (33) ergänze, durch den der Verwaltungsrat befugt wird, gewisse
Bestimmungen des Übereinkommens zu ändern.

#DWe: Die Delegation der Bundesrepublik Deutschland hält es aus dogmatischen
Gründen nicht für zulässig, die Frage der Patentierbarkeit dieser
Gegenstände oder Tätigkeiten dem Verwaltungsrat zur Regelung zu
überlassen.

#EWn2: Ebenso wenig glaubt die jugoslawische Delegation, dass eine solche
Lösung -- und zwar auch aus Gründen der Rechtssicherheit -- in Frage
kommen könne.

#DWu: Die schwedische und portugiesische Delegation teilen ebenfalls die
Auffassung der deutschen Delegation.

#DnW: Die französische Delegation weist im übrigen darauf hin, dass Artikel
50(52) ein grundlegender Artikel des Übereinkommens sei.  Die in ihm
geregelten Probleme der Patentierbarkeit dürften nicht dem
Verwaltungsrat überlassen werden; dieser dürfte die einzelnen
Bestimmungen -- auf welchem rechtstechnischen Wege auch immer -- nicht
von sich aus ändern können.

#Dlw: Die schweizerische Delegation spricht sich ebenfalls gegen eine
Übernahme der drei genannten Bestimmungen in die Ausführungsordnung
aus.  Sie hebt dabei hervor, das, wollte man der Anregung des CNIPA
folgen, der Verwaltungsrat auch die unerwünschte Möglichkeit hätte,
sowohl neue Patentierungstatbestände in die Übereinkommen einzufügen,
als auch die an Artikel 50 gebundenen Nichtigkeitsgründe abzuändern.

#Iht: Im Anschluss hieran verzichtet die britische Delegation auf eine
Übernahme der Buchstaben c, d und e in die Ausführungsordnung.

#Ese: Es zeigt sich hier deutlich, dass, abgesehen von den Briten, deren
traditionelle Abneigung gegen geschriebenes Recht und Metaphysik hier
wohl zum Ausdruck kommt, die meisten Delegationen der Auffassung
waren, dass es sich bei den geplanten Patentierbarkeitsausschlüssen um
besonders hohe, möglicherweise in der Verfassungsordnung verankerte
Rechtsgüter handele, die das EPA %(q:auf welchem rechtstechnischen
Wege auch immer) nicht ändern dürfe.

#EWn3: Es folgen einige Abschnitte über Fragen aus Bereichen wie Chemie und
Medizin, bei denen wiederum die patentfreundlicheren Delegationen
Probleme aufwerfen und explizite Beschränkungen der Beschränkungen
verlangen.  Schließlich schlägt die deutsche Delegation die
berühmt-berüchtigte %(q:als solche)-Floskel vor.  Man merke: dieser
Vorschlag schließt keineswegs an eine Diskussion über DV-Programme an,
sondern umfasst gleichermaßen alle Ausschlüsse des heutigen Art 52(2):

#Aeh: Auf Vorschlag der Delegation der Bundesrepublik Deutschland (Dok. M/11
Nr. 21) kommt der Hauptausschuss überein, dass die Patentierbarkeit
der in Absatz 2 aufgeführten Gegenstände und Tätigkeiten nur insoweit
ausgeschlossen ist, als sich die Anmeldung oder das Patent auf diese
Gegenstände bzw Tätigkeiten als solche bezieht.

#IWe: Insgesamt lässt sich wohl folgendes festhalten:

#Uhe: Unter den Konferenzteilnehmern und/oder -dokumentatoren herrschte eine
gewisse Unsicherheit bezüglich des Verständnisses von %(q:Programme
für Datenverarbeitungsanlagen).  Dies ist nicht weiter verwunderlich. 
Man kann bei einer historischen Gesetzesauslegung nicht bei der Akte
einer Diplomatischen Konferenz stehen bleiben sondern sollte sich
Hintergründe und Vorarbeiten dieser Akte anschauen, so z.B. damalige
Gerichtsurteile und Meinungen von Rechtsgelehrten in der
Patentrechtsliteratur (z.B. %(gk:Kolle 1973), der darüber ausführlich
berichtet).

#Uln: Unter den Konferenzteilnehmern und -dokumentatoren herrscht keinerlei
Unsicherheit über die Bedeutung des Appositivs %(q:als solche), und es
gibt weder eine feste Wortgruppe %(q:DV-Programme als solche) noch
ergibt sich aus der Dokumentation ein Anlass, eine solche zu
konstruieren geschweige denn ihr eine eigenständige Bedeutung
zuzuweisen (sie zu lexikalisieren).

#Ule: Unter den Konferenzteilnehmern herrscht eine gewisse Uneinigkeit über
die Richtung.  Die einen drängen auf klare Ausschlüsse und
Beschränkung der Vollmachten des EPA, die anderen wollen das Gegenteil
und werben für ihre Meinung, indem sie Angst vor angeblich drohender
allzu ausufernder Auslegung von Ausschlussgegenständen schüren. 
Dieser Angst nimmt die deutsche Delegation mit der beruhigenden (aber
sachlich nichts ändernden) Formel in Art 52(3) den Wind aus den
Segeln.  Alles ganz normale politische Dynamik einer diplomatischen
Konferenz.

#Dtr: Die meisten Delegationen, allen voran die Franzosen und Italiener,
etwas verhaltener die Deutschen und Niederländer, wollen einen
deutlichen Ausschluss der DV-Programme, wie er damals von Gerichten
praktiziert und vom Schrifttum theoretisch untermauert wurde.  Es
werden zwar Definitionsfragen aufgeworfen, diese hält man aber
allgemein nicht für allzu problematisch und vertraut daher der
Fähigkeit des EPA zu einer %(q:eindeutigen Auslegung), möchte dieses
aber andererseits bewusst daran hindern, %(q:auf welchem
rechtstechnischen Wege auch immer) die Patentierbarkeit auszuweiten.

#Eie: Es mag verwundern, wie der BGH und viele andere Patentjuristen diesen
Text zur Begründung ihres Drängens nach Softwarepatenten heranziehen
können.  Die gewöhnlichen Methoden der Rechtsauslegung

#gkh: grammatisch-lexikalische Auslegung

#stA: systematische Auslegung

#hss: historische Auslegung

#tgA: teleologische Auslegung

#vge: verfassungskonforme Auslegung

#wln: werden heute von BGH, EPA u.a. auf den Kopf gestellt.  Alles
konzentriert sich auf angebliche zugrundeliegende Ziele (teleologische
Auslegung, vor allem Ausführungen einen angeblichen Bedarf der
%(q:neuen Technologien) nach Ideenschutz) und auf eine historische
Auslegung, die sich ganz auf eine lückenhafte und obskure
Dokumentation verlässt.

#Asb: Als %(q:obskur) ist diese Konferenzakte deshalb zu bezeichnen, weil
sie

#bek: bislang tief in Bibliotheken vergraben und somit für die
Diskussionspartner unzugänglich war

#ndn: nur einen sehr knappen Einblick in eine Konferenz von Diplomaten
bietet, die eigentlich nicht die Urheber der von ihnen verhandelten
Vorlagen sind und von denen kein detailliertes Verständnis aller
Begriffe zu erwarten ist -- auch heute würden wir das von unserem BMJ
nicht erwarten.

#Ilv: Interessanterweise stützt aber selbst diese obskure Dokumentation die
Argumente derer, die einen vollen Ausschluss der
computer-implementierbaren Organisations- und Rechenregeln (Programme
für Datenverarbeitungsanlagen) im Sinne der Rechtsprechung der 70/80er
Jahre (und des BPatG von 2000) fordern.

#eeo: get the english and french versions from a library, copy the excerpts
and translate this page

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: muckonf73 ;
# txtlang: en ;
# multlin: t ;
# End: ;

