<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Schölch 2001: Softwarepatente ohne Grenzen?

#descr: Im Jahre 2000 veröffentlichte der 10. Senat des Bundesgerichtshofes
(BGH/10) mit den Urteilen %(q:Sprachanalyse) und
%(q:Logikverifikation) eine neue Doktrin über die grundsätzliche
Patentierbarkeit von allem, was als %(q:programmtechnischen
Vorrichtung) beschrieben werden kann.  Der BGH/10 verwarf dabei
Urteile einer unteren Instanz, welche die selben Patentanträge mangels
Technizität zurückgewiesen hatte.  Der 17. Senat des
Bundespatentgerichtes (BPatG/17) hatte eine Differenzbetrachtung
(Kerntheorie) angewandt und konnte daher in den fraglichen
Patentanträgen keinen %(q:Beitrag zum Stand der Technik), m.a.W. keine
%(q:neue Lehre auf dem Gebiet der angewandten Naturwissenschaften)
erkennen.  Günter Schölch, der als Prüfer am Deutschen Patentamt
täglich mit zweifelhaften Programmlogik-Patentanträgen konfrontiert
ist, findet die neuen Vorgaben des BGH/10 weniger einleuchtend als die
Position des BPatG/17 und sorgt sich um die gesellschaftlichen Folgen
einer grenzenlosen Patentierbarkeit.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-schoelch01 ;
# txtlang: de ;
# multlin: t ;
# End: ;

