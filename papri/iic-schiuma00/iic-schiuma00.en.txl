<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Schiuma 2000: TRIPs and Exclusion of Software 'as such' from
Patentability

#descr: An article by Daniele Schiuma, patent attorney from Munich, which
argues that the Treaty on Trade Related Aspects of Intellectual
Property Rights (TRIPs) of 1993 must be interpreted %(e:autonomously),
e.g. independently of regional traditions and solely in view of the
implicit assumptions of the treaty itself, such as its purpose of
establishing a uniform world trade environment on the basis of the
(American) views that were prevalent at the (patent lawyer working
groups of the) Uruguay round.  From these assumptions it follows that
products and processes in the field of %(q:software technology) must
be patentable everywhere across the world.  As a side effect, Schiuma
provides some interesting evidence on how European patent law was bent
to fit a political agenda.

#ThW: This article by Daniele Schiuma in the legal journal IIC Vol 31, which
vehemently argues that the %(q:computer program) exception in %{EPC52}
is non-conformant to %{TRIPs} Art 27 and must be deleted.

#cos: Schiuma, a young patent attorney and research fellow at the Max Planck
Institute for International Patent Copyright and Competition Law and
discussion leader of the MPI's software patent workgroup, champions
the well-known %(tf:TRIPs fallacy) and, in the process, tells us a lot
of interesting details about how the %(pl:patent lobby) managed to
pretend that granting of software patents was legal.

#Sac: Schiuma's argumentation is built on a mixture of quotations from legal
documents and personal political views, such as the assumption that
regional legal definitions of %(q:field of technology),
%(q:invention), %(q:industrial application) etc should be avoided and
a uniform global legal definition must be attempted.  According to
Schiuma's view, the object of the TRIPs treaty is %(q:reducing
distortions and impediments to international trade) and the
non-uniformity of patent law regarding software is such a distortion.

#Tcy: This is one of many unreflected assumptions that make much of
Schiuma's argumentation circular:  Schiuma attempts to prove that the
European software exclusion is not within the scope of variability
allowed by TRIPs.  So he first asserts that TRIPs must be construed so
as to allow no variability.

#Icl: It can however easily be argued that TRIPs is intended to
%(q:harmonise), as it was always called, not to %(q:uniformise) the
systems of various countries.  It imposes more abstract rules such as
that no country shall arbitrarily exclude any field of technolgy it
happens to want to protect.  These rules are intended to allow the
coexistence of diverse systems while minimalising frictions that could
arise from regional protectionism.

#MvS: Moreover, even if variability is to be completely avoided, there is
little reason to assume that the US view, which is also a regional
one, must prevail.  In fact there are, as shown below, reasons to
criticise the US for non-compliance with Art 27 TRIPs.

#Hef: Here is another example of the circularity of Schiuma's reasoning:

#Eet: Even in light of the exceptions in Art.27(3) of TRIPs, it does not
appear permissible to construe the term %(q:inventions) in Art. 27(1)
differently on a country-by-country basis by using the different legal
definitions in the individual member states.  Article 27(3) specifies
definitely those areas of invention that a member can exclude from
patentability.  The attempt to expand artificially the areas of
exclusion permitted by Art. 27(3) by using a %(q:national) legal
definition of the concept of the %(q:invention) to exclude from
patentability subject matter that is actually to be patented pursuant
to Art. 27(1), would thus represent an attempt to evade Art. 27(3) and
would lead to a conflict between the provisions of Art. 27(1) and (3).

#Hlx: Here Schiuma presupposes what he wants to prove: that software is
%(q:actually to be patented pursuant to Art. 27%(pet:1)) and that
European national legislation is %(q:artificially expanding the area
of exclusion) by using a special %(q:legal definition of the term
invention).  The opposite is true:  the scope of what is %(q:to be
patented pursuant to Art 27%(pet:1)) cannot be determined without
recourse to a %(q:definition of the term invention).

#Eqa: Even if Art 27(3) is interpreted as an exhaustive list of subject
matter that does not qualify as %(q:field of technology) according to
27(1), such an exclusion list can not provide a definition of what is
an %(q:invention) in those %(q:fields of technology) for which patents
must %(q:be available).  Without such a definition, TRIPs would have
to be interpreted as demanding the patentability of methods of
business and social engineering, which again might run counter to
basic principles of free trade into which the TRIPs treaty is
embedded.  Thus it is necessary, for an autonomous interpretation of
TRIPs, to provide a consistent and sufficiently restrictive definition
of the terms %(q:technology) and %(q:invention).  The current US
doctrines which Schiuma is advocating here fail to achieve this and
could therefore be said to be in violation Art 27 TRIPs.

#Swo: Schiuma proceeds to argue that patenting software is more in
accordance with TRIPs than not patenting and therefore the whole world
must uniformly patent software.  This argumentation, as most legal
argumenation contains a lot of personal politico-philosophical
assumptions mixed with a complicated set of laws and caselaw.  Here is
an example of Schiuma's philosophy on software:

#leh: ... no [ TRIPs negtioation group ]  member equated or intended to
equate software with mathematical methods or the like.  This would
also be in conflict with the nature of software, according to which
specific (program) commands cause a physical change in specific
elements of the computer.  Accordingly, computer programs, i.e.
software, are always of technical nature.  In contrast, mathematical
methods and scientific theories (as such) represent pure knowledge
that causes no physical changes in its abstract form.  Only the
application of a mathematical method, for instance, in a particular
field of technology (e.g. by means of a computer) causes a physical
change and would thus be technical.

#TWs: This is completely inevident.  Software programs by themselves (i.e.
when not running) also don't cause a physical change in any element of
the computer.  And even when running, the physical change is
determined by the computer hardware and independent of the abstract
idea that is claimed to be inventive.   Software solutions are
solution of problems within a mathematical model: the Turing machine. 
Thus software solutions are mathematical methods.  The fact that the
solution of a problem of Turing calculus (or other kinds of calculus)
can be applied to a technical field does not make these solution
technical ones.

#Tah: There seems also to be a big non sequitur in the following paragraph:

#JWo: Just as pharmaceutical and agrochemical products can be included
within Art 27 of TRIPs without any difficulty, and thus can be
ascribed to a %(q:field of technology), software inventions can also
be qualified as belonging to a %(q:field of technology). 
Correspondingly, software cannot be excluded a priori from the
%(q:field of technology) and thus from Art 27%(pet:1).

#AWW: According to European patent law, there are no software inventions,
because %(q:rules of organisation and calculation) i.e. %(q:programs
for computers) are already completed as problem solutions before,
during their practial application on a conventional computer, the
field of technology is touched upon.

#IiW: Moreover, even if software %(q:can) (or let's say %(q:must)) be
qualified as a %(q:field of technology), that does not imply that
there are any technical %(e:inventions) in this field, for which
patents could be granted.  It could just as well be true, that, as
held by European patent law, all innovation in the field of
%(q:software technology) occurs in the realm of abstract %(q:rules of
calculation and organisation), i.e. non-inventions which are already
completed as solutions to abstract problems of calculus before the
realm of technology is touched upon.

#Asg: Again there are big differences between %(q:software) and other
%(q:fields of technology) such as pharmaceutics.  Just to name a few:

#Str: Software is a basic cultural technique that pervades all fields of
technology and even science, education, commerce etc.  Although
astronomy belongs to a completely distinct %(q:field of technology),
astronomical observatories need to write and share their own software
in order to get their astronomical calculations done.  But they do not
need to develop their own drugs.

#Sir: Software can be copied without costs, and it cannot even be sold. 
What can be sold are only exclusion rights.  Without exclusion rights,
the price of software will tend toward zero.  Moreover, imposing
exclusion rights means reducing important technical qualities of
software, which is why in many cases free software is the best
software.

#TEe: The question of economic effect and desirability of software patenting
does not appear in Schiuma's writing, but there is the assertion that
software has become a %(q:huge industry with an annual economic growth
of 13%{pc}) which %(q:deserves equal protection) with other %(q:fields
of technology).  Evidently, Schiuma knows somehow that the patent
system is not an end in itself but a means for promoting economic
progress.  But his reasoning does not go beyond the level of
postulating %(q:equality).  Moreover, there is no mention about any
real-world cases where people have complained about inequal treatment.
 Schiuma's inequality is not a problem of injustice in real life but
rather a problem of asymetry in Schiuma's explanation model.

#Ird: In a comment on my article, %(bl:Bernhard Lang) pointed out:

#TuW: The trick of lawyers is to place the issues in their own fictitious
world.  If that were justified, the law would never evolve, or would
do so according to the oniric evolution of lawyers dreams.

#Ben: But the law is here to express real world and social consensus. In the
case of patents, it must be explained in terms of human rights,
economic growth, social benefits, innovation incentives (in random
order).

#ToW: The job of Lawyers is syntax: to put the conclusion in usable wording,
not to change or extend the semantics based on purely textual
argumentation.

#Tyc: The first fallacy of Schiumain's rants is that he has no legitimity to
begin with, as is the case for all layers when it comes to decide
whether an existing law applies to a different subject matter.

#Tou: This fallacy is the origin of many problems with the current changes
in economics.

#Lpn: Lawyers are incompetent in both senses of the word:

#tWl: they do not have the knowledge (technical, economic, social)

#ttW: they do not have the legitimacy

#Bst: But, like most, they want the power.

#Wtc: We should be careful not to give them legitimacy by arguing with them.

#Otf: On the other hand we can use their arguments against them (as you most
remarkably did).

#DwW: During the process argumentation he finds some contradictions between
TRIPs and software patenting and even contradictions between the EPC
and the EPO's legal practice.  He resolves all these by reference to
his initially stated basic presumptions that the world's IP system
must be uniformised on the basis of the dominant US practise.  Again
we have a mixture of caselaw with implicit more or less naive
presumptions.

#SqW: Schiuma's article can be quoted to support some of our criticism of
the EPO.

#Tcs: The German legislature, when ratifying TRIPs, assumed that software in
European law is %(q:not a field of technology) and that this European
view is conformant with TRIPs.  Thus, our patent lawyers are clearly
trying to impose their view on that of their sovereign:

#IiW2: In its ratification of GATT/TRIPs, the German legislature saw no
conflict between Sec. 1(2)(3) and 1(3) of the Patent Act and Art.
27(1) of TRIPs...

#LeW: Leaving aside this inaccuracy, the general criticism can, however, be
raised that the German delegation's reply takes as its starting point
the German/European interpretation of the concept of %(q:in all fields
of technology) in Art. 27(1) of TRIPs, which, as shown above, is
inadmissible.

#WaW: Within the meaning of Art. 27(1), software must be regarded both as an
%(q:invention) and as part of a %(q:field of technology), with the
result that the exclusion of software %(q:as such) from patentability
purssuant to Art. 52(2) and (3) of the EPC .. does not conform with
TRIPs.

#Hoe: Hypothesis

#Sai: Schiuma's Evidence

#Tra: The EPO's interpretation of %(q:computer programs as such) as
%(q:computer programs as far as they are non-technical) is wrong.

#Egc: Even if a distinction is made between technical and non-technical
software programs, the provisions of Art. 52(2)(c) and (3) of the EPC
... are incompatible with Art 27(1) of TRIPs, since the former exclude
%(q:programs for computers as such) entirely, i.e. irrespective of
such a distinction between technical and non-technical programs.

#Tnt: The deletion of the computer programs exceptions in 52(2) will result
in more software patenting even under the current regime of bent law

#Tlm: The non-conformity of the provisions of the .. EPC with TRIPs in
regard to software remains evident, for instance, from the decision
%(q:Computer-Related Invention/VICOM), in which one and the same
technical constellation of facts was interpreted differently on the
basis of different wording in the patent claims: on the one hand, a
%(q:process for the digital filtering of data) was regarded as a
mathematical method or as a computer program %(q:as such) and thus was
not patentable, while at the same time a %(q:procedure for the digital
processing of images in the form of a data array) was regarded as
patentable.

#Iur: It appears doubtful whether such a value judgement would have been
made in the same way if the prohibition on patents for computer
programs %(q:as such) contained in Art. 52(2)(c) and (3) of the EPC
had not existed, since an improved %(q:procedure for the digital
filtering of data) (without restriction to the processing of images)
can indeed be a technical contribution .. and thus be accessible to
patent protection as a matter of principle.

#TkE: TRIPs does not take priority over European patent law

#TWW: The decision to classify GATT/TRIPs as community law or as
international law continues to play a decisive role for its status in
its relationship to German (domestic) law: if it is regarded as
community law, it then takes priority, while it enjoys equal status if
it need %(q:only) be regarded as international law.

#TPg: There continues to be disagreement concerning the direct applicability
of the TRIPs provisions in the individual member states.  %(mb:On this
point, the German legislature has assumed that TRIPs is directly
applicable as a matter of principle).

#Ixu: In the present case .. direct applicability can, however, be excluded
since Art. 27(1) of TRIPs is insufficiently clear and unconditional on
the question of the patentability of software.

#TWn: TRIPs does not mandate a change of the EPC

#Tsh: The European Patent Organisation is not a member of the WTO Agreement
and thus not subject to any direct obligations deriving from
GATT/TRIPs.  It is true that all EPC member states, with the exception
of Monaco, are members of the WTO, but TRIPs merely imposes the
obligation to adapt national regulations and not any international
agreements.

#Aro: A lobby withing the EPO is putting European legislatures under
pressure to %(q:conform to TRIPs).  This lobby includes the patent
offices of certain countries as well as the patent lawyers and their
clients.

#Att: At present, there is no specific discussion of any change to the
German Patent Act with respect to Art. 27(1) of TRIPs.  On the other
hand, the Standing Advisory Committee before the European Patent
Office (At the insistence of Austria, Spain, Sweden, Switzerland and
the %(q:users of the system)) has pointed out (at the 28th meeting in
Munich on June 25-26 1998, cf the comment %(q:Points for a revision of
the EPC), CACEPO 2/98) that the provisions of Art. 52(2) of the EPC
with respect to computer programs do not conform with Art. 27(1) of
TRIPs, and should therefore be deleted.

#TTW: The EU harmonisation directive will bring the nation states under the
jurisdiction of TRIPs and WTO related international bodies.  The
patent inflation lobby is pushing for a EU directive in order to
deprive the nation states of their sovereignty in this area.

#Aii: According to Art. 64 of TRIPs, the Understanding on the Settlement for
Disputes (Vereinbarung ueber Regeln und Verfahren zur Beilegung von
Streitigkeiten) is applicable to TRIPs, with the result that a GATT
Member can initiate dispute settlement proceedings before the WTO in
order to reconcile the provisions of the German Patent Act concerning
the
patentability of software %(q:as such) with Art. 27(1) of TRIPs. 
However, at present it is not possible for a natural or legal person
to initiate such proceedings before the WTO.  They can only institute
domestic legal proceedings and persuade their government to initiate
dispute settlement proceedings.

#IWh: It would also be conceivable for an EU Member to invoke the European
Court of Justice pursuant to Art 107 of the EC Treaty for an
infringement of the obligation to implement EC law or secondary law by
another Member.  Proceedings on the infringement of the treaty can
also be initiated by the European Commission according to Art 169 of
the EC Treaty.  However, both possibilities presuppose the competence
of the European Community, which, according to %(eo:Opinion 1/94 of
the European Court of Justice), is divided  with respect to GATT/TRIPs
between the European Community and its member states, with the
European Community enjoying jurisdiction in those areas in which it
has applied harmonisation measures.  In case of patent law, competence
is held by the member states, since here, with the exception of a
number of narrowly defined areas, no harmonisation has (as yet) taken
place.  If the European Communities should subsequently apply
harmonisation measures (fn: The European Commission plans a Directive
for harmonising the patentability of computer software inventions),
the corresponding sector would fall within its competence.

#Tse: The decision to classify GATT/TRIPs as community law or as
international law continues to play a decisive role for its status in
its relationship to German law: if it is regarded as community law, it
then takes priority, while it enjoys equal stats if it need %(q:only)
be regarded as international law.

#TWW2: The EPO is planning to remove all legislative power as to the scope of
patentability from the nation states and take it into its own hand. 
Moreover, the EPO has already invented the new rules by two decisions
in 1997 and now wants these decisions to be codified as binding rules.

#Acs: Alternatively the Preseident of the EPO has proposed to revise Art.
52(1)-(3) to read like Art. 27(1) of TRIPs, so that the exclusion
list, according to Art. 52(2) and (3) of the EPC, would be completely
deleted.  On the other hand, the EPO will revise the examination
guidelines for software-related inventions by including the
interpretation of Art. 52(2) and 52(3) of the EPC according to
decsisions T 935/97 and T 1173/97.

#Ssi: Schiuma concludes his article by stating the conclusion that he tried
to argue for all the way along:

#Tqn: There is a binding obligation in Art. 27(1) of TRIPs to provide patent
protection for %(q:inventions .. in all fields of technology), whereby
these terms must be construed autonomously on the basis of TRIPs, i.e.
independently of national interpretation approaches.

#Ase: As we have read above, TRIPs 27 contains no %(q:obligation to construe
these terms autonomously on the basis of TRIPs).  And the German
legislature doesn't see such an obligation in there either.

#Mdi: More important, such an autonomous interpretation of TRIPs would make
it necessary for us to find a universal definition for the concepts of
%(q:technology), %(q:invention) and %(q:industrial application) which
has a clear delimiting meaning.  It has been %(sk:convincingly argued)
that the European and Japanese traditional concept of %(q:technical
invention) as an %(q:problem solution by application of natural laws)
it the only possible such definition (see especially
%(dp:Dispositionsprogramm 1976) and %(gk:Kolle 1977)).

#Ant: Apparently Schiuma and his mentors from MPI, AIPPI, WIPO and other
organisations of the international %(pm:patent movement) already take
it for granted that they are the world legislators who should, based
on a superficial and arbitrary if not wrong textual intepretation of
the TRIPs treaty, without any need of either legal systematisation or
macro-economic justification, declare the legislative decisions of
sovereign democracies and 100 years of customary patent law doctrine
invalid and instead impose a vague system of unlimited patentability,
based on recent regional specifics of the USA, on the rest of us.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: iic-schiuma00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

