<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Dul: Die Ansprüche 22 bis 24 nach dem Hilfsantrag, die bis auf die in ihnen in Bezug genommenen Verfahrensansprüche mit denen des Hauptantrags übereinstimmen, teilen daher auch die patentrechtliche Bewertung des Hauptantrags. Nachdem über die Anträge der Anmelderin nur einheitlich entschieden werden kann, war schon aus diesem Grunde auch der Hilfsantrag der Anmelderin zurückzuweisen.

#Aen: Aufgrund dieses Umstandes gehen die Verfahrensansprüche nach dem Hilfsantrag ihrem Inhalt nach nicht über den der Verfahrensansprüche nach dem Hauptantrag hinaus.

#Ees: Eine Konkretisierung dieses Sachverhalts erfolgt auch nicht unter Hinzuziehung des Patentanspruchs 2. Danach soll die Regel R~ Fehler simulieren, die durch technische Vorgänge bei der Eingabe des Textes in das Computersystem bedingt sind oder deren Erkennbarkeit durch technische Vorgänge bei der Ausgabe aus dem Computersystem erschwert wird. Dieser allgemeine Hinweis lässt offen me bei der Beseitigung konkret auftretender technischer Fehler bei der Ein- oder Ausgabe vorgegangen werden soll.

#Ecn: Eine konkrete technische Fehlerquelle und eine nachvollziehbare Lehre, wie ein entsprechender Fachmann zur Beseitigung eines solchen Fehlers vorzugehen habe, ist diesen Angaben nicht zu entnehmen. Der aufgezeigte Mangel des Anspruchs ist auch durch Ergänzungen nicht zu beheben, da sich eine diesbezügliche Offenbarung in der Beschreibung (vgl. S. 16) auf vage Andeutungen beschränkt.

#Iev: In Merkmal b ist weiterhin ergänzt, dass die Regel R, nach der die fehlerfreie Zeichenkette S; verändert wird, sich bestimmt %(q:indem eine durch das verwendete Computersystem bedingte technische Fehlerquelle simuliert wird).

#Dtn: Der Hilfsantrag der Anmelderin unterscheidet sich vom Hauptantrag durch eine andere Fassung der Ansprüche 1 und 2. Im Anspruch 1 ist ergänzt, dass das Verfahren zur computer gestützten Suche und/oder Korrektur einer fehlerhaften Zeichenketce automatisch ablaufen soll (vgl. Oberbegriff, Merkmale b und d). Eine inhaltliche Änderung kann hierin nicht erkannt werden, da sich schon aus der Angabe, dass das Verfahren %(q:computergestützt) durchgeführt wird, ergibt, dass die einzelnen Verfahrensschritte selbsttätig ausgeführt werden.

#Dea: Dem Antrag der Anmelderin auf Erteilung des Patents nach dem Hauptantrag war daher nicht zu folgen.

#DWu: Die Frage, ob der auf ein Computerprogramm gerichtete Anspruch 24 - unabhängig von der Technizität des dem Programm zugrundeliegenden Verfahrens - schon aufgrund des Wortlauts des §1 Abs. 2 u 3 PatG nicht als Erfindung gilt, bedarf daher keiner Entscheidung.

#Wi2: Wie im Beschluss des Senats vom 28.7.2000 dargelegt, begehrt die Anmelderin mit diesem Anspruch Schutz für den Programmcode eines Datenverarbeitungsprogramms in jeglicher Form, um damit auch gegen Verletzungen vorgehen zu können, bei denen das Programm über ein Datennetz, beispielsweise das Internet übertragen wird. Wie unter 1.2 ausgeführt, kommt dem Programm bzw. dem ausgeführten Verfahren kein technischer Charakter zu, so dass der Anspruch jedenfalls aus diesem Grund nicht gewährbar ist.

#Dis: Der Anspruch 24 ist auf ein Computerprogramm mit Programmcode zur Durchführung des Verfahrens nach einem der Ansprüche 1 bis 18 gerichtet.

#Dle: Der auf ein Computer-Programm-Produkt gerichtete Anspruch ist in patentrechtlicher Hinsicht daher nicht anders zu bewerten als das Speichermedium nach dem Patentanspruch 22, so dass auf die Gründe unter 1.2 verwiesen werden kann.

#DeP: Der Patentanspruch 23 betrifft ein %(q:Computer-Programm-Produkt). Wie im vorangegangenen Beschluss des Senats ausgeführt versteht die Anmelderin hierunter ein Programmpaket mit mehreren Disketten, das als handelbares Produkt angeboten wird.

#DWh: Das digitale Speichermedium nach dem Anspruch 22 nach Hauptantrag ist daher keine patentfähige technische Erfindung im Smne des § 1 PatG.

#Doi: Die von der Anmelderin angeführten, zu bekannten Verfahren unterschiedlichen Funktionen beruhen jedoch nicht auf einer Auseinandersetzung mit technischen Gegebenheiten, mithin auf technischen Überlegungen, sondern sind bedingt durch einen anderen methodischen Ansatz für die Rechtschreibprüfung, der nicht auf der Benutzung eines Wörterbuchs beruht, sondern auf einer Untersuchung menschlichen Fehlverhaltens beim Eintippen eines Textes. In der Suche fehlerhafter Zeichenketten (Wörter) in einem Text mit Hilfe von Zeichenketten, die sich aus wahrscheinlichem menschlichen, nicht aber technisch verursachtem Fehlverhalten bestimmen, kann eine Leistung auf technischem Gebiet nicht erkannt werden auch wenn die Suche computerimplementiert ausgeführt wird.

#IWf: In entsprechender Weise sind auch die von der Anmelderin hervorgehobenen neuen Funktionen zu sehen, die das vorgeschlagene Verfahren aufweist. Es ist zutreffend, dass die vorgeschlagene Suche fehlerhafter Zeichenketten sprachunabhängig arbeitet, da die Festlegung der fehlerfreien Zeichenkette S; - wie im Anspruch 13 angegeben - aus der Auftrecenshäufigkeit im Text erfolgt. Daraus ergibt sich die Fähigkeit des vorgeschlagenen Verfahrens, auch fehlerhaft geschriebene Eigennamen zu finden und zu korrigieren.

#Whe: Würde Computerimplementierungen von nichttechnischen Verfahren schon deshalb technischer Charakter zugestanden, weil sie jeweils unterschiedliche spezifische Eigenschaften zeigen, etwa weniger Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur Konsequenz dass jeglicher Computerimplementierung technischer Charakter zuzubilligen wäre. Denn jedes andersartige Verfahren zeigt bei seiner Implementierung andersartige Eigenschaften, erweist sich entweder als besonders rechenzeitsparend oder als speicherplatzsparend. Diese Eigenschaften beruhen - jedenfalls im vorliegenden Fall - nicht auf einer technischen Leistung, sondern sind durch das gewählte nichttechnische Verfahren vorgegeben. Würde schon das Erfüllen einer solchen Aufgabenstellung den technischen Charakter einer Computerimplementierung begründen, so wäre jeder Implementierung eines nichttechnischen Verfahrens Patentierbarkeit zuzubilligen; dies aber liefe der Folgerung des Bundesgerichtshofs zuwider, dass das gesetzliche Patentierungsverbot für Computerprogramme verbiete, jedwede in computergerechte Anweisungen gekleidete Lehre als patentierbar zu erachten.

#Aeh: Auch diese Argumentation der Anmelderin greift nicht durch: Was die technische Aufgabenstellung anlangt, so kann hierin nur ein Indiz, nicht aber ein Nachweis für die Technizität des Verfahrens gesehen werden.

#EWn: Ein entscheidendes Indiz für die Technizität des Verfahrens sieht die Anmelderin darin, dass es auf einer technischen Problemstellung beruht. Dadurch, dass das vorgeschlagene Verfahren nicht mit einem Wörterbuch arbeite, könne der Speicherplatz hierfür entfallen. Neben der Einsparung von technischen Ressourcen sprechen nach Ansicht der Anmelderin auch neuartige technische Funktionen, die mit dem vorgeschlagenen Verfahren verwirklicht werden. Das beanspruchte Verfahren baue sich sein Wörterbuch selbstständig auf und arbeite daher sprachunabhängig.

#ZWi: Zu einer anderen Bewertung der Patentfähigkeit des Speichermediums nach dem Anspruch 22 geben auch die weiteren Argumente der Anmelderin keinen Anlass.

#DWl: Das vorgeschlagene Verfahren zeichnet sich sonach nicht durch eine Eigenheit aus, die unter Berücksichtigung der Zielsetzung patentrechtlichen Schutzes -- nämlich Problemlösungen auf dem Gebiet der Technik zu fördern -- eine Patentierbarkeit rechtfertigt.

#Dtb: Dieser Argumentation wäre dann zu folgen, wenn sich die genannten Regeln tatsächlich mit der Erkennung von Fehlern auseinander setzen würden, die durch technische Eigenheiten bedingt sind. Ein solcher technischer Bezug istjedoch aus den Ansprüchen nicht ersichtlich. Zur Korrektur von Fehlern, die beispielsweise durch ein Tastenprellen oder durch eine fehlerhafte Zeichenerkennung hervorgerufen sind, werden keine spezifischen Abhilfemaßnahmen vorgeschlagen. Eine Berichtigung dieser Fehler erfolgt nur beiläufig und insoweit, als diese technischen Fehler dem in den Regeln 1 bis 19 angegebenen menschlichen Fehlverhalten entsprechen. Eine gezielte Beseitigung eines technischen Mangels, etwa gegen das Prellen einer Taste, ist jedenfalls nicht offenbart.

#Hii: Hiergegen wendet die Anmelderin ein, dass mit den Regeln auch Fehler beseitigt würden, die durch fehlerhafte Technik, etwa das Prellen einer Taste schlechte Zeichenerkennung beim Scannen oder bei der Eingabe eines Textes durch Spracherkennung hervorgerufen würden.

#DlW: Dieser Umstand wird besonders deutlich bei der Betrachtung der Regeln R~, die zum Auffinden der Fehler im Text verwendet werden. Diese Regeln bestimmen sich nicht aus technischen Umständen, sondern beschreiben die wahrscheinlichsten Fehlerarten, die einer Person beim Eintippen eines Textes unterlaufen können. Sämtliche der auf den Seiten 11 bis 15 in der Beschreibung genannten Regeln angefangen von der Vertauschung von zwei aufeinanderfolgenden Buchstaben nach der Regel R, bis zur Verdopplung eines doppelt auftretenden Buchstabens, so dass er dreifach auftritt nach der Regel R,9, beschreiben Fehler, die allein durch menschliches Fehlverhalten hervorgerufen werden.

#IVs: Im Vordergrund dieses Verfahrens steht sonach die Lehre, wie in einem Text Fehler - insbesondere Schreibfehler - erkannt und korrigiert werden können. Um das vorgeschlagene Verfahren zu finden bedurfte es deshalb einer Auseinandersetzung mit sprachlichen Regeln, insbesondere der Rechtschreibung, und einer Untersuchung menschlichen Fehlverhaltens beim Schreiber eines Textes. Eine Auseinandersetzung mit technischen Gegebenheiten ist - abgesehen von der Implementierung mit Datenverarbeitungsmitteln - nicht erkennbar.

#Wlw: Wie unter 1.1 ausgeführt, lehren die Anweisungen der in Bezug genommenen Verfahrensansprüche 1 bis 18 - insgesamt gesehen - zur Ermittlung fehlerhafter Zeichenketten in einem Text die Abwandlung einer als fehlerfrei postulierten Zeichenkette nach vorgegebenen Regeln und eine Uberprüfung, ob die fehlerhaft abgewandelten Zeichenketten im Text vorkommen. Komm eine fehlerhaft abgewandelte Zeichenkette im Text vor, so wird sie, zumindest bei geringer Auftretenshäufigkeit, als fehlerhaft angesehen und kann in die als fehlerfrei postulierte Zeichenkette korrigiert werden.

#NAi: Nachdem die Lehre nach dem Anspruch 22 nicht au herkömmlichem technischen Gebiet liegt, ist sonach zu bewerten, ob Anspruch 22 Anweisungen enthält, die den erforderlichen Bezug zur Technik anderweitig herstellen.  Der Bundesgerichtshof verlangt hierfür eine Gesamtbetrachtung darüber, was nach der beanspruchten Lehre im Vordergrund steht (GRUR a.a.0., 144 f., III I b bb und III 2 a).

#Fgr: Falls eine Anweisung nicht auf herkömmlichem technischem Gebiet liegt, ist ihre Patentierbarkeit dennoch unter der Voraussetzung gegeben, dass %(q:die auf Datenverarbeitung mittels eines geeigneten Computers gerichtete Lehre sich gerade durch eine Eigenheit auszeichnet die unter Berücksichtigung der Zielsetzung patentrechtlichen Schutzes eine Patentierbarkeit rechtfertigt) (GRUR a.a.0., 144, III I b bb). Mit dieser Formulierung bekräftigt der Bundesgerichtshof nach Auffassung des Senats die auch in anderen Entscheidungen vertretene Auffassung, dass der Technikbegriff des Patentrechts nicht statisch, das heißt ein für allemal feststehend verstanden werden kann, sondern Modifikationen zugänglich sein soll, sofern die technologische Entwicklung und ein daran angepasster effektiver Patentschutz dies erfordern (vgl. BGH BlPM2 2000, 273, 275 - Logikverifikation m.w.N.).

#Ats: Auf dem Speichermedium nach dem Anspruch 22 sollen elektronisch auslesbare Steuersignale aufgezeichnet sein, die so mit einem Computersystem zusammenwirken können, dass das Verfahren nach einem der Ansprüche 1 bis 18 ausgeführt wird. Das Speichermedium,trägt sonach die Aufzeichnung eines Computerprogramms, das zur Ausführung des Verfahrens geeignet ist.

#Det: Der Bundesgerichtshof hat in der Rechtsbeschwerdeentscheidung festgestellt, dass die im Anspruch 22 enthaltene Lehre nicht schon deshalb patentiert werden könne, weil dieser Anspruch insbesondere auf eine Diskette und damit auf einen körperlichen Gegenstand gerichtet sei (GRUR a.a.0. III 2 6). Wener hat der Bundesger~chtshof ausgeführt, dass die Suche und/oder Korrektur einer fehlerhaften Zeichenkette in einem Text nicht auf technischem Gebiet liege, auch wenn der zu prüfende Text mit einem computergestützten Textverarbeitungssystem erstellt worden sei (GRUR a.a.0., III 2 a).

#Dnn: Die Ansprüche 2 bis 18 befassen sich mit weiteren Einzelheiten des Verfahrens, ohne jedoch dessen grundsätzlichen Charakter in Frage zu stellen.

#SWs: Sonach besteht das computergestützt durchgeführte Verfahren nach den Ansprüchen 1 bis 18 im wesentlichen darin, eine fehlerhafte Zeichenkette (Wort) dadurch zu ermitteln, dass die in dem Text als fehlerfrei postulierten Zeichenketten nach einer (oder mehreren) vorgegebenen Regel fehlerhaft abgewandelt werden, der Text auf das Vorkommen solcher fehlerhaft abgewandelter Zeichenketten durchsucht wird und die aufgefundenen fehlerhaften Zeichenketten in die fehlerfreien berichtigt werden, jedenfalls dann, wenn sie weniger häufig vorkommen als die entsprechende fehlerfreie Zeichenkette.

#UmV: Um in einem Text auf diese Weise (nahezu) alle fehlerhaften Zeichenketten bzw. Wörter ermitteln zu können, muss dieses Verfahren auf alle im Text vorkommenden fehlerfreien Wörter angewandt werden und mit einer Vielzahl von Regeln, da mit einer Regel nur die Erkennung eines Fehlertyps beispielsweise der Vertauschung, der Auslassung oder der Verdopplung eines Buchstabens möglich ist, wie sich aus den in der Beschreibung angegebenen Regeln 1 bis 19 ergibt.

#ZiW: Zur Erkennung von Abwandlungen, die zu anderen korrekten Zeichenketten führen dient die Auswertung der Auftretenshäufigkeiten H(f;~) und H(S;) nach Merkmal e). Kommt eine als fehlerhaft erzeugte Zeichenkette im Text häufiger oder nahezu ebenso häufig vor wie die als fehlerfrei postulierte Zeichenkette, so ist davon auszugehen, dass es sich hierbei um eine andere fehlerfreie Zeichenkette handelt, die nicht korrigiert werden darf. Ist hingegen die Auftretenshäufigkeit der fehlerhaften Zeichenkette wesentlich geringer als die der fehlerfreien Zeichenkette, so ist die fehlerhafte Ze,chenkette mit hoher Wahrscheinlichkeit bspw. durch einen Schreibfehler entstanden und könnce folglich in die fehlerfreie Zeichenkette korrigiert werden, die Ausgangspunkt für die Abwandlung war.

#Nei: Nach Merkmal d) wird die Auftretenshäufigkeit H (f, ) der fehlerhaften Zeichenkette f mit der in Merkmal a) festgeste~lten Auftretenshäufigkeit H(S;) ~er als fehlerfrei postulierten Zeichenkette verglichen. Dieser Vergleich dient nach den Angaben der Anmeldenn dazu festzustellen ob es sich bei der fehlerhaft abgewandelten Zeichenkette mit hoher Wahrscheinlichkeit tatsächlich um eine fehlerhafte handelt. Denn durch die Abwandlung der fehlerfreien Zeichenkette nach der Regel R~ könnte eine andere Zeichenkette entstanden sein, die jedoch ebenfalls korrekt ist.

#VtW: Von der so erzeugten fehlerhaften Zeichenkette f;~ wird gemäß Merkmal c) die Auftretenshäufigkeit H(f ) im Text ermittelt. Aus dieser Ermittlung kann geschlossen werc~en, ob die als fehlerhaft erzeugte Zeichenkette im Text überhaupt vorkommt und wie oft sie ggf. vorkommt. Ist die Auftretenshäufigkeit H(f ~) = 0 so kommt keine nach der Regel R~ fehlerhaft abgewandelte Zeichenkette im Text vor. Im vorgenannten Beispiel würde dies bedeuten, dass - entsprechend der Regel R, - im gespeicherten Text keine fehlerhafte Vertauschung des dritten und vierten Zeichens des Wortes %(q:Olympischen) vorkommt. Kommt die Zeichenkette %(q:Olmypischen) dagegen einmal im gespeicherten Text vor, so handelt es sich bei dieser Zeichenkette wahrscheinlich um einen Fehler.

#Zsd: Zur Suche einer fehlerhaften Zeichenkette f;~ in einem Text wird nach Merkmal b) eine als fehlerfrei postulierte Zeichenkette S nach einer Regel R verändert. Die Regel R ist in den Ansprüchen nicht näher det,iniert. Wie die Anmelc~erin ausführt, sind mögliche Regeln ab S. 11 der Beschreibung erläutert. Als Regel 1 ist beispielsweise die Vertauschung von zwei aufeinander folgenden Buchstaben genannt und an Hand eines Beispiels erläutert. Die fehlerfreie Zeichenkette S, lautet %(q:Olympischen) und wird durch eine einmalige Anwendung der Regel R, in die fehlerhafte Zeichenkette f' %(q:Olmypischen) verändert. Aus dem Kontext ergibt sich dass die herausgegriffene Regel (und auch die anderen in der Beschreibung erläuterten Regeln) mehrfach angewandt werden kann, da nicht nur das dritte und das vierte, sondern auch alle anderen aufeinanderfolgenden Zeichen der Zeichenkette miteinander vertauscht werden können, um eine fehlerhafte Zeichenkette entstehen zu lassen.

#Dtk: Das Verfahren nach dem Anspruch I betrifft die computergestützte Suche einer fehlerhaften Zeichenkette in einem digital gespeicherten Text. Eine Korrektur der fehlerhaften Zeichenkette wird nach dem geltenden und erteilten Anspruch 1 nicht vorgenommen. Sie ist, wie die Anmelderin einräumt, erst Gegenstand des Anspruchs 17. Gegenstand des Verfahrens nach dem Anspruch 1 ist weiterhin nicht die Festlegung der fehlerfreien Zeichenkette S;. Eine Möglichkeit zur Festlegung der fehlerfreien Zeichenketten ist in Anspruch 13 genannt und besteht darin, im Text häufig (über einen Schwellwert ?) vorkommende Zeichenketten als fehlerfrei zu definieren.

#Zsh2: Zu Anspruch 24:

#Zsh: Zu Anspruch 23:

#Ziu: Zur Technizität des digitalen Speichermediums nach Anspruch 22:

#ZnK: Zum Verfahren nach den Ansprüchen 1 bis 18:

#Ime: In den Gründen der Rechtsbeschwerdeentscheidung ist ausgeführt, dass das Bundespatentgericht bei der erneuten Befassung mit der Patentierbarkeit des Speichermediums mit elektronisch auslesbaren Steuersignalen die verfahrensmäßigen Anweisungen der in Anspruch 22 ,n Bezug genommenen Ansprüche 1 bis 17nunmehr 1 bis 18 - zu bewerten habe (GRUR 2002, 145, III 2 c).

#Zin: Zum Hilfsantrag:

#Zan: Zum Hauptantrag:

#DaW: Der Antrag der Anmelderin auf Abänderung des angefochtenen Beschlusses und Erteilung des Patents mit den Unterlagen nach Hauptantrag oder Hilfsantrag kann keinen Erfolg haben. Das digitale Speichermedium nach dem Anspruch 22, das Computer-Programm-Produkt nach dem Anspruch 23 und das Computer-Programm nach dem Anspruch 24 sind weder in der Fassung nach dem Hauptantrag noch in der Fassung nach dem Hilfsantrag patentfähige Ertindungen i.S.d. § 1 PatG.

#Dei: Die Beschwerde ist weiterhin unbegründet.

#Dan: Die Anmelderin stellte den Antrag, den angefochtenen Beschluss dahin abzuändern, dass der Patenterteilung folgende Unterlagen zugrunde liegen: mit Hauptantrag bezeichnete Patentansprüche 1 bis 24, hilfsweise mit Hilfsantrag bezeichnete Patentansprüche 1 und 2 sowie die mit Hauptantrag bezeichneten Patentansprüche 3 bis 24 beide Anspruchsfassungen eingereicht mit Schriftsatz vom 13.3.2002; Beschreibung, Seite 1 bis 26, eingegangen am 12.2.1998 Figuren 1 bis 3 vom 12.7.1993 (AT), Figur 4, emgegangen am 12.2.1998.

#HKn: Hinsichtlich der Fassung der Patentansprüche gemäß dem Hilfsantrag führt die Anmelderin aus, dass die vorgenommenen Ergänzungen dazu dienten, den technischen Charakter des Verfahrens klar herauszustellen.

#Dmn: Daneben weise das vorgeschlagene Verfahren weitere Vorteile auf. Es sei sprachunabhängig, da es sich sein Wörterbuch aus dem Text selbst aufbaue und aktualisiere und könne beispielsweise auch falsch geschriebene Eigennamen korrigieren. Ein nach dem Verfahren arbeitendes Gerät habe daher neue Funktionen, die bekannte Rechtschreibprogramme nicht aufwiesen. Das Verfahren gebe auch eine eigenartige Nutzung einer Datenverarbeitungsanlage an denn kem vernünftiger Mensch arbeite nach der angegebenen Regel. Dies spreche dafür, dass das Verfahren durch technische Überlegungen bestimmt sei. Statistische Erhebungen, wie sie vom Bundesgerichtshof als möglicherweise patenthindernd angeführt worden seien machten den Kern der Erfindung nicht aus.

#Doe: Durch diese Art der Erkennung von Schreibfehlern sei weniger Speicherplatz als bei bekannten Verfahren zur Rechtschreibprüfung erforderlich, so dass dem beanspruchten Verfahren eine technische Problemstellung zugrunde liege. Schon deshalb komme dem Verfahren technischer Charakter zu. In der Entscheidung %(q:Suche fehlerhafter Zeichenketten) habe der Bundesgerichtshof ausgeführt, dass es ein entscheidendes Indiz für die Technizität sei, wenn die prägenden Anweisungen der beanspruchten Lehre der Lösung des konkreten technischen Problems - hier der Speicherplatzeinsparung - dienten.

#Dre: Das auf dem Speichermedium aufgezeichnete Verfahren unterscheide sich von den gängigen Verfahren zur Rechtschreibprüfung darin dass es kein Wörterbuch benutze, um die korrekte Schreibweise des gespeicherten Textes zu überprüfen. Es beruhe darauf, in als fehlerfrei vorausgesetzte Wörter des Textes häufig auftretende Fehler zu injizieren und den Text daraufhin zu untersuchen, ob diese fehlerhaft abgewandelten Wörter vorkämen.

#Wgu: Wegen der übrigen Ansprüche wird auf die Akten verwiesen. Die Anmelderin führt aus, dass nach den Ausführungen des Bundesgerichcshofs in der Rechtsbeschwerdeentscheidung die Patentfähigkeit der auf ein Speichermedium gerichteten Ansprüche dann anzuerkennen sei, wenn das aufgezeichnete Verfahren technischen Charakter habe.

#dmt: durch Vergleich der Auftretenshäufigkeiten H(;~) und H(S;) automatisch entschieden wird ob die mögliche fehlerhafte Zeichenkette f;~ als gesuchte fehlerhafte Zeichenkette F; verwendbar ist.

#drp: die Auftretenshäufigkeit H(;~) der Zeichenkette f;~ im digital gespeicherten Text ermittelt wird und

#dei: die fehlerfreie Zeichenkette S automatisch nach einer Regel R~ verändert wird, indem eine durch das verwendete Computersystem bedingte technische Fehlerquelle simuliert und dadurch mindestens eine mögliche fehlerhafte Zeichenkette f erzeugt wird,

#del: die Auftretenshäufigkeit H(S;) der fehlerfreien Zeichenkett S; im digital gespeicherten Text ermittelt wird,

#VWt: Verfahren zur automatischen computergestützten Suche und oder Korrektur einer fehlerhaften Zeichenkette F; in einem digital gespeicherten Text, der die entsprechende fehlerfreie Zeichen kette S; enthält, dadurch gekennzeichnet, dass

#VWp: Von den mit Hilfsantrag bezeichneten Patentansprüchen laute Anspruch 1:

#CuW: Computer-Programm mit Programmcode zur Durchführun des Verfahrens nach einem der Ansprüche 1 bis 18 wenn das Pro gramm auf einem Computer abläuft.

#ArW3: Anspruch 24:

#CeA: Computer-Programm-Produkt mit auf einem maschinenlesbaren Träger gespeichertem Programmcode zur Durchführun des Verfahrens nach einem der Ansprüche 1 bis 18 wenn das Programmprodukt auf einem Rechner abläuft.

#ArW2: Anspruch 23:

#DWe: Digitales Speichermedium, insbesondere Diskette, mit elektrc nisch auslesbaren Steuersignalen, die so mit einem programmie baren Computersystem zusammenwirken können, dass ein Ve fahren nach einem der Ansprüche 1 bis 18 ausgeführt wird.

#ArW: Anspruch 22:

#bee: basierend auf dem Vergleich in Schritt d) entschieden wird, c die mögliche fehlerhafte Zeichenkette f' die gesuchte fehle hafte Zeichenkette F ist.

#dev: die Auftretenshäufigkeiten H(f')und H(S') verglichen werden und

#dHW: die Auftretenshäufigkeit H(f') der Zeichenkette f' in de Text ermittelt wird,

#dRh: die fehlerfreie Zeichenkette S; nach einer Regel R verändert wird, so dass eine mögliche fehlerhafte Zeichenkette f' erzeugt wird,

#dfh: die Auftretenshäufigkeit H(S;) der fehlerfreien Zeichenke te S; ermittelt wird,

#Vth: Verfahren zur computergestützten Suche und/oder Korrekt einer fehlerhaften Zeichenkette F; in einem digital gespeichern Text, der die entsprechende fehlerfreie Zeichenkette S; enthält, dadurch gekennzeichnet, dass

#Apc: Anspruch 1:

#Vta: Von den mit Hauptantrag bezeichneten Patentansprüch lauten:

#Dn2: Die Anmelderin verfolgt in Fortführung ihrer Beschwer vor dem Senat die Patenterteilung weiter mit den mit HauF antrag bezeichneten Patentansprüchen 1 bis 24, hilfsweise n den mit als Hilfsantrag bezeichneten Ansprüchen 1 und 2 soa mit den mit Hauptantrag bezeichneten Ansprüchen 3 bis 24, j weils eingereicht mit Schriftsatz vom 13.3.2002.

#IWq: Im vorliegenden Fall sei eine Bewertung nötig, ob A spruch 22 Anweisungen enthalte, die den erforderlichen Bez zur Technik herstellten. Dabei seien vor allem die verfahrer mäßigen Anweisungen der in Anspruch 22 in Bezug genommenen Ansprüche 1 bis 17 zu bewerten

#AWa: Auf die vom Senat zugelassene Rechtsbeschwerde hin hat der Bundesgerichtshof die Sache an das Bundespatentgericht zurückverwiesen. In den Gründen ist ausgeführt, dass bei der Bestimmung, was als Programm für Datenverarbeitungsanlagen vom Patentschutz ausgenommen sei, weil es ein %(q:Programm als solches) sei, nicht allein auf das Verständnis von Computerfachleuten zurückgegriffen werden könne, sondern die Bestimmung ausgehend vom Wortlaut sachbezogen nach dem Sinn und Zweck der gesetzlichen Regelung zu erfolgen habe. Programme für Datenverarbeitungsanlagen seien weder schlechthin vom Patentschutz ausgenommen, noch könne bei Vorliegen der weiteren Voraussetzungen des Gesetzes für jedes Computerprogramm Patentschutz erlangt werden. Die Abgrenzung der für Datenverarbeitungsanlagen bestimmten Programme, für die als solche Schutz begehrt werde von computerbezogenen Gegenständen, die § 1 Abs. 2 Nr. 3 PatG nicht unterfielen, führe dazu, dass Ansprüche, die zur Lösung eines Problems auf den herkömmlichen Gebieten der Technik die Abarbeitung bestimmter Verfahrensschritte durch einen Computer vorschlügen, grundsätzlich patentierbar seien.

#Deh: Der Senat hat die Beschwerde mit der Begründung zurückgewiesen, dass das digitale Speichermedium nach dem Anspruch 22 keine patentfähige Erfindung sei und insbesondere auf ein Programm für Datenverarbeitungsanlagen %(q:als solches) gerichtet sei, das nach § 1 Abs 2 Nr. 3 und Abs 3 PatG nicht als Erfindung anzusehen sei (BPatGE 43, 35 = BIPMZ 2000, 387 - Digitales Speichermedium).

#MWe: Mit der gegen die Zurückweisung des Hauptantrags gerichteten Beschwerde verfolgte die Anmelderin die Erteilung des Patents auf der Grundlage von Ansprüchen 1 bis 24 weiter, wobei die Ansprüche 1 bis 21 mit den Ansprüchen des nach dem Hilfsantrag erteilten Patents übereinstimmen, der Anspruch 22 auf ein %(q:Dignales Speichermedium), der Anspruch 23 auf ein %(q:Computer-Programm-Produkt) und der Anspruch 24 auf ein %(q:Computerprogramm mit Programmcode) gerichtet sind.

#Ati: Auf die Anmeldung wurde von der Prüfungsstelle für Klasse GO6F des Deutschen Patentamts mit Beschluss vom 6.7.1998 ein Patent gemäß dem Hilfsantrag (Patentansprüche 1 bis 21) erteilt. Der Hauptantrag mit dem die Anmelderin die Erteilung mit Ansprüchen 1 bis 22 begehrte, wurde zurückgewiesen. In den Gründen ist ausgeführt dass das mit dem Patentanspruch 22 beanspruchte drgitale Speichermedium in keinem einheitlichen Zusammenhang zu den anderen Ansprüchen stehe.

#D7W: Die vorliegende Patentanmeldung ist beim Deutschen Patentamt am 12.7.1993 unter der Bezeichnung: %(bc:Verfahren und Computersystem zur Suche fehlerhafter Zeichenketten in einem Text) angemeldet worden.

#Bc6: BPatG, Beschl. vom 26. März 2002

#Ahi: Amtlicher Leitsatz

#IWW: In der Suche fehlerhafter Zeichenketten (Wörter) in einem Text mit Hilfe von Zeichenketten, die sich aus wahrscheinlich menschlichem, nicht aber technisch bedingtem Fehlverhalten bestimmen, kann eine Leistung auf technischem Gebiet nicht erkannt werden, auch wenn die Suche computerimplementiert ausgeführt wird.

#Ste: Suche fehlerhafter Zeichenketten -- Tippfehler

#descr: The 17th senate of the Federal Patent Court (BPatG/17) rejects a patent claim on a method for automatically correcting erroneous text strings due to lack of technical character and violation of Art 52 EPC / §1 PatG.  Saving computing time or storage space cannot constitute a patentable achievement, because otherwise any computer program (any teaching for use of computers) would become patentable subject matter.  The senate leaves open the question whether program claims could be admissible according to §1 PatG, when the process claim to which they refer is grantable.  Earlier BPatG/17 had argued that program claims are not permissible under Art 52.2c, but BGH/10 had found the argumentation unsatisfactory and sent the case back with the request that the teaching in the process claim should be examined first.

#title: BPatG Error Search 2002/03/26: system for improved computing efficiency = program as such

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bpatg17-suche02 ;
# txtlang: en ;
# multlin: t ;
# End: ;

