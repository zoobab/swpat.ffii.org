<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html lang="nl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<title>Open letter to the Members of the European Parliament w.r.t. software patents</title>
</head>
<body>
<P ALIGN=CENTER>
<A HREF="http://petition.eurolinux.org"><img src="http://aful.org/images/patent_banner.png" alt="Say no to software patents" border="0"></A>
</P>
<i>
<b>New</b>: an English <A HREF="why.html">summary of who will profit and who won't</A> of the directive as its currently proposed, as well as some information on what you can do.<br>
<br>
(this is a translation of an <A HREF="../index.html">open letter/mail</A> I sent to the Flemish members of the European Parliament. Feel free to get some inspiration from it to send your own. More information can be found on <A HREF="http://swpat.ffii.org">the website of the FFII</A>. Thanks to Alex MacFie for his linguistic advice :)</i>
<p>
Dear Members of the European Parliament,
<br><br>
<p>
I am a Master of Sciences in informatics and am currently working on my PhD at the University of Ghent. My employer is the Institute for Encouragement of Innovation using Science and Technology in Flanders (<A HREF="http://www.iwt.be">IWT</A>). I have written you already once before, about the proposal regarding the legislation concerning unsolicited email. I am very glad that back then, the European parliament firmly prioritised the rights of the individual and decided on a strict &quot;opt-in&quot; policy. Currently, there is once again a dossier on the agenda about which I hope you will make the right decision.
<p>
After reading the <A HREF="http://www.europarl.eu.int/meetdocs/committees/juri/20030616/488980en.pdf">report</a> from the rapporteur regarding software patents, I am very happy no-one wants to introduce a regime like the one in the USA. Nevertheless, I think that the current proposal still goes way too far and that it contains several misleading statements which would persuade you to approve it. I will start by giving four specific examples of such statements together with counter arguments, after which I will conclude with an analogy that will hopefully convince you that software development is fundamentally different from other industry branches that currently have patent protection.
<p>
Specific remarks with regard to the working document:
<p>
<ol>
<li> p. 9, amendment 8, article 16: &quot;With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.&quot;
<p>
&bull; Although at first sight this may indeed seem self-evident, it is not evident at all. There is only one possibile situation where software patents might benefit the European software industry (and by extension the whole community). This would be if currently a lot of imitation of innovation were occurring within the European Union (since European patent law does not have any influence anywhere else), in a way that would discourage or hamper innovation.
<p>
First of all, software patent law is completely independent from the fact that manufacturing is being moved to low-wage countries; this discussion should be completely independent from that matter and only look at whether or not software patents can benefit the European software industry. Secondly, I will further on give several arguments and refer to studies showing that a strong protection policy regarding software and computer based systems is counterproductive.
<p>
Finally, the fact that Europe does not (yet) have enforceable software patents is at least also an advantage for European companies. After all, they can already perfectly acquire and enforce software patents in countries stuck with a slack jurisdiction on this matter. On the other hand, foreign companies which are negatively affected by this situation, cannot do the same here and therefore cannot &quot;attack&quot; European companies on their home front.
</li>

<li> p. 18, point 4, &quot;The impact on small and medium-sized software developers&quot;, general
<p>
&bull; This is indeed a point that is extremely important. The rapporteur states that it should be followed very carefully, which shows that even she is not entirely certain of whether patents are so great for that kind of entrepreneurs. Then again, she does have every reason to doubt this. After all, most software patents belong to large companies, even though they are not necessarily the biggest innovators (see <A HREF="http://www.base.com/software-patents/scoreboard.html">this page</A>). A good example of how they can abuse this advantage, is the behaviour of IBM in the USA.
<p>
IBM owns <A HREF="http://www.forbes.com/2003/08/07/cx_ld_0807ibm.html?partner=yahoo&referrer=">thousands of software related patents</A>. The consequence is that there are barely any programs that do not infringe on at least one of those patents (see <A HREF="http://swpat.ffii.org/players/ibm/index.en.html#gajn">this page</A>). When IBM notices a new technology to which it does not yet have access, it just verifies which of IBM's patents the innovating company uses without having a license for it. Next, it contacts the company with a proposal to cross-license: IBM is willing to let the company use their own patents, if the company lets IBM do the same. If the company refuses this offer, IBM can sue them (after all, they are using IBM's patents without their approval) or make them pay dearly for the right to use those patents.
<p>
This means that small and medium enterprises only enjoy &quot;protection&quot; via patents with regard to other companies of the same size, but large companies can still take whatever they want. So for large companies, not much changes if software patents are allowed, apart from the fact that they start investing more in patents and less in research and development. Afterwards, they recoup their patenting costs from smaller companies, which results in these companies also having a smaller R&D budget. The bottom line is that there is less innovation, especially where it counts most (small start-ups).
<p>
I want to state clearly that this is not a purely theoretical possibility, this is the way IBM currently behaves in the USA. There is no reason why they (or another large - possibly even a European - company) could not start doing the same in Europe. IBM currently cannot use this tactic in Europe, since we do not have enforceable software patents here. In the USA however, some people fear that over time the whole software industry will have to pay a 1% to 5% &quot;software tax&quot; to IBM (see link at the start of this answer).
<p>
A final remark with regard to this point: suppose someone wants to create a very innovative product, but he wants to avoid using any patented techniques. How is he supposed to do that, given that IBM alone already owns several thousands of software patents? Almost no-one can afford the small army of lawyers necessary to figure this out.
<p>
Currently, someone with a computer at home can perfectly start doing software development and start a small company that way. He does not have to fear that all his efforts and investments are in vain. After all, when he brings his product to the market, it is not possible for another company to tell him that his program is infringing on patents X and Y and that he cannot get a license for them (or only a prohibitively expensive one). So software patents create a very uncertain framework for enterprising people to work in, which obviously discourages such people. You do not need a huge factory or a big company infrastructure to develop new software (or software techniques), just creative people with a computer.
</li>
<li> p. 18, point 4, last sentence of the first paragraph: &quot;Moreover, a study conducted by the Intellectual Property Institute in London has found that 'the patentability of computer-related inventions has helped the growth of computer program-related industries in the US, in particular the growth of small and medium enterprises and independent software developers into sizeable indeed major companies'.&quot;
<p>
&bull; On the other hand, there are studies showing that the patentability of software does not contribute at all to the growth and progress of this industry. An example is <A HREF="http://www.researchoninnovation.org/patent.pdf">this paper</A>. It first presents a complex mathematical model, which is then verified with reality. Figure 5 on page 30 clearly shows that the percentage of their budget that North-American companies spent on software R&D, strongly diminished at the start of the eighties when software patents were introduced in the USA. From then on, a part of the research budget is simply used to acquire patents (and presumably also to fund the accompanying lawsuits).
<p>
I would strongly suggest you to read at least the introduction (pp. 3-4) and the conclusion (p. 21). They clearly explain how this can be and why it is logical that patents do not encourage software development, but hamper it. This is because the software industry is a lot more dynamic than the classic manufacturing industry.
<p>
One does not have to build a completely new factory or buy new computers to simply develop a new program. Additionally, software development consists almost entirely from incremental and complementary improvements (in other words, software development is rather evolutionary than revolutionary). When these improvements can occur unhampered (i.e., without the intervention of patents), over time they benefit both the original developer and the incremental innovator - and eventually the whole industry and the community at large. This is all explained and demonstrated in that paper.
<p>
</li>
<li> p 19, point 5, general
<p>
&bull; This point partially confirms what I mentioned in the previous answer: companies invest a considerable part of their research budget on patenting the results (instead of patents allowing them to gain more money from their innovations, allowing them to raise their research budget). Since the ultimate goal of patents is to encourage innovation, this is a completely paradoxical effect. The last paragraph concludes with &quot;Moreover, academic studies have shown a link between R&D spending, patent applications and productivity.&quot; What exactly this link is, is mentioned nowhere at all. The paper I referred to in my previous answer, shows that it most certainly is not positive per definition.
<p>
<br>
Finally, I would like to quote a comparison made by <A HREF="http://www.stalllman.org">Mr. Richard Stallman</A> in a speech he gave at our university. Although I am not in complete agreement with his opinions on all issues, in this case he absolutely hit the nail right on the head. He compared software development to composing a symphony.
<p>
Just like you cannot simply throw together a few known &quot;musical techniques&quot; and get something that sounds beautiful, you likewise cannot just mix a few (patented) software methods and get a great program. A program only becomes good because of the many hours of detail work spent on it - like the searching of errors (bugs), making the interface pleasant to use and the general finish - not because it uses one or other technique.
<p>
In the same spirit: Beethoven is generally considered quite progressive for his time. Nevertheless, if musical patents had existed in his days, he would not have been able to make any of his compositions without infringing on any of them. He may have been good, but he was not that good so that he could reinvent music from scratch. Software development is identical: you can be a fantastic software developer, but you simply cannot reinvent informatics from scratch.
<p>
Both music and informatics are based on mathematics combined with the creativity of the composer/developer. Would you be a proponent of patents on music?
<p>
With friendly regards,
<p>
<br>
Jonas
</body>
</html>