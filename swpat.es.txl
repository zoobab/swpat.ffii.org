<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentes de Software contra Democracia Parlamentaria

#descr: Explicamos la situación acerca de los monopolios sobre ideas
concedidos por el Estado, especialmente en el contexto de la directiva
propuesta %(q:sobre la patentabilidad de invenciones implementadas en
ordenador) (directiva de patentes de software), la cual se ha
convertido en la demostración de hasta qué punto los Parlamentos
tienen algo que decir en la actual legislación Europea.

#banana0503: Ne al Banana Unio, Ne al Softvaraj Patentoj

#rWa: Situación actual

#eaW: A pesar que el Parlamento Europeo %(ep:excluyó claramente) las
%(si:patentes de software), la Comisión y el Consejo %(ci:ignoraron la
proposición del Parlamento) y volvieron a presentar el %(pp:texto más
favorable a las patentes) el 18 de Mayo de 2004, en una %(ri:maniobra
de engaño de último minuto orquestada por la Comisión y el Gobierno
alemán). Repetidas peticiones de varios Parlamentos nacionales y
Gobiernos para renegociar esta decisión condujeron a un gran retraso
en su adopción, pero el Consejo rehusó llevar a cabo la segunda
votación requerida y, el 7 de Marzo de 2005,  %(co:declaró que el
acuerdo había sido adoptado). Una semana antes, la %(ce:Comisión)
había %(rs:rechazado) una petición unánime del Parlamento Europeo para
recomenzar todo el proceso.

#odW: Se espera que el Parlamento Europeo comience en Abril la %(e:segunda
lectura) de la directiva sobre la base de una %(q:posición común)
%(lq:legalmente cuestionable). Se espera que el ponente designado, el
antes Primer Ministro francés %(MR), abogue por volver a la
%(ep:posición que en septiembre de 2003) sostuvo el Parlamento
(libertad de publicación e interoperabilidad, restricción de la
patentabilidad al terreno de lo físico).  Una votación tendrá lugar
probablemente en Julio. Las condiciones para alcanzar mayoría son más
estrictas que en 2003: ahora las ausencias cuentan a favor del
Consejo. Si Rocard consigue introducir todas las enmiendas clave de
2003 en esta segunda lectura, tendrá una fuerte posición negociadora
en el proceso de %(e:conciliación) subsiguiente. Sin embargo, una
directiva aceptable sólo puede ser lograda debilitando el monopolio
que los funcionarios de patentes (el grupo que controla la EPO) tienen
en el proceso de toma de decisión del Consejo. Por tanto, las
libertades básicas de la sociedad de la información y la
democratización de la UE han llegado a ser cosas estrechamente
relacionadas.

#Idj: Un drama similar está teniendo lugar en %(in:India) en este mismo
momento. Los funcionarios de patentes han usado el pretexto de la
adopción del %(tr:TRIPs) para %(na:declarar legales las patentes), en
un decreto administrativo unilateral, sujeto a la aprobación del
Parlamento en la primera mitad de 2005. Los funcionarios indios han
demostrado estar a la altura de sus contrapartidas europeas, y las
informaciones críticas en los medios han sido incluso más débiles que
en Europa.

#ott: Introducción rápida

#tae: Latest Software Patents Granted by EPO

#pra: Pending Patents

#nnn: %(ua:Urgent Appeal) for reopening of the discussion in the Council

#jt0: >%(NS) signatures, >%(NC) CEOs

#oIt: Protect Innovation

#noW: business testimonies about patents

#cse: introduction for the masses, campaign funded by %(LST)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpatdir.el ;
# mailto: tatel@infonegocio.com ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: xx ;
# End: ;

