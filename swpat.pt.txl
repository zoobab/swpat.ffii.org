<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentes de Software versus Democracia Parlamentar na União Europeia

#descr: Explicação da situação actual relativa aos monopólios de ideias
concedidos pelo Estado, especialmente no contexto da proposta de
directiva %(q:sobre a patenteabilidade de invenções implementadas em
computador) (directiva das patentes de software), que se tornou um
caso de estudo sobre a extensão da influência dos parlamentos na
legislação Europeia contemporânea.

#rWa: Situação actual

#eaW: Enquanto que o Parlamento Europeu %(ep:propôs a clara exclusão) das
%(si:patentes de software), a Comissão e o Conselho %(ci:ignoraram a
proposta do Parlamento) e, em Maio de 2004, %(ri:restabeleceram) um
%(pp:texto pró-patentes absolutamente radical), rejeitando qualquer
compromisso. Este texto %(cr:não tem o apoio da uma maioria
qualificada dos Estados-membros). Mesmo assim o Conselho recusou-se a
renegociar e está ainda a tentar forçar a adopção do texto.
Entretanto, o Parlamento Europeu %(er:pediu o reinício do processo). O
Conselho e a Comissão parecem %(pa:estar inclinados a ignorar o pedido
do Parlamento), fazendo uma interpretação anti-democrática das regras
de procedimento da União Europeia.

#odW: O Parlamento Europeu irá em Abril provavelmente começar a sua 
%(e:segunda leitura) da directiva com base numa %(q:posição comum) 
%(lq:legalmente questionável). Espera-se que o relator designado,  um
antigo primeiro ministro %(MR) Francês advogue a reafirmação da 
%(ep:posição do Parlamento de Setembro de 2003) (i.e. liberdade de 
publicação e de interoperacionalidade, restrição da patenteabilidade 
ao domínio físico). A votação irá provávelmente ser feita em Julho. 
Os requisitos de maioria são agora mais altos do que em 2003: os
membros  ausentes contam a favor do Conselho. Se Rocard tiver sucesso
em trazer  todas as emendas de 2003 para a segunda leitura, estará
numa posição  negocialmente forte durante o processo de
%(e:conciliação) subsequente.  No entanto, uma directiva desejável só
pode ser conseguida se o monopólio  dos oficiais de patentes
ministeriais (o grupo que governa o EPO) puder  ser enfraquecido.
Desta forma, as liberdades básicas da sociedade de  informação
passaram a estar fortemente ligadas à democratização da União 
Europeia.

#Idj: Um drama semelhante está simultaneamente a acontecer na  %(in:Índia).
A administração dos oficiais de patente usaram o pretexto da adopção
do %(tr:TRIPs) para com um acto administrativo inesperado,
%(na:declararem as patentes de software legalmente  válidas), medida
sujeita à aprovação do Parlamento na primeira metade de 2005. Os
oficiais Indianos revelaram ser alunos fieis dos seus congéneres
Europeus e as posições críticas por parte  dos media têm sido ainda
mais fracas do que na Europa.

#ott: Pequena Introdução

#tae: Últimas Patentes de Software Concedidas pelo EPO

#pra: Patentes Pendentes

#nnn: %(ua:Apelo Urgente) para reabertura da discussão no Conselho

#jt0: >%(NS) subscritores, >%(NC) CEOs

#oIt: Protejam a Inovação

#noW: testemunhos de empresas sobre patentes

#cse: introdução para as massas, campanha suportada pela %(LST)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpatdir.el ;
# mailto: ml1jdc@x64.com ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: pt ;
# multlin: t ;
# End: ;

