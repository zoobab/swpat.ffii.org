<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fake Limits on Patentability in the Council Proposal

#descr: We try to produce a short typology of the deceptive tricks used in the Council's proposal.

#ute: Die am häufigsten verwendete rhetorische List funktioniert wie folgt:

#inW: [A] ist nicht patentfähig, wenn nicht [Bedingung B] erfüllt ist.

#hnr: wobei sich bei näherem Hinsehen ergibt, dass B immer erfüllt ist.

#tBW: nr

#ifb: Die %(q:normale physische Interaktion zwischen einem Programm und dem Computer) bedeutet etwa so viel wie die %(q:normale physische Interaktion zwischen einem Rezept und dem Koch), nämlich nichts oder eben das, was man aus %(et:neueren Entscheidungen des EPA) entnehmen kann, in denen sie eingesetzt wurde.  Damals diente sie dazu, die Erteilung von Patenten auf geometrische Rechenregeln an IBM zu rechtfertigen.  Im vorliegenden Fall wurde laut EPA %(q:über die normale physische Interaktion ... hinaus) noch dadurch ein weiterer %(q:technischer Effekt) erzielt, dass auf einem Computerbildschirm Platz gespart wurde.  Das EPA %(ea:beurteilte) erläuterte wenig später, dass die Fiktion vom %(q:weiteren technischen Effekt) historisch bedingt war und nicht der Klärung sondern der Verwirrung diente:

#Chr: Die Ratsarbeitsgrupe lehnt im übrigen den %(4b:Artikel 4B) des Parlaments ab, der den EPA-Formeln eine begrenzendere Bedeutung gegeben hätte, gestützt auf eine neuere %(bp:deutsche Gerichtsentscheidung), derzufolge die Einsparung von Rechenressourcen keinen %(q:technischen Beitrag) darstellen kann, da andernfalls alle computer-implementierten Geschäftsmethoden patentfähig würden.  Hierdurch wird klar, dass die Ratsarbeitsgruppe im Einklang mit der neuesten EPA-Praxis %(q:computer-implementierte) Algorithmen und Geschäftsmethoden patentierbar machen möchte.

#nia: Die anscheinend einschränkende Bedingung ist immer erfüllt, s. obige Analyse.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsText0406 ;
# txtlang: de ;
# multlin: t ;
# End: ;

