<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Council's Fake Limits on Patentability

#descr: We try to produce a short typology of the deceptive tricks used in the Council's proposal.

#ute: The most frequently used rhetorical trick of the Council paper works as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always met.

#tBW: nr

#ifb: Die %(q:normale physische Interaktion zwischen einem Programm und dem Computer) bedeutet etwa so viel wie die %(q:normale physische Interaktion zwischen einem Rezept und dem Koch), nämlich nichts oder eben das, was man aus %(et:neueren Entscheidungen des EPA) entnehmen kann, in denen sie eingesetzt wurde.  Damals diente sie dazu, die Erteilung von Patenten auf geometrische Rechenregeln an IBM zu rechtfertigen.  Im vorliegenden Fall wurde laut EPA %(q:über die normale physische Interaktion ... hinaus) noch dadurch ein weiterer %(q:technischer Effekt) erzielt, dass auf einem Computerbildschirm Platz gespart wurde.  Das EPA %(ea:beurteilte) erläuterte wenig später, dass die Fiktion vom %(q:weiteren technischen Effekt) historisch bedingt war und nicht der Klärung sondern der Verwirrung diente:

#Chr: It should be noted that the Council Working group rejects the Parliament's %(4b:Art 4 B), which would have helped to attribute a more restrictive meaning to the EPO wording, based on a recent %(bp:german court decision) which held that economisation of computing ressources does not constitute a %(q:technical contribution), because otherwise practically all computer-implemented business methods would become patentable subject matter.  It is clear that the Council working group wants to make %(q:computer-implemented) algorithms and business methods patentable in accordance with recent EPO practise.

#nia: The apparent restricting condition is always fulfilled, see analysis above.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsText0406 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

