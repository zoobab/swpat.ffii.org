<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fake Limits on Patentability in the Council Proposal

#descr: We try to produce a short typology of the deceptive tricks used in the Council's proposal.

#ute: The most frequently used rhetorical trick of the Council paper works as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always met.

#tBW: nr

#ifb: The wording %(q:normal physical interaction between a program and the computer) means about as much as %(q:normal physical interaction between a recipe and the cook): nothing.  It is a magic formula whose usage can be inferred only from %(et:recent decisions of the EPO), in which it served to justify the granting of patents on geometrical calculation rules to IBM.  In the present case, according to the EPO, the %(q:further technical effect beyond ...) consisted in the economisation of space on a computer screen.  Two years later, the EPO itself %(ea:pointed out) that this construction is confusing but was needed for political purposes:

#Chr: It should be noted that the Council Working group rejects the Parliament's %(4b:Art 4 B), which would have helped to attribute a more restrictive meaning to the EPO wording, based on a recent %(bp:german court decision) which held that economisation of computing ressources does not constitute a %(q:technical contribution), because otherwise practically all computer-implemented business methods would become patentable subject matter.  It is clear that the Council working group wants to make %(q:computer-implemented) algorithms and business methods patentable in accordance with recent EPO practise.

#nia: The apparent restricting condition is always fulfilled, see analysis above.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsText0406 ;
# txtlang: en ;
# multlin: t ;
# End: ;

