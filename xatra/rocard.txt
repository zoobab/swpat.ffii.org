Sender: phm@averH8.a2e.de
To: mrocard@europarl.eu.int
Cc: gibuskro@ffii.org 
Subject: votre communiqué de presse de hier
From: PILCH Hartmut <phm@a2e.de>
Date: 08 Mar 2005 22:39:51 +0100
Message-ID: <87650195qw.fsf@averH8.a2e.de>
Lines: 53
User-Agent: Gnus/5.09 (Gnus v5.9.0) Emacs/21.3
--text follows this line--
Bonjour Mme Delers,

En lisant le dernier texte de M. Rocard, républié sur

        http://wiki.ffii.org/Rocard050308Fr

j'y trouve queque malentendus sur lesquels je voudrais tirer vôtre attention

(1) Il n'y a pas de "logiciels qui utilisent les force de la nature".
    Il faut distinguer entre logiciel et procédé.  Le logiciel est une
    construction abstraite et ne doit jamais être breveter.  Mais le
    procédé décrit par un logiciel peut être une "solution qui
    implique les forces de la nature".
    
(2) Dans la session du Conséil de hier, il n'y avait pas de vote.  Le
    Conséil a adopté sa position en point A, sans majorité qualifié.
    Le communiqué de presse du Conséil ne dit pas la vérité.  Nous
    voudrions au moin savoir comment le Conséil lui même explique,
    dans les termes des règles, ce qui s'est passé, car nous ne
    pouvons l'expliquer pas autrement que comme une violation des
    règles du Conseil.  C'est vrai que c'est plutôt une "affaire
    interne du Conseil", mais quand les états membres sont ignoré par
    le conseil et il n'y en fait a pas de contrôle de la coté des
    parlements nationaux, qui peut-on appeler?

    Pour pouvez trouvez la transcription de la session de hier avec
    nos explication sur

        http://wiki.ffii.org/Cons050307Fr
        http://wiki.ffii.org/Cons050307En

(3) Concernant la Constitution, je ne sais pas si par cela vous faites
    référence au texte de Jonas Maebe

        Open Letter to the EU: Constitution, we have a Problem 
        http://wiki.ffii.org/OpenLtr050307En

    Je croix que ce texte repond déja a vôtre critique et vaut bien
    d'être lu, pas autant a cause de ce qu'il dit sur la Constitution,
    quand comme un sommaire des leçons reçus pendant les derniers
    18 mois, avec le climax choquant de hier.

Excusez que je m'exprime rapidement, 

-- 
Hartmut Pilch, FFII,             Munich +498912789608,  Bruxelles +3227396260
Le processement de données n'est pas un champ technique http://swpat.ffii.org/  
350000 votes 3000 PDGs contre le brevet logiciel        http://noepatents.org/


    
    
 
