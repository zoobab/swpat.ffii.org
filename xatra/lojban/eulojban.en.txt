<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The EU patent organisations should help fund the development of free software for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information multilingual and maximally accessible to the public and the enterprises.
title: EU Patents in Logical Language!
Elu: Declaration
Urc: signatories
Wru: We want to support the lobbying effort against patentability of software, with a special emphasis on the following points:
Aek: Abstract ideas and programming techniques should remain non-patentable.  The Logical Language has in the past been harrassed by claims of of ownership of ideas, and it would be a severe threat to the further development of open-source Logical Language parsing tools, if the new programming ideas, which inevitably occur during the development, could be patented.
Pfe: Patent specifications should belong to the public domain.  They should be published in an open hypertext format of high abstraction power and placed under an open content license that allows anybody to rearrange and republish them under similar conditions.  This would create an open market for patent research software and services and thereby greatly reduce patent application costs.
ErW: EU citiziens should remain entitled to be informed in their own language about what they are allowed to do and what not.  Therefore patent specifications should be published in the languages of the states where they are to be enforcable.
NnW: Beside the national languages, it should be possible to submit patent specifications in a logical language like %(lb:Lojban).  A patent description in Logical Language could serve as a syntactically unambiguous reference version (for disputes arising from ambiguity) and as a source text for error-free machine-translation into all EU languages.  Also it would facilitate patent research, since a Logical Language can be seen as the final layer of hypertext markup that makes even the meaning of the text %(pa:parsable).
Tat: The %(lp:Lojban Reference Parser) works quite similar to the SGML reference parser
Did: The EU should actively support the development of open-source software in areas that are essential for the public informational infrastructure.  The current tender practice of financing only projects that lead to proprietary products should be changed.
Dcu: The EU patent organisations should help fund the development of open-source software ressources for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information maximally accessible to the public and the enterprises.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: eulojban ;
# txtlang: en ;
# End: ;

