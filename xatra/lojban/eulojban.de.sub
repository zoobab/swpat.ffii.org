\begin{subdocument}{eulojban}{Euro-Patente in Logiksprache!}{http://swpat.ffii.org/briefe/lojban/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die europ\"{a}ischen Patentorganisationen sollten helfen, die Entwicklung von freier Software zur Analyse und automatischen \"{U}bersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise k\"{o}nnen sie letztendlich Patentbesitzern sehr viel Geld und \"{A}rger sparen und gleichzeitig der \"{O}ffentlichkeit und den Unternehemen einen sprachunabh\"{a}ngigen Zugriff auf Patentinformationen erschlie{\ss}en.}
\begin{sect}{dekl}{Erkl\"{a}rung}
Wir wollen die Lobbyarbeit gegen Patentierbarkeit von Software unterst\"{u}tzen und dabei folgende Punkte besonders betonen:

\begin{enumerate}
\item
Abstrakte Ideen und Programmiertechniken sollten unpatentierbar bleiben.  Das Projekt einer Logiksprache hat in der Vergangenheit unter den Besitzanspr\"{u}chen seines Gr\"{u}nders an seinen Ideen gelitten, und es w\"{u}rde die k\"{u}nftige Entwicklung von quellenoffenen Analysewerkzeugen f\"{u}r logiksprachliche Texte schwer bedrohen, wenn neue Programmierideen, die w\"{a}hrend der Entwicklung zwangsl\"{a}ufig auftreten, patentiert werden k\"{o}nnten.

\item
Patentspezifikationen geh\"{o}ren der \"{O}ffentlichkeit.  Sie sollten in einem Hypertextformat mit hoher Abstraktionskraft ver\"{o}ffentlicht und unter eine Inhaltsoffenheit gew\"{a}hrleistende Lizenz gestellt werden.  Das sch\"{u}fe einen offenen Markt f\"{u}r Patentrecherche-Software und -Dienstleistungen und w\"{u}rde die Patentanmeldungskosten deutlich senken.

\item
EU-B\"{u}rger sollten weiterhin berechtigt sein, in ihrer eigenen Sprache dar\"{u}ber informiert zu werden, was sie tun d\"{u}rfen und was nicht.  Daher sollten Patentbeschreibungen in den Sprachen der Staaten, in denen sie durchsetzbar sein sollen, ver\"{o}ffentlicht werden.

\item
Neben den nationalen Sprachen sollte es m\"{o}glich sein, Patentbeschreibungen in einer Logiksprache wie Lojban\footnote{http://www.lojban.org} einzureichen.  Eine logiksprachliche Patentbeschreibung k\"{o}nnte als syntaktisch eindeutige Referenzversion (f\"{u}r aus der Zweideutigkeit nat\"{u}rlicher Sprachen resultierende Streitigkeiten) und als ein Quelltext zur fehlerfreien maschinellen \"{U}bersetzung in alle EU-Sprachen dienen.  Sie w\"{u}rde auch Patentrecherchen erleichtern, denn eine Logiksprache ist quasi eine letzte Ebene der Hypertextauszeichnung, die auch noch den Sinn des Textes der maschinellen Analyse erschlie{\ss}t.

\item
Die EU sollte die Entwicklung von quellenoffener Software in Bereichen, die f\"{u}r die \"{o}ffentliche informationelle Infrastruktur von Interesse sind, aktiv unterst\"{u}tzen.  Die derzeitige Ausschreibungspraxis, bei der nur solche Projekte, die zu propriet\"{a}ren Produkten f\"{u}hren, finanziert werden, sollte ge\"{a}ndert werden.

\item
Die europ\"{a}ischen Patentorganisationen sollten helfen, die Entwicklung von quellenoffenen Software-Ressourcen zur Analyse und automatischen \"{U}bersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise k\"{o}nnen sie letztendlich Patentbesitzern sehr viel Geld und \"{A}rger sparen und gleichzeitig der \"{O}ffentlichkeit und den Unternehemen den bestm\"{o}glichen Zugang zu Patentinformationen erschlie{\ss}en.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Unterzeichner}
\begin{enumerate}
\item
Hartmut PILCH\footnote{http://www.ffii.org/~phm/index.de.html}

\item
Robin TURNER\footnote{mailto:robin@bilkent.edu.tr?subject=http://swpat.ffii.org/briefe/lojban/index.de.html}

\item
Arnt Richard JOHANSEN\footnote{http://people.fix.no/arj/}

\item
Karl MUSIOL\footnote{mailto:KarlMusiol@aol.com?subject=http://swpat.ffii.org/briefe/lojban/index.de.html}

\item
Karl MOSLER\footnote{mailto:a100424@smail.Uni-Koeln.DE?subject=http://swpat.ffii.org/briefe/lojban/index.de.html}

\item
Helmut POPPENBORG\footnote{mailto:helborg@muenster.de?subject=http://swpat.ffii.org/briefe/lojban/index.de.html}
\end{enumerate}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

