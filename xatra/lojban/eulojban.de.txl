<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Euro-Patente in Logiksprache!

#descr: Die europäischen Patentorganisationen sollten helfen, die Entwicklung von freier Software zur Analyse und automatischen Übersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise können sie letztendlich Patentbesitzern sehr viel Geld und Ärger sparen und gleichzeitig der Öffentlichkeit und den Unternehemen einen sprachunabhängigen Zugriff auf Patentinformationen erschließen.

#Elu: Erklärung

#Urc: Unterzeichner

#Wru: Wir wollen die Lobbyarbeit gegen Patentierbarkeit von Software unterstützen und dabei folgende Punkte besonders betonen:

#Aek: Abstrakte Ideen und Programmiertechniken sollten unpatentierbar bleiben.  Das Projekt einer Logiksprache hat in der Vergangenheit unter den Besitzansprüchen seines Gründers an seinen Ideen gelitten, und es würde die künftige Entwicklung von quellenoffenen Analysewerkzeugen für logiksprachliche Texte schwer bedrohen, wenn neue Programmierideen, die während der Entwicklung zwangsläufig auftreten, patentiert werden könnten.

#Pfe: Patentspezifikationen gehören der Öffentlichkeit.  Sie sollten in einem Hypertextformat mit hoher Abstraktionskraft veröffentlicht und unter eine Inhaltsoffenheit gewährleistende Lizenz gestellt werden.  Das schüfe einen offenen Markt für Patentrecherche-Software und -Dienstleistungen und würde die Patentanmeldungskosten deutlich senken.

#ErW: EU-Bürger sollten weiterhin berechtigt sein, in ihrer eigenen Sprache darüber informiert zu werden, was sie tun dürfen und was nicht.  Daher sollten Patentbeschreibungen in den Sprachen der Staaten, in denen sie durchsetzbar sein sollen, veröffentlicht werden.

#NnW: Neben den nationalen Sprachen sollte es möglich sein, Patentbeschreibungen in einer Logiksprache wie %(lb:Lojban) einzureichen.  Eine logiksprachliche Patentbeschreibung könnte als syntaktisch eindeutige Referenzversion (für aus der Zweideutigkeit natürlicher Sprachen resultierende Streitigkeiten) und als ein Quelltext zur fehlerfreien maschinellen Übersetzung in alle EU-Sprachen dienen.  Sie würde auch Patentrecherchen erleichtern, denn eine Logiksprache ist quasi eine letzte Ebene der Hypertextauszeichnung, die auch noch den Sinn des Textes der maschinellen Analyse erschließt.

#Did: Die EU sollte die Entwicklung von quellenoffener Software in Bereichen, die für die öffentliche informationelle Infrastruktur von Interesse sind, aktiv unterstützen.  Die derzeitige Ausschreibungspraxis, bei der nur solche Projekte, die zu proprietären Produkten führen, finanziert werden, sollte geändert werden.

#Dcu: Die europäischen Patentorganisationen sollten helfen, die Entwicklung von quellenoffenen Software-Ressourcen zur Analyse und automatischen Übersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise können sie letztendlich Patentbesitzern sehr viel Geld und Ärger sparen und gleichzeitig der Öffentlichkeit und den Unternehemen den bestmöglichen Zugang zu Patentinformationen erschließen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eulojban ;
# txtlang: de ;
# multlin: t ;
# End: ;

