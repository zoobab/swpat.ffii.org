\begin{subdocument}{eulojban}{EU Patents in Logical Language!}{http://swpat.ffii.org/letters/lojban/index.en.html}{Workgroup\\swpatag@ffii.org}{The EU patent organisations should help fund the development of free software for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information multilingual and maximally accessible to the public and the enterprises.}
\begin{sect}{dekl}{Declaration}
We want to support the lobbying effort against patentability of software, with a special emphasis on the following points:

\begin{enumerate}
\item
Abstract ideas and programming techniques should remain non-patentable.  The Logical Language has in the past been harrassed by claims of of ownership of ideas, and it would be a severe threat to the further development of open-source Logical Language parsing tools, if the new programming ideas, which inevitably occur during the development, could be patented.

\item
Patent specifications should belong to the public domain.  They should be published in an open hypertext format of high abstraction power and placed under an open content license that allows anybody to rearrange and republish them under similar conditions.  This would create an open market for patent research software and services and thereby greatly reduce patent application costs.

\item
EU citiziens should remain entitled to be informed in their own language about what they are allowed to do and what not.  Therefore patent specifications should be published in the languages of the states where they are to be enforcable.

\item
Beside the national languages, it should be possible to submit patent specifications in a logical language like Lojban\footnote{http://www.lojban.org}.  A patent description in Logical Language could serve as a syntactically unambiguous reference version (for disputes arising from ambiguity) and as a source text for error-free machine-translation into all EU languages.  Also it would facilitate patent research, since a Logical Language can be seen as the final layer of hypertext markup that makes even the meaning of the text parsable\footnote{The Lojban Reference Parser\footnote{http://www.animal.helsinki.fi/lojban/parser.html} works quite similar to the SGML reference parser}.

\item
The EU should actively support the development of open-source software in areas that are essential for the public informational infrastructure.  The current tender practice of financing only projects that lead to proprietary products should be changed.

\item
The EU patent organisations should help fund the development of open-source software ressources for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information maximally accessible to the public and the enterprises.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{signatories}
\begin{enumerate}
\item
Hartmut PILCH\footnote{http://www.ffii.org/~phm/index.en.html}

\item
Robin TURNER\footnote{mailto:robin@bilkent.edu.tr?subject=http://swpat.ffii.org/letters/lojban/index.en.html}

\item
Arnt Richard JOHANSEN\footnote{http://people.fix.no/arj/}

\item
Karl MUSIOL\footnote{mailto:KarlMusiol@aol.com?subject=http://swpat.ffii.org/letters/lojban/index.en.html}

\item
Karl MOSLER\footnote{mailto:a100424@smail.Uni-Koeln.DE?subject=http://swpat.ffii.org/letters/lojban/index.en.html}

\item
Helmut POPPENBORG\footnote{mailto:helborg@muenster.de?subject=http://swpat.ffii.org/letters/lojban/index.en.html}
\end{enumerate}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
% mode: latex ;
% End: ;

