<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#cWn: %(nl:Softwarepatent-Arbeitsgruppe im:Förderverein für eine Freie Informationelle Infrastruktur e.V.)

#MnW: Mit freundlichen Grüssen,

#Vrl: Vielleicht mag dieser kurze Anriss des überaus komplexen Themenbereiches %(q:Softwarepatente) Sie anregen, die Problematik Ihrer Leserschaft in einem ausführlicheren Hintergrundbericht etwas näherzubringen und das Problembewusstsein zu schärfen - egal, wie die jeweilige Sichtweise aussehen mag, an der Thematik kommt heutzutage kein Firmenchef oder IT-Entscheider mehr vorbei.

#AsW: Aufgrund der einleuchtenden Argumente haben sich jedoch auch Großfirmen wie Oracle und Adobe und mittelständische Software-Innovatoren wie Infomatec, Phaidros, Netpresenter, Intradat u.v.m. bereits deutlich gegen die Patentierbarkeit von Software ausgesprochen (vergleiche http://swpat.ffii.org/vreji/prina/).

#Otu: Opensource-Software ist besonders gefährdet.  Das Patentsystem erzwingt geradezu die Geheimhaltung der Quelltexte vor potentiellen Abmahnern sowie die generalstabsmässige Organisation des Programmierens und den genau kontrollierten Verkauf von Einzellizenzen. Auch flexible Vertriebsformen wie Shareware werden benachteiligt.

#Ios: Insbesondere für kleinere und mittlere Unternehmen (KMU) stellen Softwarepatente denn auch eher eine grosse Hürde und ein Innovationshemmnis als eine Hilfe dar - von Neugründern ganz zu schweigen.

#DWC6: Gegenwärtig sind bereits über 25000 Unterschriften von oftmals leitenden oder selbstständigen IT-Fachleuten zusammengekommen, darunter auch über 300 Firmenchefs (siehe detaillierte Auswertung unter %{URL}

#DWC5: Die Diskussion in der IT-Branche ist allerdings bereits ein wenig weiter gediehen: unter %{PET} wurde eine Petition gestartet, deren Ziel es ist, die Ausdehnung von Patenten auf den Software-Bereich zu verhindern, wobei eindringlich vor möglichen Gefahren gewarnt wird. Ein weiteres Thema ist die fast schon an Desinformation grenzende Informationspolitik der zuständigen Behörden -- Presseinfo finden Sie unter %{WARN}.

#Lse: Leider befasst sich nur ein kurzer Artikel auf Seite 2 mit der von der EU-Kommission angestrebten Neufassung des Patentrechtes - und gerade die Auswirkungen auf die Quellenoffenheit und den freien Austausch von Programm-Informationen werden hierin nicht thematisiert.

#iad: in Ihrer Ausgabe Nr. 30 vom 27. Juli 2000 würdigen Sie unter anderem das Thema der globalen Wissensökonomie (Okinawa-Konferenz) und melden, dass Sun %(q:Open Source) benutzen will, um seine Server zu vermarkten.

#ShR: Sehr geehrte CZ-Redaktion,

#descr: Diesen Brief sandten wir nach Diskussion in der %(AG) am 31. Juli 2000 an die %(CZ).  Der erste Entwurf stammte von %(AK).

#title: Computer Zeitung 2000-07-27: SWPAT unterbelichtet

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxkcz27 ;
# txtlang: de ;
# multlin: t ;
# End: ;

