<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Beschwerde an das ZDF wegen Heute-Journal-Beitrag über
%(q:Raubkopierer und Ideenklauer)

#descr: In dem beanstandeten Film-Beitrag von Manfred Ahlers vom 6. Juli 2004
wird der FFII e.V. durch Verfälschung und Weglassen wesentlicher
Tatsachen sowie durch Einsatz von Suggestivmethoden wahrheitswidrig
als Interessenvertretung von Softwarepiraten und Raubkopierern
diskreditiert. Inhalt und Form des Beitrages verstoßen dabei gegen die
im ZDF-Staatsvertrag festgeschriebenen Programmgrundsätze und gegen
die Programmrichtlinien des ZDF.

#50i: 55100 Mainz

#isn: Zweites Deutsches Fernsehen

#loa: Richtlinien- und Koordinierungsausschuß

#sFi: An den Fernsehratsvorsitzenden des ZDF Richtlinien- und
Koordinierungsausschuß

#b5t: Programmbeschwerde gemäß § 15 Absätze 1,2  ZDF-Staatsvertrag

#tuB: Hiermit erheben wir gegen den Filmbeitrag von Herrn Dr. Manfred Ahlers
über das Symposium %(q:Innovation und Geistiges Eigentum in
Deutschland) in der ZDF-Sendung %(q:Heute Journal) vom Dienstag, den
6. Juli 2004 Beschwerde wegen Verletzung von Programmgrundsätzen.

#WWb: Eine Zusendung in Papierform per Post folgt.  Hier senden wir Ihnen
die wesentlichen Inhalte vorab zu Ihrer Information.

#rot: In dem beanstandeten Beitrag wird der FFII e.V. durch Verfälschung und
Weglassen wesentlicher Tatsachen sowie durch Einsatz von
Suggestivmethoden wahrheitswidrig als Interessenvertretung von
Urheberrechtsverletzern diskreditiert. Inhalt und Form des Beitrages
verstoßen dabei gegen die im ZDF-Staatsvertrag festgeschriebenen
Programmgrundsätze und gegen die Programmrichtlinien des ZDF.

#WMs: Der FFII e.V. ist ein gemeinnütziger Verein mit über 70.000
Unterstützern, darunter über 1000 Unternehmen, die den FFII mit der
Wahrnehmung ihrer Interessen im Bereich der Software-Eigentumsrechte
beauftragt haben.  Dabei agiert der FFII ferner regelmäßig im Konzert
mit dem Bundesverband der Mittelständischen Wirtschaft und dessen
europäischem Dachverband CEA-PME, der 500.000 Mitgliedsunternehmen
vertritt.  Auch im Vorfeld der Veranstaltung hatten diese beide
Verbände zusammen mit dem ebenfalls KMU repräsentierenden Patentverein
einen Offenen Brief an den Kanzler geschrieben, der in der Presse
Beachtung fand.

#ztx: Die genannten Verbände wenden sich derzeit im Interesse ihrer
Mitglieder und Unterstützer gegen den Versuch von Teilen der
EU-Kommission, durch die Legalisierung von Patenten auf Software das
Urheberrecht zu entwerten und damit nach unserer Auffassung die
Existenzgrundlage für eine selbständige Europäische
Softwareentwicklung zu zerstören. Der Vorschlag der Europäischen
Kommission 2002/0047 für eine %(q:Richtlinie über die Patentierbarkeit
computerimplementierter Erfindungen), kurz
%(q:Softwarepatent-Richtlinie) genannt, ist derzeit Gegenstand einer
heftigen, parteiübergreifenden Auseinandersetzung.

#eWa: Das Europäische Parlament hat sich bei der Abstimmung am 24. September
2003 der Position des FFII angeschlossen und mit zahlreichen
Änderungen des Kommissionsentwurfes die Patentierung von Leistungen
auf dem Gebiet der automatischen Datenverarbeitung wirksam
ausgeschlossen.  Solche Leistungen unterliegen nach traditioneller
Rechtsauffassung, wie sie auch in der Software-Richtlinie von 1991 zum
Ausdruck kommt, allein dem Urheberrecht.

#pae: Der Vorsitz des EU-Ministerrates hat in einem als
%(q:Kompromisspapier) titulierten Entwurf die Änderungen des
Europäischen Parlaments wieder gestrichen und darüberhinaus noch eine
direkte Beanspruchung von Programmtexten vorgeschlagen, wodurch die
Urheber solcher Programme ihrer Eigentumsrechte faktisch beraubt
werden.  Diesem Papier ist im Ministerrat eine qualifizierte Mehrheit
verschafft worden.

#aiW: Das Niederländische Parlament hat daraufhin mit Beschluss vom 1. Juli
2004 die Regierung der Niederlande angewiesen, ihre - vorgeblich
irrtümliche - Zustimmung zu dem Ratsbeschluss zurückzuziehen und die
Linie des Europaparlaments zu unterstützen.

#Wee: Beim Deutschen Bundestag ist inzwischen ein Entschließungsantrag der
FDP-Fraktion vom 27. Mai 2004 anhängig, mit dem die Bundesregierung
ebenfalls aufgefordert wird, ihre bisherige Unterstützung für
Softwarepatente aufzugeben und die Position des Europaparlaments zu
übernehmen.

#dWd: Die Position des FFII und der FDP wird dabei auch von vielen
Abgeordneten der Regierungskoalition unterstützt, die sich aber bisher
nicht gegen das im EU-Rat Bundesjustizministerium durchsetzen konnten,
s. dazu den überaus deutlichen Brief von Jörg Tauss MdB vom 2. Juni
2004.

#nlc: Die Fronten verlaufen bisher bei allen Abstimmungen quer durch die
Parteien.

#tns: Mit dem beanstandeten Beitrag wird der FFII als Interessenvertretung
von Softwarepiraten und Raubkopierern diskreditiert. Dabei wird der
tatsächliche Umfang der aktuellen politischen Auseinandersetzung um
die von der EU-Kommission propagierte Softwarepatentrichtlinie in
einer Weise ignoriert, die mit den Grundsätzen journalistischer Arbeit
nicht mehr vereinbar ist. Zusätzlich werden die hinter der
Auseinandersetzung stehenden wirtschaftlichen Interessen durch falsche
Angaben zur Person eines zentralen Interviewpartners verschleiert.

#rhW: Der Beitrag berichtet zunächst ausführlich über Musik, Film- und
Softwarepiraterie.  Das hierin genannte Anliegen, dem Urheberrecht
Geltung zu verschaffen, ist auch ein zentrales Anliegen des FFII
(%(q:den Schöpfer vor dem Plagiator und die Öffentlichkeit vor
Monopolen schützen), s. Titelseite http://www.ffii.org/) und darüber
hinaus weithin konsensfähig.  Ein Großteil dieses Abschnitts in Ahlers
Film besteht aus Aussagen von Georg Herrenleben, der als Vertreter
einer %(q:Agentur für Softwareindustrie) untertitelt wird.

#Wtn: Tatsächlich ist Herrenleben Regionalmanager der %(BSA) für
Mitteleuropa. Die BSA ist ein Zusammenschluss von großen, überwiegend
US-amerikanischen Softwareherstellern, die in Fachkreisen allgemein
als das Sprachrohr der Microsoft Corporation wahrgenommen wird.  Die
ursprüngliche Fassung der vorgeschlagenen Softwarepatentrichtlinie
stammt zumindest teilweise aus der Feder der BSA, die auch sonst aktiv
Lobbyarbeit für Softwarepatente im Sinne des Ahlersschen Filmbeitrags
betreibt und dabei regelmäßig (so auch in der von BSA verfassten
Kommissionslage) Urheberrecht und Patentrecht durcheinanderwirft. 
Durch die falsche Untertitelung verschleiert Ahlers, welche
Interessengruppen möglicherweise hinter seinem Filmstreifen stehen.

#hnM: Anschließend wird mit Aussagen von Bundesjustizministerin Zypries und
Siemens-Chef Heinrich von Pierer die Behauptung der Patentbranche
(mitsamt einigen Anhängseln in Großndustrie und Politik)
wiedergegeben, Software in technischen Zusammenhängen könne nicht
wirkungsvoll geschützt werden, weil sie nicht patentierbar sei. Pierer
fordert Patentschutz für Software.  Diese Behauptung ist in der
Softwarebranche alles andere als konsensfähig.  Breite Monopole, wie
das Patentrecht sie ermöglicht, widersprechen der Ethik und den
Gepflogenheiten der Branche.

#Ird: Darauf wird die Webseite des FFII mit der gut lesbaren Überschrift
%(q:Förderverein für eine Freie Informationelle Infrastruktur)
bildschirmfüllend eingeblendet, während dazu der folgende
Kommentartext gesprochen wird:

#ean: Doch es gibt eine starke Gegenbewegung. Sie fordert eine frei
zugängliche Software für alle. Jeder soll Computerprogramme unbegrenzt
weiterentwickeln können. Der Kanzler hält dagegen.

#tcI: Irgendwelche anderen Organisationen oder Personen, die solche
Forderungen (angeblich) stellen, werden in dem Beitrag nicht genannt. 
Mit der %(q:Gegenbewegung) ist folglich eindeutig und für den
Zuschauer erkennbar der FFII gemeint. Dem FFII wird damit unterstellt,
er fordere frei zugängliche Software für alle und das Recht, sich
fremde Computerprogramme anzueignen und weiterzuentwickeln.  Die
innerhalb der Softwarebranche vorherrschende Abneigung gegen das
Patentsystem wird allein dem FFII zugeschrieben, und diesem werden
illegitime, der üblichen Ethik der Branche widersprechende Positionen
unterstellt.

#wtR: Anschließend wird Bundeskanzler Schröder mit folgendem Auszug aus
seiner Rede eingeblendet:

#EWu: Ohne den nachhaltigen Schutz des Geistigen Eigentums wären die
Unternehmen zum einen gezwungen, ihre Erfindungen möglichst lange
geheim zu halten wenn sie erfolgreich sein wollten, zum anderen
müssten sie aus ökonomischen Gründen ihre Forschungs- und
Entwicklungstätigkeit auf wenige Gebiete konzentrieren.

#Wuc: Hieraus und aus der Einleitung %(q:Der Kanzler hält dagegen) ergibt
sich unmissverständlich die Behauptung, der FFII wende sich gegen den
vom Bundeskanzler geforderten nachhaltigen Schutz des Geistigen
Eigentums.

#nte: Danach endet der Beitrag mit dem eingesprochenen Kommentar:

#erW: Für viele ist die Freiheit im Computer wohl grenzenlos, doch die hat
ihren Preis: Der Schaden durch Raubkopien und geklaute Ideen geht
bereits in die Milliarden.

#ute: Dadurch wird der FFII mit den Raubkopierern, über die zuvor
ausführlich berichtet wurde, auf eine Stufe gestellt. In der Tat sind
die beiden Fragen sachlich unverbunden, der Autor stellt eine
kriminalisierende Verbindung her.

#uss: Nach § 5 Absatz 1 Satz 1 ZDF-Staatsvertrag und Abschnitt I
Unterabschnitt (3) der ZDF-Programmgrundsätze soll in den Sendungen
des ZDF den Fernsehteilnehmern in Deutschland ein objektiver Überblick
über das Weltgeschehen, insbesondere ein umfassendes Bild der
deutschen Wirklichkeit vermittelt werden.

#Wee2: Nach § 6 Absatz 1 Satz 1 desselben Staatsvertrages soll die
Berichterstattung umfassend, wahrheitsgetreu und sachlich sein.

#Wdi: Abschnitt I Unterabschnitt (4) der ZDF-Programmrichtlinien bestimmt:

#DiW: Die Informationssendungen müssen durch Darstellung der wesentlichen
Materialien der eigenen Meinungsbildung dienen. Sie dürfen dabei nicht
durch Weglassen wichtiger Tatsachen, durch Verfälschung oder durch
Suggestivmethoden die persönliche Entscheidung zu bestimmen versuchen.

#nWr: Der beanstandete Beitrag verletzt diese Grundsätze.

#hWW: Das %(q:Heute Journal) ist eine Informationssendung im Sinne der
Programmrichtlinien. Der beanstandete Beitrag verstößt gegen die für
Informationssendungen geltenden Richtlinien. Er versucht, durch
Weglassen wesentlicher Tatsachen, durch Verfälschung und durch
Suggestivmethoden die persönliche Entscheidung des Zuschauers in
unredlicher Weise zum Nachteil des FFII und der Softwarepatent-Gegner
zu bestimmen. Er ist nicht geeignet, dem Zuschauer das in den
Programmgrundsätzen geforderte objektive Bild der Wirklichkeit zu
vermitteln.

#eDn: Der Beitrag verschweigt vollständig, dass der FFII und seine
Verbündeten unbestreitbar einen erheblichen Teil der Europäischen
Wirtschaft repräsentieren und erhebliche politische Unterstützung quer
durch alle Parteien gefunden haben. Das Weglassen dieser für die
Meinungsbildung objektiv wesentlichen Tatsache ist geeignet, die
persönliche Entscheidung des Zuschauers zu Lasten des FFII zu
bestimmen, und ist deshalb nicht mit Abschnitt I Unterabschnitt 4 der
ZDF-Programmrichtlinien vereinbar.

#riW: Der Beitrag täuscht durch Weglassen dieser Information weiter vor,
dass es eine einhellige Forderung nach mehr Patentschutz für Software
in Wirtschaft und Politik gebe, der sich nur der angeblich den
Softwarepiraten nahestehende FFII entgegensetze. Damit verfälscht der
Bericht Tatsachen.

#ioe: Allgemein ist zu bemängeln, dass zu diesem Thema mit Siemens-Chef
Heinrich von Pierer und Bundesjustizministerin Zypries ausschließlich
Vertreter der Softwarepatent-Befürworter zu Wort kamen, obwohl auf der
Veranstaltung auch Vertreter des FFII und dem FFII nahestehende
Vertreter der Wirtschaft anwesend waren und immer wieder von den
Hauptrednern in den Diskurs einbezogen wurden.

#rIh: Der Beitrag verfälscht weiter die Positionen des FFII in einer Weise,
die geeignet ist, den FFII in eine Reihe mit Urheberrechtsverletzern
zu stellen und den Zuschauer damit gegen den FFII einzunehmen.

#ruh: Der FFII hat sich tatsächlich zu keiner Zeit in irgendeiner Weise
direkt oder indirekt für die Interessen von Raubkopierern oder
Softwarepiraten engagiert, wie dies in dem Beitrag durch die nicht
sachgemäße Verbindung von Softwarepiraterie und Patentdiskussion sowie
durch den zusammenfassenden Schlusssatz suggeriert wird.  Diese in dem
Beitrag enthaltene Aussage beanstanden wir sowohl wegen ihrer
sachlichen Unrichtigkeit als auch wegen der unter Verletzung der
Programmrichtlinien zu Ihrer Vermittlung eingesetzten
Suggestivtechnik.

#fel: Der FFII hat zu keinem Zeitpunkt die Auffassung vertreten, es müsse
eine frei zugängliche Software für alle oder gar ein Recht, fremde
Software weiterzuentwickeln, geben. Diese Forderungen, die dem FFII in
dem Beitrag unterstellt werden, sind von Herrn Ahlers frei erfunden
worden.

#miB: Der FFII hat sich niemals gegen einen wirksamen Schutz für Geistiges
Eigentum ausgesprochen. Der im Beitrag zitierten Forderung des
Bundeskanzlers nach wirksamem Schutz des Geistigen Eigentums wird ein
falscher Kontext unterlegt, indem behauptet wird, sie richte sich
gegen die Position des FFII. Tatsächlich kann der FFII der zitierten
Äußerung des Bundeskanzlers ohne weiteres zustimmen. In Herrn Ahlers
Beitrag wird sowohl der Bedeutungsgehalt der Kanzlerforderung als auch
die Position des FFII in unzulässiger Weise verfälscht.

#san: Die beschriebene Darstellung des FFII ist mit dem Grundsatz einer
wahrheitsgetreuen und sachlichen Berichterstattung in keiner Weise
vereinbar.

#gmn: Bei den beanstandeten Inhalten des Beitrages handelt es sich
durchgängig um nachweislich falsche Tatsachenbehauptungen hinsichtlich
der vom FFII vertretenen Positionen, der von ihm erfahrenen
politischen Unterstützung und des Umfangs und der Beteiligten der
aktuellen politischen Diskussion.

#Bgr: Die Fehler in dem Beitrag sind auch nicht durch den notwendigerweise
begrenzten Umfang journalistischer Recherche entschuldbar.

#WWe: Herr Ahlers hätte hinreichend Zeit gehabt, sich besser über die
Sachlage und die tatsächlichen Ziele des FFII zu informieren. Die im
Bild aufgeführte Website des FFII wurde nicht am fraglichen Tag
aufgenommen. Es bestand also genug Zeit für eine sorgfältige
Recherche. Vertreter des FFII waren auf der Veranstaltung erkennbar
anwesend und hätten Herrn Ahlers jederzeit für weitere Auskünfte zur
Verfügung gestanden. Der FFII versendet regelmäßig Pressemitteilungen
und hat dies auch aus Anlass der fraglichen Veranstaltung getan. Über
die zugrundeliegende Auseinandersetzung wird nicht nur in der
Fachpresse, sondern beispielsweise auch im %(q:Spiegel Online) bereits
seit langem ausführlich berichtet.

#ten: Herr Ahlers hat auf persönliche Beschwerden einzelner
Vereinsmitglieder bisher keine Einsicht gezeigt.

#tWr: Er bestreitet entgegen den Tatsachen, dass er den FFII in eine Reihe
mit Raubkopierern und Kriminellen gestellt hat.

#WWw: Er hat sich weiter mit der Behauptung verteidigt, auf der
Veranstaltung sei ausschließlich von Vertretern der Wirtschaft der
unzureichende Patentschutz für Geistiges Eigentum beklagt worden.
Wörtlich: %(q:Mein Beitrag hat dieseeinhellig vertretenen Positionen
dargestellt. Davon abweichende Meinungen wurden auf dieser
Veranstaltung nicht vertreten.

#dWd2: Diese Behauptung ist unzutreffend. Die Veranstaltung war von
demonstrierenden Vertretern kleiner und mittlerer Unternehmen umgeben.
An der Veranstaltung selbst haben nicht weniger als vier Vertreter des
FFII teilgenommen, zwei von ihnen mit eigenen Redebeiträgen, darunter
der Unterzeichner. Sämtliche Hauptredner gingen in Ihren Beiträgen auf
die %(q:Sorgen) der vom FFII vertretenen Interessengruppen ein. Die
Vertreter des FFII hätten Herrn Ahlers im Übrigen auch jederzeit für
weitere Auskünfte zur Verfügung gestanden.

#Wds: Herrn Ahlers Beitrag kann nur noch als Desinformation bezeichnet
werden.  Solches Verhalten ist mit seinem Beruf und dem Anspruch
seines Senders unvereinbar.  Herr Ahlers hätte in der Lage sein
müssen, dies zumindest im nachhinein zu erkennen.

#hkn: Angesichts der bisherigen Reaktion von Herrn Ahlers sehen wir keine
Möglichkeit zu einer gütlichen Beilegung der Angelegenheit mehr.

#tnw: Nebenbei möchten ich auch anmerken, dass bei der neuesten
Berichterstattung über die Münchener Linux-Umstellung sich das ZDF
irreführende und falsche Aussagen der Bundesregierung über ihr Wirken
im EU-Rat und über dessen Position zu eigen macht.  Es wird im
wesentlichen genau die unwahre Behauptung über einen angeblichen
%(q:Kompromiss) des Rates mit dem Parlament aufgestellt, um deretwegen
sich die niederländische Regierung eine Rüge seitens ihres Parlamentes
eingehandelt hat.

#htW: Auch wenn so extreme Tiefpunkte wie der Ahlers-Bericht eher selten
anzutreffen sind, besteht durchaus eine generelle Gefahr, dass
angesichts der starken Interessengegensätze bei einer für viele
Beobachter schwer durchschaubaren Materie die journalistische Ethik
unter die Räder geraten könnte.

#cte: Wir möchten Sie daher bitten, der Herausforderung, welche das Thema
Softwarepatente für die Standards Ihres Hauses darstellt, durch
geeignete Anstrengungen zu begegnen.  Hierzu könnten aus unserer Sicht
sowohl Disziplinarmaßnahmen gegenüber Herrn Ahlers als auch eine
besonders sorgfältige und intensive Begleitung des Themas in kommenden
Programmen und auf Ihren Webseiten gehören.

#fhc: Mit freundlicher Hochachtung,

#FWV: FFII e.V.

#thz: Hartmut Pilch, Vorsitzender

#nae: Anlagen:

#htW2: Mitschrift des Beitrages vom 6. Juli 2004

#eWw: Schreiben von Herrn Dr. Ahlers an Beschwerdeführer

#oes: Offener Brief von BVMW, FFII und Patentverein an den Bundeskanzler vom
6.7.2004 anlässlich des Symposiums

#WmW: unter %(q:Presse) mit Datum vom 6.7.2004

#mW1: Pressemitteilung BVMW/CEA-PME vom 14.5.2004

#utr: unter

#ino: Offener Brief des SPD-Medienexperten Jörg Tauss an Zypries vom 2. Juni
2004

#isn2: Parteitagsbeschluss der Münchner SPD

#DAr: FDP-Antrag

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: zdf0407 ;
# txtlang: de ;
# multlin: t ;
# End: ;

