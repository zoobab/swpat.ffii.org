\begin{subdocument}{ConsText0406}{Fake Limits on Patentability in the Council Proposal}{http://swpat.ffii.org/letters/ConsText0406/ConsText0406.en.html}{Workgroup\\swpatag@ffii.org\\english version 2004/06/03 by FFII\footnote{http://lists.ffii.org/mailman/listinfo/traduk}}{We try to produce a short typology of the deceptive tricks used in the Council's proposal.}
The most frequently used rhetorical trick of the Council paper works as follows

\begin{quote}
{\it [A] is not patentable, unless [condition B] is met.}
\end{quote}

where, upon close scrutiny, it turns out that condition B is always met.

\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
nr & The Text & remark\\\hline
Article 4A &  & The wording ``normal physical interaction between a program and the computer'' means about as much as ``normal physical interaction between a recipe and the cook'': nothing.  It is a magic formula whose usage can be inferred only from recent decisions of the EPO\footnote{http://swpat.ffii.org/papers/epo-t971173/epo-t971173.en.html}, in which it served to justify the granting of patents on geometrical calculation rules to IBM.  In the present case, according to the EPO, the ``further technical effect beyond ...'' consisted in the economisation of space on a computer screen.  Two years later, the EPO itself pointed out\footnote{http://swpat.ffii.org/papers/epo-tws-app6/epo-tws-app6.en.html} that this construction is confusing but was needed for political purposes:

\begin{quote}
{\it This scheme makes no mention of the ``further technical effect'' discussed in T1173/97. There is no need to consider this concept in examination, and it is preferred not to do so for the following reasons: firstly, it is confusing to both examiners and applicants; secondly, the only apparent reason for distinguishing ``technical effect'' from ``further technical effect'' in the decision was because of the presence of ``programs for computers'' in the list of exclusions under Article 52(2) EPC. If, as is to be anticipated, this element is dropped from the list by the Diplomatic Conference, there will no longer be any basis for such a distinction. It is to be inferred that the BoA would have preferred to be able to say that no computer-implemented invention is excluded from patentability by the provisions of Articles 52(2) and (3) EPC.}
\end{quote}

It should be noted that the Council Working group rejects the Parliament's Art 4 B\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/plen0309.en.html\#art4c}, which would have helped to attribute a more restrictive meaning to the EPO wording, based on a recent german court decision\footnote{http://swpat.ffii.org/papers/bpatg17-suche02/bpatg17-suche02.de.html} which held that economisation of computing ressources does not constitute a ``technical contribution'', because otherwise practically all computer-implemented business methods would become patentable subject matter.  It is clear that the Council working group wants to make ``computer-implemented'' algorithms and business methods patentable in accordance with recent EPO practise.

see also http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/plen0309.en.html\#art4a\\\hline
Article 5(2) &  & The apparent restricting condition is always fulfilled, see analysis above.\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/Cons0406Text.el ;
% mode: latex ;
% End: ;

