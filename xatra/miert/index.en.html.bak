<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Hartmut PILCH">
<link href="http://phm.ffii.org/phm.en" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/01/06">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="phm@a2e.de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Software Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="Brief an den Wettbewerbskommissar">
<meta name="description" content="Wir haben diesen Brief, den mittlerweile an die 10000 Menschen unterschrieben haben, an Herrn Van Miert weitergeleitet und werden auch seinen Nachfolger sowie weitere Kommissionsmitglieder und zust&auml;ndige Stellen davon unterrichten.">
<title>Brief an den Wettbewerbskommissar</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>Brief an den Wettbewerbskommissar<br>
<!--#include virtual="../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#alv">Dear Sir!</a></li>
<li><a href="#mfg">Sincerely yours</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="alv">Dear Sir!</a></h2>
</div>

<div class="sectbody">
Many times during recent months and years, you have publicly expressed your concern about monopoly dangers in the software area.  This month, at the Berlin kartell conference, you mentioned Microsoft as an example and correctly stated that it is not the size of the company but the dominance of communication interfaces which is dangerous.  You reassured us that your establishment is keeping a close eye on this danger.<p>Unfortunately there seem to be some colleagues in the EU who do not share your danger consciousness.  They are currently planning measures which can only result in aggravating the anti-competitive tendencies of the software industry and its heavy bias in favor of very few, mainly American, software giants.<p>On June 24th a legislative proposal is to be presented at the Intellectual Property Conference in Paris, according to which a US-like &quot;software patent&quot; system is to be introduced in Europe.<p>To appreciate this danger, we must first understand why the software industry has been plagued for decades by especially gross anticompetitive behaviour.  The reason, we believe, lies in its heavy use of protective measures, which, unlike in the traditional manufacturing and publishing industries, are all used simultaneously and cumulatively:<p><ol><li>Authorship Rights and utilization restrictions derived therefrom (e.g. restrictions on copying and modification)</li>
<li>Trade Secret (Withholding of source code, use of opaque data formats etc)</li>
<li>Platform Strategy (hard-linking of diverse systems together into unified and mutually exclusive platforms, limitation of interoperability, barring the way for independent manufacturers to compete on the basis of technical merit)</li></ol><p>If now patents on programming ideas are introduced as an additional protective measure, this can only aggravate the anticompetitive situation.<p>During recent months, Microsoft and other companies have secured themselves patent rights to Internet standards and, in doing so, <a href="http://www.w3.org/1999/05/P3P-PatentPressRelease.html">brought the Geneva World Wide Web Consortium W3C to the brink of disruption</a>.<p>So far the EU countries have strictly refused to grant patent protection for software, as far as pure information works and not parts of industrial machinery are concerned.  This is because, unlike with machines, chemicals and other objects of traditional patent protection, a computer program is not an industrial product but a description of ideas and instructions in a formalized language, similar to a scientific thesis.  Only in a second step can one transform the pure information work into a virtual industry product by compiling it into an opaque machine code and removing its potential for development.<p>The wisdom of the European approach to accord no patent rights for programming methods has, during recent years, been shown by the increasing significance of <em>open-source software</em>.  Systems like <a href="http://www.gnu.org">GNU/Linux</a>,<a href="http://www.freebsd.org">FreeBSD</a> etc have grown out of the unrestricted communication of specialists in the Internet and have, by their computing power and stability, outperformed many well-known industrial products.  A new post-industrial mode of software development has shown its superior productivity and has become an integral part of public life.<p>It is not surprising that some large industrial software producers of the USA have in recent years become concerned about this new tidal wave, in which Europe occupies a share of about 50%.<p>Thus in an <a href="http://www.opensource.org/halloween.html">internal study of October 1998</a>, a Microsoft strategist states that his company can hardly compete with systems like Linux and Apache at the quality level, since the latter &quot;scale much better&quot; in the Internet.  Therefore he advises his company to employ two well-proven remedies specific to the industrial mode of production:<p><ol><li>proprietary extension/reinvention of Internet protocols</li>
<li>extensive acquisition of software patents</li></ol><p>Software patents are subject to ardent controversy even in the USA.  Unlike industrial patents, they hardly serve as a source of information about innovations, but rather as an uncalculable liability for software programmers, who by the nature of their profession have to innovate every day.  Dedicated technological pioneering enterprises like <a href="http://lpf.ai.mit.edu/Patents/testimony/statements/adobe.testimony.html">Adobe</a> and <a href="http://lpf.ai.mit.edu/Patents/testimony/statements/oracle.statement.html">Oracle</a> have expressed their view that software patents are more harmful than useful for innovation.  Only some giant corporations with an all-encompassing platform strategy are in a position to really profit from the general legal insecurity caused by software patents.<p>A sudden strengthening of such platform strategies by European legislators would subject the European software culture with its thousands of small, innovative enterprises, to crushing pressures of technologically stagnant but legally well-equipped American corporate giants and thereby destroy more than an EU competition commissioner can achieve for Europe during many years of attentive competition protection work.<p>In view of the urgence of this matter we would be very grateful for an early response.  We would especially be interested to know what exactly the EU is planning, with whom we may seek contact and to whom we could relate as a dialog partner, e.g. for our <a href="http://kwiki.ffii.org/KongressEn">http://kwiki.ffii.org/KongressEn</a> planned for June 13th in Cologne.<p>At this conference we will be dealing with two more threats to the European competition order:<p><ol><li><a href="http://www.ffii.org/index.en.html">the planned coorperation of the German Land of Northrhine-Westfalia with Microsoft</a></li>
<li><a href="http://www.heise.de/newsticker/data/cp-12.05.99-004/">the planned cooperation/fusion between the successor of the German telekommunication state monopoly company with Microsoft</a></li></ol><p>In both cases representatives of more or less public functions are promoting a monopoly system that affects all of Europe.<p>Such regional anti-competitive measures are however harmless in comparison to the devastation that a pan-European software patent system is likely to cause.<p>We wish you good health and good inspiration for the great challenges of your office.
</div>

<div class="secthead">
<h2><a name="mfg">Sincerely yours</a></h2>
</div>

<div class="sectbody">
<a href="/xatra/miert/msub/index.en.html">list of signatories</a><p>P.S. For more detailed reading on this subject, we recommend
<ul><li><a href="http://www.freepatents.org">http://www.freepatents.org</a></li>
<li><a href="http://swpat.ffii.org">http://swpat.ffii.org</a></li></ul>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/xatra/miert/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/01/06 (1999/05/25)
<a href="http://phm.ffii.org/phm.en">Hartmut PILCH</a><br>
english version 2004/08/16 by <a href="http://www.a2e.de">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el -->
<!-- mode: html -->
<!-- End: -->

