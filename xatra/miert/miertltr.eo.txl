<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SrW: Kara sinjoro Van Miert

#Mue: Kun koraj salutoj

#Ser: Plurfoje en la lastaj monatoj vi esprimis vian zorgon pri la
monopoldanøeroj en la programara sektoro.  Ankoraý sur la Berlina
kartelkonferenco de komenco de Majo vi nomcitis la ekzemplon de
Mikrosofto kaj trafe eksplikis, ke danøeron kaýzas ne tiom la grandeco
de la enterpreno kiom øia dominado de komunikaj interfacoj.  Vi nin
certigis ke via oficejo tre atente observas tiun æi danåron.

#Ltt: Sed bedaýrinde en la EU þajnas ekzisti kolegoj kiuj ne kunhavas vian
danøer-konsciecon.  Oni planas tiuæitempe leødecidojn kiuj
konsekvencos al multobligo de la jam nun por Eýropo tre malavantaøaj
monopoltendencoj en la programara sektoro.

#Aie: Je 1999-06-24 sur konferenco pri Intelekta Poseda¼aro en Parizo oni
prezentos leøproponon, kiu enkondukos en Eýropon
%(q:programarpatentan) sistemon lau usona modelo.

#IWd: Kial tio estas danøera?  Oni unue pripensu, kial jam dum multaj
jardekoj en la programara sektoro regas precipe akraj formoj de
konkurenc-malhelpo.  Laý ni la æefa kaýzo estas, ke la
programarfirmoj, malsame kun tradiciaj publikigaj aý industriaj
enterprenoj, uzas por si jam tre multajn protektilojn samtempe kaj
kumule, ekzemple:

#Uae: verkintrajtoj kaj de tiuj derivataj uzlimigoj

#zbw: ekzemple malpermeso de kopiado aý pludisvolvigo

#Beh: enterprena sekreto

#Vxs: kaþo de fontoteksto, uzo de maltransparentaj datformatoj ktp

#Poa: platformstrategio

#Ftu: durkunigado de malsamaj sistemoj en unuecan platformon, limigo de
interoperebleco kaj sekve la ebleco de sendependaj produktantoj
konkurenci surbaze de teknika boneco.

#Wmk: Se nun al tio aliøas ankoraý programadkonceptaj patentoj kiel kroma
protektilo, tio povas nur pliakrigi la konkurencmalhelpan situacion.

#Ien: En la lastaj monatoj Mikrosofto kaj aliaj firmaoj akiris patentrajtojn
pri interretaj normoj kaj tiuvoje %(wc:subdanøerigis la pluekziston de
la Øeneva retnormigejon W3C).  Bonþance tiu patentprotektado øis nun
ne validas por Eýropo.

#Bto: Øis nun la EU-landoj strikte malakceptis patentprotektadon por
programaroj, tiugrade ke traktiøas pri puraj informverkoj kaj ne pri
partoj de industriaj aparatoj.  Malsame kun maþinoj, ¶emia¼oj kaj
aliaj objektoj de klasika patentprotektado, komputila programo unue
nur estas formlingva priskribo de ideoj kaj agad-instrukcioj.  Nur per
dua paþo oni povas redukti la informverkon al industriprodukto, igante
øin maltransparenta kaj malpli disvolvigebla.

#Dln: La saøeco de la eýropa farmaniero de ne doni patentojn por
programadmetodoj montriøis dum la lastaj jaroj per la altiøanta
signifo de %(e:malkaþfontaj programaroj).  Sistemoj kiel %{GF} estiøis
graý la libera komunikado de fakuloj en Interreto kaj per sia
komputforteco kaj stabileco enombrigis multajn agnoskitajn industriajn
programarojn.  Nova post-industria modelo de programardisvolvigo
montris sian superan produktemon kaj jam etabliøis en multaj domajnoj
de nia publika vivo.

#Een: Kompreneble kelkaj grandaj usonaj industriaj programarproduktantoj
zorgas pri tiu nova disvolviøo, en kiu Eýropo okupas porcion de 50%.

#Inh: En %(os:interna priseræriporto de 1998-10a fino) ja konstatas
mikrosofta strategisto, ke sia firmao malfacilas kvalite konkurenci
kun malkaþfontaj sistemoj kiel Linux kaj Apache, æar tiuj æi
%(q:skaliøas multe pli bone) en Interreto.  Tial la verkinto konsultas
ke sia firmao uzu du batalilojn el la armilaro de la tradicia
industria produktadfasono:

#PuW: posedeca eltendo/reinvento de interretaj protokoloj

#FWS: grandskala akiro de programarpatentoj

#cea: programaraj patentoj ankaý en Usono okazigas samopiniecon. Malsame kun
industripatentoj ili apenaý helpas la disvastigadon de novaj teknikoj
kaj kontraýe minacas la en tiu profesio tute ne eviteblan æiutagan
plinovigon per damoklesa gladio.  Specialigitaj pionirenterprenoj kiel
%{AO} dirintas ke %(q:programarpatentoj pli malhelpas ol helpas
plinovigon).  Nur kelkaj enterprenegoj kun æionampleksanta
platformstrategio povas klare profiti per la jura necerteco kaýzita
per programadmetodaj patentoj.

#Eoa: Tuja plifortigo de usonaj platformstrategioj per EUaj leødonistoj
probable neniogos plion ol æio kion povas aæevi EUa
konkurenskomisiisto por Eýropio dum longaj jaroj de atenta
konkurencprotektado.

#IId: Konsiderante la urøecon de tiuæi afero, ni tre dankemus por baldaýa
respondo.  Speciale ni volus baldaý lerni, kion ekzakte planas la EU
kaj kun kiu ni povus ankoraý kontaktiøi kaj kiun ni povus obteni kial
dialogulo, ekzemple por la %{KG} okazonta je 1999-06-13 en Kolonjo.

#AwW: Sur tiu kongreso pritraktatos ankoraý du aliaj danøeroj al eýropa
konkurencordo:

#dnW: la planata kunlaboro de la þtato Nordrajn-Vestfalio kun Mikrosofto

#don: la planata kuniøo de la Germana Telekomunikejo kun Mikrosofto

#Iie: En la du kazoj plenumantoj de pli aý malpli publikaj funkcioj provas
servi kiel vojgvidantoj de monopolo rilatanta al tuta Eýropio.

#Soe: Sed tiaj regionaj konkurenc-malhelpadoj estas maldanøeraj komparate
kun la katastrofo kiun povus okazigi tut-eýropa programarpatenta
sistemo.

#Wng: Ni aýguras al vi robustan sanecon kaj bonan inspiron por la grandaj
defioj de votra ofico.

#Uer: Subskribantaro

#Pem: P.S. Pli detalajn informojn pri tiu temo vi povos trovi sub

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertltr ;
# txtlang: eo ;
# multlin: t ;
# End: ;

