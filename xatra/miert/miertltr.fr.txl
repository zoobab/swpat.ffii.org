<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SrW: Monsieur,

#Mue: Sincères salutations

#Ser: Depuis ces derniers mois et dernières années, vous avez maintes fois
exprimé publiquement vos craintes concernant le danger de
monopolisation dans le domaine du logiciel. Encore au début de ce mois
de mai lors de la conférence des cartels de Berlin, vous avez
mentionné l`exemple de Microsoft et vous avez démontré justement que
ce n`est pas la taille de l`entreprise, mais la domination sur les
jonctions de communication qui présente un danger. Vous nous assuriez
que votre institution s`applique à observer attentivement ce danger.

#Ltt: Hélas, il semble qu`au sein de l`UE certains collègues ne partagent
pas votre conscience du danger. La commission de l`UE prévoit en ce
moment des mesures qui risquent d`aggraver encore plus les tendances
de monopolisation du logiciel déjà existantes et très désavantageuses
pour les Européens.

#Aie: Le 24 juin, une proposition de loi devrait être présentée lors de la
conférence sur la propriété intellectuelle à Paris introduisant ainsi
en Europe un système de %(q:brevets de logiciel) à l`américaine.

#IWd: Si depuis ces dernières décénnies on constate un durcissement accru de
la monopolisation, c`est parce que les firmes de logiciels
contrairement aux firmes traditionnelles de publication ou de firmes
industrielles, emploient beaucoup de mesures protectrices en même
temps comme par exemple:

#Uae: droits d4auteur et les restrictions d`utilisation en résultant

#zbw: par exemple interdiction de copier et de développement

#Beh: secret d`entreprise

#Vxs: retenue des sources, utilisation de formats de données insondables
etc.

#Poa: stratégie de plateforme

#Ftu: lier entre eux des systèmes de nature différente en une plateforme
commune, limiter l`interopérabilité et par là, limiter la possibilité
des fabricants indépendants d`entrer en concurrence grâce à leur
compétence technique

#Wmk: Si on ajoute à cela des brevets sur idées de programmation pour une
protection supplémentaire, on aggrave la situation de monopolisation.

#Ien: Durant ces derniers mois, Microsoft et d`autres firmes se sont
assurées des droits par brevet sur les standards d`Internet et de ce
fait %(wc:le consortium Genevois World-Wide-Web W3C a subi une épreuve
de force difficile). Heureusement, ces brevets ne sonts pas valable
pour l`Europe.

#Bto: Jusqu`à présent, les pays de l`UE avaient toujours strictement refusé
la protection par des brevets de logiciels dans la mesure où il s`agit
purement de matériel d`information et non de pièces d`appareils
industriels. À l`opposé des machines, produits chimiques et autres
objets tombant sous le droit de brevet, un logiciel n`est autre qu`une
oevre d'information, und déscription formelle d`idées et de modes
d`emploi comparable à un article scientifique. C`est seulement au
second degré que l`on peut quasiment réduire l'oevre informatique à un
produit industriel en le rendant impénétrable et en limitant son
potentiel de développement.

#Dln: La sagesse de l`approche européenne de ne pas accorder de brevets aux
méthodes de programmation s`est avérée ces dernières années
particulièrement par l`importance croissante des %(e:logiciels à
source ouverte). Des systèmes comme %{GF} se sont développés en raison
de la libre communication des experts dans l`Internet et se sont
montrés supérieur à beaucoup de produits industriels connus, grâce à
leur efficacité et leur stabilité. Un nouveau modèle post-industriel
du développement des logiciels a prouvé sa productivité supérieure et
s`est déjà ábli entre temps dans beaucoup de domaines de la vie
publique.

#Een: Pas étonnant que certains grands fabricants de logiciels aux
États-Unis se font des soucis face à ce nouveau développement auquel
l`Europe participe à environ 50%.

#Inh: Dans une %(os:analyse interne de fin octobre 1998) un stratège de
Microsoft constate que sa firme ne peut concurrencer, au niveau de la
qualité, avec les systèmes à source ouverte tel que Linux et Apache
puisque ces derniers %(q:s`ajustent bien mieux) dans l`Internet. Ainsi
cet expert conseille à sa firme d`utiliser deux armes de l`arsénal de
l`ancien mode de production industriel:

#PuW: Élargissement/réinvention propriétaire de protocoles Internet

#FWS: Application extensive de brevets de logiciel

#cea: Les brevets de logiciel font aussi l`objet de fortes controverses aux
États-Unis. Contrairement aux brevets industriels, ils ne servent
guère à faire connaître des innovations, mais menacent l`inévitable
innovation quotidienne comme une épée de Damoclès. Les entreprises
spécialisées pionnières comme %{AO} ont exprimé leur opinion que
%(q:les brevets de logiciel sont plutot étouffants que utils pour
l`innovation). Seulement certains géants de cette branche avec une
stratégie de plateformes générales sont en mesure de profiter de
l`insécurité juridique due aux %(q:brevets de logiciels).

#Eoa: Un renforcement de la stratégie de plateforme pratiquée par des
législateurs de l`UE détruirait probablement plus qu`un commissaire de
contrôle de la concurrence de l`UE peut obtenir pour l`Europe en
plusieurs années de travail consciencieux sur la protection de la
compétitivité.

#IId: Vu l`urgence de cette affaire, nous vous serions très reconnaissant de
nous répondre au plus vite. Nous aimerions surtout savoir ce que l`UE
prévoit, qui nous pourrions encore contacter et qui pourrait être
notre interlocuteur, notamment pour le %{KG} du 13 juin à Cologne.

#AwW: Lors de ce congrès, nous parlerons aussi de deux autres dangers pour
la compétitivité européenne:

#dnW: la coopération prévue entre le Land Nordrhein-Westfalen et Microsoft

#don: la fusion/coopération prévue entre Deutsche Telekom et Microsoft

#Iie: Dans les deux cas, des responsables de fonctions plus ou moins
publiques essaient ensemble d`ouvrir la route à une monopolisation
recouvrant toute l`Europe.

#Soe: Ces perturbations régionales de compétitivité sont bénignes comparées
aux dévastations que pourrait causer un système de brevets de logiciel
à l`échelle européenne.

#Wng: Nous vous souhaitons une santé de fer et une bonne inspiration pour le
grand défi de votre fonction.

#Uer: Liste des signataires

#Pem: P.S. Vous trouverez des informations supplémentaires à ce sujet sous

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: seyfertd ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertltr ;
# txtlang: fr ;
# multlin: t ;
# End: ;

