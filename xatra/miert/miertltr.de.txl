<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SrW: Sehr geehrter Herr van Miert!

#Mue: Mit freundlichen Grüßen

#Ser: Sie haben in den letzten Monaten und Jahren immer wieder Ihrer Sorge
über Monopolgefahren im Softwarebereich öffentlich Ausdruck verliehen.
 Noch auf der Berliner Kartellkonferenz von Anfang Mai nannten Sie das
Beispiel Microsoft und führten treffend aus, dass nicht so sehr die
Unternehmensgröße sondern die Herrschaft über
Kommunikationsschnittstellen eine Gefahr darstelle.  Sie versicherten
dabei, Ihre Behörde beobachte diese Gefahr sehr aufmerksam.

#Ltt: Leider scheint es aber in der EU Kollegen zu geben, die Ihr
Gefahrenbewusstsein nicht teilen.  Es werden derzeit Maßnahmen
geplant, die auf eine Potenzierung der schon jetzt für Europa sehr
nachteilhaften Monopoltendenzen in der Softwarebranche hinauslaufen.

#Aie: Am 24. Juni soll auf der Konferenz der Europäischen
Patentorganisationen in Paris ein Gesetzesentwurf vorgelegt werden,
der in Europa ein %(q:Softwarepatent)-System nach amerikanischem
Vorbild einführt.

#IWd: In der Softwarebranche herrschen besonders deshalb seit Jahrzehnten
besonders krasse Formen der Wettbewerbsverhinderung, weil
Softwarefirmen, anders als traditionelle Publikations- oder
Industrieunternehmen, besonders viele Schutzmittel auf einmal in
Anspruch nehmen, z.B.:

#Uae: Urheberrecht und daraus abgeleitete Verwendungsbeschränkungen

#zbw: z.B. Kopierverbot, Weiterentwicklungsverbot

#Beh: Betriebsgeheimnis

#Vxs: Vorenthalten des Quelltextes, Verwendung undurchsichtiger Datenformate
etc

#Poa: Plattformstrategie

#Ftu: Festverdrahtung verschiedenartiger Systeme untereinander zu
Einheitsplattformen, Beschränkung der Interoperabilität und damit der
Möglichkeit unabhängiger Hersteller, aufgrund technischer Leistungen
zu konkurrieren

#Wmk: Wenn sich nun hierzu auch noch Patente auf Programmierideen als
zusätzliches Schutzmittel gesellen, kann das die Kartellsituation nur
noch verschärfen.

#Ien: In den letzten Monaten haben Microsoft und andere Firmen sich
Patentrechte auf Internet-Standards gesichert und damit %(wc:das
Genfer World-Wide-Web-Konsortium W3C vor eine Zerreißprobe gestellt). 
Zum Glück gilt dieser Patentschutz bisher nicht für Europa.

#Bto: Bisher haben die EU-Länder den Patentschutz für Software strikt
abgelehnt, soweit es sich um reine Informationswerke und nicht um
Teile von industriellen Apparaten handelt.  Anders als Maschinen,
Chemikalien und sonstige Gegenstände des herkömmlichen Patentschutzes,
ist ein Computerprogramm nämlich zunächst, ähnlich wie ein
wissenschaftlicher Artikel, nicht ein industrielles Produkt, sondern
eine formalsprachliche Beschreibung von Ideen und
Handlungsanweisungen.  Erst in einem zweiten Schritt kann man das
reine Informationswerk zum Quasi-Industrieprodukt reduzieren, indem
man es undurchsichtig macht und sein Entwicklungspotential
einschränkt.

#Dln: Die Weisheit des europäischen Ansatzes, für Programmiermethoden keine
Patente zu vergeben, hat sich in den letzten Jahren besonders durch
die steigende Bedeutung der %(e:quellenoffenen Software) gezeigt. 
Systeme wie %{GF} sind aus der freien Kommunikation von Fachleuten im
Internet entstanden und haben durch ihre Leistungsfähigkeit und
Stabilität viele bekannte industrielle Erzeugnisse in den Schatten
gestellt.  Ein neues post-industrielles Modell der
Software-Entwicklung hat seine überlegene Produktivität bewiesen und
sich inzwischen schon in vielen Bereichen des öffentlichen Lebens fest
etabliert.

#Een: Es ist verständlich, dass einige große industrielle
Softwareproduzenten der USA sich angesichts dieser neuen Entwicklung,
an der Europa einen Anteil von ca 50% hat, Sorgen machen.

#Inh: In einer %(os:internen Studie von Ende Oktober 1998) konstatiert denn
auch ein Microsoft-Stratege, dass seine Firma rein auf der Ebene der
Qualität mit quellenoffenen Systemen wie Linux und Apache kaum
konkurrieren könne, da letztere im Internet %(q:unvergleichlich besser
skalieren).  Daher rät er seiner Firma zum Einsatz von zwei
Kampfmitteln aus dem Arsenal der industriellen Produktionsweise:

#PuW: Proprietäre Erweiterung/Neuerfindung von Internetprotokollen

#FWS: Flächendeckende Anmeldung von Softwarepatenten

#cea: %(q:Softwarepatente) sind auch in den USA Gegenstand heftiger
Kontroverse. Anders als Industriepatente dienen sie kaum der
Bekanntmachung von Neuerungen, bedrohen aber die im Softwarebereich
ohnehin unvermeidliche tägliche Innovationstätigkeit mit einem
Damoklesschwert.  Spezialisierte Pionierunternehmen wie %{AO} haben
Softwarepatente als %(q:eher schädlich denn nützlich für die
Innovation) bezeichnet.  Nur einige wenige Branchenriesen mit
allumfassender Plattformstrategie sind in der Lage, von der durch
Softwarepatente erzeugten allgemeinen Rechtsunsicherheit zu
profitieren.

#Eoa: Eine plötzliche Stärkung solcher Plattformstrategien durch europäische
Gesetzgeber würde die gewachsene, von kleiner Unternehmensgröße,
Vielfalt und Innovationsfreude gekennzeichnete europäische
Softwarekultur dem übermächtigen Druck technisch stagnierender aber
juristisch bestens ausgerüsteter amerikanischer Giganten aussetzen und
dadurch mehr zunichte machen, als ein EU-Wettbewerbskommissar in
vielen Jahren aufmerksamen Wettbewerbsschutzes für Europa erreichen
kann.

#IId: In Anbetracht der Dringlichkeit dieser Angelegenheit wären wir Ihnen
für eine baldige Antwort sehr dankbar.  Insbesondere würden wir gerne
erfahren, was die EU genau plant, mit wem wir noch in Kontakt treten
könnten und wen wir als Dialogpartner, nicht zuletzt für den am 13.
Juni in Köln stattfindenden %{KG}, gewinnen könnten.

#AwW: Auf diesem Kongress werden noch zwei weitere Gefährdungen der
europäische Wettbewerbsordnung zur Sprache kommen:

#dnW: die geplante Kooperation des Landes Nordrhein-Westfalen mit Microsoft

#don: die geplante Kooperation/Fusion der Deutschen Telekom mit Microsoft

#Iie: In beiden Fällen versuchen Träger von mehr oder weniger öffentlichen
Funktionen, sich als Wegbereiter eines ganz Europa betreffenden
Kartells zu verdingen.

#Soe: Solche regionalen Wettbewerbsstörungen sind aber noch harmlos im
Vergleich zu den Verheerungen, die ein EU-weites Software-Patentwesen
anrichten könnte.

#Wng: Wir wünschen Ihnen eine robuste Gesundheit und gute Inspiration für
die großen Herausforderungen Ihres Amtes.

#Uer: Unterzeichnerliste

#Pem: P.S. Weitere detaillierte Informationen zu diesem Thema finden Sie
unter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertltr ;
# txtlang: de ;
# multlin: t ;
# End: ;

