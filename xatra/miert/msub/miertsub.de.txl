<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Unterzeichner

#descr: Liste der Unterzeichner des %(km:Briefes an EU-Wettbewerbskommissar Karel van Miert)

#PaR: Vorbemerkungen

#Sos: Bisherige Unterzeichner

#Nio: Neuunterzeichner

#AWo: Alle Unterzeichner sprechen nur für sich selbst, nicht für ihre (z.T. erläuternd in Klammern angeführten) Arbeitgeber.

#Tun: Nach Paragraph 28 Abs. 3 Bundesdatenschutzgesetz untersagen wir der Nutzung oder Übermittlung dieser Unterzeichnerliste für Werbezwecke oder für die Markt- oder Meinungsforschung.  Die Internetadressen der Unterzeichner sollen ausschließlich Rückfragen an die betreffenden Personen erleichtern und so die Glaubwürdigkeit der Liste erhöhen.

#MlS: Dipl.Ing.

#Vtr: Verein für Internetbenutzer Österreichs

#cre: %{VIBE}, vertreten durch %{PK}

#pcu: Physikstudent

#UWc: Uni %{T}

#Wll: Wir haben jetzt ein %(sl:Unterzeichner-Gästebuch) eingerichtet, in das Sie sich %(sf:eintragen) können.

#YWr: Sie können uns auch einen Kommentar zu diesem Thema schicken, den wir gerne veröffentlichen oder referenzieren, wie z.B. die folgenden:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertsub ;
# txtlang: de ;
# multlin: t ;
# End: ;

