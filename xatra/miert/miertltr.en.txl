<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SrW: Dear Sir!

#Mue: Sincerely yours

#Ser: Many times during recent months and years, you have publicly expressed
your concern about monopoly dangers in the software area.  This month,
at the Berlin kartell conference, you mentioned Microsoft as an
example and correctly stated that it is not the size of the company
but the dominance of communication interfaces which is dangerous.  You
reassured us that your establishment is keeping a close eye on this
danger.

#Ltt: Unfortunately there seem to be some colleagues in the EU who do not
share your danger consciousness.  They are currently planning measures
which can only result in aggravating the anti-competitive tendencies
of the software industry and its heavy bias in favor of very few,
mainly American, software giants.

#Aie: On June 24th a legislative proposal is to be presented at the
Intellectual Property Conference in Paris, according to which a
US-like %(q:software patent) system is to be introduced in Europe.

#IWd: To appreciate this danger, we must first understand why the software
industry has been plagued for decades by especially gross
anticompetitive behaviour.  The reason, we believe, lies in its heavy
use of protective measures, which, unlike in the traditional
manufacturing and publishing industries, are all used simultaneously
and cumulatively:

#Uae: Authorship Rights and utilization restrictions derived therefrom

#zbw: e.g. restrictions on copying and modification

#Beh: Trade Secret

#Vxs: Withholding of source code, use of opaque data formats etc

#Poa: Platform Strategy

#Ftu: hard-linking of diverse systems together into unified and mutually
exclusive platforms, limitation of interoperability, barring the way
for independent manufacturers to compete on the basis of technical
merit

#Wmk: If now patents on programming ideas are introduced as an additional
protective measure, this can only aggravate the anticompetitive
situation.

#Ien: During recent months, Microsoft and other companies have secured
themselves patent rights to Internet standards and, in doing so,
%(wc:brought the Geneva World Wide Web Consortium W3C to the brink of
disruption).

#Bto: So far the EU countries have strictly refused to grant patent
protection for software, as far as pure information works and not
parts of industrial machinery are concerned.  This is because, unlike
with machines, chemicals and other objects of traditional patent
protection, a computer program is not an industrial product but a
description of ideas and instructions in a formalized language,
similar to a scientific thesis.  Only in a second step can one
transform the pure information work into a virtual industry product by
compiling it into an opaque machine code and removing its potential
for development.

#Dln: The wisdom of the European approach to accord no patent rights for
programming methods has, during recent years, been shown by the
increasing significance of %(e:open-source software).  Systems like
%{GF} have grown out of the unrestricted communication of specialists
in the Internet and have, by their computing power and stability,
outperformed many well-known industrial products.  A new
post-industrial mode of software development has shown its superior
productivity and has become an integral part of public life.

#Een: It is not surprising that some large industrial software producers of
the USA have in recent years become concerned about this new tidal
wave, in which Europe occupies a share of about 50%.

#Inh: Thus in an %(os:internal study of October 1998), a Microsoft
strategist states that his company can hardly compete with systems
like Linux and Apache at the quality level, since the latter %(q:scale
much better) in the Internet.  Therefore he advises his company to
employ two well-proven remedies specific to the industrial mode of
production:

#PuW: proprietary extension/reinvention of Internet protocols

#FWS: extensive acquisition of software patents

#cea: Software patents are subject to ardent controversy even in the USA. 
Unlike industrial patents, they hardly serve as a source of
information about innovations, but rather as an uncalculable liability
for software programmers, who by the nature of their profession have
to innovate every day.  Dedicated technological pioneering enterprises
like %{AO} have expressed their view that software patents are more
harmful than useful for innovation.  Only some giant corporations with
an all-encompassing platform strategy are in a position to really
profit from the general legal insecurity caused by software patents.

#Eoa: A sudden strengthening of such platform strategies by European
legislators would subject the European software culture with its
thousands of small, innovative enterprises, to crushing pressures of
technologically stagnant but legally well-equipped American corporate
giants and thereby destroy more than an EU competition commissioner
can achieve for Europe during many years of attentive competition
protection work.

#IId: In view of the urgence of this matter we would be very grateful for an
early response.  We would especially be interested to know what
exactly the EU is planning, with whom we may seek contact and to whom
we could relate as a dialog partner, e.g. for our %{KG} planned for
June 13th in Cologne.

#AwW: At this conference we will be dealing with two more threats to the
European competition order:

#dnW: the planned coorperation of the German Land of Northrhine-Westfalia
with Microsoft

#don: the planned cooperation/fusion between the successor of the German
telekommunication state monopoly company with Microsoft

#Iie: In both cases representatives of more or less public functions are
promoting a monopoly system that affects all of Europe.

#Soe: Such regional anti-competitive measures are however harmless in
comparison to the devastation that a pan-European software patent
system is likely to cause.

#Wng: We wish you good health and good inspiration for the great challenges
of your office.

#Uer: list of signatories

#Pem: P.S. For more detailed reading on this subject, we recommend

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertltr ;
# txtlang: en ;
# multlin: t ;
# End: ;

