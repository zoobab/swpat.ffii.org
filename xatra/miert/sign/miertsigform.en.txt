<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Bitte füllen Sie, soweit zutreffend, die verschiedenen Felder unten aus. Es dürfen Felder ausgelassen werden.  Durch genaue Angaben können Sie aber u.U. helfen, die Kampagne optimal zu organisieren.  Vielen Dank!
title: Signature Form
FeS: In case entries do not show up properly please %(mb:mail to %{HB})
Nam: Name
Fln: family name
Mtm: Doe
Pon: personal name
Eri: John
Rhf: sequence
FeW: given name first
Wep: West
FeW2: family name first
Byr: Bavaria
Ugr: Hungaria
Ots: East Asia
Gce: sex
wsa: m
Xqq: f
BWW: occupation
Pri: Programmer
Fmi: affiliation
Wsn: affiliation web address
Tlf: phone
tag: daytime
aed: evening
SrW: Email or snailmail address
PlW: personal webpage
Kmt: Comments
vce: venceremos!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: miertsigform ;
# txtlang: en ;
# End: ;

