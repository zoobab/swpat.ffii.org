<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#SrW: Egregio Signore,

#Mue: Distinti saluti

#Ser: Negli ultimi mesi ed anni, Lei ha più volte espresso in pubblico la
Sua preoccupazione riguardo ai pericoli di monopolizzazione nel campo
dei programmi per elaboratore. Questo mese, nel corso della conferenza
di Berlino, Lei ha citato l'esempio di Microsoft, mostrando
giustamente come non sia la dimensione dell'impresa a costituire un
pericolo, bensì il suo dominio sulle interfacce di comunicazione. Lei
ci ha assicurati che la Sua istituzione sta tenendo d'occhio il
problema.

#Ltt: Sfortunatamente, sembra che alcuni colleghi in seno all'UE non
condividano la Sua percezione del pericolo. La commissione dell'UE sta
pianificando delle misure che non possono che aggravare le tendenze
anti-competitive dell'industria del software, a solo vantaggio di
pochissime grandi compagnie americane.

#Aie: Il 24 giugno deve essere presentata, all Conferenza sulla Proprietà
Intellettuale di Parigi, una proposta legislativa che tende ad
introdurre anche in Europa un sistema di %(q:brevetti sul software)
all'americana.

#IWd: Per comprendere questo pericolo, dobbiamo innanzitutto capire perchè
l'industria del software è stata afflitta per decenni da comportamenti
gravemente anti-competitivi. A nostro parere, ciò è dovuto al suo
pesante uso di misure di protezione che, a differenza che nelle
industrie manifatturiere ed editoriali tradizionali, vengono usate
simultaneamente e cumulativamente:

#Uae: Diritti d'autore e conseguenti restrizioni all'utilizzo

#zbw: per esempio, restrizioni sulla copiatura e sulla modifica

#Beh: Segreto industriale

#Vxs: Rifiuto di rivelare il codice sorgente, uso di formati oscuri per i
dati, ecc.

#Poa: Strategia di piattaforma

#Ftu: connessione di sistemi diversi in una piattaforma comune, limitazione
dell'interoperabilità, con la conseguenza di impedire ai fabbricanti 
indipendenti di entrare in concorrenza con la loro competenza tecnica

#Wmk: Se ora vengono introdotti i brevetti sul software come ulteriore
misura protettiva, ciò non può che aggravare la situazione di
momopolio.

#Ien: Negli ultimi mesi, Microsoft ed altre compagnie si sono assicurate i
diritti di brevetto sugli standard Internet, ed in tal modo %(wc:hanno
portato il Consorzio per il World Wide Web W3C di Ginevra sull'orlo
della disgregazione).

#Bto: Finora, i Paesi dell'UE hanno sempre rifiutato la protezione del
software mediante brevetto, nella misura in cui sia coinvolto un puro
lavoro informatico e non parti di apparecchiature industriali. Ciò
perchè, a differenza delle macchine, dei prodotti chimici e di
quant'altro cade sotto la giurisdizione dei brevetti tradizionali, un
programma per elaboratore non è un prodotto  industriale, bensì la
descrizione di idee ed istruzioni in un linguaggio astratto, non
dissimile da una tesi scientifica. Solamente in un secondo stadio la
pura opera informatica può essere trasformata in prodotto industriale
compilandola in un codice oggetto opaco e rimuovendo così il suo
potenziale di sviluppo.

#Dln: La saggezza della scelta europea di non concedere brevetti per metodi
di programmazione è stata dimostrata dalla crescente importanza, negli
ultimi anni, del %(e:software open-source). Sistemi come %{GF} sono
nati e cresciuti in virtù della libera comunicazione degli esperti su
Internet, mostrandosi, quanto a potenza di calcolo e stabilità,
superiori a molti ben noti prodotti industriali. Un nuovo modo
post-industriale di sviluppo del software ha dimostrato la sua
maggiore produttività ed è entrato a far parte della vita pubblica.

#Een: Non c'è da stupirsi se negli ultimi anni alcuni grossi produttori
americani di software hanno cominciato a preoccuparsi di questa ondata
di produzione indipendente, in cui l'Europa occupa una quota intorno
al 50%.

#Inh: In uno %(os:studio confidenziale dell'ottobre 1998), uno stratega
della Microsoft asserisce che la sua compagnia non è in grado di
competere a livello qualitativo contro sistemi come Apache e Linux,
dato che questi ultimi %(q:vanno molto meglio) su Internet. Egli
consiglia perciò la sua compagnia di utilizzare due ben sperimentate
armi ereditate dal vecchio modo di produzione industriale:

#PuW: estensione proprietaria o reinvenzione dei protocolli Internet

#FWS: acquisizione di brevetti software su larga scala

#cea: I brevetti sul software sono oggetto di grandi discussioni anche negli
USA. A differenza dei brevetti tradizionali, essi servono non già a
far conoscere le innovazioni, ma a minacciare come una spada di
Damocle i programmatori, per la cui professione l'innovazione è
pratica quotidiana. Imprese come %{AO}, che hanno svolto un lavoro da
pionieri in questo campo, hanno espresso l'opinione che i brevetti sul
software siano più dannosi che utili per l'innovazione. Soltanto pochi
giganti del software con una strategia di piattaforma globale sono in
grado di trarre profitto dalla situazione di incertezza giuridica
causata dai brevetti sul software.

#Eoa: Un rafforzamento di tali strategie di piattaforma da parte dei
legislatori europei assoggetterebbe le migliaia di piccole imprese
innovative europee alla pressione dei giganti americani, stagnanti dal
punto di vista tecnologico ma ben armati da quello legale, e
distruggerebbe in poco tempo ben più di ciò che un commissario europeo
di controllo sulla concorrenza possa sperare di realizzare in anni di
lavoro coscienzioso.

#IId: Vista l'urgenza della materia, Le saremmo grati di una risposta
sollecita. Vorremmo in particolare sapere quali siano precisamente i
piani dell'UE, chi  possiamo contattare al riguardo e chi
potrebbeessere nostro interlocutore, per esempio riguardo al %{KG} del
13 giugno a Colonia.

#AwW: Questa conferenza tratterà pure di altre due minacce per la
competitività europea:

#dnW: la cooperazione prevista tra il Land tedesco di Nordrhein-Westfalen e
Microsoft

#don: la cooperazione/fusione prevista fra Deutsche Telekom e Microsoft

#Iie: In ambo i casi, rappresentanti di funzioni più o meno pubbliche stanno
promuovendo un sistema monopolistico che riguarda l'intera Europa.

#Soe: Tali iniziative monopolistiche regionali sono comunque un'inezia in
confronto alla devastazione che può arrecare un sistema pan-europeo di
brevetti sul software.

#Wng: Le auguriamo una buona salute ed una buona ispirazione per
fronteggiare la grande sfida che l'attende nel Suo lavoro.

#Uer: lista dei firmatari

#Pem: P.S. Per maggiori detagli sull'argomento, raccomandiamo di consultare
i seguenti siti:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: miertltr ;
# txtlang: it ;
# multlin: t ;
# End: ;

