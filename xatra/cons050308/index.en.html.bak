<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Jonas Maebe, Phm, Mvernon and Bhenrion">
<link href="http://lists.ffii.org/mailman/listinfo/traduk" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/03/18">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="jmaebe@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Logic Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="Letter to the Council: What happened on 7th March 2005?">
<meta name="description" content="FFII is asking questions to the Legal Service of the Council.  In its press release of this monday, the Luxembourg Council Presidency claims to have adopted a Common Position backed by a qualified majority, but it appears that this majority does not exist and that there were multiple violations of procedural rules.  The Council should at least explain how it interprets the events at monday's session in light of the procedural rules, e.g. who was the Council that denied the B-item request, whether and at what point the blocking opposition against the A-Item suddenly ceased to exist, etc.">
<title>Letter to the Council: What happened on 7th March 2005?</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>Letter to the Council: What happened on 7th March 2005?<br>
<!--#include virtual="../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
On Monday, 7 March 2005, the Council claimed to have reached a Common Position on the controversial software patents directive. Several national Parliaments had given orders to their governments to oppose the draft text that was on table. Additionally, many governments were not happy with that text, so they attached a unilateral declaration to the reached agreement. Some governments even asked to reopen discussions. Finally, from the publicly available audio and video data, it seems as if the vote on the software patents A item simply never took place.<p>We would therefore like to receive answers to the following questions:<p><ol><li>Was the agenda sent to the other members of the Council within 14 days before the meeting (article 3(1))?</li>
<li>In case the software patents A item was not put on the agenda at least 14 days in advance of the Council meeting, did not a single member state ask for it to be removed from the list of A items?</li>
<li>When did the presidency send a note regarding its inclusion on the agenda? When did other Council members receive it?</li>
<li>(a) Did Bulgaria and Romania receive the required 7 days to have a look on this important directive? (b) If, as we are told, this was not the case, what reasons of urgency caused the Council Presidency to shorten their period? (c) Did those countries agreed to shorten this period?</li>
<li>The European Affairs Committee of the Danish Parliament obliged the Danish minister to request a B item according to article 3(8). The Council Presidency in its initial speech mentions that Denmark, alongside with Poland and Portugal, indeed asked for a B-Item, but that their request was inadmissible.  Is it therefore correct to assume that Denmark requested a B-Item, as called for by its Parliament?</li>
<li>Does requesting a B item not automatically imply a request to remove the A item?</li>
<li>Article 3(8) of the Council's Rules of Procedure states that &quot;if a position on an A item might lead to further discussion thereof&quot;, the A item must be removed from the agenda. Is the fact that three member states wanted to reopen the discussions not an indication that certain positions &quot;might&quot; lead to further discussions? If not, can you give an example of something which would fulfill the conditions set out in this rule?</li>
<li>Only &quot;the Council&quot; can decide that a request to remove an A item or to turn it into a B items is inadmissible. Who was &quot;the Council&quot; in this case?</li>
<li>In case an actual vote is held about this, what kind of majority rules are used?</li>
<li>If this vote were initiated, what question would the Council members have been asked to vote on?</li>
<li>The Council Presidency referred to itself as &quot;the Council&quot; in relation to rejecting the Danish B item request. (a) Does the Presidency want to imply that the reference to &quot;the Council&quot; in Article 3 of the Rules of Procedure is a reference to the Presidency, i.e. that the Presidency alone can decline a B item requests? (b) If so, based on which rule?</li>
<li>With regard to reopening the discussions, the Presidency stated it could not reopen discussions because &quot;there are procedures which have to be respected&quot;. (a) Can you point out which procedures state that a political agreement cannot be discussed again? (b) If there is no such rule, how can this statement be justified?</li>
<li>(a) If the Presidency alone cannot deny a B item request, how did a majority of the Council vote against removing the item from the agenda? (b) Article 8(1)(b) states that the outcome of voting must be &quot;indicated by visual means&quot;, but no such indication was seen on the public video. Did we miss it, or was no vote called? (c) If no vote was called, how does this fit in the Council's Rules of Procedure?</li>
<li>At the end of the session, the Presidency asked whether everyone agreed on the &quot;other&quot; A items. Does this mean the software patents A item was already voted on before? If so, when did this happen?</li>
<li>Certain parts of the session were not public, e.g. the speech by Minister Brinkhorst. Why? In the recording of the English simultaneous translation, we can hear &quot;Switch me off!&quot; asked by a Council Member. Is it compliant with the Council's Rules of Procedure to switch off a microphone so that the public cannot hear what is said in a public deliberation?</li>
<li>Can the Common Position be sent to the European Parliament and be presented there before the minutes are signed?</li>
<li>What happens if a member state does not sign the minutes?</li>
<li>Belgium and France attached unilateral declarations to the text on which a political agreement was reached in May, which is substantially the same text as on the Monday agenda. Why were those declarations not mentioned on Monday, and why have those declarations not been sent to the European Parliament?</li>
<li>How many directives in the history of the Council had more unilateral statements attached to the formal approval of the Council in first reading than this one?</li>
<li>Minister Verwilghen declared in the Belgian Parliament that no country asked for a B item. (a) Can you explain why the Presidency mentions the request from those 3 countries in his speech if nobody asked? (b) If those countries have really asked the presidency in private or informally, does this not go against the principle that this was a &quot;public deliberation&quot;? (c) How much of the &quot;public deliberation&quot; was not public, and at which point does it become a non-public deliberation with a short public overview of the end result? (d) Does this not go against at least the spirit of the Council's Rules of Procedure?</li>
<li>The Rules of Procedure clearly state that a removal of an A item from the agenda, or changing it back into a B item, can be denied by &quot;the Council&quot;. This means that so long as the Council makes sure it does not incite motions from 4 Parliaments and declarations of concerns from 9 member states (7 last Monday, 2 in May 2004) regarding its political agreement text, there is little chance of a request like that coming up and actually surviving a vote. As such, how can the Council Presidency justify denying the B item request based on &quot;institutional reasons&quot; and for fear of creating a precedent which will potentially delay all future Council work?</li>
<li>Are full minutes made of the Council meeting? If so, in which formats (audio, steno, ...), and is it possible for the public to get access to that part of the minutes which corresponds to the public deliberations?</li>
<li>How can a delegate express his wish to speak in an urgent matter, even when it's not his turn, for example to react to a surprising developement in the course of proceedings where he wouldn't be asked again normally ? Are there conditions when a delegate can not speak even if he wants to do so? Can the Presidency or anyone else turn off a delegates microphone, or interrupt the recording and public broadcasting in such cases ? Under what conditions would a delegate be allowed to speak anyway to the Council in such cases ?</li></ol><p>Even if all interpretations of the Rules were done with the best intentions, possibly even in our interest without us understanding how, we hope you can see that this kind of creative rule interpretation does not encourage faith in the democratic functioning of the Council. It seems as if the whole Council meeting was just a rehearsed performance, that most of the &quot;public deliberations&quot; did not occur publicly at all and that even worse precedents have been set than those the Council was afraid of:<p><ul><li>The Council seems to put the speed with which legislation enters into force above quality, even though it recognises the enormous importance of this legislation.</li>
<li>The Council implied that a political agreement is always final due to &quot;institutional reasons&quot;. Nevertheless, national Parliaments can only review the final text subject of a political agreement after said agreement has been reached, since this text can still be changed at the meeting where the agreement is reached (as happened in May 2004). Thus they will never be able to decide whether they want to accept a Common Position adopted by the Council.</li></ul><p>We are sending all these procedural questions to you not because we dislike the text which came out of the Council's first reading. We are sad because of the democratic deficit surrounding the decision. We would much rather have seen an even worse text coming from the Council (in so far this is possible) with full democratic support, than a text which clearly forbids software patents using Monday's procedures. This is not about software patents, it is about respect for the foundations of European Democracy.<p>Sincerely yours,<p>The Board of FFII
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/letters/cons050308/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/03/18 (2005/03/08)
<a href="http://lists.ffii.org/mailman/listinfo/traduk">Jonas Maebe</a><br>
english version 2005/03/18 by <a href="http://www.a2e.de">Phm</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatxatra.el -->
<!-- mode: html -->
<!-- End: -->

