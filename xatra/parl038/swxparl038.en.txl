<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2003/08 Letter to Software Creators and Users

#descr: The European Parliament will, in its plenary session on September 1st,
decide on a directive proposal which ensures that algorithms and
business methods like Amazon One Click Shopping become patentable
inventions in Europe.  This proposal has the backing of about half of
the parliament.  Please help us make sure that it will be rejected. 
Here are some things to do.

#rrh: Dear Software Users, Authors and Investors

#isp: Register as a Supporter

#WWu: Be in Brussels on Wednesday August 27th

#mal: Demonstrate Online

#fte: Inform the Media

#aov: Donate for our activities

#aar: If you haven't done it already, please register at %(URL).

#ogn: We are planning another day of physical presence in and near the
European Parliament, including a performance beside the Parliament at
12.00-14.00 and a conference in the Parliament in the afternoon.

#Wlk: It would be great if you could come and use the opportunity to stay
for 1-2 days and make an appointment with your MEP.  To facilitate and
coordinate this, you can use %(AU) or write to europarl at ffii org. 
More explanations are found at %(EL).  We also help with reserving
beds in Brussels.  They are available for free under certain
conditions, please contact hotel at ffii org.  If you want to do more,
e.g. spend a week talking to MEPs in Brussels, we may be able to
reimburse your travel fees and even pay moderate stipends.

#sso: Do you think that coming to Brussels costs you too much time?  Have
you considered how much time it will cost you later to face patent
infringement claims and to cope with monopoly software?  Which of the
two is better spent?

#egn: If you administrate some servers or own some webspace, you might wish
to block or delay access to certain web pages on August 27th.

#nou: This can be done in a more or less gentle fashion.  Some examples on
what you could do are shown at

#Wwt: Do you think that blocking access to web pages is too drastic a
measure to take?  Have you considered how many web pages will be
rendered inaccessible later by means of software patents if we fail to
prevent this law now?  Which of the two is less drastic?

#lrd: We are turning out %(sn:press releases and other news) all the time. 
Please help us to systematically inform newspapers, radio, tv etc in
your country.  Contact media at ffii org (or media-xx at ffii org,
where xx is your country code) for more info.  If you are willing to
reserve some time and do the work systematically, we may be able to
pay a moderate stipend.

#ety: During these last days and weeks FFII will be paying fees and stipends
for helpers from all over Europe.  We will also be paying for printed
materials, banners, performances etc.  All this will cost us around
10000 eur, slightly more than we currently have at our disposal. 
Donations would not only help us to stay liquid and fully realise our
plans.  They might also enable us to enlarge the plans, e.g. by
placing an effective advertisement somewhere where MEPs read it (would
cost about 10,000 eur).

#aoW: The bank account for a donation is %(BA)

#onr: Country

#aWd: Bank Code

#aoa: Name of Bank

#ktr: Bank's Postal Address

#con: Account

#raW: international bank account number

#Ami: We are told that, using the IBAN, money can now be transferred between
european countries for the domestic fee, thanks to a recent EU
internal market harmonisation directive which can be enforced against
banks.

#cnw: Account Owner

#usd: Account Owner's Postal Address

#ewr: Keyword

#adW: Alternatively you can donate via %(PW) to %(PM).

#WnW: FFII is acknowledged as a public-interest association and we can send
you a receipt that at least in Germany makes the donation
tax-deductible.

#pWr: What will you spend on patent trouble and crippled software later if
this directive proposal goes through?

#uie: Yours Sincerely

#PFi: Hartmut Pilch on behalf of FFII e.V. and Eurolinux Alliance

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl038 ;
# txtlang: en ;
# multlin: t ;
# End: ;

