<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brief von BVMW, Patentverein und FFII an den Bundeskanzler

#descr: Zwei Tage vor dem Gipfeltreffen der deutschen Patentbewegung, auf dem
Bundeskanzler Gerhard Schröder und Justizministerin Brigitte Zypries
zusammen mit Siemens-Chef Heinrich von Pierer das Patentwesen und den
Einsatz der Regierung für Softwarepatente in der EU feiern werden,
üben die Vorsitzenden des Bundesverbandes der Mittelständischen
Wirtschaft, des Patentvereins und des Fördervereins für eine Freie
Informationelle Infrastruktur scharfe Kritik an der zügellosen
Entwicklung des Patentwesens in Deutschland.  Das Kanzleramt
versprach, den Brief dem Kanzler vorzulegen.  Justizministerin
Brigitte Zypries antwortete 8 Wochen später an Stelle des
Bundeskanzlers durch einen Brief, in dem sie einen Konsens über die
angeblich überragende Bedeutung des Patentwesens für die Förderung der
Innovation konstatiert und einen Teil der bekannten BMJ-Falschaussagen
über die Haltung der Bundesregierung zu Softwarepatenten wiederholt. 
Auf die Ausführungen des Schreibens der drei Verbände wird nicht
eingegangen.  Dafür wird eine Fortsetzung von Dialog, u.a. durch eine
öffentliche %(q:Fachdiskussion) zum Thema %(q:Schutz
computerimplementierter Erfindungen -- Wie geht es weiter?) am 21.
Oktober in München angekündigt.

#edr: 2004-07-05 Brief von BVMW, Patentverein und FFII

#ory: 2004-09-01 Antwort von Brigitte Zypries

#tZk: Kritik an Zypries-Brief

#gvh: Sehr geehrter Herr Bundeskanzler, am Vortag Ihres Besuches der
Münchner Veranstaltung %(q:Innovation und Geistiges Eigentum) möchten
wir vor %(q:Patentkinschen Dörfern) warnen, die bei gefälligen
Veranstaltungen den Blick auf die Wirklichkeit verstellen können. Den
Grundgedanken von Patenten, geistiges Eigentum zu schützen und
Innovation zu fördern, teilen wir uneingeschränkt. Von dieser
Idealvorstellung hat sich das Patentwesen in Europa und anderen Teilen
der Welt jedoch immer weiter entfernt. Inzwischen besteht dringender
Bedarf für eine Reform und Selbstdisziplin im Patentwesen.

#stW: am Vortag Ihres Besuches der Münchner Veranstaltung %(q:Innovation und
Geistiges Eigentum) möchten wir vor %(q:Patentkinschen Dörfern)
warnen, die bei gefälligen Veranstaltungen den Blick auf die
Wirklichkeit verstellen können. Den Grundgedanken von Patenten,
geistiges Eigentum zu schützen und Innovation zu fördern, teilen wir
uneingeschränkt. Von dieser Idealvorstellung hat sich das Patentwesen
in Europa und anderen Teilen der Welt jedoch immer weiter entfernt.
Inzwischen besteht dringender Bedarf für eine Reform und
Selbstdisziplin im Patentwesen.

#WcW: Der deutsche Mittelstand braucht nicht nur den Schutz durch Patente,
sondern immer mehr den Schutz vor Patenten. Manche Patente werden
taktisch und zum Schaden der Wirtschaft für die Blockade ganzer
Marktsegmente, aber auch von produktlosen Profiteuren für eine
legalisierte Erpressung missbraucht.

#AaP: Was Patente ihrem Besitzer geben, wird der Allgemeinheit genommen. Mit
solch einem zweischneidigen Schwert muss vorsichtig hantiert werden.
Solange die Innovationskraft von Staaten und Unternehmen in der
Quantität und nicht der Qualität ihrer Patente gemessen wird, sind
Fehlentwicklungen programmiert.

#Wnt: Viele Patente dienen längst nicht mehr dazu, Innovationen zu schützen
und zu belohnen, sondern sie zu behindern und zu bestrafen.
Kapitalmacht schafft Recht, und der Mittelstand resigniert vor der
Drohkulisse einer Patent-Sintflut.

#awi: Patente dürfen kein Selbstzweck sein. Unser Land braucht Wachstum und
Arbeitsplätze in der produzierenden Wirtschaft mehr als in
Patentbehörden und Anwaltskanzleien. Das Patentwesen muss auf den
Prüfstand gestellt und aus Bereichen herausgehalten werden, in denen
es der Allgemeinheit mehr Schaden als Nutzen bringt.

#he0: Wenn selbst mathematische Logik und Geschäftsabläufe unter
pseudotechnischen Vorwänden patentierbar werden und wenn die
Entwicklung von Computersoftware in Zukunft nur noch in den Händen
einiger weniger Konzerne liegen soll, so belastet dies die gesamte
Wirtschaft und Gesellschaft. Wir bitten Sie deshalb, in der Frage der
%(q:computer-implementierten Erfindungen) die Position des
Europaparlaments vom September 2003 zu unterstützen, welche die
Patentierung reiner Programmlogik wirkungsvoll ausschließen würde.

#eoi: Wir würden uns wünschen, dass Sie auch die negativ Betroffenen und die
konstruktiven Kritiker des Patentwesens anhören. Einige weitere
Informationen zu den angesprochenen Problemen befinden sich auf den
Internetseiten unserer Organisationen (www.ffii.org,
www.patentverein.de und www.bvmw.de).

#fhn: Mit freundlichen Grüßen gez.

#uer: vielen Dank für Ihr an den Herrn Bundeskanzler gerichtetes Schreiben
vom 5. Juli 2004 zum Patentwesen.  Herr Bundeskanzler hat mich wegen
der Zuständigkeit des Bundesministeriums der Justiz für das
Patentrecht gebeten, Ihnen zu antworten.

#zWJ: Ich bin mit Ihnen einer Meinung, dass Patente für das wirtschaftliche
Wohlergehen unseres Landes eine ganz wesentliche Bedeutung haben.  Sie
bieten gerade für die mittelstandsgeprägte deutsche Wirtschaft die
Möglichkeit, Innovationen zu schützen und dadurch neue Arbeitsplätze
zu schaffen und bestehende Arbeitsplätze zu sichern.  Ich stimme mit
Ihnen auch darin überein, dass allein die Anzahl der erteilten Patente
keine abschließende Aussage über die Innovationskraft eines Landes
zulässt.  Jedoch kann ganz allgemein festgestellt werden, dass die
drei patentaktivsten Staaten der Welt -- USA, Japan und Deutschland --
nicht nur wirtschaftlich führend sind, sondern auch hinsichtlich der
Innovationskraft die vordersten Plätze belegen.

#dss: Sie haben auch Recht, wenn Sie darauf hinweisen, dass bei allem
positiven Einfluss des Patentwesens auf die Stimulierung von
Erfindungen und die Verbreitung von technischem Wissen mögliche
Überdehnungen des Schutzsystems verhindert werden müssen.  Im Rahmen
der aktuellen Diskussion um die geplante Richtlinie zur
Patentierbarkeit computer-implementierter Erfindungen haben wir uns
deshalb immer für eine deutliche Begrenzung der
Patentierungsmöglichkeiten eingesetzt.  So soll z.B. die Patentierung
von Geschäftsmethoden -- anders als in den USA -- ausdrücklich
ausgeschlossen bleiben.

#nnZ: Gleichzeitig befinden wir uns mit Ihnen wie auch mit anderen Kritikern
der Richtlinie von Beginn der Beratungen an in einem intensiven
Dialog, der insbesondere bei den Expertengesprächen und zuletzt auch
bei dem von mir initiierten %(q:Runden Tisch) geführt wurde.   Ich
kann Ihnen versichern, dass sich die Bundesregierung auch in Zukunft
um Transparenz bemühen und den offenen Dialog suchen wird.  Ich möchte
Sie deshalb ermuntern, Ihre sachliche Kritik auch in Zukunft in die
Diskussion einzubringen, damit wir möglichen Fehlentwicklungen
frühzeitig entgegentreten können.

#0cn: Dazu schon jetzt ein wichtiger Hinweis: Am 21. Oktober 2004 wird im
Rahmen der von mir ins Leben gerufenen Reihe %(q:Geistiges Eigentum im
Gespräch) eine öffentliche Fachdiskussion zum Thema %(q:Schutz
computer-implementierter Erfindungen -- Wie geht es weiter?)
stattfinden, zu der ich Sie schon jetzt recht herzlich einladen
möchte.  Eine förmliche Einladung mit den näheren Details erfolgt in
Kürze.

#WlG: Mit freundlichen Grüßen

#nec: Wo Patente über Wohl und Weh entscheiden, hat selbstverständlich
derjenige Erfolg, die viele Patente besitzt.  Sicherlich waren im
kalten Krieg die rüstungsaktivsten Staaten der Welt auch die politisch
mächtigsten.  Hieraus folgt nicht, dass das System des kalten Krieges
sinnvoll war.  Zypries begeht hier einen von unserem Schreiben
angeprangerten elementaren Fehler, den wir übrigens auch immer wieder
bei %(am:Arlene McCarthy) feststellten und kritisierten.  Den von
Zypries konstatierten Konsens über die Bedeutung des Patentwesens für
die Volkswirtschaft gibt es nicht.  Einigen Studien zufolge ist die
Bedeutung des heutigen Patentwesens insbesondere für KMU gering, und
eine stimulierende Wirkung auf die Innovationstätigkeit lässt sich
bestenfalls für einzelne Branchen (z.B. Chemie, Pharma) nachweisen. 
Ein Konsens besteht nur insoweit, dass Immaterialgüterrechte
(geistiges Eigentum) bei richtiger Gestaltung der Bestimmungen
wünschenswert sind.

#hnk: Die Bundesregierung hat sich leider innerhalb der EU immer %(e:gegen)
jede wirksame Begrenzung der Patentierungsmöglichkeiten eingesetzt. 
Sie hat die vom Europäischen Parlament vorgeschlagenen Begrenzungen
abgelehnt und selbst den Verzicht der Kommission auf Programmansprüche
(eine besonders breite Form des Software-Anspruchs, bei der schon die
Veröffentlichung eines Textes genügt, um das Patent zu verletzen) hat
die Bundesregierung nicht akzeptiert.  Die Durchsetzung von
Programmansprüchen und die Erstellung des bis dato extremsten
Pro-Patent-Gesetzgebungsvorschlages im EU-Rat gehen wesentlich auf
Initiativen der Bundesregierung zurück.  Die von der Bundesregierung
vorangetriebene Ratsvorlage schließt auch die Patentierung von
Geschäftsmethoden nicht im geringsten aus.  Im Inland hat die
Bundesregierung mit ihren aushöhlenden Interpretationen des PatG dafür
gesorgt, dass nicht nur Patente sondern auch Gebrauchsmuster (d.h.
ungeprüfte 10-jährige Monopolrechte) auf Software und
Geschäftsmethoden erteilt werden, da es sich ja laut BMJ-Auslegung
niemals um die genannten Gegenstände als solche handeln kann.  So
sieht sich z.B. Google in Deutschland dank BMJ mit einem
Gebrauchsmuster auf suchwort-gesteuerte Werbung und darauf gegründeten
Zahlungsforderungen von Yahoo konfrontiert.

#nge: Im Moment geht es, wie auch unser Brief betonte, um die Korrektur von
Fehlentwicklungen in einem relativ späten Stadium.  Ein %(q:intensiver
Dialog) hat viele Jahre lang stattgefunden, aber weitgehend ohne
Beteiligung des BMJ.  Auch der jetzige Briefwechsel zeigt, dass die
Bundesregierung diesen Dialog ignoriert.   In den letzten Monaten
behaupteten BMJ-Vertreter auf Anfrage immer wieder, die Patentierung
%(q:computer-implementierter Erfindungen) sei für die Volkswirtschaft
segensreich, ohne die gegenteiligen Erkenntnisse, die sich u.a. aus
%(bm:Auftragsstudien der Bundesregierung) ergaben, auch nur einer
Erwähnung zu würdigen.  Dennoch beteiligen wir uns natürlich gerne an
weiteren %(q:öffentlichen Fachdiskussionen).  Solche Diskussionen
könnten wirksamer und glaubwürdiger werden, wenn das BMJ im Vorfeld
den bisherigen Stand der Diskussion in ihrer ganzen Vielfalt
dokumentieren und die Fragen ergebnisoffen formulieren würde.  
Formulierungen wie %(q:Schutz computer-implementierter Erfindungen)
engen den Blickwinkel ein.  Es wäre stattdessen zu fragen, was die
Anforderungen an Immaterialgüterrechte für Software u.a. heute sind
und in Zukunft sein werden, wie das bestehende System aus Patenten und
Urheberrechten reformiert werden muss, damit es den Anforderungen
gerecht werden kann, und was dies insbesondere für das laufende
Gesetzgebungsverfahren bedeutet.

#scu: Im übrigen %(lc:fordern) wir, dass die Bundesregierung ihre
Unterstützung für die Ratsvereinbarung vom 18. Mai widerruft, um Raum
für die versprochene konstruktive Diskussion zu schaffen.  In seiner
jetzigen Form kommt das Ratspapier einem Signal zur schnellen
Beerdigung des gesamten Richtlinienprojektes gleich.  Der Rat muss
seine Arbeit auf den Parlamentsvorschlag vom September 2003 und nicht
auf den Ratstext vom November 2002 oder den von diesem inspirierten
JURI-Text vom Juni 2003 gründen.  Jeder Widerstand gegen die
Vorschläge des Europäischen Parlamentes bedarf sorgfältiger Begründung
und Legitimation.  Beides fehlt hier völlig.  Am 18. Mai und davor hat
das BMJ auf Intransparenz und Dialogverhinderung hingewirkt, s.
%(jt:Schreiben von MdB Tauss).  Wenn das BMJ sich %(q:auch in Zukunft)
um Transparenz und offenen Dialog bemühen will, nimmt es sich
widersprüchliches vor.  Es kann nur entweder Kontinuität oder
%(q:Transparenz und offenen Dialog) geben.  Offener Dialog setzt eine
Distanzierung von der Vergangenheit und insbesondere von der
Vereinbarung des 18. Mai voraus, wie sie in der %(np:Niederländischen
Parlamentsentscheidung vom 1. Juli) und in dem im Bundestag
anliegenden %(fd:Entschließungsantrag der FDP) gefordert werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bvmw040705 ;
# txtlang: de ;
# multlin: t ;
# End: ;

