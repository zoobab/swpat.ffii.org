<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Petición urgente a los Gobiernos y Parlamentos nacionales

#descr: Los gobiernos de Europa están a punto de firmar una propuesta de
directiva a favor de la patentabilidad y aplicabilidad ilimitada de
las patentes de algoritmos y métodos de negocio %(q:implementados en
ordenador). El acuerdo del Consejo de Ministros del 18 de Mayo de 2004
descarta decisiones bien debatidas del Parlamento Europeo y los
órganos consultivos de la Unión sin ninguna justificación, y sin
legitimación democrática. La mayoría se consiguió por un paquete de
propuestas decepcionante y por maniobras diplomáticas bastante
cuestionables durante la sesión de decisión. Los abajo firmantes, que
representan a los líderes de la innovación en el campo del software y
la discusión informada sobre políticas de innovación informáticas en
Europa, piden a los responsables políticos echar mano del freno de
emergencia y reorganizar el proceso de legislación sobre
competitividad en el Consejo.

#epn: Receptores

#2ln: Cabezas de lista de 25 gobiernos y parlamentos, y otros políticos
preocupados.

#ujc: Asunto

#ati: Directiva sobre Patentabilidad de Software: Petición para reabrir la
discusión en el Consejo.

#act: Nos preocupa que

#toe: Por estas razones le apremiamos a

#iar: Abajo firmantes

#pee: Anexos

#aMe: La sesión del Consejo de Competitividad del 18 de Mayo de 2004 alcanzó
una mayoría cualificada para una versión de la directiva de patentes
de software 2002/0047 COM (COD) que impondría patentabilidad y
aplicabilidad ilimitada de las patentes de algoritmos y métodos de
negocio %(q:implementados en ordenador) en Europa. Hay un consenso
general entre economistas y profesionales del sector del software en
que tal régimen, tal y como existe en EEUU, es desastroso para la
innovación, la competencia y el crecimiento de la economía basada en
la información.

#iwc: El texto propuesto está diseñado para confundir a los ministros acerca
de sus verdaderos efectos. Consiste en multitud de sentencias del
tipo: %(q:El software es... [frase con gran retórica] ... no
patentable, a no ser que [condición que, bajo escrutinio, resulta ser
siempre cierta]). Los límites imaginarios de este tipo pervierten la
propuesta, y especialmente las provisiones especiales que fueron
utilizadas para persuadir a los ministros.

#por: Los moderadores de la sesión del  Consejo de la Competencia
presionaron a los participantes para aceptar la propuesta mediante el
engaño y tácticas sorpresas, haciendo cuestionable incluso que se
alcanzase una mayoría válida. Puede decirse con certeza que sólo una
minoría de los gobiernos están de acuerdo con lo que se negoció, pero
varios de estos gobiernos no estuvieron bien representados por sus
negociadores, quienes rompieron acuerdos intra-ministeriales o incluso
violaron instrucciones de sus superiores.

#isi: El Parlamento Europeo ya ha rechazado, a través de una serie de
enmiendas, textos de la Comisión y el Comité JURI, que eran
prácticamente idénticos tanto en letra como en espíritu al aprobado
ahora por el Consejo. Las enmiendas reflejan las demandas de la gran
mayoría de innovadores en el terreno del software e investigadores de
políticas de innovación en la UE, incluyendo a los autores de estudios
ordenados por la Comisión así como miembros de los órganos consultivos
de la UE.

#eWe: El Consejo ha ignorado y rechazado todo el trabajo del Parlamento y
los órganos consultivos de la UE sin ninguna justificación ni
legitimidad democrática. Este texto no se presenta como un medio de
alcanzar un objetivo político, sino como un (%q:compromiso) entre
gobiernos. Fue negociado bajo un velo de secretismo entre oficiales
anónimos de los ministerios, la mayoría de los cuales están al cargo
de la gestión de las oficinas nacionales de patentes y por lo tanto
forman parte de una comunidad interesada en la patentabilidad
ilimitada.

#Wmh: pedir a la Presidencia del Consejo retirar la votación de la directiva
de patentabilidad del software (2002/0047 COM (COD)) de la agenda de
la próxima sesión del Consejo donde se espera su aprobación.

#doe: retirar el dossier de manos de la burocracia de las patentes, y
reinstaurar el escrutinio político sobre el impacto del texto
propuesto. La designación de representantes en el grupo de trabajo del
Consejo debería ser presentado y debatido públicamente (en los
Parlamentos donde las reglas institucionales lo permitan).

#tln: urgir a otros gobiernos a hacer lo mismo, y como último término
reformar el Consejo Europeo para prevenir catástrofes como la actual
de ocurrir en el futuro.

#iin: ConfÃ©dÃ©ration EuropÃ©enne des Associations Petites et Moyennes
Entreprises

#cs5: 22 asociaciones de estados miembros y 19 países europeos representados
en más de 500.000 empresas.

#ich: Consortium for Open Source Middleware Architectures

#alW: Entre los miembros están varias grandes compañías

#tdW: representa los interesas de 60000 firmantes y 1000 compañías en
materia de propiedad del software

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: member of Parliament

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Polish Senator

#aiW: Social Democratic Party of Poland

#csr: IT expert of Socialist Party

#VERT: Verdes

#atP: candidato principal al Parlamento Europeo

#WaS: Partido de Los Verdes de Suecia

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#thW: El texto principal se envía conjuntamente con la %(CA:Llamada para la
acción II) con logotipos y firmas. Además, los siguientes anexos
también se adjuntan.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de informa acerca de esta carta.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: chema ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: es ;
# multlin: t ;
# End: ;

