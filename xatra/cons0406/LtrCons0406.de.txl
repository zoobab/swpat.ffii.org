<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Dringender Aufruf an nationale Regierungen und Parlamente

#descr: Die Regierungen Europas sind dabei, ihre Unterschrift unter einen
Richtlinienentwurf zu setzen, der uneingeschränkte Patentierbarkeit
und Patentdurchsetzbarkeit auf %(q:computer-implementierte)
Algorithmen und Geschäftsmethoden erlaubt. Die Einigung im Ministerrat
vom 18. Mai 2004 verwirft dabei ohne jede Rechtfertigung und auf
demokratisch nicht legitimierte Weise wohlbedachte Entscheidungen des
Europäischen Parlaments und der beratenden EU-Organe. Die
Stimmenmehrheit auf der entscheidenden Sitzung wurde stattdessen durch
irreführende Formulierungen und fragwürdige diplomatische Winkelzüge
gesichert. Die Unterzeichnenden, führend im Bereich von
Softwareinnovation und der Diskussion über deren Sicherung im
rechtlichen Rahmen Europas, fordern von den verantwortlichen
Politikern, die Notbremse zu ziehen und den Prozess der Gesetzgebung
im Wettbewerbsrecht zu reorganisieren.

#epn: Empfänger

#2ln: Führungsspitzen von 25 Regierungen und Parlamenten sowie andere
betroffene Politiker

#ujc: Betreff

#ati: Softwarepatent-Richtlinie: Aufforderung zur erneuten Diskussion im Rat

#repr: Qualifiziert?

#step: Ausweg

#parl: Parlament

#cfax: s. Anhang %1

#act: Wir sind darüber besorgt, dass

#aMe: der Rat in der Wettbewerbsrechtssitzung vom 18.5.2004 eine
qualifizierte Mehrheit für eine Version der Softwarepatent-Richtlinie
2002/0047 COM (COD) erreicht hat, die uneingeschränkte
Patentierbarkeit und Patentdurchsetzbarkeit von
%(q:computer-implementierten) Algorithmen und Geschäftsmethoden
erlaubt. Es ist allgemeiner Konsens von Ökonomen und IT-Unternehmern,
dass ein solches den US-Verhältnissen entsprechendes Patentregime sich
desaströs auf Innovation, Wettbewerb und Wachstum der wissensbasierten
Wirtschaft auswirkt.

#iwc: der vorgeschlagene Text darauf ausgelegt ist, die Minister über seine
tatsächlichen Auswirkungen in die Irre zu führen. Er besteht aus
vielen Sätzen der Form %(q:Software ist ... [ rhetorisch überladene
Betonungen ] ... nicht patentierbar, außer ... [ Bedingung, die sich
bei näherem Hinschauen als immer wahr entpuppt ]). Derartige
Scheinbeschränkungen prägen den Vorschlag und insbesondere die
zentralen Bestimmungen, die zur Überzeugung der Minister herangezogen
wurden.

#por: die Leitung der Ratsabstimmung die Teilnehmer mittels Täuschung, Druck
und überraschenden Änderungen zur Zustimmung gedrängt hat. Letztlich
ist sogar unklar, ob überhaupt eine qualifizierte Mehrheit zu Stande
gekommen ist. Mit Sicherheit kann man sagen, dass nur eine Minderheit
der Regierungen wirklich mit dem übereinstimmen, was beschlossen
wurde. Gleich in mehreren Fällen haben die nationalen Vertreter
hingegen ihr Land falsch vertreten, Übereinkünfte zwischen
verschiedenen Ministerien ignoriert oder gar gegen die Vorgaben ihrer
Vorgesetzten gehandelt.

#isi: der Richtlinienentwurf des Ministerrates weit gehend sinn- und
textgleich mit Fassungen der Europäischen Kommission und des
JURI-Ausschusses ist, die vom Europäischen Parlament vermittels einer
umfassenden Serie von Änderungen entschieden abgelehnt worden sind.
Diese Änderungen sind geleitet von den Forderungen der übergroßen
Mehrzahl von innovativen Softwareproduzenten und Innovationsforschern
in der EU, einschließlich der Autoren von Studien, die die Kommission
selbst angefordert hat, und einschließlich der Mitglieder der
beratenden EU-Organe.

#eWe: der Rat die Arbeit des Parlaments und der beratenden Organe ohne
Rechtfertigung und ohne demokratische Legitimation einfach ignoriert
und verworfen hat. Der Text wurde nicht als Mittel zum Erreichen
irgendeines politischen Ziels präsentiert, sondern als %(q:Kompromiss)
zwischen den Regierungen. Er wurde im Geheimen verhandelt, zwischen
Ministeriumsvertretern, die meist für die nationalen Patentämter
zuständig und mit dem patentjuristischen Milieu eng verbunden sind.

#toe: Aus diesen Gründen fordern wir Sie auf,

#Wmh: die Ratspräsidentschaft dazu zu bringen, die Abstimmung über die
Softwarepatent-Richtlinie (2002/0047 COM (COD)) von der Tagesordnung
der nächsten Ratssitzung zu streichen, wo sie momentan ihre formale
Bestätigung erwartet.

#doe: den Vorgang den Händen der Patentbürokratie zu entwinden und den Text
und seine Auswirkungen wieder unter einem politisch geprägten
Blickwinkel zu betrachten. Die Bestimmung der Vertreter in der
Ratsarbeitsgruppe sollte öffentlich vorgestellt und debattiert werden
(dort, wo der rechtliche Rahmen die den Parlamenten erlaubt).

#tln: auf andere Regierungen einzuwirken, ebenso zu handeln, eine umfassende
Reform des EU-Rates zu forcieren und so in Zukunft Katastrophen wie
die momentane zu verhindern.

#iar: Unterzeichner

#iin: Europäischer Dachverband der Kleineren und Mittleren Unternehmen

#cs5: 22 Mitgliedsorganisationen in 19 europäischen Staaten, durch die mehr
als 500.000 Unternehmen vertreten werden.

#ich: Arbeitsgemeinschaft für Open-Source-Middleware-Architekturen
(Consortium for Open Source Middleware Architectures)

#alW: Unter den Mitgliedern finden sich viele große Firmen

#tdW: Vertritt die Interessen von 60000 Unterstützern und 1000 Unternehmen
in Sachen Softwarebesitz

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: Parlamentsmitglied

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Polish Senator

#aiW: Social Democratic Party of Poland

#csr: IT expert of Socialist Party

#delegit

#socpart

#VERT: Fraktion Grüne/Europäische Freie Allianz

#atP: Spitzenkandidat für das Europaparlament

#WaS: Die schwedischen Grünen

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Hersteller von Computerspielen

#bvsidesc: Berufsverband Selbständige in der Informatik e.V.

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: weitere Unterzeichner

#gcr: %(N) Personen haben diesen Appell über das %(ps:Mitwirkungssystem des
FFII) unterzeichnet.

#pee: Anlagen

#thW: Der Haupttext wird ergänzt durch den %(CA:Aufruf zum Handeln II) mit
den Logos der Unterzeichner. Weiterhin sind angehängt:

#jet: ObjectWeb-Brief

#nPr: Minimale PDF-Version

#tuW: Eine schön gesetzte Version dieses Dringenden Aufrufs ohne Bezug auf
Anhänge, zum schnellen Versenden (etwa per Fax)

#sfR

#eaW: heise.de berichtet über diesen Brief

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

