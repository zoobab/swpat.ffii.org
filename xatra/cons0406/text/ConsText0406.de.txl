<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Scheinbegrenzungen der Patentierbarkeit im Ratsentwurf

#descr: Wir versuchen einen kurzen Überblick über die Täuschungstaktiken des
Ratsentwurfs zur Software-Patentierung vom 18.05.2004 zu geben.

#ute: Der meistverwendete rhetorische Trick im Ratspapier ist nach dem
Schema

#inW: [A] ist nicht patentfähig, wenn nicht [Bedingung B] erfüllt ist

#hnr: aufgebaut, wobei bei näherer Betrachtung Bedingung B immer erfüllt
ist.

#WiW: Eine ausführliche Behandlung des Sachverhaltes bis ins Detail würde
den zeitlichen Rahmen sprengen. Daher behandeln wir nur einige der
meistdiskutierten Bestimmungen des Ratstextes um das Problem zu
illustrieren.  Eine ausführliche Auswertung steht Ihnen auf der
%(an:Analyseseite) zur Verfügung.

#cWi: Die %(q:wenn nicht) -Bedingung ist immer erfüllt.  Jedes
Computerprogramm kann als ein %(q:Produkt oder Prozess) beansprucht
werden, und in diesem Fall kann der Patentanspruch auch auf das
Programm als solches erhoben werden, %(q:sowohl auf das Programm an
sich als auch in Verbindung mit einem Datenträger).

#tub: Präziser gesagt: Ein Anspruch der Form %(q:Programm, dadurch
bezeichnet dass es [ gewisse Dinge tut  ]) wird patentierbar, und
dieser Patentanspruch deckt dann eine unendlichen Anzahl von
individuell entwickelten urheberrechtlich geschützten Programmen ab.

#ehp: Das Programm muß laut dieser Bestimmung nicht einmal neu oder
innovativ sein.  Es reicht die konventionelle Beschreibung einer neuen
Vorgehensweise.  Sobald jemand einen neuen chemischen Prozess erfunden
hat, kann er eine programmierte Logik patentieren lassen, welche den
Prozess beschreibt (z. B. A+B=C), und in der Folge wird er ermächtigt
jedem zu verbieten sowohl ein Programm zu veröffentlichen, welches
diese Logik verwendet, als auch ein solches für irgendeinen Zweck-
einschließlich der Simulation auf einem handelsüblichen
Standard-Rechner - zu nutzen.

#owi: Diese Bestimmung ist der Kernpunkt des Ratsentwurfs, und sie ruft
erheblich Zweifel an der Kompetenz der %(q:Arbeitsgruppe) hervor,
sogar in den eigenen Reihen.  Lesen Sie hierzu auch unsere
%(ca:Analyse).

#vtt: Diese Bestimmung steht in direktem Widerspruch zu Art 5(2).

#WWl: Sie wurde von Komissionsmitglied Bolkestein während der Sitzung
eingebracht als der erfolgreiche Versuch die anwesenden
Ministerialbeamten zu täuschen.

#ahp: Der oberflächliche Leser mag annehmen, dieser Artikel schliesse die
Bedrohung der Arbeit von Programmierern durch Patente aus, weil
Programme als solche nicht patentfähig seien.  Tatsächlich glaubt
sogar die deutsche Justiz-Ministerin, Brigitte Zypries, an diese
Interpretation des Artikels, denn Sie benutzte sie zehn Tage nach der
Ratssitzung in einem Chat bei dem Versuch die Ängste von Programmiern
zu zerstreuen.

#fnr: Der sachkundige Leser erkennt jedoch, dass dieser Satz genau das
Gegenteil von dem bedeutet, was Zypries und Minister anderer
Regierungen glauben.  Art 52 EPC wird durch diesen Artikel nicht
gestärkt sondern bedeutungslos.

#lpj: Computer-Programme können nicht gleichzeiting beanspruchbar (Art 5(2))
und unpatentierbar (Art 4A1) sein.  Wer nun einen Ausweg aus diesen
Widerspruch im Gesetz sucht, findet folgende Formulierung.

#bue: Hier hat Bolkestein den überflüssigen Nebensatz %(q:sei es als
Quellcode, als Objektcode oder in anderer Form ausgedrückt) eingefügt.
Dieser Nebensatz ist bedeutungslos, da kein Patent-Antragsteller
jemals ein Programm in den engen Grenzen des %(q:Codes) eines
einzelnen Programmierers beanspruchen wird.  So ein eng gefasster
Anspruch wäre die Patentregistrierungsgebühr nicht wert, und würde
auch nicht den geringsten Protest aus den Reihen der
Software-Gemeinschaft hervorrufen. Bolkestein fügte diesen Nebensatz
nur aus einem Grund ein: er suggeriert eine Interpretation für Satz
A1, welcher wiederum Art 52 EPC interpretiert.  Dieser Interpretation
folgend schließt Art 52 nur bestimmte Arten von imaginären engen
Ansprüchen aus, welche niemand je beansprucht hat und gegen die sich
auch keine Bedenken regen. Diese Interpretation steht im Widerspruch
zur klaren Bedeutung von Art 52 EPC so wie er %(ep:normalerweise von
den Patentgerichten verstanden wird), und sie ist %(fl:unzulässig  im
Bezug auf die normale Art und Weise der Interpretation von Gesetzen
und Abkommen).

#ifb: Die %(q:normale physische Interaktion zwischen einem Programm und dem
Computer) bedeutet etwa so viel wie die %(q:normale physische
Interaktion zwischen einem Rezept und dem Koch), nämlich nichts oder
eben das, was man aus %(et:neueren Entscheidungen des EPA) entnehmen
kann, in denen sie eingesetzt wurde.  Damals diente sie dazu, die
Erteilung von Patenten auf geometrische Rechenregeln an IBM zu
rechtfertigen.  Im vorliegenden Fall wurde laut EPA %(q:über die
normale physische Interaktion ... hinaus) noch dadurch ein weiterer
%(q:technischer Effekt) erzielt, dass auf einem Computerbildschirm
Platz gespart wurde.  Das EPA %(ea:beurteilte) erläuterte wenig
später, dass die Fiktion vom %(q:weiteren technischen Effekt)
historisch bedingt war und nicht der Klärung sondern der Verwirrung
diente:

#Chr: Die Ratsarbeitsgruppe lehnt im übrigen den %(4b:Artikel 4B) des
Parlaments ab, der den EPA-Formeln eine begrenzendere Bedeutung
gegeben hätte, gestützt auf eine neuere %(bp:deutsche
Gerichtsentscheidung), derzufolge die Einsparung von Rechenressourcen
keinen %(q:technischen Beitrag) darstellen kann, da andernfalls alle
computer-implementierten Geschäftsmethoden patentfähig würden.
Hierdurch wird klar, dass die Ratsarbeitsgruppe im Einklang mit der
neuesten EPA-Praxis %(q:computer-implementierte) Algorithmen und
Geschäftsmethoden patentierbar machen möchte.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: ConsText0406 ;
# txtlang: xx ;
# End: ;

