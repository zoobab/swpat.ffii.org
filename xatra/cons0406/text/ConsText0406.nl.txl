<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Valse Beperkingen op de Octrooieerbaarheid in het Raadsvoorstel

#descr: We proberen een kort overzicht te geven van de misleidende trucs in
het voorstel van de Raad over de softwarepatenten van 2004-05-18

#ute: De meest gebruikte retorische truc van de Raadstekst werkt als volgt:

#inW: [A] is niet octrooieerbaar, tenzij aan [voorwaarde B] voldaan is.

#hnr: waarbij, bij nader onderzoek, blijkt dat aan voorwaarde B altijd
voldaan is.

#WiW: Wegens de beperkingen qua tijd en plaats, halen we slechts een paar
van de meest bediscussieerde punten van de Raadstekst aan om dit punt
aan te geven. De anderen kunnen gevonden worden op onze %(an:pagina
met de volledige analyse).

#cWi: De %(q:slechts toegestaan)-voorwaarde is steeds waar. Eender welk
computerprogramma kan als een %(q:product of proces) verwoord worden
in de conclusies, waarna een andere octrooiconclusie van bovenstaand
formaat eveneens van toepassing zijn op het programma als zodanig,
%(q:hetzij op zichzelf hetzij op een drager).

#tub: Preciezer: een conclusie van de vorm %(q:programma, gekarakteriseerd
doordat [het bepaalde dingen doet]) zal toelaatbaar zijn, en deze
conclusie zal een oneindig aantal individueel ontwikkelde en
auteursrechtelijk beschermde programma's omvatten.

#ehp: Het programma moet volgens deze voorwaarde zelfs niet nieuw of
inventief zijn. Het kan een conventionele beschrijving van een nieuw
proces zijn. Wanneer iemand een nieuwe chemisch proces heeft
uitgevonden, kan hij de beschrijvende programmalogica monopoliseren
(b.v. A+B=C), en hij kan de publicatie van eender welk programma dat
deze logica gebruikt verbieden, evenals het gebruikt ervan voor eender
welk doel, inclusief simulatie op een gewone desktop PC.

#owi: Deze voorwaarde is de hoofdbrok van het werk van de Raad, en werpt
sterke twijfels op wat betreft de competentie van de %(q:werkgroep),
zelfs in haar beperkt werkgebied van octrooiwetgeving. Zie onze
%(ca:analyse) voor verdere details.

#vtt: Deze voorziening is in directe tegenspraak met Art 5(2).

#WWl: Ze werd ingevoerd op de Raadssessie door Commissaris Bolkestein als
deel van een succesvolle poging om de aanwezige ministeriële
ambtenaren te misleiden.

#ahp: De naïeve lezer zal denken dat dit artikel betekent dat het werk van
een programmeur nooit in aanraking zal komen met octrooien, omdat
programma's als zodanig niet onder octrooiconclusies kunnen vallen. De
Duitse minister van Justitie, Brigitte Zypries, leek deze
interpretatie inderdaad te geloven, toen ze hem gebruikte in een
poging om de bezorgdheden van programmeurs weg te nemen in een online
chat-sessie tien dagen na de Raadsbijeenkomst.

#fnr: De experten die dit lezen bijgrijpen dat deze zin het omgekeerde
betekent van wat Zypries en de andere regeringsministers geloven dat
hij betekent. Art 52 EPC wordt niet herbevestigd, maar wordt
betekenisloos gemaakt door deze paragraaf.

#lpj: Computerprogramma's kunnen niet tegelijkertijd claimbaar (Art 5(2)) en
niet octrooieerbaar (Art 4A1) zijn. Men moet elders gaan zoeken om een
uitweg uit deze contradictie te vinden, en deze wordt aangeboden door
de volgende paragraaf.

#bue: Hier heeft Bolkestein de overbodige bijzin %(q:uitgedrukt in broncode,
objectcode of enige andere vorm") toegevoegd. Deze zin is zinloos,
want geen enkele octrooiaanvrager zou ooit een dergelijk nauwe
conclusie willen aanvragen op een programma in termen van de
individuele %(q:code) van één programmeur. Een dergelijk nauwe
conclusie zou de kosten van het octrooi niet waard zijn, en zou geen
protesten uit de softwaregemeenschap lokken. De enige reden waarom
Bolkestein deze bijzin heeft toegevoegd, was om een interpretatie te
suggereren voor paragraaf A1, die op zijn beurt Art 52 EPC
interpreteert. Volgens deze interpretatie was de enige bedoeling van
Art 52 om bepaalde ingebeelde beperkte conclusies uit te sluiten, die
nooit door iemand aangevraagd zijn en waarover nooit iemand geklaagd
heeft. Deze interpretatie is in tegenspraak met de duidelijke
betekenis die Art 52 EPC heeft zoals die %(ep:algemeen aanvaard is
door octrooirechtbanken), en ze is %(fl:ontoelaatbaar onder de normale
interpretatie van wetten en verdragen).

#ifb: De verwoording %(q:normale fysieke interactie tussen een programma een
een computer) betekent ongeveer zoveel als %(q:de normale fysieke
interactie tussen een recept en een kok): niets. Het is een magische
formule waarvan de betekenis enkel kan begrepen worden aan de hand van
%(et:recente beslissingen van het EOB), waarin ze gebruikt werd om het
toekennen van octrooien op meetkundige berekeningen aan IBM te
rechtvaardigen. In dat geval kon, volgens het EOB, het %(q:verder
technisch effect dan de ...) bestaan uit besparingen van plaatsgebruik
op een computerscherm. Twee jaar later %(ea:merkte het EOB zelf op)
dat deze constructie verwarrend is, maar nodig was voor politieke
doeleinden:

#Chr: Merk op dat de Werkgroep van de Raad %(4b:Art 4B) van het Parlement
verwerpt, wat zou geholpen hebben om een meer restrictieve betekenis
te geven aan de verwoording van het EOB, gebaseerd op recente
%(bp:Duitse rechtspraak). Deze hield in dat de besparing van middelen
zoals geheugen of rekenkracht in een computer geen %(q:technische
bijdrage) kan zijn, omdat anders zowat alle in computers
geïmplementeerde methoden voor bedrijfsvoering octrooieerbaar zouden
worden. Het is duidelijk dat de werkgroep van de Raad %(q:in computers
geïmplementeerde) algoritmen en methoden voor bedrijfsvoering
octrooieerbaar wil maken, overeenkomstige de recente praktijk van het
EOB.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: ConsText0406 ;
# txtlang: xx ;
# End: ;

