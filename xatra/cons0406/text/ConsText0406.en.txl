<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fake Limits on Patentability in the Council Proposal

#descr: We try to produce a short overview of the deceptive tricks in the
Council's software patent proposal of 2004-05-18.

#ute: The most frequently used rhetorical trick of the Council paper works
as follows

#inW: [A] is not patentable, unless [condition B] is met.

#hnr: where, upon close scrutiny, it turns out that condition B is always
met.

#WiW: Due to constraints of time and space, we pick out only a few most
often discussed provisions from the Council text to illustrate this
point.  More can be found via the %(an:analysis page).

#cWi: The %(q:unless) condition is always true.  Any computer program can be
claimed as a %(q:product or process), and in such cases the patent
claim can also be directed to the program as such, %(q:either on its
own or on a carrier).

#tub: To put it more precisely, a claim of the form %(q:program,
characterised by that it [ does certain things  ]) will be grantable,
and this claim will cover an infinite number of individually
developped copyrighted programs.

#ehp: The program according to this provision needn't even be new or
inventive.  It can be a conventional description of a new process. 
Once somebody has invented a new chemical process, he can claim the
program logic that describes it (e.g. A+B=C), and he can forbid the
publication of any program that uses this logic as well as its use for
any purposes, including simulation on a general-purpose computer.

#owi: This provision is the centerpiece of the Council's work, and it casts
severe doubts on the competence of the Council's %(q:Working Party)
even in its own narrow field of patent law.  See our %(ca:analysis)
for further details.

#vtt: This provision is in direct contradiction with Art 5(2).

#WWl: It was introduced at the Council session by Commissioner Bolkestein in
a successful attempt to fool the ministerial officials at the session.

#ahp: The naive reader will take the article to mean that the work of a
programmer can not fall afoul of patents, because programs as such are
not covered by patent claims.  In fact the German minister of justice,
Brigitte Zypries, seemed to believe in this interpretation, when she
used it in an attempt to dispel the fears of programmers in a chat ten
days after the Council session.

#fnr: The expert reader understands that this sentence means the opposite of
what Zypries and other government ministers believe it means.  Art 52
EPC is not reaffirmed but rendered meaningless by this clause.

#lpj: Computer programs can not be claimable (Art 5(2)) and unpatentable
(Art 4A1) at the same time.  The puzzled law interpreter has to look
for a way out of the contradiction, and he finds it in the following
clause.

#bue: Here Bolkestein inserted the redundant subclause %(q:whether expressed
as source code, as object code or in any other form).  This clause is
meaningless, because no patent applicant would ever want to claim a
program narrowly in terms of the individual %(q:code) of one
programmer.  Such a narrow claim would not be worth the patent
registration fee, and it would not arouse any protests from the
software community.  The only reason why Bolkestein inserted this
subclause was to suggest an interpretation for clause A1, which in
turn interprets Art 52 EPC.  According to this interpretation, Art 52
was meant only to exclude certain kinds of imaginary narrow claims
which nobody has every applied for and against which nobody has ever
raised concerns.  This interpretation is in contradiction with the
clear meaning of Art 52 EPC as it has %(ep:generally been understood
by the patent courts), and it is %(fl:inadmissible under the normal
methodology of interpretation of laws and treaties).

#ifb: The wording %(q:normal physical interaction between a program and the
computer) means about as much as %(q:normal physical interaction
between a recipe and the cook): nothing.  It is a magic formula whose
usage can be inferred only from %(et:recent decisions of the EPO), in
which it served to justify the granting of patents on geometrical
calculation rules to IBM.  In the present case, according to the EPO,
the %(q:further technical effect beyond ...) consisted in the
economisation of space on a computer screen.  Two years later, the EPO
itself %(ea:pointed out) that this construction is confusing but was
needed for political purposes:

#Chr: It should be noted that the Council Working group rejects the
Parliament's %(4b:Art 4 B), which would have helped to attribute a
more restrictive meaning to the EPO wording, based on a recent
%(bp:german court decision) which held that economisation of computing
ressources does not constitute a %(q:technical contribution), because
otherwise practically all computer-implemented business methods would
become patentable subject matter.  It is clear that the Council
working group wants to make %(q:computer-implemented) algorithms and
business methods patentable in accordance with recent EPO practise.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: ConsText0406 ;
# txtlang: xx ;
# End: ;

