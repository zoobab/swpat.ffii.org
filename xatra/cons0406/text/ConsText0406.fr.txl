<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fausses limites à la brevetabilité dans la proposition du Conseil

#descr: Nous allons essayer d'élaborer une vue d'ensemble des astuces
trompeuses utilisées dans la proposition du Conseil du 18 mai 2004 sur
les brevets logiciels.

#ute: Le subterfuge rhétorique le plus fréquemment utilisé dans le document
du Conseil se déroule ainsi :

#inW: [A] n'est brevetable que si [condition B] est remplie.

#hnr: mais, si on examine ceci minutieusement, il s'avère que la condition B
est toujours remplie.

#WiW: En raison des contraintes de temps et d'espace, nous n'avons pris dans
le texte du Conseil que les dispositions les plus souvent discutées
pour illustrer ce point. D'autres exemples sont disponibles via la
%(an:page d'analyse).

#cWi: La condition %(q:que si) est toujours vraie. Tout programme
d'ordinateur peut être revendiqué en tant que %(q:produit ou procédé),
auquel cas la revendication de programme s'applique également au
programme en tant que tel, %(q:seul ou sur support).

#tub: Plus précisément, une revendication de la forme %(q: un programme,
caractérisé par le fait qu'il [fasse certaines choses]) sera admise et
cette revendication couvrira une infinité de programmes développés
séparément sous le régime des droits d'auteur.

#ehp: Le programme selon cette disposition n'a même pas besoin d'être
nouveau ou inventif. Il peut être une description conventionnelle d'un
nouveau procédé. Dès que quelqu'un invente un nouveau procédé
chimique, il peut revendiquer la logique qui le décrit sous forme de
programme (ex: A+B=C) et peut interdire la publication de tout
programme qui utiliserait cette logique ainsi que toute utilisation
dans n'importe quel but, y compris la simulation sur un ordinateur
générique.

#owi: Cette disposition est la pièce centrale dans le document du Conseil et
elle soulève des doutes sérieux quant aux compétences du %(q:Groupe de
travail) même dans son domaine restreint du droit des brevets. Voir
notre %(ca:analyse) pour plus de détails.

#vtt: Cette disposition est en directe contradiction avec l'article 5(2).

#WWl: Elle a été introduite durant la session du conseil par le Commissaire
Bolkestein dans une tentative fructueuse de tromper les fonctionnaires
ministériels présents à la réunion.

#ahp: Le lecteur naïf croira que l'article signifie que le travail d'un
programmeur ne peut pas tomber sous le joug de brevets. En fait, la
ministre allemande de la justice, Brigitte Zypries, a semblé croire
cette interprétation, lorsqu'elle l'a utilisée pour dissiper les
craintes de programmeurs dans un échange sur internet (%(q:chat)) dix
jours après la session du Conseil.

#fnr: Le lecteur avisé comprend que cette phrase signifie l'inverse de ce
que Zyppries et les ministres des autres grouvernements ont cru.
L'article 52 de la CBE n'est pas réaffirmé mais perd tout son sens par
cette clause.

#lpj: Il est impossible que les programmes d'ordinateurs puissent être
revendicables (article 5(2)) et non brevetables (article 4 bis 1) en
même temps. Celui qui s'interroge pour interpréter la loi doit trouver
un moyen de sortir de cette contradiction. Et il peut trouver ce moyen
dans la clause suivante.

#bue: Ici, Bolkestein a inséré une sous-clause redondante : %(q:qu'ils
soient exprimés en code source, en code objet ou sous toute autre
forme). Cette clause est dénuée de sens car aucun déposant de brevet
ne voudrait revendiquer un programme dans des termes aussi étroits que
le %(q:code) particulier d'un programmeur isolé. Une revendication
aussi étroite ne vaudrait pas le prix d'un dépôt de brevet et ne
susciterait aucune protestation de la part de la communauté
informatique. La seule raison pour laquelle Bolkestein a inséré cette
sous-clause était de suggérer une interprétation pour la clause 4 bis
(1), qui à son tour interprète l'article 52 de la CBE. Selon cette
interprétation, le but de l'article 52 était seulement d'exclure
certaines sortes de revendications étroites imaginaires que personne
n'a jamais appliquées et dont personne ne s'est jamais soucié. Cette
interprétation est en contradiction avec la signification claire de
l'article 52 de la   CBE tel que les %(ep:cours de brevets l'ont
généralement compris) et elle est %(fl:inadmissible selon la
méthodologie habituelle dans l'interprétation des lois et traités).

#ifb: La formulation %(q:interactions physiques normales entre un programme
et l'ordinateur) veut à peu près dire la même chose que
%(q:interactions physiques normales entre une recette et un plat
cuisiné) : rien ! C'est une formule magique dont l'utilisation ne peut
être déduite que des %(et:décisions récentes de l'OEB), dans
lesquelles elle a servi à justifier l'octroi de brevets sur des règles
de calcul géométrique à IBM. Dans le cas présent, selon l'OEB, les
%(q:effets techniques au-delà...) consistent dans l'économie d'espace
sur l'écran d'ordinateur. Deux ans plus tard, l'OEB lui-même
%(ea:soulignait) que cette interprétation était déroutante mais
nécessaire pour des raisons politiques :

#Chr: Il faut noter que le Groupe de travail du Conseil a rejeté
%(4b:l'article 4 bis) du Parlement, qui aurait aidé à attribuer un
sens plus restrictif à la formulation de l'OEB, en s'appuyant sur une
récente %(bp:décision de la cour allemande) qui a considéré que
l'économie de ressources informatiques ne formait pas une
%(q:contribution technique), sinon pratiquement toutes les méthodes
d'affaires mises en oeuvre par ordinateur deviendraient sujettes à la
brevetabilité. Il est clair que le Groupe de travail du Conseil veut
rendre brevetable les algorithmes et les méthodes d'affaires %(q:mis
en oeuvre par ordinateur) conformément aux pratiques récentes de
l'OEB.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: ConsText0406 ;
# txtlang: xx ;
# End: ;

