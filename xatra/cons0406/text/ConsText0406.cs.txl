<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Předstíraná omezení patentovatelnosti v návrhu Rady

#descr: Pokusíme se předvést krátký přehled klamných triků v návrhu směrnice
Rady o patentovatelnosti vynálezů realizovaných na počítači z
18.5.2004.

#ute: Nejčastější rétorický trik v písemných materiálech Rady je tento

#inW: [A] není patentovatelné, ledaže je splněna [podmínka B].

#hnr: kde je při bližším zkoumání zjevné, že podmínka B je vždy splněna.

#WiW: Z důvodů časových a prostorových omezení, vybíráme pro ilustraci pouze
několik nejčastěji diskutovaných návrhů textu Rady. Více je možné
nalézt na stránce %(an:analýzy).

#cWi: Podmínka %(q:ledaže) je vždy splněna. Na jakýkoliv počítačový program
může být uplatněn patentový nárok jako na %(q:produkt nebo postup), a
v takovém případě může být patentový nárok vznesen také na program
jako takový, %(q:buď sám o sobě, nebo na nosiči).

#tub: Přesněji řečeno, nárok ve formě %(q:programu, charakterizovaný tím, že
[ dělá určité věci ]) bude udělen, a tento patentový nárok pokryje
nekonečné množství individuálně vytvořených programů podle autorského
práva.

#ehp: Program podle této klauzule nemusí být ani nový, ani objevný. Může to
být i konvenční popis nového procesu. Pokud někdo objeví nový chemický
postup, může popsat logiku programu, která jej popisuje (např. A+B=C),
a může zakázat zveřejnění jakéhokoliv programu, který používá tuto
logiku stejně jako jeho použití pro jakýkoliv účel, včetně simulace na
obecném výpočetním stroji.

#owi: Tato klauzule je středobodem práce Rady, a vznáší pochybnosti nad
kompetencí této %(q:pracovní skupiny) i na poli jejího úzkého zaměření
na patentový zákon. Více detailů naleznete v naší %(ca:analýze).

#vtt: Toto ustanovení je v přímém rozporu s odstavcem 2 článku 5.

#WWl: Byla představena na zasedání Rady komisařem Bolkesteinem v úspěšném
pokusu ošálit ministerské úředníky během zasedání.

#ahp: Laický čtenář čte tento odstavec tak, že není možné aby se práce
programátora dostala do sporu s patenty, protože na programy jako
takové není možné vznést patentový nárok. A skutečně německá
ministryně spravedlnosti Brigitte Zypriesová, působila předsvědčeně o
této interpretaci, když ji použila k rozptýlení obav programátorů v
rozhovoru deset dní po zasedání Rady.

#fnr: Zkušený čtenář chápe, že tato věta znamená opak toho, čemu Zypriesová
a ostatní vládní úředníci věří. Článek 52 Úmluvy o udělování
evropských patentů (EPC) touto klauzulí není zdůrazněn, ale učiněn
bezvýznamným.

#lpj: Počítačové programy nemohou být zároveň patentově nárokovatelné (čl. 5
odst. 2) a nepatentovatelné (čl. 4a odst. 1). Zmatený vykladač zákona
musí nalézt způsob jak tento rozpor vyřešit. Nalezne jej v následující
klauzuli.

#bue: Zde Bolkestein vložil redundantní doložku %(q:ať již jsou vyjádřené
jako zdrojový kód, cílový kód nebo mají jakoukoli jinou formu). Tato
klauzule je bezvýznamná, protože žádný žadatel o patent nebude nikdy
vznášet nárok úzce ve smyslu individuálního %(q:kódu) jednoho
programátora. Takto vymezený nárok by nestál poplatky za registraci
patentu, a nevyburcoval by žádný protest softwarové komunity. Jediný
důvod proč Bolkestein vložil tuto doložku bylo navrhnout interpretaci
pro klauzuli z čl. 4a odst. 1, která v důsledku interpretuje čl. 52
EPC. Podle této interpretace, čl. 52 míněn pouze k vyjmutí imaginárně
úzkých nároků, které nikdo nikd  nenárokoval a proti kterým nikdo
nikdy nevznesl připomínky. Tato interpretace je v rozporu c jasným
zněním čl. 52 EPC tak jak byl %(ep:obecně pochopen patentovými soudy),
a je %(fl:nepřípustný v běžné metodologii interpretace zákonů a
úmluv).

#ifb: Formulace %(q:běžná fyzická interakce mezi programem a počítačem)
znamená asi tolik jako  %(q:běžná fyzická interakce mezi kuchařem a
receptem):  to jest nic. Je to kouzelná formule jejíž použití může být
vyvozeno pouze z %(et:nedávných rozhodnutí Evropského patentového
úřadu (EPO)), ve kterých použila k ospravedlnění udělení patentu IBM
na pravidla pro geometrické výpočty. V aktuálním případě, v souladu s
EPO, %(q:technické účinky nad rámec ...) spočívaly v ekonomizaci
prostoru na obrazovce počítače. O dva roky později sám EPO
%(ea:upozornil), že tato konstrukce je matoucí, ale nutná z
politických důvodů:

#Chr: Mělo by být zdůrazněno, že pracovní skupina Rady zamítla parlamentní
%(4b:článek 4b), který by mohl napomoci k restriktivnější formulaci
výkladu EPO, založený na nedávném %(bp:rozhodnutí německého soudu),
který prohlásil, že ekonomizace výpočetních zdrojů nepředstavuje
%(q:technický přínos), protože jinak by prakticky všechny obchodní
metody realizované na počítači představovaly patentovatelný subjekt.
Je jasné, že pracovní skupina Rady chce umožnit patentovatelnost
algoritmů a obchodních postupů %(q:realizovaných na počítači) v
souladu s nedávnou praxí EPO.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatamendb ;
# dok: ConsText0406 ;
# txtlang: xx ;
# End: ;

