<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Apelo Urgente aos Parlamentos e Governos Nacionais

#descr: Os governos Europeus estão prestes a subscrever uma proposta de
directiva para patenteabilidade ilimitada e aplicação de patentes sem
entraves sobre modelos de negócio e algoritmos %(q:implementados num
computador). O acordo do Conselho de Ministros de 18/5/2004 descarta
todas as bem deliberadas decisões do Parlamento Europeu e dos órgãos
consultivos da UE sem qualquer justificação e sem legitimação
democrática. A maioria foi assegurada por um empactoamento enganador e
por questionáveis manobras diplomáticas na sessão decisora. Os
subscritores, que representam os líderes da inovação no software e da
discussão informada sovre a política de inovação na Europa, pedem aos
políticos responsáveis que puxem os travões de emergência e
reorganizem o processo de legislação da competitividade no Conselho.

#epn: Destinatários

#2ln: Líderes dos 25 governos e parlamentos, outros políticos envolvidos

#ujc: Assunto

#ati: Directiva de Patentes de Software: Apelo à reabertura da Discussão no
Conselho

#act: Estamos preocupados porque

#toe: Por estes motivos apelamos a que

#iar: Subscritores

#pee: Anexos

#aMe: A sessão de Competitividade de 18 de Maio de 2004 do Conselho atingiu
uma maioria qualificada para uma versão da directiva de patentes de
software 2002/0047 COM (COD) que irá impôr, sobre a Europa, a
patenteabilidade ilimitada e aplicação sem restrições de patentes
sobre modelos de negócio e algoritmos %(q:implementados num
computador). Existe um consenso geral entre economistas e
profissionais de software que tal regime, tão extremista quando nos
EUA, é desastroso para a inovação, concorrência e crescimento da
economia da informação.

#iwc: O texto proposto foi escrito de forma a enganar ministros sobre os
seus efeitos reais. Consiste em muitas frases do género de %(q:o
software é ... [ ênfase recheado de retórica ] ... não patenteável, a
menos que ... [ condição, que sob escrutínio, calha de ser sempre
verdadeira ]). Falsos limites deste género encontram-se por toda a
proposta e em particular nas provisões fulcrais que foram utilizadas
para persuadir os ministros.

#por: Os moderadores da sessão de Competitividade do Conselho forçaram os
participantes a aceitar uma proposta através de táticas de engodo,
pressão, e surpresa, tornando assim até questionável se uma maioria
válida terá sido atingida. Pode ser dito com certea que apenas uma
minoria dos governos realmente concorda com o que foi negociado, mas
muitos governos foram mal representados pelos seus negociadores, que
quebraram acordos intra-ministeriais ou mesmo violado instruções dos
seus superiores.

#isi: O Parlamento Europeu já rejeitou, com uma série de emendas, textos da
Comissão e do Comité JURI, que eram largamente idênticos em fraseado e
espírito ao que agora foi aprovado pelo Conselho. As emendas
reflectiam as exigências da vasta maioria dos inovadores de software e
investigadores sobre política de inovação da UE, incluindo os autores
de estudos encomendados pela Comissão bem como dos membros dos órgão
consultivos da UE.

#eWe: O Conselho ignorou e rejeitou todo o trabalho do Parlamento e dos
órgão consultivos da UE sem qualquer justificação e sem qualquer
legitimação democrática. O texto não é apresentado como meio de
atingir um objectivo político, mas como sendo um %(q:compromisso)
entre governos. Foi negociado sob um véu de segredo entre vários
oficiais ministeriais anónimos, a maioria dos quais responsáveis pelos
gabinetes nacionais de patentes e por isso parte da comunidade com
interesse explícito na patenteabilidade ilimitada.

#Wmh: peça à Presidência do Conselho que remova a proposta de directiva de
patentes de software (2002/0047 COM (COD)) da agenda da próxima sessão
do Conselho, onde aguarda a sua aprovação formal.

#doe: remova o dossier das mãos dos burocratas de patentes, e restaure o
verdadeiro escrutínio político ao impacto do texto proposto. A
designação dos representantes do grupo de trabalho do Conselho deve
ser apresentada publicamente e debatida (nos Parlamentos onde o
enquadramento institucional o permite).

#tln: apele a outros governos para que façam o mesmo e reformem
definitivamente o Conselho da UE para que se previnam de voltar a
acontecer catástrofes como esta no futuro.

#iin: Confederação Europeia das Associações de Pequenas e Médias Empresas

#cs5: 22 associações-membro de 19 países Europeus representando um total de
500.000 empresas.

#ich: Consórcio para Arquitecturas de Middleware Open Source

#alW: Entre os %(om:membros) encontram-se várias grandes companhias

#tdW: representa os interesses de 60000 apoiantes e 1000 companhias em
matérias de propriedade de software

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: deputado do Parlamento

#fum: Orador de IT, Média e Educação do Partido Social Democrático

#lWa: Senador Polaco

#aiW: Partido Social Democrático da Polónia

#csr: perito de TI do Partido Socialista, deputado do Parlamento

#VERT: Greens

#atP: candidato elegível para o Parlamento Europeu

#WaS: Partido dos Verdes Sueco

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: Mais subscritores

#gcr: %(N) pessoas já assinaram esta Proposta de Medidas através do
%(ei:sistema de participação da FFII).

#thW: O texto principal é submetido junto com a %(CA:Call for Action II -
Proposta de Medidas II) com logotipos dos subscritores. Mais ainda se
submetem os seguintes documentos em anexo:

#jet: carta da ObjectWeb

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de noticia esta carta

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

