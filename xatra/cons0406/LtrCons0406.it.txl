<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Richiesta urgente ai governi e ai Parlamenti

#descr: I governi europei stanno per firmare una proposta di direttiva per una
brevettablità illimitata e senza restrizioni di algoritmi e metodi
commerciali %(q:implementanti per mezzo del computer). L'accordo del
Consiglio dei Ministri del 18 Maggio 2004 elimina, senza alcuna
giustificazione e senza legittimazione democratica, decisioni
correttamente deliberate dal Parlamento Europeo e dagli organi
consultivi dell'UE. La maggioranza è stata ottenuta attraverso
accorpamenti depistanti e manovre diplomatiche dubbie nella sessione
decisiva. I firmatari, che rappresentano i leader dell'innovazione nel
campo del software e delle discussioni informate nelle politiche di
innovazione nel campo del softwrae in Europa, chiedono ai politici
responsabili di tirare il freno di emergenza e riorganizzare il
processo di legislazione sulla competitività nel Consiglio.

#epn: Destinatari

#2ln: I capi di governo e i parlamentari dei 25 paesi, altri polici
interessati

#ujc: Oggetto

#ati: Direttiva sui brevetti software: Richiesta di riapertura della
Discussione in Consiglio

#act: Siamo preoccupati che

#toe: Per queste ragioni vi chiediamo urgentmente di

#iar: Firmatari

#pee: Appendici

#aMe: a sessione del 18 Maggio 2004 del Consiglio Competitività ha raggiunto
la maggiornaza qualificata per una versione della direttiva sui
brevetti software 2002/0047 COM (COD) che imporrebbe in Europa una
brevettabilità illimitata e senza restrizioni di algoritmi e metodi
commerciali %(q:implementati per mezzo del computer). C'è consenso
generale tra gli economisti e professionisti del software che tale
regime, come già esiste negli USA, è disastroso per l'innovazione, la
competizione e la crescita di una economia dell'informazione.

#iwc: Il testo proposto è scritto per ingannare i ministri a proposito degli
effetti reali che produrrebbe. Consiste di molte frase nella forma
%(q:il software è ... [testo enfatizzato da artifici retorici] ... non
brevettabile, a meno che ... [condizione, che dopo dettagliata
ispezione, risulta essere sempre vera]). Falsi limiti di questo tipo
pervadono il testo della proposta e specialmente i provvedimenti
cruciali usati per convincere i ministri

#por: I moderatori della sessione del Consiglio Competitività hanno spinto i
partecipanti ad accettare la proposta attraverso raggiri, pressioni e
uscite a sorpresa, rendendo anche dubbio se in effetti una reale
maggioranza sia stata ottenuta. È certo che solo una minoranza di
governi è realmente d'accordo con ciò che è stato negoziato, ma molti
governi sono stati malrappresentati dai propri negoziatori, che hanno
violato accordi inter-ministeriali o addirittura hanno disobbedito
alle direttive dei propri superiori

#isi: la proposta del Consiglio è quasi identica nel fraseggio e nello
spirito al testo della Commissione e del comitato JURI, testo che il
Parlamento Europeo ha già rigettato attraverso una serie di
emendamenti. Gli emendamenti del Parlamento riflettono le richieste
della rande maggioranza degli innovatori del software e dei
ricercatori sulle politiche dell'innovazione nell'UE, includendo
autori di studi ordinati dalla Commissione sia membri di organi
consultivi dell'UE

#eWe: Il Consiglio ha ignorato e rigettato tutto il lavoro del Parlamento e
degli organi consultivi dell'UE senza alcuna giustificazione e senza
legittimità democratica. Il testo non è stato presentato come mezzo
per raggiungere un obiettivo politico, ma piuttosto come un
%(q:compromesso) tra i governi. È stato negoziato dietro un velo di
segretezza tra rappresentanti ministeriali anonimi, la maggiorparte
dei quali hanno anche l'incarico di governare gli uffici brevetti
nazionali e quindi parte di una cumnità che ha diretti interessi in
ottenere brevettabilità illimitata.

#Wmh: chiedere alla Presidenza del Consiglio di eliminare dall'agenda della
prossima sessione del Consiglio il voto sulla direttiva sulla
brevettabilitò del software (2002/0047 COM (COD)) dove è in attesa di
approvazione formale.

#doe: strappare il dossier dalle mani della burocrazia brevettuale e
ripristinare un serio scrutinio politico dell'impatto del testo
proposto. La nomina dei rappresentanti nel gruppo di lavoro del
Consiglio dovrebbe essere pubblicamente presentata e dibattuta (nei
Parlamenti in quegli ordinamenti che lo permettono).

#tln: spingere altri governi a fare lo stesso ad infine riformare il
Consiglio UE in modo da scongiurare che incresciosi avvenimenti come
questo si ripresentino in futuro.

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 associazioni membre di 19 paesi europei che rappresentano in totale
più di 500,000 aziende.

#ich: Consortium for Open Source Middleware Architectures

#alW: Tra i %(om:membri) vi sono molte grosse aziende

#tdW: rappresenta gli interessi di 60000 sostenitori e 1000 aziende sui temi
della proprietà del software

#Wro: Associazione di produttori polacchi di software. La maggioranza dei
membri produce software proprietario. Cooperano con SIIA e altre
attività anti-pirateria.

#Wlp: Associazione di produttori portoghrsi di software. La maggiorparte dei
membri crea e vende software proprietario. Organizzano campagne contro
la pirateria del software in stretta cooperazione con SIIA e BSA.

#MemP: membro del Parlamento

#fum: Portavoce per l'IT i media e l'educazione del Parito Social
Democratico

#lWa: Senatore Polacco

#aiW: Parito Social Democratico di Polonia

#csr: esperto IT del Partito Socialista

#VERT: Verdi

#atP: primo candidato per il Parlamento europeo

#WaS: Partito Verdi svedese

#MEPC: candidato MPE

#fae: uno dei più grandi sindacati spagnoli

#tSW: libre software association of Spain, con più di 7,500 membri

#apu: la più grande associazione spagnole di professionisti informatici,
circa 5,000 membri

#Wtr: Associazione spagnola di utenti e professionisti di Internet

#nGs: un sindacato dei lavoratori di Grupo Telefonica, la più grane azienda
telefonica spagnola

#hpi: Gruppo di lavoro spagnolo sull'innovazione nel campo del software

#0me: 5000 membri

#eap: azienda produttrice di videogame

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: alre firme

#gcr: %(N) persone hanno finora firmato l'appello attraverso %(ps:il sistema
di partecipazione di FFII).

#thW: Il testo viene inviato insieme con la %(CA:Richiesta di Azione II) con
loghi delle firme. In aggiunta vengono inviate le seguneti appendici.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: simos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: it ;
# multlin: t ;
# End: ;

