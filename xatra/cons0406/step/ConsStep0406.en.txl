<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Steps Out of the European Software Patent Deadlock

#descr: FFII proposes a sequence of basic steps need to be undertaken if the
European Software Patent Directive is to get anywhere.  The most basic
ones consist in reverting some deteriorations that were introduced by
the Council's Patent Policy Working Group during the last weeks and
months before the Political Agreement of 2004-05-18 and that turn this
version of the directive into the most uncompromisingly
pro-software-patent version that has ever been seen during the 10
years of the directive-formulation process.

#rWa: Current situation

#qeS: Sequence of Steps

#iWe: The processes in the Council are dominated by people from the patent
institutions who are prepared to kill the directive project.  They now
see it as very unlikely that they can obtain a rubber stamp from the
European Parliament for a directive that allows their network
(administrative council of the EPO, patent departemnts of large
corporations) to go on setting the rules as they please.  In that case
they prefer to drive the project against the wall.  Above anything,
they fear being faced with discussions.

#re0: They have almost succeded in driving the directive against the wall by
means of a %(cs:Political Agreement on a Common Position of the
Commission and Council, reached on 2004-05-18 by an %(pe:un)qualified
majority).

#ypo: But the agreement has not yet been enacted and the majority is in fact
unqualified, in spite of efforts on the part of the Presidency and
Commission to prevent this from becoming apparent.

#WlW: If governments want to get out of the deadlock and secure a future for
the directive project and for a rational legislative process in the
interest of Europe's citizens and industry, it might want to orient
itself by the following steps.

#WWA: delete %(q:magical) wordings %(q:either as source code or object code)
that were inserted by Bolkestein into Art 4A in the last minute.

#atr: Reason: Nobody wants patents on individual expressions or singular
instances of programs.  These insertions serve no regulatory purpose
but rather impose an interpretation on Art 52 EPC which makes the law
meaningless and, probably for that reason, is rejected by the German
Federal Court and even by the EPO.  Delete such wordings also from
recitals where they occur.  The directive should not reinterpret the
EPC in a way that does not conform to the %(ex:normal rules of
interpretation of law).

#lrW: delete Article 5(2).

#eeW: Reason: The double negations are deceptive.  This article makes
software on its own patentable.  What is claimed must be what is
invented.  If a %(q:program, characterised by ...) is claimed, then
nothing more than a program (as such) was invented.  If an information
structure can be a patentable invention, then there is no point in
talking any more about definitions of %(q:technical).  The European
Commission refused program claims in its proposal of 2002 for good
reasons.  Allowing them closes the door to all discussions and would
therefore mean that the Council intends to kill the directive project.

#leW: Delete recital 17.

#Woo: This recital says that a right to interoperate can be secured only
through antitrust procedings.  This again removes room for
negotiations and represents a deterioration of the directive in
comparison to the Commission's version of 2002.

#oaA: Define key terminology, concretise %(TRIPs)

#itd: This approach, found in several provisions of Art 2 and 3, was
supported several delegations in the Council and by the Bundestag.  
Interpretable concepts such as %(q:technical), %(q:invention) etc may
be most appropriate at the WTO treaty level, but need concretisation
at the level of legislation, especially when the claimed purpose is to
clarify.  The %(q:forces of nature) definitions have a proven legal
theory and practise to back them.

#per: Support the Interoperability Privilege.

#Wte: Article 6a received the support of all three committees of the
European Parliament and the plenary as well as the nearly the whole
ICT industry, including some large companies such as Sun Microsystems
which normally let their patent departments formulate patent policies
on their behalf.  It is also needed to concretise Art 30 TRIPs.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: phm@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsStep0406 ;
# txtlang: xx ;
# End: ;

