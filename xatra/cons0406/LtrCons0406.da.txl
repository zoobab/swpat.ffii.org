<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Vigtig appel til Europas regeringer og folketing

#descr: Europas regeringer er i færd med at godkende et direktivforslag  som
leder til ubegrænset patenterbarhed og uindskrænket håndhævelse af 
patenter på "computer-implementerede" algoritmer og
forretningsmetoder.  Ministerrådets aftale den 18. Maj 2004 avfærdiger
uden berettigelse og  uden demokratisk legitimitet de velovervejede
beslutninger, som  Europa-parlamentet og EU's rådgivende organ har
taget. Flertallet blev  opnået ved hjælp af vildledende forklaringer
og tvivlsomme diplomatiske  manøvrer ved det beslutningstagende møde.
Underskriverne, som  repræsenterer ledende kræfter indenfor
software-udvikling og  diskussionsdeltagere med indsigt i
innovationspolitik, beder de  ansvarlige politikere om at trække i
nødbremsen og om at omorganisere  processen for konkurrencelovgivning
i Rådet.

#epn: Modtagere

#2ln: Repræsentanter for 25 regeringer og parlamentet, samt andre berørte 
politikere

#ujc: Emne

#ati: Direktivet om patenter på software: ønske om at genoptage 
diskussionen i Rådet

#act: Vi noterer at

#toe: Af disse grunde anmoder vi regeringer og folkevalgte lovgivende 
forsamlinger om at

#iar: Undertegnere

#pee: Bilag

#aMe: På ministerrådsmødet for konkurrenceevne den 18. Maj 2004 opnådes  et
kvalificeret flertal for en version af software-patentdirektivet 
2002/0047 COM (COD) som ville påføre Europa ubegrænset patenterbarhed
og     uindskrænket håndhævelse af patenter for
"computer-implementerede"  algoritmer og forretningsmetoder. Det er en
generel enighed blandt  økonomer og softwareudviklere om at et sådant
regime, som eksisterer i  USA, er katastrofalt for innovation,
konkurrence og vækst i den  informationsbaserede økonomi.

#iwc: Den foreslåede tekst er udformet for at vildlede ministre om de 
egentlige virkninger den vil få. Den består af mange sætninger af
formen  "software er ... [ retorisk opblæst understregning ] ...
upatenterbar,  med mindre ... [ betingelse, som ved nærmere eftersyn
viser sig altid at  være sand ]". Falske begrænsninger af denne type
gennemsyrer forslaget  og særlig de centrale artikler som blev brugt
til at overtale ministrene

#por: På ministerrådsmødet blev forslaget forceret igennem af  moderatorene
ved hjælp af vildledning, pres og overraskelsestaktik,  hvilket gør
det tvivlsomt om et gyldig flertal blev opnået. Det kan  siges med
sikkerhed at kun et mindretal af regeringerne er virkelig  enige i
det, som blev forhandlingens resultat, men flere regeringer blev    
misrepræsenteret af deres forhandlere, som brød mellem-departementale 
aftaler og endog brød instruksene fra deres overordnede.

#isi: Rådets forslag er stort set identisk i ordlyd og ånd med teksten  fra
Kommisionen og JURI-udvalget, som Europa-parlamentet allerede har 
afvist med en række rettelser. Parlementets rettelser reflekterede 
kravene fra størstedelen af software udviklere samt forskere på 
innovationspolitik-området i EU, heriblandt forfatterne af studier 
bestilt af Kommissionen såvel som medlemmer af EU's rådgivende organer

#eWe: Rådet har ignoreret og afvist alt arbejdet fra Parlamentet og de 
rådgivende organer i EU, uden begrundelse eller demokratisk 
legitimering. Rådets tekst blev ikke beskrevet som et middel for at
opnå  et politisk mål, men snarere som et "kompromis" mellem
regeringer.  Rådets tekst blev forhandlet under et slør af
hemmeligholdelse mellem  anonyme departementsansatte embedsmænd,
hvoraf de fleste har ansvaret  for driften af nationale patentkontorer
og derved er en del af et miljø  med en økonomisk interesse i
uindskrænket patentbarhed.

#Wmh: anmode Rådets formandskab om at slette afstemningen om 
software-patentdirektivet (2002/0047 COM (COD)) fra dagsorden for
Rådets  næste session, hvor den afventer formel godkendelse.

#doe: tage sagspapirene ud af patentbureaukratiets hænder, og genindføre  en
reel politisk gennemgang af følgerne af den foreslåede tekst. 
Udnævnelsen af repræsentanter i Rådets arbejdsudvalg bør gøres kendt
og  debatteres offentligt (i Parlamenter hvor de institutionelle
rammer  tillader det).

#tln: opfordre andre regeringer om at gøre det samme og på længere sigt 
reformere EU-rådet for at hindre lignende katastrofer i at gentage sig
i  fremtiden.

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 member associations from 19 European countries representing in
total more than 500,000 enterprises.

#ich: Consortium for Open Source Middleware Architectures

#alW: Among the %(om:members) are many large companies

#tdW: represents interests of 60000 supporters and 1000 companies on matters
of software property

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: member of Parliament

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Senator

#aiW: Social Democratic Party

#csr: IT expert of Socialist Party

#VERT: Greens

#atP: top candidate for the European Parliament

#WaS: The Green Party of Sweden

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#thW: The main text is submitted together with the %(CA:Call for Action II)
with logos of signatories.  In addition, the following annex documents
are submitted.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: da ;
# multlin: t ;
# End: ;

