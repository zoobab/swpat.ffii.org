<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Dringende Oproep aan Nationale Regeringen en Parlementen

#descr: Europa's regeringen staan op het punt om goedkeuring te geven aan een
voorstel tot richtlijn voor onbeperkte octrooieerbaarheid en
grenzeloze afdwingbaarheid van octrooien op %(q:in computers
geïmplementeerde) algoritmen en methoden voor bedrijfsvoering. De
overeenkomst van de Raad van Ministers van 2004-05-18 gooit alle
weloverwogen beslissingen van het Europees Parlement en de
consultatieve organen van de EU weg zonder enige rechtvaardiging, en
zonder democratische legitimatie. De meerderheid werd bekomen door een
misleidende vorm en door bedenkelijke diplomatische maneuvers tijdens
de beslissende sessie. De ondergetekenden, die de top
vertegenwoordigen wat betreft de Europese software-innovatie en
geïnformeerde besprekingen van haar beleid, vragen de
verantwoordelijke politici om aan de noodrem te trekken en om het
proces van de mededingingswetgeving in de Raad te reorganiseren.

#epn: Geadresseerden

#2ln: De hoofden van 25 regeringen en parlementen, andere bezorgde politici

#ujc: Onderwerp

#ati: Richtlijn over Softwarepatenten: Vraag om de Discussies in de Raad te
heropenen

#repr

#step

#parl

#cfax: zie bijlage %1

#act: We zijn bezorgd omdat

#aMe: In de sessie van 18 mei 2004 van de Raad voor Mededinging werd een
gekwalificeerde meerderheid gevonden voor een versie van de
softwarepatentenrichtlijn 2002/0047 COM (COD) die onbeperkte
octrooieerbaarheid en grenzeloze afdwingbaarheid oplegt aan Europa wat
betreft %(q:in computers geïmplementeerde) algoritmen en methoden voor
bedrijfsvoering. Er is een algemene consensus onder economen en
softwareprofessionals dat een dergelijk regime, zoals dat bestaat in
de VS, rampzalig is voor innovatie, concurrentie en groei van de
informatie-gebaseerde economie.

#iwc: De voorgestelde tekst is ontworpen om ministers te misleiden over hare
juist gevolgen. Hij bestaat uit vele zinnen van de vorm %(q: software
is ... [ retorische nadruk ] ... niet octrooieerbaar, tenzij ... [
voorwaarde, die bij nadere studie altijd waar blijkt te zijn ]). Valse
beperkingen van dit type vindt men overal terug in het voorstel, maar
vooral in de centrale voorwaarden die gebruikt werden om de ministers
te overhalen.

#por: De moderators van de Raad voor Mededinging pushten de deelnemers om
het voorstel aan te nemen, gebruik makend van misleiding, druk en
verrassingstaktieken, waardoor het zelfs twijfelachtig is of er wel
een geldige meerderheid bekomen werd. Het kan met zekerheid gezegd
worden dat slechts een minderheid van de regeringen echt akkoord is
met wat onderhandeld werd, maar verschillende regeringen werden
verkeerd vertegenwoordigd door hun onderhandelaars, dewelke
inter-mininsteriële overeenkomsten verbraken of zelfs instructies van
hun superieuren negeerden.

#isi: Het Voorstel van de Raad is zowel qua verwoording als geest
grotendeels gelijk aan de teksten van de Commissie en de JURI
Commissie, die door het Europees Parlement reeds verworpen zijn d.m.v.
een reeks amendementen. Deze amendementen gaven de eisen weer van de
overgrote meerderheid van de Europese software-innovators en
onderzoekers i.v.m. het innovatiebeleid in de EU, inclusief deze van
de de auteurs van studies besteld door de Commissie en van de leden
van de raadgevende organen van de EU.

#eWe: De Raad heeft al het werk van het Parlement en de consultatieve
organen van de EU genegeerd en verworpen, zonder ook maar enige
rechtvaardiging of democratische legitimatie. De tekst wordt niet
voorgesteld als een middel om een bepaald beleidsobjectief te halen,
maar eerder als een %(q:compromis) tussen regeringen. Hij werd
onderhandeld onder een sluier van geheimhouding tussen anonieme
ambtenaren van ministeries, waarvan de meesten tot de leiding van de
nationale octrooibureaus behoren en daardoor gevestigde belangen
hebben in onbeperkte octrooieerbaarheid.

#toe: Om deze redenen dringen wij aan dat

#Wmh: het Voorzitterschap van de Raad de stemming over de
softwarepatentenrichtlijn (2002/0047 COM (COD)) van de agenda van de
volgende Raadssessie haalt, waar hij wacht op formele goedkeuring.

#doe: het dossier uit handen wordt genomen van de octrooibureaucratie, en om
echt nauwkeurig politiek onderzoek van de gevolgen van de voorgestelde
tekst in ere te herstellen. De aanduiding van de vertegenwoordigers in
de Werkgroep van de Raad zou publiek weergegeven en gedebatteerd
moeten worden (in Parlementen daar waar de institutionele omkadering
dit toelaat).

#tln: andere regeringen hetzelfde doen, met als uiteindelijk doel de EU Raad
te hervormen om catastrofen zoals deze te vermijden in de toekomst.

#iar: Ondertekenaars

#iin: Confédération Européenne des Associations de Petites et Moyennes
Entreprises

#cs5: Belangenvereniging bestaande uit 22 verenigingen uit 19 Europese
landen die samen meer dan 500,000 ondernemingen vertegenwoordigen.

#ich: Consortium for Open Source Middleware Architectures

#alW: Onder haar %(om:leden) bevinden zich veel grote bedrijven.

#tdW: vertegenwoordigt de belangen van 60,000 steunbetuigers en 1000
bedrijven op gebied van software-eigendom.

#Wro: Belangenvereniging van Poolse software-ontwikkelaars. Bijna alle leden
maken en verkopen gesloten software. Ze werken samen met SIIA en
andere activiteiten tegen softwarepiraterij.

#Wlp: Belangenvereniging van Portugese software-ontwikkelaars. Bijna alle
leden maken en verkopen gesloten software. Ze organiseren campagnes
tegen softwarepiraterij in nauwe samenwerking met SIIA en de BSA.

#MemP: Parlementslid

#fum: Woordvoerder voor IT, Media en Onderwijs van de Sociaal Democratische
Partij

#lWa: Poolse Senator

#aiW: Sociaal Democratische Partij van Polen

#csr: IT-expert van de Socialistische Partij

#delegit: Nationaal afgevaardigde voor informatietechnolgieën

#socpart: Socialistische Partij

#VERT: Groenen

#atP: Topkandidaat voor het Europees Parlement

#WaS: De Groene Partij van Zweden

#MEPC: kandidaat-MEP

#fae: Eén van Spanje's grootste vakbonden

#tSW: Vrije-softwarevereniging van Spanje, met meer dan 7,500 leden

#apu: Grootste Spaanse vereniging van computerprofessionals, ongeveer 5.000
leden

#Wtr: Spaanse vereniging van Internetgebruikers en -professionals

#nGs: Een vakbond van werknemers van Grupo Telefonica, Spanje's grootste
telecombedrijf

#hpi: Een Spaanse werkgroep over software-innovatie

#0me: 5.000 leden

#eap: Bedrijf dat computerspelletjes maakt.

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: Meer handtekeningen

#gcr: %(N) personen hebben tot nu toe deze oproep getekend via het %(ps:FFII
deelnemerssysteem).

#pee: Bijlagen

#thW: De hoofdtekst wordt afgegeven samen met de %(CA:Oproep tot Handelen
II) met logo's van ondertekenaars. Bijkomend worden volgende bijlagen
afgegeven:

#jet: ObjectWeb brief

#nPr: Minimale PDF versie

#tuW: Een mooi geformatieerde versie van deze Dringende Oproep  zonder
referenties naar bijlagen, om snel te verzenden (b.v. via fax)

#sfR: Offensief tegen de softwarepatentenrichtlijn

#eaW: Heise.de maakt melding van deze brief

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: jmaebe ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

