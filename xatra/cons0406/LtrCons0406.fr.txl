<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Appel urgent aux gouvernements et parlements nationaux

#descr: Les gouvernements de l'Union européenne (UE) sont sur le point de
signer une proposition de directive sur une brevetabilité illimitée et
une application sans entraves des brevets sur les algorithmes et
méthodes de gestion %(q:mis en oeuvre par ordinateur). L'accord obtenu
par le Conseil des ministres du 18 mai 2004 écarte les décisions du
Parlement européen et des organes consultatifs de l'UE sans aucune
justification et sans légitimité démocratique. La majorité a été
atteinte au Conseil grâce à une proposition présentée dans un
emballage trompeur et des manoeuvres diplomatiques discutables lors de
la session décisive. Les signataires de cette lettre, représentant les
chefs de file de l'innovation en matière de logiciel, parfaitement au
fait des discussions sur les politiques d'innovation informatique en
Europe, demandent aux responsables politiques d'activer l'arrêt
d'urgence et de réorganiser la procédure d  e législation sur la
compétitivitÊ ɠau sein du Conseil.

#epn: Destinataires

#2ln: Chefs des 25 gouvernements de l'UE et des parlements nationaux, autres
responsables politiques concernés

#ujc: Objet

#ati: Directive sur les brevets logiciels : Demande de réouverture des
discussions au Conseil de l'UE

#repr: Qualifié?

#step: Sortie

#parl: Parlement

#cfax: voire annexe %1

#act: Nous sommes concernés par le fait que

#aMe: La session du 18 mai 2004 du Conseil sur la compétitivité est parvenue
à obtenir une majorité qualifiée pour une version de la directive
2002/0047 COM (COD) sur les brevets logiciels qui imposerait une
brevetabilité illimitée et une application sans entraves des brevets
sur les algorithmes et méthodes de gestion %(q:mis en oeuvre par
ordinateur). Il existe un consensus général parmi les économistes et
les professionnels du logiciel sur le fait qu'un tel régime, comme il
en existe aux USA, serait désastreux pour l'innovation, la concurrence
et la croissance de l'économie informationnelle.

#iwc: Le texte proposé est conçu pour induire en erreur les ministres sur
ses effets véritables. Il est rempli de phrases de la forme  %(q:le
logiciel est... [ emphase rhétoriquement mise en avant ]... non
brevetable, à moins que... [ condition, qui, lorsqu'on l'examine
attentivement, s'avère être toujours vraie ]). La proposition est
infestée de fausses limites de ce type et particulièrement dans ses
dispositions centrales, qui ont été employées pour convaincre les
ministres.

#por: Les présidents de séance du Conseil sur la compétitivité ont poussé
les participants à accepter la proposition avec des manoeuvres
trompeuses et pressantes les prenant par surprise, rendant même ainsi
discutable la certitude qu'une majorité valide avait été obtenue. On
peut affirmer avec certitude que seule une minorité de gouvernements a
réellement accepté ce qui était négocié, mais que plusieurs
gouvernements étaient mal représentés par leurs négociateurs, qui ont
rompu des accords intra-ministériels ou ont même enfreint les
consignes de leurs supérieurs.

#isi: La proposition du Conseil est largement identique dans la formulation
et sur le fond aux textes de la Commission européenne et de la
commission parlementaire à la justice (JURI), que le Parlement
européen a déjà rejetés, à travers une série d'amendements, Les
amendements du Parlement reflétaient les demandes d'une vaste majorité
d'innovateurs informatiques et de chercheurs en politique de
l'innovation, comprenant les auteurs d'études commandées par la
Commission, ainsi que l'avis des organes consultatifs de l'UE.

#eWe: Le Conseil a ignoré et rejeté l'intégralité du travail effectué par le
Parlement et les organes consultatifs de l'UE sans aucune
justification et sans légitimité démocratique. Le texte n'est pas
présenté comme un moyen d'accomplir un quelconque objectif politique,
mais plutôt comme un %(q:compromis) entre les gouvernements. Il a été
négocié sous le voile du secret par des fonctionnaires ministériels
anonymes, dont la plupart sont responsables de la gestion des offices
de brevets nationaux et ont de ce fait un intérêt acquis à une
brevetabilité illimitée.

#toe: Pour ces raisons, nous vous demandons expressément

#Wmh: de demander à la Présidence du Conseil de retirer le vote concernant
la directive sur les brevets logiciels (2002/0047 COM (COD)) de
l'agenda de la prochaine session du Conseil où elle s'apprête à
recevoir une approbation formelle.

#doe: de retirer le dossier des mains de la bureaucratie des brevets et de
restaurer un véritable examen politique minutieux sur les impacts du
texte proposé. La désignation des représentants au Groupe de travail
du Conseil devrait être publiquement présentée et débattue (au sein
des parlements où le cadre institutionnel le permet).

#tln: d'inviter les autres gouvernements à faire de même et en dernier lieu
à réformer le Conseil de l'UE pour que des catastrophes comme celle-ci
ne puissent se reproduire à l'avenir.

#iar: Signataires

#iin: Confédération européenne des associations de petites et moyennes
entreprises

#cs5: 22 associations membres, issue de 19 pays européens, représentant un
total de plus de 500 000 entreprises.

#ich: Consortium pour une architecture d'intergiciels libres

#alW: Compte parmi ses %(om:membres) de grandes entreprises

#tdW: représente les intérêts de 60 000 partisans et de 1000 entreprises sur
la question de la propriété du logiciel

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: membre du parlement

#fum: Porte parole pour le Parti Social-démocrate sur les technologies de
l'information, les médias et l'éducation

#lWa: Sénateur polonais

#aiW: Parti Social-démocrate de Pologne

#csr: Expert en technologies de l'information pour le Parti Socialiste

#delegit: délégué national aux technologies de l'information

#socpart: Parti Socialiste

#VERT: Verts/ALE

#atP: candidat tête de liste au Parlement européen

#WaS: Parti des Verts de Suède

#MEPC: Candidat aux élections européennes

#fae: l'un des plus importants syndicats en Espagne

#tSW: association espagnole pour le logiciel libre, comptant plus de 7 500
membres

#apu: plus importante association espagnole de professionnels de
l'informatique, environ 5 000 membres

#Wtr: Association espagnole d'utilisateurs et de professionnels d'Internet

#nGs: syndicat des travailleurs de Grupo Telefonica, plus importante
entreprise espagnole de poste et télécommunications

#hpi: Groupe de travail espagnol sur l'innovation informatique

#0me: 5000 membres

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: Autres signataires

#gcr: %(N) personnes ont d'ores et déjà signé cet appel via le  
%(ps:système de participation de la FFII).

#pee: Annexes

#thW: Le texte principal est envoyé accompagné de %(CA:l'Appel à l'action
II) avec les logos des signataires. En outre, les documents suivants
sont mis en annexe.

#jet: Lettre d'ObjectWeb

#nPr: Version PDF minimale

#tuW: Une version proprement formatée de cet Appel urgent, sans références
aux documents annexes, pour un envoi rapide (ex. par fax)

#sfR: Offensive contre la directive sur les brevets logiciels

#eaW: Heise.de (site allemand d'informations) a mentionné cette lettre

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

