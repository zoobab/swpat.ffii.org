<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Urgent Call to National Governments and Parliaments

#descr: Europe's governments are about to put their stamps under a directive
proposal for unlimited patentability and unfettered patent enforcement
of %(q:computer-implemented) algorithms and business methods.  The
agreement by the Council of Ministers of 2004-05-18 discards
well-deliberated decisions of the European Parliament and the EU's
consultative organs without any justification and without democratic
legitimation.  The majority was secured by deceptive packaging and by
questionable diplomatic maneuvering at the decisive session.  The
undersigned, who represent the leaders of software innovation and
informed discussion on software innovation policy in Europe, ask the
responsible politicians to pull the emergency brake and to reorganise
the process of competitivity legislation in the Council.

#epn: Recipients

#2ln: Heads of 25 governments and parliaments, other concerned politicians

#ujc: Subject

#ati: Software Patent Directive: Request to reopen Council Discussion

#repr: Qualified?

#step: Way Out

#parl: Parliament

#cfax: see annex %1

#act: We are concerned that

#aMe: The Competitiveness Council session of 18 May 2004 reached a qualified
majority for a version of the software patent directive 2002/0047 COM
(COD) that would impose unlimited patentability and unfettered patent
enforcement of %(q:computer-implemented) algorithms and business
methods on Europe.  There is a general consensus among economists and
software professionals that such a regime, as exists in the USA, is
disastrous for innovation, competition and growth of the
information-based economy.

#iwc: The proposed text is designed to mislead ministers about its real
effects.  It consists of many sentences of the form or %(q:software is
... [ rhetorically bloated emphasis ] ... unpatentable, unless ... [
condition, which, upon closer scrutiny, turns to be always true ]). 
Fake limits of this type pervade the proposal and especially the
central provisions which were used for persuading the ministers

#por: The moderators of the Competitiveness Council session pushed the
participants toward accepting the proposal by deception, pressure and
surprise tactics, thus even making it questionable whether a valid
majority was achieved.  It can be said with certainty that only a
minority of governments really agrees with what was negotiated, but
several governments were misrepresented by their negotiators, who
broke intra-ministerial agreements or even violated instructions from
their superiors

#isi: the Council's proposal is largely identical in wording and spirit to
the texts from the Commission and the JURI Committee, which the
European Parliament has already rejected by means of a series of
amendments.  The Parliament's amendments reflected the demands of the
vast majority of software innovators and innnovation policy
researchers in the EU, including the authors of studies ordered by the
Commisson as well as the members of the EU's consultative organs

#eWe: The Council has ignored and rejected all the work of the Parliament
and the consultative organs of the EU without any justification and
without democratic legitimation.  The text is not presented as a means
of achieving any policy objective, but rather as a %(q:compromise)
between governments.  It was negotiated under a veil of secrecy
between anonymous ministerial officials,  most of whom are in charge
of running national patent offices and thereby part of a community
with a vested interest in unlimited patentability.

#toe: For these reasons we urge you to

#Wmh: ask the Council Presidency to withdraw the voting on the software
patent directive (2002/0047 COM (COD)) from the agenda of the next
Council session where it is awaiting formal approval.

#doe: take the dossier out of the hands of the patent bureaucracy, and
restore true political scrutiny of the impact of the proposed text.
The designation of representatives in the Council working party should
be publicly presented and debated (in Parliaments where the
institutional framework allows).

#tln: urge other governments to do the same and ultimately to reform the EU
Council so as to prevent catastrophes such as the present one from
happening in the future.

#iar: Signatories

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 member associations from 19 European countries representing in
total more than 500,000 enterprises.

#ich: Consortium for Open Source Middleware Architectures

#alW: Among the %(om:members) are many large companies

#tdW: represents interests of 60000 supporters and 1000 companies on matters
of software property

#Wro: Association of Polish Software producers.  Almost all members create
and sell copyrighted proprietary software. They cooperate with SIIA
and other anti-piracy activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: member of Parliament

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Polish Senator

#aiW: Social Democratic Party of Poland

#csr: IT expert of Socialist Party

#delegit: National delegate on information technology

#socpart: Socialist Party

#VERT: Greens/EFA in the European Parliament

#atP: top candidate for the European Parliament

#WaS: The Green Party of Sweden

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#pee: Annexes

#thW: The main text is submitted together with the %(CA:Call for Action II)
with logos of signatories.  In addition, the following annex documents
are submitted.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

