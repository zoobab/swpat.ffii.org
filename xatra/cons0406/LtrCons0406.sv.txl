<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Br夳kande upprop till Europas regeringar och parlament.

#descr: Europas regeringar står i begrepp att godkänna ett  direktivförslag
som innebär obegränsad patenterbarhet och oinskränkt  giltighet för
patent på "datorrelaterade" algoritmer och affärsmetoder. 
Ministerrådets överenskommelse per 18 maj 2004 avfärdar utan skäl och 
utan demokratisk legitimitet de väl övervägda beslut europaparlamentet
 och EUs rådgivande organ tagit. Genom vilseledande omskrivningar och 
tvivelaktiga diplomatiska manövrar uppnåddes majoritet vid den
avgörande  sessionen. Undertecknarna av detta upprop, som består av
ledande  representanter för innovation och innovationspolitik inom 
programvaruområdet i Europa, ber de ansvariga politikerna att dra i 
nödbromsen och att omorganisera processen för konkurrenslagstiftning i
 rådet.

#epn: Mottagare

#2ln: Företrädare för 25 regeringar och parlament och andra berörda
politiker.

#ujc: Ämne

#ati: Direktivet om patent på programvara: Hemställan om att återuppta
diskussionen i rådet.

#act: Vi noterar att

#toe: Vi uppmanar er därför att

#iar: Undertecknare

#pee: Bilagor

#aMe: Vid ministerrådsmötet för konkurrenskraft den 18 maj 2004 uppnåddes   
 en kvalificerad majoritet för mjukvarupatentdirektivet 2002/0047 COM
(COD) i en version som innebär obegränsad patenterbarhet och
oinskränkt giltighet för "datorrelaterade" algoritmer och
affärsmetoder i Europa.. Det råder stor enighet bland ekonomer och
utvecklare av mjukvara att en sådan lagstiftning, liknande den i USA,
är till skada för innovation, konkurrens och tillväxt inom den
informationsbaserade ekonomin.

#iwc: Den föreslagna texten är avsiktligt konstruerad för att vilseleda
ministrarna om dess verkliga innebörd. Den består av ett flertal
meningar på formen "mjukvara är ... [retoriskt uppblåsta fraser] ....
icke patenterbar, såvida inte ... [villkor, som vid närmare
betraktelse visar sig vara alltid sant]". Falska begränsningar av den
här typen genomsyrar förslaget, och i synnerhet de centrala
bestämmelserna som användes för att övertyga ministrarna.

#por: Vid ministerrådsmötet drevs förslaget igenom av förhandlingsledningen
med hjälp av vilseledande information, påtryckningar och
överraskningstaktik, vilket gör det tveksamt om erforderlig majoritet
verkligen nåddes. Det kan sägas med säkerhet att det bara är en
minoritet av regeringarna som verkligen står bakom det som
förhandlades fram. Flera regeringar blev felaktigt representerade av
sina förhandlare, som bröt mot ingångna överenskommelser eller till
och med åsidosatte instruktioner från sina överordnade.

#isi: Ministerrådets förslag är i huvudsak identiskt i formuleringar och
anda med de texter som kommissionen och JURI (Utskottet för rättsliga
frågor) lade fram, men som Europaparlamentet redan förkastat genom en
rad ändringsförslag. Parlamentets ändringar återspeglade kraven som
ställs av en överväldigande majoritet av alla innovatörer inom
mjukvaruområdet och innovationsforskare inom EU, inklusive författarna
till studier beställda av kommissionen och av medlemmar av EUs
rådgivande organ.

#eWe: Ministerrådet har avfärdat och förkastat alla förslag från parlamentet
och de rådgivande organen, utan motivering och utan demokratisk
legitimitet. Texten har inte presenterats som ett medel för att uppnå
något uttalat politiskt mål, utan som en "kompromiss" mellan
regeringar. Den har förhandlats fram i slutna rum av anonyma
tjänstemän, som i de flesta fall är knutna till de nationella
patentverken, och alltså ingår i den yrkesgrupp som har ett
egenintresse av obegränsad patenterbarhet.

#Wmh: Be rådets ordförandeskap att stryka omröstningen om
mjukvarupatentdirektivet (2002/0047 COM (COD)) från dagordningen för
kommande ministerrådsmöte, där det nu avvaktar formellt godkännande.

#doe: Ta ärendet ur händerna från tjänstemännen inom patentbyråkratin, och
återupprätta en verklig politisk granskning av följderna av den
föreslagna texten. Utnämningen av representanterna i ministerrådets
arbetsgrupp för frågan bör diskuteras offentligt (i de nationella
parlamenten där så är möjligt).

#tln: Uppmana andra regeringar att göra detsamma, och i förlängningen
reformera arbetsformerna i ministerrådet så att situationer som den
uppkomna kan undvikas i framtiden.

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 member associations from 19 European countries representing in
total more than 500,000 enterprises.

#ich: Consortium for Open Source Middleware Architectures

#alW: Among the %(om:members) are many large companies

#tdW: represents interests of 60000 supporters and 1000 companies on matters
of software property

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: member of Parliament

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Senator

#aiW: Social Democratic Party

#csr: IT expert of Socialist Party

#VERT: Greens

#atP: top candidate for the European Parliament

#WaS: Miljöpartiet de gröna

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: fler underskrifter

#gcr: %(N) personer har hittils skrivit på det här handlingsförslaget via
%(ps:FFIIs medverkanssystem).

#thW: The main text is submitted together with the %(CA:Call for Action II)
with logos of signatories.  In addition, the following annex documents
are submitted.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: sv ;
# multlin: t ;
# End: ;

