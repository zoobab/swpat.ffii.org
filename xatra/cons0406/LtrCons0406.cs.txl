<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Naléhavá výzva národním vládám a parlamentům

#descr: Evropské vlády se chystají podepsat návrh směrnice pro neomezenou
patentovatelnost a neomezenou vynutitelnost patentovaných algoritmů a
%(q:počítačově realizovaných) obchodních postupů.  Rozhodnutí Rady
ministrů z 18. května 2004 bez jakéhokoliv vysvětlení a bez
demokratické legitimity zamítlo předchozí vyvážené rozhodnutí
Evropského parlamentu a poradních orgánů EU.  Rozhodující většina byla
zajištěna klamavou generalizací a s pomocí pochybných diplomatických
manévrů během rozhodujícího zasedání.  Níže podepsaní zástupci vůdčích
společností a informované veřejnosti v oblasti softwarových inovací v
Evropě žádají odpovědné politiky o zatažení za záchrannou brzdu a o
reorganizaci legislativních procesů v Radě týkajících se hospodářské
soutěže.

#epn: Příjemci

#2ln: Hlavy vlád a parlamentů 25 zemí a další zainteresovaní politici

#ujc: Věc

#ati: Směrnice o patentovatelnosti vynálezů realizovaných na počítači:
Žádost o znovuotevření diskuse v Radě

#repr: Většina?

#step

#parl: Parlament

#cfax: viz příloha %1

#act: Jsme znepokojeni, že

#aMe: na zasedání Rady pro konkurenceschopnost ze dne 18. května získala
kvalifikovanou většinu taková verze směrnice o patentovatelnosti
vynálezů realizovaných na počítači 2002/0047 COM (COD), která by v
Evropě umožnila neomezenou patentovatelnost a neomezenou vynutitelnost
patentovaných algoritmů a %(q:počítačově realizovaných) obchodních
postupů. Mezi ekonomy a softwarovými odborníky existuje obecná shoda,
že podobný režim, jako existuje v USA, poškozuje inovace, hospodářskou
soutěž a rozvoj informační společnosti.

#iwc: navrhovaný text úmyslně zamlžuje ministrům své skutečné dopady.
Sestává se z mnoha vět ve formě: %(q:Není dovoleno uplatnit patentový
nárok na počítačový program ..., ledaže by ... [ podmínka, která se
při bližším prozkoumání jeví vždy jako pravdivá ]). Neúčinná omezení
tohoto druhu prostupují celým návrhem a zvláště jeho hlavní částí,
která byla použita k přesvědčení ministrů

#por: moderátoři zasedání Rady pro konkurenceschopnost přesvědčovali
účastníky k přijetí návrhu klamavou, nátlakovou a překvapivou
taktikou, což zpochybňuje, zda vůbec došlo k dosažení platné většiny
pro prosazení návrhu. S jistotou můžeme konstatovat pouze, že jen
menšina vlád skutečně souhlasí s tím, co bylo dohodnuto a že několik
zástupců vlád nereprezentovalo vládní názor, čímž nedodrželi
meziresortní dohody a dokonce porušili instrukce svých nadřízených

#isi: návrh Rady je z velké části slovně i myšlenkově shodný s texty
Evropské komise a Výboru pro právní záležitosti Evropského parlamentu,
který Evropský parlament již dříve prostřednictvím řady pozměňovacích
návrhů de facto zamítnul. Parlamentní pozměňovací návrhy odrážely
požadavky většiny softwarových inovátorů i těch, kteří se zabývají
politikou inovace v EU, včetně autorů studie vyžádané Evropskou komisí
i členů poradních orgánů EU

#eWe: Rada ignorovala a odmítla bez jakéhokoliv zdůvodnění a bez
demokratické legitimity veškeré úsilí parlamentu a poradních orgánů
EU. Text není prezentován jako snaha o prosazení konkrétního
politického cíle, ale jako výsledek mezivládního %(q:kompromisu). Toho
však bylo dosaženo pod rouškou tajemství na úrovni anonymních
ministerských úředníků, většina z nichž je odpovědná za běh národních
patentních úřadů a tudíž zastupuje komunitu mající výjimečný zájem na
právně zaručené neomezené patentovatelnosti.

#toe: Z těchto důvodů Vás žádáme, abyste

#Wmh: požádali předsedu Rady, aby stáhnul hlasování o směrnici o
patentovatelnosti vynálezů realizovaných na počítači (2002/0047 COM
(COD)) z programu příštího zasedání rady, kde se očekává její formální
schválení.

#doe: odebrali odpovědnost za osud direktivy z rukou úředníků patentních
úřadů a obnovili důvěryhodné politické přezkoumání dopadů navrženého
textu. Jmenování delegáta pracovního zasedání Rady by mělo být veřejně
prezentováno a diskutováno (v parlamentu, pokud to jeho procesní
pravidla umožňují).

#tln: naléhali na ostatní vlády, aby učinili totéž, a dále ze zasadili o
reformu Rady takovým způsobem, aby se v budoucnosti předešlo podobným
katastrofálním situacím jako je ta současná.

#iar: Signatáři

#iin: Evropská zastřešující organizace malých a středních podniků

#cs5: 22 členských asociací z 19 evropských zemí, reprezentujících celkem
více než 500 000 podniků.

#ich: Konzorcium pro open source middleware architektury

#alW: Mezi %(om:členy) je mnoho velkých společností

#tdW: reprezentuje zájmy 60 000 příznivců a 1 000 obchodních společností ve
věci softwarového vlastnictví

#Wro: Asociace polských producentů softwaru.  Téměř všichni členové
produkují proprietární software. Koperují s SIIA a ostatními
proti-pirátskými aktivitami.

#Wlp: Asociace portugalských producentů softwaru.  Téměř všichni členové
produkují a prodávají autorsky chráněný proprietární software.
Organizuje kampaně proti softwarovému pirátství v úzké spolupráci s
SIIA a BSA.

#MemP: člen parlamentu

#fum: mluvčí pro IT, media a vzdělání Sociální demokratické strany

#lWa: polský senátor

#aiW: Sociální demokratická strana Polska

#csr: IT expert Socialistické strany

#delegit

#socpart

#VERT: Zelení

#atP: vrcholný kandidát Evropského parlamentu

#WaS: Švédská strana zelených

#MEPC: kandidát na poslance EP

#fae: jeden z největších španělských odborových svazů

#tSW: španělská asociace svobodného softwaru s více než 7 500 členy

#apu: největší španělská asociace profesionálů v oboru výpočetní techniky,
okolo 5 000 členů

#Wtr: španělská asociace internetových uživatelů a profesionálů

#nGs: odborový svaz pracovníků Grupo Telefonica, španělského největšího
operátora veřejné telefonní sítě

#hpi: španělská pracovní skupina na téma softwarové inovace

#0me: 5 000 členů

#eap: společnost vyvíjející počítačové hry

#bvsidesc

#gusturl

#gustdesc

#rgr: další podpisy

#gcr: až doposud podepsalo tuto výzvu %(N) osob pomocí %(ps:účastnického
systému FFII).

#pee: Přílohy

#thW: Hlavní text je podán současně s %(CA:Výzvou k akci II) s logy
signatářů.  Dále jsou podány následující přílohy.

#jet: ObjectWeb dopis

#nPr: Stručná PDF verze

#tuW: Typograficky upravená verze této naléhavé výzvy bez odkazů na přílohy
pro účely další distribuce (např. faxem)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de informuje o tomto dopisu

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: brablc ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

