# -*- mode: makefile -*-

letter-to-politicians.lstex:
	lstex letter-to-politicians | sort -u > letter-to-politicians.lstex
letter-to-politicians.mk:	letter-to-politicians.lstex
	vcat /ul/prg/RC/texmake > letter-to-politicians.mk


letter-to-politicians.dvi:	letter-to-politicians.mk letter-to-politicians.tex
	rm -f letter-to-politicians.lta
	if latex letter-to-politicians;then test -f letter-to-politicians.lta && latex letter-to-politicians;while tail -n 20 letter-to-politicians.log | grep -w references && latex letter-to-politicians;do eval;done;fi
	if test -r letter-to-politicians.idx;then makeindex letter-to-politicians && latex letter-to-politicians;fi

letter-to-politicians.pdf:	letter-to-politicians.ps
	if grep -w '\(CJK\|epsfig\)' letter-to-politicians.tex;then ps2pdf letter-to-politicians.ps;else rm -f letter-to-politicians.lta;if pdflatex letter-to-politicians;then test -f letter-to-politicians.lta && pdflatex letter-to-politicians;while tail -n 20 letter-to-politicians.log | grep -w references && pdflatex letter-to-politicians;do eval;done;fi;fi
	if test -r letter-to-politicians.idx;then makeindex letter-to-politicians && pdflatex letter-to-politicians;fi
letter-to-politicians.tty:	letter-to-politicians.dvi
	dvi2tty -q letter-to-politicians > letter-to-politicians.tty
letter-to-politicians.ps:	letter-to-politicians.dvi	
	dvips  letter-to-politicians
letter-to-politicians.001:	letter-to-politicians.dvi
	rm -f letter-to-politicians.[0-9][0-9][0-9]
	dvips -i -S 1  letter-to-politicians
letter-to-politicians.pbm:	letter-to-politicians.ps
	pstopbm letter-to-politicians.ps
letter-to-politicians.gif:	letter-to-politicians.ps
	pstogif letter-to-politicians.ps
letter-to-politicians.eps:	letter-to-politicians.dvi
	dvips -E -f letter-to-politicians > letter-to-politicians.eps
letter-to-politicians-01.g3n:	letter-to-politicians.ps
	ps2fax n letter-to-politicians.ps
letter-to-politicians-01.g3f:	letter-to-politicians.ps
	ps2fax f letter-to-politicians.ps
letter-to-politicians.ps.gz:	letter-to-politicians.ps
	gzip < letter-to-politicians.ps > letter-to-politicians.ps.gz
letter-to-politicians.ps.gz.uue:	letter-to-politicians.ps.gz
	uuencode letter-to-politicians.ps.gz letter-to-politicians.ps.gz > letter-to-politicians.ps.gz.uue
letter-to-politicians.faxsnd:	letter-to-politicians-01.g3n
	set -a;FAXRES=n;FILELIST=`echo letter-to-politicians-??.g3n`;source faxsnd main
letter-to-politicians_tex.ps:	
	cat letter-to-politicians.tex | splitlong | coco | m2ps > letter-to-politicians_tex.ps
letter-to-politicians_tex.ps.gz:	letter-to-politicians_tex.ps
	gzip < letter-to-politicians_tex.ps > letter-to-politicians_tex.ps.gz
letter-to-politicians-01.pgm:
	cat letter-to-politicians.ps | gs -q -sDEVICE=pgm -sOutputFile=letter-to-politicians-%02d.pgm -
letter-to-politicians/letter-to-politicians.html:	letter-to-politicians.dvi
	rm -fR letter-to-politicians;latex2html letter-to-politicians.tex
xview:	letter-to-politicians.dvi
	xdvi -s 8 letter-to-politicians &
tview:	letter-to-politicians.tty
	browse letter-to-politicians.tty 
gview:	letter-to-politicians.ps
	ghostview  letter-to-politicians.ps &
print:	letter-to-politicians.ps
	lpr -s -h letter-to-politicians.ps 
sprint:	letter-to-politicians.001
	for F in letter-to-politicians.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	letter-to-politicians.faxsnd
	faxsndjob view letter-to-politicians &
fsend:	letter-to-politicians.faxsnd
	faxsndjob jobs letter-to-politicians
viewgif:	letter-to-politicians.gif
	xv letter-to-politicians.gif &
viewpbm:	letter-to-politicians.pbm
	xv letter-to-politicians-??.pbm &
vieweps:	letter-to-politicians.eps
	ghostview letter-to-politicians.eps &	
clean:	letter-to-politicians.ps
	rm -f  letter-to-politicians-*.tex letter-to-politicians.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} letter-to-politicians-??.* letter-to-politicians_tex.* letter-to-politicians*~
letter-to-politicians.tgz:	clean
	set +f;LSFILES=`cat letter-to-politicians.ls???`;FILES=`ls letter-to-politicians.* $$LSFILES | sort -u`;tar czvf letter-to-politicians.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
