\contentsline {section}{\numberline {A}Call for Action II}{2}
\contentsline {subsection}{We are concerned that}{2}
\contentsline {subsection}{For these reasons we recommend the following:}{2}
\contentsline {subsection}{Signatories}{3}
\contentsline {section}{\numberline {B}Situation in individual countries}{6}
\contentsline {section}{\numberline {C}Software Patents in Europe: A Short Overview}{7}
\contentsline {subsection}{Why all this fury about software patents?}{7}
\contentsline {subsection}{Computer Programs from the Patent System's Perspective}{7}
\contentsline {subsection}{Abortive Attempt to Amend Art 52 EPC}{8}
\contentsline {subsection}{Failed Attempt to Fool the Parliament}{8}
\contentsline {subsection}{The Council under Faltering Control of the Patent Establishment}{9}
\contentsline {section}{\numberline {D}{``}Compromise Proposal{''}}{10}
\contentsline {subsection}{Collision instead of Compromise}{10}
\contentsline {subsection}{Program Publication Not Allowed, All Bridges Torn Down}{11}
\contentsline {subsection}{No Program Disclosure: Another Step Away from the Parliament}{14}
\contentsline {subsection}{Interoperation with Patented Standards Not Allowed}{14}
\contentsline {subsection}{Patentability Limited by Meaningless Terms, Definitions Removed}{16}
\contentsline {subsection}{Technicity Mingled with Non-Obviousness}{17}
\contentsline {subsection}{Scope of Directive Enlarged to Cover Any Process Invention}{19}
\contentsline {subsection}{Fake Limits, Convoluted Ambiguities}{20}
\contentsline {subsection}{Deliberate Destruction of EU Directive Project?}{22}
\contentsline {section}{\numberline {E}ObjectWeb letter}{23}
\contentsline {section}{\numberline {F}Eurolinux Petition}{24}
\contentsline {section}{\numberline {G}Appeal of Scientists}{25}
\contentsline {section}{\numberline {H}Appeal of Informaticians}{26}
