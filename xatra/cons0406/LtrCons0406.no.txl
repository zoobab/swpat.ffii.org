<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Urgent Call to National Governments and Parliaments

#descr: Europas regjeringer er i ferd med å godkjenne et direktivforslag for
ubegrenset patenterbarhet og uinnskrenket håndhevelse av patenter av
«datamaskin-implementerte» algoritmer og forretningsmetoder. Denne
avtalen, inngått av ministerrådet av 18/5 2004, vraker veloverveide
beslutninger gjort av Europaparlamentet og EUs rådgivende organer uten
noen berettigelse og uten demokratisk legitimitet. Flertallet ble
sikret av en villedende innpakning og av tvilsomme diplomatiske
manøvre under den avgjørende sesjonen. De undertegnede, som
representerer lederne for innovasjon innen programvare og informert
diskusjon av innovasjonspolitikk for programvare i Europa, ber de
ansvarlige politikerne om å trekke i nødbremsen og omorganisere
prosessen for konkurranselovgivning i Rådet.

#epn: Recipients

#2ln: Heads of 25 governments and parliaments, other concerned politicians

#ujc: Subject

#ati: Software Patent Directive: Request to reopen Council Discussion

#act: We are concerned that

#toe: For these reasons we urge you to

#iar: Undertegnere

#pee: Annexes

#aMe: Rådets konkurransedyktighets-sesjon av 18. mai 2004 oppnådde et
kvalifisert flertall for en versjon av programvarepatentdirektivet
2002/0047 COM (COD) som ville påføre Europa ubegrenset patenterbarhet
og uinnskrenket håndhevelse av patenter for «datamaskin-implementerte»
algoritmer og forretningsmetoder. Det er en generell enighet blant
økonomer og folk i programvarebransjen om at et slikt regime, som USA
allerede har, er katastrofalt for innovasjon, konkurranse og vekst i
den informasjonsbaserte økonomien.

#iwc: Den foreslåtte teksten er utformet for å villede ministre om de
egentlige virkningene den vil få. Den består av mange setninger på
formen «programvare er ... [ retorisk oppblåst understrekning ] ...
upatenterbar, med mindre ... [ betingelse, som ved nærmere ettersyn
viser seg alltid å være sann ]». Liksom-begrensninger av denne typen
gjennomsyrer forslaget og særlig de sentrale provisions som ble brukt
til å overtale ministrene

#por: Moderatorene for Rådets konkurransedyktighetssesjon presset deltagerne
mot å godta forslaget med villedning, press og overraskelsestaktikk,
og dermed gjorde de det tvilsomt hvorvidt et gyldig flertall ble
oppnådd. Det kan sies med sikkerhet at bare et mindretall av
regjeringene er virkelig enige i det som ble fremforhandlet, men flere
regjeringer ble misrepresentert av sine forhandlere, som brøt
mellom-departementale avtaler og endog brøt instruksene from sine
overordnede

#isi: Rådets forslag er i det store og hele identisk i ånd og ordlyd med
tekstene fra Kommisjonen og JURI-kommiteen, som Europaparlamentet
allerede avviste med en serie av rettelser. Parlamentets rettelser
gjenspeilte kravene fra det store flertall av programvareutviklere og
forskere på feltet innovasjon og politikk i EU, blant disse var
forfatterne av studier bestilt av Kommisjonen så vel som medlemmer av
EUs rådgivende organer

#eWe: Rådet har ignorert og avvist alt arbeidet til Parlamentet og de
rådgivende organene i EU, uten begrunnelse eller demokratisk
legitimering. Teksten er ikke fremstilt som et middel for å oppnå et
politisk mål, men snarere som et «kompromiss» mellom regjeringer. Den
ble fremforhandlet under et slør av hemmelighold mellom anonyme
departementsansatte embedsmenn, hvorav de fleste har ansvaret for
driften av nasjonale patentkontorer og er dermed del av et miljø med
en økonomisk interesse i uinskrenket patenterbarhet.

#Wmh: be Rådets presidentskap om å trekke avstemningen om programvare-
patentdirektivet (2002/0047 COM (COD)) fra dagsorden for Kommisjonens
neste sesjon, hvor den påventer formell godkjenning. original

#doe: ta sakspapirene ut av patentbyråkratiets hender, og gjeninnføre en
reell politisk gjennomgang av følgene av den foreslåtte teksten.
Utnevnelsen av representanter i Rådets arbeidsutvalg burde bli gjøres
kjent og debatteres offentlig (i Parlamenter hvor de institusjonelle
rammeverk tillater det).

#tln: oppfordre andre regjeringer om å gjøre det samme og på lengre sikt
reformere EU-rådet for å hindre lignende katastrofer fra å gjenta seg
i fremtiden.

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 member associations from 19 European countries representing in
total more than 500,000 enterprises.

#ich: Consortium for Open Source Middleware Architectures

#alW: Among the %(om:members) are many large companies

#tdW: represents interests of 60000 supporters and 1000 companies on matters
of software property

#Wro: Association of Polish Software producers.  Almost all members produce
proprietary software. They cooperate with SIIA and other anti-pirate
activities.

#Wlp: Association of Portuguese software producers.  Allmost all members
create and sell copyrighted proprietary software.  They organise
campaigns against software piracy in close cooperation with SIIA and
BSA.

#MemP: member of Parliament

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: Senator

#aiW: Social Democratic Party

#csr: IT expert of Socialist Party

#VERT: Greens

#atP: top candidate for the European Parliament

#WaS: The Green Party of Sweden

#MEPC: MEP candidate

#fae: one of Spain's largest trade unions

#tSW: libre software association of Spain, with more than 7,500 members

#apu: largest Spanish association of computer professionals, about 5,000
members

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Spanish working group on software innovation

#0me: 5000 members

#eap: Game software company

#bvsidesc: Association of self-employed computer scientists

#gusturl: http://www.gust.org.pl/en/

#gustdesc: Polish TeX Users Group

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal via the %(ps:FFII
participation system).

#thW: The main text is submitted together with the %(CA:Call for Action II)
with logos of signatories.  In addition, the following annex documents
are submitted.

#jet: ObjectWeb letter

#nPr: Minimal PDF Version

#tuW: An nicely typeset version of this Urgent Appeal without reference to
appendices, for quick sending (e.g. by fax)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCons0406 ;
# txtlang: no ;
# multlin: t ;
# End: ;

