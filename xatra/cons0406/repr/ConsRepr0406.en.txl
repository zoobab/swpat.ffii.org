<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Council 2004-05-18: an Unqualified Majority

#descr: The moderators of the Competitiveness Council session pushed the
participants toward accepting the proposal by deception, pressure and
surprise tactics, thus even making it questionable whether a valid
majority was achieved.  It can be said with certainty that only a
minority of governments really agrees with what was negotiated, but
several governments were misrepresented by their negotiators, who
broke intra-ministerial agreements or even violated instructions from
their superiors

#vta: Deceptive Maneuver to win over other countries for Bolkestein's
position, broken intra-ministerial agreement, MPs of ruling coalition
and opposition protest against being locked out and deceived.

#nai: Leading politicians from both governing parties, Social Democrats and
Greens, condemned the behaviour of the government's patent officials,
pointing out that these officials had broken promises given to a
working group consisting of the coalition's members of parliament and
officials from other ministries.

#tauss040602t: Jörg Tauss 2004-06-02: Open Letter to Brigitte Zypries

#tauss040602d: The German social democratic party's speaker for media and science
policy and member of its board complains in a public letter to the
Justice Minister about the behaviour of her ministry in the Council.  
According to Tauss, who enjoys explicit support in this from other
party colleagues and ministerial officials, the ministry of justice
has withheld information, broken promises, acted against the governing
coalition's explicit policies and inflicted considerable harm on the
coalition parties.

#hmh: The opposing Liberal Democratic Party (FDP) has submitted a
%(fd:motion) to the Parliament which calls on the governmening
coalition to withdraw support from the Council's %(q:compromise) and
instead fully support the European Parliament's amendments of
September 2003.  On July 15 the Ministery of Economics started an
industry survey on software patents with the explicit intention of
revising the government's policy and a clear tendency of deviating
from previous governmental Patent Newspeak.  On August 5, the mayor of
Munich, also a social democrat, called on the government to withdraw
its support from the Council Agreement.  Further calls came from
Deutsche Bank and the prestigious Institute for World Economic Studies
in Kiel.  It is to be expected that the governing coalition will try
to gain control over the patent officials in its Ministry of Justice
who have in recent years been holding the will of the coalition in
contempt at many junctures.

#ajc: The ministry of prime minister communicated its position to the
ministry of foreign affairs, asking them to vote no unless the German
proposal for Art 2b is adopted. According to the applicable rules,
Hungary should have vote no.  Instead, the Hungarian representative
Gottfried Péters voted YES and gave pro-patent speeches.

#vdW: The Hungarian representative later explained that they hadn't received
the instructions of the prime minister's office which were supposedly
sent by fax.  The government said the Council proposal was OK, because
of Art 4A (reinterpretation of Art 52 EPC as forbidding only narrow
software claims but allowing broad ones). Government's work on the
dossier is handled by the president of the Hungarian patent office.

#uWj: Representative %(JP) (minister for european integration) was
instructed by IT Ministry to support German amendment 2b (technical =
use of forces of nature, not data processing), didn't know what to do
when Germany had accepted deletion of substantial parts, tried to
reach IT Ministry but failed.  Pietras wasn't asked for his position
because a majority was supposedly already achieved, later %(pt:said)
that Poland meant to abstain.  All Polish parties and all players of
the Polish IT sector are against software patents.

#aim: Minister wasn't present, diplomat didn't know what to do, the
following comical dialogue shows that he acted contrary to the
promises which he had given to his parliament.  This led to some hard
questioning by the Parliament later.

#iCr: Irish Chairman

#lxt: Belgium: abstain.

#ner: And Denmark? Can I hear from Denmark please?

#emr: Denmark

#WtW: I would really like to ask the commission why they couldn't accept the
last sentence put forward by the Italians, which was in the original
German proposal.

#rln: Ireland

#muW: I think the Commissioner already answered that question, I'm sorry
Denmark. So are you yes, no, abstain?

#emr2: Denmark

#net: I think we wouldn't, we're not hap...

#rln2: Ireland

#sex: I assume you're a %(q:yes).

#emr3: Denmark

#eop: We're not happy.

#rln3: Ireland

#e8p: Are you 80% happy?

#emr4: Denmark

#tWW: But... I think we...

#rln4: Ireland

#nWW: We don't need you to be totally happy. None of us are totally happy.

#emr5: Denmark

#Iaw: Oh, I know that, I know that.

#rln5: Ireland

#eWb: If we were, we wouldn't be here!

#emr6: Denmark

#ebW: I think we're not very happy, but I think we would, we would...

#rln6: Ireland

#auW: Thank you very much.

#emr7: Denmark

#WWn: ... we would like to see a solution today.

#rln7: Ireland

#kye: Thank you very much, Denmark.

#IWy: Ladies and Gentlemen, I'm happy to say that we have a qualified
majority, so thank you all very very much indeed, and thank you to
commissioner Bolkestein.

#rae: Minister Brinkhorst, trusting his ministerial %(q:patent expert),
misinformed the Parliament several times and acted against the
Parliament's request.  This has led to %(np:hard questions from the
Parliament) and a %(pr:parliamentary resolution), passed with many
votes from the governing coalition, that he should withdraw the
Netherlands' support.  One of Brinkhorst's excuses for misinforming
the Parliament was a %(q:word processor error), similar to the
Hungarian %(q:broken fax machine).  Later Brinkhorst pretended not to
understand the Parliament's resolution.

#bal: Luxemburg, Estonia, Latvia, Slovakia, Slovenia

#nee: Support in contradiction to previous promises of the governments.

#AnW: Luxemburg minister promised to vote No if Art 6a of the Parliament,
for which his diplomats had fought, was not accepted.  Although the
Council proposal says in recital 17 the exact opposite of what
Luxemburg claimed to be standing for, the minister nevertheless
accepted the %(q:compromise).

#tns: The situtation in Estonia, Latvia and Slovakia is similar.

#lip: In Slovenia the %(q:Intellectual Property Office) had promised the
Information Ministry to support the German modifications but then
dropped them together with Germany.

#Wiw: Ministers act against given promises of President Chirac to continue
the previous government's policy disallow software patents for reasons
of national strategy.

#rrn: The ministers capitulated in front of the perseverance of the
%(q:patent experts) from INPI (Patent Office) who succeded in
bombarding the politicians with strategically ambiguous language and
presenting this as a means of limiting patentability.  Minister of
science Haignère explained that the Council compromise would only
allow patentability of technical inventions and not of office
software, which is evidently false.

#ete: The situation in most countries is the same: ministers lacking the
stamina to defend the public interest against the combination of
pressure and lies from their patent office which, by default, handles
the dossier in the %(q:%(tp|Intellectual Property|Patents) Working
Party).

#nap: Commission: DG Internal Market sways other DGs by %(q:magic)

#oma: The Council's proposal goes significantly beyond that of the
Commission by allowing program claims (Art 5(2)) and by introducing
amendment 17, which says that interoperability concerns can only be
addressed by antitrust procedings.  Both changes go against
concessions that DG Internal Market (Bolkestein) had made DG
Information Society (Liikanen) during the negotiations leading to the
Commission's proposal of 2002-02-20.  Yet Bolkestein tried to sell the
changes his colleagues as ones that %(q:maintained the original
proposal's balance).

#tpW: Bolkestein moreover went to great lengths at the Council session to
exploit the deceptive wordings of the %(q:compromise text) which the
German delegation had so readily accepted:

#WWW: The Commission has debated quite a long time about the possibility
that a computer program as such could or should be patentable, and has
decided against it. And in order to make this absolutely and perfectly
clear, the Commission should like to introduce two amendments in
Article 4a, which is entitled %(q:exclusions from patentability). The
Commission should like to propose a new Part 1 to this Article 4a, and
the new Part 1 should quite simply read: %(q:A computer program as
such cannot constitute a patentable invention). I should take that as
clear, and should allay any kind of apprehensions that, by some
magical means, a computer program as such could after all become
patentable; once again let me read it out for all clarity's sake:
%(q:A computer program as such cannot constitute a patentable
invention).

#ggc: Bolkestein's %(q:magic) is in fact %(aa:well documented).  According
to this magic, only a narrow claim refers to the object %(q:as such),
whereas a broad claim, characterised not by a specific individual
expression (code) but in more general terms, refers to the object
%(q:not as such).  Or, as Art 5(2) says:

#olW: A claim to a computer program ... shall not be allowed, unless
[condition that is always true].

#liW2: National Parliaments: Misinformed, Ignored, Disregarded

#soh: One day after the Irish Council presidency achieved %(q:political
agreement) on software patents, May 19th 2004, its foreign minister
Brian Cowen spoke COSAC, the assembly of national parliaments in the
EU.  As the Irish Presidency's %(ie:webpage) states:

#jai: In his address to delegates, Minister Cowen will highlight the role
played by European Affairs Committees in National Parliaments in
assessing and reviewing draft EU legislation and policy.

#aaw: The %(pr:Protocol on the Role of National Parliaments of the European
Union) explicitely encourages this and lays down a set of rules so as
to enable national parliaments to review the decisions of the Council.
 These include a six week period between availability of all
translations and Council decisions.

#lyW: In the case of the software patent directive, only very few EU Affairs
Committees of national parliaments were consulted.  The parliaments of
Germany and the Netherlands were kept in the dark and ignored (as e.g.
MP Tauss complained, see above) and misinformed (as the Dutch
Parliament explicitely stated in a resolution) by their ministeries. 
Most of the national patent office administrators who took the
decisions in the Council's %(q:%(tp|Intellectual Property|Patents)
Working Party) were able to decide on their own, disregarding the
opinions of national and European consultative bodies, without giving
anybody any explanation and without even their name being disclosed.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

