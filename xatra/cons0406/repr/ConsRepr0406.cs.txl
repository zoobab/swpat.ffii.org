<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rada 18.5.2004: nekvalifikovaná většina

#descr: Moderátoři zasedání Rady (Konkurenceschopnost) přesvědčovali účastníky
k akceptování návrhu klamavou, nátlakovou a překvapivou taktikou, což
zpochybňuje, zda vůbec došlo k dosažení platné většiny pro prosazení
návrhu.  S jistotou je možné pouze konstatovat, že pouze menšina vlád
skutečně souhlasí s tím, co bylo dohodnuto a že několik zástupců vlád
nereprezentovalo vládní názor, čímž nedodrželi meziresortní dohody a
dokonce porušili instrukce svých nadřízených.

#vta: Manévry pro vítězství nad ostatními zeměmi pro Bolkesteinovu pozici
porušili meziresortní dohody. Národní poslanci vládnoucí koalice a
opozice protestují proti tomuto ignorování jejich názorů a de facto
podvodu.

#nai: Vedoucí politikové obou vládních stran, sociální demokraté a zelení,
odsoudili chování vládních patentních úředníků, s upozorněním na to,
že tito úředníci nedorželi sliby dané pracovní skupině koaličních
členů parlamentu.

#tauss040602t: Jörg Tauss 2004-06-02: Otevřený dopis Brigittě Zypriesové

#tauss040602d: Tiskový mluvčí německé sociální demokracie pro vědu a člen tohoto
výboru si ve veřejném dopise stěžoval ministryni spravedlnosti na
postoj jejího ministerstva v Radě ministrů. Podle Tausse, který se v
tomto ohledu těší zřetelné podpoře dalších spolustraníků a
ministerských úředníků, Ministerstvo spravedlnosti zatajilo informace,
nedodrželo sliby. Což je v rozporu s jednoznačnou politikou vládnoucí
koalice a značně poškozuje koaliční strany.

#hmh: Opoziční Svobodná demokratická strana (FDP) podala parlamentu
%(fd:návrh), který vyzývá vládnoucí koalici k odstoupení od podpory
%(q:kompromisu) Rady ministrů a místo toho k plné podpoře dodatku
Evropského parlamentu ze září 2003. Ministerstvo hospodářství zahájilo
15. července průzkum odvětví s ohledem na problematiku softwarových 
patentů s jednoznačným záměrem revidovat vládní politiku a zřetelnou 
tendencí odklonu od dřívějšího vládního Patent Newspeaku. Starosta 
Mnichova, také sociální demokrat, vyzval 5.srpna vládu k odstoupení 
od její podpory rozhodnutí Rady ministrů.  Další výzvy přišly od 
Deutsche Bank a prestižního Institutu pro světovou ekonomiku (Institut
 für Weltwirtschaft) v Kielu. Předpokládá se, že se vládní koalice 
pokusí získat kontrolu nad patentovými úředníky na Ministerstvu
spravedlnosti, kteří v posledních letech v důležitých bodech nedbali
vůle koalice.

#ajc: Předsedy vlády slíbil nepodpořit návrh Rady, nicméně zástupce v Radě
jej podpořil s vysvětlením, že jejich fax nepřijal tuto instrukci.

#vdW: Maďarská reprezentace později argumentovala tím, že neobdržela
instrukce vinou rozbitého faxu. Vláda, v následujících debatách, na
základě čl. 4A (změna interpretace článku 52 Úmluvy o udělování
evropských patentů (EPC) ve smyslu vyloučení pouze velmi úzce
definovaných softwarových patentových nároků a tím de facto povolení
široce pojatých)  návrh Rady podpořila. Práce vlády na sbírce
dokumentů je řízena předsedou  maďarského patentového úřadu.

#uWj: Zástupci  bylo nařízeno podporovat německý dodatek 2b.  Nebyl  však
žádán  o jeho   postoj, protože většina již byla pravděpodobně
dosažena.  Později %(pt:sdělili), že se Polsko mínilo hlasování
zdržet.

#aim: Ministr nebyl přítomen, diplomat nevěděl co má dělat, komický dialog:

#iCr: Irský předseda

#lxt: Belgie: zdržuje se.

#ner: A Dánsko? Mohu slyšet něco od Dánska prosím?

#emr: Dánsko

#WtW: Skutečně bych se rád zeptal komise, proč nemohou akceptovat poslední
větu prosazovanou Itálií, která byla v původním německém návrhu.

#rln: Irsko

#muW: Myslím si, že komisař už tuto otázku zodpověděl, je mi líto Dánsko.
Takže pro, proti, zdržet se?

#emr2: Dánsko

#net: Myslím že bychom nemělo, nejsme šťas...

#rln2: Irsko

#sex: Předpokládám že jste %(q:pro).

#emr3: Dánsko

#eop: Nejsme šťastní.

#rln3: Irsko

#e8p: Jste na 80% šťastní?

#emr4: Dánsko

#tWW: Ano ..., já myslím my ...

#rln4: Irsko

#nWW: Není nutné, abyste byli úplně šťastní. Nikdo z nás není úplně šťastný.

#emr5: Dánsko

#Iaw: Jistě, já vím, já vím.

#rln5: Irsko

#eWb: Kdybychom byli, nebyli bychom zde!

#emr6: Dánsko

#ebW: Myslím, že nejsme úplně šťastní, ale myslím, že bychom, že bychom ...

#rln6: Irsko

#auW: Děkuji velice.

#emr7: Dánsko

#WWn: ... rádi viděli řešení dnes.

#rln7: Irsko

#kye: Děkuji velice Dánsku.

#IWy: Dámy a pánové, jsem rád, že mohu oznámit dosažení kvalifikované
většiny, takže vám všem skutečně velmi děkuji, a děkuji také Vám
komisaři Bolkesteine.

#rae: Ministr Brinkhorst, důvěřující svým ministerským %(q:patentovým
expertům), parlament několikrát dezinformoval a jednal v rozporu s
jeho požadavkem.  To vedlo k %(np:nepřijemným otázkám poslanců) a
%(pr:rozhodnutí sněmovny), přijatého mnoha hlasy vládní koalice ke
stažení holandské podpory.  Jedna z výmluv Brinkhorsta pro mylné
informování parlamentu byla %(q:chyba textového editoru), podobná
maďarskému %(q:rozbitému faxu). Později Brinkhorst předstíral
neporozumění rozhodnutí parlamentu.

#bal: Lucembursko, Estonsko, Litva, Slovensko, Slovinsko

#nee: Podpora v rozporu k předchozím vládním slibům.

#AnW: Lucemburský ministr deklaroval být proti, pokud by čl. 6a, za který
jeho diplomaté tolik bojovali, nebyl akceptován. Přesto návrh Rady ve
vypracování 17 ukazuje přesný opak toho jaký postoj Lucembursko
proklamovalo, ministr nicméně %(q:kompromis) akceptoval.

#tns: Situace je u Estonska, Litvy a Slovenska podobná.

#lip: Slovenský %(q:úřad intelektuální vlastnictví) deklaroval Ministerstvu
informatiky podporu německé modifikace, potom ji však i s Německem
opustil.

#Wiw: Ministři jednají v rozporu s vyřčenými sliby prezidenta Chiraca,
vydávají falešné svědectví o důvodech svého hlasování.

#rrn: Ministři kapitulovali před vytrvalostí %(q:patentových expertů) z
INPI, kteří neustále tvrdili, že černá je   bílá. Ministr pro vědu
Haigncre vysvětloval, že kompromis Rady by měl pouze umožnit
patentovatelnost technických vynálezů a ne kancelářského softwaru, což
evidentně není pravda.

#ete: Situace v mnoha zemích je stejná: ministři postrádající ochotu hájit
veřejný zájem proti kombinaci tlaku a lží pocházejícího z patentových
úřadů, které apriori pracují s fascikly %(q:%(tp|pracovní skupiny přes
intelektuální vlastnictví|patenty)).

#nap: Komise: Generální ředitelství vnitřního trhu lže aby přesvědčilo
ostatní ředitelství akceptovat  patentové nároky na programy

#oma: Návrh Rady zachází výrazně dále než text komise tím, že umožňuje
patentovatelnost programů. Také ve všech dalších ohledech je to návrh
na neomezenou patentovatelnost a nespoutaný patentový nátlak, 
extrémnější a nekompromisnější než doposud žádný jiný.  Přesto se
Bolkestein pokusil tento návrh předložit svým kolegům jako ten, který
%(q:udržuje původní vyváženost návrhu). To bylo, jak je vidět,
dostačující pro přesvědčení Liikanena.

#tpW: Bolkestein navíc na zasedání Rady zašel velmi daleko při využití
klamavé formulace %(q:kompromisního textu), který německá delegace tak
ochotně přijala:

#WWW: Komise rokovala poměrně dlouho o možnosti, zda by mohl nebo by měl být
počítačový program jako takový patentovatelný a rozhodla, že nikoliv. 
A aby to komise učinila naprosto a dokonale jasné, ráda by rozšířila
článek 4a, označený jako %(q:odepření patentovatelnosti), o dva
dodatky.  Komise by ráda navrhla novou část 1 článku 4a, kde by mělo
být celkem jasně uvedeno: %(q:Počítačový program jako takový nemůže
představovat patentovatelný vynález).  Tento výrok můžeme považovat za
jednoznačný. Je ale třeba rozptýlit jakékoliv obavy, že působením
nějaké magické síly by se mohl počítačový program jako takový konec
konců stát patentovatelným; dovolte mi to ještě jednou pro všechny
smysluplné důvody zopakovat: %(q:Počítačový program jako takový nemůže
představovat patentovatelný vynález).

#ggc: %(aa:Dodatek A) fakticky vysvětluje %(q:magičnost) textu komise.  
Podle Bolkesteinovy magie, je pouze úzce definovaný nárok nárokem  na
%(q:program jako takový), kdežto obsáhlé nároky, které nejsou 
charakteristické určitým individuálním vyjádřením, ale nějakým druhem 
nespecifikovaného %(q:technického příspěvku), nejsou programem
%(q:jako takovým). Nebo, jak říká například čl. 5(2):

#olW: Není dovoleno uplatnit patentový nárok na počítačový program ...,
ledaže  [podmínka, kterou lze vždy splnit].

#liW2: Národní parlamenty: mylně informovány a ignorovány

#soh

#jai

#aaw

#lyW

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

