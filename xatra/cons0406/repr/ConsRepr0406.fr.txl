<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Conseil 18/05/2004 : une majorité non-qualifiée

#descr: Les présidents de séance à la session du Conseil sur la compétitivité
ont poussé les participants à accepter la proposition avec des
manoeuvres trompeuses et pressantes les prenant par surprise, rendant
même ainsi discutable la certitude qu'une majorité valide avait été
obtenue. On peut affirmer avec certitude que seule une minorité de
gouvernements a réellement accepté ce qui était négocié, mais que
plusieurs gouvernements étaient mal représentés par leurs
négociateurs, qui ont rompu des accords intra-ministériels ou ont même
enfreint les consignes de leurs supérieurs.

#vta: Manoeuvres trompeuses pour gagner les autres pays à la position de
Bolkestein, accord inter-ministériel rompu, les députés nationaux de
la coalition dirigeante et de l'opposition protestent contre le fait
d'avoir été mis à l'écart et trompés.

#nai: Les dirigeants politiques des deux partis gouvernants,
Sociaux-démocrates et Verts, ont condamné l'attitude des
fonctionnaires des brevets du gouvernement, soulignant que ces
fonctionnaires avaient trahi les promesses données à un groupe de
travail des membres de la coalition au parlement.

#tauss040602t: Jörg Tauss 2004-06-02: Lettre Ouverte à Brigitte Zypries

#tauss040602d: Le porte parole du parti social-démocrate allemand pour la politique
en matière de médias et de science, également membre du bureau du
parti, s'est plaint dans une lettre ouverte au ministre de la justice
de l'attitude de son ministère au Conseil. D'après Tauss, qui a
bénéficié de l'appui explicite de ses collègues du parti et de
fonctionnaires ministériels, le ministre de la justice a caché des
informations, cassé ses promesse, agi contre la polituqe explicite de
la coalition au gouvernemnt et a infligé de sérieux dommages aux
partis de la coalition.

#hmh: Le parti libéral d'opposition (FDP) a soumis une %(fd:motion) au
parlement, appelant la coalition gouvernementale à retirer son soutien
au %(q:compromis) du Conseil et à la place à soutenir pleinement les
amendement du Parlement européen de septembre 2003. Le 15 juillet, le
ministre de l'économie a lancé une étude dans l'industrie sur les
brevet slogiciels, avec comme objectif explicite de réviser la
politique dy gouvernement, ayant clairement tendance à s'écarter de la
novlangue sur les brevets précédemment utilisée par le gouvernement.
Le 5 août, le maire de Munich, également un social-démocrate, a appelé
le gouvernement à retirer son soutien à l'accord du Conseil. D'autres
appel sont venus de Deutsche Bank et du prestigieux Intitut pour les
études économiques mondiales (Institure for World Economic Studies) à
Kiel. On s'attend à ce que la coalition gouvernementale tente de
prendre contrôle sur les fonctionnaires des brevets du ministère de la
  justice qui ont mépriser ces dernières années la volonté de la
coalition.

#ajc: Le premier ministre avait promis de ne pas soutenir la proposition, le
représentant au Conseil l'a soutenue malgré tout, expliquant par la
suite que leur fax avait eu une panne pour recevoir les instructions.

#vdW: Le premier ministre avait promis de ne pas soutenir la proposition, le
représentant au Conseil l'a soutenue malgré tout, expliquant par la
suite que leur fax avait eu une panne pour recevoir les instructions.

#uWj: Le représentant avait comme consigne de suivre l'amendement allemand à
l'article 2b, on ne lui pas demandé sa position car la majorité était
supposée être déjà atteinte. Par la suite, il %(pt: a déclaré) que la
Pologne avait l'intention de s'abstenir.

#aim: Le ministre était absent, le diplomate ne savait pas quoi faire, on a
assisté au dialogue ubuesque suivant :

#iCr: Présidente de séance irlandaise

#lxt: Belgique : abstention.

#ner: Et le Danemark ? Puis-je entendre le Danemark, s'il vous plaît ?

#emr: Danemark

#WtW: J'aimerais vraiment demander à la Commission pourquoi ils ne pouvaient
pas accepter la dernière phrase proposée par les Italiens et qui était
dans la proposition allemande originale.

#rln: Irlande

#muW: Je pense que le Commissaire a déjà répondu à cette question. Je suis
désolé, le Danemark. Alos êtes pour, contre ou abstention ?

#emr2: Danemark

#net: Je pense que nous ne, nous ne sommes pas cont...

#rln2: Irlande

#sex: Je considère que c'est un %(q:oui).

#emr3: Danemark

#eop: Nous ne sommes pas content

#rln3: Irlande

#e8p: Êtes-vous content à 80% ?

#emr4: Danemark

#tWW: Mais... Je pense que nous...

#rln4: Irlande

#nWW: Nous n'avons pas besoin que vous soyez complètement content. Aucun de
nous n'est complètement content.

#emr5: Danemark

#Iaw: Oh, je sais cela, je sais cela.

#rln5: Irlande

#eWb: Si nousl'étions, nous ne serions pas ici !

#emr6: Danemark

#ebW: Je pense que nous ne sommes pas très content, mais je pense que nous,
que nous...

#rln6: Irlande

#auW: Merci beaucoup.

#emr7: Danemark

#WWn: ... nous aimerions voir un solution aujourd'hui.

#rln7: Irlande

#kye: Merci beaucoup le Danemark.

#IWy: Mesdames et messieurs, je suis heureuse d'annoncer que nous avons une
majorité qualifiée, merci donc à tous, vraiment, et merci au
commissiaire Bolkestein.

#rae: Le ministre Brinkhorst, en faisant confiance à son %(q:expert en
brevets) ministériel, a mal informé le parlement néerlandais à
plusieurs reprises et a agi contre la demande du parlement. Ceci a
conduit à %(np:de graves questions de la part du parlement) qui a
demandé que le soutien des Pays-Bas soit rétracté. L'une des excuses
de Brinkhorst pour avoir donné de mauvaises informations au parlement
a été qu'une %(q:erreur de traitement de texte) s'était produite, de
manière similaire à la panne de fax des Hongrois.

#bal: Luxembourg, Estonie, Lettonie, Slovakie, Slovénie

#nee: Soutien en contradiction avec les précédentes promesses des
gouvernements.

#AnW: Le ministre luxembourgeois avait promis de s'opposer si l'article 6
bis du Parlement, pour lequel ses diplomates s'étaient battu, n'était
pas accepté. Bien que la proposition du Conseil stipule dans son
considérant 17 l'exact opposé de ce que le Luxembourg affirmait
supporter, le ministre a néanmoins accepté le %(q:compromis).

#tns: La situation est la même en Estonie, en Lettonie et en Slovakie eest
similaire.

#lip: En Slovenie %(q:l'Office de la Propriété intellectuelle) avait promis
au ministère de l'Information de soutenir les modifications allemandes
mais les a abandonnées en suivant l'Allemagne.

#Wiw: Les ministres ont agi contre les promesses qu'ils avaient données au
président et ont fait des déclarations fantoches sur ce qu'ils
allaient voter.

#rrn: Les ministres ont capitulé devant la persévérance des %(q:experts en
brevets) de l'INPI qui ont continuellement essayé de leur faire
prendre des vessies pour des lanternes. La Ministre déléguée aux
affaires européennes, Claudie Haigneré a déclaré que le compromis du
Conseil n'autoriserait que la brevetabilité d'inventions techniques et
pas celle de logiciels bureautiques, ce qui est à l'évidence erroné.

#ete: La situation dans la plupart des pays est la même : les ministres ont
manqué de vigueur pour défendre les intérêts publics contre une
succession de pressions et de mensonges de leurs offices des brevets
qui, par défaut, s'occupaient du dossier au %(q:Groupe de travail sur
la %(tp|Propriété Intellectuelle|brevets)).

#nap: Commission: La DG Marché intérieur ment pour faire accepter par les
autres DG les revendications de programmes

#oma: La proposition du Conseil va de manière significative au-delà de celle
de la Commission en autorisant les revendications de programmes. Il
s'agit également à tout égard d'une proposition pour une brevetabilité
illimitée et une application des brevets sans entraves, plus
extrémiste et dénuée de compromis que toute proposition jusqu'ici.
Bolkestein a néanmoins tenté de vendre cette proposition à ses
collègues comme %(q:maintenant l'équilibre de la proposition
initiale). Cela a apparemment suffi à influencer Liikanen.

#tpW: Bolkestein s'est d'ailleurs donné beaucoup de peine à la session du
Conseil pour exploiter les formulations trompeuses du %(q:texte de
compromis) que la délégation allemande avait si rapidement acceptées :

#WWW: La Commission a débattu assez longuement de la possibilité qu'un
programme d'ordinateur en tant que tel puisse ou doive être brevetable
et s'est prononcée contre. Et pour que cela soit absolument et
parfaitement clair, la Commission aimerait introduire deux amendements
à l'article 4 bis, qui est intitulé %(q:exclusions de brevetabilité).
La Commission aimerait proposer une nouvelle première partie à cet
article 4 bis, et cette nouvelle première partie se lit simplement :
%(q:Un programme d'ordinateur en tant que tel ne peut constituer une
invention brevetable). Cela ne peut être plus clair et devrait apaiser
les appréhensions de toutes sortes que, par magie, un programme
d'ordinateur en tant que tel puisse après tout devenir brevetable ;
laissez-moi le lire encore une fois par souci de clarté : %(q:Un
programme d'ordinateur en tant que tel ne peut constituer une
invention brevetable).

#ggc: %(aa:L'annexe A) explique dans les faits la %(q:magie) de la
Commission. Selon la magie de Bolkestein, seule une revendication
étroite est une revendication sur %(q:un programme en tant que tel),
alors qu'une revendication étendue, caractérisée non par une
expression individuelle spécifique mais par une sorte de
%(q:contribution technique) indéfinie est un programme %(q:qui n'est
pas en tant que tel). Ou comme l'affirme l'article 5(2) :

#olW: Une revendication pour un programme d'ordinateur... n'est autorisée
que si [condition toujours vraie].

#liW2: Parlments nationaux : Désinformés, ignorés, méprisés

#soh: Un jour après que la présidence irlandaise du Conseil est parvenu à un
%(q:accord politique) sur les brevets logiciels, le 19 mai 2004, son
ministre des affaires étrangères, Brian Coven, s'est exprimé devant le
COSAC, l'assemblée des parlements nationaux dans l'UE. Comme le
précise la %(ie:page web) de la présidence irlandaise :

#jai: Dans son allocution aux délégués, le ministre Cowen soulignera le rôle
joué par le Comité des parlements nationaux pour l'évaluation et la
revue des propositions dans la législation et la politique de l'UE.

#aaw: Le %(pr:protocole relatif au rôle des Parlements nationaux de l'union
européenne) encourage explicitement ceci et établit un ensemble de
règles afin de permettre aux parlements nationaux de passer en revue
les décisions du Conseil. Celles-ci incluent une période de six
semaines entre la disponibilité de toutes les traductions et les
décisions du Conseil.

#lyW: Dans le cas de la directive sur les brevets logiciels, seule une
minorité de comités aux affaires euopéennes a été consulté dans les
parlements nationaux. Les parlements de l'Allemagne et des Pays-Bas
ont été maintenus dans l'obscurité, ignorés (comme s'en est plaint le
député Tauss, voir ci-dessus) et mal informés (comme le Parlement
néerlandais l'indiquait explicitement dans une résolution) par leurs
ministères. La plupart des administrateurs des offices des brevets
nationaux, qui ont pris les décisions au %(q:Groupe de travail
%(tp|Propriété intellectuelle (brevets))) du Conseil, ont pu prendre
des décisions de leur propre chef, faisant abstraction de l'avis des
organes consultatifs nationaux et européens, sans donner d'explication
à personne et sans même que leurs noms soient révélés.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

