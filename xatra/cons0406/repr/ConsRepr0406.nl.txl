<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Raad 2004-05-18: Een Niet-Gekwalificeerde Meerderheid

#descr: De moderators van de Raad voor Mededinging pushten de deelnemers om
het voorstel aan te nemen, gebruik makend van misleiding, druk en
verrassingstaktieken, waardoor het zelfs twijfelachtig is of er wel
een geldige meerderheid bekomen werd. Er kan met zekerheid gezegd
worden dat slechts een minderheid van de regeringen echt akkoord is
met wat onderhandeld werd, maar verschillende regeringen werden
verkeerd vertegenwoordigd door hun onderhandelaars, dewelke
inter-mininsteriële overeenkomsten verbraken of zelfs instructies van
hun superieuren negeerden.

#vta: Misleidend maneuver om andere landen te overtuigen voor Bolkestein's
positie, verbroken inter-ministeriële overeenkomsten, MPs van
regerende coalities en van opposities protesteren tegen uitgesloten en
misleid te zijn.

#nai: Vooraanstaande politici van beide regerende partijen,
Sociaal-Democraten en Groenen, verwierpen het gedrag van de
octrooiambtenaren van de regering, opmerkend dat deze ambtenaren
beloften gebroken hadden die gegeven waren aan een werkgroep bestaande
uit parlementsleden van de coalitie.

#tauss040602t: Jörg Tauss 2004-06-02: Open brief aan Brigitte Zypries

#tauss040602d: De woordvoerder voor media en wetenschapsbeleid en lid van de raad van
bestuur van de Duitse Sociaal Democratische Partij maakt in een open
brief aan het Ministerie van Justitie zijn ontevredenheid kenbaar over
het gedrag ervan tijdens de Raad van Ministers. Volgens Tauss, die de
uitdrukkelijke steun geniet van andere partijleden en ministeriële
ambtenaren, heeft het ministerie informatie achtergehouden, is het
beloften niet nagekomen, heeft het uitdrukkelijk tegen beleid van de
regerende coalitie gehandeld en zware schade toegericht aan de
coalitiepartners.

#hmh: De Liberale Democratische Partij (FDP), die in de oppositie zit, heeft
een %(fd:motie) ingediend in het parlement die de regerende coalitie
oproept om haar steun voor het %(q:compromis) van de Raad terug te
trekken en daarentegen de amendementen van het Europees Parlement van
september 2003 volledig te steunen. Op 15 juli startte het Ministerie
van Economische Zaken met een rondvraag binnen industrie over
softwarepatenten, met de expliciete bedoeling om het beleid van de
regering te herzien en een duidelijke tendens die afstapt van het
gebruik van %(q:octrooinieuwspraak). Op 5 augustus riep de
burgemeester van München, een sociaal democraat, de regering op om
haar steun aan het politiek Raadsakkoord in te trekken. Verdere
oproepen kwamen van Deutsche Bank en het prestigieuze Instituut voor
Wereldeconomie in Kiel. Het valt te verwachten dat de regerende
coalitie zal proberen vat te krijgen op de octrooiambtenaren in haar
Ministerie van Justitie, die de afgelopen jaren de wil van de coalitie
op verschillende punten verwaarloosd heeft.

#ajc: De Premier had beloofd het voorstel niet te steunen. De
vertegenwoordiger in de Raad steunde het niettemin, en legde dit later
uit door te zeggen dat zijn faxmachine de instructies niet had
ontvangen.

#vdW: De Hongaarse vertegenwoordiger legde later uit dat ze de instructies
niet ontvangen hadden vanwege een kapotte faxmachine. In publieke
debatten achteraf beweerde de regering dat het voorstel van de Raad in
orde was, vanwege Art 4A (een herinterpretatie van Art 52 EPC, die
enkel beperkte conclusies op individuele computerprogramma's verbiedt,
maar zeer brede conclusies op hele klassen van programma's toelaat).
Het werk van de regering op dit dossier wordt behandeld door de
voorzitter van het Hongaarse octrooibureau.

#uWj: De vertegenwoordiger was opgedragen om het Duitse amendement 2b te
steunen, maar werd niet om zijn positie gevraagd omdat een meerderheid
reeds zou bereikt zijn, %(pt:meldde) later dat Polen zich wou
onthouden.

#aim: De minister was niet ter plaatse en de diplomaat wist niet wat te
doen, wat resulteerde in een tragikomische dialoog:

#iCr: Ierse Voorzitter

#lxt: België: onthouding.

#ner: En Denemarken? Kan ik Denemarken horen alstublieft?

#emr: Denemarken

#WtW: Ik zou echt graag willen vragen aan de Commissie waarom ze de laatste
zin naar voren geschoven door de Italianen niet konden aanvaarden,
dewelke in het oorspronkelijk voorstel stond.

#rln: Ierland

#muW: Ik denk dat de Commissaris deze vraag reeds beantwoord heeft, sorry
Denemarken. Dus bent u een ja, nee, onthouding?

#emr2: Denemarken

#net: Ik denk dat we niet, we zijn niet bl...

#rln2: Ireland

#sex: Ik veronderstel dat u een %(q:ja) bent.

#emr3: Denmark

#eop: We zijn niet blij.

#rln3: Ierland

#e8p: Bent u voor 80% blij?

#emr4: Denemarken

#tWW: Maar... Ik denk dat we...

#rln4: Ierland

#nWW: Je moet niet helemaal blij zijn. Niemand van ons is helemaal blij.

#emr5: Denemarken

#Iaw: Oh, ik weet dat, ik weet dat.

#rln5: Ierland

#eWb: Als we dat waren, zouden we niet hier zijn!

#emr6: Denemarken

#ebW: Ik denk niet dat we erg blij zijn, maar ik denk dat we, dat we...

#rln6: Ierland

#auW: Heel erg bedankt.

#emr7: Denemarken

#WWn: ... dat we vandaag een oplossing zouden willen zien.

#rln7: Ierland

#kye: Heel erg bedankt, Denemarken.

#IWy: Dames en Heren, ik ben blij te kunnen zeggen dat we een
gekwalificeerde meerderheid hebben, dus heel erg bedankt iedereen, en
danku aan Commissaris Bolkestein.

#rae: Minister Brinkhorst, die de %(q:octrooiexperten) op zijn kabinet
vertrouwde, verstrekte verschillende keren foutieve informatie aan het
Parlement en handelde tegen de wil van het Parlement in. Dit heeft
geleid tot %(np:moeilijke vragen van het Parlement) en eisen dat hij
de steun van Nederland zou terugtrekken). Eén van Brinkhorst's excuses
voor de desinformatie aan het Parlement was een %(q:fout in de
tekstverwerker), een beetje zoals de Hongaarse %(q:kapotte
faxmachine).

#bal: Luxemburg, Estland, Letland, Slovenië

#nee: Steun in tegenspraak met eerdere beloften van de regeringen.

#AnW: De Luxemburgse minister beloofde tegen te stemmen indien Art 6a van
het Parlement, waarvoor zijn diplomaten geijverd hadden, niet aanvaard
werd. Hoewel het voorstel van de Raad in recital 17 net het
tegenovergestelde zegt van waar Luxemburg beweerde voor te staan,
aanvaardde de minister niettemin het %(q:compromis).

#tns: De situatie in Estland en Letland is gelijkaardig.

#lip: In Slovenië had het %(q:Intellectueel Eigendomsbureau) het
Informatieministerie beloofd om de Duitse aanpassingen te steunen,
maar liet deze eis samen met Duitsland vallen tijdens de Raad.

#Wiw: Ministers handelen tegen beloften van de President, maken valse
beweringen over waarvoor ze gestemd hebben.

#rrn: De Minister capituleerde voor het doorzettingsvermogen van de
%(q:octrooiexperten) van INPI, die voortdurend beweerden dat zwart wit
was. Minister van Wetenschapsbeleid Haignère stelde dat het
Raadscompromis enkel de octrooieerbaarheid van technische uitvindingen
zou toelaten en niet van bureausoftware, wat duidelijk niet zo is.

#ete: De situatie in de meeste landen is hetzelfde: Ministers die het
uithoudingsvermogen missen om om het publieke belang te verdedigen
tegen een combinatie van druk en leugens komende van hun
octrooibureaus die, automatisch, het dossier reeds behandelden in de
%(q:Werkgroep Intellectueel Eigendom). Zij zijn het eveneens die deze
praktijk oorspronkelijk ingevoerd hebben en waren dus zowel overtreder
als wetgever om hun overtredingen te legaliseren, en bovendien nog
eens adviseur bij de stemming over hun eigen tekst.

#nap: Commissie: DG Interne Markt liegt om andere DG's programmaconclusies
te laten aanvaarden

#oma: Het Raadsvoorstel gaat beduidend verder dan dat van de Commissie door
programmaconclusies toe te laten. Ook op alle andere gebieden is het
een  voorstel voor onbeperkte octrooieerbaarheid en grenzeloze
afdwingbaarheid van octrooien, meer extreem en compromisloos dan enig
ander voorstel gezien tot nu toe. Niettemin probeerde Bolkestein dit
voorstel te verkopen aan zijn collega's als één dat %(q:het evenwicht
van het oorspronkelijke voorstel behield). Dit was blijkbaar genoeg om
Liikanen over te halen.

#tpW: Bolkestein spaarde bovendien geen moeite tijdens de Raadssessie om de
misleidende verwoordingen van de %(q:compromistekst) uit te buiten,
die zo makkelijk geslikt werd door de Duitse delegatie:

#WWW: De Commissie heeft een lange tijd gedebatteerd over de mogelijkheid
dat een computerprogramma als zodanig octrooieerbaar zou kunnen of
moeten zijn, en heeft besloten dat dit niet zo mag zijn. En om dit
absoluut en volkomen duidelijk te maken, zou de Commissie twee
amendementen in Artikel 4a willen toevoegen, wat getiteld is
%(q:uitzonderingen van octrooieerbaarheid). De Commissie zou een nieuw
deel 1 willen voorstellen voor dit Artikel 4a, en dit nieuwe deel 1
moet simpelweg stellen: %(q:Een computerprogramma als zodanig kan geen
octrooieerbare uitvinding zijn)". Ik zou denken dat dit duidelijk is,
en dit zou alle terughoudendheden moeten wegnemen dat, op een of
andere magische wijze, een computerprogramma als zodanig toch nog
octrooieerbaar zou kunnen worden; laat mij het nogmaals lezen voor
alle duidelijkheid: %(q:Een computerprogramma als zodanig kan geen
octrooieerbare uitvinding zijn).

#ggc: %(aa:Appendix A) legt de magie van de Commissie uit. Volgens
Bolkestein's getover is enkel een beperkte conclusie een conclusie op
een %(q:programma als zodanig). Een algemene conclusie, niet
gekenmerkt door een specifieke individuele expressie, maar door een
soort niet verder gedefinieerde %(q:technische bijdrage), is dan een
(toegelaten) conclusie op een programma %(q:niet als zodanig).

#olW: Een conclusie op een computerprogramma ... zal niet worden toegelaten,
tenzij [voorwaarde die altijd waar gemaakt gemaakt kan worden].

#liW2

#soh

#jai

#aaw

#lyW

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

