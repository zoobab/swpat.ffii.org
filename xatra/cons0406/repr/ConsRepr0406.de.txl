<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rat 2004-05-18: Eine unqualifizierte Mehrheit

#descr: Die Moderatoren der Sitzung des Rates für Wettbewerbsfähigkeit
drängten die Teilnehmer dazu, den Vorschlag anzunehmen, und zwar durch
Täuschung, Druck und Überrumpelungstaktiken. Das macht es fragwürdig,
ob überhaupt eine gültige Mehrheit erreicht wurde. Mit Sicherheit kann
jedoch gesagt werden, dass nur eine Minderheit der Regierungen
wirklich mit dem übereinstimmte, was verhandelt wurde.  Einige
Regierungen wurden jedoch von ihren Verhandlungsführern falsch
repräsentiert, indem diese Ministeriums-interne Vereinbarungen brachen
oder sich sogar über Anweisungen ihrer Vorgesetzten hinweg setzten.

#vta: Täuschungsmanöver, um andere Länder für Bolkesteins Position zu
gewinnen, Bruch Ministeriums-interner Vereinbarungen,
Bundestagsmitglieder aus Regierungskoalition und Opposition
protestieren gegen Ausschluß und Irreführung.

#nai: Führende Politiker beider Regierungsparteine (SPD und Grüne)
verurteilen das Verhalten der deutschen Regierungs-Vertreter und
weisen darauf hin, dass diese Versprechen gebrochen hätten, die einer
Arbeitsgruppe der Koalition gegeben worden seien.

#tauss040602t: Jörg Tauss 2004-06-02: Offener Brief an Brigitte Zypries

#tauss040602d: Der medien- und bildungspolitische Sprecher der Sozialdemokratischen
Partei Deutschlands und Mitglied ihres Vorstands klagt in einem
offenen Brief an die Justizministerin über das Verhalten von deren
Ministerium im EU-Rat.  Laut Tauss, dessen Meinung sich auf
Unterstützung durch Parteikollegen und Ministerialbeamten stützt, hat
das Ministerium Informationen zurückgehalten, Zusagen gebrochen, den
wirtschaftspolitischen Zielen der Regierungskoalition
entgegengearbeitet und den Regierungsparteien schweren Schaden
zugefügt.

#hmh: The opposing Liberal Democratic Party (FDP) has submitted a
%(fd:motion) to the Parliament which calls on the governmening
coalition to withdraw support from the Council's %(q:compromise) and
instead fully support the European Parliament's amendments of
September 2003.  On July 15 the Ministery of Economics started an
industry survey on software patents with the explicit intention of
revising the government's policy and a clear tendency of deviating
from previous governmental Patent Newspeak.  On August 5, the mayor of
Munich, also a social democrat, called on the government to withdraw
its support from the Council Agreement.  Further calls came from
Deutsche Bank and the prestigious Institute for World Economic Studies
in Kiel.  It is to be expected that the governing coalition will try
to gain control over the patent officials in its Ministry of Justice
who have in recent years been holding the will of the coalition in
contempt at many junctures.

#ajc: Das Ministerium des Premierministers hatte das Außenministerium in
einem späten Faxschreiben angewiesen, gegen den Vorschlag zu stimmen,
es sei denn der deutsche Artikel 2b werde angenommen.  Der Vertreter
im Rat, Gottfried Péter, erklärte dennoch seine Unterstützung für den
Vorschlag und stimmte nicht dagegen.

#vdW: Der ungarische Vertreter erklärte später, dass sie die Anweisungen
wegen eines kaputten Faxgerätes nicht erhalten hätten. In späteren
öffentlichen Debatten behauptete die Regierung, dass der
Rats-Vorschlag in Ordnung sei, und zwar wegen des Artikels 4a
(Neuinterpretation von Artikel 52 EPÜ, die nur eng begrenzte
Software-Ansprüche verbietet, aber weit gefasste zulässt). Für die
Regierung arbeitet der Präsident des ungarischen Patentamtes an diesem
Dossier.

#uWj: Der Vertreter wurde beauftrag, den deutschen Änderungsantrag 2b zu
unterstützen, konnte aber seine Stimme nicht abgegeben, da angeblich
die Mehrheit bereits erreicht wurde. Später %(pt:erklärte) er, dass
Polen sich enthalten wollte.

#aim: Der Minister war nicht anwesend, der Diplomat wusste nicht, was er tun
sollte, seltsamer Dialog:

#iCr: Irischer Vorsitzender

#lxt: Belgien: Enthaltung.

#ner: Und Dänemark? Kann ich bitte Dänemark hören?

#emr: Dänemark

#WtW: Ich möchte gerne die Kommission fragen, warum sie den letzten Satz
nicht akzeptieren konnten, den die Italiener vorgeschlagen haben, der
im ursprünglichen deutschen Vorschlag war?

#rln: Irland

#muW: Ich denke das Kommissar hat die Frage bereits beantwortet. Tut mir
leid, Dänemark. Sind sie also dafür, dagegen oder enthalten sie sich?

#emr2: Dänemark

#net: Ich denke wir würden nicht, wir sind nicht glück ...

#rln2: Irland

#sex: Ich denke, das soll %(q:dafür) heissen.

#emr3: Dänemark

#eop: Wir sind nicht glücklich.

#rln3: Irland

#e8p: Sind sie zu 80% glücklich?

#emr4: Dänemark

#tWW: Aber ... Ich denke wir ...

#rln4: Irland

#nWW: Es ist nicht nötig, dass sie vollkommen glücklich sind. Niemand von
uns ist vollkommen glücklich.

#emr5: Dänemark

#Iaw: Sicher, ich weiss das, ich weiss das.

#rln5: Irland

#eWb: Wenn wir es wären, wären wir nicht hier!

#emr6: Dänemark

#ebW: Ich denke, wir sind nicht sehr glücklich, aber ich denke wir würden,
wir würden ...

#rln6: Irland

#auW: Vielen Dank.

#emr7: Dänemark

#WWn: ... wir würden gerne heute eine Löung sehen.

#rln7: Irland

#kye: Vielen Dank, Dänemark.

#IWy: Meine Damen und Herren, ich freue mich, sagen zu können, dass wir eine
qualifizierte Mehrheit haben, also Ihnen allen vielen, vielen Dank und
Danke, Kommissar Bolkestein.

#rae: Im Vertrauen auf seine %(q:Patent-Experten) informierte Minister
Brinkhorst das Parlament mehrere Male falsch und handelte gegen
Anliegen des Parlamentes. Dies hat zu %(np:scharfen Fragen des
Parlaments) und einem, mit vielen Stimmen der Regierungskoalition
durchgesetzten, %(pr:Parlamentsbeschluss) geführt, dass er die
Unterstützung der Niederlande zurückziehen soll. Eine von Brinkhorst's
Entschuldigungen war ein %(q:Fehler in der Textverarbeitung), ähnlich
dem %(q:kaputten Faxgerät) der Ungarn. Später gab er vor, den
Parlamentsbeschluss nicht zu verstehen.

#bal: Luxemburg, Estland, Lettland, Slowenien, Slowakei

#nee: Unterstützten im Widerspruch zu den vorherigen Versprechungen der
einzelnen Regierungen den Vorschlag.

#AnW: Der luxemburgische Minister hatte versprochen, mit Nein zustimmen,
wenn der Artikel 6a des Parlamentes, für den seine Diplomaten gekämpft
hatten, nicht angenommen würde. Obwohl der Ratsentwurf in
Erwägungsgrund 17 genau das Gegenteil von dem, für das Luxemburg
angeblich steht, aussagt, akzeptierte der Minister den
%(q:Kompromiss).

#tns: Ähnlich sieht die Situation in Estland, Lettland und der Slowakei aus.

#lip: In Slowenien hatte die %(q:Behörde für geistiges Eigentum) dem
Informations-Ministerium versprochen, die deutschen Änderungen zu
unterstützen, verwarf diese dann aber zusammen mit Deutschland.

#Wiw: Minister handeln gegen die Versprechungen des Präsidenten und machen
Falschaussagen über das, wofür sie gestimmt hatten.

#rrn: Die Minister gaben der Beharrlichkeit der %(q:Patent-Experten) des
INPI %(pe: Nationales Institut für Gewerblichen Rechtsschutz) nach,
die die ganze Zeit behaupteten, dass schwarz weiss sei. Der Minister
für Wissenschaft Haignère erklärte, dass der Rats-Kompromiss nur die
Patentierbarkeit von technischen Erfindungen und nicht die von
Bürosoftware erlauben würde, was offensichtlicher Unsinn ist.

#ete: Die Situation in der meisten Länder ist die gleiche: Den Ministern
fehlt das Stehvermögen, um das Öffentliche Interesse gegen die
Kombination aus Druck und den Lügen der Patentämter, die normalerweise
dieses Dossier in der %(q:(%tp:Working Party Intellectual
Property|Patents)) bearbeiten, durchzusetzen.

#nap: Kommission: DG Markt lügt, um andere DGs dazu zu bewegen,
Programm-Anspüche zu akzeptieren

#oma: Der Rats-Entwurf geht weit über den der Kommission hinaus, indem er
Programmansprüche zulässt. Auch unter allen anderen Gesichtspunkten
ist es ein Vorschlag für die unbegrenzte Patentierbarkeit und
uneingeschränkte Durchsetzung von Patenten, extremer und
kompromissloser als alle vorherigen Entwürfe. Dennoch versuchte
Bolkestein, ihn als einen zu verkaufen, der die %(q:Ausgeglichenheit
des ursprünglichen Vorschlags bewahre). Das war anscheinend genug, um
Liikanen zu überzeugen.

#tpW: Außerdem bemühte sich Bolkestein in der Rats-Sitzung sehr, den
irreführenden Wortlaut des %(q:Kompromiss-Textes) auszunutzen, den die
deutsche Delegation so bereitwillig angenommen hatte:

#WWW: Die Kommission hat ausführlich über die Möglichkeit, ob ein
Computerprogramm als solches patentierbar sein kann oder sollte,
beraten und hat sich dagegen entschieden. Und um dieses wirklich
absolut deutlich zu mochen, möchte die Kommission zwei Änderungen in
Artikel 4a mit dem Namen %(q:Ausschluss von der Patentierbarkeit)
einfügen. Die Kommission möchte einen neuen Teil 1 zu diesem Artikel
4a vorschlagen und dieser neue Teil 1 soll ganz einfach lauten:
%(q:Ein Computerprogramm als solches kann keine patentierbare
Erfindung darstellen). Ich möchte das noch einmal klarstellen und alle
Befürchtungen zerstreuen, dass auf irgendeine magische Weise ein
Computer-Programm als solches am Ende doch noch patentierbar werden
könnte; lassen Sie es mich im Sinne der Klarheit noch einmal
wiederholen: %(q:Ein Computerprogramm als solches kann keine
patentierbare Erfindung darstellen).

#ggc: %(aa:Anhang A) erklärt in der Tat die %(q:Magie) der Kommission. Folgt
man Bolkestein's Trick, so ist nur ein eng begrenzter Anspruch ein
Anspruch auf ein %(q:Programm als solches), während ein weit gefasster
Anspruch, gekennzeichnet nicht durch eine besondere, einzelne
Eigenschaft, sondern durch einen ziemlich undefinierten
%(q:technischen Beitrag) ein Programm %(q:als nicht-solches) ist.
Oder, wie Artikel 5(2) es ausdrückt:

#olW: Ein Anspruch auf ein Computer-Programm ... soll nicht erlaubt werden,
außer [ Bedingung, die sich bei näherem Hinschauen als immer wahr
entpuppt ].

#liW2: National Parliaments: Misinformed, Ignored, Disregarded

#soh: One day after the Irish Council presidency achieved %(q:political
agreement) on software patents, May 19th 2004, its foreign minister
Brian Cowen spoke COSAC, the assembly of national parliaments in the
EU.  As the Irish Presidency's %(ie:webpage) states:

#jai: In his address to delegates, Minister Cowen will highlight the role
played by European Affairs Committees in National Parliaments in
assessing and reviewing draft EU legislation and policy.

#aaw: The %(pr:Protocol on the Role of National Parliaments of the European
Union) explicitely encourages this and lays down a set of rules so as
to enable national parliaments to review the decisions of the Council.
 These include a six week period between availability of all
translations and Council decisions.

#lyW: In the case of the software patent directive, only very few EU Affairs
Committees of national parliaments were consulted.  The parliaments of
Germany and the Netherlands were kept in the dark and ignored (as e.g.
MP Tauss complained, see above) and misinformed (as the Dutch
Parliament explicitely stated in a resolution) by their ministeries. 
Most of the national patent office administrators who took the
decisions in the Council's %(q:%(tp|Intellectual Property|Patents)
Working Party) were able to decide on their own, disregarding the
opinions of national and European consultative bodies, without giving
anybody any explanation and without even their name being disclosed.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

