<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rada 2004-05-18: Większość niekwalifikowana

#descr: Prowadzący posiedzenie Rady Konkurencyjności nakłaniali uczestników do
zaakceptowania swoich propozycji, stosując taktykę wybiegów, nacisków
i zaskoczeń, co stawia pod znakiem zapytania czy rzeczywiście
osiągnięto wystarczającą większość. Jest całkiem pewne, że tylko
mniejszość rządów zgodziła się na to, co jest ostatecznym tekstem, a
niektóre rządy nie były właściwie reprezentowane przez swych
negocjatorów. Nie trzymali się oni ustaleń międzyresortowych, a
niekiedy naruszali instrukcje otrzymane od swoich przełożonych.

#vta: Zwodniczy manewr w celu pozyskania innych krajów dla stanowiska
Bolkesteina, złamana umowa międzyresortowa, parlamentarzyści partii
koalicyjnych i opozycyjnych protestują przeciw ich pomijaniu i
zwodzeniu.

#nai: Czołowi politycy z obu partii rządzących, Socjaldemokratów i
Zielonych, potępili zachowanie rządowych urzędników patentowych,
wskazując, że urzędnicy ci nie dotrzymali obietnic dawanych grupie
roboczej parlamentarzystów koalicji rządzącej.

#tauss040602t: Jörg Tauss 2004-06-02: Offener Brief an Brigitte Zypries

#tauss040602d: The German social democratic party's speaker for media and science
policy and member of its board complains in a public letter to the
Justice Minister about the behaviour of her ministry in the Council.  
According to Tauss, who enjoys explicit support in this from other
party colleagues and ministerial officials, the ministry of justice
has withheld information, broken promises, acted against the governing
coalition's explicit policies and inflicted considerable harm on the
coalition parties.

#hmh: The opposing Liberal Democratic Party (FDP) has submitted a
%(fd:motion) to the Parliament which calls on the governmening
coalition to withdraw support from the Council's %(q:compromise) and
instead fully support the European Parliament's amendments of
September 2003.  On July 15 the Ministery of Economics started an
industry survey on software patents with the explicit intention of
revising the government's policy and a clear tendency of deviating
from previous governmental Patent Newspeak.  On August 5, the mayor of
Munich, also a social democrat, called on the government to withdraw
its support from the Council Agreement.  Further calls came from
Deutsche Bank and the prestigious Institute for World Economic Studies
in Kiel.  It is to be expected that the governing coalition will try
to gain control over the patent officials in its Ministry of Justice
who have in recent years been holding the will of the coalition in
contempt at many junctures.

#ajc: The ministry of prime minister communicated its position to the
ministry of foreign affairs, asking them to vote no unless the German
proposal for Art 2b is adopted. According to the applicable rules,
Hungary should have vote no.  Instead, the Hungarian representative
Gottfried Péters voted YES and gave pro-patent speeches.

#vdW: Premier przyrzekał nie popierać propozycji, tym niemniej reprezentant
w Radzie poparł ją, wyjaśniając następnie, że nie dotarła do niego
taka instrukcja (%(q:awaria faksu)).

#uWj: Reprezentant miał instrukcję by poprzeć niemiecka poprawkę 2b. Nie
został zapytany o stanowisko ponieważ większość jakoby została już
osiągnięta; %(pt:wyjaśniał) później, że Polska miała zamiar wstrzymać
się od głosu.

#aim: Minister był nieobecny, dyplomata nie wiedział jak się zachować,
komiczny dialog:

#iCr: Irlandzka przewodnicząca

#lxt: Belgia wstrzymuje się od głosu.

#ner: A Dania? Przepraszam nie słyszę Danii?

#emr: Dania

#WtW: Chciałbym usłyszeć odpowiedź Komisji dlaczego nie mogła zaakceptować
ostatniego zdania zgłoszonego przez Włochów, które pochodzi z
poprzedniej propozycji niemieckiej.

#rln: Irlandia

#muW: Myślę, że Komisarz już odpowiedział na to pytanie. Żałuje Danio, ale
jesteście za, przeciw czy wstrzymujecie się od głosu?

#emr2: Dania

#net: Nie możemy być. Nie jesteśmy zadowoleni...

#rln2: Irlandia

#sex: Zakładam, że jesteście %(q:za).

#emr3: Dania

#eop: Nie jesteśmy zadowoleni.

#rln3: Irlandia

#e8p: Czy jesteście zadowoleni w 80%?

#emr4: Dania

#tWW: Ale... uważam, że my...

#rln4: Irlandia

#nWW: Nie musimy być całkiem zadowoleni. Nikt z nas nie jest całkiem
zadowolony.

#emr5: Dania

#Iaw: Och, wiem to, wiem to.

#rln5: Irlandia

#eWb: Gdyby tak było, nie bylibyśmy tutaj!

#emr6: Dania

#ebW: Uważam, że daleko nam od zadowolenia, ale sądzę że chcielibyśmy,
chcielibyśmy...

#rln6: Irlandia

#auW: Dziękuje bardzo.

#emr7: Dania

#WWn: ... chcielibyśmy zakończyć dziś negocjacje.

#rln7: Irlandia

#kye: Dziękuję bardzo, Danio.

#IWy: Panie i Panowie, Mam przyjemność stwierdzić, że mamy większość
kwalifikowaną, więc bardzo wszystkim Państwu dziękuje, szczególnie
dziękuję komisarzowi Bolkesteinowi.

#rae: Minister Brinkhorst wielokrotnie wprowadzał w błąd parlament i działał
wbrew jego życzeniom. Doprowadziło to do %(np:ostrych przesłuchań
parlamentarnych) i żądań wycofania holenderskiego poparcia. Jedną z
wymówek Brinkhorsta podczas dezinformowania parlamentu był %(q:błąd
działania komputera), podobny do węgierskiej %(q:awarii faksu).

#bal: Luksemburg, Estonia, Łotwa, Słowenia

#nee: Poparcie w sprzeczności z wcześniejszymi obietnicami rządowymi.

#AnW: Minister Luksemburga przyrzekł oponować, jeśli Art 6a wersji
Parlamentu o który jego dyplomaci walczyli, nie będzie zaakceptowany.
Mimo, że propozycja Rady w punkcie 17 wprowadzenia ma treść odwrotną
niż ta przy której Luksemburg miał się upierać, jednak minister
zaakceptował %(q:kompromis).

#tns: Sytuacja z Estonią i Łotwą jest podobna.

#lip: W Słowenii %(q:Urząd Własności Intelektualnej) przyrzekł Ministrowi
Informatyzacji poparcie niemieckich poprawek, ale następnie porzucił
je razem z Niemcami.

#Wiw: Ministrowie działali przeciw obietnicom prezydenta, robiąc dziwne
zastrzeżenia odnośnie tego za czym głosują.

#rrn: Ministrowie skapitulowali wobec wytrwałości %(q:ekspertów patentowych)
z INPI (urzędu patentowego), którzy ciągle twierdzili, że czarne jest
białe. Minister Nauki, Haignère, powiedział, że kompromis Rady
dopuszcza wyłącznie patentowalność wynalazków technicznych, a nie
oprogramowania biurowego, co oczywiście nie jest prawdą.

#ete: Sytuacja w większości krajów jest taka sama: ministrom brakuje
determinacji do obrony interesu publicznego przeciw kombinacji
nacisków i kłamstw ze swoich urzędów patentowych, które, z definicji,
dzierżą akta %(q:Grupy Roboczej Intelektualnego Krętactwa).

#nap: Komisja: Dyrekcja Generalna Rynku Wewnętrznego kłamie aby skłonić inne
Dyrekcje do zaakceptowania zastrzeżeń programowych

#oma: Propozycja Rady idzie dalej niż propozycja Komisji gdyż dopuszcza
zastrzeżenia programowe. Także pod innymi względami jest to propozycja
prowadząca do usuwania ograniczeń w patentowalności i nieskrępowanego
egzekwowania patentów, jest ona bardziej skrajna i bezkompromisowa niż
wszystkie dotąd. Mimo to Bolkestein próbował przedstawiać tę
propozycję swoim kolegom jako %(q:utrzymującą równowagę propozycji
pierwotnej). To najwyraźniej wystarczyło aby pozyskać Liikanena.

#tpW: Ponadto Bolkestein włożył dużo wysiłku by na sesji Rady wyeksponować
zwodnicze słownictwo o %(q:kompromisowym tekście), który delegacja
niemiecka tak ochoczo zaakceptowała:

#WWW: Komisja debatowała dość długo na temat możliwości, czy program
komputerowy jako taki może lub powinien być patentowalny, i
opowiedziała się przeciw. W celu uczynienia tego całkowicie jasnym,
Komisja chciałaby wprowadzić 2 poprawki w Artykule 4a, który jest
zatytułowany %(q:wykluczenia z patentowalności). Komisja chciałaby
zaproponować dodanie nowego ustępu 1 w Artykule 4a. Ten nowy
(poprzedzający dotychczasową treść) ustęp powinien po prostu brzmieć:
%(q:Program komputerowy jako taki nie może stanowić patentowalnego
wynalazku). Chciałbym traktować to jako niewątpliwe i odrzucić
jakiekolwiek przypuszczenia, że w jakiś magiczny sposób program
komputerowy jako taki mógłby być mimo wszystko patentowalny. Niech mi
będzie wolno raz jeszcze to stwierdzić dla całkowitej jasności:
%(q:Program komputerowy jako taki nie może stanowić patentowalnego
wynalazku).

#ggc: %(aa:Załącznik A) faktycznie wyjaśnia %(q:magię) komisji. Zgodnie z
magią Bolkesteina, jedynie wąskie zastrzeżenie patentowe (opisane
elementami tekstowymi) odnosi się do %(q:programu jako takiego),
podczas gdy szerokie zastrzeżenie, charakteryzowane nie poprzez
konkretną postać, lecz poprzez pewien rodzaj niedefiniowanego
%(q:wkładu technicznego) odnosi się już do programu %(q:nie jako
takiego) lub, zgodnie z brzmieniem Art 5(2)

#olW: Zastrzeganie programu komputerowego ... jest niedopuszczalne, chyba że
(warunek, który zawsze może być spełniony).

#liW2: National Parliaments: Misinformed, Ignored, Disregarded

#soh: One day after the Irish Council presidency achieved %(q:political
agreement) on software patents, May 19th 2004, its foreign minister
Brian Cowen spoke COSAC, the assembly of national parliaments in the
EU.  As the Irish Presidency's %(ie:webpage) states:

#jai: In his address to delegates, Minister Cowen will highlight the role
played by European Affairs Committees in National Parliaments in
assessing and reviewing draft EU legislation and policy.

#aaw: The %(pr:Protocol on the Role of National Parliaments of the European
Union) explicitely encourages this and lays down a set of rules so as
to enable national parliaments to review the decisions of the Council.
 These include a six week period between availability of all
translations and Council decisions.

#lyW: In the case of the software patent directive, only very few EU Affairs
Committees of national parliaments were consulted.  The parliaments of
Germany and the Netherlands were kept in the dark and ignored (as e.g.
MP Tauss complained, see above) and misinformed (as the Dutch
Parliament explicitely stated in a resolution) by their ministeries. 
Most of the national patent office administrators who took the
decisions in the Council's %(q:%(tp|Intellectual Property|Patents)
Working Party) were able to decide on their own, disregarding the
opinions of national and European consultative bodies, without giving
anybody any explanation and without even their name being disclosed.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsRepr0406 ;
# txtlang: xx ;
# End: ;

