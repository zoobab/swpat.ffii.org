<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Pilny apel do Rządów i Parlamentów narodowych

#descr: Rządy europejskie są w przeddzień zatwierdzenia projektu dyrektywy
pozwalającej na nieograniczone udzielanie i egzekwowanie patentów na
%(q:wdrażane komputerowo) algorytmy i metody prowadzenia działalności
gospodarczej. Porozumienie osiągnięte przez Radę Ministrów UE 18 maja
2004 uchyla przemyślane i rozważne decyzje Parlamentu Europejskiego i
organów konsultacyjnych UE. Podjęto je bez podania uzasadnienia i przy
braku demokratycznej legitymacji. Większość w Radzie osiągnięto
poprzez obłudne manipulacje i wątpliwe dyplomatyczne manewry podczas
ostatniej decydującej sesji. Podpisani, znani ze swego wkładu w
dziedzinie innowacyjnego oprogramowania i debat na temat zapewnienia
właściwego miejsca tej problematyce w polityce europejskiej, wzywają
odpowiedzialnych polityków do użycia nadzwyczajnych środków w celu
wpłynięcia na prowadzony w Radzie proces legislacyjny dotyczący
konkurencyjności.

#epn: Adresaci

#2ln: Szefowie rządów i parlamentów 25 krajów, inni zainteresowani politycy

#ujc: Temat

#ati: Dyrektywa o patentach na oprogramowanie: Wniosek o ponowne wszczęcie
dyskusji w Radzie

#repr

#step

#parl: Parlament

#cfax: zobacz załącznik %1

#act: Jesteśmy zaniepokojeni tym, że

#aMe: Rada Konkurencyjności UE na posiedzeniu 18 maja 2004 osiągnęła
kwalifikowaną większość za wersją dyrektywy o patentach na
oprogramowanie 2002/0047 COM (COD), która narzuciłaby Europie
nieograniczone udzielanie i egzekwowanie patentów na %(q:wdrażane
komputerowo) algorytmy i metody prowadzenia działalności gospodarczej.
Wśród ekonomistów i fachowych informatyków jest niekwestionowana
zgoda, że praktykowanie takich zasad, jak to się dzieje w USA, jest
katastrofalne dla konkurencji, innowacji i rozwoju opartej na
informacji gospodarki.

#iwc: Zaproponowany tekst został opracowany w ten sposób, by wprowadzić w
błąd ministrów co do swych faktycznych konsekwencji. Składa się on z
wielu zdań o konstrukcji: %(q:oprogramowanie jest . . . [retoryczne,
rozdęte sformułowanie] . . niepatentowalne, chyba że . . [warunki,
które po bliższym przyjrzeniu się okazują się zawsze spełnione]). Tego
rodzaju fałszywymi ograniczeniami usiany jest cały projekt,
szczególnie jego główne przepisy, których sens nierzetelnie
przedstawiano ministrom

#por: Prowadzący posiedzenie Rady Konkurencyjności nakłaniali uczestników do
zaakceptowania swoich propozycji, stosując taktykę wybiegów, nacisków
i zaskoczeń, co  stawia pod znakiem zapytania czy rzeczywiście
osiągnięto wystarczającą większość. Jest całkowicie pewne, że tylko
mniejszość rządów zgodziła się na to, co jest ostatecznym tekstem, a
niektóre rządy nie były właściwie reprezentowane przez swych
negocjatorów. Nie trzymali się oni ustaleń międzyresortowych, a
niekiedy naruszali instrukcje otrzymane od swoich przełożonych

#isi: Parlament Europejski, wprowadzając zasadnicze poprawki już w
przeszłości odrzucił tekst swej komisji prawnej (JURI), który był
prawie identyczny z literą i duchem tekstu zaaprobowanym ostatnio
przez Radę. Poprawki Parlamentu cieszyły się poparciem zdecydowanej
większości innowacyjnych przedsiębiorstw programistycznych i
analityków polityki innowacyjnej, włączając w to autorów studiów
zamawianych przez Komisje i członków organów konsultacyjnych UE

#eWe: Rada zignorowała i odrzuciła prace Parlamentu Europejskiego i
konsultacyjnych organów UE, nie przedstawiając żadnego uzasadnienia i
nie posiadając do tego dostatecznej demokratycznej legitymacji. Jej
wersja dyrektywy nie jest przedstawiana jako środek do osiągnięcia
jakiegokolwiek politycznego celu, lecz raczej jako %(q:kompromis)
między rządami. Był on negocjowany w tajemnicy pomiędzy anonimowymi
ministerialnymi urzędnikami, z których większość nadzoruje krajowe
urzędy patentowe i jest ściśle powiązana z interesami patentowego
lobby.

#toe: Z tych powodów wzywamy do

#Wmh: Nakłonienia prezydencji Rady do wycofania głosowania nad dyrektywą
dotyczącą patentów na oprogramowanie (2002/0047 COM (COD)) z porządku
obrad następnego posiedzenia Rady, na którym ma być ona formalnie
zatwierdzona.

#doe: Odebrania sprawy z rąk biurokracji patentowej i przekazania jej do
rozstrzygnięcia narodowym parlamentom celem przywrócenia prawdziwej
politycznej odpowiedzialności za skutki proponowanego tekstu.
Wyznaczenie przedstawicieli w grupie roboczej Rady powinno mieć
charakter jawny i podlegać publicznej ocenie (w parlamentach, gdzie
zasady regulujące dziłalność tych instytucji na to pozwalają).

#tln: Nakłonienia innych rządów do analogicznego działania, a także do
zreformowania Rady UE, tak, by na przyszłość uniknąć tego rodzaju
katastrofalnych sytuacji.

#iar: Sygnatariusze

#iin: Confédération Européenne des Associations Petites et Moyennes
Entreprises

#cs5: 22 stowarzyszenia członkowskie z 19 europejskich krajów reprezentujące
łącznie ponad 500,000 przedsiębiorstw.

#ich: Consortium for Open Source Middleware Architectures

#alW: pośród %(om:członków) jest wiele dużych firm

#tdW: reprezentuje interesy ponad 60000 osób wspierających i ponad 1000 firm
w kwestii własności oprogramowania

#Wro: Stowarzyszenie Polski Rynek Oprogramowania. Prawie wszystkie firmy
członkowskie tworzą oprogramowanie komercyjne, współpracują z SIIA
oraz biorą udział w innych działaniach antypirackich.

#Wlp: Stowarzyszenie portugalskich twórców oprogramowania. Prawie wszystkie
firmy członkowskie tworzą i sprzedają komercyjne oprogramowanie
chronione prawem autorskim. Organizują również, w bliskiej współpracy
z SIIA i BSA, kampanie przeciwko piractwu komputerowemu.

#MemP: członek Parlamentu

#fum: Speaker for IT, Media and Education of Social Democratic Party

#lWa: polski senator

#aiW: Socialdemokracja Polska

#csr: IT expert of Socialist Party

#delegit

#socpart

#VERT: Zieloni

#atP: główny kandydat do Parlamentu Europejskiego

#WaS: szwedzka Partia Zielonych

#MEPC: kandydat do Parlamentu

#fae: jeden z największych hiszpańskich związków handlowych

#tSW: hiszpańskie stowarzyszenie wolnego oprogramowania, ponad 7,500
członków

#apu: największe hiszpańskie stowarzyszenie profesjonalnych informatyków,
ponad 5,000 członków

#Wtr: Spanish association of Internet users and professionals

#nGs: a trade union of the workers of Grupo Telefonica, Spanish largest PTT

#hpi: Hiszpańska grupa robocza do spraw innowacji w dziedzinie
oprogramowania

#0me: 5000 członków

#eap: Firma tworząca gry komputerowe

#bvsidesc

#gusturl: http://www.gust.org.pl/

#gustdesc: GUST

#rgr: Pozostali sygnatariusze

#gcr: %(N) osób dotychczas podpisało się pod tym apelem poprzez  %(ps:system
FFII).

#pee: Załączniki

#thW: Główny tekst jest powiązany z %(CA:Wezwaniem do działania II) wraz z
logo każdego z sygnatariuszy.  Dodatkowo dokument jest uzupełniony
następującymi załącznikami.

#jet: list ObjectWeb

#nPr: Minimalna wersja PDF

#tuW: Estetycznie przygotowana wersja Pilnego Apelu bez odniesień do
załączników, przeznaczona do szybkiej wysyłki (np. faksem)

#sfR: Offensive gegen Softwarepatent-Richtlinie

#eaW: Heise.de reports about this letter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrCons0406 ;
# txtlang: xx ;
# End: ;

