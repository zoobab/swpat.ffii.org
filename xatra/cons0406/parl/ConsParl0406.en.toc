\contentsline {section}{\numberline {1}Studies and Opinions related to the Software Patent Directive Project}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}The competitiveness of European enterprises in the face of globalisation -- Opinion of the Committee of Regions\cite {cor99en}}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}NL 2000: Ruimere octrooiering van computerprogramma's : technicality of revolutie?\cite {verk00}}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}US National Research Council 2000: The Digital Dilemma\cite {nrc00}}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}CEC 2000: ``Patent protection of computer programmes''\cite {tap00}}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}CEC 2000: ``The Economic Impact of Software Patents''\cite {ipi00}}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}CEC 2000: Consultation on the Planned Software Patent Directive\cite {ec00}}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}DE 2001: Micro- and Macroeconomic Implications of the Patentability of Software Innovations\cite {isi01}}{5}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}NL 2001: ``Intellectueel Eigendom en Innovatie -- Over de rol van intellectueel eigendom in de Nederlandse kenniseconomie''\cite {ezi01en}}{6}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}NL 2002: ``Toets op het concurrentievermogen''\cite {ezt02}}{6}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Bakels \& Hugenholtz 2002: Discussion of European-level legislation in the field of patents for software\cite {bah02}}{7}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}DE 2002-07-08\cite {mok02}}{7}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}FR Gov 2002: Report on the Software Economy\cite {cdp02}}{7}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Koski 2002: Technology policy in the telecommunication sector: Market responses and economic impacts\cite {kio02}}{7}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}ESC 2002-09-24: Opinion Economic and Social Committee on the Commission Proposal\cite {esc02en}}{8}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}CULT 2003-01-22: Opinion Committee for Cultural Affairs and Youth of EP\cite {cult02}}{8}{subsection.1.15}
\contentsline {subsection}{\numberline {1.16}ITRE 2003-02-21: Opinion Committee for Industry and Trade of EP\cite {itre02}}{8}{subsection.1.16}
\contentsline {subsection}{\numberline {1.17}Bessen \& Hunt 2003: An Empirical Look at Software Patents\cite {bh03}}{8}{subsection.1.17}
\contentsline {subsection}{\numberline {1.18}Federal Trade Commission Hearings of 2002-2003\cite {ftc03}}{9}{subsection.1.18}
\contentsline {subsection}{\numberline {1.19}Deutsche Bank Research 2004: Innovation in Germany\cite {db04}}{10}{subsection.1.19}
\contentsline {section}{\numberline {2}FAQ}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}What is a software patent?}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}What does the law say about software patents?}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}What are ``computer-implemented inventions''?}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Why did the EP decide to reaffirm the non-patentability of software?}{11}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Don't investments in software development have to be protected?}{11}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}What about mobile phones and washing machines?}{11}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Doesn't the EP version contradict international treaties such as TRIPS?}{12}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Isn't the Commission/Council version merely confirming the status quo?}{12}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Why is ``data processing not a field of technology'' and what does this mean?}{12}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Why does the EP define ``technology'' by reference to ``forces of nature''?}{12}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Is insistence on ``forces of nature'' still adequate to today's inventions?}{13}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Why was the EP not content with the Commission's safeguards for interoperability?}{13}{subsection.2.12}
