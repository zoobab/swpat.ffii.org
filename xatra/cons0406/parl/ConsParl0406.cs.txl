<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Analýza a názory o rozhodnutí parlamentu

#descr: Evropský parlament sérií pozměňovacích návrhů již de facto zamítnul
texty Komise a Výboru pro právní záležitosti Evropského parlamentu,
které byly ve velké části slovně a ideově identické s texty s těmi,
které teď odsouhlasila Rada. Parlamentní pozměňovací návrhy odrážely
požadavky většiny nositelů softwarové inovace a výzkumníků politiky
inovace v EU, včetně autorů studie vyžádané Evropskou komisí a dále
členů poradních orgánů EU.

#sto: Studie a názory týkající se projektu směrnice o patentovatelnosti
vynálezů realizovaných na počítači

#Con: EU Council of Regions 1999

#ett: Konkurenceschopnost evropských společností v době globalizace a jak ji
povzbudit.

#ftW: Názory podepsané hlavami regionálních vlád po celé Evropě,
upozorňující, že patetové právo nelze aplikovat včeobecně a na všecho,
zvláště ne na software.

#iui: NL 2000: Ruimere octrooiering van computerprogramma's : technicality
of revolutie?

#lhy: Studie Dirka W. F. Verkada, profesora práva intelektuální vlastnictví,
zaměstnaného na holandském Ministerstvu financí, publikovaná jako
kniha s ISBN 90-5409-267-X. Jejím hlavní závěrem je, že patentování
software je velmi nebezpečné pro celé odvětví a že nesmí být
podceňována ochrana autorským právem.

#wWW: Vydávání patentů ve Spojených státech bylo zahájeno bez patřičného
pousouzení z legislativní stránky (podobnou věc se chystáme udělat i v
Evropě)

#Ser: Zpochybňuje schopnost Patentového úřadu Spojených států (US Patent
Office), zpracovávat patentové přihlášky na softwarové patenty,
protože nemá dostatek znalostí a informací o stávajících řešeních, aby
mohl posoudit tzv. prior art, tedy to, že myšlenka se již běžně
používá.

#fmi: Softwarový trh se výrazně liší od ostatních odvětví. Zcela minimálně
se prodávají jednotlivé %(q:komponenty), vetšina vývojářů píše
programy od základů, nevyhledávají v patentové literatuře stávající
implementace, které by mohli licencovat, neboť je rychlejší danou
komoponentu napsat znovu, a tak hrozí, že bude nevědomky porušen
patent.

#adn: V oblasti vývoje software probíhá vývoj daleko rychleji než v
jakékoliv jiné oblasti. Patenty jsou často uděleny až v době, kdy je
patentovaná technologie zastaralá.

#WeW: Softwarové patenty mohou způsobit, že ustane současný překotný vývoj a
software bude produkovat jen několik velkých společností, které se
budou držet v šachu křížovým licencováním.

#she: Studie o vhodnosti softwarových patentů objednaná Generálním
ředitelstvím pro podnikání Evropské komise, provedená britskými
výzkumníky v oblasti  malých a středních i větších firem a ve
výzkumných institucích.

#aWf: Žádná ze společností v tomto segmentu nepoužívá ve výraznější míře
patenty k ochraně svých ivestic.

#Wte: Střední a malé společnosti se obávají, že vzhledem k nákladům na
patentové řízení a následné obhajování patentů u soudů, si nebudou
moci patenty dovolit. (Tento argument neplatí v ČR v plném rozsahu,
protože náklady na patentové řízení jsou zde výrazně nižší, než v
jiných státech. Společnosti, které by ale rády podnikaly na celém
území EU narazí na to, že v jiných státech tomu tak není. Naopak u nás
bude problém s pomalostí soudů, což naopak neznají v jiných státech.
Pozn. překladatele)

#prm: Vzhledem ke krátké životnosti počítačových programů, se střední a malé
společnosti soustředí na další vývoj, aby jejich produkty nezastaraly,
mnohem raději, než na boj o patentování svých současných produktů.

#omc: Velké společnosti patentují mnohem více než malé.

#tWh: Střední a malé společnosti považují za jeden z hlavních problémů pro
svou existenci vznik %(e:nežádoucích zákonů)

#afn: Všeobecně se má za to, že patentovatelnost software bude klást odpor
rozvoji malých a středních společností.

#tno: Na objednávku společnosti  DG Internal Market vznikla v prostředí
londýnských patentových právníků sympatizujících se softwarovými
patenty.  Nicméně v reakci na petici Eurolinux byla přejmenována na
studii %(q:o ekonomických dopadech) a byl najmut ekonom, aby doplnil
kapitolu o ekonomii. Ten však nedospěl ke stejnému závěru jako Komise
a tak byla studie odložena a 6 měsíců zapomenuta. Poté se stala
součástí %(q:konzultačního cvičení) (ec00):

#fon: Jak ukazují naše ekonomické studie literatury, (sekce III naší
zprávy), většina ekonomů má pochybnosti, zda je možné dosáhnout vyšší
účinnosti ekonomiky, tedy zvýšení všeobecného blaha, zavedením
patentovatelnosti počítačových programů a souvisejících činností. Tyto
pochybnosti jsou dále umocňovány stále stoupajícím počtem případů, kdy
v USA dochází k použití patentů nekalým způsobem. Diskuze o těchto
aspektech v USA stále pokračuje a nemá zatím žádný závěr.

#nrW: Neexistují důkazy, že softwarové patenty přinášejí pozitivní efekty,
které by převýšily tyto negativní:

#dsW: patenty jsou udělovány na triviální a leckdy i staré nápady přičemž 
braní ohledu na tyto patenty klade velké břímě zejména na malé a
střední podniky a na nezávislé vývojáře;

#tai: patenty posilují tržní pozici nejsilnějších hráčů;

#atd: oblast vývoje počítačových programů se vyznačuje tím, že vývoj je
pozvolný a kontinuální. Nedochází zde k žádným převratným objevům.
Neexistují důkazy, že by při takovém způsobu vývoje patenty vedly k
jeho urychlení či zlepšení.

#oom: EU Consultation by Commission

#hyb: Po nečekamém rozhodnutí národních vlád upustit od změny článku 52
Úmluvy o udělování evropských patentů (EPC), zveřejnila Komise jiné 
%(q:konzultační cvičení).   Předchozích konzulatací se zúčastnili
pouze zástupci výboru pro intelektuální vlastnictví, tedy asi 40
korporátních patentových právníků a položili jen několik otázek
zpracovaných jejich skupinou. Nová konzultace byla organizována
stejným způsobem, ale vzhledem k pozornosti veřejnosti bylo nyní
odpovědí téměř 1500 i to i z kruhů, které v prvním kole neprojevily o
věc zájem.

#gWs: Jaká část účastníků, příslušejících jednotlivým skupinám, byla proti
softwarovým patentům:

#nvu: Jednotlivci

#SME: Malé a střední podniky

#rnr: Velké společnosti

#sio: Asociace

#Use: Uživatelé

#tdn: Studenti

#cec: Akademici

#fWo: Vývojáři software

#Wen: Patentoví profesionálové

#nnp: Zástupci vlád

#ieW: Zdá se, že zástupci států jsou ještě většími příznivci praktik
Evropského patentového úřadu (EPO) než patentoví právníci velkých
korporací. To však není žádným překvapením, protože praktiky EPO jsou
těmito lidmi vytvářeny a schvalovány, zastupují totiž své vlády v
administrativní radě EPO a v radě EU pro patenty.

#Wts: Komise se usnesla, vycházejíce z prohlášení asociací jako je EICTA a
UNICE, které jsou všeobecně známé prosazováním zájmů právníků velkých
korporací, že %(q:ekonomická většina) podporuje zavedení softwarových
patentů. Nicméně 2/3 daňových příjmů a pracovních míst vytvářejí malé
a střední společnosti (SME) a jen velmi málo z nich bylo pro zavedení
softwarových patentů.

#EWa: %(q:Ekonomická většina) v diskuzi o softwarových patentech

#oof: 2001/09: Micro- and Macroeconomic Implications of the Patentability of
Software Innovations

#sPr: Průzkum provedený na objednávku patentového odboru německého
ministerstva financí  v několika stech firmách společnostmi Fraunhofer
Innovation Research Institute a Max-Planck Institute for Intellectual
Property, které jsou poměrně propatentově zaujaté, vedl k těmto
výsledkům:

#Wns: Patenty jsou tou nejméně často užívanou a nejméně významnou možností,
jak chránit investice do vývoje softwaru.

#Wto: Čas potřebný pro vývoj je velice krátký a inovace probíhají velmi
rychle, mnohem rychleji než v jiných oblastech.

#ewo: Vývoj probíhá inkrementálně, odlišně od ostatních odvětví.

#ctn: Na poli vývoje programového vybavení je nejdůležitější rychlost a
efektivita. Jakékoliv překážky kladené vývoji mají právě v této
oblasti horší důsledky než kde jinde.

#osm: Je mimořádně důležité zajistit interoperabilitu.

#eln: Intenzita výzkumu a vývoje nemá vliv na patentové chování.

#Wdi: Všeobecně platí, že větší společnosti jsou držiteli více patentů.

#tjo: Teorie, že patenty pomáhají mladým společnostem při vstupu na trh
nebyla potvrzena.

#eie: Strategická výhoda plynoucí z patentů je v mezinárodní soutěži zřejmá,
ale je poskytnuta jen několika málo velkým společnostem.

#fra: Zpráva holandského ministerstva financí z roku 2001

#oth: Částečně neplánovaný efekt vědomnějšího zacházení s intelektuálním
vlastnictvím a patentovou firemní strategií je vzrůstající problém
'anti-commons' (Lze popsat jako upřednostňování proprietárních
patentovaných technologií před veřejně přístupnými standardy. Pozn.
překladatele.) Jednotlivé strany se stávají navzájem vězni v
patentovém minovém poli. Tímto strategickým patentováním trpí
nejčastěji malé a středně velké (technologicky vyspělé) podniky.

#loe: Kromě toho, patenty jsou pouze částí celkové strategie ochrany
duševního vlastnictví. Většina společností považuje patentování za
méně důležité než utajení a technologický náskok.

#rps: Existující patentová portfolia jsou pro malé a střední společnosti
relativně větší přítěží. Setkávají se s velkými obtížemi, pokud se
pokusí patentovat vlastní nápady.

#eWf: Vzhledem k rozdílům mezi jednotlivými sektory a ve velikosti
společností, by bylo zajímavé, kdyby byl diferencován i patentový
systém. Bylo by to prospěšné pro inovace.

#bic: Studie holandského ministerstva financí

#inW: Důležitost toho, jakým způsobem je řešena ochrana intelektuálního
vlastnictví se liší segment od segmentu. V oblasti biotechnologií a
léků mají patenty důležitou roli vzhledem k dlouhodobé návratnosti
investic. V oblasti softwarového vývoje probíhá pokrok tak rychle, že
patenty nejsou používány jako nástroj zajišťující ochranu investic.

#Woo: Dále je potřeba se soustředit na obstrukce, které mají původ v
možnosti patentovat software a obchodní metody.

#hkW: EP 2002: Discussion of European-level legislation in the field of
patents for software

#raa: Studie holandských odborníků na počítačové právo, prof.  Bernda
Hugenholtze  a  Reiniera Bakelse vyžádaná Generálním ředitelstvím pro
výzkum Evropského parlamentu.

#lWa: Problémy patentového systému jako takového.

#Wnv: Problém %(q:triviálních patentů) nemůže být vyřešen zdokonalením
zkoumání patentových přihlášek.

#nmn: V USA způsobily softwarové patenty mnoho problémů a to jak
ekonomických, tak administrativních.

#rme: Požadavek na %(q:technický přínos) je v návrhu Komise příliš vágní a
může být snadno obejit. Při stávajícím přístupu Komise může dokonce
umožnit patentovatelnost obchodních metod.

#on0: Monopolkommission report 2002-07-08

#wsb: Německá komise pro kontrolu monopolů (orgán pro dohled nad trhem
spojený s ministerstvem financí) vyzdvihuje praktiky patentových
právníků směřující k patentování software a upozorňuje, že tyto
praktiky nejsou legální a vedou k narušení svodobné soutěže a
potlačují inovace.

#a0m: FR Gov 2002: Report on the Software Economy

#WpW: Zpráva Francouzské státní komise pro hospodářské plánování publikovaná
dne 17. října 2002 ukazuje, že softwarový průmysl ve Francii, který
zaměstnává 270 000 lidí a v roce 1999 měl obrat 31,6  miliard EUR, je
handicapován proprietárními standardy a nebezpečím patentů.
Doporučuje, aby nebylo dovoleno patentovat algoritmy a obchodní
metody. Z patentovatelnosti je nutné vyjmout datové formáty a
standardy, patenty na vynálezy technické povahy, které používají
počítač mají být platné jen na 3-5 let.

#cia: Koski 2002: Technology policy in the telecommunication sector: Market
responses and economic impacts

#rnW: Studie Heliho Koskeho z Finského z Finského institutu pro technologii
objednaná Evropskou komisí.

#Wrl: V segmentu telekomunikací způsobují patenty mnoho problémů.

#skn: Patenty jsou používány především ze strategických důvodů (blokují
konkurenta, zajišťují, že konkurent nemůže zablokovat vás), nikoliv
jako prostředek pro ochranu návratnosti investic

#nom: 2002-09 Opinion Economic and Social Committee on the Commission
Proposal

#WEd: ESC je hlavní poradní orgán EU, jehož názor byl potvrzen při plenárním
hlasování

#nWW: text Komise dovoluje patentování programu provedeného na počítači,

#nse: text Komise legalizuje a kodifikuje stávající praktiky Evropského
patentového úřadu, které jsou dle současných zákonů sporné,

#tnW: text Komise nedokáže zabránit patentování obchodních (a dalších)
metod,

#epW: text Komise nezajištuje interoperabilitu, naopak vnáší do současného
stavu další nejasnosti,

#eWa: vyvolává pochybnosti o cílech Komise když mluví v úvodu o několika
nesouvisejících věcech, např. o pirátství,

#tsf: neexistuje spolehlivá ekonomická analýza dokazující přínosy pro
segment malých a středních podniků,

#tow: je stěží uvěřitelné, že by tato směrnice byla jen tři roky trvajícím
experimentem, na jehož konci bude možné provést zhodnocení a obnovit
původní stav.

#OWW: 2002-12: Opinion Committee for Cultural Affairs and Youth of EP

#nre: %(q:Technický) znamená %(q:aplikující přírodní síly k ovládání
fyzikálních jevů kromě digitální reprezentace informace)

#rot: Zpracování dat nepatří do oblasti technologií

#xeW: 2002-12: Opinion Committee for Industry and Trade of EP

#cvr: Nikdy nemůže být porušeno právo na svododu slova

#rcn: Dosažení interoperability nemůže vést k porušení patentu

#rts: Bessen & Hunt 2003: An Empirical Look at Software Patents

#taa: Softwarové patenty vedly v USA k převedení zdrojů z vývzkumu a vývoje
k patentovým aktivitám.

#sea: Více patentů znamená méně inovací. To platí i pro společnosti, které
vlastní největší množství patentů.

#sie: Většina softwarových patentů je vlastněna velkými společnostmi, které
je nepoužívají pro ochranu svých produktů, ale pro zajištění
strategické pozice.

#nlh: V oblastech, kde vývoj probíhá inkrementálně po malých krocích, brání
softwarové patenty inovacím, místo aby je podporovaly.

#liW: Federal Trade Commission Hearings of 2002-2003

#ccc: V USA Federal Trade Commission (FTC) uspořádala slyšení se
softwarovými společnostmi, které ukázalo všeobecnou nechuť k
softwarovým patentům. Při předchozím slyšení v roce 1994 velké
společnosti, jako je Adobe, Oracle, Autodesk, vyjádřili silný
nesouhlas s patentovatelností software. Tentokrát vystoupil Robert
Barr, víceprezident a šéf přes otázky intelektuálního vlastnictví u
společnosti Cisco, která vede, díky vysoké inovativnosti, na trhu s
internetovými technologiemi a řekl:

#Mti: Podle mých pozorování nejsou patenty tou silou, která by nás hnala
vpřed při zavádění inovací. Motivuje nás konkurence, která přináší na
trh nové produkty a my na ně musíme ihned reagovat. Všechno co děláme
pro zavádění nových produktů bychom dělali i kdybychom nedostávali
patenty na inovace a vylepšení v našich produktech obsažená. Vím to,
protože se mě ještě nikdo nezeptal %(q:Můžeme tohle patentovat?),
předtím než se rozhodl investovat čas a prostředky do vývoje daného
produktu.

#Tne: Čas a peníze, které nás stojí práce s patenty, jejich podávání,
vymáhání, soudy a licence by mohly být lépe využity na vývoj nových
produktů, výzkum a vedly by k dalším inovacím. Místo toho podáváme
každý rok stovky patentů z důvodů, které nesouvisí s ochranou inovací.

#Mea: Navíc hromadění patentů ve skutečnosti neřeší problém  neúmyslného
porušení patentů v důsledku nezávislého vývoje.  Pokud jsme obviněni z
porušení vlastníkem patentu, který nevyrábí ani neprodává výrobky, 
nebo jenž prodává v mnohem menším objemu než my, nemají naše patenty
dostatečnou hodnotu pro druhou stranu k tomu aby zastrašily soudní
spor nebo snížily množství peněz požadované druhou společností.  Místo
toho, aby patentový systém oceňoval inovace, penalizuje  inovativní
společnosti, které úspěšně přinášejí nové výrobky na trh a dotuje či
odměňuje ty, jež při tomto selhali.

#eWa2: The final report of the FTC, published in October 2003, comes to the
conclusion that the patent system stimulates competition and
productivity in some fields (pharma is cited as an example), whereas
it tends to harm both in others, especially where software and
business methods are concerned.  The report expresses doubts as to the
wisdom of past court decisions to admit patentability in these areas
and proposes a series of measures for repairing some of the damage. 
The positons expressed at the hearings are summarised as follows:

#laa: Computer hardware and software industry representatives generally
emphasized competition to develop more advanced technologies as a
driver of innovation in these rapidly changing industries. These
representatives, particularly those from the software industry,
described an innovation process that is generally significantly less
costly than in the pharmaceutical and biotech industries, and they
spoke of a product life cycle that is generally much shorter. Some
software representatives observed that copyrights or open source code
policies facilitate the incremental and dynamic nature of software
innovation. They discounted the value of patent disclosures, because
they do not require the disclosure of a software product's underlying
source code.

#unB: As one of for %(q:urgent measures for strengthening innovation and
economic growth in Germany), a report from Deutsche Bank Research
recommends:

#tti: 3. Set up a balanced IP protection regime to foster the creation and
flow of ideas. Stronger IP protection is not always better. Chances
are that patents on software, common practice in the US and on the
brink of being legalised in Europe, in fact stifle innovation. Europe
could still alter course.

#FAQ: Často kladené dotazy

#top: What is a software patent?

#aWc: A software patent is a patent (20 year monopoly) on all computer
programs that match a specified set of features.

#tfa: Since 1998 the European Patent Office (EPO) has been allowing
%(e:program claims), i.e. claims of the form

#out: computer program [stored on a data carrier], characterised by that
upon loading the program into memory ... [a process with certain
features is carried out].

#ota: Since 1986 the EPO has been allowing process claims to objects where
the only %(q:inventive) achievement lies in data processing and for
which therefore program claims would have been more straightforward.

#tnf: What does the law say about software patents?

#Wen2: Art 52 of the European Patent Convention (Munich Convention of 1973)
says that %(q:programs for computers) are, along with %(q:mathematical
methods) and %(q:presentations of information), not inventions in the
sense of patent law.  The Examination Guidelines of the European
Patent Office (EPO) of 1978 explain:

#oto: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a mathematical method or a presentation or
information.If the contribution to the known art resides solely in a
computer program then the subject matter is not patentable in whatever
manner it may be presented in the claims.  For example, a claim to a
computer characterised by having the particular program stored in its
memory or to a process for operating a computer under control of the
program would be as objectionable as a claim to the program per se or
the program when recorded on magnetic tape.

#vtm: In other words: whenever the allegedly novel achievement could be
adequately framed in a program claim, it is not an invention in the
sense of the law.

#rkv: What are %(q:computer-implemented inventions)?

#aWp: This term was %(ep:introduced) in May 2000 by the EPO as a euphemism
for %(q:computer programs in the context of patent claims), i.e.
non-inventions according to the present law.   This term was
introduced as part of the %(q:Trilateral Project), an effort of patent
offices to create uniform rules for patentability for
%(q:computer-implemented business methods) in USA, Japan and Europe.

#Eid: The European Commission's proposal uses the EPO definition, whereas
the European Parliament has redefined the term to mean the opposite:
technical inventions (engineering solutions involving forces of
nature) for whose implementation a computer is used.  The Council
Working Party has yet another definition which includes both technical
inventions and software innovations.

#hrt: Why did the EP decide to reaffirm the non-patentability of software?

#ihm: Patent law is an economical law, and virtually all economical studies
show mainly negative effects of software patents. The Commission did
not carry out a thorough impact assessment study. The majority of
European companies is against software patents.  The consultative
organs (COR00, ESC02) and two of the European Parliament's concerned
committees advised against legalisation of the EPO practise.

#ede: Don't investments in software development have to be protected?

#erW: Yes, indeed they do.  The major investments in software development
are protected by copyright, and several other protection means (which
require this copyright protection to work) are employed as well.
Software patents undermine the protections offered by copyright.

#bnW: What about mobile phones and washing machines?

#ear: The European Parliament's version allows patents on novel ways of
harnessing forces of nature, regardless of whether or not a computer
is used.  Some of the patents in the telecom (and electronics) sector
are however indeed directed to data abstraction and, in an age of
%(q:media convergence), would cover programming on the Internet. 
There is little reason to assume that pure software patents have any
positive effect on innovation.

#rdt: Why Software -- in particular embedded software -- should not be
patentable

#eWW: Doesn't the EP version contradict international treaties such as
TRIPS?

#Wnl: No.  On the contrary, the Commisson and Council versions arguably
violate TRIPs.

#Wxh: Isn't the Commission/Council version merely confirming the status quo?

#mWe: No.  The Commission/Council texts impose a practise of the European
Patent Office on Europe which is not accepted by all courts and
thereby make 30,000 software and business method patents much more
difficult to contest than they are at present.

#dft: Why is %(q:data processing not a field of technology) and what does
this mean?

#ltT: All a computer can do, is process data, i.e. calculate with symbolic
entities. When it is used to control an invention, the computer still
merely processes data, but the peripheral equipment might be doing
something patentable. This article ensures compatibility with TRIPs
and clarifies that only the peripheral process but not software as
such is patentable.

#eyr: Why does the EP define %(q:technology) by reference to %(q:forces of
nature)?

#Wtn: The directive proponents have insisted that the concept of
%(q:technical [contribution / considerations / effects]) is the only
acceptable criterion for limiting patentable subject matter, and they
have insisted that this directive should clarify what is patentable
and what not.  It follows from this that a definition is needed.

#oas: The reference to %(q:forces of nature) is omnipresent in the
traditional patent law.  It has been codified in the Scandinavian
Patent Law treaty as well as in several patent laws of Eastern Europe
and East Asia.  It appears in most of the decisions against software
patentability of German, but also US, French and other courts.

#nra: Is insistence on %(q:forces of nature) still adequate to today's
inventions?

#onc: Indeed today there is an ongoing trend toward %(q:convergence). 
Everybody tries to abstract from the quirkiness of matter and to
transfer as many problems as possible to the level of data processing.
 This is because data processing is so convenient, so %(q:calculable),
so easy.

#obt: Je velmi moderní dělat věci jednoduše. Otázka ovšem je, zda je stejně
moderní patentovat trivilaity.

#Wmn: Why was the EP not content with the Commission's safeguards for
interoperability?

#rlu: Komise garantuje právo na reverzní inženýring, tedy zjištění jak věc
funguje z hotového výrobku. Zní to sice dobře, ale toto právo stejně
není možné patentem zrušit. Co však článek nedovoluje, je použití
získaných informací. Antiminopolní zákony nedokáží tento problém
vyřešit.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

