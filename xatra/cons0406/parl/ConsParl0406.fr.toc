\select@language {french}
\contentsline {section}{\numberline {1}\'{E}tudes et opinions relatives au projet de directive sur les brevets logiciels}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}AVIS du Comit\'{e} des r\'{e}gions du 18 novembre 1999 sur ``La comp\'{e}titivit\'{e} des entreprises europ\'{e}ennes face \`{a} la mondialisation - comment l'encourager''\cite {cor99fr}}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Pays-Bas en 2000: Ruimere octrooiering van computerprogramma's : technicality of revolutie?\cite {verk00}}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}US National Research Council 2000: The Digital Dilemma\cite {nrc00}}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}CEC 2000: ``Patent protection of computer programmes''\cite {tap00}}{2}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}CEC 2000: ``The Economic Impact of Software Patents''\cite {ipi00}}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Consultation de l'UE par la Commission\cite {ec00}}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}DE 2001 : Cons\'{e}quences micro- et macro\'{e}conomiques de la brevetabilit\'{e} des innovations logicielles\cite {isi01}}{4}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}NL 2001: ``Intellectueel Eigendom en Innovatie -- Over de rol van intellectueel eigendom in de Nederlandse kenniseconomie''\cite {ezi01fr}}{5}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}NL 2002: ``Toets op het concurrentievermogen''\cite {ezt02}}{5}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Bakels \& Hugenholtz en 2002: Discussion sur la l\'{e}gislation au niveau europ\'{e}en dans le domaine des brevets pour le logiciel\cite {bah02}}{6}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}Rapport de la Monopolkommission du 08/07/2002\cite {mok02}}{6}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}FR 2002: Rapport sur l'\'{e}conomie du logiciel\cite {cdp02}}{6}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Koski en 2002 : Politique sur la technologie dans le domaine des t\'{e}l\'{e}communications : R\'{e}ponses du march\'{e} et impacts \'{e}conomiques\cite {kio02}}{6}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}CES 2002-09-24: Avis du Comit\'{e} \'{e}comique et social sur la proposition de la Commission\cite {esc02fr}}{7}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}22/01/2003 : Avis de la commission de la culture, de la jeunesse, de l'\'{e}ducation, des m\'{e}dias et des sports du Parlement europ\'{e}en\cite {cult02}}{7}{subsection.1.15}
\contentsline {subsection}{\numberline {1.16}ITRE 2003-02-21: Avis de la commission de l'industrie, du commerce ext\'{e}rieur, de la recherche et de l'\'{e}nergie du Parlement europ\'{e}en\cite {itre02}}{7}{subsection.1.16}
\contentsline {subsection}{\numberline {1.17}Bessen \& Hunt en 2003 : Une vision empirique des brevets logiciels\cite {bh03}}{7}{subsection.1.17}
\contentsline {subsection}{\numberline {1.18}Auditions de la Federal Trade Commission en 2002-2003\cite {ftc03}}{8}{subsection.1.18}
\contentsline {subsection}{\numberline {1.19}Deutsche Bank Research 2004: Innovation in Germany\cite {db04}}{9}{subsection.1.19}
\contentsline {section}{\numberline {2}Foire aux questions (FAQ)}{9}{section.2}
\contentsline {subsection}{\numberline {2.1}Qu'est-ce qu'un brevet logiciel ?}{9}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Que dit la loi \`{a} propos des brevets logiciels ?}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Que sont des ``inventions mises en oeuvre par ordinateur'' ?}{10}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Pourquoi le Parlement europ\'{e}en a-t-il d\'{e}cid\'{e} de r\'{e}affirmer la non brevetabilit\'{e} du logiciel ?}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Les investissements dans le logiciel ne doivent-ils pas \^{e}tre prot\'{e}g\'{e}s ?}{11}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Que se passe-t-il pour les t\'{e}l\'{e}phones mobiles et les machines \`{a} laver ?}{11}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}La version du Parlement europ\'{e}en ne contredit-elle pas les trait\'{e}s internationnaux tels que les ADPIC ?}{11}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Les textes de la Commission et du Conseil ne confirment-ils pas simplement le status quo ?}{11}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Pourquoi le ``traitement de donn\'{e}es'' est-il exclu de la brevetabilit\'{e} et qu'est-ce que cela signifie ?}{12}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Pourquoi le Parlement europ\'{e}en a-t-il d\'{e}fini la ``technique'' en se r\'{e}f\'{e}rant aux ``forces de la nature'' ?}{12}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Insister sur les ``forces de la nature'' est-il toujours appropri\'{e} aux inventions actuelles ?}{12}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Pourquoi le Parlement europ\'{e}en ne s'est pas satisfait des garde-fous de la Commission sur l'interop\'{e}rabilit\'{e} ?}{12}{subsection.2.12}
