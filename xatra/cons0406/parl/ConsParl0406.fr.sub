\begin{subdocument}{ConsParl0406}{Analyses et opinions derri\`{e}re la d\'{e}cision du Parlement}{http://swpat.ffii.org/lettres/cons0406/parl/index.fr.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\version fran\c{c}aise 2004-06-02 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Le Parlement europ\'{e}en a d\'{e}j\`{a} rejet\'{e}, \`{a} travers une s\'{e}rie d'amendements, les textes de la Commission europ\'{e}enne et de la commission parlementaire \`{a} la justice (JURI), qui \'{e}taient largement identiques dans la formulation et dans l'esprit \`{a} celui approuv\'{e} maintenant par le Conseil. Ces amendements refl\'{e}taient les demandes d'une vaste majorit\'{e} d'innovateurs informatiques et de chercheurs en politique de l'innovation, comprenant les auteurs d'\'{e}tudes command\'{e}es par la Commission, ainsi que l'avis des organes consultatifs de l'UE.}
\begin{sect}{stud}{\'{E}tudes et opinions relatives au projet de directive sur les brevets logiciels}
\begin{sect}{cor99}{Committee des Regions de l'UE 1999\cite{cor99fr}}
La comp'{e}titivit'{e} des entreprises europ'{e}ennes face \`{a} la mondialisation - comment l'encourager

L'avis sign\'{e} par les dirigeants des gouvernements r\'{e}gionaux de toute l'Europe avertit que le syst\`{e}me de brevets n'est pas universellement applicable et en particulier ne devrait pas \^{e}tre \'{e}tendu aux logiciels.
\end{sect}

\begin{sect}{vkd00}{Pays-Bas en 2000: Ruimere octrooiering van computerprogramma's : technicality of revolutie?\cite{vkd00}}
\'{E}tude par Dirk W.F. Verkade, professeur de droit sur la Propri\'{e}t\'{e} intellectuelle, command\'{e}e par le minist\`{e}re n\'{e}erlandais de l'\'{e}conomie, publi\'{e}e dans un ouvrage (ISBN 90-5409-267-X). L'id\'{e}e forte du livre est que l'extension de la brevetabilit\'{e} au logiciel est tr\`{e}s dangereuse et que l'on ne devrait pas sous-estimer la puissance du droit d'auteur dans l'industrie logicielle.
\end{sect}

\begin{sect}{nrc00}{NRC 2000: The Dilemme Numirique\cite{nrc00}}
\begin{itemize}
\item
L'acceptation de brevets logiciels a commenc\'{e} aux \'{E}tats-Unis sans droit de regard des l\'{e}gislateurs (tout comme en Europe).

\item
Des doutes sont apparus sur la capacit\'{e} de l'Office des brevets des \'{E}tats-Unis \`{a} s'occuper de d\'{e}cisions relatives aux brevets logiciels et s'il avait ou non la connaissance suffisante et la disponibilit\'{e} d'informations sur l'\'{e}tat de l'art.

\item
Le march\'{e} du logiciel est diff\'{e}rent des industries traditionnelles : peu ou pas de march\'{e} des ``composants'', la plupart des gens \'{e}crivent des programmes en partant de z\'{e}ro, pas de consultation de la litt\'{e}rature sur les brevets, de grandes chances d'infraction.

\item
L'innovation dans le d\'{e}veloppement de logiciels se produit plus rapidement que dans les autres industries, les brevets sont souvent accord\'{e}s apr\`{e}s que la technologie est devenue obsol\`{e}te.

\item
Les brevets logiciels pourrait entra\^{\i}ner l'industrie du logiciel \`{a} cesser d'\^{e}tre un havre de cr\'{e}ativit\'{e}, la confinant aux grosses entreprises qui concluent des accords de licences crois\'{e}es.
\end{itemize}
\end{sect}

\begin{sect}{tap01}{CEC 2000: ``Patent protection of computer programmes''\cite{tap01fr}}
\'{E}tude sur la pertinence des brevets logiciels par la Direction g\'{e}n\'{e}rale Entreprises de la Commission europ\'{e}enne, conduite par des chercheurs du Royaume-Unis parmis des PME, de grandes entreprises et des instituts de recherche.

\begin{itemize}
\item
Aucun des groupes examin\'{e}s ne fait un grand usage des brevets pour prot\'{e}ger ses investissements (logiciels).

\item
Les PME pensent qu'elles n'auront aucune chance d'\^{e}tre prot\'{e}g\'{e}es par des brevets devant les tribunaux, par manque d'argent.

\item
\'{E}tant donn\'{e} le cycle de vie tr\`{e}s court des programmes d'ordinateur, les PME pensent qu'elles font mieux de d\'{e}penser leur temps dans le d\'{e}veloppement de nouveaux programmes que dans l'obtention de brevets.

\item
Les grandes entreprises brev\`{e}tent plus que les petites.

\item
``Les PME consid\`{e}rent l'\'{e}laboration et l'application de \emph{lois ind\'{e}sirables} comme l'une de leurs inqui\'{e}tudes primordiales.''

\item
``Il existe un consensus g\'{e}n\'{e}ral sur le fait que la brevetabilit\'{e} du logiciel accro\^{\i}tra probablement l'inqui\'{e}tude des PME.''
\end{itemize}
\end{sect}

\begin{sect}{ipi00}{CEC 2000: ``The Economic Impact of Software Patents''\cite{ipi00}}
\'{E}tude command\'{e}e par la DG March\'{e} int\'{e}rieur \`{a} un ``r\'{e}servoir \`{a} pens\'{e}es'' (think-tank), bas\'{e}s \`{a} Londres, d'avocats des brevets bien connus pour leur soutien actif aux brevets logiciels. Apparemment en r\'{e}action \`{a} la p\'{e}tition d'Eurolinux, l'\'{e}tude juridique initiale a \'{e}t\'{e} renomm\'{e}e pour devenir une ``\'{e}tude sur l'impact \'{e}conomique'' et un \'{e}conomiste a \'{e}t\'{e} recrut\'{e} pour \'{e}crire le chapitre sur l'\'{e}conomie, qui cependant n'a pas d\'{e}bouch\'{e} sur les conclusions esp\'{e}r\'{e}es par la Commission. L'\'{e}tude a donc \'{e}t\'{e} mise sous cl\'{e} pendant 6 mois avant de devenir la base d'une consultation (con00):

\begin{quote}
{\it Ainsi que le montre notre \'{e}tude \'{e}conomique de la litt\'{e}rature (Partie III de notre rapport), la plupart des \'{e}conomistes ont des doutes sur le fait qu'une efficacit\'{e} \'{e}conomique, i.e. une sant\'{e} globale accrue, soit atteinte en ayant ou rendant brevetables les inventions relatives aux programmes d'ordinateur. Cette mise en garde est soutenue par l'inqui\'{e}tude permanente, et m\^{e}me croissante, aux \'{E}tats-Unis sur les probl\`{e}mes autour des brevets sur les inventions relatives aux programmes d'ordinateur. Le d\'{e}bat aux \'{E}tats-Unis n'est pas termin\'{e}.

Il n'existe aucune preuve que les effets positifs d\'{e}coulant de la possession de brevets logiciels l'emportent sur les profondes inqui\'{e}tudes suivantes :

\begin{itemize}
\item
que des brevets ont \'{e}t\'{e} accord\'{e}s pour des id\'{e}es triviales, et en fait anciennes, et que prendre en consid\'{e}ration de tels brevets, sans parler de les attaquer, est une charge majeure pour les PME et les d\'{e}veloppeurs ind\'{e}pendants de logiciels ;

\item
que les brevets peuvent renforcer la position sur le march\'{e} des grands acteurs ; et

\item
que les industries relatives aux programmes d'ordinateur sont des exemples d'industries o\`{u} se produisent des innovations incr\'{e}mentales et o\`{u} il existe de s\'{e}rieus doutes que, dans de telles industries, les brevets accroissent le bien-\^{e}tre.
\end{itemize}}
\end{quote}
\end{sect}

\begin{sect}{con00}{Consultation de l'UE par la Commission\cite{con00}}
Apr\`{e}s une d\'{e}cision inattendue des gouvernements nationaux de s'abstenir de changer l'article 52 de la Convention sur le brevet europ\'{e}en (CBE), la Commission a annonc\'{e} un autre ``processus de consultation''. Les consultations pr\'{e}c\'{e}dentes n'avaient impliqu\'{e} que le groupe des confr\`{e}res de l'Unit\'{e} Propri\'{e}t\'{e} industrielle, i.e. environ 40 avocats des brevets, et n'avaient pos\'{e} que des questions adapt\'{e}es \`{a} ce groupe de confr\`{e}res. La nouvelle consultation a \'{e}t\'{e} con\c{c}ue de la m\^{e}me mani\`{e}re mais, \`{a} cause du d\'{e}gr\'{e} d'attention publique plus \'{e}lev\'{e} que le processus avait atteint entre temps, elle a re\c{c}ue environ 1500 r\'{e}ponses provenant d'origines auxquelles elle ne 'attendait pas.

Quel pourcentage de participants appartenant aux groupes suivants \'{e}tait contre les brevets logiciels :

\begin{center}
\begin{tabular}{R{44}L{44}}
Particuliers & 98.5\percent{}\\
PME & 95\percent{}\\
grosses enterprises & 81\percent{}\\
Associations & 45\percent{}\\
Utilisateurs & 99,6\percent{}\\
\'{E}tudiants & 99,5\percent{}\\
Universitaires & 98,0\percent{}\\
D\'{e}veloppeurs de logiciels & 95,8\percent{}\\
Professionels des brevets & 33\percent{}\\
Gouvernements & 22\percent{}\\
\end{tabular}
\end{center}

Il ressort de ceci que les administrateurs gouvernementaux des brevets sont encore plus fortement partiaux en faveur des pratiques de l'OEB que ne le sont les grosses entreprises et les avocats des brevets. Cela ne surprend personne puisque les pratiques de l'OEB ont \'{e}t\'{e} amen\'{e}es par ces m\^{e}mes personnes, qui repr\'{e}sentent leurs gouvernements au Conseil d'administration de l'OEB et au Groupe de travail sur les brevets du Conseil.

La Commission a conclu \`{a} partir des d\'{e}clarations de certaines associations telles que l'EICTA ou UNICE, dont la politique en mati\`{e}re de brevets est conduite par les avocats en brevets de grosses entreprises, qu'une ``majorit\'{e} \'{e}conomique'' \'{e}tait en faveur des brevets logiciels. Cependant, les 2/3 des emplois et des taxes per\c{c}ues dans le secteur du logiciel proviennent des PME, dont tr\`{e}s peu ont int\'{e}r\^{e}t \`{a} breveter.

voir aussi La ``majorit\'{e} \'{e}conomique'' dans le d\'{e}bat sur les brevets logiciels\footnote{\url{http://swpat.ffii.org/stidi/sektor/index.de.html}}
\end{sect}

\begin{sect}{isi01}{DE 2001 : Cons\'{e}quences micro- et macro\'{e}conomiques de la brevetabilit\'{e} des innovations logicielles\cite{isi01}}
Enqu\^{e}te men\'{e}e parmi plusieurs centaines d'entreprises par l'Institut Fraunhofer pour la recherche sur l'innovation et l'Institut Max-Planck sur la propri\'{e}t\'{e} industrielle, command\'{e}e par le d\'{e}partement des brevets du Minist\`{e}re de l'\'{e}conomie allemand, tous ayant un fort parti pris pro-brevets, r\'{e}v\'{e}lant pourtant les r\'{e}sulats suivants :
\begin{itemize}
\item
les brevets sont le moyen le moins utilis\'{e} et le moins important pour prot\'{e}ger les investissements dans le d\'{e}veloppement de logiciels ;

\item
le temps de d\'{e}veloppement est tr\`{e}s court et l'innovation advient extr\^{e}mement rapidement dans le domaine du logiciel en comparaison avec d'autres domaines ;

\item
il y a plus de d\'{e}veloppement incr\'{e}mental dans le secteur logiciel que dans la plupart des autres industries ;

\item
les processus d'innovation rapide et de d\'{e}veloppement efficace sont encore plus importants dans le logiciel que dans les autres domaines, les obstacles \`{a} la poursuite du travail de d\'{e}veloppement sont donc ici encore plus graves ;

\item
l'interop\'{e}rabilit\'{e} est extr\^{e}mement importante ;

\item
l'intensit\'{e} de la R\&D n'a aucune influence sur l'attitude vis \`{a} vis du brevetage ;

\item
la r\`{e}gle de base comme dans les autres secteurs veut que les plus grosses compagnies obtiennent plus de brevets ;

\item
``la th\'{e}orie selon laquelle les brevets facilitent l'acc\`{e}s au march\'{e}, avant tout pour les jeunes entreprises, n'a pas pu \^{e}tre confirm\'{e}e ;''

\item
``l'avantage strat\'{e}gique des brevets pour la concurrence internationale est \'{e}vident, mais concentr\'{e} sur tr\`{e}s peu de grosses entreprises.''
\end{itemize}
\end{sect}

\begin{sect}{ezi01}{NL 2001: ``Intellectueel Eigendom en Innovatie -- Over de rol van intellectueel eigendom in de Nederlandse kenniseconomie''\cite{ezi01fr}}
Rapport du minist\`{e}re n\'{e}erlandais de l'\'{e}conomie en 2001.
\begin{itemize}
\item
``Un effet en partie impr\'{e}vu d'une prise en main perspicace de la strat\'{e}gie en Propri\'{e}t\'{e} intellectuelle et en brevets de la part des entreprises est l'apparition du probl\`{e}me des `anti-commons' (commons : biens communs, ressources partag\'{e}es par un groupe de gens). Les parties en pr\'{e}sence s'emprisonnent r\'{e}ciproquement dans un champ de mines de brevets. [...] Les PME de la high-tech sont les principales \`{a} souffrir du brevetage strat\'{e}gique.''

\item
``En outre, les brevets ne sont qu'une partie de la strat\'{e}gie globale des entreprises en mati\`{e}re de connaissances. Pour la plupart des entreprises, les brevets sont moins importants que le secret ou que l'avance technologique.''

\item
``Les innovations des PME sont relativement plus encombr\'{e}es par les portefeuilles de brevets existants. Elles rencontrent \'{e}galement plus d'obstacles \`{a} breveter les choses elles-m\^{e}mes.''

\item
``\'{E}tant donn\'{e} les diff\'{e}rences entre les secteurs d'activit\'{e} et celles entre les tailles des entreprises, un syst\`{e}me de brevets diff\'{e}renci\'{e} est une option attrayante du point de vue de l'innovation.''
\end{itemize}
\end{sect}

\begin{sect}{ezt02}{NL 2002: ``Toets op het concurrentievermogen''\cite{ezt02}}
\'{E}tude par le minist\`{e}re n\'{e}erlandais de l'\'{e}conomie.
\begin{itemize}
\item
``L'importance du r\'{e}gime de Propri\'{e}t\'{e} intellectuelle en ce qui concerne l'innovation diff\`{e}re selon les domaines. Dans les domaines biotechniques et pharmaceutiques, les brevets ont un r\^{o}le essentiel \'{e}tant donn\'{e} les d\'{e}lais importants avant le retour sur investissement. Dans le domaine du logiciel, les d\'{e}veloppement sont si rapides que les brevets sont moins utilis\'{e}s pour recouvrir ses investissements.''

\item
``Plus encore, on devrait regarder les obstacles \`{a} l'innovation provenant de la tendance \`{a} breveter les technologies existantes (par ex. : le logiciel) et les m\'{e}thodes d'affaires au large champ d'application.''
\end{itemize}
\end{sect}

\begin{sect}{bah02}{Bakels \& Hugenholtz en 2002: Discussion sur la l\'{e}gislation au niveau europ\'{e}en dans le domaine des brevets pour le logiciel\cite{bah02}}
\begin{itemize}
\item
\'{E}tude command\'{e}e par la Direction g\'{e}n\'{e}rale pour la recherche du Parlement europ\'{e}en

\item
Probl\`{e}mes g\'{e}n\'{e}raux avec le syst\`{e}me de brevets dans son ensemble

\item
Le probl\`{e}me des ``brevets triviaux'' ne peut \^{e}tre r\'{e}solu en am\'{e}liorant les proc\'{e}dures d'examen

\item
Les brevets logiciels ont caus\'{e} beaucoup de probl\`{e}mes aux \'{E}tats-Unis (\`{a} la fois \'{e}conomiques et administratifs)

\item
L'exigence d'une ``contribution technique'' est trop vague dans la proposition de la Commission, peut \^{e}tre ais\'{e}ment contourn\'{e}e et peut m\^{e}me ne pas \^{e}tre pertinente selon l'aveu de la Commission elle-m\^{e}me (en ce qu'elle ne peut emp\^{e}cher toutes les m\'{e}thodes d'affaires d'\^{e}tre brevet\'{e}es)
\end{itemize}
\end{sect}

\begin{sect}{mok02}{DE 2002: Rapport de la Commission de Concurrence\cite{mok02}}
La Commission allemande sur le monopole (organe d'observation de la concurrence attach\'{e} au Minist\`{e}re de l'\'{e}conomie) soul\`{e}ve des inqui\'{e}tudes quant aux r\'{e}centes pratiques dans les offices et tribunaux de brevets consistant \`{a} accorder des brevets logiciels, critique ces pratiques comme \'{e}tant ill\'{e}gales et dangereuses pour l'innovation et la concurrence.
\end{sect}

\begin{sect}{cdp02}{FR 2002: Rapport sur l'\'{e}conomie du logiciel\cite{cdp02}}
Un rapport du Commissariat g\'{e}n\'{e}ral du plan en France sur l'\'{e}conomie du logiciel, publi\'{e} le 17/10/2002, donne des chiffres sur l'industrie du logiciel en France (270 000 employ\'{e}s, chiffre d'affaires de 31,6 Mds d'euros en 1999), consid\`{e}re que l'\'{e}conomie du logiciel de la France est handicap\'{e}e par les standards propri\'{e}taires et les dangers des brevets et recommande que les algorithmes et les m\'{e}thodes d'affaires ne soient pas brevetables; que les formats et standards en soient exempt\'{e}s et que les brevets sur des inventions techniques utilisant des logiciels soient limit\'{e} \`{a} une dur\'{e}e de 3 \`{a} 5 ans.
\end{sect}

\begin{sect}{kio02}{CCE 2002: Politique sur la technologie dans le domaine des t\'{e}l\'{e}communications : R\'{e}ponses du march\'{e} et impacts \'{e}conomiques\cite{kio02}}
\'{E}tude command\'{e}e par la DG Entreprise de la Commission europ\'{e}enne
\begin{itemize}
\item
Les brevets ont caus\'{e} beaucoup de probl\`{e}mes dans le secteur des t\'{e}l\'{e}coms

\item
Les brevets sont ici principalement utilis\'{e}s comme moyens strat\'{e}giques (pour bloquer les concurrents, pour s'assurer que vous n'\^{e}tes pas bloqu\'{e} par un concurrent), pas pour recouvrir ses investissements
\end{itemize}
\end{sect}

\begin{sect}{esc02}{CES 2002-09-24: Avis du Comit\'{e} \'{e}comique et social sur la proposition de la Commission\cite{esc02fr}}
Le CES est le principal organe consultatif de l'UE, son avis a \'{e}t\'{e} approuv\'{e} par le vote en s\'{e}ance pl\'{e}ni\`{e}re
\begin{itemize}
\item
Le texte de la Commission autorise les brevets sur les programmes ex\'{e}cut\'{e}s sur un ordinateur

\item
Le texte de la Commission ne fait que codifier les pratiques juridiquement discutables de l'OEB

\item
Le texte de la Commission n'emp\^{e}che pas les brevets sur les m\'{e}thodes d'affaires (ni sur toute autre m\'{e}thode)

\item
Le texte de la Commission ne garanti pas l'interop\'{e}rabilit\'{e} mais \`{a} la place embrouille la question

\item
Doutes sur les intentions de la Commission qui parle de plusieurs choses hors de propos (comme le piratage) dans son introduction

\item
Aucune analyse \'{e}conomique probante montrant les avantages pour les PME

\item
``Il est tr\`{e}s plausible de nous faire croire que la directive serait une sorte d'exp\'{e}rience r\'{e}versible sur trois ans, \`{a} la fin desquels une \'{e}valuation serait faite''
\end{itemize}
\end{sect}

\begin{sect}{cult03}{CULT 2003-01-22: Avis de la commission de la culture, de la jeunesse, de l'\'{e}ducation, des m\'{e}dias et des sports du Parlement europ\'{e}en\cite{cult03fr}}
\begin{itemize}
\item
``Technique'' signifie ``application des forces de la nature pour contr\^{o}ler des effets physiques au-del\`{a} de la repr\'{e}sentation num\'{e}rique de l'information'' (Article 2)

\item
Le traitement de donn\'{e}es n'est pas un domaine technique (Article 3)
\end{itemize}
\end{sect}

\begin{sect}{itre03}{ITRE 2003-02-21: Avis de la commission de l'industrie, du commerce ext\'{e}rieur, de la recherche et de l'\'{e}nergie du Parlement europ\'{e}en\cite{itre03fr}}
\begin{itemize}
\item
La publication ne peut jamais constituer une infraction (Article 5)

\item
L'interop\'{e}rabilit\'{e} ne peut jamais constituer une infraction \`{a} un brevet (Article 6a)
\end{itemize}
\end{sect}

\begin{sect}{beh03}{Bessen \& Hunt en 2003 : Une vision empirique des brevets logiciels\cite{beh03}}
\begin{itemize}
\item
Les brevets logiciels ont conduit aux \'{E}tats-Unis \`{a} un transfert des ressources de R\&D vers des activit\'{e}s li\'{e}es au brevetage

\item
Plus de brevets a conduit \`{a} moins d'innovation m\^{e}me au sein des entreprises qui brevetaient le plus

\item
La plupart des brevets logiciels sont d\'{e}tenus par de grosses entreprises de mat\'{e}riel et sont obtenus pour des raisons strat\'{e}giques plut\^{o}t que pour emp\^{e}cher l'imitation de produits

\item
Les brevets g\^{e}nent l'innovation au lieu de l'encourager dans des domaines o\`{u} la majeure partie de l'innovation est incr\'{e}mentale, tels que le d\'{e}veloppement de logiciels
\end{itemize}
\end{sect}

\begin{sect}{ftc03}{Auditions de la Federal Trade Commission en 2002-2003\cite{ftc03}}
La Commission f\'{e}d\'{e}rale du commerce (Federal Trade Commission, FTC) des \'{E}tats-Unis a conduit des auditions parmi les soci\'{e}t\'{e}s informatiques qui ont montr\'{e} une animosit\'{e} incessante de l'industrie \'{e}tasunienne du logiciel envers les brevets logiciels. Au cours de pr\'{e}c\'{e}dentes auditions en 1994, de grandes entreprises telles qu'Adobe, Oracle ou Autodesk avait exprim\'{e} une forte opposition \`{a} la brevetabilit\'{e} du logiciel. Cette fois, Robert Barr, vice-pr\'{e}sident \`{a} la t\^{e}te de la propri\'{e}t\'{e} industrielle de Cisco Inc., soci\'{e}t\'{e} \`{a} la pointe des technologies internet, que beaucoup voient comme un mod\`{e}le de gestion moderne de l'innovation, a d\'{e}clar\'{e} :

\begin{quote}
{\it J'observe que les brevets n'ont pas \'{e}t\'{e} une force positive pour stimuler l'innovation chez Cisco. Ce qui l'a motiv\'{e}e a \'{e}t\'{e} la concurrence ; apporter de nouveaux produits sur le march\'{e} au moment opportun est crucial. Tout ce que nous avons fait pour cr\'{e}er de nouveaux produits l'aurait \'{e}t\'{e} m\^{e}me si nous n'avions pu obtenir de brevets sur les innovations et inventions contenues dans ces produits. Je sais cela car personne ne m'a jamais demand\'{e} ``peut-on breveter ceci ?'' avant de d\'{e}cider s'il fallait investir du temps et des ressources dans le d\'{e}veloppement du produit.}

{\it [\dots]}

{\it Le temps et l'argent que nous d\'{e}pensons dans les d\'{e}p\^{o}ts des brevets, les poursuites judiciaires, la maintenance, les contentieux et op\'{e}rations de licence pourraient \^{e}tre mieux d\'{e}pens\'{e}s dans le d\'{e}veloppement de produits et la recherche conduisant \`{a} davantage d'innovation. Mais nous d\'{e}posons chaque ann\'{e}e des centaines de brevets pour des motifs sans rapport avec la promotion ou la protection de l'innovation.}

{\it [\dots]}

{\it Plus encore, l'accumulation de brevets ne r\'{e}soud pas vraiment le probl\`{e}me de l'infraction involontaire de brevets par le d\'{e}veloppement ind\'{e}pendant. Si nous sommes accus\'{e}s d'infraction par un d\'{e}tenteur de brevet qui ne fabrique ni ne vend de produits ou qui vend des produits en beaucoup plus petite quantit\'{e} que nous, nos brevets ne valent pas assez pour l'autre partie pour la dissuader d'attenter un proc\`{e}s ou pour r\'{e}duire la somme d'argent exig\'{e}e par l'autre soci\'{e}t\'{e}. Ainsi, au lieu de r\'{e}compenser l'innovation, le syst\`{e}me de brevets p\'{e}nalise les soci\'{e}t\'{e}s innovantes qui r\'{e}ussisent \`{a} mettre sur le march\'{e} de nouveaux produits et il subventionne ou r\'{e}compense ceux qui n'y arrivent pas.}
\end{quote}

Le rapport final de la FTC, publi\'{e} en octobre 2003, est arriv\'{e} \`{a} la conclusion que le syst\`{e}me de brevets stimulait la concurrence et la productivit\'{e} dans certains domaines (la pharmacie est cit\'{e}e en exemple), alors qu'il avait tendance \`{a} causer du tort dans d'autres domaines, particuli\`{e}rement lorsqu'il s'applique aux logiciels et aux m\'{e}thodes d'affaires. Le raport \'{e}met des doutes quant au bon sens des d\'{e}cisions de justice pass\'{e}es admettant la brevetabilit\'{e} dans ces secteurs et propose une serie de mesures pour r\'{e}parer certains des dommages occasionn\'{e}s. Les positions exprim\'{e}es durant les auditions sont ainsi r\'{e}sum\'{e}es :

\begin{quote}
{\it Les repr\'{e}sentants de l'industrie du mat\'{e}riel informatique et du logiciel ont en g\'{e}n\'{e}ral mis l'accent sur la concurrence dans le d\'{e}veloppement de technologies plus en avance comme moteur de l'innovation dans ces industries \'{e}voluant rapidement. Ces repr\'{e}sentants, particuli\`{e}rement ceux de l'industrie logicielle, ont d\'{e}crit un processus d'innovation  qui est en g\'{e}n\'{e}ral significativement moins co\^{u}teux que dans les industries pharmaceutique et biotechnologique, et ils ont parl\'{e} d'un cycle de vie du produit qui est en g\'{e}n\'{e}ral bien plus court. Certains repr\'{e}sentant du logiciel ont observ\'{e} que les droits d'auteurs ou les politiques de code source ouvert facilitait la nature incr\'{e}mentale et dynamique de l'innovation dans le logiciel. Ils ont discr\'{e}dit\'{e} la valeur des divulgations de brevets, parce qu'elles n'exigeaient pas la divulgation du code source sous-jacente au produit logiciel.}
\end{quote}
\end{sect}

\begin{sect}{dbr04}{Deutsche Bank Research 2004: Innovation in Germany\cite{dbr04fr}}
Un rapport du plus grand investisseur financier dans les PME allemandes recommande comme l'une des quatre ``mesures urgentes pour renforcer l'innovation et la croissance \'{e}conomique en Allemagne'' :

\begin{quote}
{\it 3. Mettre en place un r\'{e}gime de protection de la Propri\'{e}t\'{e} intellectuelle (PI) \'{e}quilibr\'{e} pour favoriser la cr\'{e}ation et la circulation des id\'{e}es. Il n'est pas toujours plus avantageux d'avoir une plus forte protection de la PI. Il y a des chance pour que les brevets sur les logiciels, qui sont une pratique courante aux USA et en passe d'\^{e}tre l\'{e}galis\'{e}s en Europe, \'{e}touffent en fait l'innovation. L'Europe pourrait encore changer de ligne de conduite.}
\end{quote}
\end{sect}
\end{sect}

\begin{sect}{faq}{Foire aux questions (FAQ)}
\begin{sect}{qswp}{Qu'est-ce qu'un brevet logiciel ?}
Un brevet logiciel est un brevet dont les revendications s'appliquent \`{a} tous les programmes d'ordinateur correspondant \`{a} certaines fonctionnalit\'{e}s.

Depuis 1998, L'Office europ\'{e}en des brevets (OEB) a accord\'{e} des \emph{revendications de programmes}, i.e. des revendications sous la forme :

\begin{quote}
{\it un programme d'ordinateur [stock\'{e} sur un support de donn\'{e}es], caract\'{e}ris\'{e} par le fait que lorsqu'on charge le programme en m\'{e}moire ... [un proc\'{e}d\'{e} avec certaines fonctionnalit\'{e}s est r\'{e}alis\'{e}].}
\end{quote}

Depuis 1986, l'OEB a accord\'{e} des revendications de proc\'{e}d\'{e} sur des objets o\`{u} la seule r\'{e}alisation ``inventive'' r\'{e}side dans du traitement de donn\'{e}es et pour laquelle des revendications de programmes auraient donc \'{e}t\'{e} plus simples et directes.
\end{sect}

\begin{sect}{epc}{Que dit la loi \`{a} propos des brevets logiciels ?}
L'article 52 de la Convention sur le brevet europ\'{e}en (Convention de Munich de 1973) pr\'{e}cise que ``les programmes d'ordinateur'', ainsi que ``les m\'{e}thodes math\'{e}matiques'' et ``les pr\'{e}sentations d'informations'', ne sont pas des inventions au sens du droit des brevets.  Les directives d'examen de l'Office europ\'{e}en des brevets (OEB) de 1978 expliquent :

\begin{quote}
{\it Un programme d'ordinateur peut prendre diff\'{e}rentes formes, par exemple un algorithme, un organigramme ou une s\'{e}rie d'instructions cod\'{e}es, qui peut \^{e}tre enregistr\'{e} sur bande ou sur tout un autre appareil d'enregistrement et qui peut \^{e}tre consid\'{e}r\'{e} comme un cas particulier soit d'une m\'{e}thode math\'{e}matique, soit d'une pr\'{e}sentation d'informations. Si la contribution \`{a} l'\'{e}tat de l'art connu r\'{e}side seulement dans un programme d'ordinateur alors l'objet n'est pas brevetable quelque soit la mani\`{e}re dont les revendications sont pr\'{e}sent\'{e}es. Par exemple, une revendication sur un ordinateur caract\'{e}ris\'{e} par le fait d'avoir le programme particulier stock\'{e} dans sa m\'{e}moire ou d'un proc\'{e}d\'{e} pour agir sur un ordinateur sous le contr\^{o}le du programme serait aussi discutable qu'une revendication d'un programme en tant que tel ou du programme lorsqu'il serait enregistr\'{e} sur une bande magn\'{e}tique.}
\end{quote}

En d'autres termes : \`{a} chaque fois qu'une r\'{e}alisation pr\'{e}tendument nouvelle pour laquelle on recherche une protection par un brevet tombe dans la port\'{e}e d'une r\'{e}clamation de programme, elle n'est pas brevetable..

voir aussi Art 52 CEB: Interpr\'{e}tation and R\'{e}vision\footnote{\url{http://swpat.ffii.org/stidi/epc52/index.en.html}}
\end{sect}

\begin{sect}{cii}{Que sont des ``inventions mises en oeuvre par ordinateur'' ?}
Ce terme a \'{e}t\'{e} introduit\footnote{\url{http://swpat.ffii.org/papiers/epo-tws-app6/index.en.html}} en mai 2000 par l'OEB comme un euph\'{e}misme pour un ``programme d'ordinateur dans le contexte de revendications de brevets'', i.e. des non-inventions selon le droit actuel. Ce terme a \'{e}t\'{e} inclu dans le ``Projet trilat\'{e}ral'', une tentative des offices de brevets de cr\'{e}er des r\`{e}gles uniformes pour la brevetabilit\'{e} des ``m\'{e}thode d'affaires mises en oeuvres par ordinateur'' aux \'{E}tats-Unis, au Japon et en Europe.

La proposition de la Commission europ\'{e}enne utilise la d\'{e}finition de l'OEB, alors que le Parlement europ\'{e}en a red\'{e}fini le terme dans un sens oppos\'{e} : des inventions techniques (solutions d'ing\'{e}ni\'{e}rie impliquant les forces de la nature) mises en oevre par un ordinateurs. Le Groupe de travail du Conseil a encore une autre d\'{e}finition qui inclut \`{a} la fois les inventions techniques et les innovations logicielles.
\end{sect}

\begin{sect}{ep39}{Pourquoi le Parlement europ\'{e}en a-t-il d\'{e}cid\'{e} de r\'{e}affirmer la non brevetabilit\'{e} du logiciel ?}
Le droit des brevets est un droit \'{e}conomique et pratiquement toutes les \'{e}tudes \'{e}conomiques ont surtout montr\'{e} les effets n\'{e}gatifs des brevets logiciels. La Commission n'a pas effectu\'{e} d'\'{e}tude approfondie sur une estimation des cons\'{e}quences. La majorit\'{e} des entreprises europ\'{e}enne est contre les brevets logiciels. Les organes consultatifs (COR99, ESC02) et deux des commissions parlementaires concern\'{e}es ont mis en garde contre une l\'{e}galisation des pratiques de l'OEB.
\end{sect}

\begin{sect}{prot}{Les investissements dans le logiciel ne doivent-ils pas \^{e}tre prot\'{e}g\'{e}s ?}
Les principaux investissements dans le d\'{e}veloppement de logiciels sont prot\'{e}g\'{e}s par le droit d'auteur et plusieurs autres moyens de protection (qui pour fonctionner n\'{e}cessitent cette protection par le droit d'auteur) sont \'{e}galement employ\'{e}s. Les brevets logiciels sapent les protections offertes par le droit d'auteur.
\end{sect}

\begin{sect}{mobf}{Que se passe-t-il pour les t\'{e}l\'{e}phones mobiles et les machines \`{a} laver ?}
La version du Parlement europ\'{e}en autorise les brevets sur des moyens nouveaux d'exploiter les forces de la nature, ind\'{e}pendamment du fait qu'un ordinateur soit exploit\'{e} ou non. Certains brevets dans le secteur des t\'{e}l\'{e}coms (et de l'\'{e}lectronique) sont pourtant si abstraits et vastes que, \`{a} l'\'{e}poque de la ``convergence des m\'{e}dias'', ils couvriraient aussi l'informatique pure, p.ex. la programmation sur Internet.  Comme montrent les \'{e}tudes mentionn\'{e}es ci-dessus, il y a peu de raisons de supposer que les purs brevets logiciels aient un quelconque effet positif sur l'innovation.

voir aussi Pourquoi le logiciel -- en particulier le logiciel embarqu\'{e} -- ne devrait pas \^{e}tre brevetable\footnote{\url{http://www.debatpublic.net/Members/paigrain/blogue/embedded}} et Int\'{e}r\^{e}ts de la FFII sur la directive de l'UE concernant les brevets logiciels\footnote{\url{http://swpat.ffii.org/stidi/besoins/index.fr.html}}
\end{sect}

\begin{sect}{trips}{La version du Parlement europ\'{e}en ne contredit-elle pas les trait\'{e}s internationnaux tels que les ADPIC ?}
Non. Au contraire, on peut soutenir que ce sont les versions de la Commission et du Conseil qui violent l'Accord sur les ADPIC.

voir L’Accord sur les ADPIC et les brevets logiciels\footnote{\url{http://swpat.ffii.org/stidi/trips/index.fr.html}}
\end{sect}

\begin{sect}{statq}{Les textes de la Commission et du Conseil ne confirment-ils pas simplement le status quo ?}
Non. Les textes de la Commission et du Conseil imposent une pratique de l'Office europ\'{e}en des brevets qui n'est pas accept\'{e}e par tous les tribunaux et rendrait ainsi 30 000 logiciels et m\'{e}thodes d'affaires bien plus difficiles \`{a} contester qu'ils ne le sont \`{a} l'heure actuelle.
\end{sect}

\begin{sect}{adp}{Pourquoi le ``traitement de donn\'{e}es'' est-il exclu de la brevetabilit\'{e} et qu'est-ce que cela signifie ?}
Tout ce que peut faire un ordinateur, c'est du traitement de donn\'{e}es, i.e. du calcul sur des entit\'{e}s symboliques. Quand il est utilis\'{e} pour contr\^{o}ler une invention, l'ordinateur fait encore simplement du traitement de donn\'{e}es mais les \'{e}quipements p\'{e}riph\'{e}riques peuvent faire quelque chose de brevetable. L'article 3 bis du Parlement s'assure de la compatibilit\'{e} avec les ADPIC et clarifie que seul le proc\'{e}d\'{e} p\'{e}riph\'{e}rique est brevetable et non le logiciel en tant que tel.
\end{sect}

\begin{sect}{natur}{Pourquoi le Parlement europ\'{e}en a-t-il d\'{e}fini la ``technique'' en se r\'{e}f\'{e}rant aux ``forces de la nature'' ?}
Les partisans de la directive ont insist\'{e} pour que le concept de ``[contributions / consid\'{e}rations / effets] techniques'' soit le seul crit\`{e}re acceptable pour limiter les objets brevetables et sur le fait que cette directive devrait clarifier ce qui est brevetable et ce qui ne l'est pas. Il s'en suit qu'une d\'{e}finition est n\'{e}cessaire.

La r\'{e}f\'{e}rence aux ``forces de la nature'' est omnipr\'{e}sente dans le droit des brevets traditionnel de l'Europe de l'est et de l'Asie orientale. Elle appara\^{\i}t dans la jurisprudence, particuli\`{e}rement en Allemagne mais \'{e}galement aux \'{E}tats-Unis, en France et dans d'autres tribunaux.

voir Jurisprudence de Brevet sur Terrain Glissant: Le Prix a Payer pour le D\'{e}montage de l'Invention Technique\footnote{\url{http://swpat.ffii.org/stidi/korcu/index.fr.html}}
\end{sect}

\begin{sect}{fnold}{Insister sur les ``forces de la nature'' est-il toujours appropri\'{e} aux inventions actuelles ?}
En effet, il existe aujourd'hui une tendance suivie vers la ``convergence''. Tout le monde essaye de se soustraire aux fantaisies sur la mati\`{e}re et de transf\'{e}rer autant de probl\`{e}mes que possible au niveau du traitement de donn\'{e}es. C'est parce que l'informatique est si commode, si ``calculable'', si facile.

Rendre les choses faciles est peut-\^{e}tre moderne mais est-il \'{e}galement moderne de breveter les choses faciles ?
\end{sect}

\begin{sect}{itop}{Pourquoi le Parlement europ\'{e}en ne s'est pas satisfait des garde-fous de la Commission sur l'interop\'{e}rabilit\'{e} ?}
La Commission ne garantit que le droit \`{a} la r\'{e}tro-ing\'{e}nierie, qui en premier lieu ne peut pas \^{e}tre interdite par un brevet. Son article 6 ne permet pas cependant l'utilisation de l'information d\'{e}couverte. Le droit sur la concurrence (anti-trust) est un outil trop peu tranchant pour r\'{e}soudre ce probl\`{e}me.
\end{sect}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
% mode: latex ;
% End: ;

