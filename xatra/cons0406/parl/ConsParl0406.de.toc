\contentsline {section}{\numberline {1}Meinungen und Studien zur Softwarepatent-Richtlinie}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}STELLUNGNAHME des Ausschusses der Regionen vom 18. November 1999 zum Thema ``Wettbewerbsf\"{a}higkeit der europ\"{a}ischen Unternehmen angesichts der Globalisierung - Wie man sie f\"{o}rdern kann''\cite {cor99de}}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}NL 2000: Ruimere octrooiering van computerprogramma's : technicality of revolutie?\cite {verk00}}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}US National Research Council 2000: The Digital Dilemma\cite {nrc00}}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}CEC 2000: ``Patent protection of computer programmes''\cite {tap00}}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}CEC 2000: ``The Economic Impact of Software Patents''\cite {ipi00}}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Konsultation durch den Europ\"{a}ischen Rat\cite {ec00}}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}DE 2001: Mikro- und makro\"{o}konomische Implikationen der Patentierbarkeit von Softwareinnovationen\cite {isi01}}{5}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}NL 2001: ``Intellectueel Eigendom en Innovatie -- Over de rol van intellectueel eigendom in de Nederlandse kenniseconomie''\cite {ezi01}}{6}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}NL 2002: ``Toets op het concurrentievermogen''\cite {ezt02}}{6}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Bakels \& Hugenholtz 2002: Discussion of European-level legislation in the field of patents for software\cite {bah02}}{7}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}2002-07-08: Bericht der Monopolkommission\cite {mok02}}{7}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}2002-10-17 Franz\"{o}sisches Staatliches Planungskommissariat: Bericht \"{u}ber die Software-Wirtschaft\cite {cdp02}}{7}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Koski 2002: Technology policy in the telecommunication sector: Market responses and economic impacts\cite {kio02}}{8}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}WSA 2002-09-24: Stellungnahme des Wirtschafts- und Sozialausschusses des EP zum Kommissionsvorschlag\cite {esc02}}{8}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}2002-12: Stellungnahme des Ausschusses f\"{u}r Kultur und Bildung des EP\cite {cult02}}{8}{subsection.1.15}
\contentsline {subsection}{\numberline {1.16}ITRE 2003-02-21: Stellungnahme des Ausschusses f\"{u}r Industrie, Forschung und Energie\cite {itre02}}{9}{subsection.1.16}
\contentsline {subsection}{\numberline {1.17}Bessen \& Hunt 2003: An Empirical Look at Software Patents\cite {bh03}}{9}{subsection.1.17}
\contentsline {subsection}{\numberline {1.18}2002/2003: Anh\"{o}rungen der US Federal Trade Commission.\cite {ftc03}}{9}{subsection.1.18}
\contentsline {subsection}{\numberline {1.19}Deutsche Bank Research 2004: Innovation in Germany\cite {db04}}{10}{subsection.1.19}
\contentsline {section}{\numberline {2}H\"{a}ufig gestellte Fragen}{11}{section.2}
\contentsline {subsection}{\numberline {2.1}Was ist ein Softwarepatent?}{11}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Was sagt das Gesetz \"{u}ber Softwarepatente?}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Was sind ``computer-implementierte Erfindungen''?}{12}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Warum hat das Europ\"{a}ische Parlament entschieden die Nichtpatentierbarkeit von Software nochmals zu betonen?}{12}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Sollten Investitionen in der Softwareentwicklung nicht gesch\"{u}tzt werden?}{12}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Was ist mit Mobiltelefonen und Waschmaschinen?}{12}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Widerspricht die Version des Europ\"{a}ischen Parlaments nicht internationalen Vertr\"{a}gen wie TRIPs?}{13}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Best\"{a}tigt die Version der Kommission/des Rates nicht einfach nur den Status Quo?}{13}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Warum ist ``Datenverarbeitung kein Gebiet der Technik'' und was hei{\ss }t das?}{13}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Warum definiert das Europ\"{a}ische Parlament ``Technik'' durch Bezug auf ``Naturgewalten''?}{13}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Ist das Beharren auf ``Naturgewalten'' bei heutigen Erfindungen noch angemessen?}{13}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Warum war das Europ\"{a}ische Parlament mit den Vorkehrungen der Kommission zur Sicherung der Interoperabilit\"{a}t nicht zufrieden?}{14}{subsection.2.12}
