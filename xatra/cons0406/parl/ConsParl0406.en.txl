<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Analysis and Opinion Behind the Parliament's Decision

#descr: The European Parliament has already, through a series of amendments,
rejected texts from the Commission and the JURI Committee, which were
largely identical in wording and spirit to that approved now by the
Council.  The amendments reflected the demands of the vast majority of
software innovators and innovation policy researchers in the EU,
including the authors of studies ordered by the Commisson as well as
the members of the EU's consultative organs

#sto: Studies and Opinions related to the Software Patent Directive Project

#Con: EU Council of Regions 1999

#ett: The competitiveness of European enterprises in the face of
globalisation - How it can be encouraged

#ftW: Opinion signed by heads of regional governments all over Europe
cautions that the patent system is not universally applicable and in
particular should not be extended to software.

#iui: NL 2000: Ruimere octrooiering van computerprogramma's : technicality
of revolutie?

#lhy: Study by Dirk W.F. Verkade, professor of IP law, commissioned by Dutch
Ministery of Economic Affairs, published as a book with ISBN
90-5409-267-X. The book's main thrust is that extension of
patentability to software is very dangerous and the power copyright in
the software business should not be underestimated.

#wWW: Granting of software patents started in the US without oversight from
the legislative branch (just like in Europe)

#Ser: Doubts about ability US Patent Office to handle software patent
related decisions, and whether it has enough knowledge and prior art
information available

#fmi: Software market is different from traditional industries: small or no
market in %(q:components), most people write programs from scratch, no
consultation of patent literature, high chances of infringement

#adn: Innovation in software development occurs faster than in other
industries, patents often granted after the technology has become
obsolete.

#WeW: Software patents may cause the software industry to cease being a
creative cottage industry, restricting it to large companies that
cross-license.

#she: Study on the desirability of software patents ordered by the
Directorate General Enterprises of the European Commission, performed
by UK researchers among SMEs, large enterprises and research
institutions.

#aWf: None of the examined groups makes a lot of use of patents to protect
their (software) investments.

#Wte: SMEs think they will not have a chance when protecting patents in
front of a court due to a lack of money.

#prm: Given the short life span of computer programs, SMEs think they can
better spend their time on the development of new programs, than on
obtaining patents.

#omc: Large companies patent more than small companies.

#tWh: SMEs consider the creation and implementation of %(e:undesirable laws)
as one of their primary concerns.

#afn: There is a general consensus that the patentability of software will
probably pose a growing a concern for SMEs.

#tno: Ordered by DG Internal Market from a London-based think-tank of patent
lawyers who are well known for their advocacy of software patents. 
Apparently in reaction to the Eurolinux Petition, the initial lawyer
study was renamed to become an %(q:economic impact study) and an
economist was ordered to write an economics chapter, which however did
not reach the conclusions desired by the Commission.  The study was
therefore locked away for 6 months until it became a basis of a
%(q:consultation exercise) (con00):

#fon: As shown in our economic study of the literature (Section III of our
report), most economists have doubts whether economic efficiency, i.e.
increased overall welfare, is achieved by having or making computer
program related inventions patentable. This caution is supported by
the continuing, indeed growing, concern in the USA on the issues
surrounding patents on computer program related inventions. The debate
in the States is not finished.

#nrW: There is no evidence that the positive effects stemming from owning
software patents outweigh the following deep concerns:

#dsW: that patents are being granted on trivial, indeed old, ideas and that
consideration of such patents let alone attacking such patents is a
major burden, particularly on SME and independent software developers;

#tai: that patents may strengthen the market position of the big players;
and

#atd: that the computer program related industries are examples of
industries where incremental innovation occurs and that there are
serious concerns whether, in such industries, patents are welfare
enhancing.

#oom: CEC 2000: Consultation on the Planned Software Patent Directive

#hyb: After an unexepected decision of the national governments to refrain
from plans to change Art 52 of the European Patent Convention (EPC),
the Commission announced another %(q:consultation exercise).  Previous
consultations had involved only the peer group of the Industrial
Property Unit, i.e. about 40 corporate patent lawyers, and asked only
questions that were geared to this peer group.  The new consultation
was designed in the same way, but, due to the higher degree of public
attention that the process had meanwhile reached, it received almost
1500 responses from unexpected quarters.

#gWs: What percentage of participants belonging to the following groups was
against software patents:

#nvu: Individuals

#SME: SMEs

#rnr: Large Enterprises

#sio: Associations

#Use: Users

#tdn: Students

#cec: Academics

#fWo: Software Developers

#Wen: Patent Professionals

#nnp: Governments

#ieW: It appears from this that the governmental patent administrators are
even more strongly biased in favor of the EPO practise than the large
enterprises and patent lawyers.  This is no wonder because the EPO
practise was introduced by these people, who represent their
governments on the EPO's Administrative Council and in the EU
Council's Patent Working Party.

#Wts: The Commission concluded from the statements of some of a few
associations such as EICTA and UNICE, whose patent policy is dominated
by patent lawyers of large corporations, that an %(q:economic
majority) was in favor of software patents.  However 2/3 of the
employment and taxes in the software sector come from SMEs, very few
of whom have any interest in patenting.

#EWa: The %(q:Economic Majority) in the Software Patent Debate

#oof: DE 2001: Micro- and Macroeconomic Implications of the Patentability of
Software Innovations

#sPr: Survey among several hundred companies by Fraunhofer Innovation
Research Institute and Max-Planck Institute for Intellectual Property,
ordered by patent department of German Ministry of Economics, all with
heavy pro-patent bias, yet yielding the following results:

#Wns: Patents are the least used way and least significant means to protect
investments in software development

#Wto: Development time is very short and innovation occurs extremely rapidly
in the software field compared to other fields

#ewo: More incremental development in software sector than in most other
industries

#ctn: Rapid innovation and effective development process even more important
in software than in other fields, so obstacles to conducting
development work are even more serious here

#osm: Interoperability is extremely important

#eln: R&D intensity has no influence on patenting behaviour

#Wdi: Basic rule as in other branches holds: bigger companies obtain more
patents

#tjo: The theory that patents facilitate market access, above all for young
companies, could not be confirmed.

#eie: The strategic benefit of patents in international competition is
obvious, but concentrated on very few large companies.

#fra: Report of the Dutch Ministry of Economic Affairs of 2001

#oth: A partial unplanned effect of more conscious handling of the IP and
the patenting strategy of companies is the arising of the problem of
the `anti-commons'. Parties keep each other prisoner in a patent
minefield. [$ldots{}$] Mainly the (high-tech) SMEs suffer from this
strategic patenting.

#loe: Besides, patents are only part of the total knowledge strategy of
companies. For most companies patenting is less important than secrecy
and technological lead time.

#rps: Innovations of SMEs are relatively more encumbered by existing patent
portfolios. They also experience more obstructions to patent things
themselves.

#eWf: Given the differences between sectors and the differences in company
sizes, a differentiated patent system is an attractive option from an
innovation point of view.

#bic: Study by the Dutch Ministry of Economic Affairs.

#inW: The importance of the IP-regime as far as innovation is concerned
differs per sector. In the biotech and pharmaceuticals patents have an
essential role given the long time to earn back investments. In the
software sector developments are so quick that patents are used less
to earn back investments.

#Woo: Further, one should look at the innovation obstructions stemming from
the trend of patenting enabling technologies (e.g.software) and
broadly applicable business methods.

#hkW: Bakels & Hugenholtz 2002: Discussion of European-level legislation in
the field of patents for software

#raa: Study by dutch IT law experts Prof. Bernd Hugenholtz and Reinier
Bakels commissioned by the European Parliament's Directorate General
for Research

#lWa: General problems with patent system as a whole

#Wnv: Problem of %(q:trivial patents) can not be solved by improving
examination

#nmn: Software patents have caused a lot of problems in the US (both
economical and administrative)

#rme: Requirement of %(q:technical contribution) is too vague in Commission
proposal and can easily be circumvented, may even not be relevant by
Commission's own admission (in that it cannot prevent all business
methods from being patented)

#on0: DE 2002-07-08

#wsb: The German Monopoly Commission (competition watch organ associated
with the ministry of economics) raises concerns about the recent
practise of the patent offices and courts of allowing software
patents, criticises this practise as being illegal and harmful to
innovation and competition.

#a0m: FR Gov 2002: Report on the Software Economy

#WpW: A report of the French State Commission on Economic Planning published
on 2002/10/17 gives figures about the software industry in France
(270000 employees, 31,6 bn eur turnover in 1999), sees France's
software economy handicapped by proprietary standards and patent
dangers and recommends that algorithms and business methods should not
be patentable, formats and standards should be exempted and patents
for technical inventions that use software should be limited in
duration to 3-5 years.

#cia: Koski 2002: Technology policy in the telecommunication sector: Market
responses and economic impacts

#rnW: Study by Heli Koski et al from Finnish Institute of Engineering
ordered by European Commission's DG Enterprise

#Wrl: Patents cause a lot of problems in the Telecom sector

#skn: Patents are mainly used strategically there (block competitors, make
sure you are not blocked by a competitor), not to earn back
investments

#nom: ESC 2002-09-24: Opinion Economic and Social Committee on the
Commission Proposal

#WEd: The ESC is main consultative organ of the EU, the opinion was approved
by plenary vote

#nWW: Commission text allows patents on software executed by a computer

#nse: Commission text simply codifies legally questionable EPO practice

#tnW: Commission text does not prevent patents on business (or on any other)
methods

#epW: Commission text does not preserve interoperability, confuses matters
further instead

#eWa: Doubts about intention of Commission, which talks about several
irrelevant things (such as piracy) in its introduction

#tsf: No effective economic analysis that shows benefits for SMEs

#tow: It is hardly plausible to have us believe that the directive would
only be a sort of reversible three-year experiment, at the end of
which an assessment would be made

#OWW: CULT 2003-01-22: Opinion Committee for Cultural Affairs and Youth of
EP

#nre: %(q:Technical) means %(q:application of natural forces to control
physical effects beyond the digital representation of information)

#rot: Data processing is not a field of technology

#xeW: ITRE 2003-02-21: Opinion Committee for Industry and Trade of EP

#cvr: Publication can never be an infringement

#rcn: Interoperability can never constitute patent infringement

#rts: Bessen & Hunt 2003: An Empirical Look at Software Patents

#taa: Software patents have in the US resulted in a transfer of ressources
from R&D to patenting activities.

#sea: More patents meant less innovation even within the companies that
patented most.

#sie: Most software patents are owned by large hardware companies and
obtained for strategic purposes rather than for preventing imitation
of products.

#nlh: Software patents hinder instead of encouraging innovation in fields
where most innovation is incremental, such as in software development

#liW: Federal Trade Commission Hearings of 2002-2003

#ccc: The US Federal Trade Commission (FTC) conducted hearings among
software companies which showed continued general animosity of the US
software industry against software patents.  In previous hearings in
1994, large companies such as Adobe, Oracle, Autodesk had expressed
strong opposition against the patentability of software.  This time,
Robert Barr, vice president and head of intellectual property at Cisco
Inc, a leader in Internet technology whom many view as a model of
modern innovation managment, said:

#Mti: My observation is that patents have not been a positive force in
stimulating innovation at Cisco. Competition has been the motivator;
bringing new products to market in a timely manner is critical. 
Everything we have done to create new products would have been done
even if we could not obtain patents on the innovations and inventions
contained in these products. I know this because no one has ever asked
me %(q:can we patent this?) before deciding whether to invest time and
resources into product development.

#Tne: The time and money we spend on patent filings, prosecution, and
maintenance, litigation and licensing could be better spent on product
development and research leading to more innovation.  But we are
filing hundreds of patents each year for reasons unrelated to
promoting or protecting innovation.

#Mea: Moreover, stockpiling patents does not really solve the problem of
unintentional patent infringement through independent development.  If
we are accused of infringement by a patent holder who does not make
and sell products, or who sells in much smaller volume than we do, our
patents do not have sufficient value to the other party to deter a
lawsuit or reduce the amount of money demanded by the other company. 
Thus, rather than rewarding innovation, the patent system penalizes
innovative companies who successfully bring new products to the
marketplace and it subsidizes or  rewards those who fail to do so.

#eWa2: The final report of the FTC, published in October 2003, comes to the
conclusion that the patent system stimulates competition and
productivity in some fields (pharma is cited as an example), whereas
it tends to harm both in others, especially where software and
business methods are concerned.  The report expresses doubts as to the
wisdom of past court decisions to admit patentability in these areas
and proposes a series of measures for repairing some of the damage. 
The positons expressed at the hearings are summarised as follows:

#laa: Computer hardware and software industry representatives generally
emphasized competition to develop more advanced technologies as a
driver of innovation in these rapidly changing industries. These
representatives, particularly those from the software industry,
described an innovation process that is generally significantly less
costly than in the pharmaceutical and biotech industries, and they
spoke of a product life cycle that is generally much shorter. Some
software representatives observed that copyrights or open source code
policies facilitate the incremental and dynamic nature of software
innovation. They discounted the value of patent disclosures, because
they do not require the disclosure of a software product's underlying
source code.

#unB: As one of for %(q:urgent measures for strengthening innovation and
economic growth in Germany), a report from Deutsche Bank Research
recommends:

#tti: 3. Set up a balanced IP protection regime to foster the creation and
flow of ideas. Stronger IP protection is not always better. Chances
are that patents on software, common practice in the US and on the
brink of being legalised in Europe, in fact stifle innovation. Europe
could still alter course.

#FAQ: FAQ

#top: What is a software patent?

#aWc: A software patent is a patent (20 year monopoly) on all computer
programs that match a specified set of features.

#tfa: Since 1998 the European Patent Office (EPO) has been allowing
%(e:program claims), i.e. claims of the form

#out: computer program [stored on a data carrier], characterised by that
upon loading the program into memory ... [a process with certain
features is carried out].

#ota: Since 1986 the EPO has been allowing process claims to objects where
the only %(q:inventive) achievement lies in data processing and for
which therefore program claims would have been more straightforward.

#tnf: What does the law say about software patents?

#Wen2: Art 52 of the European Patent Convention (Munich Convention of 1973)
says that %(q:programs for computers) are, along with %(q:mathematical
methods) and %(q:presentations of information), not inventions in the
sense of patent law.  The Examination Guidelines of the European
Patent Office (EPO) of 1978 explain:

#oto: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a mathematical method or a presentation or
information.If the contribution to the known art resides solely in a
computer program then the subject matter is not patentable in whatever
manner it may be presented in the claims.  For example, a claim to a
computer characterised by having the particular program stored in its
memory or to a process for operating a computer under control of the
program would be as objectionable as a claim to the program per se or
the program when recorded on magnetic tape.

#vtm: In other words: whenever the allegedly novel achievement could be
adequately framed in a program claim, it is not an invention in the
sense of the law.

#rkv: What are %(q:computer-implemented inventions)?

#aWp: This term was %(ep:introduced) in May 2000 by the EPO as a euphemism
for %(q:computer programs in the context of patent claims), i.e.
non-inventions according to the present law.   This term was
introduced as part of the %(q:Trilateral Project), an effort of patent
offices to create uniform rules for patentability for
%(q:computer-implemented business methods) in USA, Japan and Europe.

#Eid: The European Commission's proposal uses the EPO definition, whereas
the European Parliament has redefined the term to mean the opposite:
technical inventions (engineering solutions involving forces of
nature) for whose implementation a computer is used.  The Council
Working Party has yet another definition which includes both technical
inventions and software innovations.

#hrt: Why did the EP decide to reaffirm the non-patentability of software?

#ihm: Patent law is an economical law, and virtually all economical studies
show mainly negative effects of software patents. The Commission did
not carry out a thorough impact assessment study. The majority of
European companies is against software patents.  The consultative
organs (COR00, ESC02) and two of the European Parliament's concerned
committees advised against legalisation of the EPO practise.

#ede: Don't investments in software development have to be protected?

#erW: Yes, indeed they do.  The major investments in software development
are protected by copyright, and several other protection means (which
require this copyright protection to work) are employed as well.
Software patents undermine the protections offered by copyright.

#bnW: What about mobile phones and washing machines?

#ear: The European Parliament's version allows patents on novel ways of
harnessing forces of nature, regardless of whether or not a computer
is used.  Some of the patents in the telecom (and electronics) sector
are however indeed directed to data abstraction and, in an age of
%(q:media convergence), would cover programming on the Internet. 
There is little reason to assume that pure software patents have any
positive effect on innovation.

#rdt: Why Software -- in particular embedded software -- should not be
patentable

#eWW: Doesn't the EP version contradict international treaties such as
TRIPS?

#Wnl: No.  On the contrary, the Commisson and Council versions arguably
violate TRIPs.

#Wxh: Isn't the Commission/Council version merely confirming the status quo?

#mWe: No.  The Commission/Council texts impose a practise of the European
Patent Office on Europe which is not accepted by all courts and
thereby make 30,000 software and business method patents much more
difficult to contest than they are at present.

#dft: Why is %(q:data processing not a field of technology) and what does
this mean?

#ltT: All a computer can do, is process data, i.e. calculate with symbolic
entities. When it is used to control an invention, the computer still
merely processes data, but the peripheral equipment might be doing
something patentable. This article ensures compatibility with TRIPs
and clarifies that only the peripheral process but not software as
such is patentable.

#eyr: Why does the EP define %(q:technology) by reference to %(q:forces of
nature)?

#Wtn: The directive proponents have insisted that the concept of
%(q:technical [contribution / considerations / effects]) is the only
acceptable criterion for limiting patentable subject matter, and they
have insisted that this directive should clarify what is patentable
and what not.  It follows from this that a definition is needed.

#oas: The reference to %(q:forces of nature) is omnipresent in the
traditional patent law.  It has been codified in the Scandinavian
Patent Law treaty as well as in several patent laws of Eastern Europe
and East Asia.  It appears in most of the decisions against software
patentability of German, but also US, French and other courts.

#nra: Is insistence on %(q:forces of nature) still adequate to today's
inventions?

#onc: Indeed today there is an ongoing trend toward %(q:convergence). 
Everybody tries to abstract from the quirkiness of matter and to
transfer as many problems as possible to the level of data processing.
 This is because data processing is so convenient, so %(q:calculable),
so easy.

#obt: It may be modern to make things easy, but is it also modern to patent
easy things?

#Wmn: Why was the EP not content with the Commission's safeguards for
interoperability?

#rlu: The Commission only guarantees the right to reverse-engineer, which
cannot be forbidden by a patent in the first place. Its article
however does not allow the use of the discovered information.
Anti-trust law is too blunt a tool to solve this problem.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

