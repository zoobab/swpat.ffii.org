<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Analyses en Opinies Achter de Beslissing van het Parlement

#descr: Het Europees Parlement heeft reeds, d.m.v. een reeks van amendementen,
de teksten van de Commissie en het JURI Comité verworpen. Deze teksten
waren zowel qua verwoording als geest grotendeels gelijk aan het
tegenvoorstel van de Raad. Deze amendementen voldeden aan de
verzuchtingen van de overgrote meerderheid van de Europese
software-innovators en onderzoeken i.v.m. het innovatiebeleid in de
EU, inclusief deze van de Commissie en de raadgevende organen van de
EU.

#sto: Studies en Opinies i.v.m. het Richtlijnproject over Softwarepatenten

#Con: EU Council of Regions 1999

#ett: Het concurrentievermogen van de Europese ondernemingen in de context
van de mondialisering - bevordering van het concurrentievermogen

#ftW: Opinion signed by heads of regional governments all over Europe
cautions that the patent system is not universally applicable and in
particular should not be extended to software.

#iui: NL 2000: Ruimere octrooiering van computerprogramma's : technicality
of revolutie?

#lhy: Study by Dirk W.F. Verkade, professor of IP law, commissioned by Dutch
Ministery of Economic Affairs, published as a book with ISBN
90-5409-267-X. The book's main thrust is that extension of
patentability to software is very dangerous and the power copyright in
the software business should not be underestimated.

#wWW: Granting of software patents started in the US without oversight from
the legislative branch (just like in Europe)

#Ser: Doubts about ability US Patent Office to handle software patent
related decisions, and whether it has enough knowledge and prior art
information available

#fmi: Software market is different from traditional industries: small or no
market in %(q:components), most people write programs from scratch, no
consultation of patent literature, high chances of infringement

#adn: Innovation in software development occurs faster than in other
industries, patents often granted after the technology has become
obsolete.

#WeW: Software patents may cause the software industry to cease being a
creative cottage industry, restricting it to large companies that
cross-license.

#she: Study on the desirability of software patents ordered by the
Directorate General Enterprises of the European Commission, performed
by UK researchers among SMEs, large enterprises and research
institutions.

#aWf: None of the examined groups makes a lot of use of patents to protect
their (software) investments.

#Wte: SMEs think they will not have a chance when protecting patents in
front of a court due to a lack of money.

#prm: Given the short life span of computer programs, SMEs think they can
better spend their time on the development of new programs, than on
obtaining patents.

#omc: Large companies patent more than small companies.

#tWh: SMEs consider the creation and implementation of %(e:undesirable laws)
as one of their primary concerns.

#afn: There is a general consensus that the patentability of software will
probably pose a growing a concern for SMEs.

#tno: Ordered by DG Internal Market from a London-based think-tank of patent
lawyers who are well known for their advocacy of software patents. 
Apparently in reaction to the Eurolinux Petition, the initial lawyer
study was renamed to become an %(q:economic impact study) and an
economist was ordered to write an economics chapter, which however did
not reach the conclusions desired by the Commission.  The study was
therefore locked away for 6 months until it became the basis of a
%(q:consultation exercise).

#fon: As shown in our economic study of the literature (Section III of our
report), most economists have doubts whether economic efficiency, i.e.
increased overall welfare, is achieved by having or making computer
program related inventions patentable. This caution is supported by
the continuing, indeed growing, concern in the USA on the issues
surrounding patents on computer program related inventions. The debate
in the States is not finished.

#nrW: There is no evidence that the positive effects stemming from owning
software patents outweigh the following deep concerns:

#dsW: that patents are being granted on trivial, indeed old, ideas and that
consideration of such patents let alone attacking such patents is a
major burden, particularly on SME and independent software developers;

#tai: that patents may strengthen the market position of the big players;
and

#atd: that the computer program related industries are examples of
industries where incremental innovation occurs and that there are
serious concerns whether, in such industries, patents are welfare
enhancing.

#oom: EU Consultatie door de Commissie

#hyb: After an unexepected decision of the national governments to refrain
from plans to change Art 52 of the European Patent Convention (EPC),
the Commission announced another %(q:consultation exercise).  Previous
consultations had involved only the peer group of the Industrial
Property Unit, i.e. about 40 corporate patent lawyers, and asked only
questions that were geared to this peer group.  The new consultation
was designed in the same way, but, due to the higher degree of public
attention that the process had meanwhile reached, it received almost
1500 responses from unexpected quarters.

#gWs: Percentage of consultation participants who were against software
patents:

#nvu: Individuals

#SME: SMEs

#rnr: Large Enterprises

#sio: Associations

#Use: Users

#tdn: Students

#cec: Academics

#fWo: Software Developers

#Wen: Patent Professionals

#nnp: Governments

#ieW: It appears from this that the governmental patent administrators are
even more strongly biased in favor of the EPO practise than the large
enterprises and patent lawyers.  This is no wonder because the EPO
practise was introduced by these people, who represent their
governments on the EPO's Administrative Council and in the EU
Council's Patent Working Party.

#Wts: The Commission concluded from the statements of a few associations
such as EICTA and UNICE, whose patent policy is dominated by patent
lawyers of large corporations, that an %(q:economic majority) was in
favor of software patents.  However 2/3 of the employment and taxes in
the software sector come from SMEs, very few of whom have any interest
in patenting.

#EWa: The %(q:Economic Majority) in the Software Patent Debate

#oof: DE 2001: Micro- and Macro-economische Gevolgen van de
Octrooieerbaarheid van Software Innovations

#sPr: Survey among several hundred companies by Fraunhofer Innovation
Research Institute and Max-Planck Institute for Intellectual Property,
ordered by patent department of German Ministry of Economics, all with
heavy pro-patent bias, yet yielding the following results:

#Wns: Patents are the least used way and least significant means to protect
investments in software development

#Wto: Development time is very short and innovation occurs extremely rapidly
in the software field compared to other fields

#ewo: More incremental development in software branch than in most other
industries

#ctn: Rapid innovation and effective development process even more important
in software than in other fields, so obstacles to conducting
development work are even more serious here

#osm: Interoperability is extremely important

#eln: R&D intensity has no influence on patenting behaviour

#Wdi: Basic rule as in other branches holds: bigger companies obtain more
patents

#tjo: The theory that patents facilitate market access, above all for young
companies, could not be confirmed.

#eie: The strategic benefit of patents in international competition is
obvious, but concentrated on very few large companies.

#fra: Report of the Dutch Ministry of Economic Affairs of 2001

#oth: A partial unplanned effect of more conscious handling of the IP and
the patenting strategy of companies is the arising of the problem of
the `anti-commons'. Parties keep each other prisoner in a patent
minefield. [$ldots{}$] Mainly the (high-tech) SMEs suffer from this
strategic patenting.

#loe: Besides, patents are only part of the total knowledge strategy of
companies. For most companies patenting is less important than secrecy
and technological lead time.

#rps: Innovations of SMEs are relatively more encumbered by existing patent
portfolios. They also experience more obstructions to patent things
themselves.

#eWf: Given the differences between sectors and the differences in company
sizes, a differentiated patent system is an attractive option from an
innovation point of view.

#bic: Study by the Dutch Ministry of Economic Affairs.

#inW: The importance of the IP-regime as far as innovation is concerned
differs per sector. In the biotech and pharmaceuticals patents have an
essential role given the long time to earn back investments. In the
software sector developments are so quick that patents are used less
to earn back investments.

#Woo: Further, one should look at the innovation obstructions stemming from
the trend of patenting enabling technologies (e.g.software) and
broadly applicable business methods.

#hkW: Bakels & Hugenholtz 2002: Discussion of European-level legislation in
the field of patents for software

#raa: Study ordered by the European Parliament's Directorate General for
Research

#lWa: General problems with patent system as a whole

#Wnv: Problem of %(q:trivial patents) can not be solved by improving
examination

#nmn: Software patents have caused a lot of problems in the US (both
economical and administrative)

#rme: Requirement of %(q:technical contribution) is too vague in Commission
proposal and can easily be circumvented, may even not be relevant by
Commission's own admission (in that it cannot prevent all business
methods from being patented)

#on0: Monopolkommission report 2002-07-08

#wsb: German Monopoly Commission (competition watch organ associated with
the ministry of economics) raises concerns about the recent practise
of the patent offices and courts of allowing software patents,
criticises this practise as being illegal and harmful to innovation
and competition.

#a0m: FR 2002: Rapport sur l'Économie du Logiciel

#WpW: A report of the French State Commission on Economic Planning published
on 2002/10/17 gives figures about the software industry in France
(270000 employees, 31,6 bn eur turnover in 1999), sees France's
software economy handicapped by proprietary standards and patent
dangers and recommends that algorithms and business methods should not
be patentable, formats and standards should be exempted and patents
for technical inventions that use software should be limited in
duration to 3-5 years.

#cia: Koski 2002: Technology policy in the telecommunication sector: Market
responses and economic impacts

#rnW: Study ordered by European Commission's DG Enterprise

#Wrl: Patents cause a lot of problems in the Telecom sector

#skn: Patents are mainly used strategically there (block competitors, make
sure you are not blocked by a competitor), not to earn back
investments

#nom: 2002-09 Opinion Economic and Social Committee on the Commission
Proposal

#WEd: ESC is main consultative organ of the EU, the opinion was approved by
plenary vote

#nWW: Commission text allows patents on software executed by a computer

#nse: Commission text simply codifies legally questionable EPO practice

#tnW: Commission text does not prevent patents on business (or on any other)
methods

#epW: Commission text does not preserve interoperability, confuses matters
further instead

#eWa: Doubts about intention of Commission, which talks about several
irrelevant things (such as piracy) in its introduction

#tsf: No effective economic analysis that shows benefits for SMEs

#tow: It is hardly plausible to have us believe that the directive would
only be a sort of reversible three-year experiment, at the end of
which an assessment would be made

#OWW: 2002-12: Opinion Committee for Cultural Affairs and Youth of EP

#nre: %(q:Technical) means %(q:application of natural forces to control
physical effects beyond the digital representation of information)

#rot: Data processing is not a field of technology

#xeW: 2002-12: Opinion Committee for Industry and Trade of EP

#cvr: Publication can never be an infringement

#rcn: Interoperability can never constitute patent infringement

#rts: Bessen & Hunt 2003: An Empirical Look at Software Patents

#taa: Software patents have in the US resulted in a transfer of ressources
from R&D to patenting activities.

#sea: More patents meant less innovation even within the companies that
patented most.

#sie: Most software patents are owned by large hardware companies and
obtained for strategic purposes rather than for preventing imitation
of products.

#nlh: Software patents hinder instead of encouraging innovation in fields
where most innovation is incremental, such as in software development

#liW: Federal Trade Commission Hearings of 2002-2003

#ccc: The US Federal Trade Commission (FTC) conducted hearings among
software companies which showed continued general animosity of the US
software industry against software patents.  In previous hearings in
1994, large companies such as Adobe, Oracle, Autodesk had expressed
strong opposition against the patentability of software.  This time,
Robert Barr, vice president and head of intellectual property at Cisco
Inc, a leader in Internet technology whom many view as a model of
modern innovation managment, said:

#Mti: My observation is that patents have not been a positive force in
stimulating innovation at Cisco. Competition has been the motivator;
bringing new products to market in a timely manner is critical. 
Everything we have done to create new products would have been done
even if we could not obtain patents on the innovations and inventions
contained in these products. I know this because no one has ever asked
me %(q:can we patent this?) before deciding whether to invest time and
resources into product development.

#Tne: The time and money we spend on patent filings, prosecution, and
maintenance, litigation and licensing could be better spent on product
development and research leading to more innovation.  But we are
filing hundreds of patents each year for reasons unrelated to
promoting or protecting innovation.

#Mea: Moreover, stockpiling patents does not really solve the problem of
unintentional patent infringement through independent development.  If
we are accused of infringement by a patent holder who does not make
and sell products, or who sells in much smaller volume than we do, our
patents do not have sufficient value to the other party to deter a
lawsuit or reduce the amount of money demanded by the other company. 
Thus, rather than rewarding innovation, the patent system penalizes
innovative companies who successfully bring new products to the
marketplace and it subsidizes or  rewards those who fail to do so.

#eWa2: The final report of the FTC, published in October 2003, comes to the
conclusion that the patent system stimulates competition and
productivity in some fields (pharma is cited as an example), whereas
it tends to harm both in others, especially where software and
business methods are concerned.  The report expresses doubts as to the
wisdom of past court decisions to admit patentability in these areas
and proposes a series of measures for repairing some of the damage. 
The positons expressed at the hearings are summarised as follows:

#laa: Computer hardware and software industry representatives generally
emphasized competition to develop more advanced technologies as a
driver of innovation in these rapidly changing industries. These
representatives, particularly those from the software industry,
described an innovation process that is generally significantly less
costly than in the pharmaceutical and biotech industries, and they
spoke of a product life cycle that is generally much shorter. Some
software representatives observed that copyrights or open source code
policies facilitate the incremental and dynamic nature of software
innovation. They discounted the value of patent disclosures, because
they do not require the disclosure of a software product's underlying
source code.

#unB: As one of for %(q:urgent measures for strengthening innovation and
economic growth in Germany), the largest financer of investments in
German SMEs recommends:

#tti: 3. Set up a balanced IP protection regime to foster the creation and
flow of ideas. Stronger IP protection is not always better. Chances
are that patents on software, common practice in the US and on the
brink of being legalised in Europe, in fact stifle innovation. Europe
could still alter course.

#FAQ: FAQ

#top: Wat is een softwarepatent?

#aWc: Een softwarepatent is een patent waarvan de conclusies (claims)
gericht zijn op alle computerprogramma's die aan bepaalde voorwaarden
voldoen.

#tfa: Sinds 1998 laat het Europees Octrooibureau (EOB)
%(e:programmaconclusies) toe, i.e. conclusies van de vorm

#out: computerprogramma [opgeslagen op een drager], gekarakteriseerd door
het feit dat bij het laden van het programma in het geheugen ... [een
proces met bepaalde eigenschappen uitgevoerd wordt].

#ota: Vandaf 1986 liet het EOB reeds procesconclusies toe op objecten
waarbij de enige %(q:inventieve) bijdrage lag in dataverwerking, en
waarvoor programmaconclusies (zoals nu worden toegelaten) bijgevolg
meer voor de hand liggend geweest zouden zijn.

#tnf: Wat zegt de wet over softwarepatenten?

#Wen2: Art 52 van het Europese Octrooiverdrag (Verdrag van München van 1973)
stelt echter dat %(q:programma's voor computers), samen met
%(q:wiskundige methoden) en %(q:presentatie van gegevens), geen
uitvindingen zijn in de zin van de octrooiwetgeving. De
examinatierichtlijnen van het EOB die tot 1985 in voegen waren,
verduidelijken deze beperking:

#oto: Een computerprogramma kan vele vormen aannemen, b.v. een algoritme,
een stroomschema of een reeks van gecodeerde instructies die opgenomen
kunnen worden op een magneetband of op een ander machine-leesbaar
opname-medium, en kan beschouwd worden als een speciaal geval van
ofwel een wiskundige methode (zie boven) of een presentatie van
informatie (zie onder). Indien de bijdrage tot de gekende stand van de
techniek enkel ligt in een computerprogramma, dan is het onderwerp
niet octrooieerbaar, ongeacht de manier waarop het wordt voorgesteld
in de conclusies. Bijvoorbeeld, een conclusie op een computer
gekarakteriseerd door het feit dat een bepaald programma in zijn
geheugen geladen is, of op een proces voor het besturen van een
computer onder programmacontrole, zou net zo laakbaar zijn als een
conclusie op het programma per se of het programma wanneer het
opgenomen is op magneetband.

#vtm: Met andere woorden: steeds wanneer de nieuwe bijdrage waarvoor
octrooibescherming gevraagd wordt binnen het bereik van een
programmaconclusie valt, is deze niet octrooieerbaar.

#rkv: Wat zijn %(q:in computers geïmplementeerde uitvindingen)?

#aWp: Deze term werd %(ep:ingevoerd) in mei 2000 door het EOB als een
eufemisme voor %(q:computerprogramma's in de context van
octrooiconclusies), i.e. non-uitvindingen volgens de huidige
wetgeving. Deze term werd ingevoerd als onderdeel van het
%(q:Trilateraal Project), een poging van de octrooibureaus om uniforme
regels te creëren voor de octrooieerbaarheid van %(q:in computers
geïmplementeerde methoden voor bedrijfsvoering) in de VS, Japan en
Europa.

#Eid: Het voorstel van de Europese Commissie gebruikt de definitie van het
EOB, terwijl het Europees Parlement deze term heeft gedefinieerd als
net het omgekeerde: technische uitvindingen waarbij de nieuwe bijdrage
niet ligt in het berekenen. De Werkgroep van de Raad heeft nog een
andere definitie, dewelke zowel uitvindingen als non-uitvindingen
omvat.

#hrt: Waarom heeft het EP besloten om de niet-octrooieerbaarheid van
software te herbevestigen?

#ihm: Het octrooirecht is een economische wetgeving, en vrijwel alle
economische studies geven hoofdzakelijk negatieve effecten aan van
softwarepatenten. De Commissie heeft geen grondige studie uitgevoerd
over de impact van haar voorstel. De meerderheid van de Europese
bedrijven is tegen softwarepatenten. De consultatieve organen (COR00,
ESC02) en twee van de bevoegde comités van het Europees Parlement
adviseerden tegen de legalisatie van de EOB-praktijk.

#ede: Moeten investeringen in software dan niet beschermd worden?

#erW: Ja, dat moeten ze zeker. De grootste investeringen in
softwareontwikkeling worden beschermd door het auteursrecht, en
verschillende andere beschermingsmanieren (die het auteursrecht nodig
hebben om te kunnen functioneren) worden eveneens gebruikt.
Softwarepatenten ondermijnen deze beschermingen die geboden worden
door het auteursrecht.

#bnW: Wat met mobiele telefoons en wasmachines?

#ear: De versie van het Europees Parlement laat octrooien toe op nieuwe
manieren om natuurkrachten te gebruiken, ongeacht of hiervoor een
computer(programma) gebruikt wordt. Sommige octrooien in de telecom-
en electronicasector zijn echter inderdaad pure softwarepatenten en
zouden, in het tijdperk van de %(q:mediaconvergentie), programmeren
voor het Internet omvatten. Er zijn weinig of geen redenen om aan te
nemen dat pure software patenten enig positief effect hebben op
innovatie.

#rdt: Waarom software -- in het bijzonder ingebedde software -- niet
octrooieerbaar zou mogen zijn.

#eWW: Spreekt de EP-versie internationale verdragen zoals TRIPs niet tegen?

#Wnl: Neen. In tegendeel, de Commissie- en Raadsversies zijn mogelijk in
tegenstrijd met TRIPs.

#Wxh: Bevestigen de Commissie/Raadteksten niet gewoon de status quo?

#mWe: Neen. De Commissie/Raadsteksten leggen de praktijk van het Europese
Octrooibureau, dewelke niet aanvaard wordt door alle rechtbanken, op
aan Europa, en maken op die manier 30,000 softwarepatenten en
octrooien op methoden voor bedrijfsvoering veel moeilijker om te
weerleggen dan ze dat momenteel zijn.

#dft: Waarom wordt %(q:dataverwerking) uitgesloten van octrooieerbaarheid en
wat betekent dit?

#ltT: Het enige wat een computer kan doen, is gegevens verwerken, i.e.
rekenen met symbolische entiteiten. Wanneer hij gebruikt wordt om een
uitvinding te sturen is gegevens verwerken nog steeds het enige wat de
computer doet, maar de aangesloten apparatuur kan mogelijk iets
octrooieerbaar doen. Dit artikel zorgt voor compatibiliteit met TRIPs
en verduidelijkt dat enkel de perifere processen, maar niet software
als zodanig, octrooieerbaar is.

#eyr: Waarom definieert de EP %(q:technologie) d.m.v. verwijzingen naar
%(q:natuurkrachten)?

#Wtn: De voorstanders van de richtlijn stonden erop dat het concept van
%(q:technische [bijdrage / overwegingen / effecten]) het enige
aanvaardbare criterium zou zijn om aan te geven welke zaken inherent
niet octrooieerbaar zijn (i.e., om de %(q:patentable subject matter)
af te bakenen), en ze stonden erop dat deze richtlijn moest
verduidelijken wat octrooieerbaar is en wat niet. Hieruit volgde dat
een definitie nodig was.

#oas: De verwijzing naar %(q:natuurkrachten) is alomtegenwoordig in het
traditionele octrooirecht. Ze is gecodificeerd in het Scandinavische
Octrooiwetgevingsverdrag, evenals in verschillende octooiwetten van
Oost-Europa en Oost-Azië. Ze verschijn in de meeste besluiten tegen
softwarepatenteerbaarheid van Duitse, maar ook van Amerikaanse, Franse
en andere rechtbanken.

#nra: Is aandringen op %(q:natuurkrachten) nog relevant voor de uitvindingen
van vandaag de dag?

#onc: Vandaag de dag is er inderdaad een groeiende trend naar
%(q:convergentie). Iedereen probeert te abstraheren van de
onvoorspelbaarheid van materie en om zoveel mogelijk problemen naar
het niveau van dataverwerking te verheffen. Dit is omdat
dataverwerking zo gemakkelijk, zo %(q:berekenbaar), zo gemakkelijk is.

#obt: Het kan modern zijn om dingen gemakkelijk te maken, maar is het ook
modern om gemakkelijke dingen te octrooieren?

#Wmn: Waarom was het EP niet tevreden met de garanties voor
interoperabiliteit van het EP?

#rlu: De Commissie garandeert enkel het recht om te reverse-engineeren, wat
om te beginnen gewoon niet kan verboden worden door een octrooi. Haar
artikel laat echter het gebruik van de alzo bekomen informatie niet
toe. De mededingingswetgeving is een te log middel om dit probleem op
te lossen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

