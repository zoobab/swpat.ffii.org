<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hintergrundinformationen zur Entscheidung des Parlaments

#descr: Das Europäische Parlament hat bereits durch eine Reihe von Änderungen
einen Entwurf der Kommission (in Wortlaut und Geist mit dem des Rates
weitgehend identisch) zurückgewiesen. Diese Änderungen spiegeln die
Anforderungen der überwiegenden Mehrheit der Software-Entwickler und
der mit Innovations-Politik beschäftigten Forscher wieder,
eingeschlossen die Autoren der von der Kommission in Auftrag gegebenen
Studien und die Mitglieder der EU-Beratungsgremien.

#sto: Meinungen und Studien zur Softwarepatent-Richtlinie

#Con: EU Ausschuss der Regionen 1999

#ett: The competitiveness of European enterprises in the face of
globalisation - How it can be encouraged

#ftW: Die Stellungnahme, unterschrieben von den regionalen Regierungschefs,
warnt, dass das Patentsystem nicht universell anwendbar ist und
insbesondere nicht auf Software ausgeweitet weren sollte.

#iui: NL 2000: Ruimere octrooiering van computerprogramma's : technicality
of revolutie?

#lhy: Study by Dirk W.F. Verkade, professor of IP law, commissioned by Dutch
Ministery of Economic Affairs, published as a book with ISBN
90-5409-267-X. The book's main thrust is that extension of
patentability to software is very dangerous and the power copyright in
the software business should not be underestimated.

#wWW: Die Erteilung von Softwarepatenten begann in den USA (wie auch in
Europa) ohne Einfluss der Legislative.

#Ser: Zweifel an der Fähigkeit des US-Patentamtes, Softwarepatente
betreffende Entscheidungen zu bewältigungen, und die Frage, ob es
überhaupt genug Wissen und Informationen über Prior-Art zur Verfügung
hat.

#fmi: Der Software-Markt unterscheidet sich von traditionellen
Gewerbezweigen: Kleiner bis nicht vorhandener Markt für
%(q:Komponenten), die meisten Leute schreiben Programme von Grund auf,
keine Konsultation von Patent-Literatur, hohe
Patentverletzungs-Chancen

#adn: Die Innvotion findet in der Software-Entwicklung schneller statt als
in anderen Gewerbezeigen, häufig werden Patente erteilt, nachdem die
Technik veraltet geworden ist.

#WeW: Softwarepatente bergen die Gefahr, die Innovation in der heimischen
Software-Entwicklung auszulöschen und auf Großkonzerne zu beschränken,
die untereinander Cross-Licensing-Abkommen abschließen

#she: Study on the desirability of software patents ordered by the
Directorate General Enterprises of the European Commission, performed
by UK researchers among SMEs, large enterprises and research
institutions.

#aWf: None of the examined groups makes a lot of use of patents to protect
their (software) investments.

#Wte: SMEs think they will not have a chance when protecting patents in
front of a court due to a lack of money.

#prm: Given the short life span of computer programs, SMEs think they can
better spend their time on the development of new programs, than on
obtaining patents.

#omc: Large companies patent more than small companies.

#tWh: SMEs consider the creation and implementation of %(e:undesirable laws)
as one of their primary concerns.

#afn: There is a general consensus that the patentability of software will
probably pose a growing a concern for SMEs.

#tno: In Auftrag gegeben von der Generaldirektion Binnenmarkt und durch eine
londoner Gruppe von Patentanwälten durchgeführt, die für ihr Eintreten
für Softwarepatente bekannt sind. Offenbar als Reaktion auf die
Eurolinux-Petition wurde die anfängliche Anwalts-Studie %(q:Studie
über Volkswirtschaftliche Auswirkungen) umbenannt und ein
Volkswirtschaftler beauftragt, ein Wirtschafts-Kapitel zu schreiben,
welches jedoch nicht zu den Schlussfolgerungen kam, die die Kommission
erhofft hatte. Deshalb wurde die Studie für sechs Monate unter
Verschluss gehalten, bis sie die Grundlage einer Konsultation (con00)
wurde:

#fon: Wie in unserer ökonomischen Studie gezeigt (Abschnitt III in unserem
Bericht), haben die meisten Volkswirtschaftler Zweifel, ob
wirtschaftliche Effizienz, d.h. erhöhter allgemeiner Wohlstand,
erreicht wird, indem man Erfindungen bezüglich Computerprogrammen
patentierbar macht. Diese Warnung wird unterstützt durch die
andauernde, ja wachsende Besorgnis rund um Softwarepatente in den USA.
Die Debatte in den Staaten ist noch nicht beendet.

#nrW: Es gibt keine Belege dafür, dass vom Besitzen von Softwarepatenten
ausgelöste positive Effekte die folgenden schweren Bedenken aufwiegen:

#dsW: dass Patente auf triviale sowie alte Ideen erteilt werden und solche
Patente zu berücksichtigen oder gar anzugreifen eine schwere Belastung
ist, insbesondere für kleine und mittelständige Unternehmen und
unabhängige Softwareentwickler;

#tai: dass Patente die Marktposition der Großen stärken werden; und

#atd: dass auf Computerprogramme bezogene Branchen Beispiele für einen
Industriezweig sind, in dem die Innovationen stark aufeinander
aufbauen und dass es ernsthafte Bedenken gibt, ob dort Patente für das
Gemeinwohl förderlich sind.

#oom: Konsultation durch den Europäischen Rat

#hyb: Nach der unerwarteten Entscheidung der nationalen Regierungen, Abstand
von den Plänen zu nehmen, den Artikel 52 des Europäischen
Patentübereinkommens zu ändern, kündigte die Kommission eine weitere
%(q:Konsultation) an. Vorige Beratungen hatten nur die Klientel der
Abteilung für Gewerbliches Eigentum für gewerbliches Eigentum, unter
anderem 40 gemeinsame Patent-Anwälte, die Fragen stellten, die nur auf
ihre Interessengruppe zugeschnitten waren. Die neue Beratung wurde auf
die gleiche Weise durchgeführt, aber auf Grund des höheren
öffentlichen Interesses, das der Prozess in der Zwischenzeit erreicht
hat, trafen fast 1500 Anfragen von unerwarteten Stellen ein.

#gWs: Wie viel Prozent der Teilnehmer aus den folgenden Gruppen waren gegen
Softwarepatente:

#nvu: Einzelpersonen

#SME: Kleine und mittelständige Unternehmen

#rnr: Großunternehmen

#sio: Verbände

#Use: Benutzer

#tdn: Studenten

#cec: Akademiker

#fWo: Softwareentwickler

#Wen: Patentberufler

#nnp: Regierungen

#ieW: Hieraus ist ersichtlich, dass die Patentfunktionäre der Regierungen
noch mehr für die Arbeitsweise des Europäischen Patent-Amtes
eingenommen sind, als große Unternehmen und Patentanwälte. Dies ist
nicht verwunderlich, da diese Praxis des Europäischen Patentamts von
diesen Leuten eingeführt wurde, die ihre Regierungen im
EPA-Verwaltungsrat und in der Arbeitsgruppe Patente des EU-Rates
vertreten.

#Wts: Die Kommission schloss aus den Angaben einiger weniger Verbände wie
EICTA und UNICE, deren Patentpolitik von Patentanwälten großer
Unternehmen dominiert wird, dass sich eine %(q:wirtschaftliche
Mehrheit) für Softwarepatente ausgesprochen habe. Jedoch kommen 2/3
der Arbeitsplätze und Steuern im Software-Sektor auf die kleinen und
mittelständischen Betriebe, von denen nur sehr wenige Interesse am
Patentieren haben.

#EWa: Die %(q:Ökonomische Mehrheit) in der Softwarepatent-Debatte

#oof: DE 2001: Mikro- und makroökonomische Implikationen der
Patentierbarkeit von Softwareinnovationen

#sPr: Eine Umfrage unter mehreren hundert Unternehmen, durchgeführt vom
Fraunhofer Institut für Innovationsforschung und Max-Planck-Institut
für geistiges Eigentum, in Auftrag gegeben von der Patentabteilung des
deutschen Bundesministeriums für Wirtschaft und Technologie, alle mit
starker Pro-Patent-Neigung, kam zu folgenden Ergebnissen:

#Wns: Patente sind die am seltensten genutzte und unbedeutenste Möglichkeit,
Investitionen in der Software-Entwicklung zu schützen.

#Wto: Im Softwarebereich ist die Entwicklungszeit sehr kurz und Innovationen
finden, verglichen mit anderen Branchen, sehr schnell statt.

#ewo: Stärker aufeinander aufbauende Entwicklungen im Softwarebereich als in
fast allen anderen Branchen.

#ctn: Rasche Innovation und effektive Entwicklung sind im Softwarebereich
viel wichtiger als in deren Branchen, daher sind Hindernisse bei der
Durchführung der Entwicklungsarbeit viel ernster.

#osm: Interoperabilität ist extrem wichtig

#eln: Forschungs- und Entwicklungsintensität hat keinen Einfluss auf das
Patentverhalten

#Wdi: Grundregel, wie auch in anderen Branchen, bleibt: größere Unternehmen
bekommen mehr Patente

#tjo: Die These, dass Patente vor allem jungen Unternehmen den Marktzugang
erleichtern, konnte nicht bestätigt werden.

#eie: Der strategische Nutzen von Patenten im internationalen Wettbewerb ist
zwar offensichtlich, aber er ist in der Softwarebranche auf relativ
wenige große Unternehmen konzentriert.

#fra: Report of the Dutch Ministry of Economic Affairs of 2001

#oth: A partial unplanned effect of more conscious handling of the IP and
the patenting strategy of companies is the arising of the problem of
the `anti-commons'. Parties keep each other prisoner in a patent
minefield. [$ldots{}$] Mainly the (high-tech) SMEs suffer from this
strategic patenting.

#loe: Besides, patents are only part of the total knowledge strategy of
companies. For most companies patenting is less important than secrecy
and technological lead time.

#rps: Innovations of SMEs are relatively more encumbered by existing patent
portfolios. They also experience more obstructions to patent things
themselves.

#eWf: Given the differences between sectors and the differences in company
sizes, a differentiated patent system is an attractive option from an
innovation point of view.

#bic: Study by the Dutch Ministry of Economic Affairs.

#inW: The importance of the IP-regime as far as innovation is concerned
differs per sector. In the biotech and pharmaceuticals patents have an
essential role given the long time to earn back investments. In the
software sector developments are so quick that patents are used less
to earn back investments.

#Woo: Further, one should look at the innovation obstructions stemming from
the trend of patenting enabling technologies (e.g.software) and
broadly applicable business methods.

#hkW: Bakels & Hugenholtz 2002: Discussion of European-level legislation in
the field of patents for software

#raa: Die Studie wurde von der Generaldirektion für Forschung des
Europäischen Parlaments in Auftrag gegeben

#lWa: Sie zeigt allgemeine Probleme mit dem Patentsystem als Ganzem auf

#Wnv: Das Problem der %(q:Trivialpatente) kann nicht durch verbesserte
Patentprüfung gelöst werden.

#nmn: Softwarepatente haben in den USA viele wirtschaftliche und
administrative Probleme hervorgerufen.

#rme: Das Erfordernis eines %(q:technischen Beitrags) im Vorschlag der
Kommission ist zu vage und kann leicht umgangen werden, selbst die
Kommission gesteht zu, dass es möglicherweise nicht relevant sei
(insofern, all dass es nicht alle Geschäftsmethoden von der
Patentierbarkeit ausschließt).

#on0: 2002-07-08: Bericht der Monopolkommission

#wsb: Die Monopolkommission (ein dem deutschen Wirtschaftsministerium
angehöriges Wettbewerbsbeobachtungsorgan) drückt Besorgnis über die
jüngste Praxis der Patentämter und -gerichte beim Erteilen von
Softwarepatenten aus. Sie kritisiert diese Praxis als illegal und
schädlich für Innovation und Wettbewerb.

#a0m: 2002-10-17 Französisches Staatliches Planungskommissariat: Bericht
über die Digitale Wirtschaft

#WpW: Das Staatliches Planungskommissariat Frankreichs veröffentlichte am
17.10.2002 eine Studie über die volkswirtschaftliche Planung, die
einen Überblick über die Software-Industrie in Frankreich gibt
(270.000 Angestellte, 31,6 Milliarden Umsatz im Jahr 1999) und
Frankreichs Software-Industrie durch proprietäre Standards und
Patent-Gefahren behindert sieht. Außerdem empfiehlt sie, dass
Algorithmen und Geschäftsmethoden nicht patentierbar sein sollten,
sowie Formate und Standards geöffnet und Patente für technische
Erfindungen, die Software benutzen auf 3-5 Jahre begrenzt werden
sollten.

#cia: Koski 2002: Technology policy in the telecommunication sector: Market
responses and economic impacts

#rnW: Die Studie wurde von der Generaldirektion Unternehmen der Europäischen
Kommission in Auftrag gegeben.

#Wrl: Patente verursachen im Telekommunikationsektor viele Probleme

#skn: Patente werden dort hauptsächlich strategisch eingesetzt (z.B. um
Wettbewerber zu blockieren oder um selbst nicht von einem Konkurrenten
blockiert zu werden), nicht um Investitionen zu sichern

#nom: WSA 2002-09-24: Stellungnahme des Wirtschafts- und Sozialausschusses
des EP zum Kommissionsvorschlag

#WEd: Der Wirtschafts- und Sozialausschuss ist das Haupt-Beratungsorgan der
EU, diese Stellungnahme wurde durch eine Plenar-Abstimmung bestätigt

#nWW: Der Kommissions-Text erlaubt Patente auf Software, die auf einem
Computer ausgeführt wird

#nse: Der Kommissions-Text schreibt einfach die rechtlich fragwürdige Praxis
des EPA fest

#tnW: Der Kommissions-Text verhindert keine Patente auf Geschäfts- und
andere Methoden

#epW: Der Kommissions-Text sichert die Interoperabilität nicht, sondern
verwirrt stattdessen die Angelegenheit

#eWa: Zweifel über die Beweggründe der Kommission, die in ihrer Einleitung
über verschiedene irrelevante Dinge (wie Piraterie) spricht

#tsf: Keine wirkliche ökonomische Analyse, die Vorteile für kleine und
mittelständische Unternehmen zeigt.

#tow: Glauben machen zu wollen, es handele sich bei dem dreijährigen
Zeitraum, nach dem eine Evaluation durchgeführt werden soll, lediglich
um einen jederzeit reversiblen Versuch, ist [...] völlig abwegig,
[...]

#OWW: 2002-12: Stellungnahme des Ausschusses für Kultur und Bildung des EP

#nre: %(q:Technisch) heißt %(q:Anwendung von Naturkräften um physikalische
Effekte jenseits digitaler Information zu kontrollieren)

#rot: Datenverarbeitung ist kein Gebiet der Technik

#xeW: ITRE 2003-02-21: Stellungnahme des Ausschusses für Industrie,
Forschung und Energie

#cvr: Veröffentlichung kann nie eine Patentverletzung sein

#rcn: Interoperabilität kann nie eine Patentverletzung begründen

#rts: Bessen & Hunt 2003: An Empirical Look at Software Patents

#taa: Software-Patente haben in den USA dazu geführt, dass Geldmittel aus
Forschung und Entwicklung in Patent-Aktivitäten umgeleitet wurden.

#sea: Mehr Patente bedeuteten weniger Innovation, sogar in Unternehmen, die
am meisten patentierten.

#sie: Die meisten Software-Patente werden von grossen Hardware-Unternehmen
gehalten und eher für strategische Zwecke eingesetzt, als um
Imitationen von Produkten zu verhindern.

#nlh: Statt sie zu ermutigen, behindern Software-Patente Innovationen auf
Gebieten, wo die meisten Innovationen auf vorherigen Entwicklungen
aufbaut (wie z.B. Software-Entwicklung).

#liW: 2002/2003: Anhörungen der US Federal Trade Commission.

#ccc: Die US Handelsbehörde (FTC) veranstaltete Anhörungen unter
Softwareunternehmen, die eine allgemeine Abneigung der
US-Softwareindustrie gegen Softwarepatente zeigten. In früheren
Anhörungen im Jahre 1994 hatten große Unternehmen wie Adobe, Oracle
oder Autodesk die Patentierbarkeit von Software entschieden abgelehnt.
Diese mal sagte Robert Barr, Vize-Präsident und Leiter der Abteilung
für geistigen Eigentums bei Cisco Inc (einer führenden Firma im
Bereich der Internet-Technologie, die für viele esl Beispiel für
modernes Innovations-Management ansehen):

#Mti: Nach meiner Beobachtung haben Patente niemals eine positive Auswirkung
beim Fördern der Innovation bei Cisco gehabt. Konkurrenz war immer die
Motivation; neue Produkte zeitnah auf den Markt zu bringen ist der
kritische Punkt. Alles, was wir getan haben, um neue Produkte zu
schaffen, hätten wir auch getan, wenn wir keine Patente auf die
Innovationen und Erfindungen in diesen Produkten erwerben hätten
erwerben können. Ich weiß das, weil mich keiner je gefragt %q(:Können
wir das patentierten?), bevor entschieden wurde, ob Zeit und Arbeit in
die Produktentwicklung gesteckt werden.

#Tne: Die Zeit und das Geld, die wir auf Patentanmeldungen,
Patentverfolgung, Patenterhaltung, Patentrechtsstreite, und
Patentlizensierung verwenden, hätten man besser für
Produkt-Entwicklung und -Forschung nutzen können, was zu mehr
Innovation geführt hätte. Aber wir melden jedes Jahr Hunderte von
Patenten an, aus Gründen, die nichts mit der Vermarktung oder dem
Schutz unserer Innovationen zu tun haben.

#Mea: Darüber hinaus schützt das Anhäufen von Patenten nicht wirklich das
Problem unabsichtlicher Patentverletzung durch voneinander unabhängige
Entwicklung. Wenn wir von einem Patentbesitzer verklagt werden, der
keine Produkte herstellt oder verkauft, oder nur in einem sehr viel
geringeren Maße als wir es tun, haben unsere Patente keinen
ausreichenden Wert für die andere Partei um sie von einem Prozess
abzuhalten oder ihre Geldforderungen zu verringern. Statt Innovation
zu belohnen, bestraft das Patentsystem innovative Unternehmen, die
erfolgreich neue Produkte auf den Markt bringen und bezuschußt und
belohnt so diejenigen, die es nicht schaffen.

#eWa2: Der Abschlussbericht der FTC, veröffentlicht in Oktober 2003, kommt zu
dem Schluss, dass das Patentsystem in einigen Gebieten (die
Pharmaindustrie wird als Beispiel genannt) wettbewerbsfördernd und
produktivitätssteigernd wirkt, während es in anderen Gebieten
schädlich ist, besonders dort, wo Software- und Geschäftsmethoden
betroffen sind. Der Bericht drückt Zweifel an der Weisheit früherer
Gerichtsentscheidungen aus, die Patentierbarkeit in diesen Gebieten
zuzulassen und schlägt eine Reihe von Massnahmen vor, um zumindest
einige Schäden zu reparieren. Die Stellungnahmen in der Anhörung
wurden wie folgt zusammengefasst:

#laa: Die Vertreter der Computerhardware- und Softwareindustrie betonen
generell den Wettbewerbsdruck, fortschrittlichere Technologien zu
entwickeln als Antrieb der Innovation in diesen sich schnell
verändernden Branchen. Diese Vertreter, besonders die aus der
Software-Industrie, beschrieben einen Entwicklungsprozess, der
wesentlich weniger kostenaufwendig ist, als in der Pharamazeutik oder
der Biotechnologie, und sprachen von einer Produktlaufzeit, die im
Allgemeinen wesentlich kürzer ist. Einige Software-Vertreter
bemerkten, dass Urheberrechte oder ein %(q:offene Quellen)-Ansatz die
inkrementelle und dynamische Natur von Software-Innovationen
unterstützen. Sie zweifelten den Wert der Offenlegung durch Patente
an, da nicht die Offenlegung des einem Softwareproduktes
zugrundeliegenden Quellcodes gefordert wird.

#unB: Als eine der %(q:vordringendlichsten Maßnahmen zur Stärkung der
Innovation und des wirtschaftlichen Wachstums in Deutschland)
empfiehlt ein Bericht von Deutsche Bank Research:

#tti: 3. Einen ausgeglichenen Schutz des geistigen Eigentums einrichten, um
Schaffung und Fluß von Ideen zu fördern. Stärker Schutz ist nicht
immer besser. Es ist wahrscheinlich, dass Patente auf Software --
allgemeine Praxis in den USA und gerade dabei, in Europa legalisiert
zu werden -- in Wirklichkeit die Innovation ersticken. Europa kann
seinen Kurs noch ändern.

#FAQ: Häufig gestellte Fragen

#top: Was ist ein Softwarepatent?

#aWc: Ein Softwarepatent ist ein Patent (20-Jahre-Monopol) auf alle
Computerprogramme, die eine Menge bestimmter Fähigkeiten aufweisen.

#tfa: Seit 1998 lässt das Europäische Patentamt (EPO) %(e:Programmansprüche)
zu, d.h. Ansprüche der Form

#out: Computerprogram [gespeichert auf einem Datenträger], dadurch
gekennzeichnet, dass beim Laden des Programms in den Speicher ... [ein
Prozess mit bestimmten Eigenschaften ausgeführt wird].

#ota: Seit 1986 lässt das EPO Prozessansprüche für Gegenstände zu, wo die
%(q:erfinderische) Leistung allein in der Datenverarbeitung liegt und
für die daher Programmansprüche passender und ehrlicher gewesen wären.

#tnf: Was sagt das Gesetz über Softwarepatente?

#Wen2: Artikel 52 des Europäischen Patentübereinkommens (Münchener
Übereinkommen von 1973) besagt, dass %(q:Programme für
Datenverarbeitungsanlagen), sowie %(q:mathematische Methoden) und
%(q:die Darstellung von Informationen) keine Erfindungen im Sinne des
Patentrechtes sind. Die Prüfungsrichtlinien des Europäischen
Patentamtes von 1978 erklären:

#oto: Ein Computerprogramm kann in verschiedenen Formen vorliegen, z.B. als
Algorithmus, Flussdiagramm oder als ein Reihe von Befehlsanweisungen
welche auf einem Magnetband oder anderem Computer-lesbaren Medium
gespeichert werden und wahlweise als mathematische Methode oder
Darstellung von Informationen verstanden werden können. Wenn der
Beitrag zu einer bekannten Sache einzig in einem Computer-Programm
besteht, dann ist die Sache nicht patentierbar, egal, in welcher Sache
sie enthalten sein mag. Z.B. würde der Anspruch auf einen Computer,
charakterisiert dadurch, ein bestimmtes Programm im Speicher abgelegt
zu haben oder das Arbeiten eines Computers unter Kontrolle eines
Programmes, den Anspruch auf das Programm per se oder das Programm
gespeichert auf Megnetband unzulässig machen.

#vtm: In anderen Worten: Wenn die angebliche Leistung angemessen in einem
Programmanspruch ausgedrückt werden kann, ist sie keine Erfindung im
Sinne des Gesetzes.

#rkv: Was sind %(q:computer-implementierte Erfindungen)?

#aWp: Dieser Begriff wurde im Mai 2000 vom EPA als eine sprachliche
Beschönigung für %(q:Computerprogramme im Zusammenhang mit
Patentansprüchen), d.h. Nicht-Erfindungen im Sinne des heutigen
Rechts, eingeführt. Dieser Begriff ist Teil des %(q:Trilateral
Project), einem Versuch der Patentämter, einheitliche Richtlinen für
die Patentierbarkeit %(q:computer-implementierter Geschäftsmethoden)
in den USA, Japan und Europa zu schaffen.

#Eid: Der Vorschlag der Europäischen Kommission nutzt die EPA-Definition,
während das Europäische Parlament den Begriff neu definiert hat, um
das Gegenteil auszusagen: Technische Erfindungen (technische Lösungen,
die auf Naturkräfte zurückgreifen), für deren Implementation ein
Computer benutzt wird. Die Arbeitsgruppe des Rates hat wiederum eine
andere Definition, die sowohl technische Erfindungen als auch
Software-Entwicklungen einschließt.

#hrt: Warum hat das Europäische Parlament entschieden die
Nichtpatentierbarkeit von Software nochmals zu betonen?

#ihm: Patentrecht ist ein Wirtschafts-Recht und praktisch alle
Wirtschaftsstudien kommen zu dem Ergebnis, dass Softwarepatente
hauptsächlich negative Auswirkungen haben. Die Kommission führte keine
sorgfältige Studie zur Bewertung der Auswirkungen durch. Die Mehrheit
der europäischen Unternehmen ist gegen Software-Patente! Die
beratenden Organe (COR99, ESC02) und zwei betroffene Gremien des
europäischen Parlamentes rieten von der Legalisierung der Praktiken
des EPA ab.

#ede: Sollten Investitionen in der Softwareentwicklung nicht geschützt
werden?

#erW: Natürlich! Der Großteil der Investitionen für Softwareentwicklung wird
geschützt durch das Urheberrecht und vereinzelte andere Schutzmittel
(die einen funktionierenden Urheberrechtsschutz benötigen).
Softwarepatente untergraben den Schutz, den das Urheberrecht bietet.

#bnW: Was ist mit Mobiltelefonen und Waschmaschinen?

#ear: Die Version des europäischen Parlamentes erlaubt Patente auf neue
Arten, sich Naturkräfte zu Nutze zu machen, ungeachtet ob ein Computer
benutzt wird oder nicht. Einige Patente im Telekommunikations- (und
Elektronik-)Sektor sind allerdings so abstrakt und breitgefächert,
dass sie, im Zeitalter der %(q:Medien-Konvergenz), Programmierung im
Internet abdecken würden. Es gibt wenig Grund zu der Annahme, dass
reine Software-Patente eine positive Auswirkung auf die Innovation
haben werden.

#rdt: Warum Software -- insbesonder Embedded Software -- nicht patentierbar
sein sollte

#eWW: Widerspricht die Version des Europäischen Parlaments nicht
internationalen Verträgen wie TRIPs?

#Wnl: Nein. Im Gegenteil, die Versionen der Kommission und des Rates
verletzen wohl TRIPS.

#Wxh: Bestätigt die Version der Kommission/des Rates nicht einfach nur den
Status Quo?

#mWe: Nein. Die Texte von Kommission und Rat führen in Europa eine Praxis
ein, die von keinem Gericht akzeptiert wird und es dadurch erheblich
schwieriger macht, 30.000 Patente auf Software und Geschäftsmethoden
anzufechten, als es im Moment ist.

#dft: Warum ist %(q:Datenverarbeitung kein Gebiet der Technik) und was heißt
das?

#ltT: Alles, was ein Computer tun kann, ist Daten zu verarbeiten, z.B.
Rechnen mit symbolischen Ausdrücken. Wenn er eingesetzt wird, um eine
Erfindung zu kontrollieren, verarbeitet er lediglich Daten, aber die
angeschlossenen Geräte tun eventuell etwas patentierbares. Dieser
Artikel sichert Kompatibilität mit dem TRIPs-Abkommen und stellt klar,
dass nur externe Prozesse, aber nicht die Software als solche
patentierbar ist.

#eyr: Warum definiert das Europäische Parlament %(q:Technik) durch Bezug auf
%(q:Naturkräfte)?

#Wtn: Die Richtlinien-Befürworter haben darauf bestanden, dass das Konzept
des %(q:technischen Beitrages / Effekts) das einzige akzeptable
Kriterium sei, um eine Patentierbarkeit einzuschränken und sie
bestanden weiter darauf, dass diese Richtlinie klarstellen soll, was
patentierbar ist und was nicht. Es folgt daraus, dass eine Definition
beötigt wird.

#oas: Der Bezug auf %(q:Naturkräfte) ist im traditionellen Patentrecht
omnipräsent. Der Begriff wurde im skandinavischen Patentrechts-Vertrag
kodifiziert , sowie in verschiedenen anderen Patent-Gesetzen in
Osteuropa und Ostasien. Er erscheint in den meisten Entscheidungen
gegen die Patentierbarkeit von Software von deutschen, aber auch
US-amerikanischen, französischen und vielen anderen Gerichten.

#nra: Ist das Beharren auf %(q:Naturgewalten) bei heutigen Erfindungen noch
angemessen?

#onc: In der Tat gibt es heute einen Trend zur Konvergenz. Jeder versucht,
auf der materiellen Ebene zu abstrahieren und soviele Probleme wie
möglich mittels Datenverarbeitung zu lösen. Das kommt davon, dass
Datenverarbeitung so bequem ist, so berechenbar, so einfach.

#obt: Es mag modern sein, Dinge einfach zu machen, aber ist es auch modern,
einfache Dinge zu patentieren?

#Wmn: Warum war das Europäische Parlament mit den Vorkehrungen der
Kommission zur Sicherung der Interoperabilität nicht zufrieden?

#rlu: Die Kommission garantiert nur das Recht zum reverse engineering,
welches nicht von vornherein durch ein Patent untersagt werden darf.
Aber ihr Vorschalg erlaubt die Nutzung der Resultate nicht. Das
Kartellrecht ist zu schwach und zu langsam um dieses Problem zu lösen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

