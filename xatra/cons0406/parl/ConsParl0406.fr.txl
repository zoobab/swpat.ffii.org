<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Analyses et opinions derrière la décision du Parlement

#descr: Le Parlement européen a déjà rejeté, à travers une série
d'amendements, les textes de la Commission européenne et de la
commission parlementaire à la justice (JURI), qui étaient largement
identiques dans la formulation et dans l'esprit à celui approuvé
maintenant par le Conseil. Ces amendements reflétaient les demandes
d'une vaste majorité d'innovateurs informatiques et de chercheurs en
politique de l'innovation, comprenant les auteurs d'études commandées
par la Commission, ainsi que l'avis des organes consultatifs de l'UE.

#sto: Études et opinions relatives au projet de directive sur les brevets
logiciels

#Con: Committee des Regions de l'UE 1999

#ett: La comp'{e}titivit'{e} des entreprises europ'{e}ennes face à la
mondialisation - comment l'encourager

#ftW: L'avis signé par les dirigeants des gouvernements régionaux de toute
l'Europe avertit que le système de brevets n'est pas universellement
applicable et en particulier ne devrait pas être étendu aux logiciels.

#iui: Pays-Bas en 2000: Ruimere octrooiering van computerprogramma's :
technicality of revolutie?

#lhy: Étude par Dirk W.F. Verkade, professeur de droit sur la Propriété
intellectuelle, commandée par le ministère néerlandais de l'économie,
publiée dans un ouvrage (ISBN 90-5409-267-X). L'idée forte du livre
est que l'extension de la brevetabilité au logiciel est très
dangereuse et que l'on ne devrait pas sous-estimer la puissance du
droit d'auteur dans l'industrie logicielle.

#wWW: L'acceptation de brevets logiciels a commencé aux États-Unis sans
droit de regard des législateurs (tout comme en Europe).

#Ser: Des doutes sont apparus sur la capacité de l'Office des brevets des
États-Unis à s'occuper de décisions relatives aux brevets logiciels et
s'il avait ou non la connaissance suffisante et la disponibilité
d'informations sur l'état de l'art.

#fmi: Le marché du logiciel est différent des industries traditionnelles :
peu ou pas de marché des %(q:composants), la plupart des gens écrivent
des programmes en partant de zéro, pas de consultation de la
littérature sur les brevets, de grandes chances d'infraction.

#adn: L'innovation dans le développement de logiciels se produit plus
rapidement que dans les autres industries, les brevets sont souvent
accordés après que la technologie est devenue obsolète.

#WeW: Les brevets logiciels pourrait entraîner l'industrie du logiciel à
cesser d'être un havre de créativité, la confinant aux grosses
entreprises qui concluent des accords de licences croisées.

#she: Étude sur la pertinence des brevets logiciels par la Direction
générale Entreprises de la Commission européenne, conduite par des
chercheurs du Royaume-Unis parmis des PME, de grandes entreprises et
des instituts de recherche.

#aWf: Aucun des groupes examinés ne fait un grand usage des brevets pour
protéger ses investissements (logiciels).

#Wte: Les PME pensent qu'elles n'auront aucune chance d'être protégées par
des brevets devant les tribunaux, par manque d'argent.

#prm: Étant donné le cycle de vie très court des programmes d'ordinateur,
les PME pensent qu'elles font mieux de dépenser leur temps dans le
développement de nouveaux programmes que dans l'obtention de brevets.

#omc: Les grandes entreprises brevètent plus que les petites.

#tWh: Les PME considèrent l'élaboration et l'application de %(e:lois
indésirables) comme l'une de leurs inquiétudes primordiales.

#afn: Il existe un consensus général sur le fait que la brevetabilité du
logiciel accroîtra probablement l'inquiétude des PME.

#tno: Étude commandée par la DG Marché intérieur à un %(q:réservoir à
pensées) (think-tank), basés à Londres, d'avocats des brevets bien
connus pour leur soutien actif aux brevets logiciels. Apparemment en
réaction à la pétition d'Eurolinux, l'étude juridique initiale a été
renommée pour devenir une %(q:étude sur l'impact économique) et un
économiste a été recruté pour écrire le chapitre sur l'économie, qui
cependant n'a pas débouché sur les conclusions espérées par la
Commission. L'étude a donc été mise sous clé pendant 6 mois avant de
devenir la base d'une consultation (con00):

#fon: Ainsi que le montre notre étude économique de la littérature (Partie
III de notre rapport), la plupart des économistes ont des doutes sur
le fait qu'une efficacité économique, i.e. une santé globale accrue,
soit atteinte en ayant ou rendant brevetables les inventions relatives
aux programmes d'ordinateur. Cette mise en garde est soutenue par
l'inquiétude permanente, et même croissante, aux États-Unis sur les
problèmes autour des brevets sur les inventions relatives aux
programmes d'ordinateur. Le débat aux États-Unis n'est pas terminé.

#nrW: Il n'existe aucune preuve que les effets positifs découlant de la
possession de brevets logiciels l'emportent sur les profondes
inquiétudes suivantes :

#dsW: que des brevets ont été accordés pour des idées triviales, et en fait
anciennes, et que prendre en considération de tels brevets, sans
parler de les attaquer, est une charge majeure pour les PME et les
développeurs indépendants de logiciels ;

#tai: que les brevets peuvent renforcer la position sur le marché des grands
acteurs ; et

#atd: que les industries relatives aux programmes d'ordinateur sont des
exemples d'industries où se produisent des innovations incrémentales
et où il existe de sérieus doutes que, dans de telles industries, les
brevets accroissent le bien-être.

#oom: Consultation de l'UE par la Commission

#hyb: Après une décision inattendue des gouvernements nationaux de
s'abstenir de changer l'article 52 de la Convention sur le brevet
européen (CBE), la Commission a annoncé un autre %(q:processus de
consultation). Les consultations précédentes n'avaient impliqué que le
groupe des confrères de l'Unité Propriété industrielle, i.e. environ
40 avocats des brevets, et n'avaient posé que des questions adaptées à
ce groupe de confrères. La nouvelle consultation a été conçue de la
même manière mais, à cause du dégré d'attention publique plus élevé
que le processus avait atteint entre temps, elle a reçue environ 1500
réponses provenant d'origines auxquelles elle ne 'attendait pas.

#gWs: Quel pourcentage de participants appartenant aux groupes suivants
était contre les brevets logiciels :

#nvu: Particuliers

#SME: PME

#rnr: grosses enterprises

#sio: Associations

#Use: Utilisateurs

#tdn: Étudiants

#cec: Universitaires

#fWo: Développeurs de logiciels

#Wen: Professionels des brevets

#nnp: Gouvernements

#ieW: Il ressort de ceci que les administrateurs gouvernementaux des brevets
sont encore plus fortement partiaux en faveur des pratiques de l'OEB
que ne le sont les grosses entreprises et les avocats des brevets.
Cela ne surprend personne puisque les pratiques de l'OEB ont été
amenées par ces mêmes personnes, qui représentent leurs gouvernements
au Conseil d'administration de l'OEB et au Groupe de travail sur les
brevets du Conseil.

#Wts: La Commission a conclu à partir des déclarations de certaines
associations telles que l'EICTA ou UNICE, dont la politique en matière
de brevets est conduite par les avocats en brevets de grosses
entreprises, qu'une %(q:majorité économique) était en faveur des
brevets logiciels. Cependant, les 2/3 des emplois et des taxes perçues
dans le secteur du logiciel proviennent des PME, dont très peu ont
intérêt à breveter.

#EWa: La %(q:majorité économique) dans le débat sur les brevets logiciels

#oof: DE 2001 : Conséquences micro- et macroéconomiques de la brevetabilité
des innovations logicielles

#sPr: Enquête menée parmi plusieurs centaines d'entreprises par l'Institut
Fraunhofer pour la recherche sur l'innovation et l'Institut Max-Planck
sur la propriété industrielle, commandée par le département des
brevets du Ministère de l'économie allemand, tous ayant un fort parti
pris pro-brevets, révélant pourtant les résulats suivants :

#Wns: les brevets sont le moyen le moins utilisé et le moins important pour
protéger les investissements dans le développement de logiciels ;

#Wto: le temps de développement est très court et l'innovation advient
extrêmement rapidement dans le domaine du logiciel en comparaison avec
d'autres domaines ;

#ewo: il y a plus de développement incrémental dans le secteur logiciel que
dans la plupart des autres industries ;

#ctn: les processus d'innovation rapide et de développement efficace sont
encore plus importants dans le logiciel que dans les autres domaines,
les obstacles à la poursuite du travail de développement sont donc ici
encore plus graves ;

#osm: l'interopérabilité est extrêmement importante ;

#eln: l'intensité de la R&D n'a aucune influence sur l'attitude vis à vis du
brevetage ;

#Wdi: la règle de base comme dans les autres secteurs veut que les plus
grosses compagnies obtiennent plus de brevets ;

#tjo: la théorie selon laquelle les brevets facilitent l'accès au marché,
avant tout pour les jeunes entreprises, n'a pas pu être confirmée ;

#eie: l'avantage stratégique des brevets pour la concurrence internationale
est évident, mais concentré sur très peu de grosses entreprises.

#fra: Rapport du ministère néerlandais de l'économie en 2001.

#oth: Un effet en partie imprévu d'une prise en main perspicace de la
stratégie en Propriété intellectuelle et en brevets de la part des
entreprises est l'apparition du problème des `anti-commons' (commons :
biens communs, ressources partagées par un groupe de gens). Les
parties en présence s'emprisonnent réciproquement dans un champ de
mines de brevets. [...] Les PME de la high-tech sont les principales à
souffrir du brevetage stratégique.

#loe: En outre, les brevets ne sont qu'une partie de la stratégie globale
des entreprises en matière de connaissances. Pour la plupart des
entreprises, les brevets sont moins importants que le secret ou que
l'avance technologique.

#rps: Les innovations des PME sont relativement plus encombrées par les
portefeuilles de brevets existants. Elles rencontrent également plus
d'obstacles à breveter les choses elles-mêmes.

#eWf: Étant donné les différences entre les secteurs d'activité et celles
entre les tailles des entreprises, un système de brevets différencié
est une option attrayante du point de vue de l'innovation.

#bic: Étude par le ministère néerlandais de l'économie.

#inW: L'importance du régime de Propriété intellectuelle en ce qui concerne
l'innovation diffère selon les domaines. Dans les domaines
biotechniques et pharmaceutiques, les brevets ont un rôle essentiel
étant donné les délais importants avant le retour sur investissement.
Dans le domaine du logiciel, les développement sont si rapides que les
brevets sont moins utilisés pour recouvrir ses investissements.

#Woo: Plus encore, on devrait regarder les obstacles à l'innovation
provenant de la tendance à breveter les technologies existantes (par
ex. : le logiciel) et les méthodes d'affaires au large champ
d'application.

#hkW: Bakels & Hugenholtz en 2002: Discussion sur la législation au niveau
européen dans le domaine des brevets pour le logiciel

#raa: Étude commandée par la Direction générale pour la recherche du
Parlement européen

#lWa: Problèmes généraux avec le système de brevets dans son ensemble

#Wnv: Le problème des %(q:brevets triviaux) ne peut être résolu en
améliorant les procédures d'examen

#nmn: Les brevets logiciels ont causé beaucoup de problèmes aux États-Unis
(à la fois économiques et administratifs)

#rme: L'exigence d'une %(q:contribution technique) est trop vague dans la
proposition de la Commission, peut être aisément contournée et peut
même ne pas être pertinente selon l'aveu de la Commission elle-même
(en ce qu'elle ne peut empêcher toutes les méthodes d'affaires d'être
brevetées)

#on0: DE 2002: Rapport de la Commission de Concurrence

#wsb: La Commission allemande sur le monopole (organe d'observation de la
concurrence attaché au Ministère de l'économie) soulève des
inquiétudes quant aux récentes pratiques dans les offices et tribunaux
de brevets consistant à accorder des brevets logiciels, critique ces
pratiques comme étant illégales et dangereuses pour l'innovation et la
concurrence.

#a0m: FR 2002: Rapport sur l'économie du logiciel

#WpW: Un rapport du Commissariat général du plan en France sur l'économie du
logiciel, publié le 17/10/2002, donne des chiffres sur l'industrie du
logiciel en France (270 000 employés, chiffre d'affaires de 31,6 Mds
d'euros en 1999), considère que l'économie du logiciel de la France
est handicapée par les standards propriétaires et les dangers des
brevets et recommande que les algorithmes et les méthodes d'affaires
ne soient pas brevetables; que les formats et standards en soient
exemptés et que les brevets sur des inventions techniques utilisant
des logiciels soient limité à une durée de 3 à 5 ans.

#cia: CCE 2002: Politique sur la technologie dans le domaine des
télécommunications : Réponses du marché et impacts économiques

#rnW: Étude commandée par la DG Entreprise de la Commission européenne

#Wrl: Les brevets ont causé beaucoup de problèmes dans le secteur des
télécoms

#skn: Les brevets sont ici principalement utilisés comme moyens stratégiques
(pour bloquer les concurrents, pour s'assurer que vous n'êtes pas
bloqué par un concurrent), pas pour recouvrir ses investissements

#nom: CES 2002-09-24: Avis du Comité écomique et social sur la proposition
de la Commission

#WEd: Le CES est le principal organe consultatif de l'UE, son avis a été
approuvé par le vote en séance plénière

#nWW: Le texte de la Commission autorise les brevets sur les programmes
exécutés sur un ordinateur

#nse: Le texte de la Commission ne fait que codifier les pratiques
juridiquement discutables de l'OEB

#tnW: Le texte de la Commission n'empêche pas les brevets sur les méthodes
d'affaires (ni sur toute autre méthode)

#epW: Le texte de la Commission ne garanti pas l'interopérabilité mais à la
place embrouille la question

#eWa: Doutes sur les intentions de la Commission qui parle de plusieurs
choses hors de propos (comme le piratage) dans son introduction

#tsf: Aucune analyse économique probante montrant les avantages pour les PME

#tow: Il est très plausible de nous faire croire que la directive serait une
sorte d'expérience réversible sur trois ans, à la fin desquels une
évaluation serait faite

#OWW: CULT 2003-01-22: Avis de la commission de la culture, de la jeunesse,
de l'éducation, des médias et des sports du Parlement européen

#nre: %(q:Technique) signifie %(q:application des forces de la nature pour
contrôler des effets physiques au-delà de la représentation numérique
de l'information)

#rot: Le traitement de données n'est pas un domaine technique

#xeW: ITRE 2003-02-21: Avis de la commission de l'industrie, du commerce
extérieur, de la recherche et de l'énergie du Parlement européen

#cvr: La publication ne peut jamais constituer une infraction

#rcn: L'interopérabilité ne peut jamais constituer une infraction à un
brevet

#rts: Bessen & Hunt en 2003 : Une vision empirique des brevets logiciels

#taa: Les brevets logiciels ont conduit aux États-Unis à un transfert des
ressources de R&D vers des activités liées au brevetage

#sea: Plus de brevets a conduit à moins d'innovation même au sein des
entreprises qui brevetaient le plus

#sie: La plupart des brevets logiciels sont détenus par de grosses
entreprises de matériel et sont obtenus pour des raisons stratégiques
plutôt que pour empêcher l'imitation de produits

#nlh: Les brevets gênent l'innovation au lieu de l'encourager dans des
domaines où la majeure partie de l'innovation est incrémentale, tels
que le développement de logiciels

#liW: Auditions de la Federal Trade Commission en 2002-2003

#ccc: La Commission fédérale du commerce (Federal Trade Commission, FTC) des
États-Unis a conduit des auditions parmi les sociétés informatiques
qui ont montré une animosité incessante de l'industrie étasunienne du
logiciel envers les brevets logiciels. Au cours de précédentes
auditions en 1994, de grandes entreprises telles qu'Adobe, Oracle ou
Autodesk avait exprimé une forte opposition à la brevetabilité du
logiciel. Cette fois, Robert Barr, vice-président à la tête de la
propriété industrielle de Cisco Inc., société à la pointe des
technologies internet, que beaucoup voient comme un modèle de gestion
moderne de l'innovation, a déclaré :

#Mti: J'observe que les brevets n'ont pas été une force positive pour
stimuler l'innovation chez Cisco. Ce qui l'a motivée a été la
concurrence ; apporter de nouveaux produits sur le marché au moment
opportun est crucial. Tout ce que nous avons fait pour créer de
nouveaux produits l'aurait été même si nous n'avions pu obtenir de
brevets sur les innovations et inventions contenues dans ces produits.
Je sais cela car personne ne m'a jamais demandé %(q:peut-on breveter
ceci ?) avant de décider s'il fallait investir du temps et des
ressources dans le développement du produit.

#Tne: Le temps et l'argent que nous dépensons dans les dépôts des brevets,
les poursuites judiciaires, la maintenance, les contentieux et
opérations de licence pourraient être mieux dépensés dans le
développement de produits et la recherche conduisant à davantage
d'innovation. Mais nous déposons chaque année des centaines de brevets
pour des motifs sans rapport avec la promotion ou la protection de
l'innovation.

#Mea: Plus encore, l'accumulation de brevets ne résoud pas vraiment le
problème de l'infraction involontaire de brevets par le développement
indépendant. Si nous sommes accusés d'infraction par un détenteur de
brevet qui ne fabrique ni ne vend de produits ou qui vend des produits
en beaucoup plus petite quantité que nous, nos brevets ne valent pas
assez pour l'autre partie pour la dissuader d'attenter un procès ou
pour réduire la somme d'argent exigée par l'autre société. Ainsi, au
lieu de récompenser l'innovation, le système de brevets pénalise les
sociétés innovantes qui réussisent à mettre sur le marché de nouveaux
produits et il subventionne ou récompense ceux qui n'y arrivent pas.

#eWa2: Le rapport final de la FTC, publié en octobre 2003, est arrivé à la
conclusion que le système de brevets stimulait la concurrence et la
productivité dans certains domaines (la pharmacie est citée en
exemple), alors qu'il avait tendance à causer du tort dans d'autres
domaines, particulièrement lorsqu'il s'applique aux logiciels et aux
méthodes d'affaires. Le raport émet des doutes quant au bon sens des
décisions de justice passées admettant la brevetabilité dans ces
secteurs et propose une serie de mesures pour réparer certains des
dommages occasionnés. Les positions exprimées durant les auditions
sont ainsi résumées :

#laa: Les représentants de l'industrie du matériel informatique et du
logiciel ont en général mis l'accent sur la concurrence dans le
développement de technologies plus en avance comme moteur de
l'innovation dans ces industries évoluant rapidement. Ces
représentants, particulièrement ceux de l'industrie logicielle, ont
décrit un processus d'innovation  qui est en général significativement
moins coûteux que dans les industries pharmaceutique et
biotechnologique, et ils ont parlé d'un cycle de vie du produit qui
est en général bien plus court. Certains représentant du logiciel ont
observé que les droits d'auteurs ou les politiques de code source
ouvert facilitait la nature incrémentale et dynamique de l'innovation
dans le logiciel. Ils ont discrédité la valeur des divulgations de
brevets, parce qu'elles n'exigeaient pas la divulgation du code source
sous-jacente au produit logiciel.

#unB: Un rapport du plus grand investisseur financier dans les PME
allemandes recommande comme l'une des quatre %(q:mesures urgentes pour
renforcer l'innovation et la croissance économique en Allemagne) :

#tti: 3. Mettre en place un régime de protection de la Propriété
intellectuelle (PI) équilibré pour favoriser la création et la
circulation des idées. Il n'est pas toujours plus avantageux d'avoir
une plus forte protection de la PI. Il y a des chance pour que les
brevets sur les logiciels, qui sont une pratique courante aux USA et
en passe d'être légalisés en Europe, étouffent en fait l'innovation.
L'Europe pourrait encore changer de ligne de conduite.

#FAQ: Foire aux questions (FAQ)

#top: Qu'est-ce qu'un brevet logiciel ?

#aWc: Un brevet logiciel est un brevet dont les revendications s'appliquent
à tous les programmes d'ordinateur correspondant à certaines
fonctionnalités.

#tfa: Depuis 1998, L'Office européen des brevets (OEB) a accordé des
%(e:revendications de programmes), i.e. des revendications sous la
forme :

#out: un programme d'ordinateur [stocké sur un support de données],
caractérisé par le fait que lorsqu'on charge le programme en mémoire
... [un procédé avec certaines fonctionnalités est réalisé].

#ota: Depuis 1986, l'OEB a accordé des revendications de procédé sur des
objets où la seule réalisation %(q:inventive) réside dans du
traitement de données et pour laquelle des revendications de
programmes auraient donc été plus simples et directes.

#tnf: Que dit la loi à propos des brevets logiciels ?

#Wen2: L'article 52 de la Convention sur le brevet européen (Convention de
Munich de 1973) précise que %(q:les programmes d'ordinateur), ainsi
que %(q:les méthodes mathématiques) et %(q:les présentations
d'informations), ne sont pas des inventions au sens du droit des
brevets.  Les directives d'examen de l'Office européen des brevets
(OEB) de 1978 expliquent :

#oto: Un programme d'ordinateur peut prendre différentes formes, par exemple
un algorithme, un organigramme ou une série d'instructions codées, qui
peut être enregistré sur bande ou sur tout un autre appareil
d'enregistrement et qui peut être considéré comme un cas particulier
soit d'une méthode mathématique, soit d'une présentation
d'informations. Si la contribution à l'état de l'art connu réside
seulement dans un programme d'ordinateur alors l'objet n'est pas
brevetable quelque soit la manière dont les revendications sont
présentées. Par exemple, une revendication sur un ordinateur
caractérisé par le fait d'avoir le programme particulier stocké dans
sa mémoire ou d'un procédé pour agir sur un ordinateur sous le
contrôle du programme serait aussi discutable qu'une revendication
d'un programme en tant que tel ou du programme lorsqu'il serait
enregistré sur une bande magnétique.

#vtm: En d'autres termes : à chaque fois qu'une réalisation prétendument
nouvelle pour laquelle on recherche une protection par un brevet tombe
dans la portée d'une réclamation de programme, elle n'est pas
brevetable..

#rkv: Que sont des %(q:inventions mises en oeuvre par ordinateur) ?

#aWp: Ce terme a été %(ep:introduit) en mai 2000 par l'OEB comme un
euphémisme pour un %(q:programme d'ordinateur dans le contexte de
revendications de brevets), i.e. des non-inventions selon le droit
actuel. Ce terme a été inclu dans le %(q:Projet trilatéral), une
tentative des offices de brevets de créer des règles uniformes pour la
brevetabilité des %(q:méthode d'affaires mises en oeuvres par
ordinateur) aux États-Unis, au Japon et en Europe.

#Eid: La proposition de la Commission européenne utilise la définition de
l'OEB, alors que le Parlement européen a redéfini le terme dans un
sens opposé : des inventions techniques (solutions d'ingéniérie
impliquant les forces de la nature) mises en oevre par un ordinateurs.
Le Groupe de travail du Conseil a encore une autre définition qui
inclut à la fois les inventions techniques et les innovations
logicielles.

#hrt: Pourquoi le Parlement européen a-t-il décidé de réaffirmer la non
brevetabilité du logiciel ?

#ihm: Le droit des brevets est un droit économique et pratiquement toutes
les études économiques ont surtout montré les effets négatifs des
brevets logiciels. La Commission n'a pas effectué d'étude approfondie
sur une estimation des conséquences. La majorité des entreprises
européenne est contre les brevets logiciels. Les organes consultatifs
(COR99, ESC02) et deux des commissions parlementaires concernées ont
mis en garde contre une légalisation des pratiques de l'OEB.

#ede: Les investissements dans le logiciel ne doivent-ils pas être protégés
?

#erW: Les principaux investissements dans le développement de logiciels sont
protégés par le droit d'auteur et plusieurs autres moyens de
protection (qui pour fonctionner nécessitent cette protection par le
droit d'auteur) sont également employés. Les brevets logiciels sapent
les protections offertes par le droit d'auteur.

#bnW: Que se passe-t-il pour les téléphones mobiles et les machines à laver
?

#ear: La version du Parlement européen autorise les brevets sur des moyens
nouveaux d'exploiter les forces de la nature, indépendamment du fait
qu'un ordinateur soit exploité ou non. Certains brevets dans le
secteur des télécoms (et de l'électronique) sont pourtant si abstraits
et vastes que, à l'époque de la %(q:convergence des médias), ils
couvriraient aussi l'informatique pure, p.ex. la programmation sur
Internet.  Comme montrent les études mentionnées ci-dessus, il y a peu
de raisons de supposer que les purs brevets logiciels aient un
quelconque effet positif sur l'innovation.

#rdt: Pourquoi le logiciel -- en particulier le logiciel embarqué -- ne
devrait pas être brevetable

#eWW: La version du Parlement européen ne contredit-elle pas les traités
internationnaux tels que les ADPIC ?

#Wnl: Non. Au contraire, on peut soutenir que ce sont les versions de la
Commission et du Conseil qui violent l'Accord sur les ADPIC.

#Wxh: Les textes de la Commission et du Conseil ne confirment-ils pas
simplement le status quo ?

#mWe: Non. Les textes de la Commission et du Conseil imposent une pratique
de l'Office européen des brevets qui n'est pas acceptée par tous les
tribunaux et rendrait ainsi 30 000 logiciels et méthodes d'affaires
bien plus difficiles à contester qu'ils ne le sont à l'heure actuelle.

#dft: Pourquoi le %(q:traitement de données) est-il exclu de la
brevetabilité et qu'est-ce que cela signifie ?

#ltT: Tout ce que peut faire un ordinateur, c'est du traitement de données,
i.e. du calcul sur des entités symboliques. Quand il est utilisé pour
contrôler une invention, l'ordinateur fait encore simplement du
traitement de données mais les équipements périphériques peuvent faire
quelque chose de brevetable. L'article 3 bis du Parlement s'assure de
la compatibilité avec les ADPIC et clarifie que seul le procédé
périphérique est brevetable et non le logiciel en tant que tel.

#eyr: Pourquoi le Parlement européen a-t-il défini la %(q:technique) en se
référant aux %(q:forces de la nature) ?

#Wtn: Les partisans de la directive ont insisté pour que le concept de
%(q:[contributions / considérations / effets] techniques) soit le seul
critère acceptable pour limiter les objets brevetables et sur le fait
que cette directive devrait clarifier ce qui est brevetable et ce qui
ne l'est pas. Il s'en suit qu'une définition est nécessaire.

#oas: La référence aux %(q:forces de la nature) est omniprésente dans le
droit des brevets traditionnel de l'Europe de l'est et de l'Asie
orientale. Elle apparaît dans la jurisprudence, particulièrement en
Allemagne mais également aux États-Unis, en France et dans d'autres
tribunaux.

#nra: Insister sur les %(q:forces de la nature) est-il toujours approprié
aux inventions actuelles ?

#onc: En effet, il existe aujourd'hui une tendance suivie vers la
%(q:convergence). Tout le monde essaye de se soustraire aux fantaisies
sur la matière et de transférer autant de problèmes que possible au
niveau du traitement de données. C'est parce que l'informatique est si
commode, si %(q:calculable), si facile.

#obt: Rendre les choses faciles est peut-être moderne mais est-il également
moderne de breveter les choses faciles ?

#Wmn: Pourquoi le Parlement européen ne s'est pas satisfait des garde-fous
de la Commission sur l'interopérabilité ?

#rlu: La Commission ne garantit que le droit à la rétro-ingénierie, qui en
premier lieu ne peut pas être interdite par un brevet. Son article 6
ne permet pas cependant l'utilisation de l'information découverte. Le
droit sur la concurrence (anti-trust) est un outil trop peu tranchant
pour résoudre ce problème.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: obenassy ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

