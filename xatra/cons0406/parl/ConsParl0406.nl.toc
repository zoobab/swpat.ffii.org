\contentsline {section}{\numberline {1}Studies en Opinies i.v.m. het Richtlijnproject over Softwarepatenten}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}The competitiveness of European enterprises in the face of globalisation -- Opinion of the Committee of Regions\cite {cor99nl}}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}NL 2000: Ruimere octrooiering van computerprogramma's : technicality of revolutie?\cite {verk00}}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}US National Research Council 2000: The Digital Dilemma\cite {nrc00}}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}CEC 2000: ``Patent protection of computer programmes''\cite {tap00}}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}CEC 2000: ``The Economic Impact of Software Patents''\cite {ipi00}}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}CEC 2000: Consultation on the Planned Directive\cite {ec00}}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}DE 2001: Micro- and Macroeconomic Implications of the Patentability of Software Innovations\cite {isi01}}{5}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}NL 2001: ``Intellectueel Eigendom en Innovatie -- Over de rol van intellectueel eigendom in de Nederlandse kenniseconomie''\cite {ezi01}}{6}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}NL 2002: ``Toets op het concurrentievermogen''\cite {ezt02}}{7}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Bakels \& Hugenholtz 2002: Discussion of European-level legislation in the field of patents for software\cite {bah02}}{7}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}Monopolkommission report 2002-07-08\cite {mok02}}{7}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}Commissariat g\'{e}n\'{e}ral au plan 2002-10-17: Rapport sur l'\'{E}conomie du Logiciel\cite {cdp02}}{7}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Koski 2002: Technology policy in the telecommunication sector: Market responses and economic impacts\cite {kio02}}{8}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}2002-09 Opinion Economic and Social Committee on the Commission Proposal\cite {esc02nl}}{8}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}2002-12: Opinion Committee for Cultural Affairs and Youth of EP\cite {cult02}}{8}{subsection.1.15}
\contentsline {subsection}{\numberline {1.16}2002-12: Opinion Committee for Industry and Trade of EP\cite {itre02}}{8}{subsection.1.16}
\contentsline {subsection}{\numberline {1.17}Bessen \& Hunt 2003: An Empirical Look at Software Patents\cite {bh03}}{9}{subsection.1.17}
\contentsline {subsection}{\numberline {1.18}Federal Trade Commission Hearings of 2002-2003\cite {ftc03}}{9}{subsection.1.18}
\contentsline {subsection}{\numberline {1.19}Deutsche Bank Research 2004: Innovation in Germany\cite {db04}}{10}{subsection.1.19}
\contentsline {section}{\numberline {2}FAQ}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Wat is een softwarepatent?}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wat zegt de wet over softwarepatenten?}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Wat zijn ``in computers ge\"{\i }mplementeerde uitvindingen''?}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Waarom heeft het EP besloten om de niet-octrooieerbaarheid van software te herbevestigen?}{12}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Moeten investeringen in software dan niet beschermd worden?}{12}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Wat met mobiele telefoons en wasmachines?}{12}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Spreekt de EP-versie internationale verdragen zoals TRIPs niet tegen?}{12}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Bevestigen de Commissie/Raadteksten niet gewoon de status quo?}{12}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Waarom wordt ``dataverwerking'' uitgesloten van octrooieerbaarheid en wat betekent dit?}{13}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Waarom definieert de EP ``technologie'' d.m.v. verwijzingen naar ``natuurkrachten''?}{13}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Is aandringen op ``natuurkrachten'' nog relevant voor de uitvindingen van vandaag de dag?}{13}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Waarom was het EP niet tevreden met de garanties voor interoperabiliteit van het EP?}{13}{subsection.2.12}
