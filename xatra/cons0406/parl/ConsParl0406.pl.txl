<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Analysis and Opinion Behind the Parliament's Decision

#descr: The European Parliament has already, through a series of amendments,
rejected texts from the Commission and the JURI Committee, which were
largely identical in wording and spirit to that approved now by the
Council.  The amendments reflected the demands of the vast majority of
software innovators and innovation policy researchers in the EU,
including the authors of studies ordered by the Commisson as well as
the members of the EU's consultative organs

#sto

#Con: EU Council of Regions 1999

#ett: The competitiveness of European enterprises in the face of
globalisation - How it can be encouraged

#ftW

#iui: NL 2000: Ruimere octrooiering van computerprogramma's : technicality
of revolutie?

#lhy: Study by Dirk W.F. Verkade, professor of IP law, commissioned by Dutch
Ministery of Economic Affairs, published as a book with ISBN
90-5409-267-X. The book's main thrust is that extension of
patentability to software is very dangerous and the power copyright in
the software business should not be underestimated.

#wWW

#Ser

#fmi

#adn

#WeW

#she: Study on the desirability of software patents ordered by the
Directorate General Enterprises of the European Commission, performed
by UK researchers among SMEs, large enterprises and research
institutions.

#aWf: None of the examined groups makes a lot of use of patents to protect
their (software) investments.

#Wte: SMEs think they will not have a chance when protecting patents in
front of a court due to a lack of money.

#prm: Given the short life span of computer programs, SMEs think they can
better spend their time on the development of new programs, than on
obtaining patents.

#omc: Large companies patent more than small companies.

#tWh: SMEs consider the creation and implementation of %(e:undesirable laws)
as one of their primary concerns.

#afn: There is a general consensus that the patentability of software will
probably pose a growing a concern for SMEs.

#tno

#fon

#nrW

#dsW

#tai

#atd

#oom

#hyb

#gWs

#nvu

#SME

#rnr

#sio

#Use

#tdn

#cec

#fWo

#Wen

#nnp

#ieW

#Wts

#EWa

#oof

#sPr

#Wns

#Wto

#ewo

#ctn

#osm

#eln

#Wdi

#tjo

#eie

#fra: Report of the Dutch Ministry of Economic Affairs of 2001

#oth: A partial unplanned effect of more conscious handling of the IP and
the patenting strategy of companies is the arising of the problem of
the `anti-commons'. Parties keep each other prisoner in a patent
minefield. [$ldots{}$] Mainly the (high-tech) SMEs suffer from this
strategic patenting.

#loe: Besides, patents are only part of the total knowledge strategy of
companies. For most companies patenting is less important than secrecy
and technological lead time.

#rps: Innovations of SMEs are relatively more encumbered by existing patent
portfolios. They also experience more obstructions to patent things
themselves.

#eWf: Given the differences between sectors and the differences in company
sizes, a differentiated patent system is an attractive option from an
innovation point of view.

#bic: Study by the Dutch Ministry of Economic Affairs.

#inW: The importance of the IP-regime as far as innovation is concerned
differs per sector. In the biotech and pharmaceuticals patents have an
essential role given the long time to earn back investments. In the
software sector developments are so quick that patents are used less
to earn back investments.

#Woo: Further, one should look at the innovation obstructions stemming from
the trend of patenting enabling technologies (e.g.software) and
broadly applicable business methods.

#hkW

#raa

#lWa

#Wnv

#nmn

#rme

#on0: Monopolkommission report 2002-07-08

#wsb

#a0m

#WpW

#cia

#rnW

#Wrl

#skn

#nom

#WEd

#nWW

#nse

#tnW

#epW

#eWa

#tsf

#tow

#OWW

#nre

#rot

#xeW

#cvr

#rcn

#rts

#taa

#sea

#sie

#nlh

#liW

#ccc

#Mti

#Tne

#Mea

#eWa2

#laa

#unB

#tti

#FAQ

#top

#aWc

#tfa

#out

#ota

#tnf

#Wen2

#oto

#vtm

#rkv

#aWp

#Eid

#hrt

#ihm

#ede

#erW

#bnW

#ear

#rdt

#eWW

#Wnl

#Wxh

#mWe

#dft

#ltT

#eyr

#Wtn

#oas

#nra

#onc

#obt

#Wmn

#rlu

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ConsParl0406 ;
# txtlang: xx ;
# End: ;

