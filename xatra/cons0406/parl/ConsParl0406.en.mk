# -*- mode: makefile -*-

ConsParl0406.en.lstex:
	lstex ConsParl0406.en | sort -u > ConsParl0406.en.lstex
ConsParl0406.en.mk:	ConsParl0406.en.lstex
	vcat /ul/prg/RC/texmake > ConsParl0406.en.mk


ConsParl0406.en.dvi:	ConsParl0406.en.mk
	rm -f ConsParl0406.en.lta
	if latex ConsParl0406.en;then test -f ConsParl0406.en.lta && latex ConsParl0406.en;while tail -n 20 ConsParl0406.en.log | grep -w references && latex ConsParl0406.en;do eval;done;fi
	if test -r ConsParl0406.en.idx;then makeindex ConsParl0406.en && latex ConsParl0406.en;fi

ConsParl0406.en.pdf:	ConsParl0406.en.ps
	if grep -w '\(CJK\|epsfig\)' ConsParl0406.en.tex;then ps2pdf ConsParl0406.en.ps;else rm -f ConsParl0406.en.lta;if pdflatex ConsParl0406.en;then test -f ConsParl0406.en.lta && pdflatex ConsParl0406.en;while tail -n 20 ConsParl0406.en.log | grep -w references && pdflatex ConsParl0406.en;do eval;done;fi;fi
	if test -r ConsParl0406.en.idx;then makeindex ConsParl0406.en && pdflatex ConsParl0406.en;fi
ConsParl0406.en.tty:	ConsParl0406.en.dvi
	dvi2tty -q ConsParl0406.en > ConsParl0406.en.tty
ConsParl0406.en.ps:	ConsParl0406.en.dvi	
	dvips  ConsParl0406.en
ConsParl0406.en.001:	ConsParl0406.en.dvi
	rm -f ConsParl0406.en.[0-9][0-9][0-9]
	dvips -i -S 1  ConsParl0406.en
ConsParl0406.en.pbm:	ConsParl0406.en.ps
	pstopbm ConsParl0406.en.ps
ConsParl0406.en.gif:	ConsParl0406.en.ps
	pstogif ConsParl0406.en.ps
ConsParl0406.en.eps:	ConsParl0406.en.dvi
	dvips -E -f ConsParl0406.en > ConsParl0406.en.eps
ConsParl0406.en-01.g3n:	ConsParl0406.en.ps
	ps2fax n ConsParl0406.en.ps
ConsParl0406.en-01.g3f:	ConsParl0406.en.ps
	ps2fax f ConsParl0406.en.ps
ConsParl0406.en.ps.gz:	ConsParl0406.en.ps
	gzip < ConsParl0406.en.ps > ConsParl0406.en.ps.gz
ConsParl0406.en.ps.gz.uue:	ConsParl0406.en.ps.gz
	uuencode ConsParl0406.en.ps.gz ConsParl0406.en.ps.gz > ConsParl0406.en.ps.gz.uue
ConsParl0406.en.faxsnd:	ConsParl0406.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo ConsParl0406.en-??.g3n`;source faxsnd main
ConsParl0406.en_tex.ps:	
	cat ConsParl0406.en.tex | splitlong | coco | m2ps > ConsParl0406.en_tex.ps
ConsParl0406.en_tex.ps.gz:	ConsParl0406.en_tex.ps
	gzip < ConsParl0406.en_tex.ps > ConsParl0406.en_tex.ps.gz
ConsParl0406.en-01.pgm:
	cat ConsParl0406.en.ps | gs -q -sDEVICE=pgm -sOutputFile=ConsParl0406.en-%02d.pgm -
ConsParl0406.en/ConsParl0406.en.html:	ConsParl0406.en.dvi
	rm -fR ConsParl0406.en;latex2html ConsParl0406.en.tex
xview:	ConsParl0406.en.dvi
	xdvi -s 8 ConsParl0406.en &
tview:	ConsParl0406.en.tty
	browse ConsParl0406.en.tty 
gview:	ConsParl0406.en.ps
	ghostview  ConsParl0406.en.ps &
print:	ConsParl0406.en.ps
	lpr -s -h ConsParl0406.en.ps 
sprint:	ConsParl0406.en.001
	for F in ConsParl0406.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	ConsParl0406.en.faxsnd
	faxsndjob view ConsParl0406.en &
fsend:	ConsParl0406.en.faxsnd
	faxsndjob jobs ConsParl0406.en
viewgif:	ConsParl0406.en.gif
	xv ConsParl0406.en.gif &
viewpbm:	ConsParl0406.en.pbm
	xv ConsParl0406.en-??.pbm &
vieweps:	ConsParl0406.en.eps
	ghostview ConsParl0406.en.eps &	
clean:	ConsParl0406.en.ps
	rm -f  ConsParl0406.en-*.tex ConsParl0406.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} ConsParl0406.en-??.* ConsParl0406.en_tex.* ConsParl0406.en*~
ConsParl0406.en.tgz:	clean
	set +f;LSFILES=`cat ConsParl0406.en.ls???`;FILES=`ls ConsParl0406.en.* $$LSFILES | sort -u`;tar czvf ConsParl0406.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
