<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: FITUG, a German association concerned with the social aspects of information technology, pushes for more legal security in the area of software patents.
title: Fitug 2000: Problems with Software Patents
FKW2: FITUG, a German association concerned with the social aspects of information technology, pushes for more legal security in the area of software patents.
DWe: The current legal situation of software patenting threatens to stifle the burgeoning development of free software, without any apparent necessity.
Ige: This leads to a waste of innovation and employment potentials; the programmers of free software are subjected to uncalculable, unjustifiable, and inevitable risks of litigation.
Aee: At the European level, planning is under way for an extension of the scope of patentability of software, although it is clear that even in its current form patent law is more harmful than useful for the software industry.
Dfe: Since programmers can never, not even by extensive patent research, protect themselves agains the danger of involuntarily infringing on someone's patent, publishing one's software source code is tantamount to playing russian roulette.
DWE: This situation not only places an undue burden on individual programmers, but is also macro-economically unreasonable:  the biggest problem of the European software industry is a lack of skilled personnel.
Fih: Free Software can help to reduce this lack of skill by lowering the entry barrier for newcomers and allowing them to professionalise themselves step by step.
Dse: Yet, as experience shows, free software is by no means inferior but rather a source of important innovative impulses.
DHd: The legislators should therefore refrain from erecting any unnecessary entry barriers to the production of software.
Dno: The financial effort required to minimize the risk of patent lawsuits and the unavoidable remaining risks are high barriers with dissuasive effects.
DdW: These barriers are completely unnecessary, they do not even benefit the patent owners themselves.
DmW: The danger of involuntary patent infringment is of grave concern not only to small companies and developpers of free software, but also to large companies that can afford to spend a lot of money on their patent departments
Doa: The world's second largest software publisher, Oracle, meanwhile acknowledge publicly, that they are spending money on software patents for purely defensive purposes, e.g. to deter other software companies from suing Oracle for patent infringment.
Dir: That tells everything.
Dee: The material and legal situation of software patents, especially the scope of protection, is so intransparent, that the law in this area currently generates random results.
DnK: That is not only due to the complexity of patent law itself, but even more to the complexity of the underlying subject matter.
Dtw: Patent law cannot survive in the long run, if its design continues to exhibit such flaws.
Ipn: In the interest of the European software industry and for the sake of the acceptability of the legal system by the concerned parties, our legislators should take action to end this unacceptable situation, rather than to worsen it.
DWg: This problem must be solved immediately.
FuK: La FITUG essaiera de développer des propositions de solutions pratiques, et elle lance un appel à tous les concernés de faire de même.
Urc: signataries
Ffi: German Association for Information Technology and Society
LeW: Linux User Group Austria
Vre: %(q:Virtual Section) of the German Social Democratic Party
Vri: chairman
Fnn: German-speaking Association for the Promotion of a Free Informational Infrastructure
Vte: Association of Austrian Internet Users
Die: German Speaking TeX User Group

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: fitugpe ;
# txtlang: en ;
# End: ;

