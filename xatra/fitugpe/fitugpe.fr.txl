<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Problèmes de Brevets Logiciels

#descr: L`association FITUG lutte pour plus de sécurité juridique dans le domaine des brevets de logiciel.

#FKW2: L`association FITUG lutte pour plus de sécurité juridique dans le domaine des brevets de logiciel.

#DWe: Le droit actuel en matière de brevets de logiciel met en danger sans nécessité apparente, le développement futur des logiciels libres qui s`avèrent pleins de succès.

#Ige: Des potentiels d`innovations et d`emplois restent inutilisés.  Les programmeurs de logiciels libres s`exposeront inévitablement à des risques de procès incalculables, imprévisibles et injustifiés.

#Aee: En ce moment, on songe à élargir les droits de brevet sur les logiciels au niveau européen, et cela, bien que les droits de brevet actuels nuisent plus aux logiciels qu`ils ne leur servent.

#Dfe: Même en travaillant soigneusement leurs recherches, les programmeurs ne peuvent s`assurer d`empiéter involontairement sur des brevets d`autrui, et si en plus, les programmeurs publient le code source, cela revient à jouer à la roulette russe.

#DWE: Cette situation n`est pas seulement un facteur de risques injustes pour le programmeur individuel, mais c`est insensé pour l`économie générale: Le manque de main d`oeuvre est le plus grand problème du logiciel européen.

#Fih: Les logiciels libres pourraient remédier à reduire ce manque de main d`oeuvre, en permettant un accès plus facile aux nouveaux arrivants, et en leur ouvrant les possibilités d`une professionalisation évolutive.

#Dse: La pratique a prouvé que les logiciels libres ne sont nullement des produits de seconde classe, mais plutôt capable d`inciter d`importantes innovations.

#DHd: Le législateur devrait donc éviter d`ériger des obstacles inutiles qui freinent la productivité des nouveaux arrivants, et ainsi l`enrichissement du marché des logiciels.

#Dno: Les frais financiers nécessaires à minimiser les risques de recours en justice sur les brevets, ainsi que les risques subsistants malgré un travail de recherche minutieux, tout cela présente d`importants obstacles et un effet dissuasif.

#DdW: Ces obstacles sont totalement inutiles puisque la situation juridique ne va pas non plus dans l`intérêt même du propriétaire de brévet

#DmW: Le danger d`empiéter involontairement sur des brevets n`existe pas seulement pour les petites entreprises ou les créateurs de logiciels libres, mais ce danger présente aussi un grand problème pour les grandes entreprises qui investissent beaucoup d`argent dans leurs services juridiques.

#Doa: Le deuxième producteur mondial de logiciels, Oracle, reconnaît officiellement qu`il dépose des brevets seulement dans l`intention de monter une force de dissuasion pour empêcher d`autres entreprises d`attaquer Oracle pour utilisation illégitime de brevets.

#Dir: Cela veut tout dire.

#Dee: La situation de fait et la situation juridique des brevets de logiciels est si obscure, en particulier sur le périmètre de la protection accordée, que le droit produit actuellement dans ce domaine, des résultats relevant du hasard.

#DnK: Cela n`est pas uniquement dû à la complexité du droit de brevet même, mais surtout aussi dû à la complexité de la réalité à la base.

#Dtw: Le droit de brevet ne pourra survivre à long terme s`il présente de tels déficites.

#Ipn: Le législateur devrait en finir au plus vite avec cette situation inacceptable au lieu de l`empirer plus, et cela dans l`intérêt de la branche du logiciel européen, mais aussi pour que les concernés acceptent ses lois.

#DWg: Il est nécessaire de régler ce problème au plus vite.

#FuK: FITUG will endeavor to develop useful proposals for solving the problem and calls upon all concerned parties to do the same.

#Urc: signataires

#Ffi: Association Allemande pour la Technologie d'Information et la Société

#LeW: Groupe Utilisateurs Linux Autriche

#Vre: %(q:Section Virtuelle) du Parti Social-Democrate Allemand

#Vri: président

#Fnn: Assiociation germanophone pour la Promotion d'une Infrastructure Informationelle Libre

#Vte: Assiciation Utilisateurs Internet Autriche

#Die: Association Germanophone Utilisateurs TeX

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: seyfertd ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: fitugpe ;
# txtlang: fr ;
# multlin: t ;
# End: ;

