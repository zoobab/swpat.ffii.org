<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Probleme mit Swpat

#descr: FITUG e. V. setzt sich für mehr Rechtssicherheit bei Softwarepatenten ein.

#FKW2: FITUG e. V. setzt sich für mehr Rechtssicherheit bei Softwarepatenten ein.

#DWe: Durch die gegenwärtige Rechtslage bei Softwarepatenten werden Bestand und weitere Entwicklung des Erfolgsmodells freie Software gefährdet, ohne daß dies nötig wäre.

#Ige: Innovations- und Beschäftigungspotentiale bleiben ungenutzt; die Programmierer freier Software werden unkalkulierbaren, ungerechtfertigten, unübersehbar großen und unvermeidbaren Prozeßrisiken ausgesetzt.

#Aee: Auf europäischer Ebene wird derzeit eine Ausweitung der Patentierbarkeit von Software erwogen.  Dabei schadet das Patentrecht bereits in seiner gegenwärtigen Form der Softwarebranche mehr, als es ihr nützt.

#Dfe: Da sich Programmierer selbst durch sorgfältigste Recherche nicht vor der Gefahr schützen können, bei ihrer Arbeit ungewollt fremde Patente zu verletzten, wird insbesondere die Offenlegung der Quelltexte zum russischen Roulette.

#DWE: Das ist nicht nur für den einzelnen Programmierer eine ungerechte Belastung mit fremden Risiken, sondern auch gesamtwirtschaftlich völlig unvernünftig: Das größte Problem der europäischen Softwarebranche ist der Mangel an Arbeitskräften.

#Fih: Freie Software kann dabei helfen, diesen Arbeitskräftemangel zu verringern, indem sie die Marktzutrittsschwelle für Newcomer senkt und ihnen erlaubt, sich in evolutionären Schritten zu professionalisieren.

#Dse: Daß freie Software keineswegs zweitklassig ist, sondern wichtige Innovationsimpulse setzen kann, ist längst durch die Praxis erwiesen.

#DHd: Der Gesetzgeber sollte deshalb keine unnötigen Hürden errichten, die es Newcomern erschweren, den Softwaremarkt um neue Produkte zu bereichern.

#Dno: Der finanzielle Aufwand, der nötig ist, um das Risiko von Patentrechtsklagen zu minimieren und das bei aller Sorgfalt stets dennoch verbleibende Risiko derartiger Klagen sind ganz erhebliche Hürden mit abschreckender Wirkung.

#DdW: Diese Hürden sind auch völlig unnötig, denn die gegenwärtige Rechtslage liegt auch nicht im Interesse der Patentrechtsinhalber selbst

#DmW: Die Gefahr ungewollter Patentrechtsverletzungen besteht nicht nur für kleine Firmen und die Entwickler freier Software, sondern ist selbst für große Firmen, die viel Geld in ihre Patentrechtsabteilungen investieren können, ein großes Problem

#Doa: Der zweitgrößte Softwarehersteller der Welt, Oracle, gibt mittlerweile öffentlich zu, daß er Softwarepatente nur zu dem Zweck anmeldet, um hierdurch ein eigenes Drohpotential aufzubauen, das andere Softwarehersteller davon abschreckt, Oracle wegen Patentrechtsverletzung zu verklagen.

#Dir: Das spricht für sich.

#Dee: Die Sach- und Rechtslage bei Softwarepatenten, insbesondere die Reichweite des gewährten Schutzes, ist so intransparent, daß das Recht in diesem Bereich derzeit reine Zufallsergebnisse produziert.

#DnK: Das liegt nicht nur an der Komplexität des Patentrechts selbst, sondern vor allem auch an der Komplexität der zugrunde liegenden Wirklichkeit.

#Dtw: Das Patentrecht selbst ist langfristig gesehen nicht überlebensfähig, wenn es derartige Defizite aufweist.

#Ipn: Im Interesse der europäischen Softwarebranche, aber auch im Interesse einer Akzeptanz des Rechts durch die Betroffenen sollte der Gesetzgeber diesen unzumutbaren Zustand so schnell wie möglich beenden, anstatt ihn noch mehr zu verschlimmern.

#DWg: Dieses Problem muß umgehend gelöst werden.

#FuK: Fitug e. V. wird versuchen, praktikable Lösungsvorschläge zu entwickeln und ruft alle übrigen Beteiligten auf, das Gleiche zu tun.

#Urc: Unterzeichner

#Ffi: Förderverein für Informationstechnologie und Gesellschaft

#LeW: Linux User Group Austria

#Vre: Virtueller Ortsverein der SPD

#Vri: Vorsitz

#Fnn: Förderverein für eine Freie Informationelle Infrastruktur

#Vte: Verein für Internet-BEnutzer Österreichs

#Die: Deutschsprachige Anwendervereinigung TeX

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: fitugpe ;
# txtlang: de ;
# multlin: t ;
# End: ;

