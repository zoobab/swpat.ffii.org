<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Musterbrief zum Schutz der Informatischen Innovation

#descr: Diesen oder einen ähnlichen Brief könnten Sie unterschreiben und mit
geeigneten Anhängen an diverse Entscheidungsträger versenden.

#DWe: Der %(eb:Eurolinux-Brieferzeugungsautomat) könnte Ihnen die Arbeit
dabei noch leichter machen.  Es empfiehlt sich, ferner einige
%(pd:druckbare Dokumente) in den Umschlag zu stecken, wie z.B. unsere
aktuellen %(OL).

#Sgt: Sehr geehrte...

#Ien: In den letzten Jahren hat das Europäische Patentamt (EPA) zehntausende
von unverdienten und gesetzeswidrigen Patenten auf Programmierideen,
Geschäftsmodelle und geistige Methoden erteilt.  Dieses Patentgerümpel
vermint unser Arbeitsumfeld und vergiftet die Infrastruktur der
Informationsgesellschaft.  Nun drängt das EPA Europas Regierungen,
neue Gesetze zu erlassen und alte zu ändern, um seine unwürdige Praxis
im nachhinein zu legalisieren.

#Web: Wir bitten Sie, Ihren Einfluss bei der anstehenden
Entscheidungsfindung auf europäischer Ebene geltend zu machen, um zu
verlangen, dass

#AWs: Artikel 52 des Europäischen Patentübereinkommens bis auf weiteres
nicht angerührt wird.

#thW: die Begriffe %(q:Technik), %(q:gewerbliche Anwendbarkeit) und
%(q:Computerprogramm als solches) dahingehend %(kl:geklärt) werden,
dass logische Verfahren und Erzeugnisse (Algorithmen und Programme auf
Diskette) nicht patentierbar sind, während physische Erzeugnisse mit
dazugehörigen Verfahren patentierbar sein können, soweit sie einen
erfinderischen Beitrag zum Stand der Technik leisten.

#dcm: der öffentliche Dialog über die Auswirkungen des Patentsystems auf die
Informationswirtschaft und -gesellschaft verbreitert und vertieft wird

#dbU: der Stand der Technik systematisch dokumentiert und in Form
quelloffener Datenbanksysteme gemeinschaftlich weiterentwickelt wird,
damit Kleine und Mittlere Unternehmen für die Patentschlacht daheim
und in Übersee besser gerüstet werden.

#enj: ein %(q:Recht, selbst erarbeitete Informationswerke zu verbreiten) und
ein %(os:Recht auf Interoperabilität) auf Verfassungsebene kodifiziert
wird, um zukünftigen Begehrlichkeiten des Patentwesens klare Grenzen
zu setzen.

#Sft: Wir wären Ihnen sehr verbunden, wenn Sie sich bereit fänden, das der
Patentlobby zu widerstehen und das Gemeinwohl zu verteidigen.

#Hcg: Hochachtungsvoll

#Urr: Unterschrift

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: nopatltr ;
# txtlang: de ;
# multlin: t ;
# End: ;

