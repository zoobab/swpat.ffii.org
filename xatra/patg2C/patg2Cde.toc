\contentsline {chapter}{\numberline {A}Verteiler dieses Briefes}{7}{appendix.A}
\contentsline {chapter}{\numberline {B}Stellungnahmen von Politikern zur Softwarepatentierung}{9}{appendix.B}
\contentsline {section}{\numberline {B.1}Ausschuss der Regionen der Europ\"aischen Union}{9}{section.B.1}
\contentsline {section}{\numberline {B.2}Bundestagsfraktion B\"undnis 90/Die Gr\"unen}{9}{section.B.2}
\contentsline {section}{\numberline {B.3}J\"org Tauss, MdB (SPD), Vorsitzender des Parlamentarischen Unterausschusses f\"ur die Neuen Medien}{10}{section.B.3}
\contentsline {section}{\numberline {B.4}Bundestagsfraktion der FDP}{11}{section.B.4}
\contentsline {section}{\numberline {B.5}Bundestagsfraktion der CDU/CSU}{11}{section.B.5}
\contentsline {section}{\numberline {B.6}Dr.\ med.\ Wolfgang Wodarg, MdB (SPD)}{12}{section.B.6}
\contentsline {section}{\numberline {B.7}Herta D\"aubler-Gmelin, Bundesministerin der Justiz}{13}{section.B.7}
\contentsline {section}{\numberline {B.8}Christian Pierret, Industrieminister Frankreichs}{13}{section.B.8}
\contentsline {section}{\numberline {B.9}Jean-Yves Le D\'eaut, Abgeordneter der Moselregion in der Franz\"osischen Nationalversammlung (PS)}{13}{section.B.9}
\contentsline {chapter}{\numberline {C}Presseerkl\"arung des FFII zum EPA-Basisvorschlag}{15}{appendix.C}
\contentsline {chapter}{\numberline {D}Eurolinux-Petition}{18}{appendix.D}
\contentsline {chapter}{\numberline {E}Europ\"aische Softwarepatente: Einige Musterexemplare}{19}{appendix.E}
\contentsline {chapter}{\numberline {F}Das EP\"U und die Grenzen der Patentierbarkeit}{23}{appendix.F}
\contentsline {section}{\numberline {F.1}Wortlaut von Art 52 EP\"U und $\S $1 PatG}{23}{section.F.1}
\contentsline {section}{\numberline {F.2}BGH-Beschluss Dispositionsprogramm (1976): warum es sich verbietet, den Technikbegriff \"uber den Umweg der Computerprogramme aufzuweichen}{23}{section.F.2}
\contentsline {section}{\numberline {F.3}BGH-Beschluss Walzstabteilung (1980): eine auf technischem Gebiet anwendbare Organisations- und Rechenregel ist noch immer keine technische Erfindung}{24}{section.F.3}
\contentsline {section}{\numberline {F.4}Grunds\"atzliches aus dem ``Lehrbuch des Patentrechts'' Kra{\ss }er 1986}{25}{section.F.4}
\contentsline {section}{\numberline {F.5}Vorschlag f\"ur eine Gesetzesregel \"uber die Grenzen der Patentierbarkeit}{28}{section.F.5}
\contentsline {chapter}{\numberline {G}Logische Ideen: ``Niemandsland des geistigen Eigentums'' oder ``Drittes Paradigma''?}{30}{appendix.G}
\contentsline {chapter}{\numberline {H}TRIPS: kein Fallstrick f\"ur Software}{31}{appendix.H}
\contentsline {chapter}{\numberline {I}Wissenschaftliche Fachliteratur}{32}{appendix.I}
\contentsline {section}{\numberline {I.1}Konzeptionelle Unvertr\"aglichkeit zwischen Patentwesen und Informatik}{32}{section.I.1}
\contentsline {subsection}{\numberline {I.1.1}The Impact of Granting Patents for Information Innovations}{32}{subsection.I.1.1}
\contentsline {subsection}{\numberline {I.1.2}Abstraction orientated property of software and its relation to patentability}{33}{subsection.I.1.2}
\contentsline {subsection}{\numberline {I.1.3}The Models are broken, the models are broken!}{33}{subsection.I.1.3}
\contentsline {subsection}{\numberline {I.1.4}Anarchism Triumphant: Free Software and the Death of Copyright}{34}{subsection.I.1.4}
\contentsline {section}{\numberline {I.2}Pl\"adoyers f\"ur eine Trennung in Patentwesen und Sui-Generis-Systeme}{34}{section.I.2}
\contentsline {subsection}{\numberline {I.2.1}Needed: A New System of Intellectual Property Rights}{34}{subsection.I.2.1}
\contentsline {subsection}{\numberline {I.2.2}Patent Protection for Modern Technologies}{34}{subsection.I.2.2}
\contentsline {subsection}{\numberline {I.2.3}Toward a Third Intellectual Property Paradigm}{34}{subsection.I.2.3}
\contentsline {subsection}{\numberline {I.2.4}Acceptable protection of software intellectual property}{35}{subsection.I.2.4}
\contentsline {section}{\numberline {I.3}Leistungsbeurteilungen des Patentsystems}{35}{section.I.3}
\contentsline {subsection}{\numberline {I.3.1}The Patent Examination System is Intellectually Corrupt}{35}{subsection.I.3.1}
\contentsline {subsection}{\numberline {I.3.2}Comparative Study under Trilateral Project 24.2}{35}{subsection.I.3.2}
\contentsline {section}{\numberline {I.4}\"Okonomische Analysen}{36}{section.I.4}
\contentsline {subsection}{\numberline {I.4.1}The benefits and costs of strong patent protection}{36}{subsection.I.4.1}
\contentsline {subsection}{\numberline {I.4.2}The Patent Paradox Revisited}{36}{subsection.I.4.2}
\contentsline {subsection}{\numberline {I.4.3}Sequential Innovation, Patents, and Imitation}{36}{subsection.I.4.3}
\contentsline {subsection}{\numberline {I.4.4}Software Useright: Solving Inconsistencies of Software Patents}{37}{subsection.I.4.4}
\contentsline {subsection}{\numberline {I.4.5}Software Patents Tangle the Web}{37}{subsection.I.4.5}
\contentsline {subsection}{\numberline {I.4.6}Stimuler la concurrence et l'innovation}{37}{subsection.I.4.6}
\contentsline {section}{\numberline {I.5}Juristische Analysen}{38}{section.I.5}
\contentsline {subsection}{\numberline {I.5.1}Technik, Datenverarbeitung und Patentrecht}{38}{subsection.I.5.1}
\contentsline {subsection}{\numberline {I.5.2}BPatG-Urteil ``Suche fehlerhafter Zeichenketten''}{38}{subsection.I.5.2}
\contentsline {subsection}{\numberline {I.5.3}Computersoftware und Patentrecht}{39}{subsection.I.5.3}
\contentsline {subsection}{\numberline {I.5.4}Ruimere octrooi\"ering van computerprogramma's}{40}{subsection.I.5.4}
\contentsline {subsection}{\numberline {I.5.5}La ``r\'eservation'' du logiciel par le droit des brevets}{41}{subsection.I.5.5}
