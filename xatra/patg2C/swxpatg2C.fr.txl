<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vpo: représentant du conseil d'administration %s

#HoW: l'informaticien Heinrich Nixdorf, titulaire de la chaire d'Information et de Communication de l'Université de Rostock

#JpI: les experts en brevets et les juristes de la FFII Autriche

#Jst: Jusitiar

#Vsn: Conseil d'administration %s

#Pio: la S.A. Phaidros

#Ira: la S.A. Intradat

#imt: la S.A. Innominate

#Fte: la S.A. Frontsite

#Gtr: Société pour la Cybernétique

#vov: l'Association Locale Virtuelle du SPD

#LxW: l'Association LinuxTag

#LiVe: Linux-Verband

#Mue: Veuillez agréer, Mesdames, Messieurs, l'expression de notre respectueuse considération

#WnB: Nous vous prions de travailler avec nous dans les prochains jours et les prochains mois à des propositions significatives, et de les apporter au Bundestag ainsi qu'auprès des Commissions concernées.

#OaR: Ob und wie schnell man diese Radikalkur anstrebt, sei dahingestellt.  Wichig ist das Ziel, internationale Behörden wie das Europäische Patentamt zurechenbar zu machen, zu verschlanken wenn nicht abzuschaffen.  Statt fetter Behörden brauchen wir offene juristische Standards, die im Rahmen der national verankerten demokratischen Gewaltenteilung kontrolliert und ohne bürokratischen Aufwand internationalisiert werden können.

#EPd: Rendre possible sur l'Internet, gratuitement, sans aucun examen ni versement de droits annuels, le dépôt et la validation de brevets par simple publication sous une forme convenable. En contrepartie, créer des incitations supplémentaires aux plaintes en nullité. Quiconque fait tomber, ou encore rétrécit, un brevet, reçoit du détenteur du brevet une prime attrayante du fait du remboursement des frais de justice. Au lieu des démarches actuelles, longues et inefficaces, de l'examen administratif, il faut laisser à un groupe professionnel privé de contradicteurs de brevets le soin de démonter au grand jour les brevets invalides. L'examen administratif doit demeurer, mais sur demande du déposant. Il peut être conduit soit par l'Office des Brevets, soit par des sociétés certifiées pour cela, dans le pays ou encore à l'étranger.

#HrW: Heute steht gilt für das Europäischen Patentamt das umgekehrte Verursacherprinzip:  Der Verschmutzer wird bezahlt.  Je mehr Trivialpatente das EPA erteilt, desto reicher wird es.

#DwW: Die Qualität der erteilten Patente sinkt, während ihre Zahl explodiert.  Patentanträge werden oft erst dann geprüft, wenn die Technik schon veraltet ist.  Es wird zwar eifrig an internationaler Harmonisierung gearbeitet, aber dies führt im Ergebnis vor allem zu einer weiteren Erstarrung und Unreformierbarkeit statt zu einer Kostensenkung.  Diese Probleme werden seit Jahrzehnten kritisiert aber nicht gelöst.  Ein Grund liegt möglicherweise in einer falschen Anreizstruktur.

#Zzs: En plus de cela, il faut favoriser le développement d'un système sur-mesure d'aide à l'innovation dans les secteurs de le programmation et autres activités intellectuelles créatrices. Il faut ouvrir des crédits pour la recherche interdisciplinaire et pour des conférences internationales sur ce thème.

#Irf: Le gouvernement est également invité à se prononcer, auprès de la Commission Européenne, en faveur d'une directive clarificatrice, dans ce même esprit.

#IWn: D'après l'article 52 de l'accord sur les brevets - ou article 1 de la loi de brevet Allemande (PatG) - la jurisprudence XXXX????

#Wrr: Si par le biais d'une domination du marché des logiciels, une personne établit des standards de fait sur les moyens de communications publics, il doit être permis de respecter des standards sans qu'aucune condition ne soit posée pour cela. L'interopérabilité a été reconnue comme une valeur importante par la directive européenne de 1991 sur le droit d'auteur. Dans l'intérêt de l'interopérabilité, les procédés brevetés peuvent être aussi utilisés gratuiement et sans qu'une autorisation particulière puisse être exigée.  Les administrations d'Etat et les titulaires de responsabilités publiques ne doivent communiquer avec les citoyens qu'au moyen de standards librement disponibles. Ce droit mérite lui aussi d'être intégré dans la Constitution, car il résulte du droit du citoyen à prendre part à la société de l'information.

#Fbg: Ferner wäre das Recht auf Veröffentlichung von selbst erarbeiteten Informationswerken als eine Konsequenz der Meinungs- und Ausdrucksfreiheit auf Verfassungsebene zu schützen.

#Dml: Die Rechtsverhältnisse beim Einsatz eines programmgesteuerten Verfahrens zur gewerblichen Herstellung materieller Güter bleiben davon unberührt.

#dge: das Ausführen von Programmen auf einer Datenverarbeitungsanlage.

#dKb: das Herstellen, Anbieten, Inverkehrbringen, Besitzen oder Einführen von Urheberrechtsgegenständen aller Art, insbesondere Gebrauchsanweisungen und Programmen für Datenverarbeitungsanlagen.

#DeW: La liberté d'opinion et d'expression s'applique y compris aux oeuvres de programmation. Les brevets n'ont pas à se comporter comme une censure. Ils peuvent s'opposer à l'utilisation des logiciels, mais pas à leur publication ni à leur distribution ou re-distribution. Les programmes informatiques sont mis au même niveau que les descriptions de procédés ou les modes d'emploi. La CEB sur les brevets ne contient aucune prise de position concernant cette thématique.  On pourrait insérer les paragraphe suivantes dans §11 PatG

#SbW: Contre une bureaucratisation excessive, et pour l'internationalisation des questions de brevets

#GWi: Pour un traitement adapté de l'aide à l'innovation informatique.

#Iez: Préciser les critères de brevetabilité

#Rfi: Défense de l'accès libre aux normes de la communication publique

#Rue: Défense du droit de l'auteur à la libre publication de ses oeuvres informatiques.

#Ddg: Doch die Expansionsbestrebungen der Patentbewegung und ihrer Verbündeten in diversen Großunternehmen, Verbänden und Regierungsgremien gehen weiter.  Ihre Unterstützung brauchen wir nun mehr denn je.  Wir bitten Sie, gemeinsam mit uns auf nationaler und europäischer Ebene folgende Gesetzesinitiativen auf den Weg zu bringen:

#CWn: Les programmes d'ordinateurs ne seront pas plus brevetables à l'avenir que par le passé. Nous avons provisoirement réussi à garder un certain contrôle politique, et ce succès est aussi à mettre au compte de votre soutien.

#Stn: Mesdames, Messieurs,

#descr: nous serions heureux que vous nous fassiez des propositions d'améliorations de la rédaction de nos textes et si vous nous aidiez dans la suite de nos initiatives de lois. Dans le cas où vous pourriez agir au nom d'une entreprise du secteur de l'informatique ou encore d'un syndicat de ce secteur, nous aimerions votre soutien en tant que signataire. Voici le texte principal. La lettre complète, avec tous ses addenda, est envoyée aux politiques sous %(pf:forme papier). Voyez aussi notre %(pe:communiqué de presse).

#title: Lettre ouverte : 5 initiatives de lois pour protéger l'innovation en informatique

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: obenassy ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxpatg2C ;
# txtlang: fr ;
# multlin: t ;
# End: ;

