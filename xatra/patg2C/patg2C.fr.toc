\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}D\'{e}fense du droit de l'auteur \`{a} la libre publication de ses oeuvres informatiques.}{2}{section.0.1}
\contentsline {section}{\numberline {2}D\'{e}fense de l'acc\`{e}s libre aux normes de la communication publique}{2}{section.0.2}
\contentsline {section}{\numberline {3}Pr\'{e}ciser les crit\`{e}res de brevetabilit\'{e}}{2}{section.0.3}
\contentsline {section}{\numberline {4}Pour un traitement adapt\'{e} de l'aide \`{a} l'innovation informatique. }{3}{section.0.4}
\contentsline {section}{\numberline {5}Contre une bureaucratisation excessive, et pour l'internationalisation des questions de brevets}{3}{section.0.5}
