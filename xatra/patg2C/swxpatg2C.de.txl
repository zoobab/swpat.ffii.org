<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vpo: Vorstandssprecher %s

#HoW: Informatiker, Heinrich Nixdorf Stiftungsprofessor, Lehrstuhl für Information und Kommunikation, Universität Rostock

#JpI: Patentrechtsexperte und Justitiar des FFII in Österreich

#Jst: Jusitiar

#Vsn: Vorstand %s

#Pio: Phaidros AG

#Ira: Intradat AG

#imt: innominate AG

#Fte: Frontsite AG

#Gtr: Gesellschaft für Kybernetik e.V.

#vov: Virtueller Ortsverein der SPD

#LxW: Linuxtag e.V.

#LiVe: Linux-Verband

#Mue: Mit freundlichen Grüßen

#WnB: Wir bitten Sie, mit uns zusammen in den nächsten Tagen und Wochen entsprechende Vorschläge auszuarbeiten und in den Deutschen Bundestag sowie andere geeignete Entscheidungsgremien einzubringen.

#OaR: Ob und wie schnell man diese Radikalkur anstrebt, sei dahingestellt.  Wichig ist das Ziel, internationale Behörden wie das Europäische Patentamt zurechenbar zu machen, zu verschlanken wenn nicht abzuschaffen.  Statt fetter Behörden brauchen wir offene juristische Standards, die im Rahmen der national verankerten demokratischen Gewaltenteilung kontrolliert und ohne bürokratischen Aufwand internationalisiert werden können.

#EPd: Ein radikaler Reformansatz bestünde darin, Patente kostenlos ohne Prüfung durch formgerechte Veröffentlichung sofort gültig werden zu lassen.  Im Gegenzug dazu würden zusätzliche Hürden für Verletzungsklagen und Anreize für Nichtigkeitsklagen geschaffen.  Wer ein Patent zu Fall bringt oder einengt, erhält vom Patentinhaber eine attraktive Prämie.  Statt der langwierigen und unzuverlässigen amtlichen Prüfung soll eine private Berufsgruppe der Patentinvalidierer die Öffentlichkeit vor unberechtigten Patenten bewahren.

#HrW: Heute steht gilt für das Europäischen Patentamt das umgekehrte Verursacherprinzip:  Der Verschmutzer wird bezahlt.  Je mehr Trivialpatente das EPA erteilt, desto reicher wird es.

#DwW: Die Qualität der erteilten Patente sinkt, während ihre Zahl explodiert.  Patentanträge werden oft erst dann geprüft, wenn die Technik schon veraltet ist.  Es wird zwar eifrig an internationaler Harmonisierung gearbeitet, aber dies führt im Ergebnis vor allem zu einer weiteren Erstarrung und Unreformierbarkeit statt zu einer Kostensenkung.  Diese Probleme werden seit Jahrzehnten kritisiert aber nicht gelöst.  Ein Grund liegt möglicherweise in einer falschen Anreizstruktur.

#Zzs: Die Entwicklung maßgeschneiderter Systeme zur Förderung Innovationen im Bereich der Computerprogramme und sonstigen abstrakt-logischen Schöpfungen wird systematisch angestrebt.  Es kommen sanfte Ausschlussrechte, nicht-exklusive Vergütungsrechte und die öffentliche Förderung der Bildung und Forschung in Betracht.  Dieses Feld der Rechtspolitik wird unter Einbeziehung aller interessierten Kreise nachhaltig und systematisch bearbeitet.

#Irf: Zugleich wird die Bundesregierung aufgefordert, sich bei der EU-Kommission für eine klärende Richtlinie im selben Sinne einzusetzen.

#IWn: Bezüglich § PatG wird der deutschen Rechtsprechung eine klare und konsistente Auslegung vorgegeben, wie sie in den BGH-Beschlüssen %{LST} mustergültig formuliert und im %(gt:Eurolinux-Richtlinienvorschlag) aus aktuellem Anlass neu aufbereitet wurde.

#Wrr: Wenn jemand durch Software-Marktmacht in der öffentlichen Kommunikation %(q:Standards setzt), muss es immer erlaubt sein, diese Standards einzuhalten.  Die Herstellung der Interoperabilität ist bereits in der EU-Urheberrechtsrichtlinie von 1991 als hohes Rechtsgut anerkannt.  Zum Zwecke der Interoperabilität muss man auch patentierte Verfahren ohne besondere Erlaubnis unentgeltlich verwenden dürfen.  Staatliche Stellen und Träger öffentlicher Funktionen dürfen nur über frei zugängliche Kommunikationsstandards mit dem Bürger kommunizieren.  Auch dieses Recht verdient Erwähnung sowohl in den Schrankenbestimmungen des Patentrechts als auch auf Verfassungebene, denn es folgt aus dem Recht des Bürgers auf Teilnahme der Informationsgesellschaft.

#Fbg: Ferner wäre das Recht auf Veröffentlichung von selbst erarbeiteten Informationswerken als eine Konsequenz der Meinungs- und Ausdrucksfreiheit auf Verfassungsebene zu schützen.

#Dml: Die Rechtsverhältnisse beim Einsatz eines programmgesteuerten Verfahrens zur gewerblichen Herstellung materieller Güter bleiben davon unberührt.

#dge: das Ausführen von Programmen auf einer Datenverarbeitungsanlage.

#dKb: das Herstellen, Anbieten, Inverkehrbringen, Besitzen oder Einführen von Urheberrechtsgegenständen aller Art, insbesondere Gebrauchsanweisungen und Programmen für Datenverarbeitungsanlagen.

#DeW: Meinungs- und Ausdrucksfreiheit auch für Sprachwerke aller Art.  Patente dürfen nicht als Zensur wirken.  Sie können sich allenfalls gegen die Verwendung der Software richten, nicht gegen ihre Veröffentlichung, Verteilung oder Weitergabe.  DV-Programme werden Gebrauchsanweisungen gleichgestellt.  Das EPÜ enthält zu diesem Themenkomplex keine Bestimmungen.  Wie die gewährten europäischen Patente im einzelnen durchgesetzt werden, ist Sache nationaler Regelungen. Im deutschen PatG etwa wäre folgende Absatz in §11 einzufügen:

#SbW: Entbürokratisierung, Internationalisierung und demokratische Kontrolle des Patentwesens

#GWi: Sachgerechte Förderung informationeller Innovationen

#Iez: Präzisierung der Patentierbarkeitskriterien

#Rfi: Recht auf freien Zugang zu Normen der Öffentlichen Kommunikation

#Rue: Recht auf Veröffentlichung von eigenen Informationswerken

#Ddg: Doch die Expansionsbestrebungen der Patentbewegung und ihrer Verbündeten in diversen Großunternehmen, Verbänden und Regierungsgremien gehen weiter.  Ihre Unterstützung brauchen wir nun mehr denn je.  Wir bitten Sie, gemeinsam mit uns auf nationaler und europäischer Ebene folgende Gesetzesinitiativen auf den Weg zu bringen:

#CWn: Computerprogramme sind auch künftig laut Gesetzeslage nicht patentierbar.  Es ist gelungen, das Patentwesen in einem Punkt vorübergehend politisch zu kontrollieren.  Dieser Erfolg ist nicht zuletzt Ihrer Unterstützung zu verdanken.

#Stn: Sehr geehrte Damen und Herren!

#descr: Mit Bitte um Verbesserungsvorschläge und Mithilfe bei der weiteren Ausarbeitung der einzelnen Gesetzesinitiativen.  Wenn Sie im Namen eines IT-Unternehmens oder IT-Verbandes sprechen können, bitten wir ferner um Ihre Zustimmung zur Nennung als Unterzeichner.  Hier finden Sie nur den Haupttext.  Der vollständige Brief mit zahlreichen Anhängen wird in Papierfassung an Politiker versandt.

#title: Offener Brief: 5 Gesetzesinitiativen zum Schutz der Informatischen Innovation

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxpatg2C ;
# txtlang: de ;
# multlin: t ;
# End: ;

