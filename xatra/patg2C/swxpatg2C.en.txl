<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vpo: Vorstandssprecher %s

#HoW: Informatiker, Heinrich Nixdorf Stiftungsprofessor, Lehrstuhl für Information und Kommunikation, Universität Rostock

#JpI: Patentrechtsexperte und Justitiar des FFII in Österreich

#Jst: Jusitiar

#Vsn: Vorstand %s

#Pio: Phaidros AG

#Ira: Intradat AG

#imt: innominate AG

#Fte: Frontsite AG

#Gtr: Gesellschaft für Kybernetik e.V.

#vov: Virtueller Ortsverein der SPD

#LxW: Linuxtag e.V.

#LiVe: Linux-Verband

#Mue: Sincerely and Respectfully

#WnB: We beg you to work out these proposals together with us and to work for their adoption by your national parliament or other suitable decisionmaking bodies.

#OaR: Ob und wie schnell man diese Radikalkur anstrebt, sei dahingestellt.  Wichig ist das Ziel, internationale Behörden wie das Europäische Patentamt zurechenbar zu machen, zu verschlanken wenn nicht abzuschaffen.  Statt fetter Behörden brauchen wir offene juristische Standards, die im Rahmen der national verankerten demokratischen Gewaltenteilung kontrolliert und ohne bürokratischen Aufwand internationalisiert werden können.

#EPd: It is made possible to register patents free of charge without examination and without annual fees by a standard-conformant publication in the Internet.  Such patents become valid immediately upon registration.  In return, additional incentives for patent invalidation procedings are created.  If someone succeeds in invalidating or narrowing a patent, he receives, in addition to the reimbursement of ligitation costs, an attractive incentive payment from the owner of the bad patent.  Instead of the long and ineffective official examination, a private professional group of patent busters shall protect the public from invalid patents.  The official examination continues to exist, but is conducted only at the request of the applicant.  It can be conducted either by the patent office or by certified examination institutes at home and in foreign countries.

#HrW: Heute steht gilt für das Europäischen Patentamt das umgekehrte Verursacherprinzip:  Der Verschmutzer wird bezahlt.  Je mehr Trivialpatente das EPA erteilt, desto reicher wird es.

#DwW: Die Qualität der erteilten Patente sinkt, während ihre Zahl explodiert.  Patentanträge werden oft erst dann geprüft, wenn die Technik schon veraltet ist.  Es wird zwar eifrig an internationaler Harmonisierung gearbeitet, aber dies führt im Ergebnis vor allem zu einer weiteren Erstarrung und Unreformierbarkeit statt zu einer Kostensenkung.  Diese Probleme werden seit Jahrzehnten kritisiert aber nicht gelöst.  Ein Grund liegt möglicherweise in einer falschen Anreizstruktur.

#Zzs: At the same time, appropriate systems for stimulating innovation in the area of computer programs and other informational creations are to be developped.  Interdisciplinary studies and international conference on this subject are to be promoted.

#Irf: At the same time the government is asked to push for a Europe wide clarification in the same sense.

#IWn: Concerning Art 52 EPÜ / §1 PatG, the parliament passes a resolution which supplies the judiciary with a clear and consistent interpretation of the law, as it has been concisely %(gt:formulated) by the Eurolinux Alliance.  If the patent judiciary still insists on misinterpreting the law, the law texts itself can later be amended.

#Wrr: When someone %(q:sets standards) in public communication through software market power, everybody must always be unconditionally permitted to conform to these standards.  Interoperability has already been recognized as a high legal good in the EU copyright directive of 1991.  For the purpose of interoperability, it must be allowed to use patented procedures without obtaining a permission.  Representatives of the state and carriers of public functions must use freely accessible communication standards to communicate with citizens.  This right also deserves constitutional protection, because it follows from the right of citizens of equal participation in the information society.

#Fbg: Ferner wäre das Recht auf Veröffentlichung von selbst erarbeiteten Informationswerken als eine Konsequenz der Meinungs- und Ausdrucksfreiheit auf Verfassungsebene zu schützen.

#Dml: Die Rechtsverhältnisse beim Einsatz eines programmgesteuerten Verfahrens zur gewerblichen Herstellung materieller Güter bleiben davon unberührt.

#dge: das Ausführen von Programmen auf einer Datenverarbeitungsanlage.

#dKb: das Herstellen, Anbieten, Inverkehrbringen, Besitzen oder Einführen von Urheberrechtsgegenständen aller Art, insbesondere Gebrauchsanweisungen und Programmen für Datenverarbeitungsanlagen.

#DeW: Freedom of expression extends to works written in a programming language.  Patents may not be used to censor publications.  They may at most be directed against the use of the software, not against its publication, distribution or transmission.  Computer programs are considered equivalent to process descriptions and operating instructions.  The EPC does not contain regulations concerning this subject.  By what means European patents can be asserted is a matter of national regulation.  The German Patent Law (PatG) could be amended by inserting at §11 (7):

#SbW: Debureaucratisation of the Patent System

#GWi: Adequate Stimulation of Informational Innovation

#Iez: Clarification of the criteria of patentability

#Rfi: Right of Free Access to Standards of Public Communication

#Rue: Right to Publish one's own information works

#Ddg: Doch die Expansionsbestrebungen der Patentbewegung und ihrer Verbündeten in diversen Großunternehmen, Verbänden und Regierungsgremien gehen weiter.  Ihre Unterstützung brauchen wir nun mehr denn je.  Wir bitten Sie, gemeinsam mit uns auf nationaler und europäischer Ebene folgende Gesetzesinitiativen auf den Weg zu bringen:

#CWn: Computer program remain unpatentable according to the written law.  The patent system has been successfully subjected to political control for once on one issue.  This success is largely due to your support.

#Stn: Dear ladies and gentlemen!

#descr: Please help us to improve and work out the law initiatives.  If you can speak in the name of an IT company or an IT organisation, please allow us to list you as a signatory.  Below you find only the letter body. The complete letter with numerous appendices is sent as a paper version to politicians.

#title: Open Letter: 5 Law Initiatives to Protect Information Innovation

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxpatg2C ;
# txtlang: en ;
# multlin: t ;
# End: ;

