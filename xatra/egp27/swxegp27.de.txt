<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Die EU-Kommission möchte innerhalb des Jahres 2000 das europäische Patentwesen weiter zentralisieren, um den Nutzern des Patentsystemes %(q:viel Wert für wenig Geld) bieten zu können.  Wir fordern, dass einer solchen quantitativen Reform eine qualitative Reform vorangehen soll.  Das Patentwesen muss kontrollierbar werden.  Die Patentinflation muss wirksam und dauerhaft eingedämmt werden.  Es müssen Institutionen und Regeln geschaffen werden, die dem Patentkomplex Anreize bieten, lieber weniger und bessere Patente zu produzieren.
title: FFII 2000-07: Anregungen zum Gemeinschaftspatent
Dlm: Dieses Jahr will die EU-Kommission das Patentwesen grundlegend reformieren und dabei die Weichen für die nächsten 20 Jahre stellen.  Hauptanliegen der bisherigen Entwürfe der EU-Kommission ist es, das Patentwesen zu zentralisieren, um Patentinhabern %(q:mehr Wert für weniger Geld) bieten und somit einen stärkeren Anreiz zur Anmeldung von möglichst vielen Patenten zu schaffen.
WeW: Wir meinen:  Weniger ist mehr.  Das Patentwesen befindet sich heute in einer Krise.  Es wird von der Gesellschaft nicht mehr unbedingt als legitim angesehen.  Es hat ein Klima der Rechtsunsicherheit und Streitsucht geschaffen.  Unternehmen sammeln Patente wie Waffen.  Sie dienen längst nicht mehr dem Schutz echter Erfindungen.  Vielmehr werden Minenfelder aufgebaut, auf denen kleine Unternehmen nicht überleben und selbst große Unternehmen sich unwohl fühlen.
Dhg: Der Grund für diese Krise liegt darin, dass das Patentwesen außer Kontrolle geraten ist.  Die Gesetze werden von Patentexperten für Patentexperten gemacht.  Je mehr Patente vergeben werden, desto besser geht es dem Berufsstand der Patentexperten.  Vor Gerichten kommt nur der Standpunkt patentsuchender Unternehmer, nicht aber der Standpunkt der terrorisierten Öffentlichkeit zu Gehör.  Die Gerichte und Gesetzgeber haben sich daraus eine proprietaristische Berufsideologie zurecht gelegt, unter deren Anleitung sie ständig nach Wegen suchen, die Anforderungen an Patentierbarkeit weiter abzusenken und aufzuweichen.
AWh: Auch das Projekt %(q:Gemeinschaftspatent) steht unter dem Vorzeichen dieser Berufsideologie.  Alle Veröffentlichungen der Generaldirektion Binnenmarkt sind von dem Glaubenssatz %(q:je mehr Patente desto mehr Innovation) durchströmt.  Es fehlt jeder Ansatz einer ausgewogenen Sicht.
WWK: Wir erkennen zwar durchaus an, dass die Straffung des europäischen Patentwesens Vorteile haben kann.  Aber es nützt uns wenig, wenn ein von Grund auf unkontrollierbares und korruptes Patentwesen lediglich gestrafft und effektiver gemacht wird.  Die Reform des Jahres 2000 muss das Übel an der Wurzel packen, wenn sie wirklich ein gutes System für die nächsten 20 Jahre schaffen will.  Es müssen Systeme und Institutionen geschaffen werden, die dem Patentsystem einen Anreiz zur Selbstverbesserung geben könnten, z.B.:
Ulo: Parlamentarischer Kontrollausschuss Informationseigentum
Agn: Abschaffung der Lizenzgebühren
Nct: Neuer Zweck der Patentämter
NiK: Neuheitsprüfung
Enk: Erfindungshöhe-Prüfung
KBq: Klärung des Begriffes %(q:Technizität)
Geg: Geistiges Gemeineigentum und Allgemeinheitspatent
nur: Übersetzungsanforderungen
WeI: Weitere Ideen
Ide: Im Europa-Parlament ist ein Ausschuss für Fragen des Informationseigentums (meist irreführend %(q:geistiges Eigentum) genannt) zu gründen, der sich nur zu einem geringen Teil aus Patentexperten oder Juristen zusammensetzt.  90% der Ausschussmitglieder sollen aus Berufsgruppen kommen, deren Wohlergehen nicht von der Expansion des Informationseigentumskomplexes abhängt.
Inl: Im Zeitalter des digitalen Kapitalismus entstehen dauernd Forderungen nach Ausweitung oder Rücknahme bisheriger Informationseigentumsrechte wie z.B. Eigentum an Datenbanken, Architekturentwürfen, Fußballsenderechten.   All diese Rechte sind tendenziell unbeständig und problematisch, können aber dennoch für die Wirtschaftsentwicklung nützlich sein.  Hier ist der Gesetzgeber ständig gefordert, grundlegende politische Entscheidungen zu treffen, die nicht der Judikative überlassen werden können.
DgK: Der %(q:Parlamentarische Ausschuss für Informationseigentum) sollte sich ständig mit auftretenden Grenzverschiebungen und Grundsatzfragen beschäftigen und darüber wachen, dass die die Gerichte sich an die Vorgaben des Gesetzgebers halten.
Oel: Ähnliche Ausschüsse könnten auch in nationalen Parlamenten gegründet werden.
Prr: Patentämter sollten nicht mehr an der Vergabe von Patenten verdienen.  Die Patentgebühren sind abzuschaffen.  Die Patentgerichtsbarkeit ist stattdessen aus Steuergeldern zu finanzieren.
Hie: Hauptaufgabe der Patentämter soll es sein, der Öffentlichkeit Informationen über Patente und den Stand der Technik umfassend, offen und frei zugänglich zu machen.
Dzs: Die Patentämter sollten keine Recherchen mehr durchführen.  Für die Recherchen sollen hingegen die private Prüfungsunternehmen (nach Art von Wirtschaftsprüfern) verantwortlich sein.  Diese Unternehmen sollen immer dann Schadensersatz leisten, wenn sie nicht vollständig recherchiert haben und später weitere neuheitsschädigende Dokumente auftauchen.  Der Preis der Recherche soll vom Marktwettbewerb frei bestimmt werden.
DSW: Die Erfindungshöhe wird durch einen öffentlichen Wettbewerb überprüft.  Sobald die Neuheitsprüfung abgeschlossen ist, wird das rechtsverbindliche %(q:von der Erfindung zu lösende Problem) zusammen mit dem %(q:Stand der Technik) veröffentlicht.  Innerhalb einer Einreichungsfrist von 6 Monaten hat dann die gesamte Öffentlichkeit die Gelegenheit, Problemlösungsvorschläge einzureichen, die nach 6 Monaten veröffentlicht werden.  Wer die zu patentierende Lösung gefunden hat, wird Mitinhaber des Patents.  Andere Lösungen werden dem %(q:Stand der Technik) zugerechnet, an dem das Patent zu messen ist. Falls es mehr als 3 Mitinhaber gibt, gilt die Erfindung als nicht genügend erfinderisch und der Patentantrag wird verworfen.
Dcc: Der Begriff der Technizität spielt heute bei der Abgrenzung dessen, was eine patentierbare Erfindung darstellt, eine entscheidende Rolle.  Daher ist dieser Begriff verbindlich zu klären.  Man kann dabei auf die BGH-Definition zurückgreifen, wonach %(q:planmäßiges Handeln unter Einsatz beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit) vorliegen muss.  Erfindungen im Bereich der Programmlogik sind nicht technisch, weil sie (in ihrem erfinderischen Kern) keinen Gebrauch von Naturkräften machen.
WWs: Wenn bisher von %(q:geistigem Eigentum) die Rede ist, ist damit ausschließlich %(e:informationelles Privateigentum) gemeint.  Die Entwicklung der %(e:Freien Software) (OpenSource) macht aber deutlich, dass es auch Lizenzmodelle wie das %(e:Allgemeinheitsurheberrecht) (Copyleft) gibt, die zum Schutz eines %(e:informationellen Gemeineigentums) dienen.  Diese Modelle greifen letzter Zeit auch auf klassische Industriebranchen (z.B. Prozessorbau, Verbrennungsmotorenbau) über.  Es handelt sich hier um eine neue, wirtschaftspolitisch erwünscht Entwicklung, der das Patentwesen Rechnung tragen sollte.
Eie: Es ist klarzustellen, dass Patentansprüche sich niemals gegen informationelles Gemeineigentum richten dürfen.  Wer Informationen in das Gemeineigentum überstellt, kann damit kein Patent verletzten.  Der Begriff der %(q:gewerblichen Anwendung) ist entsprechend verbindlich zu definieren.
Eve: Es sollte eine Sonderform des Patentschutzes für informationelles Gemeineigentum geben.  Wer eine Erfindung zum Allgemeinheitspatent, macht alle Bürger zu Eigentümern des Patentes und kann damit, ähnlich wie beim Allgemeinheitsurheberrecht, die Bedingung verbinden, dass die patentierte Erfindung nicht in Systemen verwendet werden darf, in denen unfreie (privat patentierte) Erfindungen zum Einsatz kommen.  Auf diese Weise kann der Erfinder der Öffentlichkeit ein Mittel in die Hand geben, mit dem sie sich gegen frivolen Patentterror wehren kann.  Für Gemeineigentumspatente und nur für sie sollte es eine Schonfrist nach Veröffentlichung geben, innerhalb derer sie noch angemeldet werden können.  Ferner sollten für sie alle eventuellen Patentgebühren entfallen.
Eso: Ein Hauptanliegen des derzeitigen Kommissionsentwurfs ist es, die Übersetzungskosten zu senken, die laut Angaben der Kommission derzeit bei ca 25% der Kosten eines Patents liegen.
Dkf: Die Übersetzungsanforderungen sollten nicht so leichtherzig geopfert werden, wie die EU-Kommission das vorschlägt.  Ein Patent dient zu mehr als nur dazu, seinem Eigentümer %(q:viel Wert für wenig Geld) zu bieten.  Es erfüllt z.B. folgende öffentlichen Funktionen
Rsl: Rechtsaufklärung
3Ub: jeden Bürger (nicht nur Patentsexperten, die Englisch oder andere Patentsprachen flüssig beherrschen) darüber aufzuklären, was er tun darf und was nicht
Tkl: Technik-Aufklärung
pls: modernes technisches Wissen zu vermitteln
War: Wissen über wenige echte Erfindungen, die durch %(aw:ausgewogene Wettbewerbsverfahren) ausgesucht wurden, nicht übersetzungsunwürdiger Informationsmüll
Scl: Sprachpflege
ccW: Technische Terminologie in verschiedenen Sprachen zu pflegen.  Das EPA wirbt gerne bei Übersetzern damit, dass gerade im Rahmen der Patentierung eine gepflegte Sprache mit gut durchdachter und normierter Terminologie entsteht, auf die technische Übersetzer sich auch außerhalb des Patentwesens stützen können.
nsu: Durch Verwendung einer %(ll:Logiksprache), die sich automatisch in viele Sprachen übersetzen lässt, könnte man die Vielsprachigkeit billig machen und gleichzeitig kostspielige Vieldeutigkeiten beseitigen, die durch Verwendung einer syntaktisch unscharfen Sprache wie Englisch entstehen.  Es erscheint widersprüchlich, in einer Zeit, wo die EU-Kommission viel Geld für eine %(q:vielsprachige Informationsgesellschaft) ausgibt und die kulturelle Heterogenität Europas als Standortvorteil anpreist, die Vielsprachigkeit im Patentwesen ohne Not beseitigen zu wollen.
ZiW: Zwangslizenz für Teile von Normen oder komplexen Systemen
Mwl: Manche Erfindungen werden im Laufe der Zeit Teile von Normen, an die sich jeder Wettbewerbsteilnehmer halten muss, um mit anderen kompatibel zu sein.  In solchen Fällen sollte das bereits weitgehend abgeschaffte System der Zwangslizenz erneut zu Ehren kommen.
Oei: Ähnliches gilt für komplexe Systeme, in denen hunderte von Erfindungen verwendet werden.  Hier sind die Transaktionskosten zu hoch, wenn jeder einzelne Patentinhaber das Recht behält, das gesamte System zu blockieren.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxegp27 ;
# txtlang: de ;
# End: ;

