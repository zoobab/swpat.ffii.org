<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Oei: Ähnliches gilt für komplexe Systeme, in denen hunderte von Erfindungen verwendet werden.  Hier sind die Transaktionskosten zu hoch, wenn jeder einzelne Patentinhaber das Recht behält, das gesamte System zu blockieren.

#Mwl: Manche Erfindungen werden im Laufe der Zeit Teile von Normen, an die sich jeder Wettbewerbsteilnehmer halten muss, um mit anderen kompatibel zu sein.  In solchen Fällen sollte das bereits weitgehend abgeschaffte System der Zwangslizenz erneut zu Ehren kommen.

#ZiW: Compulsory License for Parts of Standards or Complex Systems

#nsu: Translation can be made cheap by using a %(ll:Logical Language) which allows reliable automatic translation into many languages.  It is contradictory to abolish translation requirements at a time when other directorates of the EU are investing a lot of money in making a %(q:multilingual information society) viable.

#ccW: cultivating technical terminology in multiple languages

#Scl: Language Cultivation

#War: knowledge about real inventions, selected by %(aw:balanced competition procedures), not masses of cheap trivial patents)

#pls: popularising advanced technological knowledge

#Tkl: Technological Information

#3Ub: telling everybody (not only patent specialists who are fluent in English or other patent languages) what they are allowed to do

#Rsl: Legal Information

#Dkf: translation requirements should not be sacrificed as light-heartedly as the EC is proposing.  Offering patent owners %(q:good value for little money) isn't everything. The patent system has some public-interest purposes such as

#Eso: Ein Hauptanliegen des derzeitigen Kommissionsentwurfs ist es, die Übersetzungskosten zu senken, die laut Angaben der Kommission derzeit bei ca 25% der Kosten eines Patents liegen.

#Eve: Es sollte eine Sonderform des Patentschutzes für informationelles Gemeineigentum geben.  Wer eine Erfindung zum Allgemeinheitspatent, macht alle Bürger zu Eigentümern des Patentes und kann damit, ähnlich wie beim Allgemeinheitsurheberrecht, die Bedingung verbinden, dass die patentierte Erfindung nicht in Systemen verwendet werden darf, in denen unfreie (privat patentierte) Erfindungen zum Einsatz kommen.  Auf diese Weise kann der Erfinder der Öffentlichkeit ein Mittel in die Hand geben, mit dem sie sich gegen frivolen Patentterror wehren kann.  Für Gemeineigentumspatente und nur für sie sollte es eine Schonfrist nach Veröffentlichung geben, innerhalb derer sie noch angemeldet werden können.  Ferner sollten für sie alle eventuellen Patentgebühren entfallen.

#Eie: Es ist klarzustellen, dass Patentansprüche sich niemals gegen informationelles Gemeineigentum richten dürfen.  Wer Informationen in das Gemeineigentum überstellt, kann damit kein Patent verletzten.  Der Begriff der %(q:gewerblichen Anwendung) ist entsprechend verbindlich zu definieren.

#WWs: Wenn bisher von %(q:geistigem Eigentum) die Rede ist, ist damit ausschließlich %(e:informationelles Privateigentum) gemeint.  Die Entwicklung der %(e:Freien Software) (OpenSource) macht aber deutlich, dass es auch Lizenzmodelle wie das %(e:Allgemeinheitsurheberrecht) (Copyleft) gibt, die zum Schutz eines %(e:informationellen Gemeineigentums) dienen.  Diese Modelle greifen letzter Zeit auch auf klassische Industriebranchen (z.B. Prozessorbau, Verbrennungsmotorenbau) über.  Es handelt sich hier um eine neue, wirtschaftspolitisch erwünscht Entwicklung, der das Patentwesen Rechnung tragen sollte.

#Dcc: Der Begriff der Technizität spielt heute bei der Abgrenzung dessen, was eine patentierbare Erfindung darstellt, eine entscheidende Rolle.  Daher ist dieser Begriff verbindlich zu klären.  Man kann dabei auf die BGH-Definition zurückgreifen, wonach %(q:planmäßiges Handeln unter Einsatz beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit) vorliegen muss.  Erfindungen im Bereich der Programmlogik sind nicht technisch, weil sie (in ihrem erfinderischen Kern) keinen Gebrauch von Naturkräften machen.

#DSW: after the novelty check, only the %(goal of the invention) and the %(q:prior art) parts are published, and anybody who submits the same solution within 6 months becomes co-patent-owner.  If there are more than 3 such co-patent-owners, the invention is deemed not inventive enough and rejected.

#Dzs: patent office no longer conduct patent examination themselves.  Instead this is given into the hands of certified companies, who are held liable later if they have not found all prior art (similar to the liability of financial audit companies).

#Hie: The chief purpose of existence of the patent offices shall be to provide valuable free information on patents and related prior art to the public.

#Prr: patent offices no longer receive money for granting patents.  Patent fees are abolished.

#Oel: Ähnliche Ausschüsse könnten auch in nationalen Parlamenten gegründet werden.

#DgK: Der %(q:Parlamentarische Ausschuss für Informationseigentum) sollte sich ständig mit auftretenden Grenzverschiebungen und Grundsatzfragen beschäftigen und darüber wachen, dass die die Gerichte sich an die Vorgaben des Gesetzgebers halten.

#Inl: Im Zeitalter des digitalen Kapitalismus entstehen dauernd Forderungen nach Ausweitung oder Rücknahme bisheriger Informationseigentumsrechte wie z.B. Eigentum an Datenbanken, Architekturentwürfen, Fußballsenderechten.   All diese Rechte sind tendenziell unbeständig und problematisch, können aber dennoch für die Wirtschaftsentwicklung nützlich sein.  Hier ist der Gesetzgeber ständig gefordert, grundlegende politische Entscheidungen zu treffen, die nicht der Judikative überlassen werden können.

#Ide: Im Europa-Parlament ist ein Ausschuss für Fragen des Informationseigentums (meist irreführend %(q:geistiges Eigentum) genannt) zu gründen, der sich nur zu einem geringen Teil aus Patentexperten oder Juristen zusammensetzt.  90% der Ausschussmitglieder sollen aus Berufsgruppen kommen, deren Wohlergehen nicht von der Expansion des Informationseigentumskomplexes abhängt.

#WeI: Further Ideas

#nur: Translation Requirements

#Geg: Public Intellectual Property and Patentleft

#KBq: Definition of %(q:Technicity)

#Enk: inventivity check

#NiK: novelty check

#Nct: New Purpose of the Patent Offices

#Agn: Abschaffung der Lizenzgebühren

#Ulo: Parlamentarischer Kontrollausschuss Informationseigentum

#WWK: Wir erkennen zwar durchaus an, dass die Straffung des europäischen Patentwesens Vorteile haben kann.  Aber es nützt uns wenig, wenn ein von Grund auf unkontrollierbares und korruptes Patentwesen lediglich gestrafft und effektiver gemacht wird.  Die Reform des Jahres 2000 muss das Übel an der Wurzel packen, wenn sie wirklich ein gutes System für die nächsten 20 Jahre schaffen will.  Es müssen Systeme und Institutionen geschaffen werden, die dem Patentsystem einen Anreiz zur Selbstverbesserung geben könnten, z.B.:

#AWh: Auch das Projekt %(q:Gemeinschaftspatent) steht unter dem Vorzeichen dieser Berufsideologie.  Alle Veröffentlichungen der Generaldirektion Binnenmarkt sind von dem Glaubenssatz %(q:je mehr Patente desto mehr Innovation) durchströmt.  Es fehlt jeder Ansatz einer ausgewogenen Sicht.

#Dhg: Der Grund für diese Krise liegt darin, dass das Patentwesen außer Kontrolle geraten ist.  Die Gesetze werden von Patentexperten für Patentexperten gemacht.  Je mehr Patente vergeben werden, desto besser geht es dem Berufsstand der Patentexperten.  Vor Gerichten kommt nur der Standpunkt patentsuchender Unternehmer, nicht aber der Standpunkt der terrorisierten Öffentlichkeit zu Gehör.  Die Gerichte und Gesetzgeber haben sich daraus eine proprietaristische Berufsideologie zurecht gelegt, unter deren Anleitung sie ständig nach Wegen suchen, die Anforderungen an Patentierbarkeit weiter abzusenken und aufzuweichen.

#WeW: Wir meinen:  Weniger ist mehr.  Das Patentwesen befindet sich heute in einer Krise.  Es wird von der Gesellschaft nicht mehr unbedingt als legitim angesehen.  Es hat ein Klima der Rechtsunsicherheit und Streitsucht geschaffen.  Unternehmen sammeln Patente wie Waffen.  Sie dienen längst nicht mehr dem Schutz echter Erfindungen.  Vielmehr werden Minenfelder aufgebaut, auf denen kleine Unternehmen nicht überleben und selbst große Unternehmen sich unwohl fühlen.

#Dlm: Dieses Jahr will die EU-Kommission das Patentwesen grundlegend reformieren und dabei die Weichen für die nächsten 20 Jahre stellen.  Hauptanliegen der bisherigen Entwürfe der EU-Kommission ist es, das Patentwesen zu zentralisieren, um Patentinhabern %(q:mehr Wert für weniger Geld) bieten und somit einen stärkeren Anreiz zur Anmeldung von möglichst vielen Patenten zu schaffen.

#descr: Die EU-Kommission möchte innerhalb des Jahres 2000 das europäische Patentwesen weiter zentralisieren, um den Nutzern des Patentsystemes %(q:viel Wert für wenig Geld) bieten zu können.  Wir fordern, dass einer solchen quantitativen Reform eine qualitative Reform vorangehen soll.  Das Patentwesen muss kontrollierbar werden.  Die Patentinflation muss wirksam und dauerhaft eingedämmt werden.  Es müssen Institutionen und Regeln geschaffen werden, die dem Patentkomplex Anreize bieten, lieber weniger und bessere Patente zu produzieren.

#title: FFII 2000-07: Suggestions for the Community Patent

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxegp27 ;
# txtlang: en ;
# multlin: t ;
# End: ;

