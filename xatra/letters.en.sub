\begin{subdocument}{swpatxatra}{Letters and Appeals}{http://swpat.ffii.org/letters/index.en.html}{Workgroup\\swpatag@ffii.org}{The FFII has written to various politicians to ask them for help against the unlawful and harmful decisions of the patent movement.  These letters include private and public letters as well as reusable letter templates.}
Various organisations have published anti swpat petitions\footnote{http://swpat.ffii.org/archive/mirror/pikci.en.html} which collected tens of thousands of signatures and aroused some attention in the press.  Our letter template\footnote{http://swpat.ffii.org/letters/nopat/index.en.html} is being used in the Eurolinux Letter Generator\footnote{http://petition.eurolinux.org/letter/}, which makes it easy for anyone to put together letters to decisionmakers and print them so that they just need to be inserted into a window envelope.

\begin{sect}{inhalt}{Contents}
\begin{itemize}
\item
{\bf {\bf Call for Action}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players.  Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf Appeal to the German Government}\footnote{http://swpat.ffii.org/letters/bund028/index.de.html}}

\begin{quote}
The Federal Government is pushing Brussels for legalisation of patents which it is at the same time infringing.  It has so far not published a position concerning the software patentability directive proposal of the European Commission.  While the parliament and the ministeries are on the whole rather critical, the lawyer-diplomats from the ministry of justice are pushing the European Council toward even more radically pro-software-patent counter-proposal.  The signatories direct four demands to the German Government.
\end{quote}
\filbreak

\item
{\bf {\bf FFII to Bitkom: let's measure law proposals against a Testsuite!}\footnote{http://swpat.ffii.org/letters/bitk025/index.de.html}}

\begin{quote}
On 2002-05-07 the German IT association Bitkom published a press release supporting the EU Directive Proposal for the patentability of ``comuter-implemented inventions''.  After a phone conversation with the authors of the press release, Hartmut Pilch wrote a letter to Bitkom which we publish here.  The letter explains a methodological consensus position which at first sight seemed convincing to the Bitkom people.
\end{quote}
\filbreak

\item
{\bf {\bf Open Letter: 5 Law Initiatives to Protect Information Innovation}\footnote{http://swpat.ffii.org/letters/patg2C/index.en.html}}

\begin{quote}
Please help us to improve and work out the law initiatives.  If you can speak in the name of an IT company or an IT organisation, please allow us to list you as a signatory.  Below you find only the letter body. The complete letter with numerous appendices is sent as a paper version to politicians.
\end{quote}
\filbreak

\item
{\bf {\bf 2001/01: Brief an das BMJ}\footnote{http://swpat.ffii.org/letters/epue31/index.de.html}}

\begin{quote}
Der FFII erhielt vom Bundesministerium der Justiz (BMJ) ein Schreiben, in dem Auskunft \"{u}ber die Ergebnisse der Diplomatischen Konferenz zur Revision des Europ\"{a}ischen Patent\"{u}bereinkommens gegeben und zu Stellungnahmen aufgerufen wird.  Dies geschieht mit diesem Brief.  Der FFII kritisiert u.a. erneut die \"{U}bernahme der TRIPs-Formel ``auf allen Gebieten der Technik'' in Art 52 EP\"{U}.
\end{quote}
\filbreak

\item
{\bf {\bf Eur. Patentamt begehrt grenzenlose Patentierbarkeit}\footnote{http://swpat.ffii.org/letters/epue28/index.de.html}}

\begin{quote}
Offener Brief zum ``Basisvorschlag f\"{u}r die Revision des Europ\"{a}ischen Patent\"{u}bereinkommens''
\end{quote}
\filbreak

\item
{\bf {\bf No E-Patents: write to your Parliamentary Representative!}\footnote{http://swpat.ffii.org/letters/nopat/index.en.html}}

\begin{quote}
You could sign and send this or a similar letter to your parliamentary representative or numerous other decisionmakers.
\end{quote}
\filbreak

\item
{\bf {\bf Letter to the Competition Commissioner}\footnote{http://swpat.ffii.org/letters/miert/index.en.html}}

\begin{quote}
We have forwarded this letter, which meanwhile has close to 10000 signatures, to Mr. Van Miert and we will also send it to his successor and other members of the EU commission
\end{quote}
\filbreak

\item
{\bf {\bf EU Patents in Logical Language!}\footnote{http://swpat.ffii.org/letters/lojban/index.en.html}}

\begin{quote}
The EU patent organisations should help fund the development of free software for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information multilingual and maximally accessible to the public and the enterprises.
\end{quote}
\filbreak

\item
{\bf {\bf Letters to Newspaper Editors}\footnote{http://swpat.ffii.org/letters/karni/index.de.html}}

\begin{quote}
The press is often famed as ``artillery of freedom'', but journalists don't always have the time to research properly.  Especially in complicated matters like software patents some help from readers is often necessary.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Letters from other sources}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Phil Karn: The US Patent System is Out of Control\footnote{http://people.qualcomm.com/karn/patents/patents.html}}}

\begin{quote}
contains some very knowledgable and moving letters from an experienced telecom programmer to American politicians
\end{quote}
\filbreak

\item
{\bf {\bf European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\footnote{http://swpat.ffii.org/papers/eukonsult00/index.en.html}}}

\begin{quote}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.
\end{quote}
\filbreak

\item
{\bf {\bf Eurolinux Consultation on Patentability of Computer Programs\footnote{http://petition.eurolinux.org/consultation}}}

\begin{quote}
about 1000 submissions of European programmers to the European Commission
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf How you can help us end the software patent nightmare\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}
\filbreak

\item
{\bf {\bf Collect addresses of people to write to!}}

\begin{quote}
Maintain an ever-improving letter-writing toolkit for our friends and for the public
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
% mode: latex ;
% End: ;

