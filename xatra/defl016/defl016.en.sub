\begin{subdocument}{swxdefl016}{Letters and Appeals}{http://swpat.ffii.org/letters/defl016/index.en.html}{Workgroup\\swpatag@ffii.org}{This petition is directed to the United Nations, the European Parliament, the US Congress, the Japanese Parliament and to all other concerned legislative bodies worldwide. Its goal is to help bring patent inflation under control and to warn against immininent dangers. This petition is initiated by the Eurolinux Alliance together with companies and non-profit associations worldwide.  Please sign this petition, make it known and, if you like, join our campaign and help us get more work done.   }
I am worried by the uncontrolled expansion of the patent system\footnote{http://swpat.ffii.org/analysis/inflation/index.en.html} into areas where it clearly does not help to promote the progress of science and the useful arts\footnote{http://swpat.ffii.org/papers/digidilem00/index.en.html}.

I note that former standards of patentability have been continuously eroding, so that more and more patents of dubious merit are being granted, and that governments are allowing their patent experts to make this trend irreversible by writing it into international law\footnote{http://swpat.ffii.org/news/wipo014/index.de.html}.

I note that patents are being granted for logical concepts, such as mathematical methods, programming solutions and business ideas, and that formerly clear doctrines on the limits of patentability are being or have been dismantled by means of legally questionable tactics\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}, resulting in a large-scale theft of copyright-based intellectual property\footnote{http://swpat.ffii.org/conferences/2001/bundestag/kiesew/index.de.html} and a huge blood transfer from the software industry to the patent industry\footnote{http://swpat.ffii.org/papers/konno95/index.ja.html}.

I note that fences are being erected around discoveries, scientific theories and other fruits of basic research which can unfold their productive effects much better when they are allowed to enter the public domain through the normal channels of diffusion.

I am surprised to find that the patent system's history can be described as a victorious movement of lawyers against economists\footnote{http://swpat.ffii.org/papers/machlup58/index.en.html}, and that even today patent policy seems to be driven by the ideology of patent lawyers and large patent portfolio holders rather than by considerations of macro-economic efficiency\footnote{http://swpat.ffii.org/archive/mirror/impact.en.html}.

I urge decisionmakers at all levels to respect the value of the programmer's work, to defend all copyright-based property against any attempts to monopolise logical concepts, to subject all compositions of logical functionalities to copyright only, to uphold the protection of the public domain which is inherent in copyright, and in general to make sure that intellectual property privileges are granted only where they serve the best interests of technical, economic and social progress.
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
% mode: latex ;
% End: ;

