<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Iet: I am worried by the %(pe:uncontrolled expansion of the patent system)
into areas where it clearly does not help to %(us:promote the progress
of science and the useful arts).

#Irg: I note that former standards of patentability have been continuously
eroding, so that more and more patents of dubious merit are being
granted, and that governments are allowing their patent experts to
%(sp:make this trend irreversible by writing it into international
law).

#Ici: I note that patents are being granted for logical concepts, such as
mathematical methods, programming solutions and business ideas, and
that formerly clear doctrines on the limits of patentability are being
or have been dismantled by means of %(sk:legally questionable
tactics), resulting in a %(kk:large-scale theft of copyright-based
intellectual property) and a %(ko:huge blood transfer from the
software industry to the patent industry).

#Ifc: I note that fences are being erected around discoveries, scientific
theories and other fruits of basic research which can unfold their
productive effects much better when they are allowed to enter the
public domain through the normal channels of diffusion.

#Iva: I am surprised to find that the patent system's history can be
described as a %(fm:victorious movement of lawyers against
economists), and that even today patent policy seems to be driven by
the ideology of patent lawyers and large patent portfolio holders
rather than by %(me:considerations of macro-economic efficiency).

#Isn: I urge decisionmakers at all levels to respect the value of the
programmer's work, to defend all copyright-based property against any
attempts to monopolise logical concepts, to subject all compositions
of logical functionalities to copyright only, to uphold the protection
of the public domain which is inherent in copyright, and in general to
make sure that intellectual property privileges are granted only where
they serve the best interests of technical, economic and social
progress.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxdefl016 ;
# txtlang: en ;
# multlin: t ;
# End: ;

