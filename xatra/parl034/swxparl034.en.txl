<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII/Eurolinux 2003/04 Letter to Software Creators and Users

#descr: The European Parliament is likely to ratify a Software Patent
Directive, possibly with helpful amendments, in May.  As a software
creator/user, your participation can make a difference, even if you
are willing to spend less than 1 hour.  Here are a few simple and
effective things to do.

#rrh: Dear Software Users, Authors and Investors

#lWu: The European Parliament is %(dc:deciding) on a Software Patent
Directive (%(q:Directive on the Patentability of Computer-Implemented
Inventions)), possibly with helpful amendments, on September 23th.

#irW: Detailed news are found at %(URL)

#neh: As a stakeholder in the software economy and information society, you
have opportunities to participate.   Your participation can have a
decisive influence.

#Wwt: Here are a few things to do:

#psg: Endorse our positions by clicking %(URL) or, if you already have a
user ID, by %(ol|logging into the %(FCT)|navigating to %(q:Signing
Appeals)|navigating to %(q:membership status) and setting it to
%(q:supporter) or higher.)

#Etg: FFII/Eurolinux Community Tool

#eWp: Now you are logged into a system which makes it easy to select and
contact Members of the European Parliament (MEPs).

#lWE: Navigate through the menu selections %(SEQ)

#lne: selection criteria

#1WM: 1 MEP

#reW: Directly phone your MEP, express your concern about the Software
Patent Directive Project, find out who in this MEP's office is
studying the matter, how they think and whom they follow, and inform
us by filling in the %(q:Confidential Note) field.

#acf: It is very important that MEPs hear %(e:your) voice directly and not
just the voices of a few %(q:Eurolinux lobbyists).

#pdt: Write to the person with whom you just phoned.

#ato: Draft a position statement, send it to an %(sk:archived mailing list)
for discussion, publish it on your website.

#iea: Whatever you say and write, keep it simple, polite and
target-oriented.  Write from your perspective as a software
stakeholder.  Focus on the answer which you want to obtain from your
MEP: she must immediately understand what you want from her.  There
must be an easy way of answering you, and there must be no good excuse
for not answering.

#toi: Your argument could contain some of the following elements, expressed
in your own words:

#too: You want to create/use software without having to worry about patents.
  Explain what your stakes in software are, how you make money from
it, why software copyright works fine for you.

#edW: When looking at the software patents granted by the European Patent
Office (EPO, Europaeisches Patentamt EPA, Office Europeen de Brevets
OEB, Ufficio de l'Brevetto Europeo UBE, Oficina Europea de Patentes
OEP) at %(URLS) you find that they are broad and trivial and, if
enforced, likely to make life very difficult for software developpers
and businessmen (such as yourself --- possibly cite example patents
which are impeding or likely to impede your work).  None of these
patents, as far as you can see, is likely to stimulate innovation.  In
general, they offend the common sense and established ethics of
software developpers, and they do not protect investments in software
development.

#tgu: You have trusted in Art 52 EPC (European Patent Convention, Convention
sur le Brevet Europeen CBE, Europaeisches Patentuebereinkommen EPUe),
according to which %(q:programs for computers) (Programme fuer
Datenverarbeitungsanlagen, programmes pour ordinateurs) are not
patentable inventions (patentfaehige Erfindungen, inventions
brevetables), see also %(URL). You want the European Parliament to
restore the trust in the law which the EPO has recently undermined.

#rsi: Words such as %(q:computer-implemented invention) or %(q:technical
contribution), as used in the European Commission's directive
proposal, make little or no sense to you, the software expert. The
proposal says its wants %(q:clarification) but, at least for
stakeholders such as yourself, it makes things extremely unclear and
confusing. Does your MEP really understand this supposed
%(q:clarification)?

#Evc: %(q:Computer-implemented invention) (inventions mise en oevre par
ordinateur, computer-implementierte Erfindung) is a new word created
by patent lawyers in order to suggest that algorithms and programs
are, contrary to what is written in Art 52 EPC, patentable inventions.
You feel strongly offended by the title of the proposed %(q:Directive
on the Patentability of Computer-Implemented Inventions) and by the
EPO jargon that is used in the text.  The discussion should be led in
terms that you, the software expert, can understand, and that have an
unambiguous meaning in the context of your work.  The legislation
should be benchmarked according to interdisciplinary criteria such as
those worked out at %(URL)  How does the directive proposal perform
according to your MEP's opinion?  Could he/she please explain?

#ciW: You have read the a %(ju:tabular listing of the amendments to the
directive proposal which the %(tp|Legal Affairs Committee|JURI)
adopted and those which it rejected).  JURI rejected everything that
could have limited patentability and adopted amendments, including
so-called %(q:compromise amendments 1-6), which further extend
patentability beyond the Commission's proposal.  According to the JURI
proposal, algorithms and business methods presented in terms of
general-purpose computing equipment, such as Amazon One Click Shopping
or JPEG Runlength Coding, are indisputably patentable inventions.  The
requirement of %(q:technical contribution) is undefined and, as
documents %(a6:by the EPO itself) and %(wh:by famous patent lawyers)
show, not an obstacle at all.  The JURI text is full of deceptive
rhetoric.  Extentions are presented as restrictions, see e.g. the use
of the word %(q:only) in %(q:compromise amendment 1).  Proponents of
this text, such as %(LST), have consistently refused to explain its
meaning in terms of example patents.  Your MEP should insist on a
clear explanation, based on the %(pb:sample patent claims and
questions on our %(q:Patentability Legislation Tests) pamphlet). As
long the proponents fail to explain what in their proposal excludes
algorithms and business methods as found in the samples, it must be
assumed that they are trying to cheat fellow MEPs.  Being cheated by a
rapporteur is not a valid excuse for cheating voters.  The eyes of
%(ep:150,000 IT-literate citizens, including 2,000 IT employers), are
on your MEP.  They don't care who is the rapporteur.   They expect
their representative to stand up and defend the public interest.

#gWr: You support the %(CP) and urge your MEP to support it likewise (some
MEPs are already listed as signatories), and to table the %(PR)
proposed therein in time before the upcoming plenary session of
2003/09/01.

#mdn: Amendments

#ody: You propose to meet your MEP, possibly around one of our %(pp:events
in/near the Parliament), to which you can invite your MEP.

#WWp: In case of doubt, contact our helpdesk %(URL).  We may be able to give
you some advice on how to procede most effectively.

#oWt: Participate in our %(ep:events at the parliament), make an appointment
with your MEP to meet at such an occasion.

#nau: Donate for our lobbying expenses.  We are spending at least 30,000 EUR
on top of a lot of unpaid voluntary work, and we will not be able to
pay all our bills unless you come forward with donations.

#aoW: The bank account for a donation is %(BA)

#onr: Country

#aWd: Bank Code

#aoa: Name of Bank

#ktr: Bank's Postal Address

#con: Account

#raW: international bank account number

#Ami: We are told that, using the IBAN, money can now be transferred between
european countries for the domestic fee, thanks to a recent EU
internal market harmonisation directive which can be enforced against
banks.

#cnw: Account Owner

#usd: Account Owner's Postal Address

#ewr: Keyword

#adW: Alternatively you can donate via %(PW) to %(PM).

#WnW: FFII is acknowledged as a public-interest association and we can send
you a receipt that at least in Germany makes the donation
tax-deductible.

#gWe: It would be great if you could get started with at least one of the
above items right now.

#auW: Thank you very much

#uie: Yours Sincerely

#PFi: Hartmut Pilch on behalf of FFII e.V. and Eurolinux Alliance

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: en ;
# multlin: t ;
# End: ;

