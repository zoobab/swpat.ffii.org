<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII/Eurolinux 2003/04: Lettre aux Créateurs et Utilisateurs de
Logiciels

#descr: Le Parlament Européen va vraisemblablement adopter un propos de
directive sur le brevet logiciel, peut-être avec des amendment utiles.
 Comme créateurs et utilisateurs du logiciels, vous pouvez avoir une
influence décisive la dessus.  Ci-dessus nous vous disons ce que vous
pouvez faire en détail.

#rrh: Dear Software Users, Authors and Investors

#lWu: Le Parlement européen var %(dc:décider) sur une directive de brevetage
du logiciel (%(q:Directive sur la brévetabilité des inventions mises
en oevre par ordinateur)) le 23 Septembre.

#irW: Des informations détaillées peuvent être trouvées à %(URL)

#neh: En tant que participant au développement logiciel, vous avez des
opportunités de participation ; celle-ci peut avoir une influence
décisive.

#Wwt: Voici quelques-unes des choses à faire :

#psg: Signer nos propositions d'amendements encliquant ici : %(URL) ou si
vous avez déjà un identifiant, en vous connectant sur %(FCT) puis en
navigant dans %(q:Signing Appeals).

#Etg: Outil communautaire FFII/Eurolinux

#eWp: Vous êtes maintenant connecté dans un système qui vous permet de
sélectionner et contacter aisément les membres du parlement européen
(MEPs).

#lWE: Naviguez dans les options de menu %(SEQ)

#lne: critère de sélection

#1WM: 1 MEP

#reW: Appelez directement le MEP choisi, exprimez vos préoccupations autour
du projet de directive sur la brevetabilité logicielle, recherchez
qui, au sein du bureau de votre MEP, est en charge du sujet, ce qu'il
enpense et qui ils suivent, puis remontez-nous l'information en
remplissant le champ %(q:Confidential Note).

#acf: Il est très important que les MEP entendent directement %(e:vôtre)
voix et non juste celles de quelques %(q:lobbyistes Eurolinux).

#pdt: Écrivez à la personne à qui vous venez de parler.

#ato: Envoyez une déclaration de position a une %(sk:liste de diffusion
archivée), publiez la sur votre site web.

#iea: Restez simple. Écrivez de votre point de vue de développeur et laissez
à votre MEP le soin de tirer les conclusions politiques.  Concentrez
sur la reponse que vous attendez de la part de votre MEP: elle doit
clairement comprendre qu'est-ce que vous voulez qu'elle fasse.  Il
doit y avoir une façon simple de repondre et pas de bonne excuse pour
ne pas le faire.

#toi: Votre argumentaire peut contenir certains des éléments qui suivent,
exprimés avec vos propres termes :

#too: Vous pouvez souhaiter créer/utiliser des logiciels sans avoir à vous
préoccuper de brevets. Expliquez ce en quoi vous êtes impliqué dans le
logiciel, comment vous en tirez des revenus, pourquoi le droit
d'auteur (ou copyright selon le cas) sur le logiciel vous convient en
l'état.

#edW: Lorsque vous consultez les brevets logiciels délivrés par l'Office
Européen de Brevets à %(URLS) vous les trouvez généralistes et
grossiers et susceptibles de rendre la vie des développeurs et
entrepreneurs (tels que vous --- citez éventuellement des exemples de
brevets ayant un impact ou susceptibles d'en avoir sur votre travail).
Aucun de ces brevets, pour autant que vous puissiez savoir, n'est
susceptible de stimuler l'innovation. En général, ils contrarient le
sens commun et l'éthique des développeurs de logiciels et ne protègent
pas l'investissement dans le développement.

#tgu: Vous avez foi en l'article 52 de la CBE (Convention pour le Brevet
Européen), aux termes duquel les %(q:programmes pour ordinateurs) ne
sont pas des inventions brevetables, voir aussi %(URL) Vous voulez que
le parlement européen restaure la confiance en la loi qui a été
récemment entamée par l'OEB.

#rsi: Les termes tels que %(q:invention mise en oevre par ordinateur) ou
%(q:contribution technique), tels qu'ils sont utilisés dans la
proposition, ont peu ou pas de sens pour vous, l'expert logiciel. La
directive prétend à la %(q:clarification) mais, au moins pour les
parties prenantes telles que vous, rend les choses extrêmement peu
claires et confuses. Votre MEP comprend-il réellement cette prétendue
%(q:clarification)?

#Evc: %(q:Invention mise en oevre par ordinateur) est un néologisme créé par
les juristes en brevets afin de suggérer que les algorithmes et
programmes sont, contrairement à la formulation de l'article 52 de
l'EPC, des inventions brevetables. Vous vous sentez fortement agressé
par le titre de la proposition %(q:Directive sur la Brevetabilité des
Inventions Mises en Oevre Par Ordinateur) et par le jargon OEB utilisé
dans le texte. La discussion doit être menée en des termes que vous,
l'expert logiciels, puissiez comprendre et qui aient une signification
non ambiguë dans le contexte de votre travail. La législation doit
être contrôlée selon des critères interdiscipliaires tels que ceux
indiqués à %(URL).

#ciW: You have read the a %(ju:tabular listing of the amendments to the
directive proposal which the %(tp|Legal Affairs Committee|JURI)
adopted and those which it rejected).  JURI rejected everything that
could have limited patentability and adopted amendments, including
so-called %(q:compromise amendments 1-6), which further extend
patentability beyond the Commission's proposal.  According to the JURI
proposal, algorithms and business methods presented in terms of
general-purpose computing equipment, such as Amazon One Click Shopping
or JPEG Runlength Coding, are indisputably patentable inventions.  The
requirement of %(q:technical contribution) is undefined and, as
documents %(a6:by the EPO itself) and %(wh:by famous patent lawyers)
show, not an obstacle at all.  The JURI text is full of deceptive
rhetoric.  Extentions are presented as restrictions, see e.g. the use
of the word %(q:only) in %(q:compromise amendment 1).  Proponents of
this text, such as %(LST), have consistently refused to explain its
meaning in terms of example patents.  Your MEP should insist on a
clear explanation, based on the %(pb:sample patent claims and
questions on our %(q:Patentability Legislation Tests) pamphlet). As
long the proponents fail to explain what in their proposal excludes
algorithms and business methods as found in the samples, it must be
assumed that they are trying to cheat fellow MEPs.  Being cheated by a
rapporteur is not a valid excuse for cheating voters.  The eyes of
%(ep:150,000 IT-literate citizens, including 2,000 IT employers), are
on your MEP.  They don't care who is the rapporteur.   They expect
their representative to stand up and defend the public interest.

#gWr: Vous promouvez le %(CP) et demandez à votre MPE de faire de même
(certains MEP sont déjà inclus dans la liste des signataires) et de
planifier la %(PR) proposée suffisamment tôt avant le vote plénier
prévu 2003/09/01.

#mdn: Amendements

#ody: Vous proposez de rencontrer votre MEP, par exemple autour des
%(pp:événements dans ou auprès du Parlament), auxquels votre MEP est
convié.

#WWp: En cas de doute, contactez nôtre %(URL).  Nous pourrions peut-être
vous aider à procéder d'un mode éfficace.

#oWt: Participez dans nos %(ep:conférences dans le Parlament), prenez un
rendez-vous avec vôtre député lors d'une telle conférence!

#nau: Faites des donations pour nos dépenses de lobbying. Nous dépensons au
moins 30 000,00 EUR en plus d'une quantité de travail bénévole et ne
serons pas à même de payer toutes nos factures sauf si vous venez à
notre aide par des dons.

#aoW: Le compte bancaire pour les donations est %(BA).

#onr: Pays

#aWd: Code banque

#aoa: Nom de la banque

#ktr: Adresse postale de la banque

#con: Compte

#raW: IBAN (numéro international de compte bancaire)

#Ami: On nous a dit que, à l'aide de l'IBAN, les transferts bancaires au
sein de l'Europe sont au tarif domestique depuis une récente directive
d'harmonisation du marché interne de l'union européenne qui peut être
opposée aux banques.

#cnw: Titulaire du compte

#usd: Adresse postale du titulaire

#ewr: Mot-clef

#adW: Par ailleurs, vous pouvez effectuer vos dons par %(PW) à %(PM).

#WnW: FFII est reconnue comme association d'intérêt public et nous pouvons
vous expédier un recu qui, au moins en Allemagne, rend le don
déductible des impôts.

#gWe: Ce serait très bien si vous pouviez commencer au moins avec l'un des
éléments ci-dessus dès maintenant.

#auW: Merci Beaucoup

#uie: Yours Sincerely

#PFi: Hartmut Pilch pour FFII e.V. et Eurolinux

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

