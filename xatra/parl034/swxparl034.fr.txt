<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


descr: Le Parlement Européen va vraisemblablement adopter une directive sur le brevet logiciel, peut-être avec des amendements utiles, en mai.  En tant que créateurs ou utilisateurs de logiciels, votre participation peut faire la différence, même si vous souhaitez consacrer moins d'une d'heure.  Voici quelques choses simples et efficaces à faire.

title: FFII/Eurolinux 04/2003: Lettre aux créateurs et utilisateurs de  logiciels

rrh: Chers utilisateurs de logiciels, créateurs et investisseurs

lWu: Le Parlement Européen est en train de prendre une %(dc:décision)  sur une directive de brevetage du logiciel (%(q:Directive sur la  brevetabilité des inventions mises en oeuvre par ordinateur)),  probablement avec des amendements utiles le 23 septembre.

irW: Vous trouverez des informations détaillées sur %(URL)

neh: En tant que participant dans l'économie du logiciel et de la  société de l'information, vous avez des moyens d'agir. Votre  participation peut avoir une influence décisive.

Wwt: Voici quelques-unes des choses à faire:

psg: Signer nos propositions d'amendements en cliquant ici: %(URL) ou, si vous avez déjà un identifiant, %(ol|en vous connectant sur %(FCT)|en naviguant sur %(q:Signer des appels)|en naviguant sur %(q:statut du membre) et le passer à %(q:soutien) ou à un niveau plus élevé.)

Etg: Outil communautaire FFII/Eurolinux

eWp: Vous êtes maintenant connecté dans un système qui vous permet de  sélectionner et contacter aisément les membres du Parlement Européen (MPE).

lWE: Naviguez dans les options de menu %(SEQ)

lne: critère de sélection

1WM: un MPE

reW: Appelez directement votre MPE, exprimez vos préoccupations à propos  du projet de directive sur la brevetabilité logicielle, recherchez qui,  au sein du bureau de votre MPE, est en charge du sujet, ce qu'ils en  pensent et qui ils suivent, puis remontez-nous l'information en  remplissant le champ %(q:Note confidentielle).

acf: Il est très important que votre MPE entende directement %(e:votre)  voix et non juste celles de quelques %(q:lobbyistes Eurolinux).

pdt: Écrivez à la personne à qui vous venez de parler.

ato: Envoyez une déclaration de position à une %(sk:liste de diffusion  archivée), publiez-la sur votre site web.

iea: Restez simple, poli et ne vous écartez pas du sujet. Écrivez de votre point de vue de personne concernée par le sujet.  Concentrez-vous sur la réponse que vous attendez de la part de votre MPE: il doit clairement comprendre ce que vous attendez de lui.  Il doit y avoir une façon simple de répondre, et il ne doit pas y avoir de bonne excuse pour ne pas le faire.

toi: Votre argumentaire peut contenir certains des éléments qui suivent, exprimés avec vos propres mots :

too: Vous souhaitez créer/utiliser des logiciels sans avoir à vous  préoccuper de brevets. Expliquez ce en quoi vous êtes impliqué dans le  logiciel, comment vous en tirez des revenus, pourquoi le droit d'auteur  (ou copyright selon le cas) sur le logiciel vous convient en l'état.

edW: Lorsque vous consultez les brevets logiciels délivrés par l'Office Européen de Brevets sur %(URLS) vous les trouvez généralistes et grossiers et susceptibles de rendre la vie des développeurs et entrepreneurs (tels que vous --- citez éventuellement des exemples de brevets ayant un impact ou susceptibles d'en avoir sur votre travail) très difficile. Aucun de ces brevets, pour autant que vous puissiez le savoir, n'est susceptible de stimuler l'innovation. En général, ils contrarient le sens commun et l'éthique des développeurs de logiciels et ne protègent pas les investissements dans le développement du logiciel.

tgu: Vous avez foi en l'article 52 de la CBE (Convention sur le Brevet  Européen), selon lequel les %(q:programmes pour ordinateurs) ne sont pas  des inventions brevetables, voir aussi %(URL). Vous voulez que le  Parlement Européen restaure la confiance dans la loi qui a été récemment  sapée par l'OEB.

rsi: Les termes tels que %(q:invention mise en oeuvre par ordinateur) ou  %(q:contribution technique), tels qu'ils sont utilisés dans la  proposition de directive de la Commission Européenne, ont peu ou pas de  sens pour vous, l'expert logiciel. La proposition de directive prétend à  la %(q:clarification) mais, au moins pour les parties prenantes telles  que vous, rend les choses extrêmement peu claires et confuses. Votre MPE  comprend-il réellement cette prétendue %(q:clarification)?

Evc: %(q:Invention mise en oeuvre par ordinateur) est un néologisme créé par les juristes en brevets afin de suggérer que les algorithmes et programmes sont, contrairement à la formulation de l'article 52 de la CBE, des inventions brevetables. Vous vous sentez fortement agressé par le titre de la proposition %(q:Directive sur la brevetabilité des inventions mises en oeuvre par ordinateur) et par le jargon de l'OEB utilisé dans le texte. La discussion doit être menée en des termes que vous, l'expert logiciel, puissiez comprendre et qui aient une signification non ambiguë dans le contexte de votre travail. La législation doit être contrôlée selon des critères interdisciplinaires tels que ceux indiqués sur %(URL). Quelle est l'action de la proposition de directive selon votre MPE? Pourrait-il ou pourrait-elle vous l'expliquer?

ciW: Vous avez lu la %(ju:liste des amendements à la proposition de directive que le %(tp|Comité des Affaires Juridiques|JURI) a adoptés et ceux qu'il a rejetés). Le JURI a rejeté tout ce qui aurait pu limiter la brevetabilité et adopté des amendements, y compris les soi-disants %(q:amendements de compromis 1-6), qui étendront la brevetabilité au-delà de la proposition de la Commission.  Selon la proposition du JURI, les algorithmes et méthodes commerciales présentés en termes d'équipement informatique de portée générale, tel que Amazon One Click Shopping ou JPEG Runlength Coding, sont indiscutablement des inventions brevetables.  La condition de %(q:contribution technique) est indéfinie et, comme le montrent les documents %(a6:de l'OEB lui-même) et %(wh:d'avocats en brevets renommés), n'est pas un obstacle du tout.  Le texte du JURI est rempli d'une rhétorique trompeuse.  Les extensions sont présentées comme des restrictions, voir par exemple l'utilisation du mot %(q:seulement) dans l'%(q:amendement de compromis 1).  Les auteurs de ce texte, tels que %(LST), ont constamment refusé d'expliquer sa signification en termes d'exemples de brevets.  Votre MPE devrait insister sur une explication claire, fondée sur un %(pb:échantillon de revendications de brevet et des questions sur notre pamphlet %(q:Tests sur la législation de la brevetabilité)). Aussi longtemps que les auteurs échoueront à expliquer ce qui dans leur proposition exclut les algorithmes et les méthodes commerciales comme ceux trouvés dans les échantillons de brevets, on doit supposer qu'ils essaient de tromper leurs collègues MPE.  Être trompé par un rapporteur n'est pas une excuse valable pour tromper les électeurs.  Les yeux de %(ep:cent cinquante mille citoyens rompus aux technologies de l'information, y compris deux mille chefs d'entreprise dans les technologies de l'information), sont sur votre MPE.  Ils ne veulent pas savoir qui est le rapporteur.   Ils attendent de leur représentant qu'il se déclare et défe

gWr: Vous soutenez le %(CP) et pressez votre MPE de faire de même  (certains MPE sont déjà inclus dans la liste des signataires) et de  planifier la %(PR) proposée suffisamment tôt avant le vote en séance  plénière prévu le 01/09/2003.

mdn: Amendements

ody: Vous proposez de rencontrer votre MPE, par exemple autour des  %(pp:événements à l'intérieur ou aux abords du Parlement), auxquels vous  pouvez convier votre MPE.

WWp: En cas de doute, contactez %(URL).  Nous pourrions peut-être vous conseiller sur la façon de procéder la plus efficace.

oWt: Participez à nos %(ep:conférences au Parlement), prenez un rendez-vous avec votre député pour vous rencontrer en de telles occasions.

nau: Faites des donations pour nos dépenses de lobbying. Nous dépensons  au moins trente mille euro sans compter la grande part de travail  bénévole et ne serons pas à même de payer toutes nos factures sauf si  vous venez à notre aide par des dons.

aoW: Le compte bancaire pour les donations est %(BA).

onr: Pays

aWd: Code banque

aoa: Nom de la banque

ktr: Adresse postale de la banque

con: Compte

raW: IBAN (numéro international de compte bancaire)

Ami: On nous a dit que, à l'aide de l'IBAN, les transferts bancaires au  sein de l'Europe sont au tarif domestique depuis une récente directive  d'harmonisation du marché interne de l'union européenne qui peut être  opposée aux banques.

cnw: Titulaire du compte

usd: Adresse postale du titulaire

ewr: Mot-clé

adW: Par ailleurs, vous pouvez effectuer vos dons par %(PW) à %(PM).

WnW: La FFII est reconnue comme association d'intérêt public et nous  pouvons vous expédier un reçu qui, au moins en Allemagne, rend le don  déductible des impôts.

gWe: Ce serait très bien si vous pouviez commencer au moins avec l'un  des éléments ci-dessus dès maintenant.

auW: Merci Beaucoup

uie: Veuillez agréer l'expression de mes sentiments les meilleurs

PFi: Hartmut Pilch pour FFII e.V. et Eurolinux

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

