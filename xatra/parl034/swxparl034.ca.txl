<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII/Eurolinux 2003/04 Letter to Software Creators and Users

#descr: The European Parliament is likely to ratify a Software Patent
Directive, possibly with helpful amendments, in May.  As a software
creator/user, your participation can make a difference, even if you
are willing to spend less than 1 hour.  Here are a few simple and
effective things to do.

#rrh: Dear Software Users, Authors and Investors

#lWu: El Parlament Europeu sembla que ratificarà una directiva de patents de
programari (%(q:Directiva sobre la Patentabilitat d'invencions
implementades a ordinadors)), possiblement amb esmenes que ajudaran,
el maig de 2003.

#irW: Hi ha notícies detallades a %(URL)

#neh: Com a part interessada del desenvolupament del programari, teniu
oportunitats de participar-hi.  La vostra participació pot tenir una
influència decisiva.

#Wwt: Aquí hi ha unes poques coses per a fer:

#psg: Confirmeu les nostres postures clicant %(URL) o, si ja teniu una
identificació d'usuari, %(ol|entrant al %(FCT)|navegant a les
%(q:apel·lacions signades)|navegant a l' %(q:estatus dels membres) i
configurar-lo a %(q:supporter) o superior.)

#Etg: Eina de la Comunitat FFII/Eurolinux

#eWp: Ara heu entrat el sistema que fa més fàcil seleccionar i contactar
Membres del Parlament Europeu (MPEs).

#lWE: Navegar per les seleccions de menú %(SEQ)

#lne: criteri de selecció

#1WM: 1 MPE

#reW: Telefoneu directament el vostre MPE, expresseu el que penseu del
Projecte de Directiva Europea de Patents de Programari, trobeu qui
estudia el tema a l'oficina del MPE, com pensen i a qui segueixen, i
informeu-nos omplint el camp %(q:Nota Confidencial).

#acf: És molt important que els MPEs escoltin %(e:la vostra) veu directament
i no només les veus d'uns pocs %(q:col·legues d'Eurolinux).

#pdt: Escribiu a la persona amb qui heu parlat per telèfon.

#ato: Considereu també publicar una declaració de postures a la vostra
pàgina web.

#iea: Feu-ho de manera simple.  Escriviu des de la vostra perspectiva com a
part interessada del programari, deixa al teu MPE pensar les
conclusions morals i polítiques.

#toi: El vostre argument pot incloure alguns dels següents elements,
expressats amb les vostres pròpies paraules:

#too: Voleu crear/usar programari sense haver-vos de preocupar per les
patents.   Expliqueu quins són els vostres interessos en el
programari, com feu diners amb ell, per què els drets d'autor del
programari van bé per a vostè

#edW: Si mireu les patents de programari concedides per l'Oficina Europea de
Patents (OEP, European Patent Office EPO, Europaeisches Patentamt EPA,
Office Europeen de Brevets OEB, Ufficio de l'Brevetto Europeo UBE,
Oficina Europea de Patentes OEP) a %(URLS) trobareu que són amples i
trivials i que, si es forcen, sembla que complicarien la vida als
desenvolupadors de programari i homes de negoci (com vostè mateix ---
podríeu citar exemples de patents que impedeixen o sembla que
impedirien la vostra feina).  Cap d'aquestes patents, tal com ho
veieu, sembla que estimuli la innovació.  En general, ofenen el sentit
comú i l'ètica establerta dels desenvolupadors de programari, i no
protexeixen les inversions al desenvolupament del programari.

#tgu: Heu estudiat l'Art 52 CEP (Convenció Europea de Patents, European
Patent Convention EPC, Convention sur le Brevet Europeen CBE,
Europaeisches Patentuebereinkommen EPUe), segons el qual %(q:els
programes per a ordinadors) (Programs for computers, Programme fuer
Datenverarbeitungsanlagen, programmes pour ordinateurs) no són invents
patentables (patentable inventions, patentfaehige Erfindungen,
inventions brevetables), veieu també %(URL) Voleu que el Parlament
Europeu restauri la legalitat que l'OEP ha minat recentment.

#rsi: Paraules com ara %(q:invenció implementada per ordinadors) o
%(q:contribució tècnica), com es fan servir a la proposta de directiva
de la Comissió Europea, tenen poc o cap sentit per a vostè, l'expert
en programari. La proposta diu que vol %(q:clarificació) però, almenys
per a les parts implicades com vostè mateix, fa les coses
extremadament difuses i confuses. Entén realment el vostre MPE aquesta
suposada %(q:clarificació)?

#Evc: La %(q:Invenció implementada per ordinadors) (Computer-implemented
invention, inventions mise en oevre par ordinateur,
computer-implementierte Erfindung) és un nou concepte creat per
advocats de patents per tal de suggerir que els algoritmes i els
programes són, contràriament al que està escrit a l'Art 52 EPC,
invencions patentables. Us sentiu fortament ofesos pel títol de la
proposta %(q:Directiva sobre la Patentabilitat d'Invencions
Implementades per Ordinadors) i per l'argot de l'OEP que es fa servir
al text.  La discussió s'ha de dirigir en termes que vostè, l'expert
en programari, pugui entendre, i que no tingui significats ambigus en
el context de la vostra feina.  La legislació s'ha de provar d'acord
amb criteris interdisciplinars com els que funcionaven a %(URL)  Com
funciona la proposta de directiva segons l'opinió del vostre MPE?  Es
podria explicar, sisplau?

#ciW: You have read the a %(ju:tabular listing of the amendments to the
directive proposal which the %(tp|Legal Affairs Committee|JURI)
adopted and those which it rejected).  JURI rejected everything that
could have limited patentability and adopted amendments, including
so-called %(q:compromise amendments 1-6), which further extend
patentability beyond the Commission's proposal.  According to the JURI
proposal, algorithms and business methods presented in terms of
general-purpose computing equipment, such as Amazon One Click Shopping
or JPEG Runlength Coding, are indisputably patentable inventions.  The
requirement of %(q:technical contribution) is undefined and, as
documents %(a6:by the EPO itself) and %(wh:by famous patent lawyers)
show, not an obstacle at all.  The JURI text is full of deceptive
rhetoric.  Extentions are presented as restrictions, see e.g. the use
of the word %(q:only) in %(q:compromise amendment 1).  Proponents of
this text, such as %(LST), have consistently refused to explain its
meaning in terms of example patents.  Your MEP should insist on a
clear explanation, based on the %(pb:sample patent claims and
questions on our %(q:Patentability Legislation Tests) pamphlet). As
long the proponents fail to explain what in their proposal excludes
algorithms and business methods as found in the samples, it must be
assumed that they are trying to cheat fellow MEPs.  Being cheated by a
rapporteur is not a valid excuse for cheating voters.  The eyes of
%(ep:150,000 IT-literate citizens, including 2,000 IT employers), are
on your MEP.  They don't care who is the rapporteur.   They expect
their representative to stand up and defend the public interest.

#gWr: Doneu suport al %(CP) i demaneu urgentment el vostre MPE a donar-li
suport de la mateixa manera (alguns MPEs estan llistats com a
signataris), i postposar la %(PR) proposta abans de la propera votació
de 2003/09/01.

#mdn: Esmenes

#ody: Proposeu reunir-vos amb el vostre MPE, possiblement durant els events
del 7 i 8 de maig, als quals el vostre MPE està convidat.

#WWp: In case of doubt, contact our helpdesk %(URL).  We may be able to give
you some advice on how to procede most effectively.

#oWt: Participate in our %(ep:events at the parliament), make an appointment
with your MEP to meet at such an occasion.

#nau: Doneu per a les despeses de col·laboració.  Gastem almenys 30 000 EUR
a més de molta feina no pagada dels voluntaris, i no podrem pagar
totes les factures si ni ajudeu amb donacions.

#aoW: El compte bancari per a fer una donació és %(BA)

#onr: País

#aWd: Codi del banc

#aoa: Nom del banc

#ktr: Adreça postal del Banc

#con: Compte

#raW: número internacional de compte corrent

#Ami: Ens han dit que, fent servir l'IBAN, els diners es poden ara
transferir entre països europeus amb la comissió local, gràcies a una
recent directiva europea d'harmonització del mercat intern, d'obligat
compliment per als bancs.

#cnw: Propietari del compte

#usd: Adreça postal del propietari del compte

#ewr: Paraula clau

#adW: Alternativament, podeu donar via %(PW) a %(PM).

#WnW: L'FFII està reconeguda com a associació d'interès públic i us podem
enviar una factura que, almenys a Alemània, fa la donació deduïble.

#gWe: Seria fantàstic si poguéssiu començar almenys per un d'aquests ítems
ara mateix.

#auW: Moltes gràcies

#uie: Yours Sincerely

#PFi: Hartmut Pilch on behalf of FFII e.V. and Eurolinux Alliance

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: ca ;
# multlin: t ;
# End: ;

