<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII/Eurolinux 2003/04: Lettera a los Interesados en Software

#descr: The European Parliament is likely to ratify a Software Patent
Directive, possibly with helpful amendments, in May.  As a software
creator/user, your participation can make a difference, even if you
are willing to spend less than 1 hour.  Here are a few simple and
effective things to do.

#rrh: Dear Software Users, Authors and Investors

#lWu: El parlamento europeo es partidario de ratificar una Directiva de
Patentes de Software (%(q:Directiva de la Patentabilidad de
Invenciones Implementables-Ordenador)), posiblemente con enmiendas
útiles, en Mayo del 2003.

#irW: Noticias detalladas se encuentran en %(URL)

#neh: Como una parte interesada en el desarrollo de software, tienes la
oportunidad de participar. Tu participación puede tener una influencia
decisiva.

#Wwt: Quedan unas pocas cosas pendientes a hacer:

#psg: Firme nuestra propuesta de Enmienda haciendo click en %(URL) o, si ya
posee un ID de usuario, identificandose en el %(FCT) y luego navegando
mediante %(q:Petición de Firmas).

#Etg: FFII/Eurolinux Community Tool

#eWp: Ahora está identificado en un sistema que permite fácilmente
seleccionar y contactar con Miembros del Parlamento Europeo (MPEs).

#lWE: Navegar mediante el menú de selección %(SEQ)

#lne: criterio de selección

#1WM: 1 MPE

#reW: Llama telefónicamente a tu MPE, expresa tu preocupación sobre el
proyecto de Directiva de Patente de Software, averigua quién está
estudiando el tema en su oficina, cómo piensa y quiénes le apoyan, e
infórmanos rellenando en el campo %(q: Nota Confidencial).

#acf: Es muy importante que los MPEs oigan %e(e:tu) voz directamente y no
únicamente las voces de unos pocos %(q:grupo Eurolinux).

#pdt: Escribe a la persona con la que hayas hablado telefónicamente.

#ato: También considera publicar una declaración de principios en tu página
web.

#iea: Mantenlo simple. Escribe desde tu perspectiva como una persona que
apuesta por el software, dejando a tu MPE que extraiga las
conclusiones políticas.

#toi: Tus razonamientos deben de contener alguno de los siguientes
elementos, expresado con tus propias palabas:

#too: Quieres crear/usar software sin tener que preocuparte sobre las
patentes. Explica cuáles son tus apuestas en el software, cómo
obtienes dinero de el, y porqué los derechos de autor (copyright) de
software funciona bien para tí.

#edW: Cuando observas las patentes de software concedidas por la  Oficina
Europea de Patentes (OEP) en %(URLS) ves que son amplias y triviales
y, si se imponen, probablemente dificultarán de los desarrolladores de
software y los empresarios (como tú mismo, cita tal vez ejemplos de
patentes que obstaculizan o podrían obstaculizar tu trabajo). Ninguna
de esas patentes, hasta donde llegas a ver, parece estimular la
innovación. En general, ofenden al sentido común y a los fundamentos
éticos de los desarrolladores de software, y no protegen a los
inventos en el desarrollo de software.

#tgu: Has confiado en el artículo 52 de la Convención Europea de Patentes
(CEP), según el cual los %(q:programas de ordenador) no son inventos
patentables (ver también %(URL)). Quieres que el Parlamento Europeo
restituya la confianza en la ley que ha socavado la OEP.

#rsi: Expresiones como %(q:invención implementable-ordenador) o
%(q:contribución técnica), que son utilizadas en la propuesta, tienen
poco o ningún sentido para tí, el experto en software. La directiva
dice que quiere una %(q:clarificación) pero, al menos para partes
interesadas como tú, deja las cosas poco claras y confusas. ¿Tu MPE
realmente entiende esta supuesta %(q:clarificación)?

#Evc: %(q:Invención implementable mediante ordenador) es un nuevo término
creado por los abogados de patentes para sugerir que los algoritmos y
los programas son, contrariamente a lo escrito en el artículo 52 de la
CEP, invenciones patentables. Te sientes fuertemente ofendido por el
título de la propuesta %(q:Directiva sobre la patentabilidad de las
invenciones implementables mediante ordenador) y por la jerga de la
OEP que se utiliza en el texto. La discusión debería ser conducida de
modo que tú, el experto en software, puedas entenderla, y que tenga un
significado preciso en el contexto de tu trabajo. La legislación
debiera estar redactada en función de criterios interdisciplinares
como los descritos  en %(URL).

#ciW: You have read the a %(ju:tabular listing of the amendments to the
directive proposal which the %(tp|Legal Affairs Committee|JURI)
adopted and those which it rejected).  JURI rejected everything that
could have limited patentability and adopted amendments, including
so-called %(q:compromise amendments 1-6), which further extend
patentability beyond the Commission's proposal.  According to the JURI
proposal, algorithms and business methods presented in terms of
general-purpose computing equipment, such as Amazon One Click Shopping
or JPEG Runlength Coding, are indisputably patentable inventions.  The
requirement of %(q:technical contribution) is undefined and, as
documents %(a6:by the EPO itself) and %(wh:by famous patent lawyers)
show, not an obstacle at all.  The JURI text is full of deceptive
rhetoric.  Extentions are presented as restrictions, see e.g. the use
of the word %(q:only) in %(q:compromise amendment 1).  Proponents of
this text, such as %(LST), have consistently refused to explain its
meaning in terms of example patents.  Your MEP should insist on a
clear explanation, based on the %(pb:sample patent claims and
questions on our %(q:Patentability Legislation Tests) pamphlet). As
long the proponents fail to explain what in their proposal excludes
algorithms and business methods as found in the samples, it must be
assumed that they are trying to cheat fellow MEPs.  Being cheated by a
rapporteur is not a valid excuse for cheating voters.  The eyes of
%(ep:150,000 IT-literate citizens, including 2,000 IT employers), are
on your MEP.  They don't care who is the rapporteur.   They expect
their representative to stand up and defend the public interest.

#gWr: Apoya la %(CP) e invita a tu MPE a que lo apoye igualmente (algunos
MPE yan aparecen en una lista como firmantes),  y para poner sobre la
mesa el %(PR) propuesto con tiempo antes de la votación de 2003/09/01.

#mdn: Enmiendas

#ody: Trata de conocer a tu MPE, posiblemente en las actividades del 7 y 8
de mayo, a las cuales tu MPE está invitado.

#WWp: In case of doubt, contact our helpdesk %(URL).  We may be able to give
you some advice on how to procede most effectively.

#oWt: Participate in our %(ep:events at the parliament), make an appointment
with your MEP to meet at such an occasion.

#nau: Has una donación para nuestros gastos de representación. Estamos
gastando al menos 30,000 EUR más mucho trabajo desinteresado de
voluntarios, y no seremos capaces de pagar nuestras facturas a menos
que nos envíes donaciones.

#aoW: La cuenta bancaria para una donación es %(BA).

#onr: País

#aWd: Código del Banco

#aoa: Nombre del Banco

#ktr: Dirección Postal del Banco

#con: Cuenta

#raW: número de cuenta de banco internacional

#Ami: Se nos ha comunicado que, mediante el IBAN, el dinero puede ahora ser
transferido entre países europeos con los mismos costes que una
transferencia dentro del país, gracias a una reciente directiva de
armonización interna que es de obligado cumplimiento para los bancos.

#cnw: Dueño de la cuenta

#usd: Dirección Postal del dueño de la cuenta.

#ewr: Palabra clave

#adW: También puedes donar mediante %(PW) o %(PM).

#WnW: FFII es reconocida como una asociación de interés público, y podemos
enviarte un recibo que, al menos en Alemania, permite que la donación
sirva para deducir impuestos.

#gWe: Sería genial si pudieses empezar ya mismo con al menos uno de los
temas mencionado.

#auW: Muchas gracias

#uie: Con saludos

#PFi: Hartmut Pilch por FFII e.V. y Eurolinux

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: es ;
# multlin: t ;
# End: ;

