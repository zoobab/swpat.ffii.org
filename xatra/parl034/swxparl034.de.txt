<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Das Europäische Parlament wird wahrscheinlich eine Softwarepatentrichtlinie verabschieden, möglicherweise mit hilfreichen Änderungen.  Als Betroffener können Sie schon viel bewegen, wenn Sie sich eine Stunde Zeit nehmen.  Wir sagen Ihnen, was Sie tun können, und machen Ihnen es so leicht wie möglich.
title: FFII/Eurolinux 2003/04 Brief an Software-Urheber und -Anwender
rrh: Liebe Software-Anwender, -Autoren und -Investoren,
lWu: Das Europäische Parlament wird am 23. September über eine Softwarepatentrichtlinie (%(q:Richtlinie über die Patentierbarkeit computer-implementierter Erfindungen)) %(dc:entscheiden), möglicherweise mit hilfreichen Änderungen.
irW: Ausführliche Nachrichten dazu finden sich unter %(URL)
neh: Als Teilhaber der Software-Ökonomie und Informationsgesellschaft haben Sie bedeutende Möglichkeiten der Einflussnahme.
Wwt: Folgendes könnten Sie tun:
psg: Unterstützen Sie unsere Positionen, indem Sie %(URL) anwählen oder, falls Sie bereits eine Nutzerkennung haben, indem Sie %(ol|sich in %(FCT) einloggen|Appelle unterzeichnen|Ihren Status auf %(q:Unterstützer) oder höher setzten.)
Etg: FFII/Eurolinux Mitwirkungs-System
eWp: Jetzt sind Sie in ein System eingeloggt, welches es Ihnen leicht macht, Mitglieder des Europäischen Parlaments (MdEP) zu finden und zu kontaktieren.
lWE: Navigieren Sie durch die Menüpunkte %(SEQ)
lne: Auswahlkriterien
1WM: 1 MdEP
reW: Rufen Sie Ihr MdEP gleich an, drücken Sie Ihre Sorge über die (von Arlene McCarthy vorangetriebene) Softwarepatentrichinie aus, finden Sie heraus, wer im Büro Ihres MdEP sich für diese Sache interessiert, wie man dort denkt und wem man folgt.  Informieren Sie uns, indem Sie das Feld %(q:Vertraulicher Hinweis) ausfüllen.
acf: Es kommt entscheidend darauf an, dass Ihr MdEP unmittelbar %(e:Ihre) Stimme hört und nicht etwa nur die Stimmen einiger %(q:Eurolinux-Lobbyisten).
pdt: Schreiben Sie die Person an, mit der Sie gerade telefoniert haben.
ato: Entwerfen Sie ein Positionspapier, stellen Sie es in einem %(sk:archivierten Verteiler) zur Diskussion, veröffentlichen es im im Web.
iea: Schreiben Sie einfach, höflich und zielgerichtet.  Schreiben Sie aus Ihrer Perspektive als Betroffener.  Konzentrieren Sie sich auf die Antwort, die Sie von Ihrer Abgeordneten erhalten möchten: sie muss sofort verstehen, was Sie von ihr wollen.  Es muss leicht sein, Ihnen zu antworten, und es darf keinen guten Vorwand geben, dies nicht zu tun.
toi: Ihre Argumentation könnte einige der folgenden Elemente, in Ihren Worten gesagt, enthalten:
too: Sie möchten Software schreiben/einsetzen, ohne sich um Patente sorgen zu müssen.  Erklären Sie, wie Software mit Ihren Interessen verbunden ist, wie Sie ggf damit Geld verdienen, warum das ohne Patente (mit Urheberrecht) gut funktioniert.
edW: Wenn Sie sich die vom Europäischen Patentamt (EPA) erteilten Softwarepatente auf %(URLS) anschauen, stellen Sie fest, dass diese Patente breit und trivial sind und, falls damit Ernst gemacht würde, Software-Entwicklern und -Firmen das Leben erschweren könnten -- führen Sie ggf Beispielpatente aus Ihrem Bereich an.  Soweit Sie erkennen können, sind Schutzrechte dieser Art sittenwidrig (widersprechen dem ethischen Konsens der Branche) und nicht geeignet, Investitionen in Softwareentwicklung zu schützen.  Vielmehr sind sie geeignet, solche Investitionen riskant zu machen.  
tgu: Sie haben auf Art 52 EPÜ (Europäisches Patentübereinkommen) vertraut, demzufolge %(e:Programme für Datenverarbeitungsanlagen) nicht zu den %(e:patentfähigen Erfindungen) zählen, s. auch %(URL).  Sie wünschen, dass das Europäische Parlament das Vertrauen in das Gesetz wiederherstellt, welches das EPA in letzter Zeit untergraben hat.
rsi: Worte wie %(q:computer-implementierte Erfindung) oder %(q:technischer Beitrag), wie sie in dem Vorschlag verwendet werden, ergeben für Sie, den Software-Experten, wenig Sinn.  Die Richtlinie soll %(q:Klärung) bringen, aber zumindest für Betroffene wie Sie macht sie die Dinge nur sehr unklar und verwirrend.  Versteht Ihr MdEP wirklich diese vermeintliche %(q:Klärung)?
Evc: Der Begriff %(q:Computer-implementierte Erfindung) ist dem Fachmann nicht geläufig.  Er wurde erst kürzlich von Patentjuristen geschaffen, um den Eindruck zu erwecken, dass Programme für Datenverarbeitungsanlagen, abweichend vom Wortlaut des Art 52 EPÜ, patentfähige Erfindungen sind.  Sie empfinden den Titel der vorgeschlagenen %(q:Richtlinie über die Patentierbarkeit computer-implementierter Erfindungen) und den im Text verwendeten EPA-Jargon als einen Schlag ins Gesicht.  Die Diskussion sollte in Begriffen geführt werden, welche Sie, der Software-Experte, verstehen können, und welche im Zusammenhang Ihrer Arbeit einen eindeutigen Sinn ergeben.  Die Gesetzgebung sollte an interdisziplinären Kriterien wie den in %(URL) erarbeiteten gemessen werden.  Wie schneidet der Entwurf nach Meinung Ihres MdEP ab?  Bitte um Erklärung.
ciW: You have read the a %(ju:tabular listing of the amendments to the directive proposal which the %(tp|Legal Affairs Committee|JURI) adopted and those which it rejected).  JURI rejected everything that could have limited patentability and adopted amendments, including so-called %(q:compromise amendments 1-6), which further extend patentability beyond the Commission's proposal.  According to the JURI proposal, algorithms and business methods presented in terms of general-purpose computing equipment, such as Amazon One Click Shopping or JPEG Runlength Coding, are indisputably patentable inventions.  The requirement of %(q:technical contribution) is undefined and, as documents %(a6:by the EPO itself) and %(wh:by famous patent lawyers) show, not an obstacle at all.  The JURI text is full of deceptive rhetoric.  Extentions are presented as restrictions, see e.g. the use of the word %(q:only) in %(q:compromise amendment 1).  Proponents of this text, such as %(LST), have consistently refused to explain its meaning in terms of example patents.  Your MEP should insist on a clear explanation, based on the %(pb:sample patent claims and questions on our %(q:Patentability Legislation Tests) pamphlet). As long the proponents fail to explain what in their proposal excludes algorithms and business methods as found in the samples, it must be assumed that they are trying to cheat fellow MEPs.  Being cheated by a rapporteur is not a valid excuse for cheating voters.  The eyes of %(ep:150,000 IT-literate citizens, including 2,000 IT employers), are on your MEP.  They don't care who is the rapporteur.   They expect their representative to stand up and defend the public interest.
gWr: Sie unterstützen den %(CP) und bitten Ihr MdEP, ihn ebenfalls zu unterstützen -- einige MdEP sind bereits als Unterzeichner aufgelistet, und die darin vorgeschlagenen %(PR) rechtzeitig vor der Plenarsitzung 2003/09/01 einzureichen.
mdn: Änderungsvorschläge
ody: Sie möchten Ihr MdEP treffen, möglicherweise im Zusammenhang mit einer unserer %(pp:Veranstaltungen im/am Parlament), zu denen Sie Ihr MdEP einladen können.
WWp: Im Zweifelsfalle kontaktieren Sie %(URL).  Wir können Ihnen vielleicht helfen, wirkungsvoll vorzugehen.
oWt: Nehmen Sie an unseren %(ep:Veranstaltungen im Parlament) teil, vereinbaren Sie mit Ihrem Abgeordneten ein Treffen anlässlich einer solchen Veranstaltung!
nau: Spenden Sie für unsere Ausgaben.  Wir geben mindestens 30000 eur zuzüglich allerlei unbezahlter Arbeit aus, und wir werden unsere Rechnungen nur dann volllständig bezahlen können, wenn sich viele Leute großzügig zeigen.
aoW: Das Bankkonto für eine Spende ist %(BA)
onr: Land
aWd: Bankleitzahl
aoa: Name der Bank
ktr: Anschrift der Bank
con: Konto
raW: Internationale Kontonummer
Ami: Es soll möglich sein, mithilfe der IBAN zur inländischen Gebühr aus dem euorpäischen Ausland zu überweisen.  Hierzu werden die Banken durch eine EU-Richtlinie gezwungen.
cnw: Kontoinhaber
usd: Anschrift des Kontoinhabers
ewr: Verwendungszweck
adW: Alternativ können Sie auch über %(PW) an %(PM) zahlen.
WnW: Der FFII ist als ein gemeinnütziger Verein anerkannt, und wir können Ihnen eine Quittung schicken, die zumindest in Deutschland die Spende steuerlich absetzbar macht.
gWe: Es wäre sehr schön, wenn Sie mit einem der obigen Punkte jetzt gleich beginnen könnten.
auW: Vielen Dank
uie: Mit freundlichen Grüßen
PFi: Hartmut Pilch für FFII e.V. und Eurolinux

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxparl034 ;
# txtlang: de ;
# End: ;

