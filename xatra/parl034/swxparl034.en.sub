\begin{subdocument}{swxparl034}{FFII/Eurolinux 2003/04 Letter to Software Creators and Users}{http://swpat.ffii.org/xatra/parl034/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{The European Parliament is likely to ratify a Software Patent Directive, possibly with helpful amendments, in May.  As a software creator/user, your participation can make a difference, even if you are willing to spend less than 1 hour.  Here are a few simple and effective things to do.}
Dear Software Users, Authors and Investors

The European Parliament is deciding\footnote{\url{}} on a Software Patent Directive (``Directive on the Patentability of Computer-Implemented Inventions''), possibly with helpful amendments, on September 23th.

Detailed news are found at \begin{quote}
FFII: Logic Patents in Europe\footnote{\url{http://localhost/swpat/index.en.html}}
\end{quote}

As a stakeholder in the software economy and information society, you have opportunities to participate.   Your participation can have a decisive influence.

Here are a few things to do:

\begin{enumerate}
\item
Endorse our positions by clicking \begin{quote}
\url{http://aktiv.ffii.org/eubsa/en}
\end{quote} or, if you already have a user ID, by \begin{enumerate}
\item
logging into the \begin{quote}
FFII/Eurolinux Community Tool\footnote{\url{http://aktiv.ffii.org/}}
\end{quote}

\item
navigating to ``Signing Appeals''

\item
navigating to ``membership status'' and setting it to ``supporter'' or higher.
\end{enumerate}

\item
Now you are logged into a system which makes it easy to select and contact Members of the European Parliament (MEPs).

Navigate through the menu selections \begin{quote}
\begin{itemize}
\item
My MEPs

\item
I would rather select an MEP myself

\item
[ selection criteria ]

\item
[ 1 MEP ]
\end{itemize}
\end{quote}

\item
Directly phone your MEP, express your concern about the Software Patent Directive Project, find out who in this MEP's office is studying the matter, how they think and whom they follow, and inform us by filling in the ``Confidential Note'' field.

It is very important that MEPs hear \emph{your} voice directly and not just the voices of a few ``Eurolinux lobbyists''.

\item
Write to the person with whom you just phoned.

Draft a position statement, send it to an archived mailing list\footnote{\url{http://localhost/swpat/lisri/krasi/index.en.html}} for discussion, publish it on your website.

Whatever you say and write, keep it simple, polite and target-oriented.  Write from your perspective as a software stakeholder.  Focus on the answer which you want to obtain from your MEP: she must immediately understand what you want from her.  There must be an easy way of answering you, and there must be no good excuse for not answering.\footnote{Your argument could contain some of the following elements, expressed in your own words:
\begin{enumerate}
\item
You want to create/use software without having to worry about patents.   Explain what your stakes in software are, how you make money from it, why software copyright works fine for you.

\item
When looking at the software patents granted by the European Patent Office (EPO, Europaeisches Patentamt EPA, Office Europeen de Brevets OEB, Ufficio de l'Brevetto Europeo UBE, Oficina Europea de Patentes OEP) at \begin{quote}
\url{http://www.espacenet.com/}\\
\url{http://swpat.ffii.org/patents/txt/ep/}
\end{quote} you find that they are broad and trivial and, if enforced, likely to make life very difficult for software developpers and businessmen (such as yourself --- possibly cite example patents which are impeding or likely to impede your work).  None of these patents, as far as you can see, is likely to stimulate innovation.  In general, they offend the common sense and established ethics of software developpers, and they do not protect investments in software development.

\item
You have trusted in Art 52 EPC (European Patent Convention, Convention sur le Brevet Europeen CBE, Europaeisches Patentuebereinkommen EPUe), according to which ``programs for computers'' (Programme fuer Datenverarbeitungsanlagen, programmes pour ordinateurs) are not patentable inventions (patentfaehige Erfindungen, inventions brevetables), see also \begin{quote}
Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{\url{http://localhost/swpat/stidi/korcu/index.en.html}}
\end{quote}. You want the European Parliament to restore the trust in the law which the EPO has recently undermined.

\item
Words such as ``computer-implemented invention'' or ``technical contribution'', as used in the European Commission's directive proposal, make little or no sense to you, the software expert. The proposal says its wants ``clarification'' but, at least for stakeholders such as yourself, it makes things extremely unclear and confusing. Does your MEP really understand this supposed ``clarification''?

``Computer-implemented invention'' (inventions mise en oevre par ordinateur, computer-implementierte Erfindung) is a new word created by patent lawyers in order to suggest that algorithms and programs are, contrary to what is written in Art 52 EPC, patentable inventions. You feel strongly offended by the title of the proposed ``Directive on the Patentability of Computer-Implemented Inventions'' and by the EPO jargon that is used in the text.  The discussion should be led in terms that you, the software expert, can understand, and that have an unambiguous meaning in the context of your work.  The legislation should be benchmarked according to interdisciplinary criteria such as those worked out at \begin{quote}
Patentability Legislation Benchmarking Test Suite\footnote{\url{http://localhost/swpat/stidi/manri/index.en.html}}
\end{quote}  How does the directive proposal perform according to your MEP's opinion?  Could he/she please explain?

\item
You have read the a tabular listing of the amendments to the directive proposal which the Legal Affairs Committee (JURI) adopted and those which it rejected\footnote{\url{}}.  JURI rejected everything that could have limited patentability and adopted amendments, including so-called ``compromise amendments 1-6'', which further extend patentability beyond the Commission's proposal.  According to the JURI proposal, algorithms and business methods presented in terms of general-purpose computing equipment, such as Amazon One Click Shopping or JPEG Runlength Coding, are indisputably patentable inventions.  The requirement of ``technical contribution'' is undefined and, as documents by the EPO itself\footnote{\url{http://localhost/swpat/papri/epo-tws-app6/index.en.html}} and by famous patent lawyers\footnote{\url{http://localhost/swpat/papri/wuesthoff-bm01/index.en.html}} show, not an obstacle at all.  The JURI text is full of deceptive rhetoric.  Extentions are presented as restrictions, see e.g. the use of the word ``only'' in ``compromise amendment 1''.  Proponents of this text, such as Arlene McCarthy\footnote{\url{http://localhost/swpat/gasnu/amccarthy/index.en.html}}, Dr. Joachim Wuermeling\footnote{\url{http://localhost/swpat/gasnu/jwuermeling/index.en.html}}, Malcolm Harbour\footnote{\url{http://localhost/swpat/gasnu/mharbour/index.en.html}} and Janelly Fourtou\footnote{\url{http://localhost/swpat/gasnu/jfourtou/index.en.html}}, have consistently refused to explain its meaning in terms of example patents.  Your MEP should insist on a clear explanation, based on the sample patent claims and questions on our ``Patentability Legislation Tests'' pamphlet\footnote{\url{http://localhost/ffii/swpat/pamflet/index.en.html}}. As long the proponents fail to explain what in their proposal excludes algorithms and business methods as found in the samples, it must be assumed that they are trying to cheat fellow MEPs.  Being cheated by a rapporteur is not a valid excuse for cheating voters.  The eyes of 150,000 IT-literate citizens, including 2,000 IT employers\footnote{\url{http://localhost/swpat/lisri/03/epet0622/index.en.html}}, are on your MEP.  They don't care who is the rapporteur.   They expect their representative to stand up and defend the public interest.

see JURI votes for Fake Limits on Patentability\footnote{\url{http://localhost/swpat/lisri/03/juri0617/index.en.html}}

\item
You support the \begin{quote}
Call for Action\footnote{\url{}}
\end{quote} and urge your MEP to support it likewise (some MEPs are already listed as signatories), and to table the \begin{quote}
Amendments\footnote{\url{}}
\end{quote} proposed therein in time before the upcoming plenary session of 2003/09/01.

\item
You propose to meet your MEP, possibly around one of our events in/near the Parliament\footnote{\url{http://localhost/swpat/penmi/2003/europarl/index.en.html}}, to which you can invite your MEP.
\end{enumerate}}

\item
In case of doubt, contact our helpdesk {\tt europarl at ffii org},{\tt europarl-uk at ffii org}\ etc.  We may be able to give you some advice on how to procede most effectively.

\item
Participate in our events at the parliament\footnote{\url{http://localhost/swpat/penmi/2003/europarl/index.en.html}}, make an appointment with your MEP to meet at such an occasion.

\item
Donate for our lobbying expenses.  We are spending at least 30,000 EUR on top of a lot of unpaid voluntary work, and we will not be able to pay all our bills unless you come forward with donations.

The bank account for a donation is \begin{quote}
\begin{description}
\item[Country:]\ DE (Germany)
\item[Bank Code:]\ 70150000
\item[BIC (bank identifier code):]\ SSKMDEMM
\item[SWIFT:]\ SSKM DE MM
\item[Name of Bank:]\ Stadtsparkasse Muenchen
\item[Bank's Postal Address:]\ DE 80335 Muenchen\\
Lotstr 1
\item[Account:]\ 31112097
\item[IBAN (international bank account number)\footnote{We are told that, using the IBAN, money can now be transferred between european countries for the domestic fee, thanks to a recent EU internal market harmonisation directive which can be enforced against banks.}:]\ DE78701500000031112097
\item[Account Owner:]\ FFII e.V.
\item[Account Owner's Postal Address:]\ DE 80636 Muenchen\\
Blutenburgstr 17
\item[Keyword:]\ europarl
\end{description}
\end{quote}

Alternatively you can donate via paypal.com\footnote{\url{http://www.paypal.com}} to paypal at ffii org.

FFII is acknowledged as a public-interest association and we can send you a receipt that at least in Germany makes the donation tax-deductible.
\end{enumerate}

It would be great if you could get started with at least one of the above items right now.

Thank you very much

Yours Sincerely

\begin{center}
Hartmut Pilch on behalf of FFII e.V. and Eurolinux Alliance
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

