<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII 2002-07: BGH geht zu weit

#descr: In der Frage der Patentierbarkeit von Programmlogik hat der Bundesgerichtshof (BGH) sich und ganz Deutschland ins politische und rechtliche Abseits manövriert.  Wir fordern den Bundestag auf, hier seine gesetzgeberischen Kompetenzen wahrzunehmen.

#EWu: 1999-2000 legalisierte der Bundesgerichtshof (BGH) durch eine Reihe von Grundsatzurteilen in Deutschland die Patentierung von Konzepten der Programmlogik bis hin zu programmierten Geschäftsmethoden, Lehrmethoden, Heilmethoden, Methoden gesellschaftlicher Organisation, musikalische Kompositionstechniken sowie allen erdenklichen geistigen und zwischenmenschlichen Verfahren, sofern sie den Anforderungen einer virtuellen Maschine (Turing-Prinzip) genügen.  Damit trifft der BGH eine sehr weitreichende gesellschaftspolitische Entscheidung.   Europas Gesetzgeber haben sich im Jahre 1973 gemeinsam anders entschieden: gegen die Patentierbarkeit von Programmlogik und Verfahren der gesellschaftlichen Organisation.  An diese Entscheidung haben sie sich selbst durch einen internationalen Vertrag (EPÜ) und ihre jeweiligen Gerichtsbarkeiten durch nationalen Gesetzen (PatG) gebunden.  Daran hat sich bis vor kurzem auch der BGH ausdrücklich gehalten.

#SSM: Seit 1997 hat der BGH jedoch zunehmend dem Druck einer Patentrechtler-Lobby nachgegeben, die auf eine Änderung der Gesetze hinarbeitet.  Mit seinem neuesten Urteil bezieht er zu politischen Streitfragen Stellung, die derzeit in Brüssel kontrovers diskutiert werden.  Auf der Seite der Informationstechniker, Wirtschaftswissenschaftler und IT-Unternehmer überwiegt dabei die Meinung, dass die Patentierung von Programmlogik der Innovation und dem Wettbwerb abträglich ist und wirtschaftspolitisch nicht erwünscht sein kann.

#WRW: Wir bitten die Bundesregierung und alle Abgeordneten des Deutschen Bundestages und Bundesrates, geeignete Maßnahmen zu ergreifen, um den Bundesgerichtshof wirksam an die Vorgaben des Gesetzgebers zu binden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxbgh27 ;
# txtlang: de ;
# multlin: t ;
# End: ;

