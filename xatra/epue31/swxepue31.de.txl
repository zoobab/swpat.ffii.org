<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Wnt: Wäre es zu viel verlangt, die Zustimmung des Bundestages zur EPÜ-Revision mit einer solchen Maßnahme zu verknüpfen?

#IPh: Insbesondere sollte die Bundesregierung schnellstmöglich ein Moratorium auf die Erteilung von Softwarepatenten durch das DPMA erwirken, um unmittelbar entstehenden Schaden abzuwenden und die Seriosität der EU-Konsultation zu unterstreichen.  Schon jetzt warten Prüfungsanträge oft 4 Jahre, bevor sie erteilt werden.  Warum können sie nicht noch 2 Jahre länger warten?

#Der: Der Bundestag sollte auch die im November beschlossenen EPÜ-Änderungen nur dann ratifizieren, wenn gleichzeitig ernsthafte Schritte zur Kontrolle des Patentwesens und Begrenzung der Patentierbarkeit unternommen werden, wie in unserem %(pg:neuesten Offenen Brief) angeregt.

#Aej: Angesichts dieser Sachlage ist es unverantwortlich, wenn weiterhin in irgendwelchen patentjuristischen Hinterzimmern Änderungen vorbereitet werden, die einer weiteren Senkung der Patentierbarkeitshürden Vorschub leisten.

#LeW: Laut der Rechtsauffassung, die dem geltenden Gesetz und mustergültigen Referenzwerken (z.B. PatG-Kommentar Benkard 1988, Lamy Droit Informatique 1998) zugrundeliegt, hätten diese Patente niemals erteilt werden dürfen.

#Dxh: Diese Lösungen greifen nicht unmittelbar auf Naturkräfte zu: Ihre Gültigkeit wird auf rein geistig-mathematische Weise bewiesen und nicht etwa empirisch-experimentell überprüft.  Die einzige %(q:beherrschbare Naturkraft), die bei einer solchen %(q:Erfindung) ins Spiel kommt ist die Heizenergie für die Badewanne, in der der Fachmann sich sein Problem überlegt (sofern es nicht ohnehin schon durch Kundenwünsche an ihn herangetragen wurde).  Die einzige Investition, die durch ein solches Patent %(q:geschützt) wird, ist die Investition in den Brennstoff für eine Badewannensitzung.

#DaE: Das EPA hat in den letzten Jahren %(gk:30000 Patente) auf triviale Probleme gewährt, deren Lösung im Verfassen eines Programmtextes besteht.

#Dze: Das kann aber nicht funktionieren.  Die Krise des Patentwesen wird immer spürbarer.  Derzeit explodiert die Zahl der Patentanmeldungen, die Wartezeiten steigen und die Qualität sinkt ins Bodenlose.  Die Meldungen von unsinnigen Patentanmeldungen und Patentangriffen in Amerika und Europa überstürzen sich.  Erst vorgestern gab das Fraunhofer-Institut für Telematik zusammen mit dem DPMA dem Steuerzahler eine schallende Patent-Ohrfeige:

#Urr: Umso unverständlicher ist die derzeitige Haltung des BMJ.  Es hüllt sich zum Thema Swpat in Schweigen und ermutigt im stillen (z.T. im Widerspruch zu Vorgaben der Bundesregierung) die Europäische Kommission, die bisherigen Fehlentwicklungen durch eine Richtlinie zu zementieren.  Offenbar soll wieder nur eine %(q:Diskussion in den zuständigen Gremien) geführt werden.  Der schwarze Peter wird der EU-Kommission zugeschoben.  Die Gelegenheit zu einer angemessenen Konsultation, wie alle Fraktionen des Deutschen Bundestages sie gefordert haben, wird verspielt.

#Dzi: Die diplomatische Konferenz hat sich in einer Abschlusserklärung dafür ausgesprochen, dass in den zuständigen Gremien der EPO die Diskussion über die auf eine zweite Revisionsrunde vertagten Fragen alsbald aufgenommen werde.  Betroffen sind Themen wie z.B. die Patentierbarkeit computerprogrammbezogener Erfindungen, die Überführung der Biotechnologie-Richtlinie in das EPÜ, die normative Verknüpfung von EPÜ und Gemeinschaftspatent sowie die Einführung einer Neuheitsschonfrist.

#Zmi: Zum Schluss wird noch einmal ein %(bq:Weiterer Revisionsbedarf) konstatiert:

#DWn: Diese Thematik wird in einer zweiten Revisionsrunde neu aufgenommen.

#Dtb: Doch das EPA %(gr:drängt weiter auf Expansion und möchte die zwei Jahren einen neuen Anlauf unternehmen). Dabei erfährt es die Unterstützung des BMJ-Patentreferats:

#Egg: Es stimmt, dass von einer Streichung eine Signalwirkung ausgegangen wäre.  Nicht nur bezüglich der im Moment beim BGH anstehenden Entscheidung über die von der IBM-Patentabteilung %(bp:gewünschten) Ansprüche auf %(q:Computerprogrammprodukte) und %(q:Computerprogramme).

#Wse: Was %(q:heute der Fall ist), wird von Menschen bestimmt.  Es ist nicht das Ergebnis einer quasi-natürlichen Rechtsfortbildung, die es nur gelehrig nachzuvollziehen gilt.  Das BMJ trägt Verantwortung für das was passiert.  Es vermittelt zwischen den Gerichten und dem Gesetzgeber.  Der BGH hat %(bg:geirrt), und es ist Sache des BMJ, Fehlentwicklungen zu widerstehen und sie korrigieren zu helfen.

#Ghr: Gleichwohl ist die Gefahr gesehen, dass eine solche Streichung als Signal dahingehend missverstanden werden könnte, dass die Patentierbarkeit von Software in weiterem Umfang möglich sein solle, als dies bereits heute der Fall ist.

#Ese: Es ist erschreckend, zu lesen, wie das BMJ sich durch seine Rhetorik mit der %(pb:Patentbewegung) identifiziert.

#Dzt: Die neue Gesetzeslage wäre selbstverständlich auch für das BPatG und zahllose andere europäische Gerichte verbindlich geworden.  Somit hätte sich auch die Rechtslage wesentlich geändert.  Mehr dazu s. unten.

#Dnr: Die vorgesehen Streichung brächte eine wesentliche Änderung der %(e:Gesetzeslage).  Die Rechtslage bewegt sich seit einigen Jahren nicht mehr im Rahmen des Gesetzes.  EPA und BGH haben den Boden des Gesetzes auf Drängen der Patentabteilungen von Siemens, IBM und anderen verlassen und bewegen sich nun in einem Geflecht von Widersprüchen, die regelmäßig vom 17. Senat des Bundespatentgerichtes aufgedeckt werden.  Die Gesetzeslage ist klar, die Rechtslage ist verworren.  Das EPA plante letzten Sommer mit Unterstützung der Berliner und Brüsseler Patentreferenten, per %(q:Harmonisierung) nun auch die Gesetzeslage in Widersprüchlichkeiten zu stürzen, den letzten Rest eines stimmigen Technikbegriffes zu demontieren, und damit den Weg ins Bodenlose der Patentierung trivialer geistiger Verfahren zu eröffnen.

#EWn: Eine Veränderung der Vorschriften über die Patentierbarkeit von computerprogrammbezogenen Erfindungen schien von der von der EU zu dieser Thematik im Rahmen der angekündigten Richtlinie angestoßenen Diskussion nicht angebracht.  Zwar waren sich die Delegationen einig in der Bewertung, dass die im Basisvorschlag vorgesehene Streichung keine wesentliche Änderung der Rechtslage bewirkt hätte.

#InA: Interessant ist, dass nicht Großbritannien sondern Dänemark mit DE und FR mitzog.  Auch in den letzten Monaten hat die britische Regierung ihren Patentfunktionären freie Hand gelassen, die EU-Kommission zur schnellen Legalisierung der Softwarepatente des EPA zu drängen.  Auch die BMJ-Vertreter ermutigten m.W. in Brüssel diesen Kurs, obwohl von seiten der Bundesregierung anders lautende Anweisungen vorlagen.

#NWW: Noch in der vorbereitenden Sitzung des Verwaltungsrates war eine Streichung von 10 gegen 8 Mitgliedstaaten bei einer Enthaltung befürwortet worden.  Deutschland, Frankreich und Dänemark haben auf der Konferenz einen Antrag eingebracht, zum gegenwärtigen Zeitpunkt die geltende Fassung des Artikels 52 EPÜ unverändert zu belassen.  Ein intesniver Austausch mit Vertretern der anderen Mitgliedstaaten hat dazu geführt, dass sich die Mitgliedstaaten auf der diplomatischen Konferenz mit überwältigender Mehrheit gegen die Streichung der Computerprogramme als solche aussprachen.

#Dtc: Das BMJ-Patentreferat berichtet weiter:

#Flu: Folgende Artikel können hier aufklärend wirken:

#WMa: Wir wünschen uns vom BMJ meht kritische Distanz zur Patentbewegung.

#Mah: Mit dem Wort %(q:als solche) versuchen expansionsbestrebte Wortführer der Patentgemeinde seit 10 Jahren Nebel zu werfen.  Aus diesem Grunde wäre eine Streichung des redundanten Abs 52.3 durchaus wünschenswert.  Leider stand der aber nicht zur Disposition.  Es ging nur um %(q:Programme für Datenverarbeitungsanlagen).  Der Zusatz %(q:als solche) ist in diesem Zusammenhang unmotiviert.  Aus ihm spricht das schlechte Gewissen des Patentexpansionisten, der die Bedeutung seines Expansionsschrittes kleinreden möchte.

#Ipn: Ich patentiere das Produkt einer programmierten chemischen Reaktion, nicht das Programm als solches.

#IWn: Ich patentiere ein Verfahren zur Herstellung von Schachfiguren, nicht das Schachspiel als solches.

#Lie: Laut EPÜ Art 52.2c sind %(q:Programme für Datenverarbeitungsanlagen) nicht patentfähig.  Die Partikel %(q:als solche) in 52.3 bezieht sich auf alle Ausschlussgegenstände gleichermaßen und gewinnt ihren Sinn erst in einem syntaktischen Kontext, nämlich:

#DdW: Der Basisvorschlag sah zu Artikel 52 Abs 2 Buchstab c die Streichung von %(q:Computerprogrammen als solchen) aus der Liste der nicht patentfähigen Erfindungen vor

#IWs: In seinem Begleitschreiben %{TIT} erwähnt das BMJ-Patentreferat die Änderungen in Art 52.1 und 2e nicht.  Offenbar werden sie nicht als wesentlich angesehen.  Zu Art 52.2c schreib das BMJ-Patentreferat:

#Exo: Eine solche enge Definition haben wir in einem %(er:Vorschlag für eine EU-Richtlinie) und einem %(ep:Vorschlag für die Revision des Art 52) veröffentlicht.

#WPn: Wir stehen selbstverständlich nach wie vor auf dem Standpunkt des Offenen Briefes, wonach auch die Änderungen von Abs 52.1 und 52.2e einen Rückschritt darstellen.  Der neue Abs 52.1 schreibt vor, dass Patente %(q:für Erfindungen auf allen Gebieten der Technik) erhältlich sein sollen.  Ein solches Universalitätspostulat erfüllt in einem abstrakten Freihandelsvertrag wie TRIPS eine ganz andere Funktion als im EPÜ.  Es gehört nicht in das EPÜ, und wäre auch nur dann akzeptabel, wenn es von einer engen Definition der Begriffe %(q:Technik), %(q:Erfindung) und %(q:industrielle Anwendung) begleitet würde.

#W2e: Wir haben in dem %(ob:Offenen Brief von Anfang August) unserem Wunsch Ausdruck verliehen, dass Artikel 52 nicht angetastet werde.  Der Offene Brief wurde zwar vom BMJ nicht beantwortet und unserem Wunsch wurde auch nicht ganz entsprochen, aber immerhin ist die Diskussion um die Computerprogramme offen geblieben.

#Aua: Aus hiesiger Sicht soll die Revisionsakte so zügig wie möglich gezeichnet und dann ratifiziert werden.  Für den Fall, dass Sie eine Stellungnahme beabsichtigen, bitte ich Sie, mir diese bis zum 30. März 2001 zu übersenden.

#IhR: In der Zeit vom 20.-29. November 2000 fand die diplomatische Konferenz  zur Revision des Europäischen Patentübereinkommens statt.  Als Anlage übersende ich Ihnen die von der Konferenz angenommene Revisionsakte MR/3/00/rev.1 sowie einen kurzen Überblick über die wesentlichen Beratungsergebnisse.

#ZdW: Zunächst aus dem Schreiben von Dr. Welp:

#DhW: Die einschlägigen Dokumente scheinen im Moment auf den %(bj:einschlägigen Webseiten des BMJ) nicht vorhanden zu sein.  Daher tippe ich hier ein paar Auszüge ab und streue ein paar Bemerkungen ein:

#descr: Der FFII erhielt vom Bundesministerium der Justiz (BMJ) ein Schreiben, in dem Auskunft über die Ergebnisse der Diplomatischen Konferenz zur Revision des Europäischen Patentübereinkommens gegeben und zu Stellungnahmen aufgerufen wird.  Dies geschieht mit diesem Brief.  Der FFII kritisiert u.a. erneut die Übernahme der TRIPs-Formel %(q:auf allen Gebieten der Technik) in Art 52 EPÜ.

#title: 2001/01: Brief an das BMJ

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxepue31 ;
# txtlang: de ;
# multlin: t ;
# End: ;

