<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII invites OpenForumEurope to explain how text patents benefit open
source software

#descr: Open Forum Europe is lobbying the European Parliament in the name of
%(q:open source companies) in order to make software directly
patentable and to ensure that interoperable software may not be
written.  We invited Graham Taylor of Open Forum to explain.  An short
dialog followed, in which Taylor presented himself as naively
well-believing about the meaning of what he signed and reluctant to
repair the damage.

#erW: Answer to Answer of Graham Taylor

#all: Dear Mr. Taylor,

#nns: At %(URL) you find news about how your initiative, Open Forum Europe,
is providing invaluable help to the community of industrial patent
lawyers in their effort to push the European Parliament to legalise
program claims, business method patents and restrictions on
interoperability.

#aen: The signatories of the %(q:Joint Statement), among them you as a
representative of the open source software community, are sending
their appeal to all members of the European Parliament (MEPs) in an
attempt to create the impression that the OSS community or its more
reasonable part (i.e. people like you) trust the European Patent
Office and want patents to be enforcable against programmers or ISPs
who publish an infringing text on their website.

#thn: I would find it very interesting to hear you argue in what way the
demands which you are advocating could be beneficial to the
constituents of your organisation.

#tWo: Could you perhaps come to the %(EVT) and explain your point of view? 
Which of the panels in the Conference, as proposed so far, do you
think would be most suitable?

#ntW: Software Patent Hearing & Conference in/at the European Parliament on
May 7.-8. in Brussels

#uie2: Yours sincerely

#jes: In an answer on Apr 28th, Mr. Taylor declined our invitation and
offered a dialogue in writing.  We ansered immediately, as below, and
have so far not received further mails:

#hei: But that's not the central point here.

#ros: There is no difference between the US practise and that of the
European Patent Office, as far as software is concerned.

#eWt: Just look at the patents granted by the EPO on the basis of the rules
which MEP McCarthy wants to codify:

#olu: Again the proposal which you endorse achieves the opposite of what you
say is your goal: it makes sure that interoperable software can
neither be published or used without a license.

#ejW: Your belief is wrong, the opposite is true.

#iap: See our analysis at %(URL) and the tabular comparison of proposal and
amendment proposals at %(PRP)

#daW: It would have been a good idea to consult the internal eurolinux list,
to which I am cc-ing, before speaking about patent legislation in the
name of the opensource community.

#iWm: We cannot let the impression that %(q:the oss community is split on
patents) stand.  It must be wiped out forcefully in one way or other. 
Damage could be turned into benefit.

#tWo2: What would you propose?

#iel: Sincerely

#eWW: Redhat Inc criticises Open Forum Europe and restates its support of
the FFII/EuroLinux position on software patents.  It remains unclear
who the other members of OFE are and whether they support the position
of Graham Taylor.

#mWu: James Heald reports about statements made by a representative of Open
Forum Europe at a booth.  When questioned by critical visitors, this
representative pretended to be politically naive and misinformed the
public about the positions which OFE had supported.

#tnc: Graham Taylor makes things only worse by saying that he did not claim
to represent the open source community.  Taylor indeed signed the
patent-extremist %(js:Joint Statement) as %(q:Graham Taylor, Open
Forum Europe Programme Director, Initiative to accelerate the market
take up of Open Source Software), and even in his excuse letter he
insinuates that he is speaking for %(q:hard business interests) of his
constituents in his function as a promoter of open-source software.

#tce: Bruce Perens points out that Graham Taylor can only speak for himself:
No real OSS community leader would speak to politicians about such a
sensitive subject without consulting other people in the community.

#pWe: UK Conservative MEP Malcolm Harbour, a relay of industrial patent
lawyers in the European Parliament, supports Graham Taylor's demands
for extension of patentability beyond the scope of the European
Commission's proposals, explains that he has met Graham Taylor and
that Graham Taylor does not claim to represent the OSS community.

#yfq: The website says %(TXT) We found NO member list (which is kind'a
strange for an %(q:open) organisation).

#thn2: mentions %(q:those organisations who contributed to the work during
the 3 month prelaunch period included Caldera, Citibank, Compaq, the
Financial Times, GBdirect, Globix, IBM, Identrus, IT Direct at Lloyds,
Reed in Partnership and the DTI.) ... hardly representative of Open
Source.  One could even imagine that OFE was created to have a
controlled %(q:open source) organization in order to be able to
misrepresent the community.

#Wfo: %(e:The Register) about founding of OpenForumEurope

#nfe: Says that it is backed only by large corporate and institutional
members, most of whom do not use opensource software, and membership
fees are from 3000 pounds upwards.  Other sources from UK say that
OFEu promotes not only software patents but also rigid copyright laws
and proprietary software.

#Wef: Positions in the current JURI discussion.  Graham Taylor took sides 
for the most extreme patent inflationist of these positions

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxofeu034 ;
# txtlang: en ;
# multlin: t ;
# End: ;

