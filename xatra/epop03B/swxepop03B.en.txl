<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#tth: Is the EPO president the right person to choose the members of such a division, or would it be better for them to be independently nominated by the legislatures of the EPC member states (including the EU) ?

#Wgt: Could he give an order-of-magnitude estimate of the number of

#psn: Does the EPO president believe there are systematic problems of quality in the EPO's work ?

#srW: Does the EPO president believe that such a quality control division would be a useful addition to the EPO structure ?

#sWc: Moreover MEPs could ask about the new CEC %(qd:proposal for a quality control division)

#kBt: What about taking the matter to the Enlarged Board of Appeal and asking them to overrule the TBA?

#dWl: Can the EPO not decide to adopt a different administrative practise, based on the political will that has been expressed by the Parliament, which is the only elected legislative body in the EU?

#ntt: Some say that not the TBA but only the Enlarged Board of Appeal (EBA) could possibly have been legitimated to take decisions on fundamental legal matters.  To what degree are these decisions binding for the EPO today?

#aWd: The current EPO practise on software appears to be based on a series of decisions taken by very few people at the Technical Boards of Appeal, from Vicom (1986) to IBM I & II (1998) and Pension Benefit Systems (2000).  Did the TBA arrive at these decisions independently or do they reflect an underlying decision by the EPO (President or the Administrative Council)?

#fng: Vice president Desantos said %(oe:in Paris at OECD) that indeed this might be a reason for the EPO to reconsider the whole policy on software.  Desantos and his colleagues showed a remarkable spirit of political leadership, very much contrasting with that of the USPTO's officials at that conference.  And it's not the only time that this has been remarked.  Dr. Kober has said something similar to a Belgian newspaper last November.  Art 52 seems to give you a legitimate basis for refusing to grant patents on data processing and business logic (such as the ones listed in %(DOK)).  In case you do consider this option, what obstacle is there to overcome?

#nrh: We welcome the EPO's new mission statement of %(q:strengthening innovation and economic growth in the interest of European citizens).  The European Parliament has recently, on the basis of overwhelming evidence from economists and software professionals, decided that certain classes of patents which are currently granted by the EPO are not desirable, because they do not serve innovation and economic growth.  Is this reason enough for the EPO to consider stopping to grant such patents?

#iaW: Questions about CEC-Proposal for Quality Control Division

#iOr: Questions about EPO policy on Art 52 EPC

#descr: Suggestions to members of the European Parliament

#title: Questions to the President of the European Patent Office

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxepop03B ;
# txtlang: en ;
# multlin: t ;
# End: ;

