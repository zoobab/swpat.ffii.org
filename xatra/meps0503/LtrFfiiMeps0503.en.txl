<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: DRAFT FFII Letter to Members of the European Parliament

#descr: A letter to be distributed in paper form to MEP mailboxes on tuesday
2005-03-22, open to signing by the public thereafter.

#ajc: As an organisation that has studied the software patent directive
dossier for many years and has been explicitly entrusted by 80,000
individuals and 1,400 companies to act as their voice on this subject,
we would like to explain to you how we see the choices that you are
currently facing and what our interests are.

#mhr: The Legal Affairs Commitee will presumably decide in early April
%(q:whether indeed a Common Position exists), and then, if that is
found to be the case, proceed to a decision on whether to propose
amendments to or rejection or acceptance of the Council's position.

#oWe: We believe that a Common Position does not exist and there is no
legitimate basis for continuing the procedure regarding this
directive.

#nte: The Council claims to have adopted its Uncommon Position on Software
Patents on 7th March.  It is evident from the session %(cm:minutes) 
that this position was not adopted by a qualified majority of member
states.  We have asked a %(sq:series of questions) to the Council's
Legal Service in order to find out what exactly happened on that day,
in terms of the Council's rules of procedure.  Looking at the session
minutes, we find that no valid decision was taken at this session. 
Instead we see a rehearsed theater performance, designed to blur the
responsabilities and make it difficult to assess clearly who violated
the rules.  This has led to conflicting explanations by the Danish and
the Dutch minister.  When questioned by their parliaments, Bendtsen
said that Brinkhorst prevented him from requesting a B-Item, whereas
Brinkhorst said that Bendtsen %(q:came to the church but did not sing)
(a dutch idiom for %(q:coitus interruptus)).

#sWc: Even in the Council Presidency's own words, the Uncommon Position was
adopted %(q:for institutional reasons, so as to not create a
precedent).  Reopening of negotiations would have created a precedent
for parliamentary democracy in Europe.  It would have fulfilled
requests which COSAC (committee of national parliamentary committees
of the EU) faised in 2000: that national parliaments should have an
opportunity to review political agreements that were reached at a
session by means of hitherto unknown %(q:compromise) amendments, such
as done on 18 May 2004.  It would thereby have created pressures on
the ministers and their civil servants to do substantial legislative
work, rather than just cling to legislative power.

#aed: Both the Council and the Commission have shown since September 2003
that they dread nothing as much as the legislative responsibility that
comes with legislative power.  Rather than work out a real common
position based on the amendments of the European Parliament, the
ministers allowed themselves to be fooled into passing the most
uncompromisingly pro-patent package that the civil servants form their
national patent offices had drafted for them, and then spent nearly a
year pressuring dissenting countries with fictitious %(q:unwritten
rules).  Rather than accepting the European Parliament's invitation to
work out a new proposal, the Commission rejected a unanimous motion
without explanation and, in addition, misrepresented the Parliament's
opposition by %(q:agreeing) to bury the whole directive project in
case the Parliament chooses to reject the current illegitimate text.

#hii: In this situation, there is in our view no longer much hope in
achieving something constructive in the co-decision procedure.  Any
law resulting from this procedure would carry the stigma of
illegality.  The cleanest option for the Parliament would therefore be
to state that the Council has not reached a legitimate Common Position
and refuse to go on with the procedure on this basis.

#Wwl: If the Parliament nevertheless decides to go on on the basis of an
Uncommon Position, the most straightforward approach would be to
resubmit the Amendments of September 2003, possibly in a slightly
tidied-up form together with a new report, so as to %(q:kick the ball
back into the Council's court), as Commissioner McCreevy would say. 
FFII has prepared some documents that explain the key differences
between the Council version and the Parliament's version of 2003:

#Wot: We also maintain an office in Brussels and a group of people who do
their best to permanently provide expertise as may be needed: ....

#eWp: If the Parliament can come out of the 2nd reading with a strong
negotiating position, we will then look forward to a concilation
procedure, in which we can melt even more ice, i.e. remove stubborn
and incompetent patent officials from their positions of influence in
the national ministries that handle the Council negotiations.  The
Parliament will then be the knight in the shining armour, and we may
achieve much more than we would otherwise achieve in many years of
hard political work.  However, we do feel that this way of legislating
is somewhat unnatural.  Good laws are not made by diplomacy.  Under a
sound legislative procedure, the basic work would be done in the
earlier stages of the legislation, by civil servants who draft the law
faithfully at the initiative and according to the wishes of the
elected legislators.  Seen from this perspective, the Parliament has
probably already gone far too deeply into the gory details of software
patent legislation.  A simple rejection right at the start would
probably have been most appropriate.

#efe: We have a good law already, which creates a huge legitimacy problem
for the European Patent Office.  Plaintiffs like IBM have managed to
stretch and twist the law by decisions of the Technical Boards of
Appeal in the 1990s, but, given the change of climate in recent years
and the incoherence of the recent caselaw, we may be able to move the
interpretation back to the path of virtue even without the help of a
directive.  The Commission's repeated threats to kill the directive
project thus do not really frighten us.

#Wre: We can therefore encourage you to do your utmost to maintain the
dignity of your institution and the integrity of the EU's legislative
procedures, without worrying too much whether the absence of a
directive might leave software developpers or SMEs exposed to an
uncontrolled patent system.  If we first bring the European house in
order, we will better equipped to fix the problems caused by the EPO a
few years from now.

#uie: Yours sincerely

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrFfiiMeps0503 ;
# txtlang: en ;
# multlin: t ;
# End: ;

