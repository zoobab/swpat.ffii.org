<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Letter of MEPs to Barroso

#descr: The undersigned ask Barroso to restart the directive and to transfer
responsibility for the dossier to another Directorate, such as DG
Information Society.

#aWd: Dear Mr. President

#ooe: Following the votes of the JURI Committee on February 2, the
Conference of Presidents on February 17, and Parliament in plenary
session on February 24, the President of the European Parliament asked
the Commission to review the process of producing a directive on
software patents.  The following day you responded simply that the
Commission did not intend to offer a new proposal.  We take note of
the speed of the Commission’s response, its failure to acknowledge the
depth of the Parliament’s interest in the subject, and its failure to
offer any explanation for its refusal of the Parliament’s request.

#snl: We are concerned that the Commission’s terse response is further
evidence of fundamental problems in how this directive was conceived
and developed.  The roots of the directive as presented in the 1997
Green Paper and the 1999 Communication to the Parliament reflect a
strong predisposition toward following the U.S. model --- as does the
choice of title, %(q:Computer-Implemented Inventions).

#pac: In little more than a page in either document, DG MARKT approached
this problem from the perspective of a patent law constituency who
naturally preferred not to have the scope of their practice
constrained in any significant way.   DG MARKT appears to have failed
to grasp the enormity of the stakes in a timely manner.   Important
economic aspects, such as transaction costs, licensing practices,
opportunistic behavior, liability, and insurance cost, were either
summarily dismissed or simply not discussed.

#hei: In this case, there have been a number of incidents that have at least
stirred concerns about the appearance of impropriety.  These include:

#iha: Citation in the Directive of undocumented assertions on the U.S.
software industry from an internally commissioned study

#iip: Premature circulation of a directive draft with metadata indicating
inappropriate access and authorship by a trade association

#Wai: A study on how patent offices should promote the patent system --
awarded to a company that holds a uniquely large patent portfolio and
a reputation for being an especially ardent proponent of software
patents

#Wew: Having committed to the position of the directive, it is
understandable that DG MARKT would be reluctant to rethink the issues
and thereby acknowledge the deficiencies of the earlier process. 
Indeed, we not believe that DG MARKT, having brought us to the present
confrontation, can participate objectively in any decision to restart
the process.  There are at least five other DGs with a commensurate
interest in the far-reaching implications of software patents and with
expertise to contribute that was not evident in the Commission’s
directive.  They and their respective commissioners should be given a
proper opportunity to consider a restart in consultation with the
Parliament and with each other without the burden of special pleading
from DG MARKT.   DG MARKT’s views should be heard – but publicly
rather than behind closed doors.   Its familiarity with patent law and
the patent community’s interests should be taken into account but as
clearly identified input into a policy decision that, as public debate
has demonstrated, is too important to be left to the lawyers.

#Ini: We also cannot avoid stating that commissioner McCreevy himself is
believed to have a stake in the issue insofar as he helped the Irish
Presidency engineer the Council's political agreement of May 18, 2004.
 This naturally raises questions about his ability to consider and
effectuate a restart in a completely open-minded manner.  Given the
sensitivity of this issue among software developers and adjacent areas
of the economy, it is impossible to overlook Ireland’s unique stake in
this issue.   By offering lower tax rates than the rest of Europe,
Ireland has attracted the distribution operations of major U.S.
vendors and become the gateway through which software is imported into
Europe, successfully making up in tax volume what it loses from a
lower tax rate.  Microsoft’s strong presence in Ireland, evident in
its official sponsorship of the Irish Presidency, does not lend
confidence to a belief that Ireland’s Commissioner can handle this
delicate matter with the impartiality and perspective it requires.

#nmz: Accordingly, we ask you to reconsider your intentions in due regard
for the Parliament’s request, a request  which has not been made
lightly.  We expect more than a statement of negative intent in
return.  Of the three bodies, only the Commission has the resources to
investigate and analyze an issue of this magnitude with the care and
diligence it deserves.

#rht: The debate over software patents is not just an issue of a single
industry or profession.  Software provides the essential
infrastructure and tools for generating, managing, communicating and
using knowledge in all sectors of the economy.  The ground rules for
innovation and competition for software deserve more attention and
perspective than the Commission has provided to date.  The consensus
for copyright is heartening, but it shows up the great danger in the
deep divisions over patents and the possible consequences of getting
the law wrong.  The cost may be measured not only in hundreds of
billions of Euros over time but in Europe’s role in the world’s market
for ideas and influence.  The impatience of one side of the debate to
resolve the issue in their favor should not be an excuse for the
Commission to abdicate its responsibility to develop sound policy with
due time, attention, and resources.

#our: In the interests of a full and proper restart on this important
directive – and to ensure maximum public trust in the transparency
and accountability of European institutions, we respectfully ask that
you remove DG MARKT from the internal leadership in reconsidering the
restart decision and any further action in this matter.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrMepsCec0503 ;
# txtlang: en ;
# multlin: t ;
# End: ;

