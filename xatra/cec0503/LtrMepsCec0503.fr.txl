<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lettre de MPE à Barroso

#descr: Les signataires demandent à Barroso de redemarrer la procédure et de
transférer la responsabilité du dossier à une autre direction, comme
p.e. DG Société d'Information.

#aWd: Cher Président

#ooe: Suite aux votes de la commission JURI le 2 février, de la Conférence
des Présidents le 17 février et du Parlement en session plénière le 24
février, le Président du Parlement européen a demandé à la Commission
de revoir la procédure d'élaboration d'une directive sur les brevets
logiciels. Le lendemain, vous avez simplement répondu que la
Commission n'avait pas l'intention de fournir une nouvelle
proposition. Nous prenons note de la rapidité avec laquelle la
Commission a répondu, du fait qu'elle ne parvient pas à reconnaître le
profond intérêt du Parlement en la matière ni  à fournir
d'explications de son refus de la demande du Parlement.

#snl: Nous nous inquiétons du fait que la réponse laconique de la Commission
est une preuve supplémentaire des problèmes fondamentaux dans lesquels
cette directive a été conçue et s'est développée. Les sources de la
directive, telles que présentées dans le Livre vert de 1997 et dans la
Communication au Parlement de 1999, reflètent une forte prédisposition
à suivre le modèle des États-Unis --- tout comme le titre choisi : «
Inventions mise en œuvre par ordinateur ».

#pac: En un peu plus d'une page dans chacun de ces documents, la DG MARKT a
abordé le problème du point de vue du  milieu du droit des brevets,
qui préférait naturellement ne pas avoir l'étendue de ses pratiques
contrainte par un quelconque moyen significatif. Il s'avère que la DG
MARKT n'est pas parvenue à saisir à temps l'énormité des enjeux.
D'importants aspects économiques, tels que les coûts de transaction,
les pratiques de session de licence,  une attitude opportuniste, la
responsabilité et les coûts d'assurance ont été sommairement écartés
ou tout simplement ignorés.

#hei: Dans ce dossier, un certain nombre d'incidents sont survenus, ce qui a
au moins eu le mérite d'éveiller les inquiétudes concernant ces
inélégances apparentes, y compris :

#iha: la citation dans la directive des affirmations non documentées sur
l'industrie logicielle des États-Unis provenant d'une étude commandée
en interne ;

#iip: la diffusion prématurée d'une ébauche de la directive avec des
informations indiquant un accès inapproprié et l'empreinte d'une
rédaction de la part d'une association commerciale ;

#Wai: une étude sur la manière dont les offices de brevets devraient
promouvoir le système de brevets – récompensant une entreprise
détenant un portefeuille de brevets dont l'épaisseur est unique en son
genre et ayant la réputation d'être un fervent partisan des brevets
logiciels.

#Wew: S'étant engagée sur la position de la directive, il est compréhensible
que la DG MARKT soit réticente à poser à nouveau les problèmes et
reconnaître ainsi les lacunes de la procédure initiale. En effet, nous
ne croyons pas que la DG MARKT, nous ayant conduit à la présente
confrontation, puisse participer objectivement à toute décision de
redémarrage de la procédure. Au moins cinq autres DG ont un intérêt
certain dans les conséquences considérables des brevets logiciels et
sont tout à fait expertes pour apporter leur contribution au fait que
cela n'était pas évident dans la directive de la Commission. On
devrait donner à ces Directions générales et à leurs commissaires
respectifs l'opportunité de considérer un redémarrage en collaboration
avec le Parlement et entre elles, sans être gênées par la plaidoirie
spécifique de la DG MARKT. Les opinions de la DG MARKT devraient être
entendues – mais de manière publique et non derrière des portes
closes. Ses accointances avec le droit des brevets et les intérêts de
la communauté des brevets devraient être prises en compte, mais en
tant qu'informations clairement identifiées entrant dans une décision
politique qui, comme le débat public l'a démontré, est trop importante
pour être abandonnée aux seuls avocats.

#Ini: Nous avons peine à constater que le Commissaire McCreevy est également
réputé avoir des intérêts sur le sujet, à tel point qu'il a aidé la
Présidence irlandaise à élaborer l'accord politique atteint par le
Conseil le 18 mai 2004. Cela soulève naturellement des questions à
propos de son aptitude à prendre en compte et effectuer un redémarrage
de manière totalement ouverte. Étant donné la sensibilité de cette
question parmi les développeurs de logiciels et des domaines adjacents
de l'économie, il est impossible de privilégier les intérêts
particuliers de l'Irlande sur ce sujet. En offrant des taux
d'imposition plus bas que dans le reste de l'Europe, L'Irlande a
attiré les activités de distribution des principaux vendeurs des
États-Unis et est devenue le passage privilégié par lequel les
logiciels sont importés en Europe, arrivant à compenser par le volume
des taxes ce qui était perdu en raison de la baisse du taux
d'imposition. La forte présence de Microsoft en Irlande, démontrée par
son partenariat officiel avec la Présidence irlandaise au premier
semestre 2004, ne pousse pas à faire confiance à la capacité du
Commissaire irlandais à prendre en charge cette question délicate avec
l'impartialité et le recul nécessaires.

#nmz: En conséquence, nous vous demandons de reconsidérer vos intentions
quant à la demande du Parlement, demande qui n'a pas été faite à la
légère. Nous attendons en retour plus qu'une réponse négative. Parmi
les trois institutions, la Commission dispose de toutes les ressources
pour enquêter et analyser une question de cette ampleur avec toute
l'attention qu'elle mérite.

#rht: Le débat sur les brevets logiciels ne concerne pas une seule industrie
ou profession. Les logiciels fournissent l'infrastructure et les
outils essentiels pour produire, gérer, communiquer et utiliser la
connaissance dans tous les secteurs économiques. Les règles de base
pour l'innovation et la concurrence en informatique méritent plus
d'attention et de recul que la Commission n'en a fournis à ce jour. Le
consensus autour du droit d'auteur est encourageant mais il met en
lumière un grand danger de profonds conflits avec les brevets et les
conséquences potentielles d'aboutir à une loi erronée. Le coût peut se
mesurer non seulement en milliards d'euros au cours du temps mais
également dans le rôle que l'Europe occupe sur le marché mondial en ce
qui concerne ses idées et son influence. L'impatience d'une partie
prenante au débat à résoudre le problème en sa faveur ne devrait pas
être une excuse pour que la Commission abdique sa responsabilité à
mettre en œuvre une politique raisonnée, avec le temps, l'attention et
les ressources nécessaires.

#our: Dans l'intérêt d'un redémarrage complet et approprié de cette
directive importante – et pour garantir une confiance maximale du
public dans la transparence et la responsabilité des institution
européennes –, nous vous demandons, avec tout le respect que nous
vous devons, de retirer à la DG MARKT le leadership interne pour
reconsidérer la décision de redémarrage et pour entreprendre toute
action qui en découlerait.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatxatra.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrMepsCec0503 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

