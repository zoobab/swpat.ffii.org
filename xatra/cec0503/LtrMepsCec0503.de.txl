<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brief der MEPs an Barroso

#descr: Die Unterzeichner bitten Barrosso um einen Neustart der Richtlinie und
um die Übertragen der Verantwortung an eine andere Direktion, wie etwa
die Generaldirektion Informationsgesellschaft.

#aWd: Sehr geehrter Herr Präsident

#ooe: Gemäss der Abstimmung der JURI-Ausschusses am 2. Februar, der
Konferenz der Präsidenten am 17. Februar, und des Parlaments in der
Plenarsitzung vom 24. Februar, hat der Präsident des Europäischen
Parlaments die Kommission gebeten das Verfahren zur Erstellung einer
Richtlinie für Softwarepatente zu prüfen. Am nächsten Tag haben Sie
einfach nur geantwortet, die Kommission beabsichtigt keinen neuen
Vorschlag anzubieten. Wir haben bemerkt mit welcher Geschwindigkeit
die Kommission geantwortet hat, ihr Scheitern im Erkennen der Tiefe
der Interessen des Parlaments in der Sache, und ihr Scheitern beim
Anbieten jeglicher Erklärung bezüglich der Ablehnung der Forderung des
Parlaments.

#snl: Wir sind betroffen davon dass die dünne Antwort der Kommission ein
weiterer Beweis für fundamentale Probleme ist mit dem diese Richtlinie
ersonnen und entwickelt wurde. Die Wurzeln dieser Richtlinie wie sie
im Jahr 1997 durch das Grünpapier vorgestellt wurde und die Gespräche
mit dem Parlament im Jahr 1999 spiegeln eine starke Voreingenommenheit
wieder dem US-Modell zu folgen --- so wie es die Wahl des Titels,
%(q:Computer-Implementiere Erfindungen), nahelegt.

#pac: In kaum mehr als einer Seite in jedem der Dokument ist die
Generaldirektion Binnenmarkt auf dieses Probleme aus dem Blickwinkel
einer Patentrechts-Klientel eingegangen, die naturgemäß bevorzugt den
Kernbereich ihres Wirkens im wesentlichen nicht beeinträchtig wissen
möchte. Die Generaldirektion Binnenmarkt scheint daran gescheitert zu
sein den enormen Umfang der Angelegenheit frühzeitig zu erfassen.
Wichtige ökonomische Aspekte, wie etwa Transfergegühren,
Lizensierungspraktiken, Opportunistische Verhaltensweisen,
Haftungspflichten, und Versicherungskosten, wurden entweder als Ganzes
bestritten oder wurden einfach nicht behandelt.

#hei: In diesem Fall gab es eine gewisse Anzahl an Vorfällen, die mindestens
Bedenken zum Auftreten von Ungereimtheiten hervorgerufen haben. Diese
beinhalten unter anderem:

#iha: Bezug in der Richtlinie auf unbelegbare Behauptungen über die
US-Softwareindustrie aus einer intern durchgeführten Studie.

#iip: Vorzeitiger Umlauf eines Richtlinienentwurfs mit Metadaten die auf
unpassenden Zugriff durch und Autorenschaft von Wirtschaftsverbänden
hinweisen.

#Wai: Eine Studie darüber wie Patentabteilungen das Patentsystem bewerben
sollen -- vergeben an ein Unternehmen, das ein herausragend großes
Patentportfolio hält und bekannt dafür ist, ein besonders
leidenschaftlicher Verfechter für Softwarepatenten zu sein.

#Wew: Nachdem die Generaldirektion Binnenmarkt sich zur Richtlinie bekannt
hat ist es verständlich dass sie nicht gewillt sein dürfte die
Angelegenheit nochmals zu überdenken und damit die Mängel des
vorangegangenen Prozesses zu bestätigen. In der Tat glauben wir nicht,
dass die Generaldirektion Binnenmarkt, die uns in die aktuelle
Konfrontation gebracht hat, objektiv an irgend einer Entscheidung zu
einem Neustart des Verfahrens teilhaben kann. Es gibt mindestens fünf
weitere Generaldirektionen, die ein vergelichbares Interesse an den
weitreichenden Folgen von Softwarepatenten haben und Kenntnissen
beizusteuern haben, das im Richtlinientext der Kommission nicht
erkennbar war. Diese und ihre jeweiligen Kommissare sollen eine
ordentliche Gelegenheit erhalten zusammen mit dem Parlament und mit
sich selbst über einen Neustart der Beratungen nachzudenken, ohne
dabei der besondere Belastung durch die besondere Argumentation der
Generaldirektion Binnenmarkt ausgesetzt zu sein. Die Ansichten der
Generaldirektion Binnenmarkt sollen angehört werden - aber eher
öffentlich und nicht hinter verschlossenen Türen. Deren Vertrautheit
mit dem Patentrecht und den Interessen der Patentkreise soll dabei
berücksichtig werden, und zwar als klar erkennbare Eingabe in einen
politischen Entscheidungsprozess, der, wie die öffentliche Diskussion
gezeigt hat, zu wichtig ist, als daß man ihn den Anwälten überlässt.

#Ini: Wir können uns weiterhin nicht erlauben auszulassen festzustellen,
dass von Kommissar McCreevy selbst angenommen wird Anteil an der Sache
zu haben weil er dabei mitgeholfen hat der irischen
Ratspräsidentschaft das politische Übereinkommen des Rates vom 18. Mai
2004 zustande zu bringen. Die wirft natürlich die Frage auf, wie er
dazu fähig sein könnte einen Neustart in einer vollkommen
unvoreingenommen Art zu erwägen und zu beschliessen. Geht man von der
Feinfühligkeit der Angelegenheit aus, wie sie unter
Software-Entwicklern und benachbarten Bereichen der Wirtschaft
herrscht, dann ist es unmöglich Irlands herausragende Rolle in dieser
Angelegenheit zu übersehen. Indem niedriegere Steuersätze als im Rest
von Europa gewährt wurden hat Irland das Distributionsgeschäft der
grossen US-Hersteller anlocken können und wurde dadurch das Tor durch
welches Software nach Europa importiert wird, was erfolgreich das
Steueraufkommen ausgleicht, das durch die geringeren Steuersätze
verloren geht. Microsofts starke Präsenz in Irland, erkennbar in der
offiziellen Sponsorenschaft der irischen Präsidentschaft, trägt nicht
zur Vertauensbildung bei, dass der irische Kommissar diese delikate
Angelegenheit mit der Unparteilichkeit und der Perspektive behandeln
kann, die erforderlich ist.

#nmz: Entsprechend bitten wir Sie darum, daß Sie ihre Absicht neu überdenken
was den Antrag des Parlaments angeht, einen Antrag, der nicht
leichtfertig gemacht wurde. Wir erwarten mehr als nur eine Erklärung
der Ablehnung als Antwort. Von den drei Körperschaften hat nur die
Kommission die Mittel um eine Angelegenheit dieser Größe mit der
nötigen Umsicht und Sorgfalt zu recherchieren und zu untersuchen.

#rht: Die Debatte über Softwarepatent ist nicht einfach nur eine
Angelegenheit einer einzelnen Industrie oder Berufsgruppe. Software
bietet die unverzichtbare Infrastruktur und die Werkzeuge für die
Erzeugung, Verwaltung, Übertragung und Nutzung von Wissen in allen
Bereichen der Wirtschaft. Die Grundregeln für Innovation und
Wettbewerb für Software verdienen mehr Aufmerksamkeit und Weitblick
als die Kommission bis heute geliefert hat. Der Konsens für Copyright
ist ermutigend, aber er offenbart die große Gefahr mit schweren
Konflikten bei Patenten und den denkbaren Konsequenzen wenn der
Gesetzestext misslingen sollte. Die Kosten werden möglicherweise nicht
nur in hundert Millionen Euro im Laufe der Jahre gemessen werden
können, sondern in der Rolle die Europa auf dem Weltmarkt der Ideen
und der Wegweisung spielt. Die Ungeduld auf der einen Seite der der
Debatte, die Angelegenheit in ihrem Sinne vom Tisch zu bringen, darf
keine Entschuldigung dafür seom, daß die Kommission ihre Verantwortung
zur Formung einer guten Politik mit angemessener Zeit, Beachtung und
Resourcen entledigt ist.

#our: Im Interesse eines vollständigen und sauberen Neustarts für diese
wichtige Richtlinie - und um ein maximales öffentliches Vertrauen in
die Transparenz und die Verlässlichkeit der Europäischen Institutionen
zu sichern, bitten wird respektvoll darum dass sie die
Generaldirektion Binnenmarkt aus der internen Führerschaft bei der
Bewertung der Neustartsentscheidung und aller weiteren Vorgänge in
diesem Verfahren entlassen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatxatra.el ;
# mailto: alexander.stohr@gmx.de ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: LtrMepsCec0503 ;
# txtlang: de ;
# multlin: t ;
# End: ;

