<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Bbd: Brief von Hartmut Pilch an Dr. Dietrich Welp (BMJ).  Berichtet von neuen Vorschlägen der Dänischen Ratspräsidentschaft, den %(q:technischen Beitrag) als genau dann gegeben zu definieren, wenn ein technischer Beitrag vorhanden ist.  Dieser dänische Vorschlag wird dem Ministerrat am Freitag, den 13. September präsentiert, in dem Welp die Bundesregierung vertritt.  Welp ließ sich von dem Brief nicht beirren und wirkte vielmehr mit einigem Erfolg auf die Legalisierung von Programm-Ansprüchen hin, vor der der EG-Kommissions-Entwurf noch zurückschreckt.

#MMb: Müntefering stellt im Namen der SPD %(q:weitgehende Übereinstimmung) mit der Patentlobby fest und will durch Kodifizierung der bestehenden Rechtsprechung %(q:die Patentierbarkeit von Software auf eine sichere Rechtsgrundlage stellen).  FDP unentschieden, wie gegenüber CZ.  Grüne kritisieren bestehende Rechtsprechung und wollen Nicht-Patentierbarkeit von Software bekräftigen.  CDU/CSU schließt sich vorsichtig der grünen Position an.

#Pro: Positionen der Parteien zu Swpat-Forderungen des DMMV

#Aie: Auf einer Fachtagung in Amsterdam wurde dem Autor klar, welche Kräfte hinter den Brüsseler Gesetzesinitiativen für die grenzenlose Patentierbarkeit stehen.  Es kamen auch weitere Details über die Politik des BMJ ans Licht.  Derweil offenbarten medienpolitische Sprecher der Parteien gegenüber der CZ ihre Positionen.  Eine klare und begründete Position kann der Autor nur bei den Grünen ausmachen.

#Hha: Hier kann man sich anmelden, ein wenig Auskunft über sich geben und den FFII moralisch oder durch Mitgliedschaft unterstützen.  Alle Unterstützer (ab Jahresbeitrag 0 EUR) erhalten Zugang zum www- und mail-basierten Möglichkeiten, Appelle zu unterzeichnen und an Umfragen u.dgl. mitzuwirken.  Das System ist noch im Beta-Stadium.

#EWs: Eintrag in die Unterstützerliste

#Hns: Helfen Sie uns, einen öffentlichen Wettbewerb zu diesem Thema zu organisieren!

#Ssa: Schreiben Sie an die Bundesregierung (s.o.) und verlangen Sie weitere Auskünfte.

#Uee: Untersuchen Sie ein paar Web-Angebote der Bundesregierung auf Verletzung der oben aufgelisteten Patente hin.

#Sbi: Suchen Sie in dem %(mf:Minenfeld) und beschreiben Sie einzelne Patente in ähnlicher Weise, wie wir es in unserer %(sm:Beispielsammlung) tun.  Schicken Sie uns eine Beschreibung in Form eines schlichten Textes.

#Pnr: Patentverletzungen der Bundesregierung dokumentieren.

#LAs: Lassen Sie sich nicht entmutigen.  Auch eine fehlende Antwort (wie etwa die von Däubler-Gmelin gegenüber Tauss) ist eine Antwort.

#Sns: Senden Sie auch uns Ihren Text!  Am besten drucken Sie schon vorher die Papiere aus, und beschriften und frankieren den Umschlag.  Wir wollen diejenigen Texte sammeln (und bei Bedarf auch polieren helfen), die auch tatsächlich in Papierform abgesandt werden.

#IKw: In Ihrem Brief sollten Sie vor allem Fragen stellen.  Sofern Sie darüber hinaus die Eindringlichkeit unserer Forderungen unterstreichen wolllen, helfen eigene Erfahrungsberichte mehr als abstrakte Aussagen.

#DzW: Die %(pb:PDF-Version dieses Dokumentes), eventuell zusammen mit weiteren Dokumenten (z.B. dem %(tp:Telepolis-Bericht) und der %(pc:PDF-Version des Aufrufs zum Handeln)) ausdrucken und zusammen mit einem eigenen auf Papier geschriebenen Brief an einen oder mehrere der genannten Adressaten senden.

#eie: elektronisch unterzeichnen

#ntu: noch weiter auszuarbeiten

#gcr: insgesamt %(N) Personen

#mte: dynamische Unterzeichnerliste

#fed: Gründer und Besitzer

#GvK: Geschäftsführer

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#vWs: stellvertretender Vorsitzender

#pse: Präsident

#Mue: Mit freundlichen Grüßen

#Unj: Bisherige Briefe und Appelle aus unserem Kreis an die Bundesregierung in dieser Sache wurden an das BMJ-Patentreferat weitergeleitet und dann nicht beantwortet.  Selbst ein %(mt:offener Brief von MdB Tauss an die Bundesjustizministerin) wurde, soweit uns bekannt, nicht beantwortet. Wir hoffen diesmal auf eine Antwort von Politikern, die die zentrale ordnungspolitische Bedeutung der zur Diskussion stehenden Frage erkannt haben und bereit sind, Verantwortung zu übernehmen.

#Dee: Die Position der Bundesregierung und des Bundestages muss von Leuten erarbeitet werden, die verstehen, wie Software-Entwicklung funktioniert.  Juristisches Fachwissen allein genügt nicht.  Im Europäischen Ministerrat sollten Personen für unser Land sprechen, die in unserer Software-Fachwelt Ansehen als Experten in Fragen der Software-Ökonomie und des optimalen Schutzumfangs für Software-Schöpfungen genießen und denen man zutrauen kann, dass sie die vom Bundestag und der Bundesregierung erarbeiteten Positionen kraftvoll vertreten.

#WuW: Wir haben zum Logikpatent-Richtlinienvorschlag der Europäischen Kommission einen umfassenden %(gv:Gegenvorschlag) ausgearbeitet.  %(zp:Zahlreiche angesehene Personen und Organisationen der Software-Branche unterstützen diesen Gegenvorschlag).  125000 Unterzeichner und 400 Firmen unterstützen eine %(ep:Petition) in ähnlichem Sinne.  Wir schlagen vor, dass die Bundesregierung sich unseren Gegenvorschlag vorläufig zu eigen macht oder zumindest so lange in Brüssel als dialektischen Gegenpol ins Gespräch bringt, wie sie keine eigene ebenso klare und begründete Position eingenommen hat.

#DeW: Die Bundesregierung sollte ihre Position zum rechtlichen Status von Logikpatenten %(bp:anhand von Beispielpatenten klar stellen).  Erst wenn es eine deutsche Position gibt, kann das BMJ diese Position im %(re:Rat der Europäischen Union) vertreten.  Zwischen nicht vorhandenen Positionen gibt es auch nichts zu harmonisieren.  Auch die Opposition sollte sich ihrer Verantwortung stellen.  Aus den bisherigen Äußerungen beider Seiten spricht kaum mehr als die hohe Kunst des delphischen Orakels.

#Dee2: Die Bundesregierung sollte darüber Rechenschaft ablegen, welche patentierten Organisations- und Rechenregeln sie auf ihren Web-Seiten verwendet, ob sie die Patente für rechtmäßig hält und welche Lizenzen sie dafür gegebenenfalls erworben hat.  Ferner sollte sie klären, zu welchen Konditionen Normalbürger, insbesondere solche, die selbst geschriebene oder frei verfügbare Software einsetzen, in den Genuss ähnlicher Lizenzen kommen können.

#Inf: Im Zusammenhang der aktuellen Diskussion über den %(eb:Vorschlag der Europäischen Kommission zur Patentierbarkeit computer-implementierter Organisations- und Rechenregeln vom 20. Februar 2002) fordern wir von der Bundesregierung folgendes:

#Dtn: Die Bundesregierung %(bm:setzt sich derzeit in der Europäischen Union für die Patentierbarkeit grundlegender Regeln des Denkens, Rechnens und Organisierens ein).  Zugleich bedient sich die Bundesregierung auf ihren Webseiten selbst einiger der über 30000 Organisations- und Rechenregeln, auf die das Europäische Patentamt bislang gegen den Buchstaben und Geist der geltenden Gesetze Patente erteilt hat.

#VEK: Vor allem der Rechtsausschuss des Bundestages wird mit dem Fall befasst sein.  Eigentlich geht es aber nicht um rechtstechnische Details, und die Ausschüsse für Wirtschaftspolitik und Neue Medien sollten sich für zuständig erklären.

#Adn: Abgeordnete des Deutschen Bundestages

#PSf: Parlamentarischer Staatssekretär Wolf-Michael Catenhusen

#Bsd: Bundesbildungsministerin Edelgard Bulmahn

#Dxe: Dr. Axel Gerlach

#DlW: Dr. Alfred Tacke

#BaW: Bundeswirtschaftsminister Werner Müller

#BMJ: Bundesministerium der Justiz

#BKA: Bundeskanzleramt

#Bld: Bundeskanzler Gerhard Schröder

#Vsr: Anhang: Von der Bundesregierung möglicherweise verletzte Logikpatente

#Urc: Unterzeichner

#EWB: Sehr geehrte Damen und Herren!

#Aea: Adressaten

#Sfa: Stefan Krempl berichtet über den dänischen %(q:Kompromissvorschlag) und weist dabei auch auf unseren Appell an die Bundesregierung hin.

#Hew: Heise: EU-Rat macht neuen Vorstoß zur Softwarepatent-Richtlinie

#Pcn: Peter Mühlbauer berichtet über unsere Aktion und weist auf Gegensätze zwischen den Konzeptionen der Bundesregierung zum Abbau der Arbeitslosigkeit (Förderung der Selbstständigkeit, %(q:Ich-AG)) und einer Patentpolitik, die Erfahrungen der Schwerindustrie auf Rechenregeln überträgt und sinnlose Kapitalkonzentration erzwingt.

#Ace: Artikel von Peter Mühlbauer über den Schlingerkurs der Bundesregierung und weiterer Politiker in Sachen Logikpatente

#T0l: Telepolis 2002-07: Befehl aus Brüssel?

#npe: Nils Baggehufwudt, der im Bundesministerium für Wirtschaft und Technologie (BMWi) für die Patentpolitik zuständig ist, vertritt in diesem Antwortschreiben, welches er auf Weisung des neuen Ministers Wolfgang Clement an einen besorgten Softwarenetwickler schickte, die (von offiziellen Positionen deutscher Gerichte abweichende) Extremposition einiger Patentanwälte, wonach Art 52 EPÜ überhaupt keinerlei Beschränkung der Patentierbarkeit darstellt, und erklärt, dass die Bundesregierung in Brüssel bemüht ist, diese Position durchzusetzen.  Hierzu gehört auch die Forderung, dass auch Text-Ansprüche (d.h. Ansprüche auf %(q:einen Programmtext / eine Datenstruktur, dadurch gekennzeichnet dass bei Ladung in den Arbeitsspeicher eines Rechners %(q:technische Effekte) erzielt werden)).  Mit diesem Brief machte Baggehufwudt bekannt, was wir schon vorher wussten:  dass auch das BMWi jederzeit an vorderster Front mit von der Partie ist, wenn es darum geht, in Brüssel vorstellig zu werden, um im Dienste der Patentlobby die Grundrechte und Überlebensinteressen der deutschen Softwareentwickler mit Füßen zu treten.  S. auch den folgenden Dialog.

#ksk: BMWi 2002-12-06 an Marcus Brinkmann: Bundesregierung in Brüssel für Logik- und Textpatente

#descr: Die Bundesregierung setzt sich in Brüssel für die Legalisierung von Logikpatenten ein, die sie selbst verletzt.  Sie hat bislang keinen Standpunkt zum Brüsseler Softwarepatent-Richtlinienentwurf veröffentlicht.  Während im Bundestag und in den Ministerien insgesamt Skepsis vorherrscht, drängt das federführende Patentreferat im Bundesministerium der Justiz (BMJ) den Rat der Europäischen Union (REU) zur Verabschiedung eines %(q:Kompromisses) zwischen dem Kommissions-Entwurf und den noch weiter gehenden Wünschen der Patentämter und ihrer Beiräte aus Großkonzern-Patentjuristen.  Demnach soll nicht erst die Ausführung sondern bereits die Veröffentlichung eines eigenständig entwickelten Computerprogramms strafbar werden.  Dabei ignoriert die Bundesregierung alle Studien und Diskussionen der letzten Jahre, einschließlich der von der Bundesregierung selbst in Auftrag gegebenen.  Die Unterzeichner richten vier Forderungen an die Bundesregierung.

#title: Appell an die Bundesregierung

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxbund028 ;
# txtlang: de ;
# multlin: t ;
# End: ;

