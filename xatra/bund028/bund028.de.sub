\begin{subdocument}{swxbund028}{Appell an die Bundesregierung}{http://swpat.ffii.org/briefe/bund028/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die Bundesregierung setzt sich in Br\"{u}ssel f\"{u}r die Legalisierung von Logikpatenten ein, die sie selbst verletzt.  Sie hat bislang keinen Standpunkt zum Br\"{u}sseler Softwarepatent-Richtlinienentwurf ver\"{o}ffentlicht.  W\"{a}hrend in im Bundestag und in den Ministerien insgesamt Skepsis vorherrscht, dr\"{a}ngen die federf\"{u}hrenden Patentjuristen unter Frau D\"{a}ubler-Gmelin den Europ\"{a}ischen Rat zur Verabschiedung eines noch vorbehaltloser die Patentierbarkeit von Computerprogrammen bejahenden Entwurfes.  Die Unterzeichner richten vier Forderungen an die Bundesregierung.}
\begin{sect}{adr}{Adressaten}
\begin{description}
\item[Bundeskanzler Gerhard Schr\"{o}der:]\ Bundeskanzleramt
10557 Berlin, Willy-Brandt-Str. 1
\item[Bundesministerium der Justiz:]\ Patentreferat: Dr. Dietrich Welp, Dr. Raimund Lutz, Dr. Elmar Hucko
10117 Berlin\\
Jerusalemer Str. 27
\item[Bundeswirtschaftsminister Werner M\"{u}ller:]\ Parlamentarische Staatssekret\"{a}re: Dr. Alfred Tacke: Dr. Axel Gerlach
Bundesministerium f\"{u}r Wirtschaft und Technologie
10115 Berlin, Scharnhorststra{\ss}e 34-37
\item[Bundesbildungsministerin Edelgard Bulmahn:]\ Parlamentarischer Staatssekret\"{a}r Wolf-Michael Catenhusen
Bundesministerium f\"{u}r Bildung und Forschung, Hannoversche Stra{\ss}e 30, 10115 Berlin
\item[Abgeordnete des Deutschen Bundestages:]\ 11011 Berlin, Unter den Linden 50
Vor allem der Rechtsausschuss des Bundestages wird mit dem Fall befasst sein.  Eigentlich geht es aber nicht um rechtstechnische Details, und die Aussch\"{u}sse f\"{u}r Wirtschaftspolitik und Neue Medien sollten sich f\"{u}r zust\"{a}ndig erkl\"{a}ren.
\end{description}
\end{sect}\begin{sect}{draft}{Sehr geehrte Damen und Herren!}
Die Bundesregierung setzt sich derzeit in der Europ\"{a}ischen Union f\"{u}r die Patentierbarkeit grundlegender Regeln des Denkens, Rechnens und Organisierens ein\footnote{http://swpat.ffii.org/akteure/bmj/index.de.html}.  Zugleich bedient sich die Bundesregierung auf ihren Webseiten selbst einiger der \"{u}ber 30000 Organisations- und Rechenregeln, auf die das Europ\"{a}ische Patentamt bislang gegen den Buchstaben und Geist der geltenden Gesetze Patente erteilt hat.

Im Zusammenhang der aktuellen Diskussion \"{u}ber den Vorschlag der Europ\"{a}ischen Kommission zur Patentierbarkeit computer-implementierter Organisations- und Rechenregeln vom 20. Februar 2002\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html} fordern wir von der Bundesregierung folgendes:

\begin{enumerate}
\item
Die Bundesregierung sollte dar\"{u}ber Rechenschaft ablegen, welche patentierten Organisations- und Rechenregeln sie auf ihren Web-Seiten verwendet, ob sie die Patente f\"{u}r rechtm\"{a}{\ss}ig h\"{a}lt und welche Lizenzen sie daf\"{u}r gegebenenfalls erworben hat.  Ferner sollte sie kl\"{a}ren, zu welchen Konditionen Normalb\"{u}rger, insbesondere solche, die selbst geschriebene oder frei verf\"{u}gbare Software einsetzen, in den Genuss \"{a}hnlicher Lizenzen kommen k\"{o}nnen.

\item
Die Bundesregierung sollte ihre Position zum rechtlichen Status von Logikpatenten anhand von Beispielpatenten klar stellen\footnote{http://swpat.ffii.org/analyse/testsuite/index.de.html}.  Erst wenn es eine deutsche Position gibt, kann das BMJ diese Position im Rat der Europ\"{a}ischen Union\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/index.de.html} vertreten.  Zwischen nicht vorhandenen Positionen gibt es auch nichts zu harmonisieren.  Auch die Opposition sollte sich ihrer Verantwortung stellen.  Aus den bisherigen \"{A}u{\ss}erungen beider Seiten spricht kaum mehr als die hohe Kunst des delphischen Orakels.

\item
Wir haben zum Logikpatent-Richtlinienvorschlag der Europ\"{a}ischen Kommission einen umfassenden Gegenvorschlag\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html\#prop} ausgearbeitet.  Zahlreiche angesehene Personen und Organisationen der Software-Branche unterst\"{u}tzen diesen Gegenvorschlag\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/appell/index.de.html}.  125000 Unterzeichner und 400 Firmen unterst\"{u}tzen eine Petition\footnote{http://petition.eurolinux.org/index.de.html} in \"{a}hnlichem Sinne.  Wir schlagen vor, dass die Bundesregierung sich unseren Gegenvorschlag vorl\"{a}ufig zu eigen macht oder zumindest so lange in Br\"{u}ssel als dialektischen Gegenpol ins Gespr\"{a}ch bringt, wie sie keine eigene ebenso klare und begr\"{u}ndete Position eingenommen hat.

\item
Die Position der Bundesregierung und des Bundestages muss von Leuten erarbeitet werden, die verstehen, wie Software-Entwicklung funktioniert.  Juristisches Fachwissen allein gen\"{u}gt nicht.  Im Europ\"{a}ischen Ministerrat sollten Personen f\"{u}r unser Land sprechen, die in unserer Software-Fachwelt Ansehen als Experten in Fragen der Software-\"{O}konomie und des optimalen Schutzumfangs f\"{u}r Software-Sch\"{o}pfungen genie{\ss}en und denen man zutrauen kann, dass sie die vom Bundestag und der Bundesregierung erarbeiteten Positionen kraftvoll vertreten.
\end{enumerate}

Bisherige Briefe und Appelle aus unserem Kreis an die Bundesregierung in dieser Sache wurden an das BMJ-Patentreferat weitergeleitet und dann nicht beantwortet.  Selbst ein offener Brief von MdB Tauss an die Bundesjustizministerin\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tauss020312/index.de.html} wurde, soweit uns bekannt, nicht beantwortet. Wir hoffen diesmal auf eine Antwort von Politikern, die die zentrale ordnungspolitische Bedeutung der zur Diskussion stehenden Frage erkannt haben und bereit sind, Verantwortung zu \"{u}bernehmen.

Mit freundlichen Gr\"{u}{\ss}en
\end{sect}\begin{sect}{sign}{Unterzeichner}
insgesamt \includefile{/ul/sig/srv/res/www/ffii/swpat/briefe/bund028/ffiibund028nac.tex} Personen

\begin{center}
Hartmut Pilch (F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V.\footnote{http://www.ffii.org/index.de.html})

RA J\"{u}rgen Siepmann und Daniel Riek (Linux-Verband LiVe e.V.\footnote{http://www.linux-verband.de})

Oliver Zendel (Linuxtag e.V.\footnote{http://www.linuxtag.de/})

Georg Greve (Pr\"{a}sident, Free Software Foundation Europe\footnote{http://www.fsfeurope.org/})

Axel Schudak, Arne Brand und Boris Piwinger (Virtueller Ortsverein der SPD\footnote{http://www.vov.de})

Dr. Frank Dittmann (Gesellschaft f\"{u}r Kybernetik e.V.\footnote{http://www.gesellschaft-fuer-kybernetik.org/})

Christian Lademann (stellvertretender Vorsitzender, German Unix User Group\footnote{http://www.guug.de/})

Prof. Dr. Clemens Cap (Informatik, Uni Rostock)

Markus DeWendt (Gesch\"{a}ftsf\"{u}hrer, Open Logic Systems\footnote{http://www.Open-LS.de/})

Dr. Peter Gerwinski (G-N-U GmbH\footnote{http://www.g-n-u.de/}, Gesch\"{a}ftsf\"{u}hrer)

Prof. Dr. iur. Christian Gizewski (Technische Univers\"{a}t Berlin, Aktion gegen \"{O}ffentliche Sprachregelungen und Kommunikationserschwernisse\footnote{http://www.tu-berlin.de/fb1/AGiW/Cricetus/SOzuC1/Aktion.htm})

Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Gesch\"{a}ftsf\"{u}hrer)

G\"{u}nter Fuchs (Gesch\"{a}ftsf\"{u}hrer, Internet Service Fuchs KEG\footnote{http://www.service-fuchs.com})

Kurt Jaeger (Oberon.net GmbH\footnote{http://www.oberon.net} und LF.net GmbH\footnote{http://www.lf.net}, Stuttgart, Gesch\"{a}ftsf\"{u}hrer)

Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, Gr\"{u}nder und Besitzer)

Stephan K\"{o}rner (Gesch\"{a}ftsf\"{u}hrer, Pilum Technology GmbH)

Walter L\"{u}ckemann (Gesch\"{a}ftsf\"{u}hrer, TiboSoft GmbH\footnote{http://www.tibosoft.de})

Dr. Karl-Friedrich Lenz\footnote{http://www.k.lenz.name/} (Professor (ky\^{o}ju) f\"{u}r Deutsches und Europ\"{a}isches Recht, Univ. Aoyama Gakuin, Japan)

Stephan Nobis (Gesch\"{a}ftsf\"{u}hrer, ESN GmbH\footnote{http://www.netz-administration.de})

Prof. on. Siegfried Piotrowski (Europa-Klub e.V.\footnote{http://www.europaklub.de/})

Stefan Pollmeier (ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat}, Gesch\"{a}ftsf\"{u}hrer)

Bernhard Reiter (Intevation GmbH\footnote{http://www.intevation.de/})

Matthias Schlegel (Vorstand, Phaidros AG\footnote{http://www.phaidros.com/})

\dots
\end{center}
\end{sect}\begin{sect}{epat}{Anhang: Von der Bundesregierung m\"{o}glicherweise verletzte Logikpatente}
EP0895689

EP0266049: Patente auf unscharfe Kompression in JPEG u.a.\footnote{http://swpat.ffii.org/patente/wirkungen/jpeg/index.de.html}

\dots

Europ\"{a}ische Softwarepatente: Einige Musterexemplare\footnote{http://swpat.ffii.org/patente/muster/index.de.html}

noch weiter auszuarbeiten
\end{sect}\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf elektronisch unterzeichnen}}
\filbreak

\item
{\bf {\bf Die PDF-Version dieses Dokumentes\footnote{http://swpat.ffii.org/briefe/bund028/bund028.de.pdf}, eventuell zusammen mit weiteren Dokumenten (z.B. dem Telepolis-Bericht\footnote{http://www.heise.de/tp/deutsch/special/copy/13219/1.html} und der PDF-Version des Aufrufs zum Handeln\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/appell/appell.de.pdf}) ausdrucken und zusammen mit einem eigenen auf Papier geschriebenen Brief an einen oder mehrere der genannten Adressaten senden.}}

\begin{quote}
In Ihrem Brief sollten Sie vor allem Fragen stellen.  Sofern Sie dar\"{u}ber hinaus die Eindringlichkeit unserer Forderungen unterstreichen wolllen, helfen eigene Erfahrungsberichte mehr als abstrakte Aussagen.

Senden Sie auch uns Ihren Text!  Am besten drucken Sie schon vorher die Papiere aus, und beschriften und frankieren den Umschlag.  Wir wollen diejenigen Texte sammeln (und bei Bedarf auch polieren helfen), die auch tats\"{a}chlich in Papierform abgesandt werden.

Lassen Sie sich nicht entmutigen.  Auch eine fehlende Antwort (wie etwa die von D\"{a}ubler-Gmelin gegen\"{u}ber Tauss) ist eine Antwort.
\end{quote}
\filbreak

\item
{\bf {\bf Patentverletzungen der Bundesregierung dokumentieren.}}

\begin{quote}
\begin{enumerate}
\item
Suchen Sie in dem Minenfeld\footnote{http://swpat.ffii.org/patente/txt/index.en.html} und beschreiben Sie einzelne Patente in \"{a}hnlicher Weise, wie wir es in unserer Beispielsammlung\footnote{http://swpat.ffii.org/patente/muster/index.de.html} tun.  Schicken Sie uns eine Beschreibung in Form eines schlichten Textes.

\item
Untersuchen Sie ein paar Web-Angebote der Bundesregierung auf Verletzung der oben aufgelisteten Patente hin.

\item
Schreiben Sie an die Bundesregierung (s.o.) und verlangen Sie weitere Ausk\"{u}nfte.

\item
Helfen Sie uns, einen \"{o}ffentlichen Wettbewerb zu diesem Thema zu organisieren!
\end{enumerate}
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{links}{Kommentierte Verweise}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Eintrag in die Unterst\"{u}tzerliste\footnote{http://www.ffii.org/ffii-cgi/eintrag?f=mcbund028}}}

\begin{quote}
Hier kann man sich anmelden, ein wenig Auskunft \"{u}ber sich geben und den FFII moralisch oder durch Mitgliedschaft unterst\"{u}tzen.  Alle Unterst\"{u}tzer (ab Jahresbeitrag 0 EUR) erhalten Zugang zum www- und mail-basierten M\"{o}glichkeiten, Appelle zu unterzeichnen und an Umfragen u.dgl. mitzuwirken.  Das System ist noch im Beta-Stadium.
\end{quote}
\filbreak

\item
{\bf {\bf BMWi-Erkl\"{a}rungen f\"{u}r \"{A}nderungsvorschl\"{a}ge der REU-Patent-Arbeitsgruppe.\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Dec/0048.html}}}

\begin{quote}
Mitte November ver\"{o}ffentlichte die d\"{a}nische Pr\"{a}sidentschaft des Europ\"{a}ischen Rates einige \"{A}nderungsvorschl\"{a}ge zum Entwurf einer Richtlinie f\"{u}r die Patentierbarkeit von Programmen f\"{u}r Datenverarbeitungsanlagen (computer-implementierten Organisations- und Rechenregeln).  Das BMWi setzt sich Ende November gegen\"{u}ber dem Europ\"{a}ischen Rat daf\"{u}r ein, \"{u}ber die Vorschl\"{a}ge der Europ\"{a}ischen Kommission hinaus auch noch Anspr\"{u}che auf ``Computerprogrammprodukte'' zuzulassen und versucht, irrationale ``\"{A}ngste der Opensource-Bewegung'' und Bedenken der Europ\"{a}ischen Kommission zu zerstreuen, wonach hiermit ``Programme als solche'' patentiert w\"{u}rden.  Hartmut Pilch zeigt Abschnitt f\"{u}r Abschnitt die Unwahrheiten auf, welche in diesem BMWi-Dokument stecken.
\end{quote}
\filbreak

\item
{\bf {\bf BMWi 2002-12-06 an Marcus Brinkmann: Bundesregierung in Br\"{u}ssel f\"{u}r Logik- und Textpatente\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Dec/0032.html}}}

\begin{quote}
Nils Baggehufwudt, der im Bundesministerium f\"{u}r Wirtschaft und Technologie (BMWi) f\"{u}r die Patentpolitik zust\"{a}ndig ist, vertritt in diesem Antwortschreiben, welches er auf Weisung des neuen Ministers Wolfgang Clement an einen besorgten Softwarenetwickler schickte, die (von offiziellen Positionen deutscher Gerichte abweichende) Extremposition einiger Patentanw\"{a}lte, wonach Art 52 EP\"{U} \"{u}berhaupt keinerlei Beschr\"{a}nkung der Patentierbarkeit darstellt, und erkl\"{a}rt, dass die Bundesregierung in Br\"{u}ssel bem\"{u}ht ist, diese Position durchzusetzen.  Hierzu geh\"{o}rt auch die Forderung, dass auch Text-Anspr\"{u}che (d.h. Anspr\"{u}che auf ``ein Computerprogramm / eine Datenstruktur [ auf Datentr\"{a}ger ], dadurch gekennzeichnet dass bei ihrer Ladung in den Arbeitsspeicher eines Rechners technische Effekte erzielt (d.h. durch Anwendung neuartiger Rechenregeln Rechenressourcen eingespart) werden'') legalisiert werden m\"{u}ssen.  Mit diesem Brief machte Baggehufwudt bekannt, was wir schon vorher wussten:  dass auch das BMWi jederzeit an vorderster Front mit von der Partie ist, wenn es darum geht, in Br\"{u}ssel vorstellig zu werden, um im Dienste der Patentlobby die Grundrechte und \"{U}berlebensinteressen der deutschen Softwareentwickler mit F\"{u}{\ss}en zu treten.  S. auch den folgenden Dialog.
\end{quote}
\filbreak

\item
{\bf {\bf Telepolis 2002-09-11: Bundesregierung f\"{o}rdert und verletzt Logikpatente\footnote{http://www.heise.de/tp/deutsch/special/copy/13219/1.html}}}

\begin{quote}
Peter M\"{u}hlbauer berichtet \"{u}ber unsere Aktion und weist auf Gegens\"{a}tze zwischen den Konzeptionen der Bundesregierung zum Abbau der Arbeitslosigkeit (F\"{o}rderung der Selbstst\"{a}ndigkeit, ``Ich-AG'') und einer Patentpolitik, die Erfahrungen der Schwerindustrie auf Rechenregeln \"{u}bertr\"{a}gt und sinnlose Kapitalkonzentration erzwingt.

siehe auch Appell an die Bundesregierung\footnote{}
\end{quote}
\filbreak

\item
{\bf {\bf Heise: EU-Rat macht neuen Vorsto{\ss} zur Softwarepatent-Richtlinie\footnote{http://www.heise.de/newsticker/data/jk-04.10.02-001/}}}

\begin{quote}
Stefan Krempl berichtet \"{u}ber den d\"{a}nischen ``Kompromissvorschlag'' und weist dabei auch auf unseren Appell an die Bundesregierung hin.

siehe auch CEU/DKPTO 2002/09/23: Amended Software Patentability Directive Proposal\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/index.de.html} und 
\end{quote}
\filbreak

\item
{\bf {\bf Aufruf zum Handeln\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/appell/index.de.html}}}

\begin{quote}
Der Richtlinienvorschlag der Europ\"{a}ischen Kommission f\"{u}r die Patentierbarkeit von Software-Innovationen erfordert eine klare Antwort vom Europ\"{a}ischen Parlament, den nationalen Regierungen und anderen Akteuren.  Hier erkl\"{a}ren wir, was unserer Meinung nach zu tun ist.
\end{quote}
\filbreak

\item
{\bf {\bf Gruselkabinett der Europ\"{a}ischen Softwarepatente\footnote{http://swpat.ffii.org/patente/index.de.html}}}

\begin{quote}
Eine Datenbank der Monopole auf Programmieraufgaben, die das Europ\"{a}ischen Patentamt gegen den Buchstaben und Geist der geltenden Gesetze massenweise gew\"{a}hrt hat, und \"{u}ber die es die \"{O}ffentlichkeit nur h\"{a}ppchenweise informiert.  Die Softwarepatent-Arbeitsgruppe des FFII versucht, die Softwarepatente herauszusuchen, besser zug\"{a}nglich zu machen, und ihre Wirkungen auf die Softwareentwicklung zu zeigen.
\end{quote}
\filbreak

\item
{\bf {\bf BMJ: \"{A}ngstlicher Vorreiter der Patentinflation in DE und EU\footnote{http://swpat.ffii.org/akteure/bmj/index.de.html}}}

\begin{quote}
F\"{u}r alle Gesetzgebung im Bereich des Patentwesens ist innerhalb der Bundesregierung das Bundesministerium der Justiz (BMJ) zust\"{a}ndig.  Das BMJ unterh\"{a}lt in seiner Abteilung f\"{u}r Industrie und Handel ein eigenes Patentreferat.  Zum Gesch\"{a}ftsbereich des BMJ geh\"{o}ren ferner das Deutsche Patent- und Markenamt (DPMA), das Bundespatentgericht (BPatG) und der Bundesgerichtshof (BGH).  Zu all diesen Organisationen und auch zum Europ\"{a}ischen Patentamt (EPA) hin bestehen innige personelle Verflechtungen.  Das BMJ-Patentreferat ist personell schwach ausgestattet und verl\"{a}sst sich daher auf EPA, DPMA und andere Patentbewegungs-Institutionen.  Die Karierre der BMJ-Beamten verl\"{a}uft h\"{a}ufig innerhalb des Patentwesens.  Sie folgen im allgemeinen (aus Gewohnheit) wortgetreu deren (von wenigen Instanzen beschlossener) herrschender Meinung und beschr\"{a}nken die argumentative Auseinandersetzung meist von vorneherein auf grammatische Fragen.  Innerhalb der Bundesregierung und des Europ\"{a}ischen Rates vertreten sie de facto die Interessen der Patentinstitutionen, und sie tun dies unter Einsatz ihrer gesetzgeberischen Macht.
\end{quote}
\filbreak

\item
{\bf {\bf Herta D\"{a}ubler-Gmelin und Logikpatente: vehement dagegen, beharrlich daf\"{u}r\footnote{http://swpat.ffii.org/akteure/dgmelin/index.de.html}}}

\begin{quote}
Allen \"{o}ffentlichen \"{A}u{\ss}erungen von 1999 bis 2002 zufolge scheint die Bundesministerin der Justiz nicht nur fest an die ``Belohnungstheorie'' und die a priori segensreichen Wirkungen des Patentwesens zu glauben, sondern sich auch durch eine besonders patentfreundliche Politik profilieren zu wollen.  Bei Gespr\"{a}chen in kleinerem Kreise zeigte sich, dass die Ministerin Schwierigkeiten hat, sich eine Welt vorzustellen, in der nicht jede kreative Idee einzeln vom Staat mit einem Monopol belohnt wird.  Auch in der Frage der Genpatente macht sich die Ministerin, trotz gelegentlicher Skrupel hinsichtlich der Fragen der ethischen Vertretbarkeit von Genmanipulationen, vehement f\"{u}r eine weitgehende Patentierbarkeit stark und scheut dabei auch nicht Dissonanzen innerhalb der Koalition.  Sie scheint generell fast blind dem Rat des M\"{u}nchener Patentpapstes Prof. Straus und der F\"{u}hrung des Deutschen Patentamtes zu vertrauen.  Auch hinsichtlich der Organisations- und Rechenregeln (Software) verfolgte D\"{a}ubler-Gmelin langezeit eine patentfreudige Linie, aber nach einigen harten Diskussionen konnte sie sich im November 2000 dazu durchringen, energisch f\"{u}r eine Vertagung der geplanten \"{A}nderung des EP\"{U} einzutreten.  Im Fr\"{u}hjahr 2001 \"{a}u{\ss}erte D\"{a}ubler-Gmelin f\"{u}r die Presse ``vehemente Kritik'' daran, dass die EU-Kommission ``Programme als solche'' patentierbar machen wolle.  Unter ``Programm als solches'' versteht D.G. jedoch nur den urheberrechtlich gesch\"{u}tzten Ausdruck, dessen Patentierung ohnehin von niemandem angestrebt wird.  Hinter den Kulissen arbeiteten D\"{a}ubler-Gmelins Ministerialbeamte weiter auf eine grenzenlose Patentierbarkeit hin.  In geheimen Stellungnahmen (z.B. der Antwort der Bundesregierung auf die Sondierung von 2000/10 sowie Redebeitr\"{a}gen auf Sitzungen) ermutigten die BMJ-Patentreferenten alle Br\"{u}sseler Initiativen in diese Richtung.  In internen Papieren der EU-Kommission wird die Bundesregierung zusammen mit der Britischen Regierung zu den Proponenten einer m\"{o}glichst unbegrenzten Patentierbarkeit gez\"{a}hlt.  Auf Sitzungen der Arbeitsgruppe f\"{u}r Geistiges Eigentum und Patente des Europ\"{a}ischen Rates setzten sie sich mehrfach daf\"{u}r ein, \"{u}ber den Vorschlag der Europ\"{a}ischen Kommission hinaus auch unmittelbare Patentanspr\"{u}che auf ``Computerprogrammprodukte'' und andere Informationsgegenst\"{a}nde zuzulassen.  Ein offener Brief von MdB Tauss an Frau D\"{a}ubler-Gmelin, der hier Gegensteuerung verlangte, blieb unbeantwortet.  \"{U}berredungsversuche des Virtuellen Ortsvereins der SPD prallten auch hart an Frau D\"{a}ubler-Gmelin ab.
\end{quote}
\filbreak

\item
{\bf {\bf Telepolis 2002-07: Befehl aus Br\"{u}ssel?\footnote{http://www.telepolis.de/deutsch/special/copy/12892/1.html}}}

\begin{quote}
Artikel von Peter M\"{u}hlbauer \"{u}ber den Schlingerkurs der Bundesregierung und weiterer Politiker in Sachen Logikpatente
\end{quote}
\filbreak

\item
{\bf {\bf phm: Amsterdamer Eindr\"{u}cke, BMJ, CZ-Wahlpr\"{u}fsteine\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Sep/0010.html}}}

\begin{quote}
Auf einer Fachtagung in Amsterdam wurde dem Autor klar, welche Kr\"{a}fte hinter den Br\"{u}sseler Gesetzesinitiativen f\"{u}r die grenzenlose Patentierbarkeit stehen.  Es kamen auch weitere Details \"{u}ber die Politik des BMJ ans Licht.  Derweil offenbarten medienpolitische Sprecher der Parteien gegen\"{u}ber der CZ ihre Positionen.  Eine klare und begr\"{u}ndete Position kann der Autor nur bei den Gr\"{u}nen ausmachen.
\end{quote}
\filbreak

\item
{\bf {\bf Positionen der Parteien zu Swpat-Forderungen des DMMV\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Sep/0052.html}}}

\begin{quote}
M\"{u}ntefering stellt im Namen der SPD ``weitgehende \"{U}bereinstimmung'' mit der Patentlobby fest und will durch Kodifizierung der bestehenden Rechtsprechung ``die Patentierbarkeit von Software auf eine sichere Rechtsgrundlage stellen''.  FDP unentschieden, wie gegen\"{u}ber CZ.  Gr\"{u}ne kritisieren bestehende Rechtsprechung und wollen Nicht-Patentierbarkeit von Software bekr\"{a}ftigen.  CDU/CSU schlie{\ss}t sich vorsichtig der gr\"{u}nen Position an.
\end{quote}
\filbreak

\item
{\bf {\bf phm 2002-09-09: Logikpatente-Ratssitzung 2002-09-13: Zirkul\"{a}res aus D\"{a}nemark\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Sep/0020.html}}}

\begin{quote}
Brief von Hartmut Pilch an Dr. Welp (BMJ).  Berichtet von neuen Vorschl\"{a}gen der D\"{a}nischen Ratspr\"{a}sidentschaft, den ``technischen Beitrag'' als genau dann gegeben zu definieren, wenn ein technischer Beitrag vorhanden ist.  Dieser d\"{a}nische Vorschlag wird dem Ministerrat am Freitag, den 13. September pr\"{a}sentiert, in dem Welp die Bundesregierung vertritt.  Welp lie{\ss} sich von dem Brief nicht beirren und wirkte vielmehr mit einigem Erfolg auf die Legalisierung von Programm-Anspr\"{u}chen hin, vor der der EG-Kommissions-Entwurf noch zur\"{u}ckschreckt.
\end{quote}
\filbreak

\item
{\bf {\bf SPD und Softwarepatente\footnote{http://swpat.ffii.org/akteure/spd/index.de.html}}}

\begin{quote}
Die mit Internet und Informatik befassten Abgeordneten der Sozialdemokratischen Partei Deutschlands (SPD), allen voran J\"{o}rg Tauss und Erika Mann, haben der Patentbewegung entscheidenden Widerstand entgegengesetzt.  Insbesondere in den von Tauss einberufene Anh\"{o}rung im Bundestag und gelegentliche Einwirkungen auf die Justizministerin zeigt sich Sachkenntnis und Engagement.  Allerdings pr\"{a}sidierte Tauss zugleich als Bildungsexperte \"{u}ber die Einf\"{u}hrung des Hochschulpatentgesetzes.  Hier rechnete man sich vielleicht geringe Chancen aus, den rollenden Zug noch anhalten zu k\"{o}nnen.  Innerhalb der SPD-Fraktion sind neben der Arbeitsgruppe Neue Medien auch der Rechtsausschuss und der Wirtschaftsausschuss mit der Swpat-Frage befasst.  Der Abgeordnete Dr. med Wolfgang Wodarg hat sich kenntnisreich gegen Genpatente engagiert.  Innerhalb der sozialdemokratischen Fraktionen in Berlin und Stra{\ss}burg gibt es jedoch auch Politiker, die den Patentjuristen vertrauen oder gar in deren Denkgewohnheiten verhaftet sind.  Hierzu geh\"{o}ren in unterschiedlichem Ma{\ss}e die Ministerinnen f\"{u}r Justiz und Bildung sowie einige Mitglieder des Rechtsausschusses, der Generalsekret\"{a}r und vielleicht auch der Kanzler.  M\"{u}ntefering \"{a}u{\ss}erte gegen\"{u}ber dem DMMV, man sei gemeinsam mit diesem f\"{u}r Softwarepatente.  Der Virtuelle Ortsverein (VOV) der SPD hat besonders klare Standpunkte gegen die Patentierbarkeit von Rechenregeln bezogen, und die SPD-Bundestagsfraktion hat sich organisiert mit dem Thema befasst.  Zwar \"{u}berwiegt dabei eine kritische Haltung, aber auch die Bremskr\"{a}fte sind st\"{a}rker als bei manchen anderen Parteien.  Im Ergebnis konnte die Patentbewegung bislang fast ungehindert den Kurs der SPD-gef\"{u}hrten Bundesregierung bestimmen.
\end{quote}
\filbreak

\item
{\bf {\bf CDU/CSU und Softwarepatente\footnote{http://swpat.ffii.org/akteure/cducsu/index.de.html}}}

\begin{quote}
Die mit Internet und Informatik befassten Bundestagsabgeordneten der CDU/CSU, allen voran Dr. Martin Mayer (CSU), haben gegen\"{u}ber Softwarepatenten eine kritische Haltung eingenommen.  Von Mayer stammt der einzige Antrag im Bundestag zu diesem Thema.  Stoibers Unterschrift steht unter einem Papier des Ausschusses der Regionen der Europ\"{a}ischen Gemeinschaft, welches Softwarepatente ablehnt.  Unter dem Vorsitz Michael Hahns von der Jungen Union verabschiedete die Jugendorganisation der Europ\"{a}ischen Volkspartei im Oktober 2000 eine wohl durchdachte Resolution, welche die Patentierbarkeit von Software ablehnt.  Allerdings ver\"{o}ffentlichte sp\"{a}ter die CDU einige (ohne Mayers oder Hahns Zutun) schludrig geschriebene Thesenpapiere, darunter ein ``Strategiepapier'' vom August 2001, welches die Legalisierung von Softwarepatenten mit f\"{u}nfj\"{a}hriger Laufzeit vorschl\"{a}gt und dabei allerlei Grundbegriffe durcheinander bringt.  Von der Bayerischen Staatskanzlei kam etwa zur gleichen Zeit der Vorschlag, zum Zwecke des Jugendschutzes im Internet ``Sendezeiten'' einzuf\"{u}hren.  Nach dem 11. September 2001 erntete die CDU/CSU f\"{u}r allerlei hastige Vorst\"{o}{\ss}e ``gegen Cyber-Terrorismus'' viel Spott von der Fachwelt.  Beim EU/BSA-Richtlinienvorschlag lie{\ss} sich die CDU/CSU mit der Erarbeitung einer Stellungnahme noch Zeit.  Mitte Mai stellte Dr. Mayer ``Kleine Anfrage'', die im wesentlichen richtige Fragen stellt.  Einige patentlastige Pr\"{a}missen wurden durch ein sp\"{a}teres Papier von Krogmann korrigiert, so dass kurz vor der Bundestagswahl die CDU/CSU als die neben den Gr\"{u}nen patentkritischste Partei erschien.
\end{quote}
\filbreak

\item
{\bf {\bf Gr\"{u}ne / B\"{u}ndnis 90 und Softwarepatente\footnote{http://swpat.ffii.org/akteure/gruene/index.de.html}}}

\begin{quote}
Im Sommer und Herbst 2000 bezogen die Gr\"{u}nen deutlich gegen die Patentierung von Rechenregeln Stellung und halfen mit, Druck auf die Regierung auszu\"{u}ben, um die geplante \"{A}nderung von Art 52 EP\"{U} zu verhindern.  W\"{a}hrend es bei den anderen Parteien in f\"{u}hrenden Positionen Juristen gibt, die eher der Autorit\"{a}t von Patent\"{a}mtern und Gerichten als den Argumenten der Patentkritiker trauen, kann man bei den Gr\"{u}nen mit einer ungeteilten Ablehnung von Logikpatenten und einer Bereitschaft zur offenen Kritik am Europ\"{a}ischen Patentamt rechnen.  Die Gr\"{u}nen sind die einzige Partei, die ihre Ablehnung von Softwarepatenten durch einen Parteitagsbeschluss und durch wiederholtes \"{o}ffentliches Engagement bekr\"{a}ftigt hat.
\end{quote}
\filbreak

\item
{\bf {\bf Freie Demokratische Partei (FDP.de) und Logikpatente\footnote{http://swpat.ffii.org/akteure/fdp/index.de.html}}}

\begin{quote}
Auf ihrem D\"{u}sseldorfer Parteitag vom Juni 2001 spricht sich die FDP daf\"{u}r aus, das geistige Eigentum an Programlogik auch k\"{u}nftig ausschlie{\ss}lich \"{u}ber das Urheberrecht zu regeln und kritisiert Bestrebungen, Programme als solche patentierbar zu machen.  Zugleich verspricht sie sich viel von Biopatenten und setzt auf fl\"{a}chendeckenden Einsatz von Kopierschutztechniken an Stelle der GEMA-Geb\"{u}hren.  Die FDP ist neben den Gr\"{u}nen die einzige Partei, die zum Thema Logikpatente auf Parteiebene etwas beschlossen hat.  Allerdings zeigt der medienpolitische Sprecher MdB Otto seit Mitte 2001 Tendenzen, den W\"{u}nschen der Patentlobby entgegenzukommen und nur noch die ``Patentierung von Gesch\"{a}ftsmodellen'' abzulehnen.  Mit den Bestrebungen einiger Bundestagsabgeordneter, im Bundestag GNU/Linux einzuf\"{u}hren, konnte sich Otto nicht so recht anfreunden.  Auf einigen von Microsofts PR-Berater Hunzinger organisierten parlamentarischen Abenden trat Otto als Ehrengast auf.  W\"{a}hrend des Wahlkampfes 2002 nahm er in der Frage der Logikpatente eine unentschiedene Haltung ein.
\end{quote}
\filbreak

\item
{\bf {\bf PDS und Softwarepatente\footnote{http://swpat.ffii.org/akteure/pds/index.de.html}}}

\begin{quote}
Angelika Marquard hat sich im Namen des PDS-Fraktion gegen\"{u}ber der Presse mehrfach deutlich ablehnend gegen\"{u}ber allen Pl\"{a}nen zur Legalisierung von Softwarepatenten ge\"{a}u{\ss}ert.  Insgesamt interessiert sich die PDS aber nicht st\"{a}rker als andere Parteien f\"{u}r die Belange der informationellen Infrastruktur oder f\"{u}r Medienpolitik.  Es hat einzelne mehr oder weniger erfolgreiche Versuche gegeben, das Thema in die PDS zu tragen.  Dabei weckt das Faszinosum ``OpenSource'' offenbar mehr Interesse als die Auseinandersetzung um die Grenzen der Patentierbarkeit.  Die PDS-nahe Rosa-Luxemburg-Stiftung unterst\"{u}tzt das Oekonux-Projekt, welches freie Software als ``freie Kooperation'' begreift und nach Wegen zur weitergehenden \"{U}berwindung der Markt- und Warenwirtschaft sucht.  In wie weit solche ``Plattformen'' Einfluss auf das politische Handeln der PDS gewinnen k\"{o}nnen, bleibt unklar.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente\footnote{http://swpat.ffii.org/termine/2001/bundestag/index.de.html}}}

\begin{quote}
Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte \"{O}ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver\"{o}ffentlichen hier das Gespr\"{a}chsprotokoll und die schriftlichen Eingaben.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
% mode: latex ;
% End: ;

