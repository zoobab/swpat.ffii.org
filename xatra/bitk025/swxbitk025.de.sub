\begin{subdocument}{swxbitk025}{FFII an Bitkom: Gesetzentwürfe an Testsuite messen!}{http://swpat.ffii.org/xatra/bitk025/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/12/19 von FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Am 7. Mai 2002 ver\"{o}ffentlichte der IT-Branchenverband Bitkom eine Presseerkl\"{a}rung, in der er den EU-Richtlinienentwurf f\"{u}r die Patentierbarkeit von ``computer-implementierbaren Erfindungen'' unterst\"{u}tzt. Nach telefonischer Unterhaltung mit Urhebern dieser Presseerkl\"{a}rung schrieb Hartmut Pilch einen Brief an Bitkom, den wir hier ver\"{o}ffentlichen.  Darin erl\"{a}uterte er einen methodologische Konsensposition, die auch den Bitkom-Leuten einzuleuchten schien.}
\begin{sect}{text}{Sehr geehrte Damen und Herren!}
Danke f\"{u}r das Gespr\"{a}ch!

Meine Position noch einmal:

\begin{itemize}
\item
Der EU-Entwurf stellt keine ``hohen Anforderungen an Erfindungen'' sondern beseitigt lediglich die letzten Reste solcher Anforderungen.  S.
\begin{itemize}
\item
Gruselkabinett der Europ\"{a}ischen Softwarepatente\footnote{\url{http://localhost/swpat/pikta/index.de.html}}

\item
EUK/BSA-Entwurf f\"{u}r die Patentierbarkeit aller n\"{u}tzlichen Ideen\footnote{\url{http://localhost/swpat/papri/eubsa-swpat0202/index.de.html}}
\end{itemize}

\item
Ihre PE weist auch sonst schwere handwerkliche und inhaltliche Fehler auf.  Sie richtet sich, soweit aus volkswirtschaftlichen Studien und bekanten Meinungbildern erkennbar, frontal gegen die IT-Branche.

\item
Das Hauptproblem scheint zu sein, dass Bitkom sich eigentlich noch keine fundierte Meinung gebildet hat, dies aber aus Gesichtswahrungsgr\"{u}nden nicht zugeben kann.
\end{itemize}

Angeh\"{a}ngt\footnote{\url{http://localhost/swpat/stidi/manri/index.de.html}} finden Sie die Webseite, die eine gemeinsame Grundlage zwischen Patentgegnern- und -bef\"{u}rwortern f\"{u}r eine rationale Diskussion darstellen k\"{o}nnte.  Wir m\"{u}ssen endlich schwammige Begriffe durch klare Vorstellungen davon ersetzen, wof\"{u}r und wogegen wir eigentlich sind und mit welchen Regeln wir daf\"{u}r sorgen wollen, dass unsere W\"{u}nsche umgesetzt werden.  Wahrscheinlich liegen wir in Wirklichkeit gar nicht weit auseinander.

Statt die Branche zu spalten und externen M\"{a}chten (z.B. EUK, Patentjuristen) blind zu folgen, sollten wir die K\"{o}pfe unserer eigenen Branche anstrengen, um interne Differenzen zu \"{u}berwinden.  An dem derzeitigen Kollisionskurs k\"{o}nnte Bitkom Schaden nehmen.

Ich bitte Sie, aus der Not eine Tugend zu machen und in einem bescheiden-wissenschaftlichen Geist Fragen zu stellen und zu einer aufkl\"{a}renden Diskussion aufzurufen.  Als Anregung k\"{o}nnte der Aufruf des ACM-Vorsitzenden von 1967\footnote{\url{http://localhost/swpat/papri/acm-titus67/index.en.html}} dienen.  Die Verh\"{a}ltnisse haben sich seitdem kaum ge\"{a}ndert.

Sobald Sie den Anspruch, alles zu wissen, wie er aus Ihrer PE spricht, ablegen und konstruktiv an die Sache herangehen, k\"{o}nnen Sie mit tatkr\"{a}ftiger Hilfe von unserer Seite rechnen.

Mit freundlichen Gr\"{u}{\ss}en

\begin{center}
Hartmut Pilch
\end{center}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Bitkom in favor of software patents\footnote{\url{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay&ObjectID=14AA029B-478C-4E0A-874AEA2AAA0EB94C}}}}

\begin{quote}
Der deutsche IT-Branchenverband Bitkom begr\"{u}{\ss}t den Richtlinienentwurf (RiLi-) zur Patentierbarkeit von Software der Europ\"{a}ischen Kommision und der BSA. Bitkom begr\"{u}{\ss}t besonders, da{\ss} die Leistungen von Programmierern unmittelbar dem Patentschutz zug\"{a}nglich sein sollen und fordert, dass konsequenterweise auch unmittelbare Programm-Anspr\"{u}che zugelassen werden m\"{u}ssen, damit diese Software-Patente auch durchsetzbar sind.  In offensichtlichem Widerspruch zu dem RiLi-Entwurfes behauptet Bitkom, dieser stelle einen hohen Standard bei der Erfindungsh\"{o}he sicher.  Da die Softwarebranche immer bedeutender w\"{u}rde und das Patentwesen im Trend der Zeit liege (laut zitierter EPA-Statistik Verzehnfachung der Lizenzgeb\"{u}hren allein in den letzten 10 Jahren), m\"{u}sse nun dringend auch der Softwarebranche dieses neue Wertsch\"{o}pfungspotential erschlossen werden.  Insbesondere kleinen und mittleren Unternehmen biete dies viele neue Chancen.  Die Bitkom-Erkl\"{a}rung wurde auf einer Sitzung des ``Bitkom-Ausschusses f\"{u}r Gewerblichen Rechtschutz und Urheberrecht'' mit 8 Teilnehmern beschlossen. 7 der 8 Teilnehmer waren Gro{\ss}unternehmens-Patentanw\"{a}lte.  Einer war ein  Marketingmitarbeiter eines KMU.  Der Entwurf wurde von der Sprecherin des Ausschusses, Frau Dr. jur. Kathrin Bremer, verfa{\ss}t, und aufgrund der dominanten Sitzungsleitung durch den IBM-Patentanwalt Fritz Teufel verabschiedet. In dem Papier wird nicht der Versuch unternommen, die enthaltenen Behauptungen durch Beispiele aus dem Erfahrungskreis der Mitglieder des Bitkom zu beweisen, auch fehlen jegliche Beispiele f\"{u}r vom Bitkom f\"{u}r sinnvoll oder f\"{u}r schlecht gehaltene Patente. In der Erkl\"{a}rung werden au{\ss}erdem alle wirtschaftlichswissenschaftlichen Studien einfach ignoriert.  In ihrem ersten Entwurf wurde sogar behauptet, dass Softwarepatente Arbeitspl\"{a}tze schaffen.  M\"{o}glicherweise fiel den versammelten Patentanw\"{a}lten dann doch auf, dass dies allenfalls f\"{u}r ihre eigene Branche (der Patentanw\"{a}lte) gilt.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{\url{http://localhost/swpat/stidi/manri/index.de.html}}}}

\begin{quote}
Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu pr\"{u}fen, sollten wir sie an Beispiel-Innovationen ausprobieren.  F\"{u}r jedes Beispiel gibt es einen Stand der Technik, eine technische Lehre und eine Reihe von Anspr\"{u}chen.  In der Annahme, dass die Beispiele zutreffend beschrieben wurden, probieren wir dann unsere neue Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2) Angemessenheit:  f\"{u}hrt die vorgeschlagene Regelung zu einem vorhersagbaren Urteil?  Welche der Anspr\"{u}che w\"{u}rden erteilt?  Entspricht dieses Ergebnis unseren W\"{u}nschen?  Wir probieren verschiedene Gesetzesvorschl\"{a}ge an der gleichen Beispielserie (Testsuite) aus und vergleichen, welches am besten abschneidet.  F\"{u}r Programmierer ist es Ehrensache, dass man ``die Fehler beseitigt, bevor man das Programm freigibt'' (first fix the bugs, then release the code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses Ziels.  Gem\"{a}{\ss} Art. 27 TRIPS geh\"{o}rt die Gesetzgebung zu einem ``Gebiet der Technik'' namens ``Sozialtechnik'' (social engineering), nicht wahr?  Technizit\"{a}t hin oder her, es ist Zeit an die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen, die \"{u}berall dort angesagt ist, wo schlechte Konstruktionsentscheidungen das Leben der Menschen stark beeintr\"{a}chtigen k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf EUK & BSA 2002-02-20: Vorschlag, alle nützlichen Ideen patentierbar zu machen\footnote{\url{http://localhost/swpat/papri/eubsa-swpat0202/index.de.html}}}}

\begin{quote}
Die Europ\"{a}ische Kommission (EUK) schl\"{a}gt vor, die Patentierung von Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren und sicher zu stellen, dass breite und triviale Patente auf Programm- und Gesch\"{a}ftslogik, wie sie derzeit vor allem in den USA von sich reden machen, k\"{u}nftig auch hier in Europa Bestand haben und von keinem Gericht mehr zur\"{u}ckgewiesen werden k\"{o}nnen.  ``Aber hallo, die EUK sagt in ihrer Presseerkl\"{a}rung etwas ganz anderes!'', m\"{o}chten Sie vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK wirklich sagt, m\"{u}ssen Sie n\"{a}mlich nicht die PE sondern den Vorschlag selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom Europ\"{a}ischen Patentamt (EPA) verfasst, in dem gew\"{o}hnliche W\"{o}rter oft das Gegenteil dessen bedeuten, was Sie erwarten w\"{u}rden. Zur Verwirrung tr\"{a}gt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit von Patenten und propriet\"{a}rer Software beschworen wird, wobei dem software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden suggeriert wird.  Dieser Text ignoriert die Meinungen von allen geachteten Programmierern und Wirtschaftswissenschaftlern und st\"{u}tzt seine sp\"{a}rlichen Aussagen \"{u}ber die \"{O}konomie der Software-Entwicklung nur auf zwei unver\"{o}ffentlichte Studien aus dem Umfeld von BSA (von Microsoft und anderen amerikanischen Gro{\ss}unternehmen dominierter Verband zur Durchsetzung des Urheberrechts) \"{u}ber die Wichtigkeit propriet\"{a}rer Software.  Diese Studien haben \"{u}berhaupt nicht Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag selber wurden offensichtlich f\"{u}r die EUK von einem Angestellten von BSA redigiert.  Unten zitieren wir den vollst\"{a}ndigen Vorschlag zusammen mit Belegen f\"{u}r die Rolle von BSA, einer Analyse des Inhalts und einer tabellarischen Gegen\"{u}berstellung der BSA- und EUK-Version sowie einer EP\"{U}-Version, d.h. eines Gegenvorschlages im Geiste des Europ\"{a}ischen Patent\"{u}bereinkommen von 1973 und der aufgekl\"{a}rten Patentliteratur.  Die EP\"{U}-Version sollte Ihnen helfen, die Klarheit und Weisheit der heute g\"{u}ltigen gesetzlichen Regelung verstehen, an deren Aushebelung die Patentanw\"{a}lte der Europ\"{a}ischen Kommission gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet haben.
\end{quote}
\filbreak

\item
{\bf {\bf Gruselkabinett der Europäischen Softwarepatente\footnote{\url{http://localhost/swpat/pikta/index.de.html}}}}

\begin{quote}
Eine Datenbank der Monopole auf Programmieraufgaben, die das Europ\"{a}ischen Patentamt gegen den Buchstaben und Geist der geltenden Gesetze massenweise gew\"{a}hrt hat, und \"{u}ber die es die \"{O}ffentlichkeit nur h\"{a}ppchenweise informiert.  Die Softwarepatent-Arbeitsgruppe des FFII versucht, die Softwarepatente herauszusuchen, besser zug\"{a}nglich zu machen, und ihre Wirkungen auf die Softwareentwicklung zu zeigen.
\end{quote}
\filbreak

\item
{\bf {\bf Vom Teufel geritten --- BITKOM e.V.\footnote{\url{http://localhost/swpat/gasnu/bitkom/index.de.html}}}}

\begin{quote}
Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, daf\"{u}r aber sprang der europ\"{a}ische Dachverband EICTA ein, f\"{u}r den offenbar ebenfalls Teufel die Stellungnahme schrieb.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

