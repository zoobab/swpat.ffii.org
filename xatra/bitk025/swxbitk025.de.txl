<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII an Bitkom: Gesetzentwürfe an Testsuite messen!

#descr: Am 7. Mai 2002 veröffentlichte der IT-Branchenverband Bitkom eine
Presseerklärung, in der er den EU-Richtlinienentwurf für die
Patentierbarkeit von %(q:computer-implementierbaren Erfindungen)
unterstützt. Nach telefonischer Unterhaltung mit Urhebern dieser
Presseerklärung schrieb Hartmut Pilch einen Brief an Bitkom, den wir
hier veröffentlichen.  Darin erläuterte er einen methodologische
Konsensposition, die auch den Bitkom-Leuten einzuleuchten schien.

#SrW: Sehr geehrte Damen und Herren!

#DKG: Danke für das Gespräch!

#Mih: Meine Position noch einmal:

#Dnh: Der EU-Entwurf stellt keine %(q:hohen Anforderungen an Erfindungen)
sondern beseitigt lediglich die letzten Reste solcher Anforderungen. 
S.

#Gdn: Gruselkabinett der Europäischen Softwarepatente

#Eil: EUK/BSA-Entwurf für die Patentierbarkeit aller nützlichen Ideen

#IeW: Ihre PE weist auch sonst schwere handwerkliche und inhaltliche Fehler
auf.  Sie richtet sich, soweit aus volkswirtschaftlichen Studien und
bekanten Meinungbildern erkennbar, frontal gegen die IT-Branche.

#Des: Das Hauptproblem scheint zu sein, dass Bitkom sich eigentlich noch
keine fundierte Meinung gebildet hat, dies aber aus
Gesichtswahrungsgründen nicht zugeben kann.

#csl: %(sm:Angehängt) finden Sie die Webseite, die eine gemeinsame Grundlage
zwischen Patentgegnern- und -befürwortern für eine rationale
Diskussion darstellen könnte.  Wir müssen endlich schwammige Begriffe
durch klare Vorstellungen davon ersetzen, wofür und wogegen wir
eigentlich sind und mit welchen Regeln wir dafür sorgen wollen, dass
unsere Wünsche umgesetzt werden.  Wahrscheinlich liegen wir in
Wirklichkeit gar nicht weit auseinander.

#EPW: Statt die Branche zu spalten und externen Mächten (z.B. EUK,
Patentjuristen) blind zu folgen, sollten wir die Köpfe unserer eigenen
Branche anstrengen, um interne Differenzen zu überwinden.  An dem
derzeitigen Kollisionskurs könnte Bitkom Schaden nehmen.

#IWx: Ich bitte Sie, aus der Not eine Tugend zu machen und in einem
bescheiden-wissenschaftlichen Geist Fragen zu stellen und zu einer
aufklärenden Diskussion aufzurufen.  Als Anregung könnte der
%(ac:Aufruf des ACM-Vorsitzenden von 1967) dienen.  Die Verhältnisse
haben sich seitdem kaum geändert.

#SEk: Sobald Sie den Anspruch, alles zu wissen, wie er aus Ihrer PE spricht,
ablegen und konstruktiv an die Sache herangehen, können Sie mit
tatkräftiger Hilfe von unserer Seite rechnen.

#Mue: Mit freundlichen Grüßen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxbitk025 ;
# txtlang: de ;
# multlin: t ;
# End: ;

