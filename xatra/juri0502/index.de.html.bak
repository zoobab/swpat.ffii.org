<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=iso-8859-15">
	<TITLE>2005-01-31 Open Letter from FFII to MEPs on Pending Decisions</TITLE>
</HEAD>
<BODY LANG="de-DE" TEXT="#ffffff" LINK="#ff9900" VLINK="#ff9900" BGCOLOR="#31454a" DIR="LTR">

<H1>Offener Brief des FFII an MdEP anl�sslich anstehender Entscheidungen �ber die Softwarepatent-Direktive</H1>

<TABLE>
  <TR VALIGN=TOP>
    <TD>Von:</TD>
    <TD><A HREF="http://swpat.ffii.org/">F�rderverein f�r eine Freie Informationelle Infrastruktur e.V.</A> (FFII)<BR>
        B�ro Br�ssel:<BR>
        Rue Michel-Ange 68<BR>
        Tel. +32-273 962 60, +32-485 832 126, +32-486 233 396<BR>
        E-Mail: info at ffii org</TD>
  </TR>
  <TR VALIGN=TOP>
    <TD>An:</TD>
    <TD>Mitglieder des Europ�ischen Parlamentes<BR>
        insbesondere JURI Mitglieder</TD>
  </TR>
  <TR VALIGN=TOP>
    <TD>Datum:</TD>
    <TD>31. Januar 2005</TD>
  </TR>
</TABLE> 

<H2>Absichern der Optionen des Parlamentes in dieser Woche</H2>
Sehr geehrte Mitglieder des Parlamentes,<BR>
<BR>
Diese Woche wird der Rechtsausschuss den Entwurf zur Direktive �ber die Patentierbarkeit computer-implementierter Erfindungen (Softwarepatent-Direktive) mit Kommissar Charlie McCreevy und Bill Gates (Microsoft) diskutieren. Der Ausschuss wird auch zu entscheiden haben, ob die MdEP die M�glichkeit haben werden, nach dem Eingang des Ratsdokumentes im Parlament �ber eine R�ckkehr zur 1. Lesung abzustimmen. 61 MdEP haben diese Recht bereits im Dezember in einer Entschlie�ung nach Regel 55(4) eingefordert, jedoch scheint das f�r die Tagesordnung zust�ndige B�ro des Pr�sidenten es bis heute ohne weitere Begr�ndung zu ignorieren.<BR>
<BR>
Wir sehen mit Besorgnis diese Erosion der Rechte der Parlamentarier einerseits, w�hrend andererseits die Kommission und der Ministerrat neue traurige Pr�zedenzf�lle der Missachtung der parlamentarischen Demokratie schaffen, sowohl auf nationaler als auch auf EU-Ebene. Unter diesen Umst�nden haben die MdEP jeden Grund, auf ihr Recht auf eine Entschlie�ung nach Regel 55(4) zu bestehen, sowie auf ihr Recht, �ber eine R�ckkehr zur 1. Lesung abzustimmen. Die Softwarepatent-Richtlinie ist einer der F�lle, in denen die Aus�bung dieses Rechtes sinnvoll ist.<BR>
<BR>
<H2>Warum die Patentindustrie gegen einen Neustart ist</H2>
Diese Tage haben sie Briefe von Nokia, Ericsson, Acatel, UNICE, und vielen anderen Stakeholdern bekommen, die ihr Gesch�ftsmodell auf das Patentsystem aufbauen, und verlangen, da� sie einer Entschlie�ung nach Regel 55 energisch widerstehen.<BR>
<BR>
Die Begr�ndung f�r diese Forderung ist gew�hnlich, da� sie die 'politische Einigung' des Ministerrates rasch abnicken sollen, weil 'die Industrie' nicht l�nger warten kann. Sie behaupten auch, da� diese Direktive das Patentieren von Software und Gesch�ftmethoden ausschlie�en wird. In internen Memoranden derselben Stakeholder jedoch sieht die Begr�ndung etwas anders aus, zum Beispiel im Schreiben eines nationalen UNICE-Mitgliedes, gesendet an die Patentanwaltkomitees der Verbandsmitglieder:<BR>
<BR>
<TABLE>
  <TR VALIGN=TOP>
    <TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD>
    <TD><I>"The Council has basically maintained its position of November 2002. It has taken on board only a few uncritical amendments from the Parliament. We must make sure that this position is adopted by the Council as soon as possible. Once it is handed to the Parliament for a second reading, the Parliament will most likely be unable to assert any of the amendments of September 2003 in view of the higher majority requirements."</I><BR><TD>
  </TR>
</TABLE> 
<BR>
Die Mehrheitsanforderungen sind in <A HREF="http://www2.europarl.eu.int/omk/sipade2?PUBREF=-//EP//TEXT+RULES-EP+20040720+RULE-062+DOC+XML+V0//EN">Regel 62</A> niedergelegt:<BR>
<BR>
<TABLE>
  <TR VALIGN=TOP>
    <TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD>
    <TD><I><B>Amendments to the Council's common position</B><BR>
    4. An amendment shall be adopted only if it secures the votes of a majority of the component Members of Parliament.</I></TD>
  </TR>
</TABLE> 
<BR>
In anderen Worten: Jeder Parlamentarier der sich in der Abstimmung enth�lt oder fehlt, wird als eine Stimme f�r den Ministerrat gez�hlt.<BR>
<BR>
<H2>Ministerrat: Unqualifizierte Mehrheit f�r ein veraltetes Paket</H2>
Die oben zitierte Analyse des UNICE-Mitgliedes ist korrekt. Als das Parlament im September 2003 seine Verbesserungsvorschl�ge an den Rat weitergab, wurden diese von der vorbereitenden Arbeitsgruppe (die Intellectual Property Working Party, tats�chlich dieselben Leute die im Verwaltungsrat des europ�ischen Patentamtes sitzen!) einfach ignoriert und die vorherige Fassung mehr oder weniger wiedereingesetzt, verziert mit ein paar kosmetischen �nderungen der Rechtsausschuss-Lesung vom Juni 2003, der seinerseits ja bereits von derselben Ratsposition abgeleitet worden war. In den Punkten, die in der Ratsversion noch ungekl�rt waren, wurde die Position des Parlamentes ins Gegenteil verkehrt, und weitere t�uschende Sprachschichten aufgetragen.<BR>
<BR>
Das Ergebnis war der bisher extremste pro-Softwarepatent-Unkompromiss, den das Verfahren bisher sah. Er besteht haupts�chlich aus dem <A HREF="http://wiki.ffii.org/Monti9710En">Vorschlag der Kommission von 1997</A>, das hei�t Harmonisierung mit US-Praktiken der Patentierung von Software und Gesch�ftsmethoden, jedoch in eine scheinbar einschr�nkende Rhetorik verpackt, in der der Ausdruck 'technisch' zugleich �fter und mit weniger Bedeutungsinhalt vorkommt als jemals zuvor. Dank der manipulativen Orchestration bestehend aus Kommissar Bolkestein, der deutschen Delegation und der irischen Ratspr�sidentschaft schaffte dieser Text es durch den <A HREF="http://wiki.ffii.org/Cons040518En">Wettbewerbsausschu� am 18. Mai 2004</A> zu kommen, ohne jemals von einer echten qualifizierten Mehrheit <A HREF="http://swpat.ffii.org/letters/cons0406/">unterst�tzt worden zu sein</A>.<BR>
<BR>
Die nationalen Parlamente erwachten erst sp�t, verabschiedeten dann jedoch einige Resolutionen und trafen sogar bindende Entscheidungen gegen das Ratspapier, f�r die Parlamentsposition. Die Regierungen der Niederlande, von Deutschland und von D�nemark sind nun verpflichtet, ihre Unterst�tzung f�r den Ratsentwurf zur�ckzuziehen. Andere nationale Regierungen haben ihre Unterst�tzung in unilateralen Mitteilungen zur�ckgezogen.<BR>
<BR>
Es ist unwahrscheinlich, da� der Rat jemals wieder eine �hnliche Einigung �ber diese Direktive beschlie�en kann.<BR>
<BR>
Aus diesem Grund will die Patentindustrie, da� das Papier auch ohne eine qualifizierte Mehrheit m�glichst schnell im Rat best�tigt wird, um danach mittels Regel 62 durch ein sich str�ubendes Parlament geschleust werden zu k�nnen.<BR>
<BR>
<H2>Wird sich das Neue Parlament von der Blitzpropaganda einlullen lassen ?</H2>
Die Patentindustrie startete mittlerweile eine 'Informationskampagne' von erstaunlichen Ausma�en, vergleichbar nur mit den Man�vern 1998, als einer der heutigen Kampagnenleiter Behinderte in Rollst�hlen ins Parlament rollte, die darum baten, da� die ungl�ckselige Genpatent-Direktive beschlossen wird. �hnlich emotionale Karten werden in gro� seitigen Presseauftritten gespielt (z.B. European Voice), oder wenn angebliche Vertreter des Mittelstandes aus softwarefremden Gebieten ins Parlament geschickt werden, um zu schildern, wie ihr Gesch�ft durch eine Einschr�nkung der Patentierbarkeit nach dem Parlamentsentwurf von 2003 verw�stet w�rde. Vertreter von Siemens erz�hlen Ihnen, da� dieser Entwurf ihr R&D f�r wichtige medizinische Erfindungen unterminiert - aber sie zeigen Ihnen h�chstwahrscheinlich keine einzige ihrer Patentanmeldungen. Wenn Sie einen Blick auf die <A HREF="http://l2.espacenet.com/espacenet/viewer?PN=EP1384191">relevanten Patente z.B. der <A HREF="http://swpat.ffii.org/players/siemens/">Siemens Medical Solutions</A> werfen w�rden, w�rden Sie bemerken, da� Siemens Monopole auf die medizinische Datenverarbeitung bei �rzten erlangt hat, also auf medizinische 'Gesch�ftsmethoden' implementiert in Computern, wodurch die Gesundheit wohl eher gef�hrdet als gerettet werden d�rfte. Sie w�rden auch bemerken, da� Verbesserungen in der medizinischen Datenverarbeitung wie alle Software-Innovationen ausreichend durch Copyright und Gesch�ftsgeheimnis gesch�tzt sind, und da� diese Patente von Siemens und anderer im selben Feld nicht gerechtfertigt sind.<BR>
<BR>
Aber werden sie die Zeit haben, die zugrunde liegenden Fragen zu erforschen ? Werden sie die Zeit haben, die Patentindustrie zu der Kommunikationsform zu zwingen, die sie so sehr f�rchtet: Zu einem Dialog, in dem ihre scheinheiligen Behauptungen �ber den Verlust der Konkurrenzf�higkeit und von Jobs <A HREF="http://gauss.ffii.org/index/pending/applicantdesignatedcountry">enttarnt und widerlegt</A> werden k�nnen ?<BR>
<BR>
Entgegen den Versicherungen von Siemens und Verb�ndeter stellen die �nderungen vom September 2003 ein konservatives Minimum dar, wenn die Ausdehnung des Patentsystemes in das Feld von Gesch�ftsmethoden und Kalkulationen verhindert werden soll. W�hrend das Parlament der freiz�gigen Patentierbarkeit 5 Schwellen entgegenstellte, riss der Rat diese wieder heraus und schuf 5 entgegengesetzte Schwellen, die jeder Beschr�nkung der Patentierbarkeit in den Weg gelegt wurden.<BR>
<BR>
Wenn Sie die n�tigen 60-70% der Stimmen zum erneuten Herausoperieren der 5 Schwellen des Rates und zum Wiedereinsetzen derer des Parlamentes nicht erreichen, werden wir mit einer Direktive enden, die niemanden gl�cklich macht, und es wird wenig Hoffnung auf erfolgreiche Verhandlungen in einem Vermittlungsverfahren geben.<BR>
<BR>
Selbst wenn sie diese 60-70% f�r die Schl�ssel-Positionen erreichen k�nnen, werden sie in einem Vermittlungsverfahren einem Rat gegen�berstehen, der den Problemen bis heute noch nicht ins Auge sehen wollte. Sie werden unter Ausschluss der �ffentlichkeit mit Patentbeamten verhandeln, die jedem MdEP in der sophistischen Fachsprache �berlegen sind, und die keiner Supervision durch ihre nationalen Regierungen unterliegen, geschweige denn durch die Parlamente.<BR>
<BR>
<H2>Noch k�nnen die MdEP den Rat zwingen, die Probleme anzuerkennen</H2>
Sowohl der Rat als auch die Welt der Patentbef�rworter zeigen sich unbeweglich und reagieren nicht auf die Forderungen der �ffentlichkeit. Die Kombination aus Ministerrat und Welt der Patente scheint besonders toxisch zu sein, und es ben�tigt ungew�hnlich viel Zeit, einen �ffentlicher Dialog anzusto�en, der sie beeindrucken kann.<BR>
<BR>
Die Patentjuristen des Rates haben es bis heute nicht f�r n�tig gehalten, den Problemen in einer offenen Diskussion ins Auge zu sehen. Sollen sie wirklich damit durchkommen ? Sollte der Rat nicht verpflichted sein, die Anliegen des Parlamentes zu respeltieren ? Und wenn der Rat sich wirklich gegen das europ�ische Parlament stellen will, sollte er sich dann nicht wenigstens der Unterst�tzung der nationalen Parlamente versichern ?<BR>
<BR>
Die parlamentarische Demokratie sieht in diesen Tagen schwach aus in der EU, aber es ist an uns, dies zu �ndern. In diesem Fall reichte es aus, wenn auch nur ein Land sich f�r eine neue politische Einigung (B-Item) einsetzt. Es gibt gen�gend �ffentliche Aufmerksamkeit, die den Rat zu echten neuen Verhandlungen zwingen wird, wenn nur der Antrag einmal gestellt ist.<BR>
<BR>
Ratsdiplomaten und Regierungsvertreter werden Ihnen erz�hlen, da� eine politische Einigung nicht ge�ndert werden kann, aber dies <A HREF="http://wiki.ffii.org/ConsReversEn">entspricht nicht den Tatsachen</A>. Als MdEP habe Sie ausreichenden Einflu�, um den Rat zu einer demokratischeren Auslegung seiner eigenen Regeln zu bewegen.<BR>
<BR>
Es ist nach wie vor gut m�glich, da� alle Bem�hungen fruchtlos bleiben, und der Ministerrat die inzwischen �berkommene Einigung vom Mai 2004 als A-Item adoptiert. In diesem Fall mu� das Parlament darauf vorbereitet sein, f�r eine R�ckkehr zur 1. Lesung zu stimmen. Eine dementsprechende Empfehlung im JURI w�re dazu eine wichtige Hilfestellung. Wenn die Entscheidung dazu nicht in dieser Woche f�llt, ist es wahrscheinlich zu sp�t.<BR>
<BR>
Eine andere Option ist es, auf die Rechte der Parlamentarier nach Regel 55(4) zu bestehen. Es w�re sogar zu erw�gen, einen Fall, in dem die nationalen Parlamente respektlos ignoriert und die europ�ischen Parlamentarier ihrer Rechte enthoben werden, aus Prinzip vor den europ�ischen Gerichtshof zu bringen.<BR>
<BR>
<H2>FFII in Br�ssel</H2>
Der FFII unterh�lt ein B�ro in Br�ssel (Rue Michel-Ange 68) und arbeitet als Mitgliedsorganisation von CEA-PME (Confederation of SME Associations in Europe). CEA-PME repr�sentiert Verb�nde mit 800,000 SME Mitgliedern aus ganz Europa.<BR>
<BR>
Der FFII wird von 80.000 Personen und 1.200 Firmen unterst�tzt, und hat sich zu einem Zentrum der Expertise in Rechtsfragen zu Software und industriellem Eigentum entwickelt.<BR>
<BR>
Wir stehen Ihnen zur Verf�gung, wenn Sie Patent-Kampagnen peinliche Fragen stellen und eine informierte europ�ische Gesetzgebung durch gew�hlte Vertreter der EU unterst�tzen wollen. Z�gern Sie nicht, uns zu kontaktieren.<BR>
<BR>
Mit freundlichen Gr��en,<BR>
<BR>
<CENTER>
<A HREF="http://www.alexander.bostrom.net/">Alexander BOSTR�M</A><BR>
<A HREF="http://www.rudert-home.de/">Andreas RUDERT</A><BR>
<A HREF="http://epatents.hellug.gr/">Antonios Christofides</A><BR>
<A HREF="http://beauprez.net/">Christian Beauprez</A><BR>
<A HREF="http://www.cs.tu-berlin.de/~ccorn">Christian Cornelssen</A><BR>
<A HREF="http://www.sslug.dk/">Erik JOSEFSSON</A><BR>
<A HREF="http://www.test.com/">Felix Edgar KLEE</A><BR>
<A HREF="http://www.phys.ethz.ch/~giedke/">Geza GIEDKE</A><BR>
<A HREF="http://gibuskro.lautre.net/">Gerald SEDRATI-DINET</A><BR>
<A HREF="http://www.lichtvollst.de/">Martin STEIGERWALD</A><BR>
<A HREF="http://www.dhcp42.de/">Jan WILDEBOER</A><BR>
<A HREF="http://www.brevets-logiciels.info/">Rene MAGES</A><BR>
<A HREF="http://www.javakaffee.de/">Martin GROTZKE</A><BR>
<A HREF="http://www.ffii.org/~miernik/">Jan MACEK</A><BR>
<A HREF="http://www.man.poznan.pl/~momat/">Pawel Widera</A><BR>
<A HREF="http://www.a2e.de">Hartmut PILCH</A><BR>
<A HREF="http://www.fellbach-solar.de/">Prof. Dr.-Ing. Reinhard MALZ</A><BR>
<A HREF="http://www.nlnet.nl/">Wytze van der Raay</A><BR>
<BR>
</CENTER>
(von 60 weiteren FFII Unterst�tzern durch ein <A HREF="http://aktiv.ffii.org/juri52/en">Webformular</A> unterschrieben und in der englischen Fassung als Brief an alle 732 MdEP versandt am 31.1.2005)<BR>
<BR>
Bitte widmen Sie auch unseren aktuellen Webseiten Ihre Aufmerksamkeit:<BR>
<UL>
  <LI><A HREF="http://wiki.ffii.org/Juri0501En">Neustart im Rechtsausschuss</A></LI>
  <LI><A HREF="http://www.ffii.org/LtrJmaebeJuri0501.en.html">Brief an die MdEP des JURI �ber 'etablierte Traditionen' von Rat und EPO</A></LI>
  <LI><A HREF="http://wiki.ffii.org/SwpatcninoEn">Aktuelle FFII Nachrichten zu Softwarepatenten</A></LI>
  <LI><A HREF="http://swpat.ffii.org/">FFII: Softwarepatente in Europa</A></LI>
  <LI><A HREF="http://demo.ffii.org/">Wie sie uns helfen k�nnen</A></LI>
</UL>
oder lesen Sie einfach auf der <A HREF="http://www.ffii.org/index.orig.en.html">Startseite des FFII</A> weiter, auf die Sie vermutlich gelangen wollten.<BR>

</BODY>
</HTML>
