<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>2005-01-31 Open Letter from FFII to MEPs on Pending Decisions</title>
</head>

<body bgcolor="#000000" text="#FFFFFF" link="#FF9900" alink="#FF9900" vlink="#FF9900">
<center>
	<table width="80%" border="1" cellpadding="0" cellspacing="0" bgcolor="#31454A">
	<tr>
	<td colspan="2" align="right">
		<a href="index.pl.html"><img border="1" height="25" src="http://ffii.org/img/flags/pl.png" alt="polski"></a>
		<a href="index.fr.html"><img border="1" height="25" src="http://ffii.org/img/flags/fr.png" alt="francais"></a>
		<a href="index.de.html"><img border="1" height="25" src="http://ffii.org/img/flags/de.png" alt="deutsch"></a>
<!--		<a href="index.es.html"><img border="1" height="25"
		src="http://ffii.org/img/flags/es.png" alt="español"></a> -->
		<a href="index.el.html"><img border="1" height="25"
		src="http://ffii.org/img/flags/gr.png" alt="ελληνικα"></a>
	</td>
	</tr>
	<tr>
	<td height="45" width="100%" align="center" valign="top">
		<table width="100%" cellpadding="40" cellspacing="0" border="0">
		<tr>
		<td colspan="2" align="left" valign="middle">

    <h1>Open Letter from FFII to MEPs
    on Pending Decisions Concerning the Software Patent Directive
<br>
(NOTE: Meanwhile <a href="http://wiki.ffii.org/Restart050202En">JURI has voted as we had hoped.</a>)</h1>

    <table cellpadding="0" cellspacing="0" border="0">
      <tr valign="top" align="left">
	<th>From:&nbsp;</th>
	<td>
	  <a href="http://swpat.ffii.org">Foundation for a Free Information
	  Infrastructure (FFII)</a>
	  <br>Brussels Office
	  <br>Rue Michel-Ange 68
	  <br>Phone: +32-273 962 62, +32-485 832 126, +32-486 233 396
	  <br>E-Mail: info at ffii org
	</td>
      </tr>
      <tr valign="top" align="left">
	<th>To:&nbsp;</th>
	<td>
	  Members of the European Parliament
	  <br>JURI members in particular
	</td>
      </tr>
      <tr valign="top" align="left">
	<th>Date:&nbsp;</th>
	<td>January 31, 2005</td>
      </tr>
    </table>

    <h3>Securing Options for the Parliament this Week</h3>

    <p>Dear Member of Parliament,</p>

    <p>this week, the Legal Affairs Committee will discuss the proposed
    <em>directive on the patentability of computer-implemented inventions</em>
    (software patent directive) with Commissioner Charlie McCreevy and
    Microsoft's Bill Gates, and it will also have to decide whether MEPs should
    have the opportunity to vote on a return to 1st reading when the Council's
    proposal arrives in the Parliament.  61 MEPs have demanded this right by a
    motion based on Rule 55(4) in late December, but until now it seems that so
    far the Tabling Office has denied this right without giving an
    explanation.</p>

    <p>We are worried to see an erosion of MEP rights taking place while the
    Commission and Council are setting new sad precedents of disrespect for
    parliamentary democracy, both at the EU and national level.  Under these
    circumstances, MEPs have very good reasons to insist on their right to file
    motions under rule 55(4) and their right to vote on a return to 1st
    reading.  As we shall argue, the software patents directive is one of the
    cases where the exercise of this right makes sense.</p>

    <h3>Why the Patent Industry Opposes a Renewed Referral</h3>

    <p>These days you have been receiving letters from Nokia, Ericsson,
    Alcatel, UNICE and many other players who have built their business models
    around the patent system, demanding that you should take a strong stance
    against any motion based on rule 55.</p>

    <p>The reason given for this demand is usually that you must quickly nod
    off the Council agreement, because &ldquo;industry&rdquo; can no longer
    wait.  They also claim that this agreement forbids the patenting of
    software and business methods.</p>

    <p>However, in internal memoranda of the same players, e.g. a memorandum
    sent out by a national member of UNICE to the patent lawyer committees of
    its member associations, the explanation is quite different:

    <blockquote>
      <em>"The Council has basically maintained its position of November 2002.
      It has taken on board only a few uncritical amendments from the Parliament.
      We must make sure that this position is adopted by the Council as soon
      as possible.  Once it is handed to the Parliament for a second reading,
      the Parliament will most likely be unable to assert any of the amendments
      of September 2003 in view of the higher majority requirements."</em>
    </blockquote>

    The majority requirements are laid down in <a href="http://www2.europarl.eu.int/omk/sipade2?PUBREF=-//EP//TEXT+RULES-EP+20040720+RULE-062+DOC+XML+V0//EN">Rule 62</a>:
    <blockquote>
      <p><strong><em>Amendments to the Council's common
      position</em></strong></p>
      <p><em>4. An amendment shall be adopted only if it secures the votes of a
      majority of the component Members of Parliament.</em></p>
    </blockquote>

    <p>In other words: any MEP who is absent or abstains on the day of the
    vote will be counted as pro-Council, and unless 60&mdash;70% of those
    present vote for each key amendment, the Parliament will not be able to go
    into Conciliation with a coherent counter-position.</p>

    <h3>Council: Unqualified Majority for an Outdated Package</h3>

    <p>The above analysis of the UNICE member organisation is correct.  After
    the Parliament passed its amendments in September 2003, the Council's
    <em>Intellectual Property (Patents) Working Party</em> (i.e. the very
    people who sit on the administrative Council of the European Patent Office)
    ignored these amendments and merely reaffirmed their previous position,
    adorning it with some cosmetic amendments from the JURI reading of June
    2003, which already had been derived from the same old Council position,
    while negating the Parliament's position on points that had
    previously been open, and adding further layers of deceptive packaging.</p>

    <p>The result was the most uncompromisingly pro-software-patent proposal yet seen
    in the procedure.  It basically consists in the 
    <a href="http://wiki.ffii.org/Monti9710En">Commission's approach of
    1997</a>, i.e. harmonisation with the US practise of patenting software and
    business methods, but guised in limiting rhetoric, with the term
    &ldquo;technical&rdquo; appearing more often and with less substance than
    ever before.  Thanks to manipulative orchestration by Commissioner
    Bolkestein together with the German delegation and the Irish presidency,
    this proposal <a href="http://wiki.ffii.org/Cons040518En">made it
    through</a> a Competitiveness Council meeting on 18th of May 2004 <a
    href="http://swpat.ffii.org/letters/cons0406/">without really enjoying the
    support</a> of a qualified majority of member state governments.</p>

    <p>The national parliaments woke up late, but did pass some resolutions and
    even binding decisions against the Council and in favor of the EP.  The
    governments of the Netherlands, Germany and Denmark are under obligation to
    withdraw their support from the Council's agreement.  Other governments
    have made unilateral statements to show their lack of support.  It is
    unlikely that the Council will ever again pass a similar agreement on this
    directive.  Therefore the patent industry wants the Council to quickly
    adopt this paper without a qualified majority, so that it can be pressed
    through an unwilling Parliament thanks to rule 62.</p>

    <h3>Overwhelming a New Parliament with a Propaganda Blitz</h3>

    <p>The patent industry is meanwhile staging a misinformation campaign of
    staggering proportions, paralleled only by the maneuvers of 1998, when one
    of the current campaign managers rolled handicapped people in wheelchairs
    into the Parliament to ask for the unfortunate gene patent directive to be
    passed.  Similar emotional cues are being used now in large newspaper
    advertisements (e.g. European Voice).  So-called SME representatives from
    non-software fields are being sent to the Parliament to tell you that their
    industry would be devastated by the limitations on patentability which the
    Parliament proposed in September 2003.  Siemens representatives are telling
    you that this directive would undermine the R&amp;D efforts for important
    medical inventions.  But they probably are not showing you any of their
    patent claims.  If you could take a look at the <a
    href="http://l2.espacenet.com/espacenet/viewer?PN=EP1384191">relevant
    patents</a> from e.g. <a
    href="http://swpat.ffii.org/players/siemens/">Siemens Medical
    Solutions</a>, you would notice that Siemens has obtained monopolies on the
    data processing methods of medical doctors, i.e. on medical business
    methods dressed up as apparatuses, and is thereby threatening rather than
    saving lives.  You would also notice that medical data processing
    achievements, like other software innovations, are sufficiently protected
    by copyright and business secret, and that the patents of Siemens and
    others in this field are without merit.</p>

    <p>But will you have the time to really investigate the questions?  Will
    you have time to force the patent industry into the form of communication
    which they dread most: a dialogue, where their wild claims about loss of
    investments and jobs can be <a
    href="http://gauss.ffii.org/index/pending/applicantdesignatedcountry">subjected
    to scrutiny</a>?</p>

    <p>Unlike Siemens and the like would have you believe, the amendments of
    September 2003 represent a conservative minimum that is needed to stop the
    patent system's expansion into the realm of business methods and
    calculation rules.  While the Parliament built 5 walls against
    patentability, the Council tore these down and built 5 opposite walls, i.e.
    walls designed to prevent any limitation of patentability.  If you fail to
    secure 60&mdash;70% of the votes for again tearing down the five walls of
    the Council and rebuilding those of the Parliament, we will end up with a
    directive that pleases nobody, and there will be little hope of successful
    negotiations in Conciliation.</p>

    <p>Even if you succeded in mustering 60&mdash;70% for all the crucial
    amendments, you will enter a Conciliation round with a Council that has not
    yet faced the problems.  You will be negotiating confidentially with
    national patent officials who are more versed in patent sophistry than any
    MEP, and who are not even under effective supervision from their national
    governments, let alone parliaments.</p>

    <h3>MEPs can still force the Council to Face the Problems</h3>

    <p>Both the Council and the patent world are immobile and unresponsive to
    the interest of the public.  The Council of Ministers in combination with
    the Patent World is a particularly toxic mixture, and it takes much more
    time than usually to build up a public discourse that can impress them.</p>

    <p>The Council's patent legislators have not yet found it necessary to face
    the problems in open discussion.  Should they really be allowed to get away
    without it?  Shouldn't the Council's be obliged to address the Parliament's
    concerns? Shouldn t it also be obliged to secure support of national
    parliaments, if it wants to go against the European Parliament?</p>

    <p>Parliamentary democracy is currently very weak in the EU, but it is up
    to us to make it happen. In this case it is enough if one country asks for
    a recount of the votes on the software patent agreement (B-item).
    Sufficient public attention has built up to force the Council into
    renegotiation, if only the question is asked. Council diplomats and
    government officials will tell you that Political Agreements can not be
    changed, but <a href="http://wiki.ffii.org/ConsReversEn">they are
    wrong</a>.  As MEPs you have the necessary influence to make
    the Council interpret its rules in a more democratic spirit.</p>

    <p>Still, it is quite possible that all efforts fail and the Council will
    one day adopt the unsupported agreement of May 2004 as an A-item.  In that
    case the Parliament needs to be prepared to vote for a return to 1st
    reading.  To prepare for that, a favorable decision by JURI would be of
    great help. If it is not taken this week, it may be too late.  Another
    option would be to insist on the rights of MEPs under rule 55(4). At a time
    where parliaments are disrespected and MEPs deprived of their rights, this
    case should perhaps be taken to the ECJ as a matter of principle.</p>

    <h3>FFII in Brussels</h3>

    <p>The FFII has an office in Brussels (Rue Michel-Ange 68) and is working
    as a member organisation of CEA-PME (Confederation of SME Associations in
    Europe). CEA-PME represents associations with 800,000 SME members from all
    over Europe.  FFII has 80,000 individual and 1,200 corporate supporters and
    has become a center of expertise on legislation in the fields of software
    and industrial property rights.</p>

    <p>We are available to help you ask embarassing questions to the patent
    campaigners and to help strengthen informed lawmaking by elected
    representatives in the EU.</p>

    <p>Please do not hesitate to contact us.</p>

    <p>Yours Sincerely,</p>

<div align=center>
<!--#include virtual="juri52-sign.html"-->
(signed by <!--#include virtual="juri52-sign.stat"--> FFII supporters through a <a href="http://aktiv.ffii.org/juri52/en">web form</a> and distributed in paper form to all 736 MEPs on 2005-01-31.) 
</div>

<p><br>Please find further information at</p>

<ul>
<li><a href="http://wiki.ffii.org/Juri0501En">Restart in the Legal Affairs Committee</a></li>
<li><a href="./LtrJmaebeJuri0501.en.html">Letter to JURI MEPs about the Council's and EPO's "Established Practises"</a></li>
<li><a href="http://wiki.ffii.org/SwpatcninoEn">FFII Software Patent News</a></li>
<li><a href="http://swpat.ffii.org/">FFII: Software Patents in Europe</a></li>
<li><a href="http://demo.ffii.org/">How to help with this campaign</a>
</ul>

or just <a href="index.orig.en.html">continue here</a> to read the original web page which you were probably looking for.

</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
</body>
</html>

