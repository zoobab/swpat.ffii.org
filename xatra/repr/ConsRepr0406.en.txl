<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Council 2004-05-18: an Unqualified Majority

#descr: The moderators of the Competitiveness Council session pushed the participants toward accepting the proposal by deception, pressure and surprise tactics, thus even making it questionable whether a valid majority was achieved.  It can be said with certainty that only a minority of governments really agrees with what was negotiated, but several governments were misrepresented by their negotiators, who broke intra-ministerial agreements or even violated instructions from their superiors

#bal: Luxemburg, Estonia, Latvia, Slovenia

#osn: Bolkestein PR

#vta: Deceptive Maneuver to win over other countries for Bolkestein's position, broken intra-ministerial agreement, MPs of ruling coalition and opposition protest against being locked out and deceived.

#ajc: Prime minister had promised not to support the proposal, representative in Council supported (%(q:broken fax machine))

#uWj: Representative was instructed to support german amendment 2b, wasn't asked for his position because majority was supposedly already achieved, later said that Poland abstains.

#aim: Minister wasn't present, diplomat didn't know what to do, comical dialogue:

#rae: Minister Brinkhorst acts against parliament's requests.

#Wiw: Ministers act against given promises of president, make bogus claims about what they voted for.

#nee: Support in contradiction to previous promises of the governments.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsRepr0406 ;
# txtlang: en ;
# multlin: t ;
# End: ;

