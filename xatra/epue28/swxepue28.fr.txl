<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Mue: Avec nos meilleurs sentiments,

#Wit: En tant que citoyens, nous vous serions infiniment obligés si vous acceptiez lors des prochaines consultations de resister au lobby des brevets et défendre l'intérêt public.

#efe: une prise de position prochaine du  Ministère de Justice sur les propositions de cette lettre, y compris sur les demandes des responsables politiques cités

#uul: notre présence dès que possible aux prochaines consultations, et le droit à la parole pour un représentant de l'alliance Eurolinux

#AWr: l'accréditation de l'association FFII et de l'alliance Eurolinux pour participer à la conférence diplomatique de novembre prochain

#Wbe: Nous demandons

#Whr: Nous pensons que cette voix, ajoutée à celle des 30000 signataires de la pétition Eurolinux, devrait avoir plus de poids que le souhait de quelques spécialistes des brevets en ce qui concerne les fruits de la nouvelle économie.

#Aen

#qey

#qdi

#qWo

#qnW

#qlW

#dan

#AWi

#Mev

#Cts

#Lio

#Dtr: Le Déaut est co-initiateur du %(oa:projet de loi) français qui impose un %(q:droit à la compatibilité) ainsi que l'usage des standards du logiciel libre dans les administrations publiques. La politique des brevets, telle qu'elle est menée à Münich et à Bruxelles, menace les buts de son action. Il a d'ailleurs expédié à de nombreux responsables politiques français, en juillet dernier, une %(lb:lettre ouverte) que nous reproduisons ici:

#Aiz: Mais, alors que le pouvoir législatif est encore dans l'incapacité de faire la lumière sur ce sujet, le pouvoir judiciaire passe déjà à l'action, accordant des milliers de brevets sur des logiciels et essayant d'imposer une modification des règles légales. Il est donc grand temps pour nous les législateurs de nous occuper de ces questions.

#ScW: Et même s'il était possible de trouver des domaines des techniques de l'information pour lesquels les brevets auraient, ou auraient eu par le passé, un effet favorable manifeste, encore faudrait-il se demander si d'éventuels effets secondaires néfastes de la brevetabilité n'en annuleraient pas les avantages.

#Itm: Dans les milieux de spécialistes des questions de politique des technologies, on entend constamment affirmer que le système des brevets doit être étendu à certains domaines des technologies de l'information parce que sans cela les investissements ne seraient pas suffisamment protégés. Jusqu'ici, cette affirmation a toujours été présentée comme une vérité abstraite et n'a été étayée par aucun fait prouvé de l'économie allemande ni européenne.

#Dxf: L'%(vv:association viruelle locale du SPD) soutient la pétition Eurolinux. Le 8 août dernier, en réponse à une question de la FFII, son fondateur Jörg Tauss remet le commentaire suivant au sujet du %(q:propos de base):

#DDf: De plus, le fait de rendre les logiciels brevetables soulèverait de considérables problèmes techniques autant qu'administratifs: avec les deux ans de délai actuel, quand le brevet serait enfin déposé il serait depuis longtemps obsolète. Le travail de documentation des brevets coûterait très cher. Les petites et moyennes entreprises seraient désavantagées par la brevetabilité: seules les grandes entreprises disposent des ressources nécessaires pour rester en phase avec le développement des brevets. Seules les grandes entreprises pourraient déposer des brevets et en tirer des revenus supplémentaires. La concurrence se jouerait sur des disputes juridiques et non plus sur la mise en oeuvre rapide des innovations. Le progrès technique s'en trouverait entravé. Il faut empêcher cela !

#Asm: De toutes façons, ce n'est qu'au cours des dernières années en Europe que le logiciel est devenu brevetable comme composant de procédés techniques. En effet, les procédés techniques qui contiennent les programmes d'ordinateurs sont brevetables. Par exemple, une machine outil pilotée par ordinateur constitue un objet entièrement brevetable. Il nous manque pour l'instant des analyses économiques détaillées des effets d'une possible brevetabilité des logiciels. Nous voyons tout de même clairement le danger d'un renforcement supplémentaire de la bureaucratie qui aurait pour effet de freiner les innovations les plus nécessaires.

#IgA: Aux Etats-Unis, l'exercice de la concurrence est déjà considérablement entravé par la brevetabilité des concepts commerciaux. Un exemple souvent cité est le brevet déposé par Amazon sur le procédé "one-click" lors de la commande de produits sur l'Internet.

#Bwd: Chez les développeurs de logiciels et sur la scène Open Source, règne une grande insécurité. On craint en effet que l'amendement de la CBE et la doctrine juridique de la Commission Européenne qui s'annonce n'aboutisse à l'introduction en Europe des habitudes américaines.

#CWa: D'après la Convention Européenne des Brevets (CBE), les programmes d'ordinateurs ne sont pas brevetables %(q:en tant que tels). Aux Etats-Unis au contraire, tout ce qui peut être créé par l'homme est par principe brevetable.

#Vir: Les créateurs sont confrontés à une situation d'insécurité du fait de la possible brevetabilité des logiciels et des concepts commerciaux. Il faut faire la lumière au plus vite sur ce sujet.

#F4U: Madame Wolf %(mw:s'est exprimée) le 14 juin 2000 lors d'une session de créateurs d'entreprises informatiques:

#vie: Au mois de novembre 1999, à la suite d'un examen approfondi, le Comité des Régions a adopté et %(publié) la présente étude en tant que son point de vue officiel. L'étude porte donc la signature de plus de 200 dirigeants politiques des régions d'Europe, y compris Stoiber, Teufel, Diepgen et bien d'autres.

#dih: Député de la Moselle

#Vns: Président de la Commission Parlementaire sur les nouveaux media

#MsW: député fédéral

#Wse: porte-parole des Verts pour les questions économiques

#CtW: Le Comité des Régions de l'Union Européenne

#FWW: Au cas où, au sein du Ministère de Justice ou encore du Conseil Administratif, un intérêt se ferait tout de même jour pour un débat sur les risques économiques de la brevetabilité des logiciels, nous nous efforcerons de retravailler complètement notre argumentation avant la réunion du Conseil le 4 septembre prochain. En attendant, nous nous contenterons de citer quelques prises de position de responsables politiques connus:

#Dke: Les risques économiques liés à la brevetabilité des logiciels sont connus depuis longtemps. Malgré cela les prises de position de personnalités n'ont provoqué jusque là qu'un silence assourdissant parmi les personnes travaillant au problème des brevets européens.

#OKt: Même si l'article 52 dans sa forme existante, dans l'interprétation que nous en donnons  ci-dessus, est en totale conformité avec le TRIPS, il existe tout de même de bonnes raisons pour réclamer sa révision. Par exemple, la règle du délai minimum de 20 ans %(q:pour tous les domaines de la technique) pourrait recevoir des aménagements. D'autre part, il manque au TRIPS des règles au sujet des limites de la brevetabilité. En particulier, la brevetabilité des procédés commerciaux entre en contradiction avec la liberté du commerce mondial, pourtant le but principal du TRIPS. Et la brevetabilité des programmes d'ordinateurs enterre le droit d'auteur dont la validité en ce qui concerne les programmes d'ordinateurs est pourtant clairement énoncée par l'article 10.

#Ee2: Une nouvelle rédaction de l'article 52 dans ce sens serait toutefois un objectf ambitieux, difficile à mener à bien d'ici au mois de novembre. C'est pourquoi nous proposons de conserver tout simplement l'article 52 en l'état, et de travailler peu à peu à une interprétation allant dans le sens des idées présentées ci-dessus.

#Dtf: Insoweit die Abgrenzung zwischen physischen und logischen Prozessinnovationen im einzelnen die Richter überfordern könnte, wäre es denkbar, hierfür die Pufferzone der %(q:weichen Patente) zu schaffen.  Erfinderische Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herkömmlichen Rechenapparatur genügt, könnten insoweit monopolisiert werden, wie sie nicht auf einem Universalrechner ablaufen.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, würde er es vermutlich vorziehen, einem Patentinhaber maßvolle Lizenzgebühren zu zahlen.  Je physischer die Prozessinnovation, desto lukrativer wird das weiche Patent.  Neuere ökonomische Studien zeigen, dass %(q:weiche Schutzrechte) dieser Art wirtschaftspolitisch erwünscht sein können.  Außerdem hätte die Einführung der weichen Pufferzone den Vorteil, dass die meisten der vom EPA gesetzeswidrig gewährten Softwarepatente ohne Bruch in einen allerseits akzeptablen rechtlichen Status überführt werden könnten.

#Set: Schließlich ließe sich auch der Erfindungsbegriff im Lichte der obigen Überlegungen sinnvoll definieren.  Der vom EPA erarbeitete Begriff der %(q:technischen Lösung eines technischen Problems) wäre zu präzisieren: %(q:auf neuem Wissen über beherrschbare Naturkräfte beruhende Lösung eines durch die Herstellung materieller Güter aufgeworfenen Problems).  M.a.W.:  Problemlösungen, die mithilfe künstlicher Intelligenz aus bekannten Modellen der realen Welt abgeleitet werden könnten, sind nicht erfinderisch.  Patente sind die Belohnung für harte Laborarbeit, nicht für geschicktes Rechnen.

#Dsd: La classique %(q:théorie du noyau), dont la Cour Fédérale de Justice faisait usage jusque tout récemment, le formule très précisément: le noyau de l'invention doit se situer dans le domaine technique (c'est à dire exploiter les forces naturelles), et il ne doit pas être possible de faire entrer le brevet en action (= violation du brevet) à l'aide d'un simple programme d'ordinateur. La théorie du noyau ajuste le domaine des inventions au domaine des interdictions. Elle fait en sorte que les objets informationnels ne puissent être interdits et que les procédés qui se consomment dans leur utilisation par une structure informationnelle originale au moyen d'outils traditionnels (= les logiciels en tant que tels) ne puissent valoir en tant qu'%(q:inventions) au sens du droit des brevets.

#Nuc: De même, l'exclusion concernant les procédés thérapeutiques et chirurgicaux doit être maintenue dans l'article 52 et non pas être transférée à l'article 53 afin qu'il soit bien clair que cette exclusion est due à l'exigence d'%(q:utilisation industrielle), non à une distinction en tant que cas particulier.

#DdF: Ceci pourrait signifier que le mot %(q:gewerblich) dans la version allemande pourrait se changer en %(q:industriell), de même que dans le %(q:propos de base), article 23.1.1, le mot de %(q:Funktion) se voit transformé en %(q:Amt): %(q:ni la version anglaise ni la version française ne sont à modifier).

#DDo: Au cours du ... développement de la jurisprudence ..., le droit des brevets s'est libéré de la traditionnelle limitation à l'industrie de transformation. Il est devenu très significatif également pour les entreprises de services des branches du commerce, de la banque, des assurances, des télécommunications etc... Il est à craindre que les entreprises allemandes de services, n'ayant pas constitué un portefeuille de brevets suffisant, se retrouvent à la traîne dans le secteur, notamment dans les situations de concurrence avec les Etats-Unis.

#Doa: Il faut mettre un terme au scénario ainsi dépeint par l'avocat Jürgen Betten, référent pour les logiciels de l'Union des Conseils en Brevets Européens dans une circulaire à ses mandants:

#DTe: Le terme de %(tpe:%(q:industriel):en anglais industrial, en allemand gewerblich) est précisé, dans le sens où le comprend traditionnellement l'%(q:industrie de transformation). Ce terme est presque identique à celui de la définition classique du mot "technique". Il requiert l'existence d'un procédé automatisé de production des biens matériels (%(q:utilisation des forces naturelles)).

#enl: Hier liegt manchmal eine schwierige Grenze, s. BGH-Urteil %(q:Antiblockiersystem) von 1980.  Der ABS-Patentantrag wurde 1976 vom BPatG abgelehnt, weil die Erfindung im Bereich der programmierten Steuerung lag, aber 1980 vom BGH für zulässig erklärt, weil sie auf Experimenten mit Naturkräften beruhte und nicht etwa durch mathematische Operationen auf bekannte Rechenmodelle zurückgeführt werden konnte.  Gegenstand der Patentansprüche war eine Fahrzeugsteuerungsprozess, nicht ein Computerprogramm.

#Drc: Dans la définition classique de la Cour Fédérale de Justice, le terme de %(q:technique) se caractérise par l'utilisation des forces naturelles contrôlables sans activité humaine interposée. Cela signifie qu'un procédé chimique original est technique, tandis que si un programme d'ordinateur le pilote il n'est pas lui-même technique.

#Ker: En résumé: Nous voulons en premier lieu le maintien de l'article 52 inchangé. Bien sûr, ce serait intellectuellement tentant de reformuler l'article pour le rapprocher de l'article 27 du TRIPS et en même temps de donner une interprétation cohérente à la liste des exception. Une telle interprétation pourrait se reconnaître dans les lignes directrices suivantes:

#Uen: Le %(q:propos de base) évoque un prétendu %(q:large consensus en faveur de la brevetabilité des programmes d'ordinateurs), ignorant en cela les 30000 citoyens - parmi lesquels 400 cadres dirigeants d'entreprises d'informatique - qui se sont exprimés pour une %(q:Europe sans brevets logiciels). Sur le site Internet de la pétition Eurolinux %{PET}, vous trouverez sans peine les bases de notre argumentation. Une discussion spécialement dédiée à cette question du %(q:propos de base) a lieu également sur le net, vous pouvez vous-même la suivre à l'adresse %{SWP}.

#eei: des frais de courtage élevés et des seuils d'accession au marché, lesquels rendent déjà la vie impossible aux fondateurs d'entreprises "startup" du domaine logiciel pourtant fort utiles sur le plan économique; l'étranglement d'un paysage informatique européen florissant; des fusions ou acquisitions forcées sans aucune création de valeur, en faveur d'un petit nombre de détenteurs de monopoles - généralement américains; l'étouffement d'un moteur jusqu'à présent considérable de croissance et d'emploi

#gEe: l'âge d'or des lobbyistes professionnels et des exploiteurs maximalistes de titres juridiques douteux; la guerre civile entre juristes; des taxations hors de tout cadre juridique, avec accords de secret de pure façade; des structures informelles, voire tout à fait mafieuses, dans l'intérêt de la protection des intérêts privés d'un groupe

#esg: La privatisation des infrastructures de technologie de l'information au détriment de l'accessibilité et de la sécurité; la promotion du secret des textes sources des programmes; l'asphyxie de la culture Opensource pourtant utile sur le plan économique; une entrave à l'%(q:accélération de l'utilisation et de la diffusion des technologies de l'information au sein de la société), but proclamé par la communauté européenne dans l'accord du 20 octobre 1998, et à l'impulsion vers une croissance du secteur de l'informatique; un ralentissement des progrès de l'informatique; la violation des articles 95, 151 et 157 du traité fondateur de la Communauté Européenne - Traité de Rome, et de l'article 81.1b du Traité d'Amsterdam.

#Blc: Aujourd'hui déjà, Microsoft interdit l'utilisation de son format de vidéo breveté ASF dans le but d'imposer par des moyens illégaux l'interdiction de la copie privée souhaitée par les producteurs de cinéma. A propos de cette possibilité, Lawrence Lessig, professeur de droit constitutionnel à Harvard, %(ll:parle) de danger de %(q:stalinisme logiciel). Les textes des programmes ont parfois une valeur de textes de loi non-officiels de la société de l'information. Aujourd'hui déjà, les grands de la branche - tels Microsoft - tirent leur valeur ajoutée essentiellement de l'abus de ce "pouvoir législatif" caché de leurs interfaces logicielles. Les brevets logiciels encourageront et renforceront ces pratiques.

#Emr: un rétrécissement de la liberté d'expression; un détournement des brevets à des fins de domination

#EaW: Même ceux qui, depuis plusieurs années, ont fourni tous seuls un rude travail de programmation ne pourront plus considérer leur travail comme leur appartenant. Au lieu de cela ils devront dépendre du bon vouloir de détenteurs de brevets qui n'auront eux-mêmes jamais écrit la moindre ligne de code. De même qu'une trop forte imposition peut faire baisser les rentrées fiscales, des droits de propriété trop rigides peuvent conduire à une expropriation des intéressés.

#euN: une hausse tendancielle des droits d'auteur; l'expropriation des programmeurs

#dde: l'entrée en vigueur en toute illégalité tous les brevets logiciels déjà déposés - plus de dix mille, de provenance à 75 % extra-européenne - qui dorment à l'O.E.B. tels des chevaux de Troie, attendant que la conférence diplomatique les légalise pour pouvoir ensuite surveiller les auteurs européens de programmes et les poursuivre.

#pac: dorénavant, seront brevetables les structures informationnelles, les procédés commerciaux, les méthodes d'organisation d'entreprise, les algorithmes conceptuels et mathématiques, les prestations de services, les banques, le commerce, l'artisanat, les créations artistiques, les techniques de composition musicale, en fait tout ce que les examinateurs ou futurs examinateurs de l'Office Européen des Brevets se sentiront disposés à examiner

#eeW: %(q:une brevetabilité sans frontières)

#euu: plus de perte de pouvoir pour la communauté, plus de concentration des compétences réglementaires, du pouvoir de jurisprudence et des moyens de contrainte, associés aux intérêts propres d'une administration qui fonctionne quasiment comme une entreprise et a déjà montré dans le passé son peu de respect envers les principes édictés par le législateur, se plaçant dans les faits en-dehors de tout contrôle.

#Dde: Les biens informationnels et les brevets forment ensemble un mélange explosif, dont les conséquences sont décrites et analysées par plusieurs des études économiques jointes (s. %{URL}). Les progrès déjà accomplis par l'O.E.B. en faveur de la levée de toutes les exceptions sont déjà contestables sur le plan conceptuel, et dans la pratique ils présentent aussi des risques que nous ne pouvons qu'évoquer brièvement ici:

#Mnn: Ainsi que se trouve franchie la frontière entre le brevet industriel et le brevet universel. Dans un tel contexte, vouloir tracer clairement d'autres frontières comme celle qui sépare les programmes d'ordinateur des procédés commerciaux, ou encore les %(q:procédés technico-commerciaux), relève de l'utopie.D'ailleurs l'O.E.B. n'a pas cette ambition : elle brevette déjà des méthodes commerciales et %(a6:qualifie) de dépassée l'exigence d'un %(q:effet technique supplémentaire). De plus, dans le texte précedémment cité, l'O.E.B. se réserve l'%(q:adaption aux développements technologiques) qui doit être faite systématiquement. Il faut comprendre par là que dans la pratique telle ou telle technique innovante de composition musicale pourrait par exemple devenir brevetable à la suite d'une simple décision de la direction de l'O.E.B.. Il suffirait pour cela qu'il y ait suffisamment de demandes et que l'O.E.B. embauche suffisamment d'examinateurs ayant des compétences musicales.

#Mne: Ainsi se trouvent monopolisées, en contradiction avec la tradition juridique européenne des brevets industriels, non seulement certaines catégories de %(q:produits et de processus) matériels, mais également l'information servant à décrire lesdits produits et processus. Pour un peu, celui qui se contenterait de recopier le texte du brevet enfreindrait alors le brevet : le texte de brevet contiendrait en effet nécessairement un exemple de réalisation, lequel pourrait %(q:permettre à un spécialiste moyen de reconstituer totalement l'invention), d'où, à la limite, un texte de brevet qui enfreint le brevet qu'il décrit.

#Deu: Tous ces %(q:efforts de jurisprudence) motivés par considérations de %(q:politique juridique) ont conduit aux verdicts %(q:produit programme d'ordinateur/IBM) et %(q:programme d'ordinateur/IBM) de la Chambre d'Appel de l'OEB, auxquels fait référence le %(q:propos de base) de l'O.E.B. déjà cité.

#Dgi: Parallèlement, la jurisprudence ... s'emploie à interpréter la loi en vigueur en accordant le caractère technique grâce à une formulation appropriée, et donc la brevetabilité à pratiquement tous les programmes d'ordinateurs pourvu qu'ils soient nouveaux et innovants.

#lgs: ... la Table Ronde de l'%(un:UNION) qui eut lieu les 9 et 10 décembre 1997 à l'O.E.B., au cours de laquelle 100 spécialistes de vingt pays européens ont débattu de l'avenir des brevets logiciels en Europe et sont arrivés aux mêmes conclusions que l'AIPPI. Ils ont de plus fait remarquer que le concept de %(q:caractère technique) de la CBE ne serait bien compris ni des candidats au brevet, ni des Offices des Brevets de chacun des pays. Pour de nombreux participants, il était clair qu'en fait tous les programmes d'ordinateur présentaient une forme de %(q:caractère technique). Depuis lors et pratiquement %(q:sur tous les canaux), on s'efforce de trouver une solution pour éliminer de la loi européenne sur les brevets cette exclusion absurde des %(q:programmes d'ordinateur en tant que tels), %(s:et aussi, par conséquent, les autres exceptions figurant à l'art. 52 alinéa 2 de la CBE). ...

#Dtn: L'exclusion des %(q:programmes d'ordinateurs en tant que tels) du champ de la protection par le brevet en vertu de l'article 52 de la CBE est depuis longtemps considérée comme une mauvaise décision de politique juridique, surtout que l'exclusion - jusqu'à présent - de larges circuits de distribution est le plus souvent comprise comme une exclusion générale de tous les programmes d'ordinateur.

#MWh: Une telle formulation ne doit pas nous faire oublier qu'après la décision de la CBE en 1973 et jusqu'aux années 80, l'O.E.B. suivait scrupuleusement les instructions du législateur, refusant de breveter toute invention %(q:comportant un programme d'ordinateuren tant qu'objet), c'est-à-dire pour laquelle la simple publication d'un programme aurait suffi à constituer une violation du brevet. Depuis 1986, la difficile %(q:interdiction de breveter les logiciels) a pourtant été grignotée peu à peu en raison de considérations d'ordre économique. A cette date, l'O.E.B. prit la décision de ne plus considérer les %(q:programmes d'ordinateurs produisant par surcroît des effets d'ordre technique) comme des %(q:programmes d'ordinateurs en tant que tels), et commença de développer une argumentation juridique incompréhensible. Ainsi %(cr:s'exprime) l'avocat Betten:

#IWm: Inzwischen scheint sich ein breiter Konsens herauszubilden, dass %(s:Computerprogramme aus der Liste der nicht patentierbaren Erfindungen nach Artikel 52%(pet:2) EPÜ gestrichen werden sollten).  Das EPA und seine Beschwerdekammern haben das EPÜ stets so ausgelegt und angewendet, dass diese Ausnahmevorschrift einen angemessenen Schutz für softwarebezogene Erfindungen, also Erfindungen, die ein Computerprogramm zum Gegenstand haben oder einschließen, in keiner Weise verhindert.  In jüngeren Entscheidungen der Beschwerdekammern (s. T 1173/97 - Computerprogrammprodukt/IBM, ABI. EPA 1999, 609) wurde in der Tat bestätigt, dass in der Regel Computerprogramme nach dem EPÜ patentierbare Gegenstände sind.  Die geltende Ausnahmevorschrift für Computerprogramme ist damit de facto überholt.

#Dbi: Il est donc proposé de %(s:supprimer l'article 52 (2) et (3) de la CBE et de modifier la numérotation des paragraphes suivants), dans le but de %(s:retirer) les %(s:programmes d'ordinateurs) de la liste des exceptions figurant à l'article 52(2). ... Si elle était adoptée, cette modification des règles du jeu faciliterait, si besoin est, l'adaptation des règlements aux développements dans des domaines comme le droit, l'économie ou la technologie. Ainsi, l'exception prévue par la loi se retrouve dans les faits ignorée.

#Ivg: Dans les commentaires du %(q:propos de base), le franchissement de la frontière entre le matériel et l'informationnel est explicitement souhaité:

#ZeG: Entre la matière et l'information, entre le hardware et le software, passe une frontière essentielle aux fortes implications économiques. Les biens matériels sont liés géographiquement à un lieu et leur production en série exige un équipement industriel; les biens informationnels sont au contraire indépendants de leur porteur et peuvent être dupliqués à un coût unitaire tendant vers zéro. Les biens matériels sont liés dans le temps: après les avoir consommés on peut encore les acheter plusieurs fois sous une forme identique; les biens informationnels au contraire ne sont pas consommables et n'acquièrent de nouvelle valeur économique que par l'innovation (toujours de nouvelles versions).

#Dta: Avec l'article 52 de la loi sur les brevets, la tradition juridique européenne fit le choix délibéré de renoncer à l'universalité de la protection par le brevet. Elle se fia à une intelligente distinction entre la matière et l'information. Lors du dépôt de brevet, le bien informationnel est publié et en contrepartie, le contrôle sur toute une catégorie de biens physiques se trouve privatisé pour une certain temps. Cette distinction repose sur les définitions classiques de concepts comme la %(q:technicité) et la %(q:possibilité d'application industrielle), ainsi que de la liste limitative figurant à l'article 52-2.

#Fsl: Certains %(q:commentaires) apparemment modérés, comme %(q:par ailleurs il existe une longue tradition juridique européenne d'après laquelle la protection par le brevet des créations est cantonnée au domaine technique) ne peuvent réussir à masquer la tendance lourde du %(q:propos de base) à éliminer systématiquement ladite tradition juridique au profit d'une pratique américanisée.

#DMd: D'ailleurs, dans son %(q:propos de base), l'O.E.B. propose pour cette notion de %(q:technicité) une définition circulaire: %(q:instruction donnée au spécialiste de réaliser telle tâche technique avec tels moyens techniques). Pour l'O.E.B., la %(q:technicité) est la %(q:condition de base pour la brevetabilité) et découle de l'exigence d'une %(q:activité inventive). En d'autres termes, la %(q:technicité) n'a pas d'autre signification que celle déjà portée sur chaque contrat de brevet de par l'%(e:axiome de base) de l'%(e:utilisabilité dans un cadre commercial ou industriel) et de l'%(e:existence d'une communauté de spécialistes intéressés par le sujet), réalités implicites et fort difficiles à réfuter. L'exigence de technicité selon l'O.E.B. n'apporte donc plus aucun caractère restrictif, au contraire: la formulation du %(q:premier projet) - %(q:des inventions dans tous les domaines techniques)  - vise %(q:bien évidemment à faire en sorte que la protection par le brevet s'ouvre à toute invention technique de quelque domaine que ce soit). Et cette %(s:doctrine de l'universalité du brevet) doit être inscrite dans la loi!

#Die: L'avancée récente de l'organisation européenne des brevets constitue une dérive particulièrement notable, en ce qu'elle propose entre autres de supprimer purement et simplement la liste de toutes les limitations à la brevetabilité figurant à l'article 52 de la loi et de promouvoir à la place la notion de %(q:technicité) des objets brevetables. Or la définition de la %(q:technicité), qualifiée de %(q:dynamique), est laissée à la libre appréciation de l'O.E.B. Alors que jusque tout récemment la jurisprudence de la Cour Fédérale de Justice donnait une définition restrictive et cohérente de la %(q:technicité) comme %(q:solution automatisée d'un problème par l'utilisation des forces naturelles), cette même notion de technicité sert au contraire à l'O.E.B. de prétexte pour lever toute possible limitation à la brevetabilité.

#Wee: Depuis quelque temps déjà nous nous inquiétons des tentatives des juristes européens en charge des brevets pour se substituer au législateur afin de doter les détenteurs de brevets d'armes toujours plus puissantes. Les pouvoirs qui leurs seraient ainsi conférés deviendraient en effet exorbitants, comparés aux buts légitimes d'un dépôt de brevet. Il s'agit là d'une menace pour l'innovation, la concurrence et le développement des techniques de l'information, ainsi que pour les droits fondamentaux des citoyens de la société de l'information.

#VPn: Nous tenons à vous remercier pour vos informations et pour votre envoi du document CA/PL 25/00 %(q:propos de base de révision de la Convention Européenne sur les Brevets), du 27 juin 2000.

#ShK: Sehr geehrter Herr Karcher!

#ShL: Sehr geehrter Herr Lutz!

#SrC: Sehr geehrter Herr Dr. Hucko!

#SrW: Monsieur,

#DiW: L'Office Européen des Brevets (O.E.B.) organise à Münich, du 20 au 29 novembre prochains, une %(dk:conférence diplomatique) afin de décider de vastes modifications aux règles fondamentales du droit européen des brevets. Les gouvernements européens se mettent d'accord actuellement sur un %(bp:propos de base de révision de la Convention Européenne sur les Brevets) qui devrait permettre à l'Office Européen des Brevets, non seulement de breveter les programmes d'ordinateurs et les méthodes commerciales, mais également d'écarter toute possible limitation à la brevetabilité d'une manière générale. La Lettre Ouverte de la FFII constitue une démonstration, à l'attention des négociateurs du gouvernement allemand, des erreurs et des dangers de ce %(q:propos de base), et une esquisse d'alternatives plausibles. La FFII réclame également l'accréditation en tant qu'organisation non-gouvernementale, afin de participer à la conférence. Nous joingnons à cette lettre le texte d'une %(pe:conférence de presse), dans laquelle s'expriment des entreprises et des hommes politiques qui nous soutiennent.

#descr: Lettre ouverte en réponse au %(q:propos de base de révision du droit européen des brevets)

#title: l'Office Européen des Brevets veut suppimer toute limitation a la brevetabilité

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxepue28 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

