\section{Ausz\"uge aus TRIPS}
\label{tripscit}

\subsection*{TRIPS Artikel 10: Computerprogramme und Zusammenstellungen von Daten}
\label{art10}

\begin{enumerate}
\item
Computerprogramme, gleichviel, ob sie in Quellcode oder in Maschinenprogrammcode ausgedr\"uckt sind, werden als Werke der Literatur nach der Berner \"Ubereinkunft (1971) gesch\"utzt.

\item
Zusammenstellungen von Daten oder sonstigem Material, gleichviel, ob in maschinenlesbarer oder anderer Form, die aufgrund der Auswahl oder Anordnung ihres Inhalts geistige Sch\"opfungen bilden, werden als solche gesch\"utzt.  Dieser Schutz, der sich nicht auf die Daten oder das Material selbst erstreckt, gilt unbeschadet eines an den Daten oder dem Malerial selbst bestehenden Urheberrechts.
\end{enumerate}

\subsection*{TRIPS Artikel 27: Patentf\"ahige Gegenst\"ande}
\label{art27}

\begin{enumerate}
\item
Vorbehaltlich der Abs\"atze 2 und 3 ist vorzusehen, da\ss{} Patente f\"ur Erfindungen auf allen Gebieten der Technik erh\"altlich sind, sowohl f\"ur Erzeugnisse als auch f\"ur Verfahren, vorausgesetzt, da\ss{} sie neu sind, auf einer erfinderischen T\"atigkeit beruhen und industriell anwendbar sind.\footnote{Fu\ss{}note 5: Im Sinne dieses Artikels kann ein Mitglied die Begriffe ``erfinderische T\"atigkeit'' und ``industriell anwendbar'' als Synonyme der Begriffe ``nicht naheliegend'' beziehungsweise ``nutzbar'' auffassen.}

Vorbehaltlich des Artikels 65 Absatz 4, des Artikels 70 Absatz 8 und des Absatzes 3 dieses Artikels sind Patente erh\"altlich und k\"onnen Patentrechte ausge\"ubt werden, ohne da\ss{} hinsichtlich des Ortes der Erfindung, des Gebiets der Technik oder danach, ob die Erzeugnisse eingef\"uhrt oder im Land hergestellt werden, diskriminiert werden darf.

\item
Die Mitglieder k\"onnen Erfindungen von der Patentierbarkeit ausschlie\ss{}en, wenn die Verhinderung ihrer gewerblichen Verwertung innerhalb ihres Hoheitsgebiets zum Schutz der \"offentlichen Ordnung oder der guten Sitten einschlie\ss{}lich des Schutzes des Lebens oder der Gesundheit von Menschen, Tieren oder Pflanzen oder zur Vermeidung einer ernsten Sch\"adigung der Umwelt notwendig ist, vorausgesetzt, da\ss{} ein solcher Ausschlu\ss{} nicht nur deshalb vorgenommen wird, weil die Verwertung durch ihr Recht verboten ist.

\item
Die Mitglieder k\"onnen von der Patentierbarkeit auch ausschlie\ss{}en
\begin{enumerate}
\item
diagnostische, therapeutische und chirurgische Verfahren f\"ur die
Behandlung von Menschen oder Tieren;

\item
Pflanzen und Tiere, mit Ausnahme von Mikroorganismen, und im wesentlichen biologische Verfahren f\"ur die Z\"uchtung von Pflanzen oder Tieren mit Ausnahme von nicht biologischen und mikrobiologischen Verfahren. Die Mitglieder sehen jedoch den Schutz von Pflanzensorten entweder durch Patente oder durch ein wirksames System sui generis oder durch eine Kombination beider vor. Die Bestimmungen dieses Buchstabens werden vier Jahre nach dem Inkrafttreten des WTO-\"Ubereinkommens \"uberpr\"uft.
\end{enumerate}
\end{enumerate}

\section{TRIPS als Hebel f\"ur die Expansion des Patentwesens}
\label{tripsetc}

Der TRIPS-Vertrag enth\"alt allerlei Klauseln, die als Hebel zur Auweitung des Patentwesens dienen k\"onnen.

Artikel 33 verlangt, dass alle Patente eine Mindestlaufzeit von 20 Jahren haben m\"ussen.

Dies ist wichtig zu wissen, denn es er\"ubrigt wohlmeinende Vorschl\"age wie den von Amazon-Chef Jeff Bezos\footnote{Bezos schlug Anfang M\"arz 2000 allerlei Ma{\ss}nahmen zur Milderung der durch Softwarepatente verursachten H\"arten vor und versprach, sich kraftvoll daf\"ur einzusetzen, s.\ http://www.amazon.com/exec/obidos/subst/misc/patents.html/103-3817438-0036605}, man solle die Patentlaufzeit im Softwarebereich auf 3-5 Jahre k\"urzen.

Gleichzeitig ist zu fragen, ob die Beanspruchung immaterieller Gegenst\"ande aus dem Bereich der Programmlogik, der Kommunikationsschnittstellen und der Organisationsmethoden nicht dazu geeignet ist, Handelsbarrieren aufzubauen, die im Gegensatz zum Geist von TRIPS und anderen Freihandels-Abkommen (wie etwa dem GATT-\"Ubereinkommen von 1947, das in Art. II:4 ausdr\"ucklich Handelsmonopole verbietet) stehen.

Der TRIPS-Vertrag wird seit 1994 unentwegt als Vorwand zur Ausweitung des europ\"aischen Patentwesens verwendet.  Es setzt dem Patentwesen nur Untergrenzen (Art. 27) aber ausdr\"ucklich keine Obergrenzen (Art. 1).  TRIPS mag wie ein brillianter Handstreich einer Gruppe von Patentjuristen erscheinen, denen es auf dem internationalen diplomatischen Parkett gelang, ihre patentuniversalistische Ideologie in einen Freihandelsvertrag hineinzumogeln.  Bis 2000 wollen Europas gesetzgebende Patentjuristen nun die patentuniversalistischen TRIPS-Klauseln in das EP\"U umschreiben und \"uberdies dem TRIPS-Vertrag selber auf dem Wege einer EU-Richtlinie zu gemeinschaftsrechtlicher Geltung verhelfen.  Europas Politiker sollten sich diesen Bestrebungen widersetzen und umgekehrt Wege zu einer baldigen Korrektur der patentuniversalistischen Tendenzen des TRIPS-Vertrages und anderer internationaler Vertr\"age finden.

