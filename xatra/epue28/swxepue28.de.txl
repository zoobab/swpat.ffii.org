<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Mue: Mit freundlichen Grüßen

#Wit: Wir wären Ihnen als Bürger unendlich verbunden, wenn Sie sich bei den kommenden Konsultationen bereit fänden, der Patentlobby zu widerstehen und das Gemeinwohl zu verteidigen.

#efe: eine baldige Stellungnahme des BMJ zu den Vorschlägen in diesem Brief, einschließlich den Forderungen der zitierten Politiker

#uul: unsere frühestmögliche Einbeziehung in die kommenden Konsultationen, Rederecht für einen Vertreter der Eurolinux-Allianz

#AWr: Akkreditierung des FFII e.V. und der Eurolinux-Allianz als Teilnehmer der diplomatischen Konferenz im November

#Wbe: Wir bitten um

#Whr: Wir glauben, dass diese Stimmen ebenso wie die Stimmen von ca 30000 Unterzeichnern der Eurolinux-Petition mehr Gewicht haben sollten als der Wunsch einiger Patentfachleute, am Wohlstand der Neuen Wirtschaft zu partizipieren.

#Aen: Da das Europäische Patentamt keinerlei Studie publiziert hat, um die Ausweitung der Patentierbarkeit auf Computerprogramme zu rechtfertigen, obwohl Wirtschaftswissenschaftler bewiesen haben, dass das Patentsystem zu einer Minderung der Innovation in der Softwareökonomie führen könnte, scheint es mir ferner an der Zeit, eine Betriebsprüfung des Europäischen Patentamtes in Auftrag zu geben, um nach Mitteln zu suchen, wie man die Entscheidungen dieser Institution besser kontrollieren und in Einklang mit dem Allgemeininteresse und den Grundprinzipien einer unparteiischen Rechtsprechung bringen kann.

#qey: dass eine frei verfügbare und über freie Software zugängliche umfassende Patentdatenbank aufgebaut wird, um den KMU die Mittel an die Hand zu geben, mit Patentgefahren in Europa und der Welt besser fertig zu werden.

#qdi: dass unverzüglich eine offene und demokratische Diskussion auf Grundlage ausführlicher wissenschaftlicher Studien über die wirtschaftlichen und gesellschaftlichen Wirkungen des Patentsystems auf die Informationsgesellschaft in Angriff genommen wird.

#qWo: dass materielle Produkte oder materielle Erweiterungen von immateriellen Produkten (z.B. MP3-Abspielgeräte) patentiert werden könnten, sofern die Kriterien der Neuheit, Technizität und industriellen Anwendbarkeit dieses materiellen Produktes erfüllt werden, und zwar unabhängig von den darin verwendeten Softwareelementen betrachtet.

#qnW: dass die Begriffe %(q:technisch), %(q:gewerbliche Anwendung) und %(q:Programm als solches) derart geklärt werden, dass kein Informationswerk und kein immaterielles Produkt (einschließlich of Informationssystemen laufende Software) als unmittelbar oder mittelbar patentverletzender Gegenstand in Betracht kommen kann.

#qlW: dass ein %(q:Recht, seine %(tpe:eigenen Werke:einschließlich Computerprogramme) zu verbreiten) und ein %(tpe:%(q:Recht auf Kompatibilität):wie im %(tpe:Gesetzesvorschlag der Abgeordneten Le Déaux, Paul, Cohen und Bloche:www.osslaw.org) ausgeführt) gesetzlich verankert werden

#dan: dass im November 2000 Art 52 des Münchener Übereinkommens nicht geändert wird und somit das Trojanische Pferd nicht aktiviert wird, das derzeit beim EPA schläft, wo zahlreiche missbräuchlich an außereuropäische Unternehmen vergebene Internet-Patente von einem Tag auf den anderen die Neue Wirtschaft Frankreichs und Europas bedrohen können.

#AWi: Ich würde mich Ihnen verbunden fühlen, wenn Sie in den kommenden Konsultationen auf nationaler, europäischer und globaler Ebene alle Ihnen zu Gebote stehenden Mittel einsetzen würden, um zu verlangen,

#Mev: Juristische Standardwerke wie «Lamy Droit Informatique» weisen allerdings darauf hin, dass diese Patente angesichts der offensichtlichen Widersprüche zwischen dem geltenden Gesetz und der Rechtsprechung des EPA keinerlei Wert haben außer demjenigen, den die jeweiligen Gerichte ihnen zuzuerkennen bereit sind.  Im Falle eines Rechtsstreits besteht kein Verlass darauf, dass ein nationaler Richter die Ansprüche dieser Patente anerkennen würde, da sie sich offensichtlich auf Gegenstände beziehen, deren Patentierung dem Geist des Gesetzes widerspricht.  Daher warten die Inhaber der Softwarepatente, der E-Geschäftsmethoden-Patente und der Internet-Patente nur noch auf eines, bevor sie sich entschließen, die französischen und europäischen Protagonisten der Neuen Wirtschaft anzugreifen:  die Revision des Münchener Übereinkommens, die sich anschickt, den Ausschluss der Computerprogramme von der Patentierbarkeit zu beseitigen.

#Cts: Diese unkontrollierte Ausweitung des Patentsystems in den Bereich der Software hinein gefährdet in zunehmendem Maße die europäischen IT-Unternehmen, die Autoren freier Software und die Grundarchitektur der Informationsgesellschaft.  Mehr als 10000 Softwarepatente wurden in den letzten 10 Jahren bei den nationalen Patentämtern durch vom EPA inspirierte juristische Winkelzüge angemeldet, während selbst die Anmeldungsanleitungen der Patentämter noch immer klar sagen, dass Computerprogramme nicht patentierbar sind.  Mehr als 75% dieser Patente wurden von außereuropäischen Unternehmen angemeldet.  Nicht wenige dieser Softwarepatente beziehen sich auf Verfahren des elektronischen Geschäftsverkehrs sowie Organisations- und Lehrmethoden.

#Lio: Das Patentsystem hat sich in den letzten Jahren weit über den Bereich seiner historischen, ökonomischen und moralischen Existenzberechtigung hinaus ausgedehnt.  Diese Ausdehnung ist das Ergebnis von Gerichtsentscheidungen des Europäischen Patentamts (EPA), die bisweilen im Gegensatz zum Geist der Gesetze stehen, wie sie von den Gesetzgebern verabschiedet wurden, wobei die Unterzeichnerstaaten des Münchener Übereinkommens meist nicht über die nötigen Mittel verfügen, um die wirtschaftlichen und gesellschaftlichen Auswirkungen dieser Entscheidungen zu kontrollieren.  Insbesondere glaube ich, dass das EPA, indem es allen Ernstes behauptete, ein %(q:Computerprogramm mit technischen Wirkungen) sei kein %(q:Computerprogramm als solches) und könne daher zum Gegenstand von Patentansprüchen werden, eindeutig seine Macht missbraucht hat.  Das EPA hat eine Rechtsprechung entwickelt, die in offensichtlichem Gegensatz zu dem internationalen Vertrag steht, mit dessen Einhaltung es betraut wurde.   Denn alle Computerprogramme haben einen technischen Effekt, wie die Europäischen Patentberater zu Recht anmahnten, die sich 1997 im EPA zu einem %(q:Runden Tisch) versammelten.

#Dtr: Le Déaut ist Mitinitiator des französischen %(oa:Gesetzesentwurfes) für ein %(q:Recht auf Kompatibilität) und die Durchsetzung von Offenheitsstandards in der Öffentlichen Verwaltung.  Er sieht die Münchener und Brüsseler Patentpolitik als eine Gefahr für sein Anliegen und sandte daher im Juli 2000 einen %(lb:Offenen Brief) an zahlreiche zuständige Politiker in Frankreich, den wir hier übersetzen:

#Aiz: Aber während bei der Legislative noch vollkommene Unklarheit herrscht, schreitet die Judikative bereits zur Tat, gewährt Tausende von Softwarepatenten und drängt auf Änderung der Gesetzesregeln.  Es ist daher höchste Zeit für uns als Gesetzgeber, uns um diese Fragen zu kümmern.

#ScW: Selbst wenn es gelänge, Bereiche der Informationstechnik zu finden, in denen Patente nachweislich vorteilhaft wirken oder gewirkt haben, müsste man noch immer untersuchen, ob eventuelle schädliche Nebenwirkungen der Patentierung diese Vorteile nicht überwiegen.

#Itm: In technologiepolitischen Fachkreisen hört man immer wieder die Behauptung, das Patentsystem müsse auf gewisse Bereiche der Informationstechnik ausgeweitet werden, weil sonst deren Investitionen nicht genügend geschützt würden.  Diese Behauptung wurde bisher allerdings immer nur als abstrakte Grundwahrheit weitergegeben und niemals anhand von Tatsachen der deutschen oder europäischen IT-Wirtschaft belegt.

#Dxf: Der %(vv:Virtuelle Ortsverein der SPD) unterstützt die Eurolinux-Petition.  Sein Gründer Jörg Tauss gab am 8. August 2000 auf Anfrage des FFII folgend vorläufige Grundsatzerklärung zum %(q:Basisvorschlag) ab:

#DDf: Die Ermöglichung der Patentierung von Software würde darüber hinaus erhebliche technische und administrative Probleme schaffen: in der Zeit, die Anmeldung eines Patentes derzeit benötigt (derzeit 2 Jahre), ist das Patent längst veraltet. Die Dokumentation der Patente wäre extrem aufwendig. Kleine und mittlere Unternehmen würden durch die Patentierung benachteiligt: Große Firmen, die über die Ressourcen verfügen, die Patententwicklung zu verfolgen und Patente anzumelden könnten auf diese Weise zusätzliche Erträge erwirtschaften. Der Wettbewerb würde sich von der schnellen Umsetzung von Innovationen auf juristische Streitereien verlagern und der technische Fortschritt würde behindert werden. Das muss verhindert werden!

#Asm: Allerdings ist bereits in den letzten Jahren in Europa Software zunehmend als Bestandteil technischer Verfahren patentiert worden, denn: Technische Verfahren, die Computerprogramme beinhalten, sind patentierbar. Beispiel dafür ist eine computergesteuerte Werkzeugmaschine, die insgesamt patentierbar ist. Es mangelt derzeit an eingehenden ökonomischen Analysen, die die Wirkungen einer möglichen Patentierung von Software beschreiben. Wir sehen allerdings die Gefahr einer weiteren Verstärkung von Bürokratie mit dem Effekt, dass dringend notwendige Innovationen behindert werden.

#IgA: In den USA wird der Wettbewerb bereits erheblich auch durch die Patentierung von Geschäftsideen behindert. Viel zitiertes Beispiel ist das Patent von Amazon auf das one-click-Verfahren bei der Bestellung von Gütern im Internet.

#Bwd: Bei Software-Entwicklern und in der Open-Source-Szene besteht erhebliche Verunsicherung, weil befürchtet wird, dass über die Novelle des EPÜ und eine angekündigte Richtlinie der Europäischen Kommission in Europa amerikanische Verhältnisse eingeführt werden sollen.

#CWa: Computerprogramme %(q:als solche) sind nach dem Europäischen Patent-Übereinkommen (EPÜ) nicht patentierbar. In den USA dagegen ist grundsätzlich alles menschengemachte patentierbar.

#Vir: Gründer werden durch die Debatte über die mögliche Patentierbarkeit von Software und Geschäftsideen auch in Europa verunsichert.  Hier muss schnell Klarheit geschaffen werden.

#F4U: Frau Wolf %(mw:sagte) am 14. Juni 2000 auf einer IT-Unternehmensgründer-Tagung:

#vie: Der AdR hat diese Studie nach ausführlicher Prüfung im November 1999 als offizielles Standpunktpapier übernommen und %(sn:veröffentlicht).  Damit trägt sie die Unterschrift von über 200 führenden europäischen Regionalpolitikern, einschl. Stoiber, Teufel, Diepgen u.v.m.

#dih: Abgeordneter der Moselregion in der Französischen Nationalversammlung

#Vns: Vorsitzender des Parlamentarischen Unterausschusses für die Neuen Medien

#MsW: MdB

#Wse: Wirtschaftspolitische Sprecherin der Grünen Fraktion

#CtW: Ausschuss der Regionen der Europäischen Union

#FWW: Falls beim BMJ oder beim Verwaltungsrat inzwischen doch Interesse an einer Auseinandersetzung mit den wirtschaftspolitischen Risiken der Softwarepatentierung bestehen sollte, werden wir uns bemühen, unsere Argumentation vor der nächsten Verwaltungsratssitzung (5.-8. September) ausführlich auszuarbeiten.  Vorläufig möchten wir uns damit begnügen, aus einigen Stellungnahme prominenter Politiker zu zitieren:

#Dke: Die wirtschaftspolitischen Risiken der Softwarepatentierung sind altbekannt, aber selbst Stellungnahmen von prominenter Seite haben in den gesetzgebenden Kreisen des europäischen Patentwesens bisher nur ein %(q:Schweigen im Walde) ausgelöst.

#OKt: Obwohl Art. 52 in seiner bestehenden Form gemäß obiger Interpretation vollkommen TRIPS-konform ist, gibt es gute Gründe, eine Revidierung von TRIPS anzustreben.  Z.B. kann die TRIPS-Vorschrift einer Mindestlaufzeit von 20 Jahren für Patente %(q:auf allen Gebieten der Technik) nicht der Weisheit letzter Schluss sein.  Ferner fehlen in TRIPS Vorschriften bezüglich der Grenzen des Patentwesens.  Gerade die Patentierung von Geschäftsverfahren wirkt der Freiheit des Welthandels entgegen, die ja das übergeordnete Ziel des TRIPS-Vertrages ist.  Und die Patentierung von Computerprogrammen untergräbt das Urheberrecht, dessen Geltung für Computerprogramme in Art 10 TRIPS festgeschrieben ist.

#Ee2: Eine Umschreibung des Art 52 in diesem Sinne wäre jedoch ein ehrgeiziges Projekt, welches diesen November kaum in angemessener Weise durchzuführen sein dürfte.  Deshalb schlagen wir vor, den Artikel 52 einfach unverändert beizubehalten und schrittweise auf eine Interpretation im obigen Sinne hinzuarbeiten.

#Dtf: Insoweit die Abgrenzung zwischen physischen und logischen Prozessinnovationen im einzelnen die Richter überfordern könnte, wäre es denkbar, hierfür die Pufferzone der %(q:weichen Patente) zu schaffen.  Erfinderische Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herkömmlichen Rechenapparatur genügt, könnten insoweit monopolisiert werden, wie sie nicht auf einem Universalrechner ablaufen.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, würde er es vermutlich vorziehen, einem Patentinhaber maßvolle Lizenzgebühren zu zahlen.  Je physischer die Prozessinnovation, desto lukrativer wird das weiche Patent.  Neuere ökonomische Studien zeigen, dass %(q:weiche Schutzrechte) dieser Art wirtschaftspolitisch erwünscht sein können.  Außerdem hätte die Einführung der weichen Pufferzone den Vorteil, dass die meisten der vom EPA gesetzeswidrig gewährten Softwarepatente ohne Bruch in einen allerseits akzeptablen rechtlichen Status überführt werden könnten.

#Set: Schließlich ließe sich auch der Erfindungsbegriff im Lichte der obigen Überlegungen sinnvoll definieren.  Der vom EPA erarbeitete Begriff der %(q:technischen Lösung eines technischen Problems) wäre zu präzisieren: %(q:auf neuem Wissen über beherrschbare Naturkräfte beruhende Lösung eines durch die Herstellung materieller Güter aufgeworfenen Problems).  M.a.W.:  Problemlösungen, die mithilfe künstlicher Intelligenz aus bekannten Modellen der realen Welt abgeleitet werden könnten, sind nicht erfinderisch.  Patente sind die Belohnung für harte Laborarbeit, nicht für geschicktes Rechnen.

#BCr: BGH-Urteil %(q:Chinesische Schriftzeichen) 1992

#Dsd: Die klassische %(q:Kerntheorie), die beim BGH %(cs:bis vor kurzem) zur Anwendung kam, wird bestätigt: der Kern der Erfindung muss im technischen (d.h. physischen, Naturkräfte einsetzenden) Bereich liegen.  Mithilfe der Kerntheorie wird der Bereich der Erfindungen an den Bereich der patentierbaren Gegenstände angeglichen: da informationelle Gegenstände nicht beansprucht werden können, können auch Prozesse, die sich im Einsatz einer neuartigen Informationsstruktur auf einer herkömmlichen Rechenapparatur erschöpfen (= Computerprogramme als solche) nicht als %(q:Erfindungen) im Sinne des Patentrechts gelten.

#Nuc: NB: Der Ausschluss therapeutischer und chirurgischer Verfahren sollte ebenfalls in Art 52 verbleiben und nicht nach Art 53 transferiert werden, damit klar bleibt, dass diesem Ausschluss die Forderung nach %(q:industrieller Anwendbarkeit) zugrunde liegt und es sich nicht um eine Ad-hoc-Regelung handelt.

#DdF: Dies könnte bedeuteten, dass das Wort %(q:gewerblich) in der deutschen Fassung zu %(q:industriell) geändert wird, ähnlich wie laut %(q:Basisvorschlag) in Art 23.1.1. das Wort %(q:Funktion) in der deutschen Fassung zu %(q:Amt) geändert wird: %(q:Die englische und französische Fassung bleiben hiervon unberührt).

#DDo: Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten.

#Doa: Damit ist folgendem Szenario ein Riegel vorzuschieben, welches der Software-Referent der Union der Europäischen Patentberater, Patentanwalt Jürgen Betten, in einem Rundschreiben an seine Mandanten ausmalt:

#DTe: Der Begriff %(tpe:%(q:gewerblich):engl. industrial, frz. industriel, jap.: sangy=o = produzierendes Gewerbe) wird ebenfalls präzisiert, und zwar im Sinne des traditionellen Verständnisses von %(q:verarbeitende Industrie). Dieser Begriff ist mit dem klassischen Technikbegriff fast identisch.  Er fordert, dass es um automatisierte Prozesse zur Fertigung materieller Güter unter Einsatz von Naturkräften gehen muss.

#enl: Hier liegt manchmal eine schwierige Grenze, s. BGH-Urteil %(q:Antiblockiersystem) von 1980.  Der ABS-Patentantrag wurde 1976 vom BPatG abgelehnt, weil die Erfindung im Bereich der programmierten Steuerung lag, aber 1980 vom BGH für zulässig erklärt, weil sie auf Experimenten mit Naturkräften beruhte und nicht etwa durch mathematische Operationen auf bekannte Rechenmodelle zurückgeführt werden konnte.  Gegenstand der Patentansprüche war eine Fahrzeugsteuerungsprozess, nicht ein Computerprogramm.

#Drc: Der Begriff %(q:technisch) wird im Sinne der klassischen BGH-Definition erklärt:  Anwendung beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit.  D.h. ein neuartiger chemischer Prozess ist technisch, das Computerprogramm, welches ihn vielleicht steuert, jedoch nicht.  Eine neuartige Naturkräfte-Anwendung ist patentierbar, aber die Veröffentlichung eines Programms zu ihrer Steuerung stellt weder eine unmittelbare noch eine mittelbare Patentverletzung dar.

#Ker: Kurz zusammengefasst:  Wir fordern vorerst die unveränderte Beibehaltung des Artikels 52.  Sicherlich wäre es intellektuell reizvoll, den Artikel umzuformulieren, um ihn an Art 27 TRIPS anzunähern und gleichzeitig der Liste der Einschränkungen eine systematische Interpretation zu geben.  Diese Interpretation könnte durch folgende Gedanken gekennzeichnet sein:

#Uen: Der %(q:Basisvorschlag) spricht von einem %(q:breiten Konsens für die Patentierbarkeit von Computerprogrammen), während sich mittlerweile 30000 Bürger, darunter 400 leitende Angestellte von IT-Unternehmen für ein %(q:softwarepatentfreies Europa) ausgesprochen haben.  Über die Webseiten der Eurolinux-Petition %{PET} können Sie auch unschwer die Grundlagen unserer Argumentation finden.  Eine Diskussion speziell zum %(q:Basisvorschlag) hat auch im Netz stattgefunden und ist unter %{SWP} einsehbar.

#eei: hohe Mittlungskosten und Marktzutrittsbarrieren, welche die politisch erwünschte Gründerkultur (Startups) gerade im Softwarebereich unmöglich machen, Strangulierung einer florierenden europäischen IT-Landschaft, Erzwingung wertevernichtender Aufkäufe und Fusionen zugunsten weniger i.d.R. amerikanischer Monopolisten, Abwürgen des derzeit bedeutendsten Arbeits- und Wachstumsmotors

#gEe: goldene Zeiten für professionelle Abmahner und Maximalausbeuter von fragwürdigen Rechtstiteln, juristischer Bürgerkrieg, außergerichtliche Schutzgeldzahlungen mit beiderseitig gesichtswahrendem Geheimhaltungsabkommen, informelle wenn nicht gar mafiöse Strukturen des kollektiven Selbstschutzes

#esg: Privatisierung informationstechnischer Infrastrukturen zu Lasten der allgemeinen Zugänglichkeit und öffentlichen Sicherheit, Förderung der Geheimhaltung von Programmtexten, Erstickung der wirtschaftspolitisch erwünschten Opensource-Kultur, Behinderung der (in den Koalitionsvereinbarung vom 20.10.1998 zum Ziel erklärten) %(q:beschleunigten Nutzung und Verbreitung der Informationstechniken in der Gesellschaft),  Behinderung der Heranbildung von IT-Nachwuchs, Verlangsamen des informationstechnischen Fortschritts, Verletzung von Art. 95, 151, 157 des EG-Gründungsvertrages (Vertrag von Rom) und Art 81.1b des EU-Gründungsvertrages (Vertrag von Amsterdam)

#Blc: Bereits heute verbietet Microsoft die Verwendung seines patentierten Videoformates ASF, um dadurch das von Filmherstellern begehrte Verbot der Privatkopie auf außergesetzlichem Wege durchsetzen zukönnen. Harvard-Verfassungsrechtler Professor Lawrence Lessig %(ll:warnt) in diesem Zusammenhang vor %(q:Software-Stalinismus).  Programmtexte wirken bisweilen wie inoffizielle Gesetzetexte der Informationsgesellschaft.  Schon heute erreichen die Großen der Branche wie z.B. Microsoft ihre Wertschöpfung vor allem durch Missbrauch der versteckten Gesetzesmacht von Software-Schnittstellen.  Diese Praxis wird durch E-Patente gefördert und gefestigt.

#Emr: Einschränkung der Ausdrucksfreiheit, Umfunktionierung von Patenten zum Zwecke politischer Herrschaft

#EaW: Auch wer ganz eigenständig jahrelange harte Programmierarbeit geleistet hat, kann ab sofort sein Werk nicht mehr sein eigen nennen und ist stattdessen möglicherweise auf die Gnade von Patentbesitzern angewiesen, die selber nie eine Zeile Programmtext geschrieben haben.  Ebenso wie eine zu starke Besteuerung die Steuereinnahmen mindert kann, können zu rigide Eigentumsrechte auf eine Enteignung der Leistungsträger hinauslaufen.

#euN: tendenzielle Aufhebung des Urheberrechts, Enteignung von Programmierern

#dde: Aktivierung von über 10000 gesetzeswidrig angemeldeten Softwarepatenten (zu über 75% außereuropäischer Herkunft), die derzeit als Trojanische Pferde im europäischen Patentamt schlafen und auf ihre Legalisierung durch die Diplomatische Konferenz warten, bevor sie Europas Programmautoren zu Leibe rücken

#pac: patentierbar werden ab sofort Informationsstrukturen, Geschäftsverfahren, Lern- und Lehrmethoden, gesellschaftliche Organisationsmethoden, geistige und mathematische Verfahren, Dienstleistungen, Banken, Handel, Handwerk, Kunstschaffen, musikalischen Kompositionstechniken und praktisch alles, wofür das Europäische Patentamt Fachprüfer einzustellen bereit ist

#eeW: %(q:Patentierbarkeit ohne Grenzen)

#euu: einen weiteren Verlust an Gewaltenteilung, weitere Konzentration von Regelungskompetenzen, Rechtsprechungsgewalten und Zwangsmitteln in Kombination mit quasi-unternehmerischen Eigeninteressen bei einer Behörde, die schon in der Vergangenheit wenig Respekt für die Vorgaben des Gesetzgebers gezeigt und sich als de facto unkontrollierbar erwiesen hat

#Dde: Informationsgüter und Patente bilden zusammen ein giftiges Gemisch, dessen Wirkung in mehreren diesem Schreiben beigefügten volkswirtschaftlichen Studien ausführlich beschrieben und analysiert wurde (s. %{URL}).  Der vorliegende Entgrenzungs-Vorstoß des EPA ist schon auf der begrifflichen Ebene widersprüchlich und birgt für die Praxis einige Risiken, die wir hier nur kurz aufzählen möchten:

#Mnn: Hiermit ist die Grenze vom industriellen zum universellen Patentwesen überschritten.  Andere Grenzen (wie etwa die zwischen Computerprogramm und Geschäftsverfahren oder gar zwischen %(q:technischem Geschäftsverfahren) und %(q:untechnischem Geschäftsverfahren)) sauber definieren zu wollen, wäre Traumtänzerei.  Auch das EPA unternimmt erst gar nicht diesen Versuch.  Es patentiert schon heute Geschäftsmethoden und hat auch schon die Forderung nach einem %(q:zusätzlichen technischen Effekt) für überholt %(a6:erklärt).  Das EPA behält sich ferner, wie oben zitiert, die jederzeitige %(q:Anpassung an technische Entwicklungen) vor.  Darunter dürfte in der Praxis z.B. zu verstehen sein, dass etwa innovative musikalische Kompositionstechniken durch einen einfachen Beschluss des EPA-Verwaltungsrats patentierbar werden könnten, sobald genügend Anfragen beim EPA vorliegen und genügend Prüfer mit musikwissenschaftlicher Ausbildung eingestellt wurden.

#Mne: Patentiert werden nunmehr, anders als in der europäischen Rechtstradition des industriellen Patentwesens, nicht mehr nur bestimmte Klassen materieller %(q:Produkte und Prozesse), sondern die zur Beschreibung solcher Produkte und Prozesse dienende Information selber.  Genau genommen kann demnach schon derjenige ein Patent verletzen, der eine Patentschrift kopiert.  Denn in einer Patentschrift sollte ein Ausführungsbeispiel enthalten sein, welches %(q:den durchschnittlichen Fachmann zur vollständigen Nachvollziehung der Erfindung befähigt), d.h. im Idealfall bei Softwarepatenten ein patentverletzender Programmtext.

#Deu: Diese %(q:rechtspolitisch) motivierten %(q:Bemühungen der Rechtssprechung) führten zu den vom EPA-Basisvorschlag zitierten Beschwerdekammer-Urteilen von 1999 zum Thema %(q:Computerprogrammprodukt/IBM) und %(q:Computerprogramm/IBM).

#Dgi: Daneben %(s:bemüht sich die Rechtsprechung) ..., die derzeitige Gesetzesregelung so eng auszulegen, dass praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentfähig sind, wenn sie neu und erfinderisch sind.

#lgs: Weiteren Auftrieb erhielt die europäische Diskussion durch die Resolution der AIPPI zur Frage Q133 vom 22.4.1997 in Wien, die in dem Satz zusammengefaßt werden kann: %(q:Alle auf einem Computer ablauffähigen Programme sind technischer Natur und daher patentfähig, wenn sie neu und erfinderisch sind), sowie den Round Table der %(un:UNION) am 9./10.12.1997, als im Europäischen Patentamt 100 Fachleute aus zwanzig europäischen Ländern über die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem ähnlichen Eindruck kamen wie die AIPPI.  Zudem wurde darauf hingewiesen, dass %(s:das Konzept des EPA zum %(q:technichen Charakter) weder von den Patentanmeldern noch von den nationalen Patentämtern richtig verstanden) würde.  Viele Teilnehmer machten klar, dass eigentlich alle Computerprogramme dem Wesen nach %(q:technischen Charakter) aufweisen würden.  %(s:Seit dieser Zeit wird praktisch %(q:auf allen Kanälen) daran gearbeitet, einen Weg zu finden, wie der irreführende Ausschluss von %(q:Computerprogrammen als solchen) aus dem europäischen Patentgesetz entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregeleungen in %(tpe:Art. 52 Abs. 2 EPÜ:§1 PatG) zur Disposition stehen.) ...

#Dtn: %(s:Der Ausschluss von %(q:Computerprogrammen als solchen) vom Patentschutz in %(tpe:Art. 52 EPÜ:§1 PatG) wird seit langem als rechtspolitische Fehlentscheidung angesehen), zumal der Ausschluss von breiten Verkehrskreisen - bis heute - missverstanden und meist als Ausschluss von Computerprogrammen allgemein verstanden wird.

#MWh: Auch diese Formulierungen können nicht darüber hinwegtäuschen, dass das EPA nach der Verabschiedung des EPÜ 1973 bis weit in die 80er Jahren die Vorgaben des Gesetzgebers streng befolgte und die Patentierung von Erfindungen ablehnte, die %(q:ein Computerprogramm zum Gegenstand) hatten, d.h. bei denen die Veröffentlichung eines Computerprogramms genügen könnte, um das Patent zu verletzen.  Seit 1986 wurde das leidige %(q:Software-Patentierverbot) jedoch Schritt für Schritt aus wirtschafspolitischen Erwägungen heraus ausgehebelt.  Damals beschloss das EPA, dass %(q:ein Computerprogramm mit einem zusätzlichen technischen Effekt) nicht ein %(q:Computerprogramm als solches) sei und entwickelte dann eine Rechtsprechung, die kaum mehr jemand verstand.  Hierzu %(cr:schreibt) der Software-Referent der Union der Europäischen Berater für den Gewerblichen Rechtschutz, PA Jürgen Betten:

#IWm: Inzwischen scheint sich ein breiter Konsens herauszubilden, dass %(s:Computerprogramme aus der Liste der nicht patentierbaren Erfindungen nach Artikel 52%(pet:2) EPÜ gestrichen werden sollten).  Das EPA und seine Beschwerdekammern haben das EPÜ stets so ausgelegt und angewendet, dass diese Ausnahmevorschrift einen angemessenen Schutz für softwarebezogene Erfindungen, also Erfindungen, die ein Computerprogramm zum Gegenstand haben oder einschließen, in keiner Weise verhindert.  In jüngeren Entscheidungen der Beschwerdekammern (s. T 1173/97 - Computerprogrammprodukt/IBM, ABI. EPA 1999, 609) wurde in der Tat bestätigt, dass in der Regel Computerprogramme nach dem EPÜ patentierbare Gegenstände sind.  Die geltende Ausnahmevorschrift für Computerprogramme ist damit de facto überholt.

#Dbi: Des weiteren wird vorgeschlagen, %(s:Artikel 52%(pet:2) und %(pet:3) EPÜ zu streichen und in die Ausführungsordnung zu überführen), mit der Vorgabe, %(s:Computerprogramme) aus dem Ausnahmenkatalog in Artikel 52(2) EPÜ %(s:herauszunehmen).  ...  Die vorgeschlagene Überführung in die Regeln würde es .. erleichtern, diese Vorschriften bei Bedarf an rechtliche, wirtschaftliche oder technische Entwicklungen anzupassen.

#Ivg: In den Erläuterungen zum %(q:Basisvorschlag) wird eine Überschreitung der Grenze von der Materie zur Information ausdrücklich gefordert:

#ZeG: Zwischen Materie und Information (Hardware und Software, physischen und logischen Strukturen) verläuft eine fundamentale Grenze mit starken wirtschaftlichen Auswirkungen.  Materielle Güter sind ortsgebunden und ihre Serienfertigung erfordert industriellen Aufwand; informationelle Güter hingegen sind von ihrem Träger unabhängig und lassen sich tendenziell kostenlos vervielfältigen.  Materielle Güter sind zeitgebunden und lassen sich nach einmaligem Konsum erneut in identischer Form verkaufen; informationelle Güter hingegen sind unkonsumierbar und gewinnen nur aus Innovation (immer neue Versionen) einen wirtschaftlichen Wert.
Materielle Güter sind Geldwerte, informationelle Güter sind Machtfaktoren.
Materielle Güter bringen maximale Geldeinnahmen bei weitestgehender Verteilung, informationelle Güter erlauben maximalen Machtzuwachs bei weitestgehender Verknappung (Etablierung von exklusiven %(q:Standards), Geheimhaltung von %(q:Schlüsselinformationen)).
Materielle Güter verlangen nach privaten Besitzern, informationelle Güter hingegen gehen über kurz oder lang ins Gemeineigentum über, wo sie ihren volkswirtschaftlichen Wert oft erst richtig entfalten können.
Materielle Güter gehören meist auf unbefristete Zeit einem naturrechtlichen Eigentümer; für informationelle Güter gibt es hingegen befristete Ausschlussrechte, die der Staat aus wirtschaftspolitischen Erwägungen gewährt.  
Das Patentrecht erlaubt die befristete Monopolisierung bestimmter Klassen materieller Güter; für informationelle Güter hingegen gelten das Urheberrecht und die Ausdrucksfreiheit.

#Dta: Die europäische Rechtstradition nach Art 52 EPÜ hat bewusst auf einen Universalitätsanspruch für das Patentwesen verzichtet.  Sie beruht auf einer geschickten Trennung zwischen Materie und Information.  Ein wertvolles Informationsgut wird mit der Patentschrift veröffentlicht, und im Gegenzug wird die Kontrolle über eine Klasse wertvoller materieller Güter befristet privatisiert.  Diese Trennung kam in den klassischen Definitionen von Begriffen wie %(q:Technizität) und %(q:industrielle Anwendbarkeit) ebenso wie in der Einschränkungsliste des Art 52(2) zum Ausdruck.

#Fsl: Scheinbar relativierende %(q:Erläuterungen) wie %(q:Andererseits gibt es eine lange europäische Rechtstradition, wonach der Patentschutz Schöpfungen auf dem Gebiet der Technik vorbehalten ist) können nicht darüber hinwegtäuschen, dass der %(q:Basisvorschlag) in Wirklichkeit auf die restlose Aufhebung ebenjener Rechtstradition zugunsten amerikanischer Verhältnisse hinausläuft.

#DMd: Das EPA definiert in seinem %(q:Basis-Vorschlag) wie auch sonst den %(q:technischen Charakter) zirkulär: %(q:an den Fachmann gerichte Anweisung, eine bestimmte technische Aufgabe mit bestimmten technischen Mittel zu lösen).  Die %(q:Technizität) ist nach EPA-Verständnis das %(q:grundlegende Erfordernis der Patentierbarkeit), und sie leitet sich aus der Forderung nach %(q:erfinderischer Tätigkeit) her.  M.a.W. %(q:Technizität) bedeutet nicht mehr als die mit jedem Patentantrag ohnehin implizierte und kaum widerlegbare %(e:Grundannahme) der %(e:kommerziell/gewerblichen Anwendbarkeit) und der %(e:Existenz einer Gemeinde von interessierten Fachleuten).  Somit kommt der EPA-Forderung nach %(q:Technizität) bereits heute keinerlei einschränkender Charakter mehr zu.  Im Gegenteil, die %(q:Basisvorschlag)-Formulierung %(q:Erfindungen auf allen Gebieten der Technik) bezweckt laut EPA-%(q:Erläuterungen), %(q:augenfällig auszudrücken, dass der Patentschutz grundsätzlich technischen Erfindungen aller Art offensteht).  Der %(s:Universalitätsanspruch des Patentwesens) soll gesetzlich festgeschrieben werden.

#Die: Der neueste Vorstoß der Europäischen Patentorganisation stellt einen vorläufigen Höhepunkt dieser Bestrebungen dar.  Es wird u.a. vorgeschlagen, die gesamte Liste der Patentierungsbeschränkungen in Art 52 EPÜ zu streichen und stattdessen zu fordern, dass die zu patentierenden Gegenstände %(q:technisch) sein müssten, wobei die Definition der %(q:Technizität) bewusst %(q:dynamisch) gehalten, d.h. dem EPA anheimgestellt wird.  Während die BGH-Rechtsprechung bis vor kurzem %(q:Technik) konsequent als %(q:automatisierte Problemlösung unter Einsatz von Naturkräften) einschränkte, dient die der Technikbegriff im EPA-%(q:Basisvorschlag) umgekehrt dazu, alle denkbaren Beschränkungen des Patentwesens von vorneherein aufzuheben.

#Wee: Wir beobachten schon seit einiger Zeit großer Sorge die Bestrebungen der Europäischen Patentjudikative, jenseits jeglicher gesetzgeberischer Kontrolle immer großzügigere Patentrechte zu gewähren, die mittlerweile eine Bedrohung für Innovation, Wettbewerb und Wachstum der %(q:neuen Wirtschaft) sowie für grundlegende Bürgerrechte der Informationsgesellschaft darstellen.

#VPn: Vielen Dank für Ihre Hinweise und für die Übersendung des EPO-Dokuments CA/PL 25/00 %(q:Basisvorschlag für die Revision des Europäischen Patentübereinkommens) vom 27. Juni 2000.

#ShK: Sehr geehrter Herr Karcher!

#ShL: Sehr geehrter Herr Lutz!

#SrC: Sehr geehrter Herr Dr. Hucko!

#SrW: Sehr geehrter Herr Dr. Welp!

#DiW: Das Europäische Patentamt (EPA) ist am 20.-29. November 2000 in München Gastgeber einer %(dk:diplomatischen Konferenz), die weitreichende Änderungen der grundlegenden Gesetzesregeln des europäischen Patentwesens beschließen soll.  Dabei verhandeln die europäischen Regierungen über einen %(bp:Basisvorschlag für die Revision des Europäischen Patentübereinkommens), mit dem das Europäische Patentamt nicht nur die Patentierung von Computerprogrammen und Geschäftsmethoden legalisieren, sondern jedwede gesetzliche Einschränkung der patentierbaren Gegenstände beseitigen will.  Mit diesem Offenen Brief weisen mehrere IT-Verbände und Softwarefirmen die Bundesregierung auf die Irrtümer und Gefahren des %(q:Basisvorschlags) hin und skizzieren Wege zu einer klaren Eingrenzung des Patentwesens.  In einer begleitenden %(pe:Presseerklärung) kommen unterstützende Firmen und Politiker zu Wort. Wir empfehlen Ihnen, die %(pv:Papierversion des Offenen Briefes) auszudrucken.  Sie ist wesentlich aktueller und ausgefeilter und enthält zahlreiche informative Anhänge.  Sie ist inzwischen an 55 führende Politiker gesandt worden.

#descr: Offener Brief zum %(q:Basisvorschlag für die Revision des Europäischen Patentübereinkommens)

#title: Eur. Patentamt begehrt grenzenlose Patentierbarkeit

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxepue28 ;
# txtlang: de ;
# multlin: t ;
# End: ;

