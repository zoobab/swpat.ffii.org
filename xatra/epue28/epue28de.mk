epue28de.lstex:
	lstex epue28de | sort -u > epue28de.lstex
epue28de.mk:	epue28de.lstex
	vcat /ul/prg/RC/texmake > epue28de.mk


epue28de.dvi:	epue28de.mk epue28de.tex
	latex epue28de && while tail -n 20 epue28de.log | grep references && latex epue28de;do eval;done
	if test -r epue28de.idx;then makeindex epue28de && latex epue28de;fi
epue28de.pdf:	epue28de.mk epue28de.tex
	pdflatex epue28de && while tail -n 20 epue28de.log | grep references && pdflatex epue28de;do eval;done
	if test -r epue28de.idx;then makeindex epue28de && pdflatex epue28de;fi
epue28de.tty:	epue28de.dvi
	dvi2tty -q epue28de > epue28de.tty
epue28de.ps:	epue28de.dvi	
	dvips  epue28de
epue28de.001:	epue28de.dvi
	rm -f epue28de.[0-9][0-9][0-9]
	dvips -i -S 1  epue28de
epue28de.pbm:	epue28de.ps
	pstopbm epue28de.ps
epue28de.gif:	epue28de.ps
	pstogif epue28de.ps
epue28de.eps:	epue28de.dvi
	dvips -E -f epue28de > epue28de.eps
epue28de-01.g3n:	epue28de.ps
	ps2fax n epue28de.ps
epue28de-01.g3f:	epue28de.ps
	ps2fax f epue28de.ps
epue28de.ps.gz:	epue28de.ps
	gzip < epue28de.ps > epue28de.ps.gz
epue28de.ps.gz.uue:	epue28de.ps.gz
	uuencode epue28de.ps.gz epue28de.ps.gz > epue28de.ps.gz.uue
epue28de.faxsnd:	epue28de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo epue28de-??.g3n`;source faxsnd main
epue28de_tex~.ps:	
	cat epue28de.tex~ | splitlong | coco | m2ps > epue28de_tex~.ps
epue28de_tex~.ps.gz:	epue28de_tex~.ps
	gzip < epue28de_tex~.ps > epue28de_tex~.ps.gz
epue28de-01.pgm:
	cat epue28de.ps | gs -q -sDEVICE=pgm -sOutputFile=epue28de-%02d.pgm -
epue28de/epue28de.html:	epue28de.dvi
	rm -fR epue28de;latex2html epue28de.tex~
xview:	epue28de.dvi
	xdvi -s 8 epue28de &
tview:	epue28de.tty
	browse epue28de.tty 
gview:	epue28de.ps
	ghostview  epue28de.ps &
aview:	epue28de.pdf
	acroread epue28de.pdf
print:	epue28de.ps
	lpr -s -h epue28de.ps 
sprint:	epue28de.001
	for F in epue28de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	epue28de.faxsnd
	faxsndjob view epue28de &
fsend:	epue28de.faxsnd
	faxsndjob jobs epue28de
viewgif:	epue28de.gif
	xv epue28de.gif &
viewpbm:	epue28de.pbm
	xv epue28de-??.pbm &
vieweps:	epue28de.eps
	ghostview epue28de.eps &	
clean:	epue28de.ps
	rm -f  epue28de-*.tex epue28de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} epue28de-??.* epue28de_tex~.* epue28de*~
tgz:	clean
	set +f;LSFILES=`cat epue28de.ls???`;FILES=`ls epue28de.* $$LSFILES | sort -u`;tar czvf epue28de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
