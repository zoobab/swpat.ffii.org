\contentsline {chapter}{\numberline {A}Verteiler dieses Briefes}{12}{appendix.A}
\contentsline {chapter}{\numberline {B}Stellungnahmen von Politikern zur Softwarepatentierung}{14}{appendix.B}
\contentsline {section}{\numberline {B.1}Ausschuss der Regionen der Europ\"aischen Union}{14}{section.B.1}
\contentsline {section}{\numberline {B.2}Bundestagsfraktion B\"undnis 90/Die Gr\"unen}{14}{section.B.2}
\contentsline {section}{\numberline {B.3}J\"org Tauss, MdB (SPD), Vorsitzender des Parlamentarischen Unterausschusses f\"ur die Neuen Medien}{15}{section.B.3}
\contentsline {section}{\numberline {B.4}Bundestagsfraktion der FDP}{15}{section.B.4}
\contentsline {section}{\numberline {B.5}Bundestagsfraktion der CDU/CSU}{15}{section.B.5}
\contentsline {section}{\numberline {B.6}Dr.\ med.\ Wolfgang Wodarg, MdB (SPD)}{17}{section.B.6}
\contentsline {section}{\numberline {B.7}Herta D\"aubler-Gmelin, Bundesministerin der Justiz}{17}{section.B.7}
\contentsline {section}{\numberline {B.8}Jean-Yves Le D\'eaut, Abgeordneter der Moselregion in der Franz\"osischen Nationalversammlung (PS)}{17}{section.B.8}
\contentsline {chapter}{\numberline {C}Presseerkl\"arung des FFII zum EPA-Basisvorschlag}{19}{appendix.C}
\contentsline {chapter}{\numberline {D}Eurolinux-Petition}{22}{appendix.D}
\contentsline {chapter}{\numberline {E}Art.\ 52 EP\"U und die Grenzen der Patentierbarkeit}{23}{appendix.E}
\contentsline {section}{\numberline {E.1}Novellierungsvorschlag des FFII}{23}{section.E.1}
\contentsline {section}{\numberline {E.2}Bisherige Version des Art.\ 52}{23}{section.E.2}
\contentsline {section}{\numberline {E.3}Novellierungsvorschlag des EPA}{24}{section.E.3}
\contentsline {chapter}{\numberline {F}Logische Ideen: ``Niemandsland des geistigen Eigentums'' oder ``Drittes Paradigma''?}{25}{appendix.F}
\contentsline {chapter}{\numberline {G}TRIPS: kein Fallstrick f\"ur Software}{26}{appendix.G}
\contentsline {chapter}{\numberline {H}Wissenschaftliche Fachliteratur}{27}{appendix.H}
\contentsline {section}{\numberline {H.1}Konzeptionelle Unvertr\"aglichkeit zwischen Patentwesen und Informatik}{27}{section.H.1}
\contentsline {subsection}{\numberline {H.1.1}The Impact of Granting Patents for Information Innovations}{27}{subsection.H.1.1}
\contentsline {subsection}{\numberline {H.1.2}Abstraction orientated property of software and its relation to patentability}{28}{subsection.H.1.2}
\contentsline {subsection}{\numberline {H.1.3}The Models are broken, the models are broken!}{28}{subsection.H.1.3}
\contentsline {subsection}{\numberline {H.1.4}Anarchism Triumphant: Free Software and the Death of Copyright}{29}{subsection.H.1.4}
\contentsline {section}{\numberline {H.2}Pl\"adoyers f\"ur eine Trennung in Patentwesen und Sui-Generis-Systeme}{29}{section.H.2}
\contentsline {subsection}{\numberline {H.2.1}Needed: A New System of Intellectual Property Rights}{29}{subsection.H.2.1}
\contentsline {subsection}{\numberline {H.2.2}Patent Protection for Modern Technologies}{29}{subsection.H.2.2}
\contentsline {subsection}{\numberline {H.2.3}Toward a Third Intellectual Property Paradigm}{29}{subsection.H.2.3}
\contentsline {subsection}{\numberline {H.2.4}Acceptable protection of software intellectual property}{30}{subsection.H.2.4}
\contentsline {section}{\numberline {H.3}Leistungsbeurteilungen des Patentsystems}{30}{section.H.3}
\contentsline {subsection}{\numberline {H.3.1}The Patent Examination System is Intellectually Corrupt}{30}{subsection.H.3.1}
\contentsline {subsection}{\numberline {H.3.2}Comparative Study under Trilateral Project 24.2}{30}{subsection.H.3.2}
\contentsline {section}{\numberline {H.4}\"Okonomische Analysen}{31}{section.H.4}
\contentsline {subsection}{\numberline {H.4.1}The benefits and costs of strong patent protection}{31}{subsection.H.4.1}
\contentsline {subsection}{\numberline {H.4.2}The Patent Paradox Revisited}{31}{subsection.H.4.2}
\contentsline {subsection}{\numberline {H.4.3}Sequential Innovation, Patents, and Imitation}{31}{subsection.H.4.3}
\contentsline {subsection}{\numberline {H.4.4}Software Useright: Solving Inconsistencies of Software Patents}{32}{subsection.H.4.4}
\contentsline {subsection}{\numberline {H.4.5}Software Patents Tangle the Web}{32}{subsection.H.4.5}
\contentsline {subsection}{\numberline {H.4.6}Stimuler la concurrence et l'innovation}{32}{subsection.H.4.6}
\contentsline {section}{\numberline {H.5}Juristische Analysen}{33}{section.H.5}
\contentsline {subsection}{\numberline {H.5.1}Technik, Datenverarbeitung und Patentrecht}{33}{subsection.H.5.1}
\contentsline {subsection}{\numberline {H.5.2}BPatG-Urteil ``Suche fehlerhafter Zeichenketten''}{33}{subsection.H.5.2}
\contentsline {subsection}{\numberline {H.5.3}Computersoftware und Patentrecht}{34}{subsection.H.5.3}
\contentsline {subsection}{\numberline {H.5.4}Ruimere octrooi\"ering van computerprogramma's}{35}{subsection.H.5.4}
\contentsline {subsection}{\numberline {H.5.5}La ``r\'eservation'' du logiciel par le droit des brevets}{36}{subsection.H.5.5}
