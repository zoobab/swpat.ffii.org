<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patent Events Wednesday 2003/08/27 12.00-16.00

#descr: Letter to Members of the European Parliament

#Mea: Dear Member of the European Parliament

#WWW: On Monday 1st September, the European Parliament will decide about the
software patent directive COM(2002)92  2002/0047, euphemistically
titled %(q:on the patentability of computer-implemented inventions).

#fnt: The directive draft as amended by the rapporteur Arlene McCarthy with
support of the Legal Affairs Committe would %(et:make calculation
rules and business methods such as Amazon One Click Shopping
patentable), as in the USA.  30,000 US-style software patents which
the European Patent Office has recently granted against the letter and
spirit of the current law would become enforceable in Europe, and
national judges, who have %(me:criticised the EPO's practise) and
%(sp:refused to follow it), would moreover be obliged to make
practically anything man-made under the sun (or %(q:all practical
problem solutions), as a leading EPO judge %(ms:put) it) patentable.

#sWs: These plans are not popular.  They have met strong criticism from

#Wde: Our %(ec:Call for Action), signed by 350 organisations and companies
and 15000 individuals, which asks for rejection of the directive and
proposes a %(cp:comprehensive set of amendments) which could make the
directive acceptable.

#pee: A recent %(ko:petition of 10 leading economists to the European
Parliament)

#ior: A recent %(ko:petition of 30 famous European computer scientists to
the European Parliament)

#oW0: The %(ep:Eurolinux Petition for a Free Europe without Software
Patents), supported by 160,000 signatories, including 2000 CEOs and
3000 scientists

#iCC: The Opinions of the CULT and ITRE Committees

#oWW: The %(es:Opinion of the Economic and Social Council of the European
Union)

#iem: Opinions from the %(mo:German Monopoly Commission), the Council of
Regions of the European Union, the German Chamber of Commerce, several
confederations of Small and Medium Enterprises, the French State
Planning Commission, the French Government, several economic and legal
studies ordered by the EU and many others.

#oap: The critics believe that property in software is adequately protected
by copyright and that patents stifle innovation and discourage
investments in software development.  Their criticism is based on an
ethical consensus of software developpers as well as on economic
research.

#nrn: The arguments of these critics have however been consistently
disregarded by the proponents of the current directive proposal.

#riw: We have been %(wv:waiting in vain for an answer to the following two
questions):

#oia: What should be patentable and what not?  Which of of the software
patents granted by the EPO, if any, should be excluded from
patentability?  Please explain, citing a %(sp:set of sample patent
claims)!

#ype: How is this achieved by the proposed directive?  Please explain for
each sample patent claim, which provisions of the directive, if any,
would exclude it from patentability!

#eey: We urge you:

#Wts: Please reject the directive as long as the two basic questions have
not been answered!

#eti: Please reject the directive as long as it relies on abstract criteria
such as %(q:technical contribution) without clarifying how these
concepts exclude some sample patent claims!

#itW: Please reject the directive as long as it does not reliably exclude
patents on subject matter such as algorithms and business methods,
whose patentability can not be shown to be in the best interests of
innovation and productivity!

#p2e: To let you understand better what is at stake, we have put together an
outdoor performance and a conference on the subject of software
patents.  We could be very happy if you could come to Place du
Luxembourg on Wednesday at 12:00-14:00 and to Room A1E1 at 14:00-16:00
to exchange views with software developers, scientists and concerned
citizens.

#iel: Yours  Sincerely,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: en ;
# multlin: t ;
# End: ;

