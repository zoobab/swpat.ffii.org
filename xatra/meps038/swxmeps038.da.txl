<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Møde om softwarepatenter onsdag d. 27. august 2003 12.00-16.00

#descr: Letter to Members of the European Parliament

#Mea: Kære medlem af Europaparlamentet

#WWW: På mandag d. 1 september vil Europaparlamentet tage stilling til
softwarepatent direktivet COM(2002)92  2002/0047, med den forskønnede
titel %(q:om patenterbarhed af computer-implementerede opfindelser).

#fnt: Skitsen til direktivet som udformet af Arlene McCarthy med støtte af
Kommiteen for Juridiske Anliggender vil gøre beregningsmetoder og
forretningsmetoder i stil med Amazons 'One Click Shopping'
patentérbare som i USA. 30,000 software patenter efter amerikansk
forbillede, som for nyligt blev tildelt i strid med både ord og mening
af den aktuelle lov ville træde i kraft i Europa, og de enkelte landes
dommere, som har udtalt alvorlig kritik af EPOs praksis og nægter at
følge den, vil automatisk blive forpligtet til at tillade patenter på
næsten hvadsomhelst (eller %(q:enhver form for løsning af et problem),
som en fremtrædende EPO dommer formulerede det).

#sWs: Disse planer er ikke populære.  De er mødt med stærk modstand fra:

#Wde: The FFII Call for Action, underskrevet af 350 organisationer og
firmaer, samt 15000 enkeltpersoner, som opfordrer til at forkaste
direktivet og har stillet et detaljeret modforslag.

#pee: En anmodning fra 20 førende økonomer fremsat til Europaparlamentet.

#ior: En anmodning fra 30 berømte European dataloger fremsat til
Europaparlamentet.

#oW0: Eurolinux Petition for et Frit Europa uden Software patenter,
underskrevet af 160,000 personer, deriblandt 2000 topdirektører og
3000 videnskabsmænd.

#iCC: Holdningerne fra CULT and ITRE Kommiteerne.

#oWW: Holdningen fra det Økonomiske og Sociale Råd i den Europæiske Union.

#iem: Holdninger fra den tyske monopolkommission, Regionsrådene i den
Europæiske Union, det tyske handelskammer, adskillige foreninger af
mindre og mellemstore virksomheder, den franske statslige
planlægningskommission, den franske regering, samt adskillige
økonomiske og juridiske studier bestilt af EU og mange andre.

#oap: Kritikerne mener, at ejerforholdene i software er tilstrækkeligt
beskyttet af copyright, og at patenter vil hæmme snarere end fremme
opfindelse og investeringer i softwareudvikling. Deres kritik er
baseret på et fælles etisk grundlag blandt softwareudviklere, samt på
veldokumenteret forskning i softwareudviklingens dynamik.

#nrn: Kritikernes argumenter er blevet systematisk ignoreret af direktivets
tilhængere. Som anmodningen fra de 20 økonomer og andre har påpeget,
har direktivets tilhængere været systematisk vildledende ved blot at
give dogmatiske udtalelser om, hvad direktivet vil opnå i form af
juridiske og økonomiske effekter, mens de nægter at sammenholde deres
udtalelser med nogen form for empirisk viden.

#riw: Specielt har vi ventet forgæves på svar på følgende to grundlæggende
spørgsmål:

#oia: Hvad skal være patentérbart og hvad skal ikke?  Hvilke af de af EPO
udstedte  patenter på algoritmer og forretningsmetoder skal være
gyldige og juridisk bindende i Europa?  Forklar venligst, med
henvisning til %(sp:eksempler på patentansøgninger).

#ype: Hvordan opnås dette ved direktivet? Forklar venligst, med eksempler i
patentansøgninger og instruktioner fra direktivet, hvad, om muligt,
der kan give grund til at afslå et patent.

#eey: Vi beder Dem om:

#Wts: At afslå direktivet så længe disse to grundlæggende spørgsmål ikke er
besvaret!

#eti: At afslå direktivet så længe det er baseret på abstrakte kriterier
såsom  %(q:teknisk bidrag) uden at forklare klart, hvad dette begreb
ikke omfatter.

#itW: At afslå direktivet så længe det ikke udtrykkeligt udelukker patenter
på områder som algoritmer og forretningsmetoder, hvis patentérbarhed
ikke kan vises at være til gode for opfindsomhed og produktivitet.

#p2e: For bedre at illustrere, hvad der er på spil, har vi samlet en
udendørs performance og en konference om software patenter. Vi ser Dem
derfor meget gerne på Place du Luxembourg på onsdag kl. 12:00-14:00 og
i Room A1E1 kl. 14:00-16:00 for at udveksle synspunkter med
softwareudviklere, videnskabsfolk og andre engagerede borgere.

#iel: Med venlig hilsen,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: da ;
# multlin: t ;
# End: ;

