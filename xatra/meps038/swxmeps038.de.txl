<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2003/08/25 Brief an Europa-Abgeordnete

#descr: Brief an Europa-Abgeordnete

#Mea: Sehr geehrtes Mitglied des Europäischen Parlaments

#WWW: Am Montag, den 1. September, wird das Europäische Parlament über die
Software Patent-Richtlinie COM(2002)92  2002/0047 mit dem
euphemistischen Titel %(q:über die Patentierbarkeit
computer-implementierter Erfindungen) entscheiden.

#fnt: Der Entwurf der Richtlinie von Arlene McCarthy, unterstützt vom
Ausschuss des Europäischen Parlarments für Rechtsangelegenheiten und
den Binnenmarkt, JURI, würde %(et:Rechenregeln und Geschäftsmethoden
wie zum Beispiel die Ein-Klick-Kaufmethode der Firma Amazon
patentierbar machen) wie in den USA.  30.000 Softwarepatente
amerikanischer Bauart, die das Europäische Patentamt jüngst gegen
Buchstabe und Geist der gültigen Gesetzgebung erteilt hat, würden in
Europa rechtsbeständig werden, und Richter der einzelnen Länder, die
die Praxis des Europäischen Patentamtes z.T. %(me:heftig kritisiert)
haben und ihm %(sp:nicht gefolgt sind), wären gezwungen, praktisch
alles Menschengeschaffene unter der Sonne (oder %(q:alle praktischen
Lösungen), wie es ein führender EPA-Richter %(ms:formuliert) hat, für
patentfähig zu erachten.

#sWs: Diese Pläne sind nicht sehr beliebt. Sie sehen sich starkem Widerstand
ausgesetzt:

#Wde: durch den %(ec:Aufruf zum Handeln), unterzeichnet von 350
Organisationen und Unternehmen sowie 15000 Einzelpersonen, welcher die
Zurückweisung der Richtlinie fordert und zugleich einen
%(cp:umfangreichen Satz von Änderungsvorschlägen) unterbreitet, der
die Richtlinie annehmbar machen könnte.

#pee: durch eine kürzlich von 10 führenden Wirtschaftswissenschaftlern an
das Europäische Parlament gerichtete %(ko:Petition)

#ior: durch eine kürzlich von 30 berühmten europäischen Informatikern an das
EP gerichtete %(ko:Petition)

#oW0: durch die %(ep:Eurolinux-Petition für ein Freies Europa ohne
Softwarepatente), die von 160.000 Unterzeichnern unterstützt wird,
darunter 2000 Geschäftsführer und 3000 Wissenschaftler

#iCC: durch die Stellungnahmen der Ausschüsse CULT und ITRE

#oWW: durch die %(es:Stellungnahme des Europäischen Wirtschafts- und
Sozialausschusses)

#iem: durch die %(mo:Stellungnahmen der deutschen Monopolkommission), des
Ausschusses der Regionen der EU, der deutschen Industrie- und
Handelskammer, verschiedener Vereinigungen kleiner und mittlerer
Unternehmen, dem Staatlichen Planungskommissariat Frankreichs, der
französischen Regierung, sowie die Ergebnisse mehrerer von der EU
beauftragter Gutachten und viele andere.

#oap: Die Kritiker glauben, dass das Eigentum von Software hinreichend durch
das Urheberrecht geschützt ist, und dass Patente Investitionen in
Softwareentwicklung behindern, statt sie zu fördern. Ihre Kritik
begründet sich auf einen ethischen Konsens praktisch aller angesehener
Softwareentwickler, ebenso wie die gut dokumentierte Forschung der
Dynamik von Software-Innovation.

#nrn: Die Argumente dieser Kritiker sind konsequent von den Befürwortern der
aktuellen Richtlinie ignoriert worden.

#riw: Wir haben insbesondere %(wv:vergeblich auf die Beantwortung der beiden
folgenden Fragen gewartet):

#oia: Was soll patentierbar sein?  Welche der vom EPA erteilten
Softwarepatente sollen von der Patentierbarkeit ausgeschlossen werden?
 Bitte erklären Sie anhand eines %(sp:Satzes von
Beispiel-Patentansprüchen)!

#ype: Wie wird dies durch die Richtlinie bewerkstelligt? Bitte erklären Sie,
ob die Bestimmungen der Richtlinie diese beispielhaften
Patentforderungen von der Patentierbarkeit ausschließen, und welche
Bestimmung genau dies tut.

#eey: Wir bitten Sie:

#Wts: Bitte weisen Sie diese Richtlinie zurück, solange diese beiden
grundsätzlichen Fragen nicht beantwortet sind!

#eti: Bitte weisen Sie diese Richtlinie zurück, solange sie auf abstrakten
Kriterien wie %(q:technischer Beitrag) beruht, ohne nachvollziehbar zu
erklären, was dadurch von der Patentierbarkeit ausgeschlossen wird.

#itW: Bitte weisen Sie diese Richtlinie zurück, solange sie nicht
zuverlässig Patente auf Algorithmen und Geschäftsmethoden ausschließt,
denn es konnte nicht gezeigt werden, dass deren Patentierbarkeit im
Interesse von Innovation und Produktivität liegt!

#p2e: Um zu verdeutlichen, was hier auf dem Spiel steht, haben wir eine
Outdoor Performance und eine Konferenz zum Thema Softwarepatente
organisiert. Wir würden uns sehr freuen, wenn Sie kommen könnten: zur
Place du Luxembourg am Mittwoch von 12 bis 14 Uhr und zum Raum A1E1
von 14-16 Uhr, um Ansichten mit Softwareentwicklern, Wissenschaftlern
und engagierten Bürgern auszutauschen.

#iel: Mit freundlichen Grüßen,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: de ;
# multlin: t ;
# End: ;

