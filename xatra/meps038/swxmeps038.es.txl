<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Evento de Patentes de Software Miércoles 27/08/2003 12.00-16.00

#descr: Letter to Members of the European Parliament

#Mea: Estimado Miembro del Parlamento Europeo

#WWW: El lunes 1 de Septiembre, el Parlamento Europeo tomará su decisión
sobre la directiva de patente de software COM(2002)92 2002/0047,
eufemísticamente titulada %(q:on the patentability of
computer-implemented inventions).

#fnt: El borrador de directiva tal y como ha quedado enmendado por el
relator Arlene McCarthy con la ayuda del Comité de Asuntos Legales
haría patentables reglas de cálculo y métodos de negocio como el
Amazon One Click Shopping, al igual que ocurre en EEUU. 30000 patentes
de software al estilo americano que el European Patent Office ha
otorgado en contra del espíritu de la ley vigente serían aplicables en
Europa, y los jueces nacionales, que han severamente criticado la
práctica del EPO y han rechazado seguirla, serían además obligados a
convertir cualquier cosa que produce el hombre bajo el sol (o %(q:all
practical problem solutions), como lo llama un juez reconocido del
EPO) en algo patentable.

#sWs: Esos planes no son muy populares. Encuentran resistencia fuerte de
parte de:

#Wde: El FFII Call for Action, firmado por 350 organizaciones y compañías y
15000 personas, que pide el rechazo de la directiva y bosqueja una
contrapropuesta detallada

#pee: Una petición reciente de 20 economistas reconocidos del Parlamento
Europeo

#ior: Una petición reciente al Parlamento Europeo de 30 científicos
informáticos europeos famosos

#oW0: The %(ep:Eurolinux Petition for a Free Europe without Software
Patents), supported by 160,000 signatories, including 2000 CEOs and
3000 scientists

#iCC: Los opiniones de los Comités CULT y ITRE

#oWW: El opinión del Consejo Social y Económico de la Unión Europea

#iem: Opiniones de la Comisión de Monopolio Alemana, el Consejo de las
Regiones de la Unión Europea, la Cámara de Comercio Alemana, muchas
confederaciones de Pequeñas y Medianas Empresas, la Comisión de
Planificación del Estado Francés, el Gobierno Francés, numerosos
estudios encargados por la UE y muchos más.

#oap: Las voces críticas piensan que la propiedad en el software es
adecuadamente protegida por el copyright y que las patentes
desalientan más que promueven las inversiones en el desarrollo de
software. Esa crítica se basa en un consenso ético de casi todos los
desarrolladores de software, así como en una investigación seria sobre
las dinámicas de innovación de software.

#nrn: Los argumentos de estas críticas han sido sin embargo sistemáticamente
despreciadas por los promotores de la actual directiva. Como la
petición de 20 economistas y demás personas ha puesto de relieve, los
promotores de la directiva les engaña constantemente al hacer
declaraciones dogmáticas sobre lo que dicha directiva consigue en
términos de efectos legales y económicos, mientras rechazan confrontar
esas declaraciones con cualquier prueba empírica.

#riw: En particular, hemos esperado en vano una respuesta a las siguientes
preguntas básicas:

#oia: Deberían las patentes sobre los algoritmos y los métodos de negocio,
tal y como se encuentran en una muestra de %(sp:sample patent claims)
otorgados por el EPO, ser válidas y aplicables en Europa ?

#ype: Como se consigue esto a través de la directiva ? Por favor detalle,
citando las reclamaciones de patentes de la muestra y las provisiones
de la directiva, por si a caso, que las apartarían de la
patentibilidad.

#eey: Le rogamos:

#Wts: Por favor rechacen la directiva mientras estas dos preguntas básicas
no han sido contestadas!

#eti: Por favor rechacen la directiva mientras se apoya en criterios
abstractos como %(q:technical contribution) sin explicar sin
ambigüedad lo que excluyen estos conceptos.

#itW: Please reject the directive as long as it does not reliably exclude
patents on subject matter such as algorithms and business methods,
whose patentability can not be shown to be in the best interests of
innovation and productivity!

#p2e: Para hacerle comprender mejor lo que esta en juego, hemos montado una
performance al aire libre y una charla sobre las patentes de software.
Agradeceríamos mucho su presencia en la Place du Luxembourg, el
miércoles de 12:00 a 14:00, y en la Sala A1E1 de 14:00 a 16:00 para
intercambiar opiniones con desarrolladores de software, científicos y
ciudadanos preocupados.

#iel: Saludos,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: es ;
# multlin: t ;
# End: ;

