\begin{subdocument}{swxmeps038}{Software Patent Events Wednesday 2003/08/27 12.00-16.00}{http://swpat.ffii.org/xatra/meps038/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Letter to Members of the European Parliament}
\begin{sect}{xatra}{Dear Member of the European Parliament}
On Monday 1st September, the European Parliament will decide about the software patent directive COM(2002)92  2002/0047, euphemistically titled ``on the patentability of computer-implemented inventions''.

The directive draft as amended by the rapporteur Arlene McCarthy with support of the Legal Affairs Committe would make calculation rules and business methods such as Amazon One Click Shopping patentable\footnote{\url{}}, as in the USA.  30,000 US-style software patents which the European Patent Office has recently granted against the letter and spirit of the current law would become enforceable in Europe, and national judges, who have criticised the EPO's practise\footnote{e.g. Melullis 2002: Zur Sonderrechtsfähigkeit von Computerprogrammen\footnote{\url{http://localhost/swpat/papri/melullis02/index.de.html}}} and refused to follow it\footnote{e.g. BPatG Error Search 2002/03/26: system for improved computing efficiency = program as such\footnote{\url{http://localhost/swpat/papri/bpatg17-suche02/index.de.html}}}, would moreover be obliged to make practically anything man-made under the sun (or ``all practical problem solutions'', as a leading EPO judge put\footnote{\url{http://localhost/swpat/papri/jwip-schar98/index.en.html}} it) patentable.

These plans are not popular.  They have met strong criticism from
\begin{itemize}
\item
Our Call for Action\footnote{\url{}}, signed by 350 organisations and companies and 15000 individuals, which asks for rejection of the directive and proposes a comprehensive set of amendments\footnote{\url{}} which could make the directive acceptable.

\item
A recent petition of 10 leading economists to the European Parliament\footnote{\url{}}

\item
A recent petition of 30 famous European computer scientists to the European Parliament\footnote{\url{}}

\item
The Eurolinux Petition for a Free Europe without Software Patents\footnote{\url{http://petition.eurolinux.org/}}, supported by 160,000 signatories, including 2000 CEOs and 3000 scientists

\item
The Opinions of the CULT and ITRE Committees

\item
The Opinion of the Economic and Social Council of the European Union\footnote{\url{}}

\item
Opinions from the German Monopoly Commission\footnote{\url{}}, the Council of Regions of the European Union, the German Chamber of Commerce, several confederations of Small and Medium Enterprises, the French State Planning Commission, the French Government, several economic and legal studies ordered by the EU and many others.
\end{itemize}

The critics believe that property in software is adequately protected by copyright and that patents stifle innovation and discourage investments in software development.  Their criticism is based on an ethical consensus of software developpers as well as on economic research.

The arguments of these critics have however been consistently disregarded by the proponents of the current directive proposal.

We have been waiting in vain for an answer to the following two questions\footnote{see also PHM to AMccarthy 03/06/10: Questions based on 2 Example Patent Claims\footnote{\url{http://aful.org/wws/arc/patents/2003-06/msg00047.html}}}:
\begin{enumerate}
\item
What should be patentable and what not?  Which of of the software patents granted by the EPO, if any, should be excluded from patentability?  Please explain, citing a set of sample patent claims\footnote{\url{http://localhost/swpat/stidi/manri/index.en.html}}!

\item
How is this achieved by the proposed directive?  Please explain for each sample patent claim, which provisions of the directive, if any, would exclude it from patentability!
\end{enumerate}

We urge you:
\begin{enumerate}
\item
Please reject the directive as long as the two basic questions have not been answered!

\item
Please reject the directive as long as it relies on abstract criteria such as ``technical contribution'' without clarifying how these concepts exclude some sample patent claims!

\item
Please reject the directive as long as it does not reliably exclude patents on subject matter such as algorithms and business methods, whose patentability can not be shown to be in the best interests of innovation and productivity!
\end{enumerate}

To let you understand better what is at stake, we have put together an outdoor performance and a conference on the subject of software patents.  We could be very happy if you could come to Place du Luxembourg on Wednesday at 12:00-14:00 and to Room A1E1 at 14:00-16:00 to exchange views with software developers, scientists and concerned citizens.

Yours  Sincerely,
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf 2003/08/25-9 BXL: Software Patent Directive Amendments\footnote{\url{http://localhost/swpat/penmi/2003/europarl/08/index.en.html}}}}

\begin{quote}
Members of the European Parliament are coming back to work on monday August 25th.  It is the last week before the vote on the Software Patent Directive Proposal.  We are organising a conference and street rally wednesday the 27th.  Some of our friends will moreover be staying in the parliament for several days.  Time to work decide on submission of amendments to the software patent directive proposal is running out.  FFII has proposed one set of amendments that stick as closely as possible to the original proposal while debugging and somewhat simplifying it.  An alternative small set of amendments would ``cut the crap'' and rewrite the directive from scratch.  We present and explain the possible approaches.
\end{quote}
\filbreak

\item
{\bf {\bf Aug 27 Demonstrations against EU Software Patent Plans\footnote{\url{http://localhost/swpat/lisri/03/demo0819/index.en.html}}}}

\begin{quote}
The Proposal for a software patent directive, which will be submitted to the European Parliament for plenary debate and subsequent decision on September 1st, is giving rise to another wave of protests.  Various groups in Belgium and elsewhere are mobilising for a rally in Brussels on August 27th and are calling on web administrators to temporarily block their web sites.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

