<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevet logiciel: événement du mercredi 27-8-2003 12.00-16.00

#descr: Lettre aux Membres du Parlament Européen

#Mea: Cher Membre du Parlement européen

#WWW: Lundi prochain, premier septembre, le Parlement européen décidera de
la directive COM(2002)92 2002/0047 concernant le brevet logiciel,
désignée sous le titre quelque peu euphémique %(q:A propos de la
brevetabilité des inventions mises en oeuvre par ordinateur).

#fnt: Le projet de directive, tel qu'amendé par le rapporteur Arlene
McCarthy avec le soutien du Comité des Affaires Juridiques (JURI),
%(et:ferait en sorte que des méthodes de calcul et des modèles
commerciaux tels que %(q:Amazon One Click Shopping) soient
brevetables), comme c'est le cas aux Etats-Unis. Trente mille brevets
logiciels dans le style américain, que l'Office Européen des Brevets
(OEB) a accordé en dépit de la lettre et de l'esprit des lois en
vigueur, deviendraient exécutoires en Europe, et les juges nationaux,
qui ont %(me:sévèrement critiqué la pratique de l'OEB) et ont
%(sp:refusé de la suivre), seraient obligés de rendre brevetable
pratiquement toute création humaine (ou %(q:toute solution pratique de
n'importe quel problème, comme l'a %(ms:exprimé) un juge éminent de
l'OEB)).

#sWs: Ce projet n'est pas très populaire. Une forte résistance s'est
exprimée:

#Wde: L'appel de la FFII, signé par 350 organisations et sociétés et 1500
individus, demande le rejet de la directive et esquisse une
%(cp:contre-proposition détaillée)

#pee: Une pétition a été récemment déposée au Parlement européen par 20
éminents économistes.

#ior: Une récente pétition signée par 30 célèbres chercheurs européens a été
déposée au Parlement européen.

#oW0: La pétition d'Eurolinux pour une %(ep:Europe libre sans brevets
logiciels) est supportée par 160000 signataires, y compris 2000 PDG et
3000 chercheurs.

#iCC: Les avis des comités CULT et ITRE.

#oWW: L'avis du Conseil Economique et Social de l'Union Européenne.

#iem: Les avis de la Commission allemande sur le Monopole, le Conseil de
l'Europe, la Chambre de Commerce allemande, plusieurs confédérations
de petites et moyennes entreprises, le Commissariat au Plan français,
le gouvernement français, plusieurs études commandées par l'UE et de
nombreuses autres.

#oap: Les opposants croient qu'en ce qui concerne les logiciels, la
propriété est adéquatement protégée par le droit d'auteur et que les
brevets découragent, au lieu d'encourager, l'investissement dans le
développement de logiciels.  Cette critique se fonde sur un consensus
éthique des développeurs de logiciel, de même que sur des études
économiques.

#nrn: Les arguments avancés ont cependant été constamment écartés par les
partisans de la directive telle que proposée.

#riw: En particulier, %(wv:nous avons attendu, en vain, une réponse aux deux
questions suivantes):

#oia: Qu'est-ce qui doit être brevetable et qu'est-ce qui ne peut pas
l'être? Parmi les brevets logiciels accordés par l'OEB, lesquels
doivent-ils être exclus du champ de la brevetabilité?  Veuillez
expliquer, en citant %(sp:des exemples concrets de demandes de
brevet)!

#ype: Comment la directive atteint-elle ce but? Veuillez expliquer, pour
chaque exemple de demande de brevet, quelles sont les dispositions de
la directive qui l'excluraient de la brevetabilité!

#eey: Nous vous demandons instamment de bien vouloir:

#Wts: Rejeter la directive aussi longtemps que ces deux questions
élémentaires n'auront pas reçu de réponse!

#eti: Rejeter la directive tant qu'elle repose sur des critères abstraits
tels que %(q:contribution technique) sans clarifier comment ces
concepts peuvent exclure de la brevetabilité les exemples demandés
ci-dessus!

#itW: Rejeter la directive tant qu'elle n'exclut pas les brevets sur des
concepts comme les algorithmes et les méthodes de commerce, dont il ne
peut être montré que la brevetabilité est dans l'intérêt de
l'innovation et de la productivité!

#p2e: Afin que vous compreniez plus clairement ce qui est en jeu, nous avons
organisé une démonstration et une conférence sur le sujet des brevets
logiciels. Nous serions extrêmement heureux si vous pouviez y
assister, mercredi de 12 à 14 heures Place du Luxembourg et de 14 à 16
heures dans la salle A1E1, pour échanger des idées avec des
concepteurs de logiciels, des chercheurs et des citoyens inquiets.

#iel: Cordialement,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

