<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patent Events - Mercoledì 27/08/2003 12.00-16.00

#descr: Letter to Members of the European Parliament

#Mea: Gentile Membro del Parlamento Europeo,

#WWW: Lunedì 1° Settembre, il Parlamento Europeo prenderà una decisione
riguardo la direttiva sui brevetti software, siglata COM(2002)92 
2002/0047 ed intitolata eufemisticamente %(q:sulla  brevettabilità di
invenzioni implementate al calcolatore).

#fnt: La bozza della direttiva, così come presentata dalla relatrice Arlene
McCarthy con il supporto del Comitato per gli Affari Legali, mira a
rendere metodi di commercio ed algoritmi come lo "One-Click Shopping"
di Amazon patentabili, come d'altronde già avviene negli Stati Uniti.
30'000 brevetti sulla falsariga di quelli statunitensi, recentemente
garantiti contro i contenuti e lo spirito della legge attuale,
verrebbero immediatamente rafforzati in tutta Europa; i giudici
nazionali, i quali hanno aspramente criticato il comportamento
dell'EPO (Ufficio Europeo Patenti) e rifiutato di seguirlo, sarebbero
comunque costretti a rendere brevettabile pressoché qualsiasi cosa che
l'uomo abbia mai realizzato (o meglio: %(q:tutte le soluzioni a
problemi pratici), come le ha definite un eminente giudice dell'EPO).

#sWs: Questi progetti non sono molto popolari. Essi sono stati infatti
accolti con forti riserve da:

#Wde: La FFII Call for Action, supportata da 350 tra organizzazioni e
compagnie, più 15'000 singoli individui; essa preme per un rifiuto
della direttiva e ne delinea inoltre una dettagliata controproposta

#pee: Una recente petizione di 20 tra i più importanti economisti diretta al
Parlamento Europeo

#ior: Una recente petizione di 30 famosi ricercatori informatici diretta al
Parlamento Europeo

#oW0: La petizione di Eurolinux per un'Europa Libera senza i Brevetti sul
Software, sottoscritta da 160'000 firmatari, inclusi 2'000
Amministratori Delegati e 3'000 scienziati

#iCC: L'opinione dei Comitati CULT e ITRE

#oWW: L'opinione dell'Economic and Social Council dell'Unione Europea

#iem: Opinioni dalla Commissione Tedesca sui Monopoli, il Concilio delle
Regioni dell'Unione Europea, la Camera di Commercio Tedesca, svariate
associazioni di Piccole e Medie Imprese, la Commissione Statale
Francese per la Pianificazione, lo stesso Governo Francese, diversi
studi commissionati dall'UE e molti altri.

#oap: Questi oppositori ritengono che la proprietà del software sia protetta
adeguatamente dalle normative vigenti in merito di copyright, e che i
brevetti scoraggino gli investimenti nel campo dello sviluppo del
software, piuttosto che incentivarli. Il loro criticismo si basa su un
consenso di carattere etico da parte di quasi tutti i più importanti
sviluppatori, così come su ricerche ben documentate che trattano le
dinamiche dell'innovazione software.

#nrn: Gli argomenti di questi oppositori sono stati comunque sottovalutati
in modo consistente da chi ha proposto la direttiva sopracitata. Come
la petizione dei 20 economisti, nonché le altre, hanno evidenziato,
gli avvallatori della direttiva in questione stanno sistematicamente
cercando di fuorviarLa tramite asserzioni dogmatiche in merito a quali
traguardi la loro direttiva sia in grado di ottenere, in termini di
effetti legali e sull'economia, mentre si rifiutano di misurare queste
affermazioni a fronte dell'evidenza empirica.

#riw: In particolare, abbiamo atteso in vano per una risposta alle seguenti
due basilari domande:

#oia: E' giusto che i brevetti su algoritmi e metodi di commercio, come si
trovano in una serie di %(sp:esempi di rivendicazione sui brevetti)
garantiti dall'EPO, siano validi e rafforzabili in tutta Europa?

#ype: Come è perseguito questo risultato dalla direttiva? Esplicitatecelo,
per favore, citando gli esempi di rivendicazioni di brevetti e le
disposizioni della direttiva che le escluderebbero dall'obbligo di
brevetto, se ce ne sono.

#eey: Le chiediamo cortesemente:

#Wts: Per favore, rifiuti la direttiva fintantoché queste due semplici
domande non troveranno risposta!

#eti: Per favore, rifiuti la direttiva finché farà affidamento su criteri
astratti come %(q:contributo tecnico) senza esplicitare in modo non
ambiguo che cosa escludano questi concetti.

#itW: Please reject the directive as long as it does not reliably exclude
patents on subject matter such as algorithms and business methods,
whose patentability can not be shown to be in the best interests of
innovation and productivity!

#p2e: Per renderLe più semplice la comprensione di quale sia la posta in
gioco, abbiamo organizzato una manifestazione pubblica ed una
conferenza sull'argomento dei brevetti software. Saremmo onorati di
averLa con noi in Place du Luxembourg nella giornata di Mercoledì,
dalle ore 12:00 alle ore 14:00, e nella Room A1E1 dalle 14:00 alle
16:00 per scambiarci i nostri punti di vista con sviluppatori,
scienziati e cittadini interessati.

#iel: Distinti saluti,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: it ;
# multlin: t ;
# End: ;

