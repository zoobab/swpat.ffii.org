\begin{subdocument}{swxmeps038}{Brevet logiciel: événement du mercredi 27-8-2003 12.00-16.00}{http://swpat.ffii.org/xatra/meps038/index.fr.html}{Groupe de travail\\\url{swpatag@ffii.org}\\version fran\c{c}aise 2005/01/06 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Lettre aux Membres du Parlament Europ\'{e}en}
\begin{sect}{xatra}{Cher Membre du Parlement europ\'{e}en}
Lundi prochain, premier septembre, le Parlement europ\'{e}en d\'{e}cidera de la directive COM(2002)92 2002/0047 concernant le brevet logiciel, d\'{e}sign\'{e}e sous le titre quelque peu euph\'{e}mique ``A propos de la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur''.

Le projet de directive, tel qu'amend\'{e} par le rapporteur Arlene McCarthy avec le soutien du Comit\'{e} des Affaires Juridiques (JURI), ferait en sorte que des m\'{e}thodes de calcul et des mod\`{e}les commerciaux tels que ``Amazon One Click Shopping'' soient brevetables\footnote{\url{}}, comme c'est le cas aux Etats-Unis. Trente mille brevets logiciels dans le style am\'{e}ricain, que l'Office Europ\'{e}en des Brevets (OEB) a accord\'{e} en d\'{e}pit de la lettre et de l'esprit des lois en vigueur, deviendraient ex\'{e}cutoires en Europe, et les juges nationaux, qui ont s\'{e}v\`{e}rement critiqu\'{e} la pratique de l'OEB\footnote{ex. Melullis 2002: Zur Sonderrechtsfähigkeit von Computerprogrammen\footnote{\url{http://localhost/swpat/papri/melullis02/index.de.html}}} et ont refus\'{e} de la suivre\footnote{ex. BPatG 2002-03-26: Suche fehlerhafter Zeichenketten\footnote{\url{http://localhost/swpat/papri/bpatg17-suche02/index.de.html}}}, seraient oblig\'{e}s de rendre brevetable pratiquement toute cr\'{e}ation humaine (ou ``toute solution pratique de n'importe quel probl\`{e}me, comme l'a exprim\'{e}\footnote{\url{http://localhost/swpat/papri/jwip-schar98/index.en.html}} un juge \'{e}minent de l'OEB'').

Ce projet n'est pas tr\`{e}s populaire. Une forte r\'{e}sistance s'est exprim\'{e}e:
\begin{itemize}
\item
L'appel de la FFII, sign\'{e} par 350 organisations et soci\'{e}t\'{e}s et 1500 individus, demande le rejet de la directive et esquisse une contre-proposition d\'{e}taill\'{e}e\footnote{\url{}}

\item
Une p\'{e}tition a \'{e}t\'{e} r\'{e}cemment d\'{e}pos\'{e}e au Parlement europ\'{e}en par 20 \'{e}minents \'{e}conomistes.

\item
Une r\'{e}cente p\'{e}tition sign\'{e}e par 30 c\'{e}l\`{e}bres chercheurs europ\'{e}ens a \'{e}t\'{e} d\'{e}pos\'{e}e au Parlement europ\'{e}en.

\item
La p\'{e}tition d'Eurolinux pour une Europe libre sans brevets logiciels\footnote{\url{http://www.noepatents.org/}} est support\'{e}e par 160000 signataires, y compris 2000 PDG et 3000 chercheurs.

\item
Les avis des comit\'{e}s CULT et ITRE.

\item
L'avis du Conseil Economique et Social de l'Union Europ\'{e}enne.

\item
Les avis de la Commission allemande sur le Monopole, le Conseil de l'Europe, la Chambre de Commerce allemande, plusieurs conf\'{e}d\'{e}rations de petites et moyennes entreprises, le Commissariat au Plan fran\c{c}ais, le gouvernement fran\c{c}ais, plusieurs \'{e}tudes command\'{e}es par l'UE et de nombreuses autres.
\end{itemize}

Les opposants croient qu'en ce qui concerne les logiciels, la propri\'{e}t\'{e} est ad\'{e}quatement prot\'{e}g\'{e}e par le droit d'auteur et que les brevets d\'{e}couragent, au lieu d'encourager, l'investissement dans le d\'{e}veloppement de logiciels.  Cette critique se fonde sur un consensus \'{e}thique des d\'{e}veloppeurs de logiciel, de m\^{e}me que sur des \'{e}tudes \'{e}conomiques.

Les arguments avanc\'{e}s ont cependant \'{e}t\'{e} constamment \'{e}cart\'{e}s par les partisans de la directive telle que propos\'{e}e.

En particulier, nous avons attendu, en vain, une r\'{e}ponse aux deux questions suivantes\footnote{voir aussi PHM to AMccarthy 03/06/10: Questions based on 2 Example Patent Claims\footnote{\url{http://aful.org/wws/arc/patents/2003-06/msg00047.html}}}:
\begin{enumerate}
\item
Qu'est-ce qui doit \^{e}tre brevetable et qu'est-ce qui ne peut pas l'\^{e}tre? Parmi les brevets logiciels accord\'{e}s par l'OEB, lesquels doivent-ils \^{e}tre exclus du champ de la brevetabilit\'{e}?  Veuillez expliquer, en citant des exemples concrets de demandes de brevet\footnote{\url{http://localhost/swpat/stidi/manri/index.fr.html}}!

\item
Comment la directive atteint-elle ce but? Veuillez expliquer, pour chaque exemple de demande de brevet, quelles sont les dispositions de la directive qui l'excluraient de la brevetabilit\'{e}!
\end{enumerate}

Nous vous demandons instamment de bien vouloir:
\begin{enumerate}
\item
Rejeter la directive aussi longtemps que ces deux questions \'{e}l\'{e}mentaires n'auront pas re\c{c}u de r\'{e}ponse!

\item
Rejeter la directive tant qu'elle repose sur des crit\`{e}res abstraits tels que ``contribution technique'' sans clarifier comment ces concepts peuvent exclure de la brevetabilit\'{e} les exemples demand\'{e}s ci-dessus!

\item
Rejeter la directive tant qu'elle n'exclut pas les brevets sur des concepts comme les algorithmes et les m\'{e}thodes de commerce, dont il ne peut \^{e}tre montr\'{e} que la brevetabilit\'{e} est dans l'int\'{e}r\^{e}t de l'innovation et de la productivit\'{e}!
\end{enumerate}

Afin que vous compreniez plus clairement ce qui est en jeu, nous avons organis\'{e} une d\'{e}monstration et une conf\'{e}rence sur le sujet des brevets logiciels. Nous serions extr\^{e}mement heureux si vous pouviez y assister, mercredi de 12 \`{a} 14 heures Place du Luxembourg et de 14 \`{a} 16 heures dans la salle A1E1, pour \'{e}changer des id\'{e}es avec des concepteurs de logiciels, des chercheurs et des citoyens inquiets.

Cordialement,
\end{sect}

\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf 2003/08/25-9 BXL: Software Patent Directive Amendments\footnote{\url{http://localhost/swpat/penmi/2003/europarl/08/index.en.html}}}}

\begin{quote}
Members of the European Parliament are coming back to work on monday August 25th.  It is the last week before the vote on the Software Patent Directive Proposal.  We are organising a conference and street rally wednesday the 27th.  Some of our friends will moreover be staying in the parliament for several days.  Time to work decide on submission of amendments to the software patent directive proposal is running out.  FFII has proposed one set of amendments that stick as closely as possible to the original proposal while debugging and somewhat simplifying it.  An alternative small set of amendments would ``cut the crap'' and rewrite the directive from scratch.  We present and explain the possible approaches.
\end{quote}
\filbreak

\item
{\bf {\bf Aug 27 Demonstrations against EU Software Patent Plans\footnote{\url{http://localhost/swpat/lisri/03/demo0819/index.fr.html}}}}

\begin{quote}
La proposition de directive de brevets logiciels, qui sera soumis au Parlament Europ\'{e}en pour d\'{e}cision le 1\'{e}r septembre, suscite une autre onde de manifestations.  Une coalition de groupes en Belgique et autrui mobilise pour une manifestation a Bruxelles le 27 ao\^{u}t et fait appel aux administrateurs web de participer dans des ``manifestations virtuelles''.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

