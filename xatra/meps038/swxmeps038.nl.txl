<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatenten Evenement Woensdag 27/08/2003 12.00-16.00

#descr: Brief aan de leden van het Europees Parlement

#Mea: Geacht Europarlementslid

#WWW: Op maandag 1 september zal het Europees Parlement beslissen over de
richtlijn met betrekking tot patenten op software COM(2002)92 
2002/004, die de eufemistische titel draagt %(q: over de
patenteerbaarheid van computer-geïplementeerde uitvindingen).

#fnt: In de vorm waarin de richtlijn door rapporteur Arlene McCarthy met de
steun van de Commissie Juridische Zaken geamendeerd werd, zal ze
leiden tot een

#sWs: Deze plannen zijn niet erg populair, en stuiten op sterke kritiek van:

#Wde: Onze %(ec:Oproep tot Actie), gesteund door 350 institutionele en
15,000 individuele ondertekenaars, die vraagt om de verwerping van de
richtlijn en waarin een %(cp: gedetailleerd tegenvoorstel) uiteengezet
wordt.

#pee: Een recente %(ko: petitie van 10 vooraanstaande economen, gericht aan
het Europees Parlement).

#ior: Een recente %(ko: petitie van 10 beroemde Europese wetenschappers in
het domein van de informatica, gericht aan het Europees Parlement).

#oW0: De %(ep:Eurolinux Petitie voor een Vrij Europa Zonder Patenten op
Software), gesteund door 160,000 ondertekenaars, waaronder 2000
bedrijfsleiders en 3000 wetenschappers.

#iCC: Het advies van de commissies CULT en ITRE.

#oWW: Het (%es:Advies van het Europees Economisch en Sociaal Comité.

#iem: De meningen van het %(mo:Duitse Anti-Monopoliecomité), het Europese
Comité van de Regio's, de Duitse Kamer van Koophandel, verschillende
organisaties van KMO's, het Franse Algemene Planbureau, de Franse
Regering, verschillende door de EU gecommissioneerde economische en
juridische onderzoeken, en vele anderen.

#oap: De critici zijn van mening dat het eigendomsrecht met betrekking op
software al op gepaste en voldoende wijze wordt beschermd door het
auteursrecht, en dat patenten ontmoedigend werken op investeringen en
innovatie in software-ontwikkeling. Hun kritiek is gebaseerd op een
ethische consensus onder software-ontwikkelaars en op zorgvuldig
gedocumenteerd economisch onderzoek.

#nrn: De argumenten van de critici worden echter op systematische wijze
genegeerd door de voorstanders van de huidige richtlijn.

#riw: Wij %(wv: wachten vruchteloos op een antwoord op twee fundamentele
vragen):

#oia: Wat moet patenteerbaar zijn en wat niet? Welke van de patenten op
software die door het EPB werden toegekend, zouden moeten uitgesloten
worden? Gelieve dit te illustreren aan de hand van een %(sp: selectie
patentaanvragen)!

#ype: Hoe wordt dit door de voorgestelde richtlijn bereikt? Gelieve voor
elke patentaanvraag in de selectie te illustreren op basis van welke
bepalingen in de richtlijn patenteerbaarheid uitgesloten zou zijn.

#eey: Wij verzoeken u:

#Wts: De richtlijn te verwerpen zolang er geen antwoord is op de twee
fundamentele vragen!

#eti: De richtlijn te verwerpen zolang ze steunt op abstracte criteria als
%(q:technische bijdrage) zonder te verduidelijken hoe deze concepten
sommige patentaanvragen uitsluiten.

#itW: De richtlijn te verwerpen zolang ze geen patenten uitsluit die
betrekking hebben op zaken als rekenregels, algoritmen en
bedrijfsmethoden, waarvan niet kan worden aangetoond dat hun
patenteerbaarheid innovatie en productiviteit ten goede komt.

#p2e: Om u beter te kunnen uitleggen wat er op het spel staat, organiseren
wij een openluchtdemonstratie en een conferentie over patenten op
software. We zouden het erg op prijs stellen indien u woensdag van 12u
tot 14u naar het Luxemburgplein zou kunnen komen en van 14u tot 16u
aanwezig zou kunnen zijn in Kamer A1E1 om van gedachten te wisselen
met software-ontwikkelaars, wetenschappers en bezorgde burgers.

#iel: Met de meeste hoogachting,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxmeps038 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

