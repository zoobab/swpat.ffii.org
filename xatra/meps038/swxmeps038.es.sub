\begin{subdocument}{swxmeps038}{Evento de Patentes de Software Miércoles 27/08/2003 12.00-16.00}{http://swpat.ffii.org/xatra/meps038/index.es.html}{Pracovn\'{\i} skupina\\\url{swpatag@ffii.org}\\versio en nia lingva 2003/12/18 de Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Letter to Members of the European Parliament}
\begin{sect}{xatra}{Estimado Miembro del Parlamento Europeo}
El lunes 1 de Septiembre, el Parlamento Europeo tomar\'{a} su decisi\'{o}n sobre la directiva de patente de software COM(2002)92 2002/0047, eufem\'{\i}sticamente titulada ``on the patentability of computer-implemented inventions''.

El borrador de directiva tal y como ha quedado enmendado por el relator Arlene McCarthy con la ayuda del Comit\'{e} de Asuntos Legales har\'{\i}a patentables reglas de c\'{a}lculo y m\'{e}todos de negocio como el Amazon One Click Shopping, al igual que ocurre en EEUU. 30000 patentes de software al estilo americano que el European Patent Office ha otorgado en contra del esp\'{\i}ritu de la ley vigente ser\'{\i}an aplicables en Europa, y los jueces nacionales, que han severamente criticado la pr\'{a}ctica del EPO y han rechazado seguirla, ser\'{\i}an adem\'{a}s obligados a convertir cualquier cosa que produce el hombre bajo el sol (o ``all practical problem solutions'', como lo llama un juez reconocido del EPO) en algo patentable.

Esos planes no son muy populares. Encuentran resistencia fuerte de parte de:
\begin{itemize}
\item
El FFII Call for Action, firmado por 350 organizaciones y compa\~{n}\'{\i}as y 15000 personas, que pide el rechazo de la directiva y bosqueja una contrapropuesta detallada

\item
Una petici\'{o}n reciente de 20 economistas reconocidos del Parlamento Europeo

\item
Una petici\'{o}n reciente al Parlamento Europeo de 30 cient\'{\i}ficos inform\'{a}ticos europeos famosos

\item
The Eurolinux Petition for a Free Europe without Software Patents\footnote{\url{http://www.noepatents.org/}}, supported by 160,000 signatories, including 2000 CEOs and 3000 scientists

\item
Los opiniones de los Comit\'{e}s CULT y ITRE

\item
El opini\'{o}n del Consejo Social y Econ\'{o}mico de la Uni\'{o}n Europea

\item
Opiniones de la Comisi\'{o}n de Monopolio Alemana, el Consejo de las Regiones de la Uni\'{o}n Europea, la C\'{a}mara de Comercio Alemana, muchas confederaciones de Peque\~{n}as y Medianas Empresas, la Comisi\'{o}n de Planificaci\'{o}n del Estado Franc\'{e}s, el Gobierno Franc\'{e}s, numerosos estudios encargados por la UE y muchos m\'{a}s.
\end{itemize}

Las voces cr\'{\i}ticas piensan que la propiedad en el software es adecuadamente protegida por el copyright y que las patentes desalientan m\'{a}s que promueven las inversiones en el desarrollo de software. Esa cr\'{\i}tica se basa en un consenso \'{e}tico de casi todos los desarrolladores de software, as\'{\i} como en una investigaci\'{o}n seria sobre las din\'{a}micas de innovaci\'{o}n de software.

Los argumentos de estas cr\'{\i}ticas han sido sin embargo sistem\'{a}ticamente despreciadas por los promotores de la actual directiva. Como la petici\'{o}n de 20 economistas y dem\'{a}s personas ha puesto de relieve, los promotores de la directiva les enga\~{n}a constantemente al hacer declaraciones dogm\'{a}ticas sobre lo que dicha directiva consigue en t\'{e}rminos de efectos legales y econ\'{o}micos, mientras rechazan confrontar esas declaraciones con cualquier prueba emp\'{\i}rica.

En particular, hemos esperado en vano una respuesta a las siguientes preguntas b\'{a}sicas:
\begin{enumerate}
\item
Deber\'{\i}an las patentes sobre los algoritmos y los m\'{e}todos de negocio, tal y como se encuentran en una muestra de sample patent claims\footnote{\url{http://localhost/swpat/stidi/manri/index.en.html}} otorgados por el EPO, ser v\'{a}lidas y aplicables en Europa ?

\item
Como se consigue esto a trav\'{e}s de la directiva ? Por favor detalle, citando las reclamaciones de patentes de la muestra y las provisiones de la directiva, por si a caso, que las apartar\'{\i}an de la patentibilidad.
\end{enumerate}

Le rogamos:
\begin{enumerate}
\item
Por favor rechacen la directiva mientras estas dos preguntas b\'{a}sicas no han sido contestadas!

\item
Por favor rechacen la directiva mientras se apoya en criterios abstractos como ``technical contribution'' sin explicar sin ambig\"{u}edad lo que excluyen estos conceptos.

\item
Please reject the directive as long as it does not reliably exclude patents on subject matter such as algorithms and business methods, whose patentability can not be shown to be in the best interests of innovation and productivity!
\end{enumerate}

Para hacerle comprender mejor lo que esta en juego, hemos montado una performance al aire libre y una charla sobre las patentes de software. Agradecer\'{\i}amos mucho su presencia en la Place du Luxembourg, el mi\'{e}rcoles de 12:00 a 14:00, y en la Sala A1E1 de 14:00 a 16:00 para intercambiar opiniones con desarrolladores de software, cient\'{\i}ficos y ciudadanos preocupados.

Saludos,
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf 2003/08/25-9 BXL: Software Patent Directive Amendments\footnote{\url{http://localhost/swpat/penmi/2003/europarl/08/index.en.html}}}}

\begin{quote}
Members of the European Parliament are coming back to work on monday August 25th.  It is the last week before the vote on the Software Patent Directive Proposal.  We are organising a conference and street rally wednesday the 27th.  Some of our friends will moreover be staying in the parliament for several days.  Time to work decide on submission of amendments to the software patent directive proposal is running out.  FFII has proposed one set of amendments that stick as closely as possible to the original proposal while debugging and somewhat simplifying it.  An alternative small set of amendments would ``cut the crap'' and rewrite the directive from scratch.  We present and explain the possible approaches.
\end{quote}
\filbreak

\item
{\bf {\bf Aug 27 Demonstrations against EU Software Patent Plans\footnote{\url{http://localhost/swpat/lisri/03/demo0819/index.en.html}}}}

\begin{quote}
The Proposal for a software patent directive, which will be submitted to the European Parliament for plenary debate and subsequent decision on September 1st, is giving rise to another wave of protests.  Various groups in Belgium and elsewhere are mobilising for a rally in Brussels on August 27th and are calling on web administrators to temporarily block their web sites.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

