<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII calls on Committee of National Parliaments (COSAC) to review
Council Patent legislation

#descr: The lack of democratic control in the EU's lawmaking system has been a
cause of concern for decades.  In particular the Council's legislative
processes are notoriously intransparent.  One approach to address this
problem is the Committee of National Parliaments (COSAC).  A protocol
to the Amsterdam Treaty assigns this committee important functions in
the Council's legislative process.  These functions have been grossly
neglected in the case of the software patent directive.  Vrijschrift,
the dutch branch of the Foundation for a Free Information
Infrastructure, has written a letter to COSAC president Sharon
Dijksma, to raise concerns and call for an intervention of COSAC
before September 24th, the date when the Council will presumably meet
to rubberstamp a %(q:political agreement) from 2004-05-18 to remove
all limits on patentability of %(q:computer-implemented) algorithms
and business methods, thereby radically overturning the legislative
proposals of the European Parliament as well as the consultative
organs of the EU.

#Hae: Header

#bod: Body

#foe: footer

#TdT: To

#aWs: Voorzitter vaste Kamercommissie Europese Zaken, COSAC Presidency, Mrs
Sharon Dijksma

#Fro: From

#irF: Vrijschrift/FFII

#ujc: Subject

#yte: Dutch EU Presidency Negates European Democratic Procedure and the
Protocol on the Role of National Parliaments in the European Union

#tcm: Attachement

#nWl: printversion of this letter

#te5: Amsterdam, September 5 2004

#ask: Dear Mrs Dijksma,

#WWo: We write to inform you of our concerns regarding the way the Dutch EU
Presidency is conducting itself with respect to European procedures
and the Protocol on the Role of National Parliaments in the European
Union. The road taken by the Dutch government negates the Protocol on
the Role of National Parliaments and weakens European democracy.

#epi: On May10th Minister Brinkhorst misinformed the Dutch Parliament about
the Directive %(q:computer-implemented inventions), and voted for the
Council's proposal on May 18th based on that misrepresentation. As a
result, the Dutch Parliament has adopted a motion by MP Van Dam
calling the government to withdraw its vote.

#ciW: The Dutch Parliament required two sessions of the Committee on
Economic Affairs to come to realize the issues with the Council's
text, and how detrimental it would be for SMEs and innovation. The
Directive's text is overcomplicated, with double negations,
duplicitous qualifications (%(q:exceptions) that are always true) and
undefined exclusions. The Parliament fears %(q:excesses) with regards
to the patentability of software. Empirical evidence shows patents on
software impede innovation.

#nDi: The Dutch Parliament concluded that since there was ongoing discussion
of the Directive, the government should abstain from the Council's
vote for political agreement.

#aor: Minister Brinkhorst stated in a debate on the matter that for
constitutional reasons the Council's vote cannot be reversed. The
State Secretary also misrepresented the facts in the debate on the Van
Dam motion, by calling the political agreement a %(q:common position),
thereby presenting the non-binding agreement as a binding one.
Furthermore, a Brussels Permanent Representative said the vote cannot
be changed {1}. In these actions, the Dutch government has spread
critical incorrect information about European Union democratic
procedures three times {2}.

#eWo: The Protocol on the Role of National Parliaments in the European Union
assures Parliaments early access to legislative proposals. The
Parliaments of the 10 new members of the Union have not had early
access, since the translations of the EU Council's political agreement
have only very recently become available.

#enl: Since the translations became available during the Parliaments' recess
periods, it would be appropriate to add a short extension to the six
weeks period provided under the Protocol, within which national
Parliaments may render their conclusions regarding the Council's
political agreement. No shortening of the six weeks period can be
allowed.

#hrs: Without the six weeks review period, the Parliaments of the new
members are denied the rights the Protocol provides for. There is no
ground to deny the new members these rights, since, so far as we know,
there is no jurisprudence on the Protocol, and no mention of this
situation in the Treaty of Accession.  The new members of the European
Union must enjoy their full rights to address the Directive.

#crs: Eastern Europe would bear the hardest burden in the outcome of this
decision.  SMEs in Europe can little afford to compete in a context
that would include the kinds of patents this Directive will validate.
The new Eastern European members of the Union are hardly in a better
position than the rest of Europe. Eastern Europe will never own its
software industry under the Council's text.

#Wee: Europe needs a sensible patent system. The Council's misleading text
is no contribution to that. Critical amendments contributed by the
European Parliament, including a key definition clarifying the term
%(q:technical), were discarded by the Council for the dissembling
language that they favor {3}.

#vor: The Council worked entirely on the basis of text developed in the JURI
committee {4}. They simply discarded clear language such as 'programs
are not inventions' of the European Patent Convention and
clarifications like 'data processing is not a field of technology' and
'publication and distribution of software is never a patent
infringement' of the Parliament.

#aiW: We believe the Directive must be discussed as a B-item in the Council,
not allowed to pass as a mere formality under the A-item agenda
points. The Netherlands has adopted a motion showing a lack of faith
in the vote. There has been discussion all over Europe regarding the
misleading nature of the Directive and the problematic conduct of the
vote. A large group of Ministers joined in supporting Germany in
including a critical text that was dropped at the very moment of the
vote, putting those Ministers, including a substitute Danish Minister,
in a difficult position. The Directive should not be handled by
non-involved ministers {5}.

#ule: Every Parliament in the European Union has the right to debate the
Directive on Computer-Implemented Inventions, and to render their
opinions. Any Council member may unilaterally act to transfer an
A-item decision indicating a mere formality to the list of B-item
agenda points {5}. Every member of the Council has the right to change
their present vote {2}. Any Council member may unilaterally block any
proposed shortening of the six weeks review period. All of the
national members of the European Union have the freedom and the right
to engage in any decision that determines their future.

#ilo: The proceedings surrounding the Directive on Computer-Implemented
Inventions are a particularly transparent instance representing
democratic deficits in the European Union. The misrepresentations of
the procedure and of the status of the Council's political agreement,
and the failure to apply the Protocol on the Role of National
Parliaments to the new members of the Union in the deliberations, are
not only signals of a failure to conduct the deliberations on the
Directive in an open and accountable manner. They also represent a
cessary and worthy occasion to set the right precedent for democratic
process.

#tye: Please require the Council and the Dutch Presidency to make clear that
democracy is here to stay in Europe.

#WeC: Voorzitter vaste Kamercommissie Europese Zaken, we hope you will
forward this letter to the COSAC members as well.

#erm: On behalf of Vrijschrift/FFII,

#iel: Sincerely,

#nWe: Ante Wessels

#emn: Arend Lammertink, Ir

#mWl: Harmen van der Wal, Mr.

#rWs: Voorzitter vaste kamercommissie Europese Samenwerkingsorganisaties

#aaD: Detailed analysis (Dutch)

#oWo: Lopez report

#prc: This opinion was confirmed by the Council itself

#mWt: See Kauppi comment at %(URL1), and Tauss on the Danish Parliament's
level of participation at %(URL2)

#hPe: See for this the %(vh:Council Political Agreement Reversal HowTo)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: LtrCosac040905 ;
# txtlang: en ;
# multlin: t ;
# End: ;

