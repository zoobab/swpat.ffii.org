\begin{subdocument}{swxparl038}{FFII/Eurolinux 2003/08 Letter to Software Creators and Users}{http://swpat.ffii.org/letters/swxparl038/index.en.html}{Workgroup\\swpatag@ffii.org}{The European Parliament will, in its plenary session on September 1st, decide on a directive proposal which ensures that algorithms and business methods like Amazon One Click Shopping become patentable inventions in Europe.  This proposal has the backing of about half of the parliament.  Please help us make sure that it will be rejected.  Here are some things to do.}
Dear Software Users, Authors and Investors

The European Parliament will, in its plenary session on September 1st, decide on a directive proposal which ensures that algorithms and business methods like Amazon One Click Shopping become patentable inventions in Europe.  This proposal has the backing of about half of the parliament.  Please help us make sure that it will be rejected.  Here are some things to do.

\begin{sect}{aktiv}{Register as a Supporter}
If you haven't done it already, please register at http://aktiv.ffii.org/eubsa/en/.

see also Help 2003/04\footnote{http://swpat.ffii.org/letters/parl038/index.en.html}
\end{sect}

\begin{sect}{bxl030827}{Be in Brussels on Wednesday August 27th}
We are planning another day of physical presence in and near the European Parliament, including a performance beside the Parliament at 12.00-14.00 and a conference in the Parliament in the afternoon.

see Brussels 2003/08/27: Software Patent Directive Vote: Minimum Amendment Demands\footnote{http://swpat.ffii.org/events/2003/europarl/08/index.en.html}

It would be great if you could come and use the opportunity to stay for 1-2 days and make an appointment with your MEP.  To facilitate and coordinate this, you can use AU or write to europarl at ffii org.  More explanations are found at EUROLINUX Alliance\footnote{http://www.eurolinux.org/index.en.html}.  We also help with reserving beds in Brussels.  They are available for free under certain conditions, please contact hotel at ffii org.  If you want to do more, e.g. spend a week talking to MEPs in Brussels, we may be able to reimburse your travel fees and even pay moderate stipends.

Do you think that coming to Brussels costs you too much time?  Have you considered how much time it will cost you later to face patent infringement claims and to cope with monopoly software?  Which of the two is better spent?
\end{sect}

\begin{sect}{onldemo}{Demonstrate Online}
If you administrate some servers or own some webspace, you might wish to block or delay access to certain web pages on August 27th.

This can be done in a more or less gentle fashion.  Some examples on what you could do are shown at

Demonstrations Against Software Patents\footnote{http://swpat.ffii.org/group/demo/index.en.html}

Do you think that blocking access to web pages is too drastic a measure to take?  Have you considered how many web pages will be rendered inaccessible later by means of software patents if we fail to prevent this law now?  Which of the two is less drastic?
\end{sect}

\begin{sect}{media}{Inform the Media}
We are turning out press releases and other news\footnote{http://swpat.ffii.org/index.en.html\#news} all the time.  Please help us to systematically inform newspapers, radio, tv etc in your country.  Contact media at ffii org (or media-xx at ffii org, where xx is your country code) for more info.  If you are willing to reserve some time and do the work systematically, we may be able to pay a moderate stipend.
\end{sect}

\begin{sect}{donac}{Donate for our activities}
During these last days and weeks FFII will be paying fees and stipends for helpers from all over Europe.  We will also be paying for printed materials, banners, performances etc.  All this will cost us around 10000 eur, slightly more than we currently have at our disposal.  Donations would not only help us to stay liquid and fully realise our plans.  They might also enable us to enlarge the plans, e.g. by placing an effective advertisement somewhere where MEPs read it (would cost about 10,000 eur).

The bank account for a donation is \begin{quote}
\begin{description}
\item[Country:]\ DE (Germany)
\item[Bank Code:]\ 70150000
\item[BIC (bank identifier code):]\ SSKMDEMM
\item[SWIFT:]\ SSKM DE MM
\item[Name of Bank:]\ Stadtsparkasse Muenchen
\item[Bank's Postal Address:]\ DE 80335 Muenchen\\
Lotstr 1
\item[Account:]\ 31112097
\item[IBAN (international bank account number)\footnote{We are told that, using the IBAN, money can now be transferred between european countries for the domestic fee, thanks to a recent EU internal market harmonisation directive which can be enforced against banks.}:]\ DE78701500000031112097
\item[Account Owner:]\ FFII e.V.
\item[Account Owner's Postal Address:]\ DE 80636 Muenchen\\
Blutenburgstr 17
\item[Keyword:]\ europarl
\end{description}
\end{quote}

Alternatively you can donate via paypal.com\footnote{http://www.paypal.com} to paypal at ffii org.

FFII is acknowledged as a public-interest association and we can send you a receipt that at least in Germany makes the donation tax-deductible.

see also FFII Financial Reports\footnote{http://www.ffii.org/assoc/money/index.en.html}

What will you spend on patent trouble and crippled software later if this directive proposal goes through?
\end{sect}

Yours Sincerely

\begin{center}
Hartmut Pilch on behalf of FFII e.V. and Eurolinux Alliance

\dots
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatxatra.el ;
% mode: latex ;
% End: ;

