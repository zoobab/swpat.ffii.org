\begin{subdocument}{swpatxatra}{Briefe und Appelle gegen die Patentinflation}{http://swpat.ffii.org/xatra/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/12/19 von Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Eine Sammlung von Briefen zum Thema Softwarepatente, die seit 1999 von FFII und anderen an verschiedene Entscheidungstr\"{a}ger geschickt wurden.}
\begin{sect}{inhalt}{Inhalt}
\begin{itemize}
\item
{\bf {\bf Call for Action}\footnote{\url{}}}

\begin{quote}
Der Richtlinienvorschlag der Europ\"{a}ischen Kommission f\"{u}r die Patentierbarkeit von Software-Innovationen erfordert eine klare Antwort vom Europ\"{a}ischen Parlament, den nationalen Regierungen und anderen Akteuren.  Hier erkl\"{a}ren wir, was unserer Meinung nach zu tun ist.
\end{quote}
\filbreak

\item
{\bf {\bf PHM to AMccarthy 03/06/10: Questions based on 2 Example Patent Claims}\footnote{\url{http://aful.org/wws/arc/patents/2003-06/msg00047.html}}}

\begin{quote}
Hartmut Pilch von FFII schreibt \"{o}ffentlich an Arlene McCarthy und zitiert einen Anspruch auf einen Algorithmus und einen Anspruch auf eine Gesch\"{a}ftsmethode, die beide vom Europ\"{a}ischen Patentamt (EPA) als Patente gegen den Bustaben und Geist des Europ\"{a}ischen Patentabkommens erteilt wurden, und die beide US-Gegenst\"{u}cke haben, die dort schon jetzt f\"{u}r betr\"{a}chtlichen Schaden sorgen.  Pilch findet in McCarthys Texten Aussagen, die sich gegen Patente auf Algorithmen und Gesch\"{a}ftsmethoden wenden, und andere Aussagen, die die Patentierbarkeit der zitierten Beispiel-Anspr\"{u}che unvermeidbar erscheinen lassen, und fordert McCarthy auf, im Sinne des von ihr geforderten ``offenen, demokratischen Dialogs'' diese Widerspr\"{u}che zu kl\"{a}ren.  McCarthy antwortete indirekt auf der JURI-Sitzung am 16.6.2003:  ``Wir sind keine Spezialisten, wir k\"{o}nnen nur einen Interpretationsrahmen vorgeben.''
\end{quote}
\filbreak

\item
{\bf {\bf XDrudis to AMccarthy 03/06/10: Your Note on Directive COM (2002) 92}\footnote{\url{http://aful.org/wws/arc/patents/2003-06/msg00040.html}}}

\begin{quote}
Xavier Drudis Ferran von der Catalan Linux Users Group (CALIU) informiert Arlene McCarthy MEP \"{u}ber seine Analyse des FAQs welches sie an die Teilnehmer der FFII/Eurolinux Konferenz im Dorint Hotel in Brussel vom 7.-8.5.2003 verteilte, weist hin auf manche Trugschl\"{u}sse in diesem Document und l\"{a}dt McCarthy zur Teilnamhe am Dialog ein \"{u}ber wessen Abwesenheit sie sich vor Journalisten beklagt hat.  Dieser Brief ist bislang unbeantwortet geblieben.
\end{quote}
\filbreak

\item
{\bf {\bf FFII calls on Committee of National Parliaments (COSAC) to review Council Patent legislation}\footnote{\url{http://localhost/swpat/xatra/LtrCosac040905/index.en.html}}}

\begin{quote}
The lack of democratic control in the EU's lawmaking system has been a cause of concern for decades.  In particular the Council's legislative processes are notoriously intransparent.  One approach to address this problem is the Committee of National Parliaments (COSAC).  A protocol to the Amsterdam Treaty assigns this committee important functions in the Council's legislative process.  These functions have been grossly neglected in the case of the software patent directive.  Vrijschrift, the dutch branch of the Foundation for a Free Information Infrastructure, has written a letter to COSAC president Sharon Dijksma, to raise concerns and call for an intervention of COSAC before September 24th, the date when the Council will presumably meet to rubberstamp a ``political agreement'' from 2004-05-18 to remove all limits on patentability of ``computer-implemented'' algorithms and business methods, thereby radically overturning the legislative proposals of the European Parliament as well as the consultative organs of the EU.
\end{quote}
\filbreak

\item
{\bf {\bf Beschwerde an das ZDF wegen Heute-Journal-Beitrag über %(q:Raubkopierer und Ideenklauer)}\footnote{\url{http://localhost/swpat/xatra/zdf0407/index.de.html}}}

\begin{quote}
In dem beanstandeten Film-Beitrag von Manfred Ahlers vom 6. Juli 2004 wird der FFII e.V. durch Verf\"{a}lschung und Weglassen wesentlicher Tatsachen sowie durch Einsatz von Suggestivmethoden wahrheitswidrig als Interessenvertretung von Softwarepiraten und Raubkopierern diskreditiert. Inhalt und Form des Beitrages versto{\ss}en dabei gegen die im ZDF-Staatsvertrag festgeschriebenen Programmgrunds\"{a}tze und gegen die Programmrichtlinien des ZDF.
\end{quote}
\filbreak

\item
{\bf {\bf Brief von BVMW, Patentverein und FFII an den Bundeskanzler}\footnote{\url{http://localhost/swpat/xatra/bvmw040705/index.de.html}}}

\begin{quote}
Zwei Tage vor dem Gipfeltreffen der deutschen Patentbewegung, auf dem Bundeskanzler Gerhard Schr\"{o}der und Justizministerin Brigitte Zypries zusammen mit Siemens-Chef Heinrich von Pierer das Patentwesen und den Einsatz der Regierung f\"{u}r Softwarepatente in der EU feiern werden, \"{u}ben die Vorsitzenden des Bundesverbandes der Mittelst\"{a}ndischen Wirtschaft, des Patentvereins und des F\"{o}rdervereins f\"{u}r eine Freie Informationelle Infrastruktur scharfe Kritik an der z\"{u}gellosen Entwicklung des Patentwesens in Deutschland.  Das Kanzleramt versprach, den Brief dem Kanzler vorzulegen.  Justizministerin Brigitte Zypries antwortete 8 Wochen sp\"{a}ter an Stelle des Bundeskanzlers durch einen Brief, in dem sie einen Konsens \"{u}ber die angeblich \"{u}berragende Bedeutung des Patentwesens f\"{u}r die F\"{o}rderung der Innovation konstatiert und einen Teil der bekannten BMJ-Falschaussagen \"{u}ber die Haltung der Bundesregierung zu Softwarepatenten wiederholt.  Auf die Ausf\"{u}hrungen des Schreibens der drei Verb\"{a}nde wird nicht eingegangen.  Daf\"{u}r wird eine Fortsetzung von Dialog, u.a. durch eine \"{o}ffentliche ``Fachdiskussion'' zum Thema ``Schutz computerimplementierter Erfindungen -- Wie geht es weiter?'' am 21. Oktober in M\"{u}nchen angek\"{u}ndigt.
\end{quote}
\filbreak

\item
{\bf {\bf FFII invites OpenForumEurope to explain how text patents benefit open source software}\footnote{\url{http://localhost/swpat/xatra/ofeu034/index.en.html}}}

\begin{quote}
Open Forum Europe is lobbying the European Parliament in the name of ``open source companies'' in order to make software directly patentable and to ensure that interoperable software may not be written.  We invited Graham Taylor of Open Forum to explain.  An short dialog followed, in which Taylor presented himself as naively well-believing about the meaning of what he signed and reluctant to repair the damage.
\end{quote}
\filbreak

\item
{\bf {\bf FFII an Bitkom: Gesetzentwürfe an Testsuite messen!}\footnote{\url{http://localhost/swpat/xatra/bitk025/index.de.html}}}

\begin{quote}
Am 7. Mai 2002 ver\"{o}ffentlichte der IT-Branchenverband Bitkom eine Presseerkl\"{a}rung, in der er den EU-Richtlinienentwurf f\"{u}r die Patentierbarkeit von ``computer-implementierbaren Erfindungen'' unterst\"{u}tzt. Nach telefonischer Unterhaltung mit Urhebern dieser Presseerkl\"{a}rung schrieb Hartmut Pilch einen Brief an Bitkom, den wir hier ver\"{o}ffentlichen.  Darin erl\"{a}uterte er einen methodologische Konsensposition, die auch den Bitkom-Leuten einzuleuchten schien.
\end{quote}
\filbreak

\item
{\bf {\bf Letter to Telecom CEOs}\footnote{\url{http://localhost/swpat/xatra/tele03B/index.en.html}}}

\begin{quote}
Draft by Jacob Hallen
\end{quote}
\filbreak

\item
{\bf {\bf Questions to the President of the European Patent Office}\footnote{\url{http://localhost/swpat/xatra/epop03B/index.en.html}}}

\begin{quote}
Suggestions to members of the European Parliament
\end{quote}
\filbreak

\item
{\bf {\bf Urgent Call to National Governments and Parliaments}\footnote{\url{http://localhost/swpat/xatra/cons0406/index.de.html}}}

\begin{quote}
Die Regierungen Europas sind dabei, ihre Unterschrift unter einen Richtlinienentwurf zu setzen, der uneingeschr\"{a}nkte Patentierbarkeit und Patentdurchsetzbarkeit auf ``computer-implementierte'' Algorithmen und Gesch\"{a}ftsmethoden erlaubt. Die Einigung im Ministerrat vom 18. Mai 2004 verwirft dabei ohne jede Rechtfertigung und auf demokratisch nicht legitimierte Weise wohlbedachte Entscheidungen des Europ\"{a}ischen Parlaments und der beratenden EU-Organe. Die Stimmenmehrheit auf der entscheidenden Sitzung wurde stattdessen durch irref\"{u}hrende Formulierungen und fragw\"{u}rdige diplomatische Winkelz\"{u}ge gesichert. Die Unterzeichnenden, f\"{u}hrend im Bereich von Softwareinnovation und der Diskussion \"{u}ber deren Sicherung im rechtlichen Rahmen Europas, fordern von den verantwortlichen Politikern, die Notbremse zu ziehen und den Prozess der Gesetzgebung im Wettbewerbsrecht zu reorganisieren.
\end{quote}
\filbreak

\item
{\bf {\bf FFII/Eurolinux 2003/04 Letter to Software Creators and Users}\footnote{\url{http://localhost/swpat/xatra/parl034/index.de.html}}}

\begin{quote}
Das Europ\"{a}ische Parlament wird wahrscheinlich eine Softwarepatentrichtlinie verabschieden, m\"{o}glicherweise mit hilfreichen \"{A}nderungen.  Als Betroffener k\"{o}nnen Sie schon viel bewegen, wenn Sie sich eine Stunde Zeit nehmen.  Wir sagen Ihnen, was Sie tun k\"{o}nnen, und machen Ihnen es so leicht wie m\"{o}glich.
\end{quote}
\filbreak

\item
{\bf {\bf 2003/08 Letter to Software Creators and Users}\footnote{\url{http://localhost/swpat/xatra/parl038/index.en.html}}}

\begin{quote}
The European Parliament will, in its plenary session on September 1st, decide on a directive proposal which ensures that algorithms and business methods like Amazon One Click Shopping become patentable inventions in Europe.  This proposal has the backing of about half of the parliament.  Please help us make sure that it will be rejected.  Here are some things to do.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patent Events Wednesday 2003/08/27 12.00-16.00}\footnote{\url{http://localhost/swpat/xatra/meps038/index.de.html}}}

\begin{quote}
Brief an Europa-Abgeordnete
\end{quote}
\filbreak

\item
{\bf {\bf Appell an die Bundesregierung}\footnote{\url{http://localhost/swpat/xatra/bund028/index.de.html}}}

\begin{quote}
Die Bundesregierung setzt sich in Br\"{u}ssel f\"{u}r die Legalisierung von Logikpatenten ein, die sie selbst verletzt.  Sie hat bislang keinen Standpunkt zum Br\"{u}sseler Softwarepatent-Richtlinienentwurf ver\"{o}ffentlicht.  W\"{a}hrend im Bundestag und in den Ministerien insgesamt Skepsis vorherrscht, dr\"{a}ngt das federf\"{u}hrende Patentreferat im Bundesministerium der Justiz (BMJ) den Rat der Europ\"{a}ischen Union (REU) zur Verabschiedung eines ``Kompromisses'' zwischen dem Kommissions-Entwurf und den noch weiter gehenden W\"{u}nschen der Patent\"{a}mter und ihrer Beir\"{a}te aus Gro{\ss}konzern-Patentjuristen.  Demnach soll nicht erst die Ausf\"{u}hrung sondern bereits die Ver\"{o}ffentlichung eines eigenst\"{a}ndig entwickelten Computerprogramms strafbar werden.  Dabei ignoriert die Bundesregierung alle Studien und Diskussionen der letzten Jahre, einschlie{\ss}lich der von der Bundesregierung selbst in Auftrag gegebenen.  Die Unterzeichner richten vier Forderungen an die Bundesregierung.
\end{quote}
\filbreak

\item
{\bf {\bf 2001/01: Brief an das BMJ}\footnote{\url{http://localhost/swpat/xatra/epue31/index.de.html}}}

\begin{quote}
Der FFII erhielt vom Bundesministerium der Justiz (BMJ) ein Schreiben, in dem Auskunft \"{u}ber die Ergebnisse der Diplomatischen Konferenz zur Revision des Europ\"{a}ischen Patent\"{u}bereinkommens gegeben und zu Stellungnahmen aufgerufen wird.  Dies geschieht mit diesem Brief.  Der FFII kritisiert u.a. erneut die \"{U}bernahme der TRIPs-Formel ``auf allen Gebieten der Technik'' in Art 52 EP\"{U}.
\end{quote}
\filbreak

\item
{\bf {\bf Offener Brief: 5 Gesetzesinitiativen zum Schutz der Informatischen Innovation}\footnote{\url{http://localhost/swpat/xatra/patg2C/index.de.html}}}

\begin{quote}
Mit Bitte um Verbesserungsvorschl\"{a}ge und Mithilfe bei der weiteren Ausarbeitung der einzelnen Gesetzesinitiativen.  Wenn Sie im Namen eines IT-Unternehmens oder IT-Verbandes sprechen k\"{o}nnen, bitten wir ferner um Ihre Zustimmung zur Nennung als Unterzeichner.  Hier finden Sie nur den Haupttext.  Der vollst\"{a}ndige Brief mit zahlreichen Anh\"{a}ngen wird in Papierfassung an Politiker versandt.
\end{quote}
\filbreak

\item
{\bf {\bf Eur. Patentamt begehrt grenzenlose Patentierbarkeit}\footnote{\url{http://localhost/swpat/xatra/epue28/index.de.html}}}

\begin{quote}
Offener Brief zum ``Basisvorschlag f\"{u}r die Revision des Europ\"{a}ischen Patent\"{u}bereinkommens''
\end{quote}
\filbreak

\item
{\bf {\bf Musterbrief zum Schutz der Informatischen Innovation}\footnote{\url{http://localhost/swpat/xatra/nopat/index.de.html}}}

\begin{quote}
Diesen oder einen \"{a}hnlichen Brief k\"{o}nnten Sie unterschreiben und mit geeigneten Anh\"{a}ngen an diverse Entscheidungstr\"{a}ger versenden.
\end{quote}
\filbreak

\item
{\bf {\bf Euro-Patente in Logiksprache!}\footnote{\url{http://localhost/swpat/xatra/lojban/index.de.html}}}

\begin{quote}
Die europ\"{a}ischen Patentorganisationen sollten helfen, die Entwicklung von freier Software zur Analyse und automatischen \"{U}bersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise k\"{o}nnen sie letztendlich Patentbesitzern sehr viel Geld und \"{A}rger sparen und gleichzeitig der \"{O}ffentlichkeit und den Unternehemen einen sprachunabh\"{a}ngigen Zugriff auf Patentinformationen erschlie{\ss}en.
\end{quote}
\filbreak

\item
{\bf {\bf Leserbriefe an Presseorgane}\footnote{\url{http://localhost/swpat/xatra/karni/index.de.html}}}

\begin{quote}
Die Presse wird als ``Artillerie der Freiheit'' ger\"{u}hmt.  Allerdings finden Journalisten nicht immer die Zeit, sich kundig zu machen.  Besonders in komplexen Sachgebieten wie SWPAT bedarf es der Nachhilfe durch Leserbriefe.
\end{quote}
\filbreak

\item
{\bf {\bf Brief an den Wettbewerbskommissar}\footnote{\url{http://localhost/swpat/xatra/miert/index.de.html}}}

\begin{quote}
Wir haben diesen Brief, den mittlerweile an die 10000 Menschen unterschrieben haben, an Herrn Van Miert weitergeleitet und werden auch seinen Nachfolger sowie weitere Kommissionsmitglieder und zust\"{a}ndige Stellen davon unterrichten.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Briefe aus anderen Quellen}
\begin{itemize}
\item
{\bf {\bf Phil Karn: The US Patent System is Out of Control\footnote{\url{http://people.qualcomm.com/karn/patents/patents.html}}}}

\begin{quote}
Kenntnisreiche und bewegende Briefe eines erfahrenen Programmierers an diverse amerikanische Abgeordnete.
\end{quote}
\filbreak

\item
{\bf {\bf European Consultation on the Patentability of Computer-Implemented Rules of Organisation and Calculation (= Programs for Computers)\footnote{\url{http://localhost/swpat/papri/eukonsult00/index.de.html}}}}

\begin{quote}
Am 19. Okt 2000 ver\"{o}ffentlichte die Dienststelle f\"{u}r Gewerblichen Rechtsschutz der Europ\"{a}ischen Kommission (EuDGR) ein Sondierungspapier, welches eine rechtliche Argumentation darlegt, wie das Europ\"{a}ische Patentamt (EPA) sie in den letzten Jahren verwendet hat, um ihre Praxis der Patentierung von Programmen f\"{u}r Datenverarbeitungsanlagen und anderen Organisations- und Rechenregeln gegen den Buchstaben und Geist der geltenden Gesetze zu rechtfertigen.  Die Konsultation richtete sich offenbar an die Patentabteilungen diverser Unternehmen und Verb\"{a}nde und war als ein Man\"{o}ver zu ihrer Mobilisierung konzipiert.  Das Papier selber warb einseitig f\"{u}r den Standpunkt des Europ\"{a}ischen Patentamtes und stellte Fragen, die nur Patentjuristen verstehen und beantworten k\"{o}nnen.  Ferner wurde es von einer ``unabh\"{a}ngigen Studie'' best\"{a}tigt, welche eine bekannte Denkfabrik der Patentbewegung im Auftrag der EuDGR durchgef\"{u}hrt hatte.  Patentjuristen verschiedener Organisationen sandten applaudierende Antworten ein und erkl\"{a}rten dabei das bekannte Credo der Patentbewegung, wonach Patente grunds\"{a}tzlich in allen Gebieten die Innovation f\"{o}rdern und vor allem dem Wohle der kleinen und mittleren Unternehmen dienen.  Allerdings antworteten auch einige Verb\"{a}nde und Firmen sowie \"{u}ber 1000 Einzelpersonen, vor allem Programmierer, mit kritischen Stellungnahmen.  Die EuDGR hat die Stellungnahmen bisher nur schleppend und unvollst\"{a}ndig und in schwer konsultierbarer Form ver\"{o}ffentlicht.  Dem wollen wir abhelfen, und Sie k\"{o}nnen mitmachen.
\end{quote}
\filbreak

\item
{\bf {\bf (The Eurolinux Software Patent Consultation Page)\footnote{\url{http://petition.eurolinux.org/consultation}}}}

\begin{quote}
ca 1000 Eingaben europ\"{a}ischer Programmierer an die Europ\"{a}ische Kommission
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Adressen von Leuten sammeln, an die wir uns wenden sollten!}}

\begin{quote}
Wir brauchen einen st\"{a}ndig aktuellen Bausatz zum Briefeschreiben.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

