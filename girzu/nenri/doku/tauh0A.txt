# -*- coding: iso-8859-1 -*-

   Wolfgang Tauchert 1999, in: Mitteilungen der deutschen Patentanw�lte
   90:248-252.
   
   Zur Patentierbarkeit von Programmen f�r Datenverarbeitungsanlagen
   
                             Wolfgang Tauchert*
   
   l. Gesetzliche Ausgangssituation
   
   Patente werden f�r Erfindungen erteilt, die neu sind, auf einer
   erfinderischen T�tigkeit beruhen und gewerblich anwendbar sind, so
   hei�t es im � l PatG, wie im Art. 52 EP�.
   
   Erfindungen m�ssen aber auch dem Bereich der Technik zuzuordnen sein;
   dies ergibt sich indirekt aus den �� 3, 4 des Patentgesetzes; danach
   mu� eine patentf�hige Erfindung neu und erfinderisch sein gegen�ber
   dem Stand der Technik;
   
   um dies zu erf�llen, mu� sie selbst dem Bereich der Techno zuzuordnen
   sein. Der Begriff "Erfindung" ist sonach auf den Bereich der Technik
   festgelegt. Der Ausdruck "nichttechnische Erfindung" ist in sich
   widerspr�chlich, auch wenn er bisweilen verwendet wird. Die Festlegung
   des Patentschutzes auf den Bereich der Technik ist also im deutschen
   Patentgesetz implizit verankert, wobei der Begriff der Technik dort
   nicht definiert wird.
   
   Unabh�ngig davon gibt � l Abs. 2 PatG eine nicht abschlie�ende
   Aufz�hlung von Gegenst�nden, die keine Erfindungen im Sinne des PatG
   sind:
   
   - Entdeckungen, wissensch. Theorien, math. Methoden;
   
   - �sthetische Formsch�pfungen;
   
   - Pl�ne, Regeln, Verfahren f�r gedankliche T�tigkeiten, f�r Spiele
   oder f�r gesch�ftliche T�tigkeiten sowie Programme f�r
   Datenverarbeitungsanlagen; die Wiedergabe von Informationen.
   
   Diese Aufz�hlung entspricht der Negativliste des Art. 52 Abs. 2 EP�
   und enth�lt unter anderem Programme f�r Datenverarbeitungsanlagen.
   Abs. 3 PatG � l stellt jedoch -entsprechend Regel 52 - zus�tzlich
   fest:
   
   "Abs. 2 steht der Patentf�higkeit nur insoweit entgegen, als f�r die
   genannten Gegenst�nde oder T�tigkeiten als solche Schutz begehrt
   wird."
   
   Die Bedeutung der Einschr�nkung "als solche" ist zu interpretieren als
   "soweit nicht patentf�hige Verfahren oder Vorrichtungen im
   Zusammenhang mit den genannten Begriffen angesprochen sind.
   
   � l, Abs. 2 und 3 PatG sind unter dem Gesichtspunkt einer geregelten
   Koexistenz der verschiedenen Formen des gewerblichen Rechtsschutzes zu
   sehen. Eine vergleichbare Einschr�nkung ist notwendig, will man an den
   verschiedenen Formen des gewerblichen Rechtsschutzes (Patent,
   Gebrauchsmuster, Topographie, Urheberrecht, Geschmacksmuster, Marke)
   festhalten. Dies erm�glicht, da� ein Gegenstand unter
   unterschiedlichen Aspekten verschiedenen Formen des gewerblichen
   Rechtsschutzes zugeordnet werden kann; z. B. kann eine bestimmte Form
   einer Flasche unter technischen (Druckfestigkeit), gestalterischen
   (Aussehen) und markenrechtlichen (Unterscheidungskraft)
   Gesichtspunkten formuliert und entsprechen als Patent,
   Geschmacksmuster und Marke gesch�tzt werden. Die
   
   Beschreibung des entsprechenden Gegenstandes in einer Fachzeitschrift
   (nicht jedoch die Publikation der Patentschrift) begr�ndet ein
   Urheberrecht. Mit diesem ist der Autor jedoch nur gegen die Kopie oder
   weitgehend identische Wiedergabe der Darstellungsform, also der
   Beschreibung, gesch�tzt. Dagegen sch�tzt das Patent das damit bean-
   spruchte Funktionsprinzip, gerade auch in seiner praktischen
   Umsetzung, d.h. das technische Handeln.
   
   Will man also die bestehenden Formen des gewerblichen Rechtsschutzes -
   insbesondere von Urheberrecht und Patent - aufrechterhalten, dann darf
   der technische Charakter der Lehre nicht allein in der Verk�rperung
   von Programmschritten in Strukturen eines Rechners oder Datenspeichers
   zu erreichen sein. Denn dann k�nnten auch rein beschreibenden oder
   k�nstlerische Darstellungen �ber ihre elektronische Speicherung
   patentf�hig werden. Ein beliebiger Sachverhalt, wie z.B. ein
   beschreibender Text oder ein Bild, wird jedoch durch Verk�rperung in
   einem elektronischen Speicher ebensowenig technisch wie durch Druck
   auf Papier (vgl. BGH-Entscheidung "Dispositionsprogramm"1). �ber diese
   Einschr�nkung besteht auch international Konsens. Die Freiheit der
   geistigen Sch�pfung ist in jedem Falle zu gew�hrleisten.
   
   2. Rechtsprechung - �bersicht und Bewertung
   
   Der Bundesgerichtshof hat in seinen Leitentscheidungen (genannt seien
   beispielsweise "Dispositionspro^ramm"1, "ABS - Antiblockiersystem"2, -
   "Seitenpuffer"3, "chinesische Schriftzeichen"4 und "Tauchcomputer"5)
   den im Patentgesetz nicht n�her festgelegten Begriff der Technik
   angewandt.
   
   Technisch ist dabei (wie z.B. in der "ABS"-Entscheidung2 ausgef�hrt)
   eine Lehre zum planm��igen Handeln unter Einsatz beherrschbarer
   Naturkr�fte zur Erreichung eines kausal �bersehbaren Erfolgs, der ohne
   Zwischenschaltung menschlicher Verstandest�tigkeit die unmittelbare
   Folge beherrschbarer Naturkr�fte ist.
   
   Diese Definition erinnert an die mit dem Begriff der Technik
   verbundene, maschinen-gebundene Arbeitsweise, und verweist damit auf
   das "Turing-Prinzip". Danach ist eine maschinelle L�sung eines
   Problems immer dann m�glich, wenn sie durch einen Algorithmus
   beschrieben werden kann. Die Art des zu l�senden Problems ist dabei
   ohne Bedeutung. Beispielsweise k�nnen auch Ma�nahmen des Ordnens einer
   maschinellen - und damit technischen -L�sung zugef�hrt werden. Aus der
   Definition ergibt sich unmittelbar, da� sich der Bereich der Technik
   in dem Umfang �ndert, wie Naturkr�fte beherrschbar werden.
   
   Die Technik-Definition des Bundesgerichtshofs bezieht so auch den
   Begriff der Information in die Technik ein,
   
   *Dr. Wolfgang Tauchen ist Leiter derAbt. 1.53 (Datenverarbeitung) des
   Deutschen Patent- und Markenamts. Der Beitrag gibt die private Meinung
   des Autors wieder. BGH GRUR 1977, 96 = PMZ 77, 20 -
   Dispositionsprogramm BGH GRUR 1980, 849 = PMZ 81, 70 - ABS -
   Antiblockiersystem
   
   3 BGH PMZ 1991, 345 - Seitenpuffer
   
   4 BGH PMZ 1991, 388 = NJW 92, 374 - Chinesische Schriftzeichen
   
   5 BGH PMZ 1992, 255 - Tauchcomputer
   
   zumindest insoweit, als sie durch Naturkr�fte beherrschbar, d.h. durch
   physikalische Gr��en darstellbar und verarbeitbar ist.
   
   Maschinell verarbeitbare Information bezeichnet man als Daten.
   Datenverarbeitung, also die maschinelle Verarbeitung physikalisch
   darstellbarer Gr��en, ist somit auf der Basis der Definition des
   Bundesgerichtshofes ohne Einschr�nkung als technisch zu beurteilen.
   Bei entsprechendem Verst�ndnis der Begriffe erscheint somit die durch
   den Bundesgerichtshof gegebene Definition der Technik durchaus
   aktuell.
   
   Der Bundesgerichtshof hat sich f�r die Beurteilung des technischen
   Charakters in erster Linie an seiner Definition, d.h. am Begriff der
   Technik orientiert, und nicht an der Negativliste (vgl. beispielsweise
   die Entscheidung "chinesische Schriftzeichen"4). Es wurde immer wieder
   betont, da� Computerprogramme keineswegs generell nicht patentf�hig
   seien2, allerdings m�sse die Verwendung technischer Mittel Bestandteil
   der Probleml�sung sein.
   
   Bei einem Patent ist die Probleml�sung in der Gesamtheit der Merkmale
   des Hauptanspruchs zu sehen. Dies ergibt sich aus der Rechtsprechung
   des Bundesgerichtshofs zur erfinderischen T�tigkeit ("Kreiselegge"6)
   und sollte entsprechend dann auch bei der Beurteilung des technischen
   Charakters gelten. Selbstverst�ndlich sind zur Beurteilung des
   technischen Charakters nur technische Merkmale im Sinne der Definition
   von Technik in Betracht zu ziehen. Eine Differenzbildung nach
   gegen�ber dem Stand der Technik bekannten und neuen Merkmalen ist
   dagegen nicht sinnvoll, da alle Merkmale des Anspruchs zur L�sung und
   damit auch zum Charakter der L�sung beitragen.
   
   Der Bundesgerichtshof hat in der Vergangenheit zwischen technischen
   und nicht-technischen Programmen f�r Datenverarbeitungsanlagen
   unterschieden. Entsprechend "ABS"2 werden Programme als technisch
   bezeichnet, die insbes. Proze�- oder Me�werte unmittelbar ( in
   Echtzeit) verarbeiten und zur Anzeige bringen oder weitere Prozesse
   damit ansteuern (wie dies bei "ABS"2 selbst, oder auch
   "Tauchcomputer"5 der Fall ist). Bei diesen hat der Bundesgerichtshof -
   entsprechend den vorstehenden Ausf�hrungen - eine Differenzbildung
   nach bekannten und neuen Merkmalen abgelehnt und ihren technischen
   Charakter generell bejaht. Speziell die Entscheidung "Tauchcomputer"5
   ist so als Best�tigung und Fortsetzung dieser Linie zu sehen und nicht
   als Korrektur zu Entscheidungen im Bereich der "nicht-technischen"
   Programme.
   
   Bei diesen verlangt der Bundesgerichtshof f�r eine positive Bewertung
   des technischen Charakters, da� sie entweder einen neuen und
   erfinderischen Aufbau oder eine neue und erfinderische Anwendung einer
   Datenverarbeitungsanlage erm�glichen (vgl. "Dispositionsprogramm"1).
   Um dies festzustellen wird in der Regel die Differenz zum Stand der
   Technik betrachtet (auch wenn dies nicht ausdr�cklich so -oder gar mit
   dem Wort "Kerntheorie" - bezeichnet wird). F�hrt die programm-bezogene
   Lehre zu einem neuen und erfinderischen Aufbau der
   Datenverarbeitungsanlage, so wird der technische Charakter bejaht
   (Beispiel "Seitenpuffer"3, sowie viele weitere ungenannte und
   unproblematische F�lle aus dem Bereich der Festplattensteuerung,
   Cache-Ansteuerung, generell aus dem Bereich der Betriebssysteme). Wird
   dagegen von der Datenverarbeitungsanlage nur bestimmungsgem��er
   Gebrauch gemacht, also ihr Auf-
   
   bau nicht in neuer und erfinderischer Weise ver�ndert, so wird das (in
   der Regel als Verfahrensablauf oder als Vorrichtung mit entsprechenden
   Wirkungsangaben beschriebene) Programm als nicht technisch beurteilt.
   Besonders, wenn die Verfahrensschritte oder Wirkungen gedankliche oder
   gesch�ftlichen T�tigkeiten wiedergeben, generell Bereiche aus der
   "Negativliste" des � l PatG angesprochen sind, also z.B. im
   Zusammenhang mit Spracherkennung und -�bersetzung, mit Texterkennung
   und -Verarbeitung, schlie�t der Bundesgerichtshof aus der
   bestimmungsgem��en und damit ohne erfinderische Leistung erfolgenden
   Nutzung der Datenverarbeitungsanlage auf mangelnden technischen
   Charakter (vgl. "chinesische Schriftzeichen4). Dabei spielt es im
   Ergebnis keine Rolle, da� die so beschriebenen Gegenst�nde wegen der
   maschinell erfolgenden Abarbeitung dieser Schritte durchaus der
   Technik-Definition des Bundesgerichtshofs2 entsprechen. Tats�chlich
   fehlt es den Gegenst�nden aus dem Bereich der nicht-technischen
   Programme in den genannten F�llen nicht am technischen Charakter
   sondern an der erfinderischen T�tigkeit.
   
   Die weitere Modifikation des Kriteriums f�r das Vorliegen von
   technischem Charakter nach "Seitenpuffer"3 dahingehend, da� eine
   unmittelbare Einwirkung auf den Rechner erforderlich sei, ist
   zumindest interpretationsbed�rftig. Denn unmittelbare Einwirkungen in
   diesem Sinne bei Anwenderprogrammen aller Art sind �blich, ohne da�
   der Rechner dadurch einen neuen und erfinderischer Aufbau oder eine
   neue und erfinderische Wirkungsweise zeigen mu�, beispielsweise durch
   sog. DMA- (Direct Memory Access-) Zugriffe.
   
   Die Beschwerdekammern des EPA sind in diesen Bereichen bei �hnlicher
   Systematik - es wird ein technischer Beitrag gegen�ber dem Stand der
   Technik verlangt - weniger restriktiv als der Bundesgerichtshof (und
   z.T. das BPatG, vgl. "Sohei"7, ein Buchhaltungsprogramm, sowie zu Pro-
   grammen im Zusammenhang mit Sprachanalyse). Dies hat in j�ngster Zeit
   im Hinblick auf die anzustrebende Harmonisierung wieder zur Zulassung
   von Rechtsbeschwerden gef�hrt.
   
   Auch nach den Pr�fungsrichtlinien des EPA soll die Beurteilung des
   technischen Charakters durch Ermittlung des technischen Beitrags der
   im Anspruch gegebenen Lehre gegen�ber dem Stand der Technik erfolgen,
   wobei der Anspruch als ganzes zu bewerten ist. Dies kann in der Regel
   nur durch Vergleich mit dem ermittelten Stand der Technik erfolgen.
   
   Diese Vorgehensweise ist aus folgenden Gr�nden unbefriedigend:
   
   a) Die negative Beurteilung des technischen Charakters ist im
   Einzelfall schwer nachvollziehbar, da der entsprechende Stand der
   Technik bei in der Begr�ndung in der Regel nicht spezifiziert wird,
   vgl. z.B. "Schriftzeichenform"8.
   
   b) �blicherweise zeigt das Rechercheergebnis eine individuell und
   strukturell bedingte Streuung. Dies ist im Zusammenhang mit Neuheit
   und erfinderischer T�tigkeit zwangsl�ufig hinzunehmen, da beide
   gegen�ber dem Stand der Technik definiert sind. Der Begriff der
   Technik dagegen ist ersichtlich objektiv und allgemein definierbar
   (vgl. die eingangs zitierte Definition des Bundesgerichtshofs2, damit
   sollte auch die Feststellung des technischen Charakters aus der
   Bewertung der Merkmale des Anspruchs in ihrer
   
   6 BGH GRUR 1984, 194 - Kreiselegge
   
   7 EPA Mitt. 1996, 178 - Sohei
   
   8 ABI. EPA 1991, 566 - Schriftzeichenform
   
   Gesamtheit und ohne Bezug auf einen konkret ermittelten Stand der
   Technik erfolgen k�nnen.
   
   c) Dementsprechend f�hrt diese Vorgehensweise zu fragw�rdigen
   Ergebnissen, da so auch Gegenst�nde4'8 als nicht technisch und damit
   nicht patentf�hig bewertet werden, die erkennbar der
   Technik-Definition, beispielsweise des Bundesgerichtshofs entsprechen.
   
   d) Schlie�lich ist nicht zu �bersehen, da� die beschriebene
   Vorgehensweise bei der Ermittlung des technischen Beitrags zum Stand
   der Technik einen weiten Spielraum in der Handhabung l��t. Bei enger
   Betrachtung kann sie zu einem mit der - als nicht haltbar bezeichneten
   - "Kerntheorie" vergleichbaren Ergebnis f�hren. Bei gro�z�giger
   Handhabung kommt es zu unterschiedlichen Ergebnissen f�r inhaltlich
   vergleichbare Gegenst�nde, wie Mellulis9 anhand der Rechtsprechung der
   Beschwerdekammern des Europ�ischen Patentamts dargelegt hat.
   
   Die dadurch gegebene hohe Unberechenbarkeit in einem zentralen Punkt
   der Patentf�higkeit wird von den Anmeldern zu Recht als nicht
   nachvollziehbar und damit als willk�rlich empfunden. Dies f�hrt zu
   einer permanenten Infragestellung und damit Schw�chung des Systems des
   gewerblichen Rechtsschutzes insgesamt. Daraus ist zu schlie�en, da�
   die oben dargestellte Methode zur Ermittlung eines "Programms als
   solchem" f�r die praktische Anwendung nicht geeignet ist.
   
   Grunds�tzlich unterbewertet m.E. die Entscheidung des
   Bundesgerichtshofs und der Beschwerdekammern des Europ�ischen
   Patentamtes "chinesische Schriftzeichen" bzw. "Schriftzeichenform"
   und zu vergleichbaren Gegenst�nden aus dem Bereich der Wiedergabe
   intellektueller T�tigkeiten den Gesichtspunkt, da� durch die entspre-
   chende programm-bezogene Lehre nicht eine geistige T�tigkeit unter
   Schutz gestellt werden soll, sondern ein Produkt, das mit Hilfe der
   Maschine Computer eine bestimmte Funktion maschinell bewirkt. Da� die
   Funktionen auch Bereiche erfassen, die bisher im Bereich pers�nlicher,
   auch sog. geistiger F�higkeiten lagen (z.B. Spracherkennung und
   �bersetzung), ist in der Entwicklung der Technik, speziell der
   Informationstechnik begr�ndet, die durch Darstellung der Information
   in maschinell verarbeitbarer Form (d.h. in Form von Daten) diese
   Bereiche der maschinellen Verarbeitung zug�nglich gemacht hat.
   Vergleichbar hat im Mittelalter die Druckerpresse die Vervielf�ltigung
   von Schriften von einem individuell-k�nstlerischen (also
   untechnischen) zu einem maschinell-reproduktiven, also technischen
   Vorgang gemacht. Der Computer denkt nicht sch�pferisch, sondern
   reproduziert (maschinell) Information auf der Grundlage und im Umfang
   der bereitgestellten Funktionen.
   
   Am Beispiel "chinesische Schriftzeichen": Es soll nicht die geistige
   T�tigkeit bei der Gestaltung des Schriftbildes nach bestimmten Regeln
   gesch�tzt werden, sondern - ganz im Sinne des "Turing-Prinzips" - die
   maschinelle Wiedergabe dieses Vorgangs. Die Umsetzung der Lehre mit
   einer Darstellung des beanspruchten Gegenstandes unter impliziter
   Wiedergabe der Regel mag f�r den Fachmann im Einzelfall ohne
   erfinderische T�tigkeit m�glich sein, technisch (im Sinne der
   BGH-Definition) ist ein derartiger Vorgang ebenso wie die maschinelle
   Herstellung einer Reproduktion. Daher ist die BGH-Entscheidung
   "chinesische Schriftzeichen" und die ihr zugrundeliegende Systematik
   nicht unbedingt zu verallgemeinern.
   
   Um einseitige Interpretationen bei der Anwendung der Rechtsprechung zu
   Computer-Programmen zu vermeiden, hat das Deutsche Patentamt 1995
   einheitlichen Kriterien f�r die Patentpr�fung geschaffen. Punkt 4.3
   der Pr�fungsricht-linien befa�t sich speziell mit
   software-bezogenen Anmeldungen. 
   Bei der Beurteilung des technischen Charakters ist der
   Anspruchgsgegenstand als Ganzes zu beurteilen, ohne Trennung der
   Merkmale durch den Stand der Technik. Denn bei Vorliegen einer
   technischen Funktion oder Wirkung ist es f�r den technischen Charakter
   unerheblich, ob ein aus dem Stand der Technik bekannter Gegenstand die
   gleiche oder eine �hnliche Funktion oder Wirkung ausl�st. Eine
   ganzheitliche Beurteilung des Anspruchs und im Zweifel bei
   Geltendmachung guter Gr�nde eine positive Bewertung des technischen
   Charakters sind vorgegeben. Diese Richtlinien stellen f�r die
   Mitglieder und Abteilungen des Deutschen Patentamts eine
   Verwaltungsvorschrift dar, von der bei Vorliegen sachlicher Gr�nde im
   Einzelfall abgewichen werden kann.
   
   Diese Richtlinien erm�glichen dem Deutschen Patentamt eine an den
   praktischen Bed�rfnissen der Anmelder orientierte Pr�fung.
   Vergleichsweise werden bei den j�hrlich in der f�r Anmeldungen aus dem
   Bereich der Datenverarbeitung zust�ndigen Patentabteilung 53 des
   Deutschen Patentamts eingehenden ca. 1500 Pr�fungsantr�gen nur ca. 5
   bis 10 Anmeldungen wegen fehlenden technischen Charakters
   zur�ckgewiesen. Das Problem der Patentierung von Computerprogrammen
   ist daher zun�chst theoretischer Natur, zumal die so zur�ckgewiesenen
   oft auch an mangelnder erfinderischer T�tigkeit scheitern w�rden, wie
   z.B. im Falle "Tauchcomputer". Eine Streichung der Negativliste des �
   l PatG ist sonach nicht zielf�hrend. Zus�tzlich verhindert das Ma� der
   Anforderung an die erfinderische T�tigkeit in Deutschland wie auch in
   Europa eine an den USA oder an Japan orientierte Praxis bei der
   Patentierung programmbezogener Anmeldungen. Viele der beispielsweise
   in der Praxis des US-Patentamts erteilten software-bezogenen Patente,
   z.B. aus dem Bereich der "Gesch�ftsideen", ins besondere im
   Zusammenhang mit dem Internet, blieben deshalb im europ�ischen Bereich
   auch bei Streichung der Negativliste ohne Erfolg. Trotzdem wurden im
   Entwurf f�r die �nderung des Gemeinschaftspatentgesetzes die Programme
   f�r Datenverarbeitungsanlagen aus der Negativliste gestrichen.
   
   Die in deutscher Sprache beschriebenen technischen Funktionen und
   Wirkungen von Software sind jedenfalls in der Regel unter den �blichen
   Voraussetzungen (gewerbliche Anwendbarkeit, Neuheit, erfinderische
   Leistung) auch jetzt schon in Deutschland patentf�hig. F�r den Bereich
   des Europ�ischen Patent�bereinkommens gilt dies entsprechend. Damit
   ist � 27, Abs. l des TRIPS-Abkommens Rechnung getragen, wonach
   Patentschutz f�r alle Gebiete der Technik zu erlangen sein mu�.
   
   3. Zum "Programm als solches"
   
   Der Negativkatalog nach � l Abs. 2 und 3 PatG - entsprechend EP� Art.
   52 Abs. 2 und 3 - schlie�t - neben den wissenschaftlichen,
   k�nstlerischen und wirtschaftlichen Aktivit�ten unter anderem nur
   "Programme als solche" ausdr�cklich vom Patentschutz aus. Entscheidend
   f�r die Konkretisierung dieses abstrakten Begriffs ist seine
   Interpretation im Gesamtzusammenhang der nach � l Abs. 2 und 3 PatG
   ausgeschlossenen Gegenst�nde, da durch diese, insbes. auch durch das
   "Programm als solches" die Abgrenzung zum Urheberrecht (und andere
   Formen des gewerblichen Rechtsschutzes) hergestellt werden soll.
   Gemeinsamer Bezug aller im Negativkatalog genannten Gegenst�nde ist
   das Fehlen einer technischen Lehre. Wie in Melullis9 aus-
   
   f�hrlich darlegt, soll unter einem "Programm als solchem" der einem
   Programm zugrundeliegende, von einer technischen Funktion noch freie
   Programminhalt verstanden werden, also die der Umsetzung in eine
   Handlungsanweisung an einen Rechner vorausgehende Konzeption.
   Hergeleitet wird dieses Ergebnis durch die Einordnung der Programme
   f�r Datenverarbeitungsanlagen in den Kontext der Negativliste des
   Patentgesetzes und die Interpretation der ihr zugrundeliegenden
   Systematik, n�mlich der vom Gesetzgeber gewollten Freiheit der
   geistigen Sch�pfung. Zus�tzlich wird aus der Formulierung des � l PatG
   bzw. Art. 52 EPU geschlossen, da� Technizit�t nicht unbedingt zur
   Patentf�higkeit gen�ge, da� sie also zwar notwendige, nicht aber
   hinreichende Voraussetzung f�r die Patentf�higkeit sei. Hinreichend -
   bei ansonsten gegebenen Voraussetzungen (gewerbliche Anwendbarkeit,
   Neuheit, erfinderische T�tigkeit) - sei das Nichtvorhandensein einer
   geistigen Sch�pfung, wie immer dies im Einzelfall zu interpretieren
   ist.
   
   Diese systematische und in sich schl�ssige Erkl�rung ist jedoch in der
   Praxis schwer anwendbar, da sie einen abstrakten Begriff ("Programm
   als solches") durch andere ("der von der technischen Funktion noch
   freie Programminhalt", Nicht-Vorhandensein einer geistigen Sch�pfung")
   ersetzt. Will man diesen Begriff konkretisieren, so ergibt der Ver-
   gleich mit den verschiedenen Formen des geistigen Eigentums, da�
   geistige Sch�pfungen durch das Urheberrecht zu sch�tzen sind.
   Gegenstand des Urheberrechts im Bereich der Software ist ausdr�cklich
   der Quellcode.
   
   Sonach kann jedenfalls der Quellcode, die Beschreibung der
   Vereinbarungen und Ablaufschritte eines Programms auf einem beliebigen
   Tr�ger, jedoch ohne funktionelle Kopplung an eine
   Datenverarbeitungsanlage als ein "Programm als solches" angesprochen
   und damit vom Patentschutz ausgeschlossen werden. Die mit dem Programm
   verbundenen und �ber die Datenverarbeitungsanlage erzielbaren
   Funktionen und Wirkungen stellen dagegen technische Merkmale dar, die
   dem Patentschutz bei Vorliegen der formalen (Beschreibung in einer
   Amtssprache) und der �blichen sachlichen Voraussetzungen (gewerbliche
   Anwendbarkeit, Neuheit, erfinderische T�tigkeit) zug�nglich sind.
   Damit ist jedenfalls der Bereich vom Patentschutz ausgenommen, f�r den
   der Gesetzgeber eine andere Schutzform, n�mlich das Urheberrecht,
   vorgesehen hat. Die funktionelle Kopplung an die
   Datenverarbeitungsanlage, z.B. spezielle, eine Wirkung auf dem Rechner
   ausl�sende Treiberprogramme, sind technischer Natur und bei Erf�llung
   der gesetzlichen Voraussetzungen (gewerbliche Anwendbarkeit, Neuheit,
   erfinderische T�tigkeit) auch patentf�hig.
   
   Keinesfalls sollen also rein beschreibende Darstellungen, z. B. ein
   Programm in einer h�heren Programmiersprache, aber auch die z.B. auf
   einer CD-ROM gespeicherte Bildsequenzen oder Filme dem Patentschutz
   zug�nglich sein.
   
   Diese Definition ist, wenngleich nicht systematisch abschlie�end, so
   doch f�r die Anmelder hinreichend deutlich zu erfassen und stellt
   gegen�ber dem ebenfalls f�r den Schutz von Programmen in Betracht
   kommenden und den Quellcode sch�tzenden Urheberrecht eine in der
   Praxis leicht zu handhabende Abgrenzung dar. Schr�nkt man die in � l
   Abs. 2 PatG angesprochenen Programme f�r Datenverarbeitungsanlagen als
   solche auf diese Definition ein, so sollten nach Wahl des Anmelders
   sowohl der Quellcode durch das Urheberrecht wie auch gegebenenfalls
   technische
   
   Gesichtspunkte durch ein darauf gerichtetes Patent ad�quat zu sch�tzen
   sein.
   
   Diese Betrachtung ist auch aus der Sicht der Koexistenz der Formen des
   gewerblichen Rechtsschutzes vertr�glich:
   
   Der Schutz der formalen Beschreibung einer auf dem Datentr�ger
   gespeicherten Rechneranweisung mit allen f�r den Ablauf erforderlichen
   Vereinbarungen erfolgt �ber das Urheberrecht. Das Patent sch�tzt das
   funktionale Prinzip einer Vorrichtung oder eines Verfahrens, d.h. den
   Gegenstand, der die beschriebene Funktion bewirkt.
   
   4. Zur "Produktkategorie"
   
   Die in den Richtlinien als "Produktkategorie" bezeichnete Anspruchsart
   ist offenbar an den vom US-PTO erteilten "Lowry"-Anspruch10 angelehnt:
   
   "Speicherelement zur Speicherung von Daten f�r den Zugriff durch ein
   auf einer Datenverarbeitungseinheit ausgef�hrtes Anwendungsprogramm,
   umfassend ...... (es
   
          folgen Verfahrens- oder Wirkungsangaben einer programmbezogenen
                                                              Erfindung)"
   
   Dabei werden die Verfahrensmerkmale in ihrem Ablauf oder in ihrer
   Wirkung beschrieben und als Datenobjekte eines Speicherelements, also
   einer Vorrichtung dargestellt, wobei eine Datenverarbeitungsanlage
   �ber ein Anwenderprogramm auf die Daten des Speicherelementes zugreift
   oder selbst auf dem Speicherelement gespeichert ist. Besser erscheint
   mir daher die Formulierung:
   
   "Datentr�ger (oder Speicherelement) zur Ausf�hrung des Verfahrens nach
                  Anspruch nn mit einer Datenverarbeitungsanlage, dadurch
            gekennzeichnet, da� die Verfahrensschritte ... in ein auf dem
                     Datentr�ger gespeichertes Programm integriert sind".
   
   Durch den so formulierten Vorrichtungsanspruch soll die Durchsetzung
   eines �ber Verfahrensschritte beschriebenen, softwarebezogenen
   Programms erleichtert werden, da der Vertrieb von Disketten oder CD's
   leichter zu unterbinden ist als ein abstrakter Verfahrensablauf.
   
   Gegen die Anerkennung eines derartigen formulierten Patentbegehrens
   bestehen folgende Einw�nde:
   
   a) Die Formulierung eines Anspruchs als "Datentr�ger oder
   Speicherelement auf dem ein Programm mit den Verfahrensschritten gem��
   dem Anspruch nn aufgezeichnet ist..." kann wegen der Mehrdeutigkeit
   des Begriffs "Programm" auch als die Speicherung des Quellcodes auf
   dem Datentr�ger und damit als "Programm als solches" verstanden werden
   und so unter den Ausschlu� von der Patentierbarkeit fallen.
   
   b) Weiterhin stellt sich die Frage des Rechtsschutzbed�rfnisses eines
   derartigen, in der Formulierung ansonsten mit einem Verfahrensanspruch
   �bereinstimmenden Nebenanspruchs.
   
   c) Schlie�lich wird das Speicherelement durch das Programm nicht
   weitergebildet sondern nur bestimmungsgem�� benutzt, wie etwa eine in
   eine Flasche gef�llte Fl�ssigkeit durch diese in der Regel (d.h. wenn
   nicht konstruktive Merkmale angesprochen oder damit umschrieben sind)
   nicht weitergebildet, sondern nur bestimmungsgem�� genutzt wird.
   Demgem�� wird allenfalls das Kriterium der Neuheit, nicht das der
   erfinderischen T�tigkeit erf�llt. Die L�sungsmerkmale eines derartigen
   Anspruchs stellen keine
   
   9 Melullis, GRUR 1998, 843 ff.
   
   10 Mitt. 1996, 48 - in re Lowry
   
   Ausgestaltung des Speicherelements, sondern vorteilhafte
   Weiterbildungen eines allgemeinen Verfahrens dar.
   
   Insbesondere der letzte Einwand verdient m.E. Beachtung. Im Ergebnis
   soll nicht das Programm als solches (entsprechend der unter 3.
   gegebenen Darstellung), also der Quellcode auf dem Speicher gesch�tzt
   sein, sondern Verfahrensschritte nach dem Gegenstand des
   Hauptanspruchs mit ihrer Einbindung in ein auf dem Datentr�ger gespei-
   chertes Programm. Die Patentf�higkeit ruht damit wesentlich auf den
   Merkmalen des Hauptanspruchs. Es bleibt die Frage, ob ein auf den
   Datentr�ger mit den in ein Programm eingebetteten Verfahrensschritten
   gerichteter Anspruch eine unabh�ngige L�sung zur zugrundeliegenden
   Aufgabe oder nur eine vorteilhafte Ma�nahme darstellt, ob es sich also
   tats�chlich also um einen Neben- oder nur um einen Unteranspruch
   handelt. Es spricht gegen das Vorliegen einer unabh�ngigen L�sung und
   damit eine Bewertung als Nebenanspruch, wenn das Speicherelement - wie
   in der Regel gegeben - nur bestimmungsgem�� genutzt wird und die
   Patentf�higkeit allein von den Merkmalen des Hauptanspruchs getragen
   wird. Zwar hat das BPatG bereits "unechte" Nebenanspr�che, d.h.
   Nebenanspr�che, deren L�sungsmerkmale mit denen des Hauptanspruchs
   �bereinstimmen, zugelassen11, jedoch betraf dieser Fall ein Verfahren
   und den unmittelbar mit dem Verfahren hergestellten Gegenstand, der
   ohnehin mit dem Verfahren gesch�tzt war.
   
   Will man umgekehrt dem auf dem Datentr�ger gespeicherten Programm den
   Charakter einer eigenst�ndigen L�sung �ber die Wiedergabe der
   Verfahrensschritte hinaus und damit die Bewertung als Nebenanspruch
   zuerkennen, so kommt man nicht an der Tatsache vorbei, da� diese
   gegen�ber dem allgemeinen Verfahren eigenst�ndige L�sung in nicht
   selbstverst�ndlichen Merkmalen bestehen mu�, die beim konkreten
   Zusammenwirken der Programmteile im Rechner und auf dem Datentr�ger
   entstehen.
   
   Da "Produktanspr�che" auch unter den Anmeldern durchaus umstritten
   sind, bleibt ihre abschlie�ende rechtliche Beurteilung bis zum
   Vorliegen einer h�chstrichterlichen Rechtsprechung im Einzelfall
   unsicher. In diesem Zusammenhang kann eine Streichung der Negativliste
   des � l, speziell der "Programme f�r Datenverarbeitungsanlagen als
   solche" eine positiven Beurteilung derartiger Anspr�che jedenfalls
   erleichtern. Auch die zu erwartende EG-Richtlinie �ber den rechtlichen
   Schutz softwarebezogener Erfindungen k�nnte schon eine Kl�rung
   herbeif�hren.
   
   Das Rechtsschutzbed�rfnis f�r einen derartigen Anspruch im Hinblick
   auf die bessere Durchsetzbarkeit des "Datentr�ger"- oder
   "Produkt-Anspruchs k�nnte bejaht werden wegen der besseren
   Durchsetzbarkeit12. Ein entsprechender Fall liegt derzeit dem BPatG
   vor.
   
   'Zusammenfassung
   
   1. Anmeldungen, die auf einem Rechner ausgef�hrte Verfahren oder
   Verfahrensschritte beinhalten, sind als sog. "programmbezogene
   Erfindungen" unter den �blichen Voraussetzungen (gewerbliche
   Anwendbarkeit, Neuheit, erfinderischer T�tigkeit) sowie der formalen
   Anforderungen (Beschreibung in einer Amtssprache) patentierbar, dabei
   mu� die Verwendung technischer Mittel Bestandteil der Probleml�sung
   sein, d.h. eines entsprechenden Hauptanspruchs. Dieser ist bez�glich
   des technischen Charakters als Ganzes, d.h. mit allen technischen
   Merkmalen, zu pr�fen.
   
   2. Die Darstellung von Programmen in h�herer Programmiersprache oder
   Maschinensprache kann als "Programm als solches" verstanden werden und
   ist im Hinblick auf die Bedeutung der formalen Gesichtspunkte dieser
   Werkform, Gegenstand des Urheberrechts. Einer darauf gest�tzten
   Offenbarung im Sinne einer Patentanmeldung steht schon die bei einem
   Quellcode nicht vorliegende Beschreibung in einer Amtssprache
   entgegen. Eine weitergehende Analyse des Begriffs bleibt der
   richterlichen Beurteilung im Einzelfall vorbehalten.
   
   3. Die Formulierung der programmbezogenen Erfindung als
   Produktanspruch (Speicherelement mit Verfahrensschritten) erscheint
   unter den beschriebenen Voraussetzungen (die Verwendung technischer
   Mittel mu� Bestandteil der Probleml�sung sein) grunds�tzlich dem
   Patentschutz zug�nglich. Jedoch wird der Speicher durch das Programm
   in der Regel nicht weitergebildet, sondern nur bestimmungsgem��
   genutzt. Zus�tzlich stellt sich das Problem des
   Rechtsschutzinteresses. Eine Formulierung als Nebenanspruch, der auf
   einen die Erfindung beschreibenden Verfahrensanspruch r�ckbezogen ist,
   ist m�glich. Bei rechtlicher �berpr�fung k�nnte dieser nur als
   Unteranspruch zu beurteilen sein. Die rechtliche �berpr�fung eines
   derartigen Anspruchs steht noch aus.
   
   11 BPatGE29,175
   
   12 BPatGE 29,177, abweichend BPatGE 33, 153
