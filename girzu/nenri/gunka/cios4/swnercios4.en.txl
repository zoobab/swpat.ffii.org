<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#tot: total

#Wnl: Patent Database Extensions and Application

#cnr: Project Managment, Process Rationalisation

#nwA: National Campaigns

#prn: Printed Materials

#oet: Presence at UN / WIPO / WSIS

#tWa: Benchmarking Conference Brussels

#Wo2: 3000 eur per month for 12 months

#fmn: Sustained Presence in Brussels/Strasburg

#Tot: Total

#hnt: other contributions

#Itt: OSI contribution

#Ite: Item

#nr1: Assuming an exchangerate of 1 eur = 1.23 usd

#rio: Our experience shows that we can win an equal amount of donations from our supporters.  Last year we received nearly 60,000 eur in small private donations (maximum single donation amount 3000).  We spent most of this leaving a surplus of nearly 30,000 which can be spent for the more basic projects related to the organisational infrastructure (social engineering, patent database).

#Wsn: Substantial financing will come from the OSI (Soros Foundation), whose representatives will participate in monitoring the execution of the plan

#Woh: Details of the project organisation structure are published at %(URL).

#eep: Project Partners and Sponsors

#own: account owner

#cuW: account nr

#Ban: bank name

#ead: bank's physical mail address

#aWd: Bank Code

#onr: country

#aAu: Bank Account

#jaa: currently 6000 eur, derived from member fees and occasional donations by software companies and foundations.

#naa: Financial Status

#men: help programmers, information-creating entrepreneurs and informationally literate citizens to gain a fair share of influence on information policy decisionmaking

#0: protect the creator against the plagiator and the public against monopolies, i.e. promote an equitable system of property in software

#kWb: support work to make general-interest information ressources publicly accessible

#iso: Mission

#hdt: near the European and German Patent Offices

#0ng: DE 80636 München Blutenburgstr 17

#ylr: Physical Address

#syt: tax-exempt status granted and regularly checked by Munich corporate taxation office

#eei: registered in Association Register of Munich District Court

#eWc: governed on the basis of german association law

#0pe: 3600 supporters

#8mb: 180 members

#xko: tax-exempted non-profit association

#eWt: Legal Status

#oii: position

#sbr: responsible person

#Nam: Responsible Organisation

#dan: We need people regularly working on this.  The costs can be estimated as similar to those estimated for the %(q:social engineering) work above.

#eWh: This work helps us gain competence in patent policy benchmarking and helps us address the problems of many people in the community of potential patent infringers.  It also forms the basis of our participation in the research community and our role as a co-organiser of conferences and surveys which involve this community.

#wtv: Currently the only database which provides data about the category of %(q:software patents) at the EPO is the one which we provide.  The categorisation and the involvement of the public must be refined.

#WWb: We need to be able to provide more accurate information about the current status of the patents in various fields granted by the EPO and elsewhere.

#or0: The work would be done in the form of project tenders and bids, but the most likely result will be that Michael will receive an amount that allows him to concentrate fully on this work until summer 2004, i.e. a monthly 2000 eur for about 8 months.

#srW: The task of coordinating a movement of more than 50,000 registered supporters require several people's full-time work.  Processes need to be rationalised and programmed in order to allow more to be achieved by fewer people.  Quite good work has already been achieved by Michael Wasmeier, who programmed the aktiv.ffii.org participation system web interface, and further plans are waiting to be implemented.  The rudiments of a simple but powerful project coordination and knowledge managment system are already in place but need to be brought to our constituents.

#ite: These meetings require specialised volunteers who need to be compensated to some extent, both at the meetings themselves and for preparation of printed materials and establishment of contacts.

#ocW2: At WIPO in Geneva several major treaties are under negotiation.  For us the %(st:Substantive Patent Law Treaty) is of particular importance.  We need to prepare materials that can be used by negotiators from third-world countries in order to block this treaty or, even better, bring counter-proposals into the negotiation process, as at the European Parliament, e.g. freedom of publication, interoperability privilege, limitation of patents to the material sphere.

#inr: The World Summit of the Information Society is a good forum for brining the concept of a free information infrastructure into the political arena.  FFII is accredited and is paying at least travel fees for 4 participants in Geneva these days, as well as for further meetings next year.

#nWW: The Parliamentary Evening in Berlin needs a budget of 4000 eur, others may be similar.  Much of the regular work can be handled by the national groups themselves, but such special events as a Parliamentary Evening need extra funding.

#vrt: Organise small-scale evening meetings with members of parliament and other people who should or might take an interest in national policy on the directive, cf %(DOK).

#iup: Write instruction manuals for our supporters.

#pro: We need people set free from their work who can carry out the following tasks:

#ahf: Their main aim is to wake up national parliamentarians and news media and find ways to bring the pressure of public opinion to bear on the patent administrators who are usually acting in a space that is far removed from any public accountability.  This may involve a campaign for resolutions or even legislation in their national parliaments.

#auj: Some well-functioning national workgroups have been formed, others will follow suite.

#paw: In order to involve our supporter community, we also need to be present with our distributable items and performances at various software-related events where this community comes together.

#glW: We also need to be permanently ready to organise an internet strike such as that of 2004-08-27 and to bring people to the streets.  The Council's expected anti-democratic decisionmaking will need such responses and due to the secrecy of their process it is difficult to know the date long in advance.

#sgt: Street performances such as those organised by Peter Gerwinski last year (Pantomime, Choir %(q:the thoughts are free)) will continue to be helpful.  Each such performance had to be recompensed by 1-2000 eur last year.

#ege: All Campaigns need support by printed materials and other distributable items.  These were very helpful in the EP last year.  Some of them can be quite expensive when printed in the needed quantities.  Also graphical designers and other people want to be recompensed.

#Wwe: This time we will be able to get more value for the money, so that probably a successful conference would be possible with 15,000.  However it will be desirable to invite a few more attendees from Eastern Europe (new member states) and from the US and other regions with experience or research in software patents.

#tBo: In May 2003 we spent nearly 20000 on a %(ep:2 day conference in Brussels).  See our treasurer's tentative %(fo:financial report) of 2003.

#en1: This needs a budget of 20000 eur.

#eae: Members of Parliament as well as other decisionmakers should be invited as soon as  possible.

#tnh: Last May our conference was attended by the director of the US Federal Trade Commission (FTC.gov), because this commission had conducted hearings on software patents.  Meanwhile their report is published and they will have even more to say on this subject that last time.  It may be of less importance this time to have a star guest such as Lawrence Lessig, but on the other hand the presences of US researchers such as Jim Bessen and Bob Hunt will be essential.

#fEe: It has been suggested to us by members of the European Parliament that a conference in April can help to reinforce the positions of the members of the European Parliament througout the reelection phase and show the Council that the Parliament will neither be fooled nor intimidated by pro-patent pressures in the second reading.

#ioW: During the pre-election period, the conference can also serve MEPs to publicise their positions and strenghten their support for these positions.

#ytW: We will probably cooperate with a university in conducting this conference.  Talks are being conducted and seem very promising.

#tWl: It is of special political importance, because some patent lawyers believe that only they are the real experts and uses this as a pretext for discrediting the work of the Parliament.  The limelight of an inter-disciplinary conference shows most convincingly that the interesting questions are being asked and answered in other fora.

#tWe: We want to assemble leading scientists, stakeholders and politicians to a conference in Brussels in April 2004, in order to benchmark the various directive proposals.

#Cos2: Costs

#hWW: What to do

#dmp: This adds up to a minimum of 3000 eur per month.

#Wtg: A weekly stay of an external representative costs an average of 250 eur, of which 250 are the average flight cost, 250 for lodging, 250 for an allowance to cover further costs, including food and telephone.  We would like to assure a minimum of 2 man-weeks per month of external representatives.  Again, it has been crucial for our success that we were able to mobilise credible people from the less countries with a less developped patent movement, such as Portugal, Greece, Ireland.  In 2004 several East-European countries with longer travel routes will gain importance.

#W0W: Our permanent representative will cost 1500-2000 per month.  The most likely candidate for this function is Erik Josefsson from Sweden.  Erik has been very successful in networking in the European Parliament.  He stayed for several months before the vote in September and can be credited for much of the success.

#rtw: Our greatest strength lies in the large number of people who share our concerns.  One major task is to write a lobbying and election guide for volunteers, telling them what to consider when voting, whom to write to and how else to help, and to reach out to these volunteers all the time.  A major task of the paid permanent representative will be to make sure that this guide is written.  This will be something to work on whenever our representative is not  meeting politicians.

#hrW: Since the Parliament's decision has sent shock waves through the world of patent law departments and big business throughout the world, major large companies can be expected to pour large sums of money into the election campaign in order to punish those MEPs and parties who defended the public interest.  We are to some extent under a moral obligation to provide a counter-force.

#ocW: We need to work through our contacts to turn software patents and information society rights and the lack of democracy in IP legislation into an election campaign issue.

#dlW: Another subject could be to promote a charter of rights which must be assured if the information society is to be productive.   Some of these fundamental rights are embodied in the EP amendments (freedom of publication and interoperation, narrow and cautiously designed information property regimes), and ask these to be enshrined in a new directive or in the EU Consitution.  Also, we should highlight the undemocratic character of the two-chamber construction, in which governmental patent lawyers who are not directly accountable to any electorate can undo a decision of the more directly elected parliament.  These subjects may provide opportunities for setting election campaign themes and turning the defensive into an offensive in the year 2004.

#Wea: The expertise provided will be in the field not only of the software patent directive but also of the %(ip:IP enforcement directive) and other current or upcoming legislative matters as well as on the development of campaigns for securing the foundations of an open information society.  In these matters we can also cooperate with EDRI and other groups.

#Wnil: We need to continue having at least one permanent representative in Brussels/Strasburg who will %(ol|provide expertise to members of the European Parliament|prepare the ground for short stays of non-permanent representatives from all countries of Europe.)

#tpi: The European Parliament remains the main lever through which the Council can be influenced.   This time our presence in Brussels will serve to put pressure on the Council by strengthening the supportive forces in the Parliament.

#Cos: Costs

#tWs: What to do in Brussels

#Wmr: Although the Council more difficult to influence than the Parliament, it is not immune against genuine concerns of a large public, if these are organised in an effective way.  Here is what needs to be done to counter the expected backlash and make the remaining walls crumble in 2004.

#oaW: The Council is likely to work out a paper in spring or summer 2004 which the Parliament will face in a second reading in autumn 2004, immediately after the elections.

#ceW: Now the ball has been passed to the European Council of Ministers, an institution which has been pushing for unlimited patentability or, more recently, for fake limits on patentability.  The Council is traditionally the master of legislation in the EU, and even in the Co-Decision procedure it can force the parliament to submit to conciliation.  In the council, the matter is decided by a workgroup which consists solely of civil servants from the patent establishment, mostly civil servants from national patent offices who also sit on the administrative council of the European Patent Office and are responsible for the very policy of this office which was rebuked by the Parliament in september 2003.  In November 2002 the Council patent administrators produced a %(q:compromise paper) in advance, showing the Parliament what options it would have if it wanted to stay %(q:realistic).  The commissoner in charge at the European Commission was even more explicit: Frits Bolkestein warned the Parliament that it would %(q:ruin its chance of democratic participation) if it were to vote as it eventually did.

#nWd: The Parliament's amendments were of even greater significance.   They codified constitutional principles which secure the foundations of an open and productive information society:  the priority of the right of publication and the right of interoperation over any possible property claims.  Few anticipated that this would be possible, and it would probably not have happened, had there not been a concerted effort at raising awareness among MEPs and their constituents, which was made possible by a donation by OSI.

#tnq: In September 2003, the European Parliament voted for real limits on patentability and sent shock waves through the patent lawyer tyranny of the whole world, who spoke of a %(q:historical defeat), %(q:devastating consequences) etc and immediately called on the two other players in Europe's legislaton to %(q:throw the Parliament's proposal into the trash bin).

#hao: In 2003, a donation from OSI helped bring about another fall of a Berlin Wall: the wall of patents on purely informational entities, such as computer programs or mathematical methods etc, which embody no new knowledge about the workings of the forces of nature or, in Karl Popper's words, belong entirely to world 3 rather than to world 1.

#dWm: Budget Summary in USD

#pWa: People in charge of execution

#Wtp: Patent Database Maintenance and Applications

#nrl: Social Engineering

#eNm: Presence at UN / WSIS / WIPO

#tlb: National Campaigns

#dro: Printed Materials, Performances, Demonstrations

#rsa: Conference in Brussels in April 2004

#iWS: Campaign in Brussels/Strasburg

#descr: The ingredients needed for securing the foundations of an open and productive information society in the view of pending patent legislation in the EU.

#title: Computer-Implemented Open Society 2004

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnercios4 ;
# txtlang: en ;
# multlin: t ;
# End: ;

