<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#dan: We need people regularly working on this.  

#eWh: This work helps us gain competence in patent policy benchmarking and helps us address the problems of many people in the community of potential patent infringers.  It also helps improve our communication processes.

#WWb: We need to be able to provide more accurate information about the current status of the patents granted by the EPO and elsewhere.

#srW: The task of coordinating a movement of 60,000 supporters require several people's full-time work.  Processes need to be rationalised and programmed in order to allow more to be achieved by fewer people.  Quite good work has already been achieved by the aktiv.ffii.org participation system and further plans are waiting to be implemented.  The rudiments of a simple but powerful project coordination and knowledge managment system are already in place but need to be brought to our constituents.  Here again compensation of the core workers on a project tender and bid basis would help get things moving more quickly.

#nWW: The Parliamentary Evening in Berlin needs a budget of 4000 eur, others may be similar.

#vrt: Organise small-scale evening meetings with members of parliament and other people who should or might take an interest in national policy on the directive, cf %(DOK).

#iup: Write instruction manuals for our supporters.

#pro: We need people set free from their work who can carry out the following tasks:

#ahf: Their main aim is to wake up national parliamentarians and news media and find ways to bring the pressure of public opinion to bear on the patent administrators who are usually acting in a space that is far removed from any public accountability.  This may involve a campaign for resolutions or even legislation in their national parliaments.

#auj: Some well-functioning national workgroups have been formed, others will follow suite.

#ioW: During the pre-election period, the conference can also serve MEPs to publicise their positions and strenghten their support for these positions.

#ytW: We will most likely cooperate with a university in designing this conference.  Talks are being conducted and seem very promising.

#tWl: It is of special political importance, because some patent lawyers believe that only they are the real experts and uses this as a pretext for discrediting the work of the Parliament.  The limelight of an inter-disciplinary conference shows most convincingly that the real questions are being asked and answered in other fora.

#en1: This needs a budget of 5-10000 eur.

#eae: Members of Parliament as well as other decisionmakers should be invited as soon as possible.

#tWe: Assemble leading scientists, stakeholders and politicians to a conference in Brussels in April 2004, in order to benchmark the various directive proposals.

#rtw: Our greatest strength lies in the large number of people who share our concerns.  One major task is to write a lobbying and election guide for volunteers, telling them what to consider when voting, whom to write to and how else to help, and to reach out to these volunteers all the time.

#hrW: Since the Parliament's decision has sent shock waves through the world of patent law departments and big business throughout the world, major large companies can be expected to pour large sums of money into the election campaign in order to punish those MEPs and parties who defended the public interest.  We are to some extent under a moral obligation to provide a counter-force.

#W0W: This requires 3000 eur per month and 2 dedicated lobbyists who spend at least 1 week per month in the EU capitals and also work on the issue during the rest of the time.

#ocW: We need to work through our contacts to turn software patents and information society rights and the lack of democracy in IP legislation into an election campaign issue.

#dlW: Another subject could be to promote a charter of information society rights, such as those endorsed in the EP amendments (freedom of publication and interoperation, privacy and business secret, narrow and enforcable exclusivity rights), and ask these to be enshrined in a new directive or in the EU Consitution.  Also, we should highlight the undemocratic character of the two-chamber construction, in which governmental patent lawyers who are not directly accountable to any electorate can undo a decision of the more directly elected parliament.  These subjects may provide opportunities for turning the defensive into an offensive in the year 2004.

#Wea: The expertise provided will be in the field not only of the software patent directive but also of the %(ip:IP enforcement directive), the %(ns:European Network and Information Security Agency) and upcoming legislative matters.  In these matters we can also cooperate with other organisations to provide a joint infrastructure and network for our lobbyists.

#Wnil: We need to continue having at least one permanent representative providing expertise to members of the European Parliament and regular small-scale stays of external experts.

#Wmr: Although the Council more difficult to influence than the Parliament, it is not immune against genuine concerns of a large public, if these are organised in an effective way.  Here is what needs to be done to counter the expected backlash and make the remaining walls crumble in 2004.

#oaW: The Council is likely to work out a paper in spring or summer 2004 which the Parliament will face in a second reading in autumn 2004, immediately after the elections.

#ceW: Now the ball has been passed to the European Council of Ministers, an institution which has traditionally pushed for unlimited patentability or, more recently, for fake limits on patentability.  The Council is traditionally the master of legislation in the EU, and even in the Co-Decision procedure it can force the parliament to submit to arbitration.  In the council, the matter is decided by a workgroup which consists solely of civil servants from the patent establishment, mostly cadres of national patent offices who also sit on the administrative council of the European Patent Office and are responsible for the very policy of this office which has been rebuked by the Parliament.  In November 2002 the Council patent cadres produced a %(q:compromise paper) in advance, showing the Parliament what options it would have if it wanted to stay %(q:realistic).  The commissoner in charge at the European Commission was even more explicit: Frits Bolkestein warned the Parliament that it would %(q:ruin its chance of democratic participation) if it voted as it did.

#nWd: The Parliament's amendments were of even greater significance.   They codified decisions on matters of a constitutional level:  the priority of the right of publication and the right of interoperation over any possible property claims.  Few anticipated that this would be possible, and it would probably not have happened, had there not been a concerted effort at raising awareness among MEPs and their constituents, which was made possible by a donation by OSI.

#tnq: In September 2003, the European Parliament voted for real limits on patentability and sent shock waves through the patent lawyer tyranny of the whole world, who spoke of a %(q:historical defeat), %(q:devastating consequences) etc and immediately called on the two other players in Europe's legislaton to %(q:throw the Parliament's proposal into the trash bin).

#hao: In 2003, a donation from OSI brought about another fall of a Berlin Wall: the wall of patents on purely informational entities, such as computer programs or mathematical methods etc, which embody no new knowledge about the workings of the forces of nature or, in Karl Popper's words, belong entirely to world 3 rather than to world 1.

#Wtp: Patent Database Maintenance and Applications

#nrl: Coordination Work, Process Rationalisation

#tlb: National Campaigns

#rsa: Conference in Brussels in April 2004

#iWS: Sustained presence in Brussels/Strasburg

#descr: The ingredients needed for securing the foundations of an open and productive information society in the view of pending patent legislation in the EU.

#title: Computer-Implemented Open Society 2004

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swnercios4.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnercios4 ;
# txtlang: de ;
# multlin: t ;
# End: ;

