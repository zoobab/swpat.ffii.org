\contentsline {section}{\numberline {1}The Problem: Dangerous Patentability Legislation in Brussels}{2}{section.1}
\contentsline {section}{\numberline {2}Project Managers, Partners and Sponsors}{4}{section.2}
\contentsline {section}{\numberline {3}How we will organise the work}{5}{section.3}
\contentsline {section}{\numberline {4}Hierarchy of Action Items}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Work Meetings at National Parliaments}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Classified Directory of European Software Expertise}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Documentation and Getting People involved in it}{6}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Awareness-Raising with Special Industry Audiences}{7}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Influencing Jurisdiction by Exemplary Lawsuits}{8}{subsection.4.5}
\contentsline {section}{\numberline {5}Budget Summary}{8}{section.5}
\contentsline {section}{\numberline {6}Annotated Links}{9}{section.6}
