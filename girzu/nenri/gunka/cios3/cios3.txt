     _________________________________________________________________

                   Computer-Implemented Open Society 2003

   How we will halt imminent patent inflation legislation in 2003, who
   will do it, how the work will be divided, financed and monitored, and
   where we will go from there.

                        -> European Commission wants
                          Unlimited Patentability!
                           Directive Draft by BSA
                            -> Counter-Proposal
                             -> Call for Action

     * The Problem: Dangerous Patentability Legislation in Brussels
     * Project Managers, Partners and Sponsors
     * How we will organise the work
     * Hierarchy of Action Items
     * Budget Summary
     * Annotated Links

The Problem: Dangerous Patentability Legislation in Brussels

   Within the next few months, the EU patent system may either move
   toward a dark age of long-term agony, as currently experienced in the
   USA, where broad monopoly rights are granted for
   "computer-implemented" methods of organisation and calculation, or it
   may find back its place as a special-tailored property system for
   technical inventions, i.e. contributions to the art of harnessing the
   forces of nature.

   see Call for Action

   A heavy train, loaded with influential patent lawyers who have
   accumulated power in European governmental and industrial institutions
   for many years, is rolling in the direction of unlimited
   patentability. It is pushing a directive proposal for the
   "patentability of computer-implemented inventions", which, when
   translated from European Patent Office Newspeak to ordinary language,
   means "patentability of calculation rules, mathematical methods and
   business methods which run on the standard universal computer".

   see CEC & BSA 2002-02-20: proposal to make all useful ideas patentable

   The FFII and the Eurolinux Alliance have successfully engineered a
   groundswell of public opinion against software patents. 135,000 people
   have signed a petition and 400 software companies of all sizes and
   types have publicly supported this petition. Media opinion has been
   shifting from ignorance to positions much in our favor. Among numerous
   recent economic studies, none finds any positive impact of the patent
   system on the software economy. Politicians have also gradually heard
   the message and have to some extent begun to trust the expertise
   accumulated on our side. Due to their resistance, the patent lobby's
   original plans of deleting Art 52,2-3 from the European Patent
   Convention (EPC) and thereby removing all limits on patentability,
   were overturned in november 2000 and Art 52 was left untouched.
   Likewise, the European Commission's proposal to achieve the same
   through a directive has met large-scale public resistance, including
   an official rejection by the French government and amendment demands
   from most other governments.

   Yet, most of the political criticism is frustrated by the civil
   servants who actually handle the matter of patent politics. These
   civil servants are usually part of the patent establishment and
   aspirants for a career within the patent office or other institutions
   which depend on the expansion of the patent system and share a common
   set of beliefs and and trusted authorities. The officials in charge at
   the Commission of the European Community (CEC, European Commission)
   are patent lawyers from the Industrial Property Unit (indprop) at the
   General Directorate for the Internal Market (DG Markt). The CEC
   indprop lawyers view the European Patent Office (EPO) as the source of
   ultimate authority on patent matters and even regularly ask the EPO to
   represent them in the Council of the European Union (CEU). At the same
   time, the CEC wants to grab power from the EPO and become itself the
   master of the European patent system by means of the so-called
   European Community Patent (or European Union Patent). In order to
   overcome the resistance of the EPO, DG Markt has formed a symbiosis
   with the EPO. As one EPO official put it, "thanks to the proposed
   directive the EPO will finally receive the pilot's license without
   which it had been flying for years".

   Among the three pillars of the European Union's co-decision process,
   the patent policy at CEC and CEU is almost entirely dominated by
   patent lawyers. In the third pillar, the European Parliament, the
   patent lawyers are encountering more difficulties. While some MEPs
   have acted as their relay in the parliament, others have expressed
   support for the FFII/Eurolinux position or similar positions. If these
   MEPs can gain support of the majority, the proposal will be rejected
   or sent back for amendments.

   see Rocard/CULT 2002-12-09: Data Processing is Not a Field of
   Technology

   Whereever we have a chance to present our cause, we tend to be much
   more convincing than the other side. The problem is that we do need
   some time to explain the issue. The patent lawyers have enjoyed
   legislative power for many decades and complaints were not widely
   heard until recently. Moreover, the patent lawyers have to a large
   extent succeded in attributing these complaints to an alleged
   "opensource movement", e.g. a mysterious lobby of social romantics who
   are asking people to work without pay therefore may be a "vociferous
   group" but certainly not the "economic majority".

   Since there has traditionally been no separation betweeen patent
   jurisdiction and patent lawmaking, the discussions at all three
   pillars of the EU are largely dominated by quotations of caselaw from
   the EPO or other institutions which are treated by the lawyers like a
   source of ultimate wisdom. Although this lawyer-type discussion is
   full of fallacies and legally untenable, it is very exhausting to
   debate against lawyers at this level and very few people are available
   for such debates. Even the French government, which has strongly
   resisted the CEC proposal in the CEU even after the change from Jospin
   to Chirac, is showing signs of exhaustion. We have proposed that
   politicians should withdraw from this type of debate and instead work
   out a spec and test suite for patentability legislation and let the
   patent lawyers show whether their junktalk can meet the specified
   targets. We ourselves have worked out a counter-proposal which can
   meet all meaningful targets under the current legal constraints. We
   have done this in coordination with some MEPs. But talking to only a
   few is not enough. It is difficult to change long-entrenched habits,
   even habits of delegating legislative power to lawyers and allowing
   the tail to wag the dog.

   We have had some influence on the European Parliament's delegates
   thinking:

   see Europarl Hearings 2002-11-07 and 26

   After these two hearings, at least the Green Party seems to be
   completely and actively on our side, and the other parties are split
   in half. The rapporteur Arlene McCarthy has changed her position from
   complete hostility to partial support of our cause. Yet time is
   running short, and even if a more patent-critical position can
   dominate, it may still be frustrated by all kinds of confusion
   tactics, so that in effect the members of parliament may give their
   approval to a position which sounds as if it took freedom interest
   into account but in fact gives full power to the patent lobby to do
   what it wants. It would not be the first time that things have worked
   this way in the EuroParl. If this was the outcome, the freedom and
   productivity of the information society would be subjected to constant
   threats for decades to come and monopolism would be further reinforced
   in the software world. A thousand times the energy that is needed now
   might then be needed to reverse the trend.

Project Managers, Partners and Sponsors

   Responsible Organisation:
          Foundation for a Free Information Infrastructure

        responsible person:
                Hartmut PILCH <phm@ffii.org>

              position:
                      president (Vorsitzender)

        Legal Status:
                tax-exempted non-profit association, 180 members, 3600
                supporters, governed on the basis of german association
                law, registered in Association Register of Munich
                District Court, tax-exempt status granted and regularly
                checked by Munich corporate taxation office

        Physical Address:
                DE 80636 M�nchen Blutenburgstr 17 (near the European and
                German Patent Offices)

        phone:
                0049-89-1278960-8

        fax:
                0049-89-1278960-9

        Mission:

               o support work to make general-interest information
                 ressources publicly accessible
               o protect the creator against the plagiator and the public
                 against monopolies, i.e. promote an equitable system of
                 property in software
               o help programmers, information-creating entrepreneurs and
                 informationally literate citizens to gain a fair share
                 of influence on information policy decisionmaking

        Financial Status:
                currently 6000 eur, derived from member fees and
                occasional donations by software companies and
                foundations.

        Bank Account:

              country:
                      Germany

              Bank Code (BLZ):
                      70150000

              S.W.I.F.T.:
                      SSKM DE MM

              bank's physical mail address:
                      DE 80335 M�nchen Lotstr 1

              bank name:
                      Stadtsparkasse M�nchen

              account nr:
                      31112097

              account owner:
                      F�rderverein f�r eine Freie Informationelle
                      Infrastruktur e.V.

   Project Partners and Sponsors:
          Large parts of the project will be carried out by teams under
          various organisation names in european countries, in particular
          member organisations of the EuroLinux Alliance, which has
          accumulated experience in leading this campaign since the
          summer of 1999.

          Eurolinux Petition for a Software Patent Free Europe

          Moreover we are considering participation of research
          institutes in Amsterdam, Paris, Munich, Vienna and Maryland
          (US).

          An initial financing will come from the OSI (Soros Foundation),
          whose representatives will participate in monitoring the
          execution of the plan

          We will try to win additional financial support from other
          sources.

How we will organise the work

   Various individuals and Teams will sign agreements (written in a
   simple informal style) to comit themselves to achieving certain
   subtargets as laid out in the Action Plan below and in return receive
   monthly payments from the OSI grant.

   In very few cases we have to pay full expert salaries. In most cases
   we will merely help people who are already highly motivated to be able
   to spend their time without having to worry about subsistence. Models
   for this kind of support exist in the Free Software community and have
   been very successful. In other cases, especially where short-term work
   such as meetings in Brussels are concerned, we will not pay time but
   only expenses.

   Monthly progress reports will be written by these sub-contractors on a
   project managment website to which OSI has at least reading access.

   OSI will transfer an initial sum and additional monthly sums to an
   account of FFII, which will manage the project, including division of
   work and transfer of subcontractor fees.

Hierarchy of Action Items

   If the following lines are pursued energetically enough, we can bring
   the patentability legislation train to a halt within the next few
   months and later to a turning point in Europe and worldwide.

     * Regular Work Meetings at the EuroParl
     * Work Meetings at National Parliaments
     * Patent Documentation
     * Awareness-Raising with Special Industry Audiences
     * Classified Directory of European Software Expertise
     * Influencing Jurisdiction by Exemplary Lawsuits

Regular Work Meetings at the EuroParl

   What?:
          see Europarl 2003 --- Intensive Course in Patent Newspeak

          Meetings of this sort are needed once per month, mostly in
          Brussels, sometimes Strasbourg.

          The subject of the meetings is moving from awareness-raising to
          working out detailed counter-proposals. We are already working
          with parliamentary staff on various amendments and
          counter-proposals.

          The long-term goal should be to find other actors and
          institutionalise legislative monitoring of the patent system
          and related systems at the EU level.

   Who?:
          Previous meetings have been handled rather successfully and
          with great fervor by Fran�ois P�llegrini from ABUL.org and
          Laurence Van de Walle from the Green Party. Support in Brussels
          has been given by Nicolas P�ttiaux and his Belgian friends. It
          would be good if P�llegrini could continue to handle these
          meetings

   Costs:
          Monthly meetings require a budget for paying some travel
          expenses. Some people will have to come by plane. Some money
          can perhaps be raised from other sources, in particular from
          varying software companies who will hopefully send
          representatives to go with us every time. Also, once the
          planning is assured, partners at the Parliament are likely to
          organise complementary events such as hearings which will
          reinforce the success. The costs for assuring the basic
          planning, not including any compensation for invested time,
          will approximately be as follows:

   travel for 5 experts from various european locations 5x400 = 2000
   5 experts lodging in Brussels 5x200 = 1000
   invitation of 10 people (experts + europarl partners) for 1 working
   party or dinner in a hotel 50x10 = 500
   total 3500
   total for 6 months 6 x 3500 = 21000

Work Meetings at National Parliaments

   What?:
          In national capitals, parliamentary workgroups can be organised
          as well.

          The role of national parliaments is restricted to controlling
          their governments and asking them to take a more critical
          position in the Council of the European Union.

          Effective work can be done at least in NL, FR, DK and ES, to
          some extent also UK, SE, IT, GR and probably most countries.

   Who?:
          National member organisations of EuroLinux and others.

   Costs:
          A budget of 1000 per month could be allocated and used when
          national organisations have made credible plans.

Patent Documentation

   The software patent debate is still suffering from the poor
   accessibility of information on the real patents that are being
   granted by the EPO.

   Recently, the EPO itself has published a lot of raw data on the web,
   and FFII has brought it into a form where it is beginning to become
   useful for software professionals.

   see Software Patents of the European Patent Office and European
   Software Patent Statistics

   Most of all, those who legislate at the EuroParl need to be able to
   know what they are talking about and specify the rules in terms of
   output which is to be achieved (i.e. which kinds of patents do we
   want? which not?).

   see Patentability Legislation Benchmarking Test Suite

   Making European Patents available to the Public:
          So far the granted patents are available only as graphic files.
          This has kept people from gathering valid information about
          what is being patented. We have started to solve this problem
          through an OCR effort, which needs vast computation ressources
          and can only work if many people participate. Also the OCR
          software needs adaptation and fine-tuning in order to produce
          good results.

          We will soon begin distributing media (DVD, CD) with the
          collected patents on them at trade fairs. Hopefully the
          pressing of these media will be financed by the sale. The
          contents will be free and easy to access and modify. We hope
          that this will increase participation in the project.

   Integration of Websites with Zope CMF or similar:
          The current swpat.ffii.org website is rich in content but
          deficient in that it does not encourage people to participate.
          It depends to heavily on its creator Hartmut Pilch. Other
          websites such as patinfo.ffii.org are not built for sustainable
          development of great masses of documentation. An integration on
          a collaborative basis, where people of different qualities can
          more easily contribute, has begun but did not get far due to
          lack of time of the participants. A regularly paid Zope CMF
          site developper and manager is needed. We have offers of
          several Zope-related companies who are willing to do the work
          for a fairly low fee.

   Playful Methods: Patent-Sweeper Game:
          "Patent-Sweeper" or "Patent Adventure" etc are more or less
          straightforward games which can get people involved in looking
          at patent claims and finding out who might infringe on them.
          This can not only demystify the system and let many people
          intuitively grasp how bad software patents are, but it can also
          recruit new groups of people who help us build up our patent
          and patent infringement databases.

          In "Patent-Sweeper", people are handed out a kind of game-card
          with patent claims on it, drawn from our software patent
          database in a random fashion, and are asked to fill in a form
          which answers questions about this patent and to identify
          intersting infringers, such as government websites, and to
          other participants of the game, who have shown interest in
          certain fields of programming, in which then the patent mine
          shows up. It could be a kind of race across a minefield, where
          you try to move ahead by acquiring and exchanging patents,
          similar to the real-life patent game, but with the side effect
          that a patent database is gradually built up, and that news
          about example patents continuously emerge.

          Other game variants are conceivable. One was tried out by Peter
          Gerwinski (software entrepreneur and FFII PR manager) with a
          large audience at the CCC Congress in Berlin on 2002-12-28 and
          appeared quite successful.

   regular patent newsletter:
          This has already started and should improve further.

   Costs:
          All this work is expensive if a certain output is wished in a
          certain time.

          Items that may be needed include

         1. 1-5000 EUR for a bidder who will adapt and fine-tune the OCR
            software (Preferably free software such as clara or gocr is
            used. GOCR already recognises much of the text with very few
            errors, but it fails to identify text blocks in the numbered
            two-column mode used by EPO patents and therefore produces
            unusable text for many patents. CLARA is said to be made for
            fine-tuning and to have a scheme-based extension kit for this
            purpose, but we do not yet know whether it can work for us at
            all.)
         2. 2000 EUR for a RAID array of hard disks (This will form the
            backbone of a community effort, but is by itself still not
            enough for processing all the graphic files at reasonable
            speed. Consider that the EPO software patent graphics only
            consume about 500 GB, and the effort will, once taken, tend
            to be extended to national patents (of which many are not
            registered at the EPO and which are just as dangerous) and
            non-software patents.)
         3. 2000 EUR per month for 1 motivated specialist who will
            continuously improve the database and web applications. We
            already have 2-3 candidates who have shown ability and
            interests, and paying 2 people may lead to a triple result,
            assuming that they will tend to stimulate each other.

Awareness-Raising with Special Industry Audiences

   What?:
          We can, through professional press agencies, push well written
          articles into specialised news organs where they would
          otherwise have difficulty in making inroads, because trust
          relations have not yet been built up. Once the inroads are
          made, we will have the competence and energy to sustain them.

          This also requires the creation of some nice printed materials.

   Who?:
          Stefan Pollmeier and his company (40 employees) have been
          successful in pushing ahead some work of this type in the
          Electronics and Automation industry area. They will do more of
          it if certain expenses are paid.

          Peter Gerwinski, head of a small software company in Essen, has
          the ressources and motivation for working on printed materials.

   Costs:
          A booth at a special media event in Germany, where all the
          leading journalists of the automation and other IT areas come
          together and the chance of contacting and convincing them is
          very high, costs 2000 eur. Stefan Pollmeier will give some
          details.

          For continuously turning out good pamphlets and printed
          materials, it would be a good idea to support Peter Gerwinski
          with a monthly 1000 eur.

Classified Directory of European Software Expertise

   What?:
          The Eurolinux Alliance already has a lot of data about software
          expertise in Europe. These need to be further developped in a
          systematic manner. The result should be a free (GPDL) database
          of expertise in software, starting from certain fields of free
          software. Also the database and website should be free and
          sufficiently transparent.

   Who?:
          This work will be managed by Jean-Paul Smets and his company
          Nexedi, an expert in web-based enterprise ressource planning.
          Getting it going will require an initial ignition effort. Later
          people will most likely be attracted to add entries themselves.
          Further work of mobilising, managing and validating the entries
          of certain regions will be delegated to local organisations,
          such as the member organisations of EuroLinux.

   Costs:
          Initial development of the database and website will require a
          week of high-level expert time, which is normally calculated at
          1000 eur / day, i.e. 5000.

          The work of the national organisations will be done at their
          own expense, because it will help them with the pursuit of
          organisational work which they have to do anyway.

Influencing Jurisdiction by Exemplary Lawsuits

   What?:
          One large company in the automation area is preparing a lawsuit
          against a typical software patent on the basis of lack of
          technical character, violation of Art 52 and violation of the
          European Charter of Human Rights. We would like to take this
          lawsuit as far up as possible and, ressources permitting, start
          other similar lawsuits. These can create a public impact, make
          legislators think, and perhaps even solve the basic problems,
          which consist in an attempt by a part of the patent
          jurisdiction to usurp the role of the legislator and impose its
          new rules on the rest of the jurisdiction.

   Who?:
          The named automation company together with a lawyer who works
          on our side, e.g. J�rgen Siepmann.

   Costs:
          The ordinary costs of an invalidity proceding are 100-150,000
          eur in Germany. The money can be refunded if the proceding is
          successful. We cannot count on a refund and must perhaps even
          be prepared to lose in certain lower courts in order to be
          admitted to the level of constitutional courts, which may be
          our real aim. So far we do not have a promise from any company
          that it will pay. We also must try to look for cheaper ways to
          access certain courts, e.g. a warmup exercise at a lower court,
          where we win, or a direct access to the European Court of Human
          Rights. The details are to be fully explored by January.

          We need to share these costs with concerned companies. We
          cannot rely entirely on them, because our aim may not exactly
          be the same and we can maintain a fruitful cooperation only if
          we share the burden.

Budget Summary

   EuroParl Meetings                        21,000
   National Parliaments                     ?
   Patent Documentation                     10-30,000
   Awareness Raising with Special Audiences ?
   European Software Expertise Directory    5,000
   Exemplary Lawsuits                       50-100,000^[1]

Annotated Links

   -> How you can help us end the software patent nightmare
          The patent movement has during several decades won the support
          of large corporations and governments for its expansionist
          cause. Rolling trains are hard to halt. Yet FFII, Eurolinux and
          others have devoted themselves to this work with some success.
          Still, we continue to have more tasks than free hands. Here we
          tell you how you can help us move forward more quickly by
          investing either time or money.

   -> Action Plan for Protection against Swpat
          The battle against logic patents can be won. Let's get serious
          about it
     _________________________________________________________________

Notes

   [1] subject to the same amount being collected from private
   participants
     _________________________________________________________________

