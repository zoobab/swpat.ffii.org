\contentsline {section}{\numberline {1}The Problem: Dangerous Patentability Legislation in Brussels}{2}{section.1}
\contentsline {section}{\numberline {2}Project Managers, Partners and Sponsors}{4}{section.2}
\contentsline {section}{\numberline {3}How we will organise the work}{5}{section.3}
\contentsline {section}{\numberline {4}Hierarchy of Action Items}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Regular Work Meetings at the EuroParl}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Work Meetings at National Parliaments}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Patent Documentation}{7}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Awareness-Raising with Special Industry Audiences}{8}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Classified Directory of European Software Expertise}{9}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Influencing Jurisdiction by Exemplary Lawsuits}{9}{subsection.4.6}
\contentsline {section}{\numberline {5}Budget Summary}{10}{section.5}
\contentsline {section}{\numberline {6}Annotated Links}{10}{section.6}
