# -*- mode: makefile -*-

cios3.en.lstex:
	lstex cios3.en | sort -u > cios3.en.lstex
cios3.en.mk:	cios3.en.lstex
	vcat /ul/prg/RC/texmake > cios3.en.mk


cios3.en.dvi:	cios3.en.mk
	rm -f cios3.en.lta
	if latex cios3.en;then test -f cios3.en.lta && latex cios3.en;while tail -n 20 cios3.en.log | grep -w references && latex cios3.en;do eval;done;fi
	if test -r cios3.en.idx;then makeindex cios3.en && latex cios3.en;fi

cios3.en.pdf:	cios3.en.ps
	if grep -w '\(CJK\|epsfig\)' cios3.en.tex;then ps2pdf cios3.en.ps;else rm -f cios3.en.lta;if pdflatex cios3.en;then test -f cios3.en.lta && pdflatex cios3.en;while tail -n 20 cios3.en.log | grep -w references && pdflatex cios3.en;do eval;done;fi;fi
	if test -r cios3.en.idx;then makeindex cios3.en && pdflatex cios3.en;fi
cios3.en.tty:	cios3.en.dvi
	dvi2tty -q cios3.en > cios3.en.tty
cios3.en.ps:	cios3.en.dvi	
	dvips  cios3.en
cios3.en.001:	cios3.en.dvi
	rm -f cios3.en.[0-9][0-9][0-9]
	dvips -i -S 1  cios3.en
cios3.en.pbm:	cios3.en.ps
	pstopbm cios3.en.ps
cios3.en.gif:	cios3.en.ps
	pstogif cios3.en.ps
cios3.en.eps:	cios3.en.dvi
	dvips -E -f cios3.en > cios3.en.eps
cios3.en-01.g3n:	cios3.en.ps
	ps2fax n cios3.en.ps
cios3.en-01.g3f:	cios3.en.ps
	ps2fax f cios3.en.ps
cios3.en.ps.gz:	cios3.en.ps
	gzip < cios3.en.ps > cios3.en.ps.gz
cios3.en.ps.gz.uue:	cios3.en.ps.gz
	uuencode cios3.en.ps.gz cios3.en.ps.gz > cios3.en.ps.gz.uue
cios3.en.faxsnd:	cios3.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo cios3.en-??.g3n`;source faxsnd main
cios3.en_tex.ps:	
	cat cios3.en.tex | splitlong | coco | m2ps > cios3.en_tex.ps
cios3.en_tex.ps.gz:	cios3.en_tex.ps
	gzip < cios3.en_tex.ps > cios3.en_tex.ps.gz
cios3.en-01.pgm:
	cat cios3.en.ps | gs -q -sDEVICE=pgm -sOutputFile=cios3.en-%02d.pgm -
cios3.en/cios3.en.html:	cios3.en.dvi
	rm -fR cios3.en;latex2html cios3.en.tex
xview:	cios3.en.dvi
	xdvi -s 8 cios3.en &
tview:	cios3.en.tty
	browse cios3.en.tty 
gview:	cios3.en.ps
	ghostview  cios3.en.ps &
print:	cios3.en.ps
	lpr -s -h cios3.en.ps 
sprint:	cios3.en.001
	for F in cios3.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	cios3.en.faxsnd
	faxsndjob view cios3.en &
fsend:	cios3.en.faxsnd
	faxsndjob jobs cios3.en
viewgif:	cios3.en.gif
	xv cios3.en.gif &
viewpbm:	cios3.en.pbm
	xv cios3.en-??.pbm &
vieweps:	cios3.en.eps
	ghostview cios3.en.eps &	
clean:	cios3.en.ps
	rm -f  cios3.en-*.tex cios3.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} cios3.en-??.* cios3.en_tex.* cios3.en*~
cios3.en.tgz:	clean
	set +f;LSFILES=`cat cios3.en.ls???`;FILES=`ls cios3.en.* $$LSFILES | sort -u`;tar czvf cios3.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
