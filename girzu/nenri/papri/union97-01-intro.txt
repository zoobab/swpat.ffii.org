<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">
TABLE OF CONTENTS

PREFACE

P. 5

PROGRAM p. 9

WELCOMING ADDRESSES

Rolf Wiclander p. 11
GertKolle p. 13
Jurgen Betten Introduction of the Subject p. 16

PRESENT SITUATION p. 21
Peter Manna Software Patents - International Trends and Statistics p. 22
Anton Holzwarth EPO - Present Practice p. 46
Luigi Boggio EPO - Case Law p. 54
Roland Dheere Problems with Prior Art Search p. 60

RECEPTION BY THE BAVARIAN STATE

Alfons Zeiler, State Secretary p. 73

Jurgen Betten p. 79

INTERNATIONAL DEVELOPMENTS p. 83
Homer Knearl Legal and Commercial Situation in the U.S.A. p. 84
Ken Norichika Legal and Commercial Situation in Japan p. 105

NATIONAL POSITIONS IN EUROPE
Derek Haselden UK Position p. 126
Wilfried Anders German Position p. 133
Javier F.-Lasquetty Spanish Position p. 138
Katharina Fastenbauer Austrian Position p. 142
Jean-Yves Placais AIPPI Question Q 133 - Evaluation p. 148
Pascal Leardini Software-Related Inventions - "Up-to-Date" Views

of the European Commission p. 166

WHAT SHOULD EUROPE DO? p. 171
Jurgen Betten Clarification or Deletion of the Exclusion of

"Computer Programs" in Art. 52(2) EPC p. 172
GertKolle Future Policy in Europe p. 177
RainerAmann Report on Comparative Trilateral Study p. 184

FINAL DISCUSSION p. 197
MATERIALS FOR PREPARING THE UNION ROUND TABLE p. 207
BIOGRAPHIES OF SPEAKERS AND CHAIRMEN p. 301
PARTICIPANTS p. 307

REPORTS ON UNION'S ROUND TABLE "PATENTING OF COMPUTER
SOFTWARE" p. 311

PRESS RELEASE p. 313

/

PREFACE

After having discussed the question of patenting computer software on several occasions
UNION* came to the conclusion that

(1) the exclusion of "programs for computers" in Art. 52(2) European Patent Convention
should be deleted or at least clarified and that

(2) great efforts should be made to inform private inventors as well as small and
medium-sized firms in Europe that patents are available for computer software.

In the letter of invitation to the Round Table it was further stated that

"We feel that in the past the general understanding of the exclusion of patent protection for
"programs for computers as such" has been detrimental to the European computer soft-
ware industry and that Europe will lose ground to the United States in the field of computer
software technology unless the patent laws are clarified."

In order to foster the discussion on this topic in Europe and to overcome the still existing
prejudice in Europe that patents are not available for computer software (see e.g. the
article in New Scientist of 9 May 1998 on the last page of this publication), this Round
Table was organized by the Software Commission of UNION.

With 90 experts from 20 countries, the Round Table was a great success. Obviously, it
was the right topic at the right time.

From information given during the Round Table it is clear that patent systems in the United
States and Japan are more liberal than those in Europe in their attitude towards
patentability of software inventions. It is also clear that the industry worldwide is rapidly
making more extensive use of software patents rather than relying solely on copyright
protection, as in the past.

It was also made clear that to keep pace with technology an optimum patent protection of
computer programs must cover not only the computer program stored on a disk, which is
already protected in the USA and Japan, but also the computer program when it is
electronically distributed, e.g. on the Internet, and downloaded into a computer.

Mr. Leardini from the European Commission referring to many responses to the recently
published E.U. Green Paper on Improving the Patent System brought the current problems
with patenting computer software in Europe to the point. Those who know "the rules of the
game" are successful in getting patents granted for software inventions, and those who
lack this knowledge do not even try to apply for software patents in their country or with the
EPO.

Currently the EPO uses the concept of "technical character" or "technical contribution" to
distinguish patentable from non-patentable computer programs. This approach, however,
does not seem to be well understood, neither by patent applicants, nor by their represent-
atives, nor by the national patent offices, who still follow their traditional approaches.

* UNION is an association of more than 700 (independent or employed) practitioners in the field of industrial
property having their seats in more than 20 European countries.

If, and this seems to be the case, the concept of "technical character" or "technical
contribution" is, at best, understood by some experts in this field, albeit this concept has
been used by the EPO for more than ten years already, and if this concept still seems to
be misleading to most parties concerned, especially to the SME industry and their
representatives, it seems to be better to give up this concept and to make clear that all
computer programs have inherently "technical character" as their purpose is to control the
operation of the computer which is, without a doubt, part of the technical world. It is then
possible to concentrate on the real issues of the subject matter of all inventions, namely

 Is it new?

 Does it involve an inventive step?

 Is is susceptible of industrial or commercial application?

We think that the judges and examiners in Europe need no further guidance, or - as with
Art. 52(2) EPC - misguidance, to determine the merit or the patentability of an invention.

While the EPO, in view of the global trends, would clearly like to follow the lead set by the
patent offices in the USA and Japan and help to reach a uniform treatment of software
inventions worldwide, such as deletion of "computer programs as such" from Art. 52(2)
EPC or even to delete the whole Art. 52(2) EPC, it could take up to ten years to secure
agreement by all of its presently 18 member countries to implement this or other changes
to the 20-years old EPC.

To bridge the time between it is proposed in view of the Round Table and the comments
received thereafter:

(1) European industry and patents professionals, including the judges, must be given
education to ensure they make the best use of the patent system to protect inventions
in the field of computer software.

(2) The Examination Guidelines of the EPO should be updated and those of contracting
states harmonized with the EPO Guidelines to ensure that present EPO practice is
understood. The criteria for defining "technical character" should be clearly explained
to examiners and applicants, or the concept should no more be used.

(3) The EPO should consider defining the term "computer programs as such" in a new
Implementing Rule as "source and/or object code of the program" which is not sought
to be patented and which is anyway protected by copyright.

(4) The EPO should consider to catch up with recent changes in USPTO and JPO
practice on allowability of claims to program products (comprising a carrier and a
computer program) and data structures, when stored on a disk or carrier, or storable
after transmission e.g. on the Internet. With this it should be clarified that such kind of
claims have nothing to do with "presentations of information as such" or "computer
programs as such".

It is admitted that proposals (2) to (4) will take some time to come about, but should
nevertheless be considered as a matter of urgency. However, proposal (1) - EDUCATION

 can and should be started straight away.

In summary, it can be said that the result of the Round Table Meeting was

(1) to encourage European industry to follow the example of their competitors in the USA
and Japan and to make use of patent protection for computer programs as well and

(2) to encourage the EPO and/or the European Commission to amend the law and the
guidelines so that the European system provides an optimum patent protection of
computer programs and is competitive with the USA and Japan.

Jurgen Betten
in the name of
UNION'S Software Commission

June 1998

PROGRAM

Tuesday, December 9,1997

13.00 Registration

14.00 Welcoming addresses

 Rolf Wiclander, President of UNION

 Gerd Kolle, Director of International Legal Affairs of EPO

14.20 Introduction of the subject of the Round Table (Jurgen Betten, UNION SWC)

14.30-17.00 Present situation

Chairman: Ludwig Baumer, WIPO

14.30  International trends, statistics (Peter Hanna, UNION SWC)
15.00  EPO - Present practice (Anton Holzwarth, EPO)

15.30-15.50 Coffee break

15.50  EPO - Case law (Luigi Boggio, UNION SWC)
16.20  Problems with prior art search (Roland Dheere, EPO)

18.00-19.00 Reception by the Bavarian State, Ministry for Economic Affairs, Transport and
Technology in the Vierschimmel-Saal of the Residence

19.30 Buffet Dinner at the Hotel Bayerischer Hof, Konigssaal, Palais Montgelas

 by invitation of UNION

Wednesday, December 10,1997

09.00-12.30 International developments

Chairman: Gert Kolle. EPO

09.00  Legal and commercial situation in the U.S.A. (Homer Knearl, U.S. Patent

Attorney)

09.30  Legal and commercial situation in Japan (Ken Norichika, SOFTIC)
10.00  National positions in Europe

- UK position (Derek Haselden, UK Patent Office)

10.30-10.50 Coffee break

10.50 - positions of other countries (open to short contributions)
12.30  AIPPI Question Q 133 - evaluation (Jean-Yves Placais, UNION SWC)

13.00-14.00 Lunch

14.00-16.30 What should Europe do?

Chairman: Roger Burt, European Patent Practice Committee of epi

14.00  Software-related inventions - "up-to-date" views of the European Commis-
sion (Pascal Leardini, European Commission)
14.20  Clarification or deletion of the exclusion of "computer programs" in Art.

52(2) European Patent Convention (Jurgen Betten, UNION SWC)
14.40  Future policy in Europe (Gert Kolle, EPO)
15.00  Report on comparative trilateral study (Rainer Amann, EPO)

15.20-15.40 Coffee break
15.40  Final discussion together with all speakers

("SWC": UNION'S Software Commission)

WELCOMING ADDRESSES
Rolf Wiclander, President of UNION

Ladies and Gentlemen,

it is a great honor and pleasure for me to welcome such a distinguished audience.
We from the Bureau of UNION had some doubts as to whether the Software
Committee would succeed in its ambitious plans to bring together 60 to 80 persons
from all over Europe, from the patent offices, courts, the hardware industry and the
software industry as well as lawyers and patent attorneys to discuss such a very
special topic, namely "Patenting of Computer Software".

At least in the summer of this year it was, however, clear to everybody that it was the
right topic at the right time.

One of the aims of UNION is to bring the European practitioners in the field of
industrial property together and to foster the European discussion. Therefore, UNION
not only organizes congresses on a high professional level every three years, but
also prepares, in the time between, a Round Table on a special topic. These Round
Tables quite often have a great impact on the further development in Europe; for this
I refer, for example, to the Round Table on "Utility Models", which stimulated the
European Commission to issue the Green Paper on "Utility Models".

Today it will be for the first time a truly international Round Table of UNION. We have
about 90 participants from 20 European countries, with distinguished lecturers from
Japan and the United States.

You will probably know that it was UNION which raised its voice for the first time on
an international conference, the FICPI congress in Vienna in June 1994, against the
exclusion of "computer programs as such" in the European laws. This was in
response to a lecture by Dr. Remandas, Vice-President of the EPO, on the topic "The
European Patent System in Progress" with special emphasis on "fostering innovation
in Europe" In his contribution Jurgen Betten, the President of the Software Committee
of UNION, stated that Art. 52 (2) EPC and especially the general understanding of
the exclusion of patent protection for "programs for computers as such" had been
detrimental to the European industry, in particular the European software industry.
The legal uncertainty created by Art. 52 (2) EPC kept the European industry from
filing software-related patent applications, whereas the American and Japanese
companies have bothered little about the legal situation and filed patent applications
and were granted most of the more than 11.000 software-related patents which the
EPO had granted until 1994. He further stated that Europe would lose ground to the
U.S. in the field of computer software technology unless the patent laws were
clarified.

In her response to this contribution Dr. Remandas said that the EPO would soon
discuss this question with the interested circles and that the contributor would be
invited. Unfortunately, this did not take place when the Hearing '95 was held in
September 1995 in the EPO.

11

If there is not an immediate success, it is our duty as attorneys to find some other
ways to achieve the same or a similar result. Therefore, the Software Committee of
UNION decided to turn the tables and to ask 60 to 80 persons from all over Europe to
come to the EPO and to discuss the topic "Patenting of Computer Software" with the
EPO. This explains why we are here today and you will understand that we very
much regret that Dr. Remandas cannot be with us today.

This gives me the opportunity to mention at least some persons of the distinguished
audience who followed our invitation. We greet Ludwig Baumer from WIPO, Pascal
Leardini from the European Commission, and Ken Norichika, Executive Director of
SOFTIC. SOFTIC is that Japanese organization which has done a lot of fostering the
discussion on protection of computer programs, especially by organizing the annual
conferences and bringing together lecturers and participants from all over the world.
Homer Knearl came over from Denver, Colorado to tell us more about the situation in
the United States.

A special greeting goes to Gert Kolle, who from the beginning has been enthusiastic
about this Round Table and who, together with many other persons from the EPO,
helped the Software Committee a lot to prepare this Round Table.

It's a great pleasure to welcome representatives from AIPPI, CNIPA, epi, FICPI, LES
and the German Patentanwaltskammer.

We further welcome 13 participants from national courts, including the EPO Board of
Appeal, and 12 participants from national patent offices.

At the end I would like to introduce you to the Software Committee of UNION who
has organized this Round Table. The President is Jurgen Betten from Germany, the
members who are present are Luigi Boggio from Italy, Franz Anton Dietz from the
Netherlands, Peter Hanna from Ireland, Roger Palmer from the United Kingdom and
Jean-Yves Placais from France.

We know that we will also need the government's support if we want to change the
law. We therefore are very pleased that the Bavarian state showed its support by
giving a reception in the Residenz this evening.

Finally, I would now like to give the floor to Gert Kolle who is representing the EPO
today and wish us all a very fruitful discussion.

12

Gert Kolle, Director of International Legal Affairs of the EPO

Mr. Wiclander, Mr. Betten, Ladies and Gentlemen,

a very cordial welcome to this UNION Round Table, and this is in particular on behalf
of the President of the EPO, Mr. Kober, and my Vice President, Ms. Remandas. Ms.
Remandas does very much regret to be unable to attend this Round Table today, and
so I hope I can step in and say a few words of welcome to you.

First of all, I would like to thank the UNION and their Software Commission for having
taken the initiative to organize this Round Table in such a timely manner on such a
topical issue, as is today and in the future. "Patent Protection of Computer Software".
The European Commission's Green Paper has certainly created a new momentum,
and the issue is now on the agenda of many conferences and meetings to discuss
the most important questions raised in the Green Paper. How topical the matter is
can be shown by a recent letter from the U.K. Comptroller General of Patents and
Trademarks, Mr. Hartnack, who invited the President of the EPO to speak about
"Software Patents in Europe" at a forthcoming London conference in March 1998,
and Mr. Hartnack expects this event to attract around 500 delegates from the
European software industry.

It is a great pleasure for me to see, at this Round Table, such a distinguished
audience from all quarters involved: the courts, the EPO Boards of Appeal, the patent
offices, the patent profession, and industry, and last but not least even some lawyers
were given the opportunity to attend. I particularly appreciate this. Not only
representatives from Europe are gathering here, but also our guests from Japan and
the U.S., who have done, I would say, the pretty tedious way to Europe, and they
underline the global dimension of our topic.

Ladies and gentlemen, legal protection of computer software appears to be a good
example for a never-ending debate on the right approach to get to grips with this so
important field of human activity, which has become the key factor for innovation
today and more and more controls our lives today, and even more so in the future.
not only our professional but also our private lives. We have now seen more than 30
years of discussions which are behind us, and certainly there will be many years
ahead of us. The pendulum is swinging between patent, copyright, and a sui generis
protection, and this debate always reminds me of a joke when a young client is
rushing into the office of a patent agent, and the patent agent says, "Well, Sir, what
can I do for you?". And the young man says, "Well, it's a pretty simple question and
you will have no difficulty in giving me the right answer. How can I copyright my
patented trademark?" So you see, this is in a way a good paradigm for the topic we
are discussing right now and in the future.

You may know, some of you at least, that I'm an old chap, not necessarily a happy
chap in this domain, because in 1965 I started working on this matter when, after
being graduated, I was looking for an interesting doctoral thesis. I happened to learn
from a colleague that the famous professor Ulmer, I would say the German pope of
Copyright Law, was looking for two bright lawyers to deal with a new animal
"computer programs" from a copyright and a patent law point of view. I elected the
patent law part, and I should not have done so. My thesis has remained unfinished so

13

far because of the likely and still emerging developments, but I feel firmly committed
to completing my thesis after my retirement.

A few historical remarks now on how the complex issues regarding patentability of
computer software were tackled and how they developed over this long time of more
than thirty years. It is good to recall what the development was in the past because it
is only this way that you can perfectly understand how we stand today and how the
pendulum indeed swung between copyright and patent from copyright to patent law
right now. It was the French patent law of 1968 which was first to exclude expressly
computer programs from patent protection as these were considered to lack industrial
application. Then, in 1970, the PCT took up computer programs in this international
context, but in a quite different context than it was done in the French Patent Law.
Here, there was no express exclusion from patentability, but simply the rules which
say that an international searching authority and a preliminary examining authority
need not search or examine inventions which do consist of a computer program and
any of a matter listed in these rules. This was no ban on patenting but simply done in
response to the French Patent Law of 1968 so as to enable international PCT
authorities not to deal with that complex matter.

In 1973 then, the EPC as it stands today copied the list of non-patentable subject
matter from the PCT and expressly provided for that this subject matter inter alia was
not regarded as an invention. So, different from the Patent Law of France of 1968, it
was not considered to lack industrial application, but it was considered to be a non-
invention. The national laws of the EPC member states followed and adapted the
national laws to the EPC, and the post-EPC stage then saw a number of interesting
cases. The first cases, by the way, dealing with computer software date back to the
late 60ies. The first decision at all in Europe was the famous U.K. "Slee and Harris"
decision of the then Patents Appeal Tribunal and it was followed by decisions in
Germany, "BCD conversion", the 17th Senate of the Bundespatentgericht (the Federal
Patents Court), then we had a number of cases in the U.S., the old CCPA dealing
with mathematical and software-related inventions, and we had also a decision from
France, the Mobil Oil case.

The post-EPC stage saw a number of German cases, notably the famous Federal
Supreme Court decision "Disposition Program", and this was followed by many other
decisions, "Anti-Skid System" and "Minimization of Flight Costs". In my analysis of the
"Disposition Program" case of the Federal Supreme Court at the end, and I was
quoted by Mr. Betten, I was tempted to state that in light of this decision any attempts
to obtain patent protection for computer programs has no longer any future. I was
wrong in my analysis.

Then we had 1985, the first EPO Guidelines for Examination, which were
supplemented by a chapter on the patentability of computer programs. After a very
fruitful discussion with the private sector and - Mr. Wiclander - we do always discuss
with the private sector any development all of the time, and we do this in this
excercise here in a, I would say, exemplary way to show that we must do discussions
constantly with the users. So the EPO Guidelines paved the way for the first decision
of an EPO Board of Appeal, the famous "VICOM" case, which was followed by many
Board of Appeal decisions up to the most recent decision reported in the Official
Journal "SOHEI", which will certainly be dealt with in more detail later on.

14

Then we saw a number of cases in the EPC member states, in Germany, the
Netherlands, Sweden, and the U.K., and we had the impression that German case
law was not quite consistent with EPO practice, in particular the Board of Appeal
decisions, and we had also the impression that U.K. cases, although applying the
same approach more or less in literal terms, but coming to different results, was not
quite consistent with our practice. We will hear about this later on certainly.

Then, after all, a number of cases were decided in the United States by the newly
established Court of Appeals for the Federal Circuit, the CAFC, the most recent ones
well-known to all of you, I suppose, "Alappat", "Beauregard", and "Lowry", and we will
certainly hear from our American guest how these cases are to be seen from a point
of view of the practitioner.

Finally, I wish to mention that in 1988, we made a first attempt to harmonize practices
among the three big offices in the world: the USPTO, the Japanese Patent Office,
and the European Patent Office. We set up a study, a comparative study on the
patentability of computer software. At that time we saw no need to go further in our
attempts to harmonize because we considered, although the approach applied in the
three offices was different, the results were more or less the same so that there
wasn't any need for further harmonization. That was in 1988.

Finally, in light of the new U.S. cases from the CAFC, the USPTO adopted new
Guidelines for the examination of computer-related inventions in 1996, and this was
immediately, then I wouldn't say copied, but it was followed in spirit by new Japanese
Patent Office Guidelines on the same matter so that we can see today that practice,
at least in terms of, well, the language of the Guidelines, that the practice in Japan
and the US is more or less along the same lines, while our practice might slightly
differ, and this remains for our future discussion on this Round Table.

After all, the Green Paper now is on the market. We had the hearing last week in
Luxembourg, and now it gets a new dimension. Now the question has been risen, the
question namely whether it is necessary to revise the EPC or, at least in the
meantime or instead or in addition, a new Community instrument should be created
to afford adequate protection for computer software from a patentable point of view.

This was a brief historical review of our subject, and I come to the end by saying that
I wish all of us a very successful and pleasant Round Table, which I hope, and I feel
confident, will move things forward to a good end, and maybe in a few years time we
will have then reached the point where it is no longer necessary to discuss on the
right approach to protect computer software. Thank you for your attention.

15

Introduction of the Subject

Jurgen Betten, President of UNION'S Software Commission
Partner of Betten & Resch, Munich

The subject "Patenting of Computer Software" has been discussed for more than 30
years. About 30 years ago, a commission set up by the President of the U.S.A.
determined that computer programs should not be protected by patents for the
following reasons:

1. The patent office does not have enough documentation in order to be able to
examine inventions in the field of computer programs;

2. the patent office does not have enough examiners for that task; and

3. the examination is too time-consuming and expensive.

Obviously, it was intended to avoid all problems that might occur in connection with
computer programs.

In 1968 the French legislature took a similar turn when it excluded "computer
programs as such" from patent protection. From one of the founders of the European
Patent Convention (EPC) it became known that, on working out the EPC, the French
example to exclude "computer programs as such" was followed simply to have one
problem less and thus to enable the accomplishment of the EPC at all. Today, the
same person would say that it probably had been a mistake to do so and not to leave
it to the case law.

Consequently, "computer programs as such" now are excluded from patent
protection according to the EPC as well, but to this day it has not been clarified,
neither by decisions of the Boards of Appeal of the European Patent Office (EPO) nor
of the courts of the Contracting States of the EPC, what is to be understood by
"computer programs as such". In view of Art. 10 of TRIPS it can be assumed that
"computer programs as such" means only the "source or object code" of computer
programs which are protected as literary works under the Berne Convention. This will
be discussed further at the end of the Round Table.

Then WIPO made an effort to enable special protection for computer programs
which, however, was unsuccessful. Ludwig Baumer could tell you a lot of stories
about that.

In order to be able to protect computer software or computer programs at all, which
were becoming increasingly important, the copyright was used as an alternative.
Right from the beginning, however, all experts were well aware of the fact that the
copyright was intended to only prevent unauthorized copying and slight modifications
of computer programs. The real essence of a computer program, i.e. the underlying
ideas, concepts, algorithms, functions, logic etc., could not and should not be
protected by copyright.

In 1991, the several years of hard struggle for the copyright protection in the
European Union culminated in the "Guideline for the Legal Protection of Computer
Programs". In the years that followed it turned out - first of all in the U.S.A., but then

16

in other countries as well - that the copyright law could only provide weak protection
for computer programs.

In the meantime, however, there were a lot of persons who where in favor of patent
protection for computer programs again. In the U.S.A., in Japan and at the European
Patent Office more and more software patents were filed and also granted. For
example, in 1995 the European Patent Office announced that since its foundation,
i.e. basically since 1984, more than 11,000 patents had been granted on inventions
relating to computer software. Only about 100 applications had been rejected for lack
of technical character. It is currently estimated that 2,000 to 3,000 patent applications
relating to computer software are filed at the EPO every year. We will hear more
about the international trends and statistic from Peter Hanna.

In the preparatory materials you found some "software" patents that were granted by
the European Patent Office (EP), the German Patent Office (DE), the Boards of
Appeal of the EPO, or by the German Patents Court. With this, the patent offices,
boards of appeal and courts in the European Union have taken into account the
phenomenal development of computer software. While previously software only was
considered a supplement to computer hardware, it can be found today that a
computer can only be sold if there is enough good software for it.

We will hear more about the EPO case law from Luigi Boggio, and about the way the
EPO is tackling the prior art search from Roland Dheere and Peter Hanna.

Tomorrow you will hear that, not unexpectedly, several member states of the EPC
are taking a different approach. Derek Haselden will explain the UK practice,
whereas Javier Fernandez-Lasquetty will explain the Spanish position and Wilfried
Anders the German position. Katharina Fastenbauer will tell us more about the utility
model approach in Austria, and Pascal Leardini will tell us how the European
Commission will bring all these differing positions in line with each other.

The fact that a lot of software patents are granted by the EPO and most national
patent offices of the member states of the EPC, seems to have been unnoticed by a
large fraction of the parties concerned and particularly the European software
industry and its advisors. Apparently the discussion about the Software Directive in
Europe, which has been going on for many years, has drawn everybody's attention to
the copyright protection. One may get the impression that many software companies
still do not understand the possibilities of patent protection (for computer programs)
because most of the software-related patents granted by the EPO are granted for
American and Japanese applicants.

This is particularly unfortunate because

 on the one hand, an ever growing part of the value of new products is ascribed to
software services and

 on the other hand, the European industry is considered to definitely have great
chances in the international competition with the U.S.A. and Japan concerning the
application (i.e. the software), particularly of the new computer and
communication technologies.

17

I have therefore repeatedly proposed to cancel of the exclusion of "computer
programs as such" from patent protection in the respective patent laws and that
Article 27 of the TRIPS Agreement ("patents shall be available for any inventions in
all fields of technology") could be used as a reason for doing so. In connection with
this I pointed out at the FICPI Congress 1994 in Vienna that the deletion would foster
innovation in Europe and that in the past the exclusion of patent protection for
"computer programs as such" had been detrimental to the European IT industry,
especially to the software industry.

According to a study of an American market research institute, the top 100 European
enterprises invested more than 40 billion U.S. dollars in information technology in
1995, i.e. in hardware, service, and, for the most part, in software. Many of them
invested more than 4% of their annual turnover in information technology. It is
estimated that, in the countries of the European Union alone, the market for software
products meanwhile encompasses more than 200 billion U.S. dollars.

In its Annual Report 1994 the European Patent Office points out that the information
industry depends on an efficient legal protection of its products and processes to
ensure that it can cover the very high investments in the field of research and
development of new hardware and software. The information industry's need for
protection is evident. In most cases the expenses for the research and development
of a software product, particularly for a computer program, amount to more than 1
million DM, and these products can easily be copied or imitated. In connection with
this I would like to point out a misunderstanding - which could also be referred to as
lack of information. For example, it is stated time and again that computer programs
are too short-lived to be worth being legally protected. However, one can already see
today that the economically relevant useful life of a considerable part of the computer
programs is clearly longer than originally expected. At least the essential parts of a
computer program often have a long life in economic terms so that protection against
imitators is absolutely necessary.

Homer Knearl will tell us tomorrow how the U.S.A. approached this problem. The
same will be explained by Ken Norichika for Japan. Both will tell us more about the
legal and commercial situation in their countries, especially the practice after the
examination guidelines entered into force.

A broad range of protection is already available for hardware and software creations.
For example, the copyright protects all forms of a computer program, i.e. particularly
the program listing, the source code and the object code or - the "computer program
as such".

By comparison, the patent law protects the technical teaching, the technical contents
or the technical realization of the underlying ideas, algorithms, principles and
concepts or the function or logic of a computer program.

On the basis of the patented technical teaching defined in the patent claim, many
differing programs can, as a rule, be written which are all covered by patent
protection and, at the same time, result in copyrights that are independent of each
other. Patent protection and copyright protection therefore can coexist since they
relate to different aspects of computer software. With computer programs, the
inventor and the author do not need to be one and the same person.

18

Before coming to the end I would like to refer again to my introductory remark about
the commission the President of the U.S.A. set up 30 years ago. Now, 30 years later,
it can be said that, given today's tools of search and a generation that effortlessly
grows up with data processing, it will certainly be possible to make the entire field of
data processing accessible to patent protection. One will still have the possibility of
excluding from patent protection abstract ideas or purely mathematical algorithms,
i.e. algorithms without a concrete use. This seems to be the AIPPI approach. Jean-
Yves Placais will tell you more about that.

Even in case of a very liberal interpretation of the legal regulations, such an approach
will probably only become possible if the EPC and consequently the national laws of
the EPC member states are amended. You will hear more about that tomorrow
afternoon from Gert Kolle and from me.

I would like to close with a quotation of the German Federal Supreme Court, who
explained in its decision "Red Dove" that it was

"the main object of the Patent Law to include those results that are
worth patenting according to the latest state of science and
research."

Nothing else is stated in Art. 27 of the TRIPS Agreement:

"Patents shall be available for any inventions, whether products or
processes, in all fields of technology."

