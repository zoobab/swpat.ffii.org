<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">
372

NJW 1992, Heft 6

10.t Technischer Charakter eines Arbeitsspeichersystems - Seitenpuffer

PatG 1968 § l I

1. Eine programmbezogene Lehre ist technisch, wenn sie die Funktionsfähigkeit der Datenverarbeitungsanlage als solche betrifft und damit das unmittelbare Zusammenwirken ihrer Elemente ermöglicht (Ergänzung zu BGHZ 67, 22 [29] = NJW 1976, 1936 = LM § l PatG Nr. 44- Dispositionsprogramm).

2. Ein Verfahren, das in der Erfassung und Speicherung der Information über den aktuellen Speicherbereich eines in einer Datenverarbeitungsanlage ablaufenden Rechenprozesses und in einer bestimmten Ladestrategie für einen dem bevorzugten Zugriff unterliegenden, aber nur eine Auswahl von Speicher-seiten fassenden Speicher (Seitenpuffer) besteht, betrifft die Funktionsfähigkeit der Datenverarbeitungsanlage als solche; es enthält die Anweisung, die Elemente einer Datenverarbeitungsanlage beim Betrieb unmittelbar auf bestimmte Art und Weise zu benutzen.

3. Ob eine Lehre zum technischen Handeln vorliegt, hängt nicht davon ab, ob die Lehre neu, fortschrittlich und erfinderisch ist.

BGH, Beseht, v. 11. 6. \W\ - \ Zß 13/HK (BPatC,)

Zum Sachverhalt: Die Patentanmeldung P 25 42 S45.9-53 vom 28. 9. 1975, die ein Verfahren zum Betreiben eines hierarchisch gegliederten, mehrstufigen Arbeitsspeichersystems und eine Schaltungsanordnung zur Durchtührung des Verfahrens betrifft, wurde mit sechs Patentansprüchen am 13. 3. 1980 bekanntgemacht. Patentanspruch l lautet wie folgt: Verfahren zum Betreiben eines hierarchisch gegliederten, mehrstufigen Arbeitsspeichersystems einer simultan mehrere Prozesse bearbeitenden datenverarbeitenden Anlage, dessen zwei niederste Speicherstufen aus einem alle Daten der simultan ablaufenden Prozesse enthaltenen Hauptspeicher und aus einem nur eine Auswahl von Speicherseiten umfassenden Seitenpuffer bestehen, zu dem gesteuert durch eine Speichersteuereinheit bevorzugt zugegriffen wird und in den eine bei einem Speicherzugriff fehlende Speicherseite übertragen wird, dadurch gekennzeichnet, daß beim Ablauf eines Prozesses jede Anforderung auf einen Speicherzugriff zum Seitenpuffer (SSP) in der Speichersteuereinhcit (SST) durch Zwischenspeichern der Adresse (MA, SLP) der ausgewählten Speicherseite (Sm; m == l bis 32) registriert und damit der aktuelle Speicherbereich des laufenden Prozesses ermittelt wird, daß die zwischengespeicherten Seitenadressen bei einem Prozeßwechsel in den Hauptspeicher (HSP) übertragen werden, daß bei einer späteren erneuten Aktivierung des Prozesses durch einen Prozessor (ZP) der datenverarbeitenden Anlage aus dem Hauptspeicher zunächst die Information über den bisher aktuellen Speicherbereich ausgelesen und mit diesen gespeicherten Seitenadressen sequentiell die zugeordneten Speicherseiten im Hauptspeicher ausgewählt und in den Seitenpuffer übertragen werden, sofern sie inzwischen beim Verarbeiten anderer Prozesse daraus verdrängt wurden und daß nach dem Bereitstellen des bisher aktuellen Speicherbereichs des zu aktivierenden Prozesses im Seitenpuffer dieser Prozeß unbehindert durch Seitenwechselanforderungen abläuft, solange sich der aktuelle Speicherbereich nicht ändert, wobei solche durch Anforderungen bisher nicht benötigter Speicherseiten bedingten Änderungen des aktuellen Speicherbereichs wieder in der Speichersteuereinheit registriert werden.

Entscheidungen - Zivilrecht: BGH

Im Einspruchsverfahren ist das Patent mit der Begründung versagt worden, die Lehre des Patentanspruchs l sei nicht technisch und deshalb dem Patentschutz nicht zugänglich. Das BPatG hat die Beschwerde der Anmelderin zurückgewiesen.
Die Rechtsbeschwerde führte zur Aufhebung des angefochtenen Beschlusses und zur Zurückverweisung der Sache an das BPatG.

Aus den Gründen: III. 4. 
Entgegen der Auffassung des BPatG kann der technische Charakter der in Patentanspruch 1 beschriebenen Lehre nicht verneint werden.

a) Der beanspruchten Lehre liegt das Problem zugrunde, den Seitenpuffer einer simultan mehrere Prozesse bearbeitenden Datenverarbeitungsanlage mit einem hierarchisch gegliederten, mehrstufigen Arbeitsspeichersystem, auf den schnell zugegriffen werden kann, der aber nur über eine begrenzte Speicherkapazität verlügt, optimal für die gerade zu bearbeitenden Prozesse mit Speicherseiten aus dem alle Daten enthaltenden Hauptspeicher zu belegen, auf den nur ein langsamerer Zugriff möglich ist.

b) Zur Lösung dieses Problems schlägt die Anmelderin vor, beim Reaktivieren eines bereits früher bearbeiteten Prozesses die benötigten Speicherseiten, die sich nicht mehr im Seitenpuffer befinden, nicht einzeln aus dem Hauptspeicher abzurufen, was mit Wartezeiten verbunden wäre, sondern sie bereits bei der ersten Anforderung zu registrieren und die registrierten Speicherseiten bei der erneuten Aktivierung des Prozesses insgesamt in den Seitenpuffer zu übertragen.

c) Gegenstand der beanspruchten Lehre ist danach ein Verfahren zum Betreiben eines Arbeitsspeichersystems einer Datenverarbeitungsanlage, die simultan mehrere Prozesse bearbeitet, (l) wobei das Arbeitsspeichersystem folgendermaßen ausgestaltet ist:

(1.1) es ist mehrstufig und hierarchisch gegliedert,

(1.2) seine zwei niedersten Speicherstufen bestehen

(1.2.1) aus einem alle Daten der simultan ablaufenden Prozesse enthaltenden Hauptspeicher und

(1.2.2) einem nur eine Auswahl von Speicherseiten umfassenden Seitenpuffer,
(1.3) der Seitenpuffer

(1.3.1) unterliegt gesteuert durch eine Speichersteuereinheit dem bevorzugten Zugriff und

(1.3.2) in ihn wird eine bei einem Speicherzugrifffehlende Speicherseite übertragen,
(2) mit folgenden Verfahrensschritten:

(2.1) beim Ablauf eines Prozesses wird jede Anforderung auf einen Speicherzugriff zum Seitenpuffer in der Speichersteuereinheit registriert,

(2.1.1) die Registrierung erfolgt durch Zwischenspeichern der Adresse der ausgewählten Speicherseite,

(2.1.2) damit wird der aktuelle Speicherbereich des Prozesses ermittelt,

(2.2) bei einem Wechsel des Prozesses werden die zwischengespeicherten Seitenadressen in den Hauptspeicher übertragen,

(2.3) bei späterer erneuter Aktivierung des Prozesses durch einen Prozessor der Datenverarbeitungsanlage werden

(2.3.1) zunächst aus dem Hauptspeicher die Information über den bisher aktuellen Speicherbereich (dieses Prozesses) ausgelesen,

(2.3.2) mit den gespeicherten Seitenadressen sequentiell die zugeordneten Speicherseiten im Hauptspeicher ausgewählt und

(2.3.3) in den Seitenpuffer übertragen, sofern sie beim Verarbeiten anderer Prozesse aus diesem verdrängt wurden,

(2.4) nach dem Bereitstellen des bisher aktuellen Speicherbereichs des zu aktivierenden Prozesses läuft dieser Prozeß im Seitenpuffer unbehindert durch Seitenwechselanforderungen ab, solange sich der Bereich nicht ändert,

(2.5) durch die Anforderung bisher nicht benötigter Speicherseiten verursachte Änderungen des aktuellen Speicherbereichs werden in der Speichersteuereinheit registriert.

5. a) Die Patentfähigkeit des Gegenstandes der vorliegenden Anmeldung beurteilt sich nach § l I PatG 1968. Der Senat hat das Vorliegen einer Erfindung in ständiger Rechtsprechung nur dann bejaht, wenn die beanspruchte Lehre dem Bereich der Technik angehört (BGHZ 52, 74 ff. = NJW 1969, 1713 = LM § l PatG Nr. 32 - Rote Taube). 
Er hat eine Lehre zum technischen Handeln in einer Anweisung zum planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur Erreichung eines kausal übersehbaren Erfolgs gesehen, und dies bei bloßen Rechen- und Organisationsregeln unabhängig von ihrer sprachlichen Einkleidung verneint (BGHZ 67, 22 [27] = NJW 1976, 1936 = LM § l PatG Nr. 44 - Dispositionsprogramm; BGH, NJW 1977, 1637 = § l patG Nr. 46 = GRUR 1977, 657 [658] - Straken; BGH, NJW 1992, Heft 6 373, NJW 1977, 1635 = LM § l PatG Nr. 48 = GRUR 1978, 102f. - Prüfverfahren; BGH, LM § l PatG Nr. 55 = GRUR 1980, 849 f. - Antiblockiersystem; BGHZ 78, 98 f. = NJW 1981, 1617 = LM § l PatG Nr. 56 - Walzstabteilung-, BGH, NJW 1986, 994 = LM § l PatG Nr. 69 = GRUR 1986, 531 [533] - Flugkostenminimierung). An diesen Grundsätzen hält er fest.

b) Nach der Lehre des Patentanspruchs l wird die Entscheidung, welche Speicherseiten bei der Wiederaufnahme eines bestimmten Rechenprozesses aus dem Hauptspeicher in den Seitenpuffer genommen und dort weiterhin bereitgehalten werden, nach einer bestimmten Auswahlregel getroffen. Diese Auswahlregel besteht darin, die Speicherseiten bei der vorhergehenden Durchführung desselben Prozesses zu registrieren und das Ergebnis als Information über den aktuellen Speicherbereich dieses Prozesses festzuhalten, und zwar nach dem Wortlaut des Patentanspruchs l zunächst diesen Bereich zu ermitteln, die Speicheradressen zwischenzuspeichern und bei einem Prozeßwechsel in den Hauptspeicher zu übertragen, aus dem die Information bei der erneuten Aktivierung des Prozesses ausgelesen werden kann. In der Patentbeschreibung ist herausgestellt, bisher habe man erst dann eine Speicherseite in den Seitenpuffer übertragen, wenn sie bei einer Seitenanforderung gefehlt habe. Diese Ladestrategie habe den Nachteil einer Totzeit bei jeder Seitenübertragung. Beim Mehrprogramm- oder Multiprozessorbetrieb komm s hinzu, daß die laufenden Prozesse die Speicherkapazität des Seitenpuffers voll beanspruchten und ausschöpften. Bei Anforderung einer neuen Speicherseite müsse deshalb eine andere Speicherseite aus dem Seitenpuffer freigegeben werden. Bei der Deaktivierung des Prozesses würden alle im deaktivierten Prozeß benötigten Seiten freigegeben und könnten je nach den Anforderungen der ablaufenden neuen Prozesse überschrieben werden. Infolgedessen könne bei der erneuten Aktivierung eines Prozesses ein Teil oder auch der gesamte Bestand der benötigten Programmseiten im Seitenpuffer nicht mehr gespeichert sein, so daß eine erneute Anforderung aus dem Hauptspeicher erforderlich werde.
Die Beschreibung schildert mehrfache Seitenanforderungen aus dem Hauptspeicher als besonders ungünstig, weil sie jeweils eine Wartezeit des anfordernden Prozesses verursachten.

Die Patentanmeldung lehrt demgegenüber, diesen Nachteil dadurch zu vermeiden, daß anstelle mehrfacher Seitenanforderungen auf Grund der Ermittlung und Speicherung des aktuellen Speicherbereichs des Prozesses (Merkmale 2.1 mit Untermerkmalen und 2.2) bei dessen erneuter Aktivierung die Information über den bisher aktuellen Speicherbereich ausgelesen, die zugeordneten Speicherseiten im Hauptspeicher sequentiell ausgewählt und nach einem Abgleich mit dem Bestand im Seitenpuffer in diesen übertragen werden (Merkmal 2.3 mit Untermerkmalen), wodurch wiederholte Seitenanforderungen vermieden werden, solange sich der aktuelle Seitenbereich des Prozesses nicht ändert (Merkmal 2.4).

c) Die Lehre besteht somit zum einen in der Erfassung und Speicherung der Information über den aktuellen Speicherbereich eines Prozesses unter Zuordnung der benötigten Speicherseiten zu diesem (unter laufender Aktualisierung, Merkmal 2.5) und zum anderen in einer bestimmten Ladestrategie für den Seitenpuffer dergestalt, daß die benötigten Speicherseiten nicht einzeln, sondern auf Grund der durchgeführten Zuordnung "gebündelt" in diesen übertragen und dort als "Datenbündel" gespeichert werden. Diese Lehre erschöpft sich nicht in der Auswahl, Gliederung und Zuordnung von Daten, sondern verbessert die Arbeitsweise der Datenverarbeitungsanlagen.
Durch die Art der Nutzung des Seitenpuffers arbeitet die Datenverarbeitungsanlage schneller, indem Totzeiten vermieden werden. Dies betrifft die Funktion der Datenverarbeitungsanlage unmittelbar.

d) In der Entscheidung "Dispositionsprogramm" (BGHZ 67, 22 [29] == NJW 1976, 1936 = LM § l PatG Nr. 44) hat der Senat ausgeführt, die Lehre, eine Datenverarbeitungsanlage nach einem bestimmten Rechenprogramm zu betreiben, könne nur dann patentfähig sein, wenn das Programm einen neuen, erfinderischen Aufbau einer solchen Anlage erfordere und lehre oder wenn ihm die Anweisung zu entnehmen sei, die Anlage auf eine neue, bisher nicht übliche und auch nicht naheliegende Art und Weise zu benutzen. Diese Aussage bedarf der Klarstellung. Für die Frage, ob der Anmeldungsgegenstand eine Lehre zum technischen Handeln zum Inhalt hat, ist ohne Bedeutung, ob die Lehre neu, fortschrittlich und erfinderisch ist. Eine programmbezogene Lehre ist technisch, wenn sie die Funktionsfähigkeit der Datenverarbeitungsanlage als solche betrifft und damit das unmittelbare Zusammenwirken ihrer Elemente ermöglicht. Ob es noch andere programmbezogene Lehren gibt, die eine Lehre zum technischen Handeln zum Gegenstand haben, bedarf hier keiner Entscheidung.

e) Die Auffassung des BPatG, die Anmeldung beschreibe keinen bestimmten Aufbau einer Datenverarbeitungsanlage im Sin-


