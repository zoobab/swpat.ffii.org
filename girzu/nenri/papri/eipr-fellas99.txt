<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">

330 C:O,\lMEN??S: [ 19991 E.I.P.R

John Fellas
The Patentability of Software-related
Inventions in the United
States

State Street Bank
Financial Group
and Trust Co. v. Signature

The formulation of the criteria for determining when a
computer software program is patentable has been a
perennial problem on both sides of the Atlantic. This is
because at the heart of computer programs are algo-rithms,
and mathematical algorithms in fhemselves have
always been held to be uniatentable
Article 52 of the European Patent Convention is explicit
on this point. It specifies that the following are not patent-able
?as such?: ?discoveries. scientific theories and math-ematical
methods? as well as ?methods for. doing
business, and programs for computers?.? Similarly, in the
United States, ?mathematical algorithms? in themselves
are unpatentable because they are ?abstract ideas? or
?laws of nature?.2 Historicallv. ?business methods? have
also occasionally been singled out in the United States as
this type of unpatentable subject-matter.3
While the courts and the patent offices in Europe and
the United States have recognised that computer pro-grams
are in one sense reducible to algorithms, they
have also acknowledged that computer programs are
more than that. Despite the fact that they employ math-ematical
algorithms to solve problems, software-related
inventions generally have practical applications. There-fore
software-inventions as applied can satisfy the crite-ria
for patentable subject-matter. Thus, even though
Article 52 of the European Patent Convention provides
IO Ser e.y, Microsoft Corp. zi. Plaro Technolog>~ Lrd (unreported,
M a r c h 1 I, 1 9 9 9 ).
ThS unrhor, who cm, be conracred b y e -m a i l ar
fill<u [chlrgh~rhubburd.ronr. acknowledges the helpful comments of
Rotmld Abrunmm, Hughes Hubbard & Reed LLI: New York City.
I European Patent ConventIon, Art. 52 (1) (a) and (c).
2 Diumond TV,. Diehr 450 U.S. 175, 186 (1981) (noting that
m a t h e m a t i c a l a l g o r i t h m s a r e u n p a t e n t a b l e a s l a w s o f n a t u r e ).
3 LOPW?I Drw-ltl Themes, Inc. v. Park-ItI Themes, Inc. 174
F. 2d 547, 552 (1st Cir.) (characterising business methods as
abrtract Ideas), c~?rr. derGd, 398 U.S. 822 (1949).
that programs for computers are not ?as such? patent-able,
the European Patent Office has granted thousands
of software-related patents since its inception in 1984.
Similarly, thousands of software patents have been
granted in the United States despite the fact that mathe-matical
algorithms, standing alone, are not patentable.
However, the attempts by courts and Patent Offices
on both sides of the Atlantic to specify the criteria for
the patentability of software-related inventions--to
articulate the precise characteristics that computer pro-grams
must possess in order to warrant patent pro-tection-
have resulted in much confusion and
inconsistency in the law.? The difficulty has been in
specifying these criteria in a manner that avoids the
dilemma of being overly broad (that is of including
inventions that do not merit protection) or being overly
narrow (that is of excluding inventions that do). Where
patent protection is overly broad, extending to a mathe-matical
formula alone, for example, there is a concern
that it will diminish the incentive to innovate because
individuals will fear infringing another?s (overly broad)
patent. Where protection is overly narrow, by contrast,
the concern is that individuals will have little incentive
to invent new devices because their inventions may not
receive the protection of the patent laws.
In the United States, until recently, the law governing
the patentability of software-related inventions was just
as obscure as it is in Europe. The author says ?until
recently? because in State Street Bartk & Trust Co. ZI.
Sigltature Finunciul Group Z~K.,~ the Court of Appeals for
the Federal Circuit (the intermediate appellate court for
cases involving the U.S. patent laws) jettisoned the con-fusing
test for the patentability of software-related
inventions that had dominated the field for the past
several years. The Federal Circuit also rejected the
?business methods? exception to patentability. In State
Street, the Federal Circuit articulated a new test for the
patentability of software-related inventions that is both
easier to satisfy and to understand. On January 11,
1999, the U.S. Supreme Court (the appellate court of
last resort) declined to hear an appeal against the Fed-eral
Circuit?s decision, letting that decision stand.
Background
Title 35 of the United States Code, the federal statute
governing patent law in the United States, provides that
in order to be patentable an invention must satisfv cer-tain
criteria: it must constitute patentable subject-matter,
and it must also be novel, non-obvious and
adequately disclosed with adequate notice of what is
claimed as
The first
the invention.
hurdle for patentability, therefore, is that the invention claim patentable subject-matter. ?Only if it
does it is necessary to address any of the other factors
that bear on patentabilitv.
Patentable-subject-maiter is defined by section 10 1 of
Title 35 as ?any new or useful process, machine or
4 For a discussion of the confused state of the doctrine in
E u r o p e , see Jonathan Newman, ?The Patentability of
Computer-related Inventions In Europe? [ 19971 12 E.I.P.R.
7 0 1 .
5 149 F. 3d 1368, 1375 (Fed. C i r . 1998), cm. denied, 1 9 9 9 W .L . 8 6 0 1 (J a n u a r y 11, 1999).
llU991 E I PR. ISSUE 6 c? SWEET & MAXWELL LIMITEI~ [AND CONTRIBU1?0RS.1
COMMENTS: 119991 E.I.P.R. 331
Court has stated that Congress-in defining patentable
manufacture, or composition of matter, or any new and
subject-matter in section lOl-intended to ?include
anything under the sun that is made by man?,? the
useful imnrovement thereof?. While the U.S. Supreme
courts have made it clear that certain subject-matter is
not patentable, including ?laws of nature, natural phe-nomena,
and abstract ideas?.? Such discoveries are not
patentable because they are ?manifestations of. .
nature, free to all men and reserved exclusively to
none?.? Mathematical algorithms and business methods
have been held not to be patentable on the ground that
they constitute abstract ideas or laws of nature.?
In order to understand the evolution of U.S. law gov-erning
the patentability of computer software programs,
it is necessary to begin with Gottschalk v. Benson,?? the
first case in which the U.S. Supreme Court considered
the patentability of computer software. In thar case, the
Supreme Court found that the invention at issue, which
involved the conversion of binary coded numbers in
decimal form to pure binary form, did not constitute
patentable subject-matter. It characterised the inven-tion
as one involving the ?mathematical problem of
converting one form of numerical representation to
another?.? The court stated that since ?[t]he mathe-matical
formula involved [had] no substantial practical
application except in connection with a digital com-p
u t e r . the patent would wholly pre-empt the mathe-matical
formula and in practical effect would be a
patent on the algorithm itself?.? The Supreme Court
also suggested that ?[tlransformation and reduction of
an article ?to a different state or thing? is the clue to the
patentability of a process claim that does not include
particular machines?. ? ?
court stated that if the applicant was seeking patent
protection for a mathematical formula in the abstract,
nonstatutory simply because it uses a mathematical for-then
the invention was not patentable. If, however, a
claim ?implements or applies that formula in a struc-
mula, computer program or digital computer.?? The
lure or process which, when considered as a whole, is
performing a function which the patent laws were
designed to protect (e.g. transforming or reducing an
article to a different state or thing)?, then it is patent-able.
I ? )
After the Supreme Court?s decisions in Benson and
Flook, the Court of Customs and Patent Appeals (the
predecessor to the Federal Circuit as the intermediate
court of appeals) articulated a test for the patentability
of computer software that came to dominate the field
over the last two decades. The test was articulated in a
trilogy of cases-Zn re Freeman,?? In re Waite?? and In re
Abele?>-and came to be known as the Freeman-Walter-Abele
test. This two-part test for patentability addresses
the following questions: first, whether a mathematical
algorithm is recited directly or indirectly in the claim;
secondly, if so, whether the claimed invention as a
whole is no more than the algorithm itself, that is
whether the claim is directed to a mathematical algo-rithm
that is not applied to or limited by physical ele-ments
or process steps.
In Purker v. Flook,? ? the next case to be considered by
the Supreme Court, the majority held that the inven-tion,
which involved the use of a mathematical formula
for updating an alarm limit, did not constitute patent-able
subject-matter because a mathematical formula
?cannot support a patent unless there is some other
inventive concept in its application?. ? The dissent criti-cised
the majority?s decision on the ground that it was
importing into the inquiry as to whether a claimed
invention was of the appropriate statutory subject-matter
the criteria of novelty and non-obviousness.??
In recent years, the Freemaw Walter-Abele test, as well
as the view that there is a ?mathematical algorithm?
exception to patentability, have been subject to criti-cism.
In Arrythmia Research Technology, Inc. v. Corazonix
Corp.,? which involved a software program fo monitor
human heart activity, Judge Rader (in a concurring
opinion) questioned the mathematical algorithm excep-tion
to patentability, stating that it is based on the mis-taken
view that algorithms used by computers represent
natural laws.?? In the subsequent case of Zrz re AZupput,*?
which involved a software program thar could transform
numerical values in a manner that creates a smooth
display of data on an oscilloscope, the majority also
called into question the mathematical algorithm excep-tion.
The majority stated that a close analysis of Benson,
Flook and Diehr ?reveals that the Supreme Court never
intended to create an overly broad, fourth category of
subject matter excluded from section lol?.?? More-over,
in Aluppat, the majority did not even apply the
Freemuu-Walter-Abeie test. It stated, rather, that ?the
proper inquiry in dealing with the so called mathemat-ical
subject matter exception to § 101 . . is to see
whether the claimed subject matter as a whole is a dis-embodied
mathematical concept?-in which case it is
not patentable-or whether the invention produces ?a
Diumond v. Diehr?--the Supreme Court?s most
recent decision on the patentability of software-related
inventions-involved a computer program for curing
synthetic rubber, which the court found to be patent-able.
The Supreme Court stated that ?a claim drawn to
subject matter otherwise statutory does not become
6 S.Rep. No. 1979, 82d Cong., 2d Sess., 5 (1952); H.R.Rep.
No. 1923, 82d Gong. 2d Sess., 6 (1952). U.S.C.C.A.N. 1952,
pp. 2394, 2399.
I Diumorrd 0. Diehr, n. 2 above.
8 Diamond a. Chakrabarry, 447 U.S. 303, 309 (1980) (citation
useful,
is.?
concrete and tangible result?- -in which case it
o m i t t e d ).
9 See s~pru nn. 2 and 3 above.
10 409 U.S. 63, 67 (1972).
11 ibid., 65.
12 ibid., at 71-72.
13 ibid., at 70.
14 437 U.S. 584 (1978).
15 ibid., at 594.
16 ibid.. at 600.
1 7 I-L. 2?above
18
19
2 0
2 2
2 3
24
2 5
2 6
2 7
ibid., at 187.
ibid., at 191-192.
573 F. 2d. 1237 (C.C.P.A. 1978)
6 1 8 F . 2 d 7 5 8 (C.C.P.A. 1980).
6 8 4 F . 2 d 9 0 2 (C.C.P.A. 1982).
958 F. 2d 1053 (Fed. Cir. 1992).
ibid., 1066, n. 3.
33 F. 3d 1526 (Fed. Cir. 1994).
ibid., at 1543.
ibid., at 1544.
332 (:OM.UENTS: 119991 t.1.P.R.
The District Court?s Decision in State Street:
?physical Transformation?
ln ~fa[~ .Q~c<z, Signature Financial Group, Inc. was the
assignee of a patent involving a ?Hub and Spoke? data
processing system. This system permitted individual
mutual funds (Spokes) to pool their assets in an invest-ment
portfolio (Hub) organ&d as a partnership. This
pooling system yielded economies of scale and had cer-tain
tax advantages. The invention at issue involved a
computer softuarc program which was able to admin-istcr
the Hub and Spoke system by making certain com-plicated
calculations with respect to each of the Spokes?
share of the Hub?s assets and expenses.?
State Street entered into negotiations to license the
technology from Signature. After negotiations broke
down, State Street commenced a declaratory judgment
action to invalidate the patent for failure to claim statu-tory
subject-matter under section 101.
The U.S. District Court for the District of Massachu-setts
(the court of first instance) applied the two-step
t e s t o f I:rcemm- IVtdzer-Abele. T h e D i s t r i c t C o u r t
answered the first question raised by that test in the
affirmative, finding that the patent did recite a mathe-matical
algorithm. The court then turned to the second
question whether the algorithm is ?applied to or iim-ited
by any physical elements or process steps?.
Relying on Benson, the District Court found that the
key to addressing the second part of the Free~nurr-Wulter-
Abe/c test lay in the concept of ?physical trans-formation?.
The court analysed and explained prior
decisions involving the patentability of software-related
inventions in terms of the concept of physical trans-formation;
only those inventions which caused some
type of shysicai transformation were patentable. For
example, it found that the software invention in Aluppat
was patentable because the result of the invention was
physically to transform the representation of a wave-form
on an oscilloscope. In Arrhyzhnziu, the Federal
Circuit found that the software invention in issue was
patentable because it had the effect of physically trans-forming
electrocardiographic signals from a patient?s
heartbeat through various mathematical calculations in
order to determine a specified heart activity.??
The District Court summed up the concept of phys-ical
transformation in the following way: ?Regardless of
whether the invention performs mathematical opera-tions,
if it transforms or reduces subject matter to a
different state or thing, it is statutory under § 101.
While not conclusive, the clue is physical transforma-t
i o n .???
The court examined the Hub and Spoke software and
found that ?Signature?s invention does not involve [the]
type of transformation? required for patentability.?
The court stated that the invention ?involves no further
physical transformation or reduction than inputting
numbers, calculating numbers, outputting numbers,
and storing numbers?.? ?The court also stated: ?A
change of one set ot numbers mto another, without
more, is msuthcient to confer patent protecti~~n. ?l?he
invention does nothing other than present and solve a
mathemattcal algorithm and, therefore, is not patent-able.??
The District Court also found that the invention in
question was not patentable because it was a ?business
method?. In support of this finding the District Court
noted that if Signature?s Invention were patentable, then
?any financial institution desirous of implementing a
multi-tiered funding complex modeiied on a IIub and
Spoke configuration would be required to seek permis-sion
before embarking on such a project?. ?.?
The Federal Circuit: ?A Useful Concrete and
Tangible Result?
The Federal Circuit reversed the District Court, hoid-ing
that the Hub and Spoke software did qualify as
patentable subject-matter under section 101.
In the course of its decision, the Federal Circuit
stated that the t;reelrraiz-~~lzer-,4belr test ?has little, it
any, applicability to determining the presence of statu-tory
subject matter?. ? It stated, rather, that patent-ability
turned on whether an invention produced a
?useful, concrete and tangible result?. ?h Thus, whereas
the lower court explained the holdings of Alrcppnt and
,ilrrhvzhnziu by reference to the concept of ?phvsical
transformation?, the Federal (Circuit did so in terms of
the concept of a ?useful, concrete and tangible result?.
Thus it stated that, in Aluppaz, the invention ?consti-tuted
a practical application of an abstract idea (a math-ematical
algorithm, formula or calculation), because it
produced ?a useful concrete and tangible rest&---the
smooth waveform?.? Similarly, the Federal Circuit
explained Arrhyhnziu by noting that the invention ?con-stituted
a practical application of an abstract idea .
because it corresponded to a useful, concrete or tan-gible
thing- the condition of a patient?s heart?. ?x
The Federal Circuit applied this test to the Hub and
Spoke software and found that it constituted ?the prac-tical
application of a mathematical algorithm, formula
or calculation, because it produces ?a useful, concrete
and tangible result?--a tinal share price momentarily
fixed for recording and reporting purposes and even
accepted and relied upon by regulatory authorities in
subsequent trades?. I?
In the course of its decision, the Federal Circuit
directly addressed the lower court?s holding that
numerical calculations alone cannOt constitute patent-able
subject-matter. ?The ?mere fact that a claimed
invention involves inputting numbers, calculating num-bers,
outputting numbers, and storing numbers, in and
of itself, would not render it nonstatutory subject mat-ter,
unless, of course, its operation does not produce a
?useful, concrete and tangible result? ?_ ?I
3 3 rbid., a t 5 1 4 .
34 ibld., at 516.
3 5 Stat Srrcer BCIII~ ,?? fimr Co. ~1. S~pmrw I;mrlm<,l group
hc., n. 5 nbove, at 1374.
36 zbrd., at 1374.
37 ibid., 1373.
3 8 IbId.
3 9 rbrd., at 1 3 7 3 .
4 0 rbui., at 1374.
1
COMMENTS: [1999] E.I.P.R. 333
l?he Federal Circuit also took the opportunity to jet-tison
the ?business methods ? exception to patentability.
Specincally, it tound the concept to be superfluous. It
found that in every case in which an invention was
found to be an unpatentable business method, there
was a prior tinding that the invention was unpatentable
for another reason, most notably that it was an unpa-tentable
mathematical algorithm. The Federal Circuit
concluded that by noting that the question of whether
?claims are directed to subject matter within $ 101
should not turn on whether the claimed subject matter
does ?business? instead of something else?. ?
Conclusion
lratcntable sublect-matter, be novel and non-obvious.
In Europe and the United States, software-related
mvcntions have had to pass a convoluted and, in many
cases, redundant series of tests to clear the first hurdle.
In Srure Sneer, the Federal Circuit has lowered the first
hurdle for the patentability of software-related inven-tions,
discarding thcx confused tests for patentability
under section 101. ?The real question is no longer
whether software-related inventions claim patentable
subject-matter, but, rather, whether such inventions are
novel and non-obvious.
It seems clear that, after Szure Street, software-related
inventions will be easier to patent in the United States
than in Europe.
An inventioii must clear several hurdles to secure the
protectioii of the? patent laws--~ the invention must claim
4 1 ibd, ?I, I 377
JOHN FEI,LAS
Hughes Hubbard & Reed LLP
New York (:ity