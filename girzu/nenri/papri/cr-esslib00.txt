<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">

COMPUTERRECHT
ALEXANDER ESSLINGER & JÜRGEN BETTEN

Patentschutz im Internet

Die jüngste Entwicklung der Rechtsprechung in Deutschland und Europa hat die Erlangung und Durchsetzung von Softwarepatenten erheblich erleichtert. 
Industrie- und Dienstleistungsunternehmen sind daher gezwungen, über eine patentrechtliche Absicherung ihrer Investitionen in neue Software nachzudenken, nicht zuletzt deshalb, um  sich über Kreuzlizenzen Zugang zur patentgeschützten Technologie anderer verschaffen zu können. 
Bei der Auswahl der geeigneteren Patentstrategie ist zu berücksichtigen, daß sich Software im Gegensatz zu den meisten traditionellen technischen Gegenständen sehr leicht kopieren und über das Internet weltweit verbreiten läBt.

1. Einleitung

Bei der Diskussion von Rechtsfragen im Zusammenhang mit Internet und E-Commerce spielt das Patentrecht im Gegensatz zum Urheberrecht und dem durch die Domain-Name-Problematik betroffenen Markenrecht in Deutschland bisher nur eine untergeordnete Rolle. 
Dies wird sich durch geplante Gesetzesänderungen sowie bereits heute wirksame geänderte Rechtsprechung und Erteilungspraxis des Europäischen und Deutschen Patentamtes ändern, die die Erlangung und Durchsetzung von Softwarepatenten erheblich erleichtert. 
Die Nutzung und der Vertrieb von Software über das Internet wirft darüber hinaus neue patentrechtliche Fragestellungen bezüglich der Benutzung und möglichen Verletzung von patentgeschützter Software auf. 
Ein weiterer Trend kommt aus den USA: Dort sorgt die Patentierung von Geschäftsmethoden, insbesondere von Internetbasierten Geschäftsmodellen durch spektakuläre Patentverletzungsprozesse für Aufmerksamkeit.





11. Patentfähigkeit von Software-Erfindungen

1. Rechtsprechung und Erteilungspraxis in Europa und Deutschland

Nach Art. 52 Abs. 2 lit. c des Europäischen Patentübereinkommens (EPÜ) und $ 1 Abs. 2 Nr. 3 Patentgesetz (PatG) sind Programme für Datenverarbeitungsanlagen unter anderem zusammen mit mathematischen Methoden, Plänen, Regeln und Verfahren für gedankliche oder geschäftliche Tätigkeiten in einer Liste von Gegenständen enthalten, die nicht als Erfindungen anzusehen sind, für die Patente erteilt werden können.
Der darauf folgende Absatz im Gesetzestext stellt aber klar, daß die Patentfähigkeit der aufgezählten Gegenstände nur dann ausgeschlossen ist, wenn für diese Schutz "als solche" begehrt wird. Programme für Datenverarbeitungsanlagen (nach heutigem Sprachgebrauch Computerprogramme oder Computersoftware) sind daher nur »als solche" nicht patentfähig. 
Aus dieser interpretationsbedürftigen gesetzlichen Bestimmung, die etwa dem amerikanischen oder dem japanischen Patentgesetz fremd ist, leitet sich das weitverbreitete Vorurteil ab, Software sei überhaupt nicht oder nur in engem Zusammenhang mit bestimmten Hardwarekomponenten durch ein Patent Schützbar.

Das Europäische Patentamt und der Bundesgerichtshof haben das Gesetz vereinfacht ausgedrückt wie folgt ausgelegt: Nicht patentfähige Computerprogramme "als solche" sind nicht-technische Computerprogramme, während andererseits technische Computerprogramme patentfähig sind. l 
Die Rechtsprechung in Deutschland und Europa hat sich - mit einigen Umwegen - im Trend dahin gehend entwickelt, den Begriff des Technischen immer weiter auszulegen,? so daß heute bereits eine Vielzahl von Softwarepatenten vom Deutschen und Europäischen Patentamt erteilt worden sind.3

Das TRIPS-Übereinkommen (Trade Related Aspects of Intellectual Property Rights), das von allen Mitgliedsstaaten der Welthandelsorganisation (WTO) unterzeichnet wurde, sieht in Art. 27 Abs. 1 vor, daß »Patente für Erfindungen auf allen Gebieten der Technik« bei Erfüllung der sonstigen Patentierungserfordernisse gewährbar sein müssen. Eine international kompatible Interpretation der TRIPS-Bestimmungen kommt zu dem Ergebnis, dass auch Software-Erfindungen der Technik zuzuordnen sind und damit dem Patentschutz zugänglich sein müssen.4 
Was verbleibt dann als »Computerprogramm als solches« ? 
In der neueren Rechtsprechung und im Schrifttum werden zwei Interpretationen vorgeschlagen.  
Computerprogramme als solche werden entweder als die abstrakte, gedanklich-logische Konzeption eines Computerprogrammes angesehen5
oder als der urheberrechtlich geschützte Sourcecode

* Patentanwalt Dr. Alexander Esslinger und Patentanwalt Jürgen
Betten sind Partner in der Kanzlei Betten & Resch in München.
1) CR 1986,537-VICOM; CR 1995,208 - SOHEI; BGH GRUR
1980,849 - Antiblockiersystem; BGH GRUR 1992,33 - Seiten-puffer;
BGH, NJW 92,374 - Chinesische Schriftzeichen; BGH,
GRUR 1992,430 -Tauchcomputer.
2) Überblick in Betten, GRUR 1995, 775; Taucher& GRUR 1997,
149 und Mitt. 1999,248; Mefulfis, GRUR 1998, 843; Schmidr-chen,
Mitt. 1999,284.
3) Schöniger, c?t 1611999, 72.
4) Betten, GRUR 1995, 775 (778); Schiuma, GRUR 1998, 852.
5) Melullis, GRUR 1998, 843 (852); EPA Amtsblatt 1999, 609 (619) - IBMXomputerprogrammprodukt 1.
CR 1/2000
ESSLINGER/BETTEN: PATENTSCHUTZ

bzw. Objektcode.6 
Bei dem ersten Ansatz ist ein patentfähiges Computerprogramm die praktische Anwendung der gedanklich-logischen Konzeption, bei dem zweiten Ansatz die Funktionalität. 
Diese beiden entgegengesetzten Ansätze fuhren jedoch zu dem übereinstimmenden Ergebnis, da@ praktisch alle Computerprogramme als technisch anzusehen und patentfähig sind. 
In Rechtsprechung und Literatur setzt sich mehr und mehr die Auffassung durch, einen Computer als eine Maschine und daher als technisch anzusehen.? 
Die für das Funktionieren des Computers unerlässlichen Programme müssen dann auch technisch sein. 
Die Prüfung, ob eine konkrete Software-Erfindung schutzfähig ist, verlagert sich daher von der Frage der Technizität zu der Frage der erfinderischen Tätigkeit, d.h. der Frage, ob das Computerprogramm dem Fachmann durch den Stand der Technik nahegelegt ist.
Der für die Beurteilung der Patentfähigkeit einer Software-Erfindung heranzuziehende Fachmann ist ein Programmentwickler.
Es ist daher zu prüfen, ob einem Programmentwickler oder Programmierer eine Software-Erfindung durch den öffentlich zugänglichen Stand der Technik nahegelegt ist.
Dieser Argumentation folgend hat das Bundespatentgericht in einer jüngeren Entscheidung? einen Patentanspruch als nicht auf erfinderischer Tätigkeit beruhend zurückgewiesen, da dieser eine Software beschrieben hat, die lediglich die Umsetzung einer bekannten Geschäftsmethode in ein Computerprogramm darstellt.
Nur wenn neue und nicht nahegelegte programmtechnische Mittel (beispielsweise Berechnungs-, Speicher- oder Codierverfahren) beschrieben und im Patentanspruch beansprucht werden, wird das Computerprogramm dadurch patentfähig.

2. Rechtsprechung in den USA

In den USA haben Patente auf Geschäftsmethoden seit einer grundlegenden Entscheidung des US-amerikanischen Beschwerdegerichts für Patentsachen (CAFC) eine hohe Popularität erlangt. Die Zahl auf derartige Gegenstände gerichteter Patentanmeldungen ist schlagartig angestiegen. Zwei Patentverletzungsprozesse sorgen für besonderes Aufsehen: Im ersten Verfahren zwischen Patentinhaberin priceline.com und Microsoft geht es um die Verletzung eines Patentes, das das computergestützte Verfahren einer sogenannten umgekehrten Auktion schützt, bei der ein Kunde für ein bestimmtes Produkt (beispielsweise Flugtickets) einen bestimmten Kaufpreis eingibt, den er zu zahlen bereit ist. Eine Mehrzahl von Anbietern (beispielsweise 

6) Taucherr, GRUR 1997, 149 (154) und Mitt. 1999,248.
7) Melullis, GRUR 1998, 843; Schmidrcben, Mitt. 1999,281.
8) Mefdis, GRUR 1998, 843 (853); BParG GRUR 1999, 1078 -Automatische
Absatzsteuerung.
9) Vgl. (FN 8).
10) GRUR Int. 1999, 633 >,State Street Bank<<; Anm. dazu Esslin-gerlHössle,
Mitt. 1999, 327.
11) US Patent No. S, 794,207, zugänglich unter htrp://www.uspro.
gov.
12) US-Patent-Nr. 5,960,411.
13) EP-Patent 762304.

Fluggesellschaften) prüft die Gebote und verkauft einem Kunden das Produkt, wenn der gebotene Preis dem Anbieter akzeptabel erscheint. 
Im zweiten Rechtsstreit geht es um die Verletzung eines Patentes von Amazon.com durch barnesandnoble.com, das den Kauf von Produkten/Dienstleistungen an einen identifizierten Käufer mittels eines einzigen Mausklicks erlaubt.
Der Ausgang dieser Verfahren, in denen auch über die Gültigkeit der jeweiligen Patente entschieden wird, kann mit Spannung erwartet werden.
Fraglich ist, wie derartige Erfindungen von den Gerichten in Deutschland und Europa beurteilt werden. Geschäftsmethoden sind ebenso wie Computerprogramme "als solche" von der Patentierung ausgeschlossen.
Nach der oben geschilderten Sichtweise ist die reine Umsetzung eines bekannten Geschäftsverfahrens in ein Computerprogramm (beispielsweise Handel von Wertpapieren per Computer statt auf dem Parkett) oder die Wahl eines neuen Vertriebsweges (beispielsweise Verkauf von Büchern über das Internet) keine patentfähige Erfindung, da der Programmierer nur bekannte Methoden einsetzen muss, um das bekannte Geschäftsverfahren in Computercode mit den gewünschten Funktionen umzusetzen. 
Eine völlig neue Geschäftsidee, die bisher weder als Computerprogramm noch auf andere Weise existiert, ist aber unabhängig von erfinderischen programmtechnischen Mitteln patentfähig, da die nicht bekannte (und auch in ähnlicher Form nicht bekannte) Geschäftsmethode dem Programmierer als dem zur Beurteilung der erfinderischen Tätigkeit heranzuziehenden Fachmann nicht nahegelegt sein kann.
Aber auch in Europa gibt es bereits erteilte Patente auf computergestützte Geschäftsverfahren, beispielsweise ein Patent der Citibank,?? das ein Verfahren zum Optionsscheinhandel per Computer schützt, und gegen das eine Vielzahl von Banken Einspruch erhoben haben.

111. Durchsetzung von Patenten im Internet

1. Ansprüche des Patentinhabers nach PatG

Das Patentgesetz gibt dem Patentinhaber das exklusive Recht an der Benutzung der patentierten Erfindung. 
Der Patentinhaber kann es Dritten verbieten, die Erfindung gewerblich zu nutzen, oder er kann für die Nutzung Lizenzgebühren verlangen. 
Eine Patentverwertung kann somit entweder durch Ausnutzung der auf 20 Jahre nach dem Anmeldetag zeitlich begrenzten Monopolstellung oder durch Lizenzierung der Erfindung an andere erfolgen. 
In komplexen Technologiefeldern, bei denen die Durchsetzung eines "Standards" häufig Voraussetzung für einen Markterfolg beim Konsumenten ist, wie beispielsweise in der Unterhaltungs-elektronik, der Telekommunikation oder dem Internet, sind Kreuzlizenzen eine häufige und praktikable Form der Patentverwertung geworden: Mit eigenen Patenten 

COMPUTERRECHT

"als Währung" erwirbt man Zugang zu Technologien, die von Mitbewerbern patentgeschützt sind.
 
Eine erfolgreiche Patentverwertung ist jedoch nur dann möglich, wenn der Patentinhaber sein Recht im Streitfall gegen einen möglichen Verletzer durchsetzen und Unterlassungs- und Schadensersatzansprüche erfolgreich einklagen kann. 
Der vom Patentschutz umfaßte Gegenstand wird durch die Patentansprüche definiert, für deren Formulierung bei der Abfassung eines Patentes durch den Patentanwalt daher besondere Sorgfalt aufgewandt werden muß.
Ein Dritter verletzt das Patent, wenn er alle Merkmale eines Patentanspruches benutzt.
Nach §9 PatG gibt es grundsätzlich zwei Arten von schutzfähigen Gegenständen, Erzeugnisse und Verfahren. Zu den Erzeugnissen zählen insbesondere Vorrichtungen (Maschinen, Apparate, Produkte) und Stoffe wie beispielsweise chemische Verbindungen. Die Verfahren umfassen Arbeitsverfahren, Herstellungsverfahren und Verwendungen. In welche Kategorie lässt sich Computersoftware einordnen? 
Einerseits in Verbindung mit der Hardware als Computersystem, das heißt als Vorrichtung, und andererseits als Verfahren zur Ausführung auf einem Computer, das eine bestimmte Funktion erfüllt. 
Beide Patentkategorien haben für den Inhaber eines Softwarepatentes gravierende Nachteile. Das Computersystem-Patent schützt die Software nur in Verbindung mit der Hardware. 
Ein Hersteller, der die Software unabhängig vom Computer vertreibt, kann in der Regel nicht belangt werden und begeht dann allenfalls eine mittelbare Patentverletzung, die wesentlich schwieriger zu verfolgen ist. 
Das Verfahrenspatent hat den Nachteil, daß nur die Anwendung des Verfahrens und, wenn diese widerrechtlich ist, das Anbieten zur Anwendung rechtswidrig ist.
Wenn der Käufer des Computerprogrammes jedoch ein Endverbraucher ist, dessen private, nicht gewerbliche Nutzung des als Verfahren patentgeschützten Programmes zulässig ist, kann gegen den Anbieter dann auch nicht vorgegangen werden.

2. Patentverletzung » online «

Die für Softwarepatente zuständige Beschwerdekammer 3.5.1 des Europäischen Patentamtes hat in zwei neueren Entscheidungen16 den Weg frei gemacht für einen besseren Schutz von Computerprogrammen. 
Gemäß diesen Entscheidungen ist ein Computerprogramm selbst als Produkt schützbar. 
Es sind Ansprüche auf ein Computerprogramm bzw. Computerprogrammprodukt sowohl auf einem Speichermedium als auch unabhängig davon zulässig. 
Dabei unterstreicht die aus der amerikanischen Praxis stammende Formulierung »Computerprogrammprodukt« die Tatsache, daß das Computerprogramm selbst als Erzeugnis, nicht als Verfahren geschützt ist. 
Ähnlich wie bei einer chemischen Verbindung, die ihre technische Wirkung erst bei einer Anwendung in einem chemischen Verfahren entfaltet, enthält das Computerprogramm eine potentielle technische Wirkung, die sich erst beim Ablaufen auf einem Computer oder einem vernetzten Computersystem entfaltet. 
Der Patentinhaber hat also den vollen Schutz eines Erzeugnispatentes.
Das im Handel erworbene, auf CD gespeicherte Computerprogramm wird patentrechtlich nicht anders behandelt als beispielsweise ein Fernsehgerät oder ein Mobiltelefon. 
Die von einem Speichermedium unabhängigen Computerprogrammansprüche sind insbesondere dann nützlich, wenn der Patentinhaber gegen einen Vertrieb seines patentgeschützten Programmes durch »Downloaden« über das Internet vorgehen will. 
Gerade in diesen Fällen tritt jedoch ein weiteres Problem auf, da im Patentrecht das Territo-rialitätsprinzip gilt, das heißt der Patentschutz ist territorial auf diejenigen Länder beschränkt, in denen der Erfinder oder sein Rechtsnachfolger ein Patent angemeldet und nach Erteilung weiter verfolgt hat.
Nationale Barrieren sind beim E-Commerce jedoch leicht überwindbar. 
Der Patentverletzer könnte ein patentgeschütztes Programm von einem im patentfreien Ausland befindlichen Server aus über das Internet weltweit zum Downloaden anbieten. 
Wenn die Software jedoch als Erzeugnisanspruch geschützt ist, stellt nach §9 Nr. 1 PatG schon das Anbieten der patentgeschützten Software eine Patentverletzung dar.
Nach geltender Rechtsprechung*? ist bei Angeboten im Internet für den Ort der deliktischen Handlung nicht auf den Ort abzustellen, an dem die reale Einrichtung der Homepage erfolgt oder an dem der Server steht, sondern als Begehungsort kommt grundsätzlich jeder Ort in Betracht, an dem die Homepage bestimmungsgemäß abgerufen werden kann und einen Rechtsverstoi;: bewirkt. 
Das Anbieten einer in Deutschland patentgeschützten Software über eine Internet-Seite, die (auch) an deutsche Verbraucher gerichtet ist, stellt unabhängig von dem Ort des Servers (an dem die Homepage installiert ist), eine Patentverletzung dar. Falls der Patentverletzer in Deutschland oder einem anderen vom Patentschutz erfaßten Land keine Niederlassung hat, ist eine Verfolgung der Patentverletzung trotzdem schwierig, da Vernichtungs-ansprüche oder eine Zollbeschlagnahme bei über das Internet vertriebenen Softwareprogrammen natürlich ins Leere laufen. 
Es wird jedoch verhindert, daß ein im Inland ansässiger Anbieter den Patentschutz einfach dadurch unterlaufen kann, dass er die geschützte Software von einem im patentfreien Ausland angesiedelten Server über das Internet vertreibt. 
Der Patentanmelder wiederum muß darauf achten, daß er Patentanmeldungen in den Ländern tätigt, in denen die Hauptkonkurrenten Sitz oder Niederlassung besitzen.
Aufgrund der neueren Entwicklung der Rechtsprechung stellt somit das Internet auch in bezug auf das Patentrecht keinen rechtsfreien Raum mehr dar.

14) KearseylMacNaughton, Managing Intellectual Property, September 1999, 16.
15) BenkardlBruchhausen, PatG, S 9 RZ. 51.
16) EPA Amtsblatt 1999,609 - IBMKomputerprogrammprodukt 1
und T 0935/97.
17) OLG FrankfurdM. CR 1999,450 - Anbieten im Internet. CR 1/2000

IV. Softwarepatente und »Open Source « -Bewegung

1. Patentschutz contra Programmierfreiheit 

Die zunehmende Stärkung des Patentrechts für Erfindungen im Softwarebereich löst in Kreisen der Open Source-Gemeinde eher ablehnende Reaktionen aus.18
Die Programmierer fürchten um ihre Programmierfreiheit und die Vertriebs- und Servicefirmen für Open Source-Software um ihr Geschäft. 
Die Kritik am Patentrecht entzündet sich daran, daß der Patentschutz "absolut"ist und auch gegen spätere Zweiterfinder schützt, während das Urheberrecht nur das Kopieren eines Programmes oder von Teilen davon verbietet.
Das Patentrecht, das dem Erfinder unzweifelhaft eine starke Rechtsstellung verleiht, enthält jedoch auch wirksame Bestimmungen zur Begrenzung der Rechte des Patentinhabers. 
Zum einen hat derjenige, der die Erfindung schon vor der Patentanmeldung benutzt, aber nicht um ein eigenes Patent nachgesucht hat, nach §12 PatG ein Vorbenutzungs- bzw. Weiterbenutzungsrecht an seiner Erfindung. 
Der später anmeldende Patentinhaber kann sein Patent also gegen den früheren Ersterfinder nicht geltend machen. 
Außerdem erstreckt sich nach $ 11 PatG die Wirkung des Patents nicht auf Handlungen, die im privaten Bereich zu nicht gewerblichen Zwecken vorgenommen werden, sowie Handlungen zu Versuchszwecken, die sich auf den Gegenstand der patentierten Erfindung beziehen. 
Der Programmierer, der zu Hause und ohne kommerzielle Absicht ein Programm schreibt, braucht auf eventuellen Patentschutz nicht zu achten. 
Dies ändert sich jedoch, wenn die Privatsphäre verlassen wird, ein Programm also etwa kostenlos im Internet zum Herunterladen angeboten wird.

Das wirksamste Mittel, einen Patentschutz zu verhindern, ist die Veröffentlichung der Erfindung, beispielsweise im Internet. 
Genauso wie das Patent einen absoluten Schutz gegenüber Nachahmung bietet, ist eine Vorveröffentlichung ein absoluter Schutz vor Patentierung, mit der Ausnahme, dass in einigen Ländern wie den USA und Japan der Erfinder eine Frist von 12 Monaten (6 Monaten) nach der Erstveröffentlichung der Erfindung hat, um eine Patentanmeldung einzureichen.
Der Programmierer hat daher die Möglichkeit, selbst zu entscheiden, ob er ein Patent anmelden möchte oder nicht. 
Wenn er den Sourcecode durch Zugänglichmachen über das Internet veröffentlicht, ist in dem Moment eine Patentierung von in dem Programm enthaltenen Erfindungen für Dritte nicht mehr möglich.
Wie im zweiten Abschnitt erläutert wurde, erfaßt das Patentrecht nicht den Code selbst, sondern die einem Programm zugrundeliegende erfinderische Funktionalität.
Es ist also nicht so, daß ein Programmierer bei jeder Routine, die er schreibt, fürchten muss, irgendein

18) Computerwoche 43/!999,4.
19) Auskunft des Europäischen Patentamtes.
CP 1/2000
ESSLINGER/BETTEN: PATENTSCHUTZ

bestehendes Patent zu verletzen. Erst wenn das Programm eine bestimmte patentierte Funktion auf die im Patentanspruch beschriebene Art und Weise erfüllt, liegt eine Patentverletzung vor. 
Umgehungserfindungen, d.h. die Lösung des gleichen Problems mit anderen als den im Patent beschriebenen Mitteln, stellen keine Patentverletzung dar, sondern sind häufig sogar eine Quelle neuer Innovationen.

2. Rechercheproblem bei Softwareerfindungen

Ein Problem stellt die schwierige Recherchierbarkeit von Softwareerfindungen dar, da in den Datenbanken der Patentämter in diesem vergleichsweise jungen Gebiet noch nicht sehr viel leicht recherchierbare, klassifizierte Patentliteratur vorhanden ist. 
Dies hat zur Erteilung von zu breiten Patenten auf dem Softwaresektor geführt und wird sich aber spätestens dann ändern, wenn ein größerer Fundus an klassifizierter Patentlite-ratur vorhanden ist. 
Ähnliche Probleme hat es vor ca. 20 Jahren auf dem damals jungen Gebiet der Biotech-nologie gegeben. Mittlerweile zählen Biotechnologie-Erfindungen zu den am besten recherchierbaren. 
lc) 
Dar-über hinaus gibt es in Deutschland und Europa anders als etwa in den USA die Möglichkeit des Einspruchs, in dem jedermann eine Überprüfung der Patentfähigkeit eines bereits erteilten Patentes beantragen kann.
Die Rechercheproblematik illustriert den eigentlichen Zweck des Patentrechtes, nämlich die Bereitstellung der Erfindungen und Innovationen für die Allgemeinheit.
Um dem Erfinder einen Anreiz zu geben, seine Erfindung nicht geheimzuhalten, sondern der Allgemeinheit preiszugeben, wird ihm als Gegenleistung ein auf 20 Jahre begrenztes Monopolrecht gewährt. 
Der Zweck, dem das Patentrecht dient, und der, den die Open Source-Gemeinschaft verfolgt, ist somit prinzipiell der gleiche, nämlich die weltweite Verbreitung von Innovation.
Die Open Source-Bewegung versucht dieses Ziel auf freiwilliger Basis zu erreichen, während im Patentrecht der Erfinder Anspruch auf eine Belohnung hat. 
Nach Überwindung der Rechercheprobleme (auch unter Einsatz umfangreicher Datenbanken und moderner Suchsoftware) und bei sachgerechter Beurteilung des Patentierungskriteriums "erfinderische Tätigkeit" sollte das Patentrecht auf dem Gebiet der Software, wie in anderen Technologiefeldern auch, zu einem gerechten Interessenausgleich zugunsten der Förderung von Innovation führen.
Neben den Interessen der Entwickler und Vermarkter freier Software sind auch die Interessen derjenigen Unternehmen, insbesondere kleinerer und mittlerer Unternehmen, zu berücksichtigen, die auf eine kommerzielle Verwertung der unter hohem Entwicklungsaufwand erstellten Software angewiesen sind. 
Hätte beispielsweise Netscape einen wirksamen Patentschutz für seinen innovativen Browser gehabt, hätte es sich wohl nicht wehrlos dem Marketinginstrument kostenloser Software des marktstärkeren Wettbewerbers Microsoft beugen müssen.

2 1
COMPUTERRECHT

V. Rückblick und Ausblick

Der Ausschluß von "Computerprogrammen als solchen" vom Patentschutz in Art. 52 EPÜ (§1 PatG) wird seit langem als rechtspolitische Fehlentwicklung angesehen, zumal der Ausschluß von breiten Verkehrskreisen - bis heute1° - mißverstanden und meist als Ausschluß von Computerprogrammen allgemein verstanden wird.
Für die europäische Diskussion um den Patentschutz von Computerprogrammen kam die entscheidende Wende durch das am 15.12.1993 abgeschlossene GATT/TRIPS-Abkommen, da durch dieses ein internationaler Standard gesetzt wurde. In diesem Abkommen ist in Art. 27 ausdrücklich vorgesehen, daß der Patentschutz auf allen technischen Gebieten (also auch auf dem Gebiet der Computer-Software) verfügbar sein muß. 
Um die europäischen Patentgesetze mit denen in den USA und Japan und mit Art. 27 TRIPS in Einklang zu bringen, wurde daher schon bald?? die Streichung der Computerprogramme aus Art. 52 EPÜ (§1 PatG) gefordert, zumal diese Regelung die europäische Software-Industrie von der Anmeldung von Softwareerfindungen abschreckte13 und damit zum Schaden der europäischen Software-Industrie gereichte, während die amerikanische und japanische Industrie fleißig Softwarepatente anmeldete und von den inzwischen über 13.000 erteilten europäischen Software-Patenten über 75% besitzen dürfte.
Weiteren Auftrieb erhielt die europäische Diskussion durch die Resolution der AIPPI zur Frage Q133 vom 22.4.1997 in Wien, die in dem Satz zusammengefaßt werden kann: "Alle auf einem Computer ablauffähigen Programme sind technischer Natur und daher patentfähig, wenn sie neu und erfinderisch sind", sowie den Round Tablel? der UNION am 9./10.12.1997, als im  Europäischen Patentamt 100 Fachleute aus zwanzig europäischen Ländern über die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem ähnlichen Ergebnis kamen wie die AIPPI. Zudem wurde darauf hingewiesen, daß das Konzept des EPA zum "technischen Charakter" weder von den Patentanmeldern noch von den nationalen Patentämtern richtig verstanden würde. Viele Teilnehmer machten klar, daß eigentlich alle Computerprogramme dem Wesen nach "technischen Charakter" aufweisen würden.
Seit dieser Zeit wird praktisch "auf allen Kanälen" daran gearbeitet, einen Weg zu finden, wie der irreführende Ausschluß von "Computerprogrammen als solchen" aus den europäischen Patentgesetzen entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregelungen in Art. 52 Abs. 2 EPÜ (§1 Abs. 2 PatG) zur Disposition stehen.
Im November 1998 hat das Europäische Parlament2s die Patentfähigkeit von Computerprogrammen befürwortet, sofern das betreffende Produkt die Anforderungen an eine technische Erfindung im Hinblick auf Neuheit und Anwendbarkeit erfüllt, wie dies bei den Handelspartnern in den USA und Japan der Fall ist. Im Follow-Up-Papier zum Grünbuch über das Gemeinschaftspatent hat die Europäische Kommission festgestellt, daß rund 75% der ca. 13 .OOO europäischen Software-Patente im Besitz außereuropäischer Großunternehmen sind und die kleinen und mittleren Unternehmen (und deren Berater!) "schlichtweg nicht wissen, daß es möglich ist, für Software-Erfindungen Patentschutz zu erlan-gen".
Die Kommission sieht daher Handlungsbedarf und plant sowohl eine Informationskampagne als auch eine entsprechende Regelung durch eine Richtlinie zur Harmonisierung der nationalen Patentgesetze bezüglich des Patentschutzes von Computerprogrammen - ein erster Entwurf wird für Anfang 2000 erwartet -, durch die der bisherige Art. 52 EPÜ durch Art. 27 TRIPS ersetzt werden dürfte, d.h. »Computerprogramme als solche« und die anderen Ausschl&atbestände von Art. 52 Abs. 2 EPÜ nicht mehr erwähnt werden.
In eine ähnliche Richtung gehen die Bemühungen des EPA. Dieses hat am 10.3.1999 eine Streichung von Art. 52 Abs. 2 und 3 vorgeschlagen und der Ausschuss "Patentrecht" des Verwaltungsrates des EPA2? hat im März 1999 vorgeschlagen, Art. 52 Abs. 1 EPÜ mit dem TRIPS-Übereinkommen in Einklang zu bringen und aus Art. 52 Abs. 2 EPÜ zumindest die Computerprogramme zu streichen.
Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europäischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat2s erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EPÜ bezüglich des Ausschlusses von Computerprogrammen vorzulegen, so daß die geänderte Fassung vor dem 1.7.2002 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und wohl auch die anderen Ausschlussregelungen) aus Art. 52 EPÜ gestrichen sind.
Daneben bemüht sich die Rechtsprechung2? - wie ausgeführt -, die derzeitige Gesetzesregelung so eng auszulegen, daß praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentfähig sind, wenn sie neu und erfinderisch sind.
Da die Prüfung einer Patentanmeldung mindestens zwei bis drei Jahre dauert, zwingt diese Entwicklung bereits heute alle Berater dazu, ihre Mandanten darauf hinzuweisen, daß grundsätzlich alle Computerprogramme patentfähig sind und der Patentschutz - trotz der irreführenden gesetzlichen Regelung - nicht nur für Computerprogramme, sondern alle Innovationen im Internet genutzt werden kann. 

20) Heussen in Süddeutsche Zeitung v. 21.10.1999,22.
21) GRUR Int. 1994, 128 (133).
22) Betten in einer Stellungnahme zum Vortrag von Remandas
(EPA) anläl3lich des FICPI-Kongresses am 22.6.1994 in Wien;
Bericht des Britischen Patentamts vom ,,Public Forum<< am
19.10.1994; Betten, GRUR 1995, 775 (777,788).
23) Betten, GRUR 1995,775 (788).
24) Eine begrenzte Anzahl von Tagungsberichten ist bei den Auto-ren
kostenlos erhältlich.
25) EPA Amtsblatt 1999, 193 (197).
26) GRUR Int. 1999,335 (339).
27) EPA Amtsblatt 1999,429.
28) EPA Amtsblatt 1999,545 (550).
29) Insbesondere EPA-Amtsblatt 1999, 609 - IBM/Computerpro-grammprodukt
1; BPatC GRUR 1999, 1078 - Automatische
Absatzsteuerung.
22 CR 1/2000

