grurart.lstex:
	lstex grurart | sort -u > grurart.lstex
grurart.mk:	grurart.lstex
	vcat /ul/prg/RC/texmake > grurart.mk


grurart.dvi:	grurart.mk grurart.tex
	latex grurart && while tail -n 20 grurart.log | grep references && latex grurart;do eval;done
	if test -r grurart.idx;then makeindex grurart && latex grurart;fi
grurart.pdf:	grurart.mk grurart.tex
	pdflatex grurart && while tail -n 20 grurart.log | grep references && pdflatex grurart;do eval;done
	if test -r grurart.idx;then makeindex grurart && pdflatex grurart;fi
grurart.tty:	grurart.dvi
	dvi2tty -q grurart > grurart.tty
grurart.ps:	grurart.dvi	
	dvips  grurart
grurart.001:	grurart.dvi
	rm -f grurart.[0-9][0-9][0-9]
	dvips -i -S 1  grurart
grurart.pbm:	grurart.ps
	pstopbm grurart.ps
grurart.gif:	grurart.ps
	pstogif grurart.ps
grurart.eps:	grurart.dvi
	dvips -E -f grurart > grurart.eps
grurart-01.g3n:	grurart.ps
	ps2fax n grurart.ps
grurart-01.g3f:	grurart.ps
	ps2fax f grurart.ps
grurart.ps.gz:	grurart.ps
	gzip < grurart.ps > grurart.ps.gz
grurart.ps.gz.uue:	grurart.ps.gz
	uuencode grurart.ps.gz grurart.ps.gz > grurart.ps.gz.uue
grurart.faxsnd:	grurart-01.g3n
	set -a;FAXRES=n;FILELIST=`echo grurart-??.g3n`;source faxsnd main
grurart_tex.ps:	
	cat grurart.tex | splitlong | coco | m2ps > grurart_tex.ps
grurart_tex.ps.gz:	grurart_tex.ps
	gzip < grurart_tex.ps > grurart_tex.ps.gz
grurart-01.pgm:
	cat grurart.ps | gs -q -sDEVICE=pgm -sOutputFile=grurart-%02d.pgm -
grurart/grurart.html:	grurart.dvi
	rm -fR grurart;latex2html grurart.tex
xview:	grurart.dvi
	xdvi -s 8 grurart &
tview:	grurart.tty
	browse grurart.tty 
gview:	grurart.ps
	ghostview  grurart.ps &
print:	grurart.ps
	lpr -s -h grurart.ps 
sprint:	grurart.001
	for F in grurart.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	grurart.faxsnd
	faxsndjob view grurart &
fsend:	grurart.faxsnd
	faxsndjob jobs grurart
viewgif:	grurart.gif
	xv grurart.gif &
viewpbm:	grurart.pbm
	xv grurart-??.pbm &
vieweps:	grurart.eps
	ghostview grurart.eps &	
clean:	grurart.ps
	rm -f  grurart-*.tex grurart.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} grurart-??.* grurart_tex.* grurart*~
tgz:	clean
	set +f;LSFILES=`cat grurart.ls???`;FILES=`ls grurart.* $$LSFILES | sort -u`;tar czvf grurart.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
