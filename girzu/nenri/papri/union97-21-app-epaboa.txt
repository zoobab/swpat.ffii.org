<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">

12

Sonderausgabe ABI. EPA / Special edition OJ EPO / Edition speciale JO OEB

1997

TEIL II

DIE RECHTSPRECHUNG DER
BESCHWERDEKAMMERN
UNO DER GROSSEN
BESCHWERDEKAMMER DES
EPA IM JAHR 1996

I. PATENTIERBARKEIT

A. Patentfahige Erfindungen

1. Computerbezogene Erfindungen

In der Sache T 953/94 bezog sich
Anspruch 1 des Hauptantrags auf
eine Methode, mit einem digitalen
Computer eine Datenanalyse des
zyklischen Verhaltens einer Kurve zu
erzeugen, die durch eine Mehrzahl
von Punkten dargestellt war, die zwei
Parameter zueinander in Beziehung
setzten. Die Kammer war der
Ansicht, eine solche Methode konne
nicht als patentfahige Erfmdung
angesehen werden, weil eine Analy-
se des zyklischen Verhaltens einer
Kurve eindeutig eine mathematische
Methode sei und als solche von der
Patentierbarkeit ausgeschlossen sei.
Mit der Bezugnahme auf einen digi-
talen Computer werde nur der Hin-
weis gegeben, dafi die beanspruchte
Methode mit Hilfe eines Computers,
d. h. eines programmierbaren Uni-
versalrechners, ausgefuhrt werde;

seine Funktion werde von einem Pro-
gramm gesteuert, das ats solches
von der Patentierbarkeit ausge-
schlossen sei. Die Tatsache, dafi die
Beschreibung Beispiele sowohl auf
nichttechnischen als auch auftechni-
schen Gebieten offenbare, bestatige
die Auffassung, daB die durch die
beanspruchte mathematische
Methode geloste Aufgabe vom
Anwendungsgebiet unabhangig sei
und somit in diesem Fall nur auf
mathematischem und nicht auf
einem technischen Gebiet liegen
konne. Eine bloKe Bezugnahme auf
eine mathematische Methode als
solche schlieBe den Anspruch nicht
unbedingt von der Patentierbarkeit
aus, doch konne die Kammer keine
technische Wirkung erkennen, die
mit Hilfe der beanspruchten Metho-
de erzielt werden konne.

Der funfte Hilfsantrag lautete wie
folgt:

"Eine Methode zur Steuerung eines
physikalischen Vorgangs, die auf der
Analyse einer funktionellen Bezie-
hung zwischen zwei Parametern des
physikalischen Vorgangs beruht und
folgende Schritte umfaBt:

PART II

BOARD OF APPEAL AND
ENLARGED BOARD OF
APPEAL CASE LAW 1996

I. PATENTABILITY

A. Patentable inventions

1. Computer-related inventions

In T 953/94, claim 1 of the main
request related to a method of
generating with a digital computer a
data analysis of the cyclical behav-
iour of a curve represented by a plu-
rality of plots relating two parame-
ters to one another. The board held
that such a method could not be
regarded as a patentable invention,
because an analysis of the cyclical
behaviour of a curve was clearly a
mathematical method excluded as
such from patentability. The refer-
ence to a digital computer only had
the effect of indicating that the
claimed method was carried out with
the aid of a computer, ie a program-
mable general-purpose computer,
functioning under the control of a
program excluded as such from
patentability. The fact that the
description disclosed examples in
both non-technical and technical
fields confirmed that the problem
solved by the claimed mathematical
method was independent of any field
of application and could thus lie, in
the case at issue, only in the mathe-
matical and not in a technical field. A
mere "reference" to a mathematical
method as such did not necessarily
exclude the claim from patentability
but the board was also unable to
identify a "technical" effect that
would be achieved by the claimed
method.

The fifth auxiliary request read as
follows:

"A method of controlling a physical
process based on analysing a func-
tional relationship between two
parameters of the physical process
comprising the steps of:

DEUXIEME PARTIE

LA JURISPRUDENCE DES
CHAMBRES DE RECOUPS ET
DE LA GRANDE CHAMBRE DE
RECOUPS EN 1996

I. BREVETABILITE
A. Inventions brevetables

1. Inventions concernant des
ordinateurs

Dans I'affaire T 953/94, la revendica-
tion 1 de la requete principale portait
sur une methode pour realiser, avec
un ordinateur numerique, une analy-
se de donnees du comportement
cyclique d'une courbe representee
par une pluralite de points ou de tra-
ces mettant en relation deux para-
metres. La chambre a estime que
cette methode ne pouvait etre consi-
deree comme une invention breveta-
ble, au motif que ('analyse du com-
portement cyclique d'une courbe est
a ('evidence une methode mathema-
tique, exclue en tant que telle de la
brevetabilite. La reference a un ordi-
nateur numerique avail pour seui
effet d'indiquer que la methode
revendiquee etait mise en oeuvre a
I'aide d'un ordinateur, a savoir un
ordinateur universe! programmable,
dont les fonctions sont commandoes
par un programme exclu en tant que
tel de la brevetabilite. Le fait que la
description comporte des exemples
dans des domaines aussi bien non
techniques que techniques confir-
mait que Ie probleme resolu par la
methode mathematique revendiquee
etait independent d'un quelconque
domaine d'application et ne pouvait
done appartenir, en I'espece, qu'au
domaine mathematique et non pas a
un domaine technique. Si une simple
"reference" a une methode mathe-
matique en tant que telle n'exclut
pas forcement la revendication de la
brevetabilite, la chambre n'a cepen-
dant pas pu relever un effet "techni-
que" produit par la methode revendi-
quee.

La cinquieme requete subsidiaire
s'enoncait comme suit:

"Methode pour controler un proces-
sus physique fonde sur I'analyse
d'une relation fonctionnelle entre
deux parametres dudit processus
physique comprenant les operations
suivantes :

227

1997

Sonderausgabe ABI. EPA / Special edition OJ 6PO / Edition speciale JO OEB

13

Messen der beiden Parameterwerte
und Erzeugen einer Oatenanalyse
des zyklischen Verhaltens einer
(Curve, die durch eine Mehrzahl von
Punkten dargestellt ist, die die bei-
den Parameter zueinander in Bezie-
hung setzen ..., mittels eines digita-
len Computers."

Das letzte Merkmal lautete wie folgt:

"(h) wobei der Bereich des einen
Parameters zur Verwendung bei der
Steuerung des physikalischen Vor-
gangs entsprechend den zur Darstel-
lung der Kurvenverlangerung auf
einem Datensichtgerat erzeugten
Daten erweitert wird."

Die Kammer vertrat die Auffassung,
der Begriff "physikaHsch" konne so
verstanden werden, da8 die Metho-
de, bei der angeblich die mathemati-
sche Methode zur Analyse einer
Kurve verwendet wurde, ein Verfah-
ren sei, bei dem die Parameter physi-
kalischer Art oder Gegenstande
seien; hierzu gehorten nicht Verfah-
ren, bei denen die Parameter nicht
physikalischer, sondern z. B. finan-
zieller Art seien. Die Kammer betonte
jedoch, daB Anspruch 1 des funften
Hilfsantrags nur wegen der Einfu-
gung der Worte "zur Verwendung bei
der Steuerung des physikalischen
Vorgangs" nicht von der Patentier-
barkeit ausgeschlossen sei.

Anders als die Einspruchsabteilung
entschied die Kammer, da& diese
Formulierung den Anspruch in tech-
nischer Hinsicht einschranke.
Anspruch 1 beziehe sich nicht mehr
auf eine blofie Moglichkeit, die
mathematische Methode bei einem
technischen oder physikalischen Ver-
fahren anzuwenden. Wenn derAus-
druck "zur Verwendung" allerdings
nur als Hinweis darauf zu verstehen
sei, da(S die beanspruchte Erweite-
rung eines Parameterbereichs fiir die
Darstellung der Kurvenverlangerung
zur Verwendung bei der Verfahrens-
Steuerung "geeignet" sei, dann lasse
diese Auslegung tatsachlich zweifel-
haft erscheinen. ob der Anspruch
wirksam eingeschrankt worden sei.
In Verbindung mit der ausdrucklich
beabsichtigten Beschrankung der
beanspruchten Methode auf eine
"Methode ?ur Steuerung eines physi-
kalischen Verfahrens" konne das
Wort "zu" aber nicht mehr blolS als
"geeignet" interpretiert werden, son-
dern bedeute "zur Steuerung eines
physikalischen Verfahrens verwen-
det". Die Kammer gelangte zu dem
SchluB, dafi der Gegenstand des
funften Hilfsantrags bei richtiger
Auslegung nicht von der Patentier-
barkeit ausgeschlossen sei.

measuring the values of the two
parameters, and generating with a
digital computer a data analysis of
the cyclical behaviour of a curve
represented by a plurality of plots
relating the two parameters to one
another,..."

The last feature was worded as fol-
lows:

"(h) extending the range of said one
parameter in accordance with the
data generated for displaying on a
visual display unit the prolongation
of said curve for use in the control of
said physical process."

The board held that the term "physi-
cal" could be interpreted as meaning
that the method in which the mathe-
matical method of analysing a curve
was said to be used was a process in
which the parameters were of a
physical nature or were physical
"entities" and did not include pro-
cesses in which the parameters were
not of a physical nature, such as
"financial" parameters. However, the
board emphasised that claim 1 of the
fifth auxiliary request was not
excluded from patentability only
because of the insertion of the
expression "for use in the control of
said physical process".

Contrary to the decision of the oppo-
sition division the board decided that
this wording limited the claim in a
technical sense. Claim 1 no longer
referred to the mere possibility of
using the mathematical method in a
technical or physical process. It was
agreed that if the expression "for
use" were understood as merely
indicating that the claimed extension
of the range of a parameter for dis-
playing the prolongation of the curve
would be "suitable" for use in the
process control, such an inter-
pretation might cast doubt on the
effectiveness of the limitation of the
claim. However, in conjunction with
the expressly intended restriction of
the claimed method to a "method of
controlling a physical process" the
word "for", in the board's view, could
no longer be interpreted as merely
meaning "suitable" but as "used to
control a physical process". The
board concluded that the subject-
matter of the fifth auxiliary request in
its proper interpretation was not
excluded from patentability.

mesurer la valeur des deux parame-
tres et generer a I'aide d'un ordina-
teur numerique une analyse des
donnees du comportement cyclique
d'une courbe representee par une
pluralite de points ou de traces met-
tant en relation les deux parametres,

La derniere caracteristique etait
formulee comme suit:

" (h) etendre la gamme du premier
parametre conformement aux don-
nees generees pour visualiser sur
('unite de reproduction visuelle de
I'ordinateur le prolongement de la
courbe afin d'utiliser celle-ci pour
realiser (edit processus physique."

La chambre a estime que I'adjectif
"physique" pouvait etre interprete
comme signifiant que la methode
dans laquelle la methode mathemati-
que d'analyse d'une courbe etait
censee etre utilisee constituait un
processus dont les parametres ont
une nature physique, soit des "enti-
tes" physiques, et ne comprenait pas
de processus dont les parametres
n'ont pas une nature physique,
comme par exemple des parametres
financiers. Neanmoins, la chambre a
souligne que la revendication 1 de la
cinquieme requete subsidiaire n'etait
pas exclue de la brevetabilite au seui
motif qu'elle contenait ('expression
"afin d'utiliser cetle-ci pour realiser
(edit processus physique".

Contrairement a ce qu'avait decide la
division d'opposition, la chambre a
juge que cette formulation limitait la
revendication dans un sens techni-
que, la revendication 1 ne portant
plus sur la simple possibility d'utili-
ser la methode mathematique dans
le cadre d'un processus technique ou
physique. La chambre a reconnu que
si ('expression "afin d'utiliser" etait
seulement comprise comme indi-
quant que I'elargissement revendi-
que de la gamme du parametre en
vue de visualiser le prolongement de
la courbe "etait utilisabte" pour reali-
ser le processus, on pourrait douter
de la limitation apportee a la reven-
dication. Toutefois, vu ('intention
explicite de restreindre la methode
revendiquee a une "methode pour
realiser un processus physique", le
terme "afin" dans ("expression "afin
d'utiliser" ne peut plus etre interpre-
te, selon la chambre, comme signi-
fiant simplement "utilisable", et la fin
de la phrase doit se lire comme
signifiant "afin de realiser ledit pro-
cessus physique". La chambre a
conclu que I'objet de la cinquieme
requete subsidiaire, correctement
interpretee, n'etait pas exclu de la
brevetabilite.

228
