<!-- -*- coding: utf-8 -*- -->
<meta http-equiv=content content="text/plain; charset=utf-8">

ANDREAS RAUBENHEIMER
Die jüngere BGH-Rechtsprechung zum
Softwareschutz nach Patentrecht
Der Urheberrechtsschutz fir Software wird durch die am der Auguren zeigt sich jedoch in der jüngeren Rechtspre-24.
Juni 1993 in Kraft getretenen speziellen Bestimmun- chung des BGH die seit langem geforderte Abkehr von
gen des LrhG f?ir Computerprogramme erheblich verbes- dieser resuiktven Haltung. Im Rahmen dieses Beitqgs
sert. Dies wurde vom Verfasser bereits in CR 1994,69 ff., soll daher der Frage nachgegangen werde% ob durch ciie-129
ff dargestellt. Damit rucken jedoch neben dem in CR se Re& tsprechung tatsächlich die vielbach worene Er-1994,264
ff. erörterten Schutz nach dem UWG vor allem leich terung im Hinblick auf (lie Paten tiedarkeit so-b-a-die
durch das Patentrecht eröffneten ,W&lichkeiten des rebezogener Erfindungen bewirkt wurde Ferner soll im
Softwareschutzes verstärkt in den Blickpunkt, Zwar sind tUnblick aufdie in das UrhG eingefiigren Spezialregelun-Computerprogramme
als solche sowohl gemä.8 5 i-4 bs. 2 gen für Computerprogramme das Verhält& von Parent-Nr.
3, -4 bs. 3 PatG als auch gemäß Art. 52 Abs. 2 lit. c, und Urheberrecht untersucht und ein Ausblick auf die
Abs. 3 EPÜ vom Pa ten tschutz ausgenommen. So ftwarebq künftige Entwicklung des SoI%waresch uues gegeben wer-zogene
Erfindungen hingegen können durchaus in &n Ge- + den.
nuß des Pa ten tschutzes kommen. Ihre Pa tentiemng schei- --EC i.
terte in der Vergangenheit aber zumeist an der Testaktiven l t Rechtsprechung des Bundesgerichtshofs. Nach Ansicht Andreas Raubenheimer, Maitre en Droit, ist Rechtsanw-zir in .Uiirk
328 CR 6,1?94
WRTSCHAFI3XECH-T
11. Jüngere BGH-Rechtsprechung
Die am 11. Juni 199 1 ergangenen Entscheidungen
>aSeitenpufferT!3 und »Chinesische Sthriftzeicbene14
wurden seitex der Kritiker des BGH mit großer Xuf-merksamkeit
erwartet. Diese beiden Entscheidungen
wurden mittltrweile durch das Urteil *Tauchcompu-ter#
vom 4. Feb ruar 1992 ergänzt.
1. » Seitenpffer «
a/ Technischer Charakter eines Betriebssystems
In BSeitenpuEertg l6 bejahte der X. Senat die Technizität
eines Betriebqrogramms. Er begründete den techni-schen
CharakTer der programmbezogenen Lehre da-mit,
dal( sie die Funktionsfähigkeit der Datenverarbei-tungsanlage
als solcher betreffe und damit das unmit-teibare
Zusammenwirken ihrer Elemente ermögliche.
Denn die Lehre erschöpfe sich keineswegs in der &Aus-wahl,
GliedeTag und Zuordnung von Daten, sondern
enthalte die A4nweisung, die Elemente einer Datenver-arbeitungsanlage
beim Betrieb unmittelbar auf be-stimmte
Art und Weise zu benutzen und dadurch deren
Arbeitsweise zu verbessern. Somit sei deren Funktion
unmittelbar betroffen. Laut BGH Steht die Lehre
zum einen in der Erfassung und Speicherung der Infor-mation
über den aktuellen Speicherbereich eines Pro-zesses
unter Zuordnung der benötigten Speicherseiten
zu diesem und zum anderen in einer bestimmten Lade-strategie
für den Seitenpuffer dergestalt, daß die benö-tigten
Speicherseiten nicht einzeln, sondern aufgrund
der durchgef&rten Zuordnung »gebündelt« in diesen
übertragen und dort als *Datenbündel= gespeichert
werden.
Im konkreten Fall hat der BGH den technischen
Charakter der Lehre daraus abgeleitet, dag diese auf
technischem Gebiet liege. Dies folgert er zum einen aus
dem Einsatz technischer Mittel, nämlich der unmiael-baren
Benutzung der Elemente der dem Bereich der
Technik zuzuordnenden Datenverarbeitungsanlage
auf eine bestimmte Art und Weise. Zum anderen erge-be
sich die Beiahung der Technizität aber auch aus dem
erzielten technischen Effekt, nämlich der unmittelbar
verbesserten Funktion der Hardware, ohne daß
menschliche Verstandeststigkeit zwischengeschaltet
sei. Obwohl der BGHin »Seitenpuffer* noch viele Fra-gen
offengelassen hat, l8 dürfte sich hier der Grundsatz
enmehmen lassen, daß der BGH die Technizität von
Betriebs- und Systemprogrammen bejaht-l?
höhe vermischt werden darf.21 Dies bedeutst aber, daS
die Priifung auf Technizität demnach nicht xr auf das
*Neue<< und BErfinderischeti der Lehre beshränkt ist.
Daraus Iäf3t sich im Wege der Umkehrs?!usses fol-gern:
daß bereits mit NSeitenpuffera vom BGHindirekt
der sogenannte Ganzheitsgrundsatz anerkant wurde,
d.h. die Prüfung auf technischen Charakrcr den ge-samten
Anmeldungsgegenstand umfaßt urd nicht le-diglich
dessen uneuen, erfinderischen Kern< .=
Somit läßt bereits die Entscheidung >>Stitenpuffera
ein -4briicken des BGH von der sogenanrxen *Kern-theorie<<
erkennen. Allerdings ist insoweit sinschrän-kend
hinzuzufügen, daß dieser Begriff-wie bei 1. gese-hen
- vom Schrifrtum geprägt wurde,L3 wahrend der
BGH selbst nie von einer ~Kemtheorie« sprach, son-dem
!ediglich vom mals neu und erfind&sch bean-spruchten
Kern der Lehre«.
.
2. *Chinesische Schriftzeichen« -c
a) Fortgeltung der Rechtsprecfiung zum ai:en PatG
Im Gegensatz zum begrüf3enswtrten Ergebti in *Seiten-pufferu
vermag die am selben Tage vom X. knat ergan-gene
Entscheidung BChinesische Schriftzeic5enm4 nicht
zu befriedigen. Obwohl auf diesen Fall vom BGH erst-mals
5 1 PatG 1981 anzuwenden war, ging 5ieser nicht
auf die Frage ein, ob wegen § 1 -4bs. 2 Nr. 3, -ibs. 3 PatG
die Patentfähigkeit ausgeschlossen war.2s 5catt dessen
bestätigte er lediglich die Fortgeltung seiner noch zu S 1
PatG 1968 entwickelten ständigen Rechtsprechung, der-zufolge
Patentschutz nur f5.r eine Lehre mit rechnischem
Charakter möglich sei, an dem es hier-unabhängig von
den Versagungsgründen des S 1 Abs. 2 Kr. 3, ,4bs. 3
PatG 1981- bereits aus anderen Gtinden fehle.
b) Abstellen auf die fiir den molg relevaxen Mittel
Die beanspruchte Lehre betraf ein Verfahren zur Em-gabe
chinesischer Zeichen in Textsysteme. durch das
zum einen der Speicherbedarf reduziert und zum ande-ren
die Arbeitsgeschwindigkeit erhöht und insbesonde-re
die Zugriffszeit verringert werden sollte. Dabei sollte
u.a. eine Eingabetastatur, eine -4nzeigeeinheit und/oder
ein Druckwerk sowie eine Steuer- und Speichereinheit
b ) Vorsich tises *Abrücken von der »Kerntheorie*
Von grundsätzlicher Bedeutung dürfte die vom BGH
ausgesprochene Klarstellung sein, der-zufolge die Tech-nizität
der Lehre nicht davon abhängt, ob leatere
19)
20)
21)
Tz)
-krenpuffer-. CR 1991,6.58 = XF 1992,372.
Gunesische S&if~~&chen~, CR 1991,662 = NJW 1?92,314.
l Tauchcompucer~, CR 1992, 600 = NJW 1993, 203 = GRUR 1992,
430.
CR 1991,658.
CR 1991,658,658 f, 661.
Vj. insbesondere da Kommenrar vm Cokku~, CR ! 91,664; S. auch
Kindemmn, CR 19!92,577,579.
Ir! diesem Sinne Co!&ao, CR 1991,664; ebenso Berrr, -\nm. L. BGH-L;
teil .Tauchcomputer~, CR 199t, 633; L;Umann, CR :99& 641,647;
Taf.., Min 1993,73,77.
VgL CR 1991,658.659 (LS 3). 661.
VgL Goidrian. CR 1991,644.
Elicnso Kra&r :FN f), Rdnr. 86,89: a-4: Goldri;m. CX 1991,664.
Vgi. Ikmm, CR 1995 603; Guekf(Ts 2), S. 362; Kr.&ri,FX 2),Rdnr.
89 m.w.N., die dies zu Recht hervorkbcn.
mneuqt, ~fomchrittlich~~ und »erfinderische ist.2o Da-mit
trägt der BGH der Kritik im Schrifttum Rechnung --N ,@
und erkennt an, daß die Prüfung des technischen Cha- -~h 1991, 662 h.,
rakters nicht mit der im Rahmen eines weiteren separa- 25) Prcgrammal für v verarbeitunganlagen als solchen in 4 5 1
ten Prüfungspunktes zu untersuchenden Erfindungs-
.h. 2 Nr. 3, .%. J?as 1981 ein ?atent3chun zu vc!sagen; v$. dam
Ergei, GRUR i 993,194 m.w.N.
13)
14)
5)
16)
7 18)
330 CR Ml*
. R\UBEMIEL\~R: SOFiTARE.SCHLZZ NACH PA-T-EXRECHT
&t mindestens vier Speichern zum Einsatz kommen, die In der Entscheidung »Chinesische Schriftzeichen *
@eiIs mit bestimmten, dort noch w&er zu ordnenden nimmt der BGH eine Wertung nach dense!ben, aus
Infbrmationen beleg-t werden so!lten.6 Der BGH X--. pb Flugkostenminimie rung << bereis bekannten Kriterien
n&te den technischen Charakrer der beanspruchte&%, vor.3z Die Entscheidung BChinesische Schriftzeichen*
Lehre mir dem Argument, daß hier lediglich ein Ord-nungssystem
gedanklicher Art Dirn Vordergrund stehe*,
nämlich die Ordnung der chinesischen Zeichen nach
ihren Kennzeichen, Zeichenfolgen und Teilzeichen in be-stimmte
Bereiche und innerhalb dieser Bereiche nach wei-teren
Kriterien. Demgegenüber seien die technischen
Merkmale28 für den Erfolg der Lehre mvon untergeordne-ter
Bedeutung<<. Damit blendet der BGHdie technischen
Mittel einfach unter Hinweis auf deren angeblich man-gemde
Relevanz bei der Realisierung des von der bean-
wird denn auch in der Literatur - zu Recht - kritisier-t.33
Letztere bemängelt vor allem, daß die wertende Betrach-tung
des BGH die eingesetzten technischen Xciittel und
den erzielten technischen Effekt vernachlässige. As tech-nische
Xlittel sind insbesondere die bestimmte hardware-ma&
ge Ausgestaltung durch entsprechende Eingabeta-statur,
Anzeigeeinheit, Druckwerk, Steuer- und Spei-chereinheit,
etc. zu nennen, die unter anderem Anzeige
undoder Druck der Zeichen? deren Leitung zur Anzeige-einheit
bzw. zum Drucker bewirken. KindermanrP cha-rakterisiert
den Anmeldungsgegenstand denn auch tref-fend
als eine chinesische Schreibmaschine. Bei einer
Schreibmaschine wird man jedoch wohl kaum den tech-nischen
Charakter verneinen können. Der technische Ef-fekt
besteht zum einen in der ,lldglichkeit, die Zeichen
anzuzeigen oder auszudrucken, zum anderen in der ver-besserten
Funktionsweise der Datenverarbeitungsanla-ge,
nämlich der Reduzierung des Speicherplanbedarfes
sowie der Erhöhung der Arbeitsgeschwindigkeit und da-durch
bedingte verringerte Zugriffszeiten. Das in *Chi-nesische
Schriftzeichen<< gefundene Ergebnis wird noch
unbefriedigender, vergieicht man es mit der Entschei-dung
*Tauchcomputer& (dazu unten II.4.).
spruchten Lehre angestrebten Ergebnisses aus und beur-teilt
die Technizität auf der Basis der angeblich *im Vor-dergrund
stehenden * untechnischen und gedanklichen
Tätigkeit des 0lrdnens der verarbeiteten Daten. mit dem
.r- -- Erfolg der beanspruchten Lehre stehe und falleu .z9
cj Wertung anhand der aus *Flugkostenminimie-rung«
bekann ten Kriterien
Die vom BGH in *Chinesische Schriftzeichen* ge-brauchte
Diktion erinnert jedoch in fataler Weise an die-jenige
in »FIugkostenminimierung~.30 Don: stellte er dar-auf
ab, daf3 markt- und betriebswirtschaftliche Aspekte
unter Einschluß der Rechenregel gegenüber den einge-setzten
Naturkräften Bim Vordergrund sründenu. Allein
die Bewertung der technischen Megwerte nach betriebs-wirtschaftlichen
Kriterien bestimme die Steuerung nach
Gtiße und Richtung. Letztere seien demnach *zur Pro- ! Memlösung unerläßlich« und bildeten den »Kern* der
beanspruchten Lehre. Die Xiitursächiichkeit der einge-setzten
Naturkräfte reiche hingegen nicht zur Bejahung
der erforderlichen Technizität der beanspruchten Lehre,
diese seien vorliegend *keine vollständige Problem& ..-und
würden bei der Erreichung des angestrebten
Loiges »an Bedeutung zurücktretend .3 i
26) .Dü erste s pa .ch er im Hinblick auf die Kennzeichen von Zeichen, der
zwei& im Hinblick auf Zeichenfegen (Wörter), der dritte Speicher im
)fmblick auf die zeichenbildenden Teilzeichen, etc.; vgl. CR 1991,662,
663,
27) -CR 1991,662,663.
zs) dt werden u.a. eine Eingabetastatur, eine Anzeigeeinheit ~n&oder
ä.n Druckwerk sowie eine Steuer- und Speichereinheir mit mindestens
Q$dchcm; vgl CR 1991,662,663.
m =HCR 1991,662,664.
30) -CRUR 1986,531.533 f .
3x) Vd auch die Erläuterungen zu Pflugkostenminimierung- bei 1.3.
32) %trcffend Gx&f(FN 2), 362,374 ff., der dies als l Gewichtungsmetro-&=
charakterisiert; im gleichen Sinne äuf3ert sich Kra&r r FN 2), Rdnr.
87, der von einer Verengung des Blickwinkels unter dem Gesichtspunkt
Q Relevanz für das Ergebnis sprichr: a.-i.: Kindernmm, CR 1992,577,
580, und Wmum, CR 1992.641. 647, die dies als l Kemtheoriea be--
en und dabei m.E. übersehen. dad der BGH ruht - entsprechend
da oben 1.1. wiedergegebenen Definition der l Kemtheotie* - auf das
*her und w Erfinderische= , sondern statt dessen auf das für den Erfolg
l wwdichcm abstellt. Daher wären die Bezeichnungen . Wesentlich-LtfttStheorie.
, -Relevanztheorie* oder Gewtchrungsmethode~ erheb-l
i
ch PGser. Der Ausdruck l Kemtheotie~ stiftet in diesem Zusammen-hang
hdighch unnötig Verwirrung, indem er die unzutreffende Vorstd-h
herrotruft, dafJ diese &chrung nach RelevanuWesentlichkert
utx m b-g auf das Neue und Eriindensche vorgenommen wurde.
* vd- em Kindermann, CR 1992.577,579 f.; Co/&& CR I991,6M, - --
665; Goebel SN 2), 378; Teufel, Xiitt. 1993.73, 7.
u) a =92,577,580.
CR 641994
d) Unterschied zwischen HSeitenpuffera und * Chinesi-sche
Schriitzeichen *
Dem BGH ist allerdings zuzugeben, daß ein sachlicher
Unterschied zwischen den in BSeirenpufferu und Kbine-sische
Schriftzeichen* zu prüfenden .timeldungsgegen-ständen
bestand. Während es sich im ersten FaII um ein
für das Funktionieren der Datenverarbeitungsanlage als
solcher bereits unmittelbar erforderliches Beniebspro-gramm
handelte, betraf der zweite Fall hingegen ein Pro-gramm
zur Textverarbeitung. Der Einsatz eines Textve-rarbeitungsprogramms
auf einem Computer erfordert
allerdings zwangsläufig die vorherige Installation eines
entsprechenden Betriebssystems. Das Betriebssystem
bildet sozusagen die zweite Ebene der auf einem Compu-ter
zum Einsatz gelangenden Sottware, die sich an die
vom sogenannten BIOS (Basic-Input-Output-System)
gebildete erste Ebene anschliegt, wahrend Anwender-programme,
etwa fur Textverarbeitung oder CAD, erst
auf der dritten Ebene arbeiten. Ohne die hardware-nähere
Software der darunterliegenden Ebenen kann
das Textverarbeitungsprogramm gar nicht genutzt wer-den.
Dementsprechend scheint der BGH zwischen
dem unmittelbar die Funktion der Datenverarbeinmgs-anlage
betreffenden Betriebssystem und dem hardware-ferneren
nur mittelbar auf die Darenverarbeirungsanlage
einwirkenden Textverarbeitungsprogramm eine Trenn-linie
zu ziehen. Diese Betrachtung des BGHim Fall *Chi-nesische
Schriftzeichen* ist jedoch zu sehr von dem Be-mühe<
geprägt, zu der am selben Tag ergangenen Ent-scheidung
*SeitenpufferM einen Kontrapunkt zu setzen.
So wird zu einseitig darauf abgehoben, ob unmittelbar
auf die Funktionsfähigkeit der Datenverarbeitungsanla-331
WIRTSCHAFTSRECHT
ge als solcher eingewirkt werde. Dabei wird aber aufSer
acht gelassen, dai3 auch hier deren Funktionsfähigkeit
durch den erreichten technischen Effekt, nämlich die
Reduzierung des Speicherplatzbedarfs sowie die Erhö-hung
der .Arbeitsgeschwindigkeit bei einer Verringe-rung
der Zugriffszeiten. verbessen wurde. Dieser ein-seitigen
Betrachtungsweise sind offenbar auch die dar-über
hinaus ausgeklammerten technischen ,tfitte! zum
Opfer gefallen.
3. 4auchcomputer «
Im Urteil BBTauchcomputera3s erkennt der BGHtechni-sehen
,Mitteln, die nach einer bestimmten Rechenregel
betrieben werden und durch technische Geräte ermit-telte
Werte automatisch anzeigen, also einen techni-schen
Effekt bezwecken, den für den Patentschue er-forderlichen
technischen Charakter zu.
a) Der Anmeldungsgegenstand und seine Beurteilung
durch das Bundespatentgericht
Der Anmeldungsgegenstand36 betraf eine Anzeigeein-richtung
für die Parameter eines Tauchganges,37 die zu
jedem Zeitpunkt die jeweils erforderliche Gesamtauf-tauchzeit
inklusive der vorgeschriebenen Dekompres-sionshaltti8
anzeigen kann. Diese Anzeigeeinrichtung
verfügt ferner über mindestens einen Speicher für die
Dekompressionsparameter diverser Tauchtiefen und
-Zeiten sowie über eine A4uswerte- und Verkniipfnngs-stufe
für die gemessenen Werte des Tiefen- und Zeit-messers,
die mit den gespeicherten Werten angesteuert
ist. Zur Erreichung des technischen Effekts, dem Anzei-gen
der jeweils aktuellen Dekompressionsbedingungen,
kamen zwar zum einen technische Mittel, etwa Meßge-räte,
Speicher, Anzeigeeinrichtung und Wandlervor-richtung,
zum Einsatz. Zum anderen wurden aber auch
untechnische Dekompressionsparameter - so wie sie
sich bisher aus Tauchtabellen ergaben - benutzt.
Das Bundespatentgerichfi9 hatte bei der Prüfung auf
Technizität die sogenannte »Kemtheoriea angewendet
und dementsprechend auf »den als neu und erfinde-risch
beanspruchten Kern der Lehre- abgestellt, der le-diglich
in einer besonderen Interpretation/Auswertung
bekannter Tauchtabellen bestehe, also ein untechni-sches
Denkschema sei. Das Gebiet der Technik werde
demgegenüber erst nach der Lösung des eigentlichen
Problems betreten, so dai3 weder die genannten techni-schen
*Mittel noch der in der automatischen Ermittlung
und Anzeige der Gesamtauftauchzeit liegende techni-sche
Effekt den technischen Charakter des Patentge-genstandes
begründen könnten.
b) Die Ansicht des Bundesgerich&ofs
r *
Absage an die MKemtheorie«
Dieser Beurteilung erteilt der BGH eine Absage? q
36)
37)
38)
39)
40) kritisiert sie als zu einseitig auf die neuartige Berech--Te)
nung der Gesamtauftauchwerte fixiert. Ferner rügt er,
daß die gesamten technischen AMittel, die die Dekom-pressionsbedingurigen
automatisch anzeigten, nicht
332
gebührend berücksichtigt worden seien. Damit bringt
der BGH offenbar zum Ausdruck, daß die Prüfung da
Technizität nach dem Ganzheitsgrundsatz zu erfolgen
hat 2nd nicht ec1wa unter eirem auf das NNeuea und
»Erfinderisches eingeengten Blickwinkel, Trotz feh-l
e n d e r diesbrztgiicher e.xp!iziter FeststAung dürfte
diese Aussage wohl dahingehend interprtiert werden,
daG anstelle der sogenannten *Kerntheorie<< die vom
Schrifttum seit langem geiorderte Gesamtbetrach
tung vorzunehmen ist. Letztere entsprxht im übri-gen,
wie oben 1. gesehen, auch den Prüfungsrichtlinien
sowohl des DP,-i als auch des EPA.
Beurteilung der Technizität anhand des im Vorder-grund
Stehenden
Der BGH betont die enge Beziehung der neuartigen, un-technischen
Rechenregel zu den im Patenmnspruch ge-nannten
technischen Mitteln und »wertet% die bean-spruchte
Lehre als technisch. Er begründet dies damit,
daß das Betreiben der erwähnten techtichen Mittel
nach einer bestimmten Rechenregel die automatische
Anzeige von mit Hilfe von ,M&geräten erirjttelten -Cle&
großen ermögliche, es der Einschaltung menschlicher
Verstandestätigkeit also gerade nicht bedürfe. Insoweit
lehnt sich die Entscheidung .Tauchcomputer* an die Ar-gumentation
im Fall BSeitenpuffera an.
Frappierend ist andererseits jedoch auch, daß die
bereits in den Fällen l FIugkostenminirnieng~ und
*Chinesische Schriftzeichens verwendete Diktion zum
Zuge kommt, wonach im Hinblick auf die Technizitäs
durch eine Wertung zu ermitteln ist, was bei der im Pa
tentanspruch beschriebenen Erfindung *im Vorder-grund
steht*. Diese Formulierung muß nach den bishexi- .
gen Erfahrungen mit diesem Kriterium AnIaß zu gr&ter
Skepsis geben. Sie bedeutet nichts anderes, als dai3 der
BGH zwar nunmehr zunächst im Sinne einer Gesamtbe-trachtung
alle technischen IMittel berücksichtigt, techni-scher
Charakter jedoch nur dann bejaht wird, wenn diese
bei der folgenden Wertung bzw. Gewichtung Am Vor-dergrund
stehenu, *überwiegende und entscheidende
Bedeutung für die Erreichung des erstrebten Erfolges be-sitzen
« , Bder Lehre das entscheidende Gepräge geben*,
~~unmittelbar zum angestrebten Erfolg beitragena,
»allein bereits eine vollständige Problemlösung dar&-lenu
bzw. *mit ihnen der Erfolg der beanspruchten Lehre
steht und fällt&. Der Blickwinkel wird durch diese Wer-tung
abermals ganz erheblich verengt. Demnach würde
es auch weiterhin nicht genügen, wenn die technischen
LMerkmale lediglich mittelbar zum angestrebten ExfoIg
beitragen, hierfür also lediglich mitursächlich wären.
35) l Taucficomputcr-, CR 1992,600 = NJW 1993,203 = GRLJR 1992,
$30.
Vgl. CR 1992,600.601 f.
Etwa aktudk Tii maximal gcmxhte Tiefe, bkhenp Tauchzek 1
~cwds in Abhän&keit von den durchtaucbte~~ Tiefu und Zeirmm
Vgl. die Darsrehng im BGH-Lrml l Tahcompues, CR 1992,600,
6 0 2 .
vgl CR 1992 600,602 f.
Vgl. Bcmcn, CR 1992,603; Kraßa ;FN 2), Rdnr. 89: K&hnanq CR
1992 s?, 58U4 und 658,665 t; LZ!mann, CR 199f, Ml, 647; Tiufd
GroL$ -Min 1993,58. Gcnau~mwn wirdda
vom BGHscba seit l Scirmpuiiu~ anerkaMS u
Kl. j
CR 6/1*
R~LQ3ESXEI~MER: SOFXARESCHUTZ NACH PATtL?-RECHT
4. Bewertung der jüngeren BGH- stens ein Speicher, eine Wandlen-orrichtung sowie eine
Rechtsprechung .cl Anzeigeeinrichtung. Im Fall des in Verbindung mit der . w ._ Hardware wie eine Schreibmaschine arbeitenden Text-a)
m Relevanztheorie« anstelle der »Kerntheorie* -. - Verarbeitungsprogramms wurden chinesische Schrift-Als
Ergebnis der jüngeren BGH-Rechtsprechung läßt
sich somit festhalten, daß nunmehr bei der Technizitäts-f
Prüfung zwar eine begrüßenswerte Gesamtbetrachtung
zu erfolgen hat, jedoch durch eine WerrungGewichtung
die zur Erreichung des angestrebten Erfoiges wesentli-chetirelevanten
,Mittel zu besümmen sind Diese Wer-tung
könnte schlagwortartig als » RelevanrLteorie*3 be-zeichnet
werden. * Letztlich bleibt der Annelder weiter-hin
in der Ungewißheit zurück, ob der BGHdie mit der
Anmeldung beanspruchte Lehre entsprechend der *Rele-vanztheorieu
als technisch wertet. Daran vermag auch
die Tatsache nichts zu ändem, dag bei der Prüfung auf
erfinderische Tätigkeit, die sich an die separaten Prü-fr-
spunkte Technizität, vollständige Of&barung der
L 2 und Neuheit anschlie& der gesamte Erfindungs-8
,:&and unter Einschlug einer etwaigen Rechenregel
zu berücksichtigen ist,45 also auch insoweit wie schon bei
der Prüfung des technischen Charakters eine Gesamtbe-trachtung
anzustellen ist.*
b) Kritik der y Relevanztheorie«
Die Unsicherheit einer solchen Wertung nach der *Re-levanz*
der Mittel für den Erfolg zeigt sich gerade im
Vergleich der beiden Entscheidungen *Chinesische
Schriftzeichen« und »TauchcomputerH. In beiden Fäl-len
wurden nämlich technische Mittel zur Erzielung
eines technischen Effekts eingesetzt. Im ersten Fall wur-den
unter anderem eine Eingabetastatur, eine Anzeige-einheit,
ein Druckwerk sowie eine Steuer- und Speicher-einheit
verwendet, im zweiten Fall Me@eräte, minde-4
2 ) für den Erfolg wesendichen .Mittel bilden - um ti Reizwatt zu gc
uchen - den -Kern. der beanspruchten Lhre; diaer muß allerdings
4 3 )
+V
4S)
W
47)
48)
4 9 )
CR
im Gcgensaa w sogenannten ~Kcrndworie~ weder wuu~ noch l erfin-krisch.
sein, sondern wesendich* bzw. *relevant*; vgl. ~Flugkosteb
Minimierung, GRUR 1986,531, S33 f., sowie &hinesische Schr&&
cirn-, CR 1991, 662,663 f. Entgegen der Ansicht Kindermang CR
1992, S77,580 f., könnte man dies demnach als blo& Vedagemng da
l Kemsa dahingehend verstehen, dii3 dieser nunmehr wegen der vorzu-nehmenden
Gesamtbetrachtung zwar nicht mehr du& das *Neue* und
&findetische~ der beanspruchun Lehre m uird, sondan vid-mehr
aUein durch die ~Relevanza der von letzterer zur Erreichung des
baweckten Erfolgs verwendeten untechnischen und trchnischen *MitteL
Von l Kemdxoriee sollte jedoch zur Vermeidung einer begrifflichen Ver-wirrung
insoweit nicht gesprochen werden.
Vgl- die entsprechende inhaldicbc Umschreibung !xi Kraßu $N 2!,
Rdnr. 87.
Goebd (FN 2), 376,378 spricht von &ewichtungsmetfioder, lehnt eine
solche Gewichtung technischer und untechnischer Merkmale jedoch aus-drücklich
ab.
VgL BGH -Tauchcomputer l , CR 1992,600,603.
Vgl. Betun, CR 1992,603; Tidemann, CR 1992.277,580 f.. unrer-xhaut
m.E. allerdings die aus der - Relevanztheorie* resultierenden Ge-fahren
einer zu restriktiven Handhabung der Pnifuq auf Technizität.
insbesondere die der zu diesem Zweck vorzune5menden Wertung
mangsläufig unmanente Rechtsunxherheit.
lm Gegensatz zum Fall l Antiblo&ersystem*, GRCR !980,849.
Ebm.0 /an&. .Mitt. 1993,X.T 3: vgl. auch Kraf7er F?J 2), Rdnr. 90.
Einen ahnlichen Vergleich stelltjan&r, *Mit-t. 1993,72.73, für den BGH-Fd
l Tauchcompuww und den EPA-Fall l Schnt&eichenform/Sit
mensa, ABI. EPA, 1991, 566, an und hält beide für vergleichbar. Der
UA-Fall entsprach insofern der Konstellation bei Lxnesische tinft-Zeichen=,
als a em Verfahren zur Darstellung von Xhriftze~chen ati
einem Datawchcgeräc eines Computers betraf.
-Zeichen ausgedruckt undloder auf dem Bildxhirm an-gezeic
im Fall des Tauchcompurers wurde die Ge-samtauhuchzeit
und die Dekompressionshalte ange-zeigt.
In beiden Fällen ist demnach das erzielte Ergebnis
zur Wahrnehmung durch den Llenschen bestimmt, be-wirkt
also eine menschliche Verstandestätigkeit und ge-rade
keinen automatischen Einsatz von NaturTkräften.47
Auf diesen weiteren Verwendungszweck des von der
Lehre erstrebten Ergebnisses kann es indes nicht an-kommen,
so dag der technische Charakter der Lehre
nicht am möglicheweise untechnischen Verwendungs-zweck
des erzielten Resultats scheitern kann.8
In beiden Fällen wurde aber auch ein untechnisches
Denkschema gebraxh t, in »Chinesische khriftzei-chen
« ein Ordnungssystem &r die Zeichen, bei
>pTauchcomputerc< die als Rechenregel und Denksche-ma
bewertete neuartige Interpretation hkannter
Tauchtabellen. Insoweit sind die beiden Fälle also
durchaus vergleichbar,4v zumal beiden gemeinsam ist,
daf3 die Funktionsfähigkeit der Datenverarbeitungsan-lage
als solcher nicht derart unmittelbar betroffen ist
wie bei *Seitenpuffer«. Allenfalls bei *Chinesische
Schrifueichena wird die Funktionsfähigkeit der Hard-ware
zumindest mittelbar berührt, indem die durch den
Patentanspruch umschriebene Lehre die Funktion des
Computers dadurch verbessert, d& die ablaufenden
Verarbeitungsprozesse einen reduzierten Speicher-platzbedarf
haben und so die Arbeitsgeschtidigkeit
erhöhen bzw. die Zugriffszeiten verringem. Insofern
war der Anmeldungsgegenstand bei *Chinesische
Schriftzeichens sogar technischer als bei »Tauchcom-puter
l . Der einzige Unterschied zwischen beiden Fällen
besteht darin, daß im ersten Fall die mit technischen
Mitteln (Eingabetastatur) gewonnenen Daten (China-sische
Schriftzeichen) willkürlich von dem sie einge-benden
*Menschen ausgewählt werden, während die für
die Verarbeitung durch den Tauchcomputer bestimm-ten,
ehnfalls mit technischen -Mitteln (Medgeräten)
gewonnenen Daten iiMef3werte, etwa Tauchtiefe und
-Zeit) im konkreten Zeitpunkt ihrer Ermittlung nicht
vom Willen des Tauchers abhängen, sondern objektiv
feststehen. Allerdings sind auch sie zumindest mittel-bar
auf eine menschliche Verstandestätigkeir zurück-zuführen,
nämlich das willkürliche Verhalten des Tau-chers
während des Tauchgangs. Dieser Unterschied
rechtferrigt jedoch nicht die Verneinung der Technizi-tät
im Fail »Chinesische Schriftzeichen<<. Die Frage der
Auswahl der Daten betrifft nämlich eine Vorstufe des
Anmeldungsgegenstands pt Verfahren zur Eingabe chi-nesischer
Schriftzeichen in Textsysteme*.
Aufgrund obiger Erwägungen hätte auch in BChine-sische
Schriftzeichen« der technische Charakter bejaht
werden müssen. Denn hierfür genügt es, daß mit tech-nische&
,Mitteln unmittelbar, d.h. ohne Zwischenschal-tung
menschlicher Verstandestätigkeit, ein technischer
Effekt erzielt wird. Demgegenüber ist es für die Techni-zität
des Verfahrens völlig irrelevant, ob in dessen Vor-64994
. 333