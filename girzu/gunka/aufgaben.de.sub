\begin{subdocument}{swpatgunka}{Wie Sie helfen k\"{o}nnen, die Invasion des Patentwesens abzuwehren}{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\deutsche Version 2005-03-25 von Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Die Patentbewegung konnte \"{u}ber Jahrzehnte hinweg viele Unternehmen und Regierungen f\"{u}r ihr Credo gewinnen.  Rollende Z\"{u}ge sind schwer anzuhalten.  FFII und Andere haben sich dennoch seit ein paar Jahren mit Erfolg dieser Arbeit gewidmet.  Hier erkl\"{a}ren wir, wie Sie unsere Arbeit voranbringen k\"{o}nnen.}
\begin{sect}{intro}{Einf\"{u}hrung}
\begin{itemize}
\item
{\bf {\bf siehe auch noepatents.eu.org\footnote{\url{http://noepatents.eu.org/}}}}

\begin{quote}
neueste Anregungen zum Handeln
\end{quote}
\filbreak
\end{itemize}

Unsere dringendste politische Aufgabe ist es, die vom Europ\"{a}ischen Parlament ge\"{a}nderte Richtlinie gegen den Angriff des EU-Ministerrates zu verteidigen.  Der Rat wird von den Ministerialbeamten beherrscht, die am Europ\"{a}ischen Patentamt den Karren in den Dreck gefahren haben.  Diese Leute m\"{o}chten die \"{A}nderungen des Parlamentes und notfalls auch das ganze Richtlinienprojekt m\"{o}glichst ohne Diskussion kippen.

siehe Den Rat beeinflussen\footnote{\url{http://swpat.ffii.org/akteure/consilium/index.en.html}}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Unterzeichnen Sie die Petition f\"{u}r ein Softwarepatent-Freies Europa\footnote{\url{http://www.noepatents.org/}}}}

\begin{quote}
Diese Petition ist sehr einfach, unspezifisch und altehrw\"{u}rdig.  Sie sollte noch auf 1 Million Unterzeichner wachsen.
\end{quote}
\filbreak

\item
{\bf {\bf Den Aufruf zum Handeln\footnote{\url{http://swpat.ffii.org/papiere/europarl0309/appell/0504/index.en.html}} unterzeichnen}}

\begin{quote}
\begin{center}
Siehe http://aktiv.ffii.org/parl54/de
\end{center}

Indem Sie diesen Appell unterzeichnen und sich im Mitwirkungssytem als Unterst\"{u}tzer eintragen, k\"{o}nnen Sie sich an unseren Kampagnen beteiligen.
\end{quote}
\filbreak

\item
{\bf {\bf Melden Sie sich im Mitwirkungssystem an\footnote{\url{http://aktiv.ffii.org/?l=de}}}}

\begin{quote}
Unterzeichnen Sie Appelle, melden Sie sich auf Nachrichtenverteilern und Veranstaltungen an, adoptieren Sie Patente, adoptieren Sie Abgeordnete und andere politische Vertreter.

Geben Sie Ihre volle Adresse, Telefonnummer etc an, so dass Sie angesprochen werden k\"{o}nnen, wenn lokale Aktionen vonn\"{o}ten sind.
\end{quote}
\filbreak

\item
{\bf {\bf Verweisen Sie von Ihrer Webseite aus auf NoEPatents\footnote{\url{http://www.noepatents.org/}} und FFII\footnote{\url{http://swpat.ffii.org/index.de.html}}, verwenden Sie das logo\footnote{\url{http://wiki.ffii.org/Noepatents_libertyDe}} oder andere nette Grafiken\footnote{\url{http://wiki.ffii.org/VrijschriftSwpatkunstDe}}.  Integrieren Sie unseren Nachrichtenstrom\footnote{\url{http://wiki.ffii.org/DebianFfiirssDe}} in Ihren Web-Auftritt. Sprechen Sie mit Leuten in Ihrer Umgebung und gewinnen Sie sie daf\"{u}r, das gleiche zu tun.}}
\filbreak

\item
{\bf {\bf Hiesige Webseiten \"{u}bersetzen}}

\begin{quote}
siehe Vielsprachiges WWW: Wie Sie Mitweben K\"{o}nnen\footnote{\url{http://swpat.ffii.org/gruppe/langtxt/index.de.html}}
\end{quote}
\filbreak

\item
{\bf {\bf Texte in diese Webest\"{a}tte einbauen}}

\begin{quote}
Erg\"{a}nzen Sie die Dokumentation, indem Sie Wiki-Erweiterungen\footnote{\url{http://wiki.ffii.org/SwpatgunkaDe}} bestimmter FFII-SEiten bearbeiten.

If you have found any news, enter then into \url{http://wiki.ffii.org/SwpatcninoDe} or submit them into the static page via mail.

Sie k\"{o}nnen auch teilnehmen, indem Sie Quelldaten dieses Sites editieren, siehe ``Sources hypertexte multilingues\footnote{\url{http://www.ffii.org/proj/mlhtapp/index.en.html}}''.
\end{quote}
\filbreak

\item
{\bf {\bf FFII-Projekte gr\"{u}nden und unterst\"{u}tzen}}

\begin{quote}
Unsere Arbeit ist in Form von Projekten strukturiert.  Sie k\"{o}nnen laufende Projekte unterst\"{u}tzen und eigene gr\"{u}nden.

siehe Aufgaben und Projekte f\"{u}r eine Freie Informationelle Infrastruktur\footnote{\url{http://wiki.ffii.org/FfiiprojDe}}
\end{quote}
\filbreak

\item
{\bf {\bf Kontaktieren Sie Politische Vertreter}}

\begin{quote}
Hierbei kann es sich ebenso um Mitglieder nationaler Parlamente wie um Vertreter sektorieller K\"{o}rperschaften (z.B. Gewerbeverb\"{a}nde) oder auch um Ministerialbeamte handeln, die daran interessiert sein k\"{o}nnten, der nationalen Regierung auf die Finger zu schauen.

Sie sollten Ihre Vertreter auch dann kontaktieren, wenn Sie au{\ss}erhalb der EU leben.  Die Patentlobby \"{u}bt auch dadurch auf die EU Druck aus, dass sie in internationalen Foren wie WIPO Regierungen vor ihren Karren spannt oder dass sie internationale Vertr\"{a}ge wie TRIPs\footnote{\url{http://swpat.ffii.org/stidi/trips/index.de.html}} tendenzi\"{o}s auslegt.   Derzeit ist ein Vertrag \"{u}ber das Materielle Patentrecht\footnote{\url{http://wiki.ffii.org/Swpwipo05De}} in Vorbereitung, der es allen L\"{a}ndern schwer bis unm\"{o}glich machen soll, die Patentierbarkeit zu begrenzen.  Es w\"{a}re sinnvoll, innerhalb Ihres Landes das Thema vor das Parlament zu bringen, statt es den ``Patentexperten'' internationaler Foren zu erlauben, die Souver\"{a}nit\"{a}t Ihres Landes zu ihren Gunsten zu beschneiden.

Effektive Kommunikation
\begin{itemize}
\item
Kommunizieren Sie! Lassen Sie nicht nur Dampf ab!
\begin{itemize}
\item
W\"{a}hlen Sie einen politischen Vertreter aus Ihrer Region oder Ihres Berufs.

\item
Sie m\"{u}ssen bereit sein, am Ball zu bleiben.  Halten Sie Telefonnummern bereit, bevor Sie schreiben.  Rufen Sie zu geeigneter Zeit an.  Senden Sie nicht Erg\"{u}sse von allem, was Ihnen gerade durch den Kopf geht.   Beleidigen Sie nicht.  Spammen Sie nicht.

\item
In der K\"{u}rze liegt die W\"{u}rze.  Stellen Sie Fragen, die Ihr Vertreter zu antworten geneigt sein k\"{o}nnte.  Schreiben Sie klar und h\"{o}flich, was Ihr Vertreter als n\"{a}chstes tun sollte.  Lagern Sie alles andere in Anh\"{a}nge aus.  Der Anhang darf ruhig umfangreich sein.  Dies gilt insbesondere f\"{u}r Papierbriefe.

\item
Erkl\"{a}ren Sie, wie Sie pers\"{o}nlich betroffen sind.  Spekulationen \"{u}ber das Gemeinwohl ist weniger interessant und sollte denen \"{u}berlassen werden, die daf\"{u}r kompetent sind (z.B. wirtschaftswissenschaftliche Traktate im Anhang).
\end{itemize}

\item
Don't spend much time explaining how bad software patents are.  Rather, explain in concrete terms what action your MEP must take to prevent software patents.  Your instructions can include the following:
\begin{itemize}
\item
Make it clear that the proposals of the Commission and Council lead to US-style unlimited patentability, that the Parliament's approach from the 1st reading was good, and that, given the short time frame of the 2nd reading and the complete failure of the Council to take the Parliament's concerns into account, the Parliament now has no other choice but to reaffirm its previous stand.

\item
tell your MEP which of his colleagues he should consult and who is less trustworthy (e.g. for German conservatives: ask them to talk to Kauppi, Krings and Karas and to ask Wuermeling or Lehne some tough questions).

\item
a pointer to the FFII's amendments analysis pages\footnote{\url{http://ffii.org/amend/}} as a valuable reference.
\end{itemize}
\end{itemize}

Schlagen Sie vor, Ihr Vertreter m\"{o}ge
\begin{itemize}
\item
sich als Unterzeichner des Aufrufe zum Handeln\footnote{\url{http://swpat.ffii.org/papiere/europarl0309/appell/index.de.html}} auflisten lassen

\item
an einer Parlamentarischen Anfrage zur Patentierbarkeit von Software und zum Verhalten der Regierung mitwirken

\item
an einer Stellungnahme des Parlaments / eines parlamentarischen Ausschusses / eines Verbandes mitwirken.  Das Parlament sollte insbesondere (1) die Patentjustiz (2) die Regierung dazu anhalten, daf\"{u}r zu sorgen, dass Programmierer sich nicht \"{u}ber Patente sorgen machen m\"{u}ssen und dass das geltende Gesetz und der Wille des Europ\"{a}ischen Parlamentes entsprechend respektiert werden.

\item
die nationalen Vertreter des FFII kontaktieren.
\end{itemize}

Beizuf\"{u}gende Dokumente
\begin{itemize}
\item
Aufrufe zum Handeln\footnote{\url{http://swpat.ffii.org/papiere/europarl0309/appell/index.de.html}}

\item
Zitate zu Softwarepatenten\footnote{\url{http://swpat.ffii.org/archiv/zitate/index.de.html}}

\item
Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{\url{http://swpat.ffii.org/papiere/europarl0309/index.de.html}}

\item
Europ\"{a}ische Softwarepatente: Einige Musterexemplare\footnote{\url{http://swpat.ffii.org/patente/muster/index.de.html}}

\item
EU-Parlament stimmt f\"{u}r echte Begrenzung der Patentierbarkeit\footnote{\url{http://swpat.ffii.org/log/03/plen0924/index.de.html}}
\end{itemize}

Weitere Unterlagen
\begin{itemize}
\item
Web-Adressen verschiedener Europ\"{a}ischer Parlamente\footnote{\url{http://www.bundestag.de/europa/parl_peu.html}}

\item
Tauss \& Kelber 2003-09-24: EP erkl\"{a}rt Trivialpatenten eine Absage\footnote{\url{http://swpat.ffii.org/papiere/europarl0309/spdmdb030924/index.de.html}}
\end{itemize}

Foren

\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
Diskussionsforum & statische Seite & Wiki\\\hline
uk-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/uk-parl}} & \url{http://swpat.ffii.org/akteure/uk/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatukEn}}\\\hline
be-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/be-parl}} & \url{http://swpat.ffii.org/akteure/be/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatbeEn}}\\\hline
nl-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/nl-parl}} & \url{http://swpat.ffii.org/akteure/nl/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatnlEn}}\\\hline
lu-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/lu-parl}} & \url{http://swpat.ffii.org/akteure/lu/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatluDe}}\\\hline
se-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/se-parl}} & \url{http://swpat.ffii.org/akteure/se/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatseEn}}\\\hline
fi-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/fi-parl}} & \url{http://swpat.ffii.org/akteure/fi/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatfiEn}}\\\hline
ee-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/ee-parl}} & \url{http://swpat.ffii.org/akteure/swpatee/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpateeDe}}\\\hline
lv-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/lv-parl}} & \url{http://swpat.ffii.org/akteure/swpatlv/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatlvDe}}\\\hline
lt-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/lt-parl}} & \url{http://swpat.ffii.org/akteure/swpatlt/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatltDe}}\\\hline
it-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/it-parl}} & \url{http://swpat.ffii.org/akteure/it/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatitEn}}\\\hline
de-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/de-parl}} & \url{http://swpat.ffii.org/akteure/de/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatdeDe}}\\\hline
at-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/at-parl}} & \url{http://swpat.ffii.org/akteure/at/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatatDe}}\\\hline
dk-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/dk-parl}} & \url{http://swpat.ffii.org/akteure/dk/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatdkEn}}\\\hline
fr-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/fr-parl}} & \url{http://swpat.ffii.org/akteure/fr/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatfrEn}}\\\hline
es-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/es-parl}} &  & Addenda\footnote{\url{http://wiki.ffii.org/SwpatesEs}}\\\hline
pt-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/pt-parl}} & \url{http://swpat.ffii.org/akteure/pt/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatptEn}}\\\hline
gr-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/gr-parl}} & \url{http://swpat.ffii.org/akteure/gr/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatgrEn}}\\\hline
cy-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/cy-parl}} &  & Addenda\footnote{\url{http://wiki.ffii.org/SwpatcyDe}}\\\hline
ie-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/ie-parl}} & \url{http://swpat.ffii.org/akteure/ie/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatieEn}}\\\hline
pl-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/pl-parl}} & \url{http://swpat.ffii.org/akteure/pl/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatplEn}}\\\hline
cz-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/cz-parl}} & \url{http://swpat.ffii.org/akteure/swpatcz/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatczDe}}\\\hline
sk-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/sk-parl}} & \url{http://swpat.ffii.org/akteure/swpatsk/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatskDe}}\\\hline
hu-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/hu-parl}} & \url{http://swpat.ffii.org/akteure/swpathu/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpathuDe}}\\\hline
us-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/us-parl}} & \url{http://swpat.ffii.org/akteure/us/index.de.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatusDe}}\\\hline
jp-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/jp-parl}} & \url{http://swpat.ffii.org/akteure/jp/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatjpEn}}\\\hline
ca-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/ca-parl}} &  & Addenda\footnote{\url{http://wiki.ffii.org/SwpatcaDe}}\\\hline
au-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/au-parl}} & \url{http://swpat.ffii.org/akteure/swpatau/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatauEn}}\\\hline
ru-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/ru-parl}} & \url{http://swpat.ffii.org/akteure/ru/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatruEn}}\\\hline
cn-parl\footnote{\url{http://lists.ffii.org/mailman/listinfo/cn-parl}} & \url{http://swpat.ffii.org/akteure/cn/index.en.html\#tasks} & Addenda\footnote{\url{http://wiki.ffii.org/SwpatcnEn}}\\\hline
\end{tabular}
\end{center}

Die Hauptaufgabe dieser Foren liegt darin, Adressen von Mittlern zu sammeln, die es anzusprechen gilt, Entw\"{u}rfe f\"{u}r Schreiben zu diskutieren, Informationen \"{u}ber Antworten zu teilen und Dokumente vorzubereiten, die unseren Unterst\"{u}tzern helfen k\"{o}nnen, wirksam zu kommunizieren.
\end{quote}
\filbreak
\end{itemize}

(Medien kontaktieren Artikel schreiben siehe auch Nachrichtenmedien und Software-Patente\footnote{\url{http://swpat.ffii.org/akteure/media/index.en.html}} und \url{http://plone.ffii.org/media/})

(Spenden S. unseren Plan\footnote{\url{http://www.ffii.org/proj/plan/index.en.html}}, der aufzeigt, wie das Geld verwendet werden soll. \begin{description}
\item[Paypal:]\ Wenn Sie in Gro{\ss}britannien sind, m\"{o}chten Sie vielleicht an FFII UK\footnote{\url{http://www.ffii.org.uk/}} spenden:
\item[Moneybookers:]\ moneybookers@ffii.org
\item[Bankkonto:]\ \begin{description}
\item[Land:]\ DE (Deutschland)
\item[Bankleitzahl:]\ 70150000
\item[BIC (bank identifier code):]\ SSKMDEMM
\item[SWIFT:]\ SSKMDEMM
\item[Name der Bank:]\ Stadtsparkasse Muenchen
\item[Anschrift der Bank:]\ DE 80335 Muenchen\\
Lothstr 1
\item[Konto:]\ 31112097
\item[IBAN (Internationale Kontonummer)\footnote{Es soll m\"{o}glich sein, mithilfe der IBAN zur inl\"{a}ndischen Geb\"{u}hr aus dem euorp\"{a}ischen Ausland zu \"{u}berweisen.  Hierzu werden die Banken durch eine EU-Richtlinie gezwungen.}:]\ DE78701500000031112097
\item[Kontoinhaber:]\ FFII e.V.
\item[Anschrift des Kontoinhabers:]\ DE 80636 Muenchen\\
Blutenburgstr 17
\item[Verwendungszweck:]\ K\"{u}rzel f\"{u}r das Projekt\footnote{\url{http://www.ffii.org/proj/index.de.html}}, dem Ihre Spende zu gute kommen soll.  Im Zweifelsfalle einfach ``swpat'' und ``ffii'' angeben.
\end{description}
\end{description})
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Aufgaben und Projekte f\"{u}r eine Freie Informationelle Infrastruktur\footnote{\url{http://www.ffii.org/proj/index.de.html}}}}

\begin{quote}
Sie k\"{o}nnen Projekte einrichten und unterst\"{u}tzen.  Wir berichten aus unserer Projektverwaltungsdatenbank den aktuellen Stand der bisherigen Projekte.
\end{quote}
\filbreak

\item
{\bf {\bf PR-Materialien\footnote{\url{http://www.ffii.org/proj/plan/index.en.html}}}}

\begin{quote}
Unsere Anliegen Programmierern und der gr\"{o}{\ss}eren \"{O}ffentlichkeit durch Veranstaltungen und Vorf\"{u}hrungen verst\"{a}ndlich machen
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgunka.el ;
% mode: latex ;
% End: ;

