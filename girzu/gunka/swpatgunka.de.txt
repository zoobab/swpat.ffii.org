<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Die Patentbewegung konnte über Jahrzehnte hinweg Unternehmen und Regierungen für ihre Expansionsbestrebungen gewinnen.  Rollende Züge sind schwer anzuhalten.  FFII und Andere haben sich seit Jahren mit Erfolg dieser Arbeit gewidmet.  Hier erklären wir, wie Sie unsere Arbeit voranbringen können.

#title: Schluss mit der Patentinflation: Wie Sie helfen können

#stf: Unterzeichnen Sie die %(PET)

#LsW: Verweisen Sie von Ihrer Webseite aus auf %(URL), verwenden Sie das %(lg:logo) oder andere %(ng:nette Grafiken).  Sprechen Sie mit Leuten in Ihrer Umgebung und gewinnen Sie sie dafür, das gleiche zu tun. 

#WrW: Rückhalt in nationalen Parlamenten und Regierungen gewinnen.

#dor: Web-Adressen verschiedener Europäischer Parlamente

#Wve: DRINGEND: die vom Europäischen Parlament geänderte Richtlinie verteidigen.

#nut: Politiker kontaktieren, um die Position des %(sc:Europäischen Ministerrates) zu beeinflussen.  Tragen Sie sich in einen der folgenden Verteiler ein.

#arp: Auch außerhalb Europas kann man etwas tun.  Eine der wesentlichen Waffen der Patentbewegung liegt in einer fehlerhaften Auslegung des %(tr:TRIPs-Vertrages), mithilfe derer auf WTO-Ebene Scheingefechte entfaltet werden könnten, von denen sich vielleicht manche Regierungschefs einschüchtern lassen.

#dni: Ergänzen Sie die Dokumentation, indem Sie %(ew:Wiki-Erweiterungen) bestimmter FFII-SEiten bearbeiten.

#tei: FFII-Projekte gründen und unterstützen

#aktiv: Melden Sie sich in dem FFII/Eurolinux-Teilnahmesystem an, unterzeichnen Sie Appelle, melden Sie sich auf Nachrichtenverteilern und Veranstaltungen an, adoptieren Sie Patente, adoptieren Sie Abgeordnete und andere politische Vertreter.

#ihs: Texte in diese Webestätte einbauen

#aeW: Hiesige Webseiten übersetzen

#ron: Unsere Arbeit ist in Form von Projekten strukturiert.  Sie können laufende Projekte unterstützen und eigene gründen.

#lvo: S. auch die einschlägigen %(pr:Projekte).

#Dnt: Spenden

#tWW: Wenn Sie in Großbritannien sind, möchten Sie vielleicht an %(UK) spenden:

#aAu: Bankkonto

#onr: Land

#aWd: Bankleitzahl

#aoa: Name der Bank

#ktr: Anschrift der Bank

#con: Konto

#raW: Internationale Kontonummer

#Ami: Es soll möglich sein, mithilfe der IBAN zur inländischen Gebühr aus dem euorpäischen Ausland zu überweisen.  Hierzu werden die Banken durch eine EU-Richtlinie gezwungen.

#cnw: Kontoinhaber

#usd: Anschrift des Kontoinhabers

#ewr: Verwendungszweck

#cps: Kürzel für das %(pp:Projekt), dem Ihre Spende zu gute kommen soll.  Im Zweifelsfalle einfach %(TXT) angeben.

#nlc: Den Aufruf zum Handeln unterzeichnen

##? HINWEIS
#spa: Diese Petition ist sehr einfach, unspezifisch und altehrwürdig.  Sie sollte noch auf 1 Million Unterzeichner wachsen.

##? HINWEIS
#Wam: Indem Sie diesen Appell unterzeichnen und sich im Mitwirkungssytem eintragen, können Sie Ihren Art der Mitwirkung im Detail einstellen.

#oti: You can also participate in editing the source data of this site, see %(DOK).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatgunka ;
# txtlang: de ;
# multlin: t ;
# End: ;

