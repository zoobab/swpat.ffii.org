<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wie Sie helfen können, die Invasion des Patentwesens abzuwehren

#descr: Die Patentbewegung konnte über Jahrzehnte hinweg viele Unternehmen und
Regierungen für ihr Credo gewinnen.  Rollende Züge sind schwer
anzuhalten.  FFII und Andere haben sich dennoch seit ein paar Jahren
mit Erfolg dieser Arbeit gewidmet.  Hier erklären wir, wie Sie unsere
Arbeit voranbringen können.

#oae: Petition für ein Softwarepatent-Freies Europa

#tta: wohl die größte bisherige Online-Petition in IT-Angelegenheit

#sta: neueste Anregungen zum Handeln

#Wve: Unsere dringendste politische Aufgabe ist es, die vom Europäischen
Parlament geänderte Richtlinie gegen den Angriff des EU-Ministerrates
zu verteidigen.  Der Rat wird von den Ministerialbeamten beherrscht,
die am Europäischen Patentamt den Karren in den Dreck gefahren haben. 
Diese Leute möchten die Änderungen des Parlamentes und notfalls auch
das ganze Richtlinienprojekt möglichst ohne Diskussion kippen.

#stf: Unterzeichnen Sie die %(PET)

#spa: Diese Petition ist sehr einfach, unspezifisch und altehrwürdig.  Sie
sollte noch auf 1 Million Unterzeichner wachsen.

#nlc: Den %(ca:Aufruf zum Handeln) unterzeichnen

#Wam: Indem Sie diesen Appell unterzeichnen und sich im Mitwirkungssytem als
Unterstützer eintragen, können Sie sich an unseren Kampagnen
beteiligen.

#aktiv: Melden Sie sich im %(at:Mitwirkungssystem an)

#stt: Unterzeichnen Sie Appelle, melden Sie sich auf Nachrichtenverteilern
und Veranstaltungen an, adoptieren Sie Patente, adoptieren Sie
Abgeordnete und andere politische Vertreter.

#lte: Geben Sie Ihre volle Adresse, Telefonnummer etc an, so dass Sie
angesprochen werden können, wenn lokale Aktionen vonnöten sind.

#LsW: Verweisen Sie von Ihrer Webseite aus auf %(URL), verwenden Sie das
%(lg:logo) oder andere %(ng:nette Grafiken).  Integrieren Sie unseren
%(rs:Nachrichtenstrom) in Ihren Web-Auftritt. Sprechen Sie mit Leuten
in Ihrer Umgebung und gewinnen Sie sie dafür, das gleiche zu tun.

#aeW: Hiesige Webseiten übersetzen

#ihs: Texte in diese Webestätte einbauen

#dni: Ergänzen Sie die Dokumentation, indem Sie %(ew:Wiki-Erweiterungen)
bestimmter FFII-SEiten bearbeiten.

#yWi: If you have found any news, enter then into %(DOK) or %(sm:submit them
into the static page via mail).

#oti: Sie können auch teilnehmen, indem Sie Quelldaten dieses Sites
editieren, siehe %(DOK).

#tei: FFII-Projekte gründen und unterstützen

#ron: Unsere Arbeit ist in Form von Projekten strukturiert.  Sie können
laufende Projekte unterstützen und eigene gründen.

#cae: Kontaktieren Sie Politische Vertreter

#pns: Hierbei kann es sich ebenso um Mitglieder nationaler Parlamente wie um
Vertreter sektorieller Körperschaften (z.B. Gewerbeverbände) oder auch
um Ministerialbeamte handeln, die daran interessiert sein könnten, der
nationalen Regierung auf die Finger zu schauen.

#Wnn: Sie sollten Ihre Vertreter auch dann kontaktieren, wenn Sie außerhalb
der EU leben.  Die Patentlobby übt auch dadurch auf die EU Druck aus,
dass sie in internationalen Foren wie WIPO Regierungen vor ihren
Karren spannt oder dass sie internationale Verträge wie %(tt:TRIPs)
tendenziös auslegt.   Derzeit ist ein %(sp:Vertrag über das Materielle
Patentrecht) in Vorbereitung, der es allen Ländern schwer bis
unmöglich machen soll, die Patentierbarkeit zu begrenzen.  Es wäre
sinnvoll, innerhalb Ihres Landes das Thema vor das Parlament zu
bringen, statt es den %(q:Patentexperten) internationaler Foren zu
erlauben, die Souveränität Ihres Landes zu ihren Gunsten zu
beschneiden.

#Wei: Effektive Kommunikation

#ihf: Kommunizieren Sie! Lassen Sie nicht nur Dampf ab!

#WWW: Wählen Sie einen politischen Vertreter aus Ihrer Region oder Ihres
Berufs.

#iun: Sie müssen bereit sein, am Ball zu bleiben.  Halten Sie Telefonnummern
bereit, bevor Sie schreiben.  Rufen Sie zu geeigneter Zeit an.  Senden
Sie nicht Ergüsse von allem, was Ihnen gerade durch den Kopf geht.  
Beleidigen Sie nicht.  Spammen Sie nicht.

#tde: In der Kürze liegt die Würze.  Stellen Sie Fragen, die Ihr Vertreter
zu antworten geneigt sein könnte.  Schreiben Sie klar und höflich, was
Ihr Vertreter als nächstes tun sollte.  Lagern Sie alles andere in
Anhänge aus.  Der Anhang darf ruhig umfangreich sein.  Dies gilt
insbesondere für Papierbriefe.

#stW: Erklären Sie, wie Sie persönlich betroffen sind.  Spekulationen über
das Gemeinwohl ist weniger interessant und sollte denen überlassen
werden, die dafür kompetent sind (z.B. wirtschaftswissenschaftliche
Traktate im Anhang).

#hcW

#Wen

#oWa

#hWi

#ogy: Schlagen Sie vor, Ihr Vertreter möge

#geF: sich als Unterzeichner des %(CFA) auflisten lassen

#atr: an einer Parlamentarischen Anfrage zur Patentierbarkeit von Software
und zum Verhalten der Regierung mitwirken

#lWC: an einer Stellungnahme des Parlaments / eines parlamentarischen
Ausschusses / eines Verbandes mitwirken.  Das Parlament sollte
insbesondere (1) die Patentjustiz (2) die Regierung dazu anhalten,
dafür zu sorgen, dass Programmierer sich nicht über Patente sorgen
machen müssen und dass das geltende Gesetz und der Wille des
Europäischen Parlamentes entsprechend respektiert werden.

#teI: die nationalen Vertreter des FFII kontaktieren.

#csp: Beizufügende Dokumente

#rRr: Weitere Unterlagen

#dor: Web-Adressen verschiedener Europäischer Parlamente

#ani: Foren

#sii: Diskussionsforum

#Wce: statische Seite

#wiki: Wiki

#WiW: Die Hauptaufgabe dieser Foren liegt darin, Adressen von Mittlern zu
sammeln, die es anzusprechen gilt, Entwürfe für Schreiben zu
diskutieren, Informationen über Antworten zu teilen und Dokumente
vorzubereiten, die unseren Unterstützern helfen können, wirksam zu
kommunizieren.

#nWM: Medien kontaktieren

#ire: Artikel schreiben

#Dnt: Spenden

#pWe: S. unseren %(pp:Plan), der aufzeigt, wie das Geld verwendet werden
soll.

#tWW: Wenn Sie in Großbritannien sind, möchten Sie vielleicht an %(FU)
spenden:

#aAu: Bankkonto

#onr: Land

#aWd: Bankleitzahl

#aoa: Name der Bank

#ktr: Anschrift der Bank

#con: Konto

#raW: Internationale Kontonummer

#Ami: Es soll möglich sein, mithilfe der IBAN zur inländischen Gebühr aus
dem euorpäischen Ausland zu überweisen.  Hierzu werden die Banken
durch eine EU-Richtlinie gezwungen.

#cnw: Kontoinhaber

#usd: Anschrift des Kontoinhabers

#ewr: Verwendungszweck

#cps: Kürzel für das %(pp:Projekt), dem Ihre Spende zu gute kommen soll.  Im
Zweifelsfalle einfach %(TXT) angeben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgunka.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatgunka ;
# txtlang: xx ;
# End: ;

