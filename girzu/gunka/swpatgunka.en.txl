<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: How you can help us stop software patentability

#descr: The patent movement has during several decades won the support of
large corporations and governments for its set of beliefs.  Rolling
trains are hard to halt.  Yet FFII and others have devoted themselves
to this work with considerable success.  Here we tell you how you can
help us move forward more quickly.

#oae: Petition for a Software Patent Free Europe

#tta: probably the largest online petition in IT policy matters so far.

#sta: latest instructions for action

#Wve: Our most urgent political task is to defend the %(ad:directive as
amended by the European Parliament) against the national governments
who are blindly trusting their %(q:patent experts) (those people who
have screwed things at the European Patent Office) and are, more than
any other group, determined to legalise software patents and, for this
purpose, to kill the Parliament's amendments or the whole directive
project without discussion.

#stf: Sign the %(PET)

#spa: This petition is very simple, time-honored and unspecific.  It must
still grow up to a million signators.

#nlc: Sign the %(ca:Call for Action II)

#Wam: By signing this call and registering as a supporter in the
participation system, you can participate in our campaigns.

#aktiv: Sign up in the %(at:Participation System)

#stt: Support petitions, sign up for mailing lists and events, adopt
patents, adopt members of the European Parliament and other
representatives.

#lte: Specify your full address, phone number etc, so that you can be
reached when there is need of local actions.

#LsW: Link to %(URL) from your page and display the %(lg:logo) or some other
%(ng:nice graphics), or insert our %(rs:RSS NewsFeed) into your site. 
Tell others in your sphere of influence, particularly software
companies, to do the same.

#aeW: Translate pages of this site

#ihs: Edit this site

#dni: Enrich the documentation by %(ew:editing wiki extensions) to specific
FFII pages.

#yWi: If you have found any news, enter then into %(DOK) or %(sm:submit them
into the static page via mail).

#oti: You can also participate in editing the source data of this site, see
%(DOK).

#tei: Create and Support FFII Projects.

#ron: Our work is structured in terms of projects.  You can support ongoing
projects and create new ones.

#cae: Contact Political Intermediaries/Representatives

#pns: These can be members of national parliaments, activists of sectoral
bodies (such as trade associations) or even ministerial officials who
might want to take interest in the secretive legislative dealings of
your national government.

#Wnn: You need to contact your representatives even if you live outside of
the EU.  E.g. the US government's patent policy is dominated by patent
officials who frequently intervene in the EU to push for unlimited
patentability, partially by claiming that the %(tt:TRIPs treaty)
demands this or by pushing for a new %(sp:Substantive Patent Law
Treaty) which is to demand this.  Also, there is no reason why non-EU
countries should not draw inspirations from the European Parliament's
decision to make their own laws.

#Wei: How to communicate effectively

#ihf: Communicate rather than just let off steam!

#WWW: Select a representative of your area or your profession.

#iun: You must be willing to spend time to follow up on each person to whom
you write.  Have phone numbers ready before you write. Don't just send
a braindump.  Don't insult.  Don't spam.

#tde: Make it short.  Ask questions that your rep will want to answer. 
Write clearly and politely what you want your rep to do.  Put
everything else into an appendix.  The appendix can contain many
documents.

#stW: Explain what is at stake for you.  Don't speculate too much about what
might be at stake for the public.

#hcW: Don't spend much time explaining how bad software patents are. 
Rather, explain in concrete terms what action your MEP must take to
prevent software patents.  Your instructions can include the
following:

#Wen: Make it clear that the proposals of the Commission and Council lead to
US-style unlimited patentability, that the Parliament's approach from
the 1st reading was good, and that, given the short time frame of the
2nd reading and the complete failure of the Council to take the
Parliament's concerns into account, the Parliament now has no other
choice but to reaffirm its previous stand.

#oWa: tell your MEP which of his colleagues he should consult and who is
less trustworthy (e.g. for German conservatives: ask them to talk to
Kauppi, Krings and Karas and to ask Wuermeling or Lehne some tough
questions).

#hWi: a pointer to the FFII's %(aa:amendments analysis pages) as a valuable
reference.

#ogy: Suggest that your MP should

#geF: Agree to being listed as a signatory of the %(CFA)

#atr: work on a Parliamentary Inquiry on Software Patentability and/or on
the government's failure to handle legislative processes in an
accountable way

#lWC: work on a parliamentary resolution that (1) asks national patent
courts to treat patents on rules for computation using general-purpose
computing equipment (programs for computer) as void, (2) express
support the directive as amended by the European Parliament (3) ask
the government to oppose any decisionmaking by the Council that has
not received majority support from the national parliament.

#teI: contact the national representative of the FFII's campaign.

#csp: Documents to append

#rRr: Further Ressources

#dor: Web addresses of various national parliaments

#ani: Mailing Lists

#sii: Discussion List

#Wce: static page

#wiki: wiki

#WiW: The main task of these mailing lists is to collect addresses of people
to write to, to discuss letter drafts and to share information about
responses and prepare documentation that can instruct and inspire our
supporters to write useful letters.

#nWM: Contact the Media

#ire: Write articles.

#Dnt: Donate

#pWe: See our %(pp:plan) which outlines how we are using the money.

#tWW: If you are in the UK, you may want to donate to %(FU):

#aAu: Bank Account

#onr: Country

#aWd: Bank Code

#aoa: Name of Bank

#ktr: Bank's Postal Address

#con: Account

#raW: international bank account number

#Ami: We are told that, using the IBAN, money can now be transferred between
european countries for the domestic fee, thanks to a recent EU
internal market harmonisation directive which can be enforced against
banks.

#cnw: Account Owner

#usd: Account Owner's Postal Address

#ewr: Keyword

#cps: A project ID chosen from our %(pp:projects pages).  In case of doubt
just write %(TXT).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgunka.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatgunka ;
# txtlang: xx ;
# End: ;

