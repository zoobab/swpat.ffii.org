<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Comment vous pouvez nous aider à mettre fin a l'inflation de brevets

#descr: Le mouvement des brevets a pendant plusieurs décennies gagné le
soutien de grandes sociétés et de gouvernements pour l'ensemble de ses
idéologies. Il est difficile d'arrêter un train en marche. Cependant,
la FFII et d'autres se sont dévoués à cette tâche avec un succès
considérable. Ici, nous vous indiquons comment vous pouvez nous aider
à avancer plus rapidement.

#oae: pétition pour une Europe libre sans brevets logiciels.

#tta: Probablement la plus grande pétition en ligne jusqu'ici en ce qui
concerne les questions de politique sur les technologies
informatiques.

#sta: instructions à l'action plus nouvelles

#Wve: Notre tâche la plus urgente est de défendre la %(ad:directive telle
qu'amendée par le Parlement Européen) contre l'attaque du Conseil des
Ministres, c'est-à-dire contre les gouvernments nationaux qui ont
confié le dossiers à leurs %(q:experts en brevets), i.e. au groupe qui
est responsable pour les dérives illégales de l'Office européen des
brevets (OEB) et qui, plus qu'aucun autre groupe, veut absolument
légaliser les brevets logiciels, i.e. tuer les amendements du
Parlement européen et, si nécéssaire, tuer le projet de directive,
avec le moins de discussion publique possible.

#stf: Signez la %(PET)

#spa: Cette pétition est très simple, non limitée dans le temps et
non-spécifique.  Elle doit croître encore jusqu'à un million de
signataires.

#nlc: Signer l'%(ca:appel à l'action II).

#Wam: En signant cet appel et en vous enregistrant en tant que supporter
dans le système de  participation, vous pouvez participer à nos
campagnes.

#aktiv: Adhérez au %(at:système de participation).

#stt: Signez des pétitions, participez aux listes de diffusions et aux
événements,  adoptez des brevets, adoptez des membres du Parlement
Européen.

#lte: Donnez vos coordonées complètes (adresse et numéro de téléphone) pour
pouvoir être contacté si besoin lors d'actions locales.

#LsW: Mettez un lien vers %(URL) sur votre site et affichez le %(lg:logo) ou
d'autres %(ng:jolis dessins). Intégrez notre %(rs:fil d'actualité au
format RSS) sur votre site. Parlez-en à votre entourage et
particulièrement aux sociétés informatiques et encouragez-les à faire
de même.

#aeW: Traduire des pages de ce site.

#ihs: Publiez des textes sur ce site web.

#dni: Enrichissez la documentation en %(ew:éditant les extensions wiki)
liées à chaque page de la FFII.

#yWi: Si vous dénichez des nouvelles, entrez-les sur %(DOK) ou
%(sm:envoyez-les sur la page statique par mail).

#oti: Vous pouvez également participer en éditant les données sources de ce
site, voir %(DOK).

#tei: Soutenez des projets de la FFII ou créez-en de nouveaux.

#ron: Notre travail est organisé par projets. Vous pouvez contribuer aux
projets en cours ou en créer de nouveaux.

#cae: Contactez vos intermédiaires/représentants politiques

#pns: Il peut s'agir de membres des parlements nationaux, des activistes de
corporations dans ce secteurs (comme des associations de commerce) ou
même les fonctionnaires ministériels qui pourraient vouloir prendre
part aux tractations législatives sécrètes de votre gouvernement.

#Wnn: Vous devez contacter vos représentants même si vous vivez hors de
l'UE.  Par ex. la politique en matière de brevets du gouvernements des
USA est dominée par des fonctionnaires des brevets qui intervienne
fréquemment en europe pour appuyer la brevetabilité illimitée, entre
autres en affirmant que le %(tt:traité ADPIC) le requiert ou en
faisant pression pour un nouveau %(sp:Traité subtantiel du droit des
brevets) qui demanderait cela. Aussi, il n'y a aucune raison pour que
les pays hors de l'UE ne s'inspirent de la décision du Parlement
européen pour établir leurs propres lois.

#Wei: Comment communiquer efficacement

#ihf: Communiquer plutôt que brasser du vent !

#WWW: Choisissez un représentant de votre région ou dans votre profession.

#iun: Vous devez vouloir passer du temps pour suivre votre prise de contact.
Ayez les numéros de téléphones prêts avant d'écrire. N'envoyez pas de
bourrage de crâne,pas d'insultes, pas de spam.

#tde: Faites court. Posez des questions auxquelles votre représentant voudra
répondre. Écrivez clairement et poliment ce que vous voulez que votre
représentant fasse. Mettez tout le reste en annexe. Cette annexe peut
inclure plusieurs documents.

#stW: Expliquez ce qui est un enjeu pour vous. Ne spéculez pas trop sur ce
qui pourrait être un enjeu pour la collectivité publique.

#hcW

#Wen

#oWa

#hWi

#ogy: Suggérez à votre député :

#geF: D'accepter d'être publiquement listé comme signataire de l'%(CFA).

#atr: De travailler sur une enquête du parlement sur la brevetabilité du
loficiel et/ou sur l'échec du gouvernement à suivre les procédures
législatives de manière responsable.

#lWC: De travailler sur une résolution parlementaire qui (1) demanderait aux
tribunaux nationaux des brevets de traiter les brevets sur des règles
de calcul utilisant un équipement informatique générique (des
programmes d'ordinateurs) comme étant nuls, (2) soutiendrait la
directive telle qu'amendée par le Parlement européen et (3)
demanderait au gouvernement de s'opposer à toute prise de décision qui
n'aurait pas reçu un soutien de la majorité du parlement national.

#teI: De contacter le représentant national de la campagne de la FFII.

#csp: Documents à joindre

#rRr: Ressources supplémentaires

#dor: Adresses web des divers parlements nationaux

#ani: Listes de diffusion

#sii: Liste de discussion

#Wce: Page statique

#wiki: Wiki

#WiW: Le rôle principal de ces listes est de collecter les adresses des gens
à qui écrire, de discuter de brouillons de lettres et de partager les
réponses et de préparer une documentation pouvant informer et inspirer
nos soutients pour écrire des lettres utiles.

#nWM: Contactez les media.

#ire: Écrivez des articles.

#Dnt: Faites un don.

#pWe: Voir notre %(pp:programme) qui indique comment nous utilisons
l'argent.

#tWW: Si vous êtes en Angleterre, vous voudrez peut-être faire votre don à
%(FU).

#aAu: Compte bancaire

#onr: Pays

#aWd: Code de la banque

#aoa: Nom de la banque

#ktr: Adresse postale de la banque

#con: Compte

#raW: IBAN (numéro international de compte bancaire)

#Ami: À l'aide de l'IBAN, les transferts bancaires au sein de l'Union
Européenne sont soit-disant au tarif local depuis une récente
directive d'harmonisation du marché intérieur de l'Union Européenne
qui peut être opposée aux banques.

#cnw: Titulaire du compte

#usd: Adresse postale du titulaire

#ewr: Mot-clé

#cps: Un identifiant de projet choisi dans nos %(pp:pages projets). En cas
de doute, indiquez juste %(TXT).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgunka.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatgunka ;
# txtlang: xx ;
# End: ;

