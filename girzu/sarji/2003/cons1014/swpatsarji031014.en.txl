<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Winning in the Council

#descr: The ingredients needed for persuading the European Council to respect
the decision of the European Parliament

#iWS: Sustained presence in Brussels/Strasbourg

#rsa: Conference in Brussels in February 2004

#tlb: National Lobbying

#nrl: Coordination Work, Process Rationalisation

#Wtp: Patent Database Maintenance and Applications

#sne: Plans for Shanghai June 2004

#tnq: The Parliament has voted for Real Limits on Patentability.  Now the
ball has been passed to the European Council of Ministers, an
institution which has traditionally pushed for unlimited patentability
or, more recently, for fake limits on patentability.  Patent lawyer
interest groups are trying their best to discredit the Parliament and
to push the Council to %(q:throw the directive into the trash bin) or
to put pressure on the parliament in order to achieve their goals in a
second reading.

#Wmr: Although the Council is harder nut than the Parliament, it is not
uncrackable.  Here is what needs to be done.

#Wnil: We need to continue having at least one permanent representative
providing expertise to members of the European Parliament and regular
small-scale stays of external experts.

#Wea: The expertise provided will be in the field not only of the software
patent directive but also of the %(ip:IP enforcement directive), the
%(ns:European Network and Information Security Agency) and other
matters.

#W0W: This requires 3000 eur per month.

#tWe: Assemble leading scientists, stakeholders and politicians to a
conference in Brussels in February 2004, in order to benchmark the
various directive proposals.

#eae: The members of the Council's Patent Policy Working Group and other
decisionmakers should be invited as soon as possible.

#en1: This needs a workgroup and a budget of 5-10,000 eur.

#tWl: It is of special political importance, because some patent lawyers
believe that only they are the real %(q:IP experts) and uses this as a
pretext for discrediting the work of the Parliament.  The limelight of
an academic conference shows most convincingly that more of the real
critical and scientific work is being done by others.

#auj: Rudimentary national workgroups have been formed, see %(LST).

#ime: These will work better if some people can be paid (even if only as
symbolic compensation) to perform a few core functions and if there is
a budget for some expenses.

#pro: We need people set free from their work who can carry out the
following tasks:

#uWw: Invite some of our representatives and tell them that we (not EICTA)
are the industry and what our concerns are.

#tWe2: Provide us with lists of representatives or other people whom our
supporters should talk to.

#iee: Reach out to the media in order to bring the Council meetings and the
national representatives who are going to these meetings into the
limelight.

#vrt: Organise small-scale evening meetings with members of parliament and
other people who should or might take an interest in national policy
on the directive.

#uwv: This work needs to be done not only in Europe but also in the US and
elsewhere.  The patent policy process needs to be taken out ot he
hands of the patent lawyer lobby, which otherwise will threaten Europe
with the %(tl:TRIPs lie).  Although this is a lie, it can serve
politicians as a pretext or cover-up and thereby be effective.

#nWW: For each functioning national team 2000 eur should be made available. 
The UK team is planning a conference in november which may require a
bit more.

#srW: The task of coordinating a movement of 60,000 supporters require
several people's full-time work.  Processes need to be rationalised
and programmed in order to allow more to be achieved by fewer people. 
Quite good work has already been achieved by the aktiv.ffii.org
participation system and further plans are waiting to be implemented. 
Here again compensation of the core workers, at least at a symbolic
level, would help get things moving more quickly.

#WWb: We need to be able to provide more accurate information about the
current status of the patents granted by the EPO and elsewhere.

#eWh: This work helps us gain competence in patent policy benchmarking and
helps us address the problems of many people in the community of
potential patent infringers.  It also helps improve our communication
processes.

#dan: We need people regularly working on this.

#iws: There will be a major IT congress in Shanghai in 2004 in whose
Eurolinux will play a role.  This is not about patents policy, but it
can help us show organisational competence and gain opportunities also
for patent policy at a time where international support for the
European Parliament's position is necessary.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji031014 ;
# txtlang: en ;
# multlin: t ;
# End: ;

