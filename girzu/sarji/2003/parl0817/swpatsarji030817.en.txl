<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Help us Win the Plenary Vote!

#descr: The ingredients needed for persuading the European Parliament to vote
against the Software Patent Directive and how you can help make it
happen -- a letter to FFII/Eurolinux supporters

#ODI: CONFIDENTIAL

#Fuo: Dear FFII/Eurolinux %(su:supporter),

#Wee: You have registered via

#nWt: comme signataire et avez explicitement consenti à recevoir des
messages occasionnels, dont le champs est strictement limité à vous
permettre de supporter la cause.

#uoe: If it is important to you that you never receive any mail from us
again, please write to %(MT) and we will do our best to remove you
from the petition -- note that this is not easy because we lack a
secure way to do it automatically.

#lWr: If, on the contrary, you would like to hear more from us and be in
charge of how much you hear (in spam-politically correct manner), we
recommend you to register at the new participation system %(URL)

#aeW: If you are registered there already, you may be receiving our mail
twice this time.  Again we beg for your patience and understanding. 
Since different databases are used, eliminating the doubles is not a
trivial problem and not one which we can afford to solve under the
current time pressure.

#hea: The European Parliament is voting on September 1st on a proposal
which, while pretending to aim at restricting patentability, in
reality %(am:ensures that algorithms and business methods like Amazon
One Click Shopping %(tp|indisputably become patentable inventions|and
indisputably pass the bogus requirement of %(q:technical contribution
in the inventive step))).

#sit: As past experience shows, it is possible for powerful lobbies to push
incredibly bad directives through the European Parliament.  But it is
also possible to have them rejected.  Everything depends on how well
we use last week of August.

#dru: Needed: You in Brussels August Wednesday 27th

#xWg: Needed: Your Website Blocked on Wednesday August 27th

#eWa: Needed: 8,000 eur for demo and lobbying expenses

#WcW2: Needed: 3000 eur for lobbying the media

#tEc: Needed: Advertisement in European Voice et al

#oon: How to donate

#drs: Needed: Your Response NOW

#crf: We need your presence in Brussels on August.  We will stage a
performance on Luxemburg Square and a small conference in the
Parliament.

#aoW: We encourage you to participate in this event and to join our lobby
team in the parliament before and after that date.  You could also
contribute by arranging a meeting between your MEP and our team.

#aat: Beds will be available for free for those who particpate.

#btt: If the JURI proposal passes, important websites will be forced off the
Net in the near future.  It would seem preferable to take them off the
Net now, as a demonstration, for a few days.  Or, in a more gentle
manner, to hide the content behind a %(q:Page Closed) screen of the
following kind:

#WWW: Many pages are already hidden in this way and some will continue to
stay hidden until after the vote.

#dWr: You might consider hiding some or all pages at least on August 27th
and persuading your friends, especially those who administer
much-visited software download pages, to do likewise.

#lda: In June and July, we have subsidised the work of a handful of
activists who spread the word within the European Parliament.  We will
do more of this again starting on August 25th.  For each activist we
pay the trip, a bed and breakfast plus around 50 eur per day.

#als: In order to reach out to all 626 MEPs during the last week of August,
we need 10 knowledgable people from different nationalities to spend
that week in Brussels.

#WfW: You can support the work of our lobbyists by calling your MEPs on your
own.  We have arranged a series of helpdesks at %(LST) where you can
receive hints as to whom to talk to and what to say.  These again need
to be staffed and paid.

#sie: The result of our concerted effort should be that fewer of McCarthy's
amendments and more of %(q:ours) achieve a majority.  This would send
the draft report back to the JURI committee and open another round of
negotiations there.

#Wcd: If you want to donate for this project, please use the keyword
%(c:europarl).  If you want to restrict the use of the donation to a
particular country, specify %(c:europarl-uk) or analogous.

#rWn: Arlene McCarthy and friends are concentrating great efforts on
spreading misinformation in the press.  Influential journalists have
helped, sometimes against their better knowledge, to create the
impression that McCarthy is trying to restrict patentability.  See the
documentation at

#eot: We have however also actively contacted media and produced a series of
press releases.  It is largely due to this activity that a majority of
media, including some mainstream media such as Der Spiegel, Le Monde,
Libération, The Guardian, Süddeutsche Zeitung and others treated
McCarthy's proposal as what it is: an attempt to introduce US-style
unlimited patentability in Europe.  Much of this happened because we
actively established contacts with the potentially interested
journalists and gained their confidence as a reliable source of
information.

#rWv: We have however not yet succeded in finding our way into many radio
and television programs.  This again could be changed with a concerted
and paid effort.

#hbW: As with the Europarl work, the key helpers should not only directly
contact media themselves but also provide coaching for volunteers who
want to help.  Again we are providing a series of helpdesk addresses
%(LST) for this and would like these to be reachable by phone as well.

#hWk: Please consider either staffing such a helpdesk or donating for it or
volunteering to support the work by taking on some media yourself.

#drc: The keyword for donations in this area is, as you might expect:
%(c:media), %(c:media-fr), %(c:media-nl), etc.

#iur: Some MEPs have advised us to insert an advertisement into a newspaper
which is read by MEPs, such as %(e:European Voice), in the week before
the vote.  This would cost around 10000 eur.  It seems to have been
effective in the past.

#yec: This however seems a little too big at the moment.  Only when the
other items have been financed will we plan actions at this level.

#aoW2: The bank account for a donation is %(BA)

#onr: Country

#aWd: Bank Code

#aoa: Name of Bank

#ktr: Bank's Postal Address

#con: Account

#raW: international bank account number

#Ami: We are told that, using the IBAN, money can now be transferred between
european countries for the domestic fee, thanks to a recent EU
internal market harmonisation directive which can be enforced against
banks.

#cnw: Account Owner

#usd: Account Owner's Postal Address

#ewr: Keyword

#adW: Alternatively you can donate via %(PW) to %(PM).

#WnW: FFII is acknowledged as a public-interest association and we can send
you a receipt that at least in Germany makes the donation
tax-deductible.  We receipts for all donations of 50 eur or more every
3 months.

#awk: Basic information on how to help is being maintained at

#sbi: Please respond to this letter by ticking the following checkboxes

#diW: I would prefer to receive no more mail from you.

#let: I will be at the demo on Aug 27th 12.00

#iru: I would like to contribute to preparing the demo of Aug 27th 12.00

#bcg: I will be at the conference in the EP on Aug 27th 14.00

#inA: I would like to speak at one of the events on Aug 27th

#Woe: I want to participate in the lobbying team for at least 1/2 day

#oWt: I want to participate in the lobbying team for at least 1 day

#oWt2: I want to participate in the lobbying team for at least 2 days

#WeA: I need a place to sleep in BXL on Aug 26-27

#WeA2: I need a place to sleep in BXL on Aug 25-26

#WeA3: I need a place to sleep in BXL on Aug 27-28

#tWW: I will contact MEP(s) and arrange for a meeting with the lobbying team

#ito: I have followed the links listed at %(URL) and made myself familiar
with the various proposals which are under discussion in the European
Parliament

#lei: I will participate in the online demo.

#isi: I would like to help bring the software patent issue into the media.

#Wii: I'd like to help with graphic design of advertisements etc.

#lps: I'd like to help with translating.

#tse: I'd like to dedicate a few days or half-days next week to help.

#iea: I will make a donation.

#Wps: I represent

#ylo: myself only

#mxx: company xxxxxxx.com

#anx: organisation xxxxxxx.org

#htt: I'd like to have one or more %(ts:t-shirts) or other demo-related
articles sent to me.

#Wcs: Thank you very much for your support!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030817 ;
# txtlang: en ;
# multlin: t ;
# End: ;

