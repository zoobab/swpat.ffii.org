<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ajude-nos a Ganhar o Voto em Plenário!

#descr: Os ingredientes necessários para presuadir o Parlamento Europeu a
votar contra a proposta de Directiva de Patentes de Software e como
pode ajudar a conseguir este objectivo -- uma carta aos apoiantes da
FFII/Eurolinux

#ODI: CONFIDENCIAL

#Fuo: Caro %(su:apoiante) da FFII/Eurolinux,

#Wee: Registou-se via

#nWt: como um apoiante da FFII/Eurolinux e dessa forma consentido receber
emails ocasionais, cujo âmbito é estrictamente limitado a permitir-lhe
ajudar a suportar a causa. Se discorda, pode sempre diminuir o seu
status de %(q:apoiante) para %(q:utilizador das ofertas informativas
da FFII) ou mais baixo ainda via o menu no URL acima, e nunca mais
receberá os nossos emails.

#uoe: If it is important to you that you never receive any mail from us
again, please write to %(MT) and we will do our best to remove you
from the petition -- note that this is not easy because we lack a
secure way to do it automatically.

#lWr: If, on the contrary, you would like to hear more from us and be in
charge of how much you hear (in spam-politically correct manner), we
recommend you to register at the new participation system %(URL)

#aeW: If you are registered there already, you may be receiving our mail
twice this time.  Again we beg for your patience and understanding. 
Since different databases are used, eliminating the doubles is not a
trivial problem and not one which we can afford to solve under the
current time pressure.

#hea: O Parlamento Europeu votará a 1 de Setembro uma proposta que, enquanto
finge ter o objectivo de restringir a patenteabilidade, na realidade
%(am:garante que algoritmos e modelos de negócio como o One Click
Shopping da Amazon %(tp|indisputably become patentable inventions|e
indisputavelmente passam o requisito inútil de %(q:contribuição
técnica no passo inventivo))).

#sit: Como a experiencia anterior demonstra, é possível aos lobbies
poderosos empurrar directivas incrivelmente más pelo Parlamento
Europeu. Mas também é possível consiguir a sua rejeição. Tudo depende
de quão bem aproveitada for a última semana de Agosto.

#dru: Necessário: Você em Bruxelas a 27 de Agosto

#xWg: Necessário: O seu WebSite bloqueado a 27 de Agosto

#eWa: Necessário: 8,000 eur para despesas da manifestação etc

#WcW2: Necessário: 3000 eur para fazer lobby com os media

#tEc: Necessário: Publicidade na European Voice et all

#oon: Como doar

#drs: Needed: Your Response NOW

#crf: Precisamos da sua presença em Bruxelas em Agosto. Vamos organizar uma
actuação na Praça do Luxemburgo e uma pequena conferência no
Parlamento.

#aoW: We encourage you to participate in this event and to join our lobby
team in the parliament before and after that date.  You could also
contribute by arranging a meeting between your MEP and our team.

#aat: Serão providenciadas camas gratuitas para aqueles que participarem

#btt: Se a proposta da JURI passar, websites importantes serão forçados a
serem desligados da Net no futuro próximo. Seria preferível
desligá-los agora, como uma manifestação, por alguns dias. Ou, de uma
forma mais gentil, esconder o conteúdo com uma %(q:Página Fechada) no
seguinte estilo:

#WWW: A página principal www.ffii.org está já escondida desta forma e
continuará assim até após a votação.

#dWr: Poderia considerar esconder algumas ou todas as páginas pelo menos em
27 de Agosto e persuadir os seus amigos, especialmente aqueles que
administram páginas de download de software muito visitadas, a fazer o
mesmo.

#lda: Em junho e Julho pagamos a muitos activistas para poderem espalhar a
informação pelo Parlamento Europeu. Na última semana de Agosto vamos
ter uma sala no Parlamento Europeu que é utilizada por 10 activistas
bem preparados. Cada activista pode conseguir atingir 50 a 100
Eurodeputados numa semana, contribuindo para algum nível de percepção.
Dependendo no seu conhecimento e capacidades, cada representante pode
também tornar-se uma fonte de peritagem de confiança para alguns
Eurodeputados. Para cada representante pagamos a viagem, uma cama e
pequeno almoço num hotel barato, e 50 eur de despesas por dia.

#als: De forma a chegar a todos os 626 Eurodeputados durante a última semana
de Agosto, precisamos de 10 pessoas bem informadas de diferentes
nacionalidades para que passem essa semana em Bruxelas.

#WfW: Pode suportar o trabalho dos nossos activistas chamando você mesmo os
seus Eurodeputados. Temos uma série de helpdesks na %(LST) onde pode
receber pistas a respeito de com quem falar e o que dizer. Olivier
Albiez e outros já têm feito isto durante as últimas semanas, e tem-se
provado ser algo que vale a pena fazer. Gostaríamos de elevar os o
esforço e providenciar hotlines telefónicas com atendimento regular.
De forma a garantir que é feito um forte efeito de acompanhamento,
gostariamos de compensar os nossos activistas com algo como 50 eur por
dia, que deverá incluir as despesas telefónicas.

#sie: O resultado do nosso esforço concertado deverá ser que menos emendas
de McCarthy e mais das %(q:nossas) consigam a maioria. Isto faria, na
menor das hipóteses, com que o rascunho do relatório seja enviado de
volta à comissão JURI e abrir um novo round de negociações.

#Wcd: Se quiser doar para este projecto, por favor utilize como descritivo o
termo %(c:europarl). Se quiser restringir a utilização da sua doação a
um país em particular, especifique %(c:europarl-pt) ou análogo.

#rWn: Arlene McCarthy e os seus amigos estão a concentrar grandes esforços a
espalhar desinformação pela imprensa. Jornalistas influentes tem
ajudado, alguns contra o seu próprio conhecimento, para criar a
impressão de que McCarthy está a tentar restringir a patenteabilidade.
Veja a documentação em

#eot: Contudo nós também temos activamente contactado os media e produzido
uma série de press releases. É devido largamente a esta actividade que
uma maioria dos media, inclusive alguns media de mainstream como o Der
Spiegel, Le Monde, Libération, O Expresso, Süddeutsche Zeitung, The
Guardian e outros tratam  a proposta de McCarthy tal como ela é: uma
tentativa de introduzir na Europa a patenteabilidade ilimitada ao
estilo dos Estados Unidos. Muito disto aconteceu porque temos
estabelecido contactos co jornalistas potencialmente interessados a
ganho a sua confiança como uma fonte de informação confiável.

#rWv: Contudo ainda não conseguimos chegar a muitas estações de rádio e
programas televisivos. Acredito que isto é principalmente devido à
falta de pessoas que podem ajudar. Isto poderia mudar se tivessemos
ajudantes que se comprementessem a fazer telefonemas e a passar-nos os
contactos. Esses ajudantes deveriam também ser pagos, se possível.

#hbW: Tal como o trabalho no Parlamento Europeu, os principais ajudantes não
deveriam contactar os media eles mesmos, mas providenciar
acompanhamento para os voluntários que querem ajudar. Novamente,
estamos a providenciar uma série de helpdesks %(LST) para este efeito
e gostaríamos que estes fossem acessíveis por telefone também. Cada
helpdesk deve documentar as suas actividades num Wiki de acesso
restricto que instalámos para este propósito.

#hWk: Por favor considere operar um destes helpdesks ou doar para um deles
ou voluntariar-se para suportar o trabalho contactando alguns media
você mesmo.

#drc: O termo descritivo para doações nesta área é, como seria de esperar:
%(c:media), %(c:media-pt), %(c:media-nl), etc.

#iur: O Eurodeputado Marco Cappato e outros têm nos aconselhado a introduzir
publicidade num jornal que é lido pelos Eurodeputados, tal como o
%(e:European Voice), na semana antes da votação. Isto custaria cerca
de 10000 eur. Parece ter sido eficiente no passado.

#yec: Tendemos a pensar que os donativos para isto são apenas úteis após os
outros ingredientes para a nossa victória tiverem sido financiados.
Uma vez que tivermos os meios, podemos voltar a contacta-lo com uma
proposta para preprar este tipo de Press Release, que actualmente
ainda está um nível acima das nossas capacidades.

#aoW2: A conta bancária para donativos é %(BA)

#onr: País

#aWd: Código Bancário

#aoa: Nome do Banco

#ktr: Morada Postal do Banco

#con: Conta

#raW: número internacional de conta bancária

#Ami: Fomos informados que, utilizando o IBAN, o dinheiro pode agora ser
transferido entre países europeus ao custo das taxas nacionais, graças
a uma directiva de harmonização do mercado que pode ser aplicada a
bancos.

#cnw: Dono da Conta

#usd: Morada Postal do Dono da Conta

#ewr: Termo Descritivo

#adW: Alternativamente, pode doar via %(PW) para %(PM).

#WnW: A FFII é reconhecida como uma associação de interesse público e
podemos enviar-lhe um recibo que, pelo menos na Alemanha, torna os
donativos deductíveis nos impostos. Enviamos recibos de todos os
donativos de 50 eur ou mais a cada 3 meses.

#awk: Informação básica de como ajudar é mantida em

#sbi: Por favor responda a este email marcando as checkboxes seguintes com
um X

#diW: I would prefer to receive no more mail from you.

#let: Estarei na manifestação em 27 de Agosto pelas 12h00

#iru: Gostaria de contribuir para a preparação da manifestação de 27 de
Agosto pelas 12h00

#bcg: Estarei na conferência no Parlamento Europeu em 27 de Agosto pelas
12h00

#inA: Gostaria de falar na conferência no Parlamento Europeu em 27 de Agosto
pelas 12h00

#Woe: Gostaria de participar na equipa de lobby por pelo menos meio dia

#oWt: Gostaria de participar na equipa de lobby por pelo menos um dia

#oWt2: Gostaria de participar na equipa de lobby por pelo menos dois dias

#WeA: Preciso de um local onde dormir em BXL em Agosto 26-27

#WeA2: Preciso de um local onde dormir em BXL em Agosto 26-27

#WeA3: Preciso de um local onde dormir em BXL em Agosto 26-27

#tWW: Contactarei Eurodeputados e reunirei com a equipa de lobbying

#ito: Segui os links listados em %(URL) e familiarizei-me com as várias
propostas que estão sobre discussão no Parlamento Europeu

#lei: Participarei na manifestação on-line.

#isi: Gostaria de ajudar a trazer o assunto das patentes de software junto
dos media.

#Wii: I'd like to help with graphic design of advertisements etc.

#lps: I'd like to help with translating.

#tse: I'd like to dedicate a few days or half-days next week to help.

#iea: Gostaria de fazer um donativo.

#Wps: Eu represento

#ylo: apenas a minha pessoa

#mxx: a companhia xxxxxxx.com

#anx: a organização xxxxxxx.org

#htt: I'd like to have one or more %(ts:t-shirts) or other demo-related
articles sent to me.

#Wcs: Muito obrigado pelo seu apoio

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030817 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

