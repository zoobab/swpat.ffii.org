<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Helft uns, das Plenum zu gewinnen!

#descr: Zutaten, die wir brauchen, um das Europäische Parlament zu überzeugen,
gegen die Softwarepatentrichtlinie zu stimmen, und wie Sie helfen
können, dass es auch passiert - ein Brief an Unterstützer von
FFII/Eurolinux

#ODI: VERTRAULICH

#Fuo: Sehr geehrte/r FFII/Eurolinux %(su:Unterstützer/in),

#Wee: Sie haben sich via

#nWt: und uns damit erlaubt, Ihnen %(q:Informationen über weitere Aktionen
zuzusenden, soweit diese in engem Zusammenhang mit dieser Petition
stehen.)  Wir machen von dieser Erlaubnis nur sehr zurückhaltend
Gebrauch, und diesmal ist vielleicht das letzte Mal.  Wenn Sie sich
belästigt fühlen, bitten wir um Nachsicht.  Die meisten Unterzeichner
wollen solche Benachrichtigungen, und alle haben implizit zugestimmt. 
Es ist in diesem späten Stadium auch nicht mehr machbar, die Petition
spam-politisch 100% korrekt umzuprogrammieren.  Dennoch handelt es
sich bei den Benachrichtigungen nicht um das, was man unter %(q:Spam)
versteht, und sie als solche zu behandeln würde wiederum einen
Netz-Missbrauch darstellen.

#uoe: Wenn Sie großen Wert darauf legen, nie mehr von uns angeschrieben zu
werden oder wenn Sie die Petition nicht (mehr) unterstützen wollen,
bitten wir Sie, an %(MT) zu schreiben.  Wir werden Sie dann aus der
Unterzeichnerliste entfernen.  Allerdings gibt es hier auch keine
zuverlässige Methode, dies automatisch zu tun.

#lWr: Wenn Sie andererseits mehr von uns hören möchten und jederzeit
kontrollieren möchten, wie viel Sie von uns hören, empfehlen wir
Ihnen, sich im neuen Mitwirkungssystem %(URL) einzutragen.

#aeW: Falls Sie dort schon eingetragen sind, werden Sie diesmal unser
Schreiben womöglich doppelt erhalten.  Auch hier bleibt uns nichts
anderes übrig, als um Ihr Verständnis zu bitten.  Es sind verschiedene
Datenbanken im Einsatz, und uns war es angesichts des Zeitdrucks nicht
möglich, aus den Auszügen der verschiedenen Datenbanken die
Überschneidungen zu entfernen.

#hea: Das Europäische Parlament hat am 1. September einen Vorschlag auf der
Tagesordnung, der, während er vorgibt, Patentierbarkeit zu
beschränken, in Wirklichkeit %(am:sicherstellt, dass Algorithmen und
Geschäftsmethoden wie Amazon One-Click-Einkauf %(tp|anstandslos
patentierbare Erfindungen werden|und anstandslos %(q:in ihrem
erfinderischen Schritt einen technischen Beitrag leisten))).

#sit: Wie die Erfahrung der Vergangenheit zeigt, ist für mächtige
Interessengruppen möglich, auch schlechte Richtlinien durch das
Europäische Parlament zu bringen. Aber es ist auch möglich, sie zu
kippen. Alles hängt davon ab, wie gut wir die letzte Augustwoche
nutzen.

#dru: Benötigt: Sie in Brüssel am 27. August

#xWg: Benötigt: Ihr Website gesperrt am 27. August

#eWa: Benötigt: 8,000 EUR für Demo und Lobbyarbeit

#WcW2: Benötigt: 3000 EUR für Medienlobbying

#tEc: Benötigt: Anzeige in European Voice u.a.

#oon: Wie man spendet

#drs: Benötigt: Ihre Antwort JETZT

#crf: Wir benötigen Ihre Anwesenheit in Brüssel in der letzten Augustwoche.
Wir werden eine Kundgebung neben dem Parlament und eine Konferenz im
Parlament veranstalten.

#aoW: Es wäre schön, wenn Sie dieser Veranstaltung teilnehmen und davor oder
danach unsere Aktivistengruppe im Parlament verstärken könnten.  Sie
könnten auch helfen, indem Sie ein Treffen zwischen Ihrem Abgeordneten
und unserer Gruppe vereinbaren.

#aat: Betten/Schlafgelegenheiten für Teilnehmer werden zur Verfügung
gestellt.

#btt: Wenn der JURI-Vorschlag durchgeht, werden in Zukunft wichtige Inhalte
vom Netz verschwinden. Es scheint besser, sie jetzt vom Netz zu
nehmen, als eine Demonstration, für ein paar Tage. Oder, etwas
freundlicher, ihren Inhalt hinter einer %(q:Fehlermeldungs)-Seite zu
verstecken, s. folgende Beispiele:

#WWW: Die www.ffii.org Titelseite ist bereits so behandelt und wird bis nach
der Abstimmung versteckt sein.

#dWr: Sie könnten einige oder alle Seiten zumindest am 27. August um 12.00
für eine Weile verstecken und auch Ihre Bekannten überzeugen, dies zu
tun.

#lda: Im Juni und Juli haben wir eine Anzahl von Aktivisten im Europäischen
Parlament unterstützt. Dies haben wir auch in der letzten Augustwoche
vor. Für diese bezahlen wir die Reise, Unterbringung zuzüglich ca 50
eur pro Tag.

#als: Um genügend der 626 MdEPs in der letzten Woche anzusprechen, brauchen
wir 10 verschiedene Personen von verschiedenen Nationalitäten, die die
Woche in Brüssel verbringen.

#WfW: Sie können die Arbeit unserer Lobbyisten unterstützen, in dem Sie
selbst MdEPs ansprechen. Es gibt bei %(LST) Helfer, von denen Sie
Hinweise erhalten  können.  Auch hier übernehmen wir einige Kosten.

#sie: Die Strategie ist, dass wir weniger von den Aenderungsvorschlaegen von
McCarthy und mehr von unseren %(q:unseren) eine Mehrheit erhalten.
Diese würde zuletzt den Bericht and die juristische Kommission
zurücksenden und dort eine neue Verhandlungsrunde eröffnen.

#Wcd: Wenn Sie für dieses Projekt spenden wollen, verwenden Sie bitte das
Schlüsselwort %(c:europarl). Wenn Sie es auf ein bestimmtes Land
beschränken wollen, geben Sie bitte %(c:europarl-de) usw an.

#rWn: Arlene McCarthy und Mitstreiter verwenden großen Aufwand darauf, ihre
irreführenden Behauptungen in die Presse zu bringen. Einflußreiche
Journalisten haben, manchmal gegen besseres Wissen, den Eindruck
erzeugt, dass McCarthy die Patentierbarkeit einschränken will. Siehe
Dokumentation bei

#eot: Auch wir haben eine Reihe von Presseerklärungen verfasst. Diese
Aktivität hat dazu beigetragen, dass viele Medien wie Der Spiegel, Le
Monde, Libération, The Guardian, Süddeutsche Zeitung und andere
McCarthy's Vorschlag als das behandelt haben was er ist: ein Versuch,
unbegrenzte Patentierbarkeit im US-Stil in Europa einzuführen. Viel
davon passierte, weil wir aktiv Kontakte mit potentiell interessieren
Journalisten gesucht haben und uns als verlässliche Informationsquelle
etabliert haben.

#rWv: Wir sind jedoch wegen mangelnder Ressourcen nicht in viele Radio- und
Fernsehprogramme gekommen.  Auch dies würden wir gerne durch eine
konzertierte Aktion überwinden, die Geld erfordert.

#hbW: Genauso wie für die Parlamentarier gibt es hier eine Reihe von %(LST).

#hWk: Bitte überlegen Sie, selbst so einen Mailaddresse zu betreiben oder es
zu unterstützen.

#drc: Das Schlüsselwort für Spenden ist wie erwaertet: %(c:media),
%(c:media-fr), %(c:media-nl), etc.

#iur: Einige MdEPs haben uns geraten eine Annonce in eine Zeitung
einzubauen, die von MdEPs gelesen wird, wie etwa %(e:European Voice),
in der Woche vor der Abstimmung. Die Kosten wären 10000 eur. In der
Vergangenheit soll es viel bewirkt haben.

#yec: Das scheint uns im Moment aber noch einen Schritt zu gross zu sein.
Erst wenn die anderen Dinge erledigt sind, planen wir in dieser
Größenordnung.

#aoW2: Das Bankkonto für eine Spende ist %(BA)

#onr: Land

#aWd: Bankleitzahl

#aoa: Name der Bank

#ktr: Postanschrift der Bank

#con: Konto

#raW: International Bank Account Number

#Ami: Wegen einen neuen Harmonisierungsrichtlinie sind Überweisungen
innerhalb der EU nicht teurer als nationale Überweisungen, wenn die
IBAN verwendet wird.

#cnw: Kontoinhaber

#usd: Postanschrift Kontoinhaber

#ewr: Schlüsselwort

#adW: Alternativ kann auch via %(PW) an %(PM) gespendet werden.

#WnW: FFII ist als gemeinnützig anerkannt kann eine zumindest innerhalb
Deutschlands steuerabzugsfähige Spendenbescheinigung versenden, was
wir routinemäßig bei Beträgen von >50 EUR auch tun.

#awk: Grundlegende Information wie man helfen kann wird unterhalten bei

#sbi: Bitte antworten Sie auf diesen Brief, indem Sie im folgenden
ankreuzen:

#diW: Ich würde lieber keine Post mehr von Ihnen bekommen.

#let: Ich werde bei der Demo sein am 27. August 12.00

#iru: Ich würde gerne die Demo vorbereiten am 27. August 12.00

#bcg: Ich werde an der Konferenz am EP am 27. August 14.00 teilnehmen

#inA: Ich würde gerne an der Konferenz im EP am 27. August als Sprecher
teilnehmen

#Woe: Ich würde gerne an der Lobbygruppe für mindestens 1/2 Tag teilnehmen

#oWt: Ich würde gerne an der Lobbygruppe für mindestens 1 Tag teilnehmen

#oWt2: Ich würde gerne an der Lobbygruppe für mindestens 2 Tage teilnehmen

#WeA: Ich brauche eine Übernachtung in Brüssel am 26.-27. August

#WeA2: Ich brauche eine Übernachtung in Brüssel am 25.-26. August

#WeA3: Ich brauche eine Übernachtung in Brüssel am 27.-28. August

#tWW: Ich werde MdEPs kontaktieren und ein Treffen mit der Lobbygruppe
vereinbaren

#ito: Ich bin den Links bei gefolgt %(URL) und habe mich mit den
verschiedenen Vorschlägen wie sie am Europäischen Parlament diskutiert
werden auseindergesetzt

#lei: Ich werde an der Online-Demo teilnehmen

#isi: Ich möchte das Softwarepatentproblem in die Medien bringen.

#Wii: Ich wäre gerne bei der grafischen Gestaltung von Werbung etc
behilflich.

#lps: Ich würde gerne mit dem Übersetzen helfen

#tse: Ich würde nächte Woche gerne einige Tage oder Halbtage dieser Sache
widmen.

#iea: Ich werde spenden.

#Wps: Ich vertrete

#ylo: nur mich

#mxx: Firma xxxxxxx.com

#anx: Organisation xxxxxxx.org

#htt: Ich hätte es gerne, wenn man mir eines oder mehrere %(ts:T-Shirts)
oder andere demo-bezogene Artikel zuschicken würde.

#Wcs: Vielen Dank für Ihre Unterstützung!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030817 ;
# txtlang: de ;
# multlin: t ;
# End: ;

