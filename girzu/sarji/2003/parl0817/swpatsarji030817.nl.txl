<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Help ons de plenaire stemming te winnen!

#descr: De ingredienten nodig om het Europees Parlement te laten stemmen tegen
Software patenten. En Wat je kan doen om te helpen.-- een brief aan
alle FFII/Eurolinux supporters

#ODI: CONFIDENTIAL

#Fuo: Beste FFII/Eurolinux %(su:supporter),

#Wee: U hebt u geregistreerd via

#nWt: als een supporter van FFII/Eurolinux en hebt daarbij toestemming
gegeven om af en toe mails te ontvangen, die als enig doel hebben u op
de hoogte te houden van wat je kan doen om deze zaak te steunen.  Als
je hiermee niet akkoord gaat kan je uw status verlagen van
%(q:supporter) naar %(q:user of the ffii information offers) of lager
via het menu te vinden op de hoger gegeven URL, en zal je niet langer
onze mails ontvangen.

#uoe: If it is important to you that you never receive any mail from us
again, please write to %(MT) and we will do our best to remove you
from the petition -- note that this is not easy because we lack a
secure way to do it automatically.

#lWr: If, on the contrary, you would like to hear more from us and be in
charge of how much you hear (in spam-politically correct manner), we
recommend you to register at the new participation system %(URL)

#aeW: If you are registered there already, you may be receiving our mail
twice this time.  Again we beg for your patience and understanding. 
Since different databases are used, eliminating the doubles is not a
trivial problem and not one which we can afford to solve under the
current time pressure.

#hea: Het Europees parlement stemt op 1 september een voorstel dat, ondanks
het tot doel stellen van het limiteren van de patentibiliteit,het in
realiteit

#sit: Uit het verleden blijkt dat het mogelijk is voor machtige lobby's om
ongeloofelijk slechte richtlijnen door het Europees parlement te laten
goedkeuren. Het is echter ook mogelijk om ze te laten verwerpen. Alles
hangt af van de manier waarop we de laatste week van augustus
gebruiken.

#dru: Nodig: Uw aanwezigheid in Brussel op woensdag 27 augustus

#xWg: Nodig: Uw Website geblokkeerd op 27 augustus

#eWa: Nodig: 8,000 euro voor uitgaves voor het lobby-en en de protestacties

#WcW2: Nodig: 3000 euro voor het lobby-en in de media

#tEc: Nodig: Reclame en voorlichting in heel Europa

#oon: Hoe te doneren

#drs: Needed: Your Response NOW

#crf: We hebben uw aanwezigheid in Brussel nodig in augustus. We organiseren
een protestactie op hetLuxemburg-plein en een kleine conferentie in
het parlement.

#aoW: We vragen aan iedereen om mee te doen met die evenement en zich te
voegen bij ons lobby team in het parlement voor and na die datum. Je
kan ook helpen door het organiseren van een meeting tussen een
parlementslid van de Europese gemeenschap -MEP- en ons team.

#aat: Slaapgelegenheid is gratis beschikbaar voor de deelnemers.

#btt: In het geval dat het JURI voorstel erdoor komt, zullen belangrijke
websites van het net worden gedwongen in de nabije toekomst. Het is
daarom misschien beter om ze nu al off-line te halen, als vorm van
protest, voor enkele dagen. Of, op een minder drastische manier, door
de inhoud te verstoppen %(q:Page Closed) achter een pagina van de
vorm:

#WWW: De titelpagina van www.ffii.org is zo reeds verborgen en zal zo
blijven tot na de stemming.

#dWr: Je kan op z'n minst enkele of alle pagina's verbergen op 27 augustus
en uw vrienden te overtuigen, zeker diegenen met populaire
donload-paginas, hetzelfde te doen.

#lda: In juni en juli hebben we het werk van een handvol activisten
gesubsidieerd die onze standpunten hebben uitgelgd in het europees
parlement. We zullen meer van dit alles doen vanaf 25 augustus. Voor
elke activist betalen we de reis, voorzien we een bed, ontbijt en +/-
50 euro per dag.

#als: Om alle 626 europarlementsleden te bereiken tijdens de laatste week
van augustus, hebben we 10 mensen nodig van verschillende
nationaliteiten, die vertrouwd zijn met de materie en die week kunnen
doorbrengen in Brussel.

#WfW: Je kan ook het werk van onze lobby-isten ondersteunen door uw
europarlementsleden te contacteren op eigen initiatief.We hebben ook
een helpdesk voorzien %(LST) waar je informatie kan verkrijgen zoals
wie je moet aanspreken en wat te zeggen. Deze moeten dan ook bemand
worden en betaald.

#sie: Het resultaat van onze gezamenlijke inspanningen zou moeten zijn dat
er minder van McCarthy's amendmenten en meer van %(q:ours) een
meerderheid behalen.  Dit zou, op zijn minst,het voorstel terugsturen
naar het JURI commitén een nieuwe onderhandelingsronde openen.

#Wcd: Als je een donatie wilt doen voor ons project, gebruik als keyword
%(c:europarl).  Zou je uw donatie willen beperken tot een bepaald
land, gebruik dan %(c:europarl-uk) of iets gelijkaardig.

#rWn: Arlene McCarthy en haar vrienden doen veel moeite om het publiek te
misinformeren d.m.v. de pers. Invloedrijke journalisten hebben
geholpen, soms tegen beter weten in, om de indruk te wekken dat
McCarthy moeite doet om patentabiliteit te beperken.  Zie de
documentatie op

#eot: We hebben echter ook actief de media gecontacteerd en een aantal
persmappen verspreid. Het is grotendeels dankzij deze activiteit dat
een overgrote meerderheid van de media, grote kranten inbegrepen
zoals: Der Spiegel, Le Monde, Libération, The Guardian, Süddeutsche
Zeitung en anderen, McCarthy's voorstel hebben behandeld voor wat het
is: een poging om het ongecontroleerde Amerikaans patentensysteem te
introduceren in Europa. Dit was mogelijk door het contacten te leggen
met mogelijk ge﮴eresseerde journalisten en door hun vertrouwen te
winnen als een betrouwbare bron van informatie.

#rWv: We hebben echter nog geen toegang tot radio en televisie
programma's.Dit zou echter kunnen veranderen met een gezamenlijk en
betaalde inspanning.

#hbW: Zoals met het Europarl werk, zouden de belangrijkste helpers niet
allen direct met de media contact moeten opnemen, maar ook coaching
voorzien voor vrijwilligers die willen helpen. Hiervoor voorzien we
weeral in een aantal helpdesk addressen %(LST) en we zouden deze ook
graag telefonisch bereikbaar hebben.

#hWk: Overweeg misschien zelf zo een helpdesk te bemannen of ervoor te
doneren ofwel vrijwilliger te zijn om zelf wat media-activiteiten te
ondernemen.

#drc: De keywoorden voor donaties in dit gebied zijn, zoals te verachten:
%(c:media), %(c:media-fr), %(c:media-nl), enz.

#iur: Sommige europarlementsleden hebben ons geadviseerd een advertentie te
plaatsen in een tijdschrift of krant die door hen wordt gelezen, zoals
%(e:European Voice), de week voor de stemming.  Dit zou rond de 10000
euro kosten.  Het zou in ghet verleden efficient geweest zijn.

#yec: Dit is echter ietwat te duur voor de moment, alleen wanneer al de rest
gefinancieerd is zullen we acties ondernemen op dit niveau.

#aoW2: De bankrekening hiervoor is %(BA)

#onr: Land

#aWd: Bank Code

#aoa: Naam van de Bank

#ktr: Postadres van de bank

#con: Rekening

#raW: internationaal bankrekiningsnummer

#Ami: Er is ons verteld dat, gebruik makende van de IBAN, geld kan
getransfereerd worden tussen europese landen aan binnenlandse
tarieven, dankzij de harmonisatie van de europese interne markt.

#cnw: Eigenaar van de rekening

#usd: Postadres van de rekeninghouder

#ewr: Keyword

#adW: je kan ook doneren via %(PW) of %(PM).

#WnW: FFII is erkend als een public-interest vereniging en we kunnen u een
ontvangstbewijs sturen dat, op zijn minst in Duitsland, uw donatie
aftrekbaar maakt van de belastingen.We zenden deze ontvangstbewijzen
voor alle donaties van 50 euro of meer elke 3 maand.

#awk: De minimum informatie nodig om te helpen is te vinden op

#sbi: Gelieve te antwoorden op deze brief door het juiste hokje aan te
kruisen

#diW: I would prefer to receive no more mail from you.

#let: Ik zal aanwezig zijn op de protestactie van woensdag 27 augustus om
12u00

#iru: Ik zou willen bijdragen aan het organiseren van de protestactie van 27
augustus

#bcg: Ik zal aanwezig zijn op de conferentie in het europees parlement op 27
augustus om 14u00

#inA: Ik zou willen spreken op de conferentie in het europees parlement op
27 augustus om 14u00

#Woe: Ik wil meedoen met het lobby-team voor op zijn minst 1/2 dag

#oWt: Ik wil meedoen met het lobby-team voor op zijn minst 1 dag

#oWt2: Ik wil meedoen met het lobby-team voor op zijn minst 2 dagen

#WeA: Ik heb een slaapplaats nodig in Brussel op 26-27 augustus

#WeA2: Ik heb een slaapplaats nodig in Brussel op 25-26 augustus

#WeA3: Ik heb een slaapplaats nodig in Brussel op 27-28 augustus

#tWW: Ik zal een europarlementslid contacteren en een ontmoeting regelen met
het lobby-team.

#ito: Ik heb de links gevolgd op %(URL) en heb mijzelf vertrouwd gemaakt met
de verschillende voorstellen die voor de moment besproken worden in
het europees parlement.

#lei: Ik doe mee met de online protestactie.

#isi: Ik zou willen helpen om de problemen omtrent software patenten in de
media te brengen.

#Wii: I'd like to help with graphic design of advertisements etc.

#lps: I'd like to help with translating.

#tse: I'd like to dedicate a few days or half-days next week to help.

#iea: Ik maak een donatie.

#Wps: Ik vertegenwoordig

#ylo: enkel mijn eigen

#mxx: het bedrijf xxxxxxx.com

#anx: de organisatie xxxxxxx.org

#htt: I'd like to have one or more %(ts:t-shirts) or other demo-related
articles sent to me.

#Wcs: Hartelijk dank voor uw steun!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030817 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

