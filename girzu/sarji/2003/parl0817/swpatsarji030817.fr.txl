<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Aidez nous à gagner le vote en session plénière

#descr: Les ingrédients requis pour persuader le Parlement Européen de voter
contre la directive des Brevets Logiciels et comment vous pouvez aider
pour que cela arrive -- une lettre aux supporters de FFII/Eurolinux

#ODI: CONFIDENTIEL

#Fuo: Cher %(su:Signataire de la Pétition Contre les Brevets Logiciels),

#Wee: Vous vous êtes enregistrés via

#nWt: comme signataire et avez consenti explicitement à recevoir des mails
occasionnels, dont le champs est strictement limité à vous permettre
de soutenir la cause.  Nous exploitons très rarement cette permission,
et cette fois ci pourrait être la dernière.  Si ça vous déplait,
veuillez quand-même vous patienter.  La pluspart de signataires est
content d'être notifié en cas d'actions importantes de temps en temps,
et nous ne pouvons pas reprogrammer nôtre système pour l'adapter à des
besoins très différenciés a ce point.

#uoe: Si il est très important pour vous de ne plus jamais recevoir aucun
message de nous, veuillez écrire a %(MT) et nous vous effacerons de la
liste des signataires -- notez que cela n'est pas facile car nous
n'avons pas de manière automatisée pour le réaliser.

#lWr: Si,au contraire, vous voulez être tenu au courant régulièrement et
décider vous même de la quantité de courriels que vous désirez
recevoir, nous vous recommandons de vous inscrire via le nouveau
système de participation %(URL)

#aeW: Si vous y êtes déja enregistré, vous risquer de reçevoir notre mail en
double cette fois ci. Nous vous remercions de votre patience et de
votre compréhension.  Comme différentes bases de données sont
utilisées, éliminer les doublons n'est pas un problême facile et
surement pas un de ceux que l'on peut règler dans l'urgence.

#hea: Le Parlement Européen vote en septembre une proposition qui, alors
qu'elle prétend avoir pour but la restriction du brevetage, en réalité
%(am:assure que des algorithmes et des méthodes d'affaires comme la
Commande En Un Click d'Amazon %(tp|deviennent sans doute des
inventions brevetables|et sans doute %(q:fassent une contribution
technique dans leur activité inventive))).

#sit: Comme l'expérience passée le montre, il est possible pour de puissants
lobbies de passer des directives incroyablement mauvaises via le
Parlement Européen. Mais il est aussi possible de les voir rejeter.
Tout dépends de comment nous allons utiliser la dernière semaine
d'Août.

#dru: Requis : Vous à Bruxelles, Mercredi le 27 Août.

#xWg: Requis : Votre site web bloqué Mercredi le 27 Août

#eWa: Requis : 8000 euros pour la manif et les dépenses de lobbying

#WcW2: Requis : 3000 euros pour faire du lobbying auprès des médias

#tEc: Requis : Des publicités dans le journal European Voice et d'autres

#oon: Comment faire une donation

#drs: Requis: Vôtre Reponse MAINTENANT

#crf: Nous avons besoin de votre présence à Bruxelles en Août. Nous
organiserons une présentation sur La Place de Luxembourg et une petite
conférence au Parlement.

#aoW: Nous vous encourageons à participer à cet évènement et à rejoindre
notre équipe d'activistes au parlement avant et après cette date. Vous
pouvez aussi contribuer en arrangeant un rendez-vous entre votre
Député Européen et notre équipe.

#aat: Des lits seront disponibles gratuitement pour tous ceux qui
participeront.  Pour les activistes nous payons aussi les frais de
voyages et autres.

#btt: Si la proposition JURI passait, d'importants sites web seraient forcés
d'être fermés dans un proche futur. Il serait préférable de les
retirer d'internet maintenant, comme exemple, pendant quelques heurs
ou jours. Ou, d'une manière plus gentille, de cacher le contenu
derrière un écran %(q:Page Fermée) d'une manière semblable à ce qui
suit :

#WWW: Beaucoup de pages sont déjà cachées en ce sens, et quelques unes
continueront de l'être jusqu'à la fin du vote.

#dWr: Vous devriez considérer le fait de cacher quelques ou toutes les pages
au moins le 27 Août, et persuader vos amis, spécialement ceux qui
administrent les pages de téléchargement de logiciels les plus
visitées, de faire la même chose.

#lda: En juin et en juillet, nous avons soutenu le travail d'une poignée
d'activistes qui ont fait circuler l'information à l'intérieur du
parlement européen. Nous ferons plus que cela à compter du 25 Août.
Nous payons pour chaque activiste, le lit et le petit-déjeuner plus
environ 50 euros par jour.

#als: Afin d'atteindre chacun des 626 Députés Européens durant la dernière
semaine d'Août, nous avons besoin que au moin 10 personnes de
différentes nationalités (il nous manquent des Français!) passent
cette semaine à Bruxelles.

#WfW: Vous pouvez soutenir le travail de nos activistes en joignant votre
Député Européen par vous-même. Nous avons arrangé une série de bureau
d'information à %(LST) où vous pouvez obtenir des suggestions comme à
qui parler et que dire. Ceux-ci aussi ont besoin d'être logés et
payés.

#sie: Le résultat de nos efforts concertés devraient permettre que le moins
d'amendements de McCarthy et le plus des %(q:nôtres) obtiennent la
majorité.  Ceci permettrait au moins de renvoyer le rapport
préliminaire au comité du JURI et d'ouvrir une nouvelle ronde de
négociation.

#Wcd: Si vous voulez faire une donation pour ce projet, merci d'utiliser le
mot clé %(c:europarl). Si vous voulez restreindre l'utilisation de la
donation à un pays en particulier, spécifiez %(c:europarl-uk) ou
quelque chose d'analogue.

#rWn: Arlène McCarthy et ses amis concentrent de gros efforts pour faire de
la désinformation dans la presse. Des journalistes influents ont aidé,
parfois malgré leur bonne connaissance, à créer l'impression que
McCarthy essaye de restreindre la brevetabilité. Consultez les
documentations à

#eot: Mais nous avons aussi activement contacté les médias et produit une
série de parutions de presse. C'est en grande partie grâce à cette
activité qu'une majorité de médias, dont des acteurs principaux comme
Der Spiegel, Le Monde, Libération, The Guardian, Süddeutsche Zeitung
et d'autres, ont traité la proposition de McCarthy comme ce qu'elle
est : une tentative d'introduire en Europe la brevetabilité illimitée
sur le modèle américain. Beaucoup a pu être fait car nous avons
activement établi des contacts avec les journalistes potentiellement
intéressés et gagné leur confiance comme étant une source sûre
d'information.

#rWv: Mais nous n'avons pas beaucoup pu pénétré les programmes radio ni
télévisés. Ceci aussi pourrait changer grâce à un effort concerté et
rémunéré.

#hbW: Comme pour le travail Europarl, les activistes clés pourraient
pourraient non pas seulement prendre directement contact eux-mêmes
mais pourraient aussi fournir un encadrement pour qui voudrait aider.
De nouveau, nous fournissons une série d'adresses d'interlocuteurs
%(LST) pour cela.

#hWk: S'il vous plaît, considérez de fournir un tel service d'interlocuteur
ou de faire un don pour cela, ou de vous porter volontaire pour
démarcher quelque média vous mêmes.

#drc: Le mot clé pour les donations dans ce lieu est, comme vous vous y
attendiez : %(c:media), %(c:media-fr), %(c:media-be), etc.

#iur: Certains députés européens nous ont conseillé de mettre une annonce
dans un journal lu par les députés européens, comme le %(e:European
Voice), dans la semaine avant le vote. Ceci coûterait dans les
environs de 10000 euros. Il semblerait que cela ait été efficace par
le passé.

#yec: Ceci semble être un peu trop gros en ce moment. Nous planifierons des
actions à ce niveau seulement quand les autres items auront été
financés.

#aoW2: Le compte en banque pour faire une donation est %(BA)

#onr: Pays

#aWd: Code bancaire

#aoa: Nom de la banque

#ktr: Adresse postale de la banque

#con: Compte

#raW: Numéro international du compte bancaire

#Ami: Nous avons été prévenus qu'en utilisant le numéro international de
compte bancaire (IBAN), l'argent pouvait maintenant être transféré
entre des pays européens pour des charges nationales, merci à la
récente directive européenne sur l'harmonisation des marchés internes
qui a été renforcée contre les banques.

#cnw: Propriétaire du compte

#usd: Adresse postale du propriétaire du compte

#ewr: Mot clé

#adW: Vous pouvez aussi donner via %(PW) à %(PM).

#WnW: FFII est reconnu comme une association d'intérêt publique et nous
pouvons vous envoyer un reçu qui, au moins en Allemagne, rends la
donation déductible des impôts. Nous expédions des reçus pour des
donations de 50 euros et plus tous les 3 mois.

#awk: Les informations de bases sur comment aider sont maintenues à

#sbi: Merci de répondre à cette lettre en cochant les boites suivantes

#diW: Je prefererais de ne plus recevoirs des mels de vôtre part.

#let: Je serais à la manif le 27 Août à 12.00

#iru: J'aimerais contribuer à préparer la manif du 27 Août à 12.00

#bcg: Je serais à la conférence au Parlement Européen le 27 Août à 14.00

#inA: Je serais prêt à parler (3-20 minutes) à un des évenements du 27 Août.

#Woe: Je veux participer à l'équipe de lobbying pour au moins une
demi-journée

#oWt: Je veux participer à l'équipe de lobbying pour au moins une journée

#oWt2: Je veux participer à l'équipe de lobbying pour au moins deux jours

#WeA: J'ai besoin d'un endroit pour dormir à Bruxelles la nuit du 26-27 Août

#WeA2: J'ai besoin d'un endroit pour dormir à Bruxelles la nuit du 25-26 Août

#WeA3: J'ai besoin d'un endroit pour dormir à Bruxelles la nuit du 27-28 Août

#tWW: Je vais contacter un (ou plusieurs) député européen et arranger un
rendez-vous avec l'équipe de lobbying

#ito: J'ai suivis les liens listés à %(URL) et suis devenu familier avec les
différentes propositions qui sont en discussion au Parlement Européen

#lei: Je participerai à la manif en ligne.

#isi: J'aimerais aider à porter les issues sur les brevets logiciels dans
les médias.

#Wii: Je voudrais aider avec le déssein d'annonces aux journaux

#lps: Je voudrais aider avec les traductions

#tse: Je voudrais dédier quelques jours ou demi-jours à cette affaire la
prochaine semaine.

#iea: Je ferai une donation.

#Wps: Je représente

#ylo: ma propre personne

#mxx: compagnie xxxxxxx.com

#anx: organisation xxxxxxx.org

#htt: J'aimerai qu'on m'envoi un ou plusieurs %(ts:t-shirts) ou autres
articles liés à la manif.

#Wcs: Merci beaucoup pour votre soutien !

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030817 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

