<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Winning the Plenary Vote

#descr: The ingredients needed for persuading the European Parliament to vote
against the Software Patent Directive and how you can help make it
happen -- a letter to FFII/Eurolinux supporters

#hea: The European Parliament's Legal Affairs Committee seems to have the
support of the two largest parties in favor of a proposal which, while
pretending to aim at restricting patentability, in reality ensures
that algorithms and business methods like Amazon One Click Shopping
indisputably become patentable inventions (which indisputably pass the
bogus requirement of %(q:technical contribution in the inventive
step)).

#sit: As past experience shows, it is quite possible to push extremely
harmful and fraudulent directives through the European Parliament. 
But it is also possible to have them rejected.  Everything depends on
how well we use the summer months, in particular next week and the
last week of August.

#eWa: Needed: 10 experts in Brussels, 10 on the phone, 8000 eur for last
August Week

#WcW2: Needed: 2 media lobbyists per country one week each for 8000 eur

#tai: Presence at Linuxtag and other trade shows, stickers, printed
materials

#lCa: Needed: Full-time Coordination

#oIo: Needed: Development of FFII Participation System

#tEc: Needed: Advertisement in European Voice et al

#pig: Europarl Election Campaign 2004

#wen: Last week the FFII met in Strasburg to elect a new executive
committee.  Alex Macfie of FFII UK became general secretary and vice
president of FFII.  Alex spent the whole week in the European
Parliament, gaining trust of many MEPs.  Some more of us were able to
speak to MEPs on tuesday and wednesday, but basically this week was
rather quiet.

#lda: Next week we will hopefully have 1-2 more people in the Parliament in
Brussels.  Each representative can reach out to 50-100 MEP offices in
one week, contributing to some level of awareness.  Depending on his
knowledge and skills, each representative can also become a trusted
source of expertise for a few MEPs, usually not more than a handful. 
For each of these we need to pay an approximate 5-800 eur per week.

#als: This means that in order to reach out to all 626 MEPs during the last
week of August, we need 10 knowledgable people from different
nationalities to spend that week in Brussels, and 5000 eur to finance
their stay.

#WfW: You can support the work of our lobbyists by calling your MEPs on your
own.  We have arranged a series of helpdesks at %(LST) where you can
receive hints as to whom to talk to and what to say.  Olivier Albiez
and others have already done this for us for the last two weeks, and
it has proven to be worth doing.  We would like to step up the effort
and provide regularly attended telephone hotlines.  In order to ensure
that a strong coaching effort is made, we would like to pay at least
one coach per country 300 eur per week.

#sie: The result of our concerted effort should be that fewer of McCarthy's
amendments and more of %(q:ours) achieve a majority.  This would, at
the very least, send the draft report back to the JURI committee and
open another round of negotiations there.

#Wcd: If you want to donate for this project, please use the keyword
%(c:europarl).

#rWn: Arlene McCarthy and friends are concentrating great efforts on
spreading misinformation in the press.  Influential journalists have
helped, sometimes against their better knowledge, to create the
impression that McCarthy is trying to restrict patentability.  See the
documentation at

#eot: We have however also actively contacted media and produced a series of
press releases.  It is largely due to this activity that a majority of
media, including some mainstream media such as Der Spiegel, Le Monde,
Libération, The Guardian, Süddeutsche Zeitung and others treated
McCarthy's proposal as what it is: an attempt to introduce US-style
unlimited patentability in Europe.  Much of this happened because we
actively established contacts with the potentially interested
journalists and gained their confidence as a reliable source of
information.

#rWv: We have however not yet succeded in finding our way into many radio
and television programs.  I believe that this is mainly due to a lack
of active hands.  It could be changed if we had helpers who are
committed to making the phone-calls and handing the contacts over to
us.  These helpers should again be paid 300 eur per week.

#hbW: As with the Europarl work, the key helpers should not only directly
contact media themselves but also provide coaching for volunteers who
want to help.  Again we are providing a series of helpdesk addresses
%(LST) for this and would like these to be reachable by phone as well.
 Each helpdesk operator should document his activities on an
access-restricted Wiki which we have installed for this purpose.

#hWk: Please consider either operating such a helpdesk or donating for it or
volunteering to support the work by taking on some media yourself.

#drc: The keyword for donations in this area is, as you might expect:
%(c:media).

#aBn: Peter Gerwinski is organising an software patent event at Linuxtag,
based on the cabaret show which he organised on May 8th in Brussels. 
You can support this work by donations with the keyword %(c:linuxtag).
 You can contact Peter and his workforce at pr at ffii org.

#oln: Hartmut Pilch has received 2000 eur from the Free Software Foundation
as a compensation for full-time work on coordinating the lobbying
effort in June, producing press releases, amendment proposals etc.  He
would like to be able to continue to do so throughout July and August
and maybe longer.  You can make it happen by donating with the keyword
%(c:koord).

#Wcn: This participation system aktiv.ffii.org needs further development, so
as to provide a better infrastructure for coordinating the lobbying
activities and allocating the tasks.  Being able to pay developers
1000 eur per month for this would make things move ahead more quickly
and steadily.  The donation keyword is %(c:aktiv).

#iur: Marco Cappato MEP and others have advised us to insert an
advertisement into a newspaper which is read by MEPs, such as the
%(e:European Voice), in the week before the vote.  This would cost
something like 15,000 eur.  It seems to have been effective in the
past.  If we receive donations with the keyword %(q:advert), we will
begin to design an ad and give you more details.  Any donations that
are not used for the promised purpose will be paid back.

#hld: Next year the members of the European Parliament are facing elections.
 Currently local elections are being heald in various places,
including Bavaria on September 21st, home of Joachim Wuermeling, the
shadow rapporteur of the conservatives (EPP), who has, together with
Arlene McCarthy, been pushing for unlimited patentability and refusing
to take any critical opinions into account.  If we could manage to
turn this into an election campaign issue in Bavaria this summer, we
could arouse attention of both the media and MEPs.  The cabaret
performance which we staged in Brussels could, alongside with posters,
advertisement and participation, be used for this.   We have yet to
find out a cost-effective way of doing it.  The donation keyword is
%(c:elekt).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030706 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

