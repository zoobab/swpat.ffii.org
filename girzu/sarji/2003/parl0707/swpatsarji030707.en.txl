<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Winning the Plenary Vote

#descr: The ingredients needed for persuading the European Parliament to vote against the Software Patent Directive and how you can help make it happen -- a letter to FFII/Eurolinux supporters

#ODI: CONFIDENTIAL

#Fuo: Dear FFII/Eurolinux %(su:supporter),

#Wee: You have registered via

#nWt: as a supporter of FFII/Eurolinux and thereby consented to receiving occasional mails, whose scope is strictly limited to enabling you to help support the cause.  If you disagree, you can downgrade your status from %(q:supporter) to %(q:user of the ffii information offers) or lower via the menu at the above URL, and you will no longer receive our mails.

#hea: The European Parliament's Legal Affairs Committee seems to have the support of the two largest parties in favor of a proposal which, while pretending to aim at restricting patentability, in reality ensures that algorithms and business methods like Amazon One Click Shopping indisputably become patentable inventions (and indisputably pass the bogus requirement of %(q:technical contribution in the inventive step)).

#sit: As past experience shows, it is possible for powerful lobbies to push incredibly bad directives through the European Parliament.  But it is also possible to have them rejected.  Everything depends on how well we use the summer months, in particular next week and the last week of August.

#eWa: Needed: 8,000 eur for lobbyists in Brussels and Strasburg

#WcW2: Needed: 4000 eur for lobbying the media

#tEc: Needed: Advertisement in European Voice et al

#lda: During the last few weeks, we have paid lobbyists to spread the word within the European Parliament.  This week we have 5 representatives talking to MEPs in Brussels.  Each representative can reach out to 50-100 MEP offices in one week, contributing to some level of awareness.  Depending on his knowledge and skills, each representative can also become a trusted source of expertise for a few MEPs.  For each representativ we pay the trip, a bed and breakfast in a cheap hotel, and a 50 eur allowance per day.  This week will cost us about 3000 eur.  It is the last session week.  We then have only one more week starting from 2003/08/25.

#als: In order to reach out to all 626 MEPs during the last week of August, we need 10 knowledgable people from different nationalities to spend that week in Brussels, and 5000 eur to finance their stay.

#WfW: You can support the work of our lobbyists by calling your MEPs on your own.  We have arranged a series of helpdesks at %(LST) where you can receive hints as to whom to talk to and what to say.  Olivier Albiez and others have already done this for us for the last two weeks, and it has proven to be worth doing.  We would like to step up the effort and provide regularly attended telephone hotlines.  In order to ensure that a strong coaching effort is made, we would also like to compensate our coaches with something like 50 eur per day, which should include the telephone fees.

#sie: The result of our concerted effort should be that fewer of McCarthy's amendments and more of %(q:ours) achieve a majority.  This would, at the very least, send the draft report back to the JURI committee and open another round of negotiations there.

#Wcd: If you want to donate for this project, please use the keyword %(c:europarl).  If you want to restrict the use of the donation to a particular country, specify %(c:europarl-uk) or analogous.

#rWn: Arlene McCarthy and friends are concentrating great efforts on spreading misinformation in the press.  Influential journalists have helped, sometimes against their better knowledge, to create the impression that McCarthy is trying to restrict patentability.  See the documentation at

#eot: We have however also actively contacted media and produced a series of press releases.  It is largely due to this activity that a majority of media, including some mainstream media such as Der Spiegel, Le Monde, Libération, The Guardian, Süddeutsche Zeitung and others treated McCarthy's proposal as what it is: an attempt to introduce US-style unlimited patentability in Europe.  Much of this happened because we actively established contacts with the potentially interested journalists and gained their confidence as a reliable source of information.

#rWv: We have however not yet succeded in finding our way into many radio and television programs.  I believe that this is mainly due to a lack of active hands.  It could be changed if we had helpers who are committed to making the phone-calls and handing the contacts over to us.  These helpers should again be paid if possible.

#hbW: As with the Europarl work, the key helpers should not only directly contact media themselves but also provide coaching for volunteers who want to help.  Again we are providing a series of helpdesk addresses %(LST) for this and would like these to be reachable by phone as well.  Each helpdesk operator should document his activities on an access-restricted Wiki which we have installed for this purpose.

#hWk: Please consider either operating such a helpdesk or donating for it or volunteering to support the work by taking on some media yourself.

#drc: The keyword for donations in this area is, as you might expect: %(c:media), %(c:media-fr), %(c:media-nl), etc.

#iur: Marco Cappato MEP and others have advised us to insert an advertisement into a newspaper which is read by MEPs, such as the %(e:European Voice), in the week before the vote.  This would cost something like 15,000 eur.  It seems to have been effective in the past.

#yec: We tend to think donations for this are only useful after the other ingredients for our victory have already been financed.  Once we have the means, we will get back to you with a proposal for stepping up the gear to this kind of lobbying, which currently still seems to be one level beyond our means.

#awk: Basic information on how to help is being maintained at

#Wcs: Thank you very much for your support!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsarji030707 ;
# txtlang: en ;
# multlin: t ;
# End: ;

