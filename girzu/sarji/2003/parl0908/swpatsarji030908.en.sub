\begin{subdocument}{swpatsarji030908}{Help us Win the Plenary Vote!}{http://swpat.ffii.org/girzu/sarji/2003/parl0908/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{The ingredients needed for persuading the European Parliament to vote against the Software Patent Directive and how you can help make it happen -- a letter to FFII/Eurolinux supporters}
CONFIDENTIAL

Dear FFII/Eurolinux supporter\footnote{You have registered via

\begin{quote}
\url{http://petition.eurolinux.org/}
\end{quote}

comme signataire et avez explicitement consenti \`{a} recevoir des messages occasionnels, dont le champs est strictement limit\'{e} \`{a} vous permettre de supporter la cause.

If it is important to you that you never receive any mail from us again, please write to noepatents-remove-signature@ffii.org and we will do our best to remove you from the petition -- note that this is not easy because we lack a secure way to do it automatically.

If, on the contrary, you would like to hear more from us and be in charge of how much you hear (in spam-politically correct manner), we recommend you to register at the new participation system \url{http://petition.ffii.org/eubsa/en/}

If you are registered there already, you may be receiving our mail twice this time.  Again we beg for your patience and understanding.  Since different databases are used, eliminating the doubles is not a trivial problem and not one which we can afford to solve under the current time pressure.},

The European Parliament is scheduled to decide on September 22nd on a slightly mitigated software patent directive proposal, with changes resulting from a compromise within the socialist group (PSE).  The delay and the improvement are in part due to our recent activities.  Also, thanks to these activities, a set of amendments which would really fix the directive has been tabled by various smaller parties from the left and right side of the parliament, and quite a few MEPs from the large groups have expressed support for these amendments.  However this may not be enough to secure a majority for the needed amendments.  The PSE compromise is full of traps, the Liberals are ambiguous, and the largest group, the Conservatives (PPE), is are still championning extremist pro-patent policies, which have however been packaged in a moderate clothing.  Some national governments are also instructing\footnote{\url{}} their MEPs to back this kind of patent extremism.

Thanks to your massive support, we have filled our war chest and gained support for our cause both in the parliament and in the media.   To really win the vote in the European Parliament, we must now continue to step up our efforts.

\begin{sect}{natemo}{Needed: You in on the Street near your home around thursday 18th September or in Strasburg on monday 22nd September}
The demonstration in Brussels was a great success.  It has given us the confidence to do something similar in national or regional capitals.   During the week before the vote, the members of the European Parliament are at party gatherings in Madrid, Bologna and elsewhere.  We can respond to this by staging smaller events in these and some other places, such as Berlin, Munich, Nuremberg (Wuermeling), Vienna, Paris, London, Manchester (McCarthy), Barcelona, Lisbon and others, wherever someone is wiling to organise a small event.  Squares in inner cities, patent offices, responsible national ministries and patent court buildings are good places for it.  It would be enough to register a street event, borrow info booth equipment from an allied organisation (e.g. Attac, Greenpeace or a political party), obtain stickers, leaflets, posters, reimbursements and perhaps even travelling helpers from FFII/Eurolinux.  Although individual events needn't be large, we will try to mobilise all our local supporters for them, and another Online Demo will also help draw attention.   This should be enough to attract many media which we didn't reach last time.

Also, the national campaign organisers should write to national governments and ask for a meeting with the people who are responsible for the very bad ``compromise paper'' taken by the European Council's patent policy working party last november or their superiors.

All this can only happen, if people volunteer to do it for their nation or region.  If you are willing to organise a local event in your country, please subscribe at the mailing list for your country which we have installed for this purpose:

\url{http://lists.ffii.org/mailman/listfino/swpatdemo-xx}

where xx is the ISO abbreviation for your country, e.g. fr, uk, it, pt etc.
\end{sect}

\begin{sect}{onldemo}{Needed: Your Website Blocked on Wednesday September 17th}
If the JURI proposal passes, important websites will be forced off the Net in the near future.  It would seem preferable to take them off the Net now, as a demonstration, for a few days.  Or, in a more gentle manner, to hide the content behind a ``Page Closed'' screen of the following kind:

see WebDemo Against Software Patents\footnote{\url{http://localhost/swpat/girzu/jarco/index.en.html}}

Many pages are already hidden in this way and some will continue to stay hidden until after the vote.

You might consider hiding some or all pages at least on August 27th and persuading your friends, especially those who administer much-visited software download pages, to do likewise.
\end{sect}

\begin{sect}{europarl}{Needed: 8,000 eur for demo and lobbying expenses}
In June and July, we have subsidised the work of a handful of activists who spread the word within the European Parliament.  We will do more of this again starting on August 25th.  For each activist we pay the trip, a bed and breakfast plus around 50 eur per day.

In order to reach out to all 626 MEPs during the last week of August, we need 10 knowledgable people from different nationalities to spend that week in Brussels.

You can support the work of our lobbyists by calling your MEPs on your own.  We have arranged a series of helpdesks at \begin{center}
europarl@ffii.org\\
europarl-uk@ffii.org\\
europarl-de@ffii.org\\
europarl-fr@ffii.org\\
europarl-es@ffii.org\\
europarl-pt@ffii.org\\
europarl-it@ffii.org\\
europarl-gr@ffii.org\\
europarl-dk@ffii.org\\
europarl-se@ffii.org\\
europarl-fi@ffii.org\\
europarl-ie@ffii.org
\end{center} where you can receive hints as to whom to talk to and what to say.  These again need to be staffed and paid.

The result of our concerted effort should be that fewer of McCarthy's amendments and more of ``ours'' achieve a majority.  This would send the draft report back to the JURI committee and open another round of negotiations there.

If you want to donate for this project, please use the keyword {\it europarl}.  If you want to restrict the use of the donation to a particular country, specify {\it europarl-uk} or analogous.
\end{sect}

\begin{sect}{media}{Needed: 3000 eur for lobbying the media}
Arlene McCarthy and friends are concentrating great efforts on spreading misinformation in the press.  Influential journalists have helped, sometimes against their better knowledge, to create the impression that McCarthy is trying to restrict patentability.  See the documentation at

\begin{quote}
JURI votes for Fake Limits on Patentability\footnote{\url{http://localhost/swpat/lisri/03/juri0617/index.en.html}}\\
News Media and Software Patents\footnote{\url{http://localhost/swpat/gasnu/media/index.en.html}}
\end{quote}

We have however also actively contacted media and produced a series of press releases.  It is largely due to this activity that a majority of media, including some mainstream media such as Der Spiegel, Le Monde, Lib\'{e}ration, The Guardian, S\"{u}ddeutsche Zeitung and others treated McCarthy's proposal as what it is: an attempt to introduce US-style unlimited patentability in Europe.  Much of this happened because we actively established contacts with the potentially interested journalists and gained their confidence as a reliable source of information.

We have however not yet succeded in finding our way into many radio and television programs.  This again could be changed with a concerted and paid effort.

As with the Europarl work, the key helpers should not only directly contact media themselves but also provide coaching for volunteers who want to help.  Again we are providing a series of helpdesk addresses \begin{center}
media@ffii.org\\
media-uk@ffii.org\\
media-de@ffii.org\\
media-fr@ffii.org\\
media-es@ffii.org\\
media-pt@ffii.org\\
media-it@ffii.org\\
media-gr@ffii.org\\
media-dk@ffii.org\\
media-se@ffii.org\\
media-fi@ffii.org\\
media-ie@ffii.org
\end{center} for this and would like these to be reachable by phone as well.

Please consider either staffing such a helpdesk or donating for it or volunteering to support the work by taking on some media yourself.

The keyword for donations in this area is, as you might expect: {\it media}, {\it media-fr}, {\it media-nl}, etc.
\end{sect}

\begin{sect}{advert}{Needed: Advertisement in European Voice et al}
Some MEPs have advised us to insert an advertisement into a newspaper which is read by MEPs, such as \emph{European Voice}, in the week before the vote.  This would cost around 10000 eur.  It seems to have been effective in the past.

This however seems a little too big at the moment.  Only when the other items have been financed will we plan actions at this level.
\end{sect}

\begin{sect}{donac}{How to donate}
The bank account for a donation is \begin{quote}
\begin{description}
\item[Country:]\ DE (Germany)
\item[Bank Code:]\ 70150000
\item[BIC (bank identifier code):]\ SSKMDEMM
\item[SWIFT:]\ SSKM DE MM
\item[Name of Bank:]\ Stadtsparkasse Muenchen
\item[Bank's Postal Address:]\ DE 80335 Muenchen\\
Lotstr 1
\item[Account:]\ 31112097
\item[IBAN (international bank account number)\footnote{We are told that, using the IBAN, money can now be transferred between european countries for the domestic fee, thanks to a recent EU internal market harmonisation directive which can be enforced against banks.}:]\ DE78701500000031112097
\item[Account Owner:]\ FFII e.V.
\item[Account Owner's Postal Address:]\ DE 80636 Muenchen\\
Blutenburgstr 17
\item[Keyword:]\ europarl
\end{description}
\end{quote}

Alternatively you can donate via paypal.com\footnote{\url{http://www.paypal.com}} to paypal at ffii org.

FFII is acknowledged as a public-interest association and we can send you a receipt that at least in Germany makes the donation tax-deductible.  We receipts for all donations of 50 eur or more every 3 months.

Basic information on how to help is being maintained at

\begin{quote}
\url{http://localhost/eurolinux/index.en.html}
\end{quote}
\end{sect}

\begin{sect}{respond}{Needed: Your Response NOW}
Please respond to this letter by ticking the following checkboxes

\begin{center}
\begin{tabular}{C{44}C{44}}
[ ] & I would prefer to receive no more mail from you.\\
[ ] & I will be at the demo on Aug 27th 12.00\\
[ ] & I would like to contribute to preparing the demo of Aug 27th 12.00\\
[ ] & I will be at the conference in the EP on Aug 27th 14.00\\
[ ] & I would like to speak at one of the events on Aug 27th\\
[ ] & I want to participate in the lobbying team for at least 1/2 day\\
[ ] & I want to participate in the lobbying team for at least 1 day\\
[ ] & I want to participate in the lobbying team for at least 2 days\\
[ ] & I need a place to sleep in BXL on Aug 26-27\\
[ ] & I need a place to sleep in BXL on Aug 25-26\\
[ ] & I need a place to sleep in BXL on Aug 27-28\\
[ ] & I will contact MEP(s) and arrange for a meeting with the lobbying team\\
[ ] & I have followed the links listed at \url{http://localhost/swpat/penmi/2003/europarl/08/index.en.html} and made myself familiar with the various proposals which are under discussion in the European Parliament\\
[ ] & I will participate in the online demo.\\
[ ] & I would like to help bring the software patent issue into the media.\\
[ ] & I'd like to help with graphic design of advertisements etc.\\
[ ] & I'd like to help with translating.\\
[ ] & I'd like to dedicate a few days or half-days next week to help.\\
[ ] & I will make a donation.\\
[ ] & I represent
\begin{center}
\begin{tabular}{C{44}C{44}}
[ ] & myself only\\
[ ] & company xxxxxxx.com\\
[ ] & organisation xxxxxxx.org\\
\end{tabular}
\end{center}\\
[ ] & I'd like to have one or more t-shirts (``protect innovation from software patents -- protegez l'innovation contre les brevets logiciels'') or other demo-related articles sent to me.\\
\end{tabular}
\end{center}
\end{sect}

Thank you very much for your support!
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

