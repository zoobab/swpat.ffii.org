<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: WWW Multilingue - Comment vous pouvez composer avec nous

#descr: Pour ajouter une version dans votre langue d'une page de ce site, vous ne devez pas vous préoccuper de HTML ni d'autres structures de présentation. Vos traductions sont indépendantes de la conception graphique de la page et survivront les futures modifications de cette conception. Vous télécharger un simple fichier texte dans lequel les blocs de texte apparaissent précédés de balises d'identification et comprennent un marquage minimal. Vous remplacez ces blocs, et envoyez le résultat à un processeur de courrier électronique qui insère les modifications dans une base de données et recrée les pages du site.  Plusieurs personnes sont prêtes à vous aider. Quelques principes de base sont présentés ici.

#Wee: Comment est-ce que ca marche?

#HSh: Morceaux de Texte Marqués

#Wih: Quelles sont les erreurs que peut faire le traducteur?

#Dui: L'auteur utilise la commande MLHT %1 pour intégrer votre traduction dans la structure MLHT.  Cette commande insère vos lignes aux endroits où il trouve les lignes correspondantes de la source traduite.  C'est-à-dire votre ligne traduite commençant par l'identifiant XYZ: remplace la ligne dans le texte source qui commence par le même identifiant.

#rno: Les blocs peuvent contenir des sous-blocs structurés, indiqués par des parenthèses précédées par un signe pourcent, voir les détails ci-dessous.

#Ase: Certains morceaux de texte ont une fonction spéciale dans le texte, qui peut s'exprimer par des caractéristiques de présentation particulières, telles qu'impression en gras ou liens hypertextes. Ces parties du texte utilise un marquage selon le schéma suivant: %(EX)  Le texte marqué est délimité par des parenthèses, un symbole pourcent précédant la parenthèse ouvrante, et celle-ci est suivie d'un %(fn:nom de fonction), dont la signification n'est pas importante pour le traducteur, d'un %(fs:séparateur de champs), et éventuellement de plusieurs champs de texte.

#usv: La %%(tp|revendikaĵo|malprave nomita %%(q:invento)) en %%(EP) enhavas ...

#emh: Un champ de texte peut lui-même contenir du texte marqué. Cependant, il ne peut contenir de parenthèses fermantes ne faisant pas partie d'une %(tp|structure de marquage|précédée d'un signe pourcent). Les parenthèses sont réservées au marquage. Pour faire apparaître des parenthèses littérales, on utilise le marquage %(TP).

#efe: Thème %%(pe:Rhème)

#fee: %%(tp|Thème|Rhème)

#ite: Certaines fonctions de formatage sont prédéfinies:

#nam: nom

#ert: description

#xml: exemple

#quo: mise entre guillemets

#mhs: emphase

#iai: italique

#ywt: police courrier

#cit: citation

#srn: gras

#nrn: souligné

#ane: parenthèse

#eph: thème - parenthèse

#eWn: thème - note de bas de page (annotation)

#lin: lignes

#agp: paragraphes

#orl: liste

#rei: liste numérotée

#lWd: ligne de titre + liste

#eid: ligne de titre + liste numérotée

#ooe: La signification réelle d'une fonction dépend en fait du contexte local au document source; les définitions ci-dessus peuvent être redéfinies.

#Wsb: Quand vous voulez savoir quels effets auront les marqueurs hypertexte sur l'apparence finale du document, regardez la source hypertexte du document avec votre navigateur préféré.  Gardez-le à portée de main pour consultation chaque fois que vous traduisez.

#lfa: Les structures imbriquées comme %(AL), peuvent être utilisées pour rallonger indéfiniment les pages web existantes.

#Zlz: séparation de lignes

#apr: Erreurs de marquage

#jgt: Système de Codage de Fichiers

#doW: Remplacé lignes nouvelles par vieilles

#kii: Lignes mises en commentaires

#IrW: importation non réussie due à des variables incorrectes dans le pied de page

#daa: Blokoj devas esti separataj per malplenaj linioj.

#sma: Normale la malplenaj linioj jam estas en la tradukebla fontdatumaro.  Sed en kelkaj malnovaj fontdatumaroj ili mankas. Ĝis antaŭ nelonge ni ne uzis malplenaj liniojn per separado sed anstataŭe skribis la tutan blokon sur nur unu linio.

#tta: En la aŭtomate produktita datumaro unu bloko okupas unu liniojn.  Sed vi ne estas devigita labori laŭ tiu regulo.  Vi povas libere tranĉi vian blokon en multaj linioj.

#dbe: Les parenthèses ouvrante et fermante, délimitant les morceaux de texte marqués, doivent correspondre et le nombre de champs doit être correct. Autrement, la compilation échouera.

#cru: Une paire de parenthèses simples, sans le préfixe pourcent, ne peut pas être utilisée (voir ci-dessus).

#ecW: MLHT lit et écrit les textes en codage de caractères UTF-8.  Beaucoup d'utilisateurs préfèrent utiliser un autre codage de caractères, comme latin-1 ou euc-jp.  Dans ces cas là, vous pouvez utiliser un convertisseur tel que,sous les systèmes GNU/Linux/Unix, %(c:recode):

#WWs: Le fichier ne doit contenir que les lignes qui ont effectivement été modifiées. Les autres doivent être mises en commentaires. Il y a toujours un certain risque qu'un texte déjà amélioré soit remplacé par une version plus ancienne, et ce risque doit être minimisé.

#icW: Vous devez supprimer le caractère '#' précédant chacune des lignes sur lesquelles vous avez travaillé. Sinon celles-ci seront considérées comme des commentaires et ne seront pas insérées dans le texte résultant.

#arW: Vous pouvez envoyer votre travail à un processeur de courrier électronique qui insérera vos textes dans le système et recompilera les pages correspondantes. Dans ce but, il est important que vous complétiez correctement les balises du pied de page, y compris vos nom d'utilisateur et mot de passe, ainsi que la langue cible (txtlang). Si txtlang est incorrect, une page en anglais pourrait être remplacé par du texte en portugais, ou encore il pourrait ne rien se passer du tout, suite à l'étape de validation de données soumises. Remarquez que chacune des lignes de la section des variables au pied de page doit commencer avec un '#' et être terminées par un ';'.   Sinon l'importation échoue.

#WeW: A simple way to send your text under Unix-like systems is:

#Wps: AelWiki: Traduction Swpat

#Wao: Cette page Wiki est utilisée pour répartir les tâches de traduction aux traducteurs.

#Tan: Information sur la Liste de Diffusion de Traduction de FFII

#bnW: Forum de discussions et coordination des tâches de traduction devant être effectuées pour le spages de FFII.

#WoW: Cette page Wiki est utilisée pour organiser les suggestions pour les traducteurs.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: gharfang ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: fr ;
# multlin: t ;
# End: ;

