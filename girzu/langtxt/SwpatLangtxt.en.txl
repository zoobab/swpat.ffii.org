<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Multilingual Web - How You Can Weave With Us

#descr: In order to add a version in your language to a page of this site, you needn't be concerned about HTML or other object formats.  Your translations are independent of the page design and will survive future changes of this design.  You fetch a plain text file, in which the textual building blocks of the page are listed with identifying tags and minimal markup.   You overwrite these blocks and send it to a mail processor which inserts them into a data base and generates the object pages anew.  There are nice people willing to guide you.  Some basic questions are answered here.

#Wee: How does it work?

#HSh: Marked Text Pieces

#Wih: What can the translator do wrong

#Dui: Each text block has an identifier xyz: at the beginning, which allows the system to insert it into the right place in the object document.  The order of the text blocks is irrelevant.

#rno: The blocks may contain some structured sub-blocks, marked by a pair of brackets preceded by a percent sign, see details below.

#Ase: Some text pieces have special functions in the text which can be expressed by special presentation features, such as bold printing or hyperlinks.  Such text parts are marked according to the following scheme: %(EX)  The marked text piece is surrounded with brackets, preceded by a percent symbol.  The opening bracket is followed by a %(fn:function name), whose meaning needn't be of interest to the translator, a %(fs:field separator), and finally 0 or more text fields.

#usv: The %%(tp|claim object|here wrongly termed %%(q:invention)) of %%(EP) comprises ...

#emh: A text field can again contain marked text pieces.  It may however not contain any closing brackets which are not part of a %(tp|markup structure|preceded by percent symbol).  The bracket is reserved for markup purposes.  Parentheses must be expressed as markup by %(TP).

#efe: Thema %%(pe:Rhema)

#fee: %%(tp|Thema|Rhema)

#ite: Certain text functions are predefined:

#nam: name

#ert: description

#xml: example

#quo: quote

#mhs: emphasis

#iai: italic

#ywt: typewriter

#cit: cite

#srn: strong

#nrn: underline

#ane: Parenthese

#eph: theme - parenthesis

#eWn: theme - footnote (annotatio)

#lin: lines

#agp: paragraphs

#orl: unordered list

#rei: ordered list

#lWd: title line + unordered list

#eid: titel line + ordered list

#ooe: What any text function name really means is decided by the local context of the source document, the above definitions can be overridden.

#Wsb: When you want to know what effect the functions have on the final appearance of the document, take a look at the source language's hypertext with your favorite browser.  Keep it ready for consultation whenever you are translating.

#lfa: Nested structures such as %(AL) can be (ab)used for expanding existing web pages infinitely.

#Zlz: line separation

#apr: Markup errors

#jgt: File Coding System

#doW: New Blocks Overwritten

#kii: New Blocks Commented Out

#IrW: Failed Importing due to Wrong Variable Settings at Page Bottom

#daa: There must be an empty line between every two text blocks.

#sma: Normally the empty lines are already present in the translatable source file.  But in some old source files they are missing.  This is because until recently we did not use empty lines for separation but instead required that every block occupy exactly one line.  If you prefer this old behaviour, you can enable it by setting the local variable %(c:multlin) to %(c:nil).

#tta: In the generated source file one block occupies one line, and it should be preceded and followed by an empty line.  You are free to split your block into several lines, but you must preserve (or, in some old files, insert) the isolating empty lines.

#dbe: Brackets in marked text pieces must be closed and the number of fields must be correct.  Otherwise compilation will abort.

#cru: Simple bracket pairs without the percent prefix must not be used, see above.

#ecW: MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, %(c:recode).  You can also let the mlhtimport system do the conversion.  You tell it to do so by specifying something like %(CI) either in the subject line or in the %(LV) section in the footer.

#WWs: The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.

#icW: All lines on which you have worked should have the initial # removed.  Otherwise they will be treated as comments and not inserted into the object text.

#arW: You can send your translation to a mail processor which will insert your texts into the system and recompile concerned object texts.  For that purpose it is important that you correctly fill in the tags at the bottom, including your login and password as well as the correct target language (txtlang).  If lang is wrong, an English page may be overwritten with Portuguese text, or perhaps nothing will be written at all, due to entry validation.  Not that each of the lines in the variables section at the bottom must begin with a '#' and end with a ';', otherwise importation will abort.

#WeW: A simple way to send your text under Unix-like systems is:

#Wps: AelWiki: Swpat Translation

#Wao: This wiki page is used to allocate translation work to translators.

#Tan: FFII Translation Mailing List Information

#bnW: Forum for discussion about and coordination of translation work to be done for the FFII pages.

#WoW: Wiki page is used for working out further hints for translators.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: en ;
# multlin: t ;
# End: ;

