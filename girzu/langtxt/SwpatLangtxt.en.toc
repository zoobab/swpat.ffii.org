\contentsline {section}{\numberline {1}How does it work?}{2}{section.1}
\contentsline {section}{\numberline {2}Marked Text Pieces}{2}{section.2}
\contentsline {section}{\numberline {3}What can the translator do wrong}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}line separation}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Markup errors}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}File Coding System}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}New Blocks Overwritten}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}New Blocks Commented Out}{5}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Failed Importing due to Wrong Variable Settings at Page Bottom}{5}{subsection.3.6}
\contentsline {section}{\numberline {4}Annotated Links}{5}{section.4}
