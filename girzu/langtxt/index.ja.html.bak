<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
<meta name="author" content="仕事組">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2003/10/03">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="title" content="多言語超文をいかにしてさらに多言語化できるか">
<meta name="description" content="多言語文献にもう一つの言語を付けたい時は，超文プログラム組み技能は正に要らない。文献の翻訳手本を取り寄せ，その中の一行一行を目的言語に書き直した結果を超文作者に送り返えせば良いのです。本文ではその実例を挙げ，常問問題をお答えします。">
<title>多言語超文をいかにしてさらに多言語化できるか</title>
<style type="text/css">
<!--
H1,H2,H3,H4,H5,H6 {font-family:verdana,helvetica,arial,sans-serif}
A {font-family:verdana,helvetica,arial,sans-serif}
A:link {text-decoration:none;font-weight:bold}
A:visited{text-decoration:none;font-weight:bold}
A:active {text-decoration:none;font-weight:bold}-->
</style>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><a href="index.eo.html" charset=utf-8><img src="/img/flags/eo.png" alt="[EO Esperanto]" border="1" height=25></a>
<a href="index.de.html" charset=utf-8><img src="/img/flags/de.png" alt="[DE Deutsch]" border="1" height=25></a>
<a href="index.en.html" charset=utf-8><img src="/img/flags/en.png" alt="[EN English]" border="1" height=25></a>
<a href="index.fr.html" charset=utf-8><img src="/img/flags/fr.png" alt="[FR Francais]" border="1" height=25></a>
<a href="index.zh.html" charset=utf-8><img src="/img/flags/zh.png" alt="[ZH Zhongwen]" border="1" height=25></a>
<a href="index.in.html" charset=utf-8><img src="/img/flags/in.png" alt="[IN Bahasa Indonesia]" border="1" height=25></a>
<a href="langtxt.ja.txt"><img src="/img/icons.other/txt.png" alt="[翻訳原文]" border="1" height=25></a>
<a href="/girzu/gunka/index.de.html"><img src="/img/icons.other/questionmark.png" alt="[如何協助]" border="1" height=25></a>
<a href="langtxt.ja.pdf"><img src="/img/icons.other/pdficon.png" alt="[印刷用版本]" border="1" height=25></a>
<a href="http://kwiki.ffii.org/LangtxtJa"><img src="/img/icons.other/groupedit.png" alt="[読者備注]" border="0"></a>
<table width="100%">
<tr><th align=center bgcolor="#D0F9DC"><a href="../index.ja.html">論理特許</a></th><td align=center bgcolor="#D0F9DC"><a href="../lisri/index.ja.html">報導</a></td><td align=center bgcolor="#D0F9DC"><a href="../pikta/index.de.html">特許</a></td><td align=center bgcolor="#D0F9DC"><a href="../papri/index.de.html">文献評論</a></td><td align=center bgcolor="#D0F9DC"><a href="../stidi/index.de.html">分析</a></td><td align=center bgcolor="#D0F9DC"><a href="../penmi/index.en.html">会議</a></td><td align=center bgcolor="#D0F9DC"><a href="../gasnu/index.de.html">行動者</a></td></tr>
</table>
<div align="center"><h1><a name="top">多言語超文をいかにしてさらに多言語化できるか</a></h1></div>
<p>
<cite>多言語文献にもう一つの言語を付けたい時は，超文プログラム組み技能は正に要らない。文献の翻訳手本を取り寄せ，その中の一行一行を目的言語に書き直した結果を超文作者に送り返えせば良いのです。本文ではその実例を挙げ，常問問題をお答えします。</cite>
<p>
<ul><li><a href="#cel">超文作者はいかにして私の訳文を使うのか？</a></li>
<li><a href="#err">どこで間違えし易い？</a></li>
<li><a href="#links">さらに読むべき文章</a></li></ul>

<a name="cel"><h2>超文作者はいかにして私の訳文を使うのか？</h2></a>
<p>
超文作者は超文管理指令 <code>mlht-import-langtxt</code> によって貴方の超文を超文構造に読み込み，各行の挿入位置はそれと該当する原文の挿入位置によって決まります。従って，訳文の第55行は原文の第55行の訳であり，兩文行数は一致しなければなりません。
<p>
<a name="err"><h2>どこで間違えし易い？</h2></a>
<p>
<ul><li><a href="#erln">行數</a></li>
<li><a href="#erhs">超文文法記號 <code>%1</code>、<code>%(fun:text)</code>等</a></li>
<li><a href="#utf8">Fajlkodiga Sistemo</a></li>
<li><a href="#trolin">Sendi na tro multaj linioj</a></li>
<li><a href="#jingzf">Elkomentitaj linioj</a></li>
<li><a href="#mlhtimport">Automata importado</a></li></ul>

<a name="erln"><h3>行數</h3></a>
<p>
行を長く書けるようなエヂィタを使った方がいい。
<p>
<a name="erhs"><h3>超文文法記號 <code>%1</code>、<code>%(fun:text)</code>等</h3></a>
<p>
翻訳手本の中にも上記の三種の超文記号が使われます。訳者はその使い方を知る必要はなく，ただ <code>var</code> は変数, <code>fun:</code> はブロック標記関数, 両方も超文システムに属する翻訳無用のものにたいして，<code>text</code> は訳すべき文章であることを認識し，そしてそれらの記号を訳文の中に原文と同じように位置づけ，また原文通りに正しく書き写すことを注意すれば良いのである。
<p>
その超文記号の最終效果を知りたい時にはブラウザーで元言語の超文型態を参考すれば宜しいです。それがいつも参考できるような翻訳仕事場を作っておいて下さい。
<p>
<a name="utf8"><h3>Fajlkodiga Sistemo</h3></a>
<p>
MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, <cite>recode</cite>:
<p>
<blockquote>
<pre>
$ recode utf-8..latin-1 swpat.fr.txt
$ edit swpat.fr.txt
$ recode latin-1..utf-8 swpat.fr.txt
</pre>
</blockquote>
<p>
<a name="trolin"><h3>Sendi na tro multaj linioj</h3></a>
<p>
The file should contain only those lines that were actually improved.  Others should be omitted or deleted.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.
<p>
<a name="jingzf"><h3>Elkomentitaj linioj</h3></a>
<p>
la linioj devas komenci per xxx: kaj ne per #
<p>
<a name="mlhtimport"><h3>Automata importado</h3></a>
<p>
sendu al retposhta adreso kun pravaj markajhoj.
<p>
temlinio
<p>
<a name="links"><h2>さらに読むべき文章</h2></a>
<p>
<table border=1 cellpadding=20>
<tr><th align=left><dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://lists.ffii.org/mailman/listinfo/traduk" target=ffiilists>FFII Translation Mailing List Information</a></b></dt>
<dd>Forum for discussion about and coordination of translation work to be done for the FFII pages.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://wiki.ael.be/index.php/FightingSWPatentsTranslation">AelWiki: Swpat Translation</a></b></dt>
<dd>This wiki page is used to allocate translation work to translators.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://kwiki.ffii.org/LangtxtJa">http://kwiki.ffii.org/LangtxtJa</a></b></dt>
<dd>Wiki page is used for working out further hints for translators.</dd></dl></th></tr>
</table>
<p>
<br>
<br>
<div align="center"><font size=2>[ <a href="../index.ja.html">FFII: 欧州の計算法特許</a> | <a href="../lisri/index.ja.html">計算法特許の新動向</a> | <a href="../pikta/index.de.html">欧州計算法特許の恐怖展示室</a> | <a href="../papri/index.de.html">Computing and Patent Law: Review of Articles</a> | <a href="../stidi/index.de.html">論理特許: 質問，分析，提案</a> | <a href="../penmi/index.en.html">計算法特許と会議</a> | <a href="../gasnu/index.de.html">計算法特許戦争の行動者達</a> ]</font></div>
<form action="http://www.google.com/search" method="get">
<input name=q size=25 type=text value="">
<input name=btnG size=25 type=submit value="ffii.orgグーグル検索">
<input name=q size=25 type=hidden value="site:ffii.org">
</form>
<hr>
<address>
http://langtxt.ffii.org/index.ja.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2003/10/03
<a href="../girzu/index.de.html">仕事組</a>
</address></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/mlht/langtxt.el -->
<!-- mode: html -->
<!-- End: -->

