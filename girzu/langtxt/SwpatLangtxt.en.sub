\begin{subdocument}{SwpatLangtxt}{Multilingual Web - How You Can Weave With Us}{http://swpat.ffii.org/group/langtxt/index.en.html}{Workgroup\\swpatag@ffii.org}{In order to add a version in your language to a page of this site, you needn't be concerned about HTML or other object formats.  Your translations are independent of the page design and will survive future changes of this design.  You fetch a plain text file, in which the textual building blocks of the page are listed with identifying tags and minimal markup.   You overwrite these blocks and send it to a mail processor which inserts them into a data base and generates the object pages anew.  There are nice people willing to guide you.  Some basic questions are answered here.}
\begin{sect}{cel}{How does it work?}
Each text block has an identifier xyz: at the beginning, which allows the system to insert it into the right place in the object document.  The order of the text blocks is irrelevant.

The blocks may contain some structured sub-blocks, marked by a pair of brackets preceded by a percent sign, see details below.
\end{sect}

\begin{sect}{erhs}{Marked Text Pieces}
Some text pieces have special functions in the text which can be expressed by special presentation features, such as bold printing or hyperlinks.  Such text parts are marked according to the following scheme: \begin{verbatim}
The \percent{}(tp|claim object|here wrongly termed \percent{}(q:invention)) of \percent{}(EP) comprises ...
\end{verbatim}  The marked text piece is surrounded with brackets, preceded by a percent symbol.  The opening bracket is followed by a function name (e.g. {\tt tp}, {\tt q} or {\tt EP}), whose meaning needn't be of interest to the translator, a field separator (e.g. {\tt :} or {\tt |}), and finally 0 or more text fields.

A text field can again contain marked text pieces.  It may however not contain any closing brackets which are not part of a markup structure (preceded by percent symbol).  The bracket is reserved for markup purposes.  Parentheses must be expressed as markup by {\tt Thema \percent{}(pe:Rhema)} or {\tt \percent{}(tp|Thema|Rhema)}.

Certain text functions are predefined:

\begin{center}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
name & description & example\\\hline
q & quote & ``abc''\\\hline
e & emphasis & \emph{abc}\\\hline
i & italic & {\it abc}\\\hline
t & typewriter & {\tt abc}\\\hline
c & cite & {\it abc}\\\hline
s & strong & {\bf abc}\\\hline
u & underline & \underline{abc}\\\hline
pe & Parenthese & (abc)\\\hline
tp & theme - parenthesis & abc (def)\\\hline
ta & theme - footnote (annotatio) & abc\footnote{def}\\\hline
nl & lines & abc

def

ghi\\\hline
al & paragraphs & abc

def

ghi\\\hline
ul & unordered list & \begin{itemize}
\item
abc

\item
def

\item
ghi
\end{itemize}\\\hline
ol & ordered list & \begin{enumerate}
\item
abc

\item
def

\item
ghi
\end{enumerate}\\\hline
linul & title line + unordered list & abc
\begin{itemize}
\item
def

\item
ghi

\item
jkl
\end{itemize}\\\hline
linol & titel line + ordered list & abc
\begin{enumerate}
\item
def

\item
ghi

\item
jkl
\end{enumerate}\\\hline
\end{tabular}
\end{center}
\end{center}

What any text function name really means is decided by the local context of the source document, the above definitions can be overridden.

When you want to know what effect the functions have on the final appearance of the document, take a look at the source language's hypertext with your favorite browser.  Keep it ready for consultation whenever you are translating.

Nested structures such as \begin{quote}
\percent{}(al|Article 1|Conditions of Patentability|\percent{}(LARGE:Human genes are unpatentable) ...|Article 7.b|...\percent{}(small:unless they are replicable outside the human body))
\end{quote} can be (ab)used for expanding existing web pages infinitely.
\end{sect}

\begin{sect}{err}{What can the translator do wrong}
\begin{sect}{erln}{line separation}
There must be an empty line between every two text blocks.

{\small Normally the empty lines are already present in the translatable source file.  But in some old source files they are missing.  This is because until recently we did not use empty lines for separation but instead required that every block occupy exactly one line.  If you prefer this old behaviour, you can enable it by setting the local variable {\it multlin} to {\it nil}.}

In the generated source file one block occupies one line, and it should be preceded and followed by an empty line.  You are free to split your block into several lines, but you must preserve (or, in some old files, insert) the isolating empty lines.
\end{sect}

\begin{sect}{markerr}{Markup errors}
Brackets in marked text pieces must be closed and the number of fields must be correct.  Otherwise compilation will abort.

Simple bracket pairs without the percent prefix must not be used, see above.
\end{sect}

\begin{sect}{utf8}{File Coding System}
MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, {\it recode}:

\begin{verbatim}
$ recode utf-8..latin-1 swpat.fr.txt
$ edit swpat.fr.txt
$ recode latin-1..utf-8 swpat.fr.txt
\end{verbatim}
\end{sect}

\begin{sect}{trolin}{New Blocks Overwritten}
The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '\#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.
\end{sect}

\begin{sect}{jingzf}{New Blocks Commented Out}
All lines on which you have worked should have the initial \# removed.  Otherwise they will be treated as comments and not inserted into the object text.
\end{sect}

\begin{sect}{mlhtimport}{Failed Importing due to Wrong Variable Settings at Page Bottom}
You can send to a mail processor which will insert your texts into the system and recompile concerned object texts.  For that purpose it is important that you correctly fill in the tags at the bottom, including your login and password as well as the correct target language (txtlang).  If lang is wrong, an English page may be overwritten with Portuguese text, or perhaps nothing will be written at all, due to entry validation.  Not that each of the lines in the variables section at the bottom must begin with a '\#' and end with a ';', otherwise importation will abort.

The subject line of the mail should contain the words {\tt coding: utf-8}.  It is also possible to specify another coding system, such as iso-8859-1.  In that case, conversion to utf-8 will be done on the server.  The {\tt \# coding: utf-8 ;} variable setting that is found at the bottom of the text should never be edited.

A simple way to send your text under Unix-like systems is:

\begin{verbatim}
\$ cat mytext.txt | mailx -s ``coding: iso-8859-1`` mlhtimport@ffii.com
\end{verbatim}
\end{sect}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf FFII Translation Mailing List Information\footnote{http://lists.ffii.org/mailman/listinfo/traduk/TradukListinfo.en.html}}}

\begin{quote}
Forum for discussion about and coordination of translation work to be done for the FFII pages.
\end{quote}
\filbreak

\item
{\bf {\bf AelWiki: Swpat Translation\footnote{http://wiki.ael.be/index.php/FightingSWPatentsTranslation}}}

\begin{quote}
This wiki page is used to allocate translation work to translators.
\end{quote}
\filbreak

\item
{\bf {\bf http://kwiki.ffii.org/SwpatLangtxtEn}}

\begin{quote}
Wiki page is used for working out further hints for translators.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgirzu.el ;
% mode: latex ;
% End: ;

