<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Väv med oss - hur du gör flerspråkiga sidor.

#descr: För att lägga till en sida på ditt eget språk till FFIIs sidor behöver du inte bry dig om HTML eller andra format. Dina översättningar är oberoende av sidornas utseende och kommer att överleva framtida designförändringar. Du hämtar en enkel textfil uppdelad i markerade stycken, skriver över textblocken med din översättning och skickar till en mailprocessor som i sin tur genererar nya översatta webbsidor. Behöver du hjälp finnas det vänliga människor som kan ge tips och råd, några enkla frågor besvaras nedan.

#Wee: Hur fungerar det?

#HSh: Markerade textavsnitt

#Wih: Vilka fel kan en översättare göra?

#Dui: Varje textblock börjar med en identifierare, till exempel xyz:, som gör att systemet kan stoppa in avsnittet på rätt ställe i den genererade webbsidan. Ordningsföljden på textblocken spelar ingen roll.

#rno: Textblocken kan innehålla strukturerade underavsnitt markerade med parenteser som inleds med ett procenttecken, se nedan.

#Ase: En del textavsnitt innehåller markörer som ger t ex fetskrift eller länkar. Dessa markeras enligt följande schema: %(EX) Den markerade textdelen omges med parenteser med ett inledande procenttecken. Det första parentestecknet följs av ett funktionsnamn %(fn:function name), som översättaren inte behöver förstå betydelsen av, skiljetecken %(fs:field separator) och slutligen noll eller fler textavsnitt.

#usv: La %%(tp|revendikaĵo|malprave nomita %%(q:invento)) en %%(EP) enhavas ...

#emh: Ett textfält kan också innehålla markerade avsnitt. Fältet får däremot inte innehålla parenteser som inte är en del av en %(tp|markerad struktur|som föregås av ett procenttecken).  Parentesen är reserverad för markering. Parenteser måste skrivas med %(TP).

#efe: Thema %%(pe:Rhema)

#fee: %%(tp|Thema|Rhema)

#ite: Vissa textfunktioner är fördefinierade:

#nam: name

#ert: description

#xml: example

#quo: quote

#mhs: emphasis

#iai: italic

#ywt: typewriter

#cit: cite

#srn: strong

#nrn: underline

#ane: parenthesis

#eph: theme - parenthesis

#eWn: theme - footnote (annotatio)

#lin: lines

#agp: paragraphs

#orl: unordered list

#rei: ordered list

#lWd: title line + unordered list

#eid: titel line + ordered list

#ooe: Vad markörerna egentligen betyder bestäms av dokumentets lokala kontext. Definitionerna ovan kan skrivas över.

#Wsb: Titta på originaldokumentet i en webläsare när du vill veta vilken effekt markörerna har. Det underlättar översättningen.

#lfa: Nested structures such as %(AL) can be (ab)used for expanding existing web pages infinitely.

#Zlz: liniseparado

#apr: Markup errors

#jgt: Fajlkodiga Sistemo

#doW: sendi na tro multaj linioj

#kii: elkomentitaj linioj

#IrW: automata importado

#daa: Blokoj devas esti separataj per malplenaj linioj.

#sma: Normale la malplenaj linioj jam estas en la tradukebla fontdatumaro.  Sed en kelkaj malnovaj fontdatumaroj ili mankas. Ĝis antaŭ nelonge ni ne uzis malplenaj liniojn per separado sed anstataŭe skribis la tutan blokon sur nur unu linio.

#tta: En la aŭtomate produktita datumaro unu bloko okupas unu liniojn.  Sed vi ne estas devigita labori laŭ tiu regulo.  Vi povas libere tranĉi vian blokon en multaj linioj.

#dbe: Klamrar i markerade textstycken måste slutas parvis och antalet fält måste stämma. Annars går inte kompileringen igenom.

#cru: Enkla par av klamrar utan procentprefixet får inte användas, se ovan.

#ecW: MLTH läser och skriver vanligtvis texter i UTF-8 format. Många användare föredrar andra format som t ex latin-1 eller euc-jp. Om så är fallet kan man använda en konverterare, som t ex den som finns på GNU/Linux/Unix system, %(c:recode):

#WWs: The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.

#icW: la linioj devas komenci per xxx: kaj ne per ... oj, här var

#arW: sendu al retposhta adreso kun pravaj markajhoj.

#WeW: Ett enkelt sätt att skicka din text i ett unixliknande system är:

#Wps: AelWiki: Swpat översättning

#Wao: Den här wikin används för att fördela översättningsarbete till olika översättare.

#Tan: FFII Mailinglista med information om översättning.

#bnW: Forum för diskussion om och koordinering av översättningsarbete av FFIIsidor.

#WoW: Wikin används för att hitta nya tips för översättare.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: sv ;
# multlin: t ;
# End: ;

