<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 如何 往 多語超文 再 加上 一門 語言

#descr: 要在一個多語文件上另外加上一門語言, 並不需要任何超文編程技能. 你只要下載來源語言的翻譯原稿, 然後將之一行一行地用目的語言替代, 並將結果電郵回給超文作者. 本文舉進一步舉一行範例, 回答一些常問問題.

#Wee: 超文作者如何使用我的譯文

#HSh: 語法符號

#Wih: 譯者能犯何種錯誤?

#Dui: 超文作者用超文管理指令 %1 將你的譯文讀入超文結構中. 該指令將你的每行插入到與之相當的同行次原文行所在的位置. 因此, 譯文的第55行必須代表原文的第55行, 譯文行數必須與原文一致.

#rno: The blocks may contain some structured sub-blocks, marked by a pair of brackets preceded by a percent sign, see details below.

#Ase: markiloj

#emh: krampoj

#ite: Certain text functions are predefined:

#nam: name

#ert: description

#xml: example

#quo: quote

#mhs: emphasis

#iai: italic

#ywt: typewriter

#cit: cite

#srn: strong

#nrn: underline

#ane: parenthesis

#eph: theme - parenthesis

#eWn: theme - footnote (annotatio)

#lin: lines

#agp: paragraphs

#orl: unordered list

#rei: ordered list

#lWd: title line + unordered list

#eid: titel line + ordered list

#ooe: What any text function name really means is decided by the local context of the source document, the above definitions can be overridden.

#Wsb: 當 您 想 知道 文中 的 百分比 標記 符號 對 最後 結果 會 發生 何樣 效果 時, 請 看看 瀏覽 源文 的 超文文件. 其實 每次 翻譯 多語超文 譯稿 時 應該 將 超文文件 準備 好, 以便 隨時 參考.

#lfa: Nested structures such as %(AL) can be (ab)used for expanding existing web pages infinitely.

#Zlz: 行數

#apr: Markup errors

#jgt: Fajlkodiga Sistemo

#doW: sendi na tro multaj linioj

#kii: elkomentitaj linioj

#IrW: automata importado

#daa: Blokoj devas esti separataj per malplenaj linioj.

#sma: Normale la malplenaj linioj jam estas en la tradukebla fontdatumaro.  Sed en kelkaj malnovaj fontdatumaroj ili mankas. Ĝis antaŭ nelonge ni ne uzis malplenaj liniojn per separado sed anstataŭe skribis la tutan blokon sur nur unu linio.

#tta: En la aŭtomate produktita datumaro unu bloko okupas unu liniojn.  Sed vi ne estas devigita labori laŭ tiu regulo.  Vi povas libere tranĉi vian blokon en multaj linioj.

#dbe: Brackets in marked text pieces must be closed and the number of fields must be correct.  Otherwise compilation will abort.

#cru: Simple bracket pairs without the percent prefix must not be used, see above.

#ecW: MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, %(c:recode):

#WWs: The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.

#icW: la linioj devas komenci per xxx: kaj ne per #

#arW: sendu al retposhta adreso kun pravaj markajhoj.

#WeW: A simple way to send your text under Unix-like systems is:

#Wps: AelWiki: Swpat Translation

#Wao: This wiki page is used to allocate translation work to translators.

#Tan: FFII Translation Mailing List Information

#bnW: Forum for discussion about and coordination of translation work to be done for the FFII pages.

#WoW: Wiki page is used for working out further hints for translators.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: zh ;
# multlin: t ;
# End: ;

