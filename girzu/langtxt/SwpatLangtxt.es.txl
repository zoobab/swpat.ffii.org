<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Web MultilingÃ?Â?Ã?Â?e - CÃ?Â?Ã?Â³mo Puedes Escribir Con Nosotros

#descr: Para aÃ?Â?Ã?Â±adir una versiÃ?Â?Ã?Â³n en tu idioma en este portal, no 
necesitas saber HTML u otros tipos de formatos.  Tus traducciones son 
independientes del diseÃ?Â?Ã?Â±o de las pÃ?Â?Ã?Â¡ginas y
sobrevivirÃ?Â?Ã?Â¡n a futuros  cambios de este diseÃ?Â?Ã?Â±o.  Puedes
descargar un archivo de texto donde los  pÃ?Â?Ã?Â¡rrafos estÃ?Â?Ã?Â¡n
identificados con etiquetas y un pequeÃ?Â?Ã?Â±o margen.    Sustituye
esos pÃ?Â?Ã?Â¡rrafos y envÃ?Â?Ã?Â­a el archivo a nuestro procesador de
 correo que lo insertarÃ?Â?Ã?Â¡ en una base de datos y generarÃ?Â?Ã?Â¡
las pÃ?Â?Ã?Â¡ginas  de este portal de nuevo.  Hay gente dispuesta a
ayudarte.  Algunas  cuestiones bÃ?Â?Ã?Â¡sicas son respondidas
aquÃ?Â?Ã?Â­.

#Wee: Ã?Â?Ã?Â¿CÃ?Â?Ã?Â³mo funciona Ã?Â?Ã?Â©sto?

#HSh: Partes marcadas en un pÃ?Â?Ã?Â¡rrafo

#Wih: QuÃ?Â?Ã?Â© cosas puede hacer mal un traductor

#Dui: Cada bloque de texto tiene al comienzo un identificador xyz, el
cuÃ?Â?Ã?Â¡l permite al sistema insertarlo en el lugar adecuado del
documento.  El orden de los bloques es irrelevante.

#rno: Los bloques pueden contener algunas estructuras seÃ?Â?Ã?Â±aladas por
unos parÃ?Â?Ã?Â©ntesis y precedidas por el signo del tanto por ciento.
MÃ?Â?Ã?Â¡s abajo se explican con detalle.

#Ase: Algunas partes del texto tienen funciones especiales y pueden ser
presentadas con ciertas caracterÃ?Â?Ã?Â­sticas, como  la
impresiÃ?Â?Ã?Â³n en negrita o los hiperenlaces.  Estas partes de texto
estÃ?Â?Ã?Â¡n marcadas de acuerdo al siguiente esquema: %(EX)  El texto
esta delimitado por parÃ?Â?Ã?Â©ntesis precedidos por el signo del
tanto por ciento.  El primer parÃ?Â?Ã?Â©ntesis es seguido por un
%(fn:nombre de funciÃ?Â?Ã?Â³n), cuyo significado no deberÃ?Â?Ã?Â­a ser
de interÃ?Â?Ã?Â©s para el traductor, un %(fs:separador), y finalmente
de ninguno a varios campos de texto.

#usv: El %%(tp|objeto de reclamaciÃ?Â?Ã?Â³n|aquÃ?Â?Ã?Â­ mal llamado 
%%(q:invenciÃ?Â?Ã?Â³n)) de %%(EP) comprende ...

#emh: Un campo de texto puede tener a su vez partes de texto marcadas.  En 
todo caso no pueden contener parÃ?Â?Ã?Â©tesis de cierre que no sean
parte de los  %(tp| delimitadores de estructura|precedida de un
sÃ?Â?Ã?Â­mbolo de tanto por  ciento). Los parÃ?Â?Ã?Â©ntesis son
sÃ?Â?Ã?Â­mbolos reservados para marcadores y si  se quieren visualizar
deben ser expresados como una marca con %(TP).

#efe: Texto fuera del parÃ?Â?Ã?Â©ntesis %%(pe:Texto dentro del
parÃ?Â?Ã?Â©ntesis)

#fee: %%(tp|Texto fuera del parÃ?Â?Ã?Â©ntesis|Texto dentro del
parÃ?Â?Ã?Â©ntesis)

#ite: Ciertas funciones de formato estÃ?Â?Ã?Â¡n predefinidas:

#nam: nombre

#ert: descripciÃ?Â?Ã?Â³n

#xml: ejemplo

#quo: comillas

#mhs: Ã?Â?Ã?Â©nfasis

#iai: itÃ?Â?Ã?Â¡lica

#ywt: mÃ?Â?Ã?Â¡quina de escribir

#cit: cita

#srn: negrita

#nrn: subrayado

#ane: ParÃ?Â?Ã?Â©ntesis

#eph: texto - parÃ?Â?Ã?Â©ntesis

#eWn: texto - nota a pie de pÃ?Â?Ã?Â¡gina

#lin: lÃ?Â?Ã?Â­neas

#agp: pÃ?Â?Ã?Â¡rrafos

#orl: lista

#rei: lista numerada

#lWd: tÃ?Â?Ã?Â­tulo + lista

#eid: tÃ?Â?Ã?Â­tulo + lista numerada

#ooe: El significado real de una funciÃ?Â?Ã?Â³n es decidido por el contexto
local del documento de origen.  Las anteriores definiciones pueden ser
redefinidas.

#Wsb: Cuando quieras conocer el efecto que las funciones tienen en la
apariencia final del documento, echa una mirada a la pÃ?Â?Ã?Â¡gina del
lenguaje de origen con tu navegador favorito.  Tenlo listo para
consultas cuando estÃ?Â?Ã?Â©s traduciendo.

#lfa: Estructuras anidadas como %(AL) pueden ser utilizadas para expandir
infinitamente pÃ?Â?Ã?Â¡ginas web ya existentes.

#Zlz: lÃ?Â?Ã?Â­nea de separaciÃ?Â?Ã?Â³n

#apr: Errores en marcas

#jgt: Sistema de codificaciÃ?Â?Ã?Â³n del archivo

#doW: Bloques de texto actuales sustituidos por viejas versiones

#kii: No descomentar los bloques de texto

#IrW: Fracaso de la importaciÃ?Â?Ã?Â³n debido a ajustes incorrectos de las
variables a pie de pÃ?Â?Ã?Â¡gina

##? INSERTION antonioj 2004-09-18
#daa: Debe haber una lÃ?Â?Ã?Â­nea en blanco entre dos pÃ?Â?Ã?Â¡rrafos.

##? INSERTION antonioj 2004-09-18
#sma: Normalmente las lÃ?Â?Ã?Â­neas en blanco estÃ?Â?Ã?Â¡n presentes en los
archivos  de origen.  Pero no en algunos archivos antiguos.  Esto es
debido a que,  hasta que no fue un requisito el uso de las
lÃ?Â?Ã?Â­neas en blanco, cada bloque  de texto ocupaba exactamente una
lÃ?Â?Ã?Â­nea.  Si prefieres este viejo procedimiento, puedes activarlo
ajustando la variable local %(c:multlin) a %(c:nil).

##? INSERTION antonioj 2004-09-18
#tta: En cada archivo a traducir cada bloque de texto ocupa una sola
lÃ?Â?Ã?Â­nea, y debiera estar precedida y seguida por una lÃ?Â?Ã?Â­nea
en blanco.  Eres libre para dividir cada bloque en varias
lÃ?Â?Ã?Â­neas, pero debes preservar (o, en algunos archivos antiguos,
insertar) las lÃ?Â?Ã?Â­neas en blanco.

#dbe: Los parÃ?Â?Ã?Â©ntesis de apertura utilizados para delimitar bloques de
texto deben ser cerrados con sus correspondientes parÃ?Â?Ã?Â©ntesis de
cierre.  En otro caso el procesamiento del texto se abortarÃ?Â?Ã?Â¡.

#cru: Un par de parÃ?Â?Ã?Â©ntesis sin el prefijo del tanto por ciento no
deberÃ?Â?Ã?Â­ an ser usados, lee mÃ?Â?Ã?Â¡s arriba.

#ecW: MLHT normalmente usa la codificaciÃ?Â?Ã?Â³n UTF-8.  Muchos usuarios 
prefieren usar otros sistemas de codificaciÃ?Â?Ã?Â³n, como latin-1 o
euc-jp.  En  estos casos, puedes usar un convertidor como %(c:recode)
en sistemas GNU/Linux/Unix.  TambiÃ?Â?Ã?Â©n puedes dejar al sistema
mlhtimport que haga la conversiÃ?Â?Ã?Â³n.  EspecificÃ?Â?Ã?Â¡ndole algo
como %(CI) en el asunto al enviar el correo o en la secciÃ?Â?Ã?Â³n
%(LV) a pie de pÃ?Â?Ã?Â¡gina.

#WWs: El archivo deberÃ?Â?Ã?Â­a contener sÃ?Â?Ã?Â³lo aquellas lÃ?Â?Ã?Â­neas
que en  realidad fueron mejoradas.  Las otras lÃ?Â?Ã?Â­neas
deberÃ?Â?Ã?Â­an comentarse.   Siempre hay un riesgo de que un texto
mejorado sea sustituido por una  versiÃ?Â?Ã?Â³n vieja, y este riesgo
deberÃ?Â?Ã?Â­a ser minimizado.

#icW: En todas las lÃ?Â?Ã?Â­neas en las que has trabajado debes eliminar el
# inicial. De no hacerlo serÃ?Â?Ã?Â¡n tratadas como comentarios y no
serÃ?Â?Ã?Â¡n insertadas en el texto final.

#arW: Puedes enviar tu traducciÃ?Â?Ã?Â³n a un procesador de correo que 
insertarÃ?Â?Ã?Â¡ tu texto en el sistema y recompilarÃ?Â?Ã?Â¡ las
pÃ?Â?Ã?Â¡ginas  correspondientes. Para realizar esto es importante que
rellenes  correctamente las etiquetas a pie de pÃ?Â?Ã?Â¡gina,
incluidos tu usuario y  contraseÃ?Â?Ã?Â±a como el idioma al que se ha
traducido (txtlang).  Si el  cÃ?Â?Ã?Â³digo del idioma es incorrecto,
una pÃ?Â?Ã?Â¡gina en inglÃ?Â?Ã?Â©s puede ser  sustituida por una en
portuguÃ?Â?Ã?Â©s, o tal vez ni si quiera serÃ?Â?Ã?Â¡  presentada,
debido a la etapa de validaciÃ?Â?Ã?Â³n. Cada una de las lÃ?Â?Ã?Â­neas
en  esta secciÃ?Â?Ã?Â³n de variables debe empezar con un '#' y
terminar con un ';',  de otra manera la importaciÃ?Â?Ã?Â³n se
abortarÃ?Â?Ã?Â¡.

#WeW: Una manera sencilla para enviar tu texto en un sistema UNIX es:

#Wps: AelWiki: TraducciÃ?Â?Ã?Â³n Swpat

#Wao: Esta pÃ?Â?Ã?Â¡gina wiki es usada para asignar trabajo a los
traductores.

#Tan: InformaciÃ?Â?Ã?Â³n sobre la Lista de Correo de TraducciÃ?Â?Ã?Â³n de
FFII

#bnW: Foro de discusiÃ?Â?Ã?Â³n y de coordinaciÃ?Â?Ã?Â³n del trabajo de
traducciÃ?Â?Ã?Â³n  en las pÃ?Â?Ã?Â¡ginas de FFII.

#WoW: PÃ?Â?Ã?Â¡gina wiki usada para organizar las indicaciones para los
traductores.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: antonioj ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: es ;
# multlin: t ;
# End: ;

