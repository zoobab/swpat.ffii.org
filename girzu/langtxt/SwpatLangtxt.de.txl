<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Vielsprachiges WWW: Wie Sie Mitweben Können

#descr: Um einer hiesigen Webseite eine Version in Ihrer Sprache zuzufügen,
müssen Sie sich nicht mit HTML oder sonstigen Zielformaten abgeben. 
Ihre Übersetzung ist unabhängig vom Aussehen und künftigen Änderungen
des Aussehens der Webseiten.  Sie holen sich lediglich eine schlichte
Textdatei, in der die Textbausteine als besonders gekennzeichnete
Zeilen gesammelt sind.   Sie überschreiben die Blöcke und schicken sie
an einen Abarbeitungsautomaten, der die Textblöcke einbaut und die
betroffenen Webseiten neu erzeugt.  Es gibt nette Leute, die Sie dabei
anleiten.  Einige grundlegende Fragen beantworten wir hier.

#Wee: Wie funktioniert es?

#HSh: Ausgezeichnete Textteile

#Wih: Was kann der Übersetzer falsch machen?

#Dui: Jeder Textblock beginnt mit einem Identifikator XYZ:, der es dem
System erlaubt, ihn an eine bestimmte Stelle im Zieldokument
einzusetzen.  Die Reihenfolge der Textblöcke ist beliebig.

#rno: Die Blöcke können strukturierte Unterblöcke enthalten, die durch ein
Klammerpaar mit vorausgehendem Prozentzeichen markiert sind. Genaueres
s. unten.

#Ase: Manche Textteile haben besondere Funktionen im Text, die sich in
besonderer Darstellungsweise, z.B. Fettdruck oder Unterlegung mit
Verweisen, niederschlagen können.  Solche Textteile werden nach
folgendem Schema markiert: %(EX) Das markierte Textstück wird mit
einem Klammerpaar umgeben, dem als Präfix ein Prozentzeichen
vorausgeht.   Auf die öffnende Klammer folgt der
%(fn:Funktionsbezeichner), dessen Bedeutung den Übersetzer nicht
interessieren muss, und ein %(fs:Feldtrennzeichen). Darauf folgen
beliebig viele Textfelder.

#usv: Der %%(tp|Anspruchsgegenstand|hier irrtümlicherweise als
%%(q:Erfindung) bezeichnet) von %%(EP) umfasst ...

#emh: Innerhalb der Felder dürfen wiederum markierte Textstücke vorkommen. 
Es dürfen aber keine schließenden Klammern vorkommen, die nicht Teil
einer %(pe:mit Prozentpräfix beginnenden) Textauszeichnung sind. 
Runde Klammern müssen mit %(TP) ausgedrückt werden.

#efe: Thema %%(pe:Anmerkung)

#fee: %%(tp|Thema|Anmerkung)

#ite: Manche Textfunktionen sind vordefiniert:

#nam: Name

#ert: Beschreibung %(pe:Merkwort)

#xml: Beispiel

#quo: Zitat %(pe:quote)

#mhs: Betonung %(pe:emphasize)

#iai: kursiv %(pe:italics)

#ywt: Schreibmaschine %(pe:typewriter)

#cit: zitiert %(pe:cite)

#srn: fett %(pe:strong)

#nrn: unterstrichen %(pe:underline)

#ane: runde Klammern %(pe:Parenthese)

#eph: Stichwort - Kommentar %(pe:theme - Parenthese)

#eWn: Thema - Fußnote %(pe:theme - annotatio)

#lin: Zeilen %(pe:lines)

#agp: Absätze %(pe:paragraphs)

#orl: ungeordnete Liste %(pe:unordered list)

#rei: durchnumerierte Liste %(pe:orderd list)

#lWd: Titelzeile + ungeordnete Liste %(pe:title line + unorderd list)

#eid: Titelzeile + durchnumerierte Liste %(pe:title line + ordered list)

#ooe: Was eine Textfunktion wirklich bedeutet wird vom lokalen Kontext des
Quelldokumentes entschieden.  Die obigen Definitionen lassen sich
überschreiben.

#Wsb: Wenn Sie wissen wollen, wie sich die abstrakten Hypertextmarkierungen
und Variablen auswirken, lesen Sie dazu die Web-Version zu
übersetzenden Quelltextes.  Halten Sie sie beim Übersetzen immer in
Ihrer Arbeitsumgebung (z.B. im einem Browser-Fenster) bereit.

#lfa: Verschachtelte Strukturen wie %(AL) können (miss)braucht werden, um
bestehende Webseiten unendlich zu erweitern.

#Zlz: Zeilentrennung

#apr: Auszeichnungsfehler

#jgt: Dateikodierungssystem

#doW: Neue Blöcke überschrieben

#kii: neue Blöcke auskommentiert

#IrW: Textimport wegen falsch gesetzter Lokaler Variablen gescheitert

#daa: Textblöcke sind durch Leerzeilen voneinander zu trennen.

#sma: Üblicherweise sind die Leerzeilen bereits im zu übersetzenden
Quelltext vorhanden. In manchen Quelltexten fehlen sie dagegen. Dies
kommt daher, dass wir bis vor kurzem noch keine Leerzeilen für die
Trennung verwendet haben aber stattdessen war es nötig, dass jeder
Block nur genau eine Zeile verwendet hat. Wenn Sie das alte Verhalten
bevorzugen, dann können sie dieses durch das Setzen der lokalen
Variable %(c:multlin) auf %(c:nil) erzwingen.

#tta: In der generierten Quelldatei belegt ein Block eine Zeile, und er
sollte davor und danach eine Leerzeile aufweisen. Es ist Ihnen
freigestellt ihren Block über mehrere Zeilen umzubrechen, aber Sie
müssen dabei die trennenden Leerzeilen beibehalten (oder diese bei
einzelnen, alten Dateien selbst einfügen).

#dbe: Klammerpaare in markierten Textblöcken müssen abgeschlossen sein und
die Zahl der darin eventuell enthaltenen Felder muss stimmen. Sonst
bricht die Kompilierung ab.

#cru: Einfache Klammerpaare ohne Prozentpräfix dürfen nicht verwendet
werden, s. oben.

#ecW: Das MLHT-System liest und schreibt in der Kodierung UTF-8.  Viele
Übersetzer schreiben lieber in anderen Kodierungen wie z.B. iso-8859-1
oder euc-jp.  In solchen Fällen kann man sich mit einem Konverter
(z.B. auf Unix-Systemen %(c:recode)) behelfen, oder man lässt MLHT die
Konversion vornehmen, indem man eine Zeichenkette wie %(CI) in der
Betreffzeile oder unter %(LV) im Nachspann angibt.

#WWs: Aus dieser Datei sollten nur diejenigen Zeilen ausgelesen werden, die
wirklich verbessert wurden.  Immer, wenn man eine Zeile bearbeitet,
entfernt man das Gartenzaun-Zeichen am Zeilenanfang.  Diejenigen
Zeilen, die mit dem Gartenzaun-Zeichen beginnen, werden vom System als
Kommentare betrachtet, d.h. ignoriert.

#icW: Wenn Sie eine Zeile übersetzt/bearbeitet haben, nehmen Sie dort den
#Gartenzaun am Zeilenanfang weg.  Sonst wird die Zeile als Kommentar
betrachtet.

#arW: Mit geeigneter Zugangsberechtigung können Sie Ihre Texte an einen
E-Post-Abarbeitungsautomaten senden.  Dazu müssen Sie die Variablen im
Fuß des Textes richtig belegen.  Insbesondere muss txtlang stimmen,
denn sonst könnte die falsche Sprache überschrieben werden (sofern das
System Sie nicht als unzuständig abweist).  Alle Zeilen in dem
Variablenbelegungs-Abschnitt am Ende beginnen mit '#' und enden mit
';'.  Jede kleine Abweichung von dieser Syntax macht die ganze Datei
ungültig.

#eln: Die Betreffzeile des E-Briefes (mail) muss die Wörter %(KS) enthalten.
 Man kann auch ein anderes Kodierungssystem wie z.B. iso-8859-1
angeben.  In diesem Falle wird die Umkodierung vom abarbeitenden
Programm vorgenommen.  Die Variablenbelegung %(KB), die sich am Fuß
des Textes findet, darf nie geändert werden.

#WeW: Eine einfache Art, Ihren Text unter einem Unix-artigen System zu
schicken, ist:

#Wps: AelWiki: Swpat-Übersetzung

#Wao: Diese Wiki-Seite dient dazu Übersetzungsarbeiten zwischen Übersetzern
aufzuteilen.

#Tan: FFII-Übersetzungs-Verteiler

#bnW: Forum für die Koordination der Übersetzungsarbeit auf FFII-Seiten.

#WoW: Diese Wiki-Seite wird verwendet, um weitere Hinweise für  Übersetzer
auszuarbeiten.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.3/site-lisp/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: de ;
# multlin: t ;
# End: ;

