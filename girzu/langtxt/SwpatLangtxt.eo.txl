<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: TTT Multlingva - Kiel Vi Teksu Kun Ni

#descr: Por almeti novan lingvoversion al plurlingva hiperteksto MLHT, oni tute ne bezonas koni na MLHT.  Oni nur prenas fontotekstan fajlon de la originlingvo kaj lini'op'e reskribas ĝin en sia lingvo.  Ekzemplojn kaj respondojn al oftaj demandoj mi kolektas tie ĉi

#Wee: Kiel mian traduktekston uzas la verkanto?

#HSh: hipertekst-sintaksaj signoj

#Wih: Kiel la tradukanto povas erari?

#Dui: Blokoj korespondas per la identigilo ĉe la linikomenco.

#rno: The blocks may contain some structured sub-blocks, marked by a pair of brackets preceded by a percent sign, see details below.

#Ase: markiloj

#emh: krampoj

#ite: Certain text functions are predefined:

#nam: name

#ert: description

#xml: example

#quo: quote

#mhs: emphasis

#iai: italic

#ywt: typewriter

#cit: cite

#srn: strong

#nrn: underline

#ane: parenthesis

#eph: theme - parenthesis

#eWn: theme - footnote (annotatio)

#lin: lines

#agp: paragraphs

#orl: unordered list

#rei: ordered list

#lWd: title line + unordered list

#eid: titel line + ordered list

#ooe: What any text function name really means is decided by the local context of the source document, the above definitions can be overridden.

#Wsb: Se vi deziras scii kion efikas la markiloj, vidu la originan retpaĝon.  Tradukante havu kopion de la retpaĝo en via cirkaŭaĵo.

#lfa: Nested structures such as %(AL) can be (ab)used for expanding existing web pages infinitely.

#Zlz: liniseparado

#apr: Markup errors

#jgt: Fajlkodiga Sistemo

#doW: sendi na tro multaj linioj

#kii: elkomentitaj linioj

#IrW: automata importado

#daa: Blokoj devas esti separataj per malplenaj linioj.

#sma: Normale la malplenaj linioj jam estas en la tradukebla fontdatumaro.  Sed en kelkaj malnovaj fontdatumaroj ili mankas. Ĝis antaŭ nelonge ni ne uzis malplenaj liniojn per separado sed anstataŭe skribis la tutan blokon sur nur unu linio.

#tta: En la aŭtomate produktita datumaro unu bloko okupas unu liniojn.  Sed vi ne estas devigita labori laŭ tiu regulo.  Vi povas libere tranĉi vian blokon en multaj linioj.

#dbe: Brackets in marked text pieces must be closed and the number of fields must be correct.  Otherwise compilation will abort.

#cru: Simple bracket pairs without the percent prefix must not be used, see above.

#ecW: MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, %(c:recode):

#WWs: The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.

#icW: la linioj devas komenci per xxx: kaj ne per #

#arW: sendu al retposhta adreso kun pravaj markajhoj.

#WeW: A simple way to send your text under Unix-like systems is:

#Wps: AelWiki: Swpat Translation

#Wao: This wiki page is used to allocate translation work to translators.

#Tan: FFII Translation Mailing List Information

#bnW: Forum for discussion about and coordination of translation work to be done for the FFII pages.

#WoW: Wiki page is used for working out further hints for translators.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: erjos ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: eo ;
# multlin: t ;
# End: ;

