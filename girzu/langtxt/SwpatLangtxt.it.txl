<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Web Multilingue - Come aiutarci con le traduzioni

#descr: Per aggiungere la versione nella vostra lingua di una pagina di questo sito, non dovete preoccuparvi dell'HTML o di altri formati. Le traduzioni sono indipendenti dall'aspetto della pagina e sopravvivono ai cambiamenti di quest'ultimo.  Scaricate un file di testo semplice che contiene i blocchi di testo che compongono la pagina, preceduti da codici identificativi e completati da un markup minimale.  Traducete questi blocchi di testo e spedite il file via email a un sistema automatico, che li inserisce in un database e rigenera le pagine. Ci sono anche persone gentili che possono aiutarvi, ma alcune questioni di base sono affrontate qui di seguito.

#Wee: Come funziona?

#HSh: Blocchi di testo marcati

#Wih: Cosa può sbagliare il traduttore

#Dui: Ogni blocco di testo è preceduto da un codice di identificazione xyz, che permette al sistema automatico di inserirlo al posto giusto nel documento finale. L'ordine dei blocchi di testo nel file è irrilevante.

#rno: I blocchi possono contenere alcuni sotto-blocchi strutturati, racchiusi da un paio di parentesi precedute da segni di percentuale: si vedano i dettagli più sotto.

#Ase: Alcuni blocchi di testo contengono funzioni speciali che implementano caratteristiche di presentazione speciali, come il grassetto o i link ipertestuali.  Queste parti di testo sono marcate secondo lo schema seguente: %(EX)  La parte di testo marcata è circondata da parentesi precedute dal segno di percentuale. La parentesi di apertura è seguita da un %(fn:nome di funzione), il cui significato non deve interessare al traduttore, un %(fs:separatore di campi) e infine zero o più campi di testo.

#usv: L' %%(tp|oggetto della rivendicazione|qui definito erroneamente %%(q:invenzione)) di %%(EP) comprende ...

#emh: Un campo di testo può a sua volta contenere parti di testo marcato.  Tuttavia non può contenere parentesi chiuse che non siano parte di una %(tp|struttura di markup|preceduta da un segno di percentuale).  La parentesi è riservata per gli scopi del markup.  Le parentesi devono essere scritte sotto forma di markup con %(TP).

#efe: Testo fuori parentesi %%(pe:Testo tra parentesi)

#fee: %%(tp|Testo fuori parentesi|Testo tra parentesi)

#ite: Esistono alcune funzioni testuali predefinite:

#nam: nome

#ert: descrizione

#xml: esempio

#quo: virgolette

#mhs: enfasi

#iai: corsivo

#ywt: macchina da scrivere

#cit: citazione

#srn: grassetto

#nrn: sottolineato

#ane: parentesi

#eph: testo - parentesi

#eWn: testo - nota

#lin: righe

#agp: paragrafi

#orl: lista non ordinata

#rei: lista ordinata

#lWd: titolo + lista non ordinata

#eid: titolo + lista ordinata

#ooe: Il significato reale del nome di ogni funzione dipende dal contesto locale del documento sorgente; le definizioni soprastanti possono essere ridefinite.

#Wsb: Per capire l'effetto delle funzioni sull'aspetto finale del documento, è possibile dare un'occhiata al sorgente della pagina con il proprio browser.  È consigliabile tenerlo a portata di mano mentre si traduce.

#lfa: Strutture nidificate come %(AL) possono essere sfruttate per espandere le pagine web all'infinito.

#Zlz: Righe di separazione

#apr: Errori di sintassi

#jgt: Sistema di codifica dei file

#doW: Nuovi blocchi sovrascritti

#kii: Nuovi blocchi commentati

#IrW: Impostazione errata delle variabili in fondo al file

##? INSERTION crigamon 2004-05-17
#daa: Deve esistere una riga vuota tra due successivi blocchi di testo.

##? INSERTION crigamon 2004-05-17
#sma: Normalmente le righe vuote sono già presenti nel file sorgente della traduzione, ma in alcuni vecchi file possono mancare, perchè fino a poco tempo fa non venivano usate le righe vuote come separatore, veniva invece richiesto che ogni blocco di testo fosse contenuto sulla stessa riga.  Se preferite questa vecchia convenzione, potete abilitarla impostando la variabile locale %(c:multlin) a %(c:nil).

##? INSERTION crigamon 2004-05-17
#tta: Nel file sorgente da tradurre, ogni blocco è contenuto in una sola riga ed è preceduto e seguito da una riga vuota.  Siete liberi di suddividere il blocco di testo in diverse righe, ma dovete conservare (o, in alcuni vecchi file, inserire) le righe vuote di separazione.

#dbe: Le parentesi nelle parti di testo marcato devono essere chiuse e il numero dei campi all'interno delle parentesi dev'essere corretto, altrimenti la procedura di compilazione fallirà.

#cru: Le parentesi senza il segno di percentuale non possono essere usate; si veda sopra.

#ecW: Il sistema di traduzione MLHT di solito legge e scrive file con codifica UTF-8.  Molti utenti preferiscono usare altri sistemi di codifica, come latin-1 o euc-jp.  In questi casi, potete usare un convertitore, come ad esempio, per i sistemi GNU/Linux/Unix, %(c:recode).  Potete anche delegare la conversione al sistema mlhtimport, specificando %(CI) nell'oggetto della mail o nella sezione %(LV) in fondo al file.

#WWs: Il file deve contenere solo i blocchi di testo che sono stati tradotti.  Gli altri dovrebbero essere lasciati commentati, ossia cominciare con un carattere '#'.  C'è sempre il rischio che un blocco di testo già tradotto sia sovrascritto da una versione più vecchia, e questo rischio va minimizzato.

#icW: Dai blocchi di testo che sono stati modificati va rimosso il carattere # iniziale, altrimenti verranno trattati come commenti e non saranno inseriti nella pagina.

#arW: Potete spedire la traduzione a un sistema mail automatico che inserirà il vostro testo e ricompilerà le pagine indicate.  A questo scopo, è importante che riempiate correttamente i campi in fondo al file, compresi i vostri login e password e la lingua in cui avete tradotto (txtlang).  Se la lingua non è indicata correttamente, una pagina inglese potrebbe essere sovrascritta da un testo portoghese, oppure potrebbe non venir modificato nulla, a seconda della procedura di validazione.  Si noti che ognuna delle righe che contengono le variabili in fondo al file devono iniziare per '#' e finire per ';', altrimenti l'importazione fallirà.

#WeW: Un modo semplice per spedire il testo da un sistema simile a UNIX è:

#Wps: AelWiki: traduzione di swpat

#Wao: Questa pagina wiki è usata per suddividere il lavoro di traduzione tra i volontari.

#Tan: Mailing list informativa sulle traduzioni di FFII

#bnW: Un luogo di discussione e di coordinamento per il lavoro di traduzione delle pagine di FFII.

#WoW: Pagina wiki usata per aggiungere ulteriori informazioni utili ai traduttori.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatgirzu.el ;
# mailto: mlhtimport@a2e.de ;
# login: crigamon ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatLangtxt ;
# txtlang: it ;
# multlin: t ;
# End: ;

