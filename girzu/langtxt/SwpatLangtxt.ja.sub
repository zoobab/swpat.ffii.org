\begin{subdocument}{SwpatLangtxt}{多言語超文をいかにしてさらに多言語化できるか}{http://swpat.ffii.org/girzu/langtxt/index.ja.html}{仕事組\\swpatag@ffii.org}{多言語文献にもう一つの言語を付けたい時は，超文プログラム組み技能は正に要らない。文献の翻訳手本を取り寄せ，その中の一行一行を目的言語に書き直した結果を超文作者に送り返えせば良いのです。本文ではその実例を挙げ，常問問題をお答えします。}
\begin{sect}{cel}{超文作者はいかにして私の訳文を使うのか？}
超文作者は超文管理指令  によって貴方の超文を超文構造に読み込み，各行の挿入位置はそれと該当する原文の挿入位置によって決まります。従って，訳文の第55行は原文の第55行の訳であり，兩文行数は一致しなければなりません。

The blocks may contain some structured sub-blocks, marked by a pair of brackets preceded by a percent sign, see details below.
\end{sect}

\begin{sect}{erhs}{超文文法記號}
markiloj

krampoj

Certain text functions are predefined:

\begin{center}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
name & description & example\\\hline
q & quote & ``abc''\\\hline
e & emphasis & \emph{abc}\\\hline
i & italic & {\it abc}\\\hline
t & typewriter & {\tt abc}\\\hline
c & cite & {\it abc}\\\hline
s & strong & {\bf abc}\\\hline
u & underline & \underline{abc}\\\hline
pe & parenthesis & (abc)\\\hline
tp & theme - parenthesis & abc (def)\\\hline
ta & theme - footnote (annotatio) & abc\footnote{def}\\\hline
nl & lines & abc

def

ghi\\\hline
al & paragraphs & abc

def

ghi\\\hline
ul & unordered list & \begin{itemize}
\item
abc

\item
def

\item
ghi
\end{itemize}\\\hline
ol & ordered list & \begin{enumerate}
\item
abc

\item
def

\item
ghi
\end{enumerate}\\\hline
linul & title line + unordered list & abc
\begin{itemize}
\item
def

\item
ghi

\item
jkl
\end{itemize}\\\hline
linol & titel line + ordered list & abc
\begin{enumerate}
\item
def

\item
ghi

\item
jkl
\end{enumerate}\\\hline
\end{tabular}
\end{center}
\end{center}

What any text function name really means is decided by the local context of the source document, the above definitions can be overridden.

その超文記号の最終效果を知りたい時にはブラウザーで元言語の超文型態を参考すれば宜しいです。それがいつも参考できるような翻訳仕事場を作っておいて下さい。

Nested structures such as \begin{quote}
\percent{}(al|Article 1|Conditions of Patentability|\percent{}(LARGE:Human genes are unpatentable) ...|Article 7.b|...\percent{}(small:unless they are replicable outside the human body))
\end{quote} can be (ab)used for expanding existing web pages infinitely.
\end{sect}

\begin{sect}{err}{どこで間違えし易い？}
\begin{sect}{erln}{行數}
Blokoj devas esti separataj per malplenaj linioj.

{\small Normale la malplenaj linioj jam estas en la tradukebla fontdatumaro.  Sed en kelkaj malnovaj fontdatumaroj ili mankas. Ĝis antaŭ nelonge ni ne uzis malplenaj liniojn per separado sed anstataŭe skribis la tutan blokon sur nur unu linio.}

En la aŭtomate produktita datumaro unu bloko okupas unu liniojn.  Sed vi ne estas devigita labori laŭ tiu regulo.  Vi povas libere tranĉi vian blokon en multaj linioj.
\end{sect}

\begin{sect}{markerr}{Markup errors}
Brackets in marked text pieces must be closed and the number of fields must be correct.  Otherwise compilation will abort.

Simple bracket pairs without the percent prefix must not be used, see above.
\end{sect}

\begin{sect}{utf8}{Fajlkodiga Sistemo}
MLHT usually reads and writes texts in UTF-8 coding.  Many users prefer to use another coding system, such as latin-1 or euc-jp.  In such cases, you can use a converter such as, under GNU/Linux/Unix systems, {\it recode}:

\begin{verbatim}
$ recode utf-8..latin-1 swpat.fr.txt
$ edit swpat.fr.txt
$ recode latin-1..utf-8 swpat.fr.txt
\end{verbatim}
\end{sect}

\begin{sect}{trolin}{sendi na tro multaj linioj}
The file should contain only those lines that were actually improved.  Others should be turned into commented, they should be on lines that begin with a '\#' character.  There is always a certain risk that an already-improved text is overwritten by an older version, and this risk should be minimised.
\end{sect}

\begin{sect}{jingzf}{elkomentitaj linioj}
la linioj devas komenci per xxx: kaj ne per \#
\end{sect}

\begin{sect}{mlhtimport}{automata importado}
sendu al retposhta adreso kun pravaj markajhoj.

temlinio

A simple way to send your text under Unix-like systems is:

\begin{verbatim}
\$ cat mytext.txt | mailx -s ``coding: iso-8859-1`` mlhtimport@ffii.com
\end{verbatim}
\end{sect}
\end{sect}

\begin{sect}{links}{さらに読むべき文章}
\begin{itemize}
\item
{\bf {\bf FFII Translation Mailing List Information\footnote{http://lists.ffii.org/mailman/listinfo/traduk/TradukListinfo.ja.html}}}

\begin{quote}
Forum for discussion about and coordination of translation work to be done for the FFII pages.
\end{quote}
\filbreak

\item
{\bf {\bf AelWiki: Swpat Translation\footnote{http://wiki.ael.be/index.php/FightingSWPatentsTranslation}}}

\begin{quote}
This wiki page is used to allocate translation work to translators.
\end{quote}
\filbreak

\item
{\bf {\bf http://kwiki.ffii.org/SwpatLangtxtJa}}

\begin{quote}
Wiki page is used for working out further hints for translators.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgirzu.el ;
% mode: latex ;
% End: ;

