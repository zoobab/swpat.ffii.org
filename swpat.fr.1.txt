<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Ces dernières années, l'Office Européen des Brevets a, en contradiction à la fois avec la lettre et avec l'esprit du droit positif, accordé environ 30 000 brevets sur des règles d'organisation et de calcul mises en oeuvre par ordinateur (programmes d' ordinateurs). A présent, le mouvement européen pro-brevet veut entériner cette pratique en édictant une nouvelle directive.  Les programmeurs européen se voient confrontés à des risques considérables.  Vous trouverez ci-dessous la documentation sur le débat actuel, commencant par une courte introduction et l'actualité.
title: FFII: Brevets Logiciels en Europe
intprop: propriété intellectuelle
indprop: propriété industrielle
ieW: biens immatériels
Pmk: brevet logique
mathpat: méthodes mathématiques
busmeth: méthodes d'affaires
orgkalk: règles d'organisation et de calcul
invent: invention
noninv: non-inventions
cpe: inventions mises en oeuvre par ordinateur
cle: inventions pouvant êtres mises en oeuvre par ordinateur
sbE: inventions pour des logiciels
smW: inventions implémentés par logiciel
Saa: brevet logiciel
Srn: délivrance de brevets logiciels
mre: brevets d'ordinateur
Tan: brevets informatiques
fit: brevets d'information
tsr: invention technique
tca: caractère technique
Thz: technicité
technik: technologie
sri: ingénierie logicielle
gcn: caractère industriel
Pta2: brevetabilité
matpatjur: droit de brevet matériel
Nur: droit d'usage
Pta3: inflation des brevets
qlf: open source
saa: standardisation
iot: innovation
cpi: compétition
Esa: Office des Brevets
Gri: Direction Générale du Marché Intérieur
patmov: patent movement
patfam: patent family
patest: patent establishment
patjur: patent law
patjurist: patent lawyers
lobby: lobby
immjur: droit des biens immatériels
nWW: A propos de la FFII
cas: La %(fi:FFII) est une association à but non lucratif pour la promotion du développement d'interfaces ouvertes, de logiciels open source et d'une information publique librement accessible. Elle souhaite une construction sur le long terme de biens communs informationnels grà¢ce à une puissante union des logiciels libres et des logiciels propriétaires. La FFII coordonne un %(sp:groupe de travail sur les brevets logiciels) qui est %(sp:soutenu par des éditeurs de logiciels allemands reconnus). La FFII est membre de l'%(el:Alliance EuroLinux).
meps: MPE
dokurl: URL permanente de cette publication
kompet: compétition
interop: Interopérabilité
ekonist: Economists
jarco: manif
fatri: action distribuée
girzu: Groupe
groupedit: Mirroire Editable
komp0305: 30 Informaticiens
pri: A propos de %1
jinvi: Positions
google: Cherche Google %1
omit: [%{dots}]
amends: Amendments
nextven: Calendrier Europarl
hotelbed: Lits Gratuits
amend: amendement
aldon: insertion
justif: justification
esse: Essence
prop: Proposition
antiprop: Contre-Proposition
pref: Avant
techkontrib: Contribution Technique
progklm: Revendications Programme
interoper: Interopérabilité
eubsa-post: Après
propos: Proposition
fnot: Annotations
fins: Finances
subskrib: Sign
subten: Soutenir Maintenant
membr: Rejoindre
kunag: Aider
ltral: %1 à %2
lang-de: Allemand
lang-fr: Français
lang-en: Anglais
lang-zh: Chinois
lang-ja: Japonais
lang-ko: Coréen
lang-sv: Suédois
lang-da: Danois
lang-it: Italien
lang-ru: Russe
land-us: EUA
land-gb: Grande Bretagne
land-gr: Grèce
land-uk: Royaume Uni
land-fr: France
land-de: Allemagne
land-dk: Danemark
land-kr: Corée
land-jp: Japon
land-it: Italie
land-cz: Tchéquie
land-pl: Pologne
land-fi: Finlande
land-cn: Chine
land-es: Espagne
land-pt: Portugal
land-hu: Hongrie
land-ch: Suisse
land-se: Suède
land-no: Norvége
land-nl: Pays-Bas
land-ru: Russie
land-at: Autriche
land-be: Belgique
land-au: Australie
land-eu: Europe
mon1: Janvier
mon2: Février
mon3: Mars
mon4: Avril
mon5: Mai
mon6: Juin
mon7: Juillet
mon8: Aoà»t
mon9: Septembre
mon10: Octobre
mon11: Novembre
mon12: Décembre
sem0: dimanche
sem1: lundi


sem2: mardi
sem3: mercredi
sem4: jeudi
sem5: vendredi
sem6: samedi
links: Liens Annotés
localcopy: copie locale
dank: merci à %1 pour cette contribution et pour les suivantes
datei: Fichier(s)
inhalt: Contenu
docmenu: Contenu du Site
datum: Contenu
author: Contenu
remark: remarque
titel: titre
patappl: dépôt de brevet
patfinal: version accordée
source: source
patclms: Revendications de Brevet
claims: Revendications
desc: desciption
abstract: résumé
prgout: %1 output
pdfpikta: L'OEB publie le brevet final sous la forme d'une collection de fichiers images.  Pour les rendre plus lisibles, nous les avons réunis dans un seul gros %(pf:fichier PDF) et les avons %(gb:réduits) à un %(tf:fichier texte).
papiref: source
redebeitr: discours
localversion: version locale
epurl-orig: interface utilisateur de l'OEB
espacenet-warn: Pour les plus persévérants. Enregistrez tout d'abord vos fichiers, activez Javascript, prenez une tasse de café et soyez très patient !
intro: introduction
tasks: Questions, Choses à faires, Comment vous pouvez aider
status: état actuel
neues: Actualités de l'année %1
text: Le Texte
excerp: Extraits
neuesdescr: Nouvelles sur les Brevets Logiciels en %1
circa: env. %1
max: max. %1
min: min. %1
ekz: ex. %1
ff: %1 ff.
cf: cf %1
cfet: voir aussi %1
cfex: voir par ex. %1
cfsecpg: voir %1, page %2
cfsub: cf %1
ie: %(b:i.e) %1
vgl: cf %1
incl: inklud %1
pag: p. %1
mr: M. %1
ausim: %1 ou similaire
ord: %1.~%2
ante: < %1
post: < %1
version: version %1
progversion: %1 version %2
section: Section %1
managed-by: geré par %1
more: encore
docstyle-by: style de page créé par %1
tradukita-de: traduit par %1
tradathom: version française %1 par %2
moretocome: sera étendu
mail: courriel
http: WWW
note: Note
pagina: page
fig: fig. %1
notes: Annotations
keywdig: chercher un mot
submit: OK
appendix: Annexe
etc: etc
kaj: et
dun: , 
aut: ou
resp:  / 
urb-muc: Munich
urb-kln: Cologne
urb-vie: Vienne
urb-stu: Stuttgart
urb-ber: Berlin
urb-lsb: Lisboa
ltxt: texte traduisible
prin: version imprimable
uplvl: niveau supérieur
pgtop: en-tête
pgbot: pied
next: suivant
prev: précédent
orig: original
lhlp: Comment aider?
ceo: PDG
PDG: PDG
patent: brevet
copyright: droit d'auteur
matter: matière
mind: pensée
invention: invention
idea: idée
implem: mise en oeuvre
zeit: heure
frag: Questions
refe: Conférenciers
EU: UE
CEC: CCE
CEU: CUE
ESC: CES
EPC: CBE
EPO: OEB
TRIPs: ADPIC
adres: Adresse Postale
tel: tél
fax: fax
CET: TEC
zonetime: %2 %1
exegesis: Interprétation
applicant: propriétaire
invnom: Désignation de l'invention
patentnr: Numéro de brevet
pubdat: Date de publication
inventor: Inventeur
requested: demandé
applnr: Identifiant du dépôt
priordat: date d'antériorité
ipclass: classe IPC
ecclass: classe EC
claim: Revendication
claim1: Revendication 1
equivpat: Brevets similaires
grantdat: Date de délivrance
nenri: espace privé
tools: outils
materia: matière
mens: mind
iusauct: droit d'auteur
penmi: Dates
thes: thèses
consult_org_when: Consultation %1 %2
gl_pto_when: Règles %1 %2
org_when: %1 %2
kontnum: %1 %2
kontnum*: %2. %1
lok_org: %(ui:%2) de %1
profesor: professeur
ofic@org: %1 %2
univ: université
technuniv: Université de Technologie
EuroParl: EuroParl
urb-bxl: Bruxelles
urb-bru: Bruxelles
percent: Pourcent
vreji: Archive
prina: Documents à imprimer
mrilu: Rondes
cusku: Citations
minra: Miroirs
sisku: Impact
bandu: Pour
fapro: Contre
sarxe: Astuces
pikci: Pétitions
cnino: Actualité
lanle: Analyse
bende: Organisations
drata: Divers
liste: Liste
disk: Disque dur
swpdokucd: Brevets sur les logiciels -- le Débat
swpdokupe: ÃÆÃ¢â¬Â°dition périodique
swpatflalu: jurisprudence
remna: Personnalités
kamni: Organisations
xatra: Lettres
gasnu: Acteurs
WIPO: OMPI
useujp: Trilatérale
swnbmbf022: DE 2002/02 UnivPat
swnbmbf014: DE 2001/04 UnivPat
gruene: Verts
cecvult: Commission Européenne veut
unlim: Brevetabilité Sans Limite
bybsa: Proposition redigée par la BSA
propcpedu: Contre-Proposition et Appel à l'Action
ArtX: Article %1
RecX: Recital %1
ArtXX: Articles %1
noepatents: NON AUX BREVETS %(it:LOGICIELS)!
signez: veuillez signer %1
swpat: Brevets Logiciels
krasi: Sources
swnepon023: OEB 2002-03 sur les Méthodes de Gestion, Gènes
swnlxmg021: Swpat im Wahljahr
swncpat01C: Efforts hectiques pour Brevet Communautaire
swnmpis01B: Le gouvernement allemand publie une étude ambiguà«
swneuip01A: Consensus de la CE sur la directive?
swnepgl01A: Nouvelles directives
swnrand01A: Standards internet brevetés
lahaye: Convention de La Haye
swnfluh015: Le brevet logiciel : bénédiction ou malédiction ?
swnwipo014: Le projet de contrat de l'IPO comme ciment pour l'inflation des brevets 
swnpatg2C: Lettre ouverte pour 5 propositions de loi
swngrossen: Après la conférence diplomatique
swnpikta: La FFII publie le musée des horreurs des brevets logiciels européens
swnepue28: Contre la %(q:Proposition de Base)!
tisna: Inflation
swpatdotco: Patflation en DE
swpatfrati: Que faire ?
swpatfarit: Ce qui a déjà été fait
damba: Combattre de l'intérieur
lijda: Mouvement
preti: FAQ
question: demande
answer: réponse
danfu: FAQ
purci: Antériorité
sidbo: Idées
swpurcnino: Datation
gacri: Bouclier ?
pleji: Compenser ?
cteki: ÃÆÃ¢â¬Â°vasion Fiscale
manri: Test Suite
korcu: Invention
pante: Le recours au droit
javni: Directive
ekon: ÃÆÃ¢â¬Â°conomie
jursit: Lois en vigueur
xrani: Effets
djica: Souhaits
cpedu: Appel
lisri: Rapport
papri: Recensements
sarji: Sponsors
frili: Trivial
stidi: Analyse
basti: Sui Generis
moses: Moses
stalin: Stalin
trips: ADPIC
udgor: Scandinavie
MParl: membre du parlement
EuroMP: Membre du Parl. Eur.
MdB: Membre du Parlament Fédéral Allemand
topswpat: Top Software Probability Batch nr %1
versionXonly: seulement dans la version %1
novpet: Nouvelle pétition
patg2C: 5 Initiatives
epue28: Proposition de Base 2000/08
epue31: Munich 2000/11
bund028: bund.de 2002-08
nopat: Lettres
subskr: Signataires
subskrnov: Nouveaux signataires
siglist: Livre d'Or
sigform: Signature
eulojban: Les brevets en langage logique !
pr_orgdat: CdP %1 %2
bgh27: La Cour de Justice Fédérale va trop loin
egp27: Brevets sur les logiciels: La qualité d'abord !
karni: Courrier des lecteurs
mupli: ÃÆÃ¢â¬Â°chantillons
swpatpikta: Brevets
namcu: Statistiques
swpiktxt: Champ de mines
swpikimg: Tas de débris
ep533098: Contrôl d'état
ep487110: diagnostic médical automatique
ep193933: prise de contrôle d'un ordinateur par un autre
ep769170: attraper un virus
ep792493: tarification dynamique
ep328232: signature numérique avec informations supplémentaires d'authentification
ep800142: transformation des noms de fichiers
ep529915: prise de contrôle de plusieurs ordinateurs à l'aide de signaux linguistiques
ep747840: serveur web extensible
ep587827: feedback par défaut
ep490624: configuration réseau intuitive
ep762304: système de bourse sur Internet
ep767419: visualisation complète dans une petite fenêtre 
ep688450: système de fichiers pour éléments de sauvegarde flash 
ep807891: caddy
ep823173: compression de données dans les communications mobiles TCP
ep522591: traduction d'une langue naturelle en langue formelle pour interroger des bases de données à l'aide de parseurs et de tableurs
ep242131: visualisation d'un processus
ep359815: distinction entre les blocs de mémoire utilisés et non utilisés pour traiter les fautes d'allocation dans le cadre d'un système de cache
ep592062: requêtes de compression
ep756731: fabrication de listes de courses à partir de recettes de cuisine pour augmenter les ventes dans un supermarché
ep664041: examens programmés
ep517486: intégration de données contextuelles
ep825526: interprète virtuel
ep825525: héritage d'objets CORBA
ep797806: tarification dynamique
de19838253: pare-feu
ep794705: cuisson programmée
ep497041: infusion programmée
ep689133: menus en 3D
ep461127: apprentissage des langues par comparaison entre les paroles de l'élève et celles du professeur 
swpatgirzu: Groupes de travail
swpatgunka: What to do
swpatfatri: Patent OCR Netaction
swnerpapri: Disque dur
swnergunka: Disque dur
swnermesse: Présence prévue sur les salons
swnerstidi: Documents de référence 
swpberlinit: Possibilités d'initiatives légales
multask: Multità¢ches
mkomerc: M-Commerce
unwort: Unwort
swpatfukpi: Droit d'Auteur Faible?
swnersoros: NoEPatents 2002/3
kontakt: contacte
detal: details
bakgr: backgrounds
president: président
vicpres: vice président
cto: Directeur R&D
userathost: courriel à l'utilisateur %1 sur machine %2
priffii: A propos de la FFII
prieulux: A propos de l'Alliance Eurolinux
lae: Quel est le problème avec les brevets logiciels?
oOv: Situation Actuelle en Europe
and: Qu'est-ce que nous pouvons faire?
eWe: Dernières Nouvelles
Bre: Propositions de lecture
dsW: Si Haydn avait breveté %(q:une symphonie caractérisée par sa contruction [en forme de sonate élargie]), Mozart se serait retrouvé en difficulté.
ltd: Contrairement au droit d'auteur, les brevets peuvent bloquer des créations indépendantes.  Le brevetage du logiciel peut rendre le droit d'auteur du logiciel inutile.
eas: Les %(es:études économiques) montrent que les brevets logiciels ont entraà®né une diminution des investissements en recherche et développement.
iWd: %(it:Les progrès en informatique sont des progrès dans le domaine de l'abstraction). Traditionnellement, les brevets étaient conçus pour des %(e:inventions) de nature concrète, physique. Les brevets sur les logiciels concernent des idées. Au lieu de breveter un  "type de souricière", on se réserve tout %(q:moyen de capture d'animaux) ou %(ep:moyen de capture d'un flux de données dans un environnement simulé). Le fait que pour cela on utilise une machine logique universelle, l'ordinateur, ne constitue pas une limite.  %(s:Si les logiciels sont brevetables, tout est brevetable.)
tek: Dans la plupart des pays, le logiciel a, comme les mathématiques ou d'autres domaines abstraits, été expressément exclu du champr de l'invention brevetable. Ces règles ont néanmoins été brisées. Le système de brevet n'est plus contrôlable. Une communauté fermée de conseils en propriété industrielle est en train de créer, violer et réécrire ses propres règles sans aucun contrôle extérieur.
0ge: La Commission Européenne a %(pr:proposé) de remplacer les %(ep:règles de brevetabilité actuelles) (%(q:les méthodes mathématiques, les méthodes commerciales et les programmes d'ordinateurs ne sont pas des inventions brevetables)) par une brevetabilité sans limite à l'américaine, comme cela a été %(pi:pratiqué) ouvertement envers la loi par l'Office Européen du Brevet (OEB) depuis 1998 et plus ou moins officieusement depuis 1986, en acceptant la délivrance de brevets portant sur des méthodes commerciales. En échange d'un transfert formel de compétence à Bruxelles, l' Office de Munich se voit officiellement autorisé à poser les règles de la brevetabilité comme il l'entend. Les conseils en brevets européens sont néanmoins obligés d'user de figures rhétoriques plus importantes que dans le système américain: ils doivent présenter leurs algorithmes comme des " inventions mises en oeuvres par ordinateur" qui présente une " contribution technique".
ejr: L'OEB et sa communauté d'avocats en brevets des grandes groupes, assemblés dans le Conseil Consultatif de l'OEB, ont pendant des années dominé la politique du  brevet non seulement de la Commission Européenne mais aussi de la plupart des gouvernements nationaux, de la plupart des associations d'industriels, du %(dp:Conseil de l'Union Européenne) et de la %(ju:Commission des Affaires Juridiques et du Marché Intérieur du Parlement Européen).  Particulièrement dans le dernier cas oà¹ les députés %(LST) travaillent avec acharnement contre toute limitation de la brevetabilité.
sou: Se sont prononcés contre la brevetabilité du logiciel: le %(cr:Conseil des Régions de l'UE), le %(es:Conseil ÃÆÃ¢â¬Â°conomique et Social de l'UE), le %(fg:Gouvernement Français), la %(di:Chambre du Commerce et de l'Industrie Allemande), la  %(mk:Commission Anti-Monopole Allemande), la %(ui:Commission de la Propriété Intellectuelle du Gouvernment Britannique), le %(fe:Commissariat Général au Plan Français), de nombreuses %(sk:études économiques) ainsi que %(ek:91% des participants à la consultation de l'UE sur le sujet) et %(ep:2000 entreprises et 150.000 signataires d'une pétition soumise au Parlament Européen).
cWi: La %(cu:Commission de la Culture) et la %(it:Commission de l'Industrie) du Parlement Européen ont voté au début 2003 en faveur d'amendements qui excluent plus ou moins la brevetabilité du logiciel.
Wal: La commission des affaires juridiques %(jv:a voté le 17 juin) pour une proposition modifiée, qui, selon le%(et:planning actuel du Parlement Européen), va être présenté en séance plénière pour une décision le %(js:2003-09-22).  En séance plénière, des groupes de 32 députés ou plus peuvent mettre au vote des contre-proposition.
WWW: FFII/Eurolinux organisent %(em:des rencontres mensuelles à l'intérieur de et près du Parlement) pour apporter des débats interdisciplinaires dans un domaine qui a été trop longtemps dominé par les conseils en propriété industrielle.
sWe: La lettre d'Attac au Parlament Européen dénonce les brevets logiciels et la proposition de directive
Wsa: Attac Allemagne a publié une lettre aux députés européens.
sWt: L' étude IBM commandée par UE 2003/02: La Commission doit promovoir le système de brevets par l'éducation


