WIi: Wünsche des FFII an die Berliner Gesetzgeber
ErE: Es wird Zeit für eine Umorientierung der Politik.  Karl Poppers  Welt 3, die Welt der frei weiterverwertbaren Information, emanzipiert sich aus den Fesseln der industriekapitalistischen Produktionsweise.  Dieser Emanzipationskampf stößt auf unfaire Widerstände, die zu beseitigen politisch möglich und wünschenswert ist.
WSW: Warum Freie Software die Welt bewegt
Dma: Die Schlagworte OpenSource und Linux sind in aller Munde.  An der Börse boomen Redhat-Aktien, die Softwarefirmen überschlagen sich mit Ankündigungen zur Unterstützung von Linux und zur Offenlegung von Quelltexten.  Die EU setzt eine Opensource-Studienkommission ein, das Bundeswirtschaftsministerium fördert die Entwicklung der frei verfügbaren Verschlüsselungssoftware GnuPG, der französische Senat berät über ein Gesetz zur Förderung der Quellenoffenheit, die Unesco propagiert die Bedeutung freier Software für die Entwicklung der Dritten Welt, in Japan wird Turbo Linux das meistverkaufte Betriebssystem, die chinesische Regierung bringt eine Linux-Distribution namens %(q:Rote Flagge) heraus während ein %(q:Netzwerkkommunistisches Manifest) Furore macht.
Vzs: Versuchen wir mal aufzuzählen, was all diese Kreise begeistert.
Wtr: Wert/Preis
DWW: Das Freie Unix (GNU/Linux/BSD/...) ist leistungsstark, schnell und stabil.  Man fährt es hoch und es läuft, läuft und läuft, hält (z.B. als Internet-Server) hohen Belastungen stand und kostet nichts.  Gebühren für kommerzielle Systeme und Wartungsarbeiten bei Systemabstürzen sind hingegen sehr hoch und werden oft von Steuergeldern bezahlt.
Sso: Schnittstellenoffenheit
Wen: Während Microsoft gezielte Inkompatibiliät als Hebel einsetzt, um Konkurrenz gegen sein Monopol im Keim zu ersticken, spielt das Freie Unix mit offenen Karten.  Jeder kann es verwenden, wie er will und damit kombinieren, was er will.  Dadurch sind sehr viele kleine innovative Projekte (z.B. Spezialanwendungen bei NASA, Supercomputersystem BeoWulf) erst entstanden, die in der industriekapitalistisch dominierten Infosphäre mangels Masse keinen Platz hatten.  GNU/Linux läuft auf mehr Hardware-Architekturen als irgendein anderes Betriebssystem.
Qee: Quellenoffenheit
Drl: Das Freie Unix ist nicht nur kostenlos sondern wirklich frei:  jeder kann den Quelltext nehmen, daraus lernen, ihn weiterentwickeln und weiterverteilen.  Die Bewohner der Infosphäre haben endlich Luft zum Atmen.  Da es keine Eigentumsschranken gibt, finden die besten Infowerke den Weg den begnadetesten Entwicklern.
Shh: Sicherheit
Ehg: Erst dadurch, dass Tausende vernetzter Tüftler den selben Quelltext anschauen und weiter entwickeln können, wird es sehr wahrscheinlich, dass Fehler und Sicherheitslücken entdeckt werden.  Hacker und System-Einbrecher haben immer wieder vorgeführt, dass geheimniskrämerische Sicherheitskonzepte (%(q:security by obscurity)) nicht funktionieren.  Daher fördert das BMWi zuerst GnuPG.
Srq: Schutz vor dem Großen Bruder
Gnw: Geheimniskrämerische Systeme bieten häufig einigen Wenigen (wie z.B. im Falle von Windows NT dem US-Nachrichtendienst NSA) eine Hintertür zum Lauschen.   Gleichzeitig tönen Regierungen von der Notwendigkeit, Feindesstaaten wie Jugoslawien mit informatischem Krieg (cyberwar) zu überziehen.  Länder wie China und Japan springen auf den Linux-Zug, weil sie auf ihre nationale Sicherheit und auf eine unabhängige nationale Softwareindustrie Wert legen.
Wle: Wert für Bildung und Beschäftigung
DeW: Dank freier Verfügbarkeit der Informationen können junge Menschen ungehindert das Wissen und die Kompetenz erwerben, nach der sie dürsten.  Es entsteht eine große Schicht mündiger Infowelt-Bürger, die sich um Arbeitsplätze keine Sorgen machen müssen.  Denn selbst wenn sie das Gros ihrer Zeit damit verbringen, freie Software zu schreiben, sind sie als EDV-Dienstleister heiß begehrt.
Fen: Faszination eines funktionierenden Kommunismus
DcW: Die Produktivität des quellenoffenen Programmierens stellt einige herkömmliche Weisheiten in Frage.  Es hat sich gezeigt, dass es in der Welt 3 neben der industriekapitalistischen (= informationsfeudalistischen) Produktionsweise eine gemeinschaftlich orientierte Alternative gibt, die vielfach technisch bessere Ergebnisse produziert.  Dabei war dies bisher ein Kommunismus armer Privatleute, die gegen die von den führenden Kreisen propagierte industriekapitalistische Produktionsweise einen schweren Stand hatten und haben.
WvW: Wo die Infosphäre politische Hilfe braucht
Onf: Offizielle Anerkennung des Wertes freier Information
UWg: Unter den als gemeinnützig anerkannten Vereinszwecken fehlt die %(q:Schaffung von frei verfügbaren Informationen).
Dgd: Der FFII e.V. konnte seine Gemeinnützigkeit nur auf %(q:Volksbildung) gründen.  Volksbildung ist jedoch nur eine sekundäre %(e:Wirkung) der %(q:frei verfügbaren informationellen Werte), um die es uns geht.
IWz: Informationelle Werte ind eigentlich Gemeingut par excellence.  Während Bildungsaktivitäten immer nur einer begrenzten Zuhörerzahl zugute kommen, sind Informationen frei von den Begrenzungen.
hWe: Öffentlichen Funktionsträgern (darunter auch gemeinnützigen Vereine) sollten im Gegenteil dazu verpflichtet werden, informationelle Werte zu schaffen und der Allgemeinheit zur freien Verfügung zu stellen.
UdU: Universitäten sind in besonderem Maße der Welt 3 zugewandt.  Wissenschaftler und Studenten sollten das Recht haben, ihre an der Universität erarbeiteten Werke quellenoffen zu zur Verfügung zu stellen.  Bei der Bemessung öffentlicher Zuwendungen an Universitäten sind insbesondere die von diesen Universitäten geschaffenen quellenoffenen informationellen Werte zu würdigen.
Aos: Auch die Bundesbahn ist trotz Privatisierung qua Monopolstellung ein öffentlicher Funktionsträger.  Sie  sollte dazu verpflichtet werden, ihre Fahrplandaten nicht etwa verschlüsselt auf Windows-CD sondern plattformneutral und quellenoffen zur Verfügung zu stellen.
Ona: Ähnlich ist es bei der Telefonauskunft, bei geographischen Daten, Bankenstandards, Patentschriften und Behördeninformationen aller Art.  Wir müssen uns von dem falschen Denken verabschieden, welches informationelle Gemeingüter zu privaten Wirtschaftsgütern umzufunktionieren sucht.  Welt 3 gehört allen.  Ansätze die dies verkennen, sind volkswirtschaftlich unproduktiv.
Oti: Offenheitszertifizierung
Pee: Programmierer stehen häufig vor dem Problem, dass die Hersteller von Computer-Zubehörgeräten keine Informationen über die Schnittstelle ihres Gerätes liefern, so dass man z.B. keinen Linux-Treiber schreiben kann.   Dies ist letztlich der Grund für die Monopol-Situation auf dem Softwaremarkt.
HeB: Hier kann der Gesetzgeber aushelfen, indem ein System zur Zertifizierung der Offenheit von Programmierschnittstellen schafft.  In manchen Bereichen des öffentlichen Lebens, insbesondere bei den Beschaffungsrichtlinien von Behörden und öffentlichen Funktionsträgern, sollten nur noch zertifizierte Geräte den Zuschlag bekommen können.
WWa: Wer ein Gerätes mit nicht-offener Schnittstelle kauft, schließt damit einerseits bestimmte Kreise der Bevölkerung aus und begünstigt andererseits nicht nur den Auftragnehmer sondern auch ein Kartell, das sich gar nicht offiziell um den Zuschlag beworben hat.  Es findet ein verstecktes Geschäft zu Gunsten/Lasten unbeteiligter Dritter statt.   Dies ist unstatthaft.
Eon: Es darf nicht sein, dass ein Bürger proprietäre Software einsetzen muss, um mit Behörden oder öffentlichen Funktionsträgern kommunizieren zu können.
IeW: Im französischen Senat wird seit November über einen Gesetzesantrag dieses Inhalts beraten.  Er wird dort insbesondere von Konservativen und Grünen unterstützt und hat ein durchweg positives Presseecho ausgelöst.
Kam: Keine Patentierbarkeit von Information oder Leben
Dbb: Die Patentorganisationen drängen darauf, nach amerikanischem Vorbild Software patentierbar zu machen.  Sie wollen im Juni dieses Jahres den Punkt %(q:Programme für Datenverarbeitungsanlagen) von der Ausnahmenliste des Europäischen Patentübereinkommens streichen.
SWe: Sie haben keine wissenschaftliche Studie und nicht die Spur einer volkswirtschaftlihcen Folgenabschätzung vorgelegt.  Der wirkliche Grund ist, dass ihre patentinflationäre Rechtspraxis bereits heute im Widerspruch zu der lästigen EPÜ-Ausnahmenliste steht.  Greenpeace hat diese Rechtsbeugung des Europäischen Patentamtes bereits kritisiert und die Bundesjustizministerin zu einer kritischen Stellungnahme bewegt.
DWr: Die Bundesregierung ist als EPÜ-Teilnehmervertreter befugt, dem Europäischen Patentamt die Richtung vorzugeben.  Diese sollte (vereinfacht gesagt) lauten:
Pne: Patentierbar sind Gegenstände, deren %(e:Reproduktion) ein erfinderisches Verfahren erfordert.  Informationen sind kopierbar und fallen daher aus gutem Grund nicht unter das Patentrecht sondern unter das Urheberrecht.  Lebewesen reproduzieren sich selber und unterliegen daher allenfalls der Geburtenkontrolle, nicht aber dem Patentrecht.
DtW: Die Patentierbarkeit von Information erzeugt nicht nur Widersprüche auf begrifflicher Ebene, sondern sie schadet der gesamten Softwarebranche.  Am ehesten können noch Großkonzerne von einem Patentportfolio profitieren.  Aber selbst Oracle und Adobe haben sich gegen Softwarepatente ausgesprochen.  Für freie Software ist die Patentierung von Information tödlich, denn sie bedeutet, dass ein Programm nachträglich unfrei oder gar verboten werden kann, obwohl es ganz auf der eigenständigen schöpferischen Arbeit von Tausenden von wirtschaftlich uneigennützig agierenden Individuen beruht.
DrW: Der FFII e.V. hat einen detaillierten Vorschlag hierzu erarbeitet und möchte gerne zusammen mit seinen Sponsoren am Gesetzgebungsprozess beteiligt werden.

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: phm
# passwd: XXXX
# srcfile: /ul/sig/srv/res/www/ffii/swpat/stidi/tisna/indexde.txt
# feature: swpatdir
# doc: swpberlinit
# txtlang: de
# coding: utf-8
# End:
