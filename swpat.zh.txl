<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Software Patents in Europe

#descr: =E8=BF=91=E5=B9=B4=E6=9D=A5=EF=BC=8C=E6=AC=A7=E6=B4=B2=E4=B8=93=E5=
=88=A9=E5=B1=80=EF=BC=88EPO=EF=BC=89=E5=B7=B2=E7=BB=8F=E8=BF=9D=E8=83=
8C=E7= =8E=B0=E6=9C=89=E6=B3=95=E5=BE=8B=E7=9A=84=E6=9D=A1=E6=96=87=E5
=92=8C=E7=B2= =BE=E7=A5=9E=EF=BC=8C =E6=8E=88=E4=BA=88=E4=BA=86=E8=B6=
85=E8=BF=8730000=E5=AE=97=E4=B8=93=E5=88=
=A9=E7=BB=99=E6=8C=89=E7=85=A7=E9=80=9A=E7=94=A8=E8=AE=A1=E7=AE=97=E8=
AE=BE= =E5=A4=87=E5=A3=B0=E6=98=8E=E7=9A=84=E7=BB=84=E7=BB=87=E5=92=8C
=E8=AE=A1=E7= =AE=97=E6=96=B9=E6=B3=95
=E2=80=94=E2=80=94=E5=8D=B31973=E5=B9=B4=E6=B3=95=E5=BE=8B=E6=89=80=E7
=A7= =B0=E7=9A=84%(q:=E8=AE=A1=E7=AE=97=E6=9C=BA=E7=A8=8B=E5=BA=8F)=E6
=88=96=E4= =BB=8E2000=E5=B9=B4=E8=B5=B7EPO=E5=AE=98=E8=85=94=EF=BC=88N
ewspeak=EF=BC=89 =E6=89=80=E8=B0=93=E7=9A=84%(q:=E8=AE=A1=E7=AE=97=E6=
9C=BA=E5=AE=9E=E7=8E= =B0=E7=9A=84=E5=8F=91=E6=98=8E)=E3=80=82
=E6=AC=A7=E6=B4=B2=E7=9A=84=E4=B8=93=E5=88=A9=E8=BF=90=E5=8A=A8=E6=AD=
A3=E5= =8A=9B=E5=9B=BE=E9=80=9A=E8=BF=87=E8=B5=B7=E8=8D=89=E4=B8=80=E9
=83=A8=E6=96= =B0=E7=9A=84=E6=B3=95=E5=BE=8B=E4=BD=BF=E8=BF=99=E7=A7=8
D=E8=A1=8C=E4=B8=BA= =E5=90=88=E6=B3=95=E5=8C=96=E3=80=82
=E5=B0=BD=E7=AE=A1=E4=B8=93=E5=88=A9=E8=BF=90=E5=8A=A8=E8=BE=93=E6=8E=
89=E4= =BA=862000=E5=B9=B411=E6=9C=88=E5=92=8C2003=E5=B9=B49=E6=9C=88=
E7=9A=84=E4= =B8=BB=E8=A6=81=E6=88=98=E5=BD=B9=EF=BC=8C
=E4=BD=86=E6=AC=A7=E6=B4=B2=E7=9A=84=E7=A8=8B=E5=BA=8F=E5=91=98=E5=92=
8C=E5= =85=AC=E6=B0=91=E4=BB=AC=E4=BB=8D=E6=AD=A3=E9=9D=A2=E4=B8=B4=E9
=87=8D=E5=A4= =A7=E7=9A=84=E5=8D=B1=E9=99=A9=E3=80=82
=E4=BD=A0=E5=8F=AF=E4=BB=8E=E8=BF=99=E9=87=8C=E6=89=BE=E5=88=B0=E5=9F=
BA=E6= =9C=AC=E8=B5=84=E6=96=99=EF=BC=8C=E4=BB=A5=E4=B8=8B=E9=A6=96=E5
=85=88=E6=98= =AF=E6=9C=80=E6=96=B0=E6=B6=88=E6=81=AF=E5=92=8C=E7=AE=8
0=E8=A6=81=E6=A6=82= =E8=BF=B0=E3=80=82

#lae: Why all this fury about software patents?

#dsW: If Haydn had patented %(q:a symphony, characterised by that sound is
produced [ in extended sonata form ]), Mozart would have been in
trouble.

#ltd: Unlike copyright, patents can block independent creations.  Software
patents can render software copyright useless.  One copyrighted work
can be covered by hundreds of patents of which the author doesn't even
know but for whose infringement he and his users can be sued.  Some of
these patents may be impossible to work around, because they are broad
or because they are part of communication standards.

#eas: Evidence from %(es:economic studies) shows that software patents have
lead to a decrease in R&D spending.

#iWd: %(it:Advances in software are advances in abstraction).  While
traditional patents were for concrete and physical %(e:inventions),
software patents cover %(e:ideas).  Instead of patenting a specific
mousetrap, you patent any %(q:means of trapping mammals) or %(ep:means
of trapping data in an emulated environment).  The fact that the
universal logic device called %(q:computer) is used for this does not
constitute a limitation.  %(s:When software is patentable, anything is
patentable).

#tek: In most countries, software has, like mathematics and other abstract
subject matter, been explicitely considered to be outside the scope of
patentable inventions.  However these rules were broken one or another
way.  The patent system has gone out of control.  A closed community
of patent lawyers is creating, breaking and rewriting its own rules
without much supervision from the outside.

#oOv: Current Situation in Europe

#and: What we can do

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: johnhax ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: zh ;
# multlin: t ;
# End: ;

