#!/usr/bin/perl
$debug =1;

#$just_count=1;
# just count's the number of patents which fit to the classes or words
# pipe output to "grep count" ( i'm lazy ;)

# writes the numbers of all patents in, which match the words in 
# slurp_buzzword into slurp_pat
# + IPC-classes in slurp_cat

# i strongly recommend checking the buzzwords on the website before using them!
# using a buzzword which gives You 100000 hits doesn't bring You anywhere,
# unless You want a really complete collection of alllllll patents
# ( examples are system, method )

# You may start this script more than once


require "./init.pl";

# Create a user agent object
use LWP::UserAgent;
$ua = new LWP::UserAgent;
$ua->agent("Mozilla/4.0 (compatible; MSIE 5.5; AOL 5.0; Windows 98)" . $ua->agent);

use Mysql;
$dbh = Mysql->connect("$server_ip", "$database", "$db_login", "$db_password"); 

&do_buzzword_granted();
&do_ipc_granted();
#&do_buzzword_application();


exit;
############################################ the end

sub do_ipc_granted {

	# gets patnos of granted patents, which match a IPC-class

	# $searchword and $searchword_file are used global

	my $end ="";
	do {                   
		
		# only get one line at a time, to be able to start this thingy
		# more than once
		$sql =	"SELECT cat FROM slurp_cat WHERE 
					go = 'y' AND
					type='g'
					LIMIT 1
							";

		print "$sql\n" if ( $debug > 99 );
		print LOG "$sql\n";

		$sth = $dbh->query($sql);

		( $searchword ) = $sth->fetchrow;

		if ( !$searchword ) {
			# no more left 
			$end=1;
		}
		else {

			print "getting granted: $searchword\n";
			print LOG "getting granted: $searchword\n";
			$searchword_file=$searchword;
			$searchword_file=~ s/\//_/o;

			$success="";

			# mark as being in process
			$sql= "UPDATE slurp_cat SET go='doing', slurped_date='$today' WHERE 
			cat=\'$searchword\'
			";
			print "$sql\n";
			$sth3 = $dbh->query("$sql") if ( $debug < 100 );      

			# set initial page 
			#my $req = "http://localhost/catlt.htm";
			my $req = "http://l2.espacenet.com/espacenet/search?CY=ep&LG=en&DB=EPD&LS=1&NA=0&IC=$searchword&PN=EP";

			search_granted( $req, 0, "" );

			# only mark patent in DB as slurped, when we were successful
			if ( $success ) {
				&mark_granted_as_done_cat("done");
				print "successfull! $searchword\n";
				print LOG "successfull! $searchword\n\n\n";
			}
			else {
				print "not successfull! $searchword\n";
				print LOG "not successfull! $searchword\n\n\n";
				&mark_granted_as_done_cat("error");
			}
		}


	} until ( $end == 1 );      
}



sub do_buzzword_granted {

	# gets patnos of granted patents, which match a buzzword

	# $searchword and $searchword_file are used global

	my $end ="";
	do {                   
		
		# only get one line at a time, to be able to start this thingy
		# more than once
		$sql =	"SELECT buzzword FROM slurp_buzzword WHERE 
					go = 'y' AND
					type='g'
					LIMIT 1
							";

		print "$sql\n" if ( $debug > 99 );
		print LOG "$sql\n";

		$sth = $dbh->query($sql);

		( $searchword ) = $sth->fetchrow;

		if ( !$searchword ) {
			# no more left 
			$end=1;
		}
		else {

			print "getting granted: $searchword\n";
			print LOG "getting granted: $searchword\n";
			$searchword_file=$searchword;
			$searchword_file=~ s/\//_/o;

			$success="";

			# mark as being in process
			$sql= "UPDATE slurp_buzzword SET go='doing', slurped_date='$today' WHERE 
			buzzword=\'$searchword\'
			";
			print "$sql\n";
			$sth3 = $dbh->query("$sql") if ( $debug < 100 );      

			# set initial page 
			#my $req = "http://localhost/buzzresult.htm";
			my $req = "http://l2.espacenet.com/espacenet/search?CY=ep&LG=en&DB=EPD&LS=1&NA=0&AB=$searchword&PN=EP";

			search_granted( $req, 0, "" );

			# only mark patent in DB as slurped, when we were successful
			if ( $success ) {
				&mark_granted_as_done_buzz("done");
				print "successfull! $searchword\n";
				print LOG "successfull! $searchword\n\n\n";
			}
			else {
				print "not successfull! $searchword\n";
				print LOG "not successfull! $searchword\n\n\n";
				&mark_granted_as_done_buzz("error");
			}
		}


	} until ( $end == 1 );      
}


sub search_granted {
			
	# this sub gets recursively(?) called by itself because there'll only
	# 20 patents displayed on one page. there's a maximum of 25 result-pages 
	# per search. so if there are more than 500 patents, the search will
	# be divided into year, months, days ( yep, there are days with several
	# hundered patents for a single buzzword ! )
	# in each recursion-level the next decision is made on the number
	# of patents left:
	# < 20  insert all patents in DB, we're done
	# < 500 insert all patents in DB, and go to next page
	# > 500 start new search divided into year, months, days
	#
	# yep, complicated but should be complete, grmlbmlx$%#*~!@patoffice:(
	my ( $req, $level, $date ) = @_;
	my $request=$req;

	my $pat = http_fetch_page($req);
	# now we have the HTML-source in $pat

	if ( $pat eq "error" ) {
		print LOG "##############################\n";
		print LOG "ERROR while trying to fetch $searchword\n";
		print LOG "##############################\n";
	}
	else {
		@all_lines = split(/\n/,$pat);

		my ( $lola );

		my $end="";
		$count="";
		# first find the number of results
		do {
			$lola = shift @all_lines;
			#print "$lola\n";
			if ( $lola =~ /\&NA=(\d+)/o ) {
				$count = $1;
				print "count: $searchword - $count\n" if ( $debug );
				$end =1;
			}
		} until ( $end == 1 || !@all_lines );

		# here comes the big decision-making, how to go on
		if ( $just_count == 1 ) {
			# do nothing
		}
		elsif ( !$count ) {
			# do nothing, surprise surprise
		}
		elsif ( $count <= 20 ) {
			print "cool, just one page, put stuff in DB and leave \n" if ( $debug );
			print LOG "cool, just one page, put stuff in DB and leave \n";
			&put_granted_in_db();
		}
		elsif ( $count <= 500 || $level == 3 ) {
			if ( $level == 3 || $count > 500 ) {
				
				print "unbelievable, more than 500 pat on a single for to one searchword: $searchword, $date \n" if ( $debug );
				print LOG "unbelievable, more than 500 pat on a single for to one searchword: $searchword, $date \n" if ( $debug );
			}
			else {
				print "less than 500 patents put stuff in DB and get the other pages \n" if ( $debug );
				print LOG "less than 500 patents put stuff in DB and get the other pages \n";
			}

			&put_granted_in_db();

			# now loop over the rest of the pages
			$num_pages = int ($count/20)+1 ;
			print "number of pages: $num_pages\n";
			my $page;
			# start with 2nd page bec 1st is already done
			for $page (2..$num_pages ) 
			{ 
				my $this_request = $request;
				$this_request =~ s/&LS=(\d+)//go;
				$this_request .= "&NA=$count&LS=$page";
				my $pat = http_fetch_page($this_request);
				@all_lines = split(/\n/,$pat);
				&put_granted_in_db();
			}
		}
		else {
			# more than 500, we need to split the search into year, month, days

			# loop over the year
			if ( $level == 0 ) {
				my ( $dateloop);
				for $dateloop ( 1986..2000 ) {
					my $this_request .= "$request&PD=$dateloop";
					print "$this_request, 1, $dateloop\n";
					&search_granted( $this_request, 1, $dateloop );
				}
			}
			# loop over the month
			elsif ( $level == 1 ) {
				my ($dateloop);

				for $dateloop ( 1..12 ) {
					my $tmp= sprintf("%02d", $dateloop);  
					my $this_request= "$request$tmp";
					print "$this_request, 2, $dateloop\n";
					&search_granted( $this_request, 2, $dateloop );
				}
			}
			elsif ( $level == 2 ) {
				my ($dateloop);

				for $dateloop ( 1..31 ) {
					my $tmp= sprintf("%02d", $dateloop);  
					my $this_request= "$request$tmp";
					print "$this_request, 3, $dateloop\n";
					&search_granted( $this_request, 3, $dateloop );
				}
			}
			else {
				die "shouldnt happen\n";
			}
		}
	} 
}

sub http_fetch_page {

	my ( $req ) = @_;

	my $error="";
	my $num_tries=0;
	my ( $pat);

	# loop around this max 3 times, in case of an HTTP-error, evilevil internet
	do {
		# makes the actual HTTP-download
		print $req."\n" if ( $debug );
		print LOG "$req\n";
		my $request=$req;

		my $req = new HTTP::Request GET => $req;

		# Pass request to the user agent and get a response back
		my $res = $ua->request($req);

		# Check the outcome of the response
		if ($res->is_success) {
			$error="";
			$pat =  $res->content;
			print "request successfull\n" if ( $debug );

			if ( $debug ) {
				open (PAT,">>$pat_debug_dir/$searchword_file") || die "can't create $pat_debug_dir/$searchword_file";
				print PAT "####################################################\n";
				print PAT "req: $request\n";
				print PAT "####################################################\n";
				print PAT $pat;
				close (PAT);
			}

		}
		else {
			print "HTTP-error: $request\n";
			print LOG "HTTP-Error :$searchword\n";
			$pat = "error";
			$error=1;
			$num_tries++;
		}
	} until ( $error eq "" || $num_tries > 3 );

	$pat;
}

sub put_granted_in_db {

	# put 1-20 patents from a single page into the DB

	foreach $lola ( @all_lines ) {
		if ( $lola =~ / \'(EP\d\d\d\d\d\d\d)\'/o ) {
			print "$1\n";
			$patnolong=$1;
			$patno=$1;
			$patno=~s/^EP//go;

			$sql= "insert into slurp_pat values ('$patno','y','$patnolong',1,'g','$searchword')";
			print "$sql\n" if ( $debug );
			print LOG "$sql\n";
			$sth2 = $dbh->query("$sql") if ( $debug < 100 );    

			$success=1;
		}
	}
}

sub mark_granted_as_done_cat {

	my ($mark)=@_;
	# mark the category as "done"
	# we can't be sure the patents in slurp_pat will really be
	# downloaded, that should be okay for now, TODO someday ...
	# oh, and slurped_date maybe too early then of course ...

	  # don't delete in slurp_cat, just mark as done ...
	  $sql= "UPDATE slurp_cat SET go='$mark', slurped_date='$today' WHERE 
		  cat=\'$searchword\'
		  ";
	  print "$sql\n";
	  print LOG "$sql\n";
	  $sth5 = $dbh->query("$sql") if ( $debug < 100 );      

}
sub mark_granted_as_done_buzz {

	my ($mark)=@_;
	# mark the buzzwords in DB
	# we can't be sure the patents in slurp_pat will really be
	# downloaded, that should be okay for now, TODO someday ...
	# oh, and slurped_date maybe too early then of course ...

	  # don't delete in slurp_buzzword, just mark as done ...
	  $sql= "UPDATE slurp_buzzword SET go='$mark', slurped_date='$today' WHERE 
		  buzzword=\'$searchword\'
		  ";
	  print "$sql\n";
	  print LOG "$sql\n";
	  $sth5 = $dbh->query("$sql") if ( $debug < 100 );      

}
#################################################
#################################################

sub do_buzzword_application {

	# gets patnos of applications patents, which match a buzzword

	my $end ="";
	do {      
		$sql =	"SELECT buzzword FROM slurp_buzzword WHERE 
					go = 'y' AND
					type='a'
					LIMIT 1
							";

		print "$sql\n" if ( $debug > 5 );
		print LOG "$sql\n";

		$sth = $dbh->query($sql);

		( $buzzword ) = $sth->fetchrow;

		if ( !$buzzword ) {
		   # no more left, stop this
			$end =1;                 
		}
		else {
			print "getting applications: $buzzword\n";
			print LOG "getting applications: $buzzword\n";
			$buzzword_file=$buzzword;
			$buzzword_file=~ s/\//_/o;

			#$req = "http://localhost/buzzresult.htm";

			$req = "http://ep.espacenet.com/search97cgi/s97is.dll?Action=FilterSearch&SearchType=3&Filter=e/de/de/dipsfilt.hts&alltext=$buzzword&ResultField[1]=Country&ResultField[2]=EPOQUE_NO&ResultField[3]=P_Title_1&ResultCount=500";

			print $req."\n";
			print LOG "$req\n";

			my $req = new HTTP::Request GET => $req;

			# Pass request to the user agent and get a response back
			my $res = $ua->request($req);

			# Check the outcome of the response
			if ($res->is_success) {
				$pat =  $res->content;
				if ( $debug ) {
					open (PAT,">$pat_debug_dir/$buzzword_file") || die "can't create $pat_debug_dir/$buzzword_file";
					print PAT $pat;
					close (PAT);
				}
				print "got: $buzzword\n";

				@all_lines = split(/\n/,$pat);
				foreach $lola ( @all_lines ) {
					if ( $lola =~ /VdkVgwKey=(EP\d\d\d\d\d\d\dA\d)&/o ) {
						print "$1\n";
						$patnolong=$1;
						$patno=$1;
						$patno=~s/^EP//go;
						$patno=~s/A\d$//go;

						$sql= "INSERT INTO slurp_pat VALUES ('$patno','','$patnolong',1,'a','$buzzword')";
						print "$sql\n" if ( $debug );
						print LOG "$sql\n";
						$sth2 = $dbh->query("$sql") if ( $debug < 100 );    
					}
				}

				# put the buzzword into the tabel "slurp_buzzword this is risky bec
				# we can't be sure the patents in slurp_pat will really be
				# downloaded, that should be okay for now, TODO someday ...
				# oh, and slurped_date maybe too early then of course ...

				  # don't delete in slurp_buzzword, just mark as done ...
				  $sql= "UPDATE slurp_buzzword SET go='done', slurped_date='$today' WHERE 
					  buzzword=\'$buzzword\'
					  ";
				  print "$sql\n";
				  $sth3 = $dbh->query("$sql") if ( $debug < 100 );      
				
			} 
			else {
				print "Bad luck this time $buzzword\n";
				print LOG "Bad luck :$buzzword\n";
			}

		}
	} until ( $end == 1 );   
}
