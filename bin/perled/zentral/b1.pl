#!/usr/bin/perl

# inserts abstracts from HTML-files into the DB

# description and claims are far too big to be handled properly 
# (at least on my k6-3-400, 128mb )

# before You use this, make sure You have all the indices from db_schema.txt
# on Your database ! some things are not checked by this script, but will 
# get right when some indicies are unique.

# uncomment to avoid debug-stuff
# set to 100 to prevent this script to write to the DB
$debug=1;

require "./init.pl";
require "./lib.pl";

#############################################

 
use Mysql;
$dbh = Mysql->connect("$server_ip", "$database", "$db_login", "$db_password");
                    

&insert_stuff();


exit;
####################################### the end

sub insert_stuff {

	$zentral_in ="/var/tmp/von.zentral.abstr2";
	chdir "$zentral_in";
	opendir DIR, ".";
	@archives = grep { /^put.+tgz/o } readdir(DIR);
	closedir DIR;        

	# do only the 1st if debugging
	@archives=$archives[0] if ( $debug > 99 );

	$tmp_dir ="/tmp/pat3";
	
	$err = chdir "$tmp_dir";
	die "cant chdir to $tmp_dir\n" if ( !$err );

	foreach $archive ( @archives ) {        
		system "rm $tmp_dir/gra.*.html";
		$err = system "tar xfz $zentral_in/$archive";
		print "tar xfz $zentral_in/$archive";

		if ( $err ) {
			print "cant untar\n";
		}
		else {

			opendir DIR, ".";
			@files = grep { /^gra.\d+.abstr.html/o } readdir(DIR);
			closedir DIR;        
			&read_file();

			#opendir DIR, ".";
			#@files = grep { /^gra.\d+.claim.html/o } readdir(DIR);
			#closedir DIR;        
			#&read_file();

			#opendir DIR, ".";
			#@files = grep { /^gra.\d+.desc.html/o } readdir(DIR);
			#closedir DIR;        
			#&read_file();
		}
		system "mv $zentral_in/$archive /var/tmp/von.zentral.abstr3" if ( $debug < 100 );
	}
}


sub read_file {

	foreach $file ( @files ) {        
		print "--$file\n";

		#if ( $file=~ /claim/oi ) {
			#$attribute="claims";
		#}
		#elsif ( $file=~ /desc/oi ) {
			#$attribute="descr";
		#}
		#elsif ( $file=~ /abstr/oi ) {
			$attribute="abstract";
		#}
		#else {
			#die "error: $file\n";
		#}

		$file =~ /\.(\d+)\./o;
		$patno=$1;
		if ( !$patno ) {
			die "keine patno: $file \n";
		}

		$patnolong = "EP$patno";

		if ( -s "$tmp_dir/$file" ) {

			open (FILE, "< $file" ) || die "cant open $tmp_dir/$file\n";
			
			if ( $attribute eq "claims" || $attribute eq "descr" ) {
				$text="";
				do {
					$_=<FILE>;
					if ( /<B>(Description)</oi || /<B>(Claims)</oi ) {
						print "found $1\n";

						$dummy=<FILE>;
						$dummy=<FILE>;
						$text=<FILE>;
						$text=~s/<BR>/\n/goi;
						$text=~s/\'/\\\'/goi;
						$text=~s/\"/\\\"/goi;
						$text =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs; 
						$text =~ s/^\W+//o;
						$text =~s/^\n+//gmo;


						print "--$text";
						if ( $text && $attribute && $patno ) {
							&put_in_db( $text, $attribute, $patno );
						}

					}
				} until ( $text ne "" || eof FILE );
			}
			else {
				# abstract

				$pat="";
				while(<FILE>) { 
					$pat .= $_;
				}
				b1( $pat, $patnolong );      
				#put_granted_in_db( $pat, $patnolong );      
			}

			close (FILE);
		}
	}
}


sub put_in_db {
	my ( $text, $attribute, $patno) =@_;
		
	$data=3 if ( $attribute =~ /claim/oi );
	$data=4 if ( $attribute =~ /desc/oi );

		my $sql ="UPDATE pat SET 
						$attribute = '$text',
						data_gra=$data
				WHERE patno='$patno' 
				";                    

	print "$sql\n" if ( $debug );
	print LOG "$sql\n" if ( $debug );
	my $sth2 = $dbh->query($sql) if ( $debug < 100 );  

	#die;
}

