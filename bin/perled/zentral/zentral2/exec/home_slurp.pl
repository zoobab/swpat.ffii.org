#!/usr/bin/perl5

# get data from espacenet

# leave this much kb of quota free
$leave_free_kb=10000;

#$debug=1;

chdir "/usr/home/zentral/ffii/data.tmp";

$|=1;

# run for max 1 day
for ( 1..1444 ) {

	get_stuff();
	sleep 60;
}

sub get_stuff {

	opendir DIR, "../data.in";
	@lists = grep { /^list./o } readdir(DIR);
	closedir DIR;               

	if ( !@lists ) {
		print "- no list\n";
	}
	else {

	foreach $list ( @lists ) {

		print "getting $list\n";
		system "mv ../data.in/$list ../data.in/work.$list";
		open (LIST, "<../data.in/work.$list") || die "cant open ../data.in/work.$list\n";
		while(<LIST>) {
			chop;
			&check_quota();
			( $req, $filename ) = split (/\|/ );
			print "/usr/contrib/bin/lynx -source \"$req\" > $filename \n" if ( $debug );
			system "/usr/contrib/bin/lynx -source \"$req\" > $filename ";
			print "mv $filename ../data.out\n" if ( $debug );
			system "mv $filename ../data.out";
			     if ( $bla++ % 10 == 0 ) { print "$bla ";}     
		}
		system "mv ../data.in/work.$list ../data.in/old.$list";
		print "+ done $list\n";
		
	}
	}
}

sub check_quota {

	# waits until there's enough space left
	do {
		open (QUOTA,"/usr/bin/quota|") || die "cant openm /usr/bin/quota \n";
		$egal=<QUOTA>;
		$egal=<QUOTA>;
		$line=<QUOTA>;
		close (<QUOTA>);
		$line =~ /(\d+)(\D+)(\d+)(\D+)(\d+)/o;
		$now =     $1 ;
		$max =             $5;
		print "now: $now , max: $max" if ( $debug );
		$diff = $max -$now;
		print "--- $diff\n" if ( $debug );
		if ( $diff <= $leave_free_kb ) {
			print "waiting bec of quota\n";
			sleep 60 ;
		}

	} until ( $diff > $leave_free_kb );
}
