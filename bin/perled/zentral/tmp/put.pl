#!/usr/bin/perl5

# put's data home

$debug=1;

$tty= `/usr/bin/tty`;
$tty=~ /(\d+)/;
$tty= "p$1";
$line =`/usr/bin/w -n |/usr/bin/grep \" $tty \"` ;
$line=~ /(\d+\.\d+\.\d+\.\d+)/;
$ip=$1;

#$ip="127.0.0.1";
print "$remote ip : $ip\n";

chdir "/usr/home/zentral/ffii/data";

use Net::FTP;

$|=1;

# run for max 1 day
for ( 1..144 ) {

	put_stuff();
	sleep 600;
}

sub put_stuff {

	opendir DIR, ".";
	@files = grep { /^gra\..+gz/o } readdir(DIR);
	closedir DIR;               

	if ( !@files ) {
		print "no files\n";
	}
	else {

		# set up connection
		$count=0;
		$return="";
		do {
			$return = $ftp = Net::FTP->new("$ip");
			if ( $return ) {
				$return = $ftp->login("up","gibihm1");
				
			}
			if ( !$return ) {
				print "error building up connection, trying again in 10 sec\n";
				sleep 10;
			}
		} until ( $return || $count > 5 );

		# transfer files
		foreach $file ( @files ) {

			$count=0;
			$return="";
			do {
				print "putting $file\n" if ( $debug );
				$return = $ftp->put("$file");
				if ( !$return ) {
					print "error transmitting $file, trying again in 2 sec\n";
					sleep 2;
				}
			} until ( $return || $count > 5 );

			# if transfer was succesfull, delete the file, to save space
			if ( $return ) {
				print "deleting $file\n" if ( $debug );
				unlink "$file";	
			}

			if ( $bla++ % 10 == 0 ) { print ".";}
		} 

		$ftp->quit;
		print "done for now\n";
	}
}
