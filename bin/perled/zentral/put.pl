#!/usr/bin/perl5

# put's data home

#$debug=1;

$tty= `/usr/bin/tty`;
$tty=~ /(\d+)/;
$tty= "p$1";
$line =`/usr/bin/w -l |/usr/bin/grep $tty` ;
print "$line $tty\n" if ( $debug );
( $egal, $egal, $ip ) = split(/ +/, $line );
#$line=~ /\S+\s+\S+\s+(\S+)/;
#$line=~ /(\d+\.\d+\.\d+\.\d+)/;
#$ip=$1;

#$ip="127.0.0.1";
print "+ remote ip : $ip\n";

chdir "/usr/home/zentral/ffii/data.out";

use Net::FTP;

$|=1;

# run for max 1 day
for ( 1..288 ) {

	put_stuff();
	sleep 300;
}

sub put_stuff {

		# set up connection
		$count=0;
		$return="";
		do {
			$return = $ftp = Net::FTP->new("$ip");
			if ( $return ) {
				$return = $ftp->login("up","gibihm1");
				
			}
			if ( !$return ) {
				print "- error building up connection, trying again in 10 sec\n";
				sleep 10;
			}
		} until ( $return || $count > 5 );

		if ( $return ) {

			do {
			$something="";
			# at this point we have a succesfull connection
			print "+ connection !\n";
			chdir "/usr/home/zentral/ffii/data.in";
			$return = $ftp->binary();
			$return = $ftp->get("out/list.gz");
			if (!$return ) {
				print "- nothing to get\n" ;
			}
			else {
				$something=1;
				print "+ got new list\n";
				$now=`date +%s`;
				system "gunzip list.gz";
				system "mv list list.$now";
				print "+ mv list list.$now\n";
				$return = $ftp->delete("out/list.gz");
				if ( !$return ) {
					die "unable to delete out/list\n";
				}
			}
			chdir "/usr/home/zentral/ffii/data.out";

	opendir DIR, ".";
	@files = grep { /^gra\./o } readdir(DIR);
	closedir DIR;               

	if ( !@files ) {
		print "- no files\n";
	}
	else {
		$something=1;
		# 1st tar the stuff
		$now=`date +%s`;
		print "+ /usr/local/bin/gtar --remove-files -c -z -f put.$now.tgz gra.*\n" if ( $debug);
		system "/usr/local/bin/gtar --remove-files -c -z -f put.$now.tgz gra.*";
		@files = "put.$now.tgz";

		# transfer files
		foreach $file ( @files ) {

			$count=0;
			$return="";
			do {
				print "+ putting $file\n";
				$return = $ftp->cwd("in");
				$return = $ftp->binary();
				$return = $ftp->put("$file");
				if ( !$return ) {
					print "- error transmitting $file, trying again in 2 sec\n";
					sleep 2;
				}
				$return = $ftp->cwd("..");
			} until ( $return || $count > 5 );

			# if transfer was succesfull, delete the file, to save space
			if ( $return ) {
				print "+ transfer succed, deleting $file\n" if ( $debug );
				unlink "$file";	
			}

			if ( $bla++ % 10 == 0 ) { print ".";}
		} 

		}
		sleep 60;
	} until ( !$something );
	}
		$ftp->quit;
		print "\n+ done for now\n";
}
