#!/usr/bin/perl 
$debug =1;

# works on the current directory
# prepares patent applications for a CD:
# - changes the names to 8.3 ( patno.PDF )
# - creates a HTML-page with an index

############################################################
if ( !@ARGV ) {
	die "usage: $0 target-directroy\n\n";
}

$dir =$ARGV[0];

mkdir ("$dir",0777);

use Mysql;
$dbh = Mysql->connect("127.0.0.1", "pat", "pat", "suck2678");
                                                                     
# can't use pure "cp", might break on too many files
system "find -maxdepth 2 -name \"essential.EP*.pdf\" -ls -exec mv {} $dir \\; ";

chdir "$dir";

opendir DIR, ".";
%subdirs = grep { /^essential\.EP/ } readdir(DIR);
closedir DIR;   

foreach $lola (sort( %subdirs )) {   

	# check if it's just a symlink
	if ( ! -l "$lola" && $lola ) {

		# check, if it didn't got downloaded right, using "file"
		print "<$lola>\n";
		($dummy, $file) =split (/:/o , `file $lola`);
		if ( $file =~ /pdf/oi ) {
			#print "$file\n";
			$lola =~ /essential.(EP\d\d\d\d\d\d\d..)/;
			$patnolong=$1;
			$lola =~ /essential.EP(\d\d\d\d\d\d\d)/;
			$patno=$1;
			system "mv $lola $patno.pdf";
			$index_page .= &get_description();
		}
		else { 
			print "deleting $lola, bec its no PDF\n";
			unlink "$lola";
		}
	}
}

open (INDEX,"> index.htm" ) || die "cant open index.htm\n";

print INDEX "
<HEAD><TITLE>FFII Software Patent Application Collection CD</TITLE></HEAD>
<HTML>
<BODY>
<H1>FFII Software Patent Application Collection CD</H1>

<P>
All patent applications have been downloaded from <a href=http://ep.espacenet.com>espacenet</a>. They've been included in this collection because their
<a href=http://www.wipo.int/eng/clssfctn/ipc/>IPC-classes</a> and titles
make it very probable, that they're software patents. <p>

The PDF documents only include the pages 1-5, the claim page and the next
two, the drawing page and the next two. To whole applications can be
downloaded by following the link on \"Patent Number Long\". The EPO ist
constantly changing their server-scripts, so those links might get broken
in the future. You still can search manually for the patent numbers 
(without trailing A1, A2 or A3 ) in 
<a href=\"http://ep.espacenet.com/espacenet/ep/en/e_net.htm?search3\">http://ep.espacenet.com</a>. 
<p>

This collection has been compiled by <a href=http://swpat.ffii.org>FFII</a>.<p>

<p>
<TABLE BORDER=1><TR><TH>Prob<TH>PDF<TH>Espacenet<TH>Title<TH>Applicant
$index_page
</TABLE>
</BODY>
</HTML>
";

########################################## the end
	
sub get_description {

	# get relevant stuff from mysql
	$sth = $dbh->query("select 
			prob2,
			patno, 
			patnolong, 
			title_en,
			applicant_name

			from pat 
			where patnolong='$patnolong' ");       

		@line = $sth->fetchrow;


	my $fp ="<TR>";
	$fp .= "<TD>$line[0]</TD>";
	$fp .="<TD><a href=\"$line[1].pdf\">$line[1].pdf</a></TD>\n";
	$fp .="<TD><a href=\"http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/ep/en/viewer.hts&SearchType=3&VdkVgwKey=$line[2]\">$line[2]</a></TD>\n";
	$fp .="<TD>$line[3]</TD>\n";
	$fp .="<TD>$line[4]</TD>\n";

	$fp;
}
