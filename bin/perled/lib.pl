# some subroutines needed in more than one script


sub put_granted_in_db {
	my ($pat, $patnolong ) = @_;      

	$patno = $patnolong;
	$patno =~ s/^EP//go;

	my ( $line, $priority_no, $pub_date, $applicant_name, $applicant_country, $title_en, $title_de, $title_fr, $inventor, $ec_class, $alternate_patno, $abstract );

	@all_lines = split(/\n/,$pat);
	do { 
		$line = shift @all_lines;

		# ignore JS in the beginning
		if ( $line =~ /^function / ) {
			do {
				$line = shift @all_lines;
			} until ( $line =~ /^}/ || $line =~ / }/ );
		}

		$line =~ s/<BR>$//oi;
		$line =~ s/\'/\\\'/go;
		$line =~ s/&#160;//go;

		if ($line =~ /Publication date/o ) {
			$line = shift @all_lines;
			$line =~ s/\'/\\\'/go;
			$line =~ /(\d\d\d\d-\d\d-\d\d)/o;
			$pub_date = $1;
		}
		elsif ( $line =~ /Equivalents/o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          
			$line =~ s/\'/\\\'/go;
			$alternate_patno .= "$line ";
		}
		elsif ( $line =~ /Inventor\(s\)/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$inventor= $1;
			$inventor=~ s/^ +//o;
		}
		elsif ( $line =~ /Patent Number:/o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          
			$line =~ s/\'/\\\'/go;
			$alternate_patno .= "$line ";
		}
		elsif ( $line =~ s/EC Classification://o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          

			# grmblthrowoutjavascript
			$line =~ s/\'//go;
			$line =~ s/Text \+= //go;
			$line =~ s/\+ makeEcla\(//o;
			$line =~ s/\) \+//o;
			$line =~ s/ +/ /go;
			$ec_class .= "$line ";
		}
		elsif ( $line =~ /Priority Number/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$priority_no= $1;
			$priority_no=~ s/^ +//o;
		}
		elsif ( $line =~ /Applicant\(s\)/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$applicant_name= $1;
			$applicant_name=~ s/^ +//o;
			$applicant_name =~ /\((\S\S)\)/o;
			$applicant_country = $1;
		}
		elsif ( $line =~ /IPC Classification:/o ) {
			$line = shift @all_lines;
			$line =~ s/&#160;//go;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			@classes = split (/ ; /, $1);
			foreach $lola ( @classes ) {
				$lola=~ s/:/\//go;
				$lola=~ s/ +//go;
				$sql= "INSERT INTO pat_ipc VALUES (\'$patno\', \'$lola\')";
				print "$sql\n" if ( $debug );
				print LOG "$sql\n" if ( $debug );
				$sth3 = $dbh->query("$sql") if ( $debug < 100 );     
			}
		}
		elsif ( $line =~ /<TITLE>/o ) {
			$line =~ /--(.*)---/o;
			$title_en=$1;
			$title_en=~s/ +/ /go;
			$title_en=~s/^ +//go;
		}
		elsif ( $line =~ />Abstract</o && $line =~ /<TD/o ) {

			# next 2 lines are useless
			$line = shift @all_lines;
			$line = shift @all_lines;

			$abstract = shift @all_lines;

			# remove HTML-tags
			$abstract =~ s/<\/=//gs;
			$abstract =~ s/\(<//gs;
			$abstract =~ s/\(>//gs;
			$abstract =~ s/ > //gs;
			$abstract =~ s/ < //gs;
			$abstract =~ s/-><-//gs;
			$abstract =~ s/\)<-//gs;
			$abstract =~ s/\)>-//gs;
			$abstract =~ s/<#.*>//gs;
			$abstract =~ s/<#s>//gs;
			$abstract =~ s/D>E//gs;
			$abstract =~ s/D<e//gs;
			$abstract =~ s/D<e//gs;
			$abstract =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;

			# ( if the tags get more complicated someday, better solutions
			# can be found on http://www.perl.com/pub/doc/manual/html/pod/perlfaq9.html#How_do_I_remove_HTML_from_a_stri
			# but it's ok for now )

			$abstract=~s/^ +//go;
			$abstract =~ s/\'/\\\'/go;

		}
	} until ( $line =~ /<\/HTML>/oi || @all_lines==0 );

	# so much about data-quality, 1619 out of 24512 :(
	#if ( length($title_de) < 2 ) {
		#$title_de=$title_en;
	#}
	#if ( length($title_fr) < 2 ) {
		#$title_fr=$title_en;
	#}

		my $data_gra=2;
		my $type='g';

		if ( !$abstract ) {
			$abstract = "No Abstract available from Espacenet";
		}

		$alternate_patno =~ s/ +/ /go;

		$sql= "UPDATE pat  SET ";

		$sql .=" title_en         = '$title_en', " if ( $title_en );
		$sql .=" pub_date         = '$pub_date', " if ( $pub_date );
		$sql .=" dbdate            = '$today', " if ( $today );
		$sql .=" priority_no      = '$priority_no', " if ( $priority_no );
		$sql .=" applicant_name   = '$applicant_name', " if ( $applicant_name );
		$sql .=" applicant_country= '$applicant_country', " if ( $applicant_country );
		$sql .=" patnolong        = '$patnolong', " if ( $patnolong );
		$sql .=" type             = '$type', " if ( $type );
		$sql .=" data_gra         = $data_gra, " if ( $data_gra );
		$sql .=" abstract         = '$abstract', " if ( $abstract );
		$sql .=" alternate_patno  = '$alternate_patno', " if ( $alternate_patno );
		$sql .=" inventor         = '$inventor', " if ( $inventor );
		$sql .=" ec_class         = '$ec_class', " if ( $ec_class );

		# remove trainling coma
		chop ($sql); chop ($sql);

		$sql .=" WHERE patno='$patno'";
		

	$sql=~s/&amp;/&/go;
	$sql=~s/&quot;/\"/go;
	$sql=~s/&nbsp;/ /go;
	$sql =~ s/&#160;//go;

	print "$sql\n" if ( $debug );
	print LOG "$sql\n" if ( $debug );

	$sth4 = $dbh->query("$sql") if ( $debug < 100 );     
}

sub b1 {
	my ($pat, $patnolong ) = @_;      

	$patno = $patnolong;
	$patno =~ s/^EP//go;

	my ( $line  );

	@all_lines = split(/\n/,$pat);

	$type='a';
	do { 
		$line = shift @all_lines;

		if ($line =~ />B1<\/A/ ) {
			$type ='g';
			@all_lines=0;
		}
	} until ( $line =~ /<\/HTML>/oi || @all_lines==0 );

		$sql= "UPDATE pat  SET type = '$type' WHERE patno='$patno'";
		
	print "$sql\n" if ( $debug );
	print LOG "$sql\n" if ( $debug );

	$sth4 = $dbh->query("$sql") if ( $debug < 100 );     
}


1;
