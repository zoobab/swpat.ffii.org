#!/usr/bin/perl

# changes the column "prob" to the best guess, that the patent is software-
# related. checks the category-name, applicant-name and title of the patent
# for buzzwords
# only handles granted patents


use Mysql;

 $dbh = Mysql->connect("127.0.0.1", "pat", "pat", "suck2678");     

# 1st set default to 1 for all patents
#$sql= "update pat set prob=1";
#$sth = $dbh->query("$sql");     


&title();
&category_name();
&category();
#&applicant();

$sql= "update pat set prob2=(1-(1/(1+prob))) ";
$sth = $dbh->query("$sql");     



exit;

sub title {
 
# go for title_en

		open (FILE, "< title.buzz") || "die can't open title.buzz\n";
	
		while (<FILE>) {
			chop;
			if (!/^#/   ) {
				my ( $buzzword, $factor ) = split (/;/);

				$sql= "UPDATE pat SET prob=(prob * $factor) WHERE 
							type='g' AND
							( LOWER(title_en) REGEXP \'$buzzword\' OR
							  LOWER(abstract) REGEXP \'$buzzword\' )
						";
				print "$sql\n";
				$sth = $dbh->query("$sql");     

				#$sql= "UPDATE pat SET prob=(prob * $factor ) WHERE 
							#LOWER(abstract) REGEXP \'$buzzword\'
							#AND type='g'     
						#";
				#print "$sql\n";
				#$sth = $dbh->query("$sql");     
			}
		}
}


sub applicant {

		open (FILE, "< applicant_name.buzz") || "die can't open applicant_name.buzz\n";
	
		while (<FILE>) {
			chop;
			if (!/^#/   ) {
				my ( $buzzword, $factor ) = split (/;/);

				$sql= "update pat set prob=(prob * $factor) where 
							lower(applicant_name) regexp \'$buzzword\' or
							lower(applicant2) regexp \'$buzzword\' 
						";
				print "$sql\n";
				$sth = $dbh->query("$sql");     
			}
		}
}

sub category {
 

		open (FILE, "< ipc.buzz") || "die can't open ipc.buzz\n";
	
		while (<FILE>) {
			chop;
			if (!/^#/   ) {
				my ( $ipclass, $factor ) = split (/;/);

				# categories are more complicated bec there're in a diff table
				# so 1st select, then update

				$sth = $dbh->query("select patno from pat_ipc where lower(ipclass) regexp \'$ipclass\' ");  

				 while ( $patno = $sth->fetchrow ) {
					 print "$patno\n";

					$sql= "update pat set prob=(prob * $factor) where 
							patno=\'$patno\'
							";
					print "$sql\n";
					$sth2 = $dbh->query("$sql");     
				 }
			}
		}
}

sub category_name {
 

		open (FILE, "< catname.buzz") || "die can't open catname.buzz\n";
	
		while (<FILE>) {
			chop;
			if (!/^#/   ) {
				my ( $buzzword, $factor ) = split (/;/);

				# categories are more complicated bec there're in a diff table
				# so 1st select, then update

				$sth = $dbh->query("select patno from cat,pat_ipc where ipclass=cat and lower(catname) regexp \'$buzzword\' ");  

				 while ( $patno = $sth->fetchrow ) {
					 print "$patno\n";

					$sql= "update pat set prob=(prob * $factor) where 
							patno=\'$patno\'
							";
					print "$sql\n";
					$sth2 = $dbh->query("$sql");     
				 }
			}
		}
}
