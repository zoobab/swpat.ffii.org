#!/usr/bin/perl

# show stats about the pat-db on the screen in regular intervals


$reload=120;
 
require "./init.pl";               

use Mysql;
$dbh = Mysql->connect("$server_ip", "$database", "$db_login", "$db_password");

while ( 42==42 ) {

	system "clear";
	print "pat-db statistics, every $reload secs\n\n";

	$sth = $dbh->query("select count(*) from slurp_pat where go='doing' ");
	$num = $sth->fetchrow;
	if ( $num > 0 ) {
		print "------------------- slurp_pat -----------\n";
		print "in work at the moment                   : $num\n";
		$sth = $dbh->query("select count(*) from slurp_pat ");
		$num = $sth->fetchrow;
		print "total number in slurp_pat               : $num\n";

		$sth = $dbh->query("select count(*) from slurp_pat where go='y' and type='g'");
		$num = $sth->fetchrow;
		print "granted ready in slurp_pat              : $num\n";

		#$sth = $dbh->query("select count(*) from slurp_pat where go='y' and type='a'");
		#$num = $sth->fetchrow;
		#print "apps ready in slurp_pat                 : $num\n";
	}


	$sth = $dbh->query("select count(*) from slurp_buzzword where go='doing' ");
	$num = $sth->fetchrow;

	if ( $num > 0 ) {
		print "\n------------------- slurp_buzzword -----------\n";
		print "in work at the moment                   : $num\n";
		$sth = $dbh->query("select count(*) from slurp_buzzword ");
		$num = $sth->fetchrow;
		print "total number in slurp_buzzword:         : $num\n";
		$sth = $dbh->query("select count(*) from slurp_buzzword where go='y' ");
		$num = $sth->fetchrow;
		print "ready in slurp_buzzword:                : $num\n";
	}

	print "\n\n";
	system "df";

	sleep $reload;

}
