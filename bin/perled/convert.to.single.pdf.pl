#!/usr/bin/perl 
#$debug =1;

# combines several pdfs into one
# can use open source tools or pdcat from www.glance.ch, which results in 
# smaller files ( 50% smaller for PDF, 25% smaller than PS )
# an evaluation-version of pdcat can be downloaded from
# http://www.glance.ch/GSE/PDF/Linux/

# start in one direrctory or f.e. with the following shell-command:
# find -type d -exec convert.to.single.pdf.pl {} pdcat \;

if ( @ARGV < 2 ) {
	die "\nusage: $0 patent-number pdcat|oss [gzip|bzip]\n\n";
}

$dir=$ARGV[0];
$utils=$ARGV[1];
$compression = $ARGV[2];
############################################################


use Mysql;
 
$dbh = Mysql->connect("127.0.0.1", "pat", "pat", "suck2678");
                                                                     
($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat("$dir/$dir.0001.pdf");   

$dir_access_time = localtime($mtime);

chdir "$dir";

$patnolong = $dir;
$patnolong =~ s/^\.\///;
print "doing patno: $patnolong \n";

&resolve_symlinks();

$frontpage = &create_frontpage();
open (FRONTPAGE, "| txt2pdf -r $patnolong.title.pdf.tmp") || die "can't start txt2pdf or create $patnolong.title.pdf\n";
print FRONTPAGE $fp;
close (FRONTPAGE);

system "pdwebl -q $patnolong.title.pdf.tmp $patnolong.title.pdf \"http://swpat.ffii.org=http://swpat.ffii.org\" \"http://ep.espacenet.com=http://ep.espacenet.com/espacenet/ep/en/e_net.htm?search3\" \"$patnolong=http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/ep/en/viewer.hts&SearchType=3&VdkVgwKey=$patnolong\" \"http://petition.eurolinux.org=http://petition.eurolinux.org\" ";



# now read all the files

opendir DIR, ".";
%subdirs = grep { !/^\./ && /\.pdf/oi &&!/important/oi &&!/whole/oi && !/all/oi && !/essential/ && !/title/ } readdir(DIR);
closedir DIR;   

# decide which tools to use
if ( $utils =~ /oss/i ) {
	print "using xpdf, epsmerge and pstill\n";
	&oss();
}
elsif ( $utils =~ /pdcat/i ) {
	print "using pdcat\n";
	&pdcat();
}
else  {
	die "wrong \"utils\"\n";
}


#####################################################
# end of utils-specitific part

if ( $compression =~ /b/ ) {
	system "bzip2 -f essential.$patno.pdf";
	print "created: $dir/essential.$patno.pdf.bz2\n";

	if ( $utils =~ /oss/i ) {
		system "bzip2 -f essential.$patno.ps";
		print "created: $dir/essential.$patno.ps.bz2\n";
	}
}
elsif ( $compression =~ /g/ ) {
	if ( $utils =~ /oss/i ) {
		system "gzip -9 essential.$patno.ps";
		print "created: $dir/essential.$patno.ps.gz\n";
	}
	system "gzip -9 essential.$patno.pdf";
	print "created: $dir/essential.$patno.pdf.gz\n";
}
else  {
	if ( $utils =~ /oss/i ) {
		print "created: $dir/essential.$patno.ps\n";
	}
	print "created: $dir/essential.$patno.pdf\n";
}

unlink "$patnolong.title.pdf.tmp";

####################################################################

sub pdcat {

	# -r overwrites existing output-file
	# -i adds bookmark-text
	#$pdcat_cmd ="pdcat -r ";
	$pdcat_cmd .="pdcat -r -I \"About this document\" $patnolong.title.pdf ";

	foreach $lola (sort( %subdirs )) {   

		# check if it's just a symlink
		if ( ! -l "$lola" && $lola ) {

			# check, if it didn't got downloaded right, using "file"
			#print "<$lola>\n";
			($dummy, $file) =split (/:/o , `file $lola`);
			if ( $file =~ /pdf/oi ) {
				#print "<$lola>\n";
				$lola =~ /(\d\d\d\d)\.pdf/oi ;
				$pagenum= $1;
				if ( $pdf_bookmark{ $lola } ) {
					$pdcat_cmd .= " -I \"Page $pagenum $pdf_bookmark{ $lola }\" $lola ";
				}
				elsif ( $pagenum == 1 ) {
					$pdcat_cmd .= " -I \"Page $pagenum Biblio\" $lola ";
				}
				elsif ( $pagenum == 2 ) {
					$pdcat_cmd .= " -I \"Page $pagenum Description\" $lola ";
				}
				else {
					$pdcat_cmd .= " -I \"Page $pagenum\" $lola ";
				}
				( $patno, $pageno, $ext) = split(/\./o , $lola);
				$all=1;
			}
		}
	}

	if ( !$all ) {
		unlink "$patnolong.title.pdf.tmp";
		die "no PDFs found, exiting\n";
	}

	$pdcat_cmd .=" essential.$patno.pdf";
	#$pdcat_cmd .="-I \"About this document\" $patnolong.title.pdf essential.$patno.pdf";

	system "$pdcat_cmd";
	print "$pdcat_cmd\n" if ( $debug );

}

sub oss {
	foreach $lola (sort( %subdirs )) {   

		# check if it's just a symlink
		if ( ! -l "$lola" && $lola ) {

			# check, if it didn't got downloaded right, using "file"
			#print "<$lola>\n";
			($dummy, $file) =split (/:/o , `file $lola`);
			if ( $file =~ /pdf/oi ) {
				( $patno, $pageno, $ext) = split(/\./o , $lola);
				system "pdftops -eps $lola $patno.$pageno.ps ";
				$all .= "$patno.$pageno.ps ";
			}
		}
	}

	if ( !$all ) {
		unlink "$patnolong.title.pdf.tmp";
		die "no PDFs found, exiting\n";
	}

	# concat all PS-files in one
	system ("epsmerge -x 1 -y 1 -ps -o essential.$patno.ps ".$all);
	system "rm $all";

	# convert PS to PDF
	system "pstill -c -t -cgt -o essential.$patno.pdf essential.$patno.ps";
}


sub resolve_symlinks {
	if ( $utils eq "pdcat" ) {
		# built PDF-bookmarks for claims
		opendir DIR, ".";
		%claims = grep { !/^\./ && /\.claim_page\./oi } readdir(DIR);
		closedir DIR;   

		foreach $lola ( %claims ) {   
			if ( $lola ) {
				print "<$lola>\n" if ( $debug );
				$lola =~ /\.(\d)\.pdf/oi;
				$num = $1;
				$points_to = readlink "$lola";
				$pdf_bookmark{ $points_to } ="Claims $num";
			}
		}

		# built PDF-bookmarks for drawings
		opendir DIR, ".";
		%draw = grep { !/^\./ && /\.drawing_page\./oi } readdir(DIR);
		closedir DIR;   

		foreach $lola ( %draw ) {   
			if ( $lola ) {
				print "<$lola>\n" if ( $debug );
				$lola =~ /\.(\d)\.pdf/oi;
				$num = $1;
				$points_to = readlink "$lola";
				$pdf_bookmark{ $points_to } ="Drawing $num";
			}
		}

	}
}

sub create_frontpage {

	# get relevant stuff from mysql
	$sth = $dbh->query("select 
			patno, 
			patnolong, 
			e_app_no,
			app_date,
			pub_date,
			applicant_name,
			title_en,
			title_de,
			title_fr,
			prob2

			from pat 
			where patnolong='$patnolong' ");       

		@line = $sth->fetchrow;


	$fp ="
FFII Software Patent Application Collection

This patent application has been downloaded from the European Patent Office
on $dir_access_time.
It has been included in this collection because its probability of being 
a software patent has been rated: $line[9]

The PDF Document only includes the pages 1-5, the claim page and the next
two, the drawing page and the next two. To whole application can be 
downloaded from http://ep.espacenet.com or follow the link on \"Patent
Number Long\" ( May get broken because of changes in the EPO pages ).



";
	$fp .="Patent Number     : $line[0]\n";
	$fp .="Patent Number Long: $line[1]\n";
	$fp .="Application Number: $line[2]\n";
	$fp .="Application Date  : $line[3]\n";
	$fp .="Publication Date  : $line[4]\n";
	$fp .="Applicant Name    : $line[5]\n";

	# wrap lines after 80 chars for titles
	$line[6] =~ s/(.{80})/$1\n/go;    
	$line[7] =~ s/(.{80})/$1\n/go;    
	$line[8] =~ s/(.{80})/$1\n/go;    
	$fp .="\nTitle English: \n$line[6]\n\n";
	$fp .="Title German: \n$line[7]\n\n";
	$fp .="Title French: \n$line[8]\n\n";

$fp .= "
For more information about software patents and the FFII, please visit 
http://swpat.ffii.org

Please sign the petition against european software patents on 
http://petition.eurolinux.org

If You have any technical questions, please contact arnim\@ffii.org
";

	$fp;
}
