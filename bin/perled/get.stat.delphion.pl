#!/usr/bin/perl

# creates a small html-table with some country-statistics about swpat from
# delphion-patent-db

# make verbose
$debug =1;

@countries = ('US', 'JP',  'DE', 'FR', 'GB', 'EP');

$query="((program) or (programm) or (programme) or (software) or (computer)) ";
#$query="((program) or (software) or (computer) or (internet) or  (code) or (encoding) or (execution) or (network) or (terminal) or (virtual) or (web)) ";
#$query="((program) or (software) or (computer) or (internet) or  (code) or (data) or (encoding) or (execution) or (information) or (network) or (terminal) or (virtual) or (web)) ";

################################################## end of changable part


if ( !@ARGV ) { die "\nusage: $0 outputfile\n\nChange the countries and the searchwords in the $0\n\n"; }

$output_file = $ARGV[0];

open (HTML, "> $output_file" ) || die "can't create $output_file\n";


print HTML "<TABLE BORDER=1><TR><TH>Country</TH><TH>Number</TH><TH>Percent</TH></TR>\n";

( $of ) = &get_num ( $query );

print "Total $of \n" if ( $debug );

for $country ( @countries ) {

	$query_country = "( $query and (($country) <in> PR)) ";
	print "$query_country\n" if ( $debug );

	( $num ) = &get_num ( $query_country );
	$of =1 if ( !$of );
	$percent = $num / $of * 100 ;
	$percent = sprintf ("%.2f", $percent );
	print HTML "<TR><TD>$country</TD><TD>$num</TD><TD>$percent</TD></TR>\n";
	print "<TR><TD>$country</TD><TD>$num</TD><TD>$percent</TD></TR>\n";
	$total_num += $num;
	$total_percent += $percent;
}
$other_num = $of - $total_num;
$other_per = 100 - $total_percent;
$other_per = sprintf ("%.2f", $other_per );
print HTML "<TR><TD>Other</TD><TD>$other_num</TD><TD>$other_per</TD></TR>\n";
print HTML "</TABLE>\n";
close (HTML);

sub get_num {

	my ( $query ) = @_;


	use HTML::Form;
	use LWP;
	$ua = LWP::UserAgent->new;                


	  $query =~ s /</&lt;/go;
	  $query =~ s />/&gt;/go;

	$html_document="
	  <FORM METHOD=POST ACTION=\"/cgi-bin/patsearch\">
	  <INPUT TYPE=\"HIDDEN\" NAME=\"-l\" VALUE=refine>
	  <INPUT TYPE=\"HIDDEN\" NAME=\"-m\" VALUE=1>
	  <INPUT TYPE=\"HIDDEN\" NAME=\"-c\" VALUE=\"epa\">
	  <INPUT TYPE=\"HIDDEN\" NAME=\"-i\" VALUE=\"VDKVGWKEY TITLE SCORE\">
	  <INPUT TYPE=\"HIDDEN\" NAME=\"-o\" VALUE=3>

	  <INPUT TYPE=\"IMAGE\" SRC=\"/art/m_searchbutton.gif\"
	   NAME=\"Search\" WIDTH=\"64\" HEIGHT=\"22\" BORDER=\"0\"
	   ALT=\"Search\" ALIGN=\"LEFT\">

	  <INPUT NAME=\"GENERAL\" VALUE=\"$query\" SIZE=\"60\">
	  </FORM>
	  ";


	$form = HTML::Form->parse($html_document, "http://www.delphion.com");

	# generate the request out of the HTML-form, i just love this module !
	$req = $form->click;


	$htmlpage= $ua->request($req)->as_string;  


	@all_lines = split(/\n/,$htmlpage);       


	foreach $lola ( @all_lines ) {        
		print "$lola\n" if ( $debug > 10 );

		$lola =~ s/,//go;
		if ( $lola =~ /(\d+).+patents.+matched/oi  ) {            
			print "$lola\n" if ( $debug );
			$matched=$1;
		}

	}
	print "matched: $matched \n" if ( $debug );
	( $matched );
}
