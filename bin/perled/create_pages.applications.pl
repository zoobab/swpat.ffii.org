#!/usr/bin/perl

# print HTML-file from patents

$dir ='pages/perled';
chdir $dir;
print "creating in $dir\n";

use Mysql;

$dbh = Mysql->connect("127.0.0.1", "pat", "pat", "suck2678");     

$sth = $dbh->query("select count(*) from pat");
$total_number = $sth->fetchrow;


$make = $ARGV[0] ;
	
if ( $make eq "stat" ) { 
	&country_stat();
}
elsif ( $make eq "test" ) { 
	$global_page_list{ "en" } .="<a href=bla>en blub</a><br>\n";
	$global_page_list{ "de" } .="<a href=bla>de blub</a><br>\n";
	$global_page_list{ "fr" } .="<a href=bla>fr blub</a><br>\n";
	&make_homepage();
}
elsif ( $make eq "test2" ) { 
	&country_stat();
	&top_page(10);
	&top_page(20);
	&top_page(30);
	&make_homepage();
}
else {
	&country_stat();
	&top_page(100);
	&top_page(1000);
	&top_page(10000);
	#&top_page(9999999);
	&make_homepage();
}

###################### the end ...

sub top_page {
	my ( $top ) = @_;
	print "creating top-$top\n";
	&top_page2($top, "en");
	&top_page2($top, "de");
	&top_page2($top, "fr");
}

sub top_page2 {
	my ( $top, $lang ) = @_;
	&top_page3($top, $lang, "tab");
	&top_page3($top, $lang, "pre");

}

sub top_page3 {
	my ( $top, $lang, $table_or_pre ) = @_;

	if ( $top < 999999 ) {
		$topstring = "Top$top";
	}
	else {
		$topstring = "Complete";
	}

	if ( $lang =~ /en/oi ) {
		if ( $top < 999999 ) {
			$topstring = "Top$top";
		}
		else {
			$topstring = "Complete";
		}
		&save_page("$topstring Software Patent Probability ($table_or_pre)","swpat.$table_or_pre.$lang.top$top.html","select patnolong, prob2, title_$lang, applicant_name from pat order by prob desc limit $top",$table_or_pre,$lang, $topstring);
	}
	elsif ( $lang =~ /de/oi ) {
		if ( $top < 999999 ) {
			$topstring = "Top$top";
		}
		else {
			$topstring = "Komplett";
		}
		&save_page("$topstring Software Patent Wahrscheinlichkeit ($table_or_pre)","swpat.$table_or_pre.$lang.top$top.html","select patnolong, prob2, title_$lang, applicant_name from pat order by prob desc limit $top",$table_or_pre,$lang, $topstring);
	}
	elsif ( $lang =~ /fr/oi ) {
		if ( $top > 0 ) {
			$topstring = "Top$top";
		}
		else {
# TODOFR
			$topstring = "Complete";
		}
# TODOFR
		&save_page("$topstring Software Patent Probability ($table_or_pre)","swpat.$table_or_pre.$lang.top$top.html","select patnolong, prob2, title_$lang, applicant_name from pat order by prob desc limit $top",$table_or_pre,$lang, $topstring);
	}
}


#########################################
	
sub make_homepage {
	my ($lang)=en;
	print "creating homepages\n";
	open (FILE, "> index.$lang.html" ) || die "can't open index.$lang.html\n";
	header("Software Patent Crawler");

	print FILE "<a href=index.de.html><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a> <a href=index.fr.html><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";

	print FILE "<h3>Patent Lists, sorted by Software Patent Probability</h3>\n";
	print FILE $global_page_list{ $lang };
	&footer($lang);
	close (FILE);

	my $lang=de;
	open (FILE, "> index.$lang.html" ) || die "can't open index.$lang.html\n";
	header("Software Patent Crawler");
	print FILE "<a href=index.en.html><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> <a href=index.fr.html><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";

	# include mission-file
	open (EXP,"< ../mission.$lang.html") || die "can't open misson.$lang.html\n";
	while(<EXP>) { print FILE; }
	close (EXP);

	print FILE "<h3>Patent Listen, sortiert nach Software Patent Wahrscheinlichkeit</h3>\n";
	print FILE $global_page_list{ $lang };
	&footer($lang);
	close (FILE);

	my $lang=fr;
	open (FILE, "> index.$lang.html" ) || die "can't open index.$lang.html\n";

# TODOFR
	&header("Software Patent Crawler");
	print FILE "<a href=index.en.html><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> <a href=index.de.html><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a><hr><p>\n";

	print FILE "<h3>Patent Lists, sorted by Software Patent Probability</h3>\n";
	print FILE $global_page_list{ $lang };

	&footer($lang);
	close (FILE);
}

sub save_page {
	my ( $title, $filename , $sql, $table_or_pre, $lang, $topstring ) = @_;

	$global_page_list{ $lang } .="<a href=$filename>$title</a><br>\n";

	$sth = $dbh->query($sql);

	open (FILE,"> $filename") || die "can't create $filename\n";
	
	print FILE "<HEAD>\n";

	# replace "ep." with Your country-code
	#print FILE "<BASE HREF=\"http://ep.espacenet.com/search97cgi/\">\n";


	print FILE "<TITLE>$title</TITLE></HEAD><HTML><BODY BGCOLOR=#FFFFFF>\n";

	print FILE "<h2>$title</h2> \n";

	&top_header( $lang, $filename, $table_or_pre, $topstring );

	if ( $table_or_pre =~ /pre/oi ) {
		print FILE "<pre>\n";
		if ( $lang =~/en/oi ) {
			print FILE "Patent No. -- SwPat-Prob. -- Title -- Applicant\n";
		}
		elsif ( $lang =~/de/oi ) {
			print FILE "Patent Nr. -- SwPat Wahrsch. -- Titel -- Eigent&uuml;mer\n";
		}
		elsif ( $lang =~/fr/oi ) {
			# TODOFR
			print FILE "Patent No. -- SwPat-Prob -- Title -- Applicant\n";
		}
	}
	else {
		if ( $lang =~/en/oi ) {
			print FILE "<table border=1><th>Patent No.</th><th>SwPat-Prob.</th><th>Title</th><th>Applicant</th>\n";
		}
		elsif ( $lang =~/de/oi ) {
			print FILE "<table border=1><th>Patent Nr.</th><th>SwPat Wahrsch.</th><th>Titel</th><th>Eigent&uuml;mer</th>\n";
		}
		elsif ( $lang =~/fr/oi ) {
			# TODOFR
			print FILE "<table border=1><th>Patent No.</th><th>SwPat-Prob</th><th>Title</th><th>Applicant</th>\n";
		}
	}

	if ( $table_or_pre =~ /pre/oi ) {
		while ( @line = $sth->fetchrow ) {
			print FILE "<a target=\"_blank\" href=http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/de/de/viewer.hts&SearchType=3&VdkVgwKey=$line[0]>$line[0]</a> $line[1] $line[2] --- $line[3]\n";
		}
	}
	else {
		while ( @line = $sth->fetchrow ) {
			print FILE "<tr><td><a target=\"_blank\" href=http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/de/de/viewer.hts&SearchType=3&VdkVgwKey=$line[0]>$line[0]</a></td><td>$line[1]</td></td><td>$line[2]</td><td>$line[3]</td>\n";
		}
	}


	if ( $table_or_pre =~ /pre/oi ) {
		print FILE "	</pre>\n";
	}
	else {
		print FILE "</table>\n";
	}

	&footer($lang);

	close (FILE);

}

sub top_header {
	my ($lang, $filename, $table_or_pre, $topstring)=@_;

	if ( $lang =~ /en/ ) {
		$defile = $filename;
		$frfile = $filename;
		$defile =~ s/\.en\./\.de\./o;
		$frfile =~ s/\.en\./\.fr\./o;

		print FILE "$topstring out of $total_number potential software patents<p>\n";
		print FILE "<a href=index.en.html>Home</a> | \n";
		if ( $table_or_pre =~ /pre/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.pre\./\.tab\./oi;
			#TODOFR
			print FILE "<a href=$other_file>HTML-Table</a> | \n";
		}
		elsif ( $table_or_pre =~ /tab/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.tab\./\.pre\./oi;
			#TODOFR
			print FILE "<a href=$other_file>&lt;PRE&gt;</a> | \n";
		}

	#TODOFR
		print FILE "<a href=$defile><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a> &nbsp; <a href=$frfile><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";
	}
	elsif ( $lang =~ /de/ ) {
		$enfile = $filename;
		$frfile = $filename;
		$enfile =~ s/\.de\./\.en\./o;
		$frfile =~ s/\.de\./\.fr\./o;

		print FILE "$topstring aus insgesamt $total_number potentiellen Software Patenten<p>\n";
		print FILE "<a href=index.de.html>Startseite</a> | \n";

		if ( $table_or_pre =~ /pre/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.pre\./\.tab\./oi;
			#TODOFR
			print FILE "<a href=$other_file>HTML-Tabelle</a> | \n";
		}
		elsif ( $table_or_pre =~ /tab/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.tab\./\.pre\./oi;
			#TODOFR
			print FILE "<a href=$other_file>&lt;PRE&gt;</a> | \n";
		}
	#TODOFR
		print FILE "<a href=$enfile><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> &nbsp; <a href=$frfile><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";
	}
	elsif ( $lang =~ /fr/ ) {
		$enfile = $filename;
		$defile = $filename;
		$enfile =~ s/\.fr\./\.en\./o;
		$defile =~ s/\.fr\./\.de\./o;

		print FILE "$topstring out of $total_number potential software patents<p>\n";
		print FILE "<a href=index.fr.html>Home</a> | \n";
		if ( $table_or_pre =~ /pre/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.pre\./\.tab\./oi;
			#TODOFR
			print FILE "<a href=$other_file>HTML-Table</a> | \n";
		}
		elsif ( $table_or_pre =~ /tab/oi ) {
			$other_file = $filename;
			$other_file =~ s/\.tab\./\.pre\./oi;
			#TODOFR
			print FILE "<a href=$other_file>&lt;PRE&gt;</a> | \n";
		}
	#TODOFR
		print FILE "<a href=$enfile><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> &nbsp; <a href=$defile><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a><hr><p>\n";
	}
}

sub footer {
	my ( $lang)=@_;
	print FILE "<p><div align=center><hr>\n";
	print FILE "<a href=http://petition.eurolinux.org><img src=\"../patent_banner.gif\"></a>\n";
	if ( $lang =~/en/oi ) {
		print FILE "<p>For more information about Software Patents see <a href=http://swpat.ffii.org>swapt.ffii.org</a>.\n";
		print FILE "Please send feedback to <a href=\"mailto:arnim\@ffii.org\">arnim\@ffii.org</a>\n";
	}
	elsif ( $lang =~/de/oi ) {
		print FILE "<p>Weitere Informationen zu Software Patenten finden Sie auf <a href=http://swpat.ffii.org>swapt.ffii.org</a>.\n";
		print FILE "Feedback bitte an <a href=\"mailto:arnim\@ffii.org\">arnim\@ffii.org</a>\n";
	}
	elsif ( $lang =~/fr/oi ) {
		# TODOFR
		print FILE "<p>For more information about Software Patents see <a href=http://swpat.ffii.org>swapt.ffii.org</a>.\n";
		print FILE "Please send feedback to <a href=\"mailto:arnim\@ffii.org\">arnim\@ffii.org</a>\n";
	}

	print FILE "</div><p>\n";
}

sub country_stat {
	print "creating country statistics\n";

$stat{"0.9"}=&calc_stat(0.9);
$stat{"0.7"}=&calc_stat(0.7);
$stat{"0.5"}=&calc_stat(0.5);
$stat{"0"}=&calc_stat(0);

	my $lang=en;
	open (FILE, "> country_stat.$lang.html" ) || die "can't open country_stat.$lang.html\n";
	$global_page_list{ $lang } .="<a href=country_stat.$lang.html>Country Statistics</a><br>\n";
	print FILE "<HEAD>\n";
	print FILE "<TITLE>Country Statistics Software Patent Probabilities</TITLE></HEAD><HTML><BODY BGCOLOR=#FFFFFF>\n";
	print FILE "<h2>Country Statistics Software Patent Probabilities</h2> \n";
	print FILE "<a href=index.en.html>Home</a> | \n";
	print FILE "<a href=country_stat.de.html><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a> &nbsp; <a href=country_stat.fr.html><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";

	&country_stat2(0.9, $lang);
	&country_stat2(0.7, $lang);
	&country_stat2(0.5, $lang);
	&country_stat2(0, $lang);

	close (FILE);
########################################################
	my $lang=de;
	open (FILE, "> country_stat.$lang.html" ) || die "can't open country_stat.$lang.html\n";
	$global_page_list{ $lang } .="<a href=country_stat.$lang.html>L&auml;nder Statistiken</a><br>\n";
	print FILE "<HEAD>\n";
	print FILE "<TITLE>L&auml;nder Statistiken Software Patent Wahrscheinlichkeiten</TITLE></HEAD><HTML><BODY BGCOLOR=#FFFFFF>\n";
	print FILE "<h2>L&auml;nder Statistiken Software Patent Wahrscheinlichkeiten</h2> \n";
	print FILE "<a href=index.de.html>Startseite</a> | \n";
	print FILE "<a href=country_stat.en.html><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> &nbsp; <a href=country_stat.fr.html><img height=18 width=25 src=\"../fr.png\" border=0 alt=francais></a><hr><p>\n";

	&country_stat2(0.9, $lang);
	&country_stat2(0.7, $lang);
	&country_stat2(0.5, $lang);
	&country_stat2(0, $lang);

	close (FILE);
#################################################
	my $lang=fr;
	open (FILE, "> country_stat.$lang.html" ) || die "can't open country_stat.$lang.html\n";
	$global_page_list{ $lang } .="<a href=country_stat.$lang.html>Country Statistics</a><br>\n";
	print FILE "<HEAD>\n";
	print FILE "<TITLE>Country Statistics Software Patent Probabilities</TITLE></HEAD><HTML><BODY BGCOLOR=#FFFFFF>\n";
	print FILE "<h2>Country Statistics Software Patent Probabilities</h2> \n";
	#TODOFR
	print FILE "<a href=index.fr.html>Home</a> | \n";
	print FILE "<a href=country_stat.en.html><img height=18 width=25 src=\"../en.png\" border=0 alt=English></a> &nbsp; <a href=country_stat.de.html><img height=18 width=25 src=\"../de.png\" alt=Deutsch border=0></a><hr><p>\n";

	&country_stat2(0.9, $lang);
	&country_stat2(0.7, $lang);
	&country_stat2(0.5, $lang);
	&country_stat2(0, $lang);

	close (FILE);
}


sub country_stat2 {
	my ( $prob, $lang ) =@_;
	
	if ( $lang =~ /en/oi ) {
		print FILE "<h2>Country Statistics for Probability greater than $prob</h2>\n";
		print FILE "<table border=1 caption=\"Country Statistics for Probability greater than $prob\"><th>Total Number</th><th>Percent</th><th>Country Code</th>\n";
		print_stat($prob);
		print FILE "</table><p>\n";
	}
	elsif ( $lang =~ /de/oi ) {
		print FILE "<h2>L&auml;nder Statistiken f&uuml;r Wahrscheinlichkeiten gr&ouml;&szlig;er $prob</h2>\n";
		print FILE "<table border=1 caption=\"L&auml;nder Statistiken f&uuml;r Wahrscheinlichkeiten gr&ouml;&szlig;er $prob\"><th>Anzahl</th><th>Prozent</th><th>L&auml;nder Code</th>\n";
		print_stat($prob);
		print FILE "</table><p>\n";
	}
	elsif ( $lang =~ /fr/oi ) {
#TODOFR
		print FILE "<h2>Country Statistics for Probability greater than $prob</h2>\n";
		print FILE "<table border=1 caption=\"Country Statistics for Probability greater than $prob\"><th>Total Number</th><th>Percent</th><th>Country Code</th>\n";
		print_stat($prob);
		print FILE "</table><p>\n";
	}


}

# hihi :)
sub print_stat {
	my ($prob)=@_;
	print FILE $stat{"$prob"};
}

sub calc_stat {
	my ( $prob ) = @_;
	my ($result, $sum, $sumperc);
	$sth = $dbh->query("select count(*) from pat where prob2 > $prob");
	$total = $sth->fetchrow;

	$sth = $dbh->query("select count(*) as num, applicant_country from pat where prob2 > $prob group by applicant_country order by num desc");
	while ( ($num, $country) = $sth->fetchrow ) {
		$percent = int(100*( $num/$total*100))/100;
		if ( $percent > 1 ) {
			$result .= "<tr><td>$num</td><td>$percent</td><td>$country</td></tr>\n";
		}
		else {
			$sum += $num;
			$sumperc += $percent;
		}
	}
	$result .= "<tr><td>$sum</td><td>$sumperc</td><td> Rest ( < 1% ) </td></tr>\n";
	$result;
}

sub header {
	my ( $title)=@_;
	print FILE "<HEAD><TITLE>$title</TITLE>\n";
	print FILE "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n";
	print FILE " <meta  name=\"description\" content=\"FFII Software Patent Crawler\"> \n";
	print FILE "<meta  name=\"keywords\" content=\"patents, patent, logiciel libre, free software, patents, brevets, crawler, list, collection, sammlung, business\">\n";
	print FILE "</HEAD>";
	print FILE "<HTML><BODY BGCOLOR=#FFFFFF>\n";
	print FILE "<div align=center>\n";
	print FILE "<h1>under construction !!! </h1>\n";
	print FILE "<table border=0 width=\"100%\"><tr>\n";
	print FILE "<td align=left><a href=\"http://swpat.ffii.org\"><img src=\"../swpatux.png\" border=0></a></td> \n";
	print FILE "<td align=center><h1>$title</h1></td>\n";
	print FILE "<td align=right><a href=\"http://www.ffii.org\"><img src=\"../ffiirj.png\" border=0></a></td>\n";
	print FILE "</tr>\n";
	print FILE "</table>\n";
	print FILE "<div align=left>\n";
}
