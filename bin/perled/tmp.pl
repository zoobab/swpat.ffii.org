#!/usr/bin/perl

# before You use this, make sure You have all the indices from db_schema.txt
# on Your database ! some things are not checked by this script, but will 
# get right when some indicies are unique.

# downloads all patents from "slurp_pat" and saves 
# them in the table "pat" and the PDFs in $pat_pdf_dir ( see init.pl )

# the espacent-server is slow, so if You might want to start more than one
# instance of this script ( something between 3-6 for one ISDN-line )

# it CAN'T be started, as long as new patno's get inserted in slurp_pat !!!
# ( bec it won't properly handle double entries at the moment ! )


# uncomment to avoid debug-stuff
# set to 100 to prevent this script to write to the DB
#$debug=1;

require "./init.pl";
require "./lib.pl";

#############################################

use HTML::Form;
use LWP;    
#use LWP::UserAgent;

# Create a user agent object
$ua = new LWP::UserAgent;
# hehe :)
$ua->agent("Mozilla/4.0 (compatible; MSIE 5.5; AOL 5.0; Windows 98)" . $ua->agent);

 
use Mysql;
$dbh = Mysql->connect("$server_ip", "$database", "$db_login", "$db_password");
                    

#&delete_useless_data();

&get_granted();

#&get_all_app_short();
#&get_all_app_essential();

exit;
####################################### the end

sub get_granted {

	# pick granted from slurp_pat one by one, unless there are no more left
	# then download all the data until "slurp_pat.data = pat.data_gra"
	# only use patents, which have go='y'

	# all variables ending with _slurp come from the db-table slurp_pat
	# all variables ending with _pat come from the db-table pat
	
	my $end ="";
	do {

		&reopen_db();

		my $dont_get_it="";

		my $sql=	"SELECT patno, patnolong, data FROM slurp_pat WHERE 
					go = 'y' AND 
					type='g' AND
					patno > 1000000 AND
					data IS NOT NULL
					LIMIT 1
					";

		print "$sql\n" if ( $debug > 10 );
		print LOG "$sql\n" if ( $debug );
		$sth = $dbh->query($sql);
	 
		($patno_slurp, $patnolong_slurp, $data_slurp ) = $sth->fetchrow;
		print "$patno_slurp, $patnolong_slurp, $data_slurp\n" if ( $debug );
		print LOG "$patno_slurp, $patnolong_slurp, $data_slurp\n" if ( $debug );

		if ( !$patno_slurp ) {
			# no more left, stop this
			$end =1;
		}
		else {
			 
			print "checking: $patno_slurp long: $patnolong_slurp\n";             
			print LOG "checking: $patno_slurp long: $patnolong_slurp\n";             

			# mark in slurp_pat as being in process
			my $sql ="UPDATE slurp_pat SET go='doing' WHERE patno='$patno_slurp'";
			my $sth2 = $dbh->query($sql) if ($debug < 100 );

			my $sql=	"SELECT patno, patnolong, type, data_gra FROM pat WHERE 
							patno='$patno_slurp'
						";

			print LOG "$sql\n" if ( $debug );
			$sth2 = $dbh->query($sql);
		 
			($patno_pat, $patnolong_pat, $type_pat, $data_pat ) = $sth2->fetchrow;

			if ( $patno_pat ) {
				# this patent is already in DB

				if ( $type_pat eq "a" ) {
					# application which got granted in the meanwhile
					# ( or was in both databases of the EPO )

					# change in table pat
					my $sql ="UPDATE pat SET type='g', data_gra=0 WHERE patno='$patno_pat'";
					my $sth2 = $dbh->query($sql) if ($debug < 100 );

					$type_pat='g';
					$data_pat=0;

				}
				elsif ( $data_slurp <= $data_pat ) {
					# already more information in "pat" than wanted from 
					# "slurp_pat"

					my $sql ="UPDATE slurp_pat SET go='alrdy' WHERE patno='$patno_pat'";
					my $sth2 = $dbh->query($sql) if ($debug < 100 );
					$dont_get_it=1;
				}
			}
			else {
				# not yet in DB
				print "not in DB, getting: $patno_slurp long: $patnolong_slurp\n"; 
				print LOG "not in DB, getting: $patno_slurp long: $patnolong_slurp\n";             
				# create a row in DB
				my $sql ="INSERT INTO pat (patno, patnolong, type, data_gra ) VALUES ( '$patno_slurp', '$patnolong_slurp', 'g', 0 ) ";
				my $sth2 = $dbh->query($sql) if ($debug < 100 );
				$data_pat=0;

			}

			if ( $data_pat eq "" ) {
				# something wrong in DB
				print "row with unset data_gra: $patno, $patnolong\n";
				print LOG "row with unset data_gra: $patno, $patnolong\n";
				$dont_get_it=1;
			}
			
			if ( !$dont_get_it ) {
				# at this point, we can be sure, we don't have it already,
				# and there is an entry in the DB on 
				# the patno, so we can use "UPDATE" in the future

				# now go and get what is needed
				my $error = &check_data_granted( $patno_slurp, $patnolong_slurp, $data_slurp, $data_pat );

				# if there was an error, try once more
				if ( $error ) {
					$error = &check_data_granted( $patno_slurp, $patnolong_slurp, $data_slurp, $data_pat );
				}

				# if there's still something wrong, get annoyed
				if ( $error ) {
					print "error getting: $patno_slurp , $patnolong_slurp\n";
					my $sql ="UPDATE slurp_pat SET go='error' WHERE patno='$patno_slurp' ";                    
					my $sth2 = $dbh->query($sql) if ($debug < 100 );  
				}
				else {
					my $sql ="UPDATE slurp_pat SET go='done' WHERE patno='$patno_slurp' ";
					$sth2 = $dbh->query($sql) if ($debug < 100 );
				}
			}
		}
	} until ( $end == 1 );
}

sub check_data_granted {

	# checks what data is already, whats is wanted, and gets the difference

	my ( $patno, $patnolong, $data_wanted, $data_there ) = @_;

	my $error="";

	# build patnolong if we didn't get it
	if ( !$patnolong ) {
		$patnolong = "EP$patno";
	}

	# now check what we need to get
	my ( $lola, $sub );
	for $lola ( 0..1 ) {
		if ( $data_wanted > $lola && $data_there < $lola + 1 ) {
			# jump to sub get_data_granted_1, get_data_granted_2, ...
			$sub = $lola;
			$sub = "get_data_granted_".++$sub;
			$error= &$sub( $patno, $patnolong );
		}
	}
	$error;
	
}

sub get_data_granted_1 {

	# get the nav-page

	my ( $patno, $patnolong )= @_;
	my $error="";

	#$req ="http://localhost/x";
	#$req ="http://localhost/navbar.granted.htm";

	$req="http://l2.espacenet.com/dips/viewnav?PN=$patnolong&CY=ep&LG=en&DB=EPD";

	print $req."\n" if ( $debug );
	print LOG "$req\n";

	my $req = new HTTP::Request GET => $req;

	# Pass request to the user agent and get a response back
	my $res = $ua->request($req);

	my $suc="";
	my $try=0;
	do {
		my $res = $ua->request($req);
		$suc = $res->is_success;
		$try++;
	} until ( $suc || $try > 2 );

	# Check the outcome of the response
	if ($suc) {
		print "got: $patno\n";
		print LOG "got: $patno\n";
		$pat =  $res->content;

		if ( $debug ) {
			# save in file, create subfolders bec ext2 gets slowwww with lots of 
			# files :(

			$dir2=substr($patno,0,3);
			mkdir ("$pat_txt_dir/$dir2",0777);

			open (PAT,">$pat_txt_dir/$dir2/$patno.txt") || die "cant create $pat_txt_dir/$dir2/$patno.txt";
			print PAT $pat;
			close (PAT);
		}
		
		# extract everything from the HTML-page
		( $abstr_req, $claim_req, $descr_req, $page1_req, $draw_req ) = &get_espace_req($pat );

		# now the http-requests for all further pages are known
		
		my $sql ="UPDATE pat SET 
					data_gra  = 1,
					abstr_req = '$abstr_req',
					claim_req = '$claim_req',
					descr_req = '$descr_req',
					page1_req = '$page1_req',
					draw_req  = '$draw_req',
					dbdate    = '$today'
						
					WHERE patno='$patno' 
					";                    

		print "$sql\n" if ( $debug );
		print LOG "$sql\n" if ( $debug );
		my $sth2 = $dbh->query($sql) if ( $debug < 100 );  
	} 
	else {
		print "HTTP-error: $patno\n";
		print LOG "HTTP-error: $patno\n";
		$error=1;
	}
	$error;
}



sub get_data_granted_2 {

	# get the short info including abstract

	my ( $patno, $patnolong )= @_;
	my $error="";


	my $sql=	"SELECT abstr_req FROM pat WHERE patno='$patno' ";

	print "$sql\n" if ( $debug );
	print LOG "$sql\n" if ( $debug );
	$sth2 = $dbh->query($sql);
 
	my ($req ) = $sth2->fetchrow;

	print $req."\n" if ( $debug );
	print LOG "$req\n";

	my $req = new HTTP::Request GET => $req;

	# Pass request to the user agent and get a response back
	my $res = $ua->request($req);

	my $suc="";
	my $try=0;
	do {
		my $res = $ua->request($req);
		$suc = $res->is_success;
		$try++;
	} until ( $suc || $try > 2 );

	# Check the outcome of the response
	if ($suc) {
		print "got: $patno\n";
		print LOG "got: $patno\n";
		$pat =  $res->content;

		if ( $debug ) {
			# save in file, create subfolders bec ext2 gets slowwww with lots of 
			# files :(

			$dir2=substr($patno,0,3);
			mkdir ("$pat_txt_dir/$dir2",0777);

			open (PAT,">$pat_txt_dir/$dir2/$patno.abstr.txt") || die "cant create $pat_txt_dir/$dir2/$patno.txt";
			print PAT $pat;
			close (PAT);
		}
		
		put_granted_in_db( $pat, $patnolong );
		
	} 
	else {
		print "HTTP-error: $patno\n";
		print LOG "HTTP-error: $patno\n";
		$error=1;
	}
	$error;
}

sub get_espace_req {
	# extract the http-request for subpages from the navigation-bar-page
	my ( $page )= @_;

	my ( $line );
	@all_lines = split(/\n/,$page);  
	foreach $line ( @all_lines ) {
		print "$line" if ( $debug > 100 );

		# wow, 2,8kb uncommented javascript in every page :)  
		if ( $line =~ /^\/\*/o ) {
			do {
				$line = shift @all_lines;
			} until ( $line =~ /^*\//o );
		}


		# abstract / biblio
		if ( $line =~ /value=\"Biblio\"/o ) {
			my $form = HTML::Form->parse($line, "http://l2.espacenet.com");  
			$tmp = $form->click;
			$abstr_req = $tmp->HTTP::Request::uri();
			undef $tmp;
			print "$abstr_req\n";
		}
		elsif ( $line =~ /value=\"Claims\"/o ) {
			my $form = HTML::Form->parse($line, "http://l2.espacenet.com");  
			$tmp = $form->click;
			$claim_req = $tmp->HTTP::Request::uri();
			undef $tmp;
			print "$claim_req\n";
		}
		elsif ( $line =~ /value=\"Desc\"/o ) {
			my $form = HTML::Form->parse($line, "http://l2.espacenet.com");  
			$tmp = $form->click;
			$descr_req = $tmp->HTTP::Request::uri();
			undef $tmp;
			print "$descr_req\n";
		}
		#elsif ( $line =~ /value=\"Page 1\"/o ) {
			#my $form = HTML::Form->parse($line, "http://l2.espacenet.com");  
			#$tmp = $form->click;
			#$page1_req = $tmp->HTTP::Request::uri();
			#undef $tmp;
			#print "$page1_req\n";
		#}
		elsif ( $line =~ /value=\"Drawing\"/o ) {
			my $form = HTML::Form->parse($line, "http://l2.espacenet.com");  
			$tmp = $form->click;
			$draw_req = $tmp->HTTP::Request::uri();
			undef $tmp;
			print "$draw_req\n";
		}

	}
	( $abstr_req, $claim_req, $descr_req, $page1_req, $draw_req );
}


######################################################################
## no more stuff related especially to "granted" below this line
######################################################################

sub get_all_app_essential {

	my $end ="";
	do {              

		# gets the short content of all applications in slurp_pat with type='a'
		# only one at a time, so more than one process can be startet
		my $sql=	"SELECT patno, patnolong FROM slurp_pat WHERE 
					go = 'y' AND 
					type='a' AND
					data_app=2
					LIMIT 1
					";

		print LOG "$sql\n" if ( $debug );
		$sth = $dbh->query($sql);
	 
		($patno, $patnolong ) = $sth->fetchrow;
		 
		if ( !$patno ) {
			# no more left, stop this
			$end =1;
		}                  
		else {
			print "getting essential-data for application $patno, $patnolong\n";
			print LOG "getting essential-data for application $patno, $patnolong\n";

			my $sql ="UPDATE slurp_pat SET go = 'doing' WHERE patno=\'$patno\' ";                    
			my $sth2 = $dbh->query($sql) if ( $debug < 100 );  

			# check if the "short" data is already there, if not, get it
			my $sql="SELECT patno FROM pat WHERE patno=\'$patno\' ";
			print "$sql\n" if ( $debug );
			print LOG "$sql\n";

			my $sth = $dbh->query($sql);

			if ( !$sth->fetchrow ) {
				print "need to get short-data first\n";
				print LOG "need to get short-data first\n";
				( $patno, $patnolong ) = &get_app_short( $patno, $patnolong );
			}
			# if there was an error, try once more
			if ( $error_get_app_short ) {
				print "error, trying short-data again\n";
				print LOG "error, trying short-data again\n";
				( $patno, $patnolong ) = &get_app_short( $patno, $patnolong );
			}

			# only go ahead, if we got short-data
			if ( $error_get_app_short ) {
				print "#############################################\n";
				print "error getting the short-data $patno, $patnolong\n";
				print "#############################################\n";
				print LOG "error getting the short-data $patno, $patnolong\n";
				my $sql ="UPDATE slurp_pat SET go = 'error' WHERE patno=\'$patno\' ";                    
				my $sth2 = $dbh->query($sql) if ( $debug < 100 );  
			}
			else {
				print "short-data there, go on ... \n";
				print LOG "short-data there, go on ... \n";

				&get_app_essential( $patno, $patnolong );

				# try again if something went wrong
				if ( $error_get_app_essetial ) {
					print "error, trying essential-data again\n";
					print LOG "error, trying essential-data again\n";
					&get_app_essential( $patno, $patnolong );
				}

				if ( $error_get_app_essetial ) {
					print "#############################################\n";
					print "error getting the essential-data $patno, $patnolong\n";
					print "#############################################\n";
					print LOG "error getting the essential-data $patno, $patnolong\n";
					my $sql ="UPDATE slurp_pat SET go = 'error' WHERE patno=\'$patno\' ";                    
					my $sth2 = $dbh->query($sql) if ( $debug < 100 );  
				}
				else {
					print "successfully got essential for $patno, $patnolong\n";
					print LOG "successfully got essential for $patno, $patnolong\n";
					my $sql ="UPDATE slurp_pat SET go = 'done' WHERE patno=\'$patno\' ";                    
					my $sth2 = $dbh->query($sql) if ( $debug < 100 );  

					$sql ="UPDATE pat SET data = 2 WHERE patno=\'$patno\' ";                    
					$sth2 = $dbh->query($sql) if ( $debug < 100 );  
				}
			}
		}
	die;
	} until ( $end == 1);
}

sub get_app_essential {

	# gets one single application, essential info 
	# ( claim-page + following 2 pages , drawing-page + following 2 pages, 
	#   first five pages )  

	local ( $patno, $patnolong ) = @_;
	print "################################################\n";
	print "getting essential-info for application $patno, $patnolong \n"; 

	$error_get_app_essential="";


	# should be there already, but just in case ...
	if ( !$patnolong ) {
		$patnolong = get_patno_long( $patno );
	}

	chdir "$pat_pdf_dir" || die "cant chdir to $pat_pdf_dir\n";

	# Create a user agent object
	use LWP::UserAgent;
	$ua = new LWP::UserAgent;
	$ua->agent("Mozilla/4.0 (compatible; MSIE 5.5; AOL 5.0; Windows 98)" . $ua->agent);         

	$req="http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&VdkVgwKey=$patnolong&ViewTemplate=e/gb/en/Viewdnav.hts&page=0001";
	$request=$req;      

	print "$request\n" if ( $debug );

	my $req = new HTTP::Request GET => $req;  
	my $res = $ua->request($req);
	                                    

	# Check the outcome of the response
	if ($res->is_success) {
		print "got nav-page for : $patno\n";
		print LOG "got nav-page for : $patno\n";

		$get_patno= $patno;          
		$pat =  $res->content;

		if ( $debug > 5 ) {
			open (PAT,">$pat_debug_dir/$patno.txt") || die "cant create $pat_log_dir/$patno.txt";
			print "writing $pat_debug_dir/$patno.txt\n";
			print PAT $pat;
			close (PAT);
		}

		$claim_page="";
		$drawing_page="";
		
		my ( $lola);
		my @all_lines = split(/\n/,$pat);
		foreach $lola ( @all_lines ) {
			#print "$lola\n";
			if ( $lola =~ /CLAIMS/o ) {
				if ( $lola =~ /name=\"page\" value=\"(\d\d\d\d)\"/oi ) {
					$claim_page =$1;
				}
			}
			elsif ( $lola =~ /DRAWING/o ) {
				if ( $lola =~ /name=\"page\" value=\"(\d\d\d\d)\"/oi ) {
					$drawing_page =$1;
				}
			}                                       
			elsif ( $lola =~ /parent.viewer.location/o ) {
				if ( $lola =~ /websim10.exe\?(\w+)\"/oi ) {
					$get_patno_whole =$1;
					#print "$get_patno_whole\n";
					( $get_patno, $pageno ) = split (/_/, $get_patno_whole );

					# try it anyway if there's no patno in page
					if ( !$get_patno ) {
						$get_patno = $patno;
					}
				}
			}                    
		}

		print LOG "claim: $claim_page draw: $drawing_page\n";
		print "claim: $claim_page draw: $drawing_page\n";               

		
	} 
	else {
		print "Bad luck this time $request\n";
		print LOG "Bad luck :$request\n";
	}

	# i go on, even if something went wrong with the nav-page, we might
	# still get pages 1-5


	undef %slurp;
	$slurp{ "0001" } =1;
	$slurp{ "0002" } =1;
	$slurp{ "0003" } =1;
	$slurp{ "0004" } =1;
	$slurp{ "0005" } =1;
                                  
	if ( $claim_page ) {
		$weiter=$claim_page;
		$slurp{ "$weiter" } = 1;
		$weiter++;
		$slurp{ "$weiter" } = 1;
		$weiter++;
		$slurp{ "$weiter" } = 1;
	}

	if ( $drawing_page ) {
		$weiter=$drawing_page;
		$slurp{ "$weiter" } =1;
		$weiter++;
		$slurp{ "$weiter" } =1;
		$weiter++;
		$slurp{ "$weiter" } =1;
	}                                           

	# create own directory for each patent     
	mkdir ("$patnolong",0777);
	chdir "$patnolong";

	$success=0;
	$error=0;         

	my ($file);
	
	for $file ( keys %slurp ) {

		if ( -e "$patnolong.$file.pdf" ) {
			print "already there $patnolong.$file.pdf\n";
			print LOG "already there $patnolong.$file.pdf\n";
			$success=1;
		}
		else {
			$req="http://ep.espacenet.com/search97cgi/websim10.exe?".$get_patno."_$file";
			$request=$req;

			print $req."\n" if ( $debug );
			print LOG "$req\n";

			my $req = new HTTP::Request GET => $req;

			# Pass request to the user agent and get a response back
			my $res = $ua->request($req);

			# Check the outcome of the response                    
			if ($res->is_success) {
				$pat =  $res->content;
				if ( $pat =~ / Error Message /oi ) {
					$error =1;
					$error_pages_dont_exist = 1;
					print "Error requesting: $request \n\n";
					print "( not neccessarily bad, because some page numbers are just guessed from \nclaim-page +1, +2 or drawings-page +1, +2 )\n";
					print LOG "Error requesting: $request \n\n";
					print LOG "( not neccessarily bad, because some page numbers are just guessed from \nclaim-page +1, +2 or drawings-page +1, +2 )\n";

					# write the PDF
					open (PAT,">$patnolong.$file.error.html") || die "can't create $patnolong.$file.pdf";
					print PAT $pat;
					close (PAT);

				}                              
				else {
					$success=1;
					open (PAT,">$patnolong.$file.pdf") || die "can't create $patnolong.$file.pdf";
					print PAT $pat;
					close (PAT);
				}

			} 
			else {
				$error =1;
				$error_http_error = 1;
				print "HTTP-Error: $patnolong"."_"."$file\n";
				print LOG "HTTP-Error :$patnolong\n";
			}
		}                                
	}

	# now mark the drawing- and claims-pages with symlinks 
	# in the filesystem
	 
	my $lola;
	for $lola (1..3) {
		if ( -e "$patnolong.$claim_page.pdf" ) {
			system "ln -s $patnolong.$claim_page.pdf $patnolong.claim_page.$lola.pdf";
		}
		$claim_page++;
	}

	for $lola (1..3) {
		if ( -e "$patnolong.$drawing_page.pdf" ) {
			system "ln -s $patnolong.$drawing_page.pdf $patnolong.drawing_page.$lola.pdf";
		}
		$drawing_page++;
	}

	# everything is now here, or not, lets see what we got ...

	# if we got at least one page ...
	if ( $success == 1 ) {
		print "got at least one page for: $patnolong\n";
		print LOG "got at least one page for: $patnolong\n";

		system "echo e > these.are.just.the.essential.pages";

		my $sql ="UPDATE slurp_pat SET go = 'done' WHERE patno=\'$patno\' ";
		$sth2 = $dbh->query($sql) if ( $debug < 100 );
	}

	if ( $error == 1 ) {
		print "at least one error for: $patnolong\n";
		print LOG "at least one error for: $patnolong\n";

		if ( $error_http_error == 1 ) {
			print "HTTP-Error\n";
			print LOG "HTTP-Error\n";
			system "echo e > error.http_errors";
			my $sql ="UPDATE slurp_pat SET go = 'er_ht' WHERE patno=\'$patno\' ";
			$sth2 = $dbh->query($sql) if ( $debug < 100 );
		}
		elsif ( $error_pages_dont_exist == 1 ) {
			print "error.some.pages.didnt.exist\n";
			print LOG "error.some.pages.didnt.exist\n";
			system "echo e > error.some.pages.didnt.exist";
			my $sql ="UPDATE slurp_pat SET go = 'er_de' WHERE patno=\'$patno\' ";
			$sth2 = $dbh->query($sql) if ( $debug < 100 );
		}

		if ( $success == 1 ) {
			print "error.pages.are.incomplete\n";
			print LOG "error.pages.are.incomplete\n";
			system "echo e > error.pages.are.incomplete";
		}
		else {
			print "error.didnt.work.at.all\n";
			print LOG "error.didnt.work.at.all\n";
			system "echo e > error.didnt.work.at.all";

			$error_get_app_essential=1;

			my $sql ="UPDATE slurp_pat SET go = 'error' WHERE patno=\'$patno\' ";
			$sth2 = $dbh->query($sql) if ( $debug < 100 );
		}
	}              
}

sub get_all_app_short {

	# only gets the stuff which is in the table "pat", no pdfs

	my $end ="";
	do {
		# gets the short content of all applications in slurp_pat with type='a'
		my $sql=	"SELECT patno, patnolong FROM slurp_pat WHERE 
					go = 'y' AND 
					type='a' AND
					data=2
					LIMIT 1
					";

		print LOG "$sql\n" if ( $debug );
		$sth = $dbh->query($sql);
	 
		($patno, $patnolong ) = $sth->fetchrow;
			 
		if ( !$patno ) {
			# no more left, stop this
			$end =1;
		}
		else {
			print "getting: $patno long: $patnolong\n";             
			print LOG "getting: $patno long: $patnolong\n";             

			my $sql ="UPDATE slurp_pat SET go = 'doing' WHERE patno=\'$patno\' ";
			my $sth2 = $dbh->query($sql) if ( $debug < 100 );

			&get_app_short( $patno, $patnolong );

			# if there was an error, try once more
			if ( $error_get_app_short ) {
				&get_app_short( $patno, $patnolong );    
			}

			# if there's still something wrong, get annoyed
			if ( $error_get_app_short ) {
				print "error getting: $patno , $patnolong\n";
				my $sql ="UPDATE slurp_pat SET go = 'error' WHERE patno=\'$patno\' ";                    
				my $sth2 = $dbh->query($sql) if ( $debug < 100 );  
			}
			else {
				my $sql ="UPDATE slurp_pat SET go = 'done' WHERE patno=\'$patno\' ";
				$sth2 = $dbh->query($sql) if ( $debug < 100 );
			}
		}

	} until ( $end == 1 );

}

sub get_app_short {

	# gets one single application, short info

	local ( $patno, $patnolong ) = @_;
	$error_get_app_short="";

	if ( !$patnolong ) {
		$patnolong = get_patno_long( $patno );
	}

	#$req ="http://localhost/1033338.txt";
	$req ="http://ep.espacenet.com/search97cgi/s97is.dll?Action=View&ST=3&VdkVgwKey=$patnolong";

	print $req."\n" if ( $debug );
	print LOG "$req\n";

	my $req = new HTTP::Request GET => $req;

	# Pass request to the user agent and get a response back
	my $res = $ua->request($req);

	# Check the outcome of the response
	if ($res->is_success) {
		print "got: $patno\n";
		print LOG "got: $patno\n";
		$pat =  $res->content;

		# save in file, create subfolders bec ext2 gets slowwww with lots of 
		# files :(

		$dir2=substr($patno,0,3);

		mkdir ("$pat_txt_dir/$dir2",0777);

		open (PAT,">$pat_txt_dir/$dir2/$patno.txt") || die "cant create $pat_txt_dir/$dir2/$patno.txt";
		print PAT $pat;
		close (PAT);
		
		# save in db
		put_app_in_db( $pat, $patnolong );

	} 
	else {
		print "Bad luck this time $patno\n";
		print LOG "Bad luck :$patno\n";
		$error_get_app_short=1;
	}

	( $patno, $patnolong);

}



sub put_granted_in_db {
	my ($pat, $patnolong ) = @_;      

	$patno = $patnolong;
	$patno =~ s/^EP//go;

	my ( $line, $priority_no, $pub_date, $applicant_name, $applicant_country, $title_en, $title_de, $title_fr, $inventor, $ec_class, $alternate_patno );

	@all_lines = split(/\n/,$pat);
	do { 
		$line = shift @all_lines;

		$line =~ s/<BR>$//oi;
		$line =~ s/\'/\\\'/go;
		$line =~ s/&#160;//go;

		if ($line =~ /Publication date/o ) {
			$line = shift @all_lines;
			$line =~ s/\'/\\\'/go;
			$line =~ /(\d\d\d\d-\d\d-\d\d)/o;
			$pub_date = $1;
		}
		elsif ( $line =~ /Equivalents/o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          
			$line =~ s/\'/\\\'/go;
			$alternate_patno .= "$line ";
		}
		elsif ( $line =~ /Inventor\(s\)/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$inventor= $1;
			$inventor=~ s/^ +//o;
		}
		elsif ( $line =~ /Patent Number:/o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          
			$line =~ s/\'/\\\'/go;
			$alternate_patno .= "$line ";
		}
		elsif ( $line =~ s/EC Classification://o ) {
			$line = shift @all_lines;
			$line =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;          

			# grmblthrowoutjavascript
			$line =~ s/\'//go;
			$line =~ s/Text \+= //go;
			$line =~ s/\+ makeEcla\(//o;
			$line =~ s/\) \+//o;
			$line =~ s/ +/ /go;
			$ec_class .= "$line ";
		}
		elsif ( $line =~ /Priority Number/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$priority_no= $1;
			$priority_no=~ s/^ +//o;
		}
		elsif ( $line =~ /Applicant\(s\)/o ) {
			$line = shift @all_lines;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			$applicant_name= $1;
			$applicant_name=~ s/^ +//o;
			$applicant_name =~ /\((\S\S)\)/o;
			$applicant_country = $1;
		}
		elsif ( $line =~ /IPC Classification:/o ) {
			$line = shift @all_lines;
			$line =~ s/&#160;//go;
			$line =~ /> (.*)<\/FONT></o;
			$line =~ s/\'/\\\'/go;
			@classes = split (/ ; /, $1);
			foreach $lola ( @classes ) {
				$lola=~ s/:/\//go;
				$lola=~ s/ +//go;
				$sql= "INSERT INTO pat_ipc VALUES (\'$patno\', \'$lola\')";
				print "$sql\n" if ( $debug );
				print LOG "$sql\n" if ( $debug );
				$sth3 = $dbh->query("$sql") if ( $debug < 100 );     
			}
		}
		elsif ( $line =~ /<TITLE>/o ) {
			$line =~ /--(.*)---/o;
			$title_en=$1;
			$title_en=~s/ +/ /go;
			$title_en=~s/^ +//go;
		}
		elsif ( $line =~ />Abstract</o && $line =~ /<TD/o ) {

			# next 2 lines are useless
			$line = shift @all_lines;
			$line = shift @all_lines;

			$abstract = shift @all_lines;

			# remove HTML-tags
			$abstract =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;

			# ( if the tags get more complicated someday, better solutions
			# can be found on http://www.perl.com/pub/doc/manual/html/pod/perlfaq9.html#How_do_I_remove_HTML_from_a_stri
			# but it's ok for now )

			$abstract=~s/^ +//go;
			$abstract =~ s/\'/\\\'/go;

		}
	} until ( $line =~ /<\/HTML>/oi );

	# so much about data-quality, 1619 out of 24512 :(
	#if ( length($title_de) < 2 ) {
		#$title_de=$title_en;
	#}
	#if ( length($title_fr) < 2 ) {
		#$title_fr=$title_en;
	#}

		my $data_gra=2;
		my $type='g';

		if ( !$abstract ) {
			$abstract = "No Abstract available from Espacenet";
		}

		$alternate_patno =~ s/ +/ /go;

		$sql= "UPDATE pat  SET
							title_en         = '$title_en',
							pub_date         = '$pub_date',
							dbdate            = '$today',
							priority_no      = '$priority_no',
							applicant_name   = '$applicant_name',
							applicant_country= '$applicant_country',
							patnolong        = '$patnolong',
							type             = '$type',
							data_gra         = $data_gra,
							abstract         = '$abstract',
							alternate_patno  = '$alternate_patno',
							inventor         = '$inventor',
							ec_class         = '$ec_class'
					WHERE patno='$patno'";
		

	$sql=~s/&amp;/&/go;
	$sql=~s/&quot;/\"/go;
	$sql=~s/&nbsp;/ /go;
	$sql =~ s/&#160;//go;

	print "$sql\n" if ( $debug );
	print LOG "$sql\n" if ( $debug );

	$sth4 = $dbh->query("$sql") if ( $debug < 100 );     
}

sub put_app_in_db {
	my ($pat, $patnolong) = @_;      

	@all_lines = split(/\n/,$pat);
	foreach ( @all_lines ) {          
		s/<BR>$//oi;

		s/\'/\\\'/go;

		if (/^DOCDB_NO:\s(\d\d\d\d\d\d\d)/o ) {
			$patno= $1;
		}
		elsif (/^PUB_DATE:\s(\d\d\d\d\d\d\d\d)/o ) {
			$pub_date= $1;
		}
		elsif (/^Applicant-No:\s(\S*)/o ) {
			$applicant_no= $1;
		}
		elsif (/^E_APP_NO: (\S*)\sApp_date:\s(\d\d\d\d\d\d\d\d)/o ) {
			$e_app_no= $1;
			$app_date = $2;
		}
		# TODO nachfuellen
		elsif (/^Priority No.: (.+)/o ) {
			$priority_no= $1;
		}
		elsif (/^Applicant Name: (.*)/o ) {
			$applicant_name= $1;
			$applicant_name =~ /\((\S\S)\)/o;
			$applicant_country = $1;
		}
		elsif (/^Applicant Name non-standard: (.*)/o ) {
			$applicant2= $1;
		}
		elsif (s/^IPC-Classes: //o ) {
			@classes = split (/ ; /);
			foreach $lola ( @classes ) {
				$lola=~ s/:/\//go;
				$sql= "INSERT INTO pat_ipc VALUES (\'$patno\', \'$lola\')";
				print LOG "$sql\n" if ( $debug );
				$sth3 = $dbh->query("$sql") if ( $debug < 100 );     
			}
		}
		elsif (s/^Title\(s\): //o ) {
			$title_en=$_;
			$title_fr=<PAT>;
			chop($title_fr);
			$title_fr=~s/<BR>$//oi;
			$title_fr=~s/\'/\\\'/go;
			$title_de=<PAT>;
			chop($title_de);
			$title_de=~s/<BR>$//oi;
			$title_de=~s/\'/\\\'/go;
			
		}
	}

	# so much about data-quality, 1619 out of 24512 :(
	#if ( length($title_de) < 2 ) {
		#$title_de=$title_en;
	#}
	#elsif ( length($title_fr) < 2 ) {
		#$title_fr=$title_en;
	#}


	$type='a';
	$data=2;
	$sql= "INSERT INTO pat VALUES ('$patno', '$pub_date', '$applicant_no','$e_app_no', '$app_date', '$priority_no', '$applicant_name', '$applicant_country','$applicant2','$title_en','$title_fr','$title_de',0,0,0,0,0,0,'$today', '$patnolong','$type',$data,'','','','','','','','','',''  )";

	$sql=~s/&amp;/&/go;
	$sql=~s/&quot;/\"/go;
	print LOG "$sql\n" if ( $debug );

	$sth4 = $dbh->query("$sql") if ( $debug < 100 );     
}

sub delete_useless_data {

	# mark all slurp-request for patents which are already there and where
	# "data" is higher, because having data=2 also includes data=1
	# 1st, only the ones where type (granted/application) is the same, 
	# which leaves us the application which have been granted in the 
	# meanwhile ( which are a lot, it seems like the applications stay
	# in the applications-db until they're 24 months old, no mater if
	# they get granted or not ) and request for more "data"

	$sql=	"SELECT slurp_pat.patno, pat.data_gra
				FROM slurp_pat,pat 
				WHERE slurp_pat.patno = pat.patno 
				AND slurp_pat.type = pat.type
							";
	print LOG "$sql\n" if ( $debug );
	$sth = $dbh->query($sql);

	while ( ( $patno, $data ) = $sth->fetchrow ) {
		$sql=		"UPDATE slurp_pat SET go='alrdy'
						WHERE patno=\'$patno\' 
						AND data <= $data 
						AND go=\'y\'
						";
		print "$sql\n" if ( $debug );
		print LOG "$sql\n" if ( $debug );
		$sth2 = $dbh->query($sql) if ( $debug < 100 ) ;
	}
}

sub dont_use {

	# bec we need to check that anyway, beofre writing to the db

	# mark all, which are already there as applications in pat and as
	# granted in slurp_pat with go='update'
	$sql=	"SELECT slurp_pat.patno, pat.data 
				FROM slurp_pat,pat 
				WHERE slurp_pat.patno = pat.patno 
				AND slurp_pat.type != pat.type
							";
	print LOG "$sql\n" if ( $debug );
	$sth = $dbh->query($sql) if ( $debug < 100 ) ;

	while ( ( $patno, $data ) = $sth->fetchrow ) {
		$sql=		"UPDATE slurp_pat 
						SET go='n'
						WHERE patno=\'$patno\' 
						";
		print LOG "$sql\n" if ( $debug );
		$sth2 = $dbh->query($sql) if ( $debug < 100 ) ;
	}
}


sub get_patno_long {
	my ( $patno )= @_;

	print LOG "need to get patnolong for $patno\n";
	print "need to get patnolong for $patno\n" if ( $debug );

	#$req = "http://localhost/0984359.html";

	my $req = "http://ep.espacenet.com/search97cgi/s97is.dll?Action=FilterSearch&SearchType=3&Filter=e/de/de/dipsfilt.hts&PN=$patno&ResultField[1]=Country&ResultField[2]=EPOQUE_NO&ResultField[3]=P_Title_1&ResultCount=20";

	print $req."\n" if ( $debug ) ;
	print LOG "$req\n";

	my $req = new HTTP::Request GET => $req;

	# Pass request to the user agent and get a response back
	my $res = $ua->request($req);

	# Check the outcome of the response
	if ($res->is_success) {
		$pat =  $res->content;

		if ( $debug ) {
			open (PAT,">$patno") || die "can't create $patno";
			print PAT $pat;
			close (PAT);            
		}

		print "got patnolong-page for : $patno\n";
		print LOG "got: $patno\n";
		
		@all_lines22 = split(/\n/,$pat);

		$patnolong = &extract_patnolong( $patno );
	} 
	else {
		print "didnt get patnolong $patno\n";
		print LOG "didnt get patnolong $patno\n";
	}
	print "got patnolong: $patnolong\n";
	print LOG "got patnolong: $patnolong\n";

	$patnolong;

}

sub extract_patnolong {
		
	my ( $patno ) = @_;
	my ($lola, $patno_check, $patnolong_tmp);
	my $patnolong="";

	foreach $lola ( @all_lines22 ) {
		if ( $lola =~ /VdkVgwKey=(\w+)&/o ) {
			print "patnolong probably: $1\n" if ( $debug );

			$patnolong_tmp = $1;
			$patno_check = $patnolong_tmp;
			$patno_check =~ s/^EP//oi;
			$patno_check =~ s/A\d$//oi;

			if ( $patno == $patno_check ) {
				$patnolong = $patnolong_tmp;
			}
			print "xx: $patno , $patno_check, $patnolong\n";
		}
	}

	$patnolong;
}

sub reopen_db {
	$bla++;
	if ( ($bla % 100) == 0 ) {
		print "closing DB\n";
		undef $dbh;
		sleep 1;
		$dbh = Mysql->connect("$server_ip", "$database", "$db_login", "$db_password");
		print "reopening DB\n";
	}
	else {
		print "leaving DB alone\n";
	}
}
