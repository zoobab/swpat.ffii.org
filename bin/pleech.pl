#!/usr/bin/perl -w
use IO::Socket;
use File::Path;
use strict;

#Fixes:
#*  "also published as" handles multiple pages (more>>)
#*  blacklist same place as executable
#*  Mosaic & INPADOC URLs added.
#*  commandline options modified
#*  completely rewritten and joined with epoline functions

my %known;
my $outdir;
my $Nsaved=0;

#get relative path to executable
my $exedir="";
if ($0 =~ m|/|) { ($exedir) = ( $0 =~ m|^(.*/)| ); }

#position of blacklist file
my $blacklist = "${exedir}blacklist";

#handle arguments:

my $a=join " ",@ARGV;
if ($a =~ /--help/i) { die <<EOL;
Function:  pleech.pl

  Filters patent numbers from stdin, downloads patent bibliographies,
  descriptions, claims and saves to a html file.

Syntax:

    psearch.pl ...    | pleech.pl [options]
    cat searchresults | pleech.pl [options]
    pleech.pl [options] EPx dir1/EPy dir2/EPz

  Return value:
  
    Number of patent documents saved, 255 if more than 255.

  Options:

    Long           Short         Description

    --clobber      -c            Overwrite existing files
    --noblacklist  -nbl          don't use blacklist 
    --outdir dir   -o dir        output directory   (default pwd)

    --help                       prints this help

    Default is not clobber files, and to use the blacklist.

If more arguments are present on the commandline, these will be
assumed to be patents to be downloaded, if there are no further
arguments, the program will listen on STDIN.
EOL
}

#defaults options
my $ooutdir ="./";
my $doFileTest = 1;
my $doBlacklist = 1;

HandleOptions();

print "Options:";
if ($doFileTest) { print " Don't clobber files.";}
            else { print " Clobber files.";}

if ($doBlacklist) { print " Use blacklist.";}
             else { print " Don't use blacklist.";}

print " Defaultdir $ooutdir  ";

if (scalar(@ARGV)==0) { print "Listning to STDIN\n"; }
                 else { print "Loading ARGV patents\n"; }


# Flush STDOUT buffer after each command
select(STDOUT); $| = 1;

#load blacklist:
if ($doBlacklist and -e $blacklist) { LoadFilter(); }
$/=undef;

#handle patents
if (scalar(@ARGV)==0) { FilterStream(<STDIN>); }
                 else { FilterArg(); }
    
if ($Nsaved>255) { exit 255; }
            else { exit $Nsaved;}


#support functions: ------------------------------------------------------------------------

sub HandleOptions
#
# Handles commandline options
#
{
   while (scalar(@ARGV)>0 and $ARGV[0] =~ /^-/)
     {
        $a=shift @ARGV;

#get output directory
        if ( $a =~ /--outdir/i or $a =~ /-o/i)
          {
             $ooutdir=shift @ARGV;
             if (not $ooutdir =~ m|/^|) {$ooutdir.='/';}  
          }
        elsif ($a =~/--clobber/i  or $a=~/-c/) { $doFileTest=0; }
        elsif ($a =~/--noblacklist/i  or $a=~/-nbl/) { $doBlacklist=0; }
        else { die "Error: Unknown argument $a. Run with --help\n"; }
     }
}

sub HTTP
#send to port 80 and hope for a reply.
#User has to supply correct HTTP headers.
#returns whatever it receives from the net.
#supports proxies.
{
  my ($url)=@_;
  my ($server)=$url =~ m|http://([^/]+)|i;
  my $port=80;

#delay range after each DL.
  my $tmax=6;
  my $tmin=3;

  if (defined $ENV{'http_proxy'})  # http_proxy=www-proxy:8080
  {
     print STDERR "Using http_proxy: ".$ENV{'http_proxy'}."\n";
     $ENV{'http_proxy'} =~ /http:\/\/([\w-]*):([0-9]*)/;
     ($server,$port)=($1,$2);
  }

  my $sock = new IO::Socket::INET(
           PeerAddr => $server,
           PeerPort => $port,
           Proto => 'tcp');
  die "Could not create socket: $!\n" unless $sock;

#Mozilla HTTP request header:
my $req= <<EOL;
GET $url HTTP/1.0
User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.5) Gecko/20031007
Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1
Accept-Language: da,en-us;q=0.7,en;q=0.3
Accept-Encoding: gzip,deflate
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
Keep-Alive: 300
Proxy-Connection: keep-alive

EOL

  print $sock $req;
  my $content=<$sock>;
  close($sock);

  $content =~ s/^.*(?=<html)//si;   #remove html header
  sleep $tmin+rand ($tmax-$tmin);

  return $content;
}


sub DownloadPatent
#
# Downloads a EPx patent from v3.epospace.com together with claims
# and description
#
{
#
# Fix: Implement check for application numbers and convert to publication number.
#

  my $idxs=shift;  #index
  $idxs=~ s/(?<=[A-Z][A-Z])0+//;
  
#EP0892355 normalisation FIX
  $idxs=~ /([A-Z][A-Z])0*(\d*)/;
  my $idx=$1.(sprintf "%07d",$2);
  
#Get bibliography:
  my $urlBIB   = "http://v3.espacenet.com/textdoc?&DB=EPODOC&IDX=$idx";
  $_=HTTP($urlBIB);

  my ($title,$pat,$pub,$inv,$app,$ipc,$ecla,$appl,$pri,$abs,$apa,$desc,$claim,$yes);
  my ($urlJPEG,$urlFAM,$urlDESC,$urlCLAIM,$urlMOSAIC,$urlDOC,$urlLEGAL);

  if (/<H2 class="title">(.+?)<\/H2>/i)                            { $title=ucfirst(lc($1));}
  if (/<nobr>Patent number:<\/nobr><\/td><td>(.+?)<\/td>/i)        { $pat=$1;  }
  if (/<nobr>Publication date:<\/nobr><\/td><td>(.+?)<\/td>/i)     { $pub=$1   }
  if (/<nobr>Inventor:<\/nobr><\/td><td>(.+?)<\/td>/i)             { $inv=lc($1);  }
  if (/<nobr>Applicant:<\/nobr><\/td><td>(.+?)<\/td>/i)            { $app=ucfirst(lc($1));  }
  if (/<nobr>- international:<\/nobr><\/td><td>(.+?)<\/td>/i)      { $ipc=$1;  }
  if (/\(makeEcla\('(.*?)'\)\)/i)                                  { $ecla=$1; }
  if (/<nobr>Application number:<\/nobr><\/td><td>(.+?)<\/td>/i)   { $appl=$1; }
  if (/<nobr>Priority number\(s\):<\/nobr><\/td><td>(.+?)<\/td>/i) { $pri=$1;  }
  
  if (/id="jpgImg"\s*src="([^"]+)"/i)                              { $urlJPEG=$1; }
  if (/href="(family[^"]*)"/i)                                     { $urlFAM=$1;  }
  if (/href="(textdes[^"]*)"/i)                                    { $urlDESC=$1; }
  if (/href="(textclaim[^"]*)"/i)                                  { $urlCLAIM=$1; }
  if (/href="(textdraw[^"]*)"/i)                                   { $urlMOSAIC=$1;}
  if (/href="(origdoc[^"]*)"/i)                                    { $urlDOC=$1;   }
  if (/href="(legal[^"]*)"/i)                                      { $urlLEGAL=$1; }

  if (/<td valign="top" id="abCell">(.*?)<\/td>/is)                { $abs=$1; }

#loads all APA even if they cross multiple pages  (more >>)
  my $done=0;
  my %apap=();
  do
    {
       if (/Also published as:(.+)/i)
           {
              my $t=$1;
              while ($t =~ /<nobr>([A-Z]{2})([0-9]+)\s+\(([A-Z][0-9]?)\)<\/nobr>/icg)
                {
                   my $new="$1$2 $3";
                   $apap{$new}=1;
                }
           }

       if (m|<A HREF="/(textdoc\?[^"]+)">more\s+&gt;&gt;</A>|i)
           {
              $_=HTTP("http://v3.espacenet.com/$1");  #load next
           }
         else
           {  $done=1;  }

    }
  until ($done);
  $apa=join ", ",(keys %apap);
  print $apa;
  
#generate filename:
  my $fnam=$idxs.".html";

  my $selfurl='http://v3.espacenet.com/';

#get description
  if ($urlDESC)
   {
     $_=HTTP($selfurl.$urlDESC);

     if (/<td colspan="2" align="left" valign="top" class="tab_sel" ID="contentCell">\s*(.*?)\s*<\/td>/is)
       { $desc=$1; }
   }

#get claims
  if ($urlCLAIM)
   {
     $_=HTTP($selfurl.$urlCLAIM);

     if (/<td colspan="2" align="left" valign="top" class="tab_sel" ID="contentCell">\s*(.*?)\s*<\/td>/is)
       { $claim=$1; }
   }

#save data
  if (not $title)
   {
      print "pleech: $idx no title?";  #bad network?
      return undef;
   }

#handle epoline  
   my $epoline=HandleEpoline($idx);
   if (not defined $epoline)
     {
       print "epoline problem $_\n";
       return;
     }

  $epoline="<tr><td>epolinedldate</td><td>".time."</td></tr>\n".$epoline;

#Save data to file

  if (not -d $outdir) { mkpath $outdir; }

  $Nsaved++;
  open FO,">$outdir$fnam" or die "Can't save $fnam\n";

  print FO <<EOL;
<html><title>$title</title>
<body><table border>
EOL
  print FO "<tr><td>downloaddate</td><td>".time."</td></tr>\n";

  if ($title) {print FO "<tr><td>Title</td><td>$title</td></tr>\n";}
  if ($idxs)  {print FO "<tr><td>Idx</td><td>$idxs</td></tr>\n";}
  if ($pat)   {print FO "<tr><td>Pat</td><td>$pat</td></tr>\n";}
  if ($pub)   {print FO "<tr><td>Pub</td><td>$pub</td></tr>\n";}
  if ($inv)   {print FO "<tr><td>Inv</td><td>$inv</td></tr>\n";}
  if ($app)   {print FO "<tr><td>App</td><td>$app</td></tr>\n";}
  if ($ipc)   {print FO "<tr><td>IPC</td><td>$ipc</td></tr>\n";}
  if ($ecla)  {print FO "<tr><td>ECLA</td><td>$ecla</td></tr>\n";}
  if ($appl)  {print FO "<tr><td>Appl</td><td>$appl</td></tr>\n";}
  if ($pri)   {print FO "<tr><td>Pri</td><td>$pri</td></tr>\n";}
  if ($apa)   {print FO "<tr><td>APA</td><td>$apa</td></tr>\n";}

  if ($urlJPEG) {print FO "<tr><td>urlJPEG</td><td>$urlJPEG</td></tr>\n";}
  if ($abs)     {print FO "<tr><td>ABS</td><td>$abs</td></tr>\n";}
  if ($desc)    {print FO "<tr><td>Desc</td><td>$desc</td></tr>\n";}
  if ($claim)   {print FO "<tr><td>Claim</td><td>$claim</td></tr>\n";}
  
  print FO $epoline;

  print FO <<EOL;
</table></body></html>
EOL
  close FO;
  return "$outdir$fnam";
}

sub HandleEpoline
#
# Download Epoline data, and return html formatted data.
#
{
  my $pat=shift;

  my $url="http://register.epoline.org/espacenet/regsearch?CY=ep&LG=en&DB=REG&LS=1&NA=0&PN=$pat";
  my $c=HTTP($url);

  return undef unless
     ($c =~ /window.location="([^"]*)"/i);
  
  $url=$1;
  
  if ($url =~ m|^/|)
     {$url = 'http://register.epoline.org'.$url; }

  $c=HTTP($url);
  $c =~ s/\[[0-9]{4}\/[0-9]{2}\]//g;   #remove refs. to EPO doc.

  my $h;
  my ($key,$val);
  while ($c =~ /a\('([A-Z]+)','(.*?)'\)/g)
    {
       $key=$1; $val=$2;

       $val =~ s/(?<=<term>)/\n/g;
       $val =~ s/\n*$//;

       $val =~ s/Bulletin of publication of application//;
       $val =~ s/<cols>Designated States \(intention to pay\/payment received\)<term>//;
       $val =~ s/Date of publication of search report//;

       if ($key eq "DS") { $val=~ s/\s+//g; }
       $h.="<tr><td>reo_$key<\/td><td>$val<\/td><\/tr>\n";
#       print "$key->$val\n";
    }
   return $h;
}


sub LoadFilter
#
# Filter to prevent known patents are downloaded repeatedly.
#
{
  open FI,$blacklist or die "Can't load $blacklist";
  while (<FI>)
     {
        chomp;
        $_ =~ /([A-Z][A-Z])0*([0-9]+)/;
        $known{$1.$2}=$_;
     }

  print "Loaded ".scalar(keys %known)." patents from $blacklist\n";
}


sub Filter
#
# filters patent publication numbers for download
#
{
  my ($p)=@_;

  $p=uc $p;
  $p=~ s/(?<=[A-Z][A-Z])0+//;
  
  return unless $p =~ /^[A-Z][A-Z][0-9]+$/;
  print "pleech: $outdir$p ";

  if ($doFileTest and -e "${outdir}${p}.html")  #patent already present in dir
       {
          $known{$p}="${outdir}${p}.html";
          print " file exists\n";
          return;
       }
       
  if ($doBlacklist and defined $known{$p})  # and -e $exedir.$known{$p}
       {
          print "blacklisted\n";
          return;
       }
       
  my $file=DownloadPatent($p);
  if (defined $file) { $known{$p}=$file; }
  print "\n";
}

sub FilterStream
#
# Read EPx numbers from supplied text
#
{
  my ($content)=@_;  
  $outdir=$ooutdir;

  while ($content =~ /\b([A-Za-z]{2}\d{4,})\b/cgi)
   { Filter($1); }
}

sub FilterArg
#
# Loads patents from the commandline.
# Format: EPx or dir/EPx
#
{
   my $p;
   foreach my $a (@ARGV)
      {
         $outdir=$ooutdir;
                  
         if ($a =~ m|(.*/)|) { $outdir=$1; }

         next unless ($a =~ m|([^/]+)$|);
         Filter($1);
      }
}
