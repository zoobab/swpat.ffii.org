#!/usr/bin/perl

# script: ffii_cr_news-rss091.pl
# version: 0.01, 2004-02-26
# license: GPL
# creator: Robert Hase, robert.hase@muc-net.de
# publisher: FFII Hartmut Pilch, phm@a2e.de
# desc: very simple script that grabs the ffii sites for news + creates a language depended rss-file v0.91


use LWP::UserAgent;

my $ua = LWP::UserAgent->new;
$ua->timeout(30);
my %urls = (
			'de' => {
					'uri' 		  => "http://www.ffii.org/index.de.html",
					'title' 	  => 'F&ouml;rderverein f&uuml;r eine Freie Informationelle Infrastruktur e.V.',
					'description' => 'letzte Neuigkeiten bei FFII',
					},
			'en' => {
					'uri' 		  => "http://www.ffii.org/index.en.html",
					'title' 	  => 'Foundation for a Free Information Infrastructure',
					'description' => 'latest news on FFII',
					},
			'fr' => {
					'uri' 		  => "http://www.ffii.org/index.fr.html",
					'title' 	  => 'Association pour une Infrastructure Informatique Libre',
					'description' => 'Derni�res Nouvelles et FFII',
					},
			'nl' => {
					'uri' 		  => "http://www.ffii.org/index.nl.html",
					'title' 	  => 'FFI',
					'description' => 'Laatste nieuws',
					},
			'eo' => {
					'uri' 		  => "http://www.ffii.org/index.eo.html",
					'title' 	  => 'Asocio por Libera Informa Infrastrukturo',
					'description' => 'Latest News on FFII',
					},
			'it' => {
					'uri' 		  => "http://www.ffii.org/index.it.html",
					'title' 	  => 'FFII',
					'description' => 'Latest News on FFII',
					},
			);
	
for my $lang (keys(%urls))
{
	my $request = HTTP::Request->new(GET => $urls{$lang}{'uri'});
	my $response = $ua->request($request);
	if ($response->is_success)
	{
		open(FH, ">./ffii_news-${lang}.rss") || die "<b>Error creating ./ffii_news-${lang}.rss: $! $?</b>";
		print FH '<?xml version="1.0"?>' . "\n";
		print FH '<!DOCTYPE rss PUBLIC "-//Netscape Communications//DTD RSS 0.91//EN" "http://my.netscape.com/publish/formats/rss-0.91.dtd">' . "\n";
		print FH '<rss version="0.91">' . "\n";
		print FH "<channel>\n";
		print FH "<title>$urls{$lang}{'title'}</title>\n";
		print FH "<link>$urls{$lang}{'uri'}</link>\n";
		print FH "<description>$urls{$lang}{'description'}</description>\n";
		print FH "<language>$lang</language>\n";

		$response->content =~ /<div class="linkbartit">/;
		my $eins = $';
		$eins =~ /<\/ul>/;
		my $zwei = $`;
		$zwei =~ /<ul class="linkbar">/;
		my $drei = $';
		$drei =~ s/(<li>)|(<\/li>)//g;

		my @erg = split(/<\/a>/,$drei);
		for my $line (@erg)
		{
			$line =~ /<a.*>/;
			my $uri = $&;
			$line = $';
			$line =~ /: /;
			my $time = $`;
			my $desc = $';
			$uri =~ s/(<a href=)|(")|(>)//g;
			print FH "<item>\n";
			print FH "<title>$desc</title>\n";
			print FH "<link>$uri</link>\n";
			print FH "<description></description>\n";
			print FH "<dc:date>$time</dc:date>\n";
			print FH "</item>\n";
		}
	print FH "</channel>\n";
	close(FH);
	}
	else
	{
		print $response->error_as_HTML;
	}
}



