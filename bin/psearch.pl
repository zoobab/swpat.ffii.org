#!/usr/bin/perl -w

#
# Interface for making searches at v3.espacenet.com.
# Returns list of patent number and title for matches.
#


use strict;
use IO::Socket;

#delay range after each HTTP request
my $tmax=5;
my $tmin=1;

#cookie grabbed from first request
my $cookie='';

#process input
my ($ti,$tia,$co,$yr,$ap,$ip,$ecla,$appno);


if ($#ARGV != 7)
{
   if ($#ARGV == 0 and $ARGV[0] eq "--help") {Help();}

print STDERR "Syntax: psearch.pl --help        for help\n\n";

print STDERR "Enter search criteria\n";

print STDERR "Title:";
chop ($ti=<STDIN>);
print STDERR "Title+Abstract:";
chop ($tia=<STDIN>);
print STDERR "Country:";
chop ($co=<STDIN>);
print STDERR "Year:";
chop ($yr=<STDIN>);
print STDERR "Applicant:";
chop ($ap=<STDIN>);
print STDERR "IPC:";
chop ($ip=<STDIN>);
print STDERR "ECLA:";
chop ($ecla=<STDIN>);
print STDERR "Application number:";
chop ($appno=<STDIN>);

print STDERR "\nCommand: psearch.pl '$ti' '$tia' '$co' '$yr' '$ap' '$ip' '$ecla' '$appno'\n";

  Search($ti,$tia,$co,$yr,"","",$ap,$ip,$ecla,$appno);
}
else
{
  Search($ARGV[0],$ARGV[1],$ARGV[2],$ARGV[3],"","",$ARGV[4],$ARGV[5],$ARGV[6],$ARGV[7]);
}

sub HTTP
#send to port 80 and hope for a reply.
#User has to supply correct HTTP headers.
#returns whatever it receives from the net.
#supports proxies.
{
  my ($txt)=@_;
  my ($server)=$txt =~ m|http://([^/]+)|i;
  my $port="80";

  if (defined $ENV{'http_proxy'})  # http_proxy=www-proxy:8080
    {
       print STDERR "Using http_proxy: ".$ENV{'http_proxy'}."\n";
       $ENV{'http_proxy'} =~ /http:\/\/([\w-]*):([0-9]*)/;
       ($server,$port)=($1,$2);
    }

print "$server $port\n";

  my $sock = new IO::Socket::INET(
           PeerAddr => $server,
           PeerPort => $port,
           Proto => 'tcp');
  die "Could not create socket: $!\n" unless $sock;

my $req= <<EOL;
GET $txt HTTP/1.0
User-Agent: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.5) Gecko/20031007
Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1
Accept-Language: da,en-us;q=0.7,en;q=0.3
Accept-Encoding: gzip,deflate
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
Keep-Alive: 300
Proxy-Connection: keep-alive
Referer: http://v3.espacenet.com/results?sf=a&CY=ep&LG=en&DB=EPODOC&TI=&AB=&PN=EP&AP=&PR=&PD=2003&PA=microsoft&IN=&EC=&IC=&=&=&=&=&=&PGS=20&FIRST=41
$cookie

EOL

  print $sock $req;
  my $content=<$sock>;
  close($sock);

#grab set-cookie from headers:
  if ($content =~ /Set-Cookie: (JSESSIONID.*?)Path/i )
     { $cookie="Cookie: $1"; }

  $content =~ s/(^.*)(<html.*)//si;   #remove html header
  $content=$2;

  sleep $tmin+rand ($tmax-$tmin);
  return $content;
}


sub EspacenetCount
#
# Returns count of patents found.
#
{
  my ($title,$txt,$pat,$year,$month,$day,$app,$ipc,$ecla,$appno)=@_;

  my $url="http://v3.espacenet.com/results?sf=a&FIRST=1&CY=ep&LG=en&DB=EPODOC&TI=$title&AB=$txt&PN=$pat&AP=$appno&PR=&PD=$year$month$day&PA=$app&IN=&EC=$ecla&IC=$ipc&=&=&=&=&=";
#  print "$url\n";
  my $content=HTTP($url);

  if ($content =~ /Approximately <STRONG>(\d*)<\/STRONG> result/i)
     { return $1; }
   else
     { print STDERR "Could not find number of search results\n";
       return 0;
     }
}

sub printPatents
#
# Processes webpages and prints patents
#
{
  my ($content)=@_;
  my ($t,$url,$title,$pat);

  while ($content =~ /<tr\s+class\s*=\s*"rowcol4hi"\s*>\s*<td valign="top"(.*?)\s*<\/tr>/cgsi)
   {
      $t=$1;

      $t =~ /<a\s+href\s*=\s*"([^"]+)">/;              $url=$1;
      $url =~ /IDX=([A-Za-z0-9]+)/;                    $pat=$1;
      $t =~ /<strong><a[^>]+>(.+?)<\/a><\/strong>/is;  $title=ucfirst(lc($1));

      if ($t =~ /Document is too new to have information available/)
        { print "Too new document warning!\n"; }
       else
        { print "$pat $title\n"; }
   }
}


sub EspacenetSearch
#
# Makes searches, and downloads the pages listing patents
#
{
  my ($number,$title,$txt,$pat,$year,$month,$day,$app,$ipc,$ecla,$appno)=@_;
  my $content;

  for (my $ofs=1;$ofs<=$number;$ofs+=20)
    {
      $content=HTTP("http://v3.espacenet.com/results?sf=a&CY=ep&LG=en&DB=EPODOC&TI=$title&AB=$txt&PN=$pat&AP=$appno&PR=&PD=$year$month$day&PA=$app&IN=&EC=$ecla&IC=$ipc&=&=&=&=&=&PGS=20&FIRST=$ofs");
      printPatents($content);
    }
}


sub Search
#
# Performs recuive subdivision until less than 500 patent documents
# are found, these are then listed.
#
{
  my ($title,$txt,$pat,$year,$month,$day,$app,$ipc,$ecla,$appno)=@_;
  $/=undef;   #no separator

print <<EOF;
#Search data:
#
#title          =\"$title\"
#title/abstract =\"$txt\"
#country        =\"$pat\"
#Year           =\"$year\"
#Month          =\"$month\"
#Day            =\"$day\"
#Applicant      =\"$app\"
#IPC code       =\"$ipc\"
#ECLA code      =\"$ecla\"
#Appno          =\"$appno\"
EOF

#URL encrypt
  $title =~ s/ /\+/g;
  $txt =~ s/ /\+/g;
  $pat =~ s/ /\+/g;
  $app =~ s/ /\+/g;
  $ipc =~ s/ /\+/g;
  $ecla =~ s/ /\+/g;

  my $number=EspacenetCount($title,$txt,$pat,$year,$month,$day,$app,$ipc,$ecla,$appno);

print <<EOF;
#Patents        = $number
EOF

  print STDERR " Search ti=\"$title\" tab=\"$txt\" off=\"$pat\" y=\"$year\"  m=\"$month\" d=\"$day\" AP=\"$app\" I=\"$ipc\" E=\"$ecla\" A=\"$appno\" Found $number patents\n";

  if ($number==0) {return ;}

  if ($number<500 or ($year and $month and $day))  #ok can download directly
    {
      EspacenetSearch($number,$title,$txt,$pat,$year,"$month","$day",$app,$ipc,$ecla,$appno);
    }
  else
    {
   if (not $year)  #too many hits, subdivide years
     {
       print STDERR "Too many! Resolving years 2005-1985\n";
       for ($year=2005;$year>=1985;$year--)
         {
            Search($title,$txt,$pat,$year,"","",$app,$ipc,$ecla,$appno);
         }
     }
   elsif (not $month) #still too many, subdivide months
     {
       print STDERR "Too many! Resolving months 01-12\n";
       for $month (1..12)
         {
             my $monthf=sprintf("%02d",$month);
             Search($title,$txt,$pat,$year,$monthf,"",$app,$ipc,$ecla,$appno);
         }
     }
   else
     { #still to many, subdivide days
       print STDERR "Too many! Resolving days 01-31\n";
       for $day (1..31)
         {
             my $dayf=sprintf("%02d",$day);
             Search($title,$txt,$pat,$year,$month,$dayf,$app,$ipc,$ecla,$appno);
         }
     }
    } #end of if
}



sub Help
{
die <<EOLLL;
Function:  psearch.pl

  Automatises advanced searches on http://www.espacenet.com, the
  patents that are found are summarised with patent codes and titles.
  Output is printed stdout and user info and process info to stderr.

  It is impossible to distinguish between applications and granted
  patents from the search results alone. A second script pleech.pl
  examines individual patents and download granted patents.

Syntax:

  psearch.pl --help

     Prints this help

  psearch.pl

     Asks user to enter search criteria.

  psearch.pl 'method' 'software' 'EP' '1990' 'Microsoft' 'H' 'H'

    Searches for all patents with:

     String         Matches

     'method'       in title
     'software'     in title or abstract
     'EP'           issuing patent office (EP=EPO, WO=WIPO)
     '1990'         year document is published
     'Microsoft'    company
     'H'            IPC class
     'H'            ECLA class
     'EP2004...'    Application number

    Exactly 8 arguments should be specified. '' = anything.

  psearch.pl '' '' 'EP' '1990' 'Microsoft' '' '' ''

     Will return all Microsoft EP publications from 1990
     (both granted patents and applications)

  Search strings matches whole words:

  ''                    matches all documents
  'EP or WO'            matches documents from  EPO og WIPO offices
  'AA and (BB or CC)'   matches all documents with words
                           (AA and BB) or (AA and CC)

  Year must be specified as '' or as '2003'

  Boolean  AND, OR, NOT can be used as well as () e.g. (A or B) and C NOT D
  Truncation  word*  any string, word\?\?, word \#\#

Notice:

  An espacenet search returns 20 hits per webpage, and is restricted to
  displaying only 500 patents. To avoid such a silly limitation subsearches
  are performed on year (if not specified), month, and day  until the
  number of search results is less than 500. This means that a broad search
  criterion can produce quite a lot of traffic, as the subsearches are
  executed. Subsearch on year (if not specified) in done in the range
  from 1985 to 2003 inclusive.

  It is not currently possible to search for ranges of years.
EOLLL
}
