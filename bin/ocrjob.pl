#/usr/bin/perl


$IMGDIR = "/usr/share/epatext/img";
$TXTDIR = "/var/www/swpat/pikta/txt";
  
HDDIRROOT=/usr/local/epatpdfs

# nothing configurable below here

typeset -i N

NUMSTR=$(expr "$CDDIR/EPATPDFS*" : "$CDDIR/EPATPDFS\([0-9][0-9][0-9][0-9]\)");

echo $NUMSTR

HDDIR=$HDDIRROOT/$NUMSTR
test -d $HDDIR || mkdir -p $HDDIR

test -d $HDDIR/img/ep || cp -r $CDDIR/img $HDDIR

cd $HDDIR

find img -type f -name 'ep*.pdf' | \
  while read PDF;do
    set -- $(echo $PDF | sed 's|img/ep/\([0-9][0-9][0-9][0-9]\)/\([0-9][0-9][0-9]\)/ep.*\.pdf|\1 \2|')
    NUM1=$1;NUM2=$2;
    mkdir -p txt/ep/$NUM1/$NUM2
    TBZ=img/ep/$NUM1/$NUM2/ep$NUM1$NUM2.tar.bz2
    test -f $TBZ || {
      pushd img/ep/$NUM1/$NUM2;
      rm -f *.pbm;	
      pdfimages ep$NUM1$NUM2.pdf inv;
      N=0;
      for PBM in inv*.pbm;do
        N=N+1;
        pnminvert $PBM > $(printf "%03d.pbm" $N);
        rm -f $PBM;
       done;	 	  	  
      tar cvf ep$NUM1$NUM2.tar ???.pbm && bzip2 ep$NUM1$NUM2.tar;
      popd;
     };
    (cd img/ep/$NUM1/$NUM2;test -f 001.pbm || tar xIvf $TBZ);
    N=0;
    for F in img/ep/$NUM1/$NUM2/???.pbm;do
      N=N+1;
      gocr -v -m 4 -o $(printf "txt/ep/$NUM1/$NUM2/%03d.txt" $N) $F;
     done;
    rm -f img/ep/$NUM1/$NUM2/*.pbm;    
   done;
