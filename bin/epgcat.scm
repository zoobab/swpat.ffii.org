#!/usr/bin/scsh \
-lm /usr/lib/scsh/pgscsh-0.2/modules.scm -dm -o epgcat -e main -s
!#

(define-structure epgcat
  (export main)
  (open pg scsh list-lib string-lib scheme)
(begin

;; ####### SUBR ###########
(define-syntax setq
  (syntax-rules ()
  ((setq var value)
   (begin
      (set! var value)
         var))))

(define-syntax funcall
  (syntax-rules ()
    ((funcall fun . args)
     (fun . args))) )

(define (numpad num len . rest)
  (and
   (number? num)
   (number? len)
   (let ((pad (or (and (not (null? rest)) (car rest)) #\0)) (str (number->string num))) 
     (let ((padlen (- len (string-length str))))
       (if (> padlen 0)
	   (string-append (make-string padlen pad) str)
	   str ) ) ) ) )

(define (fail str . args) (begin (apply format (current-error-port) (string-append str "\n") args) #f))
(define (succ str . args) (begin (apply format (current-error-port) (string-append str "\n") args) #t))
(define (decho str . args) (if (opt 'debug) (apply format 2 (string-append str "\n") args)))

(define (string-enclose left right str) (and (nonnull-string? str) (string-append left str right)))
(define (string-enclose-lr lr str) (string-enclose lr lr str))
(define (double-quote str) (string-enclose-lr "\"" str))
(define (html-env0 tag str)
  (decho "HTML-ENV0 ~a ~a" tag str)
  (string-enclose (format #f "<~a>" tag) (format #f "</~a>" tag) str) )
(define (html-env tag attr str) 
  (decho "HTML-ENV ~a ~a ~a" tag (safe-string-join (map (lambda (lst) (safe-string-join lst "=")) attr) ";") str)
  (string-enclose (format #f "<~a ~a>" tag (safe-string-join (map (lambda (lst) (gjoin "=" (car lst) (double-quote (safe-string-join (cdr lst) " ")))) attr) " ")) (format #f "</~a>" tag) str))

(define (flatlist . args) (apply append (map (lambda (arg) (if (and arg (list? arg)) (apply flatlist arg) (list arg))) args)))

(define (cassoc prop lst) (let ((tup (assoc prop lst))) (and tup (cdr tup))))

(define (andarg n . sexp) 
;; (andarg 1 'concat 1 2 3)
;; (andarg 1 'concat nil 2 3)
  (and (list? sexp) (nth sexp n) (apply (car sexp) (cdr sexp))) )

(define (cat file) (if (file-readable? file) (run/string (cat ,file)) ""))

(define (format-to-file file str . args)
  (decho "FORMAT-TO-FILE")
  (and
   (or (file-writable? file) (fail "can't write to file ~a" file))
   (with-umask 
    #o002 
    (let ((port (open-output-file file)))
      (and
       (or port (fail "failed to open port for writable file ~a.  never happens." file))
       (begin 
	 (apply format port str args)
	 (close port)
	 (and (file-readable? file) (succ "file ~a written" file)) ) ) ) ) ) )

(define (format-tee file str . args)
  (decho "FORMAT-TEE")
  (and
   (or (file-writable? file) (fail "cant write to file ~a" file))
   (let ((port (open-output-file file))) (apply format port str args) (close port)) )
  (apply format #f str args) )

(define (tee-format file str . args)
  (decho "TEE-FORMAT")
  (let ((port (open-output-file file))) (apply format port str args) (close port)) (cat file))

(define (grep re file)
  (decho "GREP ~a ~a" re file)
  (let ((port #f) (line #f) (match #f) (search (or (and (regexp? re) (lambda (str) (regexp-search re str))) (and (string? re) (lambda (str) (string-contains str re))))))
    (and 
     file
     (string? file)
     (file-exists? file)
     (begin
       (set! port (open-input-file file))
       (let loop ()
	 (set! line (read-line port))
	 (cond
	  ((not (string? line)) #f)
	  ((setq match (search line)))
	  (else (loop)) ) )
     (close port) ) )
    match ) )

(define (list-head lst n) (decho "list-head ~d ~a" n (safe-string-join lst " ")) (if (<= n 0) '() (cons (car lst) (list-head (cdr lst) (- n 1)))))
(define (sublist lst p len) (list-head (list-tail lst p) len))
(define (try-list-head lst n) (or (and n (number? n) (< n (length lst)) (list-head lst n)) lst))
(define (ocrtexts-head lst)
  (decho "OCRTEXTS-HEAD ~a" (safe-string-join lst " "))
  (let ((n (pdfval 'Drawing))) (or (and n (number? n) (begin (set! n (- n 1)) (< n (length lst))) (list-head lst n)) lst)))

(define colonsplit (infix-splitter (rx (: ":" (+ " ")))))
(define semcolsplit (infix-splitter (rx (: (* " ") ";" (+ " ")))))
(define linesplit (infix-splitter "\n"))
(define (safe-cdr pair) (and (pair? pair) (cdr pair)))
(define (safe-member elm list) (and elm list (list? list) (member elm list)))
(define (nonnull-string? arg) (and (string? arg) (> (string-length arg) 0) arg))
(define (nonnull-list? arg) (and arg (list? arg) (not (null? arg)) arg))
(define (string arg) (if arg (or (and (string? arg) arg) (and (symbol? arg) (symbol->string arg)) (and (number? arg) (number->string arg))) ""))
(define (number arg) (or (and (number? arg) arg) (and (string? arg) (string->number (string-trim-both arg))) (and (list? arg) (length arg)) 0))
(define (strings . rest) (map string (flatlist rest)))
(define (nonnull-strings . rest) (delete #f (map (lambda (arg) (and arg (or (and (string? arg) arg) (and (symbol? arg) (symbol->string arg)) (and (number? arg) (number->string arg))))) (flatlist rest))))
(define (safe-string-join strs sep) (string-join (apply strings strs) sep))
(define (gjoin sep . rest) (string-join (apply nonnull-strings rest) sep))

(define (file-harddirectory? file) (and (not (file-symlink? file)) (file-directory? file)))
(define (file-nonempty? file) (and (file-readable? file) (not (file-directory? file)) (> (file-size file) 0)))
(define (directory-readable? dir) (and (file-readable? dir) (file-directory? dir)))
(define (directory-writable? dir) (and (file-writable? dir) (file-directory? dir)))
(define (safe-delete-file file) (and file (file-readable? file) (file-writable? file) (let ((fi (file-info file)) (uuid (user-effective-uid))) (and (eq? uuid (file-info:uid fi)) (delete-filesys-object file)))))
(define (my-directory-files-of-type type . rest) (delete #f (let ((uuid (user-effective-uid))) (map (lambda (file) (and (file-readable? file) (let ((fi (file-info file))) (and (eq? type (file-info:type fi)) (eq? uuid (file-info:uid fi)) file)))) (apply directory-files rest)))))
(define (nondelete-setmode mode . args)
  (decho "NONDELETE-SETMODE ~a ~a" mode (safe-string-join args " "))
  (let ((xfiles (map file-name-nondirectory (flatlist args))))
    (for-each (lambda (file) (if (member file xfiles) (set-file-mode file mode) (delete-file file))) (my-directory-files-of-type 'regular (cwd) #t)))
  (run (ls -la))
  #t )

(define (pdffile? pdffile) (and (file-readable? pdffile) (string-contains (run/string (file ,pdffile)) "PDF")))

(define (dirslashed . args) (safe-string-join args "/"))

(define (parent-directory dir) (and dir (let ((path (split-file-name dir))) (if (null? path) #f (apply dirslashed (reverse (cdr (reverse path))))))))

(define (ensure-directory dir) 
  (and 
   dir
   (string? dir)
   (or
    (file-exists? dir)
    (and (ensure-directory (parent-directory dir)) (file-writable? dir) (begin (create-directory dir) #t)) )
   (file-directory? dir)
   dir ) )

(define (readable-files . files) (delete #f (map (lambda (file) (and (file-exists? file) (file-readable? file) file)) files)))

(define (string->list-sub string len) (if (> len 0) (cons (string-ref string 0) (string->list-sub (substring string 1 len) (- len 1))) '()))
(define (string->list string) (and (string? string) (string->list-sub string (string-length string))))
(define (upcase string) (and (string? string) (list->string (map (lambda (char) (let ((asc (char->ascii char))) (ascii->char (if (and (> asc 96) (< asc 123)) (- asc 32) asc)))) (string->list string)))))
(define (downcase string) (and (string? string) (list->string (map (lambda (char) (let ((asc (char->ascii char))) (ascii->char (if (and (> asc 96) (< asc 123)) (- asc 32) asc)))) (string->list string)))))
(define my-user-info (user-info (user-effective-uid)))
(define my-user-name (user-info:name my-user-info))
(define hostname (string->symbol (system-name)))
(define main-opts '())
(define (opt key) (safe-cdr (assoc key main-opts)))
(define (setopt key val) (set! main-opts (alist-update key val main-opts)) val)
(define (cwd->patdirs) (let ((dir #f) (pdirs '())) (let loop ((dirs (reverse (split-file-name (cwd))))) (and (not (null? dirs)) (begin (set! dir (car dirs)) (set! pdirs (cons dir pdirs)) (and (string->number dir) (loop (cdr dirs))))) pdirs)))
(define patdirs #f)
(define patidstr #f)
(define patland #f)
(define patnum #f)
(define patidsym #f)

;; ####### CONFIG ########

(define ffiidir (case hostname ((genba) (dirslashed #f "var" "www")) (else (or (getenv "FFII") (dirslashed #f "ul" "sig" "srv" "res" "www" "ffii")))))
(define bindir (path-list->file-name (list "swpat" "bin") ffiidir))
(define my-path (list "/bin" "/usr/bin" bindir))
(define piktadir (path-list->file-name (list "swpat" "pikta") ffiidir))
(define patimgdir #f)

;; ######## NONCONFIG ########

(define epatdb #f)
(define (my-exec-path-search bin) (exec-path-search bin my-path))
(define pattxtroot (path-list->file-name (list "swpat" "pikta" "txt") ffiidir))
(define pattxtdir #f)

(define (patxxxfile xxx) (path-list->file-name (list (format #f "~a.~a" patidstr xxx)) patimgdir))
(define pattxtfile #f)
(define patpdffile #f)
(define save-ocrgrafs #f) ; save interted graphics into pattbzfile; set this if you have many terabytes of disk space and little time
(define patpdfsize 0)
(define patpdfsizefile #f)
(define pathtmlindex #f)

(define (patnumdirs land nr)
  (let* ((r3 (quotient nr 1000)) (m3 (modulo nr 1000)) (r6 (quotient r3 1000)) (m6 (modulo r3 1000)))
    (cons 
     (symbol->string land)
     (case land 
       ((de) (list (numpad r6 2) (numpad m6 3) (numpad m3 3)))
       ((ep) (list (numpad r3 4) (numpad m3 3))) ) ) ) )

(define (set-pattxtdir patid)
  (setq 
   pattxtdir
   (let-match 
    (regexp-search (rx (submatch (= 2 alnum)) (submatch (+ numeric))) (string patid))
    (match whole patland patnum)
    (and match (path-list->file-name (patnumdirs (string->symbol patland) (string->number patnum)) pattxtroot) ) ) ) )

(define patocrtexts '())

(define (set-patocrtexts) 
  (decho "SET-PATOCRTEXTS")
  (setq 
   patocrtexts 
   (ocrtexts-head (glob (path-list->file-name (list "???.txt") pattxtdir)))))

(define (squoted str) (string-append "'" (regexp-substitute/global #f (rx "'") (string-trim-right (string str)) 'pre "\\'" 'post) "'"))
(define (commed . rest) (safe-string-join rest ", "))
(define (sqcommed . rest) (apply commed (map squoted rest)))
(define (sqlbool val) (if val "true" "false"))

(define (sqlx cmd) (decho "SQLX: ~a" cmd) (pg:exec epatdb cmd))
(define (updatedb table key val . rest)
  (let ((np #f))
    (or (null? rest) (set! np (number (car rest))))
    (and 
     table
     key
     val
     (let ((update-cmd (gjoin " and " (format #f "update ~a set ~a = ~a where pat = ~a" table key (squoted val) (squoted patidstr)) (and np (format #f "np = ~a" np))))
	   (insert-cmd (format #f "insert into ~a (~a) values ( ~a )" table (apply commed (delete #f (list "pat" (and np "np") key))) (apply sqcommed (delete #f (list patidstr (and np np) val))))) )
       (let ((res (sqlx update-cmd)))
	 (or
	  (let-match (regexp-search (rx (: "UPDATE " (submatch (+ numeric)))) (pg:result res 'status)) (match whole num) (and match (> (string->number num) 0)))
	  (sqlx insert-cmd) ) ) ) ) ) )

(define (set-pdfdata)
  (set! pdfdata '())
  (let ((linerec #f) (line #f) (key #f) (value #f))
    (and
     (or
      (and (not (opt 'reget)) (grep "Claims" "data.txt"))	
      (and 
       patnum
       (my-exec-path-search "eppdfpgnums")
       (zero? (run (eppdfpgnums ,patnum) (> data.txt)))
       (grep "Claims" "data.txt") )
      (fail "failed to fetch espacenet data about PDF pages!") )
     (let ((port (open-input-file "data.txt")))
       (let loop ((line (read-line port)))
	 (and 
	  (string? line)
	  (begin
	    (and
	     (> (length (setq linerec (colonsplit line))) 1)
	     (setq key (car linerec))
	     (setq key (string->symbol key))
	     (setq value (cadr linerec))
	     (setq value (string->number value))
	     (let ((keysym (or (and (eq? key 'Desc) 'descr) (and (eq? key 'Claims) 'claims) (and (eq? key 'Drawing) 'drawings))))
	       (updatedb "pdfdata" keysym value)
	       (set! pdfdata (alist-update key value pdfdata)) ) ) 
	    (loop (read-line port)) ) ) ) 
       (close port) ) ) )
  pdfdata )
  
(define (pgpdf pg) (format #f "~a.pdf" (numpad pg 3)))

(define (recs2pages pg_from pg_tolt)
  (define pages '())
  (and
   (number? pg_from)
   (number? pg_tolt)
   (> pg_from 0)
   (> pg_tolt pg_from)
   (begin
     (setq pages '())
     (let loop ((pg pg_from))
       (and
	(< pg pg_tolt)
	(begin
	  (set! pages (append pages (list (pgpdf pg))))
	  (loop (+ 1 pg))
	  ) ) ) ) )
   pages )


(define (epgetpdfpages)
  (let ((epfile #f) (epurl #f) (patstr (numpad patnum 7)))
    (with-umask 
     #o002
     (let loop ((pg 1))
       (set! epfile (pgpdf pg))
       (set! epurl (string-append "http://l2.espacenet.com/dips/bns.pdf?PN=EP" patstr "&ID=EP+++" patstr "B1" "+I+&PG=" (number->string pg)))
       (and
	(zero? (run (wget -O ,epfile ,epurl)))
	(or (pdffile? epfile) (begin (safe-delete-file epfile) (fail "no more files")))
	(loop (+ pg 1))
	) ) )
    (allpgpdfs?)))

(define (or-pred pred . args) (and (not (null? args)) (or (let ((arg (car args))) (and (pred arg) arg)) (apply or-pred pred (cdr args)))))

(define (ephtml_ok) 
  (decho "EPTXT_OK")
  (and (grep "Abstract" "abstract.html") (or (opt 'absprob) (and (grep "Description" "desc.html") (grep "Claims" "claims.html") #t)) ) )

(define (eptxt_ok) 
  (decho "EPTXT_OK")
  (and (grep "Abstract" "abstract.txt") (or (opt 'absprob) (and (grep "Description" "desc.txt") (grep "Claims" "claims.txt") #t)) ) )

(define (epgettexts)
  (decho "EPGETTEXTS")
  (with-umask
   #o002
   (or
    (and (not (opt 'reget)) (ephtml_ok))
    (let ((opts (if (opt 'absprob) (list "-v" "-p" "a") (list "-v")))) (zero? (run (epgettexts ,@opts ,patidstr)))) )
   (for-each 
    (lambda (sym) 
      (let* ((base (symbol->string sym)) (html (string-append base ".html")) (txt (string-append base ".txt")))
	(and
	 (file-readable? html)
	 (file-writable? txt)
	 (begin
	   (setenv "TERM" "vt100")
	   (run (lynx -dump -nolist ,html) (> ,txt)) ) ) ) )
    '(claims desc abstract))
   (eptxt_ok) ) )

(define (pdfdone?)
  (decho "PDFDONE?")
  (set! patpdfsize (and (file-exists? patpdffile) (file-size patpdffile)))
  (if patpdfsize
      (with-umask #o002 (format-to-file patpdfsizefile "~d" patpdfsize))
      (set! patpdfsize (and (nonnull-list? pdfdata) (opt 'remotepdfs) (file-exists? patpdfsizefile) (let* ((port (open-input-file patpdfsizefile)) (line (read-line port))) (and (string? line) (string->number line))))) )
  (and
   (nonnull-list? pdfdata)
   patpdfsize
   (number? patpdfsize)
   (> patpdfsize 0)
   (setq pdfok #t) 
   ) )

(define (pdfval key) (let ((found (assoc key pdfdata))) (and found (cdr found))))

(define (write_pdfmarks file marks)
  (and
   (string? file)
   (file-writable? file)
   (list? marks)
   (let ((port (open-output-file file)))
     (map 
      (lambda (mark)
	(and
	 (list? mark)
	 (let ((page (car mark)) (title (cadr mark)))
	   (and
	    (number? page)
	    (string? title)
	    (format port "[ /Page ~d /Title (~a) /OUT pdfmark\n" page title)
	    ) ) ) )
      marks )
     (format port "[ /PageMode /UseOutlines /DOCVIEW pdfmark")
     (close port)
     )
   ) )

(define (pdcat target sources marks)
  (and
   (string? target)
   (file-writable? target)
   (nonnull-list? sources)
   (nonnull-list? marks)
   (begin
     (let ((fm "pdfmarks.txt"))
       (and
	(or
	 (write_pdfmarks fm marks)
	 (fail "no pdfmarks written") )
	(set! sources (append sources (list fm))) ) )
     (setq pdfok (zero? (run (gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite ,(string-append "-sOutputFile=" target) ,@sources))))
     ) ) )

(define srcfiles #f)
(define pg_clms #f)
(define (clms_pbm2png pbm png)
  (decho "CLMS_PBM2PNG ~a ~a" pbm png)
  (and
   pbm
   png
   (file-readable? pbm)
   (zero? (run (| (pnminvert ,pbm) (pbmreduce 3) (pnmtopng)) (> ,png)))
   ) )
(define (clms_pdf2png pdf png)
  (decho "CLMS_PDF2PNG ~a ~a" pdf png)
  (and
   pdf
   png
   (file-readable? pdf)
   (and
    (zero? (run (pdfimages ,pdf "dum")))
    (clms_pbm2png "dum-000.pbm" png)
    (begin (safe-delete-file "dum-000.pbm") #t)
    ) ) ) 

(define (files-bigger size . files)
  (decho "FILES-BIGGER ~a ~a" size (safe-string-join files " "))
  (or
   (null? files)
   (and 
    (let ((file (car files)))
      (and
       file
       (file-readable? file)
       (> (file-size file) size)
       (apply files-bigger size (cdr files)))))))

(define patclmspng1 #f)
(define patclmspng2 #f)

(define (clmspngs_ok) (decho "CLMSPNGS_OK") (files-bigger 5000 patclmspng1 patclmspng2))
(define (makeclmspngs) 
  (decho "MAKECLMSPNGS")
  (and
   pg_clms
   (number? pg_clms)
   (or
    (and (not (opt 'pngs)) (clmspngs_ok))
    (and
     (clms_pdf2png (gjoin "." (numpad pg_clms 3) "pdf") patclmspng1)
     (clms_pdf2png (gjoin "." (numpad (+ 1 pg_clms) 3) "pdf") patclmspng2)) 
    (let ((filebase (file-name-sans-extension patpdffile)))
      (and
       patpdffile
       (or (file-readable? patpdffile) (fail "cant find readable pdf file ~a" patpdffile))
       (zero? (run (pdfimages ,patpdffile ,filebase)))
       (and
	(clms_pbm2png (gjoin "." (gjoin "-" filebase (numpad (- pg_clms 1) 3)) "pbm") patclmspng1)
	(clms_pbm2png (gjoin "." (gjoin "-" filebase (numpad pg_clms 3)) "pbm") patclmspng2) ) ) ) )
   (clmspngs_ok)
   ) )

(define (allpgpdfs?) (> (length (setq srcfiles (glob "{0,1,2,3,4,5,6,7,8,9}*.pdf"))) pg_clms))
(define (epgetpdf)
  (decho "EPGETPDF")
  (with-umask 
   #o002
   (and
    (nonnull-list? (set-pdfdata))
    (setq pg_clms (pdfval 'Claims))
    (or (and (not (opt 'reget)) (or (pdfdone?) (allpgpdfs?)))
	(epgetpdfpages) )
    (makeclmspngs) 
    (pdcat patpdffile srcfiles (list (list 1 "title") (list (pdfval 'Desc) "description") (list (pdfval 'Claims) "claims") (list (pdfval 'Drawing) "drawings")))
    (map safe-delete-file srcfiles) )
   (and
    (pdfdone?) 
    (begin 
      (run (ls -l ,patpdffile ,patpdfsizefile))
      (format 1 "patpdfsize: ~a\n" (cat patpdfsizefile))
      #t) ) ) )

(define (make-patocrtexts)
  (set! patocrtexts '())
  (let ((ocrgrafs '()) (rawgrafs '()) (ok #f))
    (for-each safe-delete-file (glob "*.pbm"))
    (with-umask 
     #o002
     (and
      (begin
	(format 2 "Extracting raw graphics from ~a..." patpdffile)
	(zero? (run (pdfimages ,patpdffile ,(file-name-sans-extension patpdffile)))) ) 
      (begin 
	(format 2 "done\n")
	(set! rawgrafs (glob "*.pbm"))
	(nonnull-list? rawgrafs) )  
      (let ((n 0) (files '()))
	(for-each 
	 (lambda (rawgraf)
	   (set! n (+ 1 n))
	   (let ((ocrgraf (string-append (numpad n 3) ".pbm")))
	     (format 2 "Inverting ~a to ~a..." rawgraf ocrgraf)
	     (set! ok (zero? (run (pnminvert ,rawgraf) (> ,ocrgraf))))
	     (format 2 "~a\n" (if ok "done" "failed"))
	     (if ok (set! ocrgrafs (append ocrgrafs (list ocrgraf))) (safe-delete-file ocrgraf))
	     (safe-delete-file rawgraf) ) )
	 (ocrtexts-head rawgrafs) ) 
	(nonnull-list? ocrgrafs) )   
      (let ((n 0))
	(for-each 
	 (lambda (ocrgraf)
	   (set! n (+ 1 n))
	   (let ((ocrtext (path-list->file-name (list (string-append (file-name-sans-extension ocrgraf) ".txt")) pattxtdir))) 
	     (if
	      (begin (format 2 "recognizing ~a to ~a..." ocrgraf ocrtext)
		     (zero? (run (gocr -v -m 4 -o ,ocrtext ,ocrgraf))) )
	      (begin (format 2 "done\n")
		     (run (recode l1..u8 ,ocrtext))
		     (set! patocrtexts (append patocrtexts (list ocrtext))) )
	      (begin (format 2 "failed\n")
		     (safe-delete-file ocrtext) ) )
	     (safe-delete-file ocrgraf) ) ) 
	 (ocrtexts-head ocrgrafs) )
	(nonnull-list? patocrtexts) ) ) ) ) )

(define (trypatpdf2txt)
  (decho "TRYPATPDF2TXT")
  (if
   (opt 'ocr)
   (and 
    (or (file-readable? patpdffile) (fail "no PDF file ~a to ocr from!\n" patpdffile))
    (or (opt 'focr) (not (ocrdone?)))
    (make-patocrtexts) )
   #t ) )

(define lines '())
(define absdata '())
(define docdata '())
(define raw-absdata '())    
(define pdfdata '())

(define (abstract2lines)
  (decho "ABSTRACT2LINES")
  (let ((lines '()))
    (and
     (if (file-readable? "abstract.txt") #t (begin (format 1 "no such file: ~a\n" "abstract.txt") #f))
     (let ((port  (open-input-file "abstract.txt")))
       (let loop ((line (read-line port)))
	 (cond
	  ((or (not (string? line))) #f)
	  ((string-contains line "_____________") #t)
	  (else (and (setq line (string-trim-both line)) (> (string-length line) 0) (set! lines (append lines (list line)))) (loop (read-line port)))
	  ) )
       (close port) ) )
    lines ) )

(define (get-docdata)
  (decho "GET-DOCDATA")
  (and
   (or (file-readable? "data") (fail "no such file: ~a\n" "data"))
   (let ((port (open-input-file "data")))
     (let loop ((line (read-line port)))
       (and
	(string? line)
	(begin
	  (let ((match (regexp-search (rx (: (submatch (+ alpha)) ":" (* white) (submatch (+ any)))) line)))
	    (and
	     match
	     (alist-update
	      (string->symbol (match:substring 1))
	      (match:substring 2)
	      docdata ) ) )
	  (loop (read-line port))) ) )
     (close port) ) )
  docdata )

(define (lines2alist lines)
  (decho "LINES2ALIST")
  (let ((nbrlines (length lines)) (key "Invention") (match #f) (val #f))
    (let loop ((n 0))
      (and 
       (< n nbrlines)
       (let ((line (string-trim-both (nth lines n))))
	 (cond
	  ((setq match (regexp-search (rx (: (submatch (: alnum (+ any))) ":" (* blank) (submatch (* any)))) line)) (set! raw-absdata (alist-update key val raw-absdata)) (set! key (match:substring match 1)) (set! val (match:substring match 2)))
	  (else (set! val (gjoin " " val line)))
	  ) 
	 (loop (+ n 1)) ) ) ) 
    raw-absdata
    ) )

(define swpatprob #f)
(define pdfok #f)
(define txtok #f)
(define reqB1 #f)
(define progclm 0)


(define (set-swpatprob)
  (let ((confdir (dirslashed bindir "etc")) (file (or-pred file-readable? "claims.txt" "abstract.txt")))
    (and
     (or file (fail "no readable file available for probability analysis"))
     (if 
      (setq swpatprob (number (run/string (swpatprob -c ,confdir ,file))))
      (succ "analysis of ~a yielded swpatprob ~a" file swpatprob)
      (fail "no probability found in ~a" file) )
     (begin
       (or
	(and (file-readable? "claims.txt") (succ "claims analysed"))
	(and
	 (and (> swpatprob 0.80) (succ "high prob ~a, look at claims" swpatprob))
	 (or (zero? (run (epgettexts -v -p c ,patidstr))) (fail "couldn't fetch claims"))
	 (or (file-readable? "claims.html") (fail "claims not readable"))
	 (or (zero? (run (lynx -dump -nolist claims.html)  (> claims.txt))) (fail "lynx couldn't convert claims"))
	 (setq swpatprob (number (run/string (swpatprob -c ,confdir claims.txt))))
	 (succ "revised prob of ~a" swpatprob) ) )
       (set! 
	progclm
	(if-match 
	 (grep (rx (: "<BR>" (* white) (submatch (+ digit)) "." (** 1 3 white) (** 1 14 (| white alnum)) (+ white) (| "computer program" "program" "data structure" "Computerprogramm" "Computerprogrammprodukt" "Programmelement" "Datenstruktur" "programme") (+ white))) "claims.html")
	 (match whole num)
	 (number num) 0 ) )
       (updatedb "swpatprob" "prob" swpatprob)
       (format-to-file "prob" "~a" swpatprob)
       swpatprob ) ) ) )  

(define (alist2data raw-absdata)
  (decho "ALIST2DATA")
  (let ((tup '()) (key #f) (keysym #f))
    (if
     (null? raw-absdata)
     (begin (format 1 "ep ~a: no data available!\n" patidstr) #f)
     (begin
       (let loop ((alist raw-absdata))
	 (and
	  (nonnull-list? alist)
	  (begin
	    (set! tup (car alist))
	    (set! key (car tup))
	    (set! 
	     keysym 
	     (cond
	      ((string-contains-ci key "Invention") 'invention) 		  
	      ((string-contains-ci key "Equivalent") 'equiv)
	      ((string-contains-ci key "EC Class") 'ecclass)
	      ((string-contains-ci key "IPC Class") 'ipclass)
	      ((string-contains-ci key "Priority") 'prior)
	      ((string-contains-ci key "Application") 'applnr)
	      ((string-contains-ci key "Requested") 'requested)
	      ((string-contains-ci key "Applicant") 'applicant)
	      ((string-contains-ci key "Inventor") 'inventor)
	      ((string-contains-ci key "Publicat") 'pubdat)
	      ((string-contains-ci key "Patent N") 'patentnr)
	      (else #f)
	      ) )
	    (if 
	     keysym
	     (let ((val (cdr tup)))
	       (and
		(not (eq? keysym 'patentnr))
		swpatprob 
		(> swpatprob 0.95)
		(cond
		 ((member keysym '(invention pubdat requested applnr))
		  (updatedb "absdata" keysym val))
		 ((member keysym '(inventor applicant prior ipclass ecclass))
		  (let ((np 0))
		   (for-each 
		    (lambda (str)
		      (let ((sm #f) (land #f))
			(and 
			 (member keysym '(inventor applicant))
			 (setq sm (regexp-search (rx (: (submatch (* any)) (+ blank) "(" (submatch (= 2 alpha)) ")")) str))
			 (setq land (match:substring sm 2))
			 (setq str (match:substring sm 1))
			 (updatedb keysym 'land land np)))
		      (updatedb keysym keysym str np)
		      (set! np (+ np 1)) )
		    (semcolsplit val) ) 
		   #t ) ) ) )
	       (set! absdata (alist-update keysym val absdata)) )
	     (format 1 "warning: unknown abstract key ~a!!!\n" key) )
	    (loop (cdr alist)) ) ) )
       (let ((port (open-output-file "absdata")))
	 (format port (safe-string-join (map (lambda (tup) (format #f "~a: ~a" (symbol->string (car tup)) (cdr tup))) absdata) "\n"))
	 (close port) )
       (set! reqB1 (and (string-contains (string (safe-cdr (assoc 'requested absdata))) "B1") #t))
       (and (assoc 'prior absdata) absdata) ) ) ) ) 

(define (get-absdata) 
  (and
   (succ "GET-ABSDATA")
;   (or (not (nonnull-list? absdata)) (fail "absdata alist is empty"))
   (or (epgettexts) (fail "can't get text/html data on ep patent"))
   (set-swpatprob)
   (if (and (opt 'absprob) (> swpatprob 0.95)) (begin (setopt 'absprob #f) (epgettexts)) #t)
   (or (nonnull-list? (setq raw-absdata (lines2alist (abstract2lines)))) (fail "can't get raw absdata"))
   (or (nonnull-list? (setq absdata (alist2data raw-absdata))) (fail "can't get final absdata")) ) )

(define (pattxtfile? file) (setq txtok (and (file-readable? file) (or (and (> (file-size file) 5000) (grep (rx (| "claim" "Anspruch" "revendication" "patent" "brevet")) file)) (begin (delete-file file) (fail "invalid ocr result ~a" file))))))
(define (ocrdone?) 
  (decho "OCRDONE?")
  (or
   (pattxtfile? pattxtfile)
   (nonnull-list? (set-patocrtexts)) ) ) 

(define (cat-ocrtext)
  (decho "CAT_OCRTEXT")
  (or
   (and
    (pattxtfile? pattxtfile)
    (cat pattxtfile) )
   (and
    (nonnull-list? (set-patocrtexts))
    (format-tee
     pattxtfile
     (safe-string-join
      (list
       (html-env0 "ul" (safe-string-join (map (lambda (lst) (let* ((key (car lst)) (tit (cadr lst)) (val (pdfval key))) (and val (html-env0 "li" (html-env "a" (list (list "href" (format #f "#bpg~d" val))) tit))))) (list (list 'Desc  "Description") (list 'Claims "Claims"))) "\n"))
       (safe-string-join
	(let ((n 0)) (map (lambda (file) (set! n (+ 1 n)) (format #f "<div align=center><a name=bpg~d>gocr ~a-~a</a></div>\n<p><pre>\n~a\n</pre></p>" n patidstr (numpad n 3) (cat file))) patocrtexts)) 
	 "\n<hr>\n") )
      "\n\n") ) )
   (format #f "<a href=\"~a.txt\">OCR results</a> expected soon." patidstr) ) )

(define (indexbody)
  (decho "INDEXBODY")
  (or
   (and 
    (nonnull-list? absdata)
    (file-readable? "claims.txt")
    (file-readable? "desc.txt")
    (safe-string-join
     (list
      (format 
       #f 
       "<a href=\"../../../\">up</a><hr>\n<p><h1>~a ~a:<br>~a</h1>\n<p><ul>\n<li><a href=\"#data\">Data Sheet</a></li>\n<li><a href=\"#appli\">Application (text)</a></li>\n~a</ul>\n<p><h2><a name=\"data\">Data Sheet</a></h2>\n<p><dl>\n~a\n</dl>\n<p><h2><a name=appli>Application</a></h2>\n<p><ul>\n<li><a href=\"#adsc\">Description</a></li>\n<li><a href=\"#aclm\">Claims</a></li>\n</ul>\n<p><h3><a name=adsc>Description</a></h3>\n<p><pre>\n~a\n</pre>\n<p><h3><a name=aclm>Claims</a></h3>\n<p><pre>\n~a\n</pre>"
       (upcase (symbol->string patland))
       (numpad patnum 7)
       (string (safe-cdr (assoc 'invention absdata)))
       (if pdfok "<li><a href=\"#grant\">Granted Version (graphics, ocr result)</a></li>\n" "")
       (safe-string-join 
	(map
	 (lambda (tup)
	   (and (pair? tup) (format #f "<dt>~a:<dd>~a" (string (car tup)) (string (cdr tup)))))
	 raw-absdata)
	"\n")
       (cat "desc.txt")
       (cat "claims.txt") )
      (if 
       pdfok 
       (format 
	#f 
	"<p><h2><a name=grant>Granted Patent</h2><br>\n<p><h3>Claims as Graphics</h3>\n<p><ul>\n<li><a href=\"claims1.png\">Claims Pg 1</a>\n<li><a href=\"claims2.png\">Claims Pg 2</a>\n</ul>\n<p><h3>OCR result</h3>\n<p><pre>\n~a\n</pre>"
	(cat-ocrtext) )
       "")
      "<ul>\n<li><a href=\"http://www.ffii.org/\">FFII</a></li>\n<li><a href=\"http://swpat.ffii.org\">Protecting Information Innovation Against the Abuse of the Patent System</a></li>\n<li><a href=\"http://swpat.ffii.org/patents/txt/\">Overview of European Software Patents</a></li>\n</ul>"
      )
     "\n<hr>\n"
     ) ) 
   (fail "warning: empty HTML body!") ) )

(define (index_html)
  (decho "creating index.html for ~a\n" patidstr)
  (or
   (and 
    (let ((body (indexbody)))
      (and
       (nonnull-string? body)
       (file-writable? pathtmlindex)
       (with-umask 
	#o002
	(and (file-readable? pathtmlindex) (file-symlink? pathtmlindex) (delete-filesys-object pathtmlindex))
	(format-to-file 
	 pathtmlindex 
	 "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\"><!-- -*- coding: utf-8 -*- -->\n<html>\n<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">\n<title>~a~a</title>\n<body>\n~a\n</body>\n</html>"
	 (upcase (symbol->string patland))
	 (numpad patnum 7)
	 body)  
	(succ "~a successfully generated." pathtmlindex) ) ) ) )   
   (fail "unable to write ~a.\n" pathtmlindex) ) )

(define (str2val str) (and (string? str) (case (string->symbol str) ((+) #t) ((-) #f) (else str))))
(define (args2opts . args)
  ;; (args2opts "-abc" "+def" "ghi:12")
  (let ((opts '()))
    (and
     (or (nonnull-list? args) (fail "no commandline arguments available"))
     (let loop ((arg (car args)))
       (and
	(or (string? arg) (fail "unusable commandline argument ~a" arg))
	(succ "analysing argument ~a" arg)
	(match-cond
	 ((regexp-search (rx (: (submatch (+ alnum)) ":::" (submatch (+ (~ blank))))) arg)
	  (match whole key valstr)
	  (let ((vals '()) (recs ((infix-splitter (rx "::")) valstr)))
	    (for-each (lambda (rec) (set! vals (alist-update (string->symbol (car rec)) (map str2val (cdr rec)) vals))) recs)
	    (set! opts (alist-update key vals opts)) ) 
	  #t )
	 ((regexp-search (rx (: (submatch (+ alnum)) "::" (submatch (+ (~ blank))))) arg)
	  (match whole key vals)
	  (set! opts (alist-update (string->symbol key) (map str2val (colonsplit vals)) opts))
	  #t )
	 ((regexp-search (rx (: (submatch (+ alnum)) ":" (submatch (+ (~ blank))))) arg)
	  (match whole key val)
	  (set! opts (alist-update (string->symbol key) (str2val val) opts))
	  #t )
	 (else (fail "can't interpret commandline argument ~a" arg) ) ) ) 
       (and
	(nonnull-list? (setq args (cdr args)))
	(loop (car args)) ) ) ) 
    opts ) ) 

(define (epgcat . cmdargs)
  (decho "EPGCAT")
  (let ((patdirs (cwd->patdirs))
	(patidstr (apply string-append patdirs))
	(patland (string->symbol (car patdirs)))
	(patnum (string->number (apply string-append (cdr patdirs))))
	(patidsym (string->symbol patidstr))
	(patimgdir (path-list->file-name (cons "img" patdirs) piktadir))
	(pattxtdir (path-list->file-name (cons "txt" patdirs) piktadir))
	(pattxtfile (path-list->file-name (list (format #f "~a.txt" patidstr)) pattxtdir))
	(patpdffile (patxxxfile "pdf"))
	(patpdfsizefile (path-list->file-name (list ".pdfsize") patimgdir))
	(pathtmlindex (path-list->file-name (list "index.html") pattxtdir))
	(patclmspng1 (path-list->file-name (list "claims1.png") pattxtdir))
	(patclmspng2 (path-list->file-name (list "claims2.png") pattxtdir))
	(lines '())
	(absdata '())
	(docdata '())
	(raw-absdata '())    
	(pdfdata '())
	(srcfiles '())
	(pg_clms #f)
	(pattxtdir #f)
	(pattxtfile #f)
	(patpdffile #f)
	(save-ocrgrafs #f)
	(patpdfsize 0)
	(patpdfsizefile #f)
	(pathtmlindex #f)
	(patocrtexts '())
	(pages '())
	(srcfiles #f)
	(pg_clms #f)
	(patclmspng1 #f)
	(patclmspng2 #f)
	(lines '())
	(absdata '())
	(docdata '())
	(raw-absdata '())    
	(pdfdata '())
	(swpatprob #f)
	(pdfok #f)
	(txtok #f)
	(reqB1 #f)
	(progclm 0)
	(main-opts (apply args2opts cmdargs))
	)
  (with-umask 
   #o002 
   (and
    (ensure-directory pattxtdir)
    (with-cwd 
     pattxtdir 
     (and
      (or (get-absdata) (fail "can't get absdata"))
      (or (not (opt 'absprob)) (fail "fetched absdata for probability check, enough for this time."))
      (ensure-directory patimgdir)
      (with-cwd 
       patimgdir
       (and (epgetpdf) (trypatpdf2txt))
       (nondelete-setmode #o664 "data.txt" ".pdfsize" patpdffile) )
      (or (index_html) (fail "couldn't create html index")) )
     (for-each safe-delete-file (if (file-exists? "index.html") '() (glob "*.html")))
     (nondelete-setmode #o664 pattxtfile "data" "absdata" "prob" (glob "index.??.{html,txt}") (glob "{desc,index,abstract,claims}.{html,txt}") (glob "claims?.png"))
     ) ) )
  (sqlx (format #f "update swpatprob set reqb1 = ~a, pdfok = ~a, pdfsize = ~d, txtok = ~a, progclm = ~d where pat = '~a'" (sqlbool reqB1) (sqlbool pdfok) (number patpdfsize) (sqlbool txtok) (number progclm) patidstr))
  ) ) 

(define (epgcatall . cmdargs)
  (and
   (or (not (null? cmdargs)) (fail "need argument(s) on commandline"))
   (let ((arg (car cmdargs)) (match #f) (dir #f) (port #f) (patland "ep") (patnumlen 7) (patnum 0) (patnummax 1300000) (last-valid-patnum 0) (patidstr #f) (file #f) (next-patidstr (lambda () #f)) (patid-in-range (lambda () #f)) (cleanup (lambda () #f)))
     (set! match (regexp-search (rx (: (submatch (= 2 alpha)) (submatch (+ numeric)) ".." (submatch (* numeric)))) arg))
     (and
      (or 
       (and
	match
	(begin
	  (set! patland (match:substring match 1))
	  (set! patnum (match:substring match 2))
	  (set! patnumlen (string-length patnum))
	  (set! patnum (string->number patnum))
	  (set! last-valid-patnum patnum)
	  (set! patnummax (string->number (match:substring match 3)))
	  (or (and patnummax (number? patnummax) (> patnummax patnum)) (set! patnummax #f))
	  (set! next-patidstr (lambda () (if (and dir (file-readable? (dirslashed dir "absdata"))) (set! last-valid-patnum patnum)) (set! patnum (+ 1 patnum)) (string-append patland (numpad patnum patnumlen))))
	  (set! patid-in-range (lambda (str) (<= patnum (or patnummax (+ last-valid-patnum 35000)))))
	  #t ) )
       (and
	(file-readable? arg)
	(begin
	  (set! port (open-input-file arg))
	  (set! next-patidstr (lambda () (read-line port)))
	  (set! patid-in-range (lambda (str) (and str (string? str))))
	  (set! cleanup (lambda () (close port)))
	  #t ) ) 
       (fail "cant make sense of argument ~a" arg) )
      (let loop ((patidstr (next-patidstr)))
	(and 
	 (patid-in-range patidstr)
	 (set-pattxtdir patidstr)
	 (ensure-directory pattxtdir)
	 (with-cwd pattxtdir (succ "processing ~a" pattxtdir) (if (apply epgcat (cdr cmdargs)) (succ "OK") (succ "KO")))
	 (loop (next-patidstr)) ) ) 
      (cleanup) ) ) ) )

(define (main cmdline)
  (let ((cmdargs (cdr cmdline)))
    (and
     (or (not (null? cmdargs)) (fail "need argument(s) on commandline"))
     (setq epatdb (case hostname ((genba) (pg:connect "epat" my-user-name)) (else (pg:connect "epat" my-user-name #f "localhost" 3333))))
     (let ((cmd (string->symbol (car cmdargs))))
       (case cmd
	 ((cwd) (apply epgcat (cdr cmdargs)) )
	 ((all) (apply epgcatall (cdr cmdargs)) )
	 (else (fail "unknown command ~a" cmd))
	 ) 
       (pg:disconnect epatdb) ) ) ) )
  
;;structure epgcat
))

; Local Variables:
; mode: scheme
; coding: utf-8
; srcfile://
; End:
