#!/usr/bin/perl 

use LWP::UserAgent;
#use HTML::Parse;
#use HTML::FormatText;

$pat_num = $ARGV[0];
$pat_num =~ tr/a-z/A-Z/;
print $pat_num;
if (length($pat_num) < 6 || length($pat_num) > 10)
   {
   print "keine gueltige EP-Nummer\n";
   exit;
   }
$url_nav="http://l2.espacenet.com/dips/viewnav?PN=" . $pat_num . "&CY=ep&LG=en&DB=EPD";

##########################################   ABSTRACT
   $dok_typ="abstract";
   get_frame_nav($url_nav);
   get_abstract_url(@datei);
   get_abstract($pat_num, $url_abstract, $dok_typ);
   print "abstract: $url_abstract\n";

##########################################   CLAIM
   $dok_typ="claims";
   get_frame_nav($url_nav);
   get_patnrs(@datei);
   get_claim($FTDB, $PNP, $PN, $pat_num, $dok_typ);

##########################################   DESCRIPTION
   $dok_typ="desc";
   get_desc($FTDB, $PNP, $PN, $pat_num, $dok_typ);


sub get_claim($FTDB, $PNP, $PN, $pat_num, $dok_typ)
   {
   my $url = "http://l2.espacenet.com/dips/claims?LG=en&CY=ep&DB=EPD&" . $PNP . "&" . $PN . "&FTDB=" . $FTDB ;
   print "claim: $url\n";
   print "fetching claims: $pat_num\n";
      my $ua = new LWP::UserAgent;
      $ua->agent('Mozilla/5.0'); 
      my $req_claim=new HTTP::Request 'Get' => "$url";
      my $res_claim=$ua->request($req_claim);
      my $result_claim_html=$res_claim->content();
      open(CLAIM_HTML, "> $dok_typ.html");
      print CLAIM_HTML $result_claim_html;
      close(CLAIM_HTML); 
   }

sub get_desc($FTDB, $PNP, $PN, $pat_num, $dok_typ)
   {
   my $url = "http://l2.espacenet.com/dips/desc?LG=en&CY=ep&DB=EPD&" . $PNP . "&" . $PN . "&FTDB=" . $FTDB ;
print "desc: $url\n";
   print "fetching description: $pat_num\n";
      my $ua = new LWP::UserAgent;
      $ua->agent('Mozilla/5.0');
      my $req_claim=new HTTP::Request 'Get' => "$url";
      my $res_claim=$ua->request($req_claim);
      my $result_claim_html=$res_claim->content();
      open(CLAIM_HTML, "> $dok_typ.html");
      print CLAIM_HTML $result_claim_html;
      close(CLAIM_HTML); 
   }


sub get_patnrs(@datei)
   {
   my $anzahl = @datei;
   my $i=0;
   while ($i < $anzahl)
      {
print "Datei: $datei[$i]";
      #if ($datei[$i] =~ /PN\=(DE|EP|US|WO|FR)[0-9]{5,9}/ && $i==13)
      if ($datei[$i] =~ /PN\=(DE|EP|US|WO|FR)[0-9]{5,9}/)
         {
         @string = split(/&/, $datei[$i]);
         $PNP = $string[2]; 
         $PN  = $string[3];
         }
      if ($i == 155)
         {
         $FTDB=$datei[$i];
         }
      $i++;
      }
#   if (length($PNP) != 13)
#      {
#      print "Fehlgeschlagen: $PNP \n";
#      exit;
#      }
#   else
#      {
      return $PNP, $PN, $FTDB;
   #print "$PNP, $PN, $FTDB";
#      }
   } # ende get_patnrs

sub get_abstract_url(@datei)
   {
   my $i=0;
   my $anzahl = @datei;
   while ($i< $anzahl)
      {
      if ($datei[$i] =~ /dips\/abstract/ && $i==13) 
         {
         $url_abstract="http://l2.espacenet.com" . $datei[$i]; 
         return $url_abstract;
         }
      $i++;
      } 
   }

sub get_abstract($pat_num, $url_abstract, $dok_typ)
   {
   print "fetching abstract: $pat_num\n";
      my $ua = new LWP::UserAgent;
       $ua->agent('Mozilla/5.0');
      my $req_abstr_html=new HTTP::Request 'Get' => "$url_abstract";
      my $res_abstr_html=$ua->request($req_abstr_html);
      my $result_abstr_html= $res_abstr_html->is_success() ?
      $res_abstr_html->content() :
      "Error: " . $res_abstr->code() . " " . $res_abstr->message();        
 
      open(ABSTRACT_HTML, "> $dok_typ.html");
      print ABSTRACT_HTML $result_abstr_html;
      close(ABSTRACT_HTML); 
   }


sub get_frame_nav($url_nav)
      {
      my $ua = new LWP::UserAgent;
       $ua->agent('Mozilla/5.0');
      $req_abstract= new HTTP::Request 'GET' => "$url_nav";
      $res_abstract= $ua->request($req_abstract);
      $result_abstract=$res_abstract->content();

      @datei=split(/"/, $result_abstract);
      return @datei;
      }

# Local Variables:
# coding: utf-8
# End:
