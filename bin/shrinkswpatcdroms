#!/usr/bin/scsh \
-e main -s
!#

; # SUBR

(define-syntax setq
  (syntax-rules ()
    ((setq var value)
     (begin
       (set! var value)
       var))))

(define (rtrim str) (define p (string-length str)) (let loop () (and (> p 0) (char-set-contains? char-set:whitespace (string-ref str (- p 1))) (begin (set! p (- p 1)) (loop)))) (substring str 0 p))

(define (numpad num len . rest)
  (let* ((pad (or (and (not (null? rest)) (car rest)) #\0))
	 (str (number->string num))
	 (padlen (- len (string-length str))) )
    (if (> padlen 0)
	(string-append (make-string padlen pad) str)
	str ) ) )

(define (file-harddirectory? file) (and (not (file-symlink? file)) (file-directory? file)))
(define (directory-readable? dir) (and (file-readable? dir) (file-directory? dir)))
(define (directory-writable? dir) (and (file-writable? dir) (file-directory? dir)))

(define (create-path-list path-list) (or (null? path-list) (let ((dir (car path-list))) (or (and (file-exists? dir) (if (file-directory? dir) #t (begin (delete-file dir) #f))) (create-directory dir)) (with-cwd dir (create-path-list (cdr path-list))))))
(define (force-create-directory dir) (let ((cwd (cwd)) (path-list (split-file-name dir))) (or (null? path-list) (begin (and (equal? (car path-list) "") (begin (set! cwd "/") (set! path-list (cdr path-list)))) (with-cwd cwd (create-path-list path-list))))))
(define (force-open-output-file file) (and (force-create-directory (file-name-directory file)) (file-writable? file) (open-output-file file)))

(define (find-files-sub prefix dir) (with-cwd dir (apply append (map (lambda (fbas) (let ((file (path-list->file-name (list fbas) prefix))) (or (and (file-directory? fbas) (find-files-sub file fbas)) (list file)))) (directory-files)))))

(define (find-files dir) (find-files-sub dir dir))

(define (find-files-match-sub prefix dir match) (with-cwd dir (apply append (map (lambda (fbas) (let ((file (path-list->file-name (list fbas) prefix))) (case (file-type fbas) ((directory) (find-files-match-sub file fbas match)) (else (if (string-match match fbas) (list file) '()))))) (directory-files)))))
(define (find-files-match dir match) (and (directory-readable? dir) (find-files-match-sub dir dir match)))

(define (move-relpath-to-basedir relpath dir)
  (and
   (file-readable? relpath)
   (not (file-directory? relpath))
   (let ((targetdir (path-list->file-name (list (file-name-directory relpath)) dir)))
     (force-create-directory targetdir)
     (let ((err (run (mv -v ,relpath ,targetdir))))
       (or (= err 0)
	   (begin (format 2 "failed to move file ~a to dir ~a\n" relpath targetdir) (exit err)) ) ) ) ) )

;; CONFIG

(define hostname (string->symbol (rtrim (run/string (hostname)))))
(define ffiidir (case hostname ((genba) "/var/www") (else (or (getenv "FFII") "/ul/sig/srv/res/www/ffii"))))
(define piktadir (path-list->file-name (list "swpat" "vreji" "pikta") ffiidir))
(define pattxt (path-list->file-name (list "txt") piktadir))
(define patimg (path-list->file-name (list "img") piktadir))
(define cdromnum 0)
(define cdserdir (path-list->file-name (list "cdimg" "epatpdfs") ""))
(define cddir)
(define (nth-cddir n) (path-list->file-name (list (numpad n 4)) cdserdir))
(define (set-cddir) (setq cddir (nth-cddir cdromnum)) (force-create-directory cddir) cddir)
(define cdimg)
(define (nth-cdimg n) (string-append (nth-cddir) ".iso"))
(define (set-cdimg) (setq cdimg (string-append (set-cddir) ".iso")))
(define cddirfile)
(define (nth-cddirfile n) (path-list->file-name (list "FILES") (nth-cddir n)))
(define (set-cddirfile) (setq cddirfile (nth-cddirfile cdromnum)))
(define cdromsize 650000000)
(define totalsizefile)
(define (nth-totalsizefile n) (path-list->file-name (list ".totalsize") (nth-cddir n)))
(define (set-totalsizefile) (setq totalsizefile (nth-totalsizefile cdromnum)) (or (file-readable? totalsizefile) (force-format-to-file totalsizefile "~d" 0)) totalsizefile)
(define totalsize)
(define (nth-totalsize n) (let ((file (nth-totalsizefile n))) (or (and (file-readable? file) (let* ((port (open-input-file file)) (line (read-line port))) (close port) (and (string? line) (string->number line)))) (let ((num (find-files-totalsize n))) (and num (> num 0) (format-to-file file "~d" num)) num))))
(define (find-files-totalsize n) (let ((files (find-files-match (nth-cddir n) (rx (: "ep" (= 7 numeric) ".pdf"))))) (and files (apply + (map file-size files)))))

(define (set-totalsize) (setq totalsize (or (nth-totalsize cdromnum) 0)))

(define (initcdrom n) (set! cdromnum n) (set-cddir) (set-cdimg) (set-cddirfile) (set-totalsizefile) (set-totalsize) (format 2 "starting with cdrom #~d size ~d\n" cdromnum totalsize) #t)
(define (nextcdrom n)
  (set! cdromnum n)
  (let loop ((num cdromnum))
    (set! totalsize (nth-totalsize num))
    (and 
     totalsize 
     (> totalsize cdromsize)
     (begin (set! cdromnum (+ num 1))
	    (loop cdromnum) ) ) )
  (initcdrom cdromnum) )


(define patidsfile (path-list->file-name (list "PATIDS") cdserdir))

; # IMPLEM

(define (patnumdirs land nr) (let* ((r3 (quotient nr 1000)) (m3 (modulo nr 1000)) (r6 (quotient r3 1000)) (m6 (modulo r3 1000))) (cons (symbol->string land) (case land ((de) (list (numpad r6 2) (numpad m6 3) (numpad m3 3))) ((ep) (list (numpad r3 4) (numpad m3 3)))))))

(define (patdir arg)
  (let ((match (string-match (rx (submatch (= 2 alphanum)) (submatch (+ numeric))) arg)))
    (and 
     match 
     (let ((patland (string->symbol (match:substring match 1))) (patnum (string->number (match:substring match 2))))
       (path-list->file-name (patnumdirs patland patnum) "img") ) ) ) )

(define (patpdf arg)
  (let ((match (string-match (rx (submatch (= 2 alphanum)) (submatch (+ numeric))) arg)))
    (and 
     match 
     (let ((patland (string->symbol (match:substring match 1))) (patnum (string->number (match:substring match 2))))
       (path-list->file-name (append (patnumdirs patland patnum) (list (string-append arg ".pdf"))) "img") ) ) ) )

(define (chdir? dir) (and (directory-readable? dir) (begin (chdir dir) #t)))
(define debug #f)
(define (format-to-file file str . args) (let ((port (open-output-file file))) (apply format port str args) (close port)))
(define (force-format-to-file file str . args) (force-create-directory (file-name-directory file)) (apply format-to-file file str args))
(define (pdffile? pdffile) (and (file-readable? pdffile) (string-match "PDF" (run/string (file ,pdffile)))))

(define (pdfsize-cleanup listfile) (let ((port (open-input-file listfile))) (let loop ((line (read-line port))) (and (string? line) (let ((match #f)) (set! match (string-match (rx (: "ep" (submatch (= 4 numeric)) (submatch (= 3 numeric)))) line)) (and match (let* ((ms1 (match:substring match 1)) (ms2 (match:substring match 2)) (dir (path-list->file-name (list "img" "ep" ms1 ms2))) (pdffile (path-list->file-name (list (string-append "ep" ms1 ms2 ".pdf")) dir)) (pdfsizefile (path-list->file-name (list ".pdfsize") dir))) (if (file-readable? pdffile) (format-to-file pdfsizefile "~d" (file-size pdffile)) (delete-filesys-object pdfsizefile)) (run (ls -l ,pdffile ,pdfsizefile)))) (loop (read-line port))))) (close port)))

(define (open-next-cddirfile n) (and (nextcdrom n) (force-open-output-file cddirfile)))

(define (main cmdline)
  (and
   (directory-writable? cdserdir)
   (chdir? piktadir)
   (file-readable? patidsfile)
   (let ((patidsport (open-input-file patidsfile)) (cddirport (open-next-cddirfile 1)))
     (and 
      cddirport
      (let loop ((line (read-line patidsport)))
	(and 
	 (string? line)
	 (let ((pdffile (patpdf line))) 
	  (and
	   (pdffile? pdffile) 
	   (let ((pdfsize (file-size pdffile)))
	     (format-to-file (path-list->file-name (list ".pdfsize") (file-name-directory pdffile)) "~d" pdfsize)
	     (move-relpath-to-basedir pdffile cddir)
	     (format cddirport "~a\n" pdffile)
	     (set! totalsize (+ totalsize pdfsize))
	     (format-to-file totalsizefile "~d" totalsize)
	     (format 2 "total size: ~d\n" totalsize)
	     (if (< totalsize cdromsize)
		 #t
		 (begin
		   (close cddirport)
		   (format-to-file (path-list->file-name (list (string-append "EPATPDFS" (numpad cdromnum 4))) cddir) "Foerderverein fuer eine Freie Informationelle Infrastruktur\nFederation for a Free Information Infrastructure\nhttp://www.ffii.org/\n\nNetzaktion Patenttexterkennung\nNetaction Free Patent Texts\nhttp://swpat.ffii.org/girzu/fatri/\n\nEPATPDFS CDROM ~a" (numpad cdromnum 4))
		   (fork (with-cwd cddir (run (mkisofs -v -R -r -P "FFII e.V., http://www.ffii.org" -volset epatpdfs -volset-size 2000 -volset-seqno ,cdromnum -o ,cdimg "."))))
		   (setq cddirport (open-next-cddirfile (+ 1 cdromnum))) ) ) ) ) 
	  (loop (read-line patidsport)) ) ) ) )
     (close cddirport)
     (close patidsport) ) ) )


; Local Variables:
; mode: scheme
; coding: utf-8
; End:
