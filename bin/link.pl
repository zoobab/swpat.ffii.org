#!/usr/bin/perl -w
require HTML::LinkExtor;
   $p = HTML::LinkExtor->new(\&cb);

open(DESC, "> desc.url");
open(CLAIM, "> claim.url");
open(ABSTRACT, "> abstract.url");

      sub cb {
      my($tag, $par, $url) = @_;
         if ($url =~ /http:\/\/l2.espacenet.com\/dips\/desc/)
            {
            print DESC "$url\n";
            }
         if ($url =~ /http:\/\/l2.espacenet.com\/dips\/claims/)
            {
            print CLAIM "$url\n";
            }
         if ($url =~ /http:\/\/l2.espacenet.com\/dips\/viewer/)
            {
            print ABSTRACT "$url\n";
            }
      }
   $p->parse_file($ARGV[0]);
