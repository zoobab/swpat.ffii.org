<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente vs Parliamentarische Demokratie

#descr: Wir erläutern die derzeitige Situation im Hinblick auf Ideenmonopole,
insbesondere im Kontext des Richtlinienentwurfs %(q:über die
Patentierbarkeit computer-implementierte Erfindungen)
(Softwarepatentrichtlinie), der zu einem Testfall darüber geworden
ist, wie viel Parlamente in der heutigen europäischen Gesetzgebung zu
sagen haben.

#banana0503: Nein zur Bananen-Union, Nein zu Softwarepatenten

#rWa: Derzeitige Situation

#eaW: Während das Europäische Parlament %(ep:einen klaren Ausschluss) von
%(si:Softwarepatenten) vorgeschlagen hat, haben die Kommission und der
Ministerrat %(ci:den Parlamentsvorschlag ignoriert) und stattdessen
den %(pp:kompromisslosesten Pro-Patent-Text des bisherigen verfahrens)
am 18. Mai 2004 auf einer Ratssitzung mithilfe eines von der
%(ri:deutschen Bundesregierung und der Europäischen Kommission
gemeinsam durchgezogenen Überrumpelungsmanövers durchgesetzt).
Wiederholte Forderungen mehrerer nationaler Parlamente und Regierungen
nach Neuverhandlung dieser Position führten zu langen Verzögerungen,
aber der Ministerrat lehnte die geforderte Neuauszählung der Stimmen
ab und %(co:erklärte nach einem beispiellosen Schmierentheater am 7.
März 2005)  den %(q:Gemeinsamen Standpunkt) für angenommen.  Eine
Woche zuvor hatte die %(ce:Kommission) die einhellige Forderung des
Europäischen Parlaments nach Neustart des Verfahrens %(rs:ohne
Begründung zurückgewiesen).

#odW: Das Europäische Parlament wird voraussichtlich am 10. April den
%(lq:rechtlich fragwürdigen) %(q:Gemeinsamen Standpunkt) des Rates in
Empfang nehmen und auf dieser Basis eine %(e:zweite Lesung) beginnen. 
Designierter Berichterstatter ist der frühere französische
Premierminister %(MR).  Rocard wird voraussichtlich vorschlagen, die
%(ep:Gegenposition des Parlaments vom September 2003) erneut zu
bekräftigen.  Anfang Juli dürfte das Parlament über den Rocard-Bericht
abstimmen.  Die Hürden sind dabei höher als in der ersten Lesung: 
abwesende Parlamentarier zählen als Befürworter der Ratsposition. Wenn
es Rocard gelingt, alle %(nt:essentiellen Änderungen) durch die zweite
Lesung durchzubringen, wird es erneut zum Zusammenstoß mit Rat und
Kommission im Rahmen eines Schlichtungsverfahrens kommen, wobei dann
das Parlament sich jedoch in einer relativ starken Position befinden
wird.  Eine wünschenswerte Richtlinie kann jedoch nur dann
herauskommen, wenn es gelingt, die Kontrolle der 
Patentamtsfunktionäre und Regierungs-Patentbeamten über den EU-Rat
weiter zu lockern.  Somit sind nunmehr die grundlegenden
Freiheitsrechte der Informationsgesellschaft mit der Demokratisierung
der EU eng verbunden.

#Idj: Ein ähnliches Drama spielt sich derzeit in %(in:Indien) ab. Die
Patentbeamten des dortigen Wirtschaftsministeriums haben unter dem
Vorwand einer %(tr:Umsetzung des TRIPs-Vertrages) in einer
%(na:Notverordnung im Dezember 2004) am Parlament vorbei
Softwarepatente legalisiert.  Auf Druck der linken Koalitionsparteien
wuerden die Änderungen Ende März wieder rückgänig gemacht, aber das
indische Patentamt erteilt seit einiger Zeit unter Dehnung und Biegung
der bisherigen Gesetzesformulierung reine Softwarepatente, und dem
wurde kein Riegel vorgeschoben.  Die indischen Beamten haben sich als
Meisterschüler ihrer europäischen Mentoren erwiesen, und es fehlt an
einer kritisch wachenden Öffentlichkeit.

#ott: Kurzeinführung

#tae: Neueste vom EPA erteilte Softwarepatente

#pra: Anh䮧ige Patente

#nnn: %(ua:Dringender Aufruf) zur Neueröffnung der Diskussion im Rat

#jt0: >%(NS) unterschriften, >%(NC) Firmen

#oIt: Innovationsschutz

#noW: Zeugnisse von Firmen über Patente

#cse: Einführung für die Massen, von %(LST) finanzierte Kampagne

#WeW2: Die Gedanken sind frei

#KWw: Einführung von Peter Gerwinski

#vrt: Save Our Software

#isn: Plakative Einführungsseite von Xuan Baldauf

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: xx ;
# End: ;

