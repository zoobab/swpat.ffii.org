<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Softwarepatenten in Europa

#descr: Gedurende de laatste paar jaar heeft het Europees Octrooibureau (EOB)
tegen de letter en de geest van de bestaande wet in, meer dan 30.000
patenten uitgegeven aan wat de wet noemt %(q:programma's voor
computers) en wat het Europees Patentburo
%(q:computer-geïmplementeerde uitvindingen) is gaan noemen in in 2000:
software in een context van patentaanspraken, oftewel regels van
organisatie en berekening gevat in termen van algemene
computerapparatuur. Op dit moment is de Europese patentgemeenschap
druk aan het uitoefenen om de recente praktijk van het EOB op te
leggen door een nieuwe wet te schrijven. Europa's programmeurs en
burgers lopen een aanzienlijk risico. Hier vindt u de
basisdocumentatie, beginnend met een kort overzicht en het laatste
nieuws.

#lae: Waarom al die drukte over softwarepatenten?

#dsW: Als Haydn %(q:een symfonie, gekarakteriseerd doordat geluid
geproduceerd wordt [in uitgebreide sonata-vorm]) had gepatenteerd, dan
had Mozart een probleem gehad.

#ltd: In tegenstelling tot copyright, kunnen patenten onafhankelijke
creaties blokkeren. Softwarepatenten kunnen software-copyright
nutteloos maken. Een werk dat onder auteursrecht valt kan gedekt zijn
door honderden patenten waar de auteur niet eens vanaf weet maar
waarvoor hij en zijn gebruikers vervolgd kunnen worden. Sommige van
deze patenten kunnen onmogelijk omzeild worden, omdat ze erg breed
zijn of omdat ze deel zijn van communicatiestandaarden.

#eas: Bewijs uit %(es:economische onderzoeken) toont aan dat patenten hebben
geleid tot een afname in het bedrag dat aan onderzoek en ontwikkeling
wordt uitgegeven.

#iWd: %(it:Vooruitgang in software is vooruitgang in abstractie.). Waar
traditionele patenten bedoeld waren voor concrete en fysieke
%(e:uitvindingen), gaan softwarepatenten over %(e:ideeën). In plaats
van het patenteren van een muizenval, patenteer je %(ep:een manier om
gegevens te vangen in een geëmuleerde omgeving). Het feit dat het
universele logica-apparaat dat we %(q:computer) noemen hiervoor
gebruikt wordt vormt geen limitatie. %(s:Wanneer software
patenteerbaar is, is alles patenteerbaar.).

#tek: In de meeste landen werd software beschouwd, zoals wiskunde en andere
abstracte materie, als expliciet buiten het gebied van patenteerbare
uitvindingen vallend. Deze regels zijn echter op de één of andere
manier overtreden. Het patentsysteem is niet meer onder controle. Een
gesloten gemeenschap van patentadvocaten is bezig met het maken,
breken en herschrijven van zijn eigen regels zonder enig toezicht van
buitenaf.

#oOv: Huidige situatie in Europa

#and: What we can do

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: nl ;
# multlin: t ;
# End: ;

