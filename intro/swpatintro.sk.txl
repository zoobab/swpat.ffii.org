<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Softwarove patenty v Európe

#descr: V priebehu posledných pár rokov Európsky patentový úrad (EPO), napriek
súčasnému zneniu zákona, udelil viac ako 30.000 patentov, ktoré boli
požadované podľa podmienok všeobecne použiteľných výpočtových
prostriedkov ako tzv. %(q:počítačové programy) v zmysle zákona z
r.1973 a ako %(q:počítačom implementované vynálezy) v zmysle nového
názvoslovia EPO od r.2000. Zástanci patentov v Európe sa snažia
legalizovať tieto postupy presadením nového zákona. Aj keď prehrali
svoj boj v novembri 2000 a v septembri 2003, programátori a občania
Európy čelia vážnym rizikám. Nájdete tu základnú dokumentáciu,
posledné správy a skrátený prehľad situácie v tejto oblasti.

#lae: Prečo toľko rozruchu okolo softwarových patentov ?

#oOv: Súčasná situácia v Európe

#and: Čo môžeme urobiť

#dsW: Keby si Haydn dal patentovať %(q:symfoniu, charakterizovanú svojim
zvukovým podaním [ v rozšírenej forme sonáty ]), Mozart by mal
problém.

#ltd: Patenty môžu blokovať nezávislú kreativitu a tým sa odlišujú od
autorských práv tzv. copyrights. Autorské práva by boli zbytočné, ak
by platili softwarové patenty. Jeden program môže obsahovať stovky
patentov, o ktorých jeho autor ani nemusí vedieť, avšak za ktoré môže
byť autor programu a aj jeho používatelia stíhaní. Niektoré z patentov
sa nedajú vôbec obísť, pretože platia veľmi všeobecne alebo sú
súčasťou komunikačných štandardov.

#eas: Výsledky %(es:ekonomických štúdií) poukazujú na to, že softwarové
patenty spôsobujú pokles investícií do výskumu a vývoja.

#iWd: %(it:Pokroky softwarov sú pokrokmi v abstrakcii). Zatial čo tradičné
patenty boli konkrétne %(e:fyzické objavy), softwarové patenty už
predstavujé %(e:myšlienky). V prenesenom význame, už nepatentujete
konkrétnu špecifickú pascu na myši, ale akýkoľvek %(q:prostriedok na
chytanie cicavcov) alebo %(ep:prostriedky na zachytenie údajov v
umelom prostredí). Skutočnosť, že na to bolo použité univerzálne
logické zariadenie nazývané %(q:počítač) nevytvára žiadne obmedzenia.
A tak, %(s:ak je patentovatelny software, tak je patentovatelné
vlastne všetko).

#tek: Vo vačšine krajín bol software, tak ako matematika alebo iné
abstraktné vedy, vylúčené z rozsahu patentovatelných vynálezov. Avšak
tieto pravidlá boli porušené rôznymi spôsobmi. Systém patentovania sa 
dostal spod kontroly. Uzavretá komunita prívnikov zaoberajúcich sa
patentami si vytvára, porušuje a prepisuje svoje vlastné pravidlá bez
vonkajšej kontroly.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpatintro.el ;
# mailto: unix@sazp.sk ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: xx ;
# End: ;

