<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Patentes de Software na Europa

#descr: Nos últimos anos o Gabinete Europeu de Patentes (GPO) (EPO -- European
Patent Office) tem, contrariamente à letra e espírito da lei vigente,
concedido mais de 30000 patentes em regras de organização e cálculo
implementadas em computador (programas para computadores). Agora o
movimento de patentes Europeu está a pressionar para consolidar esta
práctica escrevendo uma nova lei. Os cidadãos e programadores europeus
encaram riscos consideráveis. Aqui encontrará a documentação básica,
começando por uma visão por alto e as últimas novidades.

#lae: Why all this fury about software patents?

#dsW: If Haydn had patented %(q:a symphony, characterised by that sound is
produced [ in extended sonata form ]), Mozart would have been in
trouble.

#ltd: Unlike copyright, patents can block independent creations.  Software
patents can render software copyright useless.  One copyrighted work
can be covered by hundreds of patents of which the author doesn't even
know but for whose infringement he and his users can be sued.  Some of
these patents may be impossible to work around, because they are broad
or because they are part of communication standards.

#eas: Evidence from %(es:economic studies) shows that software patents have
lead to a decrease in R&D spending.

#iWd: %(it:Advances in software are advances in abstraction).  While
traditional patents were for concrete and physical %(e:inventions),
software patents cover %(e:ideas).  Instead of patenting a specific
mousetrap, you patent any %(q:means of trapping mammals) or %(ep:means
of trapping data in an emulated environment).  The fact that the
universal logic device called %(q:computer) is used for this does not
constitute a limitation.  %(s:When software is patentable, anything is
patentable).

#tek: In most countries, software has, like mathematics and other abstract
subject matter, been explicitely considered to be outside the scope of
patentable inventions.  However these rules were broken one or another
way.  The patent system has gone out of control.  A closed community
of patent lawyers is creating, breaking and rewriting its own rules
without much supervision from the outside.

#oOv: Current Situation in Europe

#and: What we can do

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: pt ;
# multlin: t ;
# End: ;

