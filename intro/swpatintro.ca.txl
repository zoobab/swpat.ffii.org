<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Patents de Programari a Europa

#descr: Durants aquests darrers anys, l'Oficina Europea de Patents (OEP),
contràriament a la lletra i l'esperit de la legislació vigent, ha
concedit més de 30 000 patents de programes d'ordinador, és a dir,
regles d'organització i càlcul emmarcades en termes d'equipament
genèric computacional. Ara, la comunitat de patents europea pressiona
per a traduir aquesta pràctica en una nova llei. Els programadors i
els ciutadans s'enfronten a riscos considerables. Aquí trobareu la
documentació bàsica, començant per un cop d'ull ràpid i les últimes
notícies.

#lae: Per què les patents de programari són delirants?

#dsW: Si Haydn hagués patentat %(q:una simfonia, caracterizada de manera que
es produeix so [ en forma de sonata extendida ]), Mozart hauria tingut
problemes.

#ltd: Contràriament als drets d'autor, les patents poden bloquejar creacions
independents.  Les patents de programari poden causar que els drets
d'autor de programari siguin inútils.  Una feina protegida amb drets
d'autor es pot cobrir amb centenars de patents de les quals l'autor no
en sap res, però el poden demandar pel seu infringiment.  Algunes
d'aquestes patents poden ser impossibles d'esquivar, perquè són amples
o perquè formen part d'estàndards de comunicació.

#eas: Hi ha evidències d'estudis econòmics que mostren que les patents de
programari han menat a una disminució de despesa en R+D.

#iWd: %(it:Avenços en programari són avenços en abstracció).  Mentre que les
patents tradicionals eren per %(e:invencions) concretes i físiques,
les patents de programari cobreixen %(e:idees).  Per comptes de
patentar una trampa per a ratolins, es patenta una %(q:manera de caçar
dades en un ambient emulat) (EP 769170).  El fet que l'artefacte lògic
universal anomenat %(q:ordinador) es faci servir per a fer això no
constitueix una limitació.  Quan el programari és patentable,
qualsevol mètode abstracte ho és.

#tek: A la majoria de països, el programari, com les matemàtiques i altres
elements abstractes, es considera explícitament que està fora de
l'abast de les invencions patentables.  Malgrat això, aquestes regles
es trenquen d'una manera o una altra.  El sistema de patents està fora
de control.  Una comunitat tancada d'advocats de patents crea, trenca
i reescriu les seves pròpies regles, sense massa supervisió de
l'exterior.

#oOv: Situació actual a Europa

#and: What we can do

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: ca ;
# multlin: t ;
# End: ;

