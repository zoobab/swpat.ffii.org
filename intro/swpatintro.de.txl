<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Softwarepatente in Europa

#descr: In den letzten Jahren hat das Europäische Patentamt (EPA) gegen den
Buchstaben und Geist der geltenden Gesetze über 30.000 Patente auf
computer-eingekleidete Organisations- und Rechenregeln (laut Gesetz
%(e:Programme für Datenverarbeitungsanlagen), laut EPA-Neusprech von
2000 %(q:computer-implementierte Erfindungen)) erteilt.  Nun möchte
Europas Patentbewegung diese Praxis durch ein neues Gesetz
festschreiben.  Europas Programmierer und Bürger sehen sich
beträchtlichen Risiken ausgesetzt.  Hier finden Sie die grundlegende
Dokumentation zur aktuellen Debatte, ausgehend von einer
Kurzeinführung und Übersicht über die neuesten Entwicklungen.

#lae: Warum die Aufregung über Softwarepatente?

#dsW: Hätte Haydn %(q:eine Symphonie, dadurch gekennzeichnet, dass Klang [
in erweiterter Sonatenform ] erzeugt wird) patentiert, wäre Mozart in
Schwierigkeiten gekommen.

#ltd: Anders als das Urheberrecht können Patente eigenständige Schöpfungen
blockieren.  Softwarepatente können das Software-Urheberrecht
aushebeln. Ein urheberrechtlich geschütztes Werk kann von Hunderten
von Patenten belegt sein, von denen der Autor nichts weiß, für deren
Verletzung er aber belangt werden kann. Manche Patente können unter
Umständen nicht umgangen werden, weil sie sehr breit oder Teil eines
Kommunikationsstandards sind.

#eas: %(es:Wissenschaftliche Studien) belegen, dass Softwarepatente zu einer
Verminderung der Investitionen in Forschung und Entwicklung geführt
haben.

#iWd: %(it:Fortschritte in der Informatik sind Fortschritte im
Abstrahieren).  Traditionelle Patente richteten sich auf konkrete,
materielle %(e:Erfindungen). Softwarepatente richten sich auf
%(e:Ideen).  Man patentiert nicht mehr eine bestimmte Mausefalle,
sondern jedes %(q:Mittel zum Ködern von Nagetieren) oder %(ep:Mittel
zum Abfangen von Daten in einer simulierten Umgebung).  Dass hierbei
das Universelle Logikgerät namens %(q:Computer) zum Einsatz kommt,
stellt keine Begrenzung dar.  Wenn Software patentierbar ist, ist
alles patentierbar.

#tek: In den meisten Ländern gehörten Leistungen auf dem Gebiet des
Programmierens, Rechnens, Organisierens etc. traditionell nicht zu den
%(e:patentfähigen Erfindungen).  Diese Gesetze wurden jedoch in den
letzten Jahren auf verschiedene Weise gebrochen.  Das Patentsystem ist
außer Kontrolle geraten.  Eine Gemeinschaft von Patentjuristen
schafft, bricht und ändert ihre eigenen Regeln ohne nennenswerte
öffentliche Überwachung.

#oOv: Derzeitige Situation in Europa

#and: Was können wir tun?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: de ;
# multlin: t ;
# End: ;

