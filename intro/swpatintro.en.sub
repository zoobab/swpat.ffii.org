\begin{subdocument}{swpatintro}{FFII: Software Patents in Europe}{http://swpat.ffii.org/intro/index.en.html}{Members\\\url{}\\english version 2005/02/07 by Members\footnote{\url{http://wiki.ffii.org/TradukListinfoEn}}}{For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.}
\begin{sect}{intro}{Why all this fury about software patents?}
If Haydn had patented ``a symphony, characterised by that sound is produced [ in extended sonata form ]'', Mozart would have been in trouble.

Unlike copyright, patents can block independent creations.  Software patents can render software copyright useless.  One copyrighted work can be covered by hundreds of patents of which the author doesn't even know but for whose infringement he and his users can be sued.  Some of these patents may be impossible to work around, because they are broad or because they are part of communication standards.

Evidence from economic studies\footnote{\url{http://swpat.ffii.org/vreji/minra/sisku/index.de.html}} shows that software patents have lead to a decrease in R\&D spending.

Advances in software are advances in abstraction\footnote{\url{http://wiki.ffii.org/IstTamaiEn}}.  While traditional patents were for concrete and physical \emph{inventions}, software patents cover \emph{ideas}.  Instead of patenting a specific mousetrap, you patent any ``means of trapping mammals'' or ``means of trapping data in an emulated environment\footnote{\url{http://swpat.ffii.org/patents/samples/ep769170/index.en.html}}''.  The fact that the universal logic device called ``computer'' is used for this does not constitute a limitation.  {\bf When software is patentable, anything is patentable}.

In most countries, software has, like mathematics and other abstract subject matter, been explicitely considered to be outside the scope of patentable inventions.  However these rules were broken one or another way.  The patent system has gone out of control.  A closed community of patent lawyers is creating, breaking and rewriting its own rules without much supervision from the outside.
\end{sect}

\begin{sect}{europa}{Current Situation in Europe}
\begin{itemize}
\item
{\bf {\bf Software Patents in Europe: A Short Overview\footnote{\url{http://swpat.ffii.org/log/intro/index.en.html}}}}

\begin{quote}
In 20 minutes you can learn what is going on in the fight about software patents in Brussels.  Most of the complexities of the debate arise from a few simple parameters.  When you have learnt these, you can hopefully feel confident to write well-informed articles about a fascinating political drama with far-reaching implications.
\end{quote}
\filbreak

\item
{\bf {\bf FFII interests and the EU Software Patent Directive\footnote{\url{http://swpat.ffii.org/analysis/needs/index.en.html}}}}

\begin{quote}
What are the central freedom and exclusivity interests of software creators and users how do they translate into the language of the Software Patent Directive? What other interests exist? Where can space for meaningful negotiations be found?
\end{quote}
\filbreak

\item
{\bf {\bf Steps Out of the European Software Patent Deadlock\footnote{\url{http://swpat.ffii.org/letters/cons0406/step/index.en.html}}}}

\begin{quote}
FFII proposes a sequence of basic steps need to be undertaken if the European Software Patent Directive is to get anywhere.  The most basic ones consist in reverting some deteriorations that were introduced by the Council's Patent Policy Working Group during the last weeks and months before the Political Agreement of 2004-05-18 and that turn this version of the directive into the most uncompromisingly pro-software-patent version that has ever been seen during the 10 years of the directive-formulation process.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{gunka}{What we can do}
\begin{itemize}
\item
{\bf {\bf How you can help us stop Software Patents\footnote{\url{http://swpat.ffii.org/group/todo/index.en.html}}}}
\filbreak

\item
{\bf {\bf FFII Projects\footnote{\url{http://wiki.ffii.org/FfiiprojEn}}}}

\begin{quote}
We report the latest status of our projects.  You create and support projects yourself.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
% mode: latex ;
% End: ;

