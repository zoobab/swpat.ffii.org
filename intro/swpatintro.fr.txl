<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Brevets Logiels en Europe

#descr: Ces dernières années, l'Office Européen des Brevets a, en 
contradiction à la fois avec la lettre et avec l'esprit du droit 
positif, accordé environ trente mille brevets sur des règles 
d'organisation et de calcul revendiquées en termes d'ordinateur 
universel (%(q:programmes d' ordinateurs) selon la loi, %(q:inventions
 mises en oeuvre par ordinateur) selon la novlangue de l'OEB adoptée
en  2000). A présent, le mouvement européen pro-brevet veut entériner
cette  pratique en édictant une nouvelle directive.  Tandis que le
mouvement de  brevets a perdu deux batailles majeures en novembre 2000
et septembre  2003, les programmeurs européens se voient confrontés à
des risques  considérables.  Vous trouverez ci-dessous la
documentation sur le débat  actuel, commençant par une courte
introduction et l'actualité.

#lae: Pourquoi tant de fureur sur les brevets logiciels?

#dsW: Si Haydn avait breveté %(q:une symphonie caractérisée par sa 
construction [en forme de sonate élargie]), Mozart se serait retrouvé
en  difficulté.

#ltd: Contrairement au droit d'auteur, les brevets peuvent bloquer des 
créations indépendantes.  Les brevets logiciels peuvent rendre le
droit  d'auteur du logiciel inutile. Une oeuvre protégée par droit
d'auteur  peut être couverte par des centaines de brevets dont
l'auteur ignore  même l'existence mais pour lesquels lui et ses
utilisateurs peuvent être  poursuivis. Certains de ces brevets ne
peuvent être contournés car ils  sont étendus et car ils font partie
des standards de communication.

#eas: Les %(es:études économiques) montrent que les brevets logiciels ont 
entraîné une diminution des investissements en recherche et
développement.

#iWd: %(it:Les progrès en informatique sont des progrès dans le domaine  de
l'abstraction). Alors que les brevets traditionnels étaient conçus 
pour des %(e:inventions) de nature concrète, physique, les brevets 
logiciels concernent les %(e:idées). Au lieu de breveter un  "type de 
souricière", on se réserve tout %(q:moyen de capture d'animaux) ou 
%(ep:moyen de capture d'un flux de données dans un environnement 
simulé). Le fait que pour cela on utilise une machine logique 
universelle, appelée %(q:l'ordinateur), ne constitue pas une limite. 
%(s:Si les logiciels sont brevetables, tout est brevetable.)

#tek: Dans la plupart des pays, le logiciel a, comme les mathématiques ou 
d'autres domaines abstraits, été expressément exclu du champ des 
inventions brevetables. Ces règles ont néanmoins été brisées. Le
système  de brevet n'est plus contrôlable. Une communauté fermée de
conseils en  propriété industrielle est en train de créer, violer et
récrire ses  propres règles sans aucun contrôle extérieur.

#oOv: Situation actuelle en Europe

#and: Que pouvons nous faire?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatintro.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffiicmima ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatintro ;
# txtlang: fr ;
# multlin: t ;
# End: ;

