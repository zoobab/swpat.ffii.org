\begin{subdocument}{swplxtg26}{FFII auf Linuxtag 2000}{http://swpat.ffii.org/termine/2000/linuxtag/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Der FFII e.V. informierte auf dem LinuxTag 2000 in Stuttgart \"{u}ber neuere gesetzeswidrige Praktiken der deutschen und europ\"{a}ischen Patentjustiz sowie deren Pl\"{a}ne, Programmlogik und damit wirtschaftliche und gesellschaftliche Verfahren aller Art umfassend patentierbar zu machen.}
Der LinuxTag\footnote{http://www.linuxtag.de} fand dieses Jahr vom 29. Juni bis zum 2. Juli 4 Tage lang auf dem Stuttgarter Messegel\"{a}nde statt und wurde von ca 20000 Menschen besucht.  FFII informierte an Stand 6.0.1.14 auf Einladung der Veranstalter\footnote{http://www.linuxtag.de/2000/deutsch/shownews.php3?id=0079} \"{u}ber das Thema Softwarepatente\footnote{http://swpat.ffii.org/index.de.html}.

Wir f\"{u}hrten ein Veranstaltungsprogramm durch, das viele interessante Erkenntnisse zutage f\"{o}rderte:

\begin{sect}{stand}{Durchgehend am FFII-Stand}
\begin{itemize}
\item
Unterschreiben der Eurolinux-Petition gegen Softwarepatente

\item
Verteilung von voraddressierten Petitionsbriefen

\item
Vorf\"{u}hrung von Dokumentation\footnote{http://swpat.ffii.org/archiv/index.de.html}

\item
Verteilung unserer SWPAT-Dokumentations-CD\footnote{http://swpat.ffii.org/archiv/doku/cd/index.de.html}.

\item
Verteilung von Anti-SWPAT-Aufklebern und Flugbl\"{a}ttern

\item
Information \"{u}ber andere FFII-Projekte wie z.B. EGeld\footnote{http://egeld.ffii.org/index.html}/Nerdbank\footnote{http://www.nerdbank.org} und Wortbasar\footnote{http://wortbasar.ffii.org/index.html}.
\end{itemize}
\end{sect}

\begin{sect}{preleg}{Vortragsprogramm im Kongresszentrum A Saal 1}
\begin{sect}{tauchert}{Tauchert: Softwarepatentierung -- Praxis des Deutschen Patentamts}
\begin{sect}{tauchprep}{Vorbereitende Texte}
Auszug aus der vorbereitenden Diskussion \"{u}ber Herrn Taucherts Vortragszusammenfassung\footnote{http://swpat.ffii.org/termine/2000/linuxtag/tauchert/index.de.html}
\end{sect}

\begin{sect}{tauchtemp}{Uhrzeit}
Freitag, 30. Juni 11.00-12.00
\end{sect}

\begin{sect}{tauchpers}{Redner}
Dr. Wolfgang TAUCHERT Leiter der Abteilung fuer Datenverarbeitung und Informationsspeicherung am Deutschen Patent- und Markenamt
\end{sect}

\begin{sect}{tauchenhv}{Inhalt}
Das Deutsche Patentgesetz (PatG) verbietet in \S{}1 die Patentierung von Computerprogrammen als solchen.  Daran, dass der Gesetzgeber die Einschr\"{a}nkung ``als solche'' macht, erkennt man, dass er dieses Patentierungsverbot als problematisch ansieht.  Die Ausnahmenliste in \S{}1 ist unsystematisch formuliert und schwer anwendbar.  Auch die Einschr\"{a}nkung auf Gegenst\"{a}nde ``als solche'' erlaubt zahlreiche Interpretationsm\"{o}glichkeiten. Der Bundesgerichtshof (BGH) hat daher von jeher nicht den \S{}1 direkt angewendet, sondern mit dem Begriff der ``Technizit\"{a}t'' operiert, der sich indirekt aus den folgenden PatG-Paragraphen als das (neben Neuheit und Erfindungsh\"{o}he) zentrale zugrundeliegende Abgrenzungskriterium begr\"{u}nden l\"{a}sst.  Diesen Begriff definiert das PatG zwar nicht, aber hat der BGH hat eine viel zitierte klare Definition geschaffen:

\begin{quote}
{\it planm\"{a}{\ss}iges Handeln unter Anwendung beherrschbarer Naturkr\"{a}fte ohne zwischengeschaltete menschliche T\"{a}tigkeit}
\end{quote}

Anfang der 90er Jahre lehnte der BGH Antr\"{a}ge auf Erteilung von Softwarepatenten wegen mangelnder Technizit\"{a}t ab.  Im Fall ``Chinesische Schriftzeichen'' bem\"{a}ngelte der BGH, dass der Kern des Erfinderischen nicht in der Beherrschung von Naturkr\"{a}ften (Elektronen) sondern in der Programmlogik liege, da das von Siemens zur Patentierung vorgelegte Textverarbeitungsprogramm vom Computer nur den bestimmungsgem\"{a}{\ss}en Gebrauch mache.  Man nannte diese Denkweise des BGH die ``Kerntheorie''.

Die Kerntheorie l\"{o}ste bei den interessierten Fachkreisen heftige Kritik aus.  Mitte der 90er Jahre wandte sich der BGH mit der Entscheidung ``Tauchcomputer'' von der Kerntheorie ab und hob auf die Verwendung des Programms ab.

Auf dem M\"{u}nchener Kongress der UNION Ende 1997 argumentierten die versammelten Patentrechtler \"{u}berzeugend, dass jedes funktionierende Computersystem technisch sei, und dass die Frage der Technizit\"{a}t und der Erfindungsleistung nicht vermengt werden d\"{u}rfe (d.h. dass der erfinderische Kern nicht im technischen Bereich liegen m\"{u}sse).  Wenig sp\"{a}ter schwenkten das Europ\"{a}ische und das Deutsche Patentamt auf diese Sichtweise um, und der 17te Patentsenat gab seine restriktive Haltung auf.

Im Juli 1999 urteilte der BGH, dass ein programmgesteuerte System zur Bestimmung von Verkaufspreisen grunds\"{a}tzlich patentf\"{a}hig sei.  Meine Abteilung hatte den Patentantrag zun\"{a}chst zur\"{u}ckgewiesen.  Der Kollege, der ihn zur\"{u}ckwies, hing n\"{a}mlich noch der Kerntheorie an.  Der BGH verwarf aber in einem Grundsatzurteil die Kerntheorie und verwies den Patentantrag (in Form eines verengten Hilfsantrages) an unsere Abteilung zur erneuten Beurteilung zur\"{u}ck.

In zwei neuen Urteilen Sprachanalyse\footnote{http://swpat.ffii.org/papiere/bgh-sprach00/index.de.html} und Logikverifikation\footnote{http://swpat.ffii.org/papiere/bgh-logik99/index.de.html} ging der BGH sogar noch weiter.  Er schuf den Begriff der ``programmtechnischen Vorrichtung''.  Damit ist nunmehr jedes Computerprogramm patentrechtlich sch\"{u}tzbar.  Wir im DPMA h\"{a}tten auch nicht gedacht, dass der BGH so weit gehen w\"{u}rde.  Damit kehrt der BGH zu einer Haltung zur\"{u}ck, die er in den 60er Jahren bereits einmal eingenommen hatte, bevor die Mathematikerfraktion eine restriktive Rechtsauffassung durchsetzte und daf\"{u}r sorgte, dass Programme lange Zeit nicht direkt patentiert werden konnten.  Es handelt sich hier um einen politischen Sieg der ingenieurwissenschaftlich orientierten Fraktion.
\end{sect}

\begin{sect}{frag}{Antworten auf Fragen und Einw\"{a}nde}
\begin{sect}{jur}{WRiek: D\"{u}rfen ein paar Patentjuristen auf diese Weise Normen grundlegend ver\"{a}ndern?  Sind das nicht politische Fragen, die \"{u}ber die Kompetenz der Gerichte und Patent\"{a}mter hinausgehen?}
Die Rechtsprechung hat das Recht, das Gesetz zu interpretieren.  Nat\"{u}rlich ist das auch eine politische Angelegenheit.  Die ingenieurwissenschaftliche Sicht hat sich durchgesetzt.  Dabei geht es aber nicht um materielle Interessen einer Gruppe, sondern darum, unertr\"{a}gliche rechtssystematische Widerspr\"{u}che aus dem System zu entfernen.  Wir haben die jetzigen Entscheidungen des BGH als eine Befreiung empfunden.  Ob au{\ss}erdem das EP\"{U} ge\"{a}ndert wird, ist v\"{o}llig zweitrangig, denn wir haben bereits ein schl\"{u}ssiges System.  Wenn Sie als Nicht-Juristen da nun Einfluss aus\"{u}ben wollen, m\"{u}ssen Sie erst einmal das System verstehen.  Ihre Forderung, das Programmierungsverbot f\"{u}r Computer aufrecht zu erhalten, f\"{u}hrt nicht zum Ziel.  Im Gegenteil, erst wenn es abgeschafft wird, kann es f\"{u}r Sie neue Chancen geben, die Gesetzgebung in Bewegung zu bringen und Ihren Vorstellungen Geh\"{o}r zu verschaffen.
\end{sect}

\begin{sect}{oek}{phm: Ist es wirklich in Ordnung, wenn man Software patentierbar macht, ohne vorher eine systematische Studie der \"{o}konomischen Auswirkungen einer solchen \"{A}nderung zu erstellen?}
Selbstverst\"{a}ndlich.  Wir brauchen keine \"{o}konomische Studie.  Die Wirklichkeit spricht f\"{u}r sich.  Der Markt hat das Urteil bereits gesprochen.  Bei uns gehen jedes Jahr Tausende von Antr\"{a}gen auf Softwarepatente ein, und unser Patentsystem wirft Gewinne ab.  Es ern\"{a}hrt ohne staatliche Zusch\"{u}sse 20000 Patentspezialisten.
\end{sect}

\begin{sect}{raub}{Siepmann: K\"{o}nnte man nicht auf gleiche Weise auch argumentieren, ein R\"{a}ubersyndikat sei legitim, da es ja auf dem Markt gesiegt habe und ohne staatliche Zusch\"{u}sse auskomme?}
Nein.  Das R\"{a}ubersyndikat handelt rechtswidrig, wir nicht.  Wir st\"{u}tzen uns auf Grundsatzurteile des BGH.  Der BGH ist befugt, die Gesetze zu interpretieren, das R\"{a}ubersyndikat nicht.
\end{sect}

\begin{sect}{pres}{phm: Herr Tauchert, Sie sprechen hier sehr grundlegende Wahrheiten aus, die man selten in dieser Deutlichkeit zu h\"{o}ren bekommt.  D\"{u}rfen wir das, was Sie gerade gesagt haben, in unserer Presseerkl\"{a}rung zitieren?}
Selbstverst\"{a}ndlich.  Dazu stehe ich.
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{spring}{Springorum: Z\"{a}hmung des Patent-Ungeheuers -- Konzept eines Schutzb\"{u}ndisses}
\begin{sect}{springpers}{}
PA Dipl.Inf. Dr. Harald Springorum
\end{sect}

\begin{sect}{sprintenhv}{}
Seit ca 1990 denken Programmierer Freier Software und andere Softwarepatent-Gesch\"{a}digte \"{u}ber die M\"{o}glichkeit nach, B\"{u}ndnisse zum gegenseitigen Schutz von Patenten zu schlie{\ss}en.  Ein solches B\"{u}ndnis w\"{u}rde selber Patentrechte erwerben und als Verhandlungsmasse zum Schutz von ``geistigem Gemeineigentum'' einzusetzen.\footnote{http://swpat.ffii.org/archiv/spiegel/sarxe/index.de.html}

PA Springorum stellte hierzu ein Modell vor, \"{u}ber dessen Machbarkeit sp\"{a}ter gewinnbringend diskutiert wurde.  An der Diskussion beteiligten sich vor allem Wolfgang Tauchert (DPMA), Swantje Weber-Cludius (BMWi), Peter Gerwinski (G.N.U. GmbH), Werner Riek (Journalist) und Hartmut Pilch (FFII).

PA Springorum schlug vor, zun\"{a}chst solle ein Verein Softwareentwicklern \"{U}berblick \"{u}ber drohende Patentgefahren schaffen und gegen vermeidbare Softwarepatente Einspruch einlegen.  Ferner k\"{o}nnte dieser Verein in Zusammenarbeit mit einer Versicherungsgesellschaft Rechtsschutzversicherungen gegen Patentrisiken anbieten.

Daneben solle eine nicht-gewinnorientierte Gesellschaft (Pool GmbH oder Pool AG, da gewerblich t\"{a}tig) gegr\"{u}ndet werden, die f\"{u}r die freien Softwareentwickler Erfindungen zum Patent anmeldet und zum Gebrauch in freier Software freigibt, aber gleichzeitig denjenigen den Gebrauch verbietet, die freie Software mit Patenten angreifen.

Schwierigkeiten k\"{o}nnte es beim Kartellamt geben, besonders wenn die Pool AG potente Mitglieder wie IBM als Gesellschafter gewinne.  Aber diese Schwierigkeiten m\"{u}ssten sich \"{u}berwinden lassen, da es ja gerade nicht um die Schaffung von Kartellen sondern um das Gegenteil davon gehe.

Es gibt bereits m\"{o}gliche Vorbilder, n\"{a}mliche eine K\"{o}lner Schutzvereinigung f\"{u}r den fairen Gebrauch von Rundfunkrechten.  Deren Satzung legte PA Springorum vor.
\end{sect}

\begin{sect}{springfrag}{}
\begin{sect}{restr}{Juristen begehen h\"{a}ufig den Fehler, die Ausschlusskraft restriktiver Gemeineigentumslizenzen (Copyleft-Lizenzen wie GNU GPL) zu \"{u}bersch\"{a}tzen.  Solche Lizenzen taugen nur in sehr geringem Ma{\ss}e als Faustpf\"{a}nder.  Wenn ein Programmierverfahren erst einmal in GPL-Software implementiert ist, kann man niemanden mehr daran hindern, dieses Verfahren zu verwenden.}
Dann muss man die GPL eben so \"{a}ndern, dass das geht.  Wer die GPL zur heiligen Kuh erhebt, ist selbst schuld, wenn er sich dann nicht gegen Patente wehren kann.
\end{sect}

\begin{sect}{modul}{Selbst angenommen, wir erfinden eine neue Allgemeine \"{O}ffentliche Lizenz, die es einer bestimmten Pool AG erlaubt, die GPL als Waffe einzusetzen, dann ist das noch immer eine stumpfe Waffe.  Denn freie Software steht im Internet offen zur Verf\"{u}gung.  Jeder kann sie verwenden, egal ob legal oder nicht.  Wenn wir z.B. Microsoft verbieten, ein bestimmtes Programmierverfahren zu verwenden, muss Microsoft lediglich seine Software so schreiben, dass f\"{u}r das entsprechende Verfahren ein unter GPL stehendes Modul (z.B. \"{u}ber Corba) aus dem Internet geladen wird.}
Das k\"{o}nnte Microsoft aber schon gen\"{u}gend weh tun.
\end{sect}

\begin{sect}{patfund}{Was tun wir, wenn unser Gegner eine Patentverwertungsgesellschaft ist, die selbst \"{u}berhaupt keine Programme schreibt, sondern lediglich jede Woche mehrere Patente erzeugt, hinzukauft und den Meistbietenden (eventuell exklusiv) lizenziert?}
Gegen solche Gesellschaften w\"{a}re unsere Pool AG machtlos, es sei denn sie h\"{a}tte genug Geld, um alle gro{\ss}en Konzerne zu \"{u}berbieten.  Wir k\"{o}nnen nur hoffen, dass das nicht der typische Fall ist.
\end{sect}

\begin{sect}{anarch}{Die Welt der freien Software besteht aus vielen einzelnen Programmierern, denen es gegen den Strich gehen w\"{u}rde, sich in einem Verein oder einer Pool AG generalstabsm\"{a}{\ss}ig zu organisieren.  Unter den kleinen und mittleren Unternehmen, die auch als Teilnehmer in Frage k\"{a}men, gibt es wiederum sehr viel konkurrenzorientiertes Denken und wenig Bereitschaft, im Interesse der Solidarit\"{a}t Opfer zu bringen.   Der bisher geringe Erfolg zahlreicher vergleichbarer Versuche in den USA\footnote{http://swpat.ffii.org/archiv/spiegel/sarxe/index.de.html} scheint diese Sicht zu best\"{a}tigen}
Selbstverst\"{a}ndlich sollte die Mitgliedschaft in dem Verein und der Pool AG auf Freiwilligkeit beruhen.  Man braucht auch unbedingt eine gute Initialz\"{u}ndung.  Eine Firma wie SuSE, die ja letztlich die Fr\"{u}chte der Arbeit unz\"{a}hliger kostenlos arbeitender Programmier absch\"{o}pft, sollte da 1 Million DM investieren.
\end{sect}

\begin{sect}{aufwand}{Der Arbeitsaufwand ist so gewaltig, dass selbst eine personell gut ausgestattete Pool AG keine guten Erfolgsaussichten h\"{a}tte.  Sie m\"{u}sste n\"{a}mlich hunderttausende von Programmzeilen auf m\"{o}gliche Verletzungen von hunderttausenden von Patenten absuchen.  Au{\ss}erdem m\"{u}sste sie unentgeltlich arbeitende Programmierer dazu motivieren, ihre Werke auf m\"{o}glicherweise patentierbare Trivialit\"{a}ten abzusuchen und diese zu dokumentieren.  Es ist bekannt, dass echte Programmierer ungern Dokumentation schreiben.  Schon gar nicht Patentdokumentation, die wiederum eine besondere Erfahrung erfordert.  Um eine Patentschrift zu schreiben, muss man vielleicht kein Jurist sein, aber immerhin erfordert das viel M\"{u}he und Erfahrung, die man bei freien Entwicklern nicht antrifft, und die Personalkosten von mindestens mehreren 100000 DM monatlich bei der Pool AG erfordern w\"{u}rde, wenn sie ihre Aufgabe wenigstens zu 10\percent{} bew\"{a}ltigen will.}
Nicht unbedingt.  Man muss die Entwickler schulen.  Wenn die OpenSource-Gemeinde auf ihren Gewohnheiten beharren und sich nicht vom Fleck bewegen will, ist sie selber schuld.
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{smets}{Smets: Software Patent Tactics for OpenSource Developpers}
\begin{sect}{smetstemp}{}
Samstag, 1. Juli 16.00-17.00
\end{sect}

\begin{sect}{smetsprep}{}
J.P. Smets: Practical Software Patent Tactics for OpenSource Developpers\footnote{http://www.freepatents.org/shanghai/shanghai.pdf}
\end{sect}

\begin{sect}{smetsfrag}{}
\begin{sect}{anreiz}{Die Smetssche GPPL ist gegen Patentverwertungsfirmen machtlos.  Somit wirkt sie, falls sie erfolgreich wird, auf eine weitere Konzentration von Patentrechten in den H\"{a}nden von Patentverwertungsfirmen hin.}
Ja, zweifellos.  Wenn Microsoft seine Patente an eine Patentverwertungsfirmat verkauft und dann von dieser lizenziert, sind wir mit der GPPL machtlos.  Es sei denn wir haben diejenigen Verfahren patentiert, mit denen Patentverwertungsfirmen und Patentanwaltsb\"{u}ros arbeiten m\"{u}ssen.
\end{sect}

\begin{sect}{freipat}{Es gibt aber eine alternative Freipatentierungslizenz (GPPL), mit der wir auch gegen Patentverwertungsfirmen vorgehen k\"{o}nnten.  Wir k\"{o}nnten eine Freipatentierungslizenz (GPPL) schaffen, die es verbietet, unsere Patente im Zusammenspiel mit propriet\"{a}ren Patenten zu verwenden.  D.h. \"{u}berall dort, wo auf einem Rechner ein propriet\"{a}r patentiertes Verfahren zum Einsatz kommt -- darf unser Verfahren nicht verwendet werden.  Und zwar bedingungslos.  \"{O}ffentliche Patente sch\"{u}tzen offensiv das Gemeineigentum.  Dar\"{u}ber kann, genau wie auch im parallelen Fall des GPL-Urheberrechts, keine Privatperson verhandeln.  Da im Patentwesen mit h\"{a}rteren Bandagen gek\"{a}mpft wird, muss die GPPL h\"{a}rter zuschlagen als die GPL.  Durch eine solche harte Vorgehensweise k\"{o}nnten wir die privaten Patente entwerten und das Gesch\"{a}ft der Patentverwertungsfirmen austrocknen.  Denn wer wird noch eie Patentlizenz kaufen, wenn die damit patentierte Technik in fast keiner Situation mehr legal eingesetzt werden darf?}
Das w\"{a}re tats\"{a}chlich ein folgerichtiger Entwurf, mit dem man auch die Patentverwertungsfirmen trifft.  Allerdings br\"{a}uchte ich mehr Zeit, um die praktischen Folgen einer solchen Strategie zu Ende zu denken.
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{far}{FFII: Softwarepatent-Lobbyarbeit:  Was wurde erreicht und was ist zu tun?}
\begin{sect}{farpers}{}
Hartmut Pilch und Holger Blasum
\end{sect}

\begin{sect}{farenhv}{}
Die Patentjustiz hat sich selbst Regeln geschaffen, die sowohl von ihrer Form als auch von ihrer Wirkung her im Gegensatz zum Buchstaben und zum Geist der geltenden Gesetze stehen.  Mit seiner Recherche- und Dokumentationsarbeit konnte der FFII dies klar belegen und dadurch den Argumentationsspielraum der Patentexpansionisten stark einengen.  Unsere Gespr\"{a}che in Berlin und auf dem Linuxtag haben das deutlich gezeigt.  Die Patentexpansionisten haben auch viel weniger R\"{u}ckhalt in der Wirtschaft als sie es gerne glaubhaft machen:  die Front verl\"{a}uft zwischen Informationstechnikern und Patentexperten, nicht zwischen freier und propriet\"{a}rer Softwareindustrie.  Wir rennen \"{u}berall, sogar bei Gro{\ss}unternehmen wie Siemens, offene T\"{u}ren ein.  Das macht Spa{\ss}, und es gibt sehr viel zu tun, z.B.:

\begin{itemize}
\item
Erstellen und Versand von Aufklebern und bedruckten Gegenst\"{a}nden aller Art, die auf unsere Petition und Dokumentation hinweisen.

\item
Fortsetzung von Holgers Arbeit des Sammelns und Digitalisierens einschl\"{a}giger Papierdokumente.

\item
Ver\"{o}ffentlichung von Rezensionen dieser Dokumente\footnote{http://swpat.ffii.org/papiere/index.de.html}.

\item
Weiterentwicklung der bisherigen Dokumentations-CD\footnote{http://swpat.ffii.org/archiv/doku/cd/index.de.html}

\item
Gezieltes Ansprechen von Firmen und Politikern. (Einige Diskussionsteilnehmer haben gute Dr\"{a}hte zu bestimmten Unternehmen.)

\item
Suche nach \"{O}konomen mit informationstechnischen Kenntnissen, die weitere Wirkungsstudien ver\"{o}ffentlichen k\"{o}nnten.

\item
Weitere offene Briefe an verschiedene Institutionen, die uns helfen oder ihre bisherige Linie \"{a}ndern k\"{o}nnten.  Z.B. k\"{o}nnten wir Siemens dazu veranlassen, sich von dem bisherigen Pro-SWPAT-Auftreten des Herrn K\"{o}rber zu distanzieren und stattdessen eine Stellungnahme zu ver\"{o}ffentlichen, die zu unvoreingenommener Erforschung der \"{o}konomischen Wirkungen r\"{a}t.

\item
Anmelden von Patenten und Gebrauchsmustern unter Allgemeiner \"{O}ffentlicher Patentlizenz (GPPL). (Die Geb\"{u}hren f\"{u}r Gebrauchsmusteranmeldung beim Deutschen Patentamt liegen bei ca 100 DM.  Man kann au{\ss}erdem Anteile an den Rechten verkaufen.  Jeder Teilinhaber kann dann den Rechtstitel gegen beliebige Dritte durchsetzen.)

\item
Formulieren von m\"{o}glichen Allgemeinen \"{O}ffentlichen Patentlizenzen und durchsetzung g\"{u}nstiger Geb\"{u}hrenbedingungen f\"{u}r diese Art von Lizenz beim Gesetzgeber und im Rahmen des BGH-Fallrechts.  Weitere Gesetzesvorschl\"{a}ge, z.B. zur prinzipiellen Nichtgewerblichkeit von GPL-Software.

\item
Wie zeigen, dass man vorher da war?\footnote{http://swpat.ffii.org/analyse/stand/index.de.html}
\end{itemize}
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{etc}{Nebengespr\"{a}che}
\begin{sect}{etcmosdorf}{Siegmar Mosdorf}
Im Anschluss an seine Er\"{o}ffnungsansprache \"{a}u{\ss}erte Wirtschafts-Staatssekret\"{a}r Siegmar Mosdorf (SPD) in einem Handelsblatt-Interview, er nehme die \"{A}ngste der Informatiker vor den Br\"{u}sseler Patentpl\"{a}nen sehr ernst.  ``Wir wollen den Innovationsprozess erhalten.  Open Source muss m\"{o}glich bleiben''.  Man m\"{u}sse aber auch die andere Seite sehen: ``Autorenschutz ist wichtig.  Wer etwas entwickelt hat, muss auch etwas davon haben.  Aber m\"{o}glichst viel Offenheit ist ebenfalls wichtig''.  Zun\"{a}chst gelte es abzuwarten, was die europ\"{a}ische Diskussion und die anschlie{\ss}enden Verhandlungen mit den USA ergeben.   ``Das wird eine der schwierigsten Diskussionen \"{u}berhaupt'', meint Mosdorf sorgenvoll.

Diese enigmatischen Aussagen werfen Fragen auf:
\begin{itemize}
\item
Verwechselt Mosdorf, wenn er sich f\"{u}r den ``Autorenschutz'' einsetzt, m\"{o}glicherweise Patentrecht und Urehberrecht?  Unterliegt er dem Missverst\"{a}ndnis, die ``OpenSource-Bewegung'' wolle alle Programmierer zur Preisgabe ihrer Rechte zwingen?  Treffender w\"{a}re folgende Formulierung gewesen:
\begin{quote}
{\it Autorenschutz (Urheberrecht) ist wichtig.  Wer etwas entwickelt hat, muss auch etwas davon haben.  M\"{o}glichst viel Offenheit ist ebenfalls wichtig.  Softwarepatente schaffen ein Minenfeld, welches die Investitionsrisiken f\"{u}r Softwarefirmen erh\"{o}ht und insbesondere die Autoren offener Software stark gef\"{a}hrdet.}
\end{quote}

\item
Um welche ``europ\"{a}ische Diskussion'' und welche ``Verhandlungen mit den USA'' geht es hier?  Unserer Kenntnis nach steht eine Diskussion der Fachminister an, f\"{u}r die in Deutschland das Bundesjustizministerium zust\"{a}ndig ist.  Dieses h\"{a}lt sich bisher jedoch bedeckt und scheint unserem Einruck nach entschlossen, die EU-Pl\"{a}ne mitzutragen.  Von einer anstehenden Diskussion mit den USA hat der FFII noch nichts erfahren.
\end{itemize}

In dem Handelsblatt-Gespr\"{a}ch k\"{u}ndigt Mosdorf ferner die Errichtung eines ``Kompetenzzentrums f\"{u}r die F\"{o}rderung Freier Software'' an, das Software-Entwicklern als Diskussionsforum und Marktplatz dienen soll.  ``Au{\ss}erdem soll es der Ausbildung und Qualifikation dienen und Unternehmensberater, Handels- und Handwerkskammern mit einbinden.  Ziel ist, die OpenSource-Gemeinde zu st\"{a}rken und zu verbreitern''.

Gleichzeitig lehnt Mosdorf jedoch eine omin\"{o}se ``gesetzliche Regelung nach franz\"{o}sischem Vorbild'' ab.  Damit d\"{u}rfte die die franz\"{o}sische Gesetzesinitiative f\"{u}r offene Software-Standards\footnote{http://www.osslaw.org/articles.html} gemeint sein, die Staatsorgane und Tr\"{a}ger \"{o}ffentlicher Funktionen dazu verpflichten soll, bei der Kommunikation mit dem B\"{u}rger offene Standards einzusetzen und dar\"{u}ber hinaus Quelltexte verwendeter Programme zu hinterlegen.  Mosdorf begr\"{u}ndet seine Ablehnung dieser franz\"{o}sischen Regelung mit der Bemerkung: ``Wir sehen die Rolle des Staates vor allem als die eines neutralen Moderators''.

Dem FFII erscheint dies widerspr\"{u}chlich.  Um ein neutraler Moderator sein zu k\"{o}nnen, muss der Staat sich zun\"{a}chst auf offene Kommunikationsstandards verpflichten lassen.  Das Hinterlegen von Quelltexten hat wiederum nichts mit Quellenoffenheit zu tun:  es ist eine Minimalforderung, die lediglich der Sicherheit der betroffenen Beh\"{o}rden dient.

Ein Kompetenzzentrum w\"{a}re nat\"{u}rlich ein sch\"{o}nes Geschenk vom Staat, aber der FFII w\"{u}nscht sich von der Regierung zu aller erst klare und faire Regeln (wie sie in Frankreich eingef\"{u}hrt sind oder werden).  Ferner w\"{u}nschen wir uns die aktive und regelm\"{a}{\ss}ige Teilnahme der Regierung an einem internet-basierten Dialog zur F\"{o}rderung einer freien informationellen Infrastruktur.  Die Regierung hat hierzu bereits vorbildliche Schritte unternommen, z.B. F\"{o}rderung der Programmierung des GNU Privacy Guard.  Auftr\"{a}ge dieser Art sollten h\"{a}ufiger und systematischer ausgeschrieben werden.  Daran sollten Universit\"{a}ten und Firmen und letztlich die gesamte \"{O}ffentlichkeit beteiligt werden.  Ein r\"{a}umlich gebundenes Kompetenzzentrum w\"{a}re ein m\"{o}glicher dritter Schritt.  Aber gerade wenn der Staat ``neutraler Moderator'' sein will, bietet das Internet daf\"{u}r f\"{u}rs erste die beste m\"{o}gliche Infrastruktur.
\end{sect}

\begin{sect}{etcrms}{Richard M. Stallman}
RMS lie{\ss} w\"{a}hrend mehrerer Ansprachen und Podiumsdiskussionen keine Gelegenheit aus, um sein Publikum zur Aktion gegen Softwarepatente aufzurufen.  Laut Stallman ist jeder Benutzer von GNU/Linux und freier Software moralisch verpflichtet, die Eurolinux-Petition zu unterschreiben.  Jede Firma, die im GNU/Linux-Umfeld arbeitet ist verpflichtet, ihre Kunden auf die Patentgefahr aufmerksam zu machen.  Ein Petitions-Werbeaufkleber sollte z.B. mit dem SuSE Linux oder Redhat Linux Paket an jeden K\"{a}ufer ausgeliefert werden.  Stallman kam pers\"{o}nlich zum FFII-Stand, um dort inmitten einer Menschentraube seine Unterzeichnungszeremonie zu zelebrieren.
\end{sect}

\begin{sect}{etccox}{Alan Cox}
Linuxkern-Entwickler Alan Cox, der bei Redhat arbeitet, trug durchweg unseren Eurolinux-Petitions-Aufkleber an seinem roten Hut.  Cox kennt die Problematik sowohl von der informatischen als auch von der rechtlichen Seite her gut und wird sich in den n\"{a}chsten Wochen sowohl bei Redhat als auch bei einigen britischen Politikern f\"{u}r unsere Sache einsetzen.  Dies erz\"{a}hlte er uns w\"{a}hrend mehrerer Gespr\"{a}che am FFII-Stand.
\end{sect}

\begin{sect}{etcwclu}{Swantje Weber-Cludius}
Frau Weber-Cludius ist beim Bundeswirtschaftsministerium f\"{u}r Patentrecht zust\"{a}ndig.  Sie beteiligte sich am Freitag und Samstag intensiv an unseren Vortrags- und Diskussionsveranstaltungen und nutzte auch dar\"{u}ber hinaus die Gelegenheit zu stundenlangen Gespr\"{a}chen mit Wolfgang Tauchert, PA Springorum, Peter Gerwinski, Hartmut Pilch, Jean-Paul Smets, Holger Blasum und anderen Protagonisten der SWPAT-Debatte.
\end{sect}

\begin{sect}{etctauch}{Wolfgang Tauchert}
In privaten Gespr\"{a}chen erl\"{a}uterte der Datenverarbeitungs-Experte des Deutschen Patentamtes seine Position weiter.  Ihn st\"{o}rt insbesondere, dass die Rechtssprechung sich jahrelang in Richtung auf Ausweitung der Patentierbarkeit entwickelte und niemand h\"{o}rbar etwas dagegen sagte.  Wenn nun die betroffenen Kreise anderer Meinung seien, dann sollten sie nicht gegen angebliche Rechtsmissbr\"{a}uche zetern sondern lieber die Mittel des Systems nutzen und sich in die juristische Diskussion einbringen, um die Rechtssprechung des BGH auf den alten Kurs zur\"{u}ckzubringen.  Das sei auch dann noch m\"{o}glich, wenn die Computerprogramm-Ausnahme aus dem EP\"{U}/BPatG gestrichen worden sei.
\end{sect}

\begin{sect}{etcspring}{PA Dipl.Inf. Dr. Harald Springorum}
PA Springorum hielt es f\"{u}r nicht ganz aussichtslos, ein Gesetz oder ein Grundsatzurteil zu erwirken, welches GPL-Software generell als nicht-gewerblich definiert.  Er kritisiert jedoch, dass bei Firmen wie SuSE eine Wertsch\"{o}pfung stattfindet, an der die freien Entwickler, auf deren Schultern SuSE stehe, nicht fair beteiligt w\"{u}rden.  Das Patentwesen schaffe hier durch das Arbeitnehmererfindergesetz mehr Gerechtigkeit.

Herr Springorum argumentierte auch sonst bevorzugt mit moralischen Argumenten f\"{u}r die Patentexpansion, nicht ohne gleichzeitig die Patentgegner als Moralisten/Ideologen darzustellen.

Die naheliegenden Gegenargumente sind
\begin{itemize}
\item
Undank ist der Welt Lohn.  Auch diejenigen, welche die jahrzehntelange mathematische Vorarbeit f\"{u}r das MP3-Patent geleistet haben, gingen leer aus (und d\"{u}rfen heute noch nicht einmal die MP3-Technologie frei verwenden).  Bei der Masse der Trivialpatente ist es noch schlimmer:  unz\"{a}hlige Parallelerfinder werden enteignet.  Abgesehen davon belohnt der ``digitale Kapitalismus'' generell nicht unbedingt bevorzugt diejenigen, die zum Fortschritt der Zivilisation beitragen.  Das sieht man schon an der Qualit\"{a}t des Privatfernsehens oder des kommerziellen Internets.  Den digitalen Kapitalismus durch Patente gerecht machen zu wollen, ist vielleicht gerade eine jener gef\"{a}hrlichen Utopien, die den Himmel versprechen und die H\"{o}lle schaffen: totalit\"{a}re Ideologie in Reinform.  Professor Lessig (Harvard-Verfassungsrechtler) nennt\footnote{http://futurezone.orf.at/futurezone.orf?read=detail\&id=30188\&tmp=4994} das nicht umsonst ``Software-Stalinismus''.

\item
Firmen wie SuSE und Redhat sind von sich aus bem\"{u}ht, Beitr\"{a}ge zur Linux-Gemeinde zu leisten.  Sie bezahlen z.B. zahlreiche Entwickler daf\"{u}r, dass sie freie Software schreiben.

\item
Das Distributionsgesch\"{a}ft ist bei der Freien Software kein notwendiges Glied der Wertsch\"{o}pfungskette, sofern es eine solche \"{u}berhaupt gibt.  Das Distributionsgesch\"{a}ft wird m\"{o}glicherweise bald ganz sogar verschwinden und durch nicht-kommerzielle verteilte Systeme wie Debians apt-get ersetzt werden.  Andererseits erf\"{u}llt die SuSE-Distribution heute eine wichtige volkswirtschaftliche Rolle: sie erleichtert zahlreichen heimischen KMUs den Zugang zum Weltmarkt.  Das Patentrecht w\"{a}re in diesem System gar nicht anwendbar:  man m\"{u}sste Geb\"{u}hren f\"{u}r Millionen von Programmkopien erheben, die niemand verwendet.  Das Patentrecht w\"{u}rde somit fortschrittlichere Distributionsformen wie Shareware benachteiligen und eine R\"{u}ckkehr in die Zeiten des Verkaufs von Glanzkartons am Ladentisch erzwingen.  Das w\"{a}re auch nicht im Sinne des Patentrechts, denn es handelt sich hierbei nicht um ein Kopierrecht (Urheberrecht) um ein Nutzungsrecht:  die Lizenzgeb\"{u}hr sollte erst bei der tats\"{a}chlichen Verwendung der patentierten Technik erhoben werden.  Egal ob das Programm aus dem Internet oder von einer SuSE-CD bezogen wurde.

\item
Unsere Hauptargumente liegen nicht im Bereich der Moral sondern der Volkswirtschaft.  F\"{o}rdern Softwarepatente die Innovation und den Wettbewerb?  F\"{u}hren Sie wenigstens zu einem Anwachsen der F\&E-Ausgaben bei den Softwareunternehmen?  Unsere Erfahrung legt nahe, dass beides zu verneinen ist.  Wissenschaftliche Studien best\"{a}tigen dies.
\end{itemize}
\end{sect}

\begin{sect}{etcsiemens}{Gespr\"{a}che am Siemens-Stand}
Mehrere Vertreter der Siemens IT Services meinten, dass Arno K\"{o}rber, der Chef der Siemens-Patentabteilung, der in der \"{O}ffentlichkeit immer wieder (zuletzt unter dem Mantel des ZVEI) vehement f\"{u}r die Patentexpansion eingetreten ist, nicht wirklich die Interessen von Siemens vertrete.  Siemens habe von der Patentexpansion durchaus mehr Nachteile als Vorteile zu erwarten.

Da Siemens jedoch derzeit noch mit Microsoft ``verheiratet'' sei, sei es unwahrscheinlich, dass das Siemens-Logo auf der Eurolinux-Petition erscheinen d\"{u}rfe.  Man k\"{o}nne jedoch vermutlich vom Siemens-Vorstand eine Erkl\"{a}rung erwirken, die zu M\"{a}{\ss}igung bei der Patentexpansion r\"{a}t und die Durchf\"{u}hrung einer volkswirtschaftlichen Studie empfiehlt.

Die Informatiker und Forscher bei Siemens reagierten in j\"{u}ngster Zeit recht irritiert auf Anweisungen von seiten der Patentabteilung, m\"{o}glichst alles, was irgendwie innovativ sein k\"{o}nnte, der patentrechtlichen Verwertung zuzuf\"{u}hren.  Diese Anweisung h\"{a}tten deutlich gemacht, dass Patente nicht mehr als ``Mittel zum Schutz unserer Erfindungen'' sondern als ``Tauschwaren'' betrachtet w\"{u}rden.  Den Mitarbeitern sei klar, dass mit dem Patentwesen irgendetwas faul ist, und es k\"{o}nne nicht angehen, dass Siemens sich in der \"{O}ffentlichkeit f\"{u}r ein solches System stark mache.  Solche Stellungnahmen seien, wenn sie wirklich existierten, gegen die Konzernpolitik gerichtet.  Es m\"{u}sse im Interesse der Konzernleitung liegen, das klarzustellen.

Siemens verwendet laut Mitarbeiter-Aussagen gelegentlich Softwarepatente, um zu verhindern, dass Partnerfirmen sich selbstst\"{a}ndig machen und von Siemens entwickelte Technik eigenst\"{a}ndig vermarkten.  Doch dieses Ziel l\"{a}sst sich ebenso ohne Softwarepatente erreichen.  Meistens gen\"{u}gt einfach der Erfahrungs-Vorsprung der Siemens-Mitarbeiter, welche die Technik in vielen Jahren und vielen kleinen Schritten entwickelt haben.  Wo das nicht reiche, habe man auch noch Urheberrecht und Betriebsgeheimnis.
\end{sect}

\begin{sect}{etcfirmen}{Gespr\"{a}che an anderen St\"{a}nden}
Wir f\"{u}hrten noch Gespr\"{a}che mit einem Gro{\ss}teil der Ausstellerfirmen.  Viele Firmen entschlossen sich daraufhin, in den n\"{a}chsten Tagen an unserer Petition teilzunehmen.
\end{sect}

\begin{sect}{etcradio}{Radiointerview}
Ein Radiojournalist f\"{u}hrte ein 20-min\"{u}tiges Gespr\"{a}ch mit Hartmut Pilch, das gesendet werden soll.  Er hatte zuvor ebenfalls mit Stefan Meretz und Stefan Merten gesprochen.
\end{sect}
\end{sect}

\begin{sect}{dank}{Danksagung}
Unser Stand konnte nur deshalb von Anfang an gut aussehen, weil Arnim Rupp und sein Bruder sch\"{o}ne Schilder anfertigen lie{\ss}en und am Tag vor der Messe anbrachten.

Dank Holger Blasums Bem\"{u}hungen konnten wir am Stand sch\"{o}n bedruckte FFII-Hemden tragen.  Ihm ist auch der schnelle Rechner mit gro{\ss}em Bildschirm zu verdanken, mit dem wir aufwarten konnten.

Felix Nenz sorgte f\"{u}r weitere Hinweisschilder und Getr\"{a}nke.  Er beantwortete drei Tage lang geduldig unseren Besuchern Fragen und verzichtete dabei auf die Teilnahme an vielen der interessantesten Veranstaltungen.

Holger, Felix, Bernhard Reiter, Frank Kormann und Hartmut Pilch verbrachten je 3-4 Tage am FFII-Stand, ohne jegliche Entsch\"{a}digung f\"{u}r Arbeitszeitausfall oder Hotelkosten in Anspruch zu nehmen.

Der Linuxtag e.V. leistete gro{\ss}artige Arbeit und unterst\"{u}tzte uns unb\"{u}rokratisch.

Jean-Paul Smets nahm sich trotz dr\"{a}ngender Aufgaben und mangelnden Schlafes einen Tag.  Seine Reisespesen zahlte Mandrake\footnote{http://www.mandrake.fr}.

Diverse Spesen konnten nur deshalb bezahlt werden, weil Infomatec AG\footnote{http://www.infomatec.de}, SuSE Linux AG\footnote{http://www.suse.de}, Harald Welte\footnote{http://www.sunbeam.franken.de}, Intradat AG\footnote{http://www.intradat.de}\ usw. den FFII mit gro{\ss}z\"{u}gigen Spenden unterst\"{u}tzt haben.

Firmen wie SuSE Linux AG\footnote{http://www.suse.de}, Intevation GmbH\footnote{http://www.intevation.de}\ usw. sahen gro{\ss}z\"{u}gig dar\"{u}ber hinweg, dass einige ihrer bezahlten Mitarbeiter ihre Erwerbst\"{a}tigkeit vernachl\"{a}ssigten.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

