\begin{subdocument}{swplxtg26}{FFII auf Linuxtag 2000}{http://swpat.ffii.org/dates/2000/linuxtag/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/08/21 par Odile BÉNASSY\footnote{http://obenassy.free.fr/}}{Au cours des Journ\'{e}es Linux 2000 \`{a} Stuttgart, l'association FFII a d\'{e}voil\'{e} de nouvelles pratiques ill\'{e}gales de la justice allemande et europ\'{e}enne sur les brevets, ainsi que les projets de rendre brevetables les algorithmes des logiciels et les pratiques \'{e}conomiques et sociales de toutes sortes qui en d\'{e}coulent.}
Les LinuxTag\footnote{http://www.linuxtag.de} se sont d\'{e}roul\'{e}es sur 4 jours cette ann\'{e}e, du 29 juin au 2 juillet, sur les lieux de la foire de Stuttgart. La fr\'{e}quentation a \'{e}t\'{e} d'environ 20 000 personnes. La FFII s'est tenue sur le stand 6.0.1.14 \`{a} l'invitation des organisateurs\footnote{http://www.linuxtag.de/2000/deutsch/shownews.php3?id=0079}, avec pour th\`{e}me les brevets sur les logiciels\footnote{http://swpat.ffii.org/index.fr.html}.  

Nous avons pr\'{e}sid\'{e} une manifestation durant laquelle certaines \'{e}vidences ont \'{e}t\'{e} d\'{e}gag\'{e}es.

\begin{sect}{stand}{Notre permanence sur le stand de la FFII}
\begin{itemize}
\item
Signature de la p\'{e}tition Eurolinux contre les brevets sur les logiciels 

\item
Distribution du mod\`{e}le de lettre-p\'{e}tition  

\item
Pr\'{e}sentation de notre documentation\footnote{http://swpat.ffii.org/archive/index.fr.html} 

\item
Distribution de notre CD de Documentation SWPAT\footnote{http://swpat.ffii.org/archive/doku/cd/index.fr.html}

\item
Distribution d'autocollants et de tracts anti-SWPAT

\item
Annonces concernant les autres projets de la FFII, par exemple EGeld\footnote{http://egeld.ffii.org/index.html}/Nerdbank\footnote{http://www.nerdbank.org} et Wortbasar\footnote{http://wortbasar.ffii.org/index.html} 
\end{itemize}
\end{sect}

\begin{sect}{preleg}{Conf\'{e}rence au Centre A des Congr\`{e}s, salle 1 }
\begin{sect}{tauchert}{Tauchert: Brevetabilit\'{e} des logiciels -- la pratique de l'Office allemand des Brevets }
\begin{sect}{tauchprep}{Textes pr\'{e}paratoires }
Extrait de la discussion pr\'{e}paratoire sur le r\'{e}sum\'{e} de la conf\'{e}rence de Monsieur Tauchert\footnote{http://swpat.ffii.org/dates/2000/linuxtag/tauchert/index.de.html}
\end{sect}

\begin{sect}{tauchtemp}{Horaire}
Vendredi 30 juin  11.00-12.00
\end{sect}

\begin{sect}{tauchpers}{Conf\'{e}rencier}
Dr. Wolfgang TAUCHERT Chef du Service des Technologies et Supports de l'Information \^{E} l'Office allemand des Brevets et des Marques 
\end{sect}

\begin{sect}{tauchenhv}{R\'{e}sum\'{e} }
La loi allemande sur les brevets (PatG) interdit dans son article 1 le d\'{e}p\^{o}t de brevet sur un logiciel en tant que tel. Le fait m\^{e}me que le l\'{e}gislateur ait formul\'{e} cette limitation ``en tant que telle'' montre bien qu'il consid\`{e}re cette interdiction comme un probl\`{e}me. La liste des exceptions de l'article 1 est formul\'{e}e sans ordre clair ce qui la rend difficile \`{a} appliquer. De plus, la limitation de certains objets ``en tant que tels'' permet d'innombrables interpr\'{e}tations. A cause de cela, la Cour de Justice F\'{e}d\'{e}rale (CJF) dans les faits n'a pas appliqu\'{e} directement l'article 1. Elle a utilis\'{e} le concept de ``technicit\'{e}'', lequel est utilis\'{e} indirectement, dans les paragraphes suivants de la loi et \`{a} c\^{o}t\'{e} de ceux de nouveaut\'{e} et d'inventivit\'{e}, comme crit\`{e}re de base permettant de d\'{e}limiter le champ d'application de la loi. Certes un tel concept ne suffit pas \`{a} \'{e}clairer la PatG, mais la CJF en a mis au point une d\'{e}finition claire, souvent cit\'{e}e :   

\begin{quote}
{\it une exploitation planifi\'{e}e mettant en jeu des forces naturelles sous contr\^{o}le, sans activit\'{e} humaine intercal\'{e}e }
\end{quote}

Au d\'{e}but des ann\'{e}es 90, la CJF a refus\'{e} des d\'{e}p\^{o}ts de brevets sur des logiciels en se basant sur le manque de technicit\'{e}. Dans le proc\`{e}s dit des ``caract\`{e}res chinois'', la CJF a soulign\'{e} que l'essentiel de l'inventivit\'{e} ne se situait pas dans la domination des forces naturelles (\'{e}lectrons) mais dans la logique du programme, et que par cons\'{e}quent le texte du programme que Siemens pr\'{e}sentait dans le but d'obtenir un brevet ne faisait qu'utiliser la destination naturelle de l'ordinateur. Depuis, on appelle cette fa\c{c}on de penser de la CJF la ``th\'{e}orie fondamentale''.

Cette th\'{e}orie fondamentale a soulev\'{e} des critiques vigoureuses dans les cercles d'experts. Au milieu des ann\'{e}es 90, avec le verdict des ``ordinateurs de plong\'{e}e'' la CJF s'est d\'{e}tourn\'{e}e de la th\'{e}orie fondamentale et s'est int\'{e}ress\'{e}e au crit\`{e}re de l'utilisation du programme. 

Au cours du Congr\`{e}s de l'UNION, fin 1997 \`{a} Munich, les juristes sp\'{e}cialistes des brevets qui \'{e}taient pr\'{e}sents ont argument\'{e} d'une mani\`{e}re convaincante sur le fait que chaque syst\`{e}me d'ordinateur en fonctionnement \'{e}tait technique et que la question de la technicit\'{e} et de l'inventivit\'{e} n'\'{e}tait pas une chose que l'on pouvait mesurer. Ce qui signifie que le noyau d'inventivit\'{e} ne devait pas forc\'{e}ment se trouver dans le domaine technique. Peu apr\`{e}s, les bureaux europ\'{e}en et allemand des brevets ont fait volte-face sur ce point, et la 17\`{e}me Commission S\'{e}natoriale sur les Brevets a abandonn\'{e} sa position restrictive.

En juillet 1999, un verdict de la CJF a \'{e}nonc\'{e} qu'un syst\`{e}me pilot\'{e} par un logiciel pour la fixation de prix de ventes \'{e}tait par principe brevetable. Mon service avait tout d'abord rejet\'{e} le d\'{e}p\^{o}t de brevet. Le coll\`{e}gue qui avait trait\'{e} le dossier s'en tenait en effet \`{a} la th\'{e}orie fondamentale. La CJF n'en rejeta pas moins la th\'{e}orie fondamentale par un verdict de principe et renvoya la demande de brevet, sous la forme d'une demande d'aide limit\'{e}e, \`{a} nos services, pour un nouvel examen.     

Deux verdicts de la CJF, sur les th\`{e}mes de l'``analyse linguistique'' et de ... sont all\'{e}s depuis encore plus loin. La Cour cr\'{e}a le terme de ``dispositif logitechnique''. Ainsi, tous les programmes d'ordinateur ont droit \`{a} une protection par le brevet. A l'Office des Brevets et Marques nous n'avions pas pens\'{e} que la Cour irait aussi loin. La CJF revient l\`{a} \`{a} une position qu'elle avait d\'{e}j\`{a} eue dans les ann\'{e}es 60, avant que le groupe des math\'{e}maticiens n'impose un tournant restrictif et ne s'assure pour longtemps que le logiciel ne puisse \^{e}tre directement brevet\'{e}. Il s'agit l\`{a} d'une victoire politique du lobby des Sciences de l'Ing\'{e}nieur. 
\end{sect}

\begin{sect}{frag}{R\'{e}ponses aux questions et aux interventions}
\begin{sect}{jur}{WRiek: Comment se fait-il qu'une poign\'{e}e de juristes sp\'{e}cialistes des brevets puisse changer aussi fondamentalement les normes ? Ne seraient-ce pas l\`{a} des questions politiques qui d\'{e}passent les comp\'{e}tences des tribunaux et administrations ?}
Ceux qui appliquent la loi ont le droit de l'interpr\'{e}ter, m\^{e}me s'il est vrai que de tels probl\`{e}mes rel\`{e}vent aussi du domaine politique. La vision des ing\'{e}nieurs s'est impos\'{e}e. Il ne s'agit pas l\`{a} des int\'{e}r\^{e}ts mat\'{e}riels d'un groupe mais de s'affranchir d'un syst\`{e}me qui incluait d'insupportables contradictions. Nous avons accueilli les derni\`{e}res d\'{e}cisions de la CJF comme une lib\'{e}ration. D'autre part, la question de savoir si oui ou non la loi va \^{e}tre modifi\'{e}e reste secondaire puisque nous disposons d\'{e}j\`{a} d'un syst\`{e}me de crit\`{e}res permettant de conclure. Si un non juriste comme vous veut exercer une influence il lui faut tout d'abord comprendre ce syst\`{e}me. Votre exigence de maintien de l'interdiction de programmer pour les ordinateurs manque son but. Au contraire, ce n'est que lorsque ce sera fait que de nouvelles possibilit\'{e}s appara\^{\i}tront pour vous de faire bouger la loi, de faire valoir vos propositions. 
\end{sect}

\begin{sect}{oek}{phm: Est-ce vraiment une bonne chose de rendre les logiciels brevetables sans avoir r\'{e}alis\'{e} auparavant une \'{e}tude syt\'{e}matique des effets \'{e}conomiques d'une telle modification ?}
Absolument. Nous n'avons aucun besoin d'une \'{e}tude \'{e}conomique. La r\'{e}alit\'{e} parle d'elle-m\^{e}me. Le march\'{e} a d\'{e}j\`{a} rendu son verdict. Chaque ann\'{e}e il nous arrive des milliers de d\'{e}p\^{o}ts de brevets sur des logiciels, et notre syst\`{e}me de brevets rapporte de l'argent. Sans aucune subvention de l'Etat, il nourrit 20 000 sp\'{e}cialistes des brevets.  
\end{sect}

\begin{sect}{raub}{Siepmann: Ne pourrait-on pas, avec les m\^{e}mes arguments, l\'{e}gitimer un syndicat de voleurs, qui aurait gagn\'{e} sa place sur le march\'{e} sans la moindre subvention ? }
Le syndicat de voleurs se conduit d'une mani\`{e}re ill\'{e}gale. Ce n'est pas notre cas. Nous sommes prot\'{e}g\'{e}s par les verdicts fondamentaux de la CJF. La CJF est habilit\'{e}e \`{a} interpr\'{e}ter les lois, le syndicat de voleurs ne l'est pas...
\end{sect}

\begin{sect}{pres}{phm: Monsieur Tauchert, vous \'{e}noncez ici des v\'{e}rit\'{e}s essentielles que l'on a rarement l'occasion d'entendre aussi explicitement. Nous autorisez-vous \`{a} citer ce que vous venez de dire dans nos communiqu\'{e}s de presse ?}
Certainement. Ce sont l\`{a} mes positions.
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{spring}{Springorum: Apprivoiser le ``monstre'' : les brevets -- L'id\'{e}e d'un consortium de protection}
\begin{sect}{springpers}{}
PA Dipl.Inf. Dr. Harald Springorum
\end{sect}

\begin{sect}{sprintenhv}{}
Depuis 1990 environ, les programmeurs de logiciels libres et autres victimes des brevets sur les logiciels r\'{e}fl\'{e}chissent \`{a} la possibilit\'{e} de contracter des alliances pour une protection r\'{e}ciproque contre les brevets. Une alliance de ce genre pourrait acqu\'{e}rir elle-m\^{e}me des droits de brevets et se servirait de sa force de n\'{e}gociation pour prot\'{e}ger la ``propri\'{e}t\'{e} intellectuelle commune''.\footnote{http://swpat.ffii.org/archive/miroir/sarxe/index.fr.html}

PA Springorum propose alors un mod\`{e}le dont l'efficacit\'{e} est ensuite discut\'{e}e. Wolfgang Tauchert (Bureau Allemand des Brevets), Swantje Weber-Cludius (BMWi), Peter Gerwinski (Soci\'{e}t\'{e} G.N.U.), Werner Riek (journaliste) et Hartmut Pilch (F.F.I.I.) sont les principaux intervenants de cette discussion.

PA Springorum sugg\`{e}re ensuite qu'une association de d\'{e}veloppeurs de logiciels se tienne inform\'{e}e sur les menaces de brevets dangereux et d\'{e}pose des recours contre ceux qui pourraient \^{e}tre \'{e}vit\'{e}s. Une telle association pourrait m\^{e}me offrir un contrat d'assurance contre les risques dus aux brevets, en partenariat avec une soci\'{e}t\'{e} d'assurances.

En plus de cela, une soci\'{e}t\'{e} \`{a} but non lucratif (sous forme d'un groupement de SARL ou de SA \`{a} but professionnel) pourrait \^{e}tre fond\'{e}e pour d\'{e}poser des brevets sur les d\'{e}couvertes des d\'{e}veloppeurs de logiciels libres. Elle les mettrait gratuitement \`{a} la disposition du logiciel libre et en interdirait l'utilisation \`{a}4ous ceux qui attaquent le logiciel libre \`{a} l'aide des brevets.

Des difficult\'{e}s appra\^{\i}traient peut-\^{e}tre au sein de cette alliance, surtout si le groupement de SA ralliait des membres puissants comme IBM. Mais ces difficult\'{e}s devraient pouvoir \^{e}tre surmont\'{e}es car il ne s'agirait pas de cr\'{e}er un monopole, bien au contraire.

Il existe d\'{e}j\`{a} des exemples de telles organisations, comme l'Association pour une Juste Utilisation des Droits d'Emission Radiophonique, \`{a} Cologne. P.A. Springorum d\'{e}crit les statuts de cette association.
\end{sect}

\begin{sect}{springfrag}{}
\begin{sect}{restr}{Les juristes commettent souvent l'erreur de sur\'{e}valuer le pouvoir d'exclusion des licences restrictives publiques (les licences de ``copyleft`` telles que la GPL de GNU). Ces licences n'ont qu'une tr\`{e}s petite valeur de protection. D\`{e}s qu'un composant de programme a \'{e}t\'{e} impl\'{e}ment\'{e} dans un logiciel sous GPL, il devient impossible d'emp\^{e}cher qui que ce soit de l'utiliser. }
Il faudrait donc modifier la GPL pour que cela devienne possible. Quiconque \'{e}rige la GPL en vache sacr\'{e}e se rend responsable de sa propre impossibilit\'{e} \`{a} se d\'{e}fendre contre les brevets.
\end{sect}

\begin{sect}{modul}{Apparemment nous serions en train de cr\'{e}er une nouvelle licence ouverte g\'{e}n\'{e}rale qui permettrait \`{a} un groupement de soci\'{e}t\'{e}s anonymes de se servir de la G.P.L. comme d'une arme. Mais cette arme serait encore bien \'{e}mouss\'{e}e. En effet, les logiciels libres sont ouvertement disponibles sur l'Internet. Tout le monde peut les utiliser, que ce soit ou non de mani\`{e}re l\'{e}gale. Si nous interdisions \`{a} Microsoft d'utiliser tel composant, il suffirait \`{a} cette entreprise d'\'{e}crire son programme de mani\`{e}re que pour le composant en question un module sous GPL (par exemple avec Corba) puisse \^{e}tre t\'{e}l\'{e}charg\'{e} sur l'Internet. }
Ceci pourrait d\'{e}j\`{a} causer pas mal de tort \`{a} Microsoft.
\end{sect}

\begin{sect}{patfund}{Que ferions-nous si notre adversaire \'{e}tait une soci\'{e}t\'{e} de brevets qui n'\'{e}crirait aucun programme elle-m\^{e}me mais se contenterait de produire plusieurs brevets par semaine, de les acheter et d'en vendre la licence au plus offrant avec \'{e}ventuellement un droit d'exclusivit\'{e} ?}
Notre groupement de SA resterait impuissant devant de telles soci\'{e}t\'{e}s, sauf s'il \'{e}tait assez riche pour couvrir les ench\`{e}res des grosses soci\'{e}t\'{e}s. Il ne nous reste qu'\`{a} esp\'{e}rer que ce cas-l\`{a} ne serait pas fr\'{e}quent.  
\end{sect}

\begin{sect}{anarch}{Le monde du logiciel libre consiste en un grand nombre de programmeurs isol\'{e}s qui seraient tr\`{e}s contrari\'{e}s de devoir s'organiser d'une mani\`{e}re un peu militaire, au sein d'une association ou d'un groupement de soci\'{e}t\'{e}s. Parmi les petites et moyennes entreprises - dont la participation serait souhaitable - on trouve malheureusement beaucoup d'esprit de concurrence et de faibles dispositions \`{a} se sacrifier aux int\'{e}r\^{e}ts du groupe. Le peu de succ\`{e}s des nombreuses tentatives similaires aux Etats-Unis\footnote{http://swpat.ffii.org/archive/miroir/sarxe/index.fr.html} semble bien confirmer ce point de vue.  }
Evidemment, les membres de l'association et du groupement de soci\'{e}t\'{e}s devraient adh\'{e}rer sur la base du volontariat. Il faudrait aussi, sans aucun doute, une bonne impulsion de d\'{e}part. Il faudrait qu'une entreprise comme SuSE, qui a absorb\'{e} ces derniers temps les r\'{e}sultats d'innombrables programmeurs travaillant gratuitement, y investisse un million de marks.  
\end{sect}

\begin{sect}{aufwand}{La somme de travail initialement n\'{e}cessaire est tellement \'{e}norme que m\^{e}me un groupement d'entreprises bien dot\'{e} en personnel n'aurait pas de grandes chances de succ\`{e}s. Il faudrait en effet examiner des centaines de milliers de lignes de programmes \`{a} la recherche d'\'{e}ventuels pr\'{e}judices de centaines de milliers de brevets. De plus, il faudrait r\'{e}ussir \`{a} donner envie aux progammeurs b\'{e}n\'{e}voles d'examiner eux-m\^{e}mes leurs travaux \`{a} la recherche de trivialit\'{e}s brevetables et de les documenter. Or c'est une chose bien connue que les vrais programmeurs n'aiment pas \'{e}crire de la documentation. Encore moins de la documentation sur le sujet des brevets, qui requiert en plus une exp\'{e}rience particuli\`{e}re. Il n'est pas forc\'{e}ment n\'{e}cessaire d'\^{e}tre juriste pour \'{e}crire au sujet des brevets, mais cela demande tout de m\^{e}me beaucoup d'efforts et un type d'exp\'{e}rience que l'on ne trouve pas parmi les d\'{e}veloppeurs libres. Finalement, le groupement de SA devrait d\'{e}penser au minimum 100 000 marks en personnel pour avoir l'espoir d'atteindre ne serait-ce que 10 \percent{} de ses objectifs.   }
Ce n'est pas s\^{u}r. Il faut former les d\'{e}veloppeurs. Si la communaut\'{e} Open Source campe sur ses habitudes et reste dans son coin, alors ce sera de sa faute. 
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{smets}{Smets: Software Patent Tactics for OpenSource Developpers }
\begin{sect}{smetstemp}{}
Samedi 1er juillet 16.00-17.00
\end{sect}

\begin{sect}{smetsprep}{}
J.P. Smets: Practical Software Patent Tactics for OpenSource Developpers\footnote{http://www.freepatents.org/shanghai/shanghai.pdf}
\end{sect}

\begin{sect}{smetsfrag}{}
\begin{sect}{anreiz}{La GPPL de Monsieur Smets ne sera d'aucun secours contre les entreprises de valorisation des brevets. De plus si elle r\'{e}ussit, elle favorisera une plus grande concentration des licences entre les mains de ces entreprises.  }
Oui, sans doute. Quand Microsoft vend ses droits \`{a} une entreprise de valorisation des brevets puis acquiert une licence aupr\`{e}s de cette entreprise, nous ne pouvons rien faire avec notre GPPL. Sauf si nous avons pr\'{e}alablement d\'{e}pos\'{e} les composants avec lesquels doivent travailler la soci\'{e}t\'{e} de valorisation des brevets et le bureau des brevets.
\end{sect}

\begin{sect}{freipat}{Il existe cependant une autre licence libre (GPPL) avec laquelle nous pouvons agir \'{e}galement contre les soci\'{e}t\'{e}s de valorisation des brevets.Nous pourrions \'{e}laborer une licence libre (GPPL) qui interdirait d'utiliser en m\^{e}me temps une licence propri\'{e}taire. Ce qui veut dire que chaque fois que l'on mettrait en oeuvre un composant prot\'{e}g\'{e} par une licence propri\'{e}taire il ne serait plus possible d'utiliser notre composant libre. Et ceci sans recours. Les licences ouvertes prot\`{e}gent le patrimoine commun d'une mani\`{e}re offensive. Toute n\'{e}gociation individuelle serait impossible, tout comme dans le cas du droit d'auteur sous GPL. Gr\^{a}ce \`{a} une mani\`{e}re forte comme celle-l\`{a}, nous pourrions enlever toute valeur aux licences priv\'{e}es et ass\'{e}cher le commerce des entreprises de valorisation des brevets. En effet, qui voudra encore acheter une licence s'il ne reste presque aucune situation dans laquelle il puisse l'impl\'{e}menter dans le cadre l\'{e}gal ?         }
En effet, un tel plan serait pertinent pour toucher aussi les entreprises de valorisation des brevets. Cela dit, j'aurais besoin d'un peu plus de temps pour r\'{e}fl\'{e}chir aux cons\'{e}quences pratiques de cette strat\'{e}gie. 
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{far}{FFII: Notre travail de lobbying contre les brevets : quelles sont les r\'{e}alisations et que reste-t-il \`{a} accomplir ?}
\begin{sect}{farpers}{}
Hartmut Pilch et Holger Blasum
\end{sect}

\begin{sect}{farenhv}{}
La justice sur les brevets s'est elle-m\^{e}me fix\'{e} certaines r\`{e}gles qui sont en fait contraires aux lois en vigueur tant dans la lettre que dans l'esprit, ainsi que la FFII a pu l'\'{e}tablir par son travail de recherche et de documentation. Cette constatation nous permet de restreindre la marge d'argumentation des expansionnistes des brevets. Nos conf\'{e}rences \`{a} Berlin et aux Journ\'{e}es Linux l'ont clairement montr\'{e}. Par ailleurs, les partisans d'une g\'{e}n\'{e}ralisation des brevets b\'{e}n\'{e}ficient de bien moins de soutien du le monde \'{e}conomique qu'ils aiment \`{a} le laisser entendre : le front se situe entre les techniciens de l'information et les sp\'{e}cialistes des brevets, et non entre les industries du logiciel libre et du logiciel propri\'{e}taire. Partout nous enfon\c{c}ons des portes ouvertes, m\^{e}me aupr\`{e}s de grosses soci\'{e}t\'{e}s comme Siemens. C'est bien agr\'{e}able, et nous pouvons faire beaucoup de choses, par exemple:  

\begin{itemize}
\item
Fabriquer et distribuer des autocollants et des imprim\'{e}s de toutes sortes pour faire diffuser notre p\'{e}tition et notre documentation. 

\item
Poursuivre le travail commenc\'{e} par Holger Blasum : r\'{e}unir et digitaliser les documents papier sur le sujet.

\item
Publier les commentaires sur ces documents\footnote{http://swpat.ffii.org/papiers/index.fr.html}.

\item
D\'{e}velopper un peu plus l'actuel CD de documentation\footnote{http://swpat.ffii.org/archive/doku/cd/index.fr.html}

\item
Faire passer notre message aupr\`{e}s de soci\'{e}t\'{e}s et d'hommes politiques cibl\'{e}s. (Quelques-uns des participants \`{a} cette discussion ont de bons contacts avec certaines entreprises.)

\item
Partir \`{a} la recherche d'\'{e}conomistes ayant des connaissances techniques pour qu'ils publient de nouvelles \'{e}tudes d'impact. 

\item
Ecrire encore des lettres \`{a} diff\'{e}rentes institutions susceptibles de nous aider ou d'infl\'{e}chir leurs positions actuelles. Par exemple, nous pourrions inciter Siemens \`{a} prendre des distances par rapport \`{a} ses r\'{e}centes manifestations en faveur des brevets sur les logiciels, et \`{a} publier \`{a} la place une prise de position pr\'{e}conisant une enqu\^{e}te sans parti-pris sur les effets \'{e}conomiques des brevets.

\item
D\'{e}poser des brevets et des sp\'{e}cifications sous la licence g\'{e}n\'{e}rale ouverte (GPPL).  (Les frais de d\'{e}p\^{o}t de sp\'{e}cifications au Bureau allemand des Brevets sont d'environ 100 Marks. Il est possible d'ailleurs de vendre des parts de ces droits, chacun des ayant-droit ayant alors la possibilit\'{e} d'imposer son titre de licence aupr\`{e}s d'un tiers.)

\item
Mettre en forme les projets de licences ouvertes et exiger des conditions financi\`{e}res plus favorables pour ce type de licences aupr\`{e}s du l\'{e}gislateur, dans le cadre du droit appliqu\'{e} par la CJF. Pr\'{e}senter d'autres propositions de lois, par exemple pour \'{e}riger en principe l'aspect non-commercial des logiciels sous GPL.

\item
Comment prouver que vous etiez la avant?\footnote{http://swpat.ffii.org/analyse/anterior/index.fr.html}
\end{itemize}
\end{sect}
\end{sect}
\end{sect}

\begin{sect}{etc}{Autres interventions}
\begin{sect}{etcmosdorf}{Siegmar Mosdorf}
A la suite de son discours d'ouverture, le Secr\'{e}taire d'Etat \`{a} l'Economie, Siegmar Mosdorf (SPD) s'est exprim\'{e} dans une interview donn\'{e}e au Handelsblatt. Il dit qu'il prend tr\`{e}s au s\'{e}rieux les appr\'{e}hensions des informaticiens devant les plans de Bruxelles. ``Nous voulons maintenir le processus d'innovation. Le mouvement Open Source doit rester possible''. Cela dit, il faut voir aussi l'autre aspect: ``La protection de l'auteur est une chose importante . Celui qui a d\'{e}velopp\'{e} quelque chose doit aussi en b\'{e}n\'{e}ficier d'une mani\`{e}re ou d'une autre. Mais il est \'{e}galement important de favoriser l'ouverture au maximum''. Ensuite, il faut attendre de savoir les r\'{e}sultats de la discussion europ\'{e}enne et des n\'{e}gociations attenantes avec les Etats-Unis. ``Ce sera tr\`{e}s certainement l'une des discussions les plus difficiles'', dit Mosdorf, soucieux.

Ces propos \'{e}nigmatiques soul\`{e}vent plusieurs questions: 
\begin{itemize}
\item
En se pronon\c{c}ant pour la ``protection de l'auteur'', Mosdorf ne confondrait-il pas, par hasard, brevet et droit d'auteur ? Ignorerait-il ce malentendu qui essaie de faire croire que le ``mouvement OpenSource'' veut obliger tous les programmeurs \`{a} renoncer \`{a} leurs droits ? Une formulation plus ad\'{e}quate aurait pu \^{e}tre la suivante :
\begin{quote}
{\it La protection de l'auteur - le droit d'auteur - est importante. Celui qui a d\'{e}velopp\'{e} quelque chose doit pouvoir en tirer un b\'{e}n\'{e}fice. Il est \'{e}galement important de favoriser l'ouverture autant que faire se peut. Les brevets sur les logiciels font na\^{\i}tre un champ de mines qui accro\^{\i}t les risques li\'{e}s aux investissements pour les entreprises de logiciels et menace tout particuli\`{e}rement les auteurs de programmes.  }
\end{quote}

\item
De quelle ``discussion europ\'{e}enne'' et de quelles ``n\'{e}gociations avec les Etats-Unis'' s'agit-il? D'apr\`{e}s ce que nous savons une discussion des ministres concern\'{e}s est en cours, \`{a} laquelle participe le Ministre allemand de la Justice. Cette discussion se tient pour l'instant \`{a} huis clos et d'apr\`{e}s nos informations, les participants semblent d\'{e}cid\'{e}s \`{a} faire leurs les projets de l'UE. Quant \`{a} une discussion en cours avec les Etats-Unis, la FFII n'en a pas encore entendu parler. 
\end{itemize}

Dans son interview au Handelsblatt, Mosdorf annonce \'{e}galement la cr\'{e}ation d'un ``centre de comp\'{e}tences pour favoriser le logiciel libre'' qui devrait servir de forum de discussion et d'\'{e}changes commerciaux. ``Ce centre devrait en plus remplir un r\^{o}le de formation et de qualification, entretenir des relations avec les conseillers d'entreprises, les chambres de commerce et d'industrie. Le but \'{e}tant de renforcer et d'\'{e}tendre la communaut\'{e} OpenSource''.  

Dans le m\^{e}me temps, Mosdorf s'oppose \`{a} une ``r\`{e}glementation par voie l\'{e}gale sur le mod\`{e}le fran\c{c}ais''. L'initiative l\'{e}gale pour des standards ouverts dans le domaine des logiciels\footnote{http://www.osslaw.org/articles.html} qui a lieu en France, a pour but d'obliger les organes de l'Etat, ainsi que toutes les fonctions publiques, \`{a} mettre en place des standards ouverts pour communiquer avec les citoyens, et \'{e}galement \`{a} d\'{e}poser les codes sources des programmes qu'ils utilisent. Mosdorf motive son opposition \`{a} cette directive fran\c{c}aise par la remarque suivante: ``Pour nous le r\^{o}le de l'Etat est d'abord celui d'un mod\'{e}rateur neutre''.

Pour la FFII ces positions sont contradictoires. Pour qu'il soit possible d'assumer le r\^{o}le d'un mod\'{e}rateur neutre, il faut d\'{e}j\`{a} que l'Etat s'impose des obligations d'ouverture des standards de communication. Encore une fois, le d\'{e}p\^{o}t des codes sources n'est pas comparable \`{a} l'ouverture des codes sources: il s'agit l\`{a} d'une exigence minimale, seule capable de garantir la s\'{e}curit\'{e} des administrations concern\'{e}es. 

Un centre de comp\'{e}tences constituerait certes un beau cadeau de la part de l'Etat,  mais la FFII souhaite avant tout \'{e}tablir avec le gouvernement des r\`{e}gles claires et \'{e}quitables (comme cela se fait, ou est sur le point de se faire, en France). Ensuite, nous souhaitons la participation active et r\'{e}guli\`{e}re du gouvernement \`{a} un dialogue bas\'{e} sur l'Internet, pour la promotion d'une infrastructure libre de l'information. Jusqu'ici, le gouvernement n'a agi que sur des cas ponctuels ayant valeur d'exemple, comme lors de son soutien au d\'{e}veloppement de GNU Privacy Guard. Il faudrait \'{e}mettre de genre de commandes plus souvent et d'une mani\`{e}re plus syst\'{e}matique. Les universit\'{e}s devraient y prendre part, les entreprises, et en dernier lieu la collectivit\'{e} toute enti\`{e}re. Un centre de comp\'{e}tences limit\'{e} dans l'espace serait alors possible, \`{a} titre de troisi\`{e}me \'{e}tape. Mais justement, si l'Etat veut assumer le r\^{o}le de ``mod\'{e}rateur neutre'', l'Internet offre pour cela la meilleure des infrastructures.     
\end{sect}

\begin{sect}{etcrms}{Richard M. Stallman}
Au cours de plusieurs interventions et tables rondes, RMS n'a laiss\'{e} passer aucune occasion d'appeler son public \`{a} l'action contre les brevets sur les logiciels. Pour Stallman, tout utilisateur de GNU/Linux et des logiciels libres a le devoir moral de signer la p\'{e}tition Eurolinux. Toute entreprise qui travaille dans la mouvance GNU/Linux a le devoir de faire prendre conscience \`{a} ses clients du danger des brevets. Par exemple, tout acheteur d'une distribution SuSE Linux ou Redhat Linux devrait trouver dans la bo\^{\i}te un auto-collant appelant \`{a} signer la p\'{e}tition. Stallman est venu en personne sur le stand de la FFII et y a c\'{e}l\'{e}br\'{e} sa c\'{e}r\'{e}monie de signature, entour\'{e} par la foule. 
\end{sect}

\begin{sect}{etccox}{Alan Cox}
Alan Cox, un d\'{e}veloppeur du noyau Linux qui travaille pour Redhat, avait fix\'{e} sur son chapeau rouge notre auto-collant pour la p\'{e}tition Eurolinux. Cox conna\^{\i}t bien la probl\'{e}matique, tant dans ses aspects techniques que dans ses aspects l\'{e}gaux. D'apr\`{e}s ce qu'il nous a dit sur le stand de la FFII lors de diff\'{e}rentes conversations il va se prononcer en faveur de nos positions au cours des prochaines semaines, chez Redhat et aupr\`{e}s de quelques hommes politiques britanniques.   
\end{sect}

\begin{sect}{etcwclu}{Swantje Weber-Cludius}
Madame Weber-Cludius est la responsable de la question du droit des brevets aupr\`{e}s du Minist\`{e}re allemand de l'Economie. Elle a pris une part active \`{a} nos conf\'{e}rences et \`{a} nos discussions des Journ\'{e}es de vendredi et de samedi. De plus, elle a saisi l'occasion de mener de longues discussions avec Wolfgang Tauchert, PA Springorum, Peter Gerwinski, Hartmut Pilch, Jean-Paul Smets, Holger Blasum et autres protagonistes des d\'{e}bats du SWPAT.   
\end{sect}

\begin{sect}{etctauch}{Wolfgang Tauchert}
En priv\'{e}, l'expert informatique du Bureau allemand des Brevets a expliqu\'{e} encore un peu ses positions. Il s'est surtout \'{e}tonn\'{e} que, durant toutes ces ann\'{e}es o\`{u} la jurisprudence a \'{e}volu\'{e} vers une extension de la brevetabilit\'{e}, personne n'ait protest\'{e} d'une mani\`{e}re audible. Pour lui, si les groupes concern\'{e}s \'{e}taient d'un autre avis ils auraient d\^{u}, au lieu de s'\'{e}lever contre d'\'{e}ventuels abus de droit, utiliser le coeur du syt\`{e}me, intervenir dans le d\'{e}bat juridique pour faire revenir la jurisprudence de la CJF \`{a} ses anciennes positions. Une action de ce genre resterait d'ailleurs possible si l'exclusion concernant les programmes d'ordinateurs venait \`{a}\^{e}tre supprim\'{e}e de la loi europ\'{e}enne sur les brevets.     
\end{sect}

\begin{sect}{etcspring}{PA Dipl.Inf. Dr. Harald Springorum}
Pour Monsieur Springorum, il n'est pas impossible d'envisager soit une loi, soit un verdict de principe qui d\'{e}finirait le logiciel GPL en g\'{e}n\'{e}ral comme non commercial. Cependant, il rel\`{e}ve qu'avec des entreprises comme SuSE une cr\'{e}ation de valeur a lieu sans juste contrepartie pour les d\'{e}veloppeurs libres sur les \'{e}paules desquels l'entreprise repose. Le droit des brevets fournirait dans ce cas plus de justice \`{a} travers la loi sur les droits de l'inventeur face \`{a} son employeur. 

D'une mani\`{e}re g\'{e}n\'{e}rale, M. Springorum aime employer une argumentation moralisante pour justifier l'expansionnisme des brevets, ce qui ne l'emp\^{e}che pas de pr\'{e}senter les opposants aux brevets comme des moralistes ou des id\'{e}ologues. 

Voici les raisons les plus \'{e}videntes de s'opposer \`{a} ce discours: 
\begin{itemize}
\item
L'absence de reconnaissance est une constante dans le monde. Les auteurs des travaux math\'{e}matiques qui ont pr\'{e}par\'{e} pendant des dizaines d'ann\'{e}es le d\'{e}veloppement du proc\'{e}d\'{e} brevet\'{e} MP3 se sont notamment retrouv\'{e}s sans rien - et en plus, ils n'ont m\^{e}me plus le droit d'employer gratuitement la technologie MP3. Pour la grande majorit\'{e} des brevets qui concernent des proc\'{e}d\'{e}s triviaux c'est encore pire: leurs innombrables inventeurs parall\`{e}les se retrouvent d\'{e}poss\'{e}d\'{e}s. D'une mani\`{e}re g\'{e}n\'{e}rale on ne peut pas dire que le ``capitalisme informatique'' favorise particuli\`{e}rement ceux qui contribuent au progr\`{e}s de la civilisation. Cela se voit bien \`{a} la qualit\'{e} de la t\'{e}l\'{e}vision priv\'{e}e ou de l'Internet commercial. Vouloir par les brevets rendre justice au capitalisme informatique, c'est peut-\^{e}tre justement l'une de ces dangeureuses utopies qui promettent le ciel et r\'{e}alisent l'enfer: une id\'{e}ologie totalitaire sous une forme propre. Ce n'est pas pour rien que le professeur Lessig, sp\'{e}cialiste du droit constitutionnel \`{a} Harvard, parle\footnote{http://futurezone.orf.at/futurezone.orf?read=detail\&id=30188\&tmp=4994} de ``stalinisme des logiciels''.

\item
Des entreprises comme SuSE et Redhat se sont d'elles m\^{e}mes efforc\'{e}es de contribuer \`{a} la communaut\'{e} Linux. Elles r\'{e}mun\`{e}rent notamment une quantit\'{e} de d\'{e}veloppeurs pour qu'ils \'{e}crivent des logiciels libres.  

\item
Le commerce des distributions n'est pas un \'{e}l\'{e}ment indispensable de la cha\^{\i}ne de cr\'{e}ation de valeur dans le monde du logiciel libre, si tant est qu'un telle cha\^{\i}ne existe vraiment. Le commerce des distributions pourrait m\^{e}me dispara\^{\i}tre bient\^{o}t compl\`{e}tement au profit d'un syst\`{e}me de distribution non commercial comme le syst\`{e}me ``apt-get`` de Debian. Cela dit, la distribution SuSE remplit un r\^{o}le \'{e}conomique important en facilitant l'acc\`{e}s au march\'{e} mondial \`{a} un grand nombre d'ordinateurs domestiques. Le droit des brevets ne peut pas s'appliquer dans un tel cadre: il faudrait collecter des droits pour les millions de copies de logiciels que personne n'utilise. En plus de cela, le droit de brevet d\'{e}savantagerait les formes avanc\'{e}es de distribution comme le shareware et obligerait \`{a} un retour au temps de la vente de brillants emballages sur un comptoir. Ce ne serait m\^{e}me pas dans l'esprit du droit de brevet car il ne s'agit pas l\`{a} d'un droit de copie (droit d'auteur) ni d'un droit d'usage. Le paiement de la licence ne devrait \^{e}tre r\'{e}clam\'{e} que lors de l'utilisation effective de la technique brevet\'{e}e et ce indiff\'{e}remment, que le programme ait \'{e}t\'{e} t\'{e}l\'{e}charg\'{e} ou trouv\'{e} sur un CD SuSE.          

\item
Nos arguments principaux ne rel\`{e}vent pas du domaine moral mais bien du domaine \'{e}conomique. Les brevets sur les logiciels favorisent-ils l'innovation et la concurrence ? Conduisent-ils du moins \`{a} une croissance des d\'{e}penses de R\&D des entreprises des entreprises de logiciels ? Notre exp\'{e}rience nous laisse \`{a} penser que dans les deux cas la r\'{e}ponse est non. Et des \'{e}tudes scientifiques le confirment.  
\end{itemize}
\end{sect}

\begin{sect}{etcsiemens}{D\'{e}bats sur le stand Siemens}
Le chef du service des brevets chez Siemens, Arno K\"{o}rber, s'est r\'{e}guli\`{e}rement et vigoureusement exprim\'{e} publiquement en faveur de l'extension des brevets. La derni\`{e}re fois, dans le cadre du ZVEI. Plusieurs repr\'{e}sentants de Siemens IT Services \'{e}taient cependant d'avis que ces positions ne servaient pas vraiment les int\'{e}r\^{e}ts de l'entreprise. D'apr\`{e}s eux, l'extension des brevets aurait pour Siemens plus d'inconv\'{e}nients que d'avantages. 

Comme Siemens est encore actuellement ``mari\'{e}'' avec Microsoft, il est peu vraisemblable que leur logo puisse appara\^{\i}tre sur la p\'{e}tition Eurolinux. En revanche, il serait sans doute possible d'obtenir de leur direction un texte qui appellerait \`{a} la mod\'{e}ration de l'extension des brevets et recommanderait qu'une \'{e}tude \'{e}conomique soit r\'{e}alis\'{e}e.  

Chez Siemens, les informaticiens et les chercheurs ont tr\`{e}s r\'{e}cemment r\'{e}agi avec irritation aux instructions, venant du service des brevets, de signaler autant que possible tout ce qui est un tant soit peu innovant, dans le but d'une valorisation par un brevet. D'apr\`{e}s cette instruction, il \'{e}tait clair que le brevet ne serait plus consid\'{e}r\'{e} comme un ``moyen de protection de nos inventions'', mais comme une ``valeur d'\'{e}change''. Pour les collaborateurs, il est clair qu'il y a dans cette histoire de brevets quelques chose qui n'est pas tr\`{e}s clair et que Siemens ne peut pas se prononcer ouvertement pour un tel syst\`{e}me. Si elles \'{e}taient av\'{e}r\'{e}es, ces positions seraient contraires \`{a} la politique du groupe. Il serait dans l'int\'{e}r\^{e}t de la direction du groupe de clarifier tout cela. 

D'apr\`{e}s ce que disent tout haut ses collaborateurs, Siemens emploie parfois les brevets sur les logiciels pour emp\^{e}cher des entreprises partenaires de devenir ind\'{e}pendantes en d\'{e}posant sous leurs propres marques des techniques d\'{e}velopp\'{e}es par elle. Cela dit, m\^{e}me ce but-l\`{a} ne n\'{e}cessite en rien les brevets sur les logiciels. La plupart du temps, le simple fait de l'avance technologique des collaborateurs de Siemens en termes d'exp\'{e}rience acquise lors des nombreuses ann\'{e}es pass\'{e}es \`{a} d\'{e}velopper ladite technique et des nombreux petits progr\`{e}s qui ont ponctu\'{e} ce d\'{e}veloppement, suffirait. Et si ce n'\'{e}tait pas le cas il resterait encore le droit d'auteur et le secret commercial.
\end{sect}

\begin{sect}{etcfirmen}{Discussions sur d'autres stands }
En plus de tout cela, nous avons discut\'{e} avec la majorit\'{e} des entreprises exposantes, et elles ont \'{e}t\'{e} nombreuses \`{a} d\'{e}cider de prendre part \`{a} notre p\'{e}tition dans les prochains jours. 
\end{sect}

\begin{sect}{etcradio}{Interview radiophonique}
Hartmut Pilch a \'{e}t\'{e} interview\'{e} par la radio pendant 20 minutes. Cette interview sera diffus\'{e}e. Stefan Meretz et Stefan Merten ont \'{e}galement \'{e}t\'{e} interview\'{e}s. 
\end{sect}
\end{sect}

\begin{sect}{dank}{Remerciements}
C'est gr\^{a}ce \`{a} Arnim Rupp et \`{a} son fr\`{e}re que notre stand a \'{e}t\'{e} aussi beau depuis le d\'{e}but : ils ont fait pr\'{e}parer de magnifiques panneaux et les ont apport\'{e}s la veille de l'exposition.  

Gr\^{a}ce aux efforts de Holger Blasum, tous sur le stand nous portions de beaux Tee-shirts imprim\'{e}s aux couleurs de la FFII. Nous le remercions \'{e}galement pour l'ordinateur rapide, muni d'un \'{e}cran de grande taille, sur lequel nous avons fait nos pr\'{e}sentations.   

Felix Nenz s'est occup\'{e} de quelques panneaux de d\'{e}monstration suppl\'{e}mentaires et des boissons. Pendant trois jours il a r\'{e}pondu patiemment aux questions de nos visiteurs ce qui l'a oblig\'{e} \`{a} renoncer \`{a} assister \`{a} bien des \'{e}v\'{e}nements parmi les plus int\'{e}ressants.

Holger, Felix, Bernhard Reiter, Frank Kormann et Hartmut Pilch sont rest\'{e}s chacun trois ou quatre jours sur le stand de la FFII. Ils n'ont demand\'{e} aucun d\'{e}fraiement pour le manque \`{a} gagner ni pour les frais d'h\^{o}tel.

L'association des Journ\'{e}es Linux a effectu\'{e} un \'{e}norme travail et nous a soutenus d'une mani\`{e}re non bureaucratique.

Jean-Paul Smets, malgr\'{e} des obligations urgentes et le manque de sommeil, a pris une journ\'{e}e. Pour l'occasion il a re\c{c}u un soutien financier de la soci\'{e}t\'{e} Mandrake. 

Divers frais ont pu \^{e}tre pay\'{e}s gr\^{a}ce au fait que Infomatec AG\footnote{http://www.infomatec.de}, SuSE Linux AG\footnote{http://www.suse.de}, Harald Welte\footnote{http://www.sunbeam.franken.de}, Intradat AG\footnote{http://www.intradat.de}\ etc ont soutenu la FFII par leurs dons g\'{e}n\'{e}reux.   

Des soci\'{e}t\'{e}s telles que SuSE Linux AG\footnote{http://www.suse.de}, Intevation GmbH\footnote{http://www.intevation.de}\ etc ont bien voulu fermer les yeux sur le fait que certains de leurs collaborateurs ont quelque peu n\'{e}glig\'{e} leur activit\'{e} professionnelle pendant ces Journ\'{e}es. 
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

