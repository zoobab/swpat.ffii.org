<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wem gehört das Wissen?

#descr: Das Digitale Dilemma und Antwortversuche von OpenSource bis zu
Urheberrechtsabgaben und Softwarepatenten.  Eine Tagung am 20.-21.
Oktober in Berlin in der Galerie der Heinrich Böll Stiftung

#Dlf: Diese Tagung war überaus anregende.  Inzwischen gibt es dazu einen
%(hb:offiziellen Dokumentationsband), bei dem die mündlichen
Redebeiträge aufgrund von Tonbandaufzeichnungen protokolliert wurden.

#Vti: Veranstalter

#IWe: Im Mittelpunkt der Tagung stehen die Fragen:

#Pga: Pogramm

#Hzr: %{HBS} in Kooperation mit Netzwerk Neue Medien und %{FFII} (FFII e.V.)

#Wra: Wie wollen wir die Schöpfer von digitalen Werken und Innovationen
künftig belohnt sehen?

#Won: Wohin führt die Verschärfung des Urheberrechts, die Ausweitung des
Patentrechts, die Schaffung neuer Schutzrechte?

#Wad: Was bringt die kollektive Belohnung über Pflichtabgaben
(Urheberrechtsabgabe, Rundfunkgebühren, Steuergelder etc) oder
Mikro-Spenden (digitales Straßenmusikmodell)?

#Bhc: Erübrigt der Erfolg der Freien Software die Sorge um
Belohnungssysteme?

#Dor: Bem:  Dies ist ein möglicherweise veralteter Entwurf.  Das
%(pg:definitive Programm) ist im %(vk:Veranstaltungskalender der
Heinrich-Böll-Stiftung) zu finden.

#Fa0: Freitag, 20.10.00

#Sal: Samstag 21.10.00

#Efu: Eröffnung

#1k5: 17.30-18.15 Uhr

#Wib: Wissen als Eigentum? Wie kann das Recht auf freien Zugang zu Wissen
und Information in der globalen Wissensgesellschaft gewährleistet
werden?

#1W0: 18.15 -19.00 Uhr

#Gez: Geistiges Eigentum in der Wissensgesellschaft: Herausforderungen an
ein zeitgemäßes Urheber- und Patentrecht

#Ans: Abendessen

#FwW: Fishbowldiskussion:

#Pid: Patentrecht, Urheberrecht oder maßgeschneidertes Softwarerecht:  womit
lässt sich die Software-Innovation wirklich fördern?

#D0h: Das Europäische Patentamt möchte in Kürze das Patentrecht ändern, um
Computerprogramme patentierbar zu machen.  Derweil unterstützen 50000
Bürger, 200 Softwareunternehmen und zahlreiche Politiker eine
%(ep:Petition für ein softwarepatentfreies Europa).  Die meisten von
ihnen bevorzugen die ausschließliche Anwendung des Urheberrechts auf
Software.  Darüber hinaus gibt es diverse Entwürfe für
softwarespezifische Alternativen zum Patentschutz.

#WmW: Wer will eigentlich wann auf welchem Wege was entscheiden?  Wie können
wir auf diese Entscheidungen einwirken?

#W3s: Warum sind Computerprogramme laut PatG/EPÜ nicht patentierbar?  Der
Technizitätsbegriff und seine Anwendung in alten und neuen Grenzfällen

#Wgk: Was haben wir davon, wenn wir informationelle Innovationen mit
Patenten belohnen? Kurzvorstellung alter und neuer
volkswirtschaftlicher Theorien über die Wirkungen des Patentwesens

#Boz: Bedarf das Urheberrecht der Ergänzung durch ein gesondertes
Schutzrecht für Software-Innovationen? Kurzeinführung in die Debatte
über Sui-Generis-Systeme

#Maa: Moderation: Stefan Krempl (Journalist, Telepolis)

#C3n: Erübrigt der Erfolg des Opensource-Geschäftsmodells die Sorge um das
richtige Belohnungssystem?

#Woi: Was leistet heute Freie Software für die Volkswirtschaft?  Welche
Rolle spielen dabei diverse %(q:Opensource-Geschäftsmodelle)?

#Wsr: Würden in einer Welt ohne künstliche Belohnungssysteme (staatlich
sanktionierte Eigentumsrechte, staatliche Forschungs- und
Kulturförderung) genügend gute Computerprogramme, Kunstwerke etc
entstehen?

#Btd: Beweist der Erfolg der Freien Software, dass es eine %(q:Tragödie der
digitalen Allmende) nicht gibt?

#Oev: Welche politischen Rahmenbedingungen braucht die digitale Allmende, um
zu gedeihen?

#Ilf: Impulsreferate

#Met: Moderation

#_101: 12.30 - 14 Uhr

#Pnl: Panel II

#IeW: Ist die von der Bundesregierung geplante Urheberrechtsabgabe auf
IT-Geräte der richtige Weg, private und öffentliche Interessen zu
wahren?

#WMr: Wohin führt der Gegenvorschlag einiger IT-Verbandsfunktionäre, einen
perfekten digitalen Kopierschutz und damit einen Markt für digitale
Waren zu schaffen?  Was ist von entsprechenden Initiativen (DMCA,
Ucita, IFPI-Vorstoß, DVD-Streit, ASF-Streit, NAIIN) zu halten?

#Wit: Sind die traditionellen Verwertungsrechte angesichts der digitalen
Möglichkeiten (Napster, Gnutella, FreeNet, Mojonation) noch haltbar? 
Bringt die Einführung von freiwilligen Mikrogebühren (z.B. Mojonation,
Street Performance Protocol) eine interessante Entlohnungsmöglichkeit
für Musiker und Autoren?

#IWi: Ist die Klassische Musik von Bach bis Brahms eine Opensource-Kultur
früherer Zeiten?  Wo sind dann heute die Kulturmäzene zu suchen?  In
der Werbebranche?  In öffentlich-rechtlichen Systemen?

#Lrm: Lässt sich die Tradition der Urheberrechtsabgaben in etwas
zeitgemäßeres umwandeln?  Etwa in ein System zur Förderung der
Informationsallmende, bei dem qualifizierte Fördergesellschaften ihre
Belohnungsgrundsätze selber bestimmen und der Bürger über die
Zuteilung seiner Pflichtabgabe an die eine oder andere Gesellschaft
frei entscheidet?

#Sln: Stellungnahmen

#GtW: Grietje Bettin, MdB +   NN   +   NN

#Met2: Moderation

#Oea: Oliver Passek

#Mfd: Mittagsbüffet, Ende der Tagung

#TWN: Treffen des Netzwerks Neue Medien

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpboell2A ;
# txtlang: de ;
# multlin: t ;
# End: ;

