<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

DnS: Economic Effects of Software Patenting
DWr: The German Ministery of Economics is convening a public conference to investigate the economic impact of software patentability on May 18 in Berlin.
Dte: The %{FFII} and the %(sj:sponsors) of its %(sa:Software Patent Workgroup) are invited. We will play an active role in this %(bw:conference).
Der: The FFII is preparing to %(fp:argue the case against the expansion of the European patent system into more and more upstream informational realms).  This conference is a rare chance.  It must become a success for all those who have nothing to gain from informational monopolism.  You can contribute in several ways.
Wek: What you can do
HuK: Background of the Conference
Urc: signed by
Krr: Conference Programme
Uee: Our Special Guest
Ane: [ in Germany only: ] Attend the conference
WTe: [ in Germany only: ] Get others to participate
Dnn: [ everywhere: ] Help put together a Documentation CD
Dnh: The conference is public, but participants should register and submit a short statement to the host.  Contact us.  We have also reserved some hotel beds in Berlin.
Ddt: This is a first and maybe last opportunity for programmers and executives to inform themselves and voice their opinions about some thorny issues which may decide their fate tomorrow.  Please speak to people within your realm of influence.  Bring them into contact with us, so that we may help in drafting statements and cooperate in preparing the conference.
Dei: The FFII is putting together a %(sd:documentation) (printed and on CD) for the conference.  We need to ask many publishers for copying permissions and some important texts must be gathered from non-digital media.  This work is done via a %(ml:publically accessible mailing list) with daily progress reports by the project leader, %{IN}.  There is a lot to do for everybody, and even occasional small advice can help.
DKh: The FFII is in the lucky position that it can offer some compensation on a per hour basis for work being done, thanks to its corporate Sponsors.
DWc: The European Patent Organisations and the EU Commission are planning to remove all obstacles to software patentability from European patent law.
EWn: This is the first time that a governmental body in Europe is taking a closer look at the socio-economic effects of this proposed change.
Dgi: The German governement is a member state of the European Patent Convention therefore entitled to deny its approval to any change of this convention.  If it denies its approval, %(q:computer programs) will remain on the list of exceptions to patentability.  If not, the way to full enforcement of software patent claims will be paved later this year.
DkW: The FFII fears that software patentability will favor proprietary standards,  reinforce monopolistic tendencies, slow down the rate of software innovation and damage small to medium size software companies.
Icr: In a few months an avalanche of patent ligitation and extorsion letters may roll over us, and you may no longer dare to publish your source code without first consulting with a laywer.  And it will then be too late to do anything about it.
Hcr: So let's act now.  Let's make the Berlin Conference a great success.
pH8: on behalf of the FFII
Tus: Conference Site
Lgp: Position Map
BmW: Federal Ministery for Economics and Technology
HuW: Building D
1ec: Floor 1
Hra: Auditorium
Krp: Conference language
Dus: German
Sll: Simultaneous interpreting into English is provided.
Zip: Schedule
urd: non-committal
Wan: When?
Was: What?
Efu: Opening
Wel: Economic Siginificance of Free Software
Ste: Status of Current Developments in Patent Law
Wee: Economic Interests affected by the Patenting of Computer Programs, with Special Regard to Free Software
Van: %{GA} Presentation
Shf: Lunch Buffet
Sls: Concluding Speech
OWi: The above statements are intended to provide an easy overview.  In case of doubt, refer to the %(si:official homepage of the event).
TmW: Participants and Speakers
Afa: Interested Parties, especially from German software companies, have been specially invited.
Jcw: Each active participant should submit a position paper so that there is a basis on which to discuss.  Most time will be allocated to free discussion.
Ade: As a special guest invited by the FFII, Gregory Aharonian will be available to answer questions.
Ben: Statements made so far
Teh: Overview Page of the host (in German)
Esu: This conference is intended to assemble interested parties in Germany and let them form an opinion independent of the omnipresent international lobbying.  Therefore we refrained from inviting famous foreign guests.  With one exception.
cgd: %{GA} has thouroughly investigated the swelling flood of software patents since 1994 and has sought to document and utilise the (mal)functioning of the software patent system as it stands. His analyses make share values crash, his hints make stock option buyers rich.  On this ability Aharonian has built his own powerful %(bp:company), which makes unique insights into the econonomy of software patents possible and is probably one of the most innovative and exciting institutions of this New Economy.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpbmwi25.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpbcpe25 ;
# txtlang: en ;
# End: ;

