<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#KWn: KMU-Konsens gegen Softwarepatente beim BMWi

#Inr: Im BMWi in Berlin begann am 18. Mai 2000 ein Dialog zwischen
Vertretern des Patentwesens und IT-Unternehmen, die sich von dessen
ungezügelter Expansion bedroht fühlen.  Presseerklärung des aktiv
beteiligten FFII.

#Pev: Presseerklärung

#EWi: Etwa 40 Vertreter von deutschen Softwarefirmen, Regierungsstellen,
Patentämtern, Hochschulen und Verbänden trafen sich heute im
Bundesministerium für Wirtschaft und Technologie in Berlin, um über
die Wirkung von Softwarepatenten auf Wirtschaft und Gesellschaft zu
diskutieren.

#VWn: Vertreter erfolgreicher deutscher Softwarefirmen wie %{LST} und einer
Reihe kleinerer IT-Firmen übten scharfe Kritik an der Ausweitung des
Patentsystems auf informationelle Güter.

#Reo: Patentrechtler von Siemens, IBM, Patentämtern und Anwaltskanzleien
führten rechtliche und moralische Gründe an, um die Ausdehnung des
Patentwesens auf die Sphäre der Information zu rechtfertigen.  Der
FFII stellte jedoch eine große Sammlung juristischer Dokumente vor, um
zu belegen, dass die Europäischen Patentämter es nur durch
Rechtsbeugung geschafft haben, gegen die Bestimmungen des Gesetzes
Patente auf reine Information zu gewähren.  Der FFII wies ferner nach,
dass, entgegen üblichen Propagandabehauptungen der Patentrechtler, der
TRIPS-Vertrag Europa nicht im geringsten dazu verpflichtet,
Softwarepatente zu gewähren.  Die Konferenzteilnehmer führten dazu
Beispiele von gesetzeswidrig gewährten europäischen Softwarepatenten
an und kritisierten deren Trivialität und verheerende ökonomische
Wirkung.

#Aem: Auf Einladung des FFII gab Gregory Aharonian, der Gründer und Leiter
eines in Fachkreisen viel beachteten
Internet-Patentnachrichtendienstes, einen Überblick über das
amerikanische Patentsystem und dessen Unfähigkeit, vernünftige
Prüfprozeduren anzubieten und echte Erfindungen auszuwählen.

#AWn: Aharonian schöpfte aus einem reichen Fundus an Beispielen und eigenen
Statistiken, um zu zeigen, dass Software eines der Gebiete ist, die
zur Innovation keines besonderen Anreizsystems bedürfen und in denen
Patente eher hemmend als fördernd wirken.

#Dct: Die vom FFII erstellte Sammlung patentrechtlicher Dokumente wurde auf
der Konferenz auf %(cd:CDROM) zusammen mit einem %(jv:Positionspapier)
verteilt, das dem Europäischen Patentamt und nationalen Patentämtern
Rechtsbeugung vorwirft und dafür ausführliche Belege liefert.

#DeW: Die Vertreter der Kleineren und Mittleren Unternehmen argumentierten,
dass das Urheberrecht besser geeignet und im allgemeinen völlig
ausreichend sei, um Neuerungen zu belohnen und die Interessen
kleinerer und mittlerer Softwarehäuser zu schützen.

#Kna: Kontakt

#nrF: Mehr über den FFII

#cas: Der %(fi:FFII) ist ein gemeinnütziger Verein, der die Entwicklung
offener Schnittstellen, quellenoffener Software und frei verfügbarer
öffentlicher Informationen fördert.  Der FFII koordiniert eine
%(sp:Softwarepatent-Arbeitsgruppe), die von %(sj:erfolgreichen
deutschen Softwarefirmen gefördert) wird.  Der FFII ist Mitglied des
%(el:EuroLinux-Bündnisses).

#Vre: Liens

#Pkd: Déclaration de Presse du Ministère d'Econ. & Tech.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpbmwi25 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

