\contentsline {section}{\numberline {1}Previous Papers}{1}{section.1}
\contentsline {section}{\numberline {2}Outline of the Berlin Presentation}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Whom do swpatents help? Which economic behaviors do they encourage?}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Whom do SWPatents hurt? What kind of behaviors do they discourage?}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}New Entrants}{2}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Stock Market Investors}{3}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Extorsion Victims}{3}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Owners of understimated patents}{3}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Companies that experience delays}{3}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Startups in patent-mined areas}{4}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Are the Patent Offices properly equipped to do their job?}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Are the Europeans missing Chances?}{5}{subsection.2.4}
