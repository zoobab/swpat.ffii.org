\begin{subdocument}{swpahar25}{Vortrag von Gregory Aharonian}{http://swpat.ffii.org/treffen/2000/bmwi0518/aharonian/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{}
\begin{sect}{purci}{Fr\"{u}here Artikel}
Aharonian's Statistiken werden gerne von Softwarepatent-Apologeten zitiert.  Peter Hanna, Software-Referent der UNION\footnote{http://swpat.ffii.org/papiere/union97/index.en.html}, begr\"{u}ndete damit auf dem M\"{u}nchenr UNIONs-Konferenz 1997 seine zentrale Botschaft ``The Debate is Over -- Industry Demands Software Patents'' und empfahls sie u.a. mit den Worten:
\begin{quote}
The present International Patent Classification System does not allow for any easy analysis of the most common subject areas for Software Patents, or to find some of the new emerging software technologies.  The best analysis of this that I have seen has been done by Gregory Aharonian.
\end{quote}

\begin{itemize}
\item
1998 Software Patent Statistics:  Who is getting the Patents?\footnote{stat-1998.txt}

\item
Inadequacy of Prior Art Searches in US, EU and Japan\footnote{search-1995.txt}

\item
trivial/obnoxious business method patents\footnote{gest-2000.txt}

\item
The Patent Examinations System is Intellectually Corrupt\footnote{http://www.bustpatents.com/corrupt.htm}
\end{itemize}
\end{sect}

\begin{sect}{velgaz}{Ger\"{u}st des Berliner Vortrags}
\begin{sect}{util}{Wem n\"{u}tzen die SWPatente?  Was f\"{o}rdern sie?}
Software patents basically help two different types of companies.  It helps small companies that actually have invented something new in the world of software (a hard thing to do given the tens of millions of programmers around the world) and need protection from large competitors as they enter the market with their product.  One of the few examples of this was when Stac won \$90 million from Microsoft after Microsoft infringed Stac's disk file compression patent.  But large settlements like this are rare (and some argue that Microsoft should have won the lawsuit).

The other group of entities to benefit are very large companies like IBM, which with their portfolios of hundreds and thousands of software patents, can obtain hundreds of millions of dollars, if not billions, from forcing smaller and newer companies to license part or all of their portfolios.  This is a intergenerational transfer of wealth.
\end{sect}

\begin{sect}{neutil}{Wem schaden die SWPatente?  Was hemmen sie?}
\begin{sect}{newent}{Neuland betretende Firmen}
First, the smaller and newer (or new entrants such as European companies) companies having to pay royalties to older companies with well established patenting systems and large portfolios of issued patents.  These royalties are monies not then available to compete in the marketplace, sometimes against these larger and older companies.
\end{sect}

\begin{sect}{investors}{Aktion\"{a}re}
A second set of losers is stock market investors.  Increasingly companies getting software patents are touting them in the press and on the Internet, even if the patent is of questionable validity.  This leads a naive investing public to bid up the price of the company's stock.  The stock price eventually drops as the market determines that the company's patent is probably of little value as a strategic weapon.  Those investing too late thus lose money as the stock price readjusts to a more realistic valuation.
\end{sect}

\begin{sect}{extort}{Opfer von Schutzgelderpressungen}
A third sets of losers is large numbers of small and large companies being forced to pay monies to avoid patent lawsuits.  For example, if someone with a patent of questionable validity offers you a universal license for \$30,000, it is hard for a company not to pay the \$30,000 because to do anything else (starting out with obtaining a invalidity opinion from their lawyers) will cost as much if not more.  The \$30,000 becomes a cheap ``insurance policy'' (compared to the alternatives), even though the patent may be completely invalid.  Some argue that companies asserting such patents are engaging in little more than extortion.

With the US Patent Office issuing over 20,000 software patents a year, companies could easily soon be in a position to have to pay these fees a few times each year, say \$100,000 worth of licensing fees, money better spent on R\&D and marketing.  These possibilities have lead to a growing market for patent infringement insurance.
\end{sect}

\begin{sect}{inflat}{Inhaber untersch\"{a}tzter Patente}
A four set of losers is those small companies with new software patents that are actually valid, but presumed to be invalid like so many other software patents actually are invalid.  These small companies thus have to spend more time and money trying to have their patent(s) considered more valid than public perceptions assume them to be.  Large numbers of low quality patents diminish the importance and value of the small numbers of high quality patents.
\end{sect}

\begin{sect}{delay}{Flinke Unternehmer, deren Arbeit verz\"{o}gert wird}
A fifth set of losers are companies that experience delays in entering a software market while they assess the quality of patents that affect the market.  For example, Priceline.com has a patent on its reverse auction technique, a patent most likely invalid in light of non-patent prior art not discovered and consider by the US PTO.  Many believe that potential competitors of Priceline delayed in entering the online auction market while they assessed (or waited for others to assess) the validity of the Priceline patent.  The invalid patent is a barrier to entry.  This is related to the problem European companies will face as they enter the US software marketplace, when holders of American software patents use their software patents to delay or prevent the entry of the European company.
\end{sect}

\begin{sect}{startup}{Neugr\"{u}ndungen in patentvermintem Gel\"{a}nde}
A six set of losers are startups, which sometimes find it harder to raise venture capital if there are many patents, good and bad, in their field of software technology.  Venture capitalists see all of these patents as potential mines in the future, so directed their investment dollars towards startups with lesser degrees of being affected by issued patents.
\end{sect}

In short, large numbers of bad software patents distort marketplaces and lead to transfers of profits not necessarily in the best interests of encouraging and rewarding innovation.  These problems due to American software patents are surely to cross the Atlantic if Europe liberalizes its software patenting policies.
\end{sect}

\begin{sect}{srch}{Sind die Patent\"{a}mter ihren Aufgaben gewachsen?}
With regards to the three major Patent Offices, PTO, EPO and JPO, the answer to this question is ``NO''.  The United States Patent and Trademark Office has been known for over thirty years to have problems with software patent applications.  One of the most commonly shared complaints is that the PTO for the most part ignores non-patent prior art, such as conferences and journals, which for software patent applications is often the most relevant prior art.

For example, 80\percent{} of the issued US software patents in the last ten years, effectively cite no non-patent prior art from any of the world's engineering and computer societies such as the IEEE/IEE, ACM and others. Compounding this deficiency is the problems the PTO is having hiring and retaining people with good academic and/or work experience with software. Patent examiners with bachelors degrees in electrical engineering and a few years industry experience are not qualified to examine advanced software patent applications.

Over the last six years, the PTO has made many announcements of new initiatives to address the problems it is having examining software patents, but it is hard to detect any improvements in their practices by analyzing recently issued software patents.  Recent court decisions allowing patents on methods of doing business have increased the flood of software patent applications already overwhelming the PTO, making it likely that the overload will lead to lower quality software patents from issuing.

Assuming that bureaucracies are globally similar, is not unreasonable that as the Japanese and European Patent Offices start experiencing levels of software patent applications similar to the 50,000+ software patent applications a year the US Patent Office receives, that the JPO and EPO will demonstrate similar levels of problems handling software patents. Indeed a study I did in 1994 comparing the quality of PCT searches done by the EPO and PTO found little difference in the quality, that the EPO searches were as inadequate as those of the PTO.  Assuming that searching conditions haven't changed much in recent years at the EPO (especially in light of last falls protests by EPO examiners about being overworked), then I suspect a study of PCT search quality for recent PCT applications will find similar levels of quality.

So Europe should be careful in allowing expansion of software patenting rights at the EPO and regional offices, without first putting in place some monitoring mechanisms and support systems to make sure that EPO avoids many of the problems the PTO demonstrates (partly because there are no independent monitoring mechanisms for PTO performance quality).
\end{sect}

\begin{sect}{entp}{Verpassen die Europ\"{a}er Chancen?}
If one views patents as a form of a trade barrier, then in at least one aspect, European companies are missing an opportunity.  That is, in the United States, which has the most liberal software patenting policies, American and Japanese companies acquire over 90\percent{} of the issued software patents (Americans get about 60\percent{}, while the Japanese get around 30\percent{}). Given the comparably sized economics between the United States, Japan and Europe, one would naively assume that a three way ratio would be (2:1:1), with Americans getting 46\percent{}, Japanese getting 23\percent{} and Europeans 25\percent{}.

In reality, Europeans are getting about 5\percent{} of the US software patents, and thus at a disadvantage when conducting business in the United States, because European patent portfolios are too small to trade with American companies such as IBM with very large portfolios.  The cost of entry into the American software market is thus higher, where European companies aren't outright prevented from entering the software market due to blocking patents for which European companies have little to offer other than royalties.

Additionally, if Europe liberalizes its software patenting policies, American and Japanese companies will have an advantage at the outset due to their many decades of experiences in American and Japan acquiring and asserting software patents.  It will be easy for American and Japanese companies to take many of their patent applications already pending and adapt them for European applications (especially those patent applications pending that have filing date extensions due to PCT protection).
\end{sect}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

