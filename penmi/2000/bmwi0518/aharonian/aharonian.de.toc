\contentsline {section}{\numberline {1}Fr\"{u}here Artikel}{1}{section.1}
\contentsline {section}{\numberline {2}Ger\"{u}st des Berliner Vortrags}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Wem n\"{u}tzen die SWPatente? Was f\"{o}rdern sie?}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wem schaden die SWPatente? Was hemmen sie?}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Neuland betretende Firmen}{2}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Aktion\"{a}re}{2}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Opfer von Schutzgelderpressungen}{3}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Inhaber untersch\"{a}tzter Patente}{3}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Flinke Unternehmer, deren Arbeit verz\"{o}gert wird}{3}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Neugr\"{u}ndungen in patentvermintem Gel\"{a}nde}{3}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Sind die Patent\"{a}mter ihren Aufgaben gewachsen?}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Verpassen die Europ\"{a}er Chancen?}{5}{subsection.2.4}
