Greg Aharonian

!20000417 (Walker Asset's) trivial/obnoxious business method patents

    There are many fears of future patent litigation as everyone and their
grandmothers file business method patents, for which the PTO (despite its
current shut-up-the-public PR campaign) will all too happily allow the
majority of them to turn into issuance/maintenance fee generators.  While
such fears in moderation are very justified, there is another danger, more
psychological than financial. That is, mental nausea due to the trivial and
obnoxious obviousness of many of these patents (I have a bet going with a
friend that the Green Stamps people have a patent pending for electronic
green stamps, a perfect test case for whether or not automating the Old
Economy to exploit in the New Economy is patentable).

    Case in point is Walker Asset Management (Walker Digital/Priceline)
who with their billions of dollars and teams of PhD scientists redefining,
reengineering, revolutioning and recapitulating the ENTIRE business world
in electronic form that their president can rightly be called the Thomas
Edison of the ecommerce world as he leads his people to ecommerce solutions
for poverty, global medical care, reversing environmental destruction, all
protected by patents that are legal and intellectual writings ranking up
their with the Magna Carta ...... ahhhhhh stop me before I choke on my own
contempt.

    Here are abridged titles to some recent patents awarded to Walker Asset,
supposedly new ways of allowing people to do socially beneficial commercial
activities such as ......... gamble, phone chat and use their credit cards:

    6049778   Administering a reward program
    6024640   Offline remote lottery system
    6018718   Processing customized reward offers
    6014439   Entertaining callers in a queue
    6012983   Automated play gaming device
    6010404   Using a player input code to affect a gambling outcome
    6006205   Credit card billing method and system
    6001016   Remote gaming device
    5999596   Controlling authorization of credit card transactions
    5978467   Enabling interaction between callers with calls in a queue
    5970478   Customizing credit accounts
    5967896   Controlling a gaming device having a plurality of balances

Is this the type of business method innovation we want to incent with the
constitutionally inspired patent system?  Is this type of thinking supposed
to stand on the shoulders of hundreds, if not thousands, of years of methods
of doing business, to be rewarded by the stock market with billion dollar
capitalizations?  I am not sure.

Are they great marketers?  Yes.  Innovators?  Can't tell with this list
of patents.  And that's assuming any of this stuff is novel and unobvious.
For example, take their latest patent, awarded last week:

    6,049,778        
    Method and apparatus for administering a reward program 
    Inventors: Jay Walker, Andrew Van Luchene, James Jorasch, Dean Alderucci
        Filed: October 31, 1997                                               
    Claim 1:

    1. A computer-based method for administering a reward program based
    on a series of registrations, each registration corresponding to a
    purchaser, comprising: 

      calculating a measurement of product success; 

      determining if the measurement is within a predetermined range; 

      selecting from the series of registrations a set of registrations
      which are early-adopter registrations, the set of registrations
      thereby defining a set of early-adopter purchasers; and 

      providing a reward to each early-adopter purchaser if the
      measurement is within the predetermined range. 


In short, if sales of a product are good enough, reward early purchasers.

Brilliant.  Bravo.  Noble Prize in Economics.  Rewrite the undergraduate
marketing textbooks.  Unheard of in the world of sales and marketing, in
either manual or electronic form.  Yale University should have these guys
running their business school.

And pigs fly (which they probably do at the PTO, where even reality isn't
obvious).  One more case of a patent application that wasn't taken too
seriously by all involved.  With nothing better to do with my life, I
spent ten minutes doing a quickee search (ten minutes more than the PTO
spent searching on this patent apparently), and came up with the following
prior art item:


    Sign early, save $ on cruises.
    Advertising Age v64, n1 (Jan 4, 1993):3,33 (by Nancy Magiera)
    Abstract:
    The cruise line industry wants to change the way people book trips
    by offering discounts to consumers who sign up early.  Norwegian
    Cruise Line and Carnival Cruise Lines, 2 of the largest players,
    began offering new advance purchase discounts on January 1, 1993.
    Both Norwegian's yearlong "Dream Fares" promotion and Carnival's
    18-month "Fun Ship Super Savers" program are based on occupancy
    rates.  For example, a 7-day cruise might be discounted by up to
    $1,000 for the first 20% of tickets sold.  As more berths are filled,
    the discount is reduced.  Jennifer De La Cruz, public relations
    manager for Carnival, said that the objective was to move customers
    away from waiting until the last minute to obtain a good deal.


Now how obvious is the Walker Asset patent filed five years later (which
is little more than a post-conditional variation of the cruise line's
offering)?  We have: measurement <==> occupancy rate, early adopters <==>
first %20, providing a reward <==> upto $1000 discount, predetermined range
<==> as more berths are filled, computer-based <==> cruise line reservation
computers, purchaser <==> vacationer.  Dependent claims just as obvious:

    5. The method of claim 4, wherein the ordinal position of each
    registration indicates the order in which corresponding registration
    information was received. 

i.e., a dictionary entry for the word "ordinal" turned into a claim (what
page of Landis is that?), with more such gems in the 82 claims issued in
total, such as

    21. The method of claim 1, wherein the step of providing rewards
    comprises printing a mailing address. 

Wow - I always wanted to win a mailing address.  But how would they know
which customer I was?

    22. The method of claim 1, wherein each registration includes a
    registration identifier for uniquely identifying the registration. 

to distinguish their invention from all of those companies that use the
same identifier to identify different customers (which makes sense only
if all of your customers are the sons of George Foreman).

There seems to be one distinction trying to be made in the specification,
for example, from the background summary:

    It would be advantageous to provide a method and apparatus that
    facilitates the introduction and sale of new products. Such a method
    and apparatus would ideally overcome the drawbacks of known systems
    for promoting the sale of new products. 

Drop the word "new" from this paragraph gives you the cruise line promotion.
Adding it back in doesn't seem to innovative.

                                      ====

How much more such prior art is to be found in the marketing and sales
world if a serious search is actually done?  Where is the innovation?
(A legitimate question for a patent where the length of the text of the
specification is just a measly twice the length of the text of the claims).
One would have to prepare a sample of the universe of similar promotional
schemes from the 1980s and 1990s to seriously determine how unobvious this
patent really is.  Walker Asset didn't provide such a sample to the PTO,
and the PTO hasn't, doesn't and won't be able to do so itself. As far back
as their Priceline patent (a patent for reverse auctions that didn't use
the already known phrase/process "reverse auction" in the specification),
their searches have reflected a less than serious interest in a quality
patent examination process.

So between the PTO's inability and apathy towards business method patent
examination, and extremely rich applicants with attitudes not much better,
I am afraid that the future will see thousands of business method patents
for the non-novel, the marginally unobvious, the variations of a theme that
are cute in music but sad in the world of inventing.

And for those of you going to a certain seminar on ecommerce patents next
week in New York, one of the speakers will be a Walker Asset lawyer, a
second speaker from a company that came close to being sanctioned by the
PTO for violating Rule 56 (and should have been), and a third speaker from
a company that doesn't warn its clients that no matter how good the tools
are, poor quality patents usually lead to GIGO (garbage in garbage out)
when you analyze them.

Greg Aharonian
Internet Patent News Service
