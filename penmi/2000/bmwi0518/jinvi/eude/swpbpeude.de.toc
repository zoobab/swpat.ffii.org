\contentsline {subsection}{\numberline {0.1}Sinn des Patentwesens}{2}{subsection.0.1}
\contentsline {subsection}{\numberline {0.2}Rechtliche Situtation der Softwarepatentierung in Europa}{2}{subsection.0.2}
\contentsline {subsection}{\numberline {0.3}Patentjustiz in Widerspr\"{u}che verstrickt}{3}{subsection.0.3}
\contentsline {subsection}{\numberline {0.4}Konflikte mit anderen Rechtsg\"{u}tern}{8}{subsection.0.4}
\contentsline {subsubsection}{\numberline {0.4.1}EU-Interoperabilit\"{a}tsrichtlinie vs Eigentum an Schnittstellen}{8}{subsubsection.0.4.1}
\contentsline {subsubsection}{\numberline {0.4.2}Wettbewerbsverzerrung zugunsten schwerf\"{a}lliger Vermarktungsverfahren}{8}{subsubsection.0.4.2}
\contentsline {subsubsection}{\numberline {0.4.3}Besonderer Konzentrationseffekt bei komplexen Systemen}{9}{subsubsection.0.4.3}
\contentsline {subsubsection}{\numberline {0.4.4}F\"{o}rderung der Geheimniskr\"{a}merei vs Sicherheit und Verbraucherschutz (siehe R\"{o}mischer Vertrag \S {}\%{}s)}{10}{subsubsection.0.4.4}
\contentsline {subsubsection}{\numberline {0.4.5}Medienkartelle vs Kulturelle Vielfalt (siehe R\"{o}mischer Vertrag \S {}\%{}s)}{11}{subsubsection.0.4.5}
\contentsline {subsubsection}{\numberline {0.4.6}Kartellf\"{o}rderung vs Beschleunigung des Techniktransfers (siehe R\"{o}mischer Vertrag \S {}\%{}s)}{11}{subsubsection.0.4.6}
\contentsline {subsection}{\numberline {0.5}Was {\bf kann} die Bundesregierung tun?}{11}{subsection.0.5}
