\contentsline {subsection}{\numberline {0.1}Sinn des Patentwesens}{2}{subsection.0.1}
\contentsline {subsection}{\numberline {0.2}Current Status}{2}{subsection.0.2}
\contentsline {subsection}{\numberline {0.3}Patentjustiz in Widerspr\"{u}che verstrickt}{3}{subsection.0.3}
\contentsline {subsection}{\numberline {0.4}Legal Inconsistencies, Conflicts with other Legal Goods}{8}{subsection.0.4}
\contentsline {subsubsection}{\numberline {0.4.1}EU Interoperability Directive vs Property on Interfaces }{8}{subsubsection.0.4.1}
\contentsline {subsubsection}{\numberline {0.4.2}Distortion of Competition in favor of traditional modes of distribution}{8}{subsubsection.0.4.2}
\contentsline {subsubsection}{\numberline {0.4.3}Special Concentration Effect in case of Complex Systems}{9}{subsubsection.0.4.3}
\contentsline {subsubsection}{\numberline {0.4.4}Promotion of Secrecy vs Security \& Consumer Safety (see article \%{}s of the Treaty of Rome)}{9}{subsubsection.0.4.4}
\contentsline {subsubsection}{\numberline {0.4.5}Media Cartels vs Cultural Diversity (see article \%{}s of the Treaty of Rome)}{10}{subsubsection.0.4.5}
\contentsline {subsubsection}{\numberline {0.4.6}Promotion of Oligopolism vs Acceleration of Technology Transfer (see article \%{}s of the Treaty of Rome)}{10}{subsubsection.0.4.6}
\contentsline {subsection}{\numberline {0.5}What {\bf can} the Federal Government do?}{11}{subsection.0.5}
