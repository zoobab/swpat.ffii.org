\begin{subdocument}{swpbpeude}{Situation of Patent Law and Current Developments in DE and EU}{http://swpat.ffii.org/conferences/2000/bmwi0518/jinvi/eude/index.en.html}{Workgroup\\swpatag@ffii.org}{For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.}
\begin{sect}{patw}{Sinn des Patentwesens}
Mit der Erteilung eines Patents gew\"{a}hrt der Staat einem Erfinder ein zeitlich befristetes Monopol auf die gewerbliche Anwendung einer technischen Erfindung.

Ein Patent ist ein Tauschgesch\"{a}ft zwischen einem Erfinder und der \"{O}ffentlichkeit.  Der Erfinder erh\"{a}lt ein befristetes Verwertungsmonopol und stellt im Gegenzug der \"{O}ffentlichkeit (vertreten durch das Patentamt) alle damit zusammenh\"{a}ngenden Informationen zur Verf\"{u}gung, die sonst vielleicht dem Betriebsgeheimnis anheimgefallen w\"{a}ren.  Das beschleunigt die Verbreitung von Wissen und technischem Fortschritt.

Patente bieten eine M\"{o}glichkeit der indirekten Forschungsf\"{o}rderung.  Sie erlauben es, ohne staatliche Planung Gelder in Forschungsprojekte zu lenken, und sie bieten einen starken Anreiz f\"{u}r die Ver\"{o}ffentlichung der Forschungsergebnisse.  Hierin liegt ihre wirtschaftspolitische Bedeutung.

Patente geh\"{o}ren zu den gewerblichen Schutzrechten, d.h. der Gruppe von Rechten, die man durch ein Registrierungs- und Pr\"{u}fungsverfahren erwirbt.  Hieran wird nochmals deutlich, dass es sich nicht um ein bedingungslos zu gew\"{a}hrendes Naturrecht sondern um ein Tauschgesch\"{a}ft zwischen dem Erfinder und der \"{O}ffentlichkeit handelt.

Es ist Aufgabe der staatlichen Wirtschafts- und Technologiepolitik, dar\"{u}ber zu wachen, dass das Tauschgesch\"{a}ft zu f\"{u}r die \"{O}ffentlichkeit vorteilhaften Bedingungen stattfindet.
\end{sect}

\begin{sect}{stat}{Current Status}
\"{U}ber die Gew\"{a}hrung von Patentenrechten entscheidet das Deutsche Patentamt, das aber durch das Europ\"{a}ische Patet\"{u}bereinkommen (EP\"{U}) verpflichtet ist, Vorentscheidungen des Europ\"{a}ischen Patentamtes (EPA) umzusetzen.  Auch das Patentrecht der EP\"{U}-Vertragsstaaten ist weitgehend vom EP\"{U} vorgegeben und ist dadurch eng an Entscheidungen der \emph{Europ\"{a}ischen Patentorganisationen} (EPO) gebunden, die wiederum in vielfacher Weise mit der EU-Kommission und dem Europ\"{a}ischen Parlament zusammenarbeiten, obwohl sie nicht ein Organ der EU sondern der EP\"{U}-Vertragsstaaten sind, zu denen auch Nicht-EU-Mitglieder wie die Schweiz z\"{a}hlen.

\"{A}nderungen des EP\"{U} bed\"{u}rfen der Zustimmung aller Vertragsstaaten, und theoretisch sind es auch die Vertragsstaaten, die dar\"{u}ber wachen, ob die Patent\"{a}mter, allen voran das EPA, vertrags- und gesetzestreu im Interesse der \"{O}ffentlichkeit agieren.  Diese \"{U}berwachungsfunktion ist meist in den Justiz- und Wirtschaftsministerien der Vertragsstaaten beheimatet, wird dort aber nur sehr lasch gehandhabt.

In den letzten Jahren ist es den Patentorganisationen gelungen, das EP\"{U} in einer Weise auszulegen, die es den Patent\"{a}mtern erlaubt, viel mehr Patente zu erteilen, als ein aufmerksamer Leser des EP\"{U} (und des PatG sowie der Pr\"{u}fungsrichtlinien u.v.m.) vermuten w\"{u}rde.

In EP\"{U} \S{}52 wird festgeschrieben, f\"{u}r welche Leistungen Patentschutz gew\"{a}hrt wird ist und f\"{u}r welche nicht.  Die dort festgeschriebene ``Ausnahmenliste'' findet sich wortgetreu im Deutschen Patentgesetz PatG \S{}1 ebenso wie in anderen nationalen Patentgesetzen wieder.

Auf der europ\"{a}ischen Patentierungs-Ausnahmenliste in Art 52(2) findet sich Punkt 3 ``Programme f\"{u}r Datenverarbeitungsanlagen''.  In einer Zusatzbestimmung (3) wird ausgef\"{u}hrt, dass die Ausnahmen nur f\"{u}r den jeweils ausgenommenen Gegenstand ``als solchen'' gelten.
\end{sect}

\begin{sect}{soph}{Patentjustiz in Widerspr\"{u}che verstrickt}
Dennoch gew\"{a}hren die europ\"{a}ischen Patent\"{a}mter Patente auf reine Computerprogramme - nicht etwa nur auf computerprogramm-gest\"{u}tzte Physikalien.  Die Patentanw\"{a}lte Dr. Alexander Esslinger und J\"{u}rgen Betten irren gewiss nicht, wenn sie in der Zeitschrift ``Computerrecht'' vom Januar 2000 S.22 diesen Umstand politisch erkl\"{a}ren:

\begin{quote}
Der Ausschluss von ``Computerprogrammen als solchen'' vom Patentschutz in Art. 52 EP\"{U} (\S{}1 PatG) wird seit langem als rechtspolitische Fehlentscheidung angesehen, zumal der Ausschluss von breiten Verkehrskreisen - bis heute - missverstanden und meist als Ausschluss von Computerprogrammen allgemein verstanden wird.

... den Round Table der UNION\footnote{Proceedings of the Union Round Table Conference 1997\footnote{http://swpat.ffii.org/papers/union97/index.en.html}} am 9./10.12.1997, als im Europ\"{a}ischen Patentamt 100 Fachleute aus zwanzig europ\"{a}ischen L\"{a}ndern \"{u}ber die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem \"{a}hnlichen Eindruck kamen wie die AIPPI.  Zudem wurde darauf hingewiesen, dass das Konzept des EPA zum ``technichen Charakter'' weder von den Patentanmeldern noch von den nationalen Patent\"{a}mtern richtig verstanden w\"{u}rde.  Viele Teilnehmer machten klar, dass eigentlich alle Computerprogramme dem Wesen nach ``technischen Charakter'' aufweisen w\"{u}rden.  Seit dieser Zeit wird praktisch ``auf allen Kan\"{a}len'' daran gearbeitet, einen Weg zu finden, wie der irref\"{u}hrende Ausschluss von ``Computerprogrammen als solchen'' aus dem europ\"{a}ischen Patentgesetz entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregeleungen in Art. 52 Abs. 2 EP\"{U} (\S{}1 PatG) zur Disposition stehen.

...

Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europ\"{a}ischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EP\"{U} bez\"{u}glich des Ausschlusses von Computerprogrammen vorzulegen, so dass die ge\"{a}nderte Fassung vor dem 1.7.2000 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und auch die anderen Ausschlussregelungen) aus Art. 52 EP\"{U} gestrichen sind.

Daneben bem\"{u}ht sich die Rechtsprechung - wie ausgef\"{u}hrt -, die derzeitige Gesetzesregelung so eng auszulegen, dass praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentf\"{a}hig sind, wenn sie neu und erfinderisch sind.

Da die Pr\"{u}fung einer Patentanmeldung mindestens zwei bis drei Jahre dauert, zwingt diese Entwicklung bereits heute alle Berater dazu, ihre Mandanten darauf hinzuweisen, dass grunds\"{a}tzlich alle Computerprogramme patentf\"{a}hig sind und der Patentschutz - nicht nur f\"{u}r Computerprogramme sondern alle Innovationen im Internet genutzt werden kann.
\end{quote}

Folgende Tabelle soll aufzeigen, in welche Argumentationsn\"{o}te sich das EPA im Verlaufe der oben zitierten ``Arbeiten auf allen Kan\"{a}len'' hineinman\"{o}vriert hat.

\begin{center}
\begin{tabular}{|C{27}|C{63}|}
\hline
EPA-Behauptung & Widerlegung\\\hline
Das EP\"{U} enthalte viele Widerspr\"{u}che.\par

der Gesetzgeber habe nicht definiert, was unter ``Computerprogramm als solches'' zu verstehen sei. & Mit dem W\"{o}rtchen ``als solches'' wird das Computerprogramm von einer auf Computerprogrammen beruhenden physikalischen Struktur (z.B. elektronische Schreibmaschine) abgegrenzt.  D.h. der Patentinhaber kann einem Wettbewerber die gewerbliche Produktion einer Patent-Schreibmaschine untersagen, nicht aber die Weitergabe eines Computerprogramms, welches den PC zur Patent-Schreibmaschine macht.  So wurde es bis Anfang der 90er Jahre (see BGH-Urteil Chinesische Schriftzeichen) gehandhabt.  Die Absicht des Gesetzgebers ist auch nach Meinung von Patentjuristen\footnote{see LAMY Droit de l'Informatique et des R\'{e}seaux\footnote{http://www.lamy.fr/store/product.asp?id=48\&nav=affaires}, p. 177 ff.} klar verst\"{a}ndlich.  Das EPA und der BGH wollten jedoch aus wirtschaftspoligischen Erw\"{a}gungen heraus die Regeln \"{a}ndern.  Ihrer Meinung nach ist die ``Softwareindustrie'' inzwischen so bedeutend geworden, dass ihr die Segnungen des Patentwesens nicht l\"{a}nger versagt bleiben d\"{u}rfen.\\\hline
Die Patentierbarkeits-Ausnahmenliste leite sich aus einem einzigen \"{u}bergeordneten Grundgedanken her, wonach eine Erfindung ``technisch'' zu sein habe. & Dieser Behauptung fehlt zun\"{a}chst jede Begr\"{u}ndung.  Der Gesetzgeber hat durch nichts zu erkennen gegeben, dass der Ausnahmenliste der Gedanke der Technizit\"{a}t zugrunde liegt.  Es handelt sich hier um eine vom EPA gew\"{a}hlte rechtsprechungs-interne Hilfstheorie.  Diese Theorie ist nicht sehr gut, denn bei der ``Technizit\"{a}t'' handelt es sich um einen beliebig dehnbaren Rechtsbegriff.  Dehnbare Begriffe k\"{o}nnen jedoch durchaus zur Interpretation von Gesetzen taugen, solange das Ergebnis mit den Intentionen des Gesetzgebers \"{u}bereinstimmt, d.h. solange die Ausnahmenliste sich tats\"{a}chlich aus der gew\"{a}hlten Hilfstheorie herleiten l\"{a}sst.  Heute ist aber das Gegenteil der Fall. Die Definition der ``Technizit\"{a}t'' wird zudem von Jahr zu Jahr in Richtung ``N\"{u}tzlichkeit'' aufgeweicht.  Zun\"{a}chst wurde die ``Kerntheorie'' fallen gelassen, wonach der erfinderische Kern ``technisch'' sein musste, d.h. auf dem Gebiet der Anwendung von Naturkr\"{a}ften und nicht auf dem Gebiet der der Programmlogik liegen musste.  1999 beschloss der BGH in radikaler Abkehr von bisherigen Prinzipen: ``Der technische Charakter einer Lehre wird nicht dadurch fraglich, da{\ss} sie von einem \"{u}blichen Rechner nur den bestimmungsgem\"{a}{\ss}en Gebrauch macht''.\\\hline
Unter ``Programm als solches'' sei nur ein ``Programm, soweit es nicht technisch ist'', zu verstehen. & Dies ist eine aus der Luft gegriffene Fehlinterpretation des Gesetzestextes.  Mit ``Programm als solches'' ist die ``von einer patentierbaren physikalischen Gesamtstruktur losgel\"{o}ste Programmlogik'' gemeint.  M.a.W.:  eine Industriemaschinerie ist auch dann patentierbar, wenn darin Computerprogramme zum Einsatz kommen.  Mit einem solchen \emph{Patent auf einen programmbasierten Industrieprozess} kann man aber niemanden daran hindern, das Programm selber weiterzugeben und weiterzuentwickeln.  Man kann lediglich die Vermarktung der gesamten (Hard- und Software enthaltenden) Computersystems beschr\"{a}nken.  Das ist eine klare und h\"{o}chst sinnvolle Trennung, die auch so vom Gesetzgeber intendiert war (s. LAMY Droit de l'Informatique et des R\'{e}seaux\footnote{http://www.lamy.fr/store/product.asp?id=48\&nav=affaires} p.177 ff).\\\hline
Unter ``gewerbliche Anwendung'' (industrial application / application industrielle) falle ``alles, was man kommerziell nutzen kann''. & Traditionell h\"{a}ngt die Bedeutung von Gewerblichkeit/Industrialit\"{a}t jedoch eng mit der Bedeutung von ``Technizit\"{a}t'' zusammen:  es geht dabei um die Produktion von G\"{u}tern ``unter Anwendung von Naturgesetzen und ohne zwischengeschaltete menschliche T\"{a}tigkeit'' (BGH-Definition).  D.h. eine Arztpraxis ist kein ``Gewerbe'' im Sinne des Patentrechts (ebenso wie des Steuerrechts), weil die dortigen Heilverfahren eine menschliche T\"{a}tigkeit beinhalten.  Gewerblich / industriell ist jedoch die Herstellung von Arzneimitteln und sonstigen Physikalien.  Auch virtuelle Physikalien (propriet\"{a}re Software) werden in gewerblicher Manier vermarktet.  Vollwertige Logikalien hingegen verbreitet sich hingegen ohne jeden gewerblichen Apparat.  D.h. Gewerblichkeit entspringt bei Logikalien nicht technologischen Erfordernissen.  Auch Liebe nennt man ``das \"{a}lteste Gewerbe der Welt'', aber ob man jegliche menschlichen Liebe per Gesetz den Regeln der Gewerblichkeit unterwerfen will, ist eine politische Entscheidung.\\\hline
Das Softwarepatentierungsverbot des EP\"{U} widerspreche dem TRIPS-Vertrag. & TRIPS ist ein Vertrag zwischen Staaten, der nationale Patentsysteme im Hinblick auf den Freihandel ``harmonisieren'', d.h. auf einen relativ abstrakten gemeinsamen Nenner bringen soll.  TRIPS \S{}27 verlangt, dass \emph{auf allen Gebieten der Technik Patente erh\"{a}ltlich sein} sollen, sofern die jeweilige Erfindung neu, erfinderisch und industriell anwendbar ist.  TRIPS legt aber nicht fest, wie Technizit\"{a}t und Industrialit\"{a}t / Gewerblichkeit zu definieren sind.  Es wird noch nicht einmal ausgeschlossen, dass \"{u}ber Technizit\"{a}t und Industrialit\"{a}t hinaus weitere Ausschlusskriterien (z.B. ``nur Erfindungen aber nicht Entdeckungen'') aufgestellt werden.  Es wird lediglich gefordert, dass der jeweilige Unterzeichnerstaat seine Kriterien klar definieren und konsequent (d.h. ohne freihandelsfeindliche Ad-Hoc-Ausnahmen f\"{u}r bestimmte Branchen) anwenden soll.  Die Computerprogramm-Ausnahme des EP\"{U} ist ebenso branchenneutral wie die Ausnehmung von Entdeckungen, Spielen und mathematischen Theorien.  Es werden hier nicht bestimmte Branchen sondern bestimmte Typen und Ausformungen geistiger Leistungen ausgeklammert.  TRIPS ordnet \"{u}brigens in \S{}10 Computerprogramme ausdr\"{u}cklich dem Urheberrecht zu.\\\hline
\end{tabular}
\end{center}

Eigentlich ist es sehr leicht, die europ\"{a}ische Patentierbarkeits-Ausnahmenliste sinnvoll und widerspruchsfrei im Sinne der einschl\"{a}gigen Gesetze zu interpretieren:

\begin{quote}
Patentierbar sind nicht informationelle Gegenst\"{a}nde (Idee, Information, Logikalie, Gleichung, Algorithmus, Software), sondern deren gewerbliche Umsetzung in die automatisierte Produktion materieller G\"{u}ter.
\end{quote}

Demnach k\"{o}nnte man aufgrund von Patentanspr\"{u}chen den gewerblich organisierten Verkauf von kompletten Rechensystemen aus Hardware und Software (z.B. MP3-Abspielger\"{a}ten oder Rechnern mit vorinstallierter Software) untersagen, nicht jedoch die Weitergabe und Anwendung der Software auf beliebigen anderen Rechnern.  Dies entspricht nicht nur dem Buchstaben und Geist des EP\"{U} sondern auch dem Buchstaben und Geist des TRIPS-Vertrages.

Das EPA hat sich hingegen nach und nach in eine verworrene, selbst f\"{u}r Patentfachleute unverst\"{a}ndliche Rechtslage verstrickt.  Dies konnte deshalb unkritisiert durchgehen, weil unter den diskutierenden Patentjuristen ein bestimmter wirtschaftspolitischer Konsens herrschte.   Dieser Konsens st\"{u}tzt sich jedoch weder auf den Willen des Gesetzgebers noch auf eine sorgf\"{a}ltige wirtschaftspolitische Argumentation.
\end{sect}

\begin{sect}{diss}{Legal Inconsistencies, Conflicts with other Legal Goods}
\begin{sect}{inter}{EU Interoperability Directive vs Property on Interfaces }
Software patents jeopardise the principle of interoperability, as laid down in the EU Software Directive of 1991.

Dort ist ausdr\"{u}cklich zu lesen, dass die Nachprogrammierung von Schnittstellen deshalb nicht untersagt werden k\"{o}nne, {\bf weil nur die konkrete Ausformung eines Computerprogramms, nicht aber die zugrundeliegenden Ideen als Eigentum beansprucht werden k\"{o}nnen}.  Sinn der Patentierung ist es aber gerade, zugrundeliegende Ideen in Besitz zu nehmen, um so Interoperabilit\"{a}tsbeschr\"{a}nkungen, die in der \"{O}konomie der virtuellen Physikalien (propriet\"{a}ren Software) den wichtigsten Tauschgegenstand darstellen (s.o.), zu erm\"{o}glichen.

In order to remove all possible ambiguity, a current french law proposal\footnote{} is introducing a ``right to compatibility''.

\begin{quote}
Every natural or legal person has the right to publish and use a piece of software that is compatible to the communication standards of another software.
\end{quote}

This way it is made clear that a competition right (right to compatibility) will prevail over existing and future property rights.

This problem could be solved using the traditional means of a compulsory license.  Such licensing would however sooner or later affect all interesting software patents, because in the domain of information systems the question of interoperability / compatibility will occur sooner or later.
\end{sect}

\begin{sect}{dist}{Distortion of Competition in favor of traditional modes of distribution}
Sobald ein Patent auf ``ein Computerprogramm, dadurch gekennzeichnet, dass ...'' erteilt wird, riskiert man nunmehr durch blo{\ss}e \emph{Weitergabe eines Programms} (und nicht etwa nur durch die \emph{Ausf\"{u}hrung eines Rechenverfahrens}) das Patent zu verletzen.  Das verursacht ernsthafte Probleme f\"{u}r neuere Distributionsmodelle wie etwa Shareware, Freeware und Freie Software.

Shareware, a commercially successful concept highly developped in Europe (e.g. GoLive Cyber Studio, later bought by Adobe) is characterised by a separation of \begin{itemize}
\item
the right to copy and

\item
the right to use
\end{itemize} a piece of software.

Daher wird ein Shareware-Programm viel h\"{a}ufiger kopiert als tats\"{a}chlich verwendet.

Jede unbenutzte Kopie eines solchen Programms k\"{o}nnte Patentanspr\"{u}chen unterliegen (in den USA wurde hochgerechnet, dass ein Programm durchschnittlich 8 Patente verletzt).  Bei herk\"{o}mmlicher kommerzieller Software hingegen w\"{u}rden nur tats\"{a}chlich benutzte (weil gekaufte) Kopien unter den Patentanspruch fallen.  D.h. die ``entgangenen Gewinne'', auf die ein Patentinhaber einen Softwareautor verklagen k\"{o}nnte, w\"{a}ren bei frei kopierbaren Programmen wesentlich h\"{o}her als bei kopierbeschr\"{a}nkter Software, und der Patentinhaber h\"{a}tte grunds\"{a}tzlich ein Interesse daran, seine ``Erfindung'' an Firmen zu lizenzieren, die mit restriktiven Distributionsverfahren arbeiten.  Eine Distribution unter einer --- aus technischer Sicht oft unbedingt w\"{u}nschenswerten --- freien Lizenz wie der GPL k\"{a}me niemals in Frage, da wenigstens die Patentierungskosten eingefahren werden m\"{u}ssen.  Es handelt sich insofern bei Softwarepatenten um einen wettbewerbsverzerrenden staatlichen Eingrif zugunsten veralteter, technisch unzweckm\"{a}{\ss}iger Vermarktungsverfahren.

Es sind bereits L\"{o}sungen\footnote{http://www.freepatents.org/adapt/useright/useright.pdf} vorgeschlagen worden, durch die man im allerseitigen Interesse diesen Widerspruch aufl\"{o}sen k\"{o}nnte.  Grundlage dieser L\"{o}sungen ist eine saubere Trennung zwischen Urheber- und Patentrecht: das Urheberrecht gibt demnach dem Autor die Kontrolle \"{u}ber die Weiterverbreitung seines Werkes, w\"{a}hrend das Patentrecht dem Erfinder die Kontrolle nicht \"{u}ber die Verbreitung von Information (denn nichts anderes ist Software) sondern \"{u}ber die \emph{gewerbliche Verwertung} derselben gibt.
\end{sect}

\begin{sect}{komc}{Special Concentration Effect in case of Complex Systems}
Die meisten Programme bestehen aus Tausenden von m\"{o}glicherweise patentierten oder patentierbaren Bausteinen.  Die Patentinhaber eines Bausteins k\"{o}nnen ihre Rechte nur verwerten, indem sie sich in einen schwerf\"{a}lligen monopolistischen Apparat einf\"{u}gen.

Diese Geflechte wirken sich in der Autoindustrie nicht besonders nachteilhaft aus, weil dort ohnehin hohe Materialinvestitionen anfallen.  Im Bereich der Informationstechnik gen\"{u}gen oft geringe Investitionen, so dass sich oft kleine H\"{u}tten-Unternehmen in K\"{u}rze zu wichtigen Mitspielern mausern k\"{o}nnen.
\end{sect}

\begin{sect}{sekur}{Promotion of Secrecy vs Security \& Consumer Safety (see article \percent{}s of the Treaty of Rome)}
Vor Angriffen durch Softwarepatente sch\"{u}tzt man sich am besten, indem man die Baupl\"{a}ne (Quelltexte) seines Programmes versteckt h\"{a}lt.  Um Lizenzen zu erhalten, muss man wiederum die Kontrolle \"{u}ber die Weiterverbreitung seines Programmes maximieren (s.o.).  Beides schlie{\ss}t die Freigabe nach OpenSource-Lizenz aus.  Sicherheitssensible Programme w\"{u}rden auf das bekannte Niveau kommerzieller Software (siehe I-Love-You-Virus, NSA-Hintert\"{u}ren u.v.m.) herabgedr\"{u}ckt.  Softwarepatentierung forciert somit einen R\"{u}ckschritt in Sachen Sicherheit und Verbraucherschutz.  Dar\"{u}ber hinaus d\"{u}rften die wettbewerbsbehindernden Wirkungen der Softwarepatentierung (s.o.) auf eine geringere Wahlfreiheit des Verbrauchers hinwirken.  Gleichzeitig ist mit einer durch Patentverwaltungs- und Prozesskosten bedingten Erh\"{o}hung der Verbraucherpreise zu rechnen, zu der die Versch\"{a}rfung der Monopolsituation ein \"{u}briges beitragen wird.  All dies widerspricht den R\"{o}mischen Vertr\"{a}gen, die vorschreiben, dass neue europ\"{a}ische Gesetze mit den Interessen der \"{o}ffentlichen Sicherheit und des Verbraucherschutzes im Einklang stehen m\"{u}ssen.

Manche Leute vermuten, das Patentrecht k\"{o}nnte die Offenlegung der Quellen f\"{o}rdern, da nun Programmideen ja besser ``gesch\"{u}tzt'' w\"{u}rden.  Dies w\"{a}re zumindest in den seltenen F\"{a}llen vorstellbar, wo die Vorenthaltung des Quelltextes bisher wirklich vor allem der Geheimhaltung einer Programmidee (und nicht etwa der Blockierung von Interoperabilit\"{a}t o.\"{a}.) diente.  Aber selbst in diesem eher seltenen Fall w\"{u}rde derjenige, der Quelltext mitliefert oder gar ver\"{o}ffentlicht, sich damit einer erh\"{o}hten Gefahr von (potentiell ruin\"{o}sen) Patentklagen aussetzen. Selbst wenn er den Quelltext ver\"{o}ffentlichen w\"{u}rde, k\"{o}nnte er dadurch noch lange nicht von der weltweiten fachkompetenten Aufmerksamkeit profitieren, die quellenoffener Software zugute zu kommen pflegt.  Denn die meisten Entwickler verbringen ihre kostbare Zeit ungern mit ``patent-verseuchten'' Quelltexten.

Die Ver\"{o}ffentlichung des Quelltextes bietet den besten Schutz vor versteckten Hintert\"{u}ren in sicherheitsrelevanter Software. Daher schr\"{a}nken Software-Patente auch die M\"{o}glichkeiten eine nachvollziehbar sicheren Kommunikation ein.

Lekt\"{u}re hierzug:
\begin{itemize}
\item
Ungeniert schn\"{u}ffeln vor allem die Amerikaner die deutsche Wirtschaft aus: Mit gro{\ss}em Aufwand und High-Tech durchforsten sie Telefonleitungen und Computernetze.\footnote{http://www.spiegel.de/spiegel/0,1518,14871,00.html}

\item
Hintert\"{u}r in LotusNotes\footnote{http://www.heise.de/newsticker/data/jk-24.06.99-000/}

\item
Heise: BMWi f\"{o}rdert GnuPG\footnote{http://www.heise.de/newsticker/result.xhtml?url=/newsticker/data/fr-15.11.99-000} (Dort laesst sich eine hintertuer halt sehr viel schwerer einbauen als in lotus notes. GnuPG unterstuetzt derzeit kein RSA und IDEA wegen patent-problemen.)
\end{itemize}

(when  (blockquote La Commission, dans ses propositions prévues au paragraphe 1 en matière de santé, de sécurité, de protection de l'environnement et de protection des consommateurs, prend pour base un niveau de protection élevé en tenant compte notamment de toute nouvelle évolution basée sur des faits scientifiques. Dans le cadre de leurs compétences respectives, le Parlement européen et le Conseil s'efforcent également d'atteindre cet objectif.))
\end{sect}

\begin{sect}{kultur}{Media Cartels vs Cultural Diversity (see article \percent{}s of the Treaty of Rome)}
Auch das vom R\"{o}mischen Vertrag in besonderer Weise gesch\"{u}tzte Kulturleben wird gef\"{a}hrdet, wenn Kommunikationsnormen und sonstige grundlegende informationelle Infrastrukturen von einem oder wenigen oligopolistischen Konglomeraten kontrolliert werden.  Auch aus diesem Grund ist es wichtig, dem Recht auf Kompatibilit\"{a}t / Interoperabilit\"{a}t (s.o.) Vorrang vor Patentanspr\"{u}chen einzur\"{a}umen.

(when  (blockquote (ML LxW L'action de la Communauté vise à encourager la coopération entre états membres et, si nécessaire, à appuyer et compléter leur action dans les domaines suivants: - l'amélioration de la connaissance et de la diffusion de la culture et de l'histoire des peuples européens, - la conservation et la sauvegarde du patrimoine culturel d'importance européenne, - les échanges culturels non commerciaux, - la création artistique et littéraire, y compris dans le secteur de l'audiovisuel.) (ML Lne La Communauté tient compte des aspects culturels dans son action au titre d'autres dispositions du présent traité, afin notamment de respecter et de promouvoir la diversité de ses cultures.)))
\end{sect}

\begin{sect}{indure}{Promotion of Oligopolism vs Acceleration of Technology Transfer (see article \percent{}s of the Treaty of Rome)}
Softwarepatente schr\"{a}nken die Nutzbarmachung von technischen Fortschritten ein.  Kooperative Probleml\"{o}sungsprozesse, wie sie im akademischen Umfeld und in der Freien Software \"{u}blich und sehr erfolgreich sind, werden zerschlagen und kleineren Unternehmen wird durch ein oligopolistisches Geflecht die M\"{o}glichkeit zur Nutzung und Weiterentwicklung vorhandener Techniken verbaut.

(when  (ML Cix   La Communauté et les états membres veillent à ce que les conditions nécessaires à la compétitivité de l'industrie de la Communauté soient assurées. A cette fin, conformément à un systýme de marchés ouverts et concurrentiels, leur action vise à: - accélérer l'adaptation de l'industrie aux changements structurels; - encourager un environnement favorable à l'initiative et au développement des entreprises de l'ensemble de la Communauté, et notamment des petites et moyennes entreprises; - encourager un environnement favorable à la coopération entre entreprises; - favoriser une meilleure exploitation du potentiel industriel des politiques d'innovation, de recherche et de développement technologique.))
\end{sect}

(koal (ML KgG Koalitionsvereinbarungen zwischen SPD und Grünen von 1998-10-20 German Federal Government Coalition Agreement German Coalition Agreement between Social Democrats and Greens) (ML HWW Hierin verpflichtet sich die Regierung darauf, auf eine %(q:beschleunigte Nutzung und Verbreitung der Informationstechnologien in der Gesellschaft) hinzuarbeiten.  Dieser Imperativ ist mit einer Zustimmung zur Softwarepatentierung unvereinbar. This agreement obliges the government to work for an %(q:accelerated use and popularisation of information technologies in society).  If the government agreed to make software patentable, it would breach this obligation.))
\end{sect}

\begin{sect}{debr}{What {\bf can} the Federal Government do?}
Als Repr\"{a}sentant des am st\"{a}rksten von den wirtschaftspolitischen Folgen betroffenen europ\"{a}ischen EP\"{U}-Vertragsstaates ist die Bundesregierung juristisch und moralisch berechtigt, jeder gegen ihre Interessen gerichteten \"{A}nderung des EP\"{U} ihre Zustimmung zu verweigern und auf eine wirtschaftspolitisch sinnvolle Regelung des Patentwesens zu dr\"{a}ngen.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

