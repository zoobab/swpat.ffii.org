\begin{subdocument}{swpbpuseu}{Wirkung von Softwarepatenten in USA und EU}{http://swpat.ffii.org/termine/2000/bmwi0518/jinvi/useu/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Erstmals fragt eine europ\"{a}ische Regierungsorganisation \"{o}ffentlich nach den Auswirkungen geplanter Gesetzes\"{a}nderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII m\"{o}chte dazu beitragen, dass diese \"{u}berf\"{a}lligen Fragen mit dem geb\"{u}hrenden Ernst studiert werden, bevor man einschl\"{a}gige Gesetze und Vertr\"{a}ge \"{a}ndert.}
\begin{sect}{util}{Wo wirken Softwarepatente aufbauend?}
\begin{sect}{grandkomp}{Firmen mit gro{\ss}en Patentportfolios.}
In den USA geh\"{o}ren ca 7\percent{} der Softwarepatente IBM.  S. Statistiken von Gregory Aharonian.

Die meisten Computerprogramme sind komplexe Systeme, die gleichzeitig von vielen Patenten betroffen sein k\"{o}nnen.  Es n\"{u}tzt nicht unbedingt viel, wenn man nur ein Patent besitzt.  Meist besteht eine wirksame Absicherung darin, dass man mit anderen Gro{\ss}firmen Nicht-Angriffs-Pakte (Kreuzlizenzierungs-Abkommen) schlie{\ss}t.

Firmen wie IBM, Sun u.v.m. sind durchaus Leistungstr\"{a}ger, deren Forschungsinvestitionen gelegentlich nicht nur r\"{a}uberischen Plattformstrategien dienen sondern auch der \"{O}ffentlichkeit zugute kommen.  Es w\"{a}re interessant, zu untersuchen, in welchem Ma{\ss}e dies durch das Vorhandensein von Patentportfolios gef\"{o}rdert wird.
\end{sect}

\begin{sect}{novmerk}{Firmen, deren Gesch\"{a}ft auf wenigen patentierbaren Ideen f\"{u}r einen neuen Markt beruht}
Um sein 1-Click-Patent umzusetzen, muss Amazon kein komplexes Systeme bauen, das viele andere Patente verletzen k\"{o}nnte.  Gleichzeitig ist das 1-Click-Patent unmittelbar mit einer lukrativen Gesch\"{a}ftsidee verbunden.  Es ist ein in Informationstechnik gekleidetes Gesch\"{a}ftsideen-Patent.

Auch ein kleines Unternehmen, das Patente dieser Art besitzt, kann sich damit eine beneidenswerte Stellung sichern.  Insbesondere Unternehmen, die als erste einen werdenden Markt betreten, k\"{o}nnen durch Patentierung der dabei entstehenden Software-Ideen den Markt gegen Nachz\"{u}gler absperren.   Ein solches Unternehmen kann sich unter Umst\"{a}nden zu einem hohen Preis von einem Gro{\ss}unternehmen kaufen lassen, das damit sein Patent-Portfolio um einen wertvollen Mosaikstein erweitert.  Ferner hat es gute Chancen, Risikokapital anzuziehen.
\end{sect}

\begin{sect}{invent}{Unternehmen hochspezialisierter technischer Erfinder}
Die Anspr\"{u}che der US-Firma Stac auf eine neue Kompressionstechnik konnten in einem Prozess gegen Microsoft durchgesetzt werden.

Einige echte Erfindungen auf technischem Gebiet wie etwa das MPeg-Verfahren konnten dank Patentrechten besser verwertet werden, und dies gelang sogar in einer fairen Weise, die auch die Interoperabilit\"{a}t von MPeg mit verschiedenen Systemen erlaubte.  Das Patentrecht bietet jedoch bisher keinerlei Schutzmittel gegen eine r\"{a}uberische Vermarktung solcher Patentrechte.

Diese erfreulichen F\"{a}lle sind leider selten, und ihnen k\"{o}nnte mit einem eigens daf\"{u}r entworfenen Sui-Generis-Rechtsschutz besser geholfen werden als mit dem Patentrecht.  Das Patentrecht weist n\"{a}mlich zwei Nachteile auf:

\begin{description}
\item[undifferenziert, aussageschwach:]\ Es wirft die echten Erfinder in einen Topf mit der Masse der Wegelagerer-Patente.  Auch der Kapitalmarkt ist gegen\"{u}ber der Patentqualit\"{a}t zun\"{a}chst blind.  Zu den ohnehin hohen Patentkosten (Geb\"{u}hren f\"{u}r Durchfechtung vor Gericht) kommen also unn\"{o}tige Werbekosten hinzu.
\item[festgefahren, unverbesserlich:]\ Es l\"{a}sst sich nicht auf die Besonderheiten der einzelnen Schutzgegenst\"{a}nde (z.B. Logikalien vs Physikalien) hin optimieren.  TRIPS schreibt vor, dass ``auf allen Gebieten der Technik'' genau gleich gearteter Rechtsschutz zu gew\"{a}hren ist.  Das bedeutet z.B. dass der Patentschutz f\"{u}r durchweg mindestens 17 Jahre gew\"{a}hrt werden muss.
\end{description}
\end{sect}

\begin{sect}{patenthai}{Patentberater, Patentmakler}
Darin arbeiten z.B. mehrere 10 bis mehrere 100 Patentspezialisten, die Rechte von Kleinfirmen aufkaufen und an diejenigen Gro{\ss}firmen weitervermitteln, die aufgrund ihres fl\"{a}chendeckenden Patentportfolios in der Lage sind, daraus den gr\"{o}{\ss}ten Nutzen daraus zu ziehen. 

Andere Berater suchen in Forschungsabteilungen von Firmen nach ``verstecktem Gold'', d.h. scheinbar trivialen Erfindungen, von denen kein Techniker angenommen h\"{a}tte, dass man sie patentieren kann.\footnote{http://www.lawnewsnetwork.com/practice/techlaw/news/A11873-1999Dec17.html}
\end{sect}

\begin{sect}{patamt}{Patent\"{a}mter und Patentanw\"{a}lte}
Siehe dazu das eingangs angef\"{u}hrte Zitat aus einem Rundbrief einer im Softwarebereich besonders r\"{u}hrigen M\"{u}nchener Patentanwaltskanzlei an ihre Mandanten.

Dass diese bahnbrechende Ausdehnung des Patentwesens auch den Patent\"{a}mtern mehr Geld und Macht bringt, versteht sich von selbst.
\end{sect}
\end{sect}

\begin{sect}{neutil}{Wo wirken Softwarepatente zerst\"{o}rerisch?}
\begin{sect}{libre}{Freie Software, Informationelle Infrastruktur}
Es ist nicht m\"{o}glich, patentierte Verfahren in freier Software zu verwenden.  Patentierte Logikalien sind nicht mehr vollwertig, da nicht mehr einem uneingeschr\"{a}nkten gemeinschaftlichen Verbesserungsprozess unterliegend.  Gleichzeitig ist freie Software besonders verwundbar, da der Quelltext offen liegt und besonders leicht auf Patentverletzungen abgesucht werden kann.

\begin{itemize}
\item
Vektorfontsdarstellung durch Patent-U-Boote bedroht\footnote{http://www.freetype.org/patents.html}

\item
Bildverarbeitung durch IPIX torpediert\footnote{http://www.virtualproperties.com/noipix/patents.html}

\item
Starke Verschl\"{u}sselung nicht in GnuPG

\item
GIF-Bildererzeugung nur mit propriet\"{a}rer Software\footnote{http://burnallgifs.org}

\item
WWW-Normierung von Patenten torpediert\footnote{http://www.w3.org/1999/05/P3P-PatentPressRelease.html} (Der Pr\"{a}sident des W3C erkl\"{a}rte auf einer Softwarepatent-Anh\"{o}rung in Paris, ca 30\percent{} der Mittel seines Gremiums seien derzeit durch Patent\"{a}rger gebunden.)

\item
Bruce Perens wurde von Patentanmeldern gezwungen, sein ganz eigenst\"{a}ndig entwickeltes Programmierwerkzeug EFence aus dem Verkehr zu ziehen.
\end{itemize}
\end{sect}

\begin{sect}{distrib}{Distributoren von freier Software und Shareware}
Solange nur industrielle Prozesse mit Softwareanteil aber nicht ``Programme als solche'' patentierbar sind, stellt das Weiterverteilen der Software keine Patentverletzung dar.

In dem Moment, wo dies sich \"{a}ndert, k\"{o}nnen Distributoren wegen Patentverletzung belangt und auf ``entgangene Gewinne'' verklagt werden.

Eine Firma wie SuSE, die Tausende von Programmen verteilt, m\"{u}sste, um sich gegen solche Rechtsrisiken abzusichern, Millionen von Algorithmen mit Hundertausenden von Patenten vergleichen und auf Verletzungen absuchen.  Da dies unm\"{o}glich ist und man gerade in Logikalien-Angelegenheiten nicht den geringsten Anlass hat, auf Verst\"{a}ndnis bei der Patentjustiz zu hoffen, steht eine Distributionsfirma immer mit einem Bein im Gef\"{a}ngnis.
\end{sect}

\begin{sect}{komplexkre}{kleine bis mittlere Urheber[firmen] komplexer Systeme}
Firmen wie Netpresenter haben einerseits nicht die Mittel, um die zum Alltagsgesch\"{a}ft der Programmierung entstehenden Neuerungen patentrechtlich zu nutzen.  Andererseits m\"{u}ssen sie st\"{a}ndig damit rechnen, bei ihren t\"{a}glichen Neuerungen auf Tretminen zu sto{\ss}en.  Einige Europ\"{a}ische Firmen dieses Typs wie z.B. Netpresenter (Holland), MySQL (Schweden), Infomatec (Deutschland), Sniff (\"{O}sterreich), die durchaus nicht nur dem Linux-Umfeld entstammen, haben gro{\ss}e Angst vor dem Patentsystem.  Sie m\"{u}ssen tats\"{a}chlich f\"{u}rchten, dass ihnen angesichts der Schutzgeld-Forderungen aus zehntausenden von Patenten schnell die Luft ausgeht.
\end{sect}

\begin{sect}{kelkgrand}{manche Gro{\ss}firmen}
Selbst Firmen mit gro{\ss}em Patentportolio profitieren nicht immer von der Existenz des Patentsystems.  Patentgegner Oracle hat sich nur zu defensiven Zwecken ein gro{\ss}es Patentportfolio angelegt.  D.h. Oracle musste viel Geld ausgeben, um sich vor Gefahren zu sch\"{u}tzen, die sonst nicht existiert h\"{a}tten.  Die auf Patente gegr\"{u}ndete Firma OpenMarkets begr\"{u}ndete 1999 umfangreiche Personal-Entlassungsma{\ss}nahmen u.a. mit Fehlinvestitionen in Patente.  Die umfangreiche Patentesammlung hatte zu verlustreichen Prozessen mit anderen Firmen, u.a. mit IBM, gef\"{u}hrt.
\end{sect}

\begin{sect}{unbeteil}{Unbeteiligte Branchen, der Verbraucher, die \"{O}ffentlichkeit}
Durch Patente erh\"{o}hen sich die allgemeinen Verwaltungs- und Transaktionskosten.  Viele Unternehmen geben bereits heute 10-30\percent{} ihrer Eink\"{u}nfte f\"{u}r den Rechtsetat aus.  Das schl\"{a}gt selbstverst\"{a}ndlich auf die Preise durch.

\"{U}ber den Umweg der Softwarepatente werden Gesch\"{a}ftsmethoden aller Branchen patentierbar, und allerlei bisher unverf\"{a}ngliche Handlungen werden als ``Beihilfe zur Patentverletzung'' Gegenstand von Abmahnungen.\footnote{http://www.upside.com/texis/mvm/opinion/story?id=382a24f90}

Hinzu kommt die Zerst\"{u}ckelung der bisher relativ freien informationellen Infrastruktur und die Zerst\"{o}rung freier Software, auf deren Existenz das Internet und das WWW aufbaut und von der unz\"{a}hlige Unternehmen in kaum sch\"{a}tzbarem Ma{\ss}e profitieren.
\end{sect}
\end{sect}

\begin{sect}{srch}{Sind die Patent\"{a}mter ihren Aufgaben gewachsen?}
NEIN, meint Gregory Aharonian.\footnote{http://swpat.ffii.org/termine/2000/bmwi0518/aharonian/index.de.html}

BALD, versprechen Softwarepatent-Bef\"{u}rworter seit vielen Jahren.  U.a. schreiben Betten und Esslinger in ``Computerrecht'' 2000/1:

\begin{quote}
Ein Problem stellt die schwierige Recherchierbarkeit von Softwareerfindungen dar, da in den Datenbanken der Patent\"{a}mter in diesem vergleichsweise jungen Gebiet noch nicht sehr viel leicht recherchierbare, klassifizierte Patentliteratur vorhanden ist.  Dies hat zur Erteilung von zu breiten Patenten auf dem Softwaresektor gef\"{u}hrt und wird sich aber sp\"{a}testens dann \"{a}ndern, wenn ein gr\"{o}{\ss}erer Fundus an klassifizierter Patentliteratur vorhanden ist.
\end{quote}

Patentpr\"{u}fer sehen sich t\"{a}glich gezwungen, triviale Erfindungen zur Patentierung zuzulassen.  Es gelingt n\"{a}mlich nicht, die zuvor ver\"{o}ffentlichte Technik ausfindig zu machen.  Patententhusiasten versprechen immer wieder, dass es sich hierbei nur um ein zeitweiliges \"{U}bergangsph\"{a}nomen handelt, aber Gregory Aharonians Studien zeigen deutlich, dass die Qualit\"{a}t der Patentpr\"{u}fungen eher sinkt als steigt, und dass auch zur Abhilfe gegr\"{u}ndete Institutionen wie das SPI\footnote{http://www.spi.org} ihren Auftrag verfehlt haben.

Das Problem ist auch grunds\"{a}tzlicher Natur:

\begin{sect}{sprachbarr}{Barriere zwischen Computersprache und Recherche-Sprache}
Computerprogramme dokumentieren sich selbst.  Sie sind als solche bereits eine Form der Information und enthalten somit ihre eigene Dokumentation.   Besonders vollwertige Systeme wie z.B. das Drucksatzsystem TeX sind oft nach dem Prinzip der ``literarischen Programmierung'' verfasst.  D.h. die Dokumentation ist mit dem Programm verwoben und aus ihm erzeugbar.  Bei weniger vollwertigen Systemen ist die Dokumentation nicht getrennt erzeugbar und daher f\"{u}r Patentrecherchen kaum zug\"{a}nglich.  Bei propriet\"{a}rer Software ist die Bauplan-Information dem Patentrechercheur vollends verschlossen.  Dennoch geh\"{o}rt auch diese Software zum Stand der Technik, denn der ``in der Kunst ge\"{u}bte Fachmann'' kann sie dekompilieren, egal ob das erlaubt ist oder nicht.
\end{sect}

\begin{sect}{fachlit}{Fachliteratur spielt eine untergeordnete Rolle}
Weil bei Logikalien keine Grenze zwischen ``technischer Information'' und ``gewerblicher Anwendung'' gezogen werden kann (s.o.), spielen auch Fachzeitschriften nicht die Rolle, die sie bei den klassischen Natur- und Ingenieurwissenschaften spielen.  F\"{u}r Patentliteratur gilt dies a fortiori.  Denn ein Gro{\ss}teil der Neuerungen wird weiterhin au{\ss}erhalb des Patentwesens stattfinden.
\end{sect}
\end{sect}

\begin{sect}{entp}{Verpassen die Europ\"{a}er Chancen?}
Laut Informationen des EPA befanden sich 1997 ca 75\percent{} der (gesetzeswidrig) angemeldeten europ\"{a}ischen Softwarepatente im Besitz von amerikanischen und japanischen Firmen.  In den USA liegt diese Zahl bei 90\percent{}.

Hier\"{u}ber machen sich diejenigen Patentexperten, die diese Entwicklung ma{\ss}geblich verursacht haben, Sorgen.  Die EU plant eine Patentaufkl\"{a}rungskampagne.  Ein f\"{u}hrender Patentinflationstreiber warnt im Rundschreiben an seine Mandanten:

\begin{quote}
Durch die .. Entwicklung der Rechtsprechung .. hat sich das Patentrecht von der traditionellen Beschr\"{a}nkung auf die verarbeitende Industrie gel\"{o}st und ist heute auch f\"{u}r Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu bef\"{u}rchten, das die deutschen Dienstleistungsunternehmen ... insbesondere gegen\"{u}ber der US-amerikanischen Konkurrenz ins Hintertreffen geraten.
\end{quote}

Vermutlich w\"{u}rde die europ\"{a}ische Softwarebranche tats\"{a}chlich einiges mehr an Patenten anmelden, wenn sie nicht nur in den USA sondern auch auf dem heimischen Markt durch das Vorhandensein eines allumfassenden Patentsystems dazu gezwungen w\"{u}rde.

Dies ist aber von der Frage zu trennen, ob wir ein solches System auch haben wollen.

Im internationalen Vergleich nutzt ein solches System immer eher denjenigen,deren Industrie weiter entwickelt ist.  Die USA wurden erst in dem Moment zum Vorreiter der Patentinflation, als sie selbst Spitzenpositionen in der ``Neuen Wirtschaft'' bereits besetzt hatten.  
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

