Dns: Wirkung von Softwarepatenten in USA und EU
Eht: Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert.
W33: Wo wirken Softwarepatente aufbauend?
Fgn: Firmen mit großen Patentportfolios.
IeW: In den USA gehören ca 7% der Softwarepatente IBM.  S. Statistiken von Gregory Aharonian.
DEh: Die meisten Computerprogramme sind komplexe Systeme, die gleichzeitig von vielen Patenten betroffen sein können.  Es nützt nicht unbedingt viel, wenn man nur ein Patent besitzt.  Meist besteht eine wirksame Absicherung darin, dass man mit anderen Großfirmen Nicht-Angriffs-Pakte (Kreuzlizenzierungs-Abkommen) schließt.
Fte: Firmen wie IBM, Sun u.v.m. sind durchaus Leistungsträger, deren Forschungsinvestitionen gelegentlich nicht nur räuberischen Plattformstrategien dienen sondern auch der Öffentlichkeit zugute kommen.  Es wäre interessant, zu untersuchen, in welchem Maße dies durch das Vorhandensein von Patentportfolios gefördert wird.
Fce: Firmen, deren Geschäft auf wenigen patentierbaren Ideen für einen neuen Markt beruht
UPn: Um sein 1-Click-Patent umzusetzen, muss Amazon kein komplexes Systeme bauen, das viele andere Patente verletzen könnte.  Gleichzeitig ist das 1-Click-Patent unmittelbar mit einer lukrativen Geschäftsidee verbunden.  Es ist ein in Informationstechnik gekleidetes Geschäftsideen-Patent.
AWn: Auch ein kleines Unternehmen, das Patente dieser Art besitzt, kann sich damit eine beneidenswerte Stellung sichern.  Insbesondere Unternehmen, die als erste einen werdenden Markt betreten, können durch Patentierung der dabei entstehenden Software-Ideen den Markt gegen Nachzügler absperren.   Ein solches Unternehmen kann sich unter Umständen zu einem hohen Preis von einem Großunternehmen kaufen lassen, das damit sein Patent-Portfolio um einen wertvollen Mosaikstein erweitert.  Ferner hat es gute Chancen, Risikokapital anzuziehen.
Upc: Unternehmen hochspezialisierter technischer Erfinder
Der: Die Ansprüche der US-Firma Stac auf eine neue Kompressionstechnik konnten in einem Prozess gegen Microsoft durchgesetzt werden.
Eru: Einige echte Erfindungen auf technischem Gebiet wie etwa das MPeg-Verfahren konnten dank Patentrechten besser verwertet werden, und dies gelang sogar in einer fairen Weise, die auch die Interoperabilität von MPeg mit verschiedenen Systemen erlaubte.  Das Patentrecht bietet jedoch bisher keinerlei Schutzmittel gegen eine räuberische Vermarktung solcher Patentrechte.
Dil: Diese erfreulichen Fälle sind leider selten, und ihnen könnte mit einem eigens dafür entworfenen Sui-Generis-Rechtsschutz besser geholfen werden als mit dem Patentrecht.  Das Patentrecht weist nämlich zwei Nachteile auf:
uzs: undifferenziert, aussageschwach
Eet: Es wirft die echten Erfinder in einen Topf mit der Masse der Wegelagerer-Patente.  Auch der Kapitalmarkt ist gegenüber der Patentqualität zunächst blind.  Zu den ohnehin hohen Patentkosten (Gebühren für Durchfechtung vor Gericht) kommen also unnötige Werbekosten hinzu.
frr: festgefahren, unverbesserlich
EWz: Es lässt sich nicht auf die Besonderheiten der einzelnen Schutzgegenstände (z.B. Logikalien vs Physikalien) hin optimieren.  TRIPS schreibt vor, dass %(q:auf allen Gebieten der Technik) genau gleich gearteter Rechtsschutz zu gewähren ist.  Das bedeutet z.B. dass der Patentschutz für durchweg mindestens 17 Jahre gewährt werden muss.
Pra: Patentberater, Patentmakler
Din: Darin arbeiten z.B. mehrere 10 bis mehrere 100 Patentspezialisten, die Rechte von Kleinfirmen aufkaufen und an diejenigen Großfirmen weitervermitteln, die aufgrund ihres flächendeckenden Patentportfolios in der Lage sind, daraus den größten Nutzen daraus zu ziehen. 
Aqn: Andere Berater suchen in Forschungsabteilungen von Firmen nach %(q:verstecktem Gold), d.h. scheinbar trivialen Erfindungen, von denen kein Techniker angenommen hätte, dass man sie patentieren kann.
Pet: Patentämter und Patentanwälte
WSe: Siehe dazu das eingangs angeführte Zitat aus einem Rundbrief einer im Softwarebereich besonders rührigen Münchener Patentanwaltskanzlei an ihre Mandanten.
DtW: Dass diese bahnbrechende Ausdehnung des Patentwesens auch den Patentämtern mehr Geld und Macht bringt, versteht sich von selbst.
Wer: Wo wirken Softwarepatente zerstörerisch?
Fef: Freie Software, Informationelle Infrastruktur
Ero: Es ist nicht möglich, patentierte Verfahren in freier Software zu verwenden.  Patentierte Logikalien sind nicht mehr vollwertig, da nicht mehr einem uneingeschränkten gemeinschaftlichen Verbesserungsprozess unterliegend.  Gleichzeitig ist freie Software besonders verwundbar, da der Quelltext offen liegt und besonders leicht auf Patentverletzungen abgesucht werden kann.
Vlt: Vektorfontsdarstellung durch Patent-U-Boote bedroht
BtP: Bildverarbeitung durch IPIX torpediert
Shi: Starke Verschlüsselung nicht in GnuPG
Gur: GIF-Bildererzeugung nur mit proprietärer Software
Wnt: WWW-Normierung von Patenten torpediert
DaG: Der Präsident des W3C erklärte auf einer Softwarepatent-Anhörung in Paris, ca 30% der Mittel seines Gremiums seien derzeit durch Patentärger gebunden.
Bem: Bruce Perens wurde von Patentanmeldern gezwungen, sein ganz eigenständig entwickeltes Programmierwerkzeug EFence aus dem Verkehr zu ziehen.
Doa: Distributoren von freier Software und Shareware
Snd: Solange nur industrielle Prozesse mit Softwareanteil aber nicht %(q:Programme als solche) patentierbar sind, stellt das Weiterverteilen der Software keine Patentverletzung dar.
Isd: In dem Moment, wo dies sich ändert, können Distributoren wegen Patentverletzung belangt und auf %(q:entgangene Gewinne) verklagt werden.
Eti: Eine Firma wie SuSE, die Tausende von Programmen verteilt, müsste, um sich gegen solche Rechtsrisiken abzusichern, Millionen von Algorithmen mit Hundertausenden von Patenten vergleichen und auf Verletzungen absuchen.  Da dies unmöglich ist und man gerade in Logikalien-Angelegenheiten nicht den geringsten Anlass hat, auf Verständnis bei der Patentjustiz zu hoffen, steht eine Distributionsfirma immer mit einem Bein im Gefängnis.
ktl: kleine bis mittlere Urheber[firmen] komplexer Systeme
Fds: Firmen wie Netpresenter haben einerseits nicht die Mittel, um die zum Alltagsgeschäft der Programmierung entstehenden Neuerungen patentrechtlich zu nutzen.  Andererseits müssen sie ständig damit rechnen, bei ihren täglichen Neuerungen auf Tretminen zu stoßen.  Einige Europäische Firmen dieses Typs wie z.B. Netpresenter (Holland), MySQL (Schweden), Infomatec (Deutschland), Sniff (Österreich), die durchaus nicht nur dem Linux-Umfeld entstammen, haben große Angst vor dem Patentsystem.  Sie müssen tatsächlich fürchten, dass ihnen angesichts der Schutzgeld-Forderungen aus zehntausenden von Patenten schnell die Luft ausgeht.
meq: manche Großfirmen
Sge: Selbst Firmen mit großem Patentportolio profitieren nicht immer von der Existenz des Patentsystems.  Patentgegner Oracle hat sich nur zu defensiven Zwecken ein großes Patentportfolio angelegt.  D.h. Oracle musste viel Geld ausgeben, um sich vor Gefahren zu schützen, die sonst nicht existiert hätten.  Die auf Patente gegründete Firma OpenMarkets begründete 1999 umfangreiche Personal-Entlassungsmaßnahmen u.a. mit Fehlinvestitionen in Patente.  Die umfangreiche Patentesammlung hatte zu verlustreichen Prozessen mit anderen Firmen, u.a. mit IBM, geführt.
Uej: Unbeteiligte Branchen, der Verbraucher, die Öffentlichkeit
Dod: Durch Patente erhöhen sich die allgemeinen Verwaltungs- und Transaktionskosten.  Viele Unternehmen geben bereits heute 10-30% ihrer Einkünfte für den Rechtsetat aus.  Das schlägt selbstverständlich auf die Preise durch.
nnd: Über den Umweg der Softwarepatente werden Geschäftsmethoden aller Branchen patentierbar, und allerlei bisher unverfängliche Handlungen werden als %(q:Beihilfe zur Patentverletzung) Gegenstand von Abmahnungen.
HkW: Hinzu kommt die Zerstückelung der bisher relativ freien informationellen Infrastruktur und die Zerstörung freier Software, auf deren Existenz das Internet und das WWW aufbaut und von der unzählige Unternehmen in kaum schätzbarem Maße profitieren.
Svg: Sind die Patentämter ihren Aufgaben gewachsen?
NtA: NEIN, meint Gregory Aharonian.
Bst: BALD, versprechen Softwarepatent-Befürworter seit vielen Jahren.  U.a. schreiben Betten und Esslinger in %(q:Computerrecht) 2000/1:
EgW: Ein Problem stellt die schwierige Recherchierbarkeit von Softwareerfindungen dar, da in den Datenbanken der Patentämter in diesem vergleichsweise jungen Gebiet noch nicht sehr viel leicht recherchierbare, klassifizierte Patentliteratur vorhanden ist.  Dies hat zur Erteilung von zu breiten Patenten auf dem Softwaresektor geführt und wird sich aber spätestens dann ändern, wenn ein größerer Fundus an klassifizierter Patentliteratur vorhanden ist.
PWd: Patentprüfer sehen sich täglich gezwungen, triviale Erfindungen zur Patentierung zuzulassen.  Es gelingt nämlich nicht, die zuvor veröffentlichte Technik ausfindig zu machen.  Patententhusiasten versprechen immer wieder, dass es sich hierbei nur um ein zeitweiliges Übergangsphänomen handelt, aber Gregory Aharonians Studien zeigen deutlich, dass die Qualität der Patentprüfungen eher sinkt als steigt, und dass auch zur Abhilfe gegründete Institutionen wie das %{SPI} ihren Auftrag verfehlt haben.
Dtt: Das Problem ist auch grundsätzlicher Natur:
BCd: Barriere zwischen Computersprache und Recherche-Sprache
Cse: Computerprogramme dokumentieren sich selbst.  Sie sind als solche bereits eine Form der Information und enthalten somit ihre eigene Dokumentation.   Besonders vollwertige Systeme wie z.B. das Drucksatzsystem TeX sind oft nach dem Prinzip der %(q:literarischen Programmierung) verfasst.  D.h. die Dokumentation ist mit dem Programm verwoben und aus ihm erzeugbar.  Bei weniger vollwertigen Systemen ist die Dokumentation nicht getrennt erzeugbar und daher für Patentrecherchen kaum zugänglich.  Bei proprietärer Software ist die Bauplan-Information dem Patentrechercheur vollends verschlossen.  Dennoch gehört auch diese Software zum Stand der Technik, denn der %(q:in der Kunst geübte Fachmann) kann sie dekompilieren, egal ob das erlaubt ist oder nicht.
Fpr: Fachliteratur spielt eine untergeordnete Rolle
Wlr: Weil bei Logikalien keine Grenze zwischen %(q:technischer Information) und %(q:gewerblicher Anwendung) gezogen werden kann (s.o.), spielen auch Fachzeitschriften nicht die Rolle, die sie bei den klassischen Natur- und Ingenieurwissenschaften spielen.  Für Patentliteratur gilt dies a fortiori.  Denn ein Großteil der Neuerungen wird weiterhin außerhalb des Patentwesens stattfinden.
Vhn: Verpassen die Europäer Chancen?
Lda: Laut Informationen des EPA befanden sich 1997 ca 75% der (gesetzeswidrig) angemeldeten europäischen Softwarepatente im Besitz von amerikanischen und japanischen Firmen.  In den USA liegt diese Zahl bei 90%.
Hcn2: Hierüber machen sich diejenigen Patentexperten, die diese Entwicklung maßgeblich verursacht haben, Sorgen.  Die EU plant eine Patentaufklärungskampagne.  Ein führender Patentinflationstreiber warnt im Rundschreiben an seine Mandanten:
Dfa: Durch die .. Entwicklung der Rechtsprechung .. hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, das die deutschen Dienstleistungsunternehmen ... insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten.
Vnt: Vermutlich würde die europäische Softwarebranche tatsächlich einiges mehr an Patenten anmelden, wenn sie nicht nur in den USA sondern auch auf dem heimischen Markt durch das Vorhandensein eines allumfassenden Patentsystems dazu gezwungen würde.
Dze: Dies ist aber von der Frage zu trennen, ob wir ein solches System auch haben wollen.
Iei: Im internationalen Vergleich nutzt ein solches System immer eher denjenigen,deren Industrie weiter entwickelt ist.  Die USA wurden erst in dem Moment zum Vorreiter der Patentinflation, als sie selbst Spitzenpositionen in der %(q:Neuen Wirtschaft) bereits besetzt hatten.  

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: swpatgirzu
# passwd: XXXX
# srcfile: /ul/sig/srv/res/www/ffii/swpat/penmi/bmwi-20000518/jinvi/eude/eudede.txt
# feature: swpatdir
# doc: swpbpuseu
# txtlang: de
# coding: utf-8
# End:
