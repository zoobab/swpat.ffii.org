\contentsline {subsection}{\numberline {0.1}Wo wirken Softwarepatente aufbauend?}{2}{subsection.0.1}
\contentsline {subsubsection}{\numberline {0.1.1}Firmen mit gro{\ss }en Patentportfolios.}{2}{subsubsection.0.1.1}
\contentsline {subsubsection}{\numberline {0.1.2}Firmen, deren Gesch\"{a}ft auf wenigen patentierbaren Ideen f\"{u}r einen neuen Markt beruht}{2}{subsubsection.0.1.2}
\contentsline {subsubsection}{\numberline {0.1.3}Unternehmen hochspezialisierter technischer Erfinder}{2}{subsubsection.0.1.3}
\contentsline {subsubsection}{\numberline {0.1.4}Patentberater, Patentmakler}{3}{subsubsection.0.1.4}
\contentsline {subsubsection}{\numberline {0.1.5}Patent\"{a}mter und Patentanw\"{a}lte}{3}{subsubsection.0.1.5}
\contentsline {subsection}{\numberline {0.2}Wo wirken Softwarepatente zerst\"{o}rerisch?}{3}{subsection.0.2}
\contentsline {subsubsection}{\numberline {0.2.1}Freie Software, Informationelle Infrastruktur}{3}{subsubsection.0.2.1}
\contentsline {subsubsection}{\numberline {0.2.2}Distributoren von freier Software und Shareware}{4}{subsubsection.0.2.2}
\contentsline {subsubsection}{\numberline {0.2.3}kleine bis mittlere Urheber[firmen] komplexer Systeme}{4}{subsubsection.0.2.3}
\contentsline {subsubsection}{\numberline {0.2.4}manche Gro{\ss }firmen}{4}{subsubsection.0.2.4}
\contentsline {subsubsection}{\numberline {0.2.5}Unbeteiligte Branchen, der Verbraucher, die \"{O}ffentlichkeit}{5}{subsubsection.0.2.5}
\contentsline {subsection}{\numberline {0.3}Sind die Patent\"{a}mter ihren Aufgaben gewachsen?}{5}{subsection.0.3}
\contentsline {subsubsection}{\numberline {0.3.1}Barriere zwischen Computersprache und Recherche-Sprache}{5}{subsubsection.0.3.1}
\contentsline {subsubsection}{\numberline {0.3.2}Fachliteratur spielt eine untergeordnete Rolle}{6}{subsubsection.0.3.2}
\contentsline {subsection}{\numberline {0.4}Verpassen die Europ\"{a}er Chancen?}{6}{subsection.0.4}
