\begin{subdocument}{swpbplibr}{Wirtschaftspolitische Bedeutung der Freien Software}{http://swpat.ffii.org/treffen/2000/bmwi0518/jinvi/libr/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Erstmals fragt eine europ\"{a}ische Regierungsorganisation \"{o}ffentlich nach den Auswirkungen geplanter Gesetzes\"{a}nderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII m\"{o}chte dazu beitragen, dass diese \"{u}berf\"{a}lligen Fragen mit dem geb\"{u}hrenden Ernst studiert werden, bevor man einschl\"{a}gige Gesetze und Vertr\"{a}ge \"{a}ndert.}
\begin{sect}{open}{Ausgleich von Unzul\"{a}nglichkeiten des propriet\"{a}ren Modells}
Unter ``Freie Software'' versteht man ``vollwertige Logikalien'', d.h. logische Komponenten eines Computersystems, die nicht nur funktionieren, sondern auch \"{u}berpr\"{u}ft, verbessert, angepasst und erweitert werden k\"{o}nnen, kurzum alle n\"{u}tzlichen Merkmale aufweisen, die man von einer Logikalie erwartet.  Im Gegensatz zur ``Freien Software'' ist die ``propriet\"{a}re'' Software in ihrer N\"{u}tzlichkeit kompromittiert.  Um der besseren Vermarktbarkeit willen hat ihr Urheber sich die Logikalie zu einer virtuellen Physikalie herabgestuft, die, \"{a}hnlich wie eine Maschine f\"{u}r Au{\ss}enstehende uneinsehbar ist und nur in einer vom Hersteller vorgegebenen Weise funktioniert.

Jede Wirtschaftspolitik muss sich fragen, was sie eigentlich f\"{o}rdern will.  Sollen m\"{o}glichst viele vollwertige Logikalien entstehen?  Oder m\"{o}glichst viele vermarktbare Waren (virtuellen Physikalien)?  Oder vielleicht eine Mischung aus beiden: ein System, was den allm\"{a}hlichen \"{U}bergang von virtuellen Physikalien in vollwertige Logikalien f\"{o}rdert?

Es hat sich gezeigt, dass im Bereich des ``Kathedralenbaus'', d.h. der Programmierung von komplexen Einzelsystemen, die nur wenige Schnittstellen nach au{\ss}en aufweisen m\"{u}ssen (z.B. Branchensoftware, Spracherkennung), virtuelle Physikalien (propriet\"{a}re Software) gute Arbeit leisten und aufgrund ihrer guten Vermarktbarkeit relativ nachhaltig weiterentwickelt werden.  Die vollwertigen Logikalien (freie Software) hingegen weisen insbesondere im Bereich der Kommunikationsnetze und der modularen Systeme \"{u}berlegene Qualit\"{a}ten auf und werden dort von den interessierten Fachkreisen weiterentwickelt.

Allerdings l\"{a}sst sich das Territorium des kommerziellen Kathedralenbaus schwer von dem der vollwertigen Logikalien abgrenzen.  Alle informatischen Aufgabenstellungen neigen dazu, sich im Laufe der Zeit zunehmend in Module und Kommunikationsschnittstellen zu unterteilen.  So ist es erkl\"{a}ren, dass ein Gebiet nach dem anderen von vollwertigen Logikalien (freier Software) erschlossen wurde.  Oft spielt der propriet\"{a}re ``Kathedralenbau'' lediglich den Vorreiter in einer Anfangszeit, in der einem hohen kommerziellen Anreiz eine geringer technologischer Entwicklungsstand gegen\"{u}bersteht.

Es hat sich gezeigt, dass die Wertsch\"{o}pfung bei den virtuellen Physikalien nicht nach echten marktwirtschaftlichen Spielregeln funktioniert.  Was manche die ``Neue \"{O}konomie'' nennen, k\"{o}nnte man auch als ``virtuellen Marktwirtschaft'' beschreiben.  Es wird n\"{a}mlich nicht echte physikalische Ware gegen Geld getauscht, sondern es wird mit der Herrschaft \"{u}ber Schnittstellen und Kundenbindungen gehandelt.  Wenn z.B.  Siemens ein Abkommen mit Microsoft schlie{\ss}t, bindet Siemens damit indirekt sehr viele unbeteiligte Dritte in eine st\"{a}rkere Abh\"{a}ngigkeit der Microsoft-Platform.  Im Gegenzug erh\"{a}lt Siemens billige Lizenzen und Dienste von Microsoft.  Auf seine Kosten kommt Microsoft wiederum bei den unbeteiligten Dritten.  In der IT-Welt spricht man bei solchen Vertr\"{a}gen daher auch nicht von K\"{a}ufen sondern von ``strategischen Allianzen''.

\"{A}hnlich virtuell sind nicht nur die Tauschgesch\"{a}fte zwischen gro{\ss}en Spielern wie Siemens und Microsoft, sondern auch die Beziehungen zwischen kleinsten Marktteilnehmern.  Banken verheimlichen ihren Kunden die Tatsache, dass sie den HBCI-Standard unterst\"{u}tzen, und empfehlen stattdessen eine kundenbindende inkompatible L\"{o}sung.  Hardware-Hersteller verraten niemandem, dass ihr System eine offene Schnittstelle hat und auch unter Linux l\"{a}uft.  Standardisierungsbem\"{u}hungen liegen ebensowenig wie Vollwertigkeit im Interesse des ``Kommerzes''.  Alles, was eine virtuelle Physikalie in Richtung auf eine Vollwertigkeit / Logikalit\"{a}t weiterentwickeln k\"{o}nnte, mindert die Herrschaft \"{u}ber Schnittstellen und Kundenbindungen und wird daher so lange hinausgez\"{o}gert, bis die technologische Entwicklung durch nichts mehr aufzuhalten ist.

Der zentrale Tauschgegenstand in der ``virtuellen Marktwirtschaft'' ist nicht das ``Produkt'' (Software-Glanzkarton), sondern die Verf\"{u}gungsgewalt \"{u}ber Infrastrukturen, die von unbeteiligter Dritten um der Kompatibilit\"{a}t willen verwendet werden m\"{u}ssen.  Aus dieser Macht wird die Wertsch\"{o}pfung erzeugt.  Gleichzeitig l\"{a}uft diese Macht aber den Interessen der Technologie und ihrer Anwender zuwider.  F\"{u}r Logikalien gilt, etwas \"{u}berspitzt gesagt: Was technologisch gesehen ``vollwertig'' ist, ist wirtschaftlich gesehen ``wertlos'' -- und umgekehrt.

Technologie und Wertsch\"{o}pfung stehen in einem spannungsreichen aber nicht unvers\"{o}hnlichen Verh\"{a}ltnis zueinander.   Unter dem Leidensdruck des sprichw\"{o}rtlichen ``Kommerzes'' entstand die Freie Software.  Sie wies einen Ausweg, der schlie{\ss}lich auch gro{\ss}e Teile der vom ``Kommerz''-Kartell ins Abseits gedr\"{a}ngten Gesch\"{a}ftswelt faszinierte.  Zahlreiche Ans\"{a}tze zum besseren Zusammenleben von IT und Kommerz sind entstanden.  Die Wirtschaftspolitik ist nun gefordert, ein \"{u}briges dazu zu tun, dass unproduktive Gegens\"{a}tze \"{u}berbr\"{u}ckt und langfristig immer mehr m\"{o}glichst vollwertige Logikalien entwickelt werden k\"{o}nnen.

Lekt\"{u}re
\begin{itemize}
\item
Richard Stallman: Why Free?\footnote{http://www.gnu.org/philosophy/why-free.html}

\item
OpenSource.ORG\footnote{http://www.opensource.org}
\end{itemize}
\end{sect}

\begin{sect}{seku}{Zug\"{a}nglichkeit von Infrastrukturen und \"{o}ffentliche Sicherheit}
Bei jedem \"{o}ffentlich ausgeschriebenen Grossprojekt, etwa beim Kraftwerks- oder Stra{\ss}enbau m\"{u}ssen neben dem fertigen Produkt dem Auftraggeber auch umfangreiche Dokumentation zur Wartung des Produkts mitgeliefert werden. Diese umfasst nicht nur Bedienungsanleitungen, sondern auch Baupl\"{a}ne und Konstruktionen des Projektes. Allgemein werden die Offenlegungs- und Dokumentationsanforderungen mit wachsender Komplexit\"{a}t eines Projektes gr\"{o}{\ss}er. Sofern in diesen Projekten sicherheitsrelevante Software eingesetzt wird, muss auch deren Qualit\"{a}t sichergestellt werden.

Auf der anderen Seite waren Sicherheitsanforderungen an den Personalcomputer, wie er seit Ende der 70er Jahre auf den Markt kam, sehr gering, da der Schaden bei einem Ausfall (etwa bei einem Privatanwender) \"{u}berschaubar war. Neben Weiterentwickung der Hardware verwandelte schliesslich die Netzwerkrevolution (HTML seit 1992) die urspr\"{u}nglichen Spielekonsolen in Workstations, die aus fast keinem Betrieb mehr wegzudenken sind - aus dem Gameboy wurde auf einmal Infrastruktur.

Wenn aber statt Pingpongb\"{a}llen private und Beh\"{o}rdendaten, diplomatische Noten, Gesch\"{a}ftsvertr\"{a}ge und vielleicht bald Volksabstimmungen und Wahlstimmen ausgetauscht werden, verursachen schon kleine Sicherheitsl\"{o}cher hohe Kosten und selbst Virenwarnungen (Melissa, I love you) machen Schlagzeilen.

Auch quellenoffene Software ist nicht frei von Fehlern und Sicherheitsl\"{u}cken, hier haben aber alle Benutzer im Prinzip die gleichen Chancen, diese zu finden und sind dabei nicht dem Wohlwollen eines Herstellers (oder eines mit diesem paktierenden Geheimdienstes) ausgeliefert.
\end{sect}

\begin{sect}{okup}{Direkte und indirekte Wirkungen auf Bildung und Besch\"{a}ftigung}
Freie Software erm\"{o}glicht informationstechnisch interessierten jungen Leuten einen direkten Zugang zu den Quellen erstklassiger Programmierkenntnisse.  Ihre ungehinderte Entfaltung tr\"{a}gt viel mehr zu Bildung, Wissenschaft und Wohlstand bei als durch sie an direkter privatwirtschaftlicher Wertsch\"{o}pfung entsteht.  Es w\"{a}re eine interessante Aufgabe, diesen Beitrag hochzurechnen.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

