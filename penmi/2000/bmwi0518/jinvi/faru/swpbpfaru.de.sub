\begin{subdocument}{swpbpfaru}{Was sollte die Bundesregierung tun?}{http://swpat.ffii.org/termine/2000/bmwi0518/jinvi/faru/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Erstmals fragt eine europ\"{a}ische Regierungsorganisation \"{o}ffentlich nach den Auswirkungen geplanter Gesetzes\"{a}nderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII m\"{o}chte dazu beitragen, dass diese \"{u}berf\"{a}lligen Fragen mit dem geb\"{u}hrenden Ernst studiert werden, bevor man einschl\"{a}gige Gesetze und Vertr\"{a}ge \"{a}ndert.}
\begin{sect}{stud}{Studieren, Dokumentieren, Diskussion vertiefen}
Selbst f\"{u}hrende ``Meinungsbildner'' in der EP\"{U}-Diskussion, etwa das europ\"{a}ische Patentamt, schwanken noch \"{u}ber konkrete \"{A}nderungen des EP\"{U}. Waehrend das EPA im M\"{a}rz 1999 eine v\"{o}llige Streichung der Ausnahmeregeln in Art 52 (2) (2) und (3) forderte (Esslinger, Computerrecht 1/2000, 17), so ist im Amtsblatt vom August d.J. nur noch von einer Streichung der ``Computerprogrammausnahmen'' die Rede.

Andererseits ist auch gar nicht bekannt, wie weit der Konsens ueber die schleichende Ausweitung des Technikbegriffs innerhalb der Patent\"{a}mter wirklich geteilt wird - so wurden in den letzten 15 Jahren z.B. zum Thema Artikel 52(2) Softwarepatentierbarkeit alle oft-zitierten Entscheidungen der technischen Beschwerdekammer des Europ\"{a}ischen Patentamtes von einem Vorsitzenden und einer handvoll Beisitzer verabschiedet.

Sofern \"{o}ffentliche Diskussionen stattfanden, beschr\"{a}nkten diese sich auf den Welt der Patentfachleute.  Weder drangen diese Debatten in die Welt der Informatiker vor, noch hatten die Patentfachleute jemals Gelegenheit, sich in die Andersartigkeit der entstehenden Logikalien\"{o}konomie hineinzudenken.  Die Schlagworte ``Linux'' und ``OpenSource'' drangen erst in dem Moment an ihre Ohren, als die Pl\"{a}ne der Patentfachleute bereits feste Formen angenommen hatten.

W\"{a}hrend 1997 viele Patentfachleute noch guten Gewissens von einem Konsens ausgehen konnten und deshalb die ``auf allen Kan\"{a}len'' betriebene Arbeit nicht n\"{a}her in Frage stellen zu m\"{u}ssen glaubten, ist es sp\"{a}testens heute Zeit, innezuhalten und die Folgen zu studieren.

Nach Erhalt weiteren Datenmaterials sollte das Thema Softwarepatente im Bundestag ebenso wie auch im Europ\"{a}ischen Parlament zur erneuten Debatte gestellt werden.

\begin{sect}{priodat}{Europ\"{a}er f\"{u}r die Patentschlacht in US+JP r\"{u}sten: Stand der Technik systematisch aufarbeiten }
Auch wenn sich Europa, wie wir hoffen, f\"{u}r eine freie informationelle Infrastruktur entscheidet, wird die europ\"{a}ische Softwarebranche sich in den USA und anderen L\"{a}ndern zumindest einige Jahre lang h\"{a}ufig mit Patentanspr\"{u}chen auseinandersetzen m\"{u}ssen.  Hierbei k\"{o}nnte eine leistungsf\"{a}hige und vollwertige (d.h. frei zug\"{a}ngliche und weiterverwendbare) \"{o}ffentliche Datenbank \"{u}ber den Stand der Technik gute Dienste leisten. 

Die Softwarepatent-Arbeitsgruppe des FFII liefert regelm\"{a}{\ss}ig Datentr\"{a}ger an die Bayerische Staatsbibliothek, mit denen Entwicklern freier Software geholfen werden soll, im Falle einer Patentklage den Zeitpunkt ihrer Ideen nachzuweisen.  Das hilft allerdings nur denjenigen, die genau wissen, f\"{u}r welche Erfindungen sie einen Zeitnachweis brauchen.  Ziel muss es sein, den aktuellen und fr\"{u}heren Stand der Technik f\"{u}r alle transparent abfragbar zu machen.  Hierzu k\"{o}nnte man ein dezentrales System von Rechnern aufbauen, die sich gegenseitig entweder Plattenplatz oder Geld anbieten.  Manches lie{\ss}e sich sogar kommerziell organisieren.

Ein solches Projekt k\"{o}nnte in einem neuartigen internet-gerechten Verfahren der \"{o}ffentlichen Ausschreibung entstehen.  In dieses Verfahren w\"{a}ren sowohl freie Entwickler als auch Firmen einzubeziehen.  Alle abzuliefernden Ergebnisse m\"{u}ssten vollwertige Logikalien sein.  Dort, wo sich f\"{u}r einen aufwendigen Projektteil keine Freiwilligen finden, k\"{o}nnten kleine bis mittelgro{\ss}e Dienstleistungsfirmen nach Ausschreibung einzelne Projektteile \"{u}bernehmen.
\end{sect}

\begin{sect}{umfrag}{Systematische Meinungsforschung}
Sinnvoll w\"{a}re vermutlich auch eine von der Bundesregierung in Auftrag gegebene Studie, auf der sich interessierte Kreise f\"{u}r oder gegen Softwarepatente aussprechen k\"{o}nnen (eventuell Neuauflage der unver\"{o}ffentlichten Umfragen von Bernhard M\"{u}ller (Computerrecht 1/2000).

Diese Studie sollte sich allerdings nicht nur an 44 Patentrechtsexperten sondern vor allem an die betroffenen Kreise wenden.  Die darin gestellten Fragen sollten vielseitiger sein und sich in einem dynamischen Dialog \"{u}ber WWW-Umfrageformulare entwickeln.  Einen einsatzbereiten Prototyp eines solchen Systems mitsamt Fragenvorschl\"{a}gen hat die Softwarepatent-Arbeitsgruppe des FFII bereits entwickelt.
\end{sect}

\begin{sect}{efekt}{Systematische Erforschung der Wirtschaftlichen Auswirkungen}
In unserem Positionspapier konnten wir nur einige m\"{o}gliche Denkwege aufzeigen, aber viele unserer Antworten auf die aufgeworfenen Fragen befinden sich auf dem Niveau von Mutma{\ss}ungen.

Es w\"{a}re sinnvoll, interessierte Wirtschaftswissenschaftler in die Diskussion einzubeziehen und dabei sowohl die Fragestellungen zu verfeinern als sie durch repr\"{a}sentative Datenerhebung zu beantworten.  Auch hierbei k\"{o}nnte das von uns angeregte internet-gest\"{u}tzte Umfragesystem gute Dienste leisten.  Ferner k\"{o}nnten sofort weitere Konferenzen anberaumt werden.

Ferner sollte auch eine ernsthafte Diskussion \"{u}ber m\"{o}gliche Sui-Generis-Alternativen zum Patentrecht in den F\"{a}llen nachgedacht werden, wo besondere Vorrechte auf Iden wirtschaftspolitisch erw\"{u}nscht sein k\"{o}nnen, wie etwa bei MPEG.   Hierbei lie{\ss}e sich bei der 1994 in Columbia Law Review begonnenen Diskussion um ``Manifesto Concerning the Legal Protection of Computer Programs'' ansetzen.  Diese Diskussion wurde damals nur deshalb beendet, weil die Patentlobby in Amerika bereits vollendete Tatsachen geschaffen hatte.
\end{sect}
\end{sect}

\begin{sect}{epue}{EP\"{U}-Revision abblasen und Rechtslage kl\"{a}ren}
Die Empfehlungen der letzten Regierungskonferenz sind nur ``Empfehlungen'', aber in keiner Weise bindend, und k\"{o}nnen im November 2000 von der n\"{a}chsten Regierungskonferenz begr\"{u}ndet verworfen werden. Die Bundesregierung sollte ihre Vertreter auf ein Negativvotum zur \"{A}nderung von Art 52(2) binden und diese Absicht auch rechtzeitig anderen Mitgliedsstaaten mitteilen.

Gleichzeitig k\"{o}nnte die Bundesregierung die deutsche Patentjustiz wegen ihrer Rechtsbeugung r\"{u}gen und mit einer unmissverst\"{a}ndlichen Richtlinie auf den Kurs zur\"{u}ckverweisen, der nicht nur im Gesetz sondern auch in den Pr\"{u}fungsrichtlinien bis heute eigentlich schon recht unmissverst\"{a}ndlich vorgeschrieben ist:
\begin{quote}
2.6.2. Dem Patentschutz nicht zug\"{a}ngliche Anmeldungsgegenst\"{a}nde

Ein Anmeldungsgegenstand stellt nur dann im Sinne des \S{} 42 PatG seinem Wesen nach eine Erfindung dar, wenn er in einer Anweisung besteht, Kr\"{a}fte, Stoffe oder in der Natur vorkommende Energien zur unmittelbaren Herbeif\"{u}hrung eines kausal \"{u}bersehbaren, wiederholbaren Erfolgs zu benutzen, und wenn dieser Erfolg ohne Zwischenschaltung menschlicher Verstandest\"{a}tigkeit unmittelbare Folge des Einsatzes der Naturkr\"{a}fte ist.

Erf\"{u}llt ein Gegenstand die beschriebenen Voraussetzungen nicht oder handelt es sich um einen der in \S{} 1 Abs. 2 PatG genannten Gegenst\"{a}nde, ist die Anmeldung zu beanstanden.
\end{quote}
Hieraus ergibt sich, dass nicht eine Logikalie (Computerprogramm als solches), wohl aber eine programmgest\"{u}tzte Physikalie (z.B. Industrieprozess, MP3-Abspielger\"{a}t), die ``Kr\"{a}fte, Stoffe oder in der Natur vorkommende Energien'' benutzt, Gegenstand von Patentanspr\"{u}chen sein kann.  Patente sollten nicht verwendet werden, um das Kopieren von Information (und letztlich auch sonstiges nicht-industrielles Duplizieren, wie z.B. die Vermehrung von Leben) zu untersagen.
\end{sect}

\begin{sect}{dleg}{Rechtsg\"{u}ter wie Kompatibilit\"{a}t, Quellenoffenheit etc durch getrennte Gesetze gegen Patentgefahren absichern}
Die oben angeregten Kl\"{a}rungsdirektiven des BMJu k\"{o}nnten auch als eigenst\"{a}ndige Gesetze formuliert werden.  Durch ein nationales Gesetz k\"{o}nnten auf diese Weise zumindest verhindert werden, dass die Patentjustiz sich an den Entwicklern freier Software vergreift, wie es die oben zitierten Patentanw\"{a}lte Esslinger und Betten unter ``Softwarepatente und ``Open Source''-Bewegung'' in Aussicht stellen:

\begin{quote}
Au{\ss}erdem erstreckt sich nach \S{}11 PatG die Wirkung des Patents nicht auf Handlunben, die im privaten Bereich zu nicht gewerblichen Zwecken vorgenommen werden, sowie Handlungen zu Versuchszwecken, die sich auf den Gegenstand der patentierten Erfindung beziehen.  Der Programmierer, der zu Hause und ohne kommerzielle Absicht ein Programm schriebt, braucht auf eventuellen Patentschutz nicht zu achten.  Dies \"{a}ndert sich jedoch, wenn die Privatsph\"{a}re verlassen wird, ein Programm also etwa kostenlos im Internet zum Herunterladen angeboten wird.
\end{quote}

Hier k\"{o}nnte der deutsche Gesetzgeber klarstellen, dass das ``Anbieten zum Herunterladen'' von vollwertigen Logikalien, d.h. Programmen mit einer OpenSource-konformen Lizenz, nicht als ``gewerblich'' im Sinne des Patentrechts zu betrachten ist.  Die Freiheit der Informationsweitergabe soll Vorrang vor den Verwertungsanspr\"{u}chen der Vermarkter konkurrierender propriet\"{a}rer Software haben.  Eine solche Regelung k\"{o}nnte sogar dann Bestand haben, wenn die Europ\"{a}ische Patentorganisation ihre Reformpl\"{a}ne durchsetzen sollte.

Ferner sollte in Deutschland das Recht auf Kompatibilit\"{a}t, wie es von der oben zitierten EU-Direktive von 1991 im Ansatz vorgesehen ist, Vorrang vor Patentanspr\"{u}chen erhalten, ebenso wie es bereits heute Vorrang vor aus dem Urheberrecht abgeleiteten Dekompilierungsverboten u.\"{a}. hat.  Auch dies w\"{a}re mit einem getrennten Gesetz zu kl\"{a}ren.  Dabei k\"{o}nnte die laufende Gesetzesinitiative einiger franz\"{o}sischer Parlamentarier\footnote{http://www.osslaw.org/articles.html} zu Rate gezogen werden.

Schlie{\ss}lich k\"{o}nnten Patent\"{a}mter verpflichtet werden, auf Anfrage verbindliche Ausk\"{u}nfte dar\"{u}ber zu erteilen, ob ein bestimmtes Programm ein Patent verletzt.  Dieser Auskunftsdienst sollte zumindest im Falle von Freier Software unentgeltlich geleistet werden, wie Brian Behlendorf, f\"{u}hrender Entwickler des weltweit f\"{u}hrenden WWW-Servers in \emph{Open Sources - Voices from the OpenSource Revolution, O'Reilly, 1999, S.166} begr\"{u}ndet:
\begin{quote}
[The Mozilla Public License] has several provisions protecting both the project as a whole and its developers against patent issues in contributed code. ...

Taking care of the patent issue is a Very Good Thing.  There is always the risk that a company could innocently offer code to a project, and then once that code has been implemented thoroughly, try and demand some sort of patent fee for its use.  ...

Of course it doesn't block the possibility that someone \emph{else} owns a patent that would apply; there is no legal instrument that does provide that type of protection.  I would actually advocate that this is an appropriate service for the USPTO to perform; they seem to have the authority to declare certain ideas or algorithms as property someone owns, so shouldn't they also be required to do the opposite and certify my submitted code as patent-free, granting me some protection from patent lawsuits?
\end{quote}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

