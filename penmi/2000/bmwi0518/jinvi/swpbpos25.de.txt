<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

AeB: Anregungen des FFII zur BMWi-Konferenz
Eht: Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert.
Wih: Im Rundschreiben an ihre Mandanten vom Januar 2000 verkündet eine Münchener Patentanwaltskanzlei erregende Neuigkeiten:
Neg: Neue BGH-Entscheidung ermöglicht Patentierung von Computerprogrammen
IWo: In einer noch unveröffentlichten Entscheidung vom 13. Dezember 1999 hat der Bundesgerichtshof nun endlich die Gelegenheit ergriffen und Softwareerfindungen, denen technische Überlegungen zu Grunde liegen, für patentfähig erklärt.  Damit hat der BGH mit der Entscheidungspraxis des Europäischen Patentamtes gleichgezogen und das Tor für die Erteilung von Softwarepatenten auch durch das Deutsche Patent- und Markenamt weit geöffnet.  Da unter diese technischen Überlegungen auch programmtechnische Gedanken wie etwa die Verringerung der Rechenzeit, die Einsparung von Speicherplatz usw fallen, sind in der Praxis fast alle Programme dem Patentschutz zugänglich, sofern sie neu sind und auf einer erfinderischen Tätigkeit beruhen. ...
Pna: Patentierung von Geschäftsverfahren in den USA
DWW: Die neueste Entwicklung geht dahin, dass Geschäftsverfahren sogar unabhängig von einem Computerprogramm patentiert werden.
Ken: Konsequenzen
Dtr: Durch die oben skizzierte Entwicklung der Rechtsprechung sowie die zunehmende Internationalisierung des Geschäftslebens hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten. ...
ccn: %(q:Das Patentrecht) hat %(q:sich) von den Vorgaben des Gesetzgebers gelöst.
UWn: Unsere Frage ist:  Gibt es für diese %(q:Loslösung des Patentrechts) vernünftige wirtschaftspolitische Gründe?
DnW: Diese Frage wurde von %(q:dem Patentrecht) selber nie beantwortet.  Die Loslösung wurde, wie wir unten aufzeigen, von einer Gruppe politisch aktiver Patentrechtler vorangetrieben.  Ein breiter Konsens innerhalb der beteiligten Kreise erübrigte in den Jahren 1991-97 die wirtschaftspolitischen Untersuchungen, die wir heute anstoßen wollen:
WSg: Wie funktioniert der Softwaremarkt?  Was für Entwicklungen sollte die Wirtschaftspolitik dabei fördern?  Funktioniert das Patentwesen heute bestimmungsgemäß?  Wo wirken Softwarepatente eher aufbauend, wo eher zerstörend?  Welche Handlungsoptionen hat die Bundesregierung?
UWe: Unsere vorläufigen Untersuchungen dieser Fragen führen uns zu dem Schluss, dass sich die %(q:Loslösung des Patentrechts vom verarbeitenden Gewerbe) wirtschaftspolitisch nicht wünschenswert sein kann, und dass sie überdies gesetzeswidrig ist und zu einer selbstverschuldeten Begriffsverwirrung in der Patentrechtsprechung geführt hat.
Ief: Wir bitten daher die Bundesregierung, die Rechtslage zu entwirren und die von den Patentämtern angestrebten Gesetzesänderungen abzusagen.  Ferner schlagen wir geeignete Maßnahmen vor, um einige vernachlässigte Rechtsgüter vor Übergriffen durch das Patentwesen zu schützen, um die Patentprüfungspraxis zu verbessern, und um europäische Firmen für Rechtsstreitigkeiten in überseeischen Patentinflationsländern zu rüsten.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpbmwi25.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpbpos25 ;
# txtlang: de ;
# End: ;

