jinvide.lstex:
	lstex jinvide | sort -u > jinvide.lstex
jinvide.mk:	jinvide.lstex
	vcat /ul/prg/RC/texmake > jinvide.mk


jinvide.dvi:	jinvide.mk eude/eudede.tex faru/farude.tex jinvide.tex libr/librde.tex useu/useude.tex
	latex jinvide && while tail -n 20 jinvide.log | grep references && latex jinvide;do eval;done
	if test -r jinvide.idx;then makeindex jinvide && latex jinvide;fi
jinvide.pdf:	jinvide.mk eude/eudede.tex faru/farude.tex jinvide.tex libr/librde.tex useu/useude.tex
	pdflatex jinvide && while tail -n 20 jinvide.log | grep references && pdflatex jinvide;do eval;done
	if test -r jinvide.idx;then makeindex jinvide && pdflatex jinvide;fi
jinvide.tty:	jinvide.dvi
	dvi2tty -q jinvide > jinvide.tty
jinvide.ps:	jinvide.dvi	
	dvips  jinvide
jinvide.001:	jinvide.dvi
	rm -f jinvide.[0-9][0-9][0-9]
	dvips -i -S 1  jinvide
jinvide.pbm:	jinvide.ps
	pstopbm jinvide.ps
jinvide.gif:	jinvide.ps
	pstogif jinvide.ps
jinvide.eps:	jinvide.dvi
	dvips -E -f jinvide > jinvide.eps
jinvide-01.g3n:	jinvide.ps
	ps2fax n jinvide.ps
jinvide-01.g3f:	jinvide.ps
	ps2fax f jinvide.ps
jinvide.ps.gz:	jinvide.ps
	gzip < jinvide.ps > jinvide.ps.gz
jinvide.ps.gz.uue:	jinvide.ps.gz
	uuencode jinvide.ps.gz jinvide.ps.gz > jinvide.ps.gz.uue
jinvide.faxsnd:	jinvide-01.g3n
	set -a;FAXRES=n;FILELIST=`echo jinvide-??.g3n`;source faxsnd main
jinvide_tex.ps:	
	cat jinvide.tex | splitlong | coco | m2ps > jinvide_tex.ps
jinvide_tex.ps.gz:	jinvide_tex.ps
	gzip < jinvide_tex.ps > jinvide_tex.ps.gz
jinvide-01.pgm:
	cat jinvide.ps | gs -q -sDEVICE=pgm -sOutputFile=jinvide-%02d.pgm -
jinvide/jinvide.html:	jinvide.dvi
	rm -fR jinvide;latex2html jinvide.tex
xview:	jinvide.dvi
	xdvi -s 8 jinvide &
tview:	jinvide.tty
	browse jinvide.tty 
gview:	jinvide.ps
	ghostview  jinvide.ps &
aview:	jinvide.pdf
	acroread jinvide.pdf
print:	jinvide.ps
	lpr -s -h jinvide.ps 
sprint:	jinvide.001
	for F in jinvide.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	jinvide.faxsnd
	faxsndjob view jinvide &
fsend:	jinvide.faxsnd
	faxsndjob jobs jinvide
viewgif:	jinvide.gif
	xv jinvide.gif &
viewpbm:	jinvide.pbm
	xv jinvide-??.pbm &
vieweps:	jinvide.eps
	ghostview jinvide.eps &	
clean:	jinvide.ps
	rm -f  jinvide-*.tex jinvide.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} jinvide-??.* jinvide_tex.* jinvide*~
tgz:	clean
	set +f;LSFILES=`cat jinvide.ls???`;FILES=`ls jinvide.* $$LSFILES | sort -u`;tar czvf jinvide.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
