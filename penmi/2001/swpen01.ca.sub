\begin{subdocument}{swpen01}{Conferences on Software Patenting 2001}{http://swpat.ffii.org/penmi/2001/index.ca.html}{Grup de treball\\swpatag@ffii.org}{The FFII Workgroup for the Protection of Digital Innovation against Software Patents frequently participates in conferences and exhibitions.   We have presented our case at booths of Systems 2001 and Linuxtag 2001 as well as several hearings of governments and parties.  We try to document these activities.}
\begin{sect}{penmi}{Continguts}
\begin{itemize}
\item
{\bf {\bf FFII at Systems 2001-10-15..9}}

\begin{quote}
The Federation for a Free Information Infrastructure maintains a booth at the computing trade fair in Munich and presents informations on current concerns of the information economy, especially the threats arising from the extension of the patent system.
\end{quote}
\filbreak

\item
{\bf {\bf Seminar WOS Berlin 2001-10-11..13: 15 years of software patenting at the European Patent Office: the concept of ``technical invention'' in 1986, today and in the future}}

\begin{quote}
Europe is about to take major decisions on what kinds of innovations are to be patentable.  Recently several government-ordered studies on the impact of software patents have been published.  Most of these studies find that, while software patents are not helpful in promoting the progress of the software sector, they should nonetheless be legalised due to considerations of patent law doctrine.  In this workshop we give an overview of the studies and take a look especially at their legal argumentation, thereby presenting the options that the legislator has to  choose from and their implications.
\end{quote}
\filbreak

\item
{\bf {\bf Seminar Linuxtag Stuttgart 2001-07-05: 15 years of software patenting at the European Patent Office: the concept of ``technical invention'' in 1986, today and in the future}}

\begin{quote}
Around 1986 European manuals and commentaries of patent law unanimously explained that european patent law does not offer the software industry any chances of obtaining patents to protect its investments against imitators.  Some complained that copyright was not enough, but yet agreed that the only way to change this situation was through legislation.  Few noticed that the change was already underway:  it was implied in ambiguous wordings of the European Patent Office's Examination Guidelines of 1985: the door was opened by a narrow slot for ``computer programs with a technical effect'', and the ground was prepared for further loosening of the concept of technical character, in a way that, according to the legal doctrines of the time, amounted to removing all overseeable limits on patentability.  A year later, in 1986, the slot was widened into a visible breach by two decisions of the EPO's Technical Board of Appeal.  Since then, the EPO has granted more than 30000 patents for (un)technical teachings that had previously been regarded as unpatentable ``rules of organisation and calculation'' or ``computer programs as such''.  Judges and law scholars have meanwhile proposed various delimitation concepts in order to make sense of this somewhat chaotic caselaw development.  This seminar aims to assess to what extent this has been achieved or can be achieved.  We need to research into the space of innovations, as they are distributed along coordinates such as matter vs mind, laws of nature vs rules of calculation, concrete vs abstract, solution vs problem, trivial vs difficult, causal vs functional etc, and what kind of results various possible rules of delimitation tend to produce within this space.  An empirical basis for this innovation cartography is provided by the patents which the EPO has granted during the last 15 years.  A seminar on thursday 2001-07-05 at Linuxtag on the Stuttgart trade fair site and subsequent seminars, conducted by several interested associations, are expected to give rise to interesting new ideas in this intellectually intriguing and politically important interdisciplinary area.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente}}

\begin{quote}
Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte \"{O}ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver\"{o}ffentlichen hier das Gespr\"{a}chsprotokoll und die schriftlichen Eingaben.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente - Motor oder Bremse der Wirtschaft}}

\begin{quote}
Am 24. April 2001 veranstaltet das Wirtschaftswissenschaftliche Institut der Johann-Wolfgang-von-Goethe-Universit\"{a}t Frankfurt in der Aula selbiger Universit\"{a}t ein Symposium zum Thema ``Softwarepatente - Motor oder Bremse der Wirtschaft?''  Es sprechen einige Wortf\"{u}hrer der Debatte, u.a. Eric Maskin (MIT-\"{O}konom und Mitautor einer bekannten Studie zur sequentiellen Innovation), Bernd Lutterbeck (Autor einer vom BMWi in Auftrag gegebenen Studie), Harald Hagedorn (Leiter der Patentabteilung von SAP), Heinrich Mayr (Pr\"{a}sident der GI) und f\"{u}r den FFII Hartmut Pilch. 
\end{quote}
\filbreak

\item
{\bf {\bf CeBit 200103}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Enlla\c{c}os anotats}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf FFII Termine\footnote{http://offen.ffii.org/ffii/termine/}}}

\begin{quote}
Further events are being prepared on a collaborative web portal of the FFII.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: mlatex ;
% End: ;

