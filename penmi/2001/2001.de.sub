\begin{subdocument}{swpen01}{Logikpatentierung und Konferenzen 2001}{http://swpat.ffii.org/termine/2001/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die Softwarepatent-Arbeitsgruppe des FFII nimmt h\"{a}ufig an \"{o}ffentlichen Veranstaltungen und Messen teil.  U.a. haben wir auf der Systems und auf dem Linuxtag je einen Stand betrieben und sind auf einigen Anh\"{o}rungen von Parlamenten, Regierungen und Parteien aufgetreten.  Wir versuchen, diese Aktivit\"{a}ten hier zu dokumentieren.}
\begin{sect}{penmi}{Inhalt}
\begin{itemize}
\item
{\bf {\bf FFII auf Systems 2001-10-15..9}}

\begin{quote}
Der F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V. pr\"{a}sentiert 5 Tage lang an einem Stand auf der M\"{u}nchener EDV-Fachmesse Anschauungsmaterialien \"{u}ber die Informations\"{o}konomie und insbesondere die Gefahren, die von der Ausweitung des Patentwesens ausgehen.
\end{quote}
\filbreak

\item
{\bf {\bf Seminar WOS Berlin 2001-11..13: 15 Jahre Softwarepatentierung am Europ\"{a}ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft}}

\begin{quote}
In Europa stehen wichtige Entscheidungen dar\"{u}ber an, was f\"{u}r Arten von Neuerungen patentierbar sein sollen.  In letzter Zeit haben mehrere von Regierungen bestellte Studien \"{u}ber die Wirkungen des Patentwesens auf die Software-Innovation einerseits festgestellt, dass eher negative als positiven Wirkungen zu erwarten sind, andererseits aber dennoch aufgrund juristischer Argumente f\"{u}r eine Legalisierung von Softwarepatenten pl\"{a}diert.  In diesem Workshop bieten wir einen \"{U}berblick \"{u}ber die Studien und schauen insbesondere ihre juristische Argumentation an.  Dadurch soll sich ein verst\"{a}ndliches Bild der Optionen ergeben, zwischen denen der Gesetzgeber zu w\"{a}hlen hat. 
\end{quote}
\filbreak

\item
{\bf {\bf Seminar Linuxtag Stuttgart 2001-07-05: 15 Jahre Softwarepatentierung am Europ\"{a}ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft}}

\begin{quote}
Um 1986 erkl\"{a}rten Patentrechtslehrb\"{u}cher und -kommentare einhellig, das europ\"{a}ische Patentrecht biete keinerlei Spielraum, um Forderungen aus der Softwarebranche nach Patentschutz f\"{u}r ihre erfinderischen Leistungen entgegenzukommen.  Vielfach wurde beklagt, das geltende Urheberrecht biete nicht gen\"{u}gend Schutz gegen Nachahmung, aber die T\"{u}r zum Patentschutz f\"{u}r Software schien fest verschlossen.  Mit den Pr\"{u}fungsrichtlinien des Europ\"{a}ischen Patentamtes (EPA) von 1985 wurde indes unbemerkt ein Spalt f\"{u}r ``Programme mit technischem Effekt'' ge\"{o}ffnet und eine Lockerung des Technikbegriffs vorbereitet, die nach herrschender Rechtsauffassung nur einer Aufgabe jeglicher \"{u}bersehbarer Begrenzung der Patentierbarkeit gleichkommen konnte.  Ein Jahr sp\"{a}ter, 1986, wurde der Spalt durch zwei Entscheidungen der Technischen Beschwerdekammer des EPA zu einer sichtbaren Bresche verbreitert.  Seitdem hat das EPA mehr als 30000 Patente f\"{u}r (un)technische Lehren erteilt, die bis dahin als Organisations- und Rechenregeln oder als Programme f\"{u}r Datenverarbeitungsanlagen (als solche) abgelehnt worden w\"{a}ren.  Richter und Rechtsgelehrte haben inzwischen allerlei Abgrenzungsregeln vorgeschlagen, um diese Entwicklung erneut auf eine systematische Grundlage zu stellen und im Sinne der Innovationsf\"{o}rderung angemessen zu gestalten.  Ziel dieses Seminars ist es, zu ergr\"{u}nden, in wie weit dies gelungen ist oder gelingen kann.  Es gilt zu erforschen, wie sich der Raum der Innovationen entlang der Achsen materiell vs immateriell, Naturgesetze vs Rechenregeln, konkret vs abstrakt, L\"{o}sung vs Problem, trivial vs schwierig, kausal vs funktional usw aufteilt, und was verschiedene m\"{o}gliche Abgrenzungsregeln in diesem Raum bewirken.  Als empirische Grundlage dieser Innovationskartographie dienen die in den letzten 15 Jahren vom Europ\"{a}ischen Patentamt erteilten Patente.  Wir halten wir zun\"{a}chst ein eint\"{a}tiges Seminar am Donnerstag den 5. Juli im Rahmen des Linuxtages auf dem Stuttgarter Messegel\"{a}nde ab.  Zusammen mit weiteren Folgeveranstaltungen erhoffen sich der FFII e.V., ENEF e.V., VOV e.V. und andere Beteiligte von diesem Seminar Impulse zu wertvollen Erkenntnissgewinnen auf einem wissenschaftlich faszinierenden und politisch bedeutenden interdisziplin\"{a}ren Gebiet.
\end{quote}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente}\footnote{http://swpat.ffii.org/termine/2001/bundestag/index.de.html}}

\begin{quote}
Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte \"{O}ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver\"{o}ffentlichen hier das Gespr\"{a}chsprotokoll und die schriftlichen Eingaben.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente - Motor oder Bremse der Wirtschaft}}

\begin{quote}
Am 24. April 2001 veranstaltet das Wirtschaftswissenschaftliche Institut der Johann-Wolfgang-von-Goethe-Universit\"{a}t Frankfurt in der Aula selbiger Universit\"{a}t ein Symposium zum Thema ``Softwarepatente - Motor oder Bremse der Wirtschaft?''  Es sprechen einige Wortf\"{u}hrer der Debatte, u.a. Eric Maskin (MIT-\"{O}konom und Mitautor einer bekannten Studie zur sequentiellen Innovation), Bernd Lutterbeck (Autor einer vom BMWi in Auftrag gegebenen Studie), Harald Hagedorn (Leiter der Patentabteilung von SAP), Heinrich Mayr (Pr\"{a}sident der GI) und f\"{u}r den FFII Hartmut Pilch. 
\end{quote}
\filbreak

\item
{\bf {\bf CeBit 200103}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf FFII Termine\footnote{http://offen.ffii.org/ffii/termine/}}}

\begin{quote}
Weitere Veranstaltungen werden derzeit auf einem kollaborativen Webportal des FFII geplant
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

