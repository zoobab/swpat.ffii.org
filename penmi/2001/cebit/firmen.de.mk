firmen.de.lstex:
	lstex firmen.de | sort -u > firmen.de.lstex
firmen.de.mk:	firmen.de.lstex
	vcat /ul/prg/RC/texmake > firmen.de.mk


firmen.de.dvi:	firmen.de.mk
	latex firmen.de && while tail -n 20 firmen.de.log | grep references && latex firmen.de;do eval;done
	if test -r firmen.de.idx;then makeindex firmen.de && latex firmen.de;fi
firmen.de.pdf:	firmen.de.mk
	pdflatex firmen.de && while tail -n 20 firmen.de.log | grep references && pdflatex firmen.de;do eval;done
	if test -r firmen.de.idx;then makeindex firmen.de && pdflatex firmen.de;fi
firmen.de.tty:	firmen.de.dvi
	dvi2tty -q firmen.de > firmen.de.tty
firmen.de.ps:	firmen.de.dvi	
	dvips  firmen.de
firmen.de.001:	firmen.de.dvi
	rm -f firmen.de.[0-9][0-9][0-9]
	dvips -i -S 1  firmen.de
firmen.de.pbm:	firmen.de.ps
	pstopbm firmen.de.ps
firmen.de.gif:	firmen.de.ps
	pstogif firmen.de.ps
firmen.de.eps:	firmen.de.dvi
	dvips -E -f firmen.de > firmen.de.eps
firmen.de-01.g3n:	firmen.de.ps
	ps2fax n firmen.de.ps
firmen.de-01.g3f:	firmen.de.ps
	ps2fax f firmen.de.ps
firmen.de.ps.gz:	firmen.de.ps
	gzip < firmen.de.ps > firmen.de.ps.gz
firmen.de.ps.gz.uue:	firmen.de.ps.gz
	uuencode firmen.de.ps.gz firmen.de.ps.gz > firmen.de.ps.gz.uue
firmen.de.faxsnd:	firmen.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo firmen.de-??.g3n`;source faxsnd main
firmen.de_tex.ps:	
	cat firmen.de.tex | splitlong | coco | m2ps > firmen.de_tex.ps
firmen.de_tex.ps.gz:	firmen.de_tex.ps
	gzip < firmen.de_tex.ps > firmen.de_tex.ps.gz
firmen.de-01.pgm:
	cat firmen.de.ps | gs -q -sDEVICE=pgm -sOutputFile=firmen.de-%02d.pgm -
firmen.de/firmen.de.html:	firmen.de.dvi
	rm -fR firmen.de;latex2html firmen.de.tex
xview:	firmen.de.dvi
	xdvi -s 8 firmen.de &
tview:	firmen.de.tty
	browse firmen.de.tty 
gview:	firmen.de.ps
	ghostview  firmen.de.ps &
print:	firmen.de.ps
	lpr -s -h firmen.de.ps 
sprint:	firmen.de.001
	for F in firmen.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	firmen.de.faxsnd
	faxsndjob view firmen.de &
fsend:	firmen.de.faxsnd
	faxsndjob jobs firmen.de
viewgif:	firmen.de.gif
	xv firmen.de.gif &
viewpbm:	firmen.de.pbm
	xv firmen.de-??.pbm &
vieweps:	firmen.de.eps
	ghostview firmen.de.eps &	
clean:	firmen.de.ps
	rm -f  firmen.de-*.tex firmen.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} firmen.de-??.* firmen.de_tex.* firmen.de*~
tgz:	clean
	set +f;LSFILES=`cat firmen.de.ls???`;FILES=`ls firmen.de.* $$LSFILES | sort -u`;tar czvf firmen.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
