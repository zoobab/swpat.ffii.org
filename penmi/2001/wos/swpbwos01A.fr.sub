\begin{subdocument}{swpbwos01A}{Berlin 2000-10-12: FFII-Seminar auf WOS}{http://swpat.ffii.org/dates/2001/wos/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/08/25 par Odile BÉNASSY\footnote{http://obenassy.free.fr/}}{L'Europe se pr\'{e}pare pour des d\'{e}cisions importantes sur la question, quels sortes de innovations seront brevetables en Europe.  Dans les dernieres ann\'{e}es plusieurs \'{e}tudes sur l'impacte des brevets logiciels command\'{e}es par des agences gouvernementales ont constat\'{e} que, tandis que le brevets logiciel nuit a l'\'{e}conomie du logiciel, il doit quand-m\^{e}me \^{e}tre l\'{e}galis\'{e}.  Dans ce s\'{e}minaire nous \'{e}tudions ces \'{e}tudes et surtout leur argumentation juridique pour en obtenir un image clair des options auxquels fait face le l\'{e}gislateur et leurs implications syst\'{e}matiques.  }
\begin{sect}{kiam}{Wann?}
2001-10-12

dans le cadre de WOS 2\footnote{http://www.mikro.org/Events/OS/wos2/schedule.html}
\end{sect}

\begin{sect}{kiok}{Wo?}
Berlin, Haus der Kulturen, WOS 2\footnote{http://www.mikro.org/Events/OS/wos2/}
\end{sect}

\begin{sect}{kima}{Wie?}
Pour participer activement, veuillez contacter wos-2001@ffii.org\footnote{mailto:wos-2001@ffii.org?subject=http://swpat.ffii.org/dates/2001/wos/index.fr.html} informellement.  Veuilez aussi a consid\'{e}rer le programme int\'{e}gral du congr\'{e}s WOS\footnote{http://www.mikro.org/Events/OS/wos2/} dans vos plans.
\end{sect}

\begin{sect}{fre}{Vendredi 2001-10-12}
\begin{description}
\item[16-19:00 Critique d'\'{e}tudes gouvernmentales r\'{e}centes sur Brevets Logiciels:]\ L'Europe se pr\'{e}pare pour des d\'{e}cisions importantes sur la question, quels sortes de innovations seront brevetables en Europe.  Dans les dernieres ann\'{e}es plusieurs \'{e}tudes sur l'impacte des brevets logiciels command\'{e}es par des agences gouvernementales ont constat\'{e} que, tandis que le brevets logiciel nuit a l'\'{e}conomie du logiciel, il doit quand-m\^{e}me \^{e}tre l\'{e}galis\'{e}.  Dans ce s\'{e}minaire nous \'{e}tudions ces \'{e}tudes et surtout leur argumentation juridique pour en obtenir un image clair des options auxquels fait face le l\'{e}gislateur et leurs implications syst\'{e}matiques.
\begin{description}
\item[Horaire:]\ \begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
16.00-17.00 & Grundlegende Fragestellungen, \"{U}berblick \"{u}ber die Literatur & phm\\\hline
17.00-18.00 & Y-a-t-il un ``domaine d'ambivalence'' entre la maitrise des force physiques et le calcul logique?  Quelle est l'avantage d'un ``privil\`{e}ge de code source''? & te\\\hline
18.00-19.00 & Est-ce que le r\'{e}gime l\'{e}gal actuel manque de clarit\'{e}?  Quels sont les implications r\'{e}elles de la partie \'{e}cnomique de l'\'{e}tude Fraunhofer et des autres \'{e}tudes gouvernmentales? & phm\\\hline
\end{tabular}
\end{center}
\item[A Lire:]\ \begin{itemize}
\item
Software Patentability with Compensatory Regulation: a Cost Evaluation\footnote{http://swpat.ffii.org/analyse/pleji/index.en.html}

\item
La S\'{e}curit\'{e} dans la Technologie d'Information et la Protection de Brevet pour les Produits Logiciels -- Opinion d'Expert de Lutterbeck et autres a l'ordre du Minist\`{e}re d'\'{E}conomie et Technologie Allemand\footnote{http://swpat.ffii.org/papiers/bmwi-luhoge00/index.de.html}

\item
Fraunhofer/MPI 2001: \'{E}tude \'{E}conomique/L\'{e}gale sur les Brevets Logiciels\footnote{http://swpat.ffii.org/papiers/bmwi-fhgmpi01/index.de.html}

\item
Stimuler la concurrence et l'innovation dans la Soci\'{e}t\'{e} d'Information\footnote{http://www.pro-innovation.org/rapport\_brevet/brevets\_plan.pdf}

\item
IPI Study: The Economic Impact of Patentability of Computer Programs\footnote{http://europa.eu.int/comm/internal\_market/en/indprop/comp/study.pdf}

\item
Intellectual Property Initiative 2000\footnote{http://info.sm.umist.ac.uk/esrcip/background.htm}

\item
http://www.patent.gov.uk/about/ippd/consultation/conclusions.htm

\item
Avis de l'acad\'{e}mie des technologies concernant la Brevetabilit\'{e} des inventions mises en oeuvre par ordinateur\footnote{http://www.internet.gouv.fr/francais/textesref/avisacatec180701.htm}
\end{itemize}
\item[Bisher angemeldete Teilnehmer:]\ Xu\^{a}n Baldauf (Leipzig)\\
Susanne Br\"{a}cklein (Berlin)\\
Thomas Ebinger (Berlin)\\
Jens Freimann (Frankfurt)\\
Erik Josefsson (Malm\"{o})\\
Hartmut Pilch (M\"{u}nchen)\\
Hubertus Soquat (Berlin)\\
Oliver Zendel (Kaiserslautern)\\
N.N. (Quelques participants ne veulent pas \^{e}tre list\'{e}s par nom.)
\end{description}
\item[20-22:00 Discussion Podium Swpat dans l'Auditoire:]\ \begin{description}
\item[Suj\'{e}t:]\ Significance \'{E}conomique des Brevets Logiciels
\item[Discutants:]\ Fritz Teufel (Patentanwalt, IBM)\\
Daniel Probst (Volkswirt, Uni Mannheim)\\
Knuth Blind (Innovationsforscher, Fraunhofer ISI)\\
Moderator: Boris Gr\"{o}ndahl (Journalist, The Standard)
\item[Attention:]\ \begin{center}
\begin{tabular}{|C{89}|}
\hline
Cette discussion n'est pas organis\'{e} par la FFII, et elle demande un ticket d'entr\'{e}e\footnote{http://www.mikro.org/Events/OS/wos2/registration.html}.\\\hline
\end{tabular}
\end{center}
\end{description}
\end{description}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

