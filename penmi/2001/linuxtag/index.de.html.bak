<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
<meta name="author" content="Arbeitsgruppe">
<link href="../../../gruppe/index.de.html" rev="made">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2003/09/18">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="title" content="15 Jahre Softwarepatentierung am Europ&auml;ischen Patentamt">
<meta name="description" content="Um 1986 erkl&auml;rten Patentrechtslehrb&uuml;cher und -kommentare einhellig, das europ&auml;ische Patentrecht biete keinerlei Spielraum, um Forderungen aus der Softwarebranche nach Patentschutz f&uuml;r ihre erfinderischen Leistungen entgegenzukommen.  Vielfach wurde beklagt, das geltende Urheberrecht biete nicht gen&uuml;gend Schutz gegen Nachahmung, aber die T&uuml;r zum Patentschutz f&uuml;r Software schien fest verschlossen.  Mit den Pr&uuml;fungsrichtlinien des Europ&auml;ischen Patentamtes (EPA) von 1985 wurde indes unbemerkt ein Spalt f&uuml;r &quot;Programme mit technischem Effekt&quot; ge&ouml;ffnet und eine Lockerung des Technikbegriffs vorbereitet, die nach herrschender Rechtsauffassung nur einer Aufgabe jeglicher &uuml;bersehbarer Begrenzung der Patentierbarkeit gleichkommen konnte.  Ein Jahr sp&auml;ter, 1986, wurde der Spalt durch zwei Entscheidungen der Technischen Beschwerdekammer des EPA zu einer sichtbaren Bresche verbreitert.  Seitdem hat das EPA mehr als 30000 Patente f&uuml;r (un)technische Lehren erteilt, die bis dahin als Organisations- und Rechenregeln oder als Programme f&uuml;r Datenverarbeitungsanlagen (als solche) abgelehnt worden w&auml;ren.  Richter und Rechtsgelehrte haben inzwischen allerlei Abgrenzungsregeln vorgeschlagen, um diese Entwicklung erneut auf eine systematische Grundlage zu stellen und im Sinne der Innovationsf&ouml;rderung angemessen zu gestalten.  Ziel dieses Seminars ist es, zu ergr&uuml;nden, in wie weit dies gelungen ist oder gelingen kann.  Es gilt zu erforschen, wie sich der Raum der Innovationen entlang der Achsen materiell vs immateriell, Naturgesetze vs Rechenregeln, konkret vs abstrakt, L&ouml;sung vs Problem, trivial vs schwierig, kausal vs funktional usw aufteilt, und was verschiedene m&ouml;gliche Abgrenzungsregeln in diesem Raum bewirken.  Als empirische Grundlage dieser Innovationskartographie dienen die in den letzten 15 Jahren vom Europ&auml;ischen Patentamt erteilten Patente.  Wir halten wir zun&auml;chst ein eint&auml;tiges Seminar am Donnerstag den 5. Juli im Rahmen des Linuxtages auf dem Stuttgarter Messegel&auml;nde ab.  Zusammen mit weiteren Folgeveranstaltungen erhoffen sich der FFII e.V., ENEF e.V., VOV e.V. und andere Beteiligte von diesem Seminar Impulse zu wertvollen Erkenntnissgewinnen auf einem wissenschaftlich faszinierenden und politisch bedeutenden interdisziplin&auml;ren Gebiet.">
<title>15 Jahre Softwarepatentierung am Europ&auml;ischen Patentamt</title>
<style type="text/css">
<!--
H1,H2,H3,H4,H5,H6 {font-family:verdana,helvetica,arial,sans-serif}
A {font-family:verdana,helvetica,arial,sans-serif}
A:link {text-decoration:none;font-weight:bold}
A:visited{text-decoration:none;font-weight:bold}
A:active {text-decoration:none;font-weight:bold}-->
</style>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><a href="index.en.html" charset=utf-8><img src="/img/flags/en.png" alt="[EN English]" border=1 height=25></a>
<a href="index.fr.html" charset=utf-8><img src="/img/flags/fr.png" alt="[FR Francais]" border=1 height=25></a>
<a href="swplxtg017.de.txt"><img src="/img/icons.other/txt.png" alt="[&Uuml;bersetzungsvorlage]" border=1 height=25></a>
<a href="/gruppe/aufgaben/index.de.html"><img src="/img/icons.other/questionmark.png" alt="[Wie helfen?]" border=1 height=25></a>
<a href="swplxtg017.de.pdf"><img src="/img/icons.other/pdficon.png" alt="[Papierfassung]" border=1 height=25></a>
<a href="http://kwiki.ffii.org/Swplxtg017De"><img src="/img/icons.other/groupedit.png" alt="[Anmerkungen der Leser]" border=1 height=25></a>
<table width="100%">
<tr><th align=center bgcolor="#B0F5C5"><a href="../index.de.html">2001</a></th><td align=center bgcolor="#004010"><b><font color="#B0F5C5">Linuxtag 2001</font></b></td><td align=center bgcolor="#B0F5C5"><a href="jh/index.en.html">Józef Halbersztadt</a></td><td align=center bgcolor="#B0F5C5"><a href="xuan/index.de.html">Xuân Baldauf</a></td></tr>
</table>
<div align="center"><h1><a name="top"><div align="center"><div align="center"><table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=7 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=4 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung 1986, <i><font size=25 color="#FF0000">heute</font></i> und morgen</font><br>
<font size=4 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=4 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table></div></div></a></h1></div>
<p>
<cite>Um 1986 erkl&auml;rten Patentrechtslehrb&uuml;cher und -kommentare einhellig, das europ&auml;ische Patentrecht biete keinerlei Spielraum, um Forderungen aus der Softwarebranche nach Patentschutz f&uuml;r ihre erfinderischen Leistungen entgegenzukommen.  Vielfach wurde beklagt, das geltende Urheberrecht biete nicht gen&uuml;gend Schutz gegen Nachahmung, aber die T&uuml;r zum Patentschutz f&uuml;r Software schien fest verschlossen.  Mit den Pr&uuml;fungsrichtlinien des Europ&auml;ischen Patentamtes (EPA) von 1985 wurde indes unbemerkt ein Spalt f&uuml;r &quot;Programme mit technischem Effekt&quot; ge&ouml;ffnet und eine Lockerung des Technikbegriffs vorbereitet, die nach herrschender Rechtsauffassung nur einer Aufgabe jeglicher &uuml;bersehbarer Begrenzung der Patentierbarkeit gleichkommen konnte.  Ein Jahr sp&auml;ter, 1986, wurde der Spalt durch zwei Entscheidungen der Technischen Beschwerdekammer des EPA zu einer sichtbaren Bresche verbreitert.  Seitdem hat das EPA mehr als 30000 Patente f&uuml;r (un)technische Lehren erteilt, die bis dahin als Organisations- und Rechenregeln oder als Programme f&uuml;r Datenverarbeitungsanlagen (als solche) abgelehnt worden w&auml;ren.  Richter und Rechtsgelehrte haben inzwischen allerlei Abgrenzungsregeln vorgeschlagen, um diese Entwicklung erneut auf eine systematische Grundlage zu stellen und im Sinne der Innovationsf&ouml;rderung angemessen zu gestalten.  Ziel dieses Seminars ist es, zu ergr&uuml;nden, in wie weit dies gelungen ist oder gelingen kann.  Es gilt zu erforschen, wie sich der Raum der Innovationen entlang der Achsen materiell vs immateriell, Naturgesetze vs Rechenregeln, konkret vs abstrakt, L&ouml;sung vs Problem, trivial vs schwierig, kausal vs funktional usw aufteilt, und was verschiedene m&ouml;gliche Abgrenzungsregeln in diesem Raum bewirken.  Als empirische Grundlage dieser Innovationskartographie dienen die in den letzten 15 Jahren vom Europ&auml;ischen Patentamt erteilten Patente.  Wir halten wir zun&auml;chst ein eint&auml;tiges Seminar am Donnerstag den 5. Juli im Rahmen des Linuxtages auf dem Stuttgarter Messegel&auml;nde ab.  Zusammen mit weiteren Folgeveranstaltungen erhoffen sich der FFII e.V., ENEF e.V., VOV e.V. und andere Beteiligte von diesem Seminar Impulse zu wertvollen Erkenntnissgewinnen auf einem wissenschaftlich faszinierenden und politisch bedeutenden interdisziplin&auml;ren Gebiet.</cite>
<p>
<ul><li><a href="#kiam">Wann?</a></li>
<li><a href="#kiok">Wo?</a></li>
<li><a href="#kima">Wie?</a></li>
<li><a href="#plan">Tagungsplan (2001-07-05)</a></li>
<li><a href="#pod">Podiumsdiskussion (2001-07-08, Sonntag)</a></li>
<li><a href="#org">Veranstalter und Unterst&uuml;tzer</a></li>
<li><a href="#hom">Teilnehmer</a></li>
<li><a href="#fol">Werbemittel</a></li>
<li><a href="#etc">Weitere Lekt&uuml;re</a></li></ul>

<a name="kiam"><h2>Wann?</h2></a>
<p>
2001-07-05, Donnerstag, 10.00-18.00
<p>
ferner gibt es eine <a href="#pod">Podiumsdiskussion am Sonntag</a>.
<p>
<a name="kiok"><h2>Wo?</h2></a>
<p>
Messegel&auml;nde Stuttgart/Killesberg, Linuxtag, <a href="http://www.infodrom.ffis.de/Debian/events/LinuxTag2001/workshops.php3">CongressCentrum ASaal III</a>
<p>
<a href="http://www.linuxtag.org/2001/deutsch/showitem.php3?item=91&amp;lang=de_DE@euro">Anreise</a>
<dl><dt>per Bahn:</dt>
<dd>Stuttgart Hauptbahnhof, U7 Richtung Killesberg/Messe</dd>
<dt>per Auto:</dt>
<dd>den Schildern nach &quot;Messegel&auml;nde Killesberg&quot; oder &quot;Messe&quot; folgen</dd></dl>
<p>
<a name="kima"><h2>Wie?</h2></a>
<p>
Wenn Sie an dem Seminar aktiv teilnehmen m&ouml;chten, nehmen Sie sich bitte formlos zu <a href="mailto:linuxtag-2001@ffii.org?subject=http://swpat.ffii.org/termine/2001/linuxtag/index.de.html">linuxtag-2001@ffii.org</a> Kontakt auf.  Zuh&ouml;ren k&ouml;nnen Sie auch ohne Anmeldung.
<p>
Wir m&ouml;chten eingesandte Beitr&auml;ge mit dieser Webseite verweben und auch in papiergerechter Form (PDF) aufbereiten.  Eine gr&ouml;&szlig;ere <a href="../systems/index.de.html">Folgeveranstaltung</a> ist geplant.
<p>
<a name="plan"><h2>Tagungsplan (2001-07-05, Donnerstag)</h2></a>
<p>
<table border=1>
<tr><th align=center>Zeit</th><th align=center>Fragen</th><th align=center>Referenten</th></tr>
<tr><th>10:00-11:00</th><td><a href="../../../patente/index.de.html">Die Softwarepatente des EPA</a>
<ul><li>Klassifikation und Beispiele</li>
<li>Wirkungsanspr&uuml;che: Probleme oder L&ouml;sungen?</li>
<li>Schwierigkeit und Abstraktheit</li>
<li>Statistik: Verteilung und historische Entwicklung</li></ul></td><td>ar, xuan usw.</td></tr>
<tr><th>11:30-12:30</th><td><a href="../../../papiere/krasser86/index.de.html">Probleml&ouml;sung durch Naturkr&auml;fte oder durch Rechnen? -- Der Erfindungs- und Technikbegriff des EP&Uuml; und des Gewohnheitsrechts bis 1986</a>
<ul><li><a href="../../../papiere/bgh-dispo76/index.de.html">Dispositionsprogramm 1976</a>, <a href="../../../papiere/bgh-walzst80/index.de.html">Walzstabteilung 1980</a>, <a href="../../../papiere/bgh-abs80/index.de.html">Antiblockiersystem 1980</a> usw.</li>
<li>Physische Modellierung der Logik (Turing-Maschine) vs Logische Modellierung der Physik (Pralltestsimulation)</li>
<li>Wie viele der heutigen EPA-Patente k&ouml;nnen/m&uuml;ssen als technisch gelten?</li></ul></td><td>phm, xuan usw.</td></tr>
<tr><th>13:30-14:30</th><td><a href="../../../analyse/erfindung/index.de.html">Die Rechtsprechungspraxis seit 1986, m&ouml;gliche Systematisierungen und deren Auswirkungen:</a>
<ul><li>Vicom (1986), Sohei (1986), Koch & Sterzel (1987), Chinesische Schriftzeichen (1992), Seitenpuffer (1992), Tauchcomputer (1993), IBM computer programm product (1997), Automatische Absatzsteuerung (1998), Sprachanalyse (2000), Logikverifikation (2000), <a href="../../../papiere/bpatg17-suche00/index.de.html">Suche fehlerhafter Zeichenketten</a> (2000), pension fund system (2000) usw.</li>
<li><a href="../../../papiere/jwip-schar98/index.en.html">Schar (EPA): Erfindung = praktische wiederholbare Probleml&ouml;sung</a></li>
<li><a href="../../../papiere/grur-mellu98/index.de.html">Melullis (BGH): Erfindung = konkrete wiederholbare Probleml&ouml;sung ohne zugrundeliegendes Konzept</a></li>
<li><a href="../../../papiere/uplj-chisum86/index.en.html"><a href="../../../papiere/uplj-chisum86/index.en.html">Chisum 1986</a>, <a href="../../../papiere/grur-nack99/index.de.html">Nack 1999</a>, <a href="../../../papiere/irle-laat00/index.en.html">Laat 2000</a> usw.: Erfindung = abstrakte wiederholbare L&ouml;sung.  Wozu die Angst?</a></li></ul></td><td>te, rn usw.</td></tr>
<tr><th>15:00-16:30</th><td><a href="../../../analyse/suigen/index.de.html">Wo bietet das Urheberrecht ungen&uuml;gende Leistungsanreize und welche Alternativen gibt es?</a>
<ul><li><a href="../../../papiere/uplr-newell86/index.en.html">Newell 1986</a>, <a href="../../../papiere/ist-tamai98/index.en.html">Tamai 1998</a> usw.: Organisations- und Rechenregeln sind sind entweder trival oder abstrakt</li>
<li><a href="jh/index.en.html">Leistungsschutzbed&uuml;rfnis vs Freihaltungsbed&uuml;rfnis im technischen vs untechnischen Bereich</a></li>
<li>Probleme des Patentwesen bei echten &quot;technischen Erfindungen&quot; am Beispiel des Streits um s&uuml;dafrikanische AIDS-Medikamente: brauchen wir insgesamt eine andere Arbeitsteilung zwischen &ouml;ffentlicher und privater Forschung?</li></ul></td><td>jh, rs, cl usw.</td></tr>
<tr><th>17:00-18:00</th><td>&Ouml;ffentlicher Vortrag mit Diskussion: Europ&auml;ische Softwarepatente und Freie Software</td><td>Daniel Riek</td></tr>
</table>
<p>
<a name="pod"><h2>Podiumsdiskussion (2001-07-08, Sonntag)</h2></a>
<p>
Au&szlig;erdem finden sich einige der Seminarteilnehmer am Sonntag im Rahmen des Linuxtag-Kongressprogramms zu einer abschlie&szlig;ende <a href="http://www.linuxtag.org/2001/showitem.php3?item=103&lang=de&tid=lt-000000002">Podiumsdiskussion</a> ein.
<p>
<dl><dt>Wann?:</dt>
<dd>2001-07-08 14:30-16:00</dd>
<dt>Wo?:</dt>
<dd>CongressCentrum B/Saal X a/b</dd>
<dt>Ablauf:</dt>
<dd>Drei Podiumsteilnehmer referieren jeweils 5 Minuten zu einer Frage.  Danach gibt es 15 Minuten Diskussion.  Zum Schluss stellt das Publikum dem Podium Fragen.
<p>
<table border=1>
<tr><th align=center>Zeit</th><th align=center>Fragen</th><th align=center>Referenten</th></tr>
<tr><th>14:30</th><td>Wie sehen typische europ&auml;ische Softwarepatente aus?  Wie viele &quot;gute Patente&quot; gibt es?</td><td>df</td></tr>
<tr><th>14:50</th><td>Die Grenzen der Patentierbarkeit:  Macht der Universalrechner jede neue <em>Organisations- und Rechenregel</em> zu einer <em>technischen Erfindung</em>?</td><td>phm</td></tr>
<tr><th>15:10</th><td>Welche Art von Investitionsschutz brauchen Softwareunternehmen heute?</td><td>rs</td></tr>
<tr><th>15:30</th><td>? ? ?</td><td>Publikum</td></tr>
</table></dd></dl>
<p>
<a name="org"><h2>Veranstalter und Unterst&uuml;tzer</h2></a>
<p>
Wir danken insbesondere der Firma <a href="http://www.skyrix.com">Skyrix</a> f&uuml;r die &Uuml;bernahme von Reisespesen einiger unserer Redner und der Firma <a href="http://www.suse.de">SuSE</a> f&uuml;r die Freistellung von Personalressourcen zur Unterst&uuml;tzung dieser Veranstaltung.  Statten Sie bitte diesen Firmen einen Besuch auf dem Linuxtag ab, schauen Sie sich ihre Produkte und Dienstleistungen genau an!
<p>
<div align="center"><a href="http://www.ffii.org/index.de.html" target=ffii><img src="/img/logos/ffii.png" alt="FFII" border=0></a>
<p>
<a href="http://www.enef.org"><img src="/img/logos/enef.png" alt="ENEF" border=0></a>
<p>
<a href="http://www.vov.de"><img src="/img/logos/vov.png" alt="VOV" border=0></a>
<p>
<a href="http://www.eurolinux.org/index.de.html" target=eurolinux><img src="/img/logos/eurolinux.jpg" alt="EUROLINUX" border=0></a>
<p>
<img src="/img/logos/skyrix.png" alt="SKYRIX" border=0>
<p>
<a href="http://www.suse.de"><img src="/img/logos/suse.png" alt="SUSE" border=0></a></div>
<p>
<a name="hom"><h2>Teilnehmer</h2></a>
<p>
Zugesagt haben bisher folgende Teilnehmer.
<p>
<table border=1>
<tr><th>Arnim Rupp</th><td>Heidelberg</td><td><a href="../../../patente/index.de.html">Gruselkabinett der Europ&auml;ischen Softwarepatente</a></td></tr>
<tr><th>N.N.</th><td>Berlin</td><td><a href="http://www.enef.org">European Net Economy Forum</a></td></tr>
<tr><th>Jean-Paul Smets</th><td>FR Paris</td><td>Erstellte im Auftrag einer franz&ouml;sischen Regierungsorganisation eine sehr lesenswerte <a href="http://www.pro-innovation.org/rapport_brevet/brevets_plan.pdf">Studie</a> zum Thema Softwarepatente</td></tr>
<tr><th><a href="jh/index.en.html">J&oacute;zef Halbersztadt</a></th><td>PL Warszawa</td><td>Polnisches Patentamt</td></tr>
<tr><th>Gerd Becker</th><td>Stuttgart</td><td>Sprecher des <a href="http://www.vov.de">Virtuellen Ortsvereins der SPD</a> (VOV.de)</td></tr>
<tr><th>Xuan Baldauf</th><td>Leipzig</td><td><a href="http://www.medium.net">Softwareunternehmer</a>, Autor einer <a href="http://www.save-our-software.de">popularisierenden Darstellung der Problematik</a>, hat sich im Rahmen der Arbeit des FFII detailliert Gedanken &uuml;ber den Technikbegriff gemacht</td></tr>
<tr><th>Thomas Ebinger</th><td>Berlin</td><td>Jurist, schrieb Magisterarbeit &uuml;ber Patentierbarkeit von Computerprogrammen</td></tr>
<tr><th>Ralph Nack</th><td>M&uuml;nchen</td><td>Max-Planck-Institut f&uuml;r Internationales Patent-, Urheber- und Wettbewerbsrecht, promoviert &uuml;ber Grenzen der Patentierbarkeit insbes im Hinblick auf Gesch&auml;ftsverfahren: neuere Rechtsprechung und m&ouml;gliche Systematik</td></tr>
<tr><th>J&uuml;rgen Siepmann</th><td>Freiburg</td><td>RA Dipl. Phys., Justitiar des <a href="http://www.linux-verband.de">Linux-Verband</a>es, Autor zahlreicher Artikel &uuml;ber Softwarepatente</td></tr>
<tr><th>Christian Labadie</th><td>Hannover</td><td>im Entwicklungshilfe-Bereich t&auml;tig, Kenner der Probleme S&uuml;dafrikas mit Pharmapatenten</td></tr>
<tr><th>Holger Blasum</th><td>M&uuml;nchen</td><td>Kenner der juristischen und &ouml;konomischen Fachliteratur zu Softwarepatenten, Triebfeder der Dokumentationsarbeit des FFII, Lieferant von neuheitssch&auml;digenden Dokumenten f&uuml;r Bountyquest</td></tr>
<tr><th>Daniel Riek</th><td>Bonn</td><td>Vorstandsmitglied <a href="http://www.linux-verband.de">Linux-Verband</a>, Gesch&auml;ftsf&uuml;hrer der Alcove Deutschland GmbH</td></tr>
<tr><th>Hartmut Pilch</th><td>M&uuml;nchen</td><td>Mitarbeiter der <a href="http://www.suse.de">SuSE Linux AG</a>, Vorsitzender des FFII, Eurolinux-Sprecher</td></tr>
<tr><th>Luuk van Dijk</th><td>NL Groningen</td><td><a href="http://vereniging.opensource.nl/werkgroepen/PR+A/patent/">VOSN en Software Patenten</a> und Softwareunternehmer, <a href="http://vereniging.opensource.nl/werkgroepen/PR+A/patent/">bearbeitet f&uuml;r eine holl&auml;ndische IT-Vereinigung das Thema Softwarepatente</a> und wurde vom holl&auml;ndischen Parlament aufgefordert, zusammen mit der Gegenseite einen Vorschlag zur Verhinderung von Trivialpatenten zu erarbeiten</td></tr>
<tr><th>Flemming Bjerke</th><td>DK K&oslash;bnhavn</td><td>lehrt in Verwaltungshochschule Kopenhagen, hat in seinem <a href="../../../papiere/eukonsult00/bjerke/index.en.html">EU-Konsultationspapier</a> den Technikbegriff neu erfunden.</td></tr>
<tr><th>NIIBE Yutaka</th><td>JP Tsukuba</td><td>leitet in einem f&uuml;hrenden japanischen Forschungszentrum informatische Projekte und hat sich kritisch mit den Entwicklungen der Patentexpansion der letzten Jahre auseinandergesetzt</td></tr>
<tr><th>Ralf Schwöbel</th><td>Frankfurt</td><td>Vorstand <a href="http://www.intradat.de">Intradat AG</a></td></tr>
<tr><th>N.N.</th><td>Hamburg</td><td><a href="http://www.ifross.de">ifross</a></td></tr>
<tr><th>Andreas St&ouml;ckigt</th><td>Schwerte</td><td>Unternehmensberater, Vorstandsmitglied der <a href="http://www.gi-ev.de/praxis/patente/patente-ueb.shtml">Gesellschaft f&uuml;r Informatik</a></td></tr>
<tr><th>Hubertus Soquat</th><td>Berlin</td><td><a href="http://www.sicherheit-im-internet.de">BMWi</a></td></tr>
<tr><th>Norman Hoppen</th><td>Frankfurt</td><td>promoviert am <a href="http://www.wiwi.uni-frankfurt.de">Institut f&uuml;r Wirtschaftsinformatik der Universit&auml;t Frankfurt</a> &uuml;ber &ouml;konomische Aspekte der Softwarepatierung, organisierte k&uuml;rzlich eine <a href="http://www.softwarepatente.net">Gro&szlig;veranstaltung</a> zu diesem Thema</td></tr>
<tr><th>Erik Josefsson</th><td>SE Malm&ouml;</td><td>Softwareentwickler mit starkem politischem und philosophischem Interesse, Kenner der Lage zu Softwarepatenten in Schweden</td></tr>
</table>
<p>
<a name="fol"><h2>Werbemittel</h2></a>
<p>
<dl><dt>Druckbares Flugblatt:</dt>
<dd><ul><li><a href="linuxtag-2001de.tex">TeX Quelltext</a></li>
<li><a href="linuxtag-2001de.ps">Postscript</a></li>
<li><a href="linuxtag-2001de.pdf">PDF</a></li>
<li><a href="linuxtag-2001en.tex">TeX Quelltext</a></li>
<li><a href="linuxtag-2001en.ps">Postscript</a></li>
<li><a href="linuxtag-2001en.pdf">PDF</a></li></ul></dd>
<dt>WWW-Verweisschild:</dt>
<dd>Bitte verwenden Sie eines der folgenden HTML-basierte Rechtecke, um von Ihren Webseiten aus auf uns zu verweisen.
<p>
<div align="center"><table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=4 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung <i><font size=25 color="#FF0000">seit</font></i> 1986</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=4 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung <i><font size=25 color="#FF0000">seit</font></i> 1986</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=5 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung <i><font size=25 color="#FF0000">seit</font></i> 1986</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=6 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=4 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung <i><font size=25 color="#FF0000">seit</font></i> 1986</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=4 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung 1986, <i><font size=25 color="#FF0000">heute</font></i> und morgen</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=4 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung 1986, <i><font size=25 color="#FF0000">heute</font></i> und morgen</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=1 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=5 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung 1986, <i><font size=25 color="#FF0000">heute</font></i> und morgen</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=2 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table>
<p>
<table cellpadding=4 cellspacing=0>
<tr bgcolor="0000BF" align="center"><td><b><a style="color:#FFFF00; font-family:arial, helvetica; text-decoration:none;"><font size=6 face="Arial, Helvetica" color="#FFFF00">15 Jahre <i><font size=25 color="#FF0000">e</font></i>PATENTE in Europa</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">Die technische Erfindung 1986, <i><font size=25 color="#FF0000">heute</font></i> und morgen</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">Seminar 2001-07-05 Stuttgart Linuxtag CCA III</font><br>
<font size=3 face="Arial, Helvetica" color="#FFFF00">FFII.org, ENEF.org, VOV.de und eurolinux.org</font></a></b></td></tr>
</table></div></dd></dl>
<p>
<a name="etc"><h2>Weitere Lekt&uuml;re</h2></a>
<p>
<table border=1 cellpadding=20>
<tr><th align=left><dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../../index.de.html#news">Neues</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../bundestag/index.de.html">Berlin 2001-06-21: Bundestags-Expertengespr&auml;ch Softwarepatente</a></b></dt>
<dd>Im Bundestag stehen acht Fachleute aus den Bereichen Recht, Informatik und Wirtschaftswissenschaften zum Thema Softwarepatentierung den Abgeordneten Rede und Antwort stehen, nachdem sie schriftliche Stellungnahmen zu einer Reihe von Fragen abgegeben haben.  Die interessierte &Ouml;ffentlichkeit war ebenfalls aufgerufen, in schriftlicher Form zu diesen Fragen Stellung zu nehmen.  Wir ver&ouml;ffentlichen hier das Gespr&auml;chsprotokoll und die schriftlichen Eingaben.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../../papiere/eukonsult00/index.en.html">Europ&auml;ische Konsultation &uuml;ber die Patentierbarkeit von Computer-Implementierbaren Organisations- und Rechenregeln (= Programmen f&uuml;r Datenverarbeitungsanlagen)</a></b></dt>
<dd>Am 19. Okt 2000 ver&ouml;ffentlichte die Dienststelle f&uuml;r Gewerblichen Rechtsschutz der Europ&auml;ischen Kommission (EuDGR) ein Sondierungspapier, welches eine rechtliche Argumentation darlegt, wie das Europ&auml;ische Patentamt (EPA) sie in den letzten Jahren verwendet hat, um ihre Praxis der Patentierung von Programmen f&uuml;r Datenverarbeitungsanlagen und anderen Organisations- und Rechenregeln gegen den Buchstaben und Geist der geltenden Gesetze zu rechtfertigen.  Die Konsultation richtete sich offenbar an die Patentabteilungen diverser Unternehmen und Verb&auml;nde und war als ein Man&ouml;ver zu ihrer Mobilisierung konzipiert.  Das Papier selber warb einseitig f&uuml;r den Standpunkt des Europ&auml;ischen Patentamtes und stellte Fragen, die nur Patentjuristen verstehen und beantworten k&ouml;nnen.  Ferner wurde es von einer &quot;unabh&auml;ngigen Studie&quot; best&auml;tigt, welche eine bekannte Denkfabrik der Patentbewegung im Auftrag der EuDGR durchgef&uuml;hrt hatte.  Patentjuristen verschiedener Organisationen sandten applaudierende Antworten ein und erkl&auml;rten dabei das bekannte Credo der Patentbewegung, wonach Patente grunds&auml;tzlich in allen Gebieten die Innovation f&ouml;rdern und vor allem dem Wohle der kleinen und mittleren Unternehmen dienen.  Allerdings antworteten auch einige Verb&auml;nde und Firmen sowie &uuml;ber 1000 Einzelpersonen, vor allem Programmierer, mit kritischen Stellungnahmen.  Die EuDGR hat die Stellungnahmen bisher nur schleppend und unvollst&auml;ndig und in schwer konsultierbarer Form ver&ouml;ffentlicht.  Dem wollen wir abhelfen, und Sie k&ouml;nnen mitmachen.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../../akteure/fhg/index.de.html">Die Fraunhofer-Gesellschaft als Bastion der Patentbewegung</a></b></dt>
<dd>Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild f&uuml;r relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle f&uuml;r die deutschen Hochschulen, die eine &auml;hnliche Pilotfunktion aus&uuml;bt.  Das Fraunhofer-Institut f&uuml;r Innovationsforschung verfasst regelm&auml;&szlig;ig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungstr&auml;ger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../../papiere/bmwi-luhoge00/index.de.html">Sicherheit in der Informationstechnologie und Patentschutz f&uuml;r Software-Produkte - Kurzgutachten von Lutterbeck et al im Auftrag des BMWi</a></b></dt>
<dd>Prof. Dr.iur. Bernd Lutterbeck von der TU-Berlin, sein Assistent Robert Gehring und der M&uuml;nchener Patentanwalt Axel Horns nahmen im Sp&auml;tsommer 2000 unter dem Namen &quot;Forschergruppe Internet Governance&quot; einen Auftrag des BMWi f&uuml;r dieses 166 Seiten lange &quot;Kurzgutachten&quot; an, das im Dezember 2000 ver&ouml;ffentlicht wurde.   Darin vertreten sie eine bereits h&auml;ufig zuvor ver&ouml;ffentlichte Rechtsauffassung des M&uuml;nchener Patentanwalts Axel H. Horns zur Frage der Patentierbarkeit von Computerprogrammen in Europa.  PA Horns kann dem Art 52 EP&Uuml; keine klare Bedeutung abgewinnen.  Er erkl&auml;rt den traditionellen Technikbegriff des Patentwesens f&uuml;r nicht mehr zeitgem&auml;&szlig; und h&auml;lt die grenzenlose Patentierbarkeit, wie das EPA sie in der Theorie anstrebt und in der Praxis bereits weitgehend verwirklicht hat, f&uuml;r unvermeidbar.  Gleichzeitig wird aber vor diversen negativen Folgen des Patentwesens f&uuml;r Wirtschaft und Gesellschaft gewarnt und es werden diverse Ma&szlig;nahmen vorgeschlagen, mit denen die Sperrwirkung der Patente im Bereich der Informationsverarbeitung abgeschw&auml;cht werden k&ouml;nnte, so dass zumindest in Deutschland ein Reservat &uuml;brig bleiben k&ouml;nnte, in dem Software-Quelltexte ungehindert ver&ouml;ffentlicht aber nicht gewerblich genutzt werden d&uuml;rfen.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../../analyse/erfindung/index.de.html">Patentjurisprudenz auf Schlitterkurs -- der Preis f&uuml;r die Demontage des Technikbegriffs</a></b></dt>
<dd>enth&auml;lt eine lange Liste kommentierter Verweise</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="../../2000/linuxtag/index.de.html">FFII auf Linuxtag 2000</a></b></dt>
<dd>Der FFII e.V. informierte auf dem LinuxTag 2000 in Stuttgart &uuml;ber neuere gesetzeswidrige Praktiken der deutschen und europ&auml;ischen Patentjustiz sowie deren Pl&auml;ne, Programmlogik und damit wirtschaftliche und gesellschaftliche Verfahren aller Art umfassend patentierbar zu machen.</dd></dl></th></tr>
</table>
<p>
<table border=1 cellpadding=20>
<tr><th align=left><dl><dt><img src="/img/icons.other/folder.png" alt="*"><a href="jh/index.en.html"><b>Remarks on the Patentability of Computer Software -- History, Status, Developments</b></a>:</dt>
<dd>Ein Aufsatz von Jozef Halbersztadt, Patentpr&uuml;fer am Polnischen Patentamt, f&uuml;r ein Seminar in Stuttgart im Juli 2001 vorbereitet.  Der Aufsatz dr&uuml;ckt seine private Sichtweise aus, wonach Software ein ma&szlig;geschneidertes System der Sch&ouml;pferbelohnung erfordert und nicht durch blo&szlig;e Anpassung des Urheberrechts oder, schlimmer noch, Patentrechts angemessen behandelt werden kann.  Er zeichnet die Geschichte des Ringens um die richtige Form der Sch&ouml;pferrechte im Softwarebereich als die Reise einer Kutsche nach, deren Fahrtrichtung von den Pferden bestimmt wurde.  Nachdem die Kutsche nun an die Wand gefahren wurde, sei es Zeit, aufzuwachen und fr&uuml;here Ans&auml;tze eines softwarespezifischen Rechts neu zu beleben.  Halbersztadt zeigt dabei verschiedene m&ouml;gliche Optionen auf und beschreibt Wege, wie dies im Rahmen der derzeit geplanten europ&auml;ischen Rechtsetzung geschehen k&ouml;nnte.</dd>
<dt><img src="/img/icons.other/folder.png" alt="*"><a href="xuan/index.de.html"><b>Baldauf: Technische Erfindungen und Modellierung  -- Vortrag Stuttgart 2001-06</b></a>:</dt>
<dd>Patente werden f&uuml;r technische Erfindungen erteilt, d.h. Lehren zum Einsatz beherrschbarer Naturkr&auml;fte zur unmittelbaren Herbeif&uuml;hrung eines kausal &uuml;bersehbaren Erfolges.  Eine technische Probleml&ouml;sung steht und f&auml;llt mit dem Einsatz der Naturkr&auml;fte.  Damit steht sie im Gegensatz zu einer Organisations- und Rechenregel.  Was aber passiert, wenn wir die physischen Kausalit&auml;ten bereits kennen und bis ins Detail mathematisch erfasst haben?  Wird mit zunehmender Mathematisierung alles, was fr&uuml;her eine technische Aufgabe war, in dem Ma&szlig;e zu einer Rechenaufgabe reduziert und damit der Patentf&auml;higkeit entzogen?  Was bleibt bei dieser Betrachtungsweise noch patentf&auml;hig?  L&auml;sst sich eine Grenze zwischen der Modellierung der Materie (z.B. Br&uuml;cken-Statik-Modell) und der Materialisierung von Modellen (z.B. Universalrechner) ziehen oder f&uuml;hrt alle Patentierung von Funktionslogik zu den gleichen Problemen?  Xu&acirc;n Baldauf pr&auml;sentiert Antworten aus der Sicht eines Informatikers.</dd></dl></th></tr>
</table>
<p>
<br>
<br>
<div align="center"><font size=2>[ <a href="../index.de.html">Logikpatentierung und Konferenzen 2001</a>&nbsp;&rarr;&nbsp;Seminar Linuxtag Stuttgart 2001-07-05: 15 Jahre Softwarepatentierung am Europ&auml;ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft | <a href="jh/index.en.html">Remarks on the Patentability of Computer Software -- History, Status, Developments</a> | <a href="xuan/index.de.html">Baldauf: Technische Erfindungen und Modellierung  -- Vortrag Stuttgart 2001-06</a> ]</font></div>
<form action="http://www.google.com/search" method="get">
<input name=q size=25 type=text value="">
<input name=btnG size=25 type=submit value="Google-Suche ffii.org">
<input name=q size=25 type=hidden value="site:ffii.org">
</form>
<hr>
<address>
http://swpat.ffii.org/termine/2001/linuxtag/index.de.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2003/09/18 (2001/07/05)
<a href="../../../gruppe/index.de.html">Arbeitsgruppe</a>
</address></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /ul/prg/src/mlht/sys/mlhtmake.el -->
<!-- mode: html -->
<!-- End: -->

