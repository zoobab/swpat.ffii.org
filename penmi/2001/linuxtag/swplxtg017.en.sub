\begin{subdocument}{swplxtg017}{Seminar Linuxtag Stuttgart 2001-07-05: 15 years of software patenting at the European Patent Office: the concept of ``technical invention'' in 1986, today and in the future}{http://swpat.ffii.org/events/2001/linuxtag/index.en.html}{Workgroup\\swpatag@ffii.org}{Around 1986 European manuals and commentaries of patent law unanimously explained that european patent law does not offer the software industry any chances of obtaining patents to protect its investments against imitators.  Some complained that copyright was not enough, but yet agreed that the only way to change this situation was through legislation.  Few noticed that the change was already underway:  it was implied in ambiguous wordings of the European Patent Office's Examination Guidelines of 1985: the door was opened by a narrow slot for ``computer programs with a technical effect'', and the ground was prepared for further loosening of the concept of technical character, in a way that, according to the legal doctrines of the time, amounted to removing all overseeable limits on patentability.  A year later, in 1986, the slot was widened into a visible breach by two decisions of the EPO's Technical Board of Appeal.  Since then, the EPO has granted more than 30000 patents for (un)technical teachings that had previously been regarded as unpatentable ``rules of organisation and calculation'' or ``computer programs as such''.  Judges and law scholars have meanwhile proposed various delimitation concepts in order to make sense of this somewhat chaotic caselaw development.  This seminar aims to assess to what extent this has been achieved or can be achieved.  We need to research into the space of innovations, as they are distributed along coordinates such as matter vs mind, laws of nature vs rules of calculation, concrete vs abstract, solution vs problem, trivial vs difficult, causal vs functional etc, and what kind of results various possible rules of delimitation tend to produce within this space.  An empirical basis for this innovation cartography is provided by the patents which the EPO has granted during the last 15 years.  A seminar on thursday 2001-07-05 at Linuxtag on the Stuttgart trade fair site and subsequent seminars, conducted by several interested associations, are expected to give rise to interesting new ideas in this intellectually intriguing and politically important interdisciplinary area.}
\begin{sect}{kiam}{When?}
2001-07-05, Thursday, 10.00-18.00

Moreover there is a podium discussion on sunday\footnote{see \ref{pod}, page \pageref{pod}}.
\end{sect}

\begin{sect}{kiok}{Where?}
Stuttgart/Killesberg Trade Fair Area, Linuxtag, CongressCentrum ARoom III\footnote{http://www.infodrom.ffis.de/Debian/events/LinuxTag2001/workshops.php3}

How to get there\footnote{http://www.linuxtag.org/2001/deutsch/showitem.php3?item=91\texmath{\backslash}\&lang=de\_DE@euro}
\begin{description}
\item[By train:]\ Stuttgart central station (Hauptbahnhof), subway U7 to Killesberg/Messe
\item[by car:]\ look for traffic signposts to ``Messegel\"{a}nde Killesberg'' or ``Messe''.
\end{description}
\end{sect}

\begin{sect}{kima}{How?}
If you want to participate actively, please contact linuxtag-2001@ffii.org\footnote{mailto:linuxtag-2001@ffii.org?subject=http://swpat.ffii.org/events/2001/linuxtag/index.en.html} informally.  If you just want to listen, no registration is necessary.

We would like to link weave submitted texts into this website and also bring them into a paper form (PDF).  A followup seminar\footnote{http://swpat.ffii.org/events/2001/systems/index.en.html} is being planned.
\end{sect}

\begin{sect}{plan}{Schedule (2001-07-05)}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
Time & Problems & Speakers\\\hline
10:00-11:00 & the software patents of the EPO\footnote{http://swpat.ffii.org/patents/index.en.html}
\begin{itemize}
\item
classification and examples

\item
function claims: problems or solutions?

\item
difficulty and abstractness

\item
statistics: distribution and historical development
\end{itemize} & ar, xuan\ etc\\\hline
11:30-12:30 & solving problems by natural forces or by calculation -- The concepts of invention and technical character in the EPC and in customary law until 1986\footnote{http://swpat.ffii.org/papers/krasser86/index.de.html}
\begin{itemize}
\item
Dispositionsprogramm 1976\footnote{http://swpat.ffii.org/papers/bgh-dispo76/index.en.html}, Walzstabteilung 1980\footnote{http://swpat.ffii.org/papers/bgh-walzst80/index.en.html}, Antiblockiersystem 1980\footnote{http://swpat.ffii.org/papers/bgh-abs80/index.de.html}\ etc

\item
Physical modelling of logics (Turing machine) vs logical modelling of physics (crash test simulation)

\item
How many of today's EPO patents can/must be viewed as technical?
\end{itemize} & phm, xuan\ etc\\\hline
13:30-14:30 & case-by-case development since 1986, possible systematisations and their effects\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}
\begin{itemize}
\item
Vicom (1986), Sohei (1986), Koch & Sterzel (1987), Chinesische Schriftzeichen (1992), Seitenpuffer (1992), Tauchcomputer (1993), IBM computer programm product (1997), Automatische Absatzsteuerung (1998), Sprachanalyse (2000), Logikverifikation (2000), Suche fehlerhafter Zeichenketten\footnote{http://swpat.ffii.org/papers/bpatg17-suche00/index.de.html} (2000), pension fund system (2000)\ etc

\item
Schar (EPO): invention = practical repeatable solution\footnote{http://swpat.ffii.org/papers/jwip-schar98/index.en.html}

\item
Melullis (BGH): invention = concrete solution without underlying concept\footnote{http://swpat.ffii.org/papers/grur-mellu98/index.de.html}

\item
Chisum 1986\footnote{http://swpat.ffii.org/papers/uplj-chisum86/index.en.html}, Nack 1999\footnote{http://swpat.ffii.org/papers/grur-nack99/index.de.html}, Laat 2000\footnote{http://swpat.ffii.org/papers/irle-laat00/index.en.html}\ etc: invention = abstract repeatable solution. What's the harm?\footnote{http://swpat.ffii.org/papers/uplj-chisum86/index.en.html}
\end{itemize} & te, rn\ etc\\\hline
15:00-16:30 & Where does copyright offer an insufficient incentive and what alternatives exist?\footnote{http://swpat.ffii.org/analysis/suigen/index.en.html}
\begin{itemize}
\item
Newell 1986\footnote{http://swpat.ffii.org/papers/uplr-newell86/index.en.html}, Tamai 1998\footnote{http://swpat.ffii.org/papers/ist-tamai98/index.en.html}\ etc: Rules of organisation and computation are either trivial or abstract

\item
how to weight the need for appropriability against the need for freedom in in the technical vs untechnical realm\footnote{http://swpat.ffii.org/events/2001/linuxtag/jh/index.en.html}

\item
Problems of the patent system in the area of classical technical inventions as seen from the struggle around AIDS pharmaceutics. Do we need a to put more research into the public domain?
\end{itemize} & jh, rs, cl\ etc\\\hline
17:00-18:00 & public presentation withdiscussion: European software patents and free software & Daniel Riek\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{pod}{Podium Discussion (2001-07-08, Sunday)}
Moreover on sunday some of our participants will take part in a Podium Discussion\footnote{http://www.linuxtag.org/2001/showitem.php3?item=103\&lang=de\&tid=lt-000000002} on software patents, which has been scheduled as a concluding event of the Linuxtag congress.

\begin{description}
\item[:]\ 2001-07-08 14:30-16:00
\item[:]\ CongressCentrum B/Hall X a/b
\item[Schedule:]\ Each of the 4 participants gives a short lecture (5 minutes) about one question.  Then there are 5 minutes for a discussion.  At the end the listeners ask questions.
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
Time & Problems & Speakers\\\hline
14:30 & What do typical European software patents look like?  How many ``good patents'' are there? & df\\\hline
14:50 & The limits of patentability:  Does the universal computer necessarily turn any new \emph{rule of organisation and calculation} into a ``technical invention''? & phm\\\hline
15:10 & What kind of investment protection do software companies need today? & rs\\\hline
15:30 & ? ? ? & listeners\\\hline
\end{tabular}
\end{center}
\end{description}
\end{sect}

\begin{sect}{org}{Organisers and Supporters}
We thank especially Skyrix AG\footnote{http://www.skyrix.com} for taking over travel costs and SuSE Linux AG\footnote{http://www.suse.de} for setting free personal resouces for the support of this seminar.  You may wish to visit their booths on Linuxtag and take a close look at their products and services.

\begin{center}
\mbox{\includegraphics{/img/logos/ffiilogo}}\footnote{http://www.ffii.org/index.en.html}

\mbox{\includegraphics{/img/logos/eneflogo}}

\mbox{\includegraphics{/img/logos/vovlogo}}

\mbox{\includegraphics{/img/logos/eurolinuxlogo}}\footnote{http://www.eurolinux.org/index.en.html}

\mbox{\includegraphics{/img/logos/skyrixlogo}}

\mbox{\includegraphics{/img/logos/suselogo}}
\end{center}
\end{sect}

\begin{sect}{hom}{Participants}
The following participants have registered so far.

\begin{center}
\begin{tabular}{|C{89}|}
\hline
Arnim Rupp & Heidelberg & European Software Patent Horror Gallery\footnote{http://swpat.ffii.org/patents/index.en.html}\\\hline
N.N. & Berlin & European Net Economy Forum\footnote{http://www.enef.org}\\\hline
Jean-Paul Smets & FR Paris & Wrote a very enlightening study\footnote{http://www.pro-innovation.org/rapport\_brevet/brevets\_plan-en.pdf} on the software patentability question at the order of a French governmental body.\\\hline
J\'{o}zef Halbersztadt\footnote{http://swpat.ffii.org/events/2001/linuxtag/jh/index.en.html} & PL Warszawa & Polish Patent Office\\\hline
Gerd Becker & Stuttgart & speaker of the Internet association of the Social Democratic Party of Germany (VOV.de)\\\hline
Xuan Baldauf & Leipzig & software entrepreneur\footnote{http://www.medium.net}, author of a popularising presentation of the problems\footnote{http://www.save-our-software.de}, has developped detailed thoughts about the concept of technical invention in the course of the work of FFII.\\\hline
Thomas Ebinger & Berlin & Lawyer, dissertation about software and technical character\\\hline
Ralph Nack & M\"{u}nchen & Max Planck Institute for International Patent Copyright and Competition Law, dissertation about limits of patentability especially concering business methods: new caselaw and possible systematic interpretations\\\hline
J\"{u}rgen Siepmann & Freiburg & practising lawyer, physicist, legal delegate of Linux-Verband\footnote{http://www.linux-verband.de}, author of a book on free software law issues and numerous articles on software patents\\\hline
Christian Labadie & Hannover & has studied pharma patent problems of developping countries\\\hline
Holger Blasum & Munich & Expert of scientific literature concerning software patentability, mastermind of documentation work at FFII, deliverer of novelty destroying documents for Bountyquest\\\hline
Daniel Riek & Bonn & board member of Linux-Verband\footnote{http://www.linux-verband.de}, manager of Alcove Deutschland GmbH\\\hline
Hartmut Pilch &  & Employee of SuSE Linux AG\footnote{http://www.suse.de}, president of FFII\\\hline
Luuk van Dijk & NL Groningen & VOSN en Software Patenten\footnote{http://vereniging.opensource.nl/werkgroepen/PR+A/patent/} and Software entrepreneur, has been working on the patent issue on behalf of a Dutch IT organisation\footnote{http://vereniging.opensource.nl/werkgroepen/PR+A/patent/} and has received the order of the dutch parliament to work out a compromise paper with the pro patent side that would at least prevent trivial software patents.\\\hline
Flemming Bjerke & DK K\o{}bnhavn & teaches in a school of administration of Copenhague, has reinvented the concept of technical invention in is EC consultation paper\footnote{http://swpat.ffii.org/papers/eukonsult00/bjerke/index.en.html}.\\\hline
NIIBE Yutaka & JP Tsukuba & directs informatic research projects in a leading Japanese research center and has critically observed the expansion of patentability in Japan\\\hline
Ralf Schwöbel & Frankfurt & CEO of Intradat AG\footnote{http://www.intradat.de}\\\hline
N.N. & Hamburg & ifross\footnote{http://www.ifross.de}\\\hline
Andreas St\"{o}ckigt & Schwerte & Unternehmensberater, board member of German Informatics Society\footnote{http://www.gi-ev.de/praxis/patente/patente-ueb.shtml}\\\hline
Hubertus Soquat & Berlin & German Ministery of Economics\footnote{http://www.sicherheit-im-internet.de}\\\hline
Norman Hoppen & Frankfurt & researcher on economics of software patents at Frankfurt University\footnote{http://www.wiwi.uni-frankfurt.de}, recently organised a conference\footnote{http://www.softwarepatente.net} on this subject\\\hline
Erik Josefsson & SE Malm\"{o} & Software developper with a strong political and philosophical interest, well aware of the software patents situation in Sweden\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{fol}{Means of Advertisement}
\begin{description}
\item[Printable Pamphlet:]\ \begin{itemize}
\item
TeX source\footnote{linuxtag-2001de.tex}

\item
Postscript\footnote{linuxtag-2001de.ps}

\item
PDF\footnote{linuxtag-2001de.pdf}

\item
TeX source\footnote{linuxtag-2001en.tex}

\item
Postscript\footnote{linuxtag-2001en.ps}

\item
PDF\footnote{linuxtag-2001en.pdf}
\end{itemize}
\item[Web Link Button:]\ Please use one of the following HTML-based rectangles to advertise our meeting on your web pages.
\begin{center}
\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European}\\
{\small {\it {\small e}}PATENTS}\\
{\small The technical invention {\it {\small since}} 1986}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European}\\
{\small {\it {\small e}}PATENTS}\\
{\small The technical invention {\it {\small since}} 1986}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European {\it {\small e}}PATENTS}\\
{\small The technical invention {\it {\small since}} 1986}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European {\it {\small e}}PATENTS}\\
{\small The technical invention {\it {\small since}} 1986}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European}\\
{\small {\it {\small e}}PATENTS}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European}\\
{\small {\it {\small e}}PATENTS}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European {\it {\small e}}PATENTS}\\
{\small The technical invention 1986, {\it {\small today}} and tomorrow}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|C{89}|}
\hline
{\bf {\aanchor {\small 15 years of European {\it {\small e}}PATENTS}\\
{\small The technical invention 1986, {\it {\small today}} and tomorrow}\\
{\small Seminar 2001-07-05 Stuttgart Linuxtag CCA III}\\
{\small FFII.org, ENEF.org, VOV.de and eurolinux.org}}}\\\hline
\end{tabular}
\end{center}
\end{center}
\end{description}
\end{sect}

\begin{sect}{etc}{Further Reading}
\begin{itemize}
\item
{\bf {\bf News\footnote{http://swpat.ffii.org/index.en.html\#news}}}
\filbreak

\item
{\bf {\bf Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament\footnote{http://swpat.ffii.org/events/2001/bundestag/index.en.html}}}

\begin{quote}
Eight experts from the areas of law, informatics and economics will answer questions from MPs, based on written responses to a set of questions.  The interested public is also called to present its answers to any subset of these questions in writing.  We publish here the procedings and submissions.
\end{quote}
\filbreak

\item
{\bf {\bf European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\footnote{http://swpat.ffii.org/papers/eukonsult00/index.en.html}}}

\begin{quote}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer Society as a Bastion of the Patent Movement in Germany\footnote{http://swpat.ffii.org/players/fhg/index.de.html}}}

\begin{quote}
Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild f\"{u}r relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle f\"{u}r die deutschen Hochschulen, die eine \"{a}hnliche Pilotfunktion aus\"{u}bt.  Das Fraunhofer-Institut f\"{u}r Innovationsforschung verfasst regelm\"{a}{\ss}ig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungstr\"{a}ger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.
\end{quote}
\filbreak

\item
{\bf {\bf Security in Information Technology and Patent Protection for Software Products -- Expert Opinion by Lutterbeck et al written at the order of the German Ministery of Economics and Technology\footnote{http://swpat.ffii.org/papers/bmwi-luhoge00/index.en.html}}}

\begin{quote}
Prof. Lutterbeck of Berlin Technical University, his assistant Robert Gehring and Axel Horns, patent lawyer in Munich, figuring under the name of ``Internet Governance Research Group'', received an order from the German Ministery of Economics and Technology in late summer of 2000 to work out this ``short expert opinion'' which was published in December 2000.  A large part of the 166 pages is dedicated to reiterating the well-known legal opinion of Horns.  Horns states that Art 52 EPC was a misconception from the beginning, and that patent law will be seriously impaired unless any innovation that is implemented through a computer is patentable.  However he warns that software patents can have a very negative impact on open source software and proposes that patent law at least in Germany should be amended in such a way that the publication and transmission of source code does not violate the law, even if the excecution of object code on a computer does.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}}}

\begin{quote}
contains a long list of commented links
\end{quote}
\filbreak

\item
{\bf {\bf FFII auf Linuxtag 2000\footnote{http://swpat.ffii.org/events/2000/linuxtag/index.en.html}}}

\begin{quote}
Der FFII e.V. informierte auf dem LinuxTag 2000 in Stuttgart \"{u}ber neuere gesetzeswidrige Praktiken der deutschen und europ\"{a}ischen Patentjustiz sowie deren Pl\"{a}ne, Programmlogik und damit wirtschaftliche und gesellschaftliche Verfahren aller Art umfassend patentierbar zu machen.
\end{quote}
\filbreak
\end{itemize}

\begin{itemize}
\item
{\bf {\bf Remarks on the Patentability of Computer Software -- History, Status, Developments}\footnote{http://swpat.ffii.org/events/2001/linuxtag/jh/index.en.html}}

\begin{quote}
An essay by Jozef Halbersztadt, patent examiner at the Polish Patent Office, prepared for a lecture in Stutgart in July 2001.  The essay expresses his private views on the needs to accomodate the peculiarities of software by specially tailored exclusion/reward rights rather than by just adapting copyright or, even worse, patent law.  It narrates the history of software law as a chaotic evolution where the horses determined the direction of the cart until the cart ran against a wall and the driver woke up.  Halbersztadt proposes a relaunch of the Samuelson-Kappor-Davis Manifesto of 1994 with various optional modifications and ways to introduce those ideas into the current European legislation process.
\end{quote}
\filbreak

\item
{\bf {\bf Baldauf: Technical Inventing and Modelling -- Presentation Stuttgart 2001-06}\footnote{http://swpat.ffii.org/events/2001/linuxtag/xuan/index.de.html}}

\begin{quote}
Patents are granted for technical inventions, i.e. teachings for using controllable physical forces to immediately achieve a causally overseeable success.  In contrast to a \emph{rule of organisation and calculation}, a \emph{technical invention} stands and falls with the use of physical forces.  But what happens, if we already know the physical causalities and can reliably predict their behavior by mathematical models?  Does the progressing mathematisation gradually turn all problems that were once technical ones into problems of pure reason, thus making them unpatentable?  What remains for patenting?  Is there a meaningful difference between the modelling of matter (e.g. bridge statics) and the materialisation of models (e.g. universal computer) or does all patenting of functional logics lead to the same problems?  Xu\^{a}n Baldauf presents responses from the viewpoint of an informatician (software expert).
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

