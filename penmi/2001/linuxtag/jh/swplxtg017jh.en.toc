\contentsline {section}{\numberline {1}Why are these debates so difficult?}{3}{section.1}
\contentsline {section}{\numberline {2}What is beyond patents?}{3}{section.2}
\contentsline {section}{\numberline {3}What is special in software?}{4}{section.3}
\contentsline {section}{\numberline {4}The first sui generis software law proposal 1965.}{5}{section.4}
\contentsline {section}{\numberline {5}WIPO work 1974-1985}{5}{section.5}
\contentsline {section}{\numberline {6}Software copyright 1980-1994}{6}{section.6}
\contentsline {section}{\numberline {7}Chip Protection 1984-1989}{6}{section.7}
\contentsline {section}{\numberline {8}Manifesto 1994}{7}{section.8}
\contentsline {section}{\numberline {9}Software Act by Mark Paley 1996}{8}{section.9}
\contentsline {section}{\numberline {10}Chip sui generis and software sui generis}{8}{section.10}
\contentsline {section}{\numberline {11}Green Paper on the Patent System in Europe and other actions of the European Commission 1997-2001}{9}{section.11}
\contentsline {section}{\numberline {12}What can be done}{9}{section.12}
\contentsline {section}{\numberline {13}Table 1. Options of sui generis software law}{10}{section.13}
\contentsline {section}{\numberline {14}Table 2 Amendments to the European Patent Convention (after November 2000)}{11}{section.14}
\contentsline {section}{\numberline {15}References}{12}{section.15}
