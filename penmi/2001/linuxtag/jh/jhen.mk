jhen.lstex:
	lstex jhen | sort -u > jhen.lstex
jhen.mk:	jhen.lstex
	vcat /ul/prg/RC/texmake > jhen.mk


jhen.dvi:	jhen.mk jhen.tex
	latex jhen && while tail -n 20 jhen.log | grep references && latex jhen;do eval;done
	if test -r jhen.idx;then makeindex jhen && latex jhen;fi
jhen.pdf:	jhen.mk jhen.tex
	pdflatex jhen && while tail -n 20 jhen.log | grep references && pdflatex jhen;do eval;done
	if test -r jhen.idx;then makeindex jhen && pdflatex jhen;fi
jhen.tty:	jhen.dvi
	dvi2tty -q jhen > jhen.tty
jhen.ps:	jhen.dvi	
	dvips  jhen
jhen.001:	jhen.dvi
	rm -f jhen.[0-9][0-9][0-9]
	dvips -i -S 1  jhen
jhen.pbm:	jhen.ps
	pstopbm jhen.ps
jhen.gif:	jhen.ps
	pstogif jhen.ps
jhen.eps:	jhen.dvi
	dvips -E -f jhen > jhen.eps
jhen-01.g3n:	jhen.ps
	ps2fax n jhen.ps
jhen-01.g3f:	jhen.ps
	ps2fax f jhen.ps
jhen.ps.gz:	jhen.ps
	gzip < jhen.ps > jhen.ps.gz
jhen.ps.gz.uue:	jhen.ps.gz
	uuencode jhen.ps.gz jhen.ps.gz > jhen.ps.gz.uue
jhen.faxsnd:	jhen-01.g3n
	set -a;FAXRES=n;FILELIST=`echo jhen-??.g3n`;source faxsnd main
jhen_tex.ps:	
	cat jhen.tex | splitlong | coco | m2ps > jhen_tex.ps
jhen_tex.ps.gz:	jhen_tex.ps
	gzip < jhen_tex.ps > jhen_tex.ps.gz
jhen-01.pgm:
	cat jhen.ps | gs -q -sDEVICE=pgm -sOutputFile=jhen-%02d.pgm -
jhen/jhen.html:	jhen.dvi
	rm -fR jhen;latex2html jhen.tex
xview:	jhen.dvi
	xdvi -s 8 jhen &
tview:	jhen.tty
	browse jhen.tty 
gview:	jhen.ps
	ghostview  jhen.ps &
print:	jhen.ps
	lpr -s -h jhen.ps 
sprint:	jhen.001
	for F in jhen.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	jhen.faxsnd
	faxsndjob view jhen &
fsend:	jhen.faxsnd
	faxsndjob jobs jhen
viewgif:	jhen.gif
	xv jhen.gif &
viewpbm:	jhen.pbm
	xv jhen-??.pbm &
vieweps:	jhen.eps
	ghostview jhen.eps &	
clean:	jhen.ps
	rm -f  jhen-*.tex jhen.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} jhen-??.* jhen_tex.* jhen*~
tgz:	clean
	set +f;LSFILES=`cat jhen.ls???`;FILES=`ls jhen.* $$LSFILES | sort -u`;tar czvf jhen.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
