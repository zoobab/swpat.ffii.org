linuxtag-2001de.lstex:
	lstex linuxtag-2001de | sort -u > linuxtag-2001de.lstex
linuxtag-2001de.mk:	linuxtag-2001de.lstex
	vcat /ul/prg/RC/texmake > linuxtag-2001de.mk


linuxtag-2001de.dvi:	linuxtag-2001de.mk linuxtag-2001de.tex
	latex linuxtag-2001de && while tail -n 20 linuxtag-2001de.log | grep references && latex linuxtag-2001de;do eval;done
	if test -r linuxtag-2001de.idx;then makeindex linuxtag-2001de && latex linuxtag-2001de;fi
linuxtag-2001de.pdf:	linuxtag-2001de.mk linuxtag-2001de.tex
	pdflatex linuxtag-2001de && while tail -n 20 linuxtag-2001de.log | grep references && pdflatex linuxtag-2001de;do eval;done
	if test -r linuxtag-2001de.idx;then makeindex linuxtag-2001de && pdflatex linuxtag-2001de;fi
linuxtag-2001de.tty:	linuxtag-2001de.dvi
	dvi2tty -q linuxtag-2001de > linuxtag-2001de.tty
linuxtag-2001de.ps:	linuxtag-2001de.dvi	
	dvips  linuxtag-2001de
linuxtag-2001de.001:	linuxtag-2001de.dvi
	rm -f linuxtag-2001de.[0-9][0-9][0-9]
	dvips -i -S 1  linuxtag-2001de
linuxtag-2001de.pbm:	linuxtag-2001de.ps
	pstopbm linuxtag-2001de.ps
linuxtag-2001de.gif:	linuxtag-2001de.ps
	pstogif linuxtag-2001de.ps
linuxtag-2001de.eps:	linuxtag-2001de.dvi
	dvips -E -f linuxtag-2001de > linuxtag-2001de.eps
linuxtag-2001de-01.g3n:	linuxtag-2001de.ps
	ps2fax n linuxtag-2001de.ps
linuxtag-2001de-01.g3f:	linuxtag-2001de.ps
	ps2fax f linuxtag-2001de.ps
linuxtag-2001de.ps.gz:	linuxtag-2001de.ps
	gzip < linuxtag-2001de.ps > linuxtag-2001de.ps.gz
linuxtag-2001de.ps.gz.uue:	linuxtag-2001de.ps.gz
	uuencode linuxtag-2001de.ps.gz linuxtag-2001de.ps.gz > linuxtag-2001de.ps.gz.uue
linuxtag-2001de.faxsnd:	linuxtag-2001de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo linuxtag-2001de-??.g3n`;source faxsnd main
linuxtag-2001de_tex.ps:	
	cat linuxtag-2001de.tex | splitlong | coco | m2ps > linuxtag-2001de_tex.ps
linuxtag-2001de_tex.ps.gz:	linuxtag-2001de_tex.ps
	gzip < linuxtag-2001de_tex.ps > linuxtag-2001de_tex.ps.gz
linuxtag-2001de-01.pgm:
	cat linuxtag-2001de.ps | gs -q -sDEVICE=pgm -sOutputFile=linuxtag-2001de-%02d.pgm -
linuxtag-2001de/linuxtag-2001de.html:	linuxtag-2001de.dvi
	rm -fR linuxtag-2001de;latex2html linuxtag-2001de.tex
xview:	linuxtag-2001de.dvi
	xdvi -s 8 linuxtag-2001de &
tview:	linuxtag-2001de.tty
	browse linuxtag-2001de.tty 
gview:	linuxtag-2001de.ps
	ghostview  linuxtag-2001de.ps &
print:	linuxtag-2001de.ps
	lpr -s -h linuxtag-2001de.ps 
sprint:	linuxtag-2001de.001
	for F in linuxtag-2001de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	linuxtag-2001de.faxsnd
	faxsndjob view linuxtag-2001de &
fsend:	linuxtag-2001de.faxsnd
	faxsndjob jobs linuxtag-2001de
viewgif:	linuxtag-2001de.gif
	xv linuxtag-2001de.gif &
viewpbm:	linuxtag-2001de.pbm
	xv linuxtag-2001de-??.pbm &
vieweps:	linuxtag-2001de.eps
	ghostview linuxtag-2001de.eps &	
clean:	linuxtag-2001de.ps
	rm -f  linuxtag-2001de-*.tex linuxtag-2001de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} linuxtag-2001de-??.* linuxtag-2001de_tex.* linuxtag-2001de*~
tgz:	clean
	set +f;LSFILES=`cat linuxtag-2001de.ls???`;FILES=`ls linuxtag-2001de.* $$LSFILES | sort -u`;tar czvf linuxtag-2001de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
