linuxtag-2001en.lstex:
	lstex linuxtag-2001en | sort -u > linuxtag-2001en.lstex
linuxtag-2001en.mk:	linuxtag-2001en.lstex
	vcat /ul/prg/RC/texmake > linuxtag-2001en.mk


linuxtag-2001en.dvi:	linuxtag-2001en.mk linuxtag-2001en.tex
	latex linuxtag-2001en && while tail -n 20 linuxtag-2001en.log | grep references && latex linuxtag-2001en;do eval;done
	if test -r linuxtag-2001en.idx;then makeindex linuxtag-2001en && latex linuxtag-2001en;fi
linuxtag-2001en.pdf:	linuxtag-2001en.mk linuxtag-2001en.tex
	pdflatex linuxtag-2001en && while tail -n 20 linuxtag-2001en.log | grep references && pdflatex linuxtag-2001en;do eval;done
	if test -r linuxtag-2001en.idx;then makeindex linuxtag-2001en && pdflatex linuxtag-2001en;fi
linuxtag-2001en.tty:	linuxtag-2001en.dvi
	dvi2tty -q linuxtag-2001en > linuxtag-2001en.tty
linuxtag-2001en.ps:	linuxtag-2001en.dvi	
	dvips  linuxtag-2001en
linuxtag-2001en.001:	linuxtag-2001en.dvi
	rm -f linuxtag-2001en.[0-9][0-9][0-9]
	dvips -i -S 1  linuxtag-2001en
linuxtag-2001en.pbm:	linuxtag-2001en.ps
	pstopbm linuxtag-2001en.ps
linuxtag-2001en.gif:	linuxtag-2001en.ps
	pstogif linuxtag-2001en.ps
linuxtag-2001en.eps:	linuxtag-2001en.dvi
	dvips -E -f linuxtag-2001en > linuxtag-2001en.eps
linuxtag-2001en-01.g3n:	linuxtag-2001en.ps
	ps2fax n linuxtag-2001en.ps
linuxtag-2001en-01.g3f:	linuxtag-2001en.ps
	ps2fax f linuxtag-2001en.ps
linuxtag-2001en.ps.gz:	linuxtag-2001en.ps
	gzip < linuxtag-2001en.ps > linuxtag-2001en.ps.gz
linuxtag-2001en.ps.gz.uue:	linuxtag-2001en.ps.gz
	uuencode linuxtag-2001en.ps.gz linuxtag-2001en.ps.gz > linuxtag-2001en.ps.gz.uue
linuxtag-2001en.faxsnd:	linuxtag-2001en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo linuxtag-2001en-??.g3n`;source faxsnd main
linuxtag-2001en_tex.ps:	
	cat linuxtag-2001en.tex | splitlong | coco | m2ps > linuxtag-2001en_tex.ps
linuxtag-2001en_tex.ps.gz:	linuxtag-2001en_tex.ps
	gzip < linuxtag-2001en_tex.ps > linuxtag-2001en_tex.ps.gz
linuxtag-2001en-01.pgm:
	cat linuxtag-2001en.ps | gs -q -sDEVICE=pgm -sOutputFile=linuxtag-2001en-%02d.pgm -
linuxtag-2001en/linuxtag-2001en.html:	linuxtag-2001en.dvi
	rm -fR linuxtag-2001en;latex2html linuxtag-2001en.tex
xview:	linuxtag-2001en.dvi
	xdvi -s 8 linuxtag-2001en &
tview:	linuxtag-2001en.tty
	browse linuxtag-2001en.tty 
gview:	linuxtag-2001en.ps
	ghostview  linuxtag-2001en.ps &
print:	linuxtag-2001en.ps
	lpr -s -h linuxtag-2001en.ps 
sprint:	linuxtag-2001en.001
	for F in linuxtag-2001en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	linuxtag-2001en.faxsnd
	faxsndjob view linuxtag-2001en &
fsend:	linuxtag-2001en.faxsnd
	faxsndjob jobs linuxtag-2001en
viewgif:	linuxtag-2001en.gif
	xv linuxtag-2001en.gif &
viewpbm:	linuxtag-2001en.pbm
	xv linuxtag-2001en-??.pbm &
vieweps:	linuxtag-2001en.eps
	ghostview linuxtag-2001en.eps &	
clean:	linuxtag-2001en.ps
	rm -f  linuxtag-2001en-*.tex linuxtag-2001en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} linuxtag-2001en-??.* linuxtag-2001en_tex.* linuxtag-2001en*~
tgz:	clean
	set +f;LSFILES=`cat linuxtag-2001en.ls???`;FILES=`ls linuxtag-2001en.* $$LSFILES | sort -u`;tar czvf linuxtag-2001en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
