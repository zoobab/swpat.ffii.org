\contentsline {section}{\numberline {1}General Data}{4}{section.1}
\contentsline {section}{\numberline {2}Session protocol}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Einleitungsworte des Gespr\"achsleiters MdB J\"org Tauss}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Probst: innovationsf\"ordernde Wirkungen von Swpat nicht zu erkennen}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Bremer: EPA-Rechtsprechung sollte im Gesetz etabliert werden, KMU profitieren von Softwarepatenten}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Schiuma: Urheberrecht unzureichend, KMU brauchen Swpat, TRIPS erfordert Gesetzes\"anderung}{11}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Siepmann: Urheberrecht leistet f\"ur Software das, was Patente f\"ur Pharma leisten}{12}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Kiesewetter-K\"obinger: Ungereimtheiten bei der Patentpr\"ufung von Datenverarbeitungsprogrammen}{14}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Bouillon: Patente gef\"ahrden innovative KMU im Bereich der propriet\"aren Software}{15}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Henckel: Quelloffene Software besonders gef\"ahrdet}{17}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Lutterbeck: \"Okonomen gegen Swpat, viele Juristen daf\"ur}{18}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}MdB Tauss: Themenkomplexe der heutigen Diskussion}{20}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}MdB Mayer: Was sagen die swpat-freundlichen Studien zur Wirkung auf Innovation?}{20}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}MdB Griefahn: Ist Widerstand gegen US-Trends nicht zwecklos?}{22}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}MdB Otto: Fragen zum Unterschied US-DE, Urheberrecht-Patentrecht}{22}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}MdB Neumann: Gegen Swpat oder gegen Patente allgemein?}{23}{subsection.2.14}
\contentsline {subsection}{\numberline {2.15}MdB Kelber: Erfordert Bedeutung der Standardisierung nicht \"Anderungen im Patentrecht? Vertritt Bremer wirklich Meinung der Bitkom-Firmen?}{24}{subsection.2.15}
\contentsline {subsection}{\numberline {2.16}Schiuma: Patentpools gew\"ahrleisten, dass Standardisierung m\"oglich bleibt}{25}{subsection.2.16}
\contentsline {subsection}{\numberline {2.17}Lutterbeck: Begrenzung der Patentierbarkeit schwierig, Quelltextprivileg als L\"osung}{27}{subsection.2.17}
\contentsline {subsection}{\numberline {2.18}Probst: Zahlreiche Studien, nichts spricht f\"ur Swpat}{28}{subsection.2.18}
\contentsline {subsection}{\numberline {2.19}Bouillon: Urheberrecht angemessen, denn die Leistung liegt mehr im Werk als in der Einzelidee}{31}{subsection.2.19}
\contentsline {subsection}{\numberline {2.20}Bremer: Volkswirtschaftliche Studien nicht aussagekr\"aftig, OpenSource-Lager muss sich an Rechtslage anpassen, Schwierigkeiten gibt es sowieso auch beim Urheberrecht}{31}{subsection.2.20}
\contentsline {subsection}{\numberline {2.21}Siepmann: Auch aus rechtlicher Sicht spricht wenig f\"ur Softwarepatente und viel dagegen}{33}{subsection.2.21}
\contentsline {subsection}{\numberline {2.22}Schiuma: Kein Bedarf nach geringerer Patentlaufzeit, denn gut formulierte Patente wirken viel l\"anger als 1 Produktzyklus}{35}{subsection.2.22}
\contentsline {subsection}{\numberline {2.23}Tauss: Deutet die Unstetigkeit der Rechtsprechung auf Gesetzgebungsbedarf?}{36}{subsection.2.23}
\contentsline {subsection}{\numberline {2.24}MdB Otto: Freistellung von OpenSource-Software als L\"osung?}{37}{subsection.2.24}
\contentsline {subsection}{\numberline {2.25}MdB Griefahn: Was ist trivial? Wo sind die Grenzen?}{37}{subsection.2.25}
\contentsline {subsection}{\numberline {2.26}Siepmann: R\"uckkehr zu EP\"U und fr\"uherer BGH-Rechtsprechung ohne weiteres m\"oglich}{38}{subsection.2.26}
\contentsline {subsection}{\numberline {2.27}Schiuma: Heutige Rechtsprechung klar und sinnvoll, Gesetzgebung von 1973 veraltet und irref\"uhrend}{38}{subsection.2.27}
\contentsline {subsection}{\numberline {2.28}Lutterbeck: Juristische Diskussionen nutzlos, lieber an Quelltextprivileg und Verj\"ahrungsfristen denken}{38}{subsection.2.28}
\contentsline {subsection}{\numberline {2.29}Bremer: Status Quo = neuere Rechtsprechung + TRIPS, Gesetz muss angepasst werden}{40}{subsection.2.29}
\contentsline {subsection}{\numberline {2.30}Bouillon: Swpat grunds\"atzlich nicht sinnvoll, Laufzeitverk\"urzung und Wettbewerbsrechts\"anderung etc nur als Notbehelfe vorgeschlagen}{40}{subsection.2.30}
\contentsline {subsection}{\numberline {2.31}Kiesewetter-K\"obinger: Schwierigkeiten der Neuheitspr\"ufung bei Softwarepatenten}{41}{subsection.2.31}
\contentsline {subsection}{\numberline {2.32}Tauss: Auswirkungen auf Produktivit\"at, IT-Sicherheit etc}{42}{subsection.2.32}
\contentsline {subsection}{\numberline {2.33}Lutterbeck: IT-Sicherheit erfordert Freiraum f\"ur OpenSource}{43}{subsection.2.33}
\contentsline {subsection}{\numberline {2.34}Henckel: Swpat f\"ordern Abh\"angigkeit von Verk\"aufern intransparenter Informationssysteme}{44}{subsection.2.34}
\contentsline {subsection}{\numberline {2.35}MdB Mayer: Werden kleine Entwicklergruppen behindert?}{45}{subsection.2.35}
\contentsline {subsection}{\numberline {2.36}Kieswetter-K\"obinger: Patentdrohung demotiviert Entwickler und Unternehmensgr\"under}{45}{subsection.2.36}
\contentsline {subsection}{\numberline {2.37}Siepmann: Gefahr schleichender Abw\"urgung der freien Software, Quelltextprivileg gen\"ugt nicht}{46}{subsection.2.37}
\contentsline {subsection}{\numberline {2.38}MdB Mayer: M\"usste patentierte Software nicht grunds\"atzlich offengelegt werden?}{47}{subsection.2.38}
\contentsline {subsection}{\numberline {2.39}Schiuma: Patentbeschreibungen zur Offenbarung von Funktionalit\"aten geeigneter als Programm-Quelltexte}{47}{subsection.2.39}
\contentsline {subsection}{\numberline {2.40}Lutterbeck: Gesetzgebung sollte weltf\"uhrende Stellung Europas und Deutschlands im Bereich der Quelloffenen Software wiederspiegeln}{48}{subsection.2.40}
\contentsline {subsection}{\numberline {2.41}Henckel: Swpat untergraben Urheberrecht, enteignen Software-Autoren}{49}{subsection.2.41}
\contentsline {subsection}{\numberline {2.42}Bouillon: Wir Entwickler f\"uhlen uns enteignet}{49}{subsection.2.42}
\contentsline {subsection}{\numberline {2.43}Kiesewetter-K\"obinger: Quelltext einerseits f\"ur Pr\"ufer nicht bew\"altigbar, andererseits f\"ur seri\"ose Patentpr\"ufung unabdingbar}{50}{subsection.2.43}
\contentsline {subsection}{\numberline {2.44}Siepmann: Quelltext verst\"andlicher als Patentsprache, Beispiel IDEA}{50}{subsection.2.44}
\contentsline {subsection}{\numberline {2.45}Bremer: Existierende Rechte m\"ussen aufrechterhalten werden und OpenSource-Bewegung wird es \"uberleben}{51}{subsection.2.45}
\contentsline {subsection}{\numberline {2.46}Probst: Gesamtwirtschaftliches Interesse sollte entscheidend sein}{51}{subsection.2.46}
\contentsline {subsection}{\numberline {2.47}Schlussworte des Gespr\"achsleiters}{52}{subsection.2.47}
\contentsline {section}{\numberline {3}Consultation Questions and possible answers}{52}{section.3}
\contentsline {subsection}{\numberline {3.1}What is the rationale for patent protection in general?}{52}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}What protection rights exist for software and how do they work?}{52}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}How is software different from other inventions?}{53}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}What are software patents?}{53}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}How is the inventive height (non-obviousness) of software patents to be assessed?}{54}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Are software patents different from patents on business ideas, algorithms and file formats? If yes, in what way?}{54}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Where does the protection of software in Germany differ from that in other countries in and outside Europe?}{55}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}According to which criteria do the EPO, the Federal Court (BGH) and the Federal Patent Court (BPatG) judge the patentability of software? Are these criteria and rules unambiguous and which formulas have proven to be useful?}{55}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}How many software patents exist in Germany, Europe and worldwide?}{56}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}In how many of the 30000 software patents granted so far by the EPO is the claimed contribution to innovation so significant that it could considered a good deal for society to grant a 20 year monopoly on them?}{56}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Are there legal constraints (constitution, international treaties) that necessitate an extension of the patent system to software?}{56}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Art 14 GG: Eigentumsgarantie des Grundgesetzes}{56}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Art 27 TRIPS: Klausel gegen unsystematische Patentierbarkeitsausnahmen in einem Freihandelsvertrag}{57}{subsubsection.3.11.2}
\contentsline {subsection}{\numberline {3.12}How do you find the idea of introducing a special protection right (sui generis right) for software?}{59}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Where could copyright offer insufficient protection against imitation? What could a ``specially tailored software protection right'' look like?}{59}{subsection.3.13}
\contentsline {subsection}{\numberline {3.14}Does it make sense to distnguish patentable goods from non patentable ones?}{60}{subsection.3.14}
\contentsline {subsection}{\numberline {3.15}If yes, according to which criteria should the distinction between these two kinds of goods (kinds of inventions) be made?}{60}{subsection.3.15}
\contentsline {subsection}{\numberline {3.16}Which macro- and micro-economic effects would a largely unlimited patentability of so-called software products have?}{61}{subsection.3.16}
\contentsline {subsection}{\numberline {3.17}Which are the methods of work and economic foundations of the open source movement?}{61}{subsection.3.17}
\contentsline {subsection}{\numberline {3.18}What are the effects of extended patentability of software on the framework of software development under so-called open-source licenses?}{61}{subsection.3.18}
\contentsline {subsection}{\numberline {3.19}Would an extension of patent protection for software hamper the open source movement? If yes, how could these disadvantages be removed?}{61}{subsection.3.19}
\contentsline {subsection}{\numberline {3.20}Which effects would the extension of patent protection to software have on the development of small and medium enterprises?}{62}{subsection.3.20}
\contentsline {subsection}{\numberline {3.21}Do you think that an extension of patentability for software would endanger innovations in this area? How do you view the MIT study {\it Sequential Innovation, Patents and Imitation} by James Bessen\ and\ Eric Maskin (1999\ (http://www.researchoninnovation.org/)) in this context?}{63}{subsection.3.21}
\contentsline {subsection}{\numberline {3.22}Is patent law a suitable tool for promoting innovation and progress in the field of software?}{63}{subsection.3.22}
\contentsline {subsection}{\numberline {3.23}Are there more effective alternatives and, if yes, which? }{63}{subsection.3.23}
\contentsline {subsection}{\numberline {3.24}If it was now to be clarified at the EU level that software and abstract concepts are not patentable, how would this affect already existing patents in this area?}{63}{subsection.3.24}
\contentsline {section}{\numberline {4}Responses so far}{63}{section.4}
\contentsline {section}{\numberline {5}Further Reading}{67}{section.5}
