bundestag-2001de.lstex:
	lstex bundestag-2001de | sort -u > bundestag-2001de.lstex
bundestag-2001de.mk:	bundestag-2001de.lstex
	vcat /ul/prg/RC/texmake > bundestag-2001de.mk


bundestag-2001de.dvi:	bundestag-2001de.mk bundestag-2001de.tex
	latex bundestag-2001de && while tail -n 20 bundestag-2001de.log | grep references && latex bundestag-2001de;do eval;done
	if test -r bundestag-2001de.idx;then makeindex bundestag-2001de && latex bundestag-2001de;fi
bundestag-2001de.pdf:	bundestag-2001de.mk bundestag-2001de.tex
	pdflatex bundestag-2001de && while tail -n 20 bundestag-2001de.log | grep references && pdflatex bundestag-2001de;do eval;done
	if test -r bundestag-2001de.idx;then makeindex bundestag-2001de && pdflatex bundestag-2001de;fi
bundestag-2001de.tty:	bundestag-2001de.dvi
	dvi2tty -q bundestag-2001de > bundestag-2001de.tty
bundestag-2001de.ps:	bundestag-2001de.dvi	
	dvips  bundestag-2001de
bundestag-2001de.001:	bundestag-2001de.dvi
	rm -f bundestag-2001de.[0-9][0-9][0-9]
	dvips -i -S 1  bundestag-2001de
bundestag-2001de.pbm:	bundestag-2001de.ps
	pstopbm bundestag-2001de.ps
bundestag-2001de.gif:	bundestag-2001de.ps
	pstogif bundestag-2001de.ps
bundestag-2001de.eps:	bundestag-2001de.dvi
	dvips -E -f bundestag-2001de > bundestag-2001de.eps
bundestag-2001de-01.g3n:	bundestag-2001de.ps
	ps2fax n bundestag-2001de.ps
bundestag-2001de-01.g3f:	bundestag-2001de.ps
	ps2fax f bundestag-2001de.ps
bundestag-2001de.ps.gz:	bundestag-2001de.ps
	gzip < bundestag-2001de.ps > bundestag-2001de.ps.gz
bundestag-2001de.ps.gz.uue:	bundestag-2001de.ps.gz
	uuencode bundestag-2001de.ps.gz bundestag-2001de.ps.gz > bundestag-2001de.ps.gz.uue
bundestag-2001de.faxsnd:	bundestag-2001de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo bundestag-2001de-??.g3n`;source faxsnd main
bundestag-2001de_tex.ps:	
	cat bundestag-2001de.tex | splitlong | coco | m2ps > bundestag-2001de_tex.ps
bundestag-2001de_tex.ps.gz:	bundestag-2001de_tex.ps
	gzip < bundestag-2001de_tex.ps > bundestag-2001de_tex.ps.gz
bundestag-2001de-01.pgm:
	cat bundestag-2001de.ps | gs -q -sDEVICE=pgm -sOutputFile=bundestag-2001de-%02d.pgm -
bundestag-2001de/bundestag-2001de.html:	bundestag-2001de.dvi
	rm -fR bundestag-2001de;latex2html bundestag-2001de.tex
xview:	bundestag-2001de.dvi
	xdvi -s 8 bundestag-2001de &
tview:	bundestag-2001de.tty
	browse bundestag-2001de.tty 
gview:	bundestag-2001de.ps
	ghostview  bundestag-2001de.ps &
print:	bundestag-2001de.ps
	lpr -s -h bundestag-2001de.ps 
sprint:	bundestag-2001de.001
	for F in bundestag-2001de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	bundestag-2001de.faxsnd
	faxsndjob view bundestag-2001de &
fsend:	bundestag-2001de.faxsnd
	faxsndjob jobs bundestag-2001de
viewgif:	bundestag-2001de.gif
	xv bundestag-2001de.gif &
viewpbm:	bundestag-2001de.pbm
	xv bundestag-2001de-??.pbm &
vieweps:	bundestag-2001de.eps
	ghostview bundestag-2001de.eps &	
clean:	bundestag-2001de.ps
	rm -f  bundestag-2001de-*.tex bundestag-2001de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} bundestag-2001de-??.* bundestag-2001de_tex.* bundestag-2001de*~
tgz:	clean
	set +f;LSFILES=`cat bundestag-2001de.ls???`;FILES=`ls bundestag-2001de.* $$LSFILES | sort -u`;tar czvf bundestag-2001de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
