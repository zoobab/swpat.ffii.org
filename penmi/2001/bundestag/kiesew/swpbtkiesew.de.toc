\contentsline {section}{\numberline {1}Allgemeine Grundlagen und Ausgangslage}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Wie wird Patentschutz im Allgemeinen begr\"{u}ndet?}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Welche Schutzrechte gibt es f\"{u}r Software und welche Wirkungen haben sie?}{5}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Urheberrechtsschutz}{5}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Patentrechtsschutz}{6}{subsubsection.1.2.2}
\contentsline {subsection}{\numberline {1.3}Was unterscheidet Software von anderen Erfindungen?}{8}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Was sind Softwarepatente, was bedeutet der Begriff der Technizit\"{a}t von Softwarepatenten und wie sind Softwarepatente hinsichtlich der Erfindungsh\"{o}he zu beurteilen?}{10}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}``Softwarepatente''}{10}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Technizit\"{a}t bei Programmen f\"{u}r Datenverarbeitungsanlagen}{11}{subsubsection.1.4.2}
\contentsline {subsubsection}{\numberline {1.4.3}Erfindungsh\"{o}he}{12}{subsubsection.1.4.3}
\contentsline {subsection}{\numberline {1.5}Unterscheiden sich Softwarepatente von Patenten f\"{u}r Gesch\"{a}ftsideen, Algorithmen und Dateiformaten und wenn ja, wie?}{15}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Welche Unterschiede gibt es beim Schutz von Software in Deutschland im Vergleich zu anderen EU-L\"{a}ndern und im internationalen Vergleich?}{16}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Nach welchen Kriterien beurteilen EPA, BGH und BPatG heute die Patentierbarkeit von Software? Sind diese Kriterien und Regeln eindeutig und welche Formulierung erweist sich bisher am praktikabelsten?}{17}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Wie viele Patente f\"{u}r Software gibt es in Deutschland, Europa und weltweit?}{18}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Bei wie viel Prozent der bislang vom EPA gew\"{a}hrten 30.000 Softwarepatente ist die beanspruchte Innovation so bedeutend, dass es sich f\"{u}r die Gesellschaft ann\"{a}hernd lohnen k\"{o}nnte, darauf ein 20j\"{a}hriges Monopol zu vergeben?}{18}{subsection.1.9}
\contentsline {section}{\numberline {2}\"{A}nderungsbedarf}{19}{section.2}
\contentsline {subsection}{\numberline {2.1}Gibt es zwingende rechtliche Gr\"{u}nde (Verfassung, Internationale Vertr\"{a}ge), die eine Neuregelung des Patentwesens auf Software erforderlich machen?}{19}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wie beurteilen Sie die Einf\"{u}hrung eines eigenen Schutzrechtes f\"{u}r Software?}{22}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Wo k\"{o}nnte es beim Urheberrecht Unzul\"{a}ngichkeiten hinsichtlich des hinreichenden Schutzes von Software vor Nachahmung geben? Wie k\"{o}nnte ein ``ma{\ss }geschneidertes Software-Schutzrecht'' aussehen?}{23}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Wie beurteilen Sie die Einf\"{u}hrung von Sondervorschriften f\"{u}r Software im Patentgesetz?}{23}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Macht eine Differenzierung patentierbarer und nichtpatentierbarer G\"{u}ter Sinn? Wenn ja, nach welchen Kriterien sollte Ihrer Ansicht nach zwischen beiden G\"{u}terarten (Erfindungsarten) unterschieden werden?}{25}{subsection.2.5}
\contentsline {section}{\numberline {3}Auswirkungen}{26}{section.3}
\contentsline {subsection}{\numberline {3.1}Welche volkswirtschaftlichen und betriebswirtschaftlichen Effekte h\"{a}tte ihrer Meinung nach eine weitestgehend unreglementierte Patentierbarkeit von sog. Softwareprodukten? Worauf beruhen diese Annahmen?}{26}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Welches sind die Arbeitsmethoden und wirtschaftlichen Grundlagen der Open-Source-Bewegung?}{26}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Welche Auswirkungen erweiterter Patentierbarkeit von Software erwarten Sie auf die Rahmenbedingungen der Softwareentwicklung unter sog. Open-Source-Lizenzen?}{27}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}W\"{u}rde eine Ausdehnung des Patentschutzes f\"{u}r Software die Open-Source-Bewegung behindern? Wenn ja, wie k\"{o}nnten diese Nachteile beseitigt werden?}{27}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Welche Auswirkungen h\"{a}tte die Ausdehnung des Patentschutzes f\"{u}r Software auf die Entwicklung von kleinen und mittelst\"{a}ndischen Unternehmen?}{27}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Sind Sie der Meinung, dass eine Ausdehnung des Patentschutzes f\"{u}r Software Innovationen in dieser Branche gef\"{a}hrden w\"{u}rden? Wie beurteilen Sie in diesem Zusammenhang die MIT-Studie ``Sequential innovation, patents and imitation'' von James Bessen und Eric Maskin (1999)?}{28}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Ist das Patentrecht aus Ihrer Sicht das geeignete Werkzeug, um im Softwarebereich Innovation und Fortschritt zu f\"{o}rdern? Sind effektivere Alternativen denkbar, und wenn ja, welche?}{29}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Wie wirkt sich Ihrer Ansicht nach eine Klarstellung auf EU-Ebene dahingehend, dass Software und gedankliche Konzepte nicht patentierbar sind, auf bereits bestehende Patente in diesem Bereich aus?}{29}{subsection.3.8}
\contentsline {section}{\numberline {4}Bibliography}{30}{section.4}
