bundestag-2001fr.lstex:
	lstex bundestag-2001fr | sort -u > bundestag-2001fr.lstex
bundestag-2001fr.mk:	bundestag-2001fr.lstex
	vcat /ul/prg/RC/texmake > bundestag-2001fr.mk


bundestag-2001fr.dvi:	bundestag-2001fr.mk bundestag-2001fr.tex
	latex bundestag-2001fr && while tail -n 20 bundestag-2001fr.log | grep references && latex bundestag-2001fr;do eval;done
	if test -r bundestag-2001fr.idx;then makeindex bundestag-2001fr && latex bundestag-2001fr;fi
bundestag-2001fr.pdf:	bundestag-2001fr.mk bundestag-2001fr.tex
	pdflatex bundestag-2001fr && while tail -n 20 bundestag-2001fr.log | grep references && pdflatex bundestag-2001fr;do eval;done
	if test -r bundestag-2001fr.idx;then makeindex bundestag-2001fr && pdflatex bundestag-2001fr;fi
bundestag-2001fr.tty:	bundestag-2001fr.dvi
	dvi2tty -q bundestag-2001fr > bundestag-2001fr.tty
bundestag-2001fr.ps:	bundestag-2001fr.dvi	
	dvips  bundestag-2001fr
bundestag-2001fr.001:	bundestag-2001fr.dvi
	rm -f bundestag-2001fr.[0-9][0-9][0-9]
	dvips -i -S 1  bundestag-2001fr
bundestag-2001fr.pbm:	bundestag-2001fr.ps
	pstopbm bundestag-2001fr.ps
bundestag-2001fr.gif:	bundestag-2001fr.ps
	pstogif bundestag-2001fr.ps
bundestag-2001fr.eps:	bundestag-2001fr.dvi
	dvips -E -f bundestag-2001fr > bundestag-2001fr.eps
bundestag-2001fr-01.g3n:	bundestag-2001fr.ps
	ps2fax n bundestag-2001fr.ps
bundestag-2001fr-01.g3f:	bundestag-2001fr.ps
	ps2fax f bundestag-2001fr.ps
bundestag-2001fr.ps.gz:	bundestag-2001fr.ps
	gzip < bundestag-2001fr.ps > bundestag-2001fr.ps.gz
bundestag-2001fr.ps.gz.uue:	bundestag-2001fr.ps.gz
	uuencode bundestag-2001fr.ps.gz bundestag-2001fr.ps.gz > bundestag-2001fr.ps.gz.uue
bundestag-2001fr.faxsnd:	bundestag-2001fr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo bundestag-2001fr-??.g3n`;source faxsnd main
bundestag-2001fr_tex.ps:	
	cat bundestag-2001fr.tex | splitlong | coco | m2ps > bundestag-2001fr_tex.ps
bundestag-2001fr_tex.ps.gz:	bundestag-2001fr_tex.ps
	gzip < bundestag-2001fr_tex.ps > bundestag-2001fr_tex.ps.gz
bundestag-2001fr-01.pgm:
	cat bundestag-2001fr.ps | gs -q -sDEVICE=pgm -sOutputFile=bundestag-2001fr-%02d.pgm -
bundestag-2001fr/bundestag-2001fr.html:	bundestag-2001fr.dvi
	rm -fR bundestag-2001fr;latex2html bundestag-2001fr.tex
xview:	bundestag-2001fr.dvi
	xdvi -s 8 bundestag-2001fr &
tview:	bundestag-2001fr.tty
	browse bundestag-2001fr.tty 
gview:	bundestag-2001fr.ps
	ghostview  bundestag-2001fr.ps &
print:	bundestag-2001fr.ps
	lpr -s -h bundestag-2001fr.ps 
sprint:	bundestag-2001fr.001
	for F in bundestag-2001fr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	bundestag-2001fr.faxsnd
	faxsndjob view bundestag-2001fr &
fsend:	bundestag-2001fr.faxsnd
	faxsndjob jobs bundestag-2001fr
viewgif:	bundestag-2001fr.gif
	xv bundestag-2001fr.gif &
viewpbm:	bundestag-2001fr.pbm
	xv bundestag-2001fr-??.pbm &
vieweps:	bundestag-2001fr.eps
	ghostview bundestag-2001fr.eps &	
clean:	bundestag-2001fr.ps
	rm -f  bundestag-2001fr-*.tex bundestag-2001fr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} bundestag-2001fr-??.* bundestag-2001fr_tex.* bundestag-2001fr*~
tgz:	clean
	set +f;LSFILES=`cat bundestag-2001fr.ls???`;FILES=`ls bundestag-2001fr.* $$LSFILES | sort -u`;tar czvf bundestag-2001fr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
