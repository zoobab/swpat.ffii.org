\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}Renseignements Pratiques}{1}{section.0.1}
\contentsline {section}{\numberline {2}protocole de session}{2}{section.0.2}
\contentsline {subsection}{\numberline {2.1}Introduction par M. Tauss}{2}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Probst\@DP Effets N\'{e}fastes des Brevets sur l'\'{E}conomie du Logiciel}{5}{subsection.0.2.2}
\contentsline {subsection}{\numberline {2.3}Bremer\@DP Le Droit d'Auteur ne Refl\`{e}te pas la Technicit\'{e} du Logiciel}{6}{subsection.0.2.3}
\contentsline {subsection}{\numberline {2.4}Schiuma\@DP Protection par Droit d'Auteur Insufisante, TRIPs n\'{e}c\'{e}ssite un changement}{7}{subsection.0.2.4}
\contentsline {subsection}{\numberline {2.5}Siepmann\@DP Le Droit d'Auteur Acheve pour le Logiciel ce que achevent les brevets pour l'industrie pharma}{8}{subsection.0.2.5}
\contentsline {subsection}{\numberline {2.6}Kiesewetter-K\"{o}binger\@DP Incompatibilit\'{e} du Brevet avec le Logiciel}{10}{subsection.0.2.6}
\contentsline {subsection}{\numberline {2.7}Bouillon\@DP Les brevets mettent en p\'{e}ril les PME innovantes dans le domaine du logiciel propri\'{e}taire}{12}{subsection.0.2.7}
\contentsline {subsection}{\numberline {2.8}Henckel\@DP Logiciel Libre en Danger}{13}{subsection.0.2.8}
\contentsline {subsection}{\numberline {2.9}Lutterbeck\@DP \'{E}conomistes contre le Brevet logiciel, Avocats en faveur}{15}{subsection.0.2.9}
\contentsline {subsection}{\numberline {2.10}MdB Tauss\@DP Th\`{e}mes de la Discussion}{16}{subsection.0.2.10}
\contentsline {subsection}{\numberline {2.11}MdB Mayer\@DP Que disent les \'{e}tudent pro-brevet a propos de l'\'{e}ffet sur l'innovation\@PI }{17}{subsection.0.2.11}
\contentsline {subsection}{\numberline {2.12}MdB Griefahn\@DP N'est pas la Resistance contre les Tendances US sans espoir\@PI }{18}{subsection.0.2.12}
\contentsline {subsection}{\numberline {2.13}MdB Otto\@DP Questions sur Diff\'{e}rence US et EU, Droit d'Auteur et Brevets}{18}{subsection.0.2.13}
\contentsline {subsection}{\numberline {2.14}MdB Neumann\@DP Contre les Brevets Logiciels ou Contre Touts Les Brevets\@PI }{19}{subsection.0.2.14}
\contentsline {subsection}{\numberline {2.15}MdB Kelber\@DP Conflit entre Standardisation et Brevetabilit\'{e}\@PI Est-ce que Bremer repr\'{e}sente le consus des membres de Bitkom\@PI }{20}{subsection.0.2.15}
\contentsline {subsection}{\numberline {2.16}Schiuma\@DP La Auto-R\'{e}gulation par des Consortiums de Brevet assure que les Standards restent ouverts}{21}{subsection.0.2.16}
\contentsline {subsection}{\numberline {2.17}Lutterbeck\@DP La Brevetabilit\'{e} ne peut pas \^{e}tre Limit\'{e}e, il faut un privil\`{e}ge pour code source}{23}{subsection.0.2.17}
\contentsline {subsection}{\numberline {2.18}Probst\@DP Nombreuses \'{E}tudes \'{E}conomiques, Aucun Soutien pour les Brevets Logiciels}{24}{subsection.0.2.18}
\contentsline {subsection}{\numberline {2.19}Bouillon\@DP Le Droit d'Auteur est Bien Adapt\'{e}, car l'Effort est dans l'ensemble plut\^{o}t que dans les id\'{e}es}{27}{subsection.0.2.19}
\contentsline {subsection}{\numberline {2.20}Bremer\@DP Les \'{E}tudes \'{E}conomiques ne Prouvent Rien, les Mouvement Opensource ne comprend pas la Loi}{27}{subsection.0.2.20}
\contentsline {subsection}{\numberline {2.21}Siepmann\@DP D'un point de vue l\'{e}gal il y a beaucoup d'argument contre le brevet logiciel et peu pour}{29}{subsection.0.2.21}
\contentsline {subsection}{\numberline {2.22}Schiuma\@DP Aucun besoin de reduire la dur\'{e}e des brevets car les revendications bien \'{e}crites durent plus longtemps que 1 cycle de produit}{31}{subsection.0.2.22}
\contentsline {subsection}{\numberline {2.23}Tauss\@DP Est-ce que la Volatilit\'{e} de la Jurisdiction indique une n\'{e}c\'{e}ssit\'{e} de L\'{e}gislation\@PI }{32}{subsection.0.2.23}
\contentsline {subsection}{\numberline {2.24}MdB Otto\@DP Est-ce qu'on pourrait resoudre le conflit en cr\'{e}ant une exception pour le Logiciel Libre\@PI }{32}{subsection.0.2.24}
\contentsline {subsection}{\numberline {2.25}MdB Griefahn\@DP Qu'est-ce que c'est que ``trivial''\@PI Ou sont les limites\@PI }{33}{subsection.0.2.25}
\contentsline {subsection}{\numberline {2.26}Siepmann\@DP Retour a la loi (CBE) et la jurisdiction ant\'{e}rieure reste possible sans difficult\'{e}}{34}{subsection.0.2.26}
\contentsline {subsection}{\numberline {2.27}Schiuma\@DP La nouvelle Jurisdiction est claire, la loi de 1973 est cause de malentendus}{34}{subsection.0.2.27}
\contentsline {subsection}{\numberline {2.28}Lutterbeck\@DP Discussions l\'{e}gales sans m\'{e}rites, il faut penses au privil\'{e}ge code source et au questions de dur\'{e}e}{37}{subsection.0.2.28}
\contentsline {subsection}{\numberline {2.29}Bremer\@DP Status Quo = jurisdiction nouvelle + TRIPs, la loi doit \^{e}tre adapt\'{e}e}{38}{subsection.0.2.29}
\contentsline {subsection}{\numberline {2.30}Bouillon\@DP Les brevets logiciels sont auf fond unjuistifi\'{e}s, en jouant avec la longeur ou les r\`{e}gles de comp\'{e}tition on n'addresse pas le probl\`{e}me}{39}{subsection.0.2.30}
\contentsline {subsection}{\numberline {2.31}Kiesewetter-K\"{o}binger\@DP Difficult\'{e}s dans l'Examen de Nouveaut\'{e} de Brevets Logiciels}{40}{subsection.0.2.31}
\contentsline {subsection}{\numberline {2.32}Tauss\@DP Eff\`{e}ts sur la Productivit\'{e}, S\'{e}curit\'{e} etc}{41}{subsection.0.2.32}
\contentsline {subsection}{\numberline {2.33}Lutterbeck\@DP Securit\'{e} des Reseaux exige un Espace Libre pour les Logiciel Libre}{41}{subsection.0.2.33}
\contentsline {subsection}{\numberline {2.34}Henckel\@DP Brevets Logiciels renforce la D\'{e}pendance sur de Solutions Propri\'{e}taires Ins\'{e}cures}{42}{subsection.0.2.34}
\contentsline {subsection}{\numberline {2.35}MdB Mayer\@DP Est-ce que les Petits Groupes de Developpeurs sont Affect\'{e}s\@PI }{43}{subsection.0.2.35}
\contentsline {subsection}{\numberline {2.36}Kieswetter-K\"{o}binger\@DP Menace de Brevets D\'{e}motivante pour les Developpeurs et les Fondateurs d'Entreprises}{43}{subsection.0.2.36}
\contentsline {subsection}{\numberline {2.37}Siepmann\@DP Danger de asphyxie progressive du Logiciel Libre, Privilege de Code Source ne suffit pas}{44}{subsection.0.2.37}
\contentsline {subsection}{\numberline {2.38}MdB Mayer\@DP Ne devrait-on pas en prinicipe ouvrir les sources des logiciels brevet\'{e}s\@PI }{45}{subsection.0.2.38}
\contentsline {subsection}{\numberline {2.39}Schiuma\@DP Les d\'{e}scriptions de brevet sont mieux adapt\'{e}es pour communiquer l'essence du logiciel que le code source}{45}{subsection.0.2.39}
\contentsline {subsection}{\numberline {2.40}Lutterbeck\@DP La l\'{e}gislation doit refl\`{e}ter la position leader de l'Europe et l'Allemagne dans le domaine du logiciel libre}{46}{subsection.0.2.40}
\contentsline {subsection}{\numberline {2.41}Henckel\@DP Brevets Logiciels verrouillent le Droit D'Auteur, exproprient les Auteurs de Logiciels}{47}{subsection.0.2.41}
\contentsline {subsection}{\numberline {2.42}Bouillon\@DP Nous, les Developpeurs, nous sentons expropri\'{e}s}{47}{subsection.0.2.42}
\contentsline {subsection}{\numberline {2.43}Kiesewetter-K\"{o}binger\@DP L'examination s\'{e}rieuse n\'{e}c\'{e}ssite l'\'{e}tude du code source, ce qui et d'autre part infaisable}{48}{subsection.0.2.43}
\contentsline {subsection}{\numberline {2.44}Siepmann\@DP Code Source plus comprenable que langue de brevets}{48}{subsection.0.2.44}
\contentsline {subsection}{\numberline {2.45}Bremer\@DP Les privil\`{e}ges d'inventeurs existantent doivent \^{e}tre maintenus, le mouvement du logiciel libre va survivre}{49}{subsection.0.2.45}
\contentsline {subsection}{\numberline {2.46}Probst\@DP L'Int\'{e}r\^{e}t G\'{e}n\'{e}ral \'{E}conomique doit \^{e}tre le Crit\`{e}re D\'{e}cisif}{49}{subsection.0.2.46}
\contentsline {subsection}{\numberline {2.47}Remarques Finales du Mod\'{e}rateur}{50}{subsection.0.2.47}
\contentsline {section}{\numberline {3}Questions de Consultation et reponses possibles}{50}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Pour quoi accorde-t-on des brevets en g\'{e}n\'{e}ral\@PI }{50}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Quelle protection juridique existe pour le logiciel et comment fonctionne-t-elle \@PI }{50}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}An quoi le logiciel est-il diff\'{e}rent des autres inventions\@PI }{51}{subsection.0.3.3}
\contentsline {subsection}{\numberline {3.4}Qu'est-ce qu'un brevet logiciel\@PI }{51}{subsection.0.3.4}
\contentsline {subsection}{\numberline {3.5}Comment doit-on juger de l'inventivit\'{e} des brevets logiciels \@PI }{51}{subsection.0.3.5}
\contentsline {subsection}{\numberline {3.6}Les brevets logiciels diff\`{e}rent-ils des brevets sur les m\'{e}thodes de gestion, les algorithmes et les formats de fichier \@PI Si oui, dans quel sens \@PI }{52}{subsection.0.3.6}
\contentsline {subsection}{\numberline {3.7}Quelle diff\'{e}rence y a-t-il dans le syst\`{e}me de droit de protection du logiciel en Allemagne et dans les autres pays \`{a} l'int\'{e}rieur et en-dehors de l'Europe \@PI }{52}{subsection.0.3.7}
\contentsline {subsection}{\numberline {3.8}Selon quels crit\`{e}res l'OEB, la Cour F\'{e}d\'{e}rale (BGH) et la Cour F\'{e}d\'{e}rale des Brevets (BPatG) jugent-ils de la brevetabilit\'{e} du logiciel \@PI Ces crit\`{e}res sont-ils sans ambigu\"{\i }t\'{e} et quelles ont \'{e}t\'{e} les formulations les plus utiles \@PI }{53}{subsection.0.3.8}
\contentsline {subsection}{\numberline {3.9}Combien y a-t-il de brevets logiciels en Allemagne, en Europe et dans le monde\@PI }{54}{subsection.0.3.9}
\contentsline {subsection}{\numberline {3.10}Dans combien de cas de brevets logiciels parmi les 30000 (environ) accord\'{e}s jusqu'au pr\'{e}sent par l'OEB la contribution \`{a} l'innovation est-elle suffisamment importante pour qu'on puisse consid\'{e}rer que la soci\'{e}t\'{e} y gagne en les r\'{e}mun\'{e}rant par un monopole de 20 ans \@PI }{54}{subsection.0.3.10}
\contentsline {subsection}{\numberline {3.11}Existe-t-il des contraintes d'ordre l\'{e}gal - constitution, trait\'{e}s internationaux - qui n\'{e}cessiteraient l'extension de la notion de brevetabilit\'{e} au domaine du logiciel \@PI }{54}{subsection.0.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Art 14 GG\@DP La Protection de la Propri\'{e}t\'{e} Priv\'{e}e contenue dans la Constitution }{54}{subsubsection.0.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Art 27 TRIPS\@DP Des clauses contraires aux exceptions \`{a} la brevetabilit\'{e} dans un contrat de libre \'{e}change}{55}{subsubsection.0.3.11.2}
\contentsline {subsection}{\numberline {3.12}Comment jugez vous l'id\'{e}e d'introduction d'un droit sp\'{e}cialis\'{e} (droit sui generis) pour le logiciel\@PI }{56}{subsection.0.3.12}
\contentsline {subsection}{\numberline {3.13}Et dans le domaine du droit d'auteur y a-t-il des insuffisances de protection contre l'imitation \@PI Quel devrait \^{e}tre le contenu d'un ``droit sui generis adapt\'{e} pour le logiciel'' \@PI }{57}{subsection.0.3.13}
\contentsline {subsection}{\numberline {3.14}Doit-on distinguer les biens brevetables et non-brevetables\&nbsp\@PV \@PI }{58}{subsection.0.3.14}
\contentsline {subsection}{\numberline {3.15}Si oui, selon quels crit\`{e}res doit-on distinguer entre ces deux types de biens (d'inventions)\@PI }{58}{subsection.0.3.15}
\contentsline {subsection}{\numberline {3.16}Quels seraient les effets macro- et micro-\'{e}conomiques d'une brevetabilit\'{e} sans limitation clairement \'{e}tablie des soi-disant produits logiciels\@PI }{58}{subsection.0.3.16}
\contentsline {subsection}{\numberline {3.17}Quels sont les m\'{e}thodes de travail et les fondements \'{e}conomiques du mouvement du logiciel libre \@PI }{58}{subsection.0.3.17}
\contentsline {subsection}{\numberline {3.18}Quels sont les effets d'une brevetabilit\'{e} \'{e}tendue du logiciel sous des licenses libres (opensource) \@PI }{59}{subsection.0.3.18}
\contentsline {subsection}{\numberline {3.19}Est-ce qu'une extension de la brevetabilit\'{e} au logiciel pourrait freiner le d\'{e}veloppement des logiciels libres \@PI Si oui, comment pourrait-on \'{e}viter ces inconv\'{e}nients\@PI }{59}{subsection.0.3.19}
\contentsline {subsection}{\numberline {3.20}Quels seraient les effets d'une extension de la brevetabilit\'{e} pour les petites et moyennes entreprises\@PI }{60}{subsection.0.3.20}
\contentsline {subsection}{\numberline {3.21}Le droit de brevet constitue-t-il selon vous un outil adapt\'{e} \`{a} la stimulation de l'innovation et du progr\`{e}s dans le domaine du logiciel\@PI }{60}{subsection.0.3.21}
\contentsline {subsection}{\numberline {3.22}Existe-t-il des alternatives plus efficaces et si oui, lesquelles\@PI }{60}{subsection.0.3.22}
\contentsline {subsection}{\numberline {3.23}Si demain entrait en vigueur une annulation au niveau de l'UE ayant pour effet une non brevetabilit\'{e} des logiciels ou autres concepts abstraits, en quoi cela affecterait-il les brevets d\'{e}j\`{a} accord\'{e}s dans ces secteurs \@PI }{61}{subsection.0.3.23}
\contentsline {section}{\numberline {4}R\'{e}ponses d\'{e}j\`{a} re\c {c}ues}{61}{section.0.4}
\contentsline {section}{\numberline {5}Autres Textes}{64}{section.0.5}
