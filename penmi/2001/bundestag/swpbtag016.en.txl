<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament

#descr: Eight experts from the areas of law, informatics and economics will
answer questions from MPs, based on written responses to a set of
questions.  The interested public is also called to present its
answers to any subset of these questions in writing.  We publish here
the procedings and submissions.

#Aen: General Data

#Sno: session protocol

#Due: Consultation Questions and possible answers

#Bit: Responses so far

#Wrk3: Further Reading

#Vnl: Conducted by

#AfW: Commission for Culture and Media

#Use: New Media Subcommission

#Rsc: Legal Affairs Commission

#Ort: Place

#Rsg: Reichstag  Building

#Par: Plenary Area

#SgW: Session Hall 2 S 023

#KEn: via southern entrance

#Zei: Time

#Dns: thursday

#Gve: moderator

#MJW: MdB Jörg Tauss

#Kts: Consultation Questions

#sWn: See below, compare also %(KF)

#Shg: Written Responses to

#obr: or paper mail to the above Bundestag address to the attention of Mr.
%{CS}

#gex: invited experts

#v5B: lawyer

#VtW: board member of %{PA}

#Pep: patent examiner

#DPM: German Patent Office

#Pnw: patent lawyer

#Rgl: Rules

#Dnf: The session is public.

#Doh: The invited experts are asked questions by members of parliament and
other persons invited by the moderators.

#DEW: Dieses Protokoll wurde bereits als Bundestagsdrucksache
veröffentlicht.  Es wird hier wörtlich übernommen.  Zur Erleichterung
der Lektüre wurden Überschriften und Verweise eingefügt.  Ferner
wurden ein paar offensichtliche Diktierfehler korrigiert, z.B.
%(q:IDEA) statt %(q:IDR), %(q:Gert Kolle) statt %(q:Gerhard Kolle),
%(q:Colonel) statt %(q:Kernel).

#Ede2: Introduction by the Moderator Jörg Tauss

#Vsn: Economists Skeptical about Effects of Software Patents

#BeU: Copyright does not adequately reflect Technicity of Software

#ScI: Copyright protection insufficient, TRIPs requires change of law

#SsW: Copyright achieves for Software what Patents achieve for Pharma

#UWn: Incompatibility of Patent Concepts with Software Concepts

#Par2: Patents endanger innovative SMEs in the field of proprietary software

#Qod: Open Source Software unduly threatened

#hWJ: Economists against Software Patents, lawyers in favor

#Txg: Themes of Today's Discussion

#WeW2: What do pro-swpat studies say about the effects on innovation

#IgW: Isn't Resistance to US Trends Futile?

#Fhr: Questions about Differences between US and EU, Copyright and Patents

#Gde: Against Software Patents or against all Patents?

#EiB: Conflict between Standardisation and Patentability?  Does Bremer
represent consensus of Bitkom members?

#Psi: Self-Regulation through Patent Pools Keeps Standards Open

#Aer: Patentability can't be limited, Solution lies in Source Code Privilege

#Zdp: Numerous Economic Studies, nothing in support of Software
Patentability

#Uni: Copyright is Adequate, because the effort resides in the complex
tissue rather than in single ideas

#Fre: The Economic Studies are Inconclusive, the Opensource Camp doesn't
understand the Law

#Ata: Most Legal Arguments work against Software Patentability

#Kfi: No need to reduce patent term, because well-written claims last much
longer than 1 product cycle

#Dtu: Does the Volatility of Caselaw indicate a need for Legislation?

#FWw: Could an OpenSource Exception be the Solution?

#Wid: What is %(q:trivial)?  Where are the limits?

#Rhg: Return to EPC and Earlier Caselaw possible without difficulties

#HnW: Today's Caselaw is Clear, Legislation of 1973 is outdated and
misleading

#Jog: Legal discussions pointless, better think about source code privileges
and expiration terms

#Stt: Status Quo = new caselaw + TRIPs, law must be adapted

#Szr: Software Patents principally unjustified, fiddling with expiry terms
and competition law is not a lasting solution

#Srb: Difficulties in Novelty Examination of Software Patents

#AWI: Effects on Productivity, IT Security etc

#Iru: IT security demands freedom for opensource software

#Sta: Software Patents reinforce Dependence on Insecure Proprietary
Solutions

#WEp: Are Small Groups of Developpers Hampered?

#PvW: Patent Threat Demotivates Developpers and Entrepreneurs

#Gue2: Danger of gradual suffocation of Free Software, Source Code Privilege
not enough

#Mti: Shouldn't Patented Software be made open in principle?

#Ptl: Patent Descriptions better suited for disclosure of software
functionalities than source code

#Gnd: Legislation should reflect leading position of Europe and Germany in
the Area of Opensource Software

#Srn: Software Patents undermine Copyright, expropriate Software Authors

#Wlu: We, the Developpers, feel Expropriated

#Qir: Studying Source Code impossible but yet necessary for serious patent
examination

#Qia: Source code better understandable than patent language

#Ero: Existing inventor privileges must be upheld, Open Source Movement will
survive

#Ghe: Overall Economic Interest must be the Decisive Criterion

#Sev: Moderator's Concluding Remarks

#vors: president

#Mil: Meine sehr verehrten Damen und Herren, wir haben es bei uns so
eingeführt, die Pünktlichen nicht zu bestrafen, sondern eher die
Unpünktlichen zu beschämen, indem wir einfach pünktlich beginnen. Ich
darf Sie ganz herzlich zu unserem heutigen Fachgespräch
%(q:Softwarepatente/OpenSource) begrüßen, im Namen des
Unterausschusses für Neue Medien, aber zugleich auch im Namen unseres
Hauptausschusses für Kultur und Medien und seiner Vorsitzenden, der
Kollegin Monika Griefahn, sowie des Rechtsausschusses mit seinem
Vorsitzenden, dem Kollegen Rupert Scholz. Ich bin Vorsitzender des
Unterausschusses %(q:Neue Medien), der beim Hauptausschuss für Kultur
und Medien angesiedelt worden ist. Wir haben Sie heute gemeinsam
eingeladen mit unseren Kolleginnen und Kollegen des Rechtsausschusses,
der für die Frage der Softwarepatentierung, wie eigentlich für alle
Patentfragen, der federführende Ausschuss hier im Deutschen Bundestag
ist. Ich habe dem Vorsitzenden des federführenden Rechtsausschusses
angetragen, heute auch den Vorsitz zu führen, aber der Kollege Rupert
Scholz hat mir mitgeteilt, dass er dem frechsten Zwischenrufer im
Deutschen Bundestag - damit meinte er mich - dies zubillige; ich solle
dies auch in seinem Namen tun und gleichzeitig auch herzlich von ihm
grüßen.

#MWi: Meine sehr verehrten Damen und Herren, liebe Kollegen und Kolleginnen,
seit Jahrzehnten gibt es eine heftige Diskussion um Patentierungen
sogenannter Software-Erfindungen oder computerimplementierter
Erfindungen; dabei war es in der Vergangenheit so, dass die Positionen
sehr weit auseinandergelaufen sind. Insofern haben wir es mit einem
kontroversen Thema zu tun, sonst bräuchten wir auch keine Anhörung zu
machen. Die einen haben einer restriktiven Patentierungsregelung das
Wort geredet, die anderen sahen in einer solchen gar das Ende
europäischer Eigentumsordnungen oder der OpenSource-Bewegung in
Deutschland und in Europa heraufdämmern. Während die einen
Softwarepatenten große ökonomische Potentiale zuschreiben, erwarten
andere eher nachteilige wirtschaftliche Auswirkungen, auch nachteilige
Auswirkungen im Forschungsbereich. Die Rechtslage - wie auch die
Rechtspraxis - ist wohl alles andere als eindeutig. Wir haben sehr
unterschiedliche nationale Rechtsauffassungen. Wir haben heute auch
einen Wandel in den Rechtsauffassungen gegenüber den 70er Jahren zu
verzeichnen. Wir haben es verstärkt mit den europäischen
internationalen Rahmenbedingungen zu tun; EPÜ und TRIPS seien als
Stichworte genannt. Die Meinungen gehen auseinander, wie diese
Abkommen zu interpretieren sind oder für wie reformbedürftig sie
gehalten werden. Exemplarisch für diese Debatte werden gegenwärtig
zwei Fragen diskutiert: Die erste braucht uns nicht zu berühren,
nämlich die Patentierungen im Biobereich, wo auch große Bedenken
bestehen. Wir haben es im Bereich der Software mit einer ganz anderen
Frage zu tun. Die Debatte wird bei uns zu den Auswirkungen des
OpenSource-Entwicklungskonzepts geführt, wie gesagt, bis hin zu der
Befürchtung, dass dieser Entwicklung der Garaus gemacht wird und
entsprechend auch dem Hoffnungsschimmer, zu einer sichereren Software
zu kommen, ebenfalls die Chancen genommen werden. Ich würde mich sehr
freuen, wenn die heutige Veranstaltung zur Aufklärung dieser etwas
unübersichtlichen Situation und zu der - zum Teil sicherlich auch sehr
emotional geführten - Diskussion beitragen könnte.

#WWs: Wir bitten Sie um Entschuldigung, dass wir Sie relativ kurzfristig
eingeladen haben, aber wir wollten Sie einfach noch vor der
Sommerpause anhören und die Sommerpause nutzen, die Ergebnisse der
Anhörung zusammenzustellen und dann zu einer Position zu kommen, die
wir den Fraktionen zur Verfügung stellen, die dann ihrerseits die
entsprechenden politischen Konsequenzen daraus ziehen können. Wir
hatten bereits Anträge zu diesem Thema - ich denke an einen Antrag
beispielsweise des Kollegen Dr. Mayer von der CSU, der hier rechts
sitzt und der zu diesem Thema, wenn ich es recht sehe, eine
parlamentarische Initiative gestartet hat. Die Kernfragen, die heute
Gegenstand sind, haben wir Ihnen  zugeleitet. Ich würde es einfach
noch einmal ein bisschen straffen, indem ich dahin gehe zu fragen,
gibt es überhaupt so etwas wie Software-Erfindungen? Wie lassen sie
sich eindeutig bestimmen? Wie sieht die gegenwärtige
Patentierungspraxis in unterschiedlichen Ländern, möglicherweise auch
im Unterschied zu Deutschland, aus? Wie beurteilen Sie die
internationalen Rahmenbedingungen? Aber vor allem die Frage, und hier
öffnet sich jetzt der Kreis von uns ein bisschen über unsere
Kolleginnen und Kollegen aus dem Bereich Recht hinaus: Welche
wirtschaftlichen Folgen hätte eine Öffnung der Softwarepatentierung?
Welche Auswirkungen auf Rahmenbedingungen für das sogenannte
OpenSource-Konzept wären zu erwarten, ohne dass ich jetzt das
Schlagwort %(q:OpenSource) überstrapazieren will? Ich weiß, mein
Kollege Kelber schaut da immer sehr aufmerksam und sagt, das ist zum
Teil auch ein Kampfbegriff und ein unbestimmter Begriff. Welche
Auswirkungen hätte es in diesem Bereich auf Konzepte, die wir
staatlicherseits auch durchaus fördern wollen?

#Eia: Einigen von Ihnen war es möglich, ein abschließendes schriftliches
Votum vorzulegen; dafür ganz herzlichen Dank. Die anderen würde ich
herzlich bitten, auch im Hinblick auf die noch erfolgende Auswertung,
uns ihre Stellungnahmen schriftlich zukommen zu lassen, soweit dies
noch nicht erfolgt ist; möglicherweise auch ergänzt, ganz einfach
deshalb, weil dies für die Auswertung dann sehr interessant sein wird.
Wir möchten Ihnen jetzt aber dennoch die Gelegenheit geben, in fünf
Minuten, und ich bitte wirklich, sich an die fünf Minuten zu halten,
nochmals ganz kurz im Sinne der Kernfragen ein Statement abzugeben.
Möglicherweise in einer kurzen Pro- und Kontra-Version, denn
anschließend kommen wir, gegliedert nach den Fragerunden, zu den
Fragen meiner Kolleginnen und Kollegen aus dem Bereich der
Bundestagsabgeordneten, die ich alle insgesamt auch nochmals recht
herzlich begrüße. Ich werde darauf aufmerksam gemacht, dass das
Gespräch aufgezeichnet wird. Wer dies nicht wünscht, der sollte dies
sagen, dann schalten wir ab. Es macht allerdings wenig Sinn
abzuschalten, weil die Worte dann natürlich der Nachwelt
unwiderruflich verloren gehen werden. Aber aus Datenschutzgründen
machen wir Sie darauf aufmerksam.

#Irr: Ich darf jetzt, beginnend von links nach rechts, die Sachverständigen
auch nochmals namentlich vorstellen. Ich begrüße ganz herzlich Herrn
Professor Dr. Lutterbeck von der TU - hier steht Bremen, aber es ist,
glaube ich, unverändert Berlin. Insofern eine nicht allzu weite
Anreise. Herzlich willkommen Lutz Henckel vom Institut für
Kommunikationssysteme. Es folgt Frau Bouillon, die nicht nur, ich sage
dies ausdrücklich, als Fachfrau eingeladen ist. Ich begrüße ganz
herzlich vom Deutschen Patent- und Markenamt Herrn Dr.
Kiesewetter-Köbinger. Ich begrüße dann vom Linux-Verband, jetzt geht
es auf die andere Seite, Herrn Rechtsanwalt Jürgen Siepmann.
Anschließend Herrn Dr. Schiuma, herzlich willkommen. Eine %(q:alte)
junge Bekannte in unserem Raum ist Frau Bremer vom Bundesverband
Informationswirtschaft und last but not least, weil ich die Ökonomie
auch angesprochen habe, Herr Daniel Probst von der Universität
Mannheim. Das ist der Kreis unserer Sachverständigen. Wir hatten
zunächst insgesamt 20 Sachverständige vorgeschlagen bekommen und
mussten den Kreis einfach aus zeitlichen Gründen reduzieren. Dies war
keine Missachtung gegenüber denen, die heute nicht hier auf dem Podium
sitzen, aber wir dachten, mit dem, was wir hier vorne haben, haben wir
so in etwa den Themenbereich abgesteckt. Wobei ich Ihnen ganz deutlich
sage, wenn es hier jemanden im Saal gibt, der aufgrund einer anderen
Verbandszugehörigkeit, also einer Firma oder wie auch immer, sagt, er
hätte noch Interesse, auf die Fragen des Ausschusses einzugehen, dann
sind uns alle Stellungnahmen über den heutigen Tag hinaus recht
herzlich willkommen. Keine lange weitere Vorrede mehr.

#Gdg: Gibt es sogenannte Softwarepatente und wenn ja, lassen sie sich
bestimmen? Wie ist Ihre Position dazu? Ich würde Sie einfach bitten -
ausnahmsweise, entgegen meiner Gewohnheit, habe ich links begonnen und
fahre jetzt rechts fort -, der Reihe nach Stellung zu nehmen. Bitte
schön, Herr Probst.

#Doi: Dr. Daniel Probst, Universität Mannheim:

#AtF: Als Ökonom betrachte ich natürlich die Fragestellung primär vom
ökonomischen Aspekt und aus diesem Blickwinkel ist der Sinn von
Patenten, Anreize für Produktion von Wissen zu setzen. Das ist einmal
die einfachste Erklärung, die man für Patente geben kann, aber es ist
beileibe nicht die einzige Institution, die man dafür benutzen kann.
Als Wirtschaftswissenschaftler fragt man sich, wie genau muss man so
ein System, so ein Patentrecht von der Stärke, Länge, vom
Anwendungsbereich ausgestalten, damit man Vorteile gegenüber
Nachteilen aufwiegen kann. Bezüglich der Softwarepatente gibt es für
mich die Fragestellung, wollen wir ein bestehendes Recht, nämlich das
Urheberrecht, verstärken? Und zwar indem wir ein stärkeres Recht, ein
Patentrecht, auf diese Materie geben. Es gibt noch zwei Blickpunkte:
Den theoretischen Blickpunkt und den Stand der aktuellen Forschung,
der diesbezüglich irgendwo neutral bis extrem skeptisch ist. Auf
theoretischer Ebene sind, wenn man sich ein bisschen kompliziertere
Szenarien vorstellt, insbesondere solche Szenarien, die auf den
Software-Markt passen, solche Standardargumente wie %(q:Stärkeres
Recht führt zu mehr Wert des Patentes, führt zu größeren Anreizen der
Forschung) schlichtweg nicht haltbar. Auf theoretischer Ebene ist man
da sehr skeptisch.

#AWh: Auf empirischer Ebene kann man das aus zwei Blickpunkten betrachten.
Einerseits, wenn man Studien dazu anschaut, was in Amerika passiert
ist. Amerika ist ein Land, in dem man das Experiment durchgeführt hat
- man ist von einem relativ schwachen Schutz in diesem Gebiet zu einem
stärkeren Schutz übergegangen. Bisher gehen sämtliche empirischen
Untersuchungen bezüglich der wirtschaftlichen Auswirkungen dieses
Experiments dahin, dass die Forschungsintensität in dieser Branche
stagniert bzw. abgenommen hat. Also, es ist extrem schwierig, noch
irgendwelche vorteilhaften Anreize nachzuweisen, die man
typischerweise in gewissen Kreisen mit einer Verstärkung des
Schutzrechtes verbinden würde. Auf der anderen Seite, wenn man kleine
und mittelständische Unternehmen betrachtet, kommen hier aktuelle
Studien, insbesondere Auftragsstudien der Europäischen Kommission und
englische Studien, die ich noch weiter zitieren kann, falls Interesse
besteht, zu dem Ergebnis, dass vor allem kleine und mittelständische
Unternehmen das Patentrecht wenig nutzen und auch immer weniger nutzen
werden. Ausnahmen bilden da vielleicht die Chemie- und Pharmabranche,
aber vor allem im Hochtechnologie- und Softwaresektor bedienen sich
kleine und mittelständische Unternehmen primär anderer
Schutzmechanismen, um ihre Investitionen zu schützen. Wenn man sie
danach befragt, sagen sie typischerweise, dass sie nicht aus Ignoranz,
sondern auch aus Kostengründen nicht an diesem Schutzrecht
interessiert seien. Also, kleine und mittelständische Unternehmen
sprechen sich eher dagegen aus. Aus Sicht von größeren Unternehmen ist
durchaus zu erwarten, dass sie mit großen Patentabteilungen
komparative Vorteile haben und auch mit großen bestehenden
Patentportfolios sicher nicht besonders schlecht dastehen, indem sie
sich strategisch mit Kreuzportfolio-Lizenzierungen absichern können,
was aus ökonomischen Überlegungen heraus auch wieder als bedenklich zu
betrachten ist, da es den Markteintritt sehr stark einschränken würde.
Damit habe ich im Wesentlichen die Punkte gebracht, die aus meiner
Sicht wichtig sind.

#Vin: President

#Dbr: Damit haben Sie die fünf Minuten auch sehr vorbildlich eingehalten.
Recht herzlichen Dank für den guten Einstieg. Bitte schön, Frau
Bremer.

#Dna: Dr. Kathrin Bremer, Bundesverband Informationswirtschaft,
Telekommunikation und Neue Medien e.V. (BITKOM)

#DNU: Danke schön. Meine Damen und Herren. Ich wurde hier schon vorgestellt
als Vertreterin des BITKOM, der viele Software-Unternehmen als
Mitglieder hat, und zwar kleinere und größere. Zu dem Fragenkatalog
möchte ich gleich voranstellen, dass wir hier von unserer Seite gleich
am Anfang die Frage aufwerfen möchten, was Softwarepatente überhaupt
sind. Die Frage ist auch gestellt worden, sie wird und wurde auch in
der Vergangenheit in der Praxis überhaupt nicht einheitlich behandelt
und beantwortet. Durch diesen Begriff %(q:Softwarepatente) wird auch
in der Diskussion immer etwas vermischt. Es sind eigentlich
Erfindungen, die in Form eines Computerprogrammes realisiert werden,
d.h. wir reden auch bei der Software und der Patentierbarkeit von
Software von dem klassischen Patentschutz, der auch die Erfordernisse,
die im Patentrecht existieren, d.h. die Technizität und auch die
Neuheit erfüllen muss. Ich denke, das ist ganz wichtig, dass man sich
das in der Diskussion immer wieder vor Augen führt. Um was geht es uns
überhaupt? Man diskutiert von einem Patentschutzkomplex bis hin zu der
Forderung %(q:Wir wollen den Patentschutz hier ganz groß ausweiten).
So wird es auch gerade bei der Wirtschaft immer wieder suggeriert, die
den Patentschutz in einem extremen Maße ausweiten und alles andere
damit abtöten will. Das ist aber nicht das Ziel dabei. Vielmehr ist es
so, dass wir den Status quo, wie er von der Rechtsprechung eigentlich
in den letzten Jahren etabliert worden ist, erhalten wollen, gerade im
Hinblick auf Artikel 52, der, obwohl er in dem Übereinkommen steht, so
in der Praxis nicht angewandt wird. Wenn die Erfordernisse, die das
Patentschutzsystem stellt, erfüllt sind, ist nämlich durchaus auch in
diesem Fall eine Patentierung möglich. Und das sollte unserer
Auffassung nach in der Zukunft auch klar so bleiben, d.h., Status quo
erhalten und nicht hier sui generis einen Patentschutz ausweiten.
Dasselbe gilt auch für die Horrorszenarien und Geschäftsmethoden mit
Blick nach USA. Auch da kann es nicht das Ziel sein, dass man diese
Geschäftsmethoden auf einmal patentieren lässt. Ist das Schutzniveau
im Sinne des Patentrechts erfüllt, dann kann es auch sein, dass in
Verbindung mit einer Geschäftsmethode durchaus ein Patent erteilt
werden kann. Zu den internationalen Rahmenbedingungen denken wir, dass
das Europäische Patentübereinkommen eigentlich ein ganz guter Rahmen
ist und vor allem von der Rechtsprechung so ausgefüllt ist, dass man
damit ganz gut leben kann, wenn es so auch im Gesetz etabliert werden
würde oder in dem Übereinkommen. Allerdings ist in den USA in der Tat
das Patentrecht etwas weitergehend.

#Jee: Jetzt möchte ich noch einmal zu den wirtschaftlichen Auswirkungen
kommen, dazu hat sich Herr Probst ja auch schon geäußert. Wir haben
die Erfahrung, dass durchaus auch kleine und mittlere Unternehmen den
Patentschutz nutzen, weil sie auch ein erhebliches Entwicklungsrisiko
tragen und dem begegnen können, indem sie ihre Erfindungen auch durch
Patente schützen. Gerade im Hinblick auf die New Economy hat es der
eine oder andere vielleicht bereut, dass er sein Patent nicht
angemeldet hat, weil es ihm vielleicht etwas besser gehen würde als es
vielleicht jetzt einigen Start-ups geht. Durch Patente sehen wir schon
eine Innovation, die auf jeden Fall ganz klar gefördert wird,
verbunden mit einer Sicherheit für den Entwickler, der eben einen
Patentschutz bekommt und dieses erhebliche Risiko, das auch mit einer
Erfindung, wenn ich sie herausgebe, verbunden ist, etwas minimieren
kann. Denn das Urheberrecht ist eben nicht alleine ausreichend, weil
die Technizität beim Urheberrecht nicht ausreichend berücksichtigt
werden kann. Das Urheberrecht schützt allein ein einziges Werk, aber
nicht darüber hinausgehend eine Technik, wenn ich die in Verbindung im
gesamten System umsetze. Was die wirtschaftlichen Auswirkungen
betrifft, ist es auch so, dass es als Investitionsmittel durchaus
hilfreich ist, wenn das Patent eingesetzt wird und dann einem
kleineren Unternehmen, das nicht die finanziellen Mittel hat, zugute
gehalten werden kann. Aus diesen jetzt genannten Gründen ist es auch
nicht so, dass man eine Aufsplittung zwischen kleinen und großen
Unternehmen sehen kann. Auch gerade bei den großen Unternehmen - für
die ist es natürlich auch ein erheblicher Kostenaufwand, wenn sie
Patente anmelden, die werden jetzt nicht %(q:auf Teufel komm raus)
angemeldet zum Manifestieren sämtlicher Ideen - überlegt man sich sehr
gut, ob man das tut. Man macht es nur da, wo es wirklich sinnvoll ist.
Diese Möglichkeiten werden nach unseren Erfahrungen von sämtlichen
Unternehmen benutzt, von klein bis groß. Ja, ich denke, das waren als
Eingangsstatement jetzt meine Punkte. Danke.

#Van: Vielen herzlichen Dank. Ich will das Lob auch auf Sie ausdehnen, was
den zeitlichen Rahmen angeht, und würde Herrn Schiuma das Wort geben.

#DGa: Dr. Daniele Schiuma

#Ank: Software Patents Workgroup at %(mp:Max Planck Institute)

#Vsu: Vielen Dank. Ich möchte mich kurz vorstellen: Ich bin Physiker von der
Ausbildung her, habe dann im Fach Jura am Max-Planck-Institut in
München im Patentrecht promoviert, bin als Patentanwalt tätig und
leite im Max-Planck-Institut die Arbeitsgruppe %(q:Patentierbarkeit
von Software). Daher interessiere ich mich auch aus wissenschaftlicher
Sicht für diese Problematik. Grundsätzlich ist es so: Ich befürworte
generell das Patentrecht als Grundlage des Schutzes. Ich betrachte das
Urheberrecht aus Gründen, die wir nachher sicher noch erörtern werden,
als nicht geeignet, zumindest als nicht ausreichend, und lehne
eigentlich ein Recht sui generis, sprich ein außerhalb des Urheber-
und Patentrechtes liegendes Recht, ab. Das Patentrecht hat eine mehr
als hundertjährige Tradition und Entwicklung durchgemacht, und ich
befürworte die weitestgehende Anwendung von generellen Bestimmungen
aus dem Patentgesetz. In diesem Zusammenhang sehe ich eigentlich keine
Notwendigkeit, Sonderbestimmungen in das Patentgesetz einzuführen,
d.h. also, jetzt besondere rechtliche Bedingungen für Software in das
bestehende Patentrechtssystem einzufügen. Dennoch finde ich, dass ein
Handlungsbedarf besteht. Es ist zum einen eine Vereinheitlichung der
Rechtslage auf der europäischen Ebene notwendig. Dort gibt es schon
Tätigkeiten der Europäischen Kommission, eine Vereinheitlichung
herbeizuführen. Ebenfalls muss auch auf deutscher Ebene eine
Rechtsklarheit herbeigeführt werden, und in diesem Zusammenhang
sollten auch internationale rechtliche Abkommen - so zum Beispiel das
TRIPS-Abkommen - berücksichtigt werden. Da ich aber eher aus der
juristischen Seite komme, möchte ich mich eigentlich - was die
wirtschaftliche Sicht der Problematik angeht - eher zurückhalten. Aus
meiner patentanwaltlichen Tätigkeit kann ich jedoch das bestätigen,
was Frau Dr. Bremer vorhin ausgeführt hat, nämlich, dass kleine und
mittelständische Unternehmen durchaus auch das Patentsystem in
Anspruch nehmen und einen Patentschutz für die in den kleinen und
mittelständischen Unternehmen getätigten Erfindungen erwirken. Vielen
Dank.

#Gzs: Ganz herzlichen Dank. Ich weiß nicht, greift es um sich, neben mir
sitzt noch so eine Kombination: Ein Jurist, der Physiker ist oder ein
Physiker, der Jurist ist - aber, glaube ich, eine andere Position
vertritt.

#RpV: RA Jürgen Siepmann

#IlW: Ich brauche mich ja jetzt nicht mehr vorzustellen. Ich komme gleich
zur Sache. Wir unterhalten uns hier über den Anwendungsbereich des
Patentrechts, für welche Gebiete ist das Patentrecht geeignet und für
welche nicht. Diese Unterhaltung ist im Moment besonders wichtig, weil
es in den letzten Jahrzehnten eine Tendenz gibt, dass das Patentrecht
auf immer weitere Bereiche ausgedehnt wird. Ich möchte hier an ein
Zitat des Wirtschaftswissenschaftlers %(ma:Machlup) anknüpfen. Fritz
Machlup wurde 1961 in den USA vom Senat oder Kongress beauftragt, eine
Studie über das Patentwesen, zu Nutzen, Sinn und Zweck zu erstellen,
und er hat einen sehr interessanten Satz aufgestellt. Ich zitiere ihn
kurz: %(q:Der dem Patentwesen zurechenbare Nutzen besteht also in der
Produktionszunahme einer Volkswirtschaft, die auf jene neuen
technischen Lehren zurückgeführt werden kann, die dem Patentwesen
insofern zu verdanken sind, als sie ohne seine Anspornung entweder
überhaupt nicht oder erst später aufgekommen wären). Ich meine, daran
müssen wir messen, in welchen Bereichen das Patentwesen sinnvoll ist
und in welchen nicht und sonst gar nichts. Herr Machlup hat einzelne
Kriterien dafür genannt, die man berücksichtigen müßte, Kosten, die
durch das Patentrecht entstehen und die Kosten, die man hat. Ich habe
das ein bisschen umformuliert und möchte einmal kurz die Kriterien
ansprechen.

#Zba: Zum einen ist es die Höhe der Forschungsinvestitionen. Patentrecht ist
sehr gut geeignet in Bereichen, in denen sehr hohe
Forschungsinvestitionen sind, zum Beispiel in der Pharmabranche. Der
nächste Punkt ist die Frage, wie groß der Nachahmungsaufwand des
Imitators ist. Wenn wir das zum Beispiel anhand der Pharmabranche
sehen, ist es sehr leicht, ein Medikament zu kopieren. Betrachten wir
das jetzt mit Software - dabei müssen wir natürlich berücksichtigen,
dass es das Urheberrecht gibt - dann gilt, Software nachzuschreiben,
ist sehr aufwändig, vor allen Dingen, wenn diese nur in Binär-Form
vorliegt. Der nächste Punkt sind Vorteile durch den first mover
advantage. Derjenige, der zuerst produziert, welche Vorteile hat er?
Betrachten wir wieder die Pharmabranche: Wenn mir mein Apotheker
garantiert, dass die Tabletten, die ich nehme, genau die gleichen
sind, dann kaufe ich im Zweifelsfall die billigeren. Bei
Softwareprodukten ist das anders. Wenn ich ein Softwareprodukt
jahrelang genommen habe, dann bleibe ich dabei, auch wenn es zehnmal
so teuer ist. Dafür gibt es auch sehr viele Beispiele in der Praxis.
Das wären jetzt die betriebswirtschaftlichen Gründe. Dann gibt es noch
mehrere volkswirtschaftliche Argumente, die wir auch noch
berücksichtigen müssen. Wir müssen die Markteintrittsbarrieren der
jeweiligen Branche sehen. Betrachten wir z.B. einmal die
Autoindustrie: Da gibt es nur Großunternehmen, denn es ist völlig
unmöglich, als Schlosser ein Auto in der Garage zusammen zu schustern.
Betrachten wir auf der anderen Seite die Softwarebranche: Jeder
Jugendliche, der einen Rechner hat, kann Software entwickeln, kann
sich weiterbilden und kann das auch später auf den Markt bringen. Das
nächste Kriterium sind die Innovationszyklen, die wir sehen müssen,
wie groß die in den verschiedenen Branchen sind. Bei Software haben
wir Innovationszyklen von teilweise wenigen Monaten, d.h., dass
Software, die vor einem halben Jahr geschrieben wurde, heute manche
überhaupt nicht mehr interessiert. Weiter müssen wir noch solche Dinge
wie z. B. Produktkosten berücksichtigen. Bei Produkten, die nichts
kosten oder deren Vervielfältigung nichts kostet, bei denen -
volkswirtschaftlich gesprochen - nur die Grenzkosten anfallen, sind
zusätzliche Kosten besonders bedenklich.

#Iie: Ich will das nicht weiter fortführen, sonst reicht mir die Zeit nicht.
Aber auf einen Punkt möchte ich noch eingehen, nämlich auf einen
gesellschaftlichen Aspekt. Autos, wie gesagt, kann nicht jeder
produzieren, Medikamente auch nicht, aber Software kann jedermann
schreiben. Und wenn die jungen Nachwuchswissenschaftler mit dem
Begriff Software verbinden, dass das etwas ist, woran man sich die
Finger verbrennen kann, dann ist das sehr ungünstig für unsere
Gesellschaft. Den Gedanken, dass die Investitionen geschützt werden,
können Sie einem jungen Menschen sehr schlecht beibringen. Welche
Investitionen hat er denn? Den Rechner hat er, er muss also nur
programmieren. Ich habe als Beispiel zwei Branchen herausgenommen:
Eine Branche, für die sich das Patentwesen sehr gut eignet - das ist
meiner Ansicht nach die Pharmabranche - und im Gegensatz dazu die
Softwarebranche. Wenn wir diese zur Zeit bestehenden Schutzrechte
vergleichen, nämlich das Patentrecht in der Pharmabranche und das
Urheberrecht in der Softwarebranche, dann werden wir sehen, dass damit
im Wesentlichen die gleichen Wirkungen erzielt werden. In der Medizin
verhindert das Patentrecht, dass Sie Stoffe eins zu eins kopieren
können, Sie können aber die Gedanken, die dahinter stehen, die
Anregung aufnehmen, und andere Medikamente entwickeln. In der
Softwarebranche ist es so, dass eben diese Eins-zu-eins-Kopien, die
sogenannten Knock-out-Imitationen, durch das Urheberrecht verhindert
werden. Ich möchte jetzt auch ganz konkret werden und Beispiele nennen
für Firmen, die ohne Patente sehr groß geworden sind. Das ist z.B.
eine deutsche Firma - SAP -, die hat bis vor einem Jahr kein einziges
Patent gehabt. Sie hat ihre Software zum größten Teil in source-codes
vorliegen gehabt. Der SAP-Kernel war ein C und C++, war zum großen
Teil einsehbar und auch die Sprache, mit der die
betriebswirtschaftlichen Anwendungen geschrieben waren - das ist eine
Interpretersprache, das ist ABAB -, konnte jeder einsehen. Trotzdem
hat das niemand nachgebaut, trotzdem ist SAP zu einem Weltkonzern
geworden. Übrigens, Microsoft, auch ein gutes Beispiel für einen
Weltkonzern, hat sich auch nicht auf Patente gestützt. Die hatten zu
dem Zeitpunkt, als sie groß geworden sind, so gut wie gar keine
Patente. Zu dem nächsten Punkt - ich weiß nicht, wieviel Zeit ich noch
habe.

#EhW: Eigentlich ist sie vorüber.

#DiW: Dann überspringe ich den Rest, den ich zu sagen habe.

#SiW: Sie bekommen noch einmal das Wort, wie alle anderen hier.

#ItW: Ich möchte enden mit einem Zitat aus einer britischen Studie, und zwar
wurde die in Auftrag gegeben vom British Departement of Trade and
Industry vom Economic and Social Research Council und vom Intellectual
Property Institute. Da heißt es, ich zitiere: %(bq:The patent system
gives SMEs no help in innovating, Macdonald concludes from surveys and
interviews involving over 2.600 firms. It neither fosters nor protects
their innovation.) Am Schluss die Aussage: %(bq:The patent system is
at best an irrelevancy for most small firms.) Ich denke, eine solche
Aussage aus dem Munde eines der Schützer des geistigen Eigentums sagt
sehr viel. Vielen Dank für Ihre Aufmerksamkeit.

#HuW: Herzlichen Dank. Trotz Physik und trotz Jura sind Sie auch auf den
ökonomischen Aspekt eingegangen, insofern war die eine Minute darüber
in Ordnung. Ich komme auf der linken Seite jetzt zu jemandem, Herrn
Dr. Kiesewetter-Köbinger, der unmittelbar jeden Tag den Ärger mit dem,
was wir hier diskutieren, hat. Vielleicht können Sie uns aus Ihrer
Praxis zu der Frage auch in fünf Minuten ein paar Dinge sagen, die uns
gleich weiterhelfen.

#Dbt: Dr. Swen Kiesewetter-Köbinger

#DPd: German Patent Office

#VmA: Vielen Dank, dass ich eingeladen wurde. Ich muss aber etwas
klarstellen: Ich bin hier vorgestellt worden als jemand vom DPMA, ich
bin Prüfer beim DPMA, aber ich habe keine Befugnis, hier für das DPMA
zu sprechen und meine Ansicht wird wahrscheinlich auch nicht der
offiziellen entsprechen; ich bin hier als privater Sachverständiger.

#Sld: Sie sind auch namentlich als Sachverständiger benannt worden.

#EsW: Es stimmt, ich muss mich praktisch täglich mit Software
auseinandersetzen, na ja, vielleicht nicht täglich. Hauptsächlich
prüfe ich elektronische Anmeldungen - also ich prüfe nicht
hauptsächlich Softwareanmeldungen, sondern in überwiegender Mehrzahl
Elektronikanmeldungen, aber es hagelt auch Softwareanmeldungen da
hinein. Als ich am Amt anfing, kam als erstes eine Anmeldung von IBM
auf meinen Tisch - reine Software - und ich habe gesagt, gibt es denn
das? Es steht doch im Gesetz, § 1, dass das nicht patentfähig ist.
Seither kämpfe ich darum, wie das denn auszulegen ist und suche meinen
Weg selber. Von der Ausbildung her bin ich Astro-Physiker, der
programmiert und Elektronik entwickelt hat und deswegen zum Patentamt
kam. Software entwickeln war für mich etwas, was man als patentfreien
Raum gesehen hat. Die erste Frage, die mir mein Abteilungsleiter
gestellt hat, war: Und, wie stehen Sie zu Softwarepatenten? Daraufhin
ich: Wieso Softwarepatente? Patente auf Software gibt es doch nicht,
das steht doch überall. Vielleicht kennen Sie die Meinung von Herrn
Dr. Tauchert, der ist etwas anderer Meinung und ich musste meine
Meinung dann auch deutlich revidieren. Nach einiger Zeit kam ich dann
mit den einschlägigen Entscheidungen des Bundesgerichtshofes, des
Bundespatentgerichts zusammen und musste studieren, was sie meinen,
wie sie das Gesetz auslegen. Ich habe dann lange Zeit versucht, mich
daran sehr stark zu orientieren.

#DdW: Dr. Probst forscht an der Universität Mannheim über die Ökonomie des
Patentwesens.  In diesem für eine Anhörung dem Deutschen Bundestages
am 2001-06-21 eingereichten Papier argumentiert er, dass
Patentmonopole aus volkswirtschaftlicher Sicht immer als je nach ihrem
Anwendungsgebiet mehr oder weniger notwendige Übel angesehen werden. 
Wobei im Bereich der Software wenig Notwendigkeit und viel Übel zu
erkennen ist.  Viele der herkömmlich von Patentexperten propagierten
Glaubenssätze sind aus volkswirtschaftlicher Sicht falsch, da auf
simplistischen Modellen beruhend.  Bisherige Erkenntnisse deuten
darauf hin, dass Softwarepatente die gesamte Produktivität und
Innovationskraft der betroffenen Branchen mindern.  In einem Bereich
wie der Software sollte der Staat, wenn ihm an der Vitalität der
Softwarebranche gelegen ist, in öffentliche Infrastrukturen wie z.B.
Bildung, Forschung und Netzwerk-Hardware investieren.

#HWu: Herzlichen Dank. Frau Bouillon.

#Eos: Elke Bouillon

#Pot: Phaidros Software AG

#Mat: Mein Name ist Elke Bouillon, ich vertrete die Firma Phaidros Software
AG, und wenn Sie die jetzt nicht kennen, brauchen Sie überhaupt nicht
im Stuhl zu versinken. Das ist ein kleines Unternehmen, ein kleines
sehr innovatives Unternehmen aus Ilmenau, der Universitätsstadt
Ilmenau in Thüringen. Wir haben inzwischen ca. 30 Mitarbeiter und
damit habe ich schon deutlich gemacht, dass ich weder eine
Patentrechtsexpertin noch eine Vertreterin der OpenSource-Gemeinde
bin, wobei ich auf das Zweite noch einmal genau eingehen möchte. Wir
bewegen uns nicht auf dem Gebiet der OpenSource, stehen aber dennoch
dem Patentwesen gerade im Softwarebereich sehr, sehr kritisch
gegenüber. Der Hintergrund, warum wir angefangen haben, uns mit dieser
Situation zu beschäftigen, allgemein mit der Patentsituation im
Softwarebereich, der liegt schon einige Jahre zurück. Als wir die
ersten bürokratischen Hürden der Unternehmungsgründung genommen haben,
haben wir uns auch mit dem Patentwesen ein wenig beschäftigt. Unser
Chefentwicklungsteam hat dann resigniert die Fahnen gestrichen und
gesagt, entweder wir versuchen, uns damit zu beschäftigen, wir
versuchen zu diesem Thema eine Position auch laut zu sagen, oder wir
fahren das Unternehmen ganz unauffällig wieder zurück. Wir haben uns
für den ersten Weg entschieden. Ich möchte auf diese knallharte Frage,
die da von Seiten unseres Chefentwicklers aufgeworfen und so krass
beantwortet wurde, noch einmal kurz eingehen.

#Dnc: Das eine Problem, das auf ein junges Unternehmen in diesem Bereich
zukommt, ist eben das interne Problem, das sind die zeitlichen, die
personellen und die finanziellen Ressourcen, die notwendig sind, um
Patentrecherchen durchzuführen und um Patente anzumelden. Auf das
Zweite wird sehr oft in der Diskussion eingegangen, es wird ja auch
gesagt, junge Unternehmen melden auch Patente an, aber das Erste wird
in der Regel vernachlässigt oder taucht in der Diskussion gar nicht so
stark auf. Es ist aber ein immenser Aufwand, der gerade in den Zeiten
der personellen Knappheit im IT-Bereich von kleinen Unternehmen zu
bewerkstelligen ist, der auf solche Unternehmen zukommt, um solche
Patentrecherchen regelmäßig auch durchzuführen. Ich habe jetzt leider
die Quelle nicht mehr heraussuchen können, ich kann sie nur nennen: Es
war ein Artikel in der Computerwoche, der einmal dargestellt hat, dass
man, wenn man eine Software entwickelt, einfach einmal eine Zahl über
den Daumen gepeilt, gegen ca. 200 Softwarepatente auf dem
amerikanischen Markt verstößt. Die gesamte Patentgesetzgebung in
Amerika ist - das Experiment wurde vorhin schon sehr gut, wie ich
finde, beschrieben - nicht ganz vergleichbar mit der Situation hier in
Europa, und vor allem hier in Deutschland, wo das Patent wesentlich
stärker gehandhabt wird und nicht mit dem Wettbewerbsrecht wie in
Amerika ein wenig ausgeglichen wird. Das zweite Problem ist das
wirtschaftliche Problem, das wir ganz allgemein gesellschaftlich
sehen, nämlich, dass die großen Unternehmen Patentreservoire anlegen
und sich sozusagen mit der Cross-Lizensierung versprechen, sich
gegenseitig nichts zu tun. Damit bauen sie eine immense Machtposition
auf und lassen kleinen Unternehmen kaum die Chance, mit eigenen
Innovationen, die auf Technologien zurückgreifen, die eigentlich
inzwischen nicht mehr neu zu nennen sind, also eine eigene Erfindung
oder Entwicklung - wie auch immer man das nennen möchte - zum
wirtschaftlichen Erfolg zu bringen.

#Dij: Der dritte Punkt, auf den ich kurz eingehen möchte, ist, dass wir in
einer relativ jungen Industrie wie der Softwareindustrie gerade an
einem Stand angekommen sind, wo es darum geht, Standards einzuführen,
um eine effektive Weiterentwicklung zu machen. Der Wunsch auf der
einen Seite, Standards einzuführen und auf der anderen Seite, sein
Wissen als Privateigentum zu besitzen, widersprechen einander. Denn,
was nützt ein Standard, wenn er im Besitz eines Unternehmens ist?
Dieser Situation sehen wir uns gegenübergestellt. Der vierte Punkt
ganz kurz: Wer prüft die Patentierbarkeit? Momentan ist die Situation
so, dass die Zahl der Spezialisten, die in diesem Bereich auf dem
Arbeitsmarkt zu finden sind, nicht gerade üppig ist. Da steht dann
schon die Frage, wer prüft das und wer ist tatsächlich in der Lage,
für jeden Bereich Spezialisten einzustellen, die dann eben auch ganz
gezielt sagen, das %(q:ja), das %(q:nein). Die Situation in Amerika
hat eben gezeigt, dass es sehr oft zur Vergabe von Patenten - zu
sogenannten Trivialpatenten - gekommen ist.

#Ddu: Die Antwort von unserer Seite noch einmal zusammengefasst: Wir sind
der Meinung, wir brauchen keine Patente, wir schätzen sie als nicht
innovationsfördernd ein, als bedenklich für die Entwicklung von KMUs.
Wenn man über das Patentrecht aus irgendwelchen Gründen nachdenken
sollte, tatsächlich einen Schutz einzuführen, könnte man über
Kompromisse nachdenken. Ganz wichtige Punkte sehe ich an dieser Stelle
in der Begrenzung der Laufzeit, und zwar in der drastischen
Verringerung der Laufzeit, und, wie ich vorhin schon angesprochen
habe, darin, dass man feststellt, dass man dem Wettbewerbsrecht einen
größeren Eingriff auf das Patentrecht ermöglicht. Beispielsweise im
Falle des Erhebens von Standards, dass man dann das Patentrecht
verliert, denn das ist eigentlich schon Grund genug, sich für ein
Unternehmen zu freuen, wenn man einen Standard entwickelt hat und auch
bestimmen und weiterentwickeln kann, wie es in der Softwarebranche ja
üblich ist. Und die andere Sache ist, dass große Unternehmen in
Amerika wesentlich häufiger gegen kleine Unternehmen klagen, was als
ungerechtfertigt aufgewogen wird und Softwarestreitigkeiten aus diesem
Grunde in den USA in aller Regel die Kassen der Rechtsanwälte füllen.
Danke schön.

#WWj: Wie fast bei allen Streitigkeiten. Wir haben mit der Wissenschaft
begonnen und enden jetzt auch mit der Wissenschaft, mit zwei
Vertretern. Zunächst Herr Henckel. Ich habe ihn vom Institut der
Kommunikationssysteme begrüßt, aber das Ganze ist an der GMD, unserer
Großforschungseinrichtung in St. Augustin, zumindest dem
Forschungskollegium wohl vertraut. Bitte schön, Herr Henckel.

#DWm: Dipl.-Inf. Lutz Henckel, Institut für offene Kommunikationssysteme
(GMD)

#GfW: Guten Tag, meine Damen und Herren. Ich vertrete hier ein Projekt, das
sich auf die Fahne geschrieben hat, den Einsatz von
OpenSource-Software im Bereich von kleinen und mittelständischen
Unternehmen sowie der öffentlichen Verwaltung zu fördern. Also, von da
aus sehen Sie wahrscheinlich auch schon meinen Standpunkt sehr
deutlich. Ich bin also kein Jurist wie die meisten hier in dieser
Reihe, deswegen kann ich mich auch auf diese juristischen Fragen nicht
so sehr kaprizieren, sondern möchte lieber auf die
volkswirtschaftlichen bzw. betriebswirtschaftlichen Effekte und die
Auswirkung auf die OpenSource-Szene eingehen, wenn diese
Softwarepatente letztendlich nachträglich legitimiert werden. Was ich
so feststelle ist, dass die Intention, die Patente an sich haben,
letztendlich der Investitionsschutz und die Innovationsförderung sind.
Da zeigt sich aber, dass, wenn man dieses Patentwesen extrem
ausweitet, eigentlich genau das Gegenteil passiert, also, dass
Innovation letztendlich dadurch zurückgeht; dass, und das ist auch
ablesbar an den Studien, die hier im Fragenkatalog zitiert wurden, die
Forschungsmittel, die von den Firmen investiert werden, wesentlich
zurückgehen und dass auch die Innovation letztendlich dadurch
reduziert und nicht gefördert wird. Die Auswirkung einer
unbeschränkten Patentierbarkeit - eben auch von Software - kann dazu
führen, dass die IT-Infrastruktur sowie gesellschaftliche
Organisationsmethoden letztendlich gelähmt werden, d.h., dass die
Unternehmen, die sich sozusagen einen Patentkrieg leisten, aufrüsten
und Patente letztendlich dafür benutzen, um andere Konkurrenten aus
dem Felde zu schlagen bzw. daran zu hindern, in den Markt einzutreten.

#EPr: Ein schönes Beispiel kann man da vielleicht einmal nennen, und zwar
ist es schon sehr alt: Zu Beginn des 20. Jahrhunderts waren zwei
deutsche Firmen im Bereich der Funknachrichtenübermittlung, also der
Telegraphie, nicht tätig, das waren Siemens und AEG, und zwar nur
deswegen, weil sie gegenseitig Patente hatten und die so restriktiv
benutzt haben, dass eigentlich eine Innovation vollkommen
ausgeschlossen war. Das hat dazu geführt, dass eine andere Firma, die
Marconi-Instruments, letztendlich eine Weltmarktstellung erreicht hat.
Erst nachdem Siemens und AEG Telefunken gegründet haben und sich dann
sozusagen gegenseitig die Patente kostenlos zur Verfügung gestellt
haben, kam es dazu, dass innerhalb weniger Jahre die Innovation wieder
so ansprang, dass diese Weltmarktstellung von dieser erstgenannten
Firma letztendlich gebrochen werden konnte. Wenn man davon ausgeht,
dass Softwarepatente möglich und legitim wären, dann würde es heißen,
dass ungefähr zehntausend Softwarepatente im Augenblick im Keller des
Europäischen Patentamtes schmoren und letztendlich durch so eine
Erweiterung des Patentschutzes nachträglich legitimiert würden. Frau
Bouillon hat es schon gesagt, das hat immens negative Auswirkungen
letztendlich auf kleine und auch junge Unternehmen, die darauf
basieren, dass sie gute innovative Produktideen haben und von
vornherein gar nicht die Möglichkeit hatten, ein entsprechendes
Patentportfolio aufzubauen, um sich dadurch in eine gute Marktposition
zu bringen. Um jetzt noch einmal auf die OpenSource-Szene einzugehen,
da ist es immens wichtig, dass OpenSource darauf basiert, dass der
Source-Code freigegeben wird und dadurch die Überprüfbarkeit, ob
Patentverletzungen dadurch vorliegen, extrem hoch ist. Die Gefährdung
von OpenSource in diesem Bereich ist extrem hoch, weil die
Nachweisbarkeit, dass Patente verletzt werden, letztendlich sehr hoch
ist. Danke.

#Hrc: Herzlichen Dank. Als Letzter ist erst einmal Herr Prof. Dr. Lutterbeck
von der TU Berlin an der Reihe.

#PWl: Prof. Dr. Bernd Lutterbeck, Technische Universität Berlin (Fachbereich
Informatik)

#Wtj: Wir säßen ja nicht hier, wenn es sich um ein einfaches Problem handeln
würde. Für einen Informatiker ist es durchaus nicht völlig klar, was
Software ist. Dann hätten sie ihr Fach schon verstanden, sie wissen,
was sie tun, aber was es bedeutet, wissen sie noch nicht. Das drückt
sich dann auch in der Frage aus, wie man Softwarepatente von anderen
Patenten abgrenzt. Diese Abgrenzung ist bisher noch nicht vernünftig
gelungen. Ich habe Ihnen in meiner Stellungnahme, Punkt 6, Zahlen
zusammengetragen, die nach meiner Meinung die letzten verfügbaren
sind. Wenn man sich die Zahlen anschaut, sieht man, wie die bei der
Auswertung der gleichen Patentklasse auseinandergehen, beispielsweise
die Zahlen für Deutschland im Jahr 2000: Deutschland, schätzt Herr
Tauchert, zuständiger Beamter, 1200, in den USA nur 1 000. Man fragt
sich: %(q:Was, in USA weniger als in Deutschland?). Wie auch immer.
Sehr seriöse Untersuchungen, Josh Lerner nenne ich hier, von der
Harvard Business School, kommen bei Businesspatenten von 1969 bis
heute auf die Zahl 446. Der Direktor des Patentamtes kommt allein für
1999 auf 553, und man kann das so durchgehen, Herr Hagedorn von SAP
auf 705. Das geht dann weiter, wenn Sie sozusagen reine
Softwarepatente nennen, Herr Hagedorn nennt für Deutschland 12.000 für
fünf Jahre. Warum das so ist, weiß kein Mensch. Die seriösen
Untersuchungen von Harvard belegen das nicht und man weiß es letztlich
nicht. Diese Unklarheit hat nun mehrere Konsequenzen: Einmal im
unmittelbaren Patentbereich. Man ist sehr unsicher, und die
Unsicherheit in den letzten Jahrzehnten ist immer die gewesen: Mal war
man mehr im Patentbereich, mal war man im Urheberbereich; das ist
sozusagen ein Hin-und-her-Schwanken in der gleichen Materie. Es hat
aber auch einen elementar anderen Aspekt, den am besten Herr Dr.
Melullis angesprochen hat. Melullis ist Berichterstatter im BGH für
Patentsachen und er hat dem Sinne nach gesagt: Ich habe vor einem
Angst, nämlich, dass die Patentdiskussion in eine Zensurdiskussion
kommt, wo plötzlich ein Textverarbeitungsprogramm dann über
irgendwelche unklaren Kriterien patentiert wird. Insofern denke ich
auch, im Kultur- und Medienausschuss ist es richtig angesiedelt, es
gibt nämlich hier einen Zusammenhang: Patent; Zensur wegen der nicht
klaren Abgrenzbarkeit.

#Zss: Zweitens, die ökonomischen Probleme. Da muss man ja schon sagen,
Patente lassen sich rechtlich nur legitimieren, wenn es dafür eine
wirtschaftliche Rationalität gibt. Herr Probst, ich stimme Ihnen zu,
ich würde das sogar noch zuspitzen, allerdings dabei differenzieren
zwischen einzelwirtschaftlicher und makroökonomischer Betrachtung. Es
ist das SAP-Beispiel genannt worden; allerdings haben Sie vergessen,
die Pointe von SAP zu erwähnen. Richtig ist: SAP ist über Jahrzehnte
Weltmarktführer ohne ein einziges Patent geworden. Das spricht nicht
dafür, dass sozusagen in diesem Bereich Patente notwendig sind, um als
innovatives Unternehmen den Wettbewerb anzuführen. Aber, seit 1997
baut SAP eine Patentabteilung auf. Der Grund ist, SAP ist inzwischen
mit 31 Prozent am amerikanischen Markt und nur noch mit 16 Prozent am
deutschen Markt vertreten. Die SAP-Leute sagen, und nach meiner
Meinung läßt sich das nicht widerlegen: %(q:Wenn ich am amerikanischen
Markt mithalten will, dann muss ich dort Patente beantragen.) Das
leuchtet mir ein. Man kann ja von SAP nicht verlangen, dass sie sich -
bloß, weil es in Deutschland anders läuft - auf ihrem Hauptmarkt
anders verhalten. Das zeigt aber, dass hier ein Element in der
Diskussion ist, ich nenne nur Deutschland/Europäische Union, ohne das
man das Problem nicht klären kann. Makroökonomisch ist es so, das ist
auch schon erwähnt, dass natürlich die Transaktionskosten steigen. Bei
Herrn Lerner, den ich hier erwähne, findet sich eine Zahl: Ungefähr
drei Mark werden für Innovationen eingesetzt, mindestens eine Mark für
Patentverfolgungskosten. Und da muss man schon abwägen, wo ist
eigentlich der Nutzen und der Schaden.

#ZmW: Zusammengefasst: Ich stimme Herrn Probst zu. Aus ökonomischer Sicht
ist nicht ersichtlich, wo der Nutzen der sogenannten Softwarepatente
ist. Einzelwirtschaftlich muss man die Frage teilweise anders
beantworten. In der Literatur gibt es eine Diskrepanz zwischen den
Stimmen: Die Ökonomen sind überwiegend skeptisch, unter den Juristen
gibt es aber viele Befürworter. Herr Machlup ist ja schon einmal
erwähnt worden. Ich glaube, die beste Studie, die es überhaupt auf
diesem Gebiet gibt, hat Herr Machlup für den Senat in den USA
verfasst. Herr Machlup sagt, im Grunde ist es so, dass alle Argumente
schon im letzten Jahrhundert gefallen sind. Es ist nichts Neues mehr
dazu gekommen. Wohl haben sich im letzten Jahrhundert Juristen gegen
Ökonomen durchgesetzt. Warum? Das müsste man anderswo erörtern. Danke.

#HWt: Herzlichen Dank, Herr Prof. Lutterbeck. Ich werde nachher noch einmal
danach fragen, was die Konsequenzen aus dem Gesagten sind, aus der
europäisch-amerikanischen Situation. Ob man hier dann von Europa her
ein Signal setzen kann, sich in die eine oder andere Richtung
anschließen, das würde mich interessieren. Aber, ich will jetzt in
keinem Falle voranschreiten, zumal wir drei Themenkomplexe haben. Wir
hatten gesagt bis ca. 18.00 Uhr, aber ich glaube, es ist niemand
traurig - die einen oder anderen haben auch angedeutet, etwas früher
zum Flieger zu müssen -, wenn wir es etwas früher schaffen. Mein
Vorschlag wäre, dass wir uns für die drei Themenkomplexe jetzt
ungefähr je dreißig Minuten nehmen, wenn es kürzer ist, ist das auch
kein Drama. Dann hätten wir ungefähr eineinhalb Stunden und wären dann
bei 17.30 Uhr, dann sehen wir auch, wie weit wir mit den einzelnen
Fragestellungen sind. Ja, herzlichen Dank noch einmal den Expertinnen
und Experten, auch für die große Disziplin, was die fünf Minuten
anlangt, das haben wir hier sehr selten im Haus, insofern haben wir
tatsächlich bis jetzt unseren Zeitrahmen voll eingehalten.

#Een: Erster Themenkomplex: Allgemeine Grundlagen und Ausgangslage.
Zweitens: Änderungsbedarf. Drittens: Auswirkungen. Bleiben wir
zunächst bei den allgemeinen Grundlagen, zwei Wortmeldungen. Ich
beginne bei Herrn Kollegen Dr. Mayer.

#Aar: Abg. Dr. Martin Mayer

#Meo: Meine Damen und Herren, ich möchte zunächst noch zwei Vorbemerkungen
machen, nämlich, dass ich die Sachverständigen bitte, Fachbegriffe
dann möglichst auch zu erläutern. In Vorbereitung auf die Anhörung
habe ich bei zwei Sachverständigen Begriffe gefunden, wie
%(q:inkrementell). Ich habe dann aber im Internet gefunden, dass das
so etwas ähnliches heißt wie evolutionäre Vorgehensweise usw. Und auch
den Begriff %(q:intrinsisch), das heißt, von innen her kommend, z.B.
intrinsische Motivation. Also, ich bitte, bei den Stellungnahmen
Begriffe zu erläutern. Das Zweite ist: Es ist - nicht hier in dem
Raum, aber im Netz - bereits Kritik daran laut geworden, dass der
Fragenkatalog in dieser Anhörung zu wenig ausgereift sei und dass die
Anhörung zu kurzfristig sei. Ich will, nachdem ich den Fragenkatalog
mit meiner Mitarbeiterin, Frau Braun, im Wesentlichen auch mit
gestaltet habe, sagen, man kann alles besser machen und ich war auch
nicht bei denen, die gesagt haben, es ist so eilig. Aber ich stehe
auch dazu, dass die Anhörung heute ist, weil es ein Thema ist, mit dem
wir einfach einmal anfangen müssen. Ich glaube auch nicht, dass wir
heute sozusagen schon zu einer endgültigen Beurteilung des
Sachverhaltes kommen können. Das zunächst als Vorbemerkung. Ich möchte
auch als Antragsteller noch dazu sagen - der Antrag ist ja nun auch
mit ein Anlass für die Anhörung -, dass ich durch die Initiative des
Herrn Pilch auf dieses Thema gestoßen bin. Ich teile dessen
Auffassungen und vor allem auch seine Art nicht in allen Punkten, um
es mal vorsichtig zu sagen, aber, jedenfalls möchte ich das hier auch
einmal sagen, damit seine Arbeit hier genannt wird. Das ist eine
eigene Initiative im Netz, und das ist im Übrigen auch ein Beispiel
dafür, dass sich die politische Diskussion mit dem Internet in neuen
Formen vollzieht.

#Ien: Ich habe jetzt eine Reihe von Argumentationen gehört und war ein
bisschen irritiert, Herr Henckel, dass Sie nun ein Beispiel dafür
gebracht haben, dass Patente nicht innovationsfördernd sind, was eben
nicht ein Beispiel von Software ist, sondern offenbar von Hardware,
denn in der Zeit, als das war, war das ja nun Hardware. Das ist meine
Frage im Grunde an die, die dazu etwas sagen möchten: Bezieht sich die
Aussage, dass Patente innovationshemmend sein können oder sind,
ausschließlich auf Software oder generell auf Patente? Herr Probst,
Sie haben ja die Studien angeführt. Da möchte ich auch noch einmal die
Frage an diejenigen stellen, die eher die Patentierung von Software,
die Ausweitung der Patentierungsmöglichkeiten von Software, vertreten:
Gibt es denn in jüngster Zeit andere Studien? Gibt es Studien, die
belegen, dass Patentierung von Software ein hervorragendes Instrument
ist, weil es Innovationen fördert? Wir müssen ja in der Politik immer
abwägen, da möchte ich auch ganz gern die beiden Seiten hören und die
Frage stellen, wer hat welche Studien, um die Dinge zu untermauern?

#HiW: Herzlichen Dank.

#Izg: Ich habe über unsere Regularien, die wir hier normalerweise haben,
übrigens nicht gesprochen. Kollege Dr. Mayer hat sich nichtsdestotrotz
daran gehalten. Wir haben so eine Übung, damit es auch ein bisschen
schneller geht, sich lieber mehrmals zu melden und sich dafür mit zwei
Fragen an einen Sachverständigen oder an eine Sachverständige oder mit
je einer Frage an zwei unterschiedliche Sachverständige zu begnügen;
das sollten wir heute auch so beibehalten. Die Nächste wäre die
Kollegin Griefahn. In Reihenfolge haben sich dann gemeldet: Die
Kollegen Otto, Neumann und Kelber, aber zunächst Frau Kollegin
Griefahn, die ich übrigens in Abwesenheit schon als
Ausschussvorsitzende für Kultur und Medien recht herzlich begrüßt
habe.

#Aif: Abg. Monika Griefahn

#Jaa: Ja, ich war zwei Minuten zu spät, weil ich ein bisschen langsamer bin
als sonst mit dem Fahrrad. Entschuldigung. Nachdem ich so die erste
Runde gehört habe und jetzt auch noch einmal besonders von Herrn Prof.
Lutterbeck gehört habe, dass die Entwicklung in den USA so ist, wie
sie ist - wir ja wissen, dass alle Entwicklungen aus den USA
spätestens mit fünf Jahren Verspätung, jetzt auch etwas schneller, zu
uns kommen - , stellt sich für mich noch einmal die Frage: Selbst wenn
wir uns freiwillig entscheiden würden, keine Ausweitung der
Patentierung vorzunehmen, um genau das, was einige von Ihnen
dargestellt haben, die Innovation, die Freiheit etc. nicht
einzuschränken, ob es nicht trotzdem zwangsläufig notwendig ist, weil
eben in den USA, natürlich auch weltweit, patentiert wird bzw. weil es
Firmen gibt, die dann weltweit patentieren und diese Programme hier
eben auch angeboten werden. Und für mich stellt sich noch einmal die
Frage, die auch im Fragenkatalog ist, wie viele Patente denn
tatsächlich im Vergleich Deutschland - Europa - USA proportional
existieren? Ich weiß jetzt nicht, wer das beantworten kann, ob Sie das
auch beantworten können, damit wir einfach noch einmal klarer sehen,
wohin die Entwicklung geht? Denn wenn große Häuser jetzt
Patentabteilungen aufbauen, wenn sie jetzt anfangen, stärker zu
patentieren, haben dann die anderen die Chance, hinterher zu kommen
und müssen sie dann nicht zwangsläufig auch patentieren? Das sind zwei
Fragen - ich weiß nicht, wer sie beantworten kann, ich bitte einfach
die Sachverständigen selber, da noch einmal das Wort zu nehmen. Also,
ich würde erst einmal sagen, es sind wahrscheinlich Herr Lutterbeck
und vielleicht auch noch Herr Henckel oder Herr Probst, ich weiß
nicht.

#Ije: Ich habe jetzt notiert, Lutterbeck und Henckel. Kollege Otto, bitte.

#AJt: Abg. Hans-Joachim Otto

#Inl: Ich habe zwei Fragen. Die erste richtet sich auch an Herrn Prof. Dr.
Lutterbeck, in einem ähnlichen Zusammenhang wie eben die Frau Kollegin
Griefahn gefragt hat. Sie haben eben gesagt, aus Ihrer Sicht - trotz
Ihrer Meinung, die ja für Deutschland eine andere ist - hätten Sie
Verständnis dafür, dass SAP wegen seiner starken Marktausrichtung auf
die USA jetzt stärker eine Patentabteilung aufbaut usw. Meine ganz
naive Frage ist, wie wirkt sich eigentlich der unterschiedliche
Schutzumfang praktisch auf die Unternehmen in USA im Vergleich zu
Deutschland aus? Warum sind Sie der Meinung, dass SAP in Deutschland
auf den Schutz verzichten kann, ihn aber in den USA braucht? Das ist
eine ähnliche Frage, aber noch ein bisschen präziser, aus praktischer
Sicht. Ich bin selber Jurist und stelle deswegen auch meine zweite
Frage an Frau Bouillon als Praktikerin. Sie haben ja in Ihrem Papier,
das ich auch mit großer Aufmerksamkeit gelesen habe, durchaus gesagt,
wir wollen im Prinzip keine Schutzlosigkeit für Software, also, es
muss schon einen Schutz von geistigem Eigentum geben. Meine Frage: Wie
sehen Sie eigentlich als Praktikerin den Unterschied zwischen
Urheberrechten und Patenten? Was ist das, was Sie als junges
Unternehmen besonders behindert bei Patenten im Unterschied zu den
bestehenden Urheberrechten, die ja unstreitig auch für Software
bestehen? Sie haben ein Beispiel erwähnt, der große Aufwand, Patente
zu finden. Ist es nicht möglicherweise so, dass der Aufwand noch
größer ist, Urheberrechte zu finden, weil die nicht so schön im
Patentregister eingetragen sind? Sie schreiben ein ganz tolles
Programm in Ilmenau und da kommt irgendeiner her und sagt, also, Frau
Bouillon, das haben wir schon vor fünf Jahren entwickelt, geben Sie
diese Entwicklung frei. In dem Zusammenhang dann auch die Folgefrage:
Sie sagen - und nicht nur Sie, auch andere -, Patente sind in diesem
Bereich innovationshemmend. Das würde mich gerade im Unterschied zu
Urheberrechten interessieren: Warum sind Patente für Sie
einschneidender als das bestehende, unstreitig ja von allen Referenten
auch eingeräumte Instrumentarium der Urheberrechte? Wo ist der
praktische Unterschied?

#Hcl: Herzlichen Dank. Es kommt Herr Neumann, ihm folgt Herr Kelber und dann
schlage ich vor, dass wir eine erste Antwortrunde machen. Oder habe
ich jetzt von den Kolleginnen und Kollegen jemanden übersehen? Das ist
nicht der Fall. Kollege Neumann.

#AdW: Abg. Bernd Neumann

#Nmt: Nach der ersten Runde des Hearings wäre mir nicht klar, was wir
eigentlich machen sollten, aber das ist häufiger bei Hearings so, so
dass sich jeder denn daraus die Position, die er hatte, noch einmal
begründet oder sie dementsprechend ablehnt. Also, die einen sagen
Patentierung ja, die anderen sagen nein. Ich habe mir von denjenigen,
die Patentierung - bezogen auf Software - kritisch hinterfragt haben,
die Argumente angehört. Der größere Teil der Argumente, die Sie
dagegen gebracht haben, würde sich grundsätzlich gegen Patentierung
richten. Ich habe ja nicht gesagt, alle, denn die Tatsache, dass
mittelständische Unternehmen sich schwerer tun, gibt es auch im
normalen Bereich. Die Tatsache, dass Großunternehmen gleichzeitig
viele Patente anmelden, das war ein Argument, haben wir auch im
klassischen Bereich. Das war ja auch der Grund, weswegen wir in der
letzten Regierung massiv im Hinblick auf die Sinnhaftigkeit von
Patentierung gerade für mittelständische Unternehmen, für KMU, ganze
Programme aufgelegt haben - nicht bezogen auf Software -, die ihre
Fähigkeit, Patente anzumelden und damit umzugehen, stärken. Jetzt
sagen einige, für Software trifft das so alles nicht zu. Meine Frage
geht möglicherweise an Herrn Schiuma, aber auch an andere. Kann nicht
auch ein Mittelweg sinnvoll sein? Man muss ja nicht das Kind mit dem
Bade ausschütten. Ich will nicht akzeptieren, dass grundsätzlich
Patentierbarkeit bei Software nicht geht und völlig abwegig ist, zumal
ich unterschiedliche Zahlen höre, wenn auch anders ausgerichtet, von
Softwarepatenten, die es bereits gibt. Kann es nicht richtig sein zu
sagen, o.k., der Grundsatz der Patentierung im Hinblick auf
Innovation, Lizenzierung, usw. gilt auch hier, aber er muss der
besonderen Situation der Software Rechnung tragen? Ich war kürzlich in
einem Kreis, da kam dann nach Diskussion etwa Folgendes heraus - und
ich frage Sie, ob das ein Weg ist: Also, prinzipiell %(q:ja) zur
Patentierbarkeit von Software, wer generell OpenSource will, braucht
ja nicht zu patentieren. Aber bestimmte Prämissen, wie z.B.
Patentzeiten deutlich kürzer, z. B. fünf Jahre, um die besondere
Situation der kürzeren Zyklen zu berücksichtigen. Oder, Quellcode
offenlegen, das würde sicherlich mit USA Schwierigkeiten bereiten, um
eben auch nicht von vornherein OpenSource-Motivation, die ja im
Prinzip positiv zu sehen ist, lahm zu legen; Schnittstellen freihalten
ja, das waren so die Kriterien, die das Ergebnis einer Diskussion
waren, wo gesagt wurde, im Prinzip ist auch hier Patentierung sinnvoll
- bezogen auch auf die Frage von Frau Griefahn, USA kann nicht völlig
verrückt sein. Wenn aber Patentierung, dann muss man der besonderen
Situation von Software durch besondere Kritierien Rechnung tragen.
Sie, Herr Schiuma, haben gesagt, pro Patentrecht. Ich habe Sie so
verstanden, kein Sonderpatentrecht, ist das das letzte Wort oder
könnten Sie sich auch etwas Differenziertes vorstellen?

#VmW: Vielen Dank, Herr Kollege Neumann. Sie sagten, Schiuma und andere.
Wegen der Grundsätzlichkeit würde ich jetzt den Herrn Siepmann fragen.
Herr Kollege Kelber.

#Aie: Abg. Ulrich Kelber

#Ver: Vielen Dank, Herr Vorsitzender. Meine erste Frage geht an Herrn Dr.
Schiuma. Sie haben ja am Anfang erwähnt, dass Sie Physiker sind, nur
die Begründung - das mögen mir die Kollegen Juristen nachsehen - war
eine eher juristische, die Sie dann gebracht haben. Sie haben
dargestellt, wie Patentrecht schon immer existiert hätte und Sie sich
nicht vorstellen könnten, dass es eine grundsätzliche Überlegung gäbe,
etwas zu verändern. Mich würde doch an der Stelle etwas stärker
interessieren, ob es aus Ihrer Sicht an keiner Stelle diese
spezifischen Bedingungen im Softwarebereich gibt, die dort besondere
Behandlung notwendig machen, eben keine reine Ausbreitung ermöglichen
und das insbesondere auch noch einmal auf dem Hintergrund des ja
ebenfalls auch aus ökonomischen Gründen gewünschten Weges hin zu
Standards, insbesondere bei Schnittstellen. Die zweite Frage, ich weiß
gar nicht, ob man das überhaupt nachfragen darf, ich tue es aber
einfach mal, geht an Frau Bremer. Wir haben, zumindest in Deutschland,
einen existierenden Wettbewerb in der Softwaretechnologie mit vielen
kleinen und mittleren Unternehmen und einigen großen Unternehmen, bei
dem man nicht das Gefühl hat, dass es eine beherrschende Marktstellung
in allen Bereichen gibt, sondern im Gegenteil, dass es immer wieder
Angriffe auf die beherrschende Situation gibt. Wenn aus einer solchen
Situation heraus die absolute Mehrzahl der Unternehmen sagen würde,
wir möchten diesen Wettbewerb noch mit einem weiteren gestaltenden
Instrument versehen, dann wäre es ein starkes Argument für eine
politische Entscheidung. Daher würde mich schon die Position
interessieren, die BITKOM als relativ junger Verband an der Stelle
gefunden hat. Ist das eine sehr einhellige Position, oder immer noch
eine umstrittene innerhalb Ihres Verbandes?

#HgD: Herzlichen Dank. Soweit die erste Fragerunde. Es gingen drei Fragen an
Herrn Schiuma, insofern würde ich mit ihm auch beginnen. Das waren die
Fragen der Kollegen Dr. Mayer, Neumann und Kelber.

#VWW: Vielen Dank. Die erste Frage bezog sich auf Studien, ob es Studien
gebe, die beweisen könnten, dass Patente Innovationen fördern. Dort
würde ich mich eigentlich gerne zurückhalten, weil ich, wie gesagt,
nicht aus der wirtschaft- oder volkswirtschaftlichen Seite komme.
Daher glaubte ich auch, dass es eher an Herrn Probst gerichtet war als
an mich. Die Fragen hingegen von Herrn Neumann und Herr Kelber kann
ich vielleicht auch in einem Punkt zusammenfassen. Es ging um die
Frage der juristischen Behandlung, die mir näher liegt. Ich bin zwar
Physiker, habe aber eine patentanwaltliche Ausbildung durchlaufen,
habe in Jura promoviert und beschäftige mich am Max-Planck-Institut
aus patentrechtlicher, d.h. wissenschaftlicher, aber eben juristischer
Sicht mit eben dieser Problematik und lege da den Schwerpunkt durchaus
auf Patentrecht. Natürlich schaue ich auch auf das Urheberrecht, aber
dort habe ich einige Schwierigkeiten gesehen. Diesbezüglich kann ich
gleich die Überleitung machen zu der Problematik der Schnittstellen.
Die Schnittstellen haben innerhalb des Urheberrechtes eine besondere
Rechtsstellung in der EU-Richtlinie erlangt, die einen Schutz für
Software mittels des Urheberrechtes vorschreibt und mittlerweile auch
in allen Mitgliedstaaten umgesetzt worden ist. Darin wurde der
besonderen Rolle der Schnittstellen dahingehend Rechnung getragen,
dass die Schnittstellen dem urheberrechtlichen Schutz nicht zugänglich
sind.

#Ain: Aus der patentrechtlichen Sicht ist das Problem, glaube ich, ein
bisschen anders einzuordnen. Beim Urheberrecht wurde die Software
singulär in das Urheberrecht eingefügt. Viele Stimmen sagen, es wurde
in ein Korsett gezwängt, das eigentlich nicht richtig passt und im
Ergebnis auch meiner Meinung nach nicht genau das erzielt, was es
erzielen sollte. Aber dort wurde eine Regelung gezielt ausschließlich
auf die Software bezogen. Das heißt, durch das Urheberrecht ist kein
anderer Technikbereich - einmal ausgenommen von Datenbanken - in dem
Sinne geschützt; kein weiterer Schutz für eine klassische Maschine
oder eine integrierte Schaltung oder wie auch immer. Hingegen besteht
beim Patentrecht diese Schnittstellenproblematik durchaus auch in
anderen Bereichen. Ich möchte jetzt einmal beispielhaft lediglich die
Elektronikindustrie insbesondere im Hinblick auf die Chiptechnologien
nennen. Auch dort gibt es durchaus Schnittstellenproblematiken und
dort hat sich noch nie jemand Sorgen gemacht, dass bei diesen
Schnittstellen Patentrechte hinderlich seien. In der Tat ist es so,
dass man auch ganz allgemein zur Frage der Standardisierung, die ja
auch in einem anderen Zusammenhang aufgekommen ist - ich glaube, Frau
Bouillon hatte den Punkt aufgeworfen -, sagt: Ja, wenn ein Standard
nun patentiert ist, dann ist das eben sozusagen eine Monopolstellung
für diesen einzelnen Patentinhaber und der kann dann tun, was er
möchte, er hat eine vorherrschende Marktposition. Aber da gibt es
einige gute Beispiele - da möchte ich jetzt nicht auf die
volkswirtschaftlichen Aspekte eingehen, die möchte ich gerne den
Kollegen aus der wirtschaftswissenschaftlichen Branche überlassen - in
der patentrechtlichen Branche, die zeigen, dass gerade, wenn man einen
Patentschutz hat und der sich restriktiv auf einen besonderen Bereich
auswirkt, sich dieser Bereich gerade nicht als Standard entwickelt.
Dementsprechend verzeichnet man eigentlich heute genau den umgekehrten
Weg.

#Mii: Man erlangt zwar auch Schutzrechte, man erlangt auch Patente, aber man
gibt diese Patente in einen offenen Pool hinein. Natürlich macht man
das nicht nur aus Altruismus, nur aus Rücksichtnahme auf die
Konkurrenten, man erhält natürlich auch Lizenzgebühren, aber man hat
eben festgestellt, dass, wenn man ein Monopol auf einen möglichen
Standard hat, der sich noch nicht durchgesetzt hat - und ein Patent
muss man eben in einem sehr frühen Stadium anmelden, wenn dieser
Standard sich noch nicht durchgesetzt hat - und man ein Patent
restriktiv einsetzt, sich dieser mögliche Standard sehr schwer tut,
sich zu behaupten. Das mag vielleicht in einigen Beispielen, die
glücklicherweise auch nicht aus der Software waren, sondern außerhalb
der Software lagen, dann trotzdem gelungen sein, aber es gibt genügend
Beispiele, die zeigen, dass sich das nicht durchsetzt. Man denke
meinetwegen an die unterschiedlichen Formate von Videokassetten, die
es gab: Was hat sich durchgesetzt? Nicht der Bereich, in dem es
Patentschutz gab. Dementsprechend verzeichnet man in der
Telekommunikationstechnologie z.B. heute diese Patentpool-Politik,
indem dieser Patentpool zur Lizenzierung offen an dritte Unternehmen
gegeben wird, die sich daran beteiligen können, so dass in diesem
Standardisierungsbereich die Patentrechte nicht als Hemmung eingesetzt
werden. Es wird gesagt, gut, du darfst das auch benutzen und ich
bekomme im Gegenzug eine Lizenzeinnahme. Dies ist im Bereich der
Telekommunikation gerade bei Handys sehr stark der Fall, das ist auch
der Fall bei der DVD-Technologie, also die
Digital-Video-Disc-Technologie. Dementsprechend muss ich sagen, was
diese Standards angeht, da würde ich zurückhaltend sein, im
Patentrecht nun eine besondere Ausnahmesituation alleine für die
Softwareschnittstellen vorzusehen, denn dann übergeht man alle anderen
möglichen Schnittstellen, die auch existieren, und bislang hat dies
eigentlich meiner Kenntnis nach nie zu größeren Problemen geführt.
Vielen Dank.

#Dee: Danke schön. Herr Prof. Lutterbeck, bitte.

#Ems: Entschuldigung, Herr Dr. Mayer, dass ich Fremdwörter benutzt habe.
Normalerweise ist Kriterium für Papiere von mir, für viele jedenfalls,
dass meine Frau sie versteht, und sie ist nicht vom Fach.
Entschuldigung.

#ItW2: Ihre Frage war, ob es befürwortende Stimmen gibt. Da würde ich ganz
allgemein sagen: Der Bereich der Mikroökonomie, also
Innovationsforschung, die von Betriebswirten gemacht wird, die haben
überwiegend eine positive Auffassung zu dem Ganzen. Das liegt aus
meiner Sicht daran, dass Sie natürlich das einzelunternehmerische
Interesse bewerten müssen; wie mein Beispiel SAP zeigte, wäre es ja
von diesem Einzelinteresse töricht, würde es sich anders verhalten
Aber die Studie, die ich hier in meiner Stellungnahme zitiere von
Herrn Lerner von der Harvard Business School, der das auch so sehen
würde, belegt das auch ganz präzise mit Zahlen, um am Schluss aber zu
einem %(q:Aber) zu kommen; das %(q:Aber) ist ein volkswirtschaftliches
Argument. Sie sehen hier eine Kurve, das sind die Patente; Sie sehen
einen dramatischen Anstieg in den letzten Jahren aller Patente, nicht
nur Software. Daraus kann man schon auf eine bestimmte Dynamik
schließen, aber es gibt sozusagen keinen Beweis dafür, dass die
Ausgaben für Research usw. zugenommen haben; im Gegenteil, sie haben
abgenommen. Und dieser Widerspruch zwischen einerseits der Dynamik,
die sich überhaupt nicht bestreiten lässt, und andererseits der
Tatsache, dass gesamtgesellschaftlich keine Innovation dazu gekommen
ist, ist bisher nicht aufgelöst. In dem Sinne lässt sich die Frage
nicht genauer beantworten.

#Ela: Ein weiteres Problem ist, dass es wenige deutsche Zahlen gibt. Aus
vielen Gründen ist man immer darauf angewiesen, amerikanische Zahlen
zu benutzen. Frau Griefahn, Sie haben ja auch nach Zahlen gefragt.
Leider habe ich jetzt die SAP-Zahlen nicht dabei, die sind in der
Tendenz so, dass es auf dem USA-Markt gewaltige Patentanmeldungen von
Japanern und Amerikanern, ganz wenige von Europäern und verschwindend
wenige von Deutschen gibt. In einer Statistik über Softwarepatente in
den USA für 1997, die ich jetzt vor mir liegen habe, ist etwa SAP mit
null Patenten drin, obwohl sie schon damals einen Marktanteil von
wahrscheinlich zwanzig Prozent hatten. Da ist also etwas offen, aber
genauer kann ich das jetzt nicht klären. Die genauesten Zahlen kenne
ich, wie gesagt, von der SAP, und da sieht man nur den deutlichen
Unterschied, Europäer und Deutsche beteiligen sich nicht daran; aus
welchen Gründen auch immer. Ich möchte die schwierigste Frage, die von
Herrn Otto, zum Schluss beantworten. Ich glaube schon, als
Wissenschaftler hat man es insofern einfacher, als man hier sagt, ich
bin ja nur Wissenschaftler, Sie sind Politiker und müssen die
praktischen Lösungen finden. Ich habe aber sowohl in meiner
Stellungnahme wie sonst auch gesagt, man muss eine Strategie
entwickeln, die verschiedene Optionen möglich macht. Ich bin mit
anderen der Auffassung, dass das ganze Property-Rights-Regime
verändert werden muss, aber wir leben ja in der Jetzt-Zeit. Da muss
man praktische Lösungen finden, eine habe ich genannt. Das könnte ein
Quelltextprivileg sein.

#Jnr: Jetzt zu Ihnen, Herr Otto, Sie haben das ja angesprochen. Ich glaube
schon, das ist ein sehr großer Widerspruch. Ich habe auch mit dem
Verantwortlichen von SAP lange über diesen Widerspruch geredet. Ich
persönlich würde sagen, das riecht danach, dass die Deutschen und die
Europäer einen eigenen Weg finden müssen. Das ist auch schwer genug.
Ich würde meinen, dass dieser eigene Weg jedenfalls im Bereich von
OpenSource denkbar und auch ökonomisch sinnvoll ist. Ich würde die
Frage im Augenblick mit diesem Hinweis noch einmal zurückstellen. Wie
ich in der Stellungnahme schon gesagt habe, glaube ich, es handelt
sich hier um das schwierigste Problem des ganzen Bereichs überhaupt.
Als Politiker würde man normalerweise sagen: Da muss ich eine
Paketlösung haben. Wenn ich im Softwarebereich etwas durchsetzen will,
muss ich in anderen Bereichen vielleicht zurückgehen, das ist ganz,
ganz schwierig, weil es die deutsch-amerikanischen
Wirtschaftsbeziehungen oder die europäisch-amerikanischen betrifft, da
bin ich nicht der Kompetenteste. Gut, das war es eigentlich soweit.

#Brn: Besonderheiten von Software, die Frage von Herrn Kelber. Natürlich
gibt es Eigenschaften von Software, die es eben so schwierig machen,
sie zu bewerten. Software, das sind Texte, aber diese Texte
funktionieren; Sie kaufen einen Text, eine Software, im Laden, Sie
kaufen ihn aber nicht, weil Sie ihn wie einen Text von Goethe lesen
wollen, sondern weil Sie wollen, dass er eine Funktionalität hat. Und
das ist der große Unterschied zu allen anderen Techniken, die es
bisher gegeben hat und nach meiner Meinung auch die Schwierigkeit. Das
gleiche können Sie sowohl als Hardware wie als Software realisieren,
prinzipiell jedenfalls. Weil das so ist, ist es bisher nicht gelungen,
sie einerseits in die Technik oder ins Geistige einzuordnen. Man
schwimmt immer hin und her, und die Gerichte haben im Grunde das
gemacht, was der Markt wollte. Sie sind an einem Punkt angekommen, den
ich vorhin genannt habe bei Herrn Melullis, wo er sagt, wenn wir einen
bestimmten Weg weiter gehen, da stoßen wir gegen eine Wand. Aber
präzise begründen kann auch er das nicht. Nur, es gibt diesen Punkt,
den wir nie erreichen dürfen.

#JgF: Ja, ganz herzlichen Dank, damit habe ich jetzt erst festgestellt, dass
der Kollege Kelber drei Sachverständige angesprochen hatte, aber o.k.,
es ist durchgegangen. Übrigens, ich erhalte gerade eine Nachricht für
die Mitglieder des Unterausschusses %(q:Neue Medien): Der Ältestenrat
hat soeben unser Projekt %(q:e-Demokratie) beschlossen. Die
Nichteingeweihten können nicht ermessen, welcher Prozess dahinter
steht, für alle Eingeweihten ist es, glaube ich, ein Grund zur Freude.
Gut, wir fahren aber fort. Es gab noch weitere Fragen. Ich mache jetzt
eine Reihenfolge: Von Herrn Dr. Mayer an Herrn Probst, von Herrn Otto
an Frau Bouillon, von Herrn Kelber an Frau Bremer und dann noch von
Herrn Neumann an Herrn Siepmann. So, in dieser Reihenfolge würde ich
jetzt auch aufrufen, bitte schön.

#VWr: Vielleicht interpretiere ich das falsch, Herr Dr. Mayer, aber es klang
so, als würden Sie mir unterstellen, ich wäre irgendwie gegen Patente
oder würde am Sinn von Patenten allgemein zweifeln. Das ist natürlich
überhaupt nicht der Fall. Ich bin Wirtschaftstheoretiker von der
Ausbildung her, ich bin kein Empiriker. Man stellt mir eine Frage und
auf theoretischer Ebene sage ich, das spricht dafür und das dagegen.
Es gibt bei Patenten Vor- und Nachteile. Da muss man spezifisch auf
Brancheneigenschaften abstelllen. Für diese Branche würde ich sagen,
spricht mehr dagegen als dafür. Es gibt andere Branchen, wo man
natürlich sagen kann, wenn es kapitalintensiv ist, dann braucht man
schon ein stärkeres Schutzrecht, um diese Kapitalinvestition wieder
zurück zu bekommen. Es ist also hier eine heikle Frage, wo man
unterscheiden muss, bei dieser Branche %(q:ja), bei dieser %(q:nein).
Von der Theorie her ist es - aber, ich wollte noch vermitteln -
allgemein gar nicht klar, dass eine Verstärkung der Schutzrechte weder
zwangsläufig im Sinn der Unternehmen noch zwangsläufig im Sinn der
Gesellschaft ist. Das sind Fragen, die spezifisch sind und die man
sich in einem spezifischen Kontext anschauen muss.

#IhW: In diesem Kontext hier möchte ich vor allem auf Netzwerkeffekte
eingehen, jetzt ohne Fachwort, also, bei Software kennt man das: Je
mehr beispielsweise Microsoft Office benutzen, desto mehr andere
müssen das auch benutzen usw. Über Standards haben wir schon
gesprochen. Herr Siepmann hat auch schon verhältnismäßig geringe
Markteintrittsbarrieren anklingen lassen. Spezifisch bezogen auf
OpenSource wurden auch sehr große Asymmetrien angesprochen bezüglich
Möglichkeiten, Patentrechtsverletzungen zu beurteilen oder
festzustellen, also, asymmetrische Verhandlungspositionen, wenn es
darum geht, Portfoliokreuzlizenzierungen durchzuführen. Das sind
spezifische Effekte, die auf diese Branche zutreffen, teils gilt das
auch für andere Branchen. Und da muss man eben gucken. Und hier, im
Fall von Software, spricht also die Mehrheit dieser theoretischen
Überlegungen eher für ein möglichst schwaches Schutzrecht, damit man
diese Nachteile, die durch diese Schutzrechte entstehen, möglichst
gering hält, weil man die Vorteile für nicht so überwiegend groß
einschätzt. Wenn Sie jetzt die empirische Frage stellen - also, wie
gesagt, ich bin Theoretiker, ich möchte nicht die eine oder andere
Position vertreten, ich bin da eigentlich hoffentlich vollkommen
neutral. Ich habe alle möglichen Studien gesucht, und natürlich hätte
ich gerne Studien %(q:pro) gesehen. Vielleicht, wenn ich eine
ethologische Stellung gehabt hätte, hätte ich sagen können, die Studie
ist falsch, weil die das falsch gemacht haben. Aber es ist mir leider
nicht gelungen, irgendein halbwegs seriöses Papier dieser Art zu
finden, auch nicht bei Lerner, Joffe im Bereich Harvard/MIT auf dem
amerikanischen Markt. Ich habe auch gerade gestern bei Kollegen im ZDW
in Mannheim jüngste Untersuchungen gesehen, die Patente betrachtet
haben in Deutschland, mit deutschlandspezifischen Zahlen, allerdings
auch in einem sehr vorläufigen Stadium. Es ist extrem schwierig, im
Hochtechnologiebereich, wo die Entwicklungszeiten sehr gering sind,
festzustellen, dass Patente die Anreize in diese gesellschaftlich
wünschenswerte Richtung lenken. Ich will nicht sagen, dass es beweist,
dass die Patente da nicht gut sind. Es gibt keine Evidenz. Und wenn
wir die Frage hier stellen, wollen wir von einem Status quo in eine
bestimmte Richtung gehen, dann muss ich als Theoretiker sagen, bevor
man diesen Schritt macht, muss man mir doch irgendwie halbwegs
belegen, dass das sinnvoll ist. Wenn ich mir dann theoretisch im
Stübchen überlege, das klingt nicht so schlau, und alle empirischen
Untersuchungen sagen entweder, das ist nicht schlau, es führt zu den
falschen Effekten, oder sie sagen, es wäre zwar schön, das zu belegen,
weil wir dafür bezahlt wurden, aber es gelingt uns leider nicht zu
belegen, dass die Anreize in die Richtung gehen, dann kann ich das
nicht ändern. Ich würde sehr gerne da Studien zitieren.

#Brt: Bezüglich kleiner und mittelständischer Unternehmen gilt: die Studien,
die ich diesbezüglich kenne, sind einerseits von dem UK Economic and
Social Research Council, da gab es eine riesig angelegte Serie von
zwanzig, dreißig Studien mit einem Gesamtvolumen von 1,2 Mio. Pfund,
wo man untersucht hat, wie KMU in England, Deutschland und Amerika
Patente verwenden. Und da ist man zum Schluss gekommen, dass es -
natürlich, wie ich vorhin betont habe - Sektoren gibt, die das
benutzen. Ich will auch nicht bestreiten, dass es KMU gibt, die
Patente benutzen, aber der überwiegend große Anteil von KMU in dem
Technologiesektor benutzt Patente nicht, weil er bessere
Schutzmechanismen hat. Und diese Schutzmechanismen sind, schneller auf
dem Markt zu sein, schneller als die Konkurrenz zu sein. Sie haben
Angst, dass sie mit einer Patentanmeldung Informationen preisgeben,
sprich, sie arbeiten mit Betriebsgeheimnissen, sie arbeiten mit
anderen Mitteln. Auch da kann ich nicht irgendwie drüber hinweg
wischen, das sind Studien, die nicht ich gemacht habe, die ich als
Theoretiker einfach hinnehmen muss. Und wenn ich da die Frage
betrachte, soll man von dem Status quo in diese Richtung gehen, muss
ich sagen, ich kenne nichts, was dafür spricht.

#Hnn: Herzlichen Dank, Herr Dr. Probst. Nur für das Protokoll und zur
Klarstellung eine kleine Nachfrage, weil irgendwo, glaube ich, in
Ihren Ausführungen ein Halbsatz gefehlt hat. Das ist kein Problem,
vielleicht hat es auch an mir gelegen. Sie sprachen im Zusammenhang
mit den Mängeln an Studien von MIT-Studien etc. und verschiedenen
anderen. Heißt dies, dass die MIT-Studie zu einem anderen Ergebnis
gekommen ist, also zu einem Kontra-Ergebnis?

#Doi3: Dr. Daniel Probst, Universität Mannheim

#Nit: Nein, nein. Entschuldigung. Es gibt diverse Studien, MIT-Studien, es
gibt National Bureau of Economic Research-Studien, es gibt Studien,
die ich vorhin zitiert habe, vom UK Economic and Social Research
Council, es gibt diesbezügliche Berichte aus Auftragsstudien an die
Europäische Kommission, die diese Untersuchung zusammenfassen, und da
sieht man im Wesentlichen bezüglich dieser zwei Punkte, die ich vorhin
gemacht habe: Es gibt keine Evidenz, dass eine Verstärkung von diesen
Rechten zu einer höheren Forschungsintensität geführt hat.

#Wra: Wenn ich vielleicht noch einen Punkt ansprechen darf. Über Anzahl von
Patenten nachzudenken ist auch falsch. Ein Großteil der Patente, die
angemeldet werden, wird nachher wirtschaftlich nicht verwendet. Dass
die Anzahl ansteigt, sagt eigentlich noch gar nichts darüber, ob es
gesamtwirtschaftlich wünschenswert ist, diese Dinger zu haben. Es
können sehr wohl auch einfach Sperrpatente sein, wo man so sein
Portfolio absichert und sie in einem strategischen Umfeld benutzt,
zwischen Betrieben. Und das ist natürlich sehr wohl sinnvoll aus Sicht
einer SAP in Amerika, ihre Interessen mit Patentportfolios
abzusichern, weil sie dies brauchen, um Verhandlungen in Amerika zu
führen. Aber wenn es dann aus Sicht der Unternehmung sinnvoll ist,
heißt es noch lange nicht, dass diese Institution sinnvoll ist.

#Hei: Herzlichen Dank noch einmal für die Klarstellung. Frau Bouillon,
bitte. An Sie war die Frage des Herrn Kollegen Otto gerichtet.

#Imt: In der Frage ging es ja im Wesentlichen um den Unterschied zwischen
Urheberrecht und Patentrecht. Der wesentliche Unterschied besteht in
der Frage, was ist das geistige Eigentum? Also, wenn ich ein Buch
schreibe, ein Programm selber entwickle, dann sind das meine geistigen
Ergüsse, die ich dort - in welcher Form auch immer, in digitaler Form
oder in schriftlicher Form - niederlege. Das Urheberrecht richtet sich
in dem Fall dann vor allem auf den Schutz des Kopierens, d.h., wenn
wir Software entwickeln, wie Sie das vorhin so schön beschrieben
haben, und das unser geistiges Eigentum ist, dann ist es sehr, sehr
unwahrscheinlich, und das müßte schon mit dem Teufel zugehen, wenn man
jetzt auf eine vollkommen identische Idee, einen vollkommen
identischen Code kommen würde. Das Patentrecht geht im Wesentlichen
davon aus, eine Erfindung als solche zu schützen. Das heißt, wenn -
und dafür gibt es auch viele Beispiele, ganz bedeutende Beispiele,
auch gerade aus der Zeit der Renaissance - zum gleichen Zeitpunkt an
unterschiedlichen Orten in gleichen Gebieten geforscht und in gleichen
Gebieten Entdeckungen, Erfindungen gemacht wurden, dann sichert das
Patentrecht demjenigen, der es als allererster anmeldet, den Schutz
zu, in einem für uns unvorstellbar langen Zeitraum dieses Patent zu
benutzen bzw. zu lizenzieren. Das heißt, genau an dem Punkt wird es
eventuell kompliziert, wenn es um den Bereich Schnittstellen oder
Ähnliches geht, denn die Schnittstellen sind ja definiert. Wenn dann
vorhin gesagt wurde, dass das keine unüberwindbaren Hürden sind, so
glaube ich auch nicht, dass Patente unbedingt unüberwindbare Hürden
für das Entstehen von Patenten sein müssen, wohl aber diese behindern.
Und unsere Industrie ist momentan gerade an einem Punkt angekommen, wo
wir Standards brauchen, wo es notwendig ist, dass sich Standards
entwickeln. Der Entwicklungsaufwand von Software ist momentan immens.
Jeder, der sich ein bisschen damit beschäftigt hat, weiß, dass das
gerade auch ein Knackpunkt ist. Man muss sich einfach überlegen, wo
der Maschinenbau heute stünde, wenn jeder sein eigenes Gewinde
erfinden müsste?

#HBg: Herzlichen Dank. Frau Bremer, bitte, die Frage vom Kollegen Kelber.

#IWc: Ich möchte mich kurz vorher noch auf ein paar Punkte beziehen, wo ich
mich so halb angesprochen gefühlt habe.

#DeH: Das ist eine neue Version, aber bitte nicht allzu umfangreich, aber
%(q:Halbansprache) würden wir akzeptieren.

#WiS: Weil Sie auch noch einmal auf diese Studien kamen: Da würde ich gerne,
wenn eine Möglichkeit für uns besteht, auch noch etwas nachreichen,
wenn wir da etwas haben. Da sage ich ganz ehrlich, da muss ich auch
noch recherchieren. Ich möchte nur, weil es eine konkrete Frage war,
auch noch einmal auf diese Studie eingehen, die hier ja als maßgeblich
genannt wurde. Da möchte ich noch einmal zu bedenken geben, dass
eigentlich in diesen Studien in Bezug auf die R&D-Kosten keine
wirklich sachgerechte Maßzahl genommen wird. Denn es ist nicht
ausschlaggebend, was da für Kosten entstehen, anders gesagt, das hängt
nicht davon ab, wieviel Patente beantragt worden sind. Man kann das
hier nicht ins Verhältnis setzen. Die R&D-Kosten hängen vielmehr von
anderen Dingen ab, wie den wirtschaftlichen Faktoren in einem
Unternehmen, der Frage, wie geht es dem Unternehmen - da können
Flauten der Grund dafür sein, dass eben gerade weniger Ausgaben
entstehen. Das hat jetzt nicht zwangsläufig etwas damit zu tun, dass
hier weniger oder mehr Patente angemeldet worden sind. Das ist nicht
unbedingt so ins Verhältnis zu setzen. Und dann wollte ich auch gern
noch auf das von Herrn Otto angesprochene Thema %(q:Problematik des
Urheberrechts bei OpenSource) eingehen. Selbst wenn wir es absolut
herunterfahren würden, hätten wir in der Tat das Problem, dass man die
Urheberrechte ja auch wahren muss und man ein erheblich größeres
Problem hat, diese zu recherchieren. Selbst wenn wir hier jetzt nicht
so eine radikale Form, die ja etwas unwahrscheinlich ist, fahren
würden, sondern einen Mittelweg gehen: Es wird immer einen Teil
Patente geben, und auch da haben wir im OpenSource-Bereich erhebliche
Probleme, das nachzuvollziehen. Das heißt, es ist nicht nur ein
Problem, wenn es Patente gibt, sondern wir haben das Problem der
Verletzungsgefahr auch, wenn es keine Ausweitung der Patente gäbe.

#Jhn: Jetzt möchte ich ganz konkret auf die Frage von Herrn Kelber eingehen.
Das ist in der Tat eine Frage, die vermutlich viele beschäftigt und
die auch interessant ist. Ich möchte die jetzt einmal ein bisschen
einkreisen und zwar dahingehend, dass es mit Sicherheit Konsens bei
uns im Verband ist, dass es kein Recht sui generis für den
Patentschutz geben soll. Des Weiteren ist es auch Konsens, dass wir
das Patentrecht hier nicht radikal abschaffen wollen. Es wird keine
OpenSource-Bewegung aus dem Verband heraus geben; da stehen auch alle
im Unternehmen dahinter. Die komplementäre Wirkung von Urheberrecht
und Patentrecht wird auch von den Unternehmen, die bei uns vertreten
sind, durchaus als sinnvoll erachtet. Wir brauchen hier schon einen
Mittelweg, weil wir natürlich die Entwicklung in den USA haben und
nicht weil es europäische und US-amerikanische Unternehmen gibt,
sondern einfach, weil wir hier global wirken. Das heißt also, auch ein
kleines Unternehmen, das hier in Deutschland ansässig ist, muss sich
ja mit dem internationalen Markt und damit auch mit den USA
auseinandersetzen und hat deswegen auch ein Interesse, dass die
Diskrepanz nicht so groß ist. Von daher gibt es keine Abwendung von
einem kompletten Patentsystem. Von daher kann ich im Moment die ein
bisschen globale Aussage machen, dass der Status quo im Moment nicht
das Schlechteste ist. Wenn man sich allerdings dann mit solchen
Detailfragen, wie sie von Herrn Dr. Mayer angesprochen worden sind,
befasst - soll man den Quellcode offen legen, wie sieht es mit den
Schnittstellen aus - dann muss ich Ihnen ganz ehrlich sagen, da sind
wir noch am Anfang. Das sind Ideen, die aufkommen und die wir auch
dann hier einmal zwischen - ich nenne es jetzt einmal etwas platt -
groß und klein abwägen müssen, damit man hier einen Mittelweg findet,
an dem hier sicherlich alle Interesse haben.

#HbW: Herzlichen Dank, Frau Bremer. Herr Siepmann war auch von ganz vielen
halb angesprochen, aber von einem konkret, das war der Kollege
Neumann, aber die Antwort geht nicht verloren. Man kann alles
nachlesen, da, wie gesagt, aufgezeichnet wird. Bitte schön, Herr
Siepmann.

#Wte: Wenn ich das noch richtig in Erinnerung habe, meinte Herr Neumann,
dass sich viele Argumente grundsätzlich gegen Patentierung richten
würden. Ich glaube, zumindest von den hier anwesenden Sachverständigen
kann man sagen, dass hier niemand der Ansicht ist, dass sich seine
Argumente grundsätzlich gegen Patentierung richten. Ich möchte nur
ganz kurz zusammenfassen, was auch Herr Probst gesagt hat: Wir müssen
die einzelnen Branchen betrachten, wir müssen die spezifischen
Besonderheiten der einzelnen Branchen betrachten - das mache ich aber
jetzt hier nicht, weil es zu lange dauert, das muss man gründlich
machen. Er fragte auch, ob es einen Mittelweg gibt. Natürlich kann man
die schädlichen Auswirkungen des Patentrechts im Bereich von Software
durch viele Sachen abschneiden, das ist gar keine Frage. Man kann
z.B., weil die Innovationszyklen im Softwarebereich so kurz sind,
kürzere Laufzeiten nehmen. Damit wäre aber klar, dass das Ganze nicht
unter das TRIPS-Übereinkommen fällt, damit wäre nämlich klar, dass
Software keine Technik ist. Das war ja übrigens 1976 auch die Ansicht
des Bundesgerichtshofs und es gibt überhaupt keinen Grund, dazu jetzt
nicht wieder zurückzukehren. Im Zusammenhang damit meinte Herr Dr.
Schiuma, man will keine Ausnahme für Software machen, da hatte man vor
zwanzig Jahren noch eine völlig andere Betrachtungsweise. Da sagte
man, Technik ist patentierbar und Software, deren einzige Funktion
darin besteht, in einer Anlage zu laufen, die technisch ist, wird
dadurch nicht technisch. Also, eigentlich kein Problem.

#Zwb: Zu anderen Fragen zum Monopol. Herr Dr. Schiuma hat sicherlich Recht:
Wenn jemand eine Schnittstelle hat, die nur von fünf Leuten eingesetzt
wird und er Patente geltend macht, dann kommt er mit seiner
Schnittstelle nicht weit. Aber wenn jetzt beispielsweise ein Verfahren
zur Erstellung eines MS-Word-Dokumentes patentiert werden würde, dann
kann das sehr wohl sehr starke Auswirkungen haben. Es ist ja gerade
so, dass diejenigen, die Patente auf Schnittstellen haben, nicht so
blöd sein werden, sie dann einzusetzen, wenn sie sich ihrer Chancen
damit selbst berauben, sondern sie werden die Patente dann einsetzen,
wenn die Schnittstelle so verbreitet ist, dass praktisch keiner mehr
zurück kann.

#IjW: Ich muss jetzt einmal meine Liste durchgehen, es wurde noch zur
Internationalisierung gefragt. Die Grundfrage, die ich hier stellen
würde, ist die: Haben wir die Möglichkeit, eine andere
Wirtschaftspolitik zu machen als die USA? Wenn man sagt, nein, die
haben wir sowieso nicht, dann brauchen wir uns nicht weiter darüber zu
unterhalten, dann können wir alle nach Hause gehen. Wenn wir die
Möglichkeit haben, eine andere Wirtschaftspolitik oder ein anderes
Wettbewerbsrecht zu machen als in den USA, dann sollten wir die auch
nutzen. Es ist nichts Besonderes, zwar nicht unbedingt wünschenswert,
aber nichts Besonderes, dass es verschiedenes Recht in verschiedenen
Staaten gibt. Gerade wenn Sie sich das US-amerikanische Recht ansehen:
Wenn Sie etwas einführen gibt es so viele Bestimmungen in den
Vereinigten Staaten und auch in anderen Ländern, was Sie einführen
dürfen und was nicht, die Sie alle beachten müssen; Exportbestimmungen
müssen Sie in jedem Land beachten, da kommt es auch nicht mehr darauf
an.

#Zej: Zu den USA ist noch etwas sehr Wichtiges zu sagen: Es gibt bis jetzt
keine höchstrichterliche Entscheidung zu dieser Frage der
Patentierbarkeit von gedanklichen Konzepten - ich verwende gerne die
Bezeichnung %(q:Patentierbarkeit von gedanklichen Konzepten), weil es
meiner Ansicht nach darum geht und nicht nur um Software. Nehmen wir
einmal an, wir entscheiden uns hier in Europa für Softwarepatente und
der Supreme Court in den Vereinigten Staaten sagt, nein, nein, das
wollen wir doch nicht. Dann haben wir wieder eine inhaltliche
Situation und können anschließend wieder hier sitzen und uns
unterhalten, ob wir denn die Situation in der anderen Richtung an die
USA anpassen. Ich glaube, der beste Ansatzpunkt ist, eine vernünftige
europäische Lösung zu finden und nicht immer den USA hinterher zu
rennen. Im Übrigen ist es sehr gut möglich, dass, wenn man sich in
Europa gegen Softwarepatente oder gegen Patente auf gedankliche
Konzepte entscheidet, die Vereinigten Staaten in der Richtung auch
anschließend zurückgehen; dort gibt es ja auch Bewegungen dagegen,
sehr starke Bewegungen, z. B. auch gerade im Verbraucherbereich, die
sagen, das macht nur die Produkte teurer, sonst bringt es nichts. Es
ist also nicht gesagt, dass wir damit auf Dauer gesehen eine
uneinheitliche Position haben. Aber in Einem können wir uns sicher
sein: Wenn wir jetzt Softwarepatente rechtlich sanktionieren, d.h.
festlegen, dass das möglich ist, dann haben wir Eigentumspositionen
geschaffen, die dann in den nächsten zwanzig Jahren jedenfalls nicht
mehr zu beseitigen sind. Da sollten wir sehr vorsichtig sein.

#Izu: Ich meine, wenn wir jetzt sagen, die Rechtslage ist unklar und wir
stellen jetzt in Europa fest, Software oder gedankliche Konzepte sind
patentierbar, vielleicht mit der einen oder anderen Ausnahme, dann
haben diejenigen, die Patente erwerben, mit Sicherheit
Rechtspositionen, und zwar durch Art. 14 GG geschützte
Rechtspositionen. In der Position, in der wir uns jetzt befinden, in
der man Ende der siebziger Jahre der Ansicht war, dass Software nicht
patentierbar sei und dies nur der Gesetzgeber ändern könne - das hat
ganz konkret Herr Gert Kolle in einem %(gk:Aufsatz) gesagt - kann man
sich durchaus auf den Standpunkt stellen und sagen: Das waren
bedauerliche Fehlentscheidungen, die meisten dieser Softwarepatente
sind wegen mangelnder Erfindungshöhe, Neuheit und allen möglichen
Gründen sowieso nichtig und die paar anderen deswegen, weil es keine
technischen Leistungen sind. Da hat man keine sehr großen Probleme
damit.

#EeW: Ein Wort zur SAP. SAP ist jetzt in einer anderen Situation. Das muss
man sehen, sie waren früher ein aufstrebendes Unternehmen, und jetzt
wollen sie sich gegen kleine Unternehmen schützen, die auch aufstreben
wollen. Das ist jetzt der Punkt, weshalb sie anfangen, Patente zu
sammeln.

#Hrc2: Herzlichen Dank. Herr Schiuma hatte noch etwas vergessen.

#Drd: Die These von Prof. Lutterbeck war, dass das Spezifikum des
amerikanischen Marktes eine andere Situation ist als die in
Deutschland. Das ist ja eine wichtige Aussage. Sie sagen, nein, der
Unterschied liegt darin, dass SAP ein gesättigtes Unternehmen und
nicht mehr hungrig ist.

#Ecw: Es hat etwas mit der Unternehmensgröße zu tun. SAP kann es sich jetzt
leisten, Patentstreitigkeiten zu führen, das konnten sie in der
Anfangsphase nicht. Dass sie in den USA Patente sammeln oder sammeln
wollen, hängt damit zusammen, dass in einer Welt, in der es Patente
gibt, klar ist, dass ein gewisser Zwang zum Patentieren herrscht. Wer
nicht patentiert, geht das Risiko ein, dass er seine eigenen
Erfindungen später einmal nicht benutzen darf.

#Huc: Herr Neumann hatte ja auch noch die Frage nach dem Mittelweg gestellt.
Die hatte Herr Siepmann jetzt auch noch einmal kurz im Hinblick auf
die kurze Patentdauer aufgegriffen; dass eben in der Software die
Produktzyklen wesentlich kürzer sind und somit eine zwanzigjährige
Laufzeit nicht gerechtfertigt ist. Hier muss man sagen, grundsätzlich
ermöglicht ja das Patentrecht auch den Schutz der zugrunde liegenden
Idee. Als Patentanwalt würde ich sicher eine schlechte Arbeit machen,
wenn ich ausschließlich die konkrete Gestalt schützen würde, wie das
Produkt meines Mandanten jetzt konkret aussieht, sondern ich werde
versuchen, herauszufinden oder zu abstrahieren, was ist denn jetzt
wirklich der zugrunde liegende Gedanke. Dieser zugrunde liegende
Gedanke kann durchaus Produktzyklen überleben. Das heißt, eine gleiche
Idee kann durchaus in verschiedenen Produkten oder neuen Versionen
eines Produkts seinen Niederschlag finden. Das heißt also, dass der
Produktzyklus an sich mittlerweile immer kürzer wird, aber das
verzeichnet man nicht nur in der Softwareindustrie. Man braucht bloß
in die Automobilindustrie zu schauen, auch dort werden laufend neue
Modelle herausgebracht. Früher konnte ein Modell fünf bis zehn Jahre
laufen, heute ist das undenkbar. Das heißt also zum einen, Ideen, die
durch das Patentrecht geschützt werden oder geschützt werden können -
es geht natürlich um die geschickte Formulierung der Patente - können
die Produktzyklen überdauern. Zum anderen aber sind die Patentgebühren
mit der Patentdauer auch steigend. Das heißt also, je länger ich ein
Patent am Leben erhalten möchte, das Schutzrecht aufrechterhalten
möchte, desto mehr muss ich dafür zahlen. Die Unternehmen mit großen
und kleinen Patentportfolios überlegen sich ganz genau Jahr für Jahr,
brauche ich das Patent, möchte ich das Patent, soll ich es erneuern
oder nicht? Es ist nicht ein Patent, sobald man es angemeldet hat;
dann hat man es nicht zwanzig Jahre. Zuerst muss es geprüft werden,
dann erteilt werden, dann muss man, startend vom dritten Jahr, Jahr
für Jahr seine Patentgebühren bezahlen; bei relativ niedrigen Kosten
eben linear steigend. Das heißt also, wenn die Technologie überholt
ist, dann wird das Unternehmen natürlich auch einen Teufel tun, dafür
Geld herauszuschmeißen. Und zum anderen, man kann auch sagen, wenn die
Technologie tatsächlich überholt ist, dann kümmert das Patent auch
keinen mehr. Es sind also mehrere Aspekte, die da herein spielen und
man kann nicht sagen, dass Software jetzt spezifisch einer kürzeren
Dauer bedarf. Ich teile die Meinung von Herrn Siepmann, dass es
ohnehin im Hinblick auf das TRIPS-Abkommen etwas problematisch ist.
Zumindest haben Sie das Wort TRIPS-Abkommen in diesem Zusammenhang in
den Mund genommen, aber ich betrachte das als problematisch und sehe
dementsprechend gar kein Bedürfnis, für Software tatsächlich eine
kürzere Patentdauer vorzusehen.

#Hee2: Herzlichen Dank. Bei dieser Frage %(q:20 Jahre) waren wir ja auch
schon mitten in unserem Punkt II, Änderungsbedarf. Das ist jetzt kein
Problem, das war eine gute Überleitung. Wenn Sie erlauben, würde ich
an der Stelle selbst eine Frage stellen. Die BGH-Rechtsprechung ist
jetzt mehrmals angesprochen worden und auch der Wandel der
BGH-Rechtsprechung ist mehrmals angesprochen worden. Wir sind mitten
im Änderungsbedarf, das wollte ich damit andeuten. Das würde mich
jetzt von den beiden Letzten, die geredet haben, durchaus auch
interessieren: Herr Schiuma und Herr Siepmann, Sie sprachen ja auch
zum Teil sybillinisch davon - in den siebziger Jahren hatten wir eine
andere Entwicklung, dann hat der BGH sich weiterentwickelt, auch was
seine Positionen angeht. Sie, Herr Siepmann, haben, wenn ich das
richtig verstanden habe, für eine Rückkehr plädiert; das wäre wieder
eine Frage des Änderungsbedarfs, also diese Frage BGH, wenn der sich
so wirr entwickelt oder, nein, so weise, entwickelt, da ist natürlich
der Gesetzgeber im Zweifel gefragt. Und dann wären wir beim
Änderungsbedarf, d.h. also die Frage, ist diese Entwicklung Ihres
Erachtens nach sinnvoll oder muss der Gesetzgeber hier etwas
zurückdrehen, weil es möglicherweise eine Fehlentwicklung auch in der
Bewertung des Sachverhalts seitens des BGH war? Das wäre meine Frage
und der Kollege Otto hat zwar hierzu keine Frage, sondern eine Frage
nach der Abteilung 2. Gibt es hierzu im Moment weitere Fragen?

#DmW: Da Sie ja wünschen, dass man immer gleich adressiert, adressiere ich
jetzt mal, ich glaube, in erster Linie an Frau Bremer. Keiner der
Experten hier am Tisch - Sie mögen mir widersprechen, wenn ich jetzt
etwas Falsches sage - hat einer Ausweitung des Patentschutzes das Wort
geredet, beispielsweise auf Geschäftsideen u.ä., wie das in den USA
überlegt wird. Für Europa hat das keiner von Ihnen gefordert. Keiner
von Ihnen, wenn ich das richtig sehe, hat die völlige Freistellung der
Software vom gewerblichen Rechtsschutz gefordert, so dass wir den
Handlungsbedarf ein bisschen eingrenzen können, der von uns liegt.
Frau Bremer, Sie haben gesagt, eigentlich kann man doch mit dem Status
quo gut leben; das war am Ende so Ihre These. Meine Fragen an Sie: Ist
es nicht so - das knüpft an das an, was Herr Tauss eben gesagt hat -
haben wir nicht Probleme in der Praxis damit, dass die Auslegung und
Anwendung von Art. 52 EPÜ doch viele Probleme macht und dass keiner so
recht weiß in der Branche, was ist jetzt patentierbar und was nicht?
Haben wir nicht einen Klarstellungsbedarf bezüglich Art. 52 EPÜ, zumal
ich gelesen habe, dass die europäischen Patentämter wohl ohnedies auf
eine Änderung drängen? Also die Frage auch an Ihren Verband und Ihre
Mitgliedsunternehmen: Gibt es da einen Klarstellungsbedarf oder ist
der Status quo materiell zu sehen, also nicht in dem Sinne von einer
Präzisierung? Die zweite Frage - ich glaube ich richte sie am besten
an Prof. Lutterbeck - wiederum Handlungsbedarf in welche Richtung?
Keiner der Experten hat wiederum, wenn ich das richtig sehe, ein
eigenes Schutzrecht für Software gefordert. Mehrere, Prof. Lutterbeck,
Frau Bouillon, wenn ich das richtig gesehen habe, haben gesagt oder
zumindest geschrieben, sie wünschen Sondervorschriften für Software im
Patentgesetz. Interessant, im Patentgesetz sagen Sie beide, dann gehen
Sie schon von der prinzipiellen Patentierbarkeit von Software oder
einigen Bereichen aus. Meine Frage - ich richte sie jetzt einfach
einmal an Sie, Herr Prof. Lutterbeck - Sie haben die
Quellcodeprivilegierung als eine mögliche Sondervorschrift erwähnt.
Kennen Sie noch andere Sondervorschriften, damit wir einmal als
Gesetzgeber Ideen haben, um was es hier geht. Und wenn Sie die
Quellcodeprivilegierung ansprechen - das ist ein spannendes Thema hier
am Tisch - geht es eigentlich bei dem Thema, über das wir heute reden,
im Kern nicht um einen Schutz generell von Software, sondern im
Speziellen um Schutz von OpenSource? Ist das Thema im Kern, um das wir
uns streiten, das Freihalten der OpenSource-Bewegung und der
OpenSource-Umgebung oder ist, wenn Sie die Quellcodeprivilegierung
wünschen, Herr Prof. Lutterbeck, mehr damit gemeint? Warum wünschen
Sie gerade, dass bezüglich des Quellcodes eine Sondervorschrift in das
Patentgesetz hineinkommt?

#DWF: Die letzte Wortmeldung, die ich habe, ist von Frau Kollegin Griefahn.

#IaW: Ich habe aus den Papieren, die ich jetzt allerdings nur schnell
durchschauen konnte, gesehen, dass schon die Idee besteht, zumindest
von der Zeit her, drei bis fünf Jahre einzurichten, das hieße ja, dass
man überhaupt einen Handlungsbedarf sieht. Die Frage, die sich für
mich stellt - es wird ja gleichzeitig gesagt, keine
Bagatellpatentierung oder keine Patentierung von Bagatellfragen - die
Frage ist, wo fängt die Bagatelle an und wo hört sie auf? Die
Schwierigkeit, die in mehreren Vorträgen dargelegt wurde, dass jeder,
der einen Computer hat, im Prinzip Programmierungen machen kann und
Software entwickeln kann, geht ja dahin, dass sich aus jedem Anwender,
der sich hinsetzt, ein neues Geschäft entstehen kann. Und das neue
Geschäft kann komplexer oder weniger komplex sein, auf jeden Fall kann
er ein Geschäft damit machen, sonst hätten wir keine Start-ups, keine
Garagenfirmen, keine Schülerfirmen und was es alles gibt. Für mich
stellt sich da sozusagen die Frage der Abgrenzbarkeit und die Frage,
wie richtet man Limits ein, also, wie kann man so etwas prüfen?
Vielleicht könnte Herr Dr. Kiesewetter-Köbinger dazu noch etwas sagen
und Frau Bouillon, ich glaube, in Ihrem Beitrag habe ich das ein
bisschen gefunden, vielleicht können Sie zu den beiden Fragen etwas
sagen.

#Vin27: Vorsitzender

#Irn: Ich werte es großzügig als eine Frage an zwei Sachverständige, sonst
müsste ich jetzt wieder zurückweisen. Ich würde jetzt gerne bei Herrn
Siepmann beginnen.

#Dna4: Die Rechtsprechung 1976 war in dieser ganzen Frage recht klar. Man
sollte sich das Urteil des BGH, das unter dem Begriff Stichwort
%(dp:Dispositionsprogramm) auftaucht, durchlesen. Man kann es wirklich
nur empfehlen, genauso wie Herr Machlup empfohlen wurde, kann man sich
diese BGH-Entscheidung durchsehen. Wenn man wieder dahin zurückkehrt,
wären vielleicht bestenfalls drei Prozent der Patente betroffen, von
denen dann klargestellt wird, solche Patente wollen wir nicht, solche
Patente gibt es nicht, das sind allein Patente auf gedankliche
Konzepte. Das ist mit Gefahren verbunden für die Gesellschaft, wie
übrigens auch der BGH in diesem Urteil sehr ausführlich ausgeführt
hat, und da sehe ich auf europäischer und auch auf deutscher Ebene
keine Probleme, dahin zurückzukehren.

#Dir: Danke schön, die gleiche Frage war an Herrn Schiuma gerichtet.

#Ict: Ich kann eigentlich Ihrer Meinung nicht so richtig folgen, dass die
Rechtsprechungsentwicklung wirr sei, ...

#lWW: ... ich habe mich sofort korrigiert.

#IoW: Ich möchte vielleicht auch einmal ein paar, sagen wir einmal,
historische Facts geben, auch, um einmal einordnen zu können, wie die
Entwicklung, wie die Rechtsgrundlage war. Die Entscheidung, die Herr
Siepmann genannt hat, Dispositionsprogramm von 1976, ergeht im Grunde
genommen auf der gleichen Rechtsgrundlage wie die spätere
BGH-Entscheidung. Das heißt also, vom Gesetzeswortlaut her hat sich
nichts geändert. Dieser Gesetzeswortlaut, dass also in § 1 Abs. 2
Ziffer c Programme für Datenverarbeitungsanlagen als Nichterfindung
angesehen würden, geht zurück auf eine Bestimmung, die im Europäischen
Patentübereinkommen enthalten ist; dort wurde sie eigentlich im
Wesentlichen deshalb aufgenommen, weil die französische Delegation
damals bei der diplomatischen Konferenz extrem starken Druck ausgeübt
hat. 1968 ist diese Bestimmung in dem französischen Patentgesetz
genauso aufgenommen worden, also vor dem EPÜ, und damals hatte man das
begründet, ja, zum einen damit, das Patentamt könne deshalb nicht so
richtig prüfen, weil man dann Stapel von Source-Codes eingeliefert
bekommen würde, da hatte sich das Patentamt sehr stark gegen gewehrt.
Zum anderen hat man gesagt, na ja, im Grunde genommen besteht auch
kein Bedürfnis dafür, weil, und das muss man sich einfach auch vor
Augen halten, in den 60er Jahren die Art der Software eine ganz andere
war. Heute gehen wir hauptsächlich auf Applikationssoftware, also,
wenn wir hier von Software sprechen, ist hauptsächlich immer
Applikationssoftware gemeint. Hingegen waren es damals wirklich nur
Software-Operating-Systems, um die Hardware zum Laufen zu bringen. Und
da hat man gesagt, na ja, die Hardware an sich bekommst du ja
geschützt, auch die Art und Weise, wie du sie betreibst, und
dementsprechend besteht kein Bedürfnis für Software. Genau diese
Argumente wurden dann auch während der Diskussion im Vorfeld der
diplomatischen Konferenz, auf der dann das EPÜ verabschiedet wurde,
vorgetragen und dort war Frankreich sehr vehement dafür eingetreten,
diese Ausnahmebestimmung einzufügen. Da hat man einfach einen, sagen
wir mal, diplomatischen Kompromiss dahingehend gefunden und hat
gesagt, also, wenn Ihr es unbedingt wollt, bekommt Ihr es, und im
Grunde genommen sind ja die Auswirkungen gar nicht so stark. Und daher
ist es dann ins EPÜ gekommen. Und weil natürlich die nationalen
Gesetzgeber keine andere Gesetzeslage für ein europäisches Patent, das
meinetwegen mit Wirkung für die Bundesrepublik Deutschland erteilt
wird, schaffen wollten als für ein nationales deutsches Patent, das
ich beim Deutschen Patentamt anmelde, hat man gesagt, o.k., wir
übernehmen das eins zu eins in unser Patentgesetz. So ist es
historisch dazu gekommen.

#Dsf: Die Rechtsprechung, die mehrmals angesprochen wurde, vielleicht
sollten wir kurz betonen, was darin gesagt wurde. Im
%(dp:Dispositionsprogramm) war der Tenor, dass gesagt wurde: Nach
Laden des Programms oder Ablaufenlassen des Programms auf dem Computer
verändert sich der Computer an sich nicht. Dementsprechend ist das
eben ein Computerprogramm als solches, somit von dem
Ausschusstatbestand des § 1 Abs. 2 und Abs. 3 erfasst und somit nicht
patentfähig. Diese Fassung, obwohl das Herr Siepmann nicht gerne
wahrhaben möchte, ist aber im letzten Jahr durch den BGH eindeutig
revidiert worden. Ich meine, Sie wollen natürlich immer gerne die alte
Situation wieder herbeirufen.

#Dih: Das war das, wo ich versehentlich %(q:wirr) sagte.

#Wah: Wenn man jetzt einfach mal schaut, was ist Status quo der
Rechtsprechung, kann man sie nicht als wirr bezeichnen, denn sie ist
ganz eindeutig. Sie sagt eben, Software kann durchaus patentierbar
sein, es gibt besondere Kriterien, und es ist jetzt vielleicht eher
die Frage: welche Kriterien; sind die Kriterien sinnvoll oder nicht?
Das müsste man vielleicht mit Leben erfüllen. Aber zu sagen, wir
müssen jetzt gesetzgeberisch tätig werden, nur weil eine wirre
Situation da ist, das stimmt nicht, weil die Rechtslage nach der
Rechtsprechung durchaus klar ist. Es gibt, wie bei jeder
Rechtsprechung, einzelne Detailfragen, die wiederum noch einmal
geklärt werden müssen, die der Auslegung bedürfen. Da ist sicherlich
auch eine gewisse Harmonisierungstätigkeit auf europäischer Ebene, die
Deutschland als Mitgliedstaat durchaus unterstützen sollte, gefragt.
Andersherum gibt es aber schon internationale Abkommen - ich beziehe
mich jetzt auf das %(tr:TRIPs)-Abkommen -, die durchaus auch einen
gesetzgeberischen Handlungsbedarf benötigen oder implizieren. Dort
wird natürlich diskutiert, inwieweit das tatsächlich der Fall ist. Ich
vertrete die Meinung, dass ein Handlungsbedarf besteht, andere
vertreten dort die Meinung, dass dies nicht der Fall sei, weil die
Bestimmung nicht hinreichend konkret sei. Es besteht aber auf alle
Fälle im Hinblick auf die Rechtsklarheit ein gewisser Handlungsbedarf.

#Ate: Also, Herr Dr. Kiesewetter-Köbinger erzählte, ganz zum Anfang, als er
als Patentprüfer angefangen hatte, hat er einfach ins Gesetz
hineingeschaut. Da steht drin, Programme für Datenverarbeitungsanlagen
als solche sind ausgeschlossen. Damals hätte er eigentlich am liebsten
die Akte zuschlagen wollen, und dann eine Abweisung oder eine
Zurückweisung der Anmeldung erlassen. Das zeugt eigentlich im Grunde
genommen davon, dass es für den Techniker, für das nicht rechtskundige
Unternehmen schwer verstädlich ist, dass im Gesetz eine
Ausnahmebestimmung vorgesehen ist, diese aber de facto durch die
neueste Rechtsprechung eigentlich ausgehebelt ist, weil man da
durchaus differenzierend herantritt. Aber für diese Differenzierung
bedarf es dieser Ausnahmebestimmung nicht, denn es geht immer nur auf
die Frage der Technologie, der Technizität, und diese kann man
durchaus auch ohne diese Ausnahmebestimmung rechtfertigen und sie wird
auch derzeit gerechtfertigt. Die Entscheidung, die aus den 60er Jahren
stammt und in der sozusagen zum ersten Mal die Legaldefinition gegeben
wurde, bezog sich nicht auf ein Computerprogramm, sondern sie bezog
sich auf ein Verfahren zum Züchten von Tauben, um am Ende rote Tauben
herauszubekommen. Da wurde dann diese Definition aufgenommen. Diese
ist wiederum ein bisschen aufgeweicht worden, weil man gesehen hat:
die ist vielleicht ein bisschen zu starr und adaptiert sich nicht an
die neuen Entwicklungen. Das ist nur, um den Hinweis zu geben, es
kommt gar nicht so sehr darauf an, ob diese Ausnahme in dem Gesetz
drin steht oder nicht.

#MrW: Man kann eine sinnvolle Rechtslage, wie sie meiner Auffassung nach
durch die letzten BGH-Entscheidungen gegeben ist, durchaus auch ohne
diese Ausnahmebestimmungen herbeiführen. Da stellt sich für mich die
Frage, wieso sollen wir dann nicht denjenigen entgegenkommen, die
nicht die fundierten und tiefgreifenden Patentkenntnisse haben, und
tatsächlich diese Ausnahmebestimmung herausstreichen, zudem sie
wiederum die Entscheidung dahingehend beeinflusst, dass man sich immer
haarscharf in dem Bereich der Frage befindet: Ist es noch
TRIPS-konform, ja oder nein? Dann müssen immer wieder Statements
gefällt werden, um das wiederum abzugrenzen, um dann eine Konformität
mit internationalen Abkommen zu erreichen. Natürlich muss der BGH
versuchen, das deutsche Recht konform im internationalen Recht
auszulegen und dementsprechend kommen immer wieder Aussagen zustande,
die dem dann Rechnung tragen sollen. Daher befürworte ich eigentlich
eine ersatzlose Streichung von Computerprogrammen, also Programmen für
Datenverarbeitungsanlagen aus � 1 Abs. 2 c sowie auch aus dem
entsprechenden Art. 52 Abs. 2 des Europäischen Patentübereinkommens.

#HEl: Herzlichen Dank. Die Opposition hat es gehört, rote Tauben; nicht
schwarze, nicht gelbe. Wir kommen zu Herrn Prof. Lutterbeck.

#PWl3: Prof. Dr. Bernd Lutterbeck, Technische Universität Berlin (Fachbereich
Informatik)

#Iel: Ich habe zwei Hüte auf: ich bin ja gelernter Jurist, aber ich arbeite
seit vielen Jahrzehnten als Wirtschaftsinformatiker. Das führt auch
dazu, dass ich differenzierend auf Ihre Frage antworte. Ich habe in
meiner Stellungnahme geschrieben, ein Schutzrecht sui generis hat
Charme und die Logik auf seiner Seite. Das ist im Übrigen auch die
Auffassung der amerikanischen Literatur, alle wesentlichen Leute sind
dieser Auffassung. Aber die Frage ist natürlich, was damit gewonnen
ist. Ich könnte mich jetzt zehn Jahre zurückziehen, falls ich Geld
dafür bekäme, und irgendein Konzept machen, das vielleicht irgendwen
interessiert, inzwischen geht ja die Welt weiter. Das führt mich zu
meinem zweiten Hut und Ihrer Frage, warum eigentlich das
Quelltextprivileg? Vor ungefähr zwei Jahren haben meine Mitarbeiter
und ich begonnen, uns für diese Diskussion mehr zu interessieren. Uns
ist da etwas aufgefallen. Da gibt es einerseits die Juristen, wir
haben das gerade hier gehört: Traditionalisten. Das Gegenteil streitet
hin und her, meist auf einer begrifflichen Ebene - diese Diskussion
hat mich nicht überzeugt. Gleichzeitig gibt es eine eher politische
Diskussion, die auch sehr hektisch geführt wird und ich fand, es muss
erforderlich sein, etwas zu liefern, was praktisch möglicherweise
einen Ausweg liefert.

#Wsi: Wir sind gestartet als Informatiker. Als Informatiker hat uns zuerst
das Problem der IT-Sicherheit, das hier überhaupt noch nicht gefallen
ist, interessiert. Es ist nach gesicherter informatischer Kenntnis so,
dass Programme, dass die Sicherheit von Programmen, nur dann
überprüfbar ist, wenn der Quelltext offen ist. Das bedeutet z.B.
bezogen auf proprietäre Produkte wie Microsoft, sie können sicher
sein, Sie können es aber nicht überprüfen. Das könnten Sie nur bei
Offenheit des Quellcodes. Wir haben versucht, sozusagen beide
Variationen, Patentrecht und dieses IT-Sicherheitsargument, zusammen
zu matchen und sind dabei darauf gekommen, dass es nützlich sein
könnte, so ein Privileg zu formulieren. Eine Formulierung finden Sie
in der schriftlichen Stellungnahme. Da kann man immer noch drüber
streiten. Ich finde aber, das bringt überhaupt nichts, sondern man
muss praktisch mal einen Weg vorangehen. Vorausgesetzt wäre aber, dass
man rechtlich das Wettbewerbsrecht stärker für diese Geschichten in
Anspruch nimmt. Das passiert nach meiner Meinung noch nicht
ausreichend genug. Stichwort: OpenSource. Ich würde nicht sagen,
dieses ist dem anderen überlegen, sondern man muss nur eine
Wettbewerbssituation schaffen, die es dieser Form von
Softwareerstellung und -vertrieb möglich macht, sich auf dem Markt zu
halten. Dann wird der Markt entscheiden, welche Lösung besser ist. Wir
glauben aber, dass man dafür auch eine kleine Veränderung im
Patentrecht vornehmen muss und das ist das von uns erwähnte
Quelltextprivileg.

#Ecn: Es gibt noch andere, das würde jetzt hier zu weit führen, natürlich
kann man an den Verjährungsfristen drehen. Im Prozess DOJ gegen
Microsoft hat dieses Problem eine bestimmte Rolle gespielt. Dort haben
ja Kollegen von Harvard begutachtet und sie haben ähnlich argumentiert
wie ich. Es nützt nichts, zu versuchen, die ganze Welt umzuschmeißen,
wir suchen einmal nach einem praktischen Problem und verkürzen die
Verjährungsfrist meinetwegen dramatisch auf zwei Jahre. Dann hätte
etwa so ein Unternehmen wie Microsoft zwei Jahre lang seine
Startvorteile und nach zwei Jahren müsste alles offen sein. Also, wenn
man dieses Thema anrührt, bekommt man gleich einen Schwanz von
Problemen mit, das wäre ein zweites Problem. Man könnte sozusagen am
Register arbeiten und das entspricht auch einer Tendenz in der
amerikanischen Literatur. Auch denen ist aufgefallen, dass bestimmte
Diskussionen einfach nutzlos sind. Also ich sage einmal, diese
juristische Diskussion hier halte ich für nutzlos. Wichtig ist,
praktische Gesichtspunkte zu finden, die es allen Beteiligten möglich
machen, weiter zu kommen und wie gesagt, das könnten so zwei Stück
sein.

#Vin32: Vorsitzender

#Hkv: Herr Prof. Lutterbeck, wir streiten hier vorn am Podium über den
Begriff %(q:Verjährungsfrist). Könnten Sie den nochmals ganz kurz
darlegen, ob Sie jetzt wirklich meinten, die Laufdauer von Patenten
auf zwei Jahre zu beschränken. Frau Bremer wäre die Nächste. Übrigens,
was IT-Sicherheit angeht, da kommen wir noch einmal unter dem Thema
Auswirkungen sicherlich dazu. Frau Bremer.

#Dna5: Dr. Kathrin Bremer, Bundesverband Informationswirtschaft,
Telekommunikation und Neue Medien e.V. (BITKOM)

#Ugh: Um Missverständnisse zu vermeiden, gehe ich gerne noch einmal auf
Ihren Punkt ein, Herr Otto. Was ist Status quo? Und zwar ist Status
quo jetzt hier nicht materiell zu verstehen, sondern einfach faktisch,
d.h., wie wird es im Moment gehandhabt? Wie sieht es die
Rechtsprechung, wie handhabt sie es? Und da hat sie ja sehr eindeutig
gezeigt, dass sie ja eben grundsätzlich die Möglichkeit eines
Patentschutzes bei computer-implementierten Empfindungen sieht und das
ist auch die Position, wie sie von uns jetzt hier mit dem Status quo
in Verbindung gebracht wird. Da sehen wir in der Tat den
Klarstellungsbedarf, den ich eingangs erwähnt hatte, dass man hier
das, was Herr Schiuma sehr ausführlich dargelegt hat, nämlich diese
Rechtsunsicherheiten, vermeidet, diese Vorschrift im Sinne der
Rechtsprechung interpretiert und ändert - und das heißt letztendlich
auch %(st:TRIPS), wo alle Erfindungen, die mit Technik in Verbindung
zu bringen sind, patentierbar sein können - und damit diese Vorschrift
heraus nimmt.

#Vin33: Vorsitzender

#Drl: Danke schön. Wir kommen zur letzten Frage der Kollegin Griefahn,
gerichtet an Frau Bouillon und Herrn Dr. Kiesewetter, in der
Reihenfolge würde ich jetzt auch das Wort geben. Frau Bouillon, bitte
schön.

#IWw: In der Frage ging es um meine schriftliche Stellungnahme. Ich möchte
dazu noch einmal ganz konkret sagen, dass ich in dieser schriftlichen
Stellungnahme im Wesentlichen natürlich auf diese Fragen eingegangen
bin, die jetzt im Vorfeld gestellt wurden. Aus diesem Grunde habe ich
auch versucht, in meiner Vorstellung auch noch einmal darauf
aufmerksam zu machen, dass wir jetzt die Position haben, grundsätzlich
Softwarepatente nicht für sinnvoll zu erachten. Das mag vielleicht
auch daran liegen, dass ich aus der Wirtschaft komme und nicht aus der
Politik; man versucht natürlich, sich irgendwo kompromissbereit zu
zeigen.

#Arg: Aus diesem Grund sehen wir uns natürlich auch in der Position, einfach
einmal die Frage zu stellen: was wäre, wenn? Wie sollte ein
Patentschutz aussehen, wenn man sagt, gut, aus bestimmten Gründen sei
es schon sinnvoll, dieses zu machen. Und da war einmal die Frage nach
der Laufzeit. Wir sind dann zu dem Schluss gekommen, drei bis fünf
Jahre erscheint uns sinnvoll, und da gehe ich auch noch einmal kurz
auf dieses Trivialpatent ein. Was heute kein Trivialpatent ist, das
ist in fünf Jahren eventuell ein Trivialpatent, eben aufgrund dieses
sehr schnellen Zyklus, den die ganze Industrie durchmacht. Also ein
C++-Compiler war vor kurzem noch eine tolle Sache. Überhaupt die ganze
Erfindung, dieser Bereich der Objektorientierung, um nur einmal ein
paar Wörter, ohne sie jetzt unbedingt erklären zu müssen, in den Raum
zu werfen. Heute ist das einfach Standard. Aus dem Grund ist auch ganz
kritisch, die Laufzeit zu überprüfen, die ermöglicht wird, auch wenn
es dann sicherlich teurer wird, und die Frage, welche Auswirkungen hat
das?

#Des: Die zweite Sache ist es zu überlegen, das Wettbewerbsrecht, was ja
eben auch noch einmal von Prof. Lutterbeck angesprochen wurde, stärker
mit einzubinden und die Frage zu stellen, welche Auswirkung die
Vergabe des einzelnen Patentes im Rechtsstreit auch auf die gesamte
Entwicklung hat. Die Frage nach den Standards, die dann plötzlich
verhindert werden können, vorhin ist das auch noch einmal dargelegt
worden, wie das vonstatten geht. Man sagt nicht, das ist mein Patent,
sondern man wartet, bis sich etwas durchsetzt und dann sagt man, aber
jetzt habe ich das Patent. Wer kann da noch mitgehen, wer kann dann
zahlen, wer bleibt einfach außen vor und sagt, ich mache jetzt etwas
anderes oder ich mache dicht? Ich glaube, es ist auch noch einmal ganz
wichtig darauf hinzuweisen, dass sich Software in der Regel etwas
anders weiterentwickelt als der Maschinenbau. Ich selber bin
Diplomingenieurin für Elektrotechnik, vielleicht kann ich deswegen
ganz gut Vergleiche ziehen. Da bauen Neuerungen viel stärker
aufeinander auf und neue Sachen, die eben Patente sind, werden einfach
in die nächste Innovation mit eingeschlossen. Darin liegt eigentlich
auch die große Gefahr, die wir für die Auswirkung auf die
Innovativkraft sehen.

#Vti: Vielen Dank. Herr Dr. Kiesewetter-Köbinger als Letzter auf die Frage
der Kollegin Griefahn.

#Dbt3: Dr. Swen Kiesewetter-Köbinger, Deutsches Patent- und Markenamt (DPMA)

#FcA: Frau Griefahn, so, wie ich es verstanden habe, war Ihre Frage darauf
gerichtet, wie man diese Trivialpatente im Patentamt verhindern kann.
Das größte Problem, das im Patentamt für die Prüfung der Software ist,
ist, dass bislang Software nicht speziell gesammelt wurde, und zwar
außerhalb dessen, was angemeldet wurde. Es war ja nicht im
Patentschutz so zugänglich, es wurden nur die wesentlichen Sachen, die
man noch dazu braucht, gesammelt. Dieser Stand der Technik ist also
eher vernachlässigt worden. Generell ist es so, dass die Prüfer
hauptsächlich anhand von Patent- und Offenlegungsschriften die
Patentanmeldungen prüfen. Das ist der allererste Weg, da findet man
auch meistens etwas. Wenn man dort nichts findet, geht man in die
Nicht-Patentliteratur, dieser Weg ist erfahrungsgemäß sehr
zeitaufwändig, sehr schwierig und bei Software uferlos. Nachdem das
nicht im Haus ist, ist das fast nicht zu machen, wenn die Anmeldungen
noch nicht offengelegt sind. Eine Internetrecherche für nicht
offengelegte Anmeldungen verbietet sich, denn da kann praktisch jeder
mitlauschen, der im Internet das nötige Wissen hat, was habe ich da
jetzt abgefragt von diesen Suchmaschinen, was könnte da dahinter sein?
Das heißt, nur das, was über Datenbankzugriffe, die einen gesicherten
Zugang haben, abfragbar ist, läßt sich recherchieren und im Internet
erst, wenn die Offenlegung erfolgt ist.

#IWe: Ich habe im letzten Jahr ziemlich heftig versucht, diese
Internetrecherche und Datenbankrecherche von Nicht-Patentliteratur mit
einzubinden. Der Endeffekt war, ich habe plötzlich nur mehr die halbe
Erledigungszahl. Es geht irgendwie ins Geld, weil ich deswegen
wahrscheinlich später befördert werde. Aber es zeigt, dass es
ungeheuer aufwändig ist, in einer Nicht-Patentliteratur, speziell bei
Software und auch Elektronikanmeldungen, zu recherchieren. Es ist
einfach viel zu viel da. Selbst im Vergleich mit den vielen
Patentanmeldungen und Offenlegungsschriften - es ist im Internet und
in den Datenbank-Hosts noch wesentlich mehr als bislang im Patent
sichtbar war. In Amerika hat man für diese Nicht-Patentliteratur ein
Institut gegründet, die heißt Software Patent Institute, das diese
Nicht-Patentliteratur sammeln soll. Ob die Qualität verbessert wurde,
weiß ich nicht, aber es gibt sehr viele kritische Stimmen, dass dieser
gigantische Aufwand immer noch viel, viel zu klein ist. Die Amerikaner
haben zudem ein anderes Instrument, Stand der Technik vom Anmelder zu
fordern. Den Stand der Technik, den man selber verwendet hat, den muss
man in Amerika angeben. Kommt man bei einem erteilten Patent später
dahinter, dass er verschwiegen wurde, ist das Patent nichtig. Das ist
eine wunderbare Möglichkeit, wirklich zu sagen: Ja, du bist davon
ausgegangen, jetzt habe ich einmal einen Anhaltspunkt, wo ich denn
überhaupt starten muss und suche noch dazu und komme dann wesentlich
schneller zu besseren Suchergebnissen.

#Hrr: Herzlichen Dank. Wir kommen zu III. Auswirkungen, wenngleich dies ja
auch, was ökonomische Kompetenten sowohl in betriebswirtschaftlicher
als auch volkswirtschaftlicher Bereich angeht, immer mal wieder
angesprochen worden ist - die Beförderung übrigens wünschen wir Ihnen
dennoch, völlig ungeachtet von diesen angesprochenen Zahlen, dies ist
ganz selbstverständlich. Ich hätte jetzt in der Tat selbst eine Frage
zu dem Thema IT-Sicherheit und darf einmal ganz kurz ausnahmsweise die
Bundesregierung zitieren in ihrer Antwort auf eine Anfrage, die
kürzlich gestellt wurde - ich glaube, Kollege Dr. Mayer, Sie waren
auch dabei -, wo es u.a. die Frage IT-Sicherheit in diesem
Zusammenhang gab. Das Zitat lautet: %(q:Ob und in welchem Umfang) -
also wie gesagt, sagt die Bundesregierung - %(q:Patentschutz auf
Softwareerfindungen Auswirkungen auf die Sicherheit von
Computerprogrammen haben kann, bedarf einer gründlichen und breiten
Diskussion, wie sie von der Bundesregierung auch auf
Gemeinschaftsebene angestrebt wird.) Zu dieser gründlichen Diskussion
würde ich Sie, Herr Henckel und Herr Lutterbeck, befragen wollen. Wie
beurteilen Sie das, ist diese Diskussion notwendig oder sehen Sie
schon ganz konkrete Auswirkungen negativer oder positiver Art auf
dieses Thema IT-Sicherheit? Gibt es weitere Fragen im Moment? Ich habe
keine Wortmeldungen, dann gehen wir zur Antwort und dann können wir ja
noch einmal nachhaken. Ich glaube, wir liegen mit 17.30 Uhr sehr gut
in der Zeit. Herr Prof. Lutterbeck, bitte schön.

#PWl4: Prof. Dr. Bernd Lutterbeck, Technische Universität Berlin (Fachbereich
Informatik)

#Ars: Also die Bundesregierung tut ja ein bisschen was, z.B. hat sie mir ein
Gutachten vergeben. Das war ja das allererste Mal, dass überhaupt eine
Gruppe beauftragt wurde, das Thema IT-Sicherheit aus ihrem fachlichen
Kontext in einen anderen hineinzutun. Natürlich kommt man mit so einem
sehr kurz geschossenen Gutachten nur zu bestimmten Punkten, also, da
muss man sagen, da haben wir auch nicht mehr als gute Thesen finden
können. Ich glaube aber, nach allem, was passiert ist, z.B. mit den
Mail-Geschichten bei Microsoft - man sagt inzwischen, das sei ein
Schaden von zwei Milliarden Euro gewesen -, weiß man sozusagen, dass
das Thema IT-Sicherheit deshalb, aber auch aus anderen Gründen der
inneren und äußeren Sicherheit, von zentraler Bedeutung ist. Was ich
vorher schon gesagt habe, es ist so, dass Informatiker meinen, dass
mit dem gegenwärtigen Zustand Sicherheit nicht erreichbar sei. Nun
müsste man weiter gehen. Das ist aber bisher nicht passiert. Die
Leute, die OpenSource entwickeln oder sich dem Bereich zuordnen, die
sind zu einem bestimmten Teil der Auffassung, dass sie deshalb
OpenSource entwickeln - das weiß ich von meiner Uni -, weil das recht
stark ist. Berlin hat zwar kein Geld, aber gute Leute und die arbeiten
eben so. Darüber hinaus gibt es bisher in der Wissenschaft kaum
Literatur, ich würde sagen, unsere Stellungnahme ist eine der wenigen.
Auch international sehe ich nicht, dass man versucht hat, diese beiden
Bereiche zu matchen. Wir hoffen ja, dass es uns gelungen ist, eine
Diskussion anzustoßen. Man muss auch sagen, dass im
Wirtschaftsministerium das Thema ja auch aufgehoben ist und dass die
offiziellen Stellungnahmen, also zusammen mit dem BSI und dem
Bundesinnenminister, so sind, dass sie aus den erwähnten Gründen eher
OpenSource favorisieren. Insofern ist das also politisch eine sehr
relevante Stellungnahme, aber sie ist sozusagen noch nicht weit genug
vorgedrungen in die Praxis.

#Han: Herzlichen Dank, Herr Henckel, bitte.

#IWf: Ich kann mich dem eigentlich nur anschließen, dass IT-Sicherheit
letztendlich nur überprüfbar ist, wenn man die Quellen von Programmen
entsprechend offenlegt. Dann hat man die Chance, wirklich zu sehen,
was ist die Funktionalität in diesem Programm, sind da nicht
irgendwelche Trojanischen Pferde drin und andere Möglichkeiten, um
Angriffe von außen zu starten? Genau diese Möglichkeit hat man
natürlich nicht, wenn man sich proprietäre Programme ins Haus holt und
die benutzt. Dann ist man letztendlich nur in der Lage, dem Hersteller
zu vertrauen, dass er so sorgfältig mit seinen Programmen umgeht, dass
das nicht möglich ist. Aber gleichzeitig spielen da staatliche
Interessen mit hinein - ich erinnere da an die Internetschutzwälle der
Amerikaner, die im Augenblick geplant werden, wo dreißig Milliarden
Dollar investiert werden sollen, die nicht nur dafür da sein sollen,
äußere Angriffe abzuwehren, sondern auch diejenigen Staaten, von denen
aus diese Angriffe gestartet werden, entsprechend wiederum selber zu
infiltrieren und selber aktiv anzugreifen. Und das kann man
letztendlich einerseits natürlich dadurch tun, dass man Bomben auf
diese Länder wirft, man kann es aber auch natürlich sehr viel subtiler
dadurch tun, indem man entsprechende versteckte Trojanische Pferde
oder Schnittstellen aktiviert, die man letztendlich normalerweise,
wenn man den Source-Code verschließt, eben nicht nachweisen kann. Und
dann macht man sich letztendlich auch politisch verantwortlich, wenn
man diesen Weg geht und das bagatellisiert.

#Wee: Wenn Sie eine kleine Nachfrage erlauben, Kollege Dr. Mayer. Ich
glaube, diese Bedrohung, also Cyber-War und all diese Dinge, sind
völlig klar. Mir hat sich aber der Zusammenhang jetzt mit der Frage
Patentierung oder Nichtpatentierung noch nicht ganz erschlossen -
nehmen Sie das einfach aus der banalen Tatsache, dass ich nicht
Informatiker bin. Denken Sie, dass, je nach dem, welchen Schritt wir
im gesetzgeberischen Rahmen unternehmen, diese Entwicklung oder diese
Bedrohung durch das eine oder andere stärker gefördert oder abgewendet
würde, also auch was %(q:Bedrohungen) in den USA oder Abhängigkeit,
sagen wir es jetzt mal positiv, betrifft? Ich nehme jetzt nicht an,
dass die USA uns wie den Irak betrachten oder so irgendetwas. Ich
würde Sie aber bitten, von solchen potentiellen Nachteilen, die man
haben kann, möglicherweise auch zu Zeiten von Wirtschaftskriegen oder
wie auch immer, den Zusammenhang zu unserem Thema noch einmal kurz
herzustellen. Denn den allgemeinen IT-Zusammenhang habe ich in der
Vergangenheit, glaube ich, begriffen; nur zum heutigen Thema nochmals
vielleicht ganz kurz, das wäre vielleicht für die Auswertung auch
interessant. Die Frage war noch einmal an Sie gerichtet, Herr Henckel,
oder wollen Sie einen Moment darüber nachdenken?

#Ica: Ich glaube, dass eigentlich kein direkter Zusammenhang dazwischen
besteht, sondern letztendlich nur, dass Patente natürlich genau die
Offenlegung von Source, nämlich diese OpenSource-Szene, behindern bzw.
sogar kaputtmachen könnten. Und dann würde man genau diese Möglichkeit
der Alternative, eben auf OpenSource zu gehen, dadurch kaputtmachen
und letztendlich dann abhängig sein von kommerziellen Herstellern, die
einem nur proprietäre geschlossene oder verschlossene Quellen
verkaufen.

#Del: Danke, jetzt habe ich es begriffen. Herr Kollege Dr. Mayer, bitte.

#Aej: Also, wenn ich insgesamt beobachte, wer nun gegen eine Ausweitung der
Patente ist, so sehe ich jetzt einmal zwei Gruppen. Das eine ist die
OpenSource-Bewegung und deshalb geht die Frage noch einmal an Herrn
Siepmann, und das andere sind die kleinen Unternehmen oder sprich, die
freien Softwareentwickler. Und da habe ich einen Artikel von Herrn
Kiesewetter-Köbinger, und deshalb geht an Sie beide die Frage, einmal
mehr aus der Seite OpenSource und zum anderen mehr aus der Seite freie
Softwareentwickler. Es gibt ja, habe ich mir sagen lassen, auch noch
ein paar Leute, die das alleine oder zu zweit oder zu dritt in ganz
kleinen Firmen machen: Wird eine Ausweitung der Patente die OpenSource
und die kleinen Softwareentwickler behindern oder schädigen? Das ist
meine Frage.

#WnH: Wollen Sie beginnen, Herr Siepmann? Ich schaue mal nach links und mal
nach rechts, dann Herr Dr. Kiesewetter-Köbinger, bitte. Herr Siepmann
schreibt nämlich noch, das war mein Gedankengang.

#Gse: Gerade bei den ganz kleinen Unternehmen, bei den Start-ups oder bei
denen, die einfach nur als freischaffende Programmierer ihr Geld
verdienen, ist natürlich schon die Sorge, dass, wenn ein
Patentrechtsdruck da ist, also wenn es heißt, das und das darfst du
nicht programmieren, von ihnen auch verlangt wird nachzuweisen, dass
das nicht so ist. Das heißt, ihre Arbeitsgrundlage ist ziemlich
erschwert. Das, was sie bislang als Einzelpersonen nicht gemacht
haben, nämlich Recherchen zum Patentrecht, müssten sie daraufhin
machen. Es ist auch so: viele Studenten machen das nebenbei, sie gehen
zu Firmen, programmieren für diese Firmen kleine Aufgaben und
verdienen sich damit ihren Lebensunterhalt als Student. Sie wachsen
sozusagen zweigleisig in diese Branche hinein und werden später
vielleicht dann Start-ups. Es mag sein, dass bei praktisch keinem
wirklich einer auftritt und sagt, du hast mein Patent verletzt, aber
allein die Drohung, dass es sein könnte, wird einige davon abhalten,
diesen Weg zu gehen, nehme ich an.

#Vlm: Vielen Dank. Herr Siepmann, bitte.

#Zbe: Zur IT-Sicherheit, darf ich dazu ganz kurz etwas sagen? Es ist ja
nicht nur so, dass es um Fehler in der Software geht, es ist auch so,
das konnten Sie in der c`t vor einigen Monaten nachlesen, dass
teilweise ganz bewusst back doors geschaffen werden, also
Möglichkeiten für den Softwarehersteller, in Software hinein zu
kommen, auch in Datenbanksoftware. Ich glaube, da sind sich auch alle
einig. Es gab einen Artikel in der c`t darüber, dass sich jemand in
die Software, die geschrieben wurde, Datenbanksoftware, einen eigenen
Account hineingebaut hat, in den er von außen über das Netzwerk
jederzeit beliebig hinein konnte. Ich habe den Artikel auch noch da
und kann Ihnen den auch gerne zukommen lassen. Ich weiß jetzt aber
nicht, welcher Name das ist, und ich möchte hier nicht öffentlich
irgendein Unternehmen diskriminieren, deswegen sage ich jetzt nichts
weiter dazu. Die Frage ist, behindern Softwarepatente die
OpenSource-Bewegung? Mit Sicherheit, würde ich sagen. Ich glaube auch
nicht, dass, wenn jetzt in Europa rechtlich klargestellt wäre, dass es
Softwarepatente gibt und Software patentierbar ist, da auf einmal alle
Unternehmen anfangen würden, die OpenSource-Szene klein zu machen.
Dann würde man die Wirkung sofort sehen, dann würde die Politik
denken, hoppla, was haben wir da gemacht? Ich glaube eher, dass die
Unternehmen, die ein Interesse daran haben, dass die
OpenSource-Software verschwindet - und da gibt es durchaus
Unternehmen, die ein Interesse daran haben - das auf subtile Art und
Weise machen, hier mal, da mal, an strategischen Punkten, so langsam,
dass man es nicht merkt, mit der Konsequenz, dass
OpenSource-Entwickler so langsam die Lust an dem verlieren, was sie
eigentlich machen wollen.

#WWm: Was kann man dagegen machen? Meines Erachtens genügt ein
Quelltextprivileg nicht. Das wäre zwar ganz interessant für ein paar
Hobbyprogrammierer, aber man muss sehen, dass OpenSource-Software auch
jetzt schon eine sehr große wirtschaftliche Bedeutung hat. Betrachten
Sie nur, weltweit gesehen, auf etwa sechzig Prozent der Webserver
läuft der OpenSource-Webserver Apachee, in Deutschland sind es sogar
noch mehr, fünfundsechzig oder noch mehr Prozent. Wenn
OpenSource-Software einerseits sehr gut einsehbar ist und jeder dort
eventuelle Patentverletzungen finden kann, dann genügt das
Quelltextprivileg für Unternehmen mit Sicherheit nicht. Sie lassen
dann die Finger davon und nehmen lieber ein anderes kommerzielles
Produkt, wo sicherlich auch Patentverletzungen enthalten sind - ich
bin überzeugt davon, es gibt heutzutage keine Software mehr, die keine
Patente verletzt -, bei dem man es nicht so schnell nachweisen kann.
Danke.

#Hrn: Herr Otto, es darf der Nachwelt nicht verlorengehen, was die Liberalen
sagen, bitte einfach noch einmal das Mikrophon einschalten.

#Iea: Ich führe die Frage noch fort, und wir sind ja hier so ein bisschen
als Gesetzgeber gefragt. Sie sagen, Quelltextprivileg reicht uns nicht
als Linux-Verband. An was, ich hatte auch Herrn Lutterbeck schon
gefragt, denken Sie denn darüber hinaus?

#Med: Man kann über eine generelle Erlaubnis nachdenken, dass
OpenSource-Software - also solche, die die OpenSource-Definition
erfüllt, die Sie auch im Netz sehen können - in jeder Art und Weise
auch als Programm frei verwendet werden darf. Davon werden natürlich
die Patentinhaber nicht begeistert sein, aber das wäre eine
Möglichkeit.

#KDr: Kollege Dr. Mayer, bitte.

#Iee: Ich habe jetzt noch einmal eine Verständnisnachfrage, die mir gekommen
ist und die mich auch schon länger beschäftigt. Sie sagen, dass
sozusagen der Patentinhaber bei OpenSource hineinschauen kann und bei
Nichtoffengelegten kann er nicht hineinschauen, was verwendet worden
ist. Müsste man denn nicht dann, und das geht an Herrn Schiuma, wenn
im Softwarebereich etwas patentiert worden ist, grundsätzlich
OpenSource verlangen bzw., dass er den Quellcode offenlegt? Denn der
Sinn von Patenten ist ja, dass man die Dinge offenlegt.

#Iie2: Ich würde - Herr Siepmann, damit Sie auch heute Nacht schlafen können
- einfach noch einmal darum bitten, dass vielleicht jeder
Sachverständige in zwei Minuten die Kernbotschaft zusammenfasst, die
er heute Mittag noch nicht übermitteln konnte. Wer nicht davon
Gebrauch macht, den loben wir ausdrücklich, aber die Gelegenheit will
ich Ihnen geben. Vielleicht können Sie, Herr Schiuma, um den Anfang zu
machen, in diese Frage auch gleich Ihre Schlussbotschaft einbauen. Und
Sie, Herr Siepmann, hätten dann auch nachher noch die Möglichkeit.
Ansonsten vorab erst einmal herzlichen Dank.

#Vsd: Vielleicht möchte ich erst einmal die Fragen beantworten, obwohl sich
mein Abschlussstatement nicht in der Beantwortung der Frage erschöpft.
Grundsätzlich ist es so, wenn Sie ein Patent anmelden, wird es nach 18
Monaten offengelegt, d.h. also, dass die Unterlagen, die körperlich
beim Patentamt eingereicht wurden, erfasst und der Öffentlichkeit
zugänglich gemacht werden. Das heißt aber noch nicht, dass man seinen
Source-Code offenlegt. Es ist aber so, dass man in der Patentanmeldung
sämtliche Funktionalitäten beschreiben muss, und zwar muss man sie
ganz explizit und ausdrücklich und verständlich beschreiben, viel,
viel detaillierter beschreiben als man sie z.B. in einem Source-Code
hat. Hier hatte ein Referent - ich weiß jetzt nicht mehr, welcher
Referent - schon vorgetragen, dass es sehr, sehr schwierig ist, aus
einem Source-Code etwas heraus zu exzerpieren. Wenn man hingegen in
Patentanmeldungen hineinschaut, dann ist dort die Regel die, dass man
versucht, die Zusammenhänge darzustellen, dass es prinzipielle
Schemata, Ablaufschemata gibt, die selbstverständlich in einen
Source-Code implementiert werden können, aber die das Prinzip der
zugrunde liegenden Idee wesentlich besser zum Ausdruck bringen. Das
heißt also, die Offenlegung findet tatsächlich dann auch statt. Man
muss aber unterscheiden: Das Produkt, das jemand selber herstellt, das
meinetwegen unter mein eigenes Patent fällt, da muss ich den Quellcode
natürlich auch nicht offenlegen. Ich muss aber die Erfindung, für die
ich Schutz beantrage, so darlegen, dass ein Fachmann sie umsetzen
kann. Das ist ein weiteres Erfordernis, das heute überhaupt nicht zur
Aussprache gekommen ist und das selbstverständlich für alle
Erfindungen gilt, also nicht nur für Software. Das gilt genauso für
jegliches chemisches Verfahren oder für irgendeine Stanzmaschine. Man
muss die Erfindung immer so deutlich darstellen, dass sie für den
Fachmann ausführbar ist. Das bedeutet jetzt im konkreten Fall der
Software: Man muss dem Programmierer, oder wen immer man als Fachmann
betrachtet, so viele Informationen zur Hand geben, dass er in die Lage
versetzt wird, diese Erfindung auch konkret realiter umzusetzen und
ein entsprechendes Programm zu schreiben. Das heißt also, Offenlegung
ist grundsätzlich da, da braucht man sicher nichts zu tun. Inwieweit
das jetzt mit der IT-Sicherheit zusammenhängt, das habe ich nicht so
genau verstanden.

#Mgn: Mein Endstatement sollte eigentlich ganz kurz und knackig meine
Position darlegen, die da ist, dass ich weiterhin der Meinung bin,
dass das Patentrecht die Grundlage des Schutzes auch für Software
bieten sollte. Wir sollten nicht grundsätzlich von der jetzigen
Situation abrücken, dennoch sind einige Klarstellungen notwendig. Zum
einen, um es für die Allgemeinheit und für die Unternehmen
verständlicher zu machen und zum anderen, um Rechtsklarheit auch
dahingehend zu schaffen, dass wir, was die internationalen Verträge
angeht, dort absolut konform sind. Dementsprechend befürworte ich auch
eine entsprechende Anpassung des Europäischen Patentübereinkommens,
was ja im Rahmen der letzten diplomatischen Konferenz zurückgestellt
wurde, in dem Sinne, dass § 1 Abs. 2 Ziffer c sowie Art. 52 Abs. 2
nicht mehr Computerprogramme oder Software oder wie man es nennen
möchte, enthalten. Vielen Dank.

#Dee2: Danke schön. Herr Prof. Lutterbeck, bitte.

#Htm: Herr Vorsitzender, ich hatte mir erlaubt, eine Frage zu beantworten,
die Sie gar nicht gestellt haben, weil ich glaube, dass es für Sie als
Politiker eine der wesentlichen in der heutigen Anhörung ist. Die
Frage, die Sie nicht gestellt haben, lautet: Gibt es einen spezifisch
deutschen oder europäischen Beitrag zur Entwicklung quelloffener
Software? Die Frage ist eindeutig zu bejahen, in beiden Fällen. Und
mit Ihrer Erlaubnis zeige ich zwei Folien. Ein Projekt von mir hat
neueste Zahlen zusammengestellt, sie kommen von dieser Woche. Ich
hoffe, Sie können das alle erkennen. Es ist nicht auf meinen Mist
gewachsen. Sie sehen, Spanien ist auch dabei. Ich zeige das jetzt so,
weil es nicht anders geht.

#Hij: Herr Prof. Lutterbeck, wenn Sie hier in die Leinwand reden, kommt es
nicht ins Protokoll.

#AWs: Also, die wesentliche Aussage ist, die Mehrzahl der offenen
Source-Entwickler kommt aus Europa, dann aus USA - andere Kontinente
sind verschwindend. Sie sehen hier unten die Zahl 712 Megabyte, das
ist ein Haufen Zeug. Es ist eine komplizierte Untersuchung, um
herauszubekommen, wo kommen die Leute eigentlich her, nach Kontinenten
geordnet. Die Gruppe %(q:Nicht näher identifiziert) liegt daran, dass
die Adressen nicht eindeutig sind - also ich habe auch eine
com-mail-adresse, und da weiß niemand, wo ich eigentlich her komme. Da
müsste man weiter forschen. Interessant ist auch dieses: Geht man nach
Ländern vor, stellt man fest, dass in Europa eindeutig die Deutschen
die Führenden sind, also mit führend meine ich nicht die Besten,
sondern von der Zahl her. Das ist ja hoch relevant, und das ist mein
letztes Statement. Ich meine, mit diesen Zahlen, die auch einige
Amerikaner schon sehr interessiert, also verwirrt haben, nachdem sie
wissen, dass Linux eine finnische Erfindung ist und viele andere
Dinge, dass die Europäer und die Deutschen führend sind. Meiner
Meinung nach ist das eine Sache, wo ich hoffe, dass Politiker sich
näher damit befassen. Eine endgültige Antwort habe ich nicht; es ist
ja ein laufendes Forschungsvorhaben, aber die Zahlen sind eindeutig
und werden auch durch andere Untersuchungen bestätigt. Deutschland ist
sozusagen führend in Europa. Das ist nicht nur eine Frage der
Ökonomie, die Herr Siepmann schon erwähnt hat, sondern betrifft
möglicherweise eine andere Frage, über die man reden müsste, wenn man
Zeit hätte. Vielen Dank.

#Gel: Ganz herzlichen Dank. Ich glaube, das werden wir auch noch tun müssen
an anderen Stellen und tun es auch. Herr Henckel, bitte.

#Jnl: Ja, eine abschließende Bemerkung, und zwar glaube ich, dass
Softwarepatente letztendlich das Urheberrecht untergraben, indem die
Enteignung der Programmierer, also der eigentlichen Urheber von
Software, gefördert wird. Der Programmierer kann sein eigenes Werk
also nicht mehr als sein eigenes betrachten und ist letztendlich von
der Gnade - kann man sagen - von Patententwicklern abhängig. Die Folge
davon für die OpenSource-Entwickler wird sein, dass sich sehr viele
von ihrem privaten Engagement verabschieden werden und letztendlich
ihre Ideen dann nur kommerziellen Firmen zur Verfügung stellen. Da
weiß man dann auch, dass die Nutzung von Ideen, von Innovationen sehr
oft durch andere Kriterien behindert wird wie Marketing usw., so dass
sehr viele innovative Ideen, die in den Köpfen der
OpenSource-Entwickler stecken, auf der Strecke bleiben.

#Dll: Danke schön. Frau Bouillon, bitte.

#AsE: Auch wenn ich selber nicht der OpenSource-Szene angehöre und auch
unser Unternehmen nicht, fand ich diese Kernaussage meines Vorredners
Herrn Henckel sehr, sehr treffend. Das ist genau das, was wir
eigentlich auch empfinden: Dass wir in Zukunft nicht mehr sicher sein
können, dass wir Software, die wir selber entwickelt, selber
programmiert und selber erdacht haben, auch als unser Eigentum sehen
können.

#Drr2: Danke schön. Herr Dr. Kiesewetter-Köbinger, bitte.

#Btt: Bei dieser Antwort des Herrn Schiuma, dass der Quelltext nicht Teil
der Patentanmeldung sein soll, bin ich erheblich anderer Meinung und
das habe ich, glaube ich, auch in meinem Statement für den heutigen
Tag zum Ausdruck gebracht und in meinem Artikel. Es ist so: Je
schwieriger die Aufgabenstellungen sind - und in der Softwarebranche
sind diese Lösungswege, die Software wirklich zu kodieren, nicht
einfach -, umso schwieriger ist nachzuweisen, dass wirklich eine
Lösung offenbart wurde, wenn man nur ein abstraktes Prinzip angibt.
Die vollständige Offenbarung nachzuarbeiten, wenn ein Prinzip da
steht, und zu suchen, welche Tools, welche Bibliotheken nehme ich und
aus welchen Architekturen löse ich dieses Problem, ist fast nicht zu
machen, wenn man aussuchen muss, welcher Prozessor für diese Anwendung
eigentlich billig genug ist, dass ich ihn hineinstecken kann und
welche Entwicklungstools und welche Bibliotheken es dafür gibt. Als
Prüfer ist das unmöglich in vernünftigen Zeiträumen zu machen, man
müsste es selber nachbauen. Schlussfolgernd ist für mich daraus die
Konsequenz: Bei derartig komplizierten Sachverhalten, wie sie in der
Softwareindustrie wie auch in der Elektronikindustrie auftreten, ist
es notwendig, für die Offenbarung einer Lösung wirklich die Quellen
anzugeben. Da ist das Amt überhaupt nicht begeistert, da bin ich mir
hundertprozentig sicher, weil anstatt zehnseitiger Anmeldung dann
kiloweise Anmeldungen hereinkommen, die man sich praktisch nicht mehr
durchlesen kann. Wie es früher auch schon behauptet wurde, ist der
Quelltext für den Prüfer irrrelevant, denn er kann nicht tausende von
Seiten, von Listings durchstöbern, aber für die Offenbarung der Lösung
finde ich, ist es unabdingbar.

#Dlm: Danke schön. Herr Siepmann, bitte.

#IsW: Ich zitiere nur ganz kurz aus einer Patentschrift, betreffend den
IDEA-Verschlüsselungsalgorithmus. Da heißt es u.a.: %(bq:Durch eine
Logik, die jeweils nacheinander wenigstens vier logische Operationen
wenigstens zweier unterschiedlicher Sorten durchführt, wobei
wenigstens die überwiegende Zahl aller Paare unmittelbar
aufeinanderfolgender Operationen aus zwei Operationen
unterschiedlicher Sorten besteht, wobei durch jede Operation jeweils
zwei Eingangsblöcke der zweiten Länge in einen Ausgangsblock dieser
Länge umgewandelt werden, wobei als Eingangsblöcke erste Teilblöcke,
Steuerblöcke und/oder Ausgangsblöcke einer jeweils vorhergehenden
Operation dienen.) Ich zitiere nicht weiter, weil ich Ihnen nicht den
Rest geben möchte, aber ich kann Ihnen versichern, dass Sie das, was
da drin steht, in wenigen Zeilen in ein Open-Source-Programm oder in
jegliches Programm schreiben können und es für jedes Programm
wesentlich verständlicher ist. Danke. Ansonsten freue ich mich in
Zukunft auf eine anregende Diskussion mit den Abgeordneten des
Deutschen Bundestages.

#Rnn: Recht herzlichen Dank. Wir werden das Zitat im Protokoll ausführlich
noch einmal nachlesen und der Vorsitzende wird es in jeder Sitzung
genauso fehlerfrei wiederholen, selbstverständlich. Frau Bremer,
bitte.

#Zra: Zum einen möchte ich noch einmal darlegen - es wurde von Ihnen kurz
angesprochen -, dass soviel dar- oder offengelegt werden muss, dass
der Fachmann es nachvollziehen kann und nicht das ganze Produkt
komplett offengelegt werden muss. Generell sehen wir hier, dass der
Patentschutz auch bei Software-implementierten Erfindungen
aufrechterhalten werden bzw. bestehen muss. Wir sehen es nicht als
erforderlich, dass der Patentschutz hier in erheblichem Maße
ausgeweitet werden muss. Wir sehen es aber als erforderlich an, dass
hier eine Anpassung an die Rechtsprechung, d.h. eine Klarstellung der
Rechtslage in der Form stattfindet, dass die Ausnahmebestimmung, nach
der Programme als solche ausgenommen sind, national und auch im
Europäischen Patentübereinkommen gestrichen wird. Wir sind auch der
Auffassung, dass es nicht die OpenSource-Bewegung lahmlegen wird.
Dafür gibt es einige Gründe: Zum einen wird es nicht eine Patentflut
geben in den nächsten Jahren, da sich jeder sehr gut überlegen wird,
ob er ein Patent anmeldet, zum anderen muss auch die
OpenSource-Bewegung schon jetzt geistige Schutzrechte beachten, zum
einen das Urheberrecht, zum anderen aber auch US-Patente. Und damit
wollen wir jetzt nicht der USA nacheifern, aber es ist einfach Fakt,
dass es diese Patente gibt, d.h. auch da muss man jetzt schon ein Auge
drauf werfen. Als Letztes denke ich, dass die OpenSource-Bewegung eine
sehr kreative und innovative Bewegung ist und ich glaube nicht, dass
sie sich durch einige erteilte Patente lahmlegen lässt.

#Vaj: Vielen Dank und last but not least, Herr Dr. Probst.

#Mew: Mein Schlussstatement wäre: Es ist hier nicht primär eine juristische
Frage, es geht nicht um Ästhetik, Einfachheit, historische Konsistenz
von Rechtsprechung, das ist sekundär. Die relevante Frage ist, ob
diese Änderung gesellschaftlich im Sinne von gesamtwirtschaftlicher
Wohlfahrt wünschenswerte Folgen hat, %(q:ja) oder %(q:nein). Und der
Stand der wissenschaftlichen Forschung bezüglich dieser Fragen ist
irgendwo zwischen %(q:wir wissen es nicht) und %(q:nein). Und es wäre
sehr gewagt, einfach in eine andere Richtung zu gehen aus
juristischen, historischen oder ästhetischen Konsistenzwünschen oder
sonstigen Sachen. Danke.

#HnW2: Herzlichen Dank. Es war eine zielgenaue Landung, was 18 Uhr angeht.
Mein Wunsch, vielleicht ein paar Minuten früher aufzuhören, hat sich
nicht erfüllt, aber ich glaube, es war spannend bis zum Schluss.
Insofern ganz, ganz herzlichen Dank an unsere Sachverständigen, die
sich die Zeit genommen haben. Herzlichen Dank an eine große
Zuhörerschaft, die aufmerksam gefolgt hat. Wir werden den Sommer über
natürlich nicht Urlaub machen, sondern all dieses jetzt aufschreiben -
das ist die Strafe des Vorsitzenden - und werden Ihnen eine
Zusammenfassung liefern. Ich glaube, es ist heute eine gute Grundlage
gelegt worden für die weiteren Debatten und dafür bedanke ich mich bei
allen Beteiligten recht herzlich.

#TGW: Traditionell machen wir das immer so, dass die Sachverständigen, die
noch nie im Bundestag waren und noch ein paar Minuten Zeit haben, auch
auf die Kuppel geführt werden. Gibt es hierfür ein gewisses Interesse?
Die Kuppel ist überglast, darauf wollte ich noch aufmerksam machen,
aber der Blick ist beeinträchtigt, das ist wahr. Also, wer mag, möge
sich gleich bei mir melden. Ganz, ganz herzlichen Dank und einen
schönen Abend noch, gute Heimreise und schönen Abend in Berlin, oder
wo auch immer sie ihn verbringen.

#Wsm: What is the rationale for patent protection in general?

#WWc: What protection rights exist for software and how do they work?

#WWe: How is software different from other inventions?

#WWe2: What are software patents?

#Wen: How is the inventive height (non-obviousness) of software patents to
be assessed?

#UWm: Are software patents different from patents on business ideas,
algorithms and file formats?  If yes, in what way?

#Won: Where does the protection of software in Germany differ from that in
other countries in and outside Europe?

#Net: According to which criteria do the EPO, the Federal Court (BGH) and
the Federal Patent Court (BPatG) judge the patentability of software? 
Are these criteria and rules unambiguous and which formulas have
proven to be useful?

#Wta: How many software patents exist in Germany, Europe and worldwide?

#Bdh: In how many of the 30000 software patents granted so far by the EPO is
the claimed contribution to innovation so significant that it could
considered a good deal for society to grant a 20 year monopoly on
them?

#GWe: Are there legal constraints (constitution, international treaties)
that necessitate an extension of the patent system to software?

#Wnh: How do you find the idea of introducing a special protection right
(sui generis right) for software?

#WeW: Where could copyright offer insufficient protection against imitation?
  What could a %(q:specially tailored software protection right) look
like?

#Mgt: Does it make sense to distnguish patentable goods from non patentable
ones?

#Wre: If yes, according to which criteria should the distinction between
these two kinds of goods (kinds of inventions) be made?

#Wve: Which macro- and micro-economic effects would a largely unlimited
patentability of so-called software products have?

#Wea: Which are the methods of work and economic foundations of the open
source movement?

#WWW: What are the effects of extended patentability of software on the
framework of software development under so-called open-source
licenses?

#Wta2: Would an extension of patent protection for software hamper the open
source movement?  If yes, how could these disadvantages be removed?

#WaW: Which effects would the extension of patent protection to software
have on the development of small and medium enterprises?

#SWt: Do you think that an extension of patentability for software would
endanger innovations in this area?  How do you view the %(si:MIT study
%{SI} by %{BM} (1999)) in this context?

#Igh: Is patent law a suitable tool for promoting innovation and progress in
the field of software?

#StW: Are there more effective alternatives and, if yes, which?

#Wde: If it was now to be clarified at the EU level that software and
abstract concepts are not patentable, how would this affect already
existing patents in this area?

#Ant: Anerkennungstheorie

#Bnt: Belohnungstheorie

#Anh: Anspornungstheorie

#Oat: Offenbarungstheorie

#Vae: Vertragstheorie

#vhe: volkswirtschaftliche Begründungen

#Ube: Urheberrecht

#vhi: verhindert Kopieren und Nachahmen/Plagiieren, soweit dies glaubhaft
nachweisbar ist

#wpW: was meist nur bei plumpem Nachahmen der Fall ist, Ideen bleiben frei

#wng: wurde durch mehrere Novellen in den 80-90er Jahren auf Sw angepasst
und dabei ausgeweitet.

#stl: sorgt vor allem zusammen mit Quelltext-Geheimnis für starken
Investitionsschutz: Nachprogrammieren ist i.d.R. kaum weniger
aufwendig als Entwicklung eigener Werke.

#Aee: Andererseits ist Nachprogrammierung um der Interoperabilität willen
nötig.  Dies wurde von der EU-Urhr-Rili von 1992 ausdrücklich
anerkannt.

#Uim: Unglückliche Formulierung

#Iut: Im patentrechtlichen Sinne sind Programme für
Datenverarbeitungsanlagen ebenso wie andere Pläne für gedankliche
Tätigkeiten sowie Organisations- und Rechenregeln keine Erfindungen.

#VWl: Von technischen Erfindungen unterscheiden sich innovative Rechenregeln
u.a. dadurch, dass man zu ihrer Anwendung keine materiellen Objekte
und keinen damit verbundenen Investitionsaufwand benötigt.

#Ikr: In der Regel sind Rechenregeln entweder sehr abstrakt oder trivial und
(mangels Notwendigkeit von Experimenten mit Naturgesetzen) relativ
leicht zu finden.

#Egi: Einem im Vergleich zu technischen Erfindungen geringeren
Belohnungsinteresse steht ein höheres Freihaltungsbedürfnis entgegen.

#PlW: Pflichtlektüre hierzu:

#csf: %(q:Softwarepatente) ist ein Arbeitsbegriff für %(q:Patente auf
Organisations- und Rechenregeln, wie sie typischerweise auf einem
Universalrechner ausgeführt werden) oder %(q:Patente auf Programme für
Datenverarbeitungsanlagen).

#Hcu: Hierunter fallen auch Patente auf festverdrahtete
Hardware-Implementierungen solcher Organisations- und Rechenregeln,
nicht jedoch Patente auf %(q:technische Erfindungen), d.h.
Problemlösungen, deren Gültigkeit durch Experimentieren mit
Naturkräften und nicht durch menschliche Verstandestätigkeit überprüft
werden muss.

#Wrk: Weitere Lektüre:

#Gue: Genau so wie andere Patente:  es ist zu prüfen, ob ein nicht
naheliegender Beitrag zum Stand der Technik vorliegt.

#Ete: Eine Organisations- und Rechenregel fügt dem Stand der Technik nichts
hinzu, weist also keine patentrechtlich relevante Erfindungshöhe auf.

#Aur: Auch wenn man den Technikbegriff so ausweitet, dass Organisations- und
Rechenregeln mit in die Abwägungen einbezogen werden, ist die
Erfindungshöhe bei den meisten heutigen Softwarepatenten sehr gering.

#Wrk2: Weitere Lektüre

#Nei: Nein.

#MWk: Man kann einer Patentschrift ansehen, ob es eine technische Erfindung
offenbart.  Offenbart sie nur eine Organisations- und Rechenregel, so
handelt es sich um en Softwarepatent.

#Mgi: Mit Begriffen wie %(q:Algorithmen), %(q:mathematische Methoden),
%(q:Pläne für verstandesmäßige Tätigkeit), %(q:Programm für
Datenverarbeitungsanlagen) u.a. bezeichnet das Gesetz (§1 PatG, Art 52
EPÜ) und das patentrechtliche Schrifttum gleichermaßen
%(e:Organisations- und Rechenregeln) aller Art, mit denen man
abstrakt-logische Probleme löst.

#Mrd: Man kann jedoch normalerweise nicht erkennen, ob eine Organisations-
und Rechenregel später einmal zum Lesen oder Schreiben eines
Dateiformates verwendet werden muss.

#Dei: Die meisten Softwarepatente beanspruchen die Wirkungen von Programmen.

#Dei2: Diese Wirkungen liegen meistens in einer bestimmten geschäftlich
nützlichen Art des Informationsaustauschs.

#Egn: Es gibt Organisations- und Rechenregeln, die vor allem dazu dienen,
eine Geschäftsbeziehung zu gestalten.

#Mtu: Manche dieser Wirkungen beschreiben Geschäftsideen im engsten Sinne,
so z.B. das Ausdrucken von Kochrezepten in einem Supermarkt.

#Vse: Verfahren für geschäftliche Tätigkeit sind eine unklar abgegrenzte
Untermenge der Organisations- und Rechenregeln.

#Fua: Fast alle neuen Geschäftsverfahren werden heutzutage in Form von
Programmen für Datenverarbeitungsanlagen formuliert und ausgeführt.

#DvE: Dies gilt insbesondere für diejenigen Geschäftsverfahren, für die
diverse Firmen beim EPA Patentschutz begehrt und erhalten haben.

#WbW: Was die Bestimmungen des Patentrechts betrifft, gibt es keine
wesentlichen Unterschiede zu anderen EU-Ländern.  Überall gilt das
Europäische Patentübereinkommen.  Es gibt aber Unterschiede zwischen
einzelnen Gerichten, auch innerhalb der Staaten, so z.B. zwischen dem 
  10. Zivilsenat des BGH (patentierungsfreundlich) und dem 17. Senat
des Bundespatentgerichts (lehnt Softwarepatente weitgehend ab).

#Ilr: In den USA fehlen gesetzliche Regelungen zur Begrenzung der
Patentierbarkeit.  In den 60er Jahren lehnte das US-Patentamt die
Patentierung von Rechenprogrammen konsequent ab.  Aber während in
Europa in den 70er Jahren der Technikbegriff erneut konsolidiert
wurde, war die Aufweichung entsprechender Grenzziehungen in den USA
schon damals in vollem Gange.  Seit 1986 hat das EPA gegenüber dem
USPTO aufgeholt und es inzwischen weitgehend eingeholt.  Wie PA Betten
richtig feststellt, liegen die Unterschiede im wesentlichen in der Art
der Anspruchsformulierung.  Einigen Studien zufolge leistet das EPA
etwas bessere Arbeit bei der Patentrecherche, stellt aber geringere
Ansprüche bei der Erfindungshöhe (Nichtnaheliegen / erfinderische
Tätigkeit).

#IiW: In Japan ist ähnlich wie in Europa der Begriff der %(q:technischen
Erfindung) im Patentgesetz verankert.  Sie wird als %(q:Anwendung von
Naturgesetzen) definiert.  Das hat das japanische Patentwesen jedoch
ebenso wenig wie das Europäische davon abgehalten, mit einiger
zeitlicher Verzögerung die Entwicklung der USA nachzuvollziehen.

#Ens: EPA und BGH befinden sich seit 1986 auf einem %(sk:Schlitterkurs): es
fehlt an ebenso an eindeutigen Kriterien wie an praktikablen
Formulierungen.  So sieht das EPA z.B. eine Datenbank-Rechenregel als
technisch an aber lehnt gelegentlich Textverarbeitungs-Rechenregeln
als untechnisch ab.  Softwarepatent-Befürworter bezeichnen diese
Rechtslage als verworren.

#DrW: Der 17. Senat des BPatG bemüht sich derweil, den Begriff der
%(q:technischen Erfindung) weiterhin %(q:restriktiv) anzuwenden, um im
Einklang mit dem noch geltenden Gesetz zu bleiben.  Er hat %(bw:im
August 2000 Ansprüche von IBM auf ein Computerprogramm zurückgewiesen)
und im Frühjahr 2001 ein Softwarepatent wegen mangelnder Technizität
abgelehnt.  In diesen Urteilen hat er immer wieder Argumentationen des
EPA und BGH widerlegt und ihnen die Gefolgschaft verweigert.  Seit
etwa 8 Jahren gibt der BGH regelmäßig Rechtsbeschwerden statt, die
dort von Klägern wie IBM und Siemens gegen das BPatG als %(q:Beiträge
zur Rechtsfortbildung) angestrengt werden.

#Bce: Bei 3% der heutigen EPA-Patente liegt die %(q:Erfindung) im Bereich
der Organisations- und Rechenregeln, die übrigen betreffen eine
erfinderische Anwendung von Naturgesetzen, sind also technisch.

#IEa: Insgesamt gibt es am EPA unter 1 Million Patenten %(ep:ca 30000
Softwarepatente).

#Dbo: Das US-Patentamt hat bereits über 100000 Softwarepatente erteilt.

#Vur: Vermutlich 0%, Ausnahmen sind uns nicht bekannt.

#Dnv: Die meisten sind Trivialpatente.

#Asg: Auch nicht-triviale Software-Neuerungen führen typischerweise zu
trivialen und breiten Ansprüchen auf ganze Aufgabenstellungen /
Programmfunktionalitäten.

#Aig: Vergleichsweise akzeptabel erscheinen Patente wie die im Bereich der
psycho-akustischen Kompression (MP3), die wiederum z.T. auf
experimentell gewonnenen Erkenntnissen beruhen, sowie einige sehr
komplizierte und spezielle Rechenregeln im Bereich der Kompression und
Kryptographie.

#AGg: Aber auch in diesen Ausnahmefällen macht die Gesellschaft mit der
Gewährung eines 20-jährigen Monopols einen schlechten Handel, s.
unten.

#Nie: No, on the contrary.

#Fef: Folgende Gründe werden gelegentlich von Softwarepatentbefürwortern ins
Feld geführt:

#ErG: Eigentumsgarantie des Grundgesetzes

#Pss: Klausel gegen unsystematische Patentierbarkeitsausnahmen in einem
Freihandelsvertrag

#Eta: Springorum, Schiuma und einige andere Softwarepatentbefürworter
begründen ihre Forderungen nach Patentierbarkeit von Rechenregeln
gelegentlich mit einem angeblichen menschenrechtsähnlichen Status des
%(q:Eigentums) an Ideen.

#Eas: Das BVerfG hat in seiner Rechtsprechung das Grundrecht auf Eigentum
tatsächlich hin und wieder auf immaterielle Ansprüche ausgedehnt. 
Dabei ist von einer %(q:Institutsgarantie) für Urheber- und
Patentrecht die Rede, die jedoch sofort wieder durch die Aufforderung
an den Gesetzgeber eingeschränkt wird, für einen gerechten Ausgleich
zwischen den Rechten des zeitweiligen %(q:Eigentümers) und der durch
Ausschlussrechte zeitweilig in ihren Rechten eingeschränkten
(enteigneten?) Öffentlichkeit zu sorgen.  Wie der Gesetzgeber diesen
Ausgleich zu bewerkstelligen hat, bleibt offen.  Die Ausdehnung des
Eigentumsbegriffs auf immaterielle Ansprüche aller Art wirft mehr
Fragen auf, als sie beantwortet.  Klar ist allenfalls, dass nur dem
Urheber und niemand anderem ein abstrakter rechtlicher Status als
bevorzugter Nutznießer eventuell zu gewährender Vorrechte oder
Vergütungen zuzuerkennen ist.

#Hil: Hieraus folgt nicht, dass tatsächlich bestimmte Vorrechte oder
Vergütungen gewährt werden.  Insbesondere folgt nicht, dass alle
geistigen Leistungen des Menschen durch ein %(q:Eigentumsrecht)
belohnt werden müssen.  1976 erklärte der BGH in seinen vielzitierten
Schlussworten im %(dp:Dispositionsprogramm-Beschluss) hierzu im
Gegenteil:

#Deh: Der Begriff der Technik erscheint auch sachlich als das einzig
brauchbare Abgrenzungskriterium gegenüber andersartigen geistigen
Leistungen des Menschen, für die ein Patentschutz weder vorgesehen
noch geeignet ist. Würde man diese Grenzziehung aufgeben, dann gäbe es
beispielsweise keine sichere Möglichkeit mehr, patentierbare
Leistungen von solchen zu unterscheiden, denen nach dem Willen des
Gesetzgebers andere Arten des Leistungsschutzes, insbesondere
Urheberrechtsschutz, zuteil werden soll.  Das System des deutschen
gewerblichen und Urheberrechtsschutzes beruht aber wesentlich darauf,
dass für bestimmte Arten geistiger Leistungen je unterschiedliche,
ihnen besonders angepasste Schutzbestimmungen gelten und dass
Überschneidungen zwischen diesen verschiedenen Leistungsschutzrechten
nach Möglichkeit ausgeschlossen sein sollten. Das Patentgesetz ist
auch nicht als ein Auffangbecken gedacht, in welchem alle etwa sonst
nicht gesetzlich begünstigten geistigen Leistungen Schutz finden
sollten. Es ist vielmehr als ein Spezialgesetz für den Schutz eines
umgrenzten Kreises geistiger Leistungen, eben der technischen,
erlassen und stets auch als solches verstanden und angewendet worden.

#Eet: Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem
Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren
Aufgabe hinauslaufen würde -- zu erlangen. Es muss vielmehr dabei
verbleiben, dass eine reine Organisations- und Rechenregel, deren
einzige Beziehung zum Reich der Technik in ihrer Benutzbarkeit für den
bestimmungsgemäßen Betrieb einer bekannten Datenverarbeitungsanlage
besteht, keinen Patentschutz verdient. Ob ihr auf andere Weise, etwa
mit Hilfe des Urheber- oder des Wettbewerbsrechts, Schutz zuteil
werden kann, ist hier nicht zu erörtern.

#FnW: Im TRIPS-Vertrag ist festgeschrieben, dass Patente für %(q:Erfindungen
auf allen Gebieten der Technik) erhältlich sein müssen.  Schiuma und
andere %(sc:leiten hieraus ab), Organisations- und Rechenregeln
müssten patentierbar sein, da die %(q:Softwareindustrie) nach üblichem
Sprachgebrauch ein %(q:Gebiet der Technik) sei, auf welchem Patente
erhältlich sein müssen.

#Dnl: Diese Argumente beruhen auf mehr oder weniger impliziten
Voraussetzungen, denen wir nicht zustimmen müssen.  Bei der von
Schiuma geforderten %(q:autonomen) Interpretation dieses Vertrages
müssen keinesfalls amerikanische Sichtweisen zugrunde gelegt werden. 
Im Gegenteil müsste gerade bei einer %(q:autonomen) (d.h. von
spezifisch regionalen Traditionen unabhängigen) Interpretation einen
universelle und scharfen, aus der Wissenschaftstheorie und nicht aus
einer zeitlich und örtlich begrenzten Rechtstradition heraus
begründeten Begriff der %(q:technischen Erfindung) anstreben.   Auch
25 Jahre nach dem oben zitierten Dispositionsprogramm-Urteil erscheint
%(q:auch sachlich der Begriff der Technik als das einzige brauchbare
Abgrenzungskriterium).   Es gibt somit für eine autonome
Interpretation von Art 27 TRIPS nur einen seriösen Bewerber:  den
Begriff der technischen Erfindung, wie er in Europa, Japan und den USA
immer wieder mehr oder weniger explizit in Gesetzen und Urteilen
formuliert worden ist.

#Wtb: Während diese für die Softwarepatentierung ins Feld geführten
juristischen Gründe auf Missverständnissen und Fehlschlüssen beruhen,
gibt es durchaus gewichtige Rechtsgüter und Bestimmungen, die einer
Patentierbarkeit von Programmen für Datenverarbeitungsanlagen
(computerimplementierbaren Organisations- und Rechenregeln)
entgegenstehen:

#Aeg: Art 5 GG: Freiheit des gesprochenen und geschriebenen Wortes

#esr: erhöhtes Freihaltungsbedürfnis bei immateriellen Geistesleistungen, s.
BGH-Beschluss Dispositionsprogramm oben und %(kr:Lehrbuch des
Patenrechts), Stichwort Freihaltungsbedürfnis

#csK: %(rv:Römische Verträge) fordern, dass alle wirtschaftspolitische
Maßnahmen dem Verbraucher zu dienen und für einen freien und fairen
Wettbewerb zu sorgen haben.

#BnE: Bestimmungen in der EU-Urheberrechtslinie, welche ausdrücklich
fordern, dass Programmierideen frei bleiben müssen und dass das
Urheberrecht nicht zur Verhinderung von Interoperabilität eingesetzt
werden darf.

#FGw: Freihandel und damit verbundene Verträge der WTO (GATT, TRIPS)
verbieten Handelsmonopole, wie sie etwa durch
Geschäftsverfahrenspatente erzeugt werden.

#Anu: Art 27 TRIPS fordert zur einer scharfen Definition der Begriffe
%(q:Technik), %(q:Erfindung) und %(q:industrielle Anwendbarkeit) auf.

#Guf: Gerade der Eigentumsbegriff erfordert gerechte Abwägung der Ansprüche
aller Seiten.  Daraus ergibt sich ein ähnlicher verfassungsmäßiger
Grundsatz wie er in der US-Verfassung formuliert ist:  die
eingeschränkte/enteignete Öffentlichkeit muss durch %(q:Fortschritt
der Wissenschaft und nützlichen Künste) entschädigt werden.  Mit der
Gewährung von Patenten auf abstrakte Ideen wird dieser
Verfassungsgrundsatz verletzt.

#Dle: Das Software-Urheberrecht stellt bereits ein solches
software-spezifisches Schutzrecht dar.

#Mkm: Man könnte es weiter mit Software-Spezifika anreichern und zu einem
unabhängigen Schutzrecht machen.

#Gte: Grundsätzlich besteht bei Geistesleistungen im digitalen Bereich, die
der Öffentlichkeit zugute kommen, ein Dilemma:  einerseits will die
Leistung teuer bezahlt werden, andererseits kommt sie erst bei einem
Preis von Null der Volkswirtschaft und damit auch der Softwarebranche
in vollem Umfang zugute.  Ein Preis von Null erlaubt keine
Gewinnmargen, eine forcierte Kontrolle über digitale Güter führt zu
unproduktiven Monopolen.

#Dwt: Dieses %(q:digitale Dilemma) gibt es in allen informationsbasierten
Branchen.  Die Softwarebranche hat dabei relativ wenig zu klagen.  Sie
hat ein Gleichgewicht gefunden.  Auf der einen Seite erlauben
Betriebsgeheimnis, Urheberrecht, komplementäre Märkte und zahlreiche
weitere Mittel eine beneidenswert lukrative Verwertung von
informatischen Leistungen.  Auf der anderen Seite wird das Problem der
behinderten Wissendiffusion durch Standardisierung und freie Software
behoben.

#NWo: Normalerweise ist es sehr aufwendig, ein existierendes Programm
nachzuprogrammieren, ohne das Urheberrecht zu verletzen.  Hinzu kommt,
dass Firmen, die Nachahmer auf Distanz halten wollen, ohne weiteres
Quelltexte vorenthalten oder unlesbar machen können.  Dies ist bei
proprietärer Software regelmäßig der Fall.  Auch Dekompilierung
liefert nur nach aufwendigem Einsatz menschlicher Intelligenz
brachbare Ergebnisse und bringt somit für den Nachahmer meist nur
weiteren Zeitverlust.

#EMs: Es gibt hier und da geniale mathematische Problemlösungen, denen man
über den vorhandenen Nachahmungsschutz hinaus einen besonderen Lohn,
vielleicht u.a. auch in Form eines erweiterten Ausschlussrechtes gegen
Nachahmung, zubilligen möchte.

#End: Eine Möglichkeit, dies umzusetzen, bestünde in einer weiten Auslegung
des Urheberrechts:  wer in den ersten 2 Jahren nach Bekanntwerdung
einer solchen Neuerung diese nachahmt, trägt die Beweislast dafür,
dass es sich um kein Plagiat handelt.  Bei dieser Beweisführung kämen
dann u.a. die Argumente der Neuheit und Erfindungshöhe ins Spiel, ohne
dass man die Softwarebranche deshalb in das Prokrustes-Bett des
überkommenen Patentwesens zwängen muss.

#Ner: Neben Ausschlussrechten kommen auch Vergütungsrechte in Frage, die
über GEMA-ähnliche Töpfe finanziert werden.  Einigen neueren Studien
zufolge bewirken diese im digitalen Bereich sogar mehr. Ferner hat die
öffentliche Forschungspolitik eine bedeutende Rolle zu spielen. 
Vielleicht sollten öffentliche Forschungsstätten stärker für
Quereinsteiger geöffnet werden.

#Dne: Denkerische Höchstleistungen sind in den abstrakten Wissenschaften ein
natürlicher Ausfluss der normalen alltäglichen Wissensarbeit.  Das
System des wissenschaftlichen Fachdialoges (peer review) ist zu ihrer
Bündelung und Anspornung vermutlich wesentlich besser geeignet als
jedes patentähnliche Ausschlussrecht.  Es wird durch das Urheberrecht
und andere sanfte Verwertungsmechanismen in nützlicher Weise ergänzt. 
Patente und andere harte Ausschlussrechte haben dort ihren Platz, wo
mit einer Innovation teure und riskante Ausrüstungsinvestitionen
verbunden sind, für die sich nicht so leicht andere
Finanzierungsmöglichkeiten finden, d.h. im Bereich der Technik.

#LkK2: Lektüre

#GD7: Ja, selbstverständlich.

#EWu: Es sollten allerdings nicht nach Gütern, für die Schutz beansprucht
wird, sondern nach Schöpfungsleistungen, aufgrund derer er beansprucht
wird, unterschieden werden.

#Dne2: Der Negativkatalog in §1 PatG / Art 52 EPÜ ist eine Liste von
nicht-technischen Schöpfungsleistungen, nicht eine Liste von
nicht-technischen Gütern.

#nga: nach dem Begriff der %(q:technischen Erfindung), wie er dem geltenden
Gesetz zugrunde liegt aber seit 1986 vom EPA und seit 1992 vom BGH
Schritt für Schritt über Bord geworfen wurde.

#sme: s. Liste im %(ob:Offenen Brief)

#Plk: Proprietäre Lizenzmodelle beschränken in vieller Hinsicht den Nutzen
von Informationswerken für den Anwender und für die Volkswirtschaft.

#Iei: Indem man seine Werke öffentlich verfügbar macht, erreicht man einen
viel größeren Gesamtnutzen und kann manchmal auch dabei einen
geschäftlich interessanten Anteil für sich selbst einfangen.

#Hee: Hierauf beruhen verschiedene Geschäftsmodelle, die viel zur
Verbreitung und Weiterentwicklung der freien Software beitragen.

#WWd: Wir sprechen nicht von einer %(q:erweiterten Patentierbarkeit von
Software) sondern von einer %(q:Patentierbarkeit von Software) oder
einer %(q:Erweiterung der Patentierbarkeit auf Software hin).

#Fas: Freie Software ist in höherem Maße gefährdet (da offen einsehbar) und
zugleich in geringerem Maße als andere in der Lage, Ausschlussrechte
für sich einzusetzen (da auf Freiheit beruhend).

#Fae: Freie Software hat nichts davon, wenn sie etwa Patent einsetzt, um die
GPL oder ein %(q:OpenSource-Geschäftsmodell) anderen aufzudrängen.

#Zii: Zugleich gibt es bei freier Software keinen Rechteeigentümer, der über
Lizenzeinnahmen ein Patent finanzieren könnte.

#Dhe: Die Gefahr ist sehr groß.

#Enr: Es gibt Firmen wie Microsoft, die freie Software grundsätzlich als
Bedrohung ansehen und Patente zu ihrer Bekämpfung einsetzen wollen (s.
Halloween-Papiere).

#Drr: Die einzige gute Karte, die Entwickler freier Sw im Patentwesen haben,
besteht in der Menge von dezentralen Vorveröffentlichungen. Diese
vergrößern die Rechtsunsicherheit im Patentwesen, da sie eine
effektive Patentrecherche erschweren und notfalls auch in gewissem
Maße zur gezielten Bekämpfung von Patentschurken eingesetzt werden
können.

#Zdr: Zum Schutz der freien Software in Deutschland wäre es notfalls
ausreichend, Schrankenbestimmungen wie die in §11 PatG dahingehend zu
erweitern, dass die Veröffentlichung und Inverkehrbringung von freie
Software (noch besser von Informationsgebilden aller Art) ebenso wie
die Ausführung der entsprechenden Programme auf einem herkömmlichen
Rechner keine Patentverletzung darstellen kann.

#EWi: Ein nachhaltigerer Schutz wäre über eine Rückkehr zum klar umgrenzten
Begriff der technischen Erfindung, wie er dem heutigen PatG zugrunde
liegt, zu erreichen.

#Ect: Ebenfalls sinnvoll wären ein gesetzlicher Schutz des Rechtes auf
Kompatibilität:  Softwareentwickler müssen unbeschadet von Rechten
Dritter für Kompatibilität sorgen können, und Träger öffentlicher
Funktionen sollten keine Software einsetzen, die den Bürger über
Dateiformate o.ä. dazu zwingt, bestimmte (proprietäre) Software zu
verwenden.

#Egi2: Es spricht nichts dagegen, all dies und weiteres gleichzeitig in
Angriff zu nehmen, wie auch im %(ob:Offenen Brief vom Dezember 2001)
gefordert.

#Fio: Ferner wären der Zuständigkeit ausländischer Rechtssprechungen für
hiesige Internetangebote klare Grenzen zu setzen.  Hier drohen
insbesondere Gefahren durch eine Ausweitung des %(ha:Haager
Übereinkommens).

#Wzn: Weitere Gefahren drohen durch zahlreiche derzeit laufende Bestrebungen
zur Ausweitung des Patentwesens, z.B. %(sp:Vertrag für Materielles
Patentrecht), %(bm:Gesetz zur Förderung des Patentwesens an den
Hochschulen).

#DwA: Diejenigen Unternehmen, die keine eigenständigen Softwareprodukte
entwickeln und sich stattdessen auf das Anmelden von Patenten
konzentrieren, werden begünstigt.

#Dhz: Die anderen haben im wesentlichen hohe Kosten zu tragen und mit
Großfirmen zu konkurrieren, die dank ihres übermächtigen
Patentportfolios mit zahlreichen breiten Grundlagenpatenten sich
regelmäßig Zugang zu allem verschaffen können, was das Unternehmen
entwickelt (und unter hohem Aufwand offengelegt) hat.

#PWm: Patente fördern die Orientierung von Unternehmen auf den Kapitalmarkt
hin.  Das ist gut für einige Spezialisten des Kapitalmarkts, für
Technologietransferbüros und Patentanwaltskanzleien, aber nicht für
typische europäische Softwarefirmen, die sich um ein Projekt herum
gruppieren und mit einfach mit einem guten Entwickler-Team gute Sachen
machen und viel Geld verdienen, vgl. Netpresenter, Phaidros etc

#Nei3: Nein.

#Mts: Marktvorsprung

#Sml: Systemkomplexität

#nhp: natürliche Oligopoltendenz

#Ube2: Urheberrecht

#Beh: Betriebsgeheimnis

#kme: komplementäre Märkte

#atd: alternative Schutzrechte und Förderungsmechanismen

#Eir: öffentliche Forschung

#Atn: Auch in herkömmlichen Industrien rangiert das Patentwesen als Mittel
zur Förderung der Innovation einigen Studien zufolge nur an hinterer
Stelle.

#Ede: Eine solche Klarstellung würde die bestehenden Gesetzesregeln nicht
ändern.

#SWt2: Sie würde aber die 3% der EPA-Patente, die in den letzten Jahren unter
Missachtung dieser Gesetzesregeln erteilt wurden, zahnlos machen.

#Die: Diese Patente könnten dann im Falle einer Verletzungsklage von dem
Beklagten im Gegenzug nichtig geklagt werden.

#DWe: Die praktischen Auswirkung wäre eher gering, da

#dew: diese Patente schon bisher kaum eingeklagt wurden, nicht zuletzt wegen
Zweifeln an ihrer Rechtsbeständigkeit

#ndN: nach Greg Aharonians Schätzungen 80% dieser Patente schon wegen
fehlender Neuheit nichtig geklagt werden können

#dqW: die meisten dieser Patente nur von außereuropäischen (US,JP)
Großunternehmen für defensive Zwecke gesammelt wurden, so dass eine
Klarstellung lediglich einer Abrüstungsvereinbarung gleichkäme.

#Mst: Report from the Federal Parliament's Press Service

#DtW: The usefulness of classical patenting in the area of the New Media is
rather dubious

#Mst3: A short summary of the contributions of the invited experts

#FDs: FFII discussion

#cWn: c't on expert hearing

#DWW: the German IT weekly %{CT} reported in detail about the expert hearing
but was, according to criticism uttered here, not careful to avoid
parrotting confusing wording such as %(aa:computer-implemented
invention).

#Bki: bio patent hearing in the federal parliament

#Hrd: Here the bias was completely different: a hymn to patents from the
congregation of patent experts

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpbtag016 ;
# txtlang: en ;
# multlin: t ;
# End: ;

