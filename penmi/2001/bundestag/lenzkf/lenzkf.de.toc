\contentsline {section}{\numberline {1}Stellungnahme zu Fragen}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Gibt es zwingende rechtliche Gr\"{u}nde (Verfassung, Internationale Vertr\"{a}ge), die eine Neuregelung des Patentwesens auf Software erforderlich machen?}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Sind Sie der Meinung, dass eine Ausdehnung des Patentschutzes f\"{u}r Software Innovationen in dieser Branche gef\"{a}hrden w\"{u}rden? Wie beurteilen Sie in diesem Zusammenhang die MIT-Studie ``Sequential innovation, patents and imitation'' von James Bessen und Eric Maskin (1999)?}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Wie wirkt sich Ihrer Ansicht nach eine Klarstellung auf EU-Ebene dahingehend, dass Software und gedankliche Konzepte nicht patentierbar sind, auf bereits bestehende Patente in diesem Bereich aus?}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Weitere Lekt\"{u}re}{3}{section.2}
