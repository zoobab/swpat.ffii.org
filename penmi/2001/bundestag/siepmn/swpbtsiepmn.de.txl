<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Jürgen Siepmann: Stellungnahme des Linux-Verbandes zum
Bundestags-Expertengespräch

#descr: RA Dipl.-Phys. Jürgen Siepmann ist Justitiar des Linux-Verbandes.  Er
beantwortet alle vom Bundestag zum Expertengespräch vom 2001-06-21
gestellten Fragen auf Grundlage fundierter Recherchen.  Er betont,
dass es weder volkswirtschaftliche noch juristische Gründe für die
nachträgliche Legalisierung von Softwarepatenten gibt, wie das
Europäische Patentamt (EPA) sie in den letzten Jahren gesetzeswidrig
erteilt hat.  Im Gegenteil, wenn Europa im Angesicht des
TRIPS-Vertrages seine Freiheit beibehalten will, den Bereich des
Patentierbaren sinnvoll zu begrenzen, muss es zu einem klaren Begriff
der technischen Erfindung zurückfinden, wie die deutschen Gerichte ihn
in den 70er und 80er Jahren erarbeitet haben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpbtsiepmn ;
# txtlang: de ;
# multlin: t ;
# End: ;

