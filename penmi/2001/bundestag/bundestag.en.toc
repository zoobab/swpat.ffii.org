\contentsline {section}{\numberline {1}General Data}{4}{section.1}
\contentsline {section}{\numberline {2}session protocol}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Introduction by the Moderator J\"{o}rg Tauss}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Probst: Economists Skeptical about Effects of Software Patents}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Bremer: Copyright does not adequately reflect Technicity of Software}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Schiuma: Copyright protection insufficient, TRIPs requires change of law}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Siepmann: Copyright achieves for Software what Patents achieve for Pharma}{11}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Kiesewetter-K\"{o}binger: Incompatibility of Patent Concepts with Software Concepts}{14}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Bouillon: Patents endanger innovative SMEs in the field of proprietary software}{15}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Henckel: Open Source Software unduly threatened}{17}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Lutterbeck: Economists against Software Patents, lawyers in favor}{18}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}MdB Tauss: Themes of Today's Discussion}{19}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}MdB Mayer: What do pro-swpat studies say about the effects on innovation}{20}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}MdB Griefahn: Isn't Resistance to US Trends Futile?}{21}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}MdB Otto: Questions about Differences between US and EU, Copyright and Patents}{22}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}MdB Neumann: Against Software Patents or against all Patents?}{22}{subsection.2.14}
\contentsline {subsection}{\numberline {2.15}MdB Kelber: Conflict between Standardisation and Patentability? Does Bremer represent consensus of Bitkom members?}{24}{subsection.2.15}
\contentsline {subsection}{\numberline {2.16}Schiuma: Self-Regulation through Patent Pools Keeps Standards Open}{24}{subsection.2.16}
\contentsline {subsection}{\numberline {2.17}Lutterbeck: Patentability can't be limited, Solution lies in Source Code Privilege}{26}{subsection.2.17}
\contentsline {subsection}{\numberline {2.18}Probst: Numerous Economic Studies, nothing in support of Software Patentability}{28}{subsection.2.18}
\contentsline {subsection}{\numberline {2.19}Bouillon: Copyright is Adequate, because the effort resides in the complex tissue rather than in single ideas}{30}{subsection.2.19}
\contentsline {subsection}{\numberline {2.20}Bremer: The Economic Studies are Inconclusive, the Opensource Camp doesn't understand the Law}{31}{subsection.2.20}
\contentsline {subsection}{\numberline {2.21}Siepmann: Most Legal Arguments work against Software Patentability}{32}{subsection.2.21}
\contentsline {subsection}{\numberline {2.22}Schiuma: No need to reduce patent term, because well-written claims last much longer than 1 product cycle}{34}{subsection.2.22}
\contentsline {subsection}{\numberline {2.23}Tauss: Does the Volatility of Caselaw indicate a need for Legislation?}{35}{subsection.2.23}
\contentsline {subsection}{\numberline {2.24}MdB Otto: Could an OpenSource Exception be the Solution?}{36}{subsection.2.24}
\contentsline {subsection}{\numberline {2.25}MdB Griefahn: What is ``trivial''? Where are the limits?}{37}{subsection.2.25}
\contentsline {subsection}{\numberline {2.26}Siepmann: Return to EPC and Earlier Caselaw possible without difficulties}{37}{subsection.2.26}
\contentsline {subsection}{\numberline {2.27}Schiuma: Today's Caselaw is Clear, Legislation of 1973 is outdated and misleading}{38}{subsection.2.27}
\contentsline {subsection}{\numberline {2.28}Lutterbeck: Legal discussions pointless, better think about source code privileges and expiration terms}{40}{subsection.2.28}
\contentsline {subsection}{\numberline {2.29}Bremer: Status Quo = new caselaw + TRIPs, law must be adapted}{42}{subsection.2.29}
\contentsline {subsection}{\numberline {2.30}Bouillon: Software Patents principally unjustified, fiddling with expiry terms and competition law is not a lasting solution}{42}{subsection.2.30}
\contentsline {subsection}{\numberline {2.31}Kiesewetter-K\"{o}binger: Difficulties in Novelty Examination of Software Patents}{43}{subsection.2.31}
\contentsline {subsection}{\numberline {2.32}Tauss: Effects on Productivity, IT Security etc}{44}{subsection.2.32}
\contentsline {subsection}{\numberline {2.33}Lutterbeck: IT security demands freedom for opensource software}{45}{subsection.2.33}
\contentsline {subsection}{\numberline {2.34}Henckel: Software Patents reinforce Dependence on Insecure Proprietary Solutions}{45}{subsection.2.34}
\contentsline {subsection}{\numberline {2.35}MdB Mayer: Are Small Groups of Developpers Hampered?}{46}{subsection.2.35}
\contentsline {subsection}{\numberline {2.36}Kieswetter-K\"{o}binger: Patent Threat Demotivates Developpers and Entrepreneurs}{47}{subsection.2.36}
\contentsline {subsection}{\numberline {2.37}Siepmann: Danger of gradual suffocation of Free Software, Source Code Privilege not enough}{47}{subsection.2.37}
\contentsline {subsection}{\numberline {2.38}MdB Mayer: Shouldn't Patented Software be made open in principle?}{48}{subsection.2.38}
\contentsline {subsection}{\numberline {2.39}Schiuma: Patent Descriptions better suited for disclosure of software functionalities than source code}{49}{subsection.2.39}
\contentsline {subsection}{\numberline {2.40}Lutterbeck: Legislation should reflect leading position of Europe and Germany in the Area of Opensource Software}{50}{subsection.2.40}
\contentsline {subsection}{\numberline {2.41}Henckel: Software Patents undermine Copyright, expropriate Software Authors}{51}{subsection.2.41}
\contentsline {subsection}{\numberline {2.42}Bouillon: We, the Developpers, feel Expropriated}{51}{subsection.2.42}
\contentsline {subsection}{\numberline {2.43}Kiesewetter-K\"{o}binger: Studying Source Code impossible but yet necessary for serious patent examination}{51}{subsection.2.43}
\contentsline {subsection}{\numberline {2.44}Siepmann: Source code better understandable than patent language}{52}{subsection.2.44}
\contentsline {subsection}{\numberline {2.45}Bremer: Existing inventor privileges must be upheld, Open Source Movement will survive}{53}{subsection.2.45}
\contentsline {subsection}{\numberline {2.46}Probst: Overall Economic Interest must be the Decisive Criterion}{53}{subsection.2.46}
\contentsline {subsection}{\numberline {2.47}Moderator's Concluding Remarks}{53}{subsection.2.47}
\contentsline {section}{\numberline {3}Consultation Questions and possible answers}{54}{section.3}
\contentsline {subsection}{\numberline {3.1}What is the rationale for patent protection in general?}{54}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}What protection rights exist for software and how do they work?}{54}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}How is software different from other inventions?}{55}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}What are software patents?}{55}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}How is the inventive height (non-obviousness) of software patents to be assessed?}{55}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Are software patents different from patents on business ideas, algorithms and file formats? If yes, in what way?}{56}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Where does the protection of software in Germany differ from that in other countries in and outside Europe?}{56}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}According to which criteria do the EPO, the Federal Court (BGH) and the Federal Patent Court (BPatG) judge the patentability of software? Are these criteria and rules unambiguous and which formulas have proven to be useful?}{57}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}How many software patents exist in Germany, Europe and worldwide?}{57}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}In how many of the 30000 software patents granted so far by the EPO is the claimed contribution to innovation so significant that it could considered a good deal for society to grant a 20 year monopoly on them?}{58}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Are there legal constraints (constitution, international treaties) that necessitate an extension of the patent system to software?}{58}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Art 14 GG: Eigentumsgarantie des Grundgesetzes}{58}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Art 27 TRIPS: Klausel gegen unsystematische Patentierbarkeitsausnahmen in einem Freihandelsvertrag}{59}{subsubsection.3.11.2}
\contentsline {subsection}{\numberline {3.12}How do you find the idea of introducing a special protection right (sui generis right) for software?}{61}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Where could copyright offer insufficient protection against imitation? What could a ``specially tailored software protection right'' look like?}{61}{subsection.3.13}
\contentsline {subsection}{\numberline {3.14}Does it make sense to distnguish patentable goods from non patentable ones?}{62}{subsection.3.14}
\contentsline {subsection}{\numberline {3.15}If yes, according to which criteria should the distinction between these two kinds of goods (kinds of inventions) be made?}{62}{subsection.3.15}
\contentsline {subsection}{\numberline {3.16}Which macro- and micro-economic effects would a largely unlimited patentability of so-called software products have?}{63}{subsection.3.16}
\contentsline {subsection}{\numberline {3.17}Which are the methods of work and economic foundations of the open source movement?}{63}{subsection.3.17}
\contentsline {subsection}{\numberline {3.18}What are the effects of extended patentability of software on the framework of software development under so-called open-source licenses?}{63}{subsection.3.18}
\contentsline {subsection}{\numberline {3.19}Would an extension of patent protection for software hamper the open source movement? If yes, how could these disadvantages be removed?}{63}{subsection.3.19}
\contentsline {subsection}{\numberline {3.20}Which effects would the extension of patent protection to software have on the development of small and medium enterprises?}{64}{subsection.3.20}
\contentsline {subsection}{\numberline {3.21}Is patent law a suitable tool for promoting innovation and progress in the field of software?}{65}{subsection.3.21}
\contentsline {subsection}{\numberline {3.22}Are there more effective alternatives and, if yes, which? }{65}{subsection.3.22}
\contentsline {subsection}{\numberline {3.23}If it was now to be clarified at the EU level that software and abstract concepts are not patentable, how would this affect already existing patents in this area?}{65}{subsection.3.23}
\contentsline {section}{\numberline {4}Responses so far}{65}{section.4}
\contentsline {section}{\numberline {5}Further Reading}{69}{section.5}
