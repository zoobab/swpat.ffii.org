\contentsline {section}{\numberline {1}Allgemeine Angaben}{4}{section.1}
\contentsline {section}{\numberline {2}Sitzungsprotokoll}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Einleitungsworte des Gespr\"{a}chsleiters MdB J\"{o}rg Tauss}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Probst: Innovationsf\"{o}rdernde Wirkungen von Swpat nicht zu erkennen}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Bremer: Urheberrecht wird der Technizit\"{a}t der Software nicht gerecht}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Schiuma: Urheberrecht unzureichend, TRIPS erfordert Gesetzes\"{a}nderung}{11}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Siepmann: Urheberrecht leistet f\"{u}r Software das, was Patente f\"{u}r Pharma leisten}{11}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Kiesewetter-K\"{o}binger: Ungereimtheiten bei der Patentpr\"{u}fung von Datenverarbeitungsprogrammen}{14}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Bouillon: Patente gef\"{a}hrden innovative KMU im Bereich der propriet\"{a}ren Software}{15}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Henckel: Quelloffene Software besonders gef\"{a}hrdet}{17}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Lutterbeck: \"{O}konomen gegen Swpat, viele Juristen daf\"{u}r}{18}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}MdB Tauss: Themenkomplexe der heutigen Diskussion}{20}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}MdB Mayer: Was sagen die swpat-freundlichen Studien zur Wirkung auf Innovation?}{20}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}MdB Griefahn: Ist Widerstand gegen US-Trends nicht zwecklos?}{21}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}MdB Otto: Unterschiede US-EU, Urheberrecht-Patentrecht}{22}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}MdB Neumann: Gegen Swpat oder gegen Patente allgemein?}{23}{subsection.2.14}
\contentsline {subsection}{\numberline {2.15}MdB Kelber: Konflikt zwischen Standardisierung und Patentierbarkeit? Konsens der Bitkom-Firmen?}{24}{subsection.2.15}
\contentsline {subsection}{\numberline {2.16}Schiuma: Patentpools gew\"{a}hrleisten, dass Standardisierung m\"{o}glich bleibt}{24}{subsection.2.16}
\contentsline {subsection}{\numberline {2.17}Lutterbeck: Begrenzung der Patentierbarkeit schwierig, Quelltextprivileg als L\"{o}sung}{26}{subsection.2.17}
\contentsline {subsection}{\numberline {2.18}Probst: Zahlreiche Studien, nichts spricht f\"{u}r Swpat}{28}{subsection.2.18}
\contentsline {subsection}{\numberline {2.19}Bouillon: Urheberrecht angemessen, denn die Leistung liegt mehr im Werk als in der Einzelidee}{30}{subsection.2.19}
\contentsline {subsection}{\numberline {2.20}Bremer: Volkswirtschaftliche Studien nicht aussagekr\"{a}ftig, OpenSource-Lager versteht zu wenig von Recht}{31}{subsection.2.20}
\contentsline {subsection}{\numberline {2.21}Siepmann: Auch aus rechtlicher Sicht spricht wenig f\"{u}r Softwarepatente und viel dagegen}{32}{subsection.2.21}
\contentsline {subsection}{\numberline {2.22}Schiuma: Kein Bedarf nach geringerer Patentlaufzeit, denn gut formulierte Patente wirken viel l\"{a}nger als 1 Produktzyklus}{35}{subsection.2.22}
\contentsline {subsection}{\numberline {2.23}Tauss: Deutet die Unstetigkeit der Rechtsprechung auf Gesetzgebungsbedarf?}{36}{subsection.2.23}
\contentsline {subsection}{\numberline {2.24}MdB Otto: Freistellung von OpenSource-Software als L\"{o}sung?}{36}{subsection.2.24}
\contentsline {subsection}{\numberline {2.25}MdB Griefahn: Was ist trivial? Wo sind die Grenzen?}{37}{subsection.2.25}
\contentsline {subsection}{\numberline {2.26}Siepmann: R\"{u}ckkehr zu EP\"{U} und fr\"{u}herer BGH-Rechtsprechung ohne weiteres m\"{o}glich}{38}{subsection.2.26}
\contentsline {subsection}{\numberline {2.27}Schiuma: Heutige Rechtsprechung klar und sinnvoll, Gesetzgebung von 1973 veraltet und irref\"{u}hrend}{38}{subsection.2.27}
\contentsline {subsection}{\numberline {2.28}Lutterbeck: Juristische Diskussionen nutzlos, lieber an Quelltextprivileg und Verj\"{a}hrungsfristen (?) denken}{41}{subsection.2.28}
\contentsline {subsection}{\numberline {2.29}Bremer: Status Quo = neuere Rechtsprechung + TRIPS, Gesetz muss angepasst werden}{42}{subsection.2.29}
\contentsline {subsection}{\numberline {2.30}Bouillon: Swpat grunds\"{a}tzlich nicht sinnvoll, Laufzeitverk\"{u}rzung und Wettbewerbsrechts\"{a}nderung etc nur als Notbehelfe vorgeschlagen}{43}{subsection.2.30}
\contentsline {subsection}{\numberline {2.31}Kiesewetter-K\"{o}binger: Schwierigkeiten der Neuheitspr\"{u}fung bei Softwarepatenten}{44}{subsection.2.31}
\contentsline {subsection}{\numberline {2.32}Tauss: Auswirkungen auf Produktivit\"{a}t, IT-Sicherheit etc}{45}{subsection.2.32}
\contentsline {subsection}{\numberline {2.33}Lutterbeck: IT-Sicherheit erfordert Freiraum f\"{u}r OpenSource}{45}{subsection.2.33}
\contentsline {subsection}{\numberline {2.34}Henckel: Swpat f\"{o}rdern Abh\"{a}ngigkeit von Verk\"{a}ufern intransparenter Informationssysteme}{46}{subsection.2.34}
\contentsline {subsection}{\numberline {2.35}MdB Mayer: Werden kleine Entwicklergruppen behindert?}{47}{subsection.2.35}
\contentsline {subsection}{\numberline {2.36}Kieswetter-K\"{o}binger: Patentdrohung demotiviert Entwickler und Unternehmensgr\"{u}nder}{47}{subsection.2.36}
\contentsline {subsection}{\numberline {2.37}Siepmann: Gefahr schleichender Abw\"{u}rgung der freien Software, Quelltextprivileg gen\"{u}gt nicht}{48}{subsection.2.37}
\contentsline {subsection}{\numberline {2.38}MdB Mayer: M\"{u}sste patentierte Software nicht grunds\"{a}tzlich offengelegt werden?}{49}{subsection.2.38}
\contentsline {subsection}{\numberline {2.39}Schiuma: Patentbeschreibungen zur Offenbarung von Funktionalit\"{a}ten geeigneter als Programm-Quelltexte}{49}{subsection.2.39}
\contentsline {subsection}{\numberline {2.40}Lutterbeck: Gesetzgebung sollte weltf\"{u}hrende Stellung Europas und Deutschlands im Bereich der Quelloffenen Software wiederspiegeln}{50}{subsection.2.40}
\contentsline {subsection}{\numberline {2.41}Henckel: Swpat untergraben Urheberrecht, enteignen Software-Autoren}{51}{subsection.2.41}
\contentsline {subsection}{\numberline {2.42}Bouillon: Wir Entwickler f\"{u}hlen uns enteignet}{52}{subsection.2.42}
\contentsline {subsection}{\numberline {2.43}Kiesewetter-K\"{o}binger: Quelltext einerseits f\"{u}r Pr\"{u}fer nicht bew\"{a}ltigbar, andererseits f\"{u}r seri\"{o}se Patentpr\"{u}fung unabdingbar}{52}{subsection.2.43}
\contentsline {subsection}{\numberline {2.44}Siepmann: Quelltext verst\"{a}ndlicher als Patentsprache}{53}{subsection.2.44}
\contentsline {subsection}{\numberline {2.45}Bremer: Existierende Erfinderprivilegien m\"{u}ssen aufrechterhalten werden und OpenSource-Bewegung wird es \"{u}berleben}{53}{subsection.2.45}
\contentsline {subsection}{\numberline {2.46}Probst: Gesamtwirtschaftliches Interesse sollte entscheidend sein}{54}{subsection.2.46}
\contentsline {subsection}{\numberline {2.47}Schlussworte des Gespr\"{a}chsleiters}{54}{subsection.2.47}
\contentsline {section}{\numberline {3}Die Konsultationsfragen und m\"{o}gliche Antworten}{54}{section.3}
\contentsline {subsection}{\numberline {3.1}Wie wird Patentschutz im Allgemeinen begr\"{u}ndet?}{54}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Welche Schutzrechte gibt es f\"{u}r Software und welche Wirkungen haben sie?}{55}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Was unterscheidet Software von anderen Erfindungen?}{55}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Was sind Softwarepatente?}{55}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Wie sind Softwarepatente hinsichtlich der Erfindungsh\"{o}he zu beurteilen?}{56}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Unterscheiden sich Softwarepatente von Patenten f\"{u}r Gesch\"{a}ftsideen, Algorithmen und Dateiformaten und wenn ja, wie?}{56}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Welche Unterschiede gibt es beim Schutz von Software in Deutschland im Vergleich zu anderen EU-L\"{a}ndern und im internationalen Vergleich?}{57}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Nach welchen Kriterien beurteilen EPA, BGH und BPatG heute die Patentierbarkeit von Software? Sind diese Kriterien und Regeln eindeutig und welche Formulierung erweist sich bisher am praktikabelsten?}{57}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Wie viele Patente f\"{u}r Software gibt es in Deutschland, Europa und weltweit?}{58}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Bei wie viel Prozent der bislang vom EPA gew\"{a}hrten 30000 Softwarepatente ist die beanspruchte Innovationsleistung so bedeutend, dass es sich f\"{u}r die Gesellschaft ann\"{a}hernd lohnen k\"{o}nnte, darauf ein 20j\"{a}hriges Monopol zu vergeben?}{58}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Gibt es zwingende rechtliche Gr\"{u}nde (Verfassung, Internationale Vertr\"{a}ge), die eine Ausdehnung des Patentwesens auf Software erforderlich machen?}{59}{subsection.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Art 14 GG: Eigentumsgarantie des Grundgesetzes}{59}{subsubsection.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Art 27 TRIPS: Klausel gegen unsystematische Patentierbarkeitsausnahmen in einem Freihandelsvertrag}{60}{subsubsection.3.11.2}
\contentsline {subsection}{\numberline {3.12}Wie beurteilen Sie die Einf\"{u}hrung eines eigenen Schutzrechtes f\"{u}r Software?}{61}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Wo k\"{o}nnte es beim Urheberrecht Unzul\"{a}nglichkeiten hinsichtlich des hinreichenden Schutzes von Software vor Nachahmung geben? Wie k\"{o}nnte ein ``ma{\ss }geschneidertes Software-Schutzrecht'' aussehen?}{61}{subsection.3.13}
\contentsline {subsection}{\numberline {3.14}Ist eine Differenzierung patentierbarer und nichtpatentierbarer G\"{u}ter sinnvoll?}{62}{subsection.3.14}
\contentsline {subsection}{\numberline {3.15}Wenn ja, nach welchen Kriterien sollte Ihrer Ansicht nach zwischen beiden G\"{u}terarten (Erfindungsarten) unterschieden werden?}{63}{subsection.3.15}
\contentsline {subsection}{\numberline {3.16}Welche volkswirtschaftlichen und betriebswirtschaftlichen Effekte h\"{a}tte ihrer Meinung nach eine weitestgehend uneingeschr\"{a}nkte Patentierbarkeit von sog. Softwareprodukten? Worauf beruhen diese Annahmen?}{63}{subsection.3.16}
\contentsline {subsection}{\numberline {3.17}Welches sind die Arbeitsmethoden und wirtschaftlichen Grundlagen der OpenSource-Bewegung?}{63}{subsection.3.17}
\contentsline {subsection}{\numberline {3.18}Welche Auswirkungen erweiterter Patentierbarkeit von Software erwarten Sie auf die Rahmenbedingungen der Softwareentwicklung unter sog. OpenSource-Lizenzen?}{63}{subsection.3.18}
\contentsline {subsection}{\numberline {3.19}W\"{u}rde eine Ausdehnung des Patentschutzes f\"{u}r Software die OpenSource-Bewegung behindern? Wenn ja, wie k\"{o}nnten diese Nachteile beseitigt werden?}{64}{subsection.3.19}
\contentsline {subsection}{\numberline {3.20}Welche Auswirkungen h\"{a}tte die Ausdehnung des Patentschutzes f\"{u}r Software auf die Entwicklung von kleinen und mittelst\"{a}ndischen Unternehmen?}{65}{subsection.3.20}
\contentsline {subsection}{\numberline {3.21}Ist das Patentrecht aus Ihrer Sicht das geeignete Werkzeug, um im Softwarebereich Innovation und Fortschritt zu f\"{o}rdern?}{65}{subsection.3.21}
\contentsline {subsection}{\numberline {3.22}Sind effektivere Alternativen denkbar, und wenn ja, welche?}{65}{subsection.3.22}
\contentsline {subsection}{\numberline {3.23}Wie wirkt sich Ihrer Ansicht nach eine Klarstellung auf EU-Ebene dahingehend, dass Software und gedankliche Konzepte nicht patentierbar sind, auf bereits bestehende Patente in diesem Bereich aus?}{65}{subsection.3.23}
\contentsline {section}{\numberline {4}Bisherige Antworten}{66}{section.4}
\contentsline {section}{\numberline {5}Weitere Lekt\"{u}re}{69}{section.5}
