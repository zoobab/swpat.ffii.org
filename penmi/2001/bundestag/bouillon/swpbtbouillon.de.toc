\contentsline {section}{\numberline {1}Zwingende rechtliche Gr\"{u}nde f\"{u}r Neuregelung des Patentwesens f\"{u}r Software}{2}{section.1}
\contentsline {section}{\numberline {2}Einf\"{u}hrung eines eigenen Schutzrechtes f\"{u}r Software}{2}{section.2}
\contentsline {section}{\numberline {3}Wie k\"{o}nnte ein ``ma{\ss }geschneidertes Softwareschutzrecht'' aussehen?}{2}{section.3}
\contentsline {section}{\numberline {4}Einf\"{u}hrung von Sondervorschriften f\"{u}r Software im Patentgesetz}{3}{section.4}
\contentsline {section}{\numberline {5}Auswirkungen}{3}{section.5}
\contentsline {section}{\numberline {6}Auswirkung einer Ausdehnung des Patentschutzes f\"{u}r Software auf die Entwicklung von KMU}{3}{section.6}
\contentsline {section}{\numberline {7}Gef\"{a}hrdung der Innovation in der Softwarebranche?}{4}{section.7}
