\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}Renseignements Pratiques}{4}{section.0.1}
\contentsline {section}{\numberline {2}Protocole de session}{5}{section.0.2}
\contentsline {subsection}{\numberline {2.1}Einleitungsworte des Gespr\"achsleiters MdB J\"org Tauss}{5}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Probst\@DP innovationsf\"ordernde Wirkungen von Swpat nicht zu erkennen}{8}{subsection.0.2.2}
\contentsline {subsection}{\numberline {2.3}Bremer\@DP EPA-Rechtsprechung sollte im Gesetz etabliert werden, KMU profitieren von Softwarepatenten}{9}{subsection.0.2.3}
\contentsline {subsection}{\numberline {2.4}Schiuma\@DP Urheberrecht unzureichend, KMU brauchen Swpat, TRIPS erfordert Gesetzes\"anderung}{11}{subsection.0.2.4}
\contentsline {subsection}{\numberline {2.5}Siepmann\@DP Urheberrecht leistet f\"ur Software das, was Patente f\"ur Pharma leisten}{11}{subsection.0.2.5}
\contentsline {subsection}{\numberline {2.6}Kiesewetter-K\"obinger\@DP Ungereimtheiten bei der Patentpr\"ufung von Datenverarbeitungsprogrammen}{14}{subsection.0.2.6}
\contentsline {subsection}{\numberline {2.7}Bouillon\@DP Patente gef\"ahrden innovative KMU im Bereich der propriet\"aren Software}{15}{subsection.0.2.7}
\contentsline {subsection}{\numberline {2.8}Henckel\@DP Quelloffene Software besonders gef\"ahrdet}{17}{subsection.0.2.8}
\contentsline {subsection}{\numberline {2.9}Lutterbeck\@DP \"Okonomen gegen Swpat, viele Juristen daf\"ur}{18}{subsection.0.2.9}
\contentsline {subsection}{\numberline {2.10}MdB Tauss\@DP Themenkomplexe der heutigen Diskussion}{19}{subsection.0.2.10}
\contentsline {subsection}{\numberline {2.11}MdB Mayer\@DP Was sagen die swpat-freundlichen Studien zur Wirkung auf Innovation\@PI }{20}{subsection.0.2.11}
\contentsline {subsection}{\numberline {2.12}MdB Griefahn\@DP Ist Widerstand gegen US-Trends nicht zwecklos\@PI }{21}{subsection.0.2.12}
\contentsline {subsection}{\numberline {2.13}MdB Otto\@DP Fragen zum Unterschied US-DE, Urheberrecht-Patentrecht}{22}{subsection.0.2.13}
\contentsline {subsection}{\numberline {2.14}MdB Neumann\@DP Gegen Swpat oder gegen Patente allgemein\@PI }{22}{subsection.0.2.14}
\contentsline {subsection}{\numberline {2.15}MdB Kelber\@DP Erfordert Bedeutung der Standardisierung nicht \"Anderungen im Patentrecht\@PI Vertritt Bremer wirklich Meinung der Bitkom-Firmen\@PI }{23}{subsection.0.2.15}
\contentsline {subsection}{\numberline {2.16}Schiuma\@DP Patentpools gew\"ahrleisten, dass Standardisierung m\"oglich bleibt}{24}{subsection.0.2.16}
\contentsline {subsection}{\numberline {2.17}Lutterbeck\@DP Begrenzung der Patentierbarkeit schwierig, Quelltextprivileg als L\"osung}{26}{subsection.0.2.17}
\contentsline {subsection}{\numberline {2.18}Probst\@DP Zahlreiche Studien, nichts spricht f\"ur Swpat}{28}{subsection.0.2.18}
\contentsline {subsection}{\numberline {2.19}Bouillon\@DP Urheberrecht angemessen, denn die Leistung liegt mehr im Werk als in der Einzelidee}{30}{subsection.0.2.19}
\contentsline {subsection}{\numberline {2.20}Bremer\@DP Volkswirtschaftliche Studien nicht aussagekr\"aftig, OpenSource-Lager muss sich an Rechtslage anpassen, Schwierigkeiten gibt es sowieso auch beim Urheberrecht}{31}{subsection.0.2.20}
\contentsline {subsection}{\numberline {2.21}Siepmann\@DP Auch aus rechtlicher Sicht spricht wenig f\"ur Softwarepatente und viel dagegen}{32}{subsection.0.2.21}
\contentsline {subsection}{\numberline {2.22}Schiuma\@DP Kein Bedarf nach geringerer Patentlaufzeit, denn gut formulierte Patente wirken viel l\"anger als 1 Produktzyklus}{34}{subsection.0.2.22}
\contentsline {subsection}{\numberline {2.23}Tauss\@DP Deutet die Unstetigkeit der Rechtsprechung auf Gesetzgebungsbedarf\@PI }{35}{subsection.0.2.23}
\contentsline {subsection}{\numberline {2.24}MdB Otto\@DP Freistellung von OpenSource-Software als L\"osung\@PI }{36}{subsection.0.2.24}
\contentsline {subsection}{\numberline {2.25}MdB Griefahn\@DP Was ist trivial\@PI Wo sind die Grenzen\@PI }{36}{subsection.0.2.25}
\contentsline {subsection}{\numberline {2.26}Siepmann\@DP R\"uckkehr zu EP\"U und fr\"uherer BGH-Rechtsprechung ohne weiteres m\"oglich}{37}{subsection.0.2.26}
\contentsline {subsection}{\numberline {2.27}Schiuma\@DP Heutige Rechtsprechung klar und sinnvoll, Gesetzgebung von 1973 veraltet und irref\"uhrend}{37}{subsection.0.2.27}
\contentsline {subsection}{\numberline {2.28}Lutterbeck\@DP Juristische Diskussionen nutzlos, lieber an Quelltextprivileg und Verj\"ahrungsfristen denken}{37}{subsection.0.2.28}
\contentsline {subsection}{\numberline {2.29}Bremer\@DP Status Quo = neuere Rechtsprechung + TRIPS, Gesetz muss angepasst werden}{39}{subsection.0.2.29}
\contentsline {subsection}{\numberline {2.30}Bouillon\@DP Swpat grunds\"atzlich nicht sinnvoll, Laufzeitverk\"urzung und Wettbewerbsrechts\"anderung etc nur als Notbehelfe vorgeschlagen}{39}{subsection.0.2.30}
\contentsline {subsection}{\numberline {2.31}Kiesewetter-K\"obinger\@DP Schwierigkeiten der Neuheitspr\"ufung bei Softwarepatenten}{40}{subsection.0.2.31}
\contentsline {subsection}{\numberline {2.32}Tauss\@DP Auswirkungen auf Produktivit\"at, IT-Sicherheit etc}{41}{subsection.0.2.32}
\contentsline {subsection}{\numberline {2.33}Lutterbeck\@DP IT-Sicherheit erfordert Freiraum f\"ur OpenSource}{42}{subsection.0.2.33}
\contentsline {subsection}{\numberline {2.34}Henckel\@DP Swpat f\"ordern Abh\"angigkeit von Verk\"aufern intransparenter Informationssysteme}{42}{subsection.0.2.34}
\contentsline {subsection}{\numberline {2.35}MdB Mayer\@DP Werden kleine Entwicklergruppen behindert\@PI }{43}{subsection.0.2.35}
\contentsline {subsection}{\numberline {2.36}Kieswetter-K\"obinger\@DP Patentdrohung demotiviert Entwickler und Unternehmensgr\"under}{44}{subsection.0.2.36}
\contentsline {subsection}{\numberline {2.37}Siepmann\@DP Gefahr schleichender Abw\"urgung der freien Software, Quelltextprivileg gen\"ugt nicht}{44}{subsection.0.2.37}
\contentsline {subsection}{\numberline {2.38}MdB Mayer\@DP M\"usste patentierte Software nicht grunds\"atzlich offengelegt werden\@PI }{45}{subsection.0.2.38}
\contentsline {subsection}{\numberline {2.39}Schiuma\@DP Patentbeschreibungen zur Offenbarung von Funktionalit\"aten geeigneter als Programm-Quelltexte}{46}{subsection.0.2.39}
\contentsline {subsection}{\numberline {2.40}Lutterbeck\@DP Gesetzgebung sollte weltf\"uhrende Stellung Europas und Deutschlands im Bereich der Quelloffenen Software wiederspiegeln}{47}{subsection.0.2.40}
\contentsline {subsection}{\numberline {2.41}Henckel\@DP Swpat untergraben Urheberrecht, enteignen Software-Autoren}{48}{subsection.0.2.41}
\contentsline {subsection}{\numberline {2.42}Bouillon\@DP Wir Entwickler f\"uhlen uns enteignet}{48}{subsection.0.2.42}
\contentsline {subsection}{\numberline {2.43}Kiesewetter-K\"obinger\@DP Quelltext einerseits f\"ur Pr\"ufer nicht bew\"altigbar, andererseits f\"ur seri\"ose Patentpr\"ufung unabdingbar}{48}{subsection.0.2.43}
\contentsline {subsection}{\numberline {2.44}Siepmann\@DP Quelltext verst\"andlicher als Patentsprache, Beispiel IDEA}{49}{subsection.0.2.44}
\contentsline {subsection}{\numberline {2.45}Bremer\@DP Existierende Rechte m\"ussen aufrechterhalten werden und OpenSource-Bewegung wird es \"uberleben}{49}{subsection.0.2.45}
\contentsline {subsection}{\numberline {2.46}Probst\@DP Gesamtwirtschaftliches Interesse sollte entscheidend sein}{50}{subsection.0.2.46}
\contentsline {subsection}{\numberline {2.47}Schlussworte des Gespr\"achsleiters}{50}{subsection.0.2.47}
\contentsline {section}{\numberline {3}Questions de Consultation et reponses possibles}{51}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Pour quoi accorde-t-on des brevets en g\'{e}n\'{e}ral\@PI }{51}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Quelle protection juridique existe pour le logiciel et comment fonctionne-t-elle \@PI }{51}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}An quoi le logiciel est-il diff\'{e}rent des autres inventions\@PI }{51}{subsection.0.3.3}
\contentsline {subsection}{\numberline {3.4}Qu'est-ce qu'un brevet logiciel\@PI }{52}{subsection.0.3.4}
\contentsline {subsection}{\numberline {3.5}Comment doit-on juger de l'inventivit\'{e} des brevets logiciels \@PI }{52}{subsection.0.3.5}
\contentsline {subsection}{\numberline {3.6}Les brevets logiciels diff\`{e}rent-ils des brevets sur les m\'{e}thodes de gestion, les algorithmes et les formats de fichier \@PI Si oui, dans quel sens \@PI }{52}{subsection.0.3.6}
\contentsline {subsection}{\numberline {3.7}Quelle diff\'{e}rence y a-t-il dans le syst\`{e}me de droit de protection du logiciel en Allemagne et dans les autres pays \`{a} l'int\'{e}rieur et en-dehors de l'Europe \@PI }{53}{subsection.0.3.7}
\contentsline {subsection}{\numberline {3.8}Selon quels crit\`{e}res l'OEB, la Cour F\'{e}d\'{e}rale (BGH) et la Cour F\'{e}d\'{e}rale des Brevets (BPatG) jugent-ils de la brevetabilit\'{e} du logiciel \@PI Ces crit\`{e}res sont-ils sans ambigu\"\it \'{e} et quelles ont \'{e}t\'{e} les formulations les plus utiles \@PI }{54}{subsection.0.3.8}
\contentsline {subsection}{\numberline {3.9}Combien y a-t-il de brevets logiciels en Allemagne, en Europe et dans le monde\@PI }{54}{subsection.0.3.9}
\contentsline {subsection}{\numberline {3.10}Dans combien de cas de brevets logiciels parmi les 30000 (environ) accord\'{e}s jusqu'au pr\'{e}sent par l'OEB la contribution \`{a} l'innovation est-elle suffisamment importante pour qu'on puisse consid\'{e}rer que la soci\'{e}t\'{e} y gagne en les r\'{e}mun\'{e}rant par un monopole de 20 ans \@PI }{54}{subsection.0.3.10}
\contentsline {subsection}{\numberline {3.11}Existe-t-il des contraintes d'ordre l\'{e}gal - constitution, trait\'{e}s internationaux - qui n\'{e}cessiteraient l'extension de la notion de brevetabilit\'{e} au domaine du logiciel \@PI }{55}{subsection.0.3.11}
\contentsline {subsubsection}{\numberline {3.11.1}Art 14 GG\@DP La Protection de la Propri\'{e}t\'{e} Priv\'{e}e contenue dans la Constitution }{55}{subsubsection.0.3.11.1}
\contentsline {subsubsection}{\numberline {3.11.2}Art 27 TRIPS\@DP Des clauses contraires aux exceptions \`{a} la brevetabilit\'{e} dans un contrat de libre \'{e}change}{56}{subsubsection.0.3.11.2}
\contentsline {subsection}{\numberline {3.12}Comment jugez vous l'id\'{e}e d'introduction d'un droit sp\'{e}cialis\'{e} (droit sui generis) pour le logiciel\@PI }{57}{subsection.0.3.12}
\contentsline {subsection}{\numberline {3.13}Et dans le domaine du droit d'auteur y a-t-il des insuffisances de protection contre l'imitation \@PI Quel devrait \^{e}tre le contenu d'un ``droit sui generis adapt\'{e} pour le logiciel'' \@PI }{57}{subsection.0.3.13}
\contentsline {subsection}{\numberline {3.14}Doit-on distinguer les biens brevetables et non-brevetables\&nbsp\@PV \@PI }{58}{subsection.0.3.14}
\contentsline {subsection}{\numberline {3.15}Si oui, selon quels crit\`{e}res doit-on distinguer entre ces deux types de biens (d'inventions)\@PI }{58}{subsection.0.3.15}
\contentsline {subsection}{\numberline {3.16}Quels seraient les effets macro- et micro-\'{e}conomiques d'une brevetabilit\'{e} sans limitation clairement \'{e}tablie des soi-disant produits logiciels\@PI }{59}{subsection.0.3.16}
\contentsline {subsection}{\numberline {3.17}Quels sont les m\'{e}thodes de travail et les fondements \'{e}conomiques du mouvement du logiciel libre \@PI }{59}{subsection.0.3.17}
\contentsline {subsection}{\numberline {3.18}Quels sont les effets d'une brevetabilit\'{e} \'{e}tendue du logiciel sous des licenses libres (opensource) \@PI }{59}{subsection.0.3.18}
\contentsline {subsection}{\numberline {3.19}Est-ce qu'une extension de la brevetabilit\'{e} au logiciel pourrait freiner le d\'{e}veloppement des logiciels libres \@PI Si oui, comment pourrait-on \'{e}viter ces inconv\'{e}nients\@PI }{59}{subsection.0.3.19}
\contentsline {subsection}{\numberline {3.20}Quels seraient les effets d'une extension de la brevetabilit\'{e} pour les petites et moyennes entreprises\@PI }{60}{subsection.0.3.20}
\contentsline {subsection}{\numberline {3.21}Reconnaissez-vous que l'extension de la brevetabilit\'{e} du logiciel puisse mettre en danger la capacit\'{e} d'innovations dans ce domaine\&nbsp\@PV \@PI Dans ce contexte, comment voyez-vous l'\'{e}tude du MIT {\it Sequential Innovation, Patents and Imitation} par James Bessen\ et\ Eric Maskin (1999\ (http\@DP //www.researchoninnovation.org/))\&nbsp\@PV \@PI }{61}{subsection.0.3.21}
\contentsline {subsection}{\numberline {3.22}Le droit de brevet constitue-t-il selon vous un outil adapt\'{e} \`{a} la stimulation de l'innovation et du progr\`{e}s dans le domaine du logiciel\@PI }{61}{subsection.0.3.22}
\contentsline {subsection}{\numberline {3.23}Existe-t-il des alternatives plus efficaces et si oui, lesquelles\@PI }{61}{subsection.0.3.23}
\contentsline {subsection}{\numberline {3.24}Si demain entrait en vigueur une annulation au niveau de l'UE ayant pour effet une non brevetabilit\'{e} des logiciels ou autres concepts abstraits, en quoi cela affecterait-il les brevets d\'{e}j\`{a} accord\'{e}s dans ces secteurs \@PI }{61}{subsection.0.3.24}
\contentsline {section}{\numberline {4}R\'{e}ponses d\'{e}j\`{a} re\c {c}ues}{62}{section.0.4}
\contentsline {section}{\numberline {5}Autres Textes}{65}{section.0.5}
