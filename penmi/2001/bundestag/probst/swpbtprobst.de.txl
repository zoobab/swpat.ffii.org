<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Daniel Probst: Software-Patentierbarkeit aus
wirtschaftswissenschaftlicher Sicht

#descr: Dr. Probst forscht an der Universität Mannheim über die Ökonomie des
Patentwesens.  In diesem für eine Anhörung dem Deutschen Bundestages
am 2001-06-21 eingereichten Papier argumentiert er, dass
Patentmonopole aus volkswirtschaftlicher Sicht immer als je nach ihrem
Anwendungsgebiet mehr oder weniger notwendige Übel angesehen werden. 
Wobei im Bereich der Software wenig Notwendigkeit und viel Übel zu
erkennen ist.  Viele der herkömmlich von Patentexperten propagierten
Glaubenssätze sind aus volkswirtschaftlicher Sicht falsch, da auf
simplistischen Modellen beruhend.  Bisherige Erkenntnisse deuten
darauf hin, dass Softwarepatente die gesamte Produktivität und
Innovationskraft der betroffenen Branchen mindern.  In einem Bereich
wie der Software sollte der Staat, wenn ihm an der Vitalität der
Softwarebranche gelegen ist, in öffentliche Infrastrukturen wie z.B.
Bildung, Forschung und Netzwerk-Hardware investieren.

#Oit: Originaltitel

#Seu: Stellungnahme zum Fragenkatalog für das Expertengespräch
%(q:Softwarepatente und Open Source) des Ausschusses für Kultur und
Medien (Unterausschuss %(q:Neue Medien)) und des Rechtsausschusses des
Deutschen Bundestages

#Aut: Autor

#Dpe: Dr. rer. pol. Daniel Probst

#LsW: Lehrstuhl für Volkswirtschaftslehre, Wirtschaftstheorie

#UsM: Universität Mannheim

#Onk: Originaldokument

#AWr: Angesichts der Kurzfristigkeit der Einladung verzichte ich zeit- und
kompetenzbedingt auf die Kommentierung der eher juristisch
orientierten Fragen und beschränke mich auf die ökonomischen
Fragestellungen.

#Ard: Allgemeine Grundlagen und Ausgangslage

#Wsm: Wie wird Patentschutz im allgemeinen begründet?

#WWc: Welche Schutzrechte gibt es für Software und welche Wirkungen haben
sie?

#WWe: Was unterscheidet Software von anderen Erfindungen?

#UWm: Unterscheiden sich Softwarepatente von Patenten für Geschäftsideen,
Algorithmen und Dateiformaten und wenn ja, wie?

#Wnh: Wie beurteilen Sie die Einführung eines eigenen Schutzrechtes für
Software?

#Wve: Welche volkswirtschaftlichen und betriebswirtschaftlichen   Effekte
hätte ihrer Meinung nach eine weitestgehend   unreglementierte
Patentierbarkeit von sog. Softwareprodukten?   Worauf basieren diese
Annahmen?

#Wdl: Welches sind die Arbeitsmethoden und wirtschaftliche Grundlagen der
Open-Source-Bewegung?

#WWW: Welche Auswirkungen erweiterter Patentierbarkeit von   Software
erwarten Sie auf die Rahmenbedingungen der   Softwareentwicklung unter
sog.   Open-Source-Lizenzen?

#Wta: Würde eine Ausdehnung des Patentschutzes für Software die
Open-Source-Bewegung behindern? Wenn ja, wie könnten diese Nachteile
beseitigt werden?

#WaW: Welche Auswirkungen hätte die Ausdehnung des Patentschutzes für
Software auf die Entwicklung von kleinen und mittelständischen
Unternehmen?

#SdW: Sind Sie der Meinung, dass eine Ausdehnung des   Patentschutzes für
Software Innovationen in dieser Branche   gefährden würden? Wie
beurteilen Sie in diesem Zusammenhang die   MIT Studie %(q:Sequential
innovation, patents and imitation) von James   Bessen and Eric Maskin
(1999)?

#IuW: Ist das Patentrecht aus Ihrer Sicht das geeignete Werkzeug, um im
Softwarebereich Innovation und Fortschritt zu fördern? Sind
effektivere Alternativen denkbar, und wenn ja, welche?

#Wde: Wie wirkt sich Ihrer Ansicht nach eine Klarstellung auf EU-Ebene
dahingehend, dass Software und gedankliche Konzepte nicht patentierbar
sind, auf bereits bestehende Patente in diesem Bereich aus?

#Wrk: Weitere Lektüre

#IiW: Ich erlaube mir, eine oberflächliche Exkursion zum ökonomischen
Effizienzbegriff anzuführen, da dieser Begriff bei der ökonomischen
Begründung von Patentrechten zentral ist.

#Ebs: Eine Allokation (Verteilung von Gütern auf die Teilnehmer der
Ökonomie) heisst %(e:effizient), wenn es keine mögliche Reallokation
der Produktionsprozesse sowie der anfänglichen Besitzverhältnisse
gibt, sodass jederman besser gestellt wird. Salopp ausgedrückt kann
man sagen, das eine Situation ineffizient ist, wenn der Kuchen, den es
zu verteilen gilt, nicht so gross ist, wie er sein könnte. Die Stärke
dieses Konzeptes liegt darin, dass auf die Verteilung der Güter
%(e:nicht) eingegangen wird. Somit wird jeder --- unabhängig von
seiner moralischen, ethischen, ideologischen Überzeugung ---
übereinstimmen, dass eine ineffiziente Situation schlecht ist.

#Inr: In diesem Sinn ist ein Monopol ineffizient (schlecht), nicht etwa weil
die Konsumenten viel bezahlen müssen, sondern weil es typischerweise
in Monopolsituationen das Potential für zusätzliche Tauschgeschäfte
gibt, deren Durchführung sowohl im Interesse des Produzenten wie auch
der Konsumenten wäre (allerdings nicht zum Monopolpreis).

#IWd: Ich beschränke mich auf die ökonomische Begründung des Patentschutzes.

#Iis: Immaterielle Güter wie Wissen zeichnen sich dadurch aus, dass sie ---
im Gegensatz zu materiellen Gütern --- beliebig und kostenlos
reproduzierbar sind. Des weiteren beeinflusst der Konsum von Wissen
durch eine Person keineswegs die Möglichkeiten einer anderen Person,
dieses Wissen auch zu konsumieren (wiederum im Gegensatz zu
materiellen Gütern). Wenn Wissen schon vorhanden ist (nicht aufwändig
produziert werden muss), spricht ökonomisch gesehen %(e:absolut
nichts) für ein Besitzrecht an Wissen (vermittels dessen Leute vom
Konsum ausgeschlossen werden können). Die einzig effiziente Situation
ist die, in der das Wissen zu den Reproduktionskosten von Null
beliebig reproduziert wird, und von allen Teilnehmern der Ökonomie
konsumiert wird.

#Dee: Diese Betrachtungsweise ändert sich, wenn man die kostenträchtige
Produktion von Wissen mit in Betracht zieht.  Ohne institutionelle
Rahmenbedingungen wird ein Individuum nur insoweit Aufwand betreiben,
um Wissen zu erzeugen, wie er durch seinen eigenen Konsum
rechtfertigen kann. Dies führt zu einer ineffizient niedrigen
Produktion von Wissen.

#DWu: Das relevante ökonomische Problem ist es nun, Mechanismen (bspw. in
Form von Patenten) zu finden, die effiziente Anreize an die Produktion
stellen. Dies wäre kein Problem in einer Welt, in der man die
Produktionskosten und Konsumnutzen der Leute kennt (die Gesellschaft
würde den billigsten Produzenten anordnen, die effiziente Menge von
Wissen zu produzieren und ihn entsprechend seiner Kosten
renumerieren).  Da diese Informationen typischerweise nicht vorhanden
sind, benutzt man in der Praxis Mechanismen wie Patente.

#EeW: Ein Patent ist also aus Sicht der Ökonomie ein mehr oder weniger
notwendiges Übel, bei dem ein ineffizientes Monopol benutzt wird, um
Produktionsanreize zu setzen.

#Del: Diese oben angedeutete, simpelste ökonomische Motivation von Patenten
wird oft als statisches Modell bezeichnet.  Die Frage nach
Effizienzeigenschaften verschiedener Ausgestaltungsmöglichkeiten von
Patenten (Dauer, Novitäts-an-for-derungen, etc.) wird typischerweise
in komplizierteren Szenarien untersucht. Dabei werden auch dynamische
Aspekte des Wettbewerbs sowie der Marktstruktur in die Betrachtung
miteinbezogen. Es ist dabei zu beachten, dass scheinbar
offensichtliche Erkenntnisse aus dem statischen Modell, wie bspw.
``eine Verstärkung des Patentschutzes führt zu vermehrtem
Forschungsaufwand'', in etwas reicheren Modellen keineswegs zwingend
sind. Dies wird oft von undifferenzierten Befürwortern eines möglichst
starken Schutzes geistigen Eigentums geflissentlich übersehen.

#Ilp: In erster Linie ist das Urheberrecht zu nennen. Es verhindert
Plagiate. Da die Dekompilierung/Analyse von kompilierter Software
einen ähnlichen Aufwand bedingt, wie die ursprüngliche Herstellung,
bewirkt das Urheberrecht einen effizienten Investitionsschutz.

#Pen: Produkt- und branchenspezifische Eigenschaften, auf die im dritten
Fragenkomplex ausführlich eingegangen wird.

#Nei: Nein

#EWl: Es gibt bereits ein Schutzrecht für Software, nämlich das
Urheberrecht.  Wie im dritten Teil des Fragenkomplexes erörtert werden
wird, sind mir keinerlei Untersuchungen bekannt, die belegen, dass die
Einführung eines andersgestalteten Schutzrechtes eine Verbesserung
darstellt.

#DOr: Die Bereitstellung von Open Source Software würde drastisch
schrumpfen.

#Dbo: Der Anteil der KMUs würde abnehmen und ein Konzentrationsprozess würde
eintreten.

#EWb: Einige wenige Grossunternehmen würden aufgrund von Netzwerkeffekten
marktdominierende Stellungen erlangen. Insoweit als dieses Verhalten
mit dem Wettbewerbsrecht vereinbar ist, würden sie untereinander
Kreuzlizenzierungsabkommen über ihre Patentportfolios abschliessen,
und vermittels Sperrpatente den Markteintritt neuer Firmen stark
beschränken.

#Did: Die Forschungsintensität der Branche würde stagnieren/fallen.

#Dir: Der Begriff %(q:Bewegung) ist unglücklich gewählt, da er eine
ideologische Motivation suggeriert.  Es wird oft von Kommentatoren
unterstellt, der Erfolg von OpenSource (hiernach OS) Business Modellen
stelle gängige ökonomische Paradigma in Frage.

#IWa: In Bezug auf folgende Fragestellungen:

#idW: individuelle Anreize

#wog: warum programmieren erstklassige Programmierer Code, den sie umsonst
freigeben, und wie ist eine solches Verhalten mit dem gängigen
ökonomischen Paradigma des eigennützigen Verhaltens vereinbar?

#Fnt: Firmenstrategien

#wnW: warum allozieren Firmen einige ihrer talentiertesten Arbeiter zu OS
Projekten?

#Ont: Organisation

#sic: stellt der auf den ersten Blick anarchistische Prozess der Open Source
Entwicklung eine neue Organisationsform dar?

#Itr: Innovationsprozess

#wsj: wie passt ein OS getriebener Innovationsprozess zum klassischen
Intellectual Property Rights Ansatz?}  Neuere Literatur
(LernerTirole2000, LernerTirole2001, Bessen2001) belegt jedoch, dass
das Entstehen von OS Projekten sehr wohl mit der klassischen Ökonomie
vereinbar ist.

#Kbe: Kurzbeschreibung der Arbeitsmethoden

#WfG: Wirtschaftliche Grundlagen

#Bie: Bei OS Software wird der Quellcode des Softwareproduktes preisgegeben
(daher der Name Open Source). Die Entwickler bestehen jedoch auf
Benutzerlizenzen. Eine der bekanntesten Lizenzen ist die GNU Public
License (GPL), die von Richard Stallman entwickelt wurde. Die GPL
erlaubt den freien Vertrieb/Gebrauch des Quellcodes, verlangt jedoch,
dass Modifikationen und Erweiterungen kostenfrei in Form von Quellcode
erhältlich sind (deshalb wird die Lizenz oft als ``viral''
bezeichnet). Andere Lizenzen (Apache, BSD) sind liberaler, indem sie
nicht die kostenlose Freigabe des Quellcodes von Modifikationen
verlangen.

#ODs: OS Entwickler benutzen zur Kommunikation/Koordination hauptsächlich
das Internet. Die Arbeitsweise erinnert in Teilkomponenten an die
akademische Produktion von Wissen (peer review, Reputation,
Gruppensanktionen bei nicht normkonformen Verhalten).

#Fir: Für weiterführende Literatur wird auf citet{Raymond,LernerTirole2000,
Bessen2001} verwiesen.

#IWu: Im folgenden soll illustriert werden, wie die Anreize zur
Bereitstellung von OS Software aus Sicht der Wirtschaftstheorie
erklärt werden.

#Ien: Individuelle Anreize: es gibt im wesentlichen zwei Ansätze,   die
individuelle Anreize der Bereitstellung von OS Software   erklären.
(citet{LernerTirole2000}) untersuchen den Aspekt der   ''career
concern incentive''. Dabei wird die Bereitstellung von OS   Software
als Signalisierung der persönlichen   Kompetenz/Fähigkeiten
interpretiert (ähnlich einer Werbeaktion).   Der dabei entstehende
Aufwand wird durch zukünftige Vorteile bei  
Jobsuche/Lohnverhandlungen wettgemacht (nicht zu vernachlässigen   ist
natürlich auch die Kompenente der intrinsischen Motivation: es   ist
intellektuell stimulierend, schwierige OS Software zu entwickeln, und 
 die Bewunderung der Gemeinde dafür zu ernten).

#EWg: Eine zweite Richtung der Literatur (citet{Bessen2001,Pappas2000})  
betrachtet den Aspekt der privaten Bereitstellung von öffentlichen  
Gütern. Dabei wird angenommen, dass Entwickler intrinsisch   motiviert
sind (bspw. wird das Softwareprodukt zur Lösung einer   anderen
Aufgabe benötigt). Die geringen Kosten der Kommunikation   über das
Internet ermöglichen die Koordination der am   stärksten intrinsisch
motivierten Entwickler.

#FiW: Firmenanreize: die Anreize kommerzieller Firmen sind relativ  
transparent. An erster Stelle wird der Gewinn durch den Verkauf  
komplementärer Produkte erwirtschaftet (bspw. die   Zusammenstellung
und Anpassung von Paketen durch einen Linux   Distributor, oder die
Anpassung von Open Source Software an   spezifische Kundenwünsche).
Durch die aktive Unterstützung von   Open Source Projekten durch
eigene Angestellte bekommt die Firma a)   wertvolle Informationen
bezüglich des aktuellen   Entwicklungsgeschehens, b) die Möglichkeit,
talentierte Open   Source Entwickler ausfindig zu machen und als
Mitarbeiter zu   gewinnen (bzw. diesen Entwicklern eine attraktive
Arbeitsumgebung   zur Verfügung zu stellen).

#Dpn: Des weiteren können Open Source Projekte dazu dienen, präemptiv  
Standards zu etablieren bevor die Konkurrenz es macht (bspw. Suns  
JAVA versus Microsofts .NET).

#Dti: Die Einführung der Patentierbarkeit von Software würde in erster Linie
die Entwicklungskosten von Open Source (OS) Software erhöhen, da
erheblicher Aufwand zur Überprüfung von Patentverletzungen betrieben
werden müsste. Des weiteren entstünden Kosten, um sich im Falle von
rechtliche Auseinandersetzungen abzusichern.  Auf der anderen Seite
ist die Nutzung von Lizenzeinnahmen von OS Patenten schwierig, da
typischerweise kein Rechteeigentümer vorhanden ist. OS Lizenzen
verbieten typischerweise den Nutzungsauschluss bestimmter
Personengruppen.

#BiX: Bezugnehmend auf die individuellen Anreize zur Bereitstellung von OS
Software ist es sehr wahrscheinlich, dass die patentinduzierten Kosten
die individuelle Motivation übersteigen. An dieser Stelle sei nochmals
betont, dass der Softwaremarkt ein Markt mit extrem geringen
Eintrittsschranken darstellt (man nehme einen Studenten, einen alten
PC und einen Internetanschluss).  Wie das Beispiel %(ip:IPIX) im
nächsten Abschnitt zeigt, dürften bereits Kosten, die normalerweise
betriebswirtschaftlich als vernachlässigbar angesehen werden,
ausreichen, um Entwickler vom Anbieten von OS Software abzuhalten.

#EWn: Ein weiterer kritischer Punkt in diesem Kontext ist die kooperative,
dezentrale Art der Entwicklung. Das Risiko, dass der Beitrag eines
anderen Entwicklers eine Patentverletzung enthält, vermindert den
(Nutz)wert des gesamten OS Projekts, und senkt damit die
Beitragsanreize des einzelnen Entwicklers.

#Sor: Softwarepatentierung würde auch eine starke Asymmetrie der
Ausgangslage bei Verhandlungen zwischen OS Softwarefirmen und den
Herstellern geschlossener Produkte erzeugen.  Durch die systembedingte
Quellcodeoffenheit von OS Software ist es für einen Konkurrenten
relativ einfach, Patentverletzungen bei OS Software aufzuspüren und
nachzuweisen. Umgekehrt sind Patentverletzungen bei geschlossener
Software schwieriger nachzuweisen.

#Dir2: Das den Punkten in Abschnitt 3.3 nicht nur theoretische Bedeutung
zukommt, verdeutlichen einige Entwicklungen der letzten Zeit.  Immer
öfter greifen haupt-sächlich US Firmen zu Patentverletzungsklagen,
bzw. die Androhung derselbigen, um gegen Open Source (OS) Konkurrenten
vorzugehen. Microsoft hat in den viel zitierten
%(hd:Halloween-Dokumenten) dokumentiert, das unter anderem das
Patentrecht als Waffe gegen Linux angesehen wird.

#IMh: Im vorangehenden Abschnitt wurde argumentiert, dass der Aufwand für
Patentrechtsstreitigkeiten die Möglichkeiten vieler OS Entwickler
übersteigt. Als illustratives Beispiel dient das kürzliche Vorgehen
der Internet Pictures Corporation (iPIX) gegen den deutschen
Mathematikprofessor Helmut Dersch.

#Pge: Professor Dersch entwickelte frei zugängliche Programme, die Bilder zu
Panoramaansichten zusammenfügen. Siehe auch %{URL}. Schon die blosse
Ankündigung eines rechtlichen Vorgehens gegen ihn (auf Basis von
US-Patenten, die in Deutschland nicht rechtskräftig sind) reichte aus,
um Professor Dersch zur Aufgabe seines Web-Angebotes zu bewegen (nach
eigener Aussage verfügt Professor Dersch über %(q:weder das Geld, noch
die Zeit oder die Nerven, dagegen vorzugehen)).  Es gibt %(xr:ähnliche
Fälle, bspw. im Bereich von Open Source Audio CODECs).

#SeW: Selbst dann, wenn der Entwicklung von OS Projekten nicht unerhebliche
finanzielle Ressourcen zugrundeliegen, besteht bei der strategischen
Verwendung von Patentverletzungsklagen immer noch eine starke
Asymmetrie zuungunsten von OS. Wie in Punkt 3.3 beschrieben, ist es
dank der Quellcodeeinsicht einfacher, die Patentverletzung bei einem
Open Source Projekt festzustellen, als bei einem klassischen
Softwareprodukt.  Die bislang beobachtbaren kooperativen
Gleichgewichte, in denen die Parteien eine Kreuzlizenzierung ihrer
Patentportfolios durchführen, sind im Kontext von OS Produkten und
Softwarepatenten nur schlecht vorstellbar.

#DWn: Diese Probleme könnten notfalls gemindert werden, indem
Veröffentlichung, Inverkehrbringung, und Ausführung von Open Source
Software keine Patentverletzung darstellen.

#Men: Man würde die Wettbewerbssituation zugunsten von grossen Anbietern mit
gut ausgebauten (Patent)Rechtsabteilungen und zuungunsten von KMUs
ändern. Es ist mittlerweile gut dokumentiert, dass in
Technologiesektoren mit Patentschutz KMUs wenig bis kaum vom
Patentschutz gebrauch machen (der Biotechnologiesektor bildet eine
Ausnahme).

#Sev: Selbst in der überraschend patentfreundlichen Auftragsstudie des
Intellectual Property Institute (IPI) von %(c:HartHolmesReid2000) wird
vermerkt, dass Europäische KMUs wenig von Patentrecht Gebrauch machen.

#Dyu: Das UK Economic and Social Research Council sponserte eine
grossangelegte (1.2 Millionen Pfund) Serie von Auftragsstudien zum
Thema von Intellectual Property Rights. Die Zusammenfassung der
Forschungsergebnisse ist unter %{URL} finden. Die Ergebnisse dieser
Studie flossen zusammen mit anderen empirischen Untersuchungen in den
Abschlussbericht der Auftragsstudie der Europäischen Kommisision
%(q:Patent Protection of Computer Programmes) von
%(c:TangAdamsPare2001) ein.

#Hhm: Hieraus seien die wesentlichen Punkte der Executive Summary, Seiten
5-6, zitiert:

#TWs: The main conclusions from these research projects also reflected, in
the main, the general findings of European and U.S. studies.
Significantly, SMEs

#rtW: rely, generally, on copyright for their digital literary   works
including software;

#per: patent less, as they find the system complicated, expensive and   do
not view patents as conferring any particular advantage for their  
software-based products;

#ail: argue that the lack of resources of SMEs make it difficult for   them
to defend patents, and would in all likelihood, lose if   challenged
by corporate players;

#dsW: do not particularly use patent information for their innovation;

#ers: employ, in addition to copyright, several informal methods of  
protection, particularly technical systems, such as encryption and  
passwords, and defer to trust (arising from networks and close  
customer/supplier relationships); market niche (the smaller the  
market, the easier it would be to detect infringement); first mover  
advantage (being first to market); and secrecy as effective methods  
of protection;

#flW: feel that amendments to and tampering   with IP law, for instance,
copyright and patent, will increasingly   make it more difficult for
SMEs to cope with developments, which in   turn, may not have any
tangible effect on them because of their   inability to keep up with
them;

#cpf: contend that while   appropriation of IP is important to them, their
main concerns in   general are developing the product and getting it
to market in the   shortest possible time. This concern emanates from
the twin   pressures of: (a) rapid developments in software and
electronic   publications; and (b) speedy obsolescence of these
products;

#Knt: Kurzantwort: Ja.

#WWe2: Wie in 1.1 angedeutet, gibt es eine umfangreiche theoretische,
ökonomische Literatur zu Patenten.  Als Referenz diene hier nochmals
%(c:HartHolmesReid2000) sowie deren Zusammenfassung in Fussnote 3.
Währendem die theoretische Literatur naturgemäss wenig geeignet ist,
in einem bestimmten Sektor zwischen einem starken oder weniger starken
Schutzrecht zu differenzieren, hinterlassen die Erkentnisse der
theoretischen Modelle Zweifel an der Effizienz starker Schutzrechte
(insbesondere seinen hier auf Netzwerkeffekte, sowie
Marktzutrittsschranken durch blockierende Patente hingewiesen).

#Wto: Wie schon einige Male angedeutet, ist der Softwaremarkt ein Markt mit
extrem geringen Zutrittskosten. In diesem Fall führen schon kleine
Transaktionskosten (die typischerweise in theoretischen Modellen nicht
thematisiert werden) zu drastischen Änderungen des Marktzutrittes.

#Ini: In dem theoretischen Teil ihrer Arbeit illustrieren
%(c:BessenMaskin2000), im Kontext von sequentiellen Innovationen, dass
Komplementaritäten zwischen Produkten zusammen mit der Annahme der
Dissipation von Renten durch Wettbewerb auf dem Produktmarkt, die
Intuition des statischen Modells in 1.1 auf den Kopf drehen kann:
Patente können zu geringerer Innovation führen als im Fall ohne
Patente.

#Idm: Im empirischen Teil ihrer Arbeit präsentieren %(c:BessenMaskin2000)
Beobachtungen, die die Relevanz der Effekte ihres dynamischen Modells
untermauern und im Kontrast zur %(q:mehr Schutz ist besser-Mentalität)
stehen. Sie betrachten(unter anderem) folgende Punkte:

#KWi: Kreuzlizenzierung von Patentportfolios: im Hochtechnologiesektor  
sind Kreuzlizenzierungen von Patentportfolios unter direkten  
Wettbewerbern häufig zu beobachten (insbesondere umfassen diese  
Lizenzabkommen auch %(e:zukünftige) Patente). Ein solches   Verhalten
ist schwer im Rahmen eines klassischen, statischen   Patentmodells zu
erklären. Es passt eher zu eine dynamischen   Modellierung, bei denen
Firmen nicht versuchen, im Rahmen von   Lizenzeinnahmen (und
Monopolrenten) F&E-Ausgaben zu kompensieren, sondern um die  
Blockierwirkung der gegnerischen Patente besorgt sind.

#Dvo: Die Auswirkung der Einführung von Softwarepatenten in den USA:   es
wird unter anderem untersucht, ob die Forschungsintensität in   Folge
der Einführung von Softwarepatenten zugenommen hat (wie es   ein
klassisches, statisches Modell prognostizieren würde).  
citet{BessenMaskin2000} finden jedoch eine sowohl eine Stagnation  
der Forschungsintensität wie auch ein Fallen der %(rd:relativen
Forschungsintensität) der Softwareindustrie im   relevanten Zeitraum.

#Fir2: Forschungsintensität im Vergleich zur verarbeitenden Industrie.

#Zms: Zusammenfassung:

#Eru: Es gibt auf wirtschaftstheoretischer Ebene %(e:keine) stringenten
Argumente für die unbedachte Verstärkung des Rechtsschutzes von
Software.  Ebensowenig gibt es auf empirischer Ebene Belege dafür,
dass eine solche Verstärkung gesamtwirtschaftlich wünschenswerte
Effekte hätte.  Die empirische Analyse von %(c:BessenMaskin2000)
deutet darauf hin, dass eine solche Verstärkung in den USA %(e:nicht)
mit der damit gewünschten, gesellschaftlich effizienten Verstärkung
der Forschungsintensität einhergegangen ist.

#DWa: Die bisherigen Überlegungen legen nahe, dass das Patentrecht nur wenig
geeignet ist, um im Softwarebereich Innovation und Fortschritt zu
fördern.

#DaW: Der klassische Weg der Förderung der Infrastruktur (bspw.
Breitbandinternet) sowie der Ausbildungsförderung sind vorzuziehen.

#Del2: Die bestehenden Patente wären wertlos, da die Gegenpartei eines
Patentinhabers eine erfolgreiche Nichtigkeitsklage anstreben könnte.

#Dmg: Die bisherige Unsicherheit bezüglich der Rechtsbeständigkeit von
Softwarepatenten legt die Vermutung nahe, das Softwarepatente in der
Vergangenheit %(e:nicht) mit dem Ziel angemeldet/erworben wurden,
Forschungsausgaben durch Lizenzgebühren oder Monopolrenten
zurückzugewinnen.  Patente wurden bislang eher für defensive Zwecke
vorgesehen.  Deshalb ist eine übermäßig negative Auswirkung des
Wertverfalls bestehender Patente auf die Rentabilität damit
verbundener Projekte %(e:nicht) zu erwarten.

#Ort: Original-Version der schriftlichen Stellungnahme

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpbtprobst ;
# txtlang: de ;
# multlin: t ;
# End: ;

