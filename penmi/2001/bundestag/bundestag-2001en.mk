bundestag-2001en.lstex:
	lstex bundestag-2001en | sort -u > bundestag-2001en.lstex
bundestag-2001en.mk:	bundestag-2001en.lstex
	vcat /ul/prg/RC/texmake > bundestag-2001en.mk


bundestag-2001en.dvi:	bundestag-2001en.mk bundestag-2001en.tex
	latex bundestag-2001en && while tail -n 20 bundestag-2001en.log | grep references && latex bundestag-2001en;do eval;done
	if test -r bundestag-2001en.idx;then makeindex bundestag-2001en && latex bundestag-2001en;fi
bundestag-2001en.pdf:	bundestag-2001en.mk bundestag-2001en.tex
	pdflatex bundestag-2001en && while tail -n 20 bundestag-2001en.log | grep references && pdflatex bundestag-2001en;do eval;done
	if test -r bundestag-2001en.idx;then makeindex bundestag-2001en && pdflatex bundestag-2001en;fi
bundestag-2001en.tty:	bundestag-2001en.dvi
	dvi2tty -q bundestag-2001en > bundestag-2001en.tty
bundestag-2001en.ps:	bundestag-2001en.dvi	
	dvips  bundestag-2001en
bundestag-2001en.001:	bundestag-2001en.dvi
	rm -f bundestag-2001en.[0-9][0-9][0-9]
	dvips -i -S 1  bundestag-2001en
bundestag-2001en.pbm:	bundestag-2001en.ps
	pstopbm bundestag-2001en.ps
bundestag-2001en.gif:	bundestag-2001en.ps
	pstogif bundestag-2001en.ps
bundestag-2001en.eps:	bundestag-2001en.dvi
	dvips -E -f bundestag-2001en > bundestag-2001en.eps
bundestag-2001en-01.g3n:	bundestag-2001en.ps
	ps2fax n bundestag-2001en.ps
bundestag-2001en-01.g3f:	bundestag-2001en.ps
	ps2fax f bundestag-2001en.ps
bundestag-2001en.ps.gz:	bundestag-2001en.ps
	gzip < bundestag-2001en.ps > bundestag-2001en.ps.gz
bundestag-2001en.ps.gz.uue:	bundestag-2001en.ps.gz
	uuencode bundestag-2001en.ps.gz bundestag-2001en.ps.gz > bundestag-2001en.ps.gz.uue
bundestag-2001en.faxsnd:	bundestag-2001en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo bundestag-2001en-??.g3n`;source faxsnd main
bundestag-2001en_tex.ps:	
	cat bundestag-2001en.tex | splitlong | coco | m2ps > bundestag-2001en_tex.ps
bundestag-2001en_tex.ps.gz:	bundestag-2001en_tex.ps
	gzip < bundestag-2001en_tex.ps > bundestag-2001en_tex.ps.gz
bundestag-2001en-01.pgm:
	cat bundestag-2001en.ps | gs -q -sDEVICE=pgm -sOutputFile=bundestag-2001en-%02d.pgm -
bundestag-2001en/bundestag-2001en.html:	bundestag-2001en.dvi
	rm -fR bundestag-2001en;latex2html bundestag-2001en.tex
xview:	bundestag-2001en.dvi
	xdvi -s 8 bundestag-2001en &
tview:	bundestag-2001en.tty
	browse bundestag-2001en.tty 
gview:	bundestag-2001en.ps
	ghostview  bundestag-2001en.ps &
print:	bundestag-2001en.ps
	lpr -s -h bundestag-2001en.ps 
sprint:	bundestag-2001en.001
	for F in bundestag-2001en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	bundestag-2001en.faxsnd
	faxsndjob view bundestag-2001en &
fsend:	bundestag-2001en.faxsnd
	faxsndjob jobs bundestag-2001en
viewgif:	bundestag-2001en.gif
	xv bundestag-2001en.gif &
viewpbm:	bundestag-2001en.pbm
	xv bundestag-2001en-??.pbm &
vieweps:	bundestag-2001en.eps
	ghostview bundestag-2001en.eps &	
clean:	bundestag-2001en.ps
	rm -f  bundestag-2001en-*.tex bundestag-2001en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} bundestag-2001en-??.* bundestag-2001en_tex.* bundestag-2001en*~
tgz:	clean
	set +f;LSFILES=`cat bundestag-2001en.ls???`;FILES=`ls bundestag-2001en.* $$LSFILES | sort -u`;tar czvf bundestag-2001en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
