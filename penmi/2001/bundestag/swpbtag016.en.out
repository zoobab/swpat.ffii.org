\BOOKMARK [1][-]{section.1}{General Data}{}
\BOOKMARK [1][-]{section.2}{session protocol}{}
\BOOKMARK [2][-]{subsection.2.1}{Introduction by the Moderator J\366rg Tauss}{section.2}
\BOOKMARK [2][-]{subsection.2.2}{Probst: Economists Skeptical about Effects of Software Patents}{section.2}
\BOOKMARK [2][-]{subsection.2.3}{Bremer: Copyright does not adequately reflect Technicity of Software}{section.2}
\BOOKMARK [2][-]{subsection.2.4}{Schiuma: Copyright protection insufficient, TRIPs requires change of law}{section.2}
\BOOKMARK [2][-]{subsection.2.5}{Siepmann: Copyright achieves for Software what Patents achieve for Pharma}{section.2}
\BOOKMARK [2][-]{subsection.2.6}{Kiesewetter-K\366binger: Incompatibility of Patent Concepts with Software Concepts}{section.2}
\BOOKMARK [2][-]{subsection.2.7}{Bouillon: Patents endanger innovative SMEs in the field of proprietary software}{section.2}
\BOOKMARK [2][-]{subsection.2.8}{Henckel: Open Source Software unduly threatened}{section.2}
\BOOKMARK [2][-]{subsection.2.9}{Lutterbeck: Economists against Software Patents, lawyers in favor}{section.2}
\BOOKMARK [2][-]{subsection.2.10}{MdB Tauss: Themes of Today's Discussion}{section.2}
\BOOKMARK [2][-]{subsection.2.11}{MdB Mayer: What do pro-swpat studies say about the effects on innovation}{section.2}
\BOOKMARK [2][-]{subsection.2.12}{MdB Griefahn: Isn't Resistance to US Trends Futile?}{section.2}
\BOOKMARK [2][-]{subsection.2.13}{MdB Otto: Questions about Differences between US and EU, Copyright and Patents}{section.2}
\BOOKMARK [2][-]{subsection.2.14}{MdB Neumann: Against Software Patents or against all Patents?}{section.2}
\BOOKMARK [2][-]{subsection.2.15}{MdB Kelber: Conflict between Standardisation and Patentability? Does Bremer represent consensus of Bitkom members?}{section.2}
\BOOKMARK [2][-]{subsection.2.16}{Schiuma: Self-Regulation through Patent Pools Keeps Standards Open}{section.2}
\BOOKMARK [2][-]{subsection.2.17}{Lutterbeck: Patentability can't be limited, Solution lies in Source Code Privilege}{section.2}
\BOOKMARK [2][-]{subsection.2.18}{Probst: Numerous Economic Studies, nothing in support of Software Patentability}{section.2}
\BOOKMARK [2][-]{subsection.2.19}{Bouillon: Copyright is Adequate, because the effort resides in the complex tissue rather than in single ideas}{section.2}
\BOOKMARK [2][-]{subsection.2.20}{Bremer: The Economic Studies are Inconclusive, the Opensource Camp doesn't understand the Law}{section.2}
\BOOKMARK [2][-]{subsection.2.21}{Siepmann: Most Legal Arguments work against Software Patentability}{section.2}
\BOOKMARK [2][-]{subsection.2.22}{Schiuma: No need to reduce patent term, because well-written claims last much longer than 1 product cycle}{section.2}
\BOOKMARK [2][-]{subsection.2.23}{Tauss: Does the Volatility of Caselaw indicate a need for Legislation?}{section.2}
\BOOKMARK [2][-]{subsection.2.24}{MdB Otto: Could an OpenSource Exception be the Solution?}{section.2}
\BOOKMARK [2][-]{subsection.2.25}{MdB Griefahn: What is ``trivial''? Where are the limits?}{section.2}
\BOOKMARK [2][-]{subsection.2.26}{Siepmann: Return to EPC and Earlier Caselaw possible without difficulties}{section.2}
\BOOKMARK [2][-]{subsection.2.27}{Schiuma: Today's Caselaw is Clear, Legislation of 1973 is outdated and misleading}{section.2}
\BOOKMARK [2][-]{subsection.2.28}{Lutterbeck: Legal discussions pointless, better think about source code privileges and expiration terms}{section.2}
\BOOKMARK [2][-]{subsection.2.29}{Bremer: Status Quo = new caselaw + TRIPs, law must be adapted}{section.2}
\BOOKMARK [2][-]{subsection.2.30}{Bouillon: Software Patents principally unjustified, fiddling with expiry terms and competition law is not a lasting solution}{section.2}
\BOOKMARK [2][-]{subsection.2.31}{Kiesewetter-K\366binger: Difficulties in Novelty Examination of Software Patents}{section.2}
\BOOKMARK [2][-]{subsection.2.32}{Tauss: Effects on Productivity, IT Security etc}{section.2}
\BOOKMARK [2][-]{subsection.2.33}{Lutterbeck: IT security demands freedom for opensource software}{section.2}
\BOOKMARK [2][-]{subsection.2.34}{Henckel: Software Patents reinforce Dependence on Insecure Proprietary Solutions}{section.2}
\BOOKMARK [2][-]{subsection.2.35}{MdB Mayer: Are Small Groups of Developpers Hampered?}{section.2}
\BOOKMARK [2][-]{subsection.2.36}{Kieswetter-K\366binger: Patent Threat Demotivates Developpers and Entrepreneurs}{section.2}
\BOOKMARK [2][-]{subsection.2.37}{Siepmann: Danger of gradual suffocation of Free Software, Source Code Privilege not enough}{section.2}
\BOOKMARK [2][-]{subsection.2.38}{MdB Mayer: Shouldn't Patented Software be made open in principle?}{section.2}
\BOOKMARK [2][-]{subsection.2.39}{Schiuma: Patent Descriptions better suited for disclosure of software functionalities than source code}{section.2}
\BOOKMARK [2][-]{subsection.2.40}{Lutterbeck: Legislation should reflect leading position of Europe and Germany in the Area of Opensource Software}{section.2}
\BOOKMARK [2][-]{subsection.2.41}{Henckel: Software Patents undermine Copyright, expropriate Software Authors}{section.2}
\BOOKMARK [2][-]{subsection.2.42}{Bouillon: We, the Developpers, feel Expropriated}{section.2}
\BOOKMARK [2][-]{subsection.2.43}{Kiesewetter-K\366binger: Studying Source Code impossible but yet necessary for serious patent examination}{section.2}
\BOOKMARK [2][-]{subsection.2.44}{Siepmann: Source code better understandable than patent language}{section.2}
\BOOKMARK [2][-]{subsection.2.45}{Bremer: Existing inventor privileges must be upheld, Open Source Movement will survive}{section.2}
\BOOKMARK [2][-]{subsection.2.46}{Probst: Overall Economic Interest must be the Decisive Criterion}{section.2}
\BOOKMARK [2][-]{subsection.2.47}{Moderator's Concluding Remarks}{section.2}
