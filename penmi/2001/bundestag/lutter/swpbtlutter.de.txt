<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Prof. Dr. iur. Bernd Lutterbeck präsentiert seine bekannte Position, wonach das Urheberrecht für Software ungeeignet und eine Patentierung von logischen Funktionalitäten unvermeidbar ist und nur auf dem Wege der Schrankenbestimmungen -- über ein sogenanntes %(q:Quelltextprivileg) -- Abhilfe gegen Probleme geschaffen werden kann, die auch Lutterbeck als höchst dramatisch darstellt.  Lutterbeck hat seine Position bisher nie der Prüfung durch eine Diskussion unterzogen, aber dafür mit schönen Präsentationsgrafiken von prestigeträchtigen Kathedern herunter doziert.  Neu am jetzigen Text ist die Forderung, eine Lösung für die von Logikpatenten verursachten Probleme dürfe nur im Einvernehmen mit den USA angestrebt werden.
title: Stellungnahme Lutterbeck zur Bundestagsanhörung über Logikpatente
Pea: Prof. Lutterbeck möchte uns keine direkte Wiederveröffentlichung seiner Antworten an den Deutschen Bundestag gestatten.  Auf unser Ansinnen, seinen Text in HTML-Form mit Anmerkungen wiederzuveröffentlichen, entgegnete er:
Idr: Informationen dazu und ein pdf-file meines Auftritts im Bundestag finden Sie auf meiner Homepage unter %(URL).  Natürlich lege ich Wert darauf, dass mein Auftritt ausschließlich über meine Site verfügbar ist.
Wer: Wir haben die Lutterbeckschen Argumente bereits in anderem zusammenhang %(lh:kritisiert).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpbtag016.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpbtlutter ;
# txtlang: de ;
# End: ;

