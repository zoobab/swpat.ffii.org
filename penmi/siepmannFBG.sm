To: peterg@ffii.org
Subject: Anmerkungen zu Rede.tex
--text follows this line--

> Der folgende in sieben Teile gegliederte Beitrag beschreibt kurz

"in sieben Teile gegliedert" erscheint auslassbar.

> die ``Legitimation des Patentwesens'', setzt sich dann mit der
> ``Mangelnden Eignung von Patenten zum Schutz von Software''
> auseinander und zeigt die ``systemimmanenten Mängel des Patentwesens''
> auf und welche ``praktischen Auswirkungen auf die Softwarebranche''
> daraus resultieren. Eine Betrachung des ``Richtlinienvorschlages''
> führt zu dem Ergebnis, dass dieser weder volkswirschaftlich sinnvoll,
> noch in rechtlicher Hinsicht ausgereift ist. Im Anschluss wird gezeigt,
> dass das durch das urheberrechtsbasierte Entwicklungskonzept von

Vorschlag: dass die bewährten, auf dem Urheberrecht aufbauenden
Geschäftsmodelle der Software-Entwicklung Innovation und Fortschritt
besser fördern als das Patentwesen, und dass diese Geschäftsmodelle
mit dem Geschäftsmodell des Patentwesens unvereinbar sind.
Insbesondere wird deutlich gemacht, dass das Urheberrecht eine
Grundlage darstellt, auf der freie und proprietäre Software einander
ergänzen, wohingegen Patente einseitig monopolistische Tendenzen der
Softwarebranche stärken und die freie Software unverhältnismäßig
benachteiligen.

> Die Eigentumstheorie ist ein typisches Beispiel für die in der
> Rechtstheorie schon seit langem als ungeeignet angesehene Methode der
> »Begriffsjurisprudenz«: Man knüpft an gleiche Begriffe gleiche
> Rechtsfolgen, ohne die sich hinter diesen Begriffen befindlichen
> Gegenstände genauer zu betrachten. Die »Begriffsjurisprudenz« hat neben der
> heute herrschenden »Interessenjurispudenz« keine Bedeutung mehr und wird
> nur noch hervorgeholt, um eine Begründung für irgendwelche aus anderen
> Gründen gewünschte Resultate zu konstruieren.

Man hat auch versucht, den Technikbegriff als typischen Ausfluss der
Begriffsjurisprudenz madig zu machen.  M.E. braucht man beides:  klare
Begriffsbildung und eine Erklärung darüber, wie die Begriffe mit
Interessen korrelieren.  Interessen sind auch nicht alles.
 
> Die Belohnungstheorie erklärt sich schon aus ihrer Bezeichnung:
> Der Erfinder soll durch Vergabe eines Ausschließungsrechts belohnt
> werden. Die Belohnungstheorie könnte man eher als »Bestrafungstheorie«
> bezeichnen: Der wesentlich nachhaltigere und einschneidendere Effekt
> ist die Bestrafung der übrigen Mitglieder der Gesellschaft.

Bestrafung wofür?
Warum nachhaltiger?
Ich verstehe das zwar, aber es wird aus diesem Abschnitt nicht klar. 
 
> Nach der Offenbarungstheorie (oder auch Vertragstheorie) erhält der
> Erfinder eine Gegenleistung von der Gesellschaft dafür, dass er seine
> Erfindung veröffentlicht. In der Praxis erfüllt der Patentinhaber im
> Allgemeinen seine ``vertraglichen Verpflichtungen'' nicht.
> Patentanmeldungen werden bewußt so formuliert und Zeichnungen werden
> bewußt so undeutlich gestaltet, dass Patentschriften als Wissenquelle
> ungeignet sind.

Die Patentämter können dem entgegenwirken und auf einer gewissen
Klarheit bestehen.  Es gibt auch taktische Anreize dafür, klare
Beschreibungen zu liefern, was auch geschieht.
 
> Zusammenfassend kann man sagen, dass die gängigen Theorien zur
> Rechtfertigung des Patentwesens keine befriedigende Erklärung bieten.

Es wäre erwähnenswert, dass dies von Wirtschaftswissenschaftlern schon 
immer in etwa so gesehen wurde.
 
> \begin{enumerate}
> \item die Höhe der Forschungsinvestitionen,
> \item der Nachahmungsaufwand des Imitators,
> \item die Vorteile durch den »first mover advantage«,

Vorsprung

> \item die Markteintrittsbarrieren,
> \item die Innovationszyklen,
> \item die Entstehung von Innovationen (sequentiell/komplementär),

Komplexität der Erzeugnisse, Stellenwert der durch Ansprüche
umschreibbaren Einzel-Innovation im Ganzen:  sind enge Ansprüche
für den Anmelder überhaupt interessant, oder bekommt man prinzipiell
nur Breitband-Trivialpatente?

> \item die Produktkosten und Verkaufszahlen,
> \item die durchschnittliche Größe der Unternehmen der Branche,
> \item die Auswirkungen auf die Verbraucher und den Arbeitsmarkt.
> \end{enumerate}
 
> Ursache für Rechtsunsicherheit sind unter anderem »unbestimmte
> Rechtsbegriffe« wie der Begriff der »Erfindungshöhe«. Das ganze
> Konzept des geistigen Eigentums steht und fällt mit diesem unbestimmten
> Rechtsbegriff.

Das Urheberrecht kennt keine Erfindungshöhe.
Man müsste also sagen:  "Das ganze Konzept des gewerblichen
Rechtschutzes .."

> würde. Selbst ein völlig minderwertiges Patent kann zur Verunsicherung
> dienen. Deshalb führt das Patentwesen dazu, dass größere Unternehmen im

Nicht nur deshalb

> Verhältnis zu kleineren begünstigt werden. Dies wiederum begünstigt
> Monopolbildung und Monopole führen bekanntermaßen zu teueren und
> schlechteren Produkten.
 
> Die praktischen Auswirkungen auf die Softwarebranche bestehen
> in erster Linie darin, dass Milliardensummen in das Patentwesen
> fließen, die früher in die Softwareentwicklung geflossen wären.
> Man könnte -- mit Bezug auf die Softwarebranche -- das Patentwesen
> als ein »schwarzes Loch« bezeichnen, welches Milliarden verschlingt,
> aber nichts hergibt.

Kannst du die Zahl "Milliarden" belegen?
Ansonsten wären vielleicht ein paar Zitate aus

	  http://swpat.ffii.org/archiv/zitate/index.de.html

von Interesse, insbesondere von den diesjährigen FTC-Anhörungen.

> Desweiteren wird sich die Unternehmensstruktur in der Softwarebranche
> ändern: Kleine und mittlere Unternehmen werden ihre Unabhängigkeit
> verlieren und nur im Schutz von größeren Unternehmen ihre Tätigkeit
> entfalten können. Viele Unternehmen werden dem Kostendruck nicht
> standhalten und insolvent werden. Wegen der höheren

Vor letztere warnt z.B. die FHG-ISI-Studie (Knuth Blind).

> Markteintrittsbarrieren werden viele Unternehmen gar nicht erst
> entstehen, die ohne Softwarepatente entstanden wären.

S. Zitat Gates in obiger Sammlung.
 
> Zusammengefasst kann man also sagen, dass eine Computerimplementierte
> Erfindung irgendewtas mit einer programmierbaren Vorrichtung und/oder
> einem Computerprogramm zu tun hat. Genaueres kann oder will sich Verfasser
> des Richtlinienentwurfes dazu nicht äußern.

"Computerimplementierte Erfindung" ist laut Art 1 ein
Anspruchsgegenstand, bei dem die wesentliche Leistung im Bereich des
Programms liegt, also nach alter patentrechtlicher Terminologie ein
[in Anspruchssprache gefasstes und grundsätzlich patentierbares]
"Programm für DV-Anlagen als solches".
 
> Da der Stand der Technik aber durch Technik und nicht durch Nichttechnik
> bestimmt ist, liegt hier ein \textbf{Paradoxon} vor.
> Ziel dieser Definition scheint es zu sein, den Begriff »Technik« aufzuweichen.
> Hier zeigt sich das expansive Bestreben der Patentlobby, das Gebiet

Patentlobby ==> Verfasser

> der
> Technik zu verlassen und das Patentwesen auf jeden beliebigen Gegenstand,
> sei er technisch oder untechnisch, auszuweiten.

> Eine hinreichende Erfindungshöhe soll gegeben sein, wenn der Beitrag
> \begin{center}\bfseries
> »für eine fachkundige Person nicht naheliegend ist.«
> \end{center}

> Wenn man bedenkt, dass eben diese Definition in der Vergangenheit zur
> Hunderttausenden von Trivialpatenten geführt hat, wäre es wünschenswert
> gewesen, wenn der Richtlinienentwurf etwas höhere Anforderungen an die
> Erfindungshöhe gestellt hätte.

was wegen TRIPs nicht geht, wie vielfach argumentiert wird.
 
> Unter »Begriffsbestimmungen« hat der Verfasser dieses Entwurfes bis jetzt
> keine einzige saubere Defintion geliefert, sondern nur inhaltslose abstrakte
> Begriffe verwendet und seine Vorliebe für Tautologien und Paradoxen

Paradoxa

> zum Ausdruck gebracht.

> In Artikel 4 wird nun neben den traditionellen Voraussetzungen der
> Patentierbarkeit gefordert, dass eine computerimplementierte Erfindung
> einen technischen Beitrag liefert.

Es wird behauptet, dies sei eine implizite generelle Voraussetzung für
die Patentierbarkeit.
 
> Damit wird nicht mehr und nicht weniger gesagt, dass ein »Irgendetwas«,
> welches irgendeinen Bezug zu Computerprogrammen und/oder einer programmierbaren
> Vorrichtung hat, allein durch ein neues nichttechnisches Merkmal

Es muss *kein* neues nichttechnisches Merkmal vorliegen.
Die Neuheit kann im Nichttechnischen liegen.
Nur der Erfinderische Schritt muss einen Technischen Beitrag
enthalten.  Der E.S. ist wiederum ein formales Konstrukt, welches sich
manipulieren lässt.  Man kann je nach dem, welches Dokument den
"nächstliegender Stand der (un)Technik" darstellen soll, den Schritt
unterschiedlich wählen.

M.a.W.: die Erfindung (= der Technische Beitrag) muss nicht neu sein. 

> patentierbar wird, sofern die notwendige Erfindungshöhe gegeben ist.
 
> \begin{zitat}
> die neben der normalen physikalischen Wechselwirkung zwischen einem Programm
> und dem Computer \textup{[\dots]} keinerlei technische Wirkungen haben,
> nicht patentierbar.
> \end{zitat}
> 
> Das heißt nicht mehr und nicht weniger, dass Erfindungen, bei denen nichts
> weiter passiert, als dass Elektronen zum Selbstzweck im Computer hin- und
> herwandern, nicht patentfähig sind. Da solche »Erfindungen« ohnehin nicht
> gewerblich anwendbar sind, läuft Artikel 4a ins Leere.

Wahrscheinlich wird das doch etwas anders verstanden:  "Erfindungen"
(= Patentanträge) auf ein rein numerisches Rechenverfahren oder ein
Pensionsberechnungssystem sind nicht patentierbar, wenn in den
Ansprüchen nur beiläufig erwähnt wird, dass ein Rechner verwendet
wird.  Es muss festgelegt werden, wie der Rechner verwendet wird.
D.h. die Rechenoperation muss mit den Begriffen der Informatik
(des "Software-Engineering"), nicht mit den (äquivalenten) Begriffen
der Arithmetik oder Betriebswirtschaft beschrieben werden.  

> keine Logikpatente

ferner:

	EDV ist kein Gebiet der Technik

	neues programmgesteuerte chemische Erfindung patentierbar,
	Steuerungsprogramme fallen nicht unter das Patent

	technische Erfindung = Problemlösung durch beherrschbare Naturkräfte
	Gegenvorschlag

	Testsuite (Erklärung des Gesetzesziels anhand von
	Beispielpatenten)

	Was herunterladbar ist (Information), ist keine technische Erfindung

	Ansprüche auf Informationsgegenstände greifen ungerechtfertigt
	in Art 10 EuKMR (Publikationsfreiheit) ein und sind
	systemwidrig: hier ist der Eur. Kommission zuzustimmen.

Ferner zu beachten:  einige der Kritikpunkte wurden von den Dänen
bereits aufgenommen ("Frontlinienbegradigung").  Es lohnt sich
nicht, den Schwerpunkt der Kritik auf Schönheitsfehler zu legen,
welche die Rats-Patentlobby zu beheben in der Lage ist.

-- 
Hartmut Pilch, FFII e.V. und Eurolinux-Allianz            +49-89-12789608
Innovation vs Patentinflation                       http://swpat.ffii.org/ 
125000 Stimmen 400 Firmen gegen Logikpatente    http://www.noepatents.org/
	

