<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Won: possibly with my own laptop or with a PDF file on a diskette

#rni: presentation

#vWt: I do not want to receive news about other events and publications outside the scope of the communication with OECD people into which I am opting in myself.

#oaW2: Hartmut Pilch, born in 1963, M.A. in Chinese and Japanese philology and linguistics, student of various other disciplines, has earned his living by simultaneous interpreting, mainly between Japanese, Chinese, German and English during oral hearings at the European Patent Office in Munich and other language services and by programming, especially in the field of multilinguisation of Unix/Linux-based computing environments.  Co-founder of the FFII in 1998 and Eurolinux Alliance in 1999, chief editor of http://swpat.ffii.org, a comprehensive web documentation of the European software patentability debate.

#nau: The coalition of engineers and economists is already taking shape.  I hope that in this round at OECD we will mercilessly benchmark the social utility of the patent system.  The civil society and the governments will form the much-called-for %(q:patent observatory) one way or other, and we will not be content with observing.  We will define benchmarks which any IP system must reach and a deadline for them to be reached by the patent system.  The patent system needs to obtain its ISO 14000 (or OECD 14000?) homologation.  As long as the patent system has not qualified under ISO/OECD, further efforts at international codification, such as e.g. the Substantive Patent Law Treaty, must be put on halt.  At a certain point, the TRIPs treaty must be revised.  If, as is to be expected, no progess is made, the patent system must be discarded, like an old piece of clothing.  In an age of austerity and harsh competition there can not be any political tolerance for outlived systems that drain our economy.  And it is clear that any score below 90% on the two minimal requirements formulated here must be a knockout criterion for any future IPR system.

#Wss: Fritz Machlup %(fm:wrote) that the introduction of the patent system was %(q:a victory of the lawyers and protectionists against the economists).  If software patents are established in Europe this year, that could be characterised as a %(q:victory of the patent industry against the software industry).  A Pyrrhean victory, I suspect.  It may not take long before we witness a %(q:victory of the engineers and economists against the patent dogmaticians).

#Wmr: Automated Data Processing has today become an indispensable auxiliary tool in all domains of human society and will remain so in the future.  It is ubiquitous.  ... Its instrumental meaning, its auxiliary and ancillary function distinguish ADP from the ... individual fields of technology and liken it to such areas as enterprise administration, whose work results and methods ... are needed by all enterprises and for which therefore prima facie a need to assure free availability is indicated.

#ymt: These insights have in no way lost any of their validity in 30 years.  While the details of technology change, the basic philosophical categories of the Federal Court of Justice's argumentation have not changed and, if anything, the balance has shifted further in disfavor of software patentability.  To quote Gert Kolle's %(gk:famous comment) on the Dispositionsprogramm Decision of 1977:

#Ene: Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren Aufgabe hinauslaufen würde -- zu erlangen.  Es muss vielmehr dabei verbleiben, dass eine reine Organisations- und Rechenregel, deren einzige Beziehung zum Reich der Technik in ihrer Benutzbarkeit für den bestimmungsgemäßen Betrieb einer bekannten Datenverarbeitungsanlage besteht, keinen Patentschutz verdient.  Ob ihr auf andere Weise, etwa mit Hilfe des Urheber- oder des Wettbewerbsrechts, Schutz zuteil werden kann, ist hier nicht zu erörtern.

#Dnu: Der Begriff der Technik erscheint auch sachlich als das einzig brauchbare Abgrenzungskriterium gegenüber andersartigen geistigen Leistungen des Menschen, für die ein Patentschutz weder vorgesehen noch geeignet ist.  Würde man diese Grenzziehung aufgeben, dann gäbe es beispielsweise keine sichere Möglichkeit mehr, patentierbare Leistungen von solchen zu unterscheiden, denen nach dem Willen des Gesetzgebers andere Arten des Leistungsschutzes, insbesondere Urheberrechtsschutz, zuteil werden soll.  Das System des deutschen gewerblichen und Urheberrechtsschutzes beruht aber wesentlich darauf, dass für bestimmte Arten geistiger Leistungen je unterschiedliche, ihnen besonders angepasste Schutzbestimmungen gelten und dass Überschneidungen zwischen diesen verschiedenen Leistungsschutzrechten nach Möglichkeit ausgeschlossen sein sollten.  Das Patentgesetz ist auch nicht als ein Auffangbecken gedacht, in welchem alle etwa sonst nicht gesetzlich begünstigten geistigen Leistungen Schutz finden sollten.  Es ist vielmehr als ein Spezialgesetz für den Schutz eines umgrenzten Kreises geistiger Leistungen, eben der technischen, erlassen und stets auch als solches verstanden und angewendet worden.

#Sai: Stets ist aber die planmäßige Benutzung beherrschbarer Naturkräfte als unabdingbare Voraussetzung für die Bejahung des technischen Charakters einer Erfindung bezeichnet worden.  Wie dargelegt, würde die Einbeziehung menschlicher Verstandeskräfte als solcher in den Kreis der Naturkräfte, deren Benutzung zur Schaffung einer Neuerung den technischen Charakter derselben begründen, zur Folge haben, dass schlechthin allen Ergebnissen menschlicher Gedankentätigkeit, sofern sie nur eine Anweisung zum planmäßigen Handeln darstellen und kausal übersehbar sind, technische Bedeutung zugesprochen werden müsste.  Damit würde aber der Begriff des Technischen praktisch aufgegeben, würde Leistungen der menschlichen Verstandestätigkeit der Schutz des Patentrechts eröffnet, deren Wesen und Begrenzung nicht zu erkennen und übersehen ist.

#osW: There may be a need for an integration of all IPR systems into one universal system.  But this is more likely to succede on the basis of copyright.  The patent system can only secure its place as a specialised system.  In the words of the %(dp:Dispositionsprogramm decsion):

#hWx: Some proponents of universal patentability say that harnessing forces of nature, has become a marginal activity in today's knowledge economy.  They may be right:  The reasons for maintaining the patent system are becoming more and more marginal, compared to the grief that today's over-extended patent system is causing.

#epd: The patent system is not irreplacable.  It has a rival that performs better, at least in some fields: copyright.  And we propose to extend copyright to all logical creations, be they materialised as books, diskettes or electronic circuits.

#t0e: A species that can not adapt to performance requirements has to disappear sooner or later.  This is true even for the patent system, which in the past has rarely been subjected to any performance requirements.

#cfb: In our document %(DOC) you may find further performance requirements.

#hlh: We demand that the patent system should be given a deadline.  It must score more than 90% on the two above benchmarks.

#Wat: Here the success ratio is near zero.  All software patents claims which I have seen represent thoughts which are the result of at best a few hours of thinking.

#Wii: ratio of granted patents which represent real investments

#cij: When it comes to logic patents, patent offices regularly fail to identify the prior art.  The success ratio is not zero, but certainly not above 50%%

#gWv: ratio of granted patents which stand the test of novelty in court

#rWw: In order to survive in the long run, the patent system needs to perform.  It needs to achieve a 90% score on the following two benchmarks:

#irc: This double game is highly risky.  It brings the European Patent System onto a track of permanent and inevitable malperformance.

#eio: FFII/Eurolinux have been preaching the concept of %(q:technical invention) as %(q:teaching of cause-effect relation in the use of controllable forces of nature).  The European Parliament's Cultural Affairs Commission has also majoritarily voted for this approach.   But the patent establishment is not happy with this approach.  They call it %(q:outdated), because they take it for granted that anything %(q:modern) must be patentable.  They want the best of both worlds: light-weight innovation covered by heavy-weight monopolies, algorithm patents presented in the verbal clothing of the %(q:technical invention).

#rey: Patents in chemistry and pharmacy are usually of this type, even today.  That's why it is normally not easy to obtain such a chemical or pharmaceutical patent.

#srn: The field where the patent system has been a relative success is the area where engineers use forces of nature to achieve a surprising effect, i.e. go beyond what known models allow a desktop researcher to predict.

#ety: Behind the software patent problem is a more fundamental one: tolerance toward patenting of logic, often expressed in the form of function claims.  This began in the 1980s.  When the German Federal Court approved the %(ba:ABS patent in 1980), it simultaneously gave green light to function claims. There are many trivial mechanical patents, and if you investigate these patents closely, you will find that they are characterised by function claims.  They teach only an abstract idea, a philosophy of how something could be done, but no new insights about how forces of nature can be harnessed.

#ecf: Even if we mix software together with pharmaceuticals, chemistry, mechanics and all other fields and calculate an overall success rate, this rate will be dragged down significantly if software patents are allowed.

#met: A system with zero success rate will not survive in the long run.

#yWW: This is a highly dangerous policy, not only for the software industry, but also for the patent industry.

#eae: The proponents of the EU Directive seem to know this.  They have chosen a the strategy of the %(q:wolf in the sheep's coat), trying to appear as if they wanted to restrict patentability to technical inventions.  Yet, as we have shown, the Directive's concept of %(q:technical contribution) is empty.  %(ao:Under the proposed directive, Amazon One Click Shopping would without doubt be a patentable invention.)

#stw: The success rate of the patent system in the field of software is near 0%.

#let: We have been looking for the %(q:good software patent).  We have called our database users to %(me:evaluate patents on two scales): %(ol|tech = technicity = concreteness and physical substance and|deal = the deal that society makes in granting this patent.)  It was found that nobody was able to report a single %(q:good software patent) (low %(q:tech) and high %(q:deal) score) from the EPO database.  The European Commission and other proponents of software patents have also, in spite our our relentless urging, not been able to report a single example of what they would consider to be socially desirable patent claims in the field of software.

#mft: FFII has created a %(db:large database of European Software Patents).  We have exhibited some of these patents in a %(hg:horror gallery).  Among the exhibited specimens are %(mp:patents on MP3), which have been awarded prizes by the European Commission and are generally considered to relate to %(q:real inventions).  The problem is that even real innovative achievements, when formulated as patent claims, tend to become broad and trivial.  This is because software is logic, and logic is built of many small steps of reasoning, which have to be claimed individually if there are to be patents in this field.

#iof: So far, many people in the software business in Europe have had very little experience with patents.   Some even confuse patents with copyright.  Once more people actually start reading patent claims, the animosity against software patents may rise from 80% to 100%.

#Wno: 80% of software salesmen are against software patents.  80% of patent salesmen are in favor of software patents.

#cne: The software industry must not be confused with the patent industry.  Some large software and hardware companies and big associations seem to be asking for software patents, but even there, in many cases these are being hijacked by their patent departments.

#iwW: There is a consensus against patents among software developpers and businessmen, which encompasses most players in the opensource field as well as in the proprietary field.

#rtW: These 160,000 are not against Intellectual Property Rights, but rather they believe that, in the field of software, intellectual property is protected by copyright, whereas patents represent a form of piracy.  Piracy against the intellectual property of software companies as much as against the public domain.

#gnf: 160,000 signatories and 2000 companies are saying NO to software patents.

#tcg: In his 15 minutes of speaking time, Hartmut Pilch might try to make the following message understood:

#ieP: The conference will provide an opportunity to conduct discussions among policy makers, patent officials, business representatives and economists from various OECD countries. Oher participants to the conference include Mr Gurry (Assistant Director General, chief for patents, WIPO), Dr Arai (former JPO Commissionner, now President of the IP Commission of the Japanese Prime Minister),  Prof. Desantes (vice-president of the EPO), Mr. Thompson (US FTC Commissioner), Mr. Sueur (head of patent department of Air Liquide and IP Commission of French Employers Confederation (MEDEF)) etc.

#Wta: Hartmut Pilch will take part in the discussion on %(q:IPR for software and services) that will take place on the second day of the conference, 29th of August. The focus of the discussion will be on the economic aspects of patenting software and services, such as the effect of patents on diffusion and further innovation in software and whether open source software has changed the economics of software and IPRs. Due to the number of participants all interventions will be limited to 15 minutes.

#WwW: Participation in the conference will be by invitation only.  About 100 participants are expected.  Most will come from patent offices and patent related professions, some from various universitarian disciplines, from the business world and from non-governmental organisations.

#fnm: Draft Version of 2003/07/23

#Aed: Agenda

#ewp: overview webpage

#Wrm: OECD IPR Conference 2003/08/28-9

#rre: OECD has been working on a project on %(q:IPR, innovation and economic performance) since 2002 under supervision of Dominic Guellec and Catalina Martinez.  Around this project a conference was organised in August 2003 in Paris.

#uWW: Introduction to OECD activities in this area

#CRj: OECD IPR Project

#erN: Speaker's Note

#ioW: Registration Form Data

#oaW: Short Biography of Hartmut Pilch

#Eso: FFII/Eurolinux Message to the Conference

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpoecd038 ;
# txtlang: eo ;
# multlin: t ;
# End: ;

