\begin{subdocument}{swpparl0348}{2003/04/08 Strasbourg: Softpat Directive and Europe's Legal Structure}{http://swpat.ffii.org/events/2003/europarl/04/08/index.en.html}{Workgroup\\swpatag@ffii.org}{Europe's Substantive Patent Law is already 100\percent{} harmonised by the European Patent Convention of 1973 (EPC), to which all EU member states adhere.  The European Parliament's Legal Affairs Commission (JURI) is discussing and a proposal to further ``harmonise'' and ``clarify'' the ``patentability of computer-implemented inventions'' in Europe by making the practise of the European Patent Office (EPO) on this matter legally binding for Europe, thus in effect making sure that computer programs and calculation rules are patentable, whereas Art 52 EPC and the EPO guidelines of 1978 say that programs and algorithms are not inventions, regardless of whether they are ``computer-implemented'' or not.  Law Scholars who were consulted by the European Parliament and other EU institutions have vehemently criticized this ``Software Patent Directive Proposal''.  Some point that the directive, far from achieving its stated aims of ``harmonisation'' and ``clarification'', makes the law less clear than ever and offers no means of preventing divergence between national caselaw systems.  Moreover there are concerns about the proper division of powers between the legislative and the judicative.  It may not be possible and desirable at all to clarify and ``harmonise'' the rules on what constitutes a patentable invention by mens of a directive.  Independently of this, patents are restrictions on citizens' basic freedom of action, as guaranteed by national constitutions and EU law, and this at least need to be carefully justified by a public benefit.  In the case of software patents, some derived freedoms and human rights, including freedom of publication and property in one's original work (copyright), are directly infringed, and no public benefit seems to be visible.  On April 8 and 9 we convene a conference of legal experts in and near the European Parliament in order to discuss these issues.}
\begin{sect}{tmplok}{Time and Place}
Tuesday 2003/04/08 14.00-16.00 Strasburg LoW S2.3

This is a working meeting of the Greens/EFA group which is coorganised with the Eurolinux Alliance

Simultaneous interpretation is provided at least for DE-EN and FR-EN
\end{sect}

\begin{sect}{prog}{2003/04/08 14.00-16.00 LoW S23: Software Patents: Fixing the CEC/McCarthy Proposal}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
14.00 & Introduction: What is at Stake & Prof. Fran\c{c}ois P\'{e}l\'{e}grini\\\hline
14.30 & JURI and CULT approaches: a diametrical contrast & Jozéf Halbersztadt, patent examiner, Warszawa, Poland\\\hline
15.00 & Amendment Proposals: Basic Structure and Recitals & Hartmut Pilch, ffii.org\\\hline
15.30 & Amendment Proposals: The Articles & RA J\"{u}rgen Siepmann, linux-verband.de\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{links}{Relevant Reading}
\begin{itemize}
\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analysis/testsuite/index.en.html}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003-02-19: Amended Software Patent Directive Proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0302/index.en.html}}}

\begin{quote}
Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with additional safeguards to align Europe on the US practise and make sure that there can be no limit on patentability.  McCarthy reiterates the CEC/BSA software patent advocacy and misrepresents the wide-spread criticism without citing any of it.  Even economic and legal expertises ordered by the European Parliament and other critical opinions of EU institutions are not taken into account.  McCarthy's economic argumentation consists of tautologies and unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R\&D, that SMEs need european software patents in order to compete in the USA, that patents are needed to keep developping countries at bay.  McCarthy uses the term ``computer-implemented inventions'' as a synonym for ``software innovations''.  These ``by their very nature belong to a field of technology''.  McCarthy insists that ``irreconcilable conflicts'' with the EPO must be avoided.  McCarthy says she wants to ``set clear limits as to what is patentable'' -- and that she wants to avoid the ``sterile discussions'' about ``technical effects'' and ``exclusions from patentability''.  Yet her proposal stays confined to such discussions.  McCarthy demands that all useful ideas, including algorithms and business methods, must be patentable as ``computer-implemented inventions''.  McCarthy proposes to recognise the EPO as Europe's supreme patent legislator and to make decisions of a few influential people at the EPO irreversible and binding for all of Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Information Economy and Swpat Conference Paris 20020610-1\footnote{http://swpat.ffii.org/events/2002/ifri06/index.en.html}}}

\begin{quote}
Institut Fran\c{c}ais des Relations Internationales (IFRI.org) and Center of Information Policy Research at Mariland University (CIP.umd.org) are organising a transatlantic conference on information economy and in particular on the limits of patentability as well as the problems in neighboring areas such as database exclusion rights and copyright.  Hartmut Pilch is participating on behalf of FFII and Eurolinux on two of the panels.
\end{quote}
\filbreak

\item
{\bf {\bf Swpat Conference Amsterdam 2002-08-30..1 (Columbanus Symposium)\footnote{http://swpat.ffii.org/events/2002/ivir08/index.en.html}}}

\begin{quote}
Hartmut Pilch is attending a conference hosted by Prof. Bernt Hugenholz and Reinier Bakels from University of Amsterdam about the typology of innovations in the software and business method area and the implications of various rules for defining what is patentable, including the European Commission's recent proposal for a directive and hopefully also our widely supported counter-proposal.
\end{quote}
\filbreak

\item
{\bf {\bf Kiesewetter-K\"{o}binger 2000: \"{U}ber die Patentpr\"{u}fung von Programmen f\"{u}r Datenverarbeitungsanlagen\footnote{http://swpat.ffii.org/papers/grur-skk01/index.de.html}}}

\begin{quote}
A patent examiner analyses the inconsistencies of examining and granting patents on computer programs. In an attempt to reinterpret the law, which clearly prohibits the granting of patents on computer programs, jurisprudence has gradually allowed the granting of functional claims which allow the applicant to disguise a computer program. But these functional claims represent problems rather than solutions, the solution being a (non-patentable) computer program as such. Patenting problems is however even less permissible and even more harmful in its consequences.
\end{quote}
\filbreak

\item
{\bf {\bf Sch\"{o}lch 2001: Softwarepatente ohne Grenzen?\footnote{http://swpat.ffii.org/papers/grur-schoelch01/index.en.html}}}

\begin{quote}
In 2000 the 10th Senate of the German Federal Court of Justice (BGH/10) published the verdicts ``Sprachanalyse'' (Language Analysis) and ``Logikverifikation'' (logic verification) and with them a new doctrine that makes anything patentable that can be described as a ``program-technical device''.  The BGH/10 overruled decisions of another court that had rejected the same patent applications due to lack of technicity (technical character).  The 17th Senate of the Federal Patent Court (BPatG/17) had applied the ``core theory'', i.e. differentiated between new and old technology and demanded that the new and inventive part (i.e. the core of the invention) be in the ``technical'' realm, i.e. that it contribute a ``teaching in the area of applied natural science'', outside the scope of the list of exclusions on \S{}1(2) PatG aka Art 52(2) EPC.  The new verdict will on the contrary admit any claims even if only a non-inventive periphery is within the ``technical sphere''.  Applied to organ building this would mean that not only a new way to build organ pipes would be considered a technical invention, but also a new piece of music played on the organ, as the author of this article observes.  G\"{u}nter Sch\"{o}lch, who is confronted with dubious software patents every day in his work at the German Patent Office, finds the BGH/10 decision unconvincing and warns that they will lead to a flood of harmful patents.  Sch\"{o}lch also reviews the process of of patent inflation (gradual expansion of the scope of patentability) during the last decade and warns about dangerous social consequences.
\end{quote}
\filbreak

\item
{\bf {\bf G\"{u}nter Sch\"{o}lch: Comments on the Consultation Paper\footnote{http://swpat.ffii.org/papers/eukonsult00/angumema/index.en.html}}}

\begin{quote}
An examiner of the German Patent Office points out that the EU patent department's consultation paper uses a meaningless concept of technical character and sticks to a shoddy reasoning that dates back to the EPO caselaw of the Sohei decision (1986).  It is this sophistry which has created the legal insecurity in Europe, and perpetuating this sophistry just perpetuates legal insecurity.  If we want to tackle the legal insecurity, we need to break with this sophistry, delete the much-abused 'as such' clause (Art 52(3) EPC) and reestablish clear and consistent definitions of what is a technical invention.   It is not enough that the claims contain technical features.  Not the claim wording but the invention must be technical.  The EPO's approach is to compare even non-technical problem solutions with with non-technical closest prior art and then deciding whether any technical feature is contained in the difference.  This is illogical and circular.  Such approaches, as proposed also in the consultation paper, are apparently using the ``technical character'' doctrine only as a cover-up for their real purpose, which is to make anything man-made under the sun patentable, like in the US.  However this could have even graver consequences in Europe than in the US, given that the EPO and European courts tends to apply formalistic rules that favor the patentee and make claims have a broader effect than the same claims would have in the US.  On the whole it can be said that the adventure of expanding patentability to all ideas constitutes a rupture of occidental civilisation and a course into a brave new world whose outline is just gradually appearing on the horizon in the US.  This adventure is undertaken in spite of strong scientific evidence in its disfavor, supported only by the irrational belief of a well entrenched lobby in the universally beneficial effect of property rights.
\end{quote}
\filbreak

\item
{\bf {\bf Karl-Friedrich Lenz\footnote{http://www.k.lenz.name}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

