<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Europe's Substantive Patent Law is already 100% harmonised by the European Patent Convention of 1973 (EPC), to which all EU member states adhere.  The European Parliament's Legal Affairs Commission (JURI) is discussing and a proposal to further %(q:harmonise) and %(q:clarify) the %(q:patentability of computer-implemented inventions) in Europe by making the practise of the European Patent Office (EPO) on this matter legally binding for Europe, thus in effect making sure that computer programs and calculation rules are patentable, whereas Art 52 EPC and the EPO guidelines of 1978 say that programs and algorithms are not inventions, regardless of whether they are %(q:computer-implemented) or not.  Law Scholars who were consulted by the European Parliament and other EU institutions have vehemently criticized this %(q:Software Patent Directive Proposal).  Some point that the directive, far from achieving its stated aims of %(q:harmonisation) and %(q:clarification), makes the law less clear than ever and offers no means of preventing divergence between national caselaw systems.  Moreover there are concerns about the proper division of powers between the legislative and the judicative.  It may not be possible and desirable at all to clarify and %(q:harmonise) the rules on what constitutes a patentable invention by mens of a directive.  Independently of this, patents are restrictions on citizens' basic freedom of action, as guaranteed by national constitutions and EU law, and this at least need to be carefully justified by a public benefit.  In the case of software patents, some derived freedoms and human rights, including freedom of publication and property in one's original work (copyright), are directly infringed, and no public benefit seems to be visible.  On April 8 and 9 we convene a conference of legal experts in and near the European Parliament in order to discuss these issues.
title: 2003/04/08 STB: Softpat Directive and Europe's Legal Structure
opD: Simultaneous interpretation is provided at least for DE-EN and FR-EN

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpparl034 ;
# txtlang: en ;
# End: ;

