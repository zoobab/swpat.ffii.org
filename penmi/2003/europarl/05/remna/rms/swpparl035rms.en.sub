\begin{subdocument}{swpparl035rms}{2003/05/07-8 BXL Swpat Conference: RMS Messages and Impressions}{http://swpat.ffii.org/events/2003/europarl/05/contrib/rms/index.en.html}{Workgroup\\swpatag@ffii.org}{Richard M. Stallman (RMS), computer science pioneer at MIT, author of GNU Emacs and various GNU operating system utilities and founder of the Free Software Foundation, spoke both in the European Parliament and a the Dorint Symposium, explaining basics of the patent problem in a very lively way.  MEP Elly Plooij-Van Gorsel came to explain why she is proposing to legalise software patents and how she wants to limit the problems created thereby.  This led to a clash with an impatient software developper in the audience, thus prompting Plooij to walk out in displeasure.  Richard regrets this communication failure.}
\begin{sect}{text}{Impressions from Greens/EFA Conference}
On May 8, the Green Party organized an event for representatives of small and medium-size software companies to speak about the expected impact of software patents on their business.  Numerous speakers explained how owning a few software patents would be useless to them, how what they need is protection from the software patents that large companies will own.

Elly Plooij-Van Gorsel, a member of the parliament's Committee on Industry Trade and Research came briefly to speak--but not to listen.  She told us what that committee has done to try to help small and medium enterprises cope with software patents.

She started by announcing she did not care about small and medium enterprises very much: ``I have had several other industries in my office on the other side.''  She did not say who they were, but we can guess.  ``In the parliament we have to find a balance.''  Balance appears to mean giving megacorporations half of whatever they ask for.  If they ask to plant 100,000 landmines for other programmers, ``balance'' means they can plant 50,000.

She then told us about three amendments proposed by the Industry Committee in order to help us.  One is null, one makes things worse, and one might indeed solve part of the problem.

\begin{itemize}
\item
Set a ``higher standard'' of inventiveness, demand that there be a ``significant contribution to the state of the art''.

This pious proposal will alter nothing.  It is up to the patent offices to interpret the standard, and they tend to apply whatever standards they are given in a formalistic manner.  If they decide to point at the most trivial matter and call it ``significant'', it will be very difficult to prove them wrong.  Nobody can control them, except courts, one patent at a time.  Moreover, small developers normally can't afford the price of going to court.

\item
Make it slightly easier for small companies to obtain software patents.

Today, in Europe, any publication of the idea before the patent application was filed counts as prior art and nominally makes the patent invalid.  The change would be to allow patent applications up to a year after the idea has been published.  In other words, ``When we let the megacorporations plant 50,000 land mines, we will help you--by removing one of the limits on your getting one landmine that you can plant.''

But it's worse than that, because this change would increase the number of patents on all sides and create additional insecurity as to whether an idea is freely usable or not.   This would make the problem worse, not better.

\item
Exclude interfaces from coverage by software patents.  Something like this could protect some parts of our software from the threat of patents, but it would only be a partial improvement.  It would be like armor for our feet and hands and eyes, but none for our torsos. This isn't enough to make a minefield safe.

Also, the actual wording adopted in ITRE Amendment 15

\begin{quote}
{\it Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.}
\end{quote}

may still not be clear enough.

If ``two different computer systems'' is interpreted as including two different application programs, then this would allow you to implement a converter between Word format and some other data format, supposing that you can find some other data format to represent the data which isn't itself restricted by patents.

If, however, governments and courts interpret ``computer systems'' in a narrower sense, this would apply only to conversion between two different architectures of computer, such as the PC and the Macintosh.  With that interpretation, it would make nearly no difference.
\end{itemize}

If Ms Plooj-Van Gorsel had come to listen and not just to talk, she might have learned what the real problem is.  It is not too late.

Copyright 2003 Richard Stallman\\
Verbatim copying and redistribution of this entire article are permitted without royalty in any medium provided this notice is preserved.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Plooij/ITRE Counter-Proposal: Publication and Interoperation do not Infringe\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itre0212/index.en.html}}}

\begin{quote}
The European Parliament's industry committee (ITRE) is working on a counter-proposal for the CEC/BSA software directive proposal, based on a draft by its rapporteur Elly Plooij Van Gorsel, a dutch liberal Member of the European Parliament (MEP).  Plooij's first draft states that patents are not needed for protecting the interests of software creators and proposes to assure that programs can at least be freely distributed without any fear of patent liabilities.  Moreover, any use of a patented method for the purpose of interoperability (compliance with de facto standards) is not considered to be an infringement.  The proposal is strong on defininig these limits but stops short of correcting CEC/BSA misconceptions about ``computer-implemented invention'', ``technical contribution'' etc, thus leaving it undecided whether Amazon One-Click and the like are patentable inventions or not.  Current law says they are not, but the European Patent Office (EPO) says they are, and the European Commission wants to bring the law in line with the EPO's position.  The ITRE counter-proposal solves only part of the problem, but fellow MEPs in ITRE have tabled amendments.  Some of these are designed to complete the solution, others to dismantle it.
\end{quote}
\filbreak

\item
{\bf {\bf 2003/05/07-08 BXL: Software Patents: From Legal Wordings to Economic Reality\footnote{http://swpat.ffii.org/events/2003/europarl/05/index.en.html}}}

\begin{quote}
During this two-day interdisciplinary conference in Brussels, near and in the European Parliament, we will bring together programmers, engineers, entrepreneurs, law scholars, economists and politicians to explore the whole chain of causality from proposed patent law regulations to European policy goals, such as promoting innovation, competition, enterpreneurial spirit and consumer protection, unbureaucratic and target-oriented governance, legal security, favorable conditions for small and medium enterprises and ``becoming the world's most competitive information society by 2010''.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

