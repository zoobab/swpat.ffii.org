<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Während dieser zweitägigen interdisziplinären Konferenz in Brüssel -- im und am Europaparlament -- bringen wir Programmierer, Ingenieure, Unternehmer, Rechtsgelehrte, Volkswirte und Politiker zusammen, um die gesamte Kausalkette von der Patentgesetzgebung bis zu den politischen Zielen der EU zu untersuchen, wie etwa Förderung von Innovation, Wettbewerb, Unternehmertum und Verbraucherschutz, schlanke und effiziente Verwaltung, Rechtssicherheit, günstige Bedingungen für Kleine und Mittlere Unternehmen (KMU) und %(q:bis 2010 zur wettbewerbsfähigsten Informationsgesellschaft der Welt zu werden).
title: 2003/05/07-08 BXL: Softwarepatente: Von Juristischen Wortspielen zur Ökonomischen Wirklichkeit
mnl: Zeit und Ort
prog: Programm
aWo: Organisatoren und Förderer
sra: Simultandolmetschen ist zumindest für die Richtungen DE-EN und FR-EN während der ganzen Veranstaltung verfügbar.
Whe: Wann?
Wee: Wo?
Wha: Was?
WWt: Wissenschaftliche Konferenz: von Juristischen Wortspielen zu Erteilten Softwarepatenten
Dne: Abendessen
rlr: Konferenz der Grünen im Europarl
rSE: Softwarepatente und Kleine & Mittlere Unternehmen
teW: Festliche Veranstaltung mit Kabarett
nfx: Wirkung von Softwarepatenten auf die Wirtschaft
nti: Open Society Institute
eWW: mehr bald aufzulisten
0en: 2003/05/08 Brussels Software Patent Event Organisation Wiki
bmW: Alexandre Dulaunoy de AEL.be et amis de toute l'Europe travaillent ensemble pour soutenir l'évenement sur les brevets logiciels
see: aus Anlass der Tagung in Bruessel
pea: Numerous photos from most of the sessions of this 2-day conference
efh: Dr. Lenz on the incoherence of the Software Patent Directive Proposal of the European Commission and Arlene McCarthy, with references to our conference
eaW: Bericht über die Konferenz
nWW: enthält Reden und Rednerlebensläufe
mra: starts from morning of 2003/05/07, but first few minutes of Lessig speach seem to be lost

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpparl035 ;
# txtlang: de ;
# End: ;

