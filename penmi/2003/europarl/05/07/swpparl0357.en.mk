# -*- mode: makefile -*-

swpparl0357.en.lstex:
	lstex swpparl0357.en | sort -u > swpparl0357.en.lstex
swpparl0357.en.mk:	swpparl0357.en.lstex
	vcat /ul/prg/RC/texmake > swpparl0357.en.mk


swpparl0357.en.dvi:	swpparl0357.en.mk
	rm -f swpparl0357.en.lta
	if latex swpparl0357.en;then test -f swpparl0357.en.lta && latex swpparl0357.en;while tail -n 20 swpparl0357.en.log | grep -w references && latex swpparl0357.en;do eval;done;fi
	if test -r swpparl0357.en.idx;then makeindex swpparl0357.en && latex swpparl0357.en;fi

swpparl0357.en.pdf:	swpparl0357.en.ps
	if grep -w '\(CJK\|epsfig\)' swpparl0357.en.tex;then ps2pdf swpparl0357.en.ps;else rm -f swpparl0357.en.lta;if pdflatex swpparl0357.en;then test -f swpparl0357.en.lta && pdflatex swpparl0357.en;while tail -n 20 swpparl0357.en.log | grep -w references && pdflatex swpparl0357.en;do eval;done;fi;fi
	if test -r swpparl0357.en.idx;then makeindex swpparl0357.en && pdflatex swpparl0357.en;fi
swpparl0357.en.tty:	swpparl0357.en.dvi
	dvi2tty -q swpparl0357.en > swpparl0357.en.tty
swpparl0357.en.ps:	swpparl0357.en.dvi	
	dvips  swpparl0357.en
swpparl0357.en.001:	swpparl0357.en.dvi
	rm -f swpparl0357.en.[0-9][0-9][0-9]
	dvips -i -S 1  swpparl0357.en
swpparl0357.en.pbm:	swpparl0357.en.ps
	pstopbm swpparl0357.en.ps
swpparl0357.en.gif:	swpparl0357.en.ps
	pstogif swpparl0357.en.ps
swpparl0357.en.eps:	swpparl0357.en.dvi
	dvips -E -f swpparl0357.en > swpparl0357.en.eps
swpparl0357.en-01.g3n:	swpparl0357.en.ps
	ps2fax n swpparl0357.en.ps
swpparl0357.en-01.g3f:	swpparl0357.en.ps
	ps2fax f swpparl0357.en.ps
swpparl0357.en.ps.gz:	swpparl0357.en.ps
	gzip < swpparl0357.en.ps > swpparl0357.en.ps.gz
swpparl0357.en.ps.gz.uue:	swpparl0357.en.ps.gz
	uuencode swpparl0357.en.ps.gz swpparl0357.en.ps.gz > swpparl0357.en.ps.gz.uue
swpparl0357.en.faxsnd:	swpparl0357.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpparl0357.en-??.g3n`;source faxsnd main
swpparl0357.en_tex.ps:	
	cat swpparl0357.en.tex | splitlong | coco | m2ps > swpparl0357.en_tex.ps
swpparl0357.en_tex.ps.gz:	swpparl0357.en_tex.ps
	gzip < swpparl0357.en_tex.ps > swpparl0357.en_tex.ps.gz
swpparl0357.en-01.pgm:
	cat swpparl0357.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swpparl0357.en-%02d.pgm -
swpparl0357.en/swpparl0357.en.html:	swpparl0357.en.dvi
	rm -fR swpparl0357.en;latex2html swpparl0357.en.tex
xview:	swpparl0357.en.dvi
	xdvi -s 8 swpparl0357.en &
tview:	swpparl0357.en.tty
	browse swpparl0357.en.tty 
gview:	swpparl0357.en.ps
	ghostview  swpparl0357.en.ps &
print:	swpparl0357.en.ps
	lpr -s -h swpparl0357.en.ps 
sprint:	swpparl0357.en.001
	for F in swpparl0357.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpparl0357.en.faxsnd
	faxsndjob view swpparl0357.en &
fsend:	swpparl0357.en.faxsnd
	faxsndjob jobs swpparl0357.en
viewgif:	swpparl0357.en.gif
	xv swpparl0357.en.gif &
viewpbm:	swpparl0357.en.pbm
	xv swpparl0357.en-??.pbm &
vieweps:	swpparl0357.en.eps
	ghostview swpparl0357.en.eps &	
clean:	swpparl0357.en.ps
	rm -f  swpparl0357.en-*.tex swpparl0357.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpparl0357.en-??.* swpparl0357.en_tex.* swpparl0357.en*~
swpparl0357.en.tgz:	clean
	set +f;LSFILES=`cat swpparl0357.en.ls???`;FILES=`ls swpparl0357.en.* $$LSFILES | sort -u`;tar czvf swpparl0357.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
