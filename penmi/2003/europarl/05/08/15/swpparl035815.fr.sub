\begin{subdocument}{swpparl035815}{2003/05/08 BXL 15:00: Impacte \'{E}conomique des Brevets Logiciels}{http://swpat.ffii.org/dates/2003/europarl/05/08/15/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/08/25 par Odile BÉNASSY}{Dans cette partie finale de n\^{o}tre conf\'{e}rence autour du Parlament Europ\'{e}en nous convenons de programmeurs, ing\'{e}nieurs, entrepreneurs, juristes, \'{e}conomistes et politiciens ensemble pour discuter comment les brevets logiciels influencent le cadre r\'{e}gulatoire sous lequel les Petites et Moyennes Entreprises (PME) agissent.  On veut obtenir plus de clarit\'{e} sur la question de l'impacte de la l\'{e}gislation propos\'{e}e en vue de objectifs politiques de l'UE comme innovation, comp\'{e}tition, esprit d'entreprise, protection de consommateurs, administration mince, s\'{e}curit\'{e} l\'{e}gale, s\'{e}curit\'{e} de r\'{e}seaux informatiques, E-Europe, E-Inclusion et ``devenir la soci\'{e}t\'{e} d'information la plus comp\'{e}titive du monde jusqu'\`{a} 2010''.}
\begin{sect}{tmplok}{Temp et Lieu}
\begin{description}
\item[Temp:]\ Jeudi 2003/05/08 15.00-19.00
\item[Lieu:]\ Dorint Hotel
Bruxelles
Boulevard Charlemagne 11-19 (\`{a} cot\'{e} de la Commission Europ\'{e}enne, 5-10 minutes a pi\'{e}d du Parlament Europ\'{e}en)
\end{description}
\end{sect}

\begin{sect}{prog}{Programme}
\begin{center}
\begin{longtable}{|C{09}|C{39}|C{39}|}
\hline
quand? & a propos de quoi? & qui?\\\hline
\endhead
15.00 & {\bf Brevets, Portefeuilles et PMEs}\par

{\small Research shows that SMEs are skeptical about the value of patents.  How do the numbers play out in the software industry with the great variation in size and business model?  Do portfolios permit large companies to ``tax''?}\par

New Bessen \& Hunt Study with Statistical Data about patterns in software patents and their use & \begin{itemize}
\item
Puay Tang (University of Sussex)

\item
Prof. Roberto DiCosmo (informaticist, Paris)

\item
Brian Kahin (professor for information policy studies, Univ. of Michigan)

\item
Sylvain Perchaud (president, europe-shareware.org)

\item
Matthieu Farcot (doctorand in information economics, Univ. Strasbourg)

\item
Jorge Cortell (software entrepreneur and professor for ecommerce law)\footnote{Unfortunately Jorge can not join us due to a traffic accident in his family.}
\end{itemize}\\\hline
16.00 & {\bf Brevets Logiciels comme Outils Fiscaux}
{\small \begin{itemize}
\item
G\'{e}n\'{e}rer des Transactions Virtuelles par Inventions Virtuelles -- Brevets Logiciels comme Outils d'\'{E}vasion Fiscale

\item
Patents and the Stock Market -- Companies' Annual reports list patent numbers as proofs of strength and correlate them to R\&D efforts.  MEP Arlene McCarthy cites such ``proven correlations'' as a motive for legalising software patents.   What do patent statistics really mean?
\end{itemize}} & Dr. Jean-Paul Smets-Solanes (CEO, nexedi.com)\par

Cortell\par

MEP N.N.\\\hline
16.45 & caf\'{e}\\\hline
17.00 & {\bf Patents, Standards, Interoperability and Competition}\par

{\small Patents have made the work of standardisation bodies such as the W3C difficult and have often led to the exclusion of free software and shareware from the use of standards.  Competition authorities in the US and Europe have tended to be critical of patents, and the software patent directive proposal contains provisions which (pretend to) address this issue by an interoperability privilege, which some MEPs have proposed to further clarify and strengthen.  Competition regulation is often seen at the forefront of efforts to limit the excesses of the patent system and similar systems.  What can be achieved by this approach?} & \begin{itemize}
\item
Richard Clark (elysium.ltd.uk, JPEG Standardisation, Royaume Uni)

\item
Peter Holmes (University of Sussex)

\item
H\aa{}kon Wium Lie (CTO, opera.com)

\item
Perchaud

\item
Mozelle W. Thompson (Commissioner, US Federal Trade Commission)
\end{itemize}\\\hline
18.00 & {\bf Bridging the Gap between Patent Science and Patent Policy}\par

{\small Fritz Machlup described the history of the patent system as a ``victorious movement of lawyers against economists''.  How have social science and patent legislation interacted since then? Why did so few scientists take part in the European Commission's consultations?  Why did economic studies practically without influence on the legislative process at the Commission, the Council and JURI?} & \begin{itemize}
\item
Tang

\item
Holmes

\item
DiCosmo

\item
Bakels

\item
Retureau

\item
Kahin
\end{itemize}\\\hline
20.00 & Dinner\par

location disclosed to participants\\\hline
\end{longtable}
\end{center}
\end{sect}

\begin{sect}{links}{Liens Annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf 2003/05/07 BXL: Brevets Logiciels -- De la S\'{e}mantique Juridique aux Brevets Logiciels Accord\'{e}s\footnote{http://swpat.ffii.org/dates/2003/europarl/05/07/index.fr.html}}}

\begin{quote}
Pendant cette premi\`{e}re journ\'{e}e d'une conf\'{e}rence interdisciplinaire de deux jours \`{a} Bruxelles au sein et a cot\'{e} du Parlament Europ\'{e}en, nous menons ensemble des programmeurs, ing\'{e}nieurs, entrepreneurs, juristes, \'{e}conomistes et politiciens pour \'{e}tudier toute la chaine de causalit\'{e} entre les r\`{e}gles de brevetabilit\'{e} propos\'{e}s et les buts et valeurs politiques de l'Union Europ\'{e}enne, comme innovation, comp\'{e}tition, adminstration mince, s\'{e}curit\'{e} juridique, s\'{e}curit\'{e} de r\'{e}seaux, e-Europe, e-inclusion et ``devenir la soci\'{e}t\'{e} d'information la plus comp\'{e}titive jusqu'\`{a} 2010''.
\end{quote}
\filbreak

\item
{\bf {\bf Recherches sur les effets macro\'{e}conomiques du syst\`{e}me de brevets\footnote{http://swpat.ffii.org/archive/miroir/sisku/index.fr.html}}}

\begin{quote}
Depuis le rapport de Fritz Machlup d\'{e}livr\'{e} au congr\'{e}s am\'{e}ricain en 1958, il y a toute une s\'{e}rie d'\'{e}tudes sur les \'{e}ffets du syst\`{e}me de brevets sur diverses domaines de l'\'{e}conomie
\end{quote}
\filbreak

\item
{\bf {\bf Citations sur la Question de la Brevetabilit\'{e} des R\`{e}gles d'Organisation et de Calcul\footnote{http://swpat.ffii.org/archive/citations/index.fr.html}}}

\begin{quote}
Citations de textes juridiques, analyses \'{e}conomiques, document politiques ainsi que \'{e}nonc\'{e}s de programmeurs, politiciens et autres partis qui s'int\'{e}ressent au d\'{e}bat sur les limites de la brevetabilit\'{e} vis-a-vis le logiciel
\end{quote}
\filbreak

\item
{\bf {\bf Ensemble de tests pour la l\'{e}gislation sur les limites de la brevetabilit\'{e}\footnote{http://swpat.ffii.org/analyse/tests/index.fr.html}}}

\begin{quote}
Pour tester la capacit\'{e} d'un loi sur la brevetabilit\'{e}, nous devons essayer des innovations exemples.  Chaque exemple est d\'{e}crit par un \'{e}tat de la technique, un enseignement technique et une s\'{e}rie de revendications.  Dans l'hypoth\`{e}se que ces d\'{e}scriptions sont pertinentes, nous essayons notre nouvelle r\`{e}gle de loi.  Notre attention se porte sur (1) la clart\'{e} (2) l'\'{e}ffet macro-\'{e}conomique du resultat:  la r\'{e}glementation propos\'{e}e m\`{e}ne-t-elle \`{a} une d\'{e}cision pr\'{e}visible?  Quelles revendications seront accept\'{e}es?  Ce r\'{e}sultat exprime-t-il nos souhaits?  Nous essayons diff\'{e}rentes propositions de lois sur la m\^{e}me s\'{e}rie d'exemples (Testsuite) et comparons lesquelles r\'{e}ussissent le mieux.  Pour un programmeur c'est une question d'honneur que de ``supprimer les erreurs avant de diffuser le programme'' (first fix the bugs, then release the code).  Les ensembles de tests sont un moyen connu pour atteindre ce but.  D'apr\`{e}s l'article 27 ADPIC (TRIPS) la l\'{e}gislation appartient \`{a} un ``domaine de la technique'' notamment ``d'ing\'{e}nierie sociale'' (social engineering), n'est-ce pas ?  Technicit\'{e} ici ou l\`{a}, il est temps d'aborder de ce c\^{o}t\'{e} la l\'{e}gislation avec cette rigueur m\'{e}thodique, qui est partout annonc\'{e}e, o\`{u} les mauvaises d\'{e}cisions de construction peuvent fortement porter atteinte \`{a} la vie des individus.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003-02-19: Propos de Directive Brevets Logiciels\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/amccarthy0302/index.en.html}}}

\begin{quote}
Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with additional safeguards to align Europe on the US practise and make sure that there can be no limit on patentability.  McCarthy reiterates the CEC/BSA software patent advocacy and misrepresents the wide-spread criticism without citing any of it.  Even economic and legal expertises ordered by the European Parliament and other critical opinions of EU institutions are not taken into account.  McCarthy's economic argumentation consists of tautologies and unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R\&D, that SMEs need european software patents in order to compete in the USA, that patents are needed to keep developping countries at bay.  McCarthy uses the term ``computer-implemented inventions'' as a synonym for ``software innovations''.  These ``by their very nature belong to a field of technology''.  McCarthy insists that ``irreconcilable conflicts'' with the EPO must be avoided.  McCarthy says she wants to ``set clear limits as to what is patentable'' -- and that she wants to avoid the ``sterile discussions'' about ``technical effects'' and ``exclusions from patentability''.  Yet her proposal stays confined to such discussions.  McCarthy demands that all useful ideas, including algorithms and business methods, must be patentable as ``computer-implemented inventions''.  McCarthy proposes to recognise the EPO as Europe's supreme patent legislator and to make decisions of a few influential people at the EPO irreversible and binding for all of Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Swpat Conference Amsterdam 2002-08-30..1 (Columbanus Symposium)\footnote{http://swpat.ffii.org/dates/2002/ivir08/index.en.html}}}

\begin{quote}
Hartmut Pilch is attending a conference hosted by Prof. Bernt Hugenholz and Reinier Bakels from University of Amsterdam about the typology of innovations in the software and business method area and the implications of various rules for defining what is patentable, including the European Commission's recent proposal for a directive and hopefully also our widely supported counter-proposal.
\end{quote}
\filbreak

\item
{\bf {\bf Information Economy and Swpat Conference Paris 20020610-1\footnote{http://swpat.ffii.org/dates/2002/ifri06/index.en.html}}}

\begin{quote}
Institut Fran\c{c}ais des Relations Internationales (IFRI.org) and Center of Information Policy Research at Mariland University (CIP.umd.org) are organising a transatlantic conference on information economy and in particular on the limits of patentability as well as the problems in neighboring areas such as database exclusion rights and copyright.  Hartmut Pilch is participating on behalf of FFII and Eurolinux on two of the panels.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

