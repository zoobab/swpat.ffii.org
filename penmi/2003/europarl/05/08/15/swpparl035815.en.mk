# -*- mode: makefile -*-

swpparl035815.en.lstex:
	lstex swpparl035815.en | sort -u > swpparl035815.en.lstex
swpparl035815.en.mk:	swpparl035815.en.lstex
	vcat /ul/prg/RC/texmake > swpparl035815.en.mk


swpparl035815.en.dvi:	swpparl035815.en.mk
	rm -f swpparl035815.en.lta
	if latex swpparl035815.en;then test -f swpparl035815.en.lta && latex swpparl035815.en;while tail -n 20 swpparl035815.en.log | grep -w references && latex swpparl035815.en;do eval;done;fi
	if test -r swpparl035815.en.idx;then makeindex swpparl035815.en && latex swpparl035815.en;fi

swpparl035815.en.pdf:	swpparl035815.en.ps
	if grep -w '\(CJK\|epsfig\)' swpparl035815.en.tex;then ps2pdf swpparl035815.en.ps;else rm -f swpparl035815.en.lta;if pdflatex swpparl035815.en;then test -f swpparl035815.en.lta && pdflatex swpparl035815.en;while tail -n 20 swpparl035815.en.log | grep -w references && pdflatex swpparl035815.en;do eval;done;fi;fi
	if test -r swpparl035815.en.idx;then makeindex swpparl035815.en && pdflatex swpparl035815.en;fi
swpparl035815.en.tty:	swpparl035815.en.dvi
	dvi2tty -q swpparl035815.en > swpparl035815.en.tty
swpparl035815.en.ps:	swpparl035815.en.dvi	
	dvips  swpparl035815.en
swpparl035815.en.001:	swpparl035815.en.dvi
	rm -f swpparl035815.en.[0-9][0-9][0-9]
	dvips -i -S 1  swpparl035815.en
swpparl035815.en.pbm:	swpparl035815.en.ps
	pstopbm swpparl035815.en.ps
swpparl035815.en.gif:	swpparl035815.en.ps
	pstogif swpparl035815.en.ps
swpparl035815.en.eps:	swpparl035815.en.dvi
	dvips -E -f swpparl035815.en > swpparl035815.en.eps
swpparl035815.en-01.g3n:	swpparl035815.en.ps
	ps2fax n swpparl035815.en.ps
swpparl035815.en-01.g3f:	swpparl035815.en.ps
	ps2fax f swpparl035815.en.ps
swpparl035815.en.ps.gz:	swpparl035815.en.ps
	gzip < swpparl035815.en.ps > swpparl035815.en.ps.gz
swpparl035815.en.ps.gz.uue:	swpparl035815.en.ps.gz
	uuencode swpparl035815.en.ps.gz swpparl035815.en.ps.gz > swpparl035815.en.ps.gz.uue
swpparl035815.en.faxsnd:	swpparl035815.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpparl035815.en-??.g3n`;source faxsnd main
swpparl035815.en_tex.ps:	
	cat swpparl035815.en.tex | splitlong | coco | m2ps > swpparl035815.en_tex.ps
swpparl035815.en_tex.ps.gz:	swpparl035815.en_tex.ps
	gzip < swpparl035815.en_tex.ps > swpparl035815.en_tex.ps.gz
swpparl035815.en-01.pgm:
	cat swpparl035815.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swpparl035815.en-%02d.pgm -
swpparl035815.en/swpparl035815.en.html:	swpparl035815.en.dvi
	rm -fR swpparl035815.en;latex2html swpparl035815.en.tex
xview:	swpparl035815.en.dvi
	xdvi -s 8 swpparl035815.en &
tview:	swpparl035815.en.tty
	browse swpparl035815.en.tty 
gview:	swpparl035815.en.ps
	ghostview  swpparl035815.en.ps &
print:	swpparl035815.en.ps
	lpr -s -h swpparl035815.en.ps 
sprint:	swpparl035815.en.001
	for F in swpparl035815.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpparl035815.en.faxsnd
	faxsndjob view swpparl035815.en &
fsend:	swpparl035815.en.faxsnd
	faxsndjob jobs swpparl035815.en
viewgif:	swpparl035815.en.gif
	xv swpparl035815.en.gif &
viewpbm:	swpparl035815.en.pbm
	xv swpparl035815.en-??.pbm &
vieweps:	swpparl035815.en.eps
	ghostview swpparl035815.en.eps &	
clean:	swpparl035815.en.ps
	rm -f  swpparl035815.en-*.tex swpparl035815.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpparl035815.en-??.* swpparl035815.en_tex.* swpparl035815.en*~
swpparl035815.en.tgz:	clean
	set +f;LSFILES=`cat swpparl035815.en.ls???`;FILES=`ls swpparl035815.en.* $$LSFILES | sort -u`;tar czvf swpparl035815.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
