# -*- mode: makefile -*-

swpparl035809.en.lstex:
	lstex swpparl035809.en | sort -u > swpparl035809.en.lstex
swpparl035809.en.mk:	swpparl035809.en.lstex
	vcat /ul/prg/RC/texmake > swpparl035809.en.mk


swpparl035809.en.dvi:	swpparl035809.en.mk
	rm -f swpparl035809.en.lta
	if latex swpparl035809.en;then test -f swpparl035809.en.lta && latex swpparl035809.en;while tail -n 20 swpparl035809.en.log | grep -w references && latex swpparl035809.en;do eval;done;fi
	if test -r swpparl035809.en.idx;then makeindex swpparl035809.en && latex swpparl035809.en;fi

swpparl035809.en.pdf:	swpparl035809.en.ps
	if grep -w '\(CJK\|epsfig\)' swpparl035809.en.tex;then ps2pdf swpparl035809.en.ps;else rm -f swpparl035809.en.lta;if pdflatex swpparl035809.en;then test -f swpparl035809.en.lta && pdflatex swpparl035809.en;while tail -n 20 swpparl035809.en.log | grep -w references && pdflatex swpparl035809.en;do eval;done;fi;fi
	if test -r swpparl035809.en.idx;then makeindex swpparl035809.en && pdflatex swpparl035809.en;fi
swpparl035809.en.tty:	swpparl035809.en.dvi
	dvi2tty -q swpparl035809.en > swpparl035809.en.tty
swpparl035809.en.ps:	swpparl035809.en.dvi	
	dvips  swpparl035809.en
swpparl035809.en.001:	swpparl035809.en.dvi
	rm -f swpparl035809.en.[0-9][0-9][0-9]
	dvips -i -S 1  swpparl035809.en
swpparl035809.en.pbm:	swpparl035809.en.ps
	pstopbm swpparl035809.en.ps
swpparl035809.en.gif:	swpparl035809.en.ps
	pstogif swpparl035809.en.ps
swpparl035809.en.eps:	swpparl035809.en.dvi
	dvips -E -f swpparl035809.en > swpparl035809.en.eps
swpparl035809.en-01.g3n:	swpparl035809.en.ps
	ps2fax n swpparl035809.en.ps
swpparl035809.en-01.g3f:	swpparl035809.en.ps
	ps2fax f swpparl035809.en.ps
swpparl035809.en.ps.gz:	swpparl035809.en.ps
	gzip < swpparl035809.en.ps > swpparl035809.en.ps.gz
swpparl035809.en.ps.gz.uue:	swpparl035809.en.ps.gz
	uuencode swpparl035809.en.ps.gz swpparl035809.en.ps.gz > swpparl035809.en.ps.gz.uue
swpparl035809.en.faxsnd:	swpparl035809.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpparl035809.en-??.g3n`;source faxsnd main
swpparl035809.en_tex.ps:	
	cat swpparl035809.en.tex | splitlong | coco | m2ps > swpparl035809.en_tex.ps
swpparl035809.en_tex.ps.gz:	swpparl035809.en_tex.ps
	gzip < swpparl035809.en_tex.ps > swpparl035809.en_tex.ps.gz
swpparl035809.en-01.pgm:
	cat swpparl035809.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swpparl035809.en-%02d.pgm -
swpparl035809.en/swpparl035809.en.html:	swpparl035809.en.dvi
	rm -fR swpparl035809.en;latex2html swpparl035809.en.tex
xview:	swpparl035809.en.dvi
	xdvi -s 8 swpparl035809.en &
tview:	swpparl035809.en.tty
	browse swpparl035809.en.tty 
gview:	swpparl035809.en.ps
	ghostview  swpparl035809.en.ps &
print:	swpparl035809.en.ps
	lpr -s -h swpparl035809.en.ps 
sprint:	swpparl035809.en.001
	for F in swpparl035809.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpparl035809.en.faxsnd
	faxsndjob view swpparl035809.en &
fsend:	swpparl035809.en.faxsnd
	faxsndjob jobs swpparl035809.en
viewgif:	swpparl035809.en.gif
	xv swpparl035809.en.gif &
viewpbm:	swpparl035809.en.pbm
	xv swpparl035809.en-??.pbm &
vieweps:	swpparl035809.en.eps
	ghostview swpparl035809.en.eps &	
clean:	swpparl035809.en.ps
	rm -f  swpparl035809.en-*.tex swpparl035809.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpparl035809.en-??.* swpparl035809.en_tex.* swpparl035809.en*~
swpparl035809.en.tgz:	clean
	set +f;LSFILES=`cat swpparl035809.en.ls???`;FILES=`ls swpparl035809.en.* $$LSFILES | sort -u`;tar czvf swpparl035809.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
