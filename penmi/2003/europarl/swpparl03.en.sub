\begin{subdocument}{swpparl03}{Software Patent Discussions in and near the European Parliament in 2003}{http://swpat.ffii.org/events/2003/europarl/index.en.html}{Workgroup\\swpatag@ffii.org}{The European Parliament may pass or reject the Software Patentability Directive Proposal of the European Commission immediately after plenary discussion on 2003-09-01.  The most likely course is that it will propose amendments.  Currently many members of the three concerned commissions (juri, itre, cult) have lost confidence in the Newspeak from the European Patent Office (EPO), in which the proposal is written.  We are trying to keep track of the Parliament's schedule and to organise some complementary occasions for an informed discussion.  In fact we want more than that: justice.  The patent lobby has trampled on our rights without justification and is asking MEPs to perpetuate the injustice.  We ask for a fair trial.  Only the European Parliament can offer it.}
\begin{sect}{sched}{Proposed Schedule}
The debate about software patents is characterised by misunderstandings.  The European Commission (CEC) has burdened the debate with ambiguous insider language of the European Patent Office (EPO).  Discussions between software professionals, economists, lawyers and politicians often lead to little progress, because each side interprets the terminology in a different way.

Unfortunately we are dealing with a very serious decision that deeply affects the so-called knowledge-based society and economy and may cause considerable damage.

By participating in our hearings and seminars you learn more about how software development and software business works, and all of us become fluent in EPO/CEC Patent Newspeak and at the same time develop more appropriate concepts which enable software developpers, lawyers and politicians to communicate meaningfully about the subject.

Here are some key dates:

\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
date & place (BXL/STB) & Contents\\\hline
2003/09/23 &  & \begin{itemize}
\item
Plenary Decision on Software Patent Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html}

\item
Action Week against EU Software Patent Plans\footnote{http://swpat.ffii.org/news/03/demo0914/index.en.html}

\item
JURI Discussion on IP Enforcement Directive\footnote{http://www.ffii.org/proj/euipd/index.en.html}: Postponed
\end{itemize}\\\hline
\multicolumn{3}{|c|}{2003/09 EP: Software Patent Directive Vote\footnote{http://swpat.ffii.org/events/2003/europarl/09/index.en.html}}\\\hline
2003/09/01 &  & Plenary Discussion of Software Patent Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}: Postponed\footnote{http://swpat.ffii.org/news/03/demo0827/index.en.html}\\\hline
2003/08/27 &  & {\bf Event in Brussels\footnote{http://swpat.ffii.org/events/2003/europarl/08/index.en.html} and Online Demo\footnote{http://swpat.ffii.org/group/demo/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/08/25-9 BXL: Software Patent Directive Amendments\footnote{http://swpat.ffii.org/events/2003/europarl/08/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/07/01-2 STB: Software Patent Directive Amendments\footnote{http://swpat.ffii.org/events/2003/europarl/07/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/06/24 BXL: Software Patents as Financial Tools\footnote{http://swpat.ffii.org/events/2003/europarl/06/24/index.en.html}}\\\hline
2003/06/17 &  & JURI vote\\\hline
\multicolumn{3}{|c|}{2003/06/17 BXL: The JURI Vote\footnote{http://swpat.ffii.org/events/2003/europarl/06/17/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/06/10 BXL: How to Patent Business Methods under the Proposed Directive\footnote{http://swpat.ffii.org/events/2003/europarl/06/10/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/06/03 STB: The JURI Software Patent Directive Proposal\footnote{http://swpat.ffii.org/events/2003/europarl/06/03/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/06 Europarl Software Patent Meetings\footnote{http://swpat.ffii.org/events/2003/europarl/06/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/05/07-08 BXL: Software Patents: From Legal Wordings to Economic Reality\footnote{http://swpat.ffii.org/events/2003/europarl/05/index.en.html}}\\\hline
\multicolumn{3}{|c|}{2003/04/08 STB: Softpat Directive and Europe's Legal Structure\footnote{http://swpat.ffii.org/events/2003/europarl/04/index.en.html}}\\\hline
2003/03/17,24 &  & JURI discussion\\\hline
2003/03/12 (3) 15.00-16.30\footnote{http://swpat.ffii.org/events/2003/europarl/03/index.en.html} & STB LOW S2.3 & {\bf Discussion}
\begin{enumerate}
\item
Proposed Directive and Amendments\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}

\item
McCarthy Report\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0302/index.en.html}
\end{enumerate}\\\hline
2003/02/12 (3) 14.00-16.00 & STB LOW S2.3 & Amendments\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}\\\hline
2003/01/15 (3) 14.00-15.00 & STB LOW S2.3 & Amendments\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf How you can help us end the software patent nightmare\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}
\filbreak

\item
{\bf {\bf I want to come to Strasburg/Brussels and help.  Where do I register?}}

\begin{quote}
In the FFII Participation System\footnote{http://aktiv.ffii.org/} under the menu item Event Participation
\end{quote}
\filbreak

\item
{\bf {\bf I'd like to talk to my MEP.  How should I do it?}}

\begin{quote}
see FFII/Eurolinux 2003/04 Letter to Software Creators and Users\footnote{http://swpat.ffii.org/letters/parl034/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf I want to support the FFII/Eurolinux activities with a donation.  How should I do it?}}

\begin{quote}
see FFII/Eurolinux 2003/04 Letter to Software Creators and Users\footnote{http://swpat.ffii.org/letters/parl034/index.en.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Europarl Hearings 2002-11-07 and 26\footnote{http://swpat.ffii.org/events/2002/europarl11/index.en.html}}}

\begin{quote}
On Nov 7, the European Parliament's Legal Affairs Committee (JURI) conducted a hearing about the proposed software patentability directive.  10 invited experts were given 5-10 minutes of time to respond to a series of questions which imply that software ideas must be patentable inventions and that opposition to this line can be dismissed as minoritarian.  The bias of this hearing was, as it turned out, the fruit of intense lobbying by industrial patent lawyers.  In spite of the initial bias however, most MEPs did begin to question what they had been taught.  They more or less noticed that the directive proposal hands over a carte blanche to patent courts in a matter where basic freedoms of citizens, including many of the most productive software businesses of Europe, are at stake.  Another hearing on 2002-11-26 helped Europe's legislature gain confidence needed to set clear and adequate rules for the patent jurisdiction to follow.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}}}

\begin{quote}
The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. computer, i/o, memory etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property-based property rights of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations that have been laid down in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.
\end{quote}
\filbreak

\item
{\bf {\bf JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/juri0304/index.en.html}}}

\begin{quote}
Members of the European Parliament's Commission on Legal Affairs and the Internal Market (JURI) submitted amendments to the European Commission's software patent directive proposal. While some MEPs are asking to bring the directive in line with Art 52 EPC so as to clearly restate that programs for computers are not patentable inventions, another group of MEPs is endorsing the EPO's recent practice of unlimited patentability, shrouded in more or less euphemistic wordings. Among the latter, some propose to make programs directly claimable, so as to ensure that software patents are not only granted but achieve maximal blocking effects.  This latter group obtained a 2/3 majority, with some exceptions.  We document in tabular form what was at stake, what various parties recommended, and what JURI finally voted for on 2003/06/17.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003/05/03: Software Patent Directive Proposal FAQ\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0305/index.en.html}}}

\begin{quote}
Arlene McCarthy, member of the European Parliament and rapporteur of the Legal Affairs Commission (JURI) on the Software Patentability Directive Proposal explains her point of view in a FAQ manner.  She asked us to distribute this document to the participants of a conference which we organised in Brussels.  In a nutshell, she says that \begin{enumerate}
\item
Software patents, as granted by the European Patent Office (EPO) at present, are needed for various reasons, e.g. protecting Europe against competition from Asia, allowing European SMEs to compete in the US, etc

\item
The EPO is granting software patents but not patents on non-technical algorithms and business methods.

\item
The current proposal is designed to ensure that the EPO's practise is followed throughout Europe in a uniform manner and that a drift toward patenting of ``non-technical algorithms and business methods'' is halted.
\end{enumerate}  We debug McCarthy's questions and answers one by one.  It seems that many of McCarthy's proclaimed objectives could be achieved only by voting in favor of a series of CULT, ITRE and JURI amendment proposals which unfortunately have not enjoyed the support of the rapporteur.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analysis/testsuite/index.en.html}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf Call for Action\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players.  Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}}}

\begin{quote}
The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. computer, i/o, memory etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property-based property rights of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations that have been laid down in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.
\end{quote}
\filbreak

\item
{\bf {\bf European Parliament and Software Patents\footnote{http://swpat.ffii.org/players/europarl/index.en.html}}}

\begin{quote}
The European Parliament has less rights than a national parliament.  Most importantly, it cannot itself propose legislation and it has only a negative voting power that again cannot block all EU legislation projects.  However it has the right to block the CEC/BSA software patentability directive proposal.  Within the European Parliament, the Legal Affairs Committee (JURI) is in charge, its rapporteur being Arlene McCarthy, a british labor MEP.  McCarthy has so far shown herself very unwilling to consider any arguments other than those coming from CEC and EPO.  She has published a few papers that look as if ghostwritten bey CEC/EPO and has proposed to conduct a hearing where only the EPO lobby is heard.  Due to pressure from other MEPs, some critical voices were also invited.  The Committees for Inudstry (rapporteur Van Gorsel) and Culture (rapporteur Rocard) will give an opinion on the matter.  General Directorate IV has commissioned a study from the University of Amsterdam which was fairly critical, as have quite a few MEPs.  But since JURI is in charge, there is a real danger that lawyer thinking (delving into textual grammar, worshipping court authorities and caselaw, not thinking about laws as means to an end, favoring measures that create employment for lawyers) will prevail over considerations of public interest.
\end{quote}
\filbreak

\item
{\bf {\bf Computer-Implemented Open Society 2003\footnote{http://swpat.ffii.org/group/internal/todo/cios3/index.en.html}}}

\begin{quote}
Plan proposed to the Open Society Institute, which is supporting our activity with a major donation.
\end{quote}
\filbreak

\item
{\bf {\bf Planning and Reporting to Supporters\footnote{http://swpat.ffii.org/group/support/index.en.html}}}

\begin{quote}
How the FFII has been using your money and what it plans to do with further donations
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

