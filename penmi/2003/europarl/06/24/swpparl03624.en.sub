\begin{subdocument}{swpparl03624}{2003/06/24 BXL: Software Patents as Financial Tools}{http://swpat.ffii.org/events/2003/europarl/06/24/index.en.html}{Workgroup\\swpatag@ffii.org}{By applying the concept of ``invention'' to software as proposed by the European Commission, the EU would unleash a stream of broad and trivial patents, which engender legal insecurity, do not protect software products and in most cases do not earn their owners any licensing revenues.  Yet these patents can be of substantial value to companies in rather surprising ways.  They allow companies, especially large corporations, to control their money flow and save taxes.  At this short public meeting with members of the European Parliament we will discuss the economics of software patents and the questions at stake, one week before a possible plenary vote on the software patent directive proposal.}
\begin{sect}{temp}{Time \& Place}
\begin{center}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
2003/06/17 09.00 & EP & entry to the parliament\\\hline
2003/06/17 12.30 & EP & \\\hline
2003/06/24 14.00-16.00 & EP & Public Meeting\\\hline
2003/06/24 14.00 &  & Erik Josefsson: Introduction of Speakers and Subjects\\\hline
2003/06/24 14.20 &  & N.N.: Basics of Patent Legislation Benchmarking -- The JURI Proposal and the Alternatives\footnote{http://swpat.ffii.org/analysis/testsuite/index.en.html}\\\hline
2003/06/24 15.00 &  & Dr. Jean-Paul Smets-Solanes: Generating Virtual Transactions with Virtual Inventions -- How Software Patents substitute R\&D and help companies save taxes\\\hline
2003/06/24 15.30 &  & Discussion\\\hline
2003/06/17 09.00 & EP & \\\hline
\end{tabular}
\end{center}
\end{center}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/juri0304/index.en.html}}}

\begin{quote}
Members of the European Parliament's Commission on Legal Affairs and the Internal Market (JURI) submitted amendments to the European Commission's software patent directive proposal. While some MEPs are asking to bring the directive in line with Art 52 EPC so as to clearly restate that programs for computers are not patentable inventions, another group of MEPs is endorsing the EPO's recent practice of unlimited patentability, shrouded in more or less euphemistic wordings. Among the latter, some propose to make programs directly claimable, so as to ensure that software patents are not only granted but achieve maximal blocking effects.  This latter group obtained a 2/3 majority, with some exceptions.  We document in tabular form what was at stake, what various parties recommended, and what JURI finally voted for on 2003/06/17.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analysis/testsuite/index.en.html}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf Bessen \& Hunt 2003/05 Study\footnote{http://www.researchoninnovation.org/swpat.pdf}}}

\begin{quote}
rich empirical data show that (1) software patents do not work ``just like any other kind of patents'', (2) software patenting has lead substituted rather than promoted innovation in the US
\end{quote}
\filbreak

\item
{\bf {\bf FFII Community Tool\footnote{http://aktiv.ffii.org/?l=en}}}

\begin{quote}
Allows you to register for our meeting, to contact MEPs, and to cooperate in other ways
\end{quote}
\filbreak

\item
{\bf {\bf EU event planning mailing list\footnote{http://lists.ffii.org/mailman/listinfo/bxl/}}}
\filbreak

\item
{\bf {\bf FFII/Eurolinux 2003/04 Letter to Software Creators and Users\footnote{http://swpat.ffii.org/letters/parl034/index.en.html}}}

\begin{quote}
The European Parliament is likely to ratify a Software Patent Directive, possibly with helpful amendments, in May.  As a software creator/user, your participation can make a difference, even if you are willing to spend less than 1 hour.  Here are a few simple and effective things to do.
\end{quote}
\filbreak

\item
{\bf {\bf Call for Action\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players.  Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}}}

\begin{quote}
The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. computer, i/o, memory etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property-based property rights of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations that have been laid down in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.
\end{quote}
\filbreak

\item
{\bf {\bf 2003/07/01-2 STB: Software Patent Directive Amendments\footnote{http://swpat.ffii.org/events/2003/europarl/07/index.en.html}}}

\begin{quote}
Time to work decide on submission of amendments to the software patent directive proposal is running out.  FFII has proposed one set of amendments that stick as closely as possible to the original proposal while debugging and somewhat simplifying it.  An alternative small set of amendments would ``cut the crap'' and rewrite the directive from scratch.  We present and explain the two approaches.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patent Discussions in and near the European Parliament in 2003\footnote{http://swpat.ffii.org/events/2003/europarl/index.en.html}}}

\begin{quote}
The European Parliament may pass or reject the Software Patentability Directive Proposal of the European Commission immediately after plenary discussion on 2003-09-01.  The most likely course is that it will propose amendments.  Currently many members of the three concerned commissions (juri, itre, cult) have lost confidence in the Newspeak from the European Patent Office (EPO), in which the proposal is written.  We are trying to keep track of the Parliament's schedule and to organise some complementary occasions for an informed discussion.  In fact we want more than that: justice.  The patent lobby has trampled on our rights without justification and is asking MEPs to perpetuate the injustice.  We ask for a fair trial.  Only the European Parliament can offer it.
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on computer-implemented rules of organisation and calculation (programs for computers).  Now Europe's patent movement is pressing to consolidate this practise by writing a new law.  Europe's programmers and citizens are facing considerable risks.  Here you find the basic documentation, starting from a short overview and the latest news.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

