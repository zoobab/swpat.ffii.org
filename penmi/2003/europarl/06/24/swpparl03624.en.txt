<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: By applying the concept of %(q:invention) to software as proposed by the European Commission, the EU would unleash a stream of broad and trivial patents, which engender legal insecurity, do not protect software products and in most cases do not earn their owners any licensing revenues.  Yet these patents can be of substantial value to companies in rather surprising ways.  They allow companies, especially large corporations, to control their money flow and save taxes.  At this short public meeting with members of the European Parliament we will discuss the economics of software patents and the questions at stake, one week before a possible plenary vote on the software patent directive proposal.
title: 2003/06/24 BXL: Software Patents as Financial Tools
ida: Time & Place
entry: entry to the parliament
ssd: Generating Virtual Transactions with Virtual Inventions -- How Software Patents substitute R&D and help companies save taxes
Wgc: Basics of Patent Legislation Benchmarking -- The JURI Proposal and the Alternatives
iui: Discussion
ooa: Allows you to register for our meeting, to contact MEPs, and to cooperate in other ways

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpparl03624 ;
# txtlang: en ;
# End: ;

