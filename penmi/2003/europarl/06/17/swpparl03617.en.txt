<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: A short public meeting between software stakeholders and members of the European Parliament to discuss about the JURI vote on the Directive Proposal, which is taking place on the same day.  This will also be a kickoff meeting for a campaign to inform MEPs in the plenary.
title: 2003/06/17 BXL: The JURI Vote
ida: Time & Place
entry: entry to the parliament
juri: Talks with MEPs and journalists near the JURI meeting site
ssd: Public Meeting in Visitors' Area with participation of MEPs and journalists
dpS: Informal talks in the room, while most of us are still near the JURI meeting site.
apt: What happened in JURI today?
odw: Who voted for what?
heW: Wich choices were made?
loR: What would be patentable/enforcable under the JURI proposal?
stl: Discussion
ooa: Allows you to register for our meeting, to contact MEPs, and to cooperate in other ways
emi: next meeting

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpparl03617 ;
# txtlang: en ;
# End: ;

