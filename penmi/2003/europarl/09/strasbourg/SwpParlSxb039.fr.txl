<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#eFs: En outre, la commission juridique discutera la proposition de la directive sur la protection de la propriété intellectuelle poussée par sa rapportrice Janelly Fourtou, qui, en combinaison avec la directive sur le brevet logiciel, menace chaque programmeur qui publi un logiciel avec la prosécution pénale.

#xhr: Le LUG de Strasbourg est un groupe d'utilisateurs de Linux et de Logiciel Libre actif sur la ville de Strasbourg et ses environs.  Ce groupe fait oeuvre d'éducation populaire en banalisant l'usage des Logiciels Libres et en organisant occasionnellement des conférences, des stands d'information, des journées de présentation ou des %(q:install-party) ouvertes à tout public.

#oWW: Autres contacts fournis sur demande

#eia: %(q:La vaste majorité de nos supporters ne sera certainement pas sur la Place Kléber le 23 septembre, ou aux autres manifestations. Ceux qui ne viendront pas à Strasbourg pourront manifester en ligne, par l'intermédiaire de leur serveur web ou d'autres services Internet), selon Hartmut Pilch, président de la FFII. %(q:Nous avons proposé une série de moyens pour ce faire. Il y a certainement un moyen pour chacun. Il est préférable de rendre l'accès à votre page d'accueil un peu plus difficile pour un ou deux jours, plutôt que de perdre votre liberté de publication pour les dix prochaines années. Si les rapporteurs des groupes des grands partis l'emportent, le droit d'auteur et la liberté de publication seront nuls et non avenus. Les programmeurs et les fournisseurs d'accès Internet deviendront régulièrement la cible de poursuites pour infraction de brevet. Si le rapport du JURI n'est pas drastiquement amendé, paragraphe par paragraphe, nous serons emprisonnés dans un système de brevetabilité illimitée des programmes et méthodes organisationnelles pendant les dix prochaines années, et notre industrie du logiciel sera à la merci de quelques grosses sociétés, principalement américaines et japonaises, qui possèdent deux tiers des brevets logiciels que l'%(tp|Office Européen des Brevets|OEB) a accordés, illégalement, depuis 1986. La date butoir pour l'examen démocratique a été fixée au 24 septembre, et la Semaine d'action pourrait bien être votre dernière chance de faire entendre votre voix dans le processus de décision européen sur les brevets.)

#kitem: Quoi?

#kilok: Où

#kiam: Quand

#gWa: L'événenement de Strasbourg du mardi 23 septembre, organisé par des groupes locaux en collaboration avec la FFII/Eurolinux, se déroulera comme suit:

#tno: %(q:Le 27 août, une manifestation dans et aux alentours du Parlement européen, à Bruxelles, a réuni 500 participants. Les dirigeants des communautés scientifiques et du monde du développement logiciel ont condamné la directive dans tous ses aspects au cours des derniers mois. Cependant, certains groupes influents du Parlement n'ont guère été impressionnés. Il est encore possible que le 24 septembre, le Parlement approuvera une directive %(et:légalisant les brevets sur les algorithmes et méthodes organisationnelles, tels %(q:Amazon One Click Shopping), rendant ceux-ci uniformément exécutoires dans toute l'UE),) comme l'explique Guy Brand, organisateur de la manifestation de Strasbourg. %(q:Un public toujours plus nombreux se rend très clairement compte de cela. Nous nous attendons à une participation plus grande encore cette fois-ci.)

#s0m: Derrière ces manifestations, on trouve une coalition d'organisations représentant pas moins de 2000 entreprises du secteur logiciel et de plus de 200 000 individus, pour la plupart des professionnels de l'informatique et les signataires d'une petition en-ligne.

#Int: De leur coté, le LUG de Strasbourg, la FFII et l'Alliance Eurolinux appellent à manifester à Strasbourg le mardi 23 septembre 2003 à partir de 11h00 place Kléber pour une marche en direction du Parlement Européen. Cette action sur le terrain sera appuyée par des manifestations en-ligne.

#hoW: Le groupe Verts/ALE au Parlement européen invite à une %(ge:conférence de presse avec des invités prestigieux le 17 septembre 2003 à Bruxelles).

#ras: La proposition de directive concernant les brevets logiciels, qui sera soumise au Parlement Européen durant la session du 22 septembre, donne lieu à une vague de protestations sur toute l'Europe.

#dokurl: URL permanente de cette dépêche

#tgr: About Strasbourg Linux User Group

#ffii: A propos de la FFII

#eurolinux: A propos de l'Alliance Eurolinux

#WWt: World-editable webspace provided by ael.be for planning the demonstration in Strasburg 2003/09/23.

#iul: AelWiki: Strasburg Event Planning

#descr: Le Parlament Européen doit décider ce mois sur une proposition qui rendrait les algorithmes et méthodes d'affaires brevetables, imposant seulement des réstrictions rhétoriques qui n'excluent rien.  Si le Parlament vote pour cette proposition le 2003/09/24 à Strasbourg, il n'y aura peu de chance de décisions démocratiques sur les limites de la brevetabilité pendant beaucoup d'années.

#title: Strasbourg 2003/09/23

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpParlSxb039 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

