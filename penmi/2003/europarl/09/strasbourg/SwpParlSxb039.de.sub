\begin{subdocument}{SwpParlSxb039}{Strasbourg 2003/09/23}{http://swpat.ffii.org/termine/2003/europarl/09/strasbourg/index.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Das Europ\"{a}ische Parlament entscheidet \"{u}ber einen Richtlinienvorschlag, der Algorithmen und Gesch\"{a}ftsmethoden patentierbar macht.  Wenn das Parlament zustimmt, r\"{u}cken damit bis auf weiteres alle Chancen demokratischer Entscheidungsfindung zu diesem Thema in weite Ferne.}
\begin{sect}{detal}{details}
Der Vorschlag einer Direktive betreffs der Einf\"{u}hrung von Software-Patenten, die in der Session vom 22.09.2003 im europ\"{a}ischen Parlament unterbreitet werden wird, l\"{o}st in ganz Europa erneut eine Protest-Welle aus.

Die Gruppierung der Gr\"{u}nen/ALE im europ\"{a}ischen Parlament laden zu einer Pressekonferenz mit wichtigen Akteuren am 17.09.2003 in Br\"{u}ssel\footnote{http://www.greens-efa.org/en/press/detail.php?id=1521\&lg=en} ein.

Zus\"{a}tzlich rufen der Strasbourger LUG, die FFI und die Allianz EuroLinux zu einer Demonstration am 23.09.2003 in Strasbourg ab 11:00 place Kl\'{e}ber auf.  Diese Aktion wird durch Online-Aktionen unterst\"{u}tzt werden.

Hinter den Organisatoren dieser Demonstration befindet sich eine Koalition von Organisationen, die mehr als 2000 Firmen und 200 000 Einzelpersonen, meist in der Softwarebranche t\"{a}tig, vertreten. Dazu kommen noch die Unterschriften auf einer online-Petition.

``On August 27th, a rally in and near the European Parliament in Brussels attracted 500 participants. Leaders of the scientific communities and software business world have taken the directive proposal apart and condemned it in every respect during the last few mongth.  This has however left some influential groups in the Parliament unimpressed.  It still seems likely that the Parliament will pass a directive on September 24th which renders broad patents on trivial algorithms and business methods, such as Amazon One Click Shopping, legal and uniformly enforcable in Europe\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html},'' explains Guy Brand, organiser of the Strasburg rally. ``The EPO has granted more than 30,000 patents of this type against the letter and spirit of the written law, of which more 70\percent{} are held by companies from US and Japan.  By unleashing these patents, Europe would mutilate the most vital sectors of its economy.  More and more people are now seeing this very clearly. We expect a new record number of participants this time.''

Das Programm der Demo in Strasbourg sieht folgenderma{\ss}en aus:

\begin{center}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
When & Wo? & Was?\\\hline
11.00-11.30 & Place Kl\'{e}ber & Demonstrationszug in den Stra{\ss}en von Strasbourg bis zum europ\"{a}ischen Parlament\\\hline
12.30-14.00 & EP & Demonstration vor dem europ\"{a}ischen Parlament mit Auff\"{u}hrungen, Luftballons, Patentketten, Vortr\"{a}gen\\\hline
\end{tabular}
\end{center}
\end{center}

``Die gro{\ss}e Mehrheit unserer Unterst\"{u}tzer wird sicherlich nicht am 23 September da sein. Aber alle die nicht zur Demo nach Strasbourg kommen k\"{o}nnen, werden, dank ihrer Webserver oder anderer Internet-Dienste, an der Online-Demo teilnehmen k\"{o}nnen'', erkl\"{a}rt Hartmut Pilch, Vorsitzender des FFII. ``Wir schlagen mehrere Arten vor, um an dieser online-Demo teilzunehmen. Jeder wird sicherlich eine Art finden die ihm zusagt. Es ist besser den Zugang zu seiner Webseite f\"{u}r ein paar Tage zu erschweren, als f\"{u}r die n\"{a}chsten 10 Jahre die Ver\"{o}ffentlichungsfreiheit zu verlieren. Wenn die Berichterstatter der gro{\ss}en Parteien sich durchsetzen, werden Urheberrecht und Publikationsfreiheit wertlos werden.  Programmierer und Netzzugangsanbieter werden dann Zielscheibe f\"{u}r Abmahnungen und Patentprozesse.  Wenn der vorgeschlagene Gesetzestext nicht Abschnitt f\"{u}r Abschnitt umgeschrieben wird, werden wir im n\"{a}chsten Jahrzehnt in Europa mit einem Morast grenzenloser Patentierbarkeit von Rechenregeln und Gesch\"{a}ftsmethoden feststecken, und unsere Softwarepatente wird von der Gnade einiger gro{\ss}er Firmen meist amerikanischer und japanischer Herkunft abh\"{a}ngen, die 2/3 der Patente auf Rechenregeln und Gesch\"{a}ftsmethoden besitzen, welche das Europ\"{a}ische Patentamt (EPA) seit 1986 gesetzeswidrig erteilt hat.  Am 24. September l\"{a}uft die Frist f\"{u}r demokratische Kontrolle ab, und die Aktionswoche k\"{o}nnte sehr wohl Ihre letzte Chance darstellen, sich in Stra{\ss}burg Geh\"{o}r zu verschaffen.''
\end{sect}

\begin{sect}{media}{Kontakt}
\begin{description}
\item[E-Post:]\ media at ffii org
\item[Telefon:]\ Guy Brand +33-3-90245102 (Stra{\ss}burg)
Hartmut Pilch +49-89-18979927 (M\"{u}nchen)
Weitere Kontakte auf Anfrage
\end{description}
\end{sect}

\begin{sect}{eurolinux}{\"{U}ber die Eurolinux-Allianz -- www.eurolinux.org}
Das Eurolinux B\"{u}ndnis f\"{u}r eine Freie Informationelle Infrastruktur ist eine offene Koalition von Firmen und Verb\"{a}nden, die gemeinsam eine auf dem Urheberrecht, offenen Standards, freiem Wettbewerb und quelloffener Software wie Linux aufbauende kraftvolle europ\"{a}ische Softwarekultur f\"{o}rdern und sch\"{u}tzen wollen.  Firmenmitglieder und F\"{o}rderer von EuroLinux entwickeln oder verkaufen Software f\"{u}r Betriebsssysteme wie GNU/Linux, MacOS oder MSWindows unter freien, halb-freien oder propriet\"{a}ren Lizenzbestimmungen.
\end{sect}

\begin{sect}{ffii}{\"{U}ber den FFII -- www.ffii.org}
Der FFII ist ein in M\"{u}nchen eingetragener gemeinn\"{u}tziger Verein f\"{u}r Volksbildung im Bereich der Datenverarbeitung.  Der FFII unterst\"{u}tzt die Entwicklung \"{o}ffentlicher Informationsg\"{u}ter auf grundlage des Urheberrechts, freien Wettbewerbs und offener Standards.  \"{U}ber 300 Mitglieder, 700 Firmen und 50.000 Unterst\"{u}tzer haben den FFII mit der Vertretung ihre Interessen im Bereich der Gesetzgebung zu Software-Eigentumsrechten beauftragt.
\end{sect}

\begin{sect}{sbxlug}{\"{U}ber die Stra{\ss}burger Linux-Anwendergruppe -- strasbourg.linuxfr.org}
Die Stra{\ss}burger LUG ist eine Gruppierung von Anwendern freier Software in Stra{\ss}burg und Umgebung. Diese Gruppe engagiert sich f\"{u}r die Volksbildung durch Verbreitung  des Gebrauchs freier Software und durch gelegentliche Vortrags-Reihen, Informationsst\"{a}nde oder \"{o}ffentlicher ``Installationsfeste''.
\end{sect}

\begin{sect}{url}{Permanente Netzadresse dieser Presseerkl\"{a}rung}
http://swpat.ffii.org/termine/2003/europarl/09/strasbourg/index.de.html
\end{sect}

\begin{sect}{links}{}
\begin{itemize}
\item
{\bf {\bf AelWiki: Strasburg Event Planning\footnote{http://wiki.ael.be/index.php/EventStrasbourg}}}

\begin{quote}
World-editable webspace provided by ael.be for planning the demonstration in Strasburg 2003/09/23.
\end{quote}
\filbreak

\item
{\bf {\bf Action Week against EU Software Patent Plans\footnote{http://swpat.ffii.org/log/03/demo0914/index.en.html}}}

\begin{quote}
The Proposal for a software patent directive, which will be submitted to the European Parliament for decision on September 24th, is giving rise to yet another wave of protests. These include a conference in Brussels on Wednesday September 17th, a rally in Strasbourg on Tuesday September 23nd, as well as a series of ``satellite demos'' in other cities of Europe. These actions will be accompanied by an Internet Strike on the 17th and 23rd.  At a comparable action on Aug 27, 500 demonstrators came to Brussels and 3000 websites went on strike.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prop/index.de.html}}}

\begin{quote}
The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a business method that is claimed with the typical features of a computer program (e.g. computer, i/o, memory etc).  We have worked out a counter-proposal that upholds the freedom of computer-aided reasoning, calculating, organising and formulating and the copyright property-based property rights of software authors while supporting the patentability of technical inventions (problem solutions involving forces of nature) according to the differentiations that have been laid down in the European Patent Convention (EPC), the TRIPs treaty and the classical patent law literature.  This counter-proposal is receiving support from numerous prominent players in the fields of software, economics, politics and law.
\end{quote}
\filbreak

\item
{\bf {\bf JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/juri0304/index.en.html}}}

\begin{quote}
Members of the European Parliament's Commission on Legal Affairs and the Internal Market (JURI) submitted amendments to the European Commission's software patent directive proposal. While some MEPs are asking to bring the directive in line with Art 52 EPC so as to clearly restate that programs for computers are not patentable inventions, another group of MEPs is endorsing the EPO's recent practice of unlimited patentability, shrouded in more or less euphemistic wordings. Among the latter, some propose to make programs directly claimable, so as to ensure that software patents are not only granted but achieve maximal blocking effects.  This latter group obtained a 2/3 majority, with some exceptions.  We document in tabular form what was at stake, what various parties recommended, and what JURI finally voted for on 2003/06/17.
\end{quote}
\filbreak

\item
{\bf {\bf Warum Amazon One Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
Gem\"{a}ss des Richtlinienvorschlags COM(2002)92 der Europ\"{a}ischen Kommission (CEC) f\"{u}r ``Patentierbarkeit Computer-Implementierter Erfindungen'' und die \"{u}berarbeitete Version genehmigt durch das Komitee f\"{u}r Rechtsangelegenheiten und Binnenmarkt (JURI) des Europa Parlaments, sind Algorithmem und Gesch\"{a}ftmethoden, wie etwa Amazon One Click Shopping, ohne Zweifel als patentierbare Gegenst\"{a}nde zu betrachten. Die ist so weil \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO). The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Programmanspr\"{u}che: Verbote der Ver\"{o}ffentlichung N\"{u}tzlicher Patentbeschreibungen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prog/index.de.html}}}

\begin{quote}
Patentanspr\"{u}che auf ein ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', werden genannt ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' oder ``information claims''. Patente die diesen Anspruch enthalten werden manchmal auch ``text patents'' oder ``information patents'' genannt. Solche Patente monopolisieren nicht l\"{a}nger ein physikalisches Objekt sondern eine Beschreibung eines solchen Objekts.  Ob dies erlaubt werden sollte ist eine der kontroversen Fragen in der Auseinandersetzung um die vorgeschlagene EU Software Patent Richtlinie. Wir versuchten zu erkl\"{a}ren wie diese Debatte aufkam und was wirklich auf dem Spiel steht.
\end{quote}
\filbreak

\item
{\bf {\bf Interop\'{e}rabilit\'{e} et Brevet: Controverse au Parlement europ\'{e}en\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  After the summer pause of 2003, Arlene McCarthy MEP proposed an amendment to Art 6a which would render Art 6a meaningless.  The movement against Art 6a was joined by Wuermeling (EPP), Manders (ELDR) as well as the governments of UK and US.  Yet explanations as to what is wrong with the Interoperability Privilege remain very vague.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf Britische Regierung 2003/09/01 eilt McCarthy zu Hilfe\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/ukrep0309/index.en.html}}}

\begin{quote}
Le minist\`{e}re des affaires \'{e}trang\`{e}res du Royaume-Uni fait circuler  un ``compte-rendu aux MPE britanniques'', dans lequel il enjoint aux  membres du Parlement Europ\'{e}en britanniques de soutenir la position de  Arlene McCarthy et de voter (1) contre toute tentative de d\'{e}finir ce qui  est technique ou qui limite en quoi que ce soit ce qui est brevetable  (2) contre l'article 6a qui permet l'\'{e}criture de convertisseurs lorsque  les standards sont brevet\'{e}s (3) pour l'article 5 de la JURI qui interdit  la publication des descriptions d'inventions brevet\'{e}es sur le Net.  L'intervention du gouvernement survient alors que McCarthy a montr\'{e} des  signes de nervosit\'{e} en voyant l'amenuisement du soutien de son parti.  La d\'{e}claration du gouvernement peut \^{e}tre attribu\'{e}e \`{a} l'Office des  Brevets Britannique et son groupe de travail politique, essentiellement  constitu\'{e} d'avocats en brevets de grandes soci\'{e}t\'{e}s.  Ce groupe a  d\'{e}termin\'{e} la politique de brevets logiciels du Royaume-Uni et largement  aussi celle de l'Union Europ\'{e}enne ces derni\`{e}res ann\'{e}es.
\end{quote}
\filbreak

\item
{\bf {\bf Arlene McCarthy 2003/09/01: ``The Myths - The Truth''\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/amccarthy030901/index.en.html}}}

\begin{quote}
In response to the wave of protests against the proposed software patent directive COM(2002)92 2002/0047 in late August 2003, the European Parliament's rapporteur for this directive, Arlene McCarthy MEP, has published a ``Factsheet'' which attempts to explain that she has been a victim of a ``misinformation campaign'' and is in reality championning the protesters' cause.  We republish the paper with comments here.
\end{quote}
\filbreak

\item
{\bf {\bf Flugbl\"{a}tter\footnote{http://www.ffii.org/proj/swpat/pamflet/index.de.html}}}
\filbreak

\item
{\bf {\bf Proposition de directive sur la protection de la propri\'{e}t\'{e} intellectuelle COM (2003) 46(01)\footnote{http://www.ffii.org/proj/euipd/index.de.html}}}

\begin{quote}
Also, a debate on the IP Enforcement Directive is scheduled for 03/09/11.  This directive, in combination with modifications to the software patent directive pushed by its rapporteur Janelly Fourtou, would threaten anyone who publishes a program on the Internet with criminal prosecution.  FFII is supporting experts who are staying in the Parliament and organising events in and around the Parliament.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
% mode: latex ;
% End: ;

