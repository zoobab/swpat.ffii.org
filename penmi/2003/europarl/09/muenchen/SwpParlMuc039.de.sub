\begin{subdocument}{SwpParlMuc039}{Demonstration M\"{u}nchen 2003/09/19: Innovation statt Patentinflation!}{http://swpat.ffii.org/termine/2003/europarl/09/muenchen/index.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Der CSU, die sich viel auf das High-Tech-Land Bayern zu gute h\"{a}lt, soll noch einmal deutlich gemacht werden, da{\ss} die von ihr im Europa-Parlament vertretene Patentgesetzgebung Gift f\"{u}r die Innovationskraft der bayerischen Wirtschaft ist.  Dazu treffen sich zahlreiche Betroffene und Spitzenkandidaten des bayerischen Wahlkampfes am Freitag, den 19. September um 15.30 vor dem Europ\"{a}ischen Patentamt und um 17.00 am Odeonsplatz.}
\begin{sect}{intro}{CSU: ``n\"{a}her am Menschen'' oder ``n\"{a}her an der Patentlobby''?}
Das Europ\"{a}ische Parlament entscheidet am 24. September \"{u}ber einen Gesetzesvorschlag der Europ\"{a}ischen Kommission, der ``Patente auf Rechenregeln und Gesch\"{a}ftsmethoden wie Amazon One Click Shopping f\"{u}r rechtens erkl\"{a}ren und in ganz Europa unvermeidbar machen w\"{u}rde.''

Der Vorschlag f\"{u}r eine Richtlinie zu Softwarepatenten, der voraussichtlich am 24. September 2003 dem Europ\"{a}ischen Parlament zur Abstimmung vorliegt, hat bereits zu einer gro{\ss}en Demonstration in Br\"{u}ssel, einer Online-Demo\footnote{http://swpat.ffii.org/gruppe/demo/index.de.html} (3000 Websites) und einer Petition (\"{u}ber 250.000 Unterzeichner) gef\"{u}hrt. Um den Parlamentariern im Ged\"{a}chtnis zu halten, dass auch nach der zweiten Verschiebung der Abstimmung noch Handlungsbedarf besteht, gibt es eine Demonstration in Stra{\ss}burg\footnote{http://swpat.ffii.org/termine/2003/europarl/09/strasbourg/index.de.html} und (begleitend) in mehreren St\"{a}dten in Europa Aktionen und Infotische.

Wir bitten Sie, mit uns am {\bf Freitag, den 19. September 1530} vor der Vertretung der Europ\"{a}ischen Kommission und dem Europ\"{a}ischen Patentamt Ihre Sorge \"{u}ber Softwarepatente zu bekunden. Beide haben die Adresse Erhardtstr. 27, gegen\"{u}ber vom Deutschen Museum. Gut erreichbar per S-Bahn Isartor.

Die CSU spielt derzeit im Europaparlament eine f\"{u}hrende Rolle bei der Legalisierung von Patenten auf Rechenregeln und Gesch\"{a}ftsmethoden.

Der Berichterstatter der gr\"{o}{\ss}ten Fraktion des Europaparlamentes (EVP) in der Frage der Softwarepatente, Dr. Joachim Wuermeling\footnote{http://swpat.ffii.org/akteure/jwuermeling/index.de.html}, folgt stramm den Weisungen einer Handvoll Konzern-Patentjuristen, die am Europ\"{a}ischen Patentamt widerrechtlich Fakten geschaffen haben und bei der Europ\"{a}ischen Kommmission ebenfalls den Ton angeben. Wohlbegr\"{u}ndete Kritik praktisch aller mit dem Thema befassten Wissenschaftler, Programmierer und Unternehmer sowie hunderttausender B\"{u}rger wurden von dieser Gruppe und mit ihr der CSU bislang ignoriert.

Der CSU, die sich viel auf das High-Tech-Land Bayern zu gute h\"{a}lt, soll noch einmal deutlich gemacht werden, da{\ss} die von M.d.E.P. Wuermeling vertretene Gesetzgebung Gift f\"{u}r die Innovationskraft der bayerischen Wirtschaft ist.

Nach der Aktion kann deshalb, wer mag, mit zum Marienplatz gehen, wo zeitgleich die Wahlkampf-Abschlusskundgebung der CSU stattfindet, um dort noch einmal f\"{u}r unser Anliegen zu werben.
\end{sect}

\begin{sect}{plan}{Zeitplan}
\begin{description}
\item[15.30 vor dem Europ\"{a}ischen Patentamt:]\ Reden von
\begin{itemize}
\item
Monica Lochner-Fischer (SPD\footnote{http://swpat.ffii.org/akteure/spd/index.de.html}, Landtagskandidatin)

\item
Daniel Kosatschek (B90/Gr\"{u}ne, Landtagskandidat)

\item
Hartmut Pilch (FFII)

\item
usw.
\end{itemize}
\item[17.00 am Odeonsplatz:]\ Reden von
\begin{itemize}
\item
Jimmy Schulz (FDP, Landtagskandidat)

\item
Prof. Dr. Klaus Buchner (\"{O}DP, Bundesvorsitzender, Landtagskandidat)

\item
Wolfgang Kreissl-D\"{o}rfler (SPD, Mitglied des Europaparlaments, Sprecher der bayrischen Europaabgeordneten)
\end{itemize}
(Chor und Pantomime: ``Die Gedanken sind Patentiert'' siehe Die Gedanken sind ...\footnote{http://patinfo.ffii.org/})
\end{description}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Muenchen-parl\footnote{http://lists.ffii.org/mailman/listinfo/muenchen-parl/}}}

\begin{quote}
Diskussionsforum zur Vorbereitung des Ereignisses.
\end{quote}
\filbreak

\item
{\bf {\bf KWiki\footnote{http://kwiki.ffii.org/SwpParlMuc039De}}}

\begin{quote}
Gemeinsame Arbeitsfl\"{a}che zur Vorbereitung des Ereignisses
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Joachim Wuermeling MEP and Software Patents\footnote{http://swpat.ffii.org/akteure/jwuermeling/index.de.html}}}

\begin{quote}
Joachim Wuermeling, doctor of law, member of the European Parliament (MEP) for Bavaria's Christian Social Union (CSU), shadow rapporteur of the European People's Party (EPP) on the software patent directive, opposed everyting that could somehow limit patentability (e.g. interoperability privilege) and advocated everything that extends it beyond the European Commission's wishes (e.g. program claims).  Most EPP colleagues tend to blindly follow their shadow rapporteur.  Many critics have in vain sought dialogue with Wuermeling.  Wuermeling has however actively contacted the press in order to present himself as an opponent of ``patents on pure software and business methods'' and to ascribe contrary impressions of the public to ``misperceptions of the opensource lobby''.  In the JURI discussions of May 2003, Wuermeling tabled a pseudo-restrictive amendment (recital 13d), according to which a patent claim is limited to a specific product an may not encompass underlying algorithms.   Some of the corporate patent lawyers on whom Wuermeling is relying have been laughing about Wuermeling's lack of basic knowledge about the subject matter on which he is legislating.  One explanation for this lack of knowledge and unwillingness to meet critics is that Wuermeling is extremely busy with other subjects, such as the EU Constitution Convention.
\end{quote}
\filbreak

\item
{\bf {\bf Angelika Niebler und Softwarepatente\footnote{http://swpat.ffii.org/akteure/aniebler/index.de.html}}}

\begin{quote}
Als Mitglied des Industrie- und Rechtsausschusses hat Angelika Niebler sich f\"{u}r die Einf\"{u}hrung von Programmanspr\"{u}chen stark gemacht und konsequent gegen alle \"{A}nderngsantr\"{a}ge argumentiert und gestimmt, die zu einer Begrenzung der Patentierbarkeit h\"{a}tten f\"{u}hren k\"{o}nnen.  Sie erwies sich als rechte Hand ihres Kollegen Joachim Wuermeling bei der durchsetzung grenzenloser Patentierbarkeit im Sinne der Forderungen der Patentanw\"{a}lte von BDI und Bitkom.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{http://swpat.ffii.org/log/03/demo0827/index.en.html}}}

\begin{quote}
On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
% mode: latex ;
% End: ;

