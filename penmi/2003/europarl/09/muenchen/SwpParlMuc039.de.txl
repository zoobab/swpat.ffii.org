<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mrE: Gemeinsame Arbeitsfläche zur Vorbereitung des Ereignisses

#irr: Diskussionsforum zur Vorbereitung des Ereignisses.

#dDW: Chor und Pantomime: %(q:Die Gedanken sind Patentiert)

#eho: Sprecher der bayrischen Europaabgeordneten

#lEl: Mitglied des Europaparlaments

#nsi2: Landtagskandidat

#nrn: Bundesvorsitzender

#nsi: Landtagskandidat

#eno2: Reden von

#lWs: 17.00 am Odeonsplatz

#aui: Hartmut Pilch

#nsi3: Landtagskandidat

#nKs: Daniel Kosatschek

#nka: Landtagskandidatin

#SPD: SPD

#ihs: Monica Lochner-Fischer

#eno: Reden von

#Wot: 15.30 vor dem Europäischen Patentamt

#lle: Nach der Aktion kann deshalb, wer mag, mit zum Marienplatz gehen, wo zeitgleich die Wahlkampf-Abschlusskundgebung der CSU stattfindet, um dort noch einmal für unser Anliegen zu werben.

#ijv: Der CSU, die sich viel auf das High-Tech-Land Bayern zu gute hält, soll noch einmal deutlich gemacht werden, daß die von M.d.E.P. Wuermeling vertretene Gesetzgebung Gift für die Innovationskraft der bayerischen Wirtschaft ist.

#Wst: Der Berichterstatter der größten Fraktion des Europaparlamentes (EVP) in der Frage der Softwarepatente, %(jw:Dr. Joachim Wuermeling), folgt stramm den Weisungen einer Handvoll Konzern-Patentjuristen, die am Europäischen Patentamt widerrechtlich Fakten geschaffen haben und bei der Europäischen Kommmission ebenfalls den Ton angeben. Wohlbegründete Kritik praktisch aller mit dem Thema befassten Wissenschaftler, Programmierer und Unternehmer sowie hunderttausender Bürger wurden von dieser Gruppe und mit ihr der CSU bislang ignoriert.

#eig: Die CSU spielt derzeit im Europaparlament eine führende Rolle bei der Legalisierung von Patenten auf Rechenregeln und Geschäftsmethoden.

#nmv: Wir bitten Sie, mit uns am %(s:Freitag, den 19. September 15:30) vor der Vertretung der Europäischen Kommission und dem Europäischen Patentamt Ihre Sorge über Softwarepatente zu bekunden. Beide haben die Adresse Erhardtstr. 27, gegenüber vom Deutschen Museum. Gut erreichbar per S-Bahn Isartor.

#Wof: Der Vorschlag für eine Richtlinie zu Softwarepatenten, der voraussichtlich am 24. September 2003 dem Europäischen Parlament zur Abstimmung vorliegt, hat bereits zu einer %(bd:großen Demonstration in Brüssel), %(od:einer Online-Demo) (3000 Websites) und einer %(ep:Petition) (über 250.000 Unterzeichner) geführt. Um den Parlamentariern im Gedächtnis zu halten, dass auch nach der zweiten Verschiebung der Abstimmung noch Handlungsbedarf besteht, gibt es eine %(sd:Demonstration in Straßburg) und (begleitend) in mehreren Städten in Europa %(it:Aktionen und Infotische).

#atl: Das Europäische Parlament entscheidet am 24. September über einen Gesetzesvorschlag der Europäischen Kommission, der %(q:Patente auf Rechenregeln und Geschäftsmethoden wie Amazon One Click Shopping für rechtens erklären und in ganz Europa unvermeidbar machen würde.)

#etl: Zeitplan

#nea: CSU: %(q:näher am Menschen) oder %(q:näher an der Patentlobby)?

#descr: Der CSU, die sich viel auf das High-Tech-Land Bayern zu gute hält, soll noch einmal deutlich gemacht werden, daß die von ihr im Europa-Parlament vertretene Patentgesetzgebung Gift für die Innovationskraft der bayerischen Wirtschaft ist.  Dazu treffen sich zahlreiche Betroffene und Spitzenkandidaten des bayerischen Wahlkampfes am Freitag, den 19. September um 15.30 vor dem Europäischen Patentamt und um 17.00 am Odeonsplatz.

#title: Demonstration München 2003/09/19: Innovation statt Patentinflation!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpParlMuc039 ;
# txtlang: de ;
# multlin: t ;
# End: ;

