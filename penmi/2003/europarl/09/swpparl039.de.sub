\begin{subdocument}{swpparl039}{2003/09 EP: Entscheidung \"{u}ber Softwarepatent-Richtlinie}{http://swpat.ffii.org/termine/2003/europarl/09/index.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Das Europ\"{a}ische Parlament entscheidet \"{u}ber einen Richtlinienvorschlag, der Algorithmen und Gesch\"{a}ftsmethoden patentierbar macht.  Wenn das Parlament am 23. September in Stra{\ss}burg diesem Vorschlag zustimmt, r\"{u}cken damit bis auf weiteres alle Chancen demokratischer Entscheidungsfindung zu diesem Thema in weite Ferne.  FFII und Andere bereiten detaillierte Analysen und Abstimmempfehlungen vor und veranstalten Kundgebungen in mehreren St\"{a}dten, mit H\"{o}hepunkt in Stra{\ss}burg am 23. September, und einen Netzstreik.  In M\"{u}nchen und Wien nahmen jeweils 300 Personen teil.}
\begin{itemize}
\item
{\bf {\bf EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{http://swpat.ffii.org/log/03/demo0827/index.en.html}}}

\begin{quote}
On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/plen0309/index.de.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Warum Amazon One Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
Gem\"{a}ss des Richtlinienvorschlags COM(2002)92 der Europ\"{a}ischen Kommission (CEC) f\"{u}r ``Patentierbarkeit Computer-Implementierter Erfindungen'' und die \"{u}berarbeitete Version genehmigt durch das Komitee f\"{u}r Rechtsangelegenheiten und Binnenmarkt (JURI) des Europa Parlaments, sind Algorithmem und Gesch\"{a}ftmethoden, wie etwa Amazon One Click Shopping, ohne Zweifel als patentierbare Gegenst\"{a}nde zu betrachten. Die ist so weil \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO). The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Programmanspr\"{u}che: Verbote der Ver\"{o}ffentlichung N\"{u}tzlicher Patentbeschreibungen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prog/index.de.html}}}

\begin{quote}
Patentanspr\"{u}che auf ein ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', werden genannt ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' oder ``information claims''. Patente die diesen Anspruch enthalten werden manchmal auch ``text patents'' oder ``information patents'' genannt. Solche Patente monopolisieren nicht l\"{a}nger ein physikalisches Objekt sondern eine Beschreibung eines solchen Objekts.  Ob dies erlaubt werden sollte ist eine der kontroversen Fragen in der Auseinandersetzung um die vorgeschlagene EU Software Patent Richtlinie. Wir versuchten zu erkl\"{a}ren wie diese Debatte aufkam und was wirklich auf dem Spiel steht.
\end{quote}
\filbreak

\item
{\bf {\bf Interop\'{e}rabilit\'{e} et Brevet: Controverse au Parlement europ\'{e}en\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  After the summer pause of 2003, Arlene McCarthy MEP proposed an amendment to Art 6a which would render Art 6a meaningless.  The movement against Art 6a was joined by Wuermeling (EPP), Manders (ELDR) as well as the governments of UK and US.  Yet explanations as to what is wrong with the Interoperability Privilege remain very vague.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf US-Regierung an Euro-Parlamentarier: Art 6a (Interoperabilit\"{a}tsprivileg) muss weg!\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/usrep0309/index.en.html}}}

\begin{quote}
Die ``Gesandschaft der Vereinigten Staaten von Amerika in der Europ\"{a}ischen Union'' in Br\"{u}ssel hat ein langes Papier ``der USA'' \"{u}ber die geplante Softwarepatent-Richtlinie an zahlreiche Mitglieder des Europ\"{a}ischen Parlaments verschickt.  Darin warnen ``die USA'', dass Europa den TRIPs-Vertrag verletzen k\"{o}nnte, wenn es die Richtlinie wie vom Parlament ge\"{a}ndert verabschiedet.  Insbesondere glauben ``die USA'', dass die Konversion aus patentierten Dateiformaten grunds\"{a}tzlich nicht ohne Lizenz erlaubt sein darf, und fordern daher die Streichung von Art 6a.  Ferner zitieren ``die USA'' BSA-Studien, um einen dringenden Bedarf der Softwarebranche nach Patenten zu suggerieren und einzufordern, und zeigen sich beunruhigt \"{u}ber zweideutige Formulierungen des JURI-Entwurfs, die zwar den Trilateralen Standard der drei gro{\ss}en Patent\"{a}mter (US, EU, JP) umsetzen, aber Begeisterung daf\"{u}r vermissen und Schwierigkeiten f\"{u}r k\"{u}nftige Harmonisierungsverhandlungen bereiten k\"{o}nnten.  Diese Warnung folgt auf einen kurz zuvor abgesandten \"{a}hnlichen Brief der britischen Regierung.  Sie ist Teil eines ``Aktionsplanes'' des US-Patentamtes f\"{u}r eine ``Strategie f\"{u}r das 21. Jahrhundert'', ``die internationale Harmonisierung des Materiellen Patentrechts voranzutreiben'', um ``die Rechte amerikanicher Besitzer Geistigen Eigentums durch Erleichterung des Erwerbs internationalen Schutzes f\"{u}r ihre Erfindungen zu st\"{a}rken''.  Diesen Plan haben f\"{u}hrende Funktion\"{a}re des US-Patentamtes ist in letzter Zeit in multilateralen Foren wie WIPO, WSIS und OECD ebenso wie in bilateralen Verhandlungen mit zunehmender H\"{a}rte verfolgt.
\end{quote}
\filbreak

\item
{\bf {\bf Britische Regierung 2003/09/01 eilt McCarthy zu Hilfe\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/ukrep0309/index.en.html}}}

\begin{quote}
Le minist\`{e}re des affaires \'{e}trang\`{e}res du Royaume-Uni fait circuler  un ``compte-rendu aux MPE britanniques'', dans lequel il enjoint aux  membres du Parlement Europ\'{e}en britanniques de soutenir la position de  Arlene McCarthy et de voter (1) contre toute tentative de d\'{e}finir ce qui  est technique ou qui limite en quoi que ce soit ce qui est brevetable  (2) contre l'article 6a qui permet l'\'{e}criture de convertisseurs lorsque  les standards sont brevet\'{e}s (3) pour l'article 5 de la JURI qui interdit  la publication des descriptions d'inventions brevet\'{e}es sur le Net.  L'intervention du gouvernement survient alors que McCarthy a montr\'{e} des  signes de nervosit\'{e} en voyant l'amenuisement du soutien de son parti.  La d\'{e}claration du gouvernement peut \^{e}tre attribu\'{e}e \`{a} l'Office des  Brevets Britannique et son groupe de travail politique, essentiellement  constitu\'{e} d'avocats en brevets de grandes soci\'{e}t\'{e}s.  Ce groupe a  d\'{e}termin\'{e} la politique de brevets logiciels du Royaume-Uni et largement  aussi celle de l'Union Europ\'{e}enne ces derni\`{e}res ann\'{e}es.
\end{quote}
\filbreak

\item
{\bf {\bf Arlene McCarthy 2003/09/01: ``The Myths - The Truth''\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/amccarthy030901/index.en.html}}}

\begin{quote}
In response to the wave of protests against the proposed software patent directive COM(2002)92 2002/0047 in late August 2003, the European Parliament's rapporteur for this directive, Arlene McCarthy MEP, has published a ``Factsheet'' which attempts to explain that she has been a victim of a ``misinformation campaign'' and is in reality championning the protesters' cause.  We republish the paper with comments here.
\end{quote}
\filbreak

\item
{\bf {\bf Flugbl\"{a}tter\footnote{http://www.ffii.org/proj/swpat/pamflet/index.de.html}}}
\filbreak

\item
{\bf {\bf Proposition de directive sur la protection de la propri\'{e}t\'{e} intellectuelle COM (2003) 46(01)\footnote{http://www.ffii.org/proj/euipd/index.de.html}}}

\begin{quote}
Also, a debate on the IP Enforcement Directive is scheduled for 03/09/11.  This directive, in combination with modifications to the software patent directive pushed by its rapporteur Janelly Fourtou, would threaten anyone who publishes a program on the Internet with criminal prosecution.  FFII is supporting experts who are staying in the Parliament and organising events in and around the Parliament.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
% mode: latex ;
% End: ;

