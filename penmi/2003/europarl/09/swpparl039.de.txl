<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#eFs: Also, a debate on the IP Enforcement Directive is scheduled for 03/09/11.  This directive, in combination with modifications to the software patent directive pushed by its rapporteur Janelly Fourtou, would threaten anyone who publishes a program on the Internet with criminal prosecution.  FFII is supporting experts who are staying in the Parliament and organising events in and around the Parliament.

#descr: Das Europäische Parlament entscheidet über einen Richtlinienvorschlag, der Algorithmen und Geschäftsmethoden patentierbar macht.  Wenn das Parlament am 23. September in Straßburg diesem Vorschlag zustimmt, rücken damit bis auf weiteres alle Chancen demokratischer Entscheidungsfindung zu diesem Thema in weite Ferne.  FFII und Andere bereiten detaillierte Analysen und Abstimmempfehlungen vor und veranstalten Kundgebungen in mehreren Städten, mit Höhepunkt in Straßburg am 23. September, und einen Netzstreik.  In München und Wien nahmen jeweils 300 Personen teil.

#title: 2003/09 EP: Entscheidung über Softwarepatent-Richtlinie

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl039 ;
# txtlang: de ;
# multlin: t ;
# End: ;

