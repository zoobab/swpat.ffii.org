<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#eFs: Also, a debate on the IP Enforcement Directive is scheduled for 03/09/11.  This directive, in combination with modifications to the software patent directive pushed by its rapporteur Janelly Fourtou, would threaten anyone who publishes a program on the Internet with criminal prosecution.  FFII is supporting experts who are staying in the Parliament and organising events in and around the Parliament.

#descr: The European Parliament is faced with a proposal which, while trying to sound harmless, would make algorithms and business methods patentable.  If the Parliament approves this proposal on 2003/09/23 in Strasburg, chances for democratic decisionmaking on software patents in Europe may be remote for a long time to come.  FFII and others are preparing detailed analyses and voting recommendations and demonstrating in various cities, including Munich, Vienna, Berlin, Stuttgart and Strasbourg.  In Vienna and Munich 300 people participated.

#title: 2003/09 EP: Software Patent Directive Vote

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl039 ;
# txtlang: en ;
# multlin: t ;
# End: ;

