<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#men: Arrange other meetings for our speaker in Brussels on the day of the conference.

#uWs: This person will report to us about the conference, so as to facilitate communication between the communities.

#Whr: Find a second participant.

#enf: Clarify details of reimbursement, minimise burden of formalities.

#aWt: Ensure that the program on the ICRT website refers to this page.

#hah: Provide the speaker with materials to distribute at the conference.

#sfW: Publish a draft of a possible speech.

#iel: This will probably be François Péllegrini.

#eta: Determine the speaker.

#rWi: More details can be expected to be published on the ICRT website soon.

#leo: ICRT will reimburse travel and accomodation expenses of our speaker.

#ice: ICRT has invited a %(q:speaker on intellectual property and open source) from Eurolinux for panel 2, and we have accepted the invitation.  It will be known before end of october who will be the speaker.

#eWW: The conference will take place on 2003-11-17 14.00-18.00 and will be attended by, as the organisers say, a %(q:high-level) audience of 50 specially invited guests, participating in three panels under the title %(q:Intellectual Property, a Tool for Economic Growth and Technological Convergence).  Panel 2 is about industry positions, panel 3 about concerns of public administrations.

#5sa: The Intellectual Property working group of ICRT, a coalition of 25 large telecom and media companies, published this paper in support of the European Commission's software patent directive project.  It argues that SMEs need software patents, that TRIPs requires program claims, that the EPO does a good job at patent examination and there is no fear that a US-like situation will occur in Europe.  Puts these assertions into an easy-to-understand FAQ form.

#omo: ICRT Advocacy Paper for CEC/BSA Directive + Program Claims

#tew: A collection of position papers on various EU law proposals.

#IPi: ICRT Intellectual Property Working Group

#mip: ICRT consists of 25 well-known companies from the telecommunications, computer and publishing industries.  Their experts meet to analyze regulatory developments to prepare approaches to public policy issues.

#nuu: International Communications Round Table

#descr: A conference in Brussels on 2003-11-17 14-17.00 with a panel on %(q:industry views) to which FFII/Eurolinux is invited.

#title: ICRT Intellectual Property Conference BXL 03-11-17

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpen03.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpIcrt0311 ;
# txtlang: en ;
# multlin: t ;
# End: ;

