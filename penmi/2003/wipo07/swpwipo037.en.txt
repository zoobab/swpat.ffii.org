<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: A WIPO workgroup held a conference together with the Legal Studies department of Turin University.  Alex Macfie attended as a speaker on behalf of FFII.
title: WIPO Turin 2003/07/28-29
one: Announcement, Schedule
rWi: Morning Session
tos: Afternoon Session
aWo: Marco Ricolfi
him: chairman
oPr: Paolo Peveraro
rtn: Turin City Council
bWe: Roberto Castello
tAu: Antonio Alberquerque
anr: Brazilian Government
egr: Georg Greve
vWs: Sven Bostyn
ars: law professor
lWW: Introduced by Marco Ricolfi, chairman. He outlined arguments, but characterized the opponents of software patents as free software advocates.  This, as it turns out, was a common error throughout the conference.  Proprietary software was linked with patents and free software was linked with copyright and anti-patents.
tki: Ricolfi said he had no opinion (but many of his comments seemed to suggest a pro-patent bias, in particular when he accused the anti-patent movement of relying too heavily on sloganeering and anecdotes) and that the issue needed further discussion. He expressed the view that one must avoid a situation of a %(q:race to the top) where the powerfuil nations take a position and others feel they need to jump on the bandwagon. Trademark law was cited as an example, as was extensions to copyright duration (eg Sonny Bono Act), which he considered to be %(q:questionable). He said that the Sonny Bono Act was specifically intended to prevent Disney films from entering the public domain. He said that slogans are unhelpful, and cited the biotech patents debate in the European Parliament as an example, where he accused the anti-patent lobby of sloganeering. He considers Rothley's fake compromises to be helpful.
jse: Paolo Peveraro, of Turin City Council, briefly spoke of the Turin CC's experience with free software.
lge: Roberto Castello of WIPO was next speaker, and again talked of the business vs free software dichotomy.
foo: Antonio Alberquerque, from the Brazilian gvt, spoke on Brazil's use of free software: preferential use is made of open source software in the public sector. The Brazilian software industry is recent and significant (1.5% of GNP); open code is needed for digital knowledge; digital inclusion becomes impossible with proprietary software. IP law in Brazil for software = copyright + licences; there is no provision for software patents, and clearly such patents would interfere with Brazil's policy.
9pb: Georg Greve, from the Free Software Foundation, started with a well-known %(bg:1991 quote by Bill Gates on software patenting), revealing his identity at the very end of the speech.  He explained the problems of software patents in free software, as well as the general problem with software patents --- that patents are best suited for fields where a single innovator has a big idea, and that it doesn't work like that in software. He cited %(si:Sun's visit by IBM patent lawyers). With copyright companies can still compete, but a patent can block off a whole market.
qeW: Ricolfi called this a %(q:one-sided) point of view and called on Sven Bostyn, professor in intellectual property law in Maastricht to give a %(q:more scientific) perspective.
rWe: Bostyn started with a strawman by claiming that some want no IP protection for software. He does not understand why copyright protection should be applied to software and asserted that software should be patented as it belongs to a %(q:field of technology). In an exchange with Georg Greve, he turned round the question of %(q:why should software patented) to %(q:why not?).  He conceded, however, that the European Commission's justification for ignoring the 90% of consultation respondants who opposed software patents that it's the opinion of the big players that matters most was a poor one.
nmr: Bostyn showed the audience a business card dispenser, whose main feature appeared to be that it can dispense business cards one at a time. He was citing this as an example of a patentable invention based on a small incremental innovation, in the same style as innovation in software. Showing a lack of understanding of how software development works, he aserted that software is vulnerable to free riding, in the form of reverse engineering.
ntW: He correctly characterized copyright as protecting an expression not an idea, and that a completely different program is not a copyright infringement (but fails to understand that that is how it should be). He admitted that the problem of patent thickets exists for softwrae developers, but said that it could also exist in other industries.
iWd: He discussed problems with the US patent system, in particular that its record on assessing the %(q:inventive step) is not good. Also that prior art is a problem as the USPTO can only understand English. He cited a patent on a ticket-based queueing system. Similarly most patented %(q:computer-implemented inventions) are not genuinely inventive.
Wkd: He also said that ho proof of patent thickets but admitted that it is difficult to prove.
uWs: Luigi Mansani
vsw: Lovells lawyer
rai: Mario Calderini
cos: economist
rWa: Enrique Chaparro
rnn: Argentina
lWc: Alex Macfie
Wos: Marco Cuircina: course administrator
nWe: Ricolfi acknowledged that his earlier comment on %(q:one-sidedness) was unfair.
lht: Costello had left as he had a plane to catch.
yec: Next speaker was Luigi Mansani, a lawyer from Lovells. He analysed US and EU patent law, in particular Art 52 EPC and the %(q:as such) clause with respect to the %(eg:2001 examination guidelines). He mentioned the euphoria surrounding the dot-com era (he said %(q:new economy)) and the increase in swpat applications during that time, as well as the increase in business method patent applications after the State Street Bank decision.
eWn: The 2001 examination guidelines effectively legalize swpat as per the directive (whose intent is to simply codify EPO practice). The need for a %(q:technical contribution) meaning that the mere presence of a computer can make something patentable.
Tte: Art 27(1) of %(tr:TRIPS) was cited, he said that the field of technology and industrial application mandated software patentability.
aai: EU proposal cautions and critical of US approach; status quo better than any change. The draft has not added much to debate, but refers to EPO case law, and different rulings in different member states can be avoided.
Wna: He said it was nonsense to evaluate the %(q:technical contribution) as part of the %(q:inventive step).  The %(q:technical contribution) requirement was worded to ensure TRIPS conformity.
WWW: Mario Calderini, economist, whose speech %(q:Beyond appropriability) was partially difficult to understand. It explained the possible economic effects of software patents, in the language of economists. His main points seemed to be:
dnt: Knowledge as public good vs Knowledge as Proprietary good (appropriable and non-tradeable). Patents are /one way/ (not the only way) to protect proprietary knowledge
ain: Economic impact of patents is uncertain. Patents are not an optimal incentive to innovation. Others may be career concern and ego-gratification.
Wtv: In areas of cumulative and sequential innovation, non-patentability in the first generation/stage of development often has a positive effect on innovation (but why not in later stages?)
ztr: Standardization of rules is not a good criterion for patent law.
Wem: EU has a problem of weak anti-trust mechanisms.
rct: compulsory licences + RAND could lead to anti-trust issues
hoo: discussion should not be on whether or not to have patents, but improvement of rules.
WWg: Other uses for patents in practice: as strategic assets, eg accounting devices (for Enron-ish accountancy), signalling devices (for investors), creating markets for knowledge.
CLa: Enrique Chaparro, from Via Libre, Argentina, mathematician
uil: software pervasive and ubiquitous, but IPR system is in crisis. Was set up to protect distribution of product (not product itself). but in software marginal cost of distribution is next to zero.
tWl: The ONLY justification for IPR is that to encourage innovation (regardless of other uses)
yni: technology gap between north and south (digital divide) widening
nsr: software engineering is an incremental process, he gave an example using Fermat's Last Theorem
gtt: Most innovation in software was during the 50s, 60s, 70s, when there were no software patents. There has been relatively little innovation in last 20 years. Hyperlinks were first discussed in 1949 (and BT's patent claim on them failed because of the prior art)
hnt: Role of USPTO was discussed, with teh Diamond v Dehr case in early 1980s which was taken by USPTO to legitimize swpat. Under Bruce Lehman, USPTO head from 1994, swpat increased He mentioned the problems of patenting what is too obvious to publish, submarine patents, and the fact that it is easier to settle than to defend a possibly invalid patent claim.
cnW: Patent inflation creates a %(q:litigation industry) --- gave the example of the company suing M$ over privacy tools, which consists only of lawyers --- and makes entry difficult for newcomers.
raa: For USPTO to do a proper job of assessing prior art and inventiveness of software patents it would need to employ 5000 software specialists in every line of software.
rxe: Hayek warned against over-expansion of IP systems in 1946.
Wot: There should be no patented standards.
oWr: Speaking from the point of view of a developer and ordinary IT professional; most developers, not just those who develop free software, oppose patenting. The debate has been mischaracterised as one between proprietary and open source; only large software companies and the patent industry want swpat.
dit: Cited %(ep:1978 examination guidelines), and explained why software development does not belong to a field of technology, is a series of logical steps which the computer merely obeys. Anything %(q:technical) in the way the computer behaves when it obeys instructions is just an ordinary consequence of the way the computer is built. A sequence of instructions is not patentable outside a computer, so it should not be patentable inside a computer. Not interested in legalistic arguments on how endeavours in field X is patented, so should endeavours in field Y.
fns: Patent system is a state bureaucracy (often self-financing, but that may be part of the problem: patent offices are encouraged to think of patent-claimants, not the general public, as their clients, hence to think %(q:the more patents the better)), its existence needs to be justified in every field, and all fields are different.
Whr: Software Directive is a wolf in sheep's clothing. Does indeed codify existing EPO practice, but that is exactly the problem --- the EPO practice is illegal, the result of a recent drift. Cited problems with %(q:technical) and %(q:industrial). Also CULT and ITRE amemdnemnts may have made directive unworkable but that was because these committees were trying to turn round a directive which supports swpat to one which opposes them.
wat: Sven Bostyn suggested that critics of patent system were saying different things for biotech patents from for swpat: for biotech the criticism was that the EPO was too conservative in allowing patent claims on biotechnological inventions, but too radical in allowing them on software. Also said that examination guidelines were not law and EPO had moved with the times.
rtP: AM answered that he was not interested in whether it was %(q:radical) or %(q:conservative), but that allowing biotech patents was also radical as it meant patenting discoveries which traditionally are excluded from patentability. Was concerned about drift to /de facto/ USPTO practice in software and asked for return to older guidelines.
lrr: TRIPS 27 does not oblige swpat, but does not disallow them either. Berne Convention : programs protected as literary works, arguments for and against software patents are the same!  But there is a conflict between free software and software patents.  TRIPS articles 7, 8, 27 need reinterpretation, with free software exempted from litigation under TRIPS 8.
htt: Speaker from floor suggested that the reason copyright applies to software is programmers did not want to go through the patent system and their lawyers knew more about coyright than patents (surely this shows that copyright is the better system?)
euh: Another speaker wished that opponents of swpat could be more articulat.  Suggested 5-year software copyright with full disclosure of source.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpwipo037 ;
# txtlang: en ;
# End: ;

