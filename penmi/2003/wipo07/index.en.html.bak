<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/girzu/index.en.html" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/01/06">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Software Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="WIPO Turin 2003/07/28-29">
<meta name="description" content="A WIPO workgroup held a conference together with the Legal Studies department of Turin University.  Alex Macfie attended as a speaker on behalf of FFII.">
<title>WIPO Turin 2003/07/28-29</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>WIPO Turin 2003/07/28-29<br>
<!--#include virtual="../../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#anonc">Announcement, Schedule</a></li>
<li><a href="#maten">Morning Session</a></li>
<li><a href="#pomer">Afternoon Session</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="anonc">Announcement, Schedule</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="http://www.turin-ip.com/turin-ip/news/1058512719/index_html">Conference Announcement</a></b></dt></dl>
</div>
</div>

<div class="secthead">
<h2><a name="maten">Morning Session</a></h2>
</div>

<div class="sectbody">
<div class="sectmenu">
<ul><li><a href="#mricolfi">Marco Ricolfi (chairman)</a></li>
<li><a href="#ppeveraro">Paolo Peveraro (Turin City Council)</a></li>
<li><a href="#rcastello">Roberto Castello, WIPO</a></li>
<li><a href="#aalberq">Antonio Alberquerque, Brazilian Government</a></li>
<li><a href="#ggreve">Georg Greve (FSF Europe)</a></li>
<li><a href="#sbostyn">Sven Bostyn, law professor, Maastricht</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h3><a name="mricolfi">Marco Ricolfi (chairman)</a></h3>
</div>

<div class="sectbody">
Introduced by Marco Ricolfi, chairman. He outlined arguments, but characterized the opponents of software patents as free software advocates.  This, as it turns out, was a common error throughout the conference.  Proprietary software was linked with patents and free software was linked with copyright and anti-patents.<p>Ricolfi said he had no opinion (but many of his comments seemed to suggest a pro-patent bias, in particular when he accused the anti-patent movement of relying too heavily on sloganeering and anecdotes) and that the issue needed further discussion. He expressed the view that one must avoid a situation of a &quot;race to the top&quot; where the powerfuil nations take a position and others feel they need to jump on the bandwagon. Trademark law was cited as an example, as was extensions to copyright duration (eg Sonny Bono Act), which he considered to be &quot;questionable&quot;. He said that the Sonny Bono Act was specifically intended to prevent Disney films from entering the public domain. He said that slogans are unhelpful, and cited the biotech patents debate in the European Parliament as an example, where he accused the anti-patent lobby of sloganeering. He considers Rothley's fake compromises to be helpful.
</div>

<div class="secthead">
<h3><a name="ppeveraro">Paolo Peveraro (Turin City Council)</a></h3>
</div>

<div class="sectbody">
Paolo Peveraro, of Turin City Council, briefly spoke of the Turin CC's experience with free software.
</div>

<div class="secthead">
<h3><a name="rcastello">Roberto Castello, WIPO</a></h3>
</div>

<div class="sectbody">
Roberto Castello of WIPO was next speaker, and again talked of the business vs free software dichotomy.
</div>

<div class="secthead">
<h3><a name="aalberq">Antonio Alberquerque, Brazilian Government</a></h3>
</div>

<div class="sectbody">
Antonio Alberquerque, from the Brazilian gvt, spoke on Brazil's use of free software: preferential use is made of open source software in the public sector. The Brazilian software industry is recent and significant (1.5% of GNP); open code is needed for digital knowledge; digital inclusion becomes impossible with proprietary software. IP law in Brazil for software = copyright + licences; there is no provision for software patents, and clearly such patents would interfere with Brazil's policy.
</div>

<div class="secthead">
<h3><a name="ggreve">Georg Greve (FSF Europe)</a></h3>
</div>

<div class="sectbody">
Georg Greve, from the Free Software Foundation, started with a well-known <a href="/vreji/cusku.en.html#bgates91">1991 quote by Bill Gates on software patenting</a>, revealing his identity at the very end of the speech.  He explained the problems of software patents in free software, as well as the general problem with software patents --- that patents are best suited for fields where a single innovator has a big idea, and that it doesn't work like that in software. He cited <a href="/vreji/cusku.en.html#reback0206">Sun's visit by IBM patent lawyers</a>. With copyright companies can still compete, but a patent can block off a whole market.<p>Ricolfi called this a &quot;one-sided&quot; point of view and called on Sven Bostyn, professor in intellectual property law in Maastricht to give a &quot;more scientific&quot; perspective.
</div>

<div class="secthead">
<h3><a name="sbostyn">Sven Bostyn, law professor, Maastricht</a></h3>
</div>

<div class="sectbody">
Bostyn started with a strawman by claiming that some want no IP protection for software. He does not understand why copyright protection should be applied to software and asserted that software should be patented as it belongs to a &quot;field of technology&quot;. In an exchange with Georg Greve, he turned round the question of &quot;why should software patented&quot; to &quot;why not?&quot;.  He conceded, however, that the European Commission's justification for ignoring the 90% of consultation respondants who opposed software patents that it's the opinion of the big players that matters most was a poor one.<p>Bostyn showed the audience a business card dispenser, whose main feature appeared to be that it can dispense business cards one at a time. He was citing this as an example of a patentable invention based on a small incremental innovation, in the same style as innovation in software. Showing a lack of understanding of how software development works, he aserted that software is vulnerable to free riding, in the form of reverse engineering.<p>He correctly characterized copyright as protecting an expression not an idea, and that a completely different program is not a copyright infringement (but fails to understand that that is how it should be). He admitted that the problem of patent thickets exists for softwrae developers, but said that it could also exist in other industries.<p>He discussed problems with the US patent system, in particular that its record on assessing the &quot;inventive step&quot; is not good. Also that prior art is a problem as the USPTO can only understand English. He cited a patent on a ticket-based queueing system. Similarly most patented &quot;computer-implemented inventions&quot; are not genuinely inventive.<p>He also said that ho proof of patent thickets but admitted that it is difficult to prove.
</div>
</div>
</div>

<div class="secthead">
<h2><a name="pomer">Afternoon Session</a></h2>
</div>

<div class="sectbody">
<div class="sectmenu">
<ul><li><a href="#lmansani">Luigi Mansani (Lovells lawyer)</a></li>
<li><a href="#mcalderini">Mario Calderini (economist)</a></li>
<li><a href="#echaparro">Enrique Chaparro (Argentina)</a></li>
<li><a href="#amacfie">Alex Macfie, FFII, UK</a></li>
<li><a href="#cuircina">Marco Cuircina: course administrator</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h3><a name="lmansani">Luigi Mansani (Lovells lawyer)</a></h3>
</div>

<div class="sectbody">
Ricolfi acknowledged that his earlier comment on &quot;one-sidedness&quot; was unfair.<p>Costello had left as he had a plane to catch.<p>Next speaker was Luigi Mansani, a lawyer from Lovells. He analysed US and EU patent law, in particular Art 52 EPC and the &quot;as such&quot; clause with respect to the <a href="http://kwiki.ffii.org/EpoGl01En">2001 examination guidelines</a>. He mentioned the euphoria surrounding the dot-com era (he said &quot;new economy&quot;) and the increase in swpat applications during that time, as well as the increase in business method patent applications after the State Street Bank decision.<p>The 2001 examination guidelines effectively legalize swpat as per the directive (whose intent is to simply codify EPO practice). The need for a &quot;technical contribution&quot; meaning that the mere presence of a computer can make something patentable.<p>Art 27(1) of <a href="/stidi/trips/index.en.html">TRIPS</a> was cited, he said that the field of technology and industrial application mandated software patentability.<p>EU proposal cautions and critical of US approach; status quo better than any change. The draft has not added much to debate, but refers to EPO case law, and different rulings in different member states can be avoided.<p>He said it was nonsense to evaluate the &quot;technical contribution&quot; as part of the &quot;inventive step&quot;.  The &quot;technical contribution&quot; requirement was worded to ensure TRIPS conformity.
</div>

<div class="secthead">
<h3><a name="mcalderini">Mario Calderini (economist)</a></h3>
</div>

<div class="sectbody">
Mario Calderini, economist, whose speech &quot;Beyond appropriability&quot; was partially difficult to understand. It explained the possible economic effects of software patents, in the language of economists. His main points seemed to be:<p><ul><li>Knowledge as public good vs Knowledge as Proprietary good (appropriable and non-tradeable). Patents are /one way/ (not the only way) to protect proprietary knowledge</li>
<li>Economic impact of patents is uncertain. Patents are not an optimal incentive to innovation. Others may be career concern and ego-gratification.</li>
<li>In areas of cumulative and sequential innovation, non-patentability in the first generation/stage of development often has a positive effect on innovation (but why not in later stages?)</li>
<li>Standardization of rules is not a good criterion for patent law.</li>
<li>EU has a problem of weak anti-trust mechanisms.</li>
<li>compulsory licences + RAND could lead to anti-trust issues</li>
<li>discussion should not be on whether or not to have patents, but improvement of rules.</li>
<li>Other uses for patents in practice: as strategic assets, eg accounting devices (for Enron-ish accountancy), signalling devices (for investors), creating markets for knowledge.</li></ul>
</div>

<div class="secthead">
<h3><a name="echaparro">Enrique Chaparro (Argentina)</a></h3>
</div>

<div class="sectbody">
Enrique Chaparro, from Via Libre, Argentina, mathematician<p><ul><li>software pervasive and ubiquitous, but IPR system is in crisis. Was set up to protect distribution of product (not product itself). but in software marginal cost of distribution is next to zero.</li>
<li>The ONLY justification for IPR is that to encourage innovation (regardless of other uses)</li>
<li>technology gap between north and south (digital divide) widening</li>
<li>software engineering is an incremental process, he gave an example using Fermat's Last Theorem</li>
<li>Most innovation in software was during the 50s, 60s, 70s, when there were no software patents. There has been relatively little innovation in last 20 years. Hyperlinks were first discussed in 1949 (and BT's patent claim on them failed because of the prior art)</li></ul><p>Role of USPTO was discussed, with teh Diamond v Dehr case in early 1980s which was taken by USPTO to legitimize swpat. Under Bruce Lehman, USPTO head from 1994, swpat increased He mentioned the problems of patenting what is too obvious to publish, submarine patents, and the fact that it is easier to settle than to defend a possibly invalid patent claim.<p>Patent inflation creates a &quot;litigation industry&quot; --- gave the example of the company suing M$ over privacy tools, which consists only of lawyers --- and makes entry difficult for newcomers.<p>For USPTO to do a proper job of assessing prior art and inventiveness of software patents it would need to employ 5000 software specialists in every line of software.<p>Hayek warned against over-expansion of IP systems in 1946.<p>There should be no patented standards.
</div>

<div class="secthead">
<h3><a name="amacfie">Alex Macfie, FFII, UK</a></h3>
</div>

<div class="sectbody">
Speaking from the point of view of a developer and ordinary IT professional; most developers, not just those who develop free software, oppose patenting. The debate has been mischaracterised as one between proprietary and open source; only large software companies and the patent industry want swpat.<p>Cited <a href="/papri/epo-gl78/index.en.html">1978 examination guidelines</a>, and explained why software development does not belong to a field of technology, is a series of logical steps which the computer merely obeys. Anything &quot;technical&quot; in the way the computer behaves when it obeys instructions is just an ordinary consequence of the way the computer is built. A sequence of instructions is not patentable outside a computer, so it should not be patentable inside a computer. Not interested in legalistic arguments on how endeavours in field X is patented, so should endeavours in field Y.<p>Patent system is a state bureaucracy (often self-financing, but that may be part of the problem: patent offices are encouraged to think of patent-claimants, not the general public, as their clients, hence to think &quot;the more patents the better&quot;), its existence needs to be justified in every field, and all fields are different.<p>Software Directive is a wolf in sheep's clothing. Does indeed codify existing EPO practice, but that is exactly the problem --- the EPO practice is illegal, the result of a recent drift. Cited problems with &quot;technical&quot; and &quot;industrial&quot;. Also CULT and ITRE amemdnemnts may have made directive unworkable but that was because these committees were trying to turn round a directive which supports swpat to one which opposes them.<p>Sven Bostyn suggested that critics of patent system were saying different things for biotech patents from for swpat: for biotech the criticism was that the EPO was too conservative in allowing patent claims on biotechnological inventions, but too radical in allowing them on software. Also said that examination guidelines were not law and EPO had moved with the times.<p>AM answered that he was not interested in whether it was &quot;radical&quot; or &quot;conservative&quot;, but that allowing biotech patents was also radical as it meant patenting discoveries which traditionally are excluded from patentability. Was concerned about drift to /de facto/ USPTO practice in software and asked for return to older guidelines.
</div>

<div class="secthead">
<h3><a name="cuircina">Marco Cuircina: course administrator</a></h3>
</div>

<div class="sectbody">
TRIPS 27 does not oblige swpat, but does not disallow them either. Berne Convention : programs protected as literary works, arguments for and against software patents are the same!  But there is a conflict between free software and software patents.  TRIPS articles 7, 8, 27 need reinterpretation, with free software exempted from litigation under TRIPS 8.<p>Speaker from floor suggested that the reason copyright applies to software is programmers did not want to go through the patent system and their lawyers knew more about coyright than patents (surely this shows that copyright is the better system?)<p>Another speaker wished that opponents of swpat could be more articulat.  Suggested 5-year software copyright with full disclosure of source.
</div>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/penmi/2003/wipo07/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/01/06 (2004/08/24)
<a href="/girzu/index.en.html">Workgroup</a><br>
english version 2004/08/16 by <a href="http://www.ffii.org/~phm">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el -->
<!-- mode: html -->
<!-- End: -->

