\contentsline {section}{\numberline {1}Announcement, Schedule}{2}{section.1}
\contentsline {section}{\numberline {2}Morning Session}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Marco Ricolfi (chairman)}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Paolo Peveraro (Turin City Council)}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Roberto Castello, WIPO}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Antonio Alberquerque, Brazilian Government}{2}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Georg Greve (FSF Europe)}{3}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Sven Bostyn, law professor, Maastricht}{3}{subsection.2.6}
\contentsline {section}{\numberline {3}Afternoon Session}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Luigi Mansani (Lovells lawyer)}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Mario Calderini (economist)}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Enrique Chaparro (Argentina)}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Alex Macfie, FFII, UK}{6}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Marco Cuircina: course administrator}{6}{subsection.3.5}
