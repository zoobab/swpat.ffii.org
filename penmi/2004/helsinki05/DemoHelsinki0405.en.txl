<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Demo Helsinki 2004/05

#descr: Under pressure from the Finnish government, the EU is being pushed to legalise patents on %(q:computer-implemented) algorithms and business methods.  The European Parliament voted to confirm the non-patentability of software in September 2003, with support from many finnish MEPs from most parties.  However the Finnish government is currently ignoring the voices of most programmers and economists   Instead, the government is following the wishes of its patent office managment and of Nokia's patent department, allowing its patent administrators to overthrow the Parliament's decision through a backroom negotiation process in the Council.  The Parliament's Comittee of Economic Affairs also supports this policy and has so far failed to assess the interests that are at stake.  By organising a demonstration, press conference and study conference we want to throw the light of public scrutiny on the questionable backroom dealings of Finnish politicians in Brussels and give this subject the weight which it deserves in the upcoming elections to the European Parliament.  Targets of the march may include government and parliament buildings, patent institutions, party headquarters and headquarters of some of the companies and trade associations that have left their decisions to corporate patent lawyers and allowed them to speak in their name against the interests of Finnland's software innovators.

#omr: Hearings of the Finnish Parliament's Economic Affairs Committee

#nWW: The software world in Finnland has largely been asleep during the hearings, leaving matters largely to patent lawyers and corporate lobbyists, opposed only by a few civil rights campaigners.  But it is still not too late to make a difference now.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: DemoHelsinki0405 ;
# txtlang: en ;
# multlin: t ;
# End: ;

