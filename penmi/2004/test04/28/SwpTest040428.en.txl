<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2003/04/28 BXL Dorint: From Legal Wordplay to Granted Software Patents

#descr: During this first day of a two-day conference in Brussels, in and near the European Parliament, we bring software professionals, patent practitioners, economists and politicians from EU and US together for a symposium in the Dorint Hotel to explore the chain of causality from a proposed EU software patent directive to resulting patenting practises.  The day begins with speeches by FTC.gov commissioner Mozelle W. Thompson and Prof. Lawrence Lessig on %(q:The US as a Test Case), and ends with a panel of software leaders from small and large enterprises, social scientists and MEPs on %(q:E-Patents and E-Commerce).

#mnl: Time and Place

#ncg: Conference Program

#tim: time

#eea: Wednesday

#pla: place

#ipl: near European Commission, 5-10 minutes walk from European Parliament

#feW: See %(aw:AEL conference wiki) for map.

#sra: Simultaneous interpreting at least for %(LST) is available

#Wlm: We have all kinds of projectors, including lcd projector (beamer), available.

#hom: who?

#enn: Software Patents -- The U.S. as a Test Case

#eia: Keynote Speech: Learning from American Mistakes

#eri: The %(fh:hearings held by the U.S. competition agencies) paint a darker picture of U.S. practice than the report than the report on which the Commission relies, and the  Undersecretary for Intellectual Property has described the Patent Office as an agency in crisis.  What can Europe learn from the U.S. experience?

#ons: Co-Panelists

#auW: Professor for Information Policy Studies, Univ. of Michigan

#ttL: Civil Liberties under Overlapping Property Regimes

#rdu: Some companies have used patents to obtain broad exclusion scopes which copyright denied them.  Some are using patents to make their competitors, copyright property useless.  Proponents of the directive say that %(q:patents and copyright are complementary and may overlap), whereas earlier doctrines called for a clear separation of spheres of property.  How has such overlapping worked in semiconductor topography and areas?  Will the broadest property right always win out?  Are civil liberties crushed under a broadness competition?  How can an integrated system of intellectual/industrial be (re)created?

#tWt: patent examiner at %(dc:German Patent office)

#neempl: speaking only for himself, not for his employer

#ter: Software Patents and Europe's Legal Structure

#Wte: With the challenges facing the European and global patent system(s), how should decisions on software and  business methods be made?  And by whom?

#iol: What can a EU directive achieve in the context of concurrence between inter-governmental treaties and EU Law?

#frw: author of a %(es:EU-sponsored study) on software patents

#yei: German attorney and writer of much-cited articles, coiner of the term %(q:confusion by clarification)

#noW: Examiner at the %(dc:Polish Patent Office)

#WrW: IP expert of ELDR

#sWc: author of JURI amendment 72, which %(e:calls on the commission to withdraw the directive) because  %(e:the fact that the European Patent Convention and Community legislation exist alongside each other makes the current legal rules concerning the granting of patents unnecessarily confusing.)

#Lun: Buffet

#atl: Invention Concepts in Europe and the Resulting Patents

#rtn: Did the European Patent Office really grant software patents %(q:from its earliest days), as MEP Arlene McCarthy says in her report?  What were the original standards of the EPC and how did the EPO change them from 1973 to 2003?

#jdo: Were the changes motivated by evoltion of technology or by internal political evolutions of the patent system?  How many patents of which types were granted as a result?  Were the changes gradual or were there major leaps and revolutions?

#lrn: Was the EPO an innovator or a follower?  How do national practises in Europe differ?  Is the EPO violating Art 52 EPC?  Is there a way back, or would that lead to %(q:irreconcilable conflicts with the EPO), as MEP McCarthy says?

#pea: What options does the European Parliament have today for defining what is a patentable invention and what not?

#lWV: legal delegate of %(LV)

#ffW: drafter of %(es:Opinion of the Economic and Social Council of the EU) on the Directive Proposal

#aWb: Emerging Phenomena in Patent Practice

#ost: Thickets, Holdup, Ambush, Broadcast Litigation, Contract Clauses.  Who benefits and who loses?

#ikr: Subsidizing Failure? -- Patents after the Bust

#tnn: software entrepreneur

#oee2: Coffee

#nWi: IT Infrastructure Patents and the Information Society

#gaW: The path of operating system kernel development is cluttered with patents.  What kind of services and applications have been affected so far?

#eoW: Yesterday's Innovators Locking In Tomorrow's Innovation?

#aht: Alcatel's proclaimed strategy of patenting the %(q:Next Generation Internet) and the consequences of a possible domination of software by patent-oriented hardware industries.

#otr: Microsoft's Patenting Strategies

#Wsc: As patent numbers increase and patent quality decreases, is the noose around Free OS Development becoming tighter or looser?

#anm: E-Patents and E-Commerce

#aao: The directive proponents seem to consider advancements in database, groupware, enterprise ressource planning etc to be patentable inventions but distance themselves from %(q:pure business method patents).  Can such a distinction prevent the worst in Europe?

#ieb: Recently various E-Commerce companies, not all of which develop software, have been attacked with E-Patents in the US.  In many cases, equivalent patents have been granted in Europe and would become enforcible under the proposed directive.  How would E-Commerce be affected?

#jWm: subject to confirmation

#Dne: Dinner

#kyr: The scientific conference %(ak:continues on the following day's afternoon at 15.00).  In between you can attend a %(eh:Parliamentary Hearing) and a %(sg:Street Performance).  See the %(eo:Event Overview).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpTest040428 ;
# txtlang: en ;
# multlin: t ;
# End: ;

