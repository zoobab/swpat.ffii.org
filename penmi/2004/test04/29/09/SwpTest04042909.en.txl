<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004/04/29 09:00 BXL EP: Foundations of a Free Information Infrastructure

#descr: The European Commission and MEP Arlene McCarthy are proposing to legalise software patents in Europe and argue that this will help Small and Medium Enterprises (SMEs) to protect their investments, to compete in the USA and to grow into large companies.  This claim is based on references to %(q:evidence) from %(q:studies).  However neither the producers of the alleged evidence nor the SMEs themselves have been adequately heard by the European Parliament.  This hearing, conducted by the fraction of Greens/EFA, tries to provide the opportunity.

#mnl: Time and Place

#nvo: Program Proposal

#Tim: Time

#ans: Please be at the Parliament entrance 08:30 for the admission procedure!

#Pla: Place

#rWa: European Parliament

#ASP: Spinelli Building

#sEo: Simultaneous interpreting between all European languages is available for this Hearing.

#Wev: The Greens/EFA are holding a hearing 09:00-12:00 May 8 in European Parliament (room 1G2) in Brussels.  A number of well-known innovative software SMEs, including MySQL, Galeco, Ilog, and Opera, will express their views on software patents.   Richard Stallman will speak about the effect of software patents on SME developers, and key researchers who have participated in Commission-funded studies will explain what the research shows.

#WfW: Software are currently protected by copyright but there a European directive (com 2002-092) has been proposed that would legalize patents on software, as is the case in the U.S.   Many Members of Parliament believe that the patent system is not suitable for software, but rapporteur Arlene McCarthy (UK, Labour) favors patents for software.   Others believe that SME issues have not yet been properly taken into account and that the costs software patents impose warrant further research and deeper analysis before Europe is locked in.

#tis: While individual patents theoretically provide protection for innovators large and small, in practice the easy availability of patents creates a numbers game that favors large established companies with patenting over SMEs.   SMEs fear that legalisation of 30,000 software patents issued on a shaky legal basis by the EPO (with many more on the way) would subject them to ever greater risks, especially since large companies in the U.S. have been setting up licensing operations designed to maximize returns to their patent portfolios.  Recent hearings held by U.S. competition agencies show that intensive patenting in the ICT sector has made inadvertent infringement commonplace.

#lsm: Research also shows that developers believe that patents will adversely affect the future of open source software.  In fact, one survey by a proponent of software patents shows that fear of inadvertent patent infringement is a substantial deterrent to corporate use of open source software.  Developers generally believe that copyright is well-suited to encouraging software development, and many worry that patents will undermine the confidence and certainty that copyright provides.

#toe2: The presentations and discussion with MEPs will cover the range of costs and benefits SMEs see in software patents.  Software professionals and developers with an interest in the question are urged to attend.  Related events around the hearing include:

#eoj: %(q:Software Patents: From Legal Wording to Economic Reality) will take place at the Dorint Hotel in Brussels all day on May 7 and the afternoon of May 8.  The symposium will address the many legal, technical, and economic aspects of software patents -- beyond their impact on SMEs, the subject of conference in the Parliament.  It will be keynoted by Professor Lawrence Lessig, author of %(q:Code and other Laws of Space) and %(q:The Future of Ideas).  Commissioner Mozelle Thompson of the U.S. Federal Trade Commission will also speak, and the panels will offer a mix of academic, patent practitioner, industry, and policy perspectives.  This conference is co-sponsored by FFII/Eurolinux and the Open Society Institute.

#hec: On May 8th, immediately after the hearing at the Parliament, a festive gathering %(q:Free ideas for a free world) will take place in front of Parliament at Place du Luxembourg, complete with street theater and a buffet.

#sin: For registration an participation, please contact %(LV).

#eWi: This hearing is conducted by %(ge:Greens/EFA), and they may (have) set it up differently.  The following is merely our construction site, on which we develop a detailed programme proposal for possible use by the Greens/EFA.

#tim: time

#ujc: subject

#hom: who

#eWm: Politicians' Questions

#Ett: What is the role of SMEs be in this process

#efn: What the Legislator needs to learn from SMEs and software innovation leaders

#esW: Statements of Software Innovation Leaders

#ano: Have Software Patents %(q:Helped SMEs Grow into Sizeable Software Companies)? -- SME Interests and How the EU is assessing them

#rta: How have European Commission, JURI and EU Council taken interests of SMEs into account?  On what evidence have they relied in building their economic rationale for software patents?  What does evidence from US (e.g. FTC hearings) and EU teach us?

#onn: Author of an %(st:EU-sponsored study) on the role of patents in SMEs

#cos: economist

#okf: co-author or a much-cited %(es:EU-sponsored study) on software patents

#Wsn: author of an %(bs:EU-sponsored study)

#rMS: Short SME Position Statements and Discussion

#iPf: president of %(PBF), CEO of %(ABS)

#alt: flash file systems

#set: custom web software

#Wau: Political Conclusions

#dua: How should policy decisions on software property be taken and by whom?

#Wei: Have SME interests been adequately taken into account by the EU's Legislative Process to Date?

#ttn: What needs to be done?  Where do we go from here?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpTest04042909 ;
# txtlang: en ;
# multlin: t ;
# End: ;

