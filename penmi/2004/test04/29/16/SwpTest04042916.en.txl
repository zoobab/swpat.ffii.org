<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004/04/29 BXL 16:00: FFII General Assembly

#descr: During this final part of our symposium in the Dorint Hotel near the European Parliament in Brussels, we bring leading software professionals, patent practitioners, economists and politicians from EU and US together to assess the economics of patent portfolios, the use of patents for tax evasion, the impact of patents on standardisation and competition and the prospects of bridging the age-old gap between patent science and patent policy.  Major talks will be given by FTC Commissioner Mozille Thompson, Software Entrepreneur and JPEG Standardiser Richard Clark, Software Entrepreneur and Economist Dr. Jean-Paul Smets-Solanes, as well as the scholars Puay Tang (UK), Peter Holmes (UK) and Brian Kahin (US).

#mnl: Time and Place

#ceu: Schedule

#tim2: time

#hrd: Thursday

#pla2: place

#ipl: beside European Commission, 5-10 minutes by foot from European Parliament

#hom: who?

#nfn: Patents, Portfolios, and SMEs

#efo: Research shows that SMEs are skeptical about the value of patents.  How do the numbers play out in the software industry with the great variation in size and business model?  Do portfolios permit large companies to %(q:tax)?

#st0: Bessen & Hunt Study 2003/05

#laa: just-published study of actual patterns in software patents.

#nmc: informaticist

#rin: president

#opo: doctorand in information economics

#ito: Univ. Strasbourg

#eWt: Unfortunately Jorge can not join us due to a traffic accident in his family.

#wta: Software Patents as Fiscal Tools

#utC: Generating Virtual Transactions with Virtual Inventions -- How Software Patents Help Companies Save Taxes

#keo: Patents and the Stock Market -- Companies' Annual reports list patent numbers as proofs of strength and correlate them to R&D efforts.  MEP Arlene McCarthy %(am:cites) such %(q:proven correlations) as a motive for legalising software patents.   What do patent statistics really mean?

#Cfe: Coffee

#jrC: Patents, Standards, Interoperability and Competition

#faf: Patents have made the work of standardisation bodies such as the W3C difficult and have often led to the exclusion of free software and shareware from the use of standards.  Competition authorities in the US and Europe have tended to be critical of patents, and the software patent directive proposal contains provisions which (pretend to) address this issue by an interoperability privilege, which some MEPs have proposed to further clarify and strengthen.  Competition regulation is often seen at the forefront of efforts to limit the excesses of the patent system and similar systems.  What can be achieved by this approach?

#eWm: Peter Holmes

#itS: University of Sussex

#Wtt: Bridging the Gap between Patent Science and Patent Policy

#ycW: Fritz Machlup %(fm:described) the history of the patent system as a %(q:victorious movement of lawyers against economists).  How have social science and patent legislation interacted since then? Why did so few scientists take part in the European Commission's consultations?  Why did economic studies practically without influence on the legislative process at the Commission, the Council and JURI?

#tsi: location disclosed to participants

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpTest04042916 ;
# txtlang: en ;
# multlin: t ;
# End: ;

