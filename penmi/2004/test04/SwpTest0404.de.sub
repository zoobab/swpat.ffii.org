\begin{subdocument}{SwpTest0404}{Konferenz \"{u}ber Erfolgsbewertung von Gesetzgebung im 2004/04}{http://swpat.ffii.org/termine/2004/test04/index.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Der FFII organisiert im April in Br\"{u}ssel eine Konferenz, die derzeitige Vorschl\"{a}ge f\"{u}r Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit im Hinblick auf Programme f\"{u}r Datenverarbeitungsanlagen unter die Lupe nimmt und ihre Wirkung auf Beispielpatente und Modelf\"{a}lle \"{u}berpr\"{u}ft.}
\begin{sect}{intro}{Einf\"{u}hrung}
This is a follow-up of a successful conference conducted in May 2003 in Brussels\footnote{http://swpat.ffii.org/termine/2003/europarl/05/index.de.html}.

Similar to last time, it occurs in a phase where legislation on software patents is pending, and where such legislation is dominated by interests of ``experts'' who tend to be rather unresponsive to the scientific community and with the people whose behaviour is to be regulated by the legislation.

The last conference contributed to a change of thinking in the European Parliament which led to a clear vote against software patentability.  However leading ``expert'' groups are trying to discredit this vote, among others by saying that the Parliament lacked the needed expertise and didn't know what it was voting about.

Like last time the conference will subject the pending legislation to more thourough benchmarking than most of the ``experts'' who hold the key to the legislation find necessary.

Like last time, we will hold this conference together with members of the European Parliament, in the hope to create interaction between the communities of politics, academia and concerned industries.

More than last time, we will produce a collection of writings, collected on a website and in a conference volume, which will advance our understanding of some of the open questions which we so far have largely had insufficient time to research, and which will be of value to decisionmakers.

Hopefully the conference will raise awareness of information economics and contribute to a more enlightened discourse about the information society and the ``Lisbon Goals'', and perhaps help give this discourse some of the importance which it deserves in the European Parliament's June elections.
\end{sect}

\begin{sect}{preti}{Questions to be answered by the Conference}
\begin{itemize}
\item
Do the EP Amendments endanger R\&D in the Telecom World: siehe auch Bosse gro{\ss}er Telekom-Ausr\"{u}ster unterzeichnen Brief gegen Europarl-\"{A}nderungsvorschl\"{a}ge\footnote{http://swpat.ffii.org/log/03/telcos1107/index.en.html}

\item
siehe Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analyse/testsuite/index.de.html}
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
Wenn Sie Fragen zum Projekt SwpTest0404\footnote{http://lists.ffii.org/mailman/listinfo/proj/index.html\#SwpTest0404} haben, z\"{o}gern Sie bitte nicht, sich mit SwpTest0404-help@ffii.org in Verbindung zu setzen.

\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Logikpatent-Schildb\"{u}rgerstreich ein Ende zu bereiten\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf Determine the exact time and place}}

\begin{quote}
It is still to be decided whether the conference will stand on its own or be connected to another conference.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{http://swpat.ffii.org/analyse/testsuite/index.de.html}}}

\begin{quote}
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
\end{quote}
\filbreak

\item
{\bf {\bf Aufruf zum Handeln II\footnote{http://swpat.ffii.org/papiere/europarl0309/appell/index.de.html}}}

\begin{quote}
Das Europ\"{a}ische Parlament hat f\"{u}r einen Gesetzentwurf gestimmt, der Programm- und Gesch\"{a}ftslogik wirklich von der Patentierbarkeit ausschlie{\ss}en w\"{u}rde.  Die Europ\"{a}ische Patentgesetzgebung liegt jedoch nach wie vor weitgehend in den H\"{a}nden ministerieller Patentexperten, von denen viele seit Jahren auf grenzenlose Patentierbarkeit hinwirken.  Diese Situation erfordert genaue Aufmerksamkeit und entschlossenes Handeln seitens nationaler Parlamentarier und betroffener B\"{u}rger.
\end{quote}
\filbreak

\item
{\bf {\bf}}
\filbreak

\item
{\bf {\bf 2003/05/07-08 BXL: Software Patents: From Legal Wordings to Economic Reality\footnote{http://swpat.ffii.org/termine/2003/europarl/05/index.de.html}}}

\begin{quote}
During this two-day interdisciplinary conference in Brussels, near and in the European Parliament, we will bring together programmers, engineers, entrepreneurs, law scholars, economists and politicians to explore the whole chain of causality from proposed patent law regulations to European policy goals, such as promoting innovation, competition, enterpreneurial spirit and consumer protection, unbureaucratic and target-oriented governance, legal security, favorable conditions for small and medium enterprises and ``becoming the world's most competitive information society by 2010''.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

