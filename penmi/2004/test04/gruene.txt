$ANREDE,

Die deutsche Bundesregierung setzt sich derzeit �ber den EU-Rat f�r
Softwarepatente ein.  Die vom Europ�ischen Parlament beschlossenen
�nderngen an der Richtlinie \"�ber die Patentierbarkeit
computer-implementierter Erfindungen\" sollen nach dem Willen des
zust�ndigen Ministerialbeamten ohne Diskussion vom Tisch gewischt und
durch versch�fte Pro-Patent-Extrempositionen ersetzt werden, die man
in zweiter Lesung durch das neue Parlament zu peitschen versuchen
wird.  Im Bundestag gibt es zwar einigen Unmut dar�ber, aber bislang
halten sowohl die Gr�nen als auch die SPD still.

Den derzeitigen Stand der Rats-Geheimverhandlungen sie auf

     http://swpat.ffii.org/papiere/europarl0309/cons0401/index.de.html

analysiert.

Wir w�rden Sie gerne zu einem Dialog mit Software-Entwicklern,
Unternehmern, Wissenschaftlern und Gesetzgebern aus Parlament,
Kommission und Rat einladen, der sich st�ndig im Netz und gelegentlich
im Europaparlament ereignet, und zwar am 14. April und 9.-10. November
in Br�ssel.

Au�erhalb des Programms vom 14. April, etwa am 13. oder 15. April,
stehe ich mit einigen Kollegen f�r Gespr�che in Ihrem B�ro in Br�ssel
zur Verf�gung.

Wie das Programm vom 14. April 

         http://plone.ffii.org/events/2004/bxl04/prep/

zeigt, gibt es drei gro�e Themen mit zugeh�riger Expertenrunde:

         Podium I (Vorsitz: MdEP Olga Zrihen)
         Neueste Entwicklungen bei Patenterteilung und -durchsetzung 
         http://lists.ffii.org/mailman/listinfo/bxl44panel1/

         Podium II (Vorsitz: MdEP Piia-Noora Kauppi)
         Leistungsbemessung von Gesetzesvorschl�gen: Parlament vs Rat
         http://lists.ffii.org/mailman/listinfo/bxl44panel2/

         Podium III (Vorsitz: MdEP Daniel Cohn-Bendit)
         Patente und Wettbewerbsf�higkeit von Wissens�konomien
         http://lists.ffii.org/mailman/listinfo/bxl44panel3/

Simultandolmtschen zwischen allen EU-Sprachen steht w�hrend der
Konferenz zur Verf�gung.

Die Diskussion wird nach den Sitzungen in Br�ssel weitergef�hrt
und hoffentlich in ein Buch m�nden, das um die Zeit der Konferenz
im November fertig sein sollte.

Abgesehen davon w�rde ich Sie gerne auch einmal am Rande der
Ereignisse, vielleicht am Dienstag oder Donnerstag, zu einem privaten
Gespr�ch treffen.

Mit freundlichen Gr��en

-- 
Hartmut Pilch, FFII & Eurolinux Alliance              tel. +49-89-18979927
Protecting Innovation against Patent Inflation	     http://swpat.ffii.org/
300,000 votes 2000 firms against software patents    http://noepatents.org/
