#!/bin/bash

function vcat { eval echo -e \""$(cat $@)"\"; } 

B=$1;
SUBJ=${2-"Coming Software Patent Week"};

cat $B.adr | while read ADR ANREDE;do echo -e "$ADR\t$ANREDE";test "$ADR" -a "$ANREDE" && echo $ADR | grep "@" && expr "$ANREDE" : "Dear" && { echo OK;sleep 1s;vcat $B.ltr | mailx -s "$SUBJ" $ADR; };done
