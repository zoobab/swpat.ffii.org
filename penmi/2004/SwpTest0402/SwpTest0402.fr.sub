\begin{subdocument}{SwpTest0402}{Legislation Benchmarking Conference Brussels February 2004}{http://swpat.ffii.org/dates/2004/SwpTest0402/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/08/25 par Odile BÉNASSY\footnote{http://obenassy.free.fr/}}{Organise a conference in Brussels in February which will scrutinise current proposals for legislation on the limits of patentability with regard to software, measuring their effect on sample patents and model cases.}
\begin{itemize}
\item
{\bf {\bf Ensemble de tests pour la l\'{e}gislation sur les limites de la brevetabilit\'{e}\footnote{http://swpat.ffii.org/analyse/tests/index.fr.html}}}

\begin{quote}
Pour tester la capacit\'{e} d'un loi sur la brevetabilit\'{e}, nous devons essayer des innovations exemples.  Chaque exemple est d\'{e}crit par un \'{e}tat de la technique, un enseignement technique et une s\'{e}rie de revendications.  Dans l'hypoth\`{e}se que ces d\'{e}scriptions sont pertinentes, nous essayons notre nouvelle r\`{e}gle de loi.  Notre attention se porte sur (1) la clart\'{e} (2) l'\'{e}ffet macro-\'{e}conomique du resultat:  la r\'{e}glementation propos\'{e}e m\`{e}ne-t-elle \`{a} une d\'{e}cision pr\'{e}visible?  Quelles revendications seront accept\'{e}es?  Ce r\'{e}sultat exprime-t-il nos souhaits?  Nous essayons diff\'{e}rentes propositions de lois sur la m\^{e}me s\'{e}rie d'exemples (Testsuite) et comparons lesquelles r\'{e}ussissent le mieux.  Pour un programmeur c'est une question d'honneur que de ``supprimer les erreurs avant de diffuser le programme'' (first fix the bugs, then release the code).  Les ensembles de tests sont un moyen connu pour atteindre ce but.  D'apr\`{e}s l'article 27 ADPIC (TRIPS) la l\'{e}gislation appartient \`{a} un ``domaine de la technique'' notamment ``d'ing\'{e}nierie sociale'' (social engineering), n'est-ce pas ?  Technicit\'{e} ici ou l\`{a}, il est temps d'aborder de ce c\^{o}t\'{e} la l\'{e}gislation avec cette rigueur m\'{e}thodique, qui est partout annonc\'{e}e, o\`{u} les mauvaises d\'{e}cisions de construction peuvent fortement porter atteinte \`{a} la vie des individus.
\end{quote}
\filbreak

\item
{\bf {\bf Propositions de mesures\footnote{http://swpat.ffii.org/papiers/europarl0309/cpedu/index.en.html}}}

\begin{quote}
La proposition de directive de la Commission Europ\'{e}enne sur la Brevetabilit\'{e} des Innovations Informatiques n\'{e}cessite une r\'{e}ponse du Parlement Europ\'{e}en, des gouvernements des \'{E}tats membres et autres acteurs politiques. Voici nos propositions.
\end{quote}
\filbreak

\item
{\bf {\bf Parlamentarischer Abend Berlin 03-12-09\footnote{http://swpat.ffii.org/dates/2003/bund12/index.de.html}}}

\begin{quote}
Das Europ\"{a}ische Parlament hat den Spie{\ss} umgedreht: aus einer geplanten Richtlinie zur Legalisierung von 30000 gesetzeswidrig erteilten Logikpatenten ist eine Richtlinie geworden, welche die Nichtigkeit dieser Patente unmissverst\"{a}ndlich best\"{a}tigt.  W\"{a}hrend der Europ\"{a}ische Rat im April 2003 sich f\"{u}r den ``Schutz von Patenten'' im Softwarebereich aussprach, hat sich das Europ\"{a}ische Parlament im September 2003 f\"{u}r einen Schutz der Software-Entwickler vor Patenten entschieden.  W\"{a}hrend die Bundesregierung sich im Ministerrat f\"{u}r Programm-Anspr\"{u}che stark machte und eine europaweite F\"{u}hrungsrolle bei ihrer Durchsetzung \"{u}bernahm, hat sich das Europ\"{a}ische Parlament mit einer Mehrheit 90\percent{} gegen Programmanspr\"{u}che ausgesprochen.  Das Parlament widerstand dabei allerlei T\"{a}uschungs- und Einsch\"{u}chterungsversuchen.  Jetzt liegt allerdings der Ball wieder im Feld des Ministerrates.  Die Patentpolitiker der Bundesregierung zeigen sich weiterhin bem\"{u}ht, ihre Alleinherrschaft \"{u}ber die Regelsetzung im Bereich des Patentwesens aufrecht zu erhalten und zum Wohle der Patentjuristenzunft zu nutzen.  Wenn das Bundesministerium der Justiz keine \"{o}ffentlichen schriftlichen Vorgaben von h\"{o}herer Stelle bekommt, wird es weiterhin in Br\"{u}sseler Hinterzimmern auf grenzenlose Patentierbarkeit hinwirken und zugleich mit beschwichtigenden Leerformeln \"{u}ber seine Politik Auskunft geben.  Der FFII e.V. l\"{a}dt zu einer Veranstaltung am Dienstag, den 9. Dezember um 19.00 bei der Deutschen Parlamentarischen Gesellschaft in Berlin ein, um nach Wegen zu suchen, wie man diese Vorgaben erarbeiten kann.
\end{quote}
\filbreak

\item
{\bf {\bf 2003/05/07-08 BXL: Brevets Logiciels: De la S\'{e}mantique Juridique \`{a} la R\'{e}alit\'{e} \'{E}conomique\footnote{http://swpat.ffii.org/dates/2003/europarl/05/index.fr.html}}}

\begin{quote}
Pendant cette conf\'{e}rence interdisciplinaire \`{a} Bruxelles -- au sein et \`{a} cot\'{e} du Parlament Europ\'{e}en -- des programmeurs, ing\'{e}nieurs, enterpreneurs, juristes, \'{e}conomistes, politologues et politiciens se rencontront pour \'{e}tudier la chaine de causalit\'{e} compl\`{e}te -- de la l\'{e}gislation de brevet jusqu'aux but politique de l'UE, comme promouvoir l'innovation et la concurrence, l'administration \'{e}fficace et mince, la s\'{e}curit\'{e} juridique, un cadre d'ordre favorable aux Petites et Moyennes Entreprises (PME), et ``devenir la soci\'{e}t\'{e} d'information la plus comp\'{e}titive du monde jusqu'a 2010''.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

