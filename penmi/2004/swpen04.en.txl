<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Software Patent Events 2004

#descr: In 2004 the FFII will participate in numerous exhibitions, conferences
and hearings.  In April we will organise a conference in Brussels on
pending legislation on patents and information infrastructures.

#uWi: Council meetings

#rWa: European Parliament

#ije: Exhibitions, Conferences

#tWi: National Activities

#figa: FFII General Assemblies

#Ren: IRC meetings

#ert: The Irish presidency has published its provisional agendas for
ministerial meetings:

#hc2: Irish presidency agenda 2004/01

#eWy: Parliamentary Answer on Irish Presidency schedule

#1eo: document from 2003/10, contains hints on the Presidency schedule,
correspondes to an MSWord document found on the presidency website.

#orm: The Irish EU Council presidency published its preliminary schedule in
MSWord format in october 2003.

#WWu: The Irish EU Council Presidency is working from January to June 2004
according to the following schedule:

#whe: when

#wha: what

#uci: Our activity

#nge: Patent Working Party Meeting

#CPo: Competitiveness Council formal meeting (Brussels) -- Patent Policy
Working Party may try to prepare an agreement.

#lut: Possibly small scale %(q:working party) counter-event in/near EP

#eio: European Council (ie Heads of Government)

#soe: Possibly counter-event

#ofa: Informal meeting of Competitiveness Ministers (Co. Clare, Ireland) --
final attempt to get agreement on various items ready by May, in case
previous attempts failed.

#tWf: Competitiveness Council formal meeting (Brussels), Swpat directive on
the agenda.

#sce: Possibly a counter-event

#rpr: The relevant ministry in charge will be the %(me:ministry for
Enterprise, Trade and Employment), headed by deputy prime minister Ms.
Mary Harney.

#aWW: The May meeting is the last date for Ireland to be able to claim the
credit for any agreement, so the goal of the presidency will probably
be to push for achievments on as many dossiers as possible by then.

#gbh: If possible, they will try to get the agreement %(q:banked) in March,
to clear the agenda; the March meeting will also consider what broad
strategy and other documents in the area of Competitiveness can be
prepared that the heads of government can discuss at the end of March.

#WtW: Given that the Intellectual Property Working Party (group of national
patent experts from patent office circles) has apparently already had
two %(q:very productive) meetings (as some insiders say -- minutes of
these meetings are not available), they may feel that March 11th is
entirely possible to adopt a finalised Council common text for the
directive.  The Irish Presidency's circulation of a %(cp:Compromise
Paper) on 04-01-29 confirms this expectation.

#crn: Elections are on June 10.

#WWr: On Apr 7-9 MEPs are supposed to be at home in their electoral
districts.

#dcW: Candidates will become known in January, election platforms and
programs probably by February. E.G. the Greens are deciding on
IT-related program statements at their meeting on February 21-22 in
Rome.

#Wxb: In january and february there will be some debate and decision about
the %(id:IP Enforcement Directive).  Thereafter, debates will be
overshadowed by the elections.

#nte: Europarl Plenary Meeting Schedule 2004

#kmy: Strasburg session weeks marked red: 01/12-15; 02/9-12; 03/8-11, 29-1;
04/19-22; 05/3-8; 06/10-13; 07/20-23; 09/13-16, 29-30; 10/25-28;
11/15-18; 12/13-16

#efo: Our permanent Brussels/Strasburg representative Erik Josefsson will
arrive in late December and prepare the ground for people who want to
fly to Brussels for a week / a few days of work, for which we pay the
costs.  Erik will be working on a citizen's guide to influencing the
elections and EU politics in general.

#abW: Our %(lb:Legislation Benchmarking Conference) will be near the
Parliament.  Panels will be presided by MEPs.

#IWl: Freedom of Information Infrastructures will be a subject in the
election campaign.

#sWe: Erik will help set up meetings that try to open up the secrecy of the
Council %(q:patent expert) meetings and get more people involved in a
discussion near the site.

#ees: The reelected parliament will face the Council's decision in a second
reading and the Council patent lawyers should see that it will not
make life easier for them than the previous one.

#iWn: Contains some info about upcoming events.

#Whe: When

#Whe2: Where

#Eve: Event

#FWl: FFII role

#nhg: in charge

#eWc: near Aachen

#enc: Geilenkirchen

#tin: We can have a booth and distribute materials.  This is an opportunity
for learning more about the telecommunication field, in which
patent-based business models are more strongly entrenched than in the
PC or server software field.

#Wum: This is mainly an opportunity to inform supporters and recruit
helpers.  A speech/workshop time would have to be determined now.

#kch: Probably Europe's largest IT fair.

#hWo: Catholic University

#uiW: speaker

#UWa: LUG Camp

#iWe: FFII will at least be able to provide a speaker.

#rnW: Grüne IT

#tre: IT event of the Greens, requested a speaker from FFII.

#EjW: FFII has been present every year since 2000.  Similar event RMLL in
Lorraine may be of interest.

#iWW: Swiss IT trade fair, participation only if local volunteers want it.

#owe: IT fair where FFII has been present during most years, but needs
preparation.

#rfr: Information Infrastructure 2004

#Wnr: FFII may plan a tour through various European capitals, especially in
Eastern Europe, in March.

#feo: Due to a conflict with the SPD New Year Reception, this has been
postponed to 2004-03-10.  Key players have been informed orally,
written inivitations will be sent out before Christmas.

#lee: Details to be expected in a few days.

#eWo: Some people at FSF IE may be doing something.

#rur: We are fairly well set up in Belgium, but no event dates are fixed so
far.

#ont: João is planning something.

#yyw: A general assembly must be held and will probably be piggybacked onto
one of the other events between FOSDEM and SYSTEMS.

#WiW: It is possible to have several general assemblies.  They can also be
virtual.  See %(st:statutes).

#cWv: We regularly meet at %(IRC) for planning, especially in the evening. 
We try to announce subjects for the talks on relevant mailing lists in
advance.

#eWt: List event schedules of various institutions here, e.g.

#oni: Council

#_903

#aie: Parliament

#WIP: WIPO

#bgn: We must be present at %(SPLT) negotiations in Geneva, June (?)

#eoD: IT exhibitions, FOSDEM etc

#WsE: EU Commission Events, e.g. IST

#iie: Vrijschrift Events

#nhs: Includes an event on patents and copyright with Richard Stallman and
Michael Hart in Amsterdam on 2004-02-23

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpen04 ;
# txtlang: en ;
# multlin: t ;
# End: ;

