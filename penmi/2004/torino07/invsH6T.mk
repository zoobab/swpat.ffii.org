# -*- mode: makefile -*-

invsH6T.lstex:
	lstex invsH6T | sort -u > invsH6T.lstex
invsH6T.mk:	invsH6T.lstex
	vcat /ul/prg/RC/texmake > invsH6T.mk


invsH6T.dvi:	invsH6T.mk invsH6T.tex
	rm -f invsH6T.lta
	if latex invsH6T;then test -f invsH6T.lta && latex invsH6T;while tail -n 20 invsH6T.log | grep -w references && latex invsH6T;do eval;done;fi
	if test -r invsH6T.idx;then makeindex invsH6T && latex invsH6T;fi

invsH6T.pdf:	invsH6T.ps
	if grep -w '\(CJK\|epsfig\)' invsH6T.tex;then ps2pdf invsH6T.ps;else rm -f invsH6T.lta;if pdflatex invsH6T;then test -f invsH6T.lta && pdflatex invsH6T;while tail -n 20 invsH6T.log | grep -w references && pdflatex invsH6T;do eval;done;fi;fi
	if test -r invsH6T.idx;then makeindex invsH6T && pdflatex invsH6T;fi
invsH6T.tty:	invsH6T.dvi
	dvi2tty -q invsH6T > invsH6T.tty
invsH6T.ps:	invsH6T.dvi	
	dvips  invsH6T
invsH6T.001:	invsH6T.dvi
	rm -f invsH6T.[0-9][0-9][0-9]
	dvips -i -S 1  invsH6T
invsH6T.pbm:	invsH6T.ps
	pstopbm invsH6T.ps
invsH6T.gif:	invsH6T.ps
	pstogif invsH6T.ps
invsH6T.eps:	invsH6T.dvi
	dvips -E -f invsH6T > invsH6T.eps
invsH6T-01.g3n:	invsH6T.ps
	ps2fax n invsH6T.ps
invsH6T-01.g3f:	invsH6T.ps
	ps2fax f invsH6T.ps
invsH6T.ps.gz:	invsH6T.ps
	gzip < invsH6T.ps > invsH6T.ps.gz
invsH6T.ps.gz.uue:	invsH6T.ps.gz
	uuencode invsH6T.ps.gz invsH6T.ps.gz > invsH6T.ps.gz.uue
invsH6T.faxsnd:	invsH6T-01.g3n
	set -a;FAXRES=n;FILELIST=`echo invsH6T-??.g3n`;source faxsnd main
invsH6T_tex.ps:	
	cat invsH6T.tex | splitlong | coco | m2ps > invsH6T_tex.ps
invsH6T_tex.ps.gz:	invsH6T_tex.ps
	gzip < invsH6T_tex.ps > invsH6T_tex.ps.gz
invsH6T-01.pgm:
	cat invsH6T.ps | gs -q -sDEVICE=pgm -sOutputFile=invsH6T-%02d.pgm -
invsH6T/invsH6T.html:	invsH6T.dvi
	rm -fR invsH6T;latex2html invsH6T.tex
xview:	invsH6T.dvi
	xdvi -s 8 invsH6T &
tview:	invsH6T.tty
	browse invsH6T.tty 
gview:	invsH6T.ps
	ghostview  invsH6T.ps &
print:	invsH6T.ps
	lpr -s -h invsH6T.ps 
sprint:	invsH6T.001
	for F in invsH6T.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	invsH6T.faxsnd
	faxsndjob view invsH6T &
fsend:	invsH6T.faxsnd
	faxsndjob jobs invsH6T
viewgif:	invsH6T.gif
	xv invsH6T.gif &
viewpbm:	invsH6T.pbm
	xv invsH6T-??.pbm &
vieweps:	invsH6T.eps
	ghostview invsH6T.eps &	
clean:	invsH6T.ps
	rm -f  invsH6T-*.tex invsH6T.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} invsH6T-??.* invsH6T_tex.* invsH6T*~
invsH6T.tgz:	clean
	set +f;LSFILES=`cat invsH6T.ls???`;FILES=`ls invsH6T.* $$LSFILES | sort -u`;tar czvf invsH6T.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
