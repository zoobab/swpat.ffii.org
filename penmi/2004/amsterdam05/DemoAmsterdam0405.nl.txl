<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Demo Amsterdam 2004/05

#descr: Under pressure from the Dutch government, the EU is being pushed to legalise patents on %(q:computer-implemented) algorithms and business methods.  The European Parliament voted to confirm the non-patentability of software in September 2003, with support from many dutch MEPs, against heavy pressure from Commissioner Frits Bolkestein and from national governments.  The dutch government is currently ignoring the voices of most programmers and economists and the decisions of the dutch parliament of 2001, which were based on a consensus of the whole software sector.  Instead, the government is slavishly following the patent arms of a few large corporate entities (e.g. Philips) and industry associations manipulated by them and blocking all discussions about different policy options and their effects.  By organising a demonstration, press conference and study conference we want to throw the light of public scrutiny on the questionable backroom dealings of Dutch politicians in Brussels and give this subject the weight which it deserves in the upcoming elections to the European Parliament.  Goals of the march may include government buildings, patent institutions, party headquarters and headquarters of some of the companies and trade associations that have been abused by the patent lobby for false representation claims.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: DemoAmsterdam0405 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

