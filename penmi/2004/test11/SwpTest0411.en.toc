\contentsline {section}{\numberline {1}When and Where -- Practical Information}{2}{section.1}
\contentsline {section}{\numberline {2}Subjects for Panels \& Workshops}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Specialised Panels (Day 1)}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Recent developments in patent granting, licensing and litigation}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Patent Strategies of Companies, Universities and other Players in the Private and Public Sector}{2}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Patent Risk Assessment, ``Patent Insurance'' and Rules for Public Procurement of Microsoft vs Free Software Solutions}{3}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}The Software Patent Directive and Software Procurement Decisions in Public Administrations}{3}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}In search of the ``Good Patent'' for a ``Computer-Implemented Invention''}{3}{subsubsection.2.1.5}
\contentsline {subsection}{\numberline {2.2}Political Panels (Day 2)}{3}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Software Patent Directive -- the Legislative's vs the Executive's Version}{3}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Toward a Copyright-Based Paradigm of Industrial Property?}{3}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Toward more Legitmate Lawmaking Processes in the EU}{4}{subsubsection.2.2.3}
