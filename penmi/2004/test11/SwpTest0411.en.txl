<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-11-09..11 Brussels: Regulating Knowledge -- Costs, Risks and
Models of Innovation

#descr: On 9-10 November of 2004, FFII and others are organising a conference
on the software patent directive proposal and related issues in and
near the European Parliament in Brussels.

#luW: On 9-10 November of 2004 a Conference on the effects of patents,
copyright and other regulatory regimes on the knowledge-based economy
will take place in and near the European Parliament in Brussels.  The
conference is organised by the Foundation for a Free Information
Infrastructure (FFII) in cooperation with the Maastricht Institute for
Research in Information Economics (MERIT) and members of the European
Parliament.  It is the third conference of its kind and may be
accompanied by other activities concerning ongoning legislation on the
limits of patentability with regard to software and business methods
in the European Union.   Practitioners, scientists and legislators
will try to subject proposed or envisaged legislation to rigid
%(q:debugging), i.e. testing for consistency and efficacy.   Players
of the recent transatlantic %(q:patent insurance) and %(q:open source
risk managment) debate will meet for the first time.  Representatives
of the patent faith are invited to present examples of what they
consider %(q:good patents) for %(q:computer-implemented inventions). 
Authors of recent studies will assess the efficiency of the patent
system in today's innovation and the viability of alternative models
such as %(q:industrial copyright).

#aPo: When and Where -- Practical Information

#ear: Subjects for Panels & Workshops

#ceu: Schedule

#01W: 2004-11-09 tue

#eWm: Hotel near Parliament

#eia: Specialist Panels

#01W2: 2004-11-10 wed

#rWa: European Parliament

#lan: Political Panels

#rft: More Information

#epo: Conference Preparation Workspace

#oia: This workspace contains more current information, especially
concerning the speakers who have so far accepted our invitation.

#tpm: Each panel will consist of 1/3 introduction (usually a presentation by
one panelist), 1/3 discussion between the panelists and 1/3 discussion
with the audience.  As in the past, most panelists will be chaired by
members of the European Parliament.

#esn: Specialised Panels

#maiXdjedi: Day %1

#lan2: Political Panels

#vgn: Recent developments in patent granting, licensing and litigation

#gWt: Patent Strategies of Companies, Universities and other Players in the
Private and Public Sector

#sWW: Patent Risk Assessment, %(q:Patent Insurance) and Rules for Public
Procurement of Microsoft vs Free Software Solutions

#WWi: The Software Patent Directive and Software Procurement Decisions in
Public Administrations

#oon: In search of the %(q:Good Patent) for a %(q:Computer-Implemented
Invention)

#odE: co-organised with CEA-PME

#eno: In late August 2004, the city of Munich announced a delay of a public
tender for implementation of its %(q:Linux base client) due to
imponderabilities of pending EU software patent legislation.  Shortly
before that, a discussion about %(q:open source risk managment) had
erupted in the United States.  Both discussions are highly
controversial.  We bring the players togehter.

#PyW: We challenge the patent lobby (e.g. EPO, DG Internal Market or EICTA)
to show us an example of a desirable claim to what they like to call a
%(q:computer-implemented invention), and to explain how they want such
claims to be distinguished from undesirable claims.

#aeu: Software Patent Directive -- the Legislative's vs the Executive's
Version

#Wrl: Toward a Copyright-Based Paradigm of Industrial Property?

#mwW: Toward more Legitmate Lawmaking Processes in the EU

#roe: Becoming the World's Most Competitive Knowledge Economy by 2010 --
Theory and Practise of the EU's Agenda

#tWo: Protagonists of the Council's and the Parliament's version will
debate.

#Wrr: As we did not provide enough time on %(s4:April 14), such a debate has
not really happened yet.

#joW: Much of today's hardware is really a kind of write-only software,
similar to architecture, where the material medium is completely
modelled and a construction plan can be safely expected to work. 
Where the forces of nature are not yet reduced to reliable
computational models, experiments are often carried out in batches of
thousands per day, potentially leading to dozens of patents in one
day, most of which are not practised but only stockpiled.

#Wkn: Under such conditions, a system of hardcoded broad claims (patents) is
likely to perform less well than a system of narrow and informally
accruing claims.

#orW: This panel will examine our %(ip:hypothesis) that the duality of
patents vs copyright law is a thing of the past and future lies in an
integrated system based on copyright-like principles, possibly
combined with some sui-generis system for special niches.

#ati: While comedies such as %(q:Yes Minister) poke fun at the circumvention
of democracy by civil servants at the national level, the EU seems to
have added an extra layer of circumvention.  A lare series of
legislative proposals that promote the interests of ministerial
bureaucracies at the expense of citizens and SMEs have been laundered
through the EU's legislative processes.  Checks and balances have been
largely put out of function at this level, and political newspeak
known from dictatorial states has become entrenched and is regularly
used to evade whatever informed discussion may still be taking place. 
The European Parliament's strong stance on the Software Patent
directive is seen by many as an exceptional phenomenon, and has made
some conflicts highly visible that otherwise are difficult to study. 
As symbolised in the celebration of September 24th as %(q:EU Democracy
Day), it may be even be interpreted as a possibly important fork in
the path of the EU toward the development of sane legislative
processes.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpTest0411 ;
# txtlang: en ;
# multlin: t ;
# End: ;

