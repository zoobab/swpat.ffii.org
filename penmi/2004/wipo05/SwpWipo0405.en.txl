<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: WIPO 2004-05-10..4 Geneva Patent Policy Session

#descr: The World Intellectual Property Organisation's Standing Committee on
Patent Law is meeting in Geneva for a week to work on the draft
Substantive Patent Law Treaty.  This treaty aims to mandate worldwide
unlimited patentability while imposing strict limitations on patent
quality.  Not all governments are happy with this attempt at bypassing
democracy in areas where fundamental freedoms are concerned.  WIPO
should act as a standardising agency and not as a world legislator on
substantive patent law.  FFII plans to spread the word in Geneva and
in the participating countries.  We will be one of very few NGOs who
are not pushing for extension of right-holder interests.

#iWn: Contains some info about upcoming events

#imf: 10th Session of the Standing Committee on Patent Law (SCP) in Geneva. 
Substantive Patent Law Treaty (SPLT) is on the agenda.

#Pia: WIPO Patent Agenda Timetable

#son: Events of 2002, no information on 2004 visible

#snt: Request for Comments on Patent Agenda

#Wis: Closing date was 2004/02/15

#mmK: Comment by Brian Kahin

#moW: Comment by John Tobey

#eWo2: We need something analogous for SPLT.  Also we need to describe FFII
in terms similar to those used here.

#ati: Contact WIPO to accredit FFII

#eWr: Prepare materials

#uiW: These should be useful for participating delegations.  They include

#asS: Analysis of SPLT

#sir: Analysis and classifciation of EPO software patents

#tsv: Proposals for standards that can be useful for cooperation in
classifying patents without the implication that any such patents must
be valid in any particular country.

#teb: Information about the European traditional approach to the technical
invention and its reconfirmation by the European Parliament

#ruW: Information about the WSIS summit.

#lse: Simple awareness-raising leaflets

#aWr: name cards

#caW: Contact Participants of the session

#hha: Find out who will participate, exchange ideas on what we can
contribute.

#tWn: Some governmental officials may be quite happy to accept our help in
developping their approaches.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpWipo0405 ;
# txtlang: en ;
# multlin: t ;
# End: ;

