<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Parlamentarischer Abend Berlin 2004-03-10

#descr: Die Patentjuristen der Bundesregierung zeigen sich weiterhin bemüht,
ihre Alleinherrschaft über die Regelsetzung im Bereich des
Patentwesens aufrecht zu erhalten und zum Wohle ihrer Zunft zu nutzen.
 Solange es keine nationale Rechtspolitik zur Begrenzung des
patentrechtlichen Erfindungsbegriffes gibt, an der sich das BMJ
orientieren kann, wird das BMJ weiterhin in Brüsseler Hinterzimmern
auf grenzenlose Patentierbarkeit hinwirken und zugleich mit
beschwichtigenden Leerformeln über sein Wirken Auskunft geben.  Der
FFII e.V. lädt zu einer Veranstaltung am Mittwoch, den 10. März 2004
in Berlin ein, um nach Wegen zu suchen, wie man die fehlende
Rechtspolitik erarbeiten und durchsetzen kann.

#bkg: Das Europäische Parlament hat den Spieß umgedreht: aus einer geplanten
Richtlinie zur Legalisierung von 30000 gesetzeswidrig erteilten
Logikpatenten ist eine Richtlinie geworden, welche die Nichtigkeit
dieser Patente unmissverständlich bestätigt.  Während der Europäische
Rat im April 2003 sich für den %(q:Schutz von Patenten) im
Softwarebereich aussprach, hat sich das Europäische Parlament im
September 2003 für einen Schutz der Software-Entwickler vor Patenten
entschieden.  Während die Bundesregierung sich im Ministerrat für
Programm-Ansprüche stark machte und eine europaweite Führungsrolle bei
ihrer Durchsetzung übernahm, hat sich das Europäische Parlament mit
einer Mehrheit von 90% gegen Programmansprüche ausgesprochen.  Das
Parlament widerstand dabei allerlei Täuschungs- und
Einschüchterungsversuchen.  Jetzt liegt allerdings der Ball wieder im
Feld des Ministerrates.  Die Patentpolitiker der Bundesregierung
zeigen sich weiterhin bemüht, ihre Alleinherrschaft über die
Regelsetzung im Bereich des Patentwesens aufrecht zu erhalten und zum
Wohle der Patentjuristenzunft zu nutzen.  Wenn das Bundesministerium
der Justiz keine öffentlichen schriftlichen Vorgaben von höherer
Stelle bekommt, wird es weiterhin in Brüsseler Hinterzimmern auf
grenzenlose Patentierbarkeit hinwirken und zugleich mit
beschwichtigenden Leerformeln über seine Politik Auskunft geben.  Der
FFII e.V. lädt Abgeordnete und Ministerialbeamte zu einer
Veranstaltung am Mittwoch, den 10. März 2004 in Berlin ein, um nach
Wegen zu suchen, wie man diese Vorgaben erarbeiten kann.

#eve: Der Parlamentarische Abend findet am 10. März 2004 um 19.00 Uhr im
Palais der Deutschen Parlamentarischen Gesellschaft,
Friedrich-Ebert-Platz 2, 10117 Berlin, statt.

#nrk: Es werden etwa 30-40 Personen erwartet, darunter 10-20 Politiker.

#eug: Schirmherr ist MdB %(UK).

#jan: Dieser Parlamentarische Abend ist die Nachfolgeveranstaltung eines
Parlamantarischen Abends vom 28. Januar 2004.

#kei: Anmeldung erfolgt über das %(mw:Mitwirkungssystem).  Nur Eingeladene
haben Zutritt.  Einige Eintrittskarten werden für 80 eur verkauft.

#Fie: Weitere Fragen richten Sie bitte an die Projektleitung %(MT).

#xB2: Antwortbrief der Justizministerin

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpBund0403 ;
# txtlang: de ;
# multlin: t ;
# End: ;

