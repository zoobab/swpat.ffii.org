<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The Spanish Patent and Trademark Office (ES-PTO/OEPM), the European Patent Office (EPO), the Industrial Property Unit of the European Commission and together with patent lawyers from UNICE (employers' federation) and other European Patent Movement strongholds are, under the auspices of the Spanish EU presidency and the Spanish Ministry for Economy and Technology, holding a two-day seminar in the Spanish Patent Office in Madrid.   The seminar's agenda and invitation presupposes that there is an %(q:evergrowing need) for %(q:protecting) software innovation and channels the discussion into questions of how companies should go ahead doing this.  In spite of the existence of a strong movement against software patents in Spain, the seminar keeps silent on the controversy and does not offer critical voices any space for exposing their views.
title: ES Patent Orifice: Software Patentability Rally Madrid 2002-05-06
ESl: Long article in El Pais, written shortly before the Madrid Patent Orifice event.  Reports about mixed sentiments toward software patents and rections to the CEC directive proposal in Spain.  Says that UK and DE are pro swpat while FR is against and ES is undecided.  Finds that most of the players know very little about the directive proposal or about swpat and that nobody really needs them while some severely oppose them.
Pgm: Programme
TeW: The seminar can be attended remotely by real audio via this %(pw:web page).
cWa: %1 page
mgW: mailing list thread
coa: commentary on the El Pais article
Snl: Spanish workgroup on innovation and related legal policies

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpespo025 ;
# txtlang: en ;
# End: ;

