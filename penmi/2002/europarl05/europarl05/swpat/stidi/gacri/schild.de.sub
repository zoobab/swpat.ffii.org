\begin{subdocument}{swpatgacri}{Kollektive Schutzschilder gegen Softwarepatente?}{http://swpat.ffii.org/analyse/schild/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die Vorstellung, die Gemeinde der Entwickler freier/quelloffener Software k\"{o}nnten ein eigenes Patentportfolio aufbauen, muss am Widerspruch zwischen den Grundregeln der freien Software und dem Verwertung erzwingenden Ansatz des Patentwesens weitgehend scheitern.  Andererseits ist die freie Welt gegen\"{u}ber der propriet\"{a}ren in einem Punkt im Vorteil:  bei der Dokumentierung und Verwertung des Standes der Technik.  Die Gemeinde k\"{o}nnte bei kluger Vorgehensweise erheblichen Nutzen aus ihrer offenen Entwicklungsweise und aus der im Patentwesen eingebauten Rechtsunsicherheit ziehen.  Sogenanntes Defensives Ver\"{o}ffentlichen, wie es Foresight Institute und andere vorschlagen, ist hingegen wiederum eine unglaublich t\"{o}richte Idee.  Die wichtigste Aufgabe zu diesem Zeitpunkt (Fr\"{u}hjahr 2001) ist eine konsequente Bewehrung aller St\"{a}tten der Softwareentwicklung mit Mechanismen der Zeitstempelung.}
\begin{sect}{def}{Schlechte Karten beim Defensiven Patentieren}
Der Erwerb und Erhalt von Patenten ist mit hohen Kosten verbunden
\begin{description}
\item[Motivationskosten:]\ Developpers find new ideas every day and are usually not interested in the tedious work of distilling these ideas into patent applications. They want to develop products and not patents, and often resent patenting on various grounds.  Even though some laws guarantee them a share in revenues from the patent, this usually is not enough of a motiviation.  Large companies like Siemens, IBM or SAP have therefore installed well-staffed committees of patent application experts and ample bonusses for developpers in order to overcome this problem and provide suffient incentives for developping patents.
\item[Erwerbskosten:]\ According to EPO figures, a European patent costs an average of 30000 EUR in office fees, assuming that it is upheld for only 10 years and not translated to all European languages.  Costs for patent attorneys are much higher and often calculated on a basis of 300 EUR per hour.
\item[Aufrechterhaltungskosten:]\ Even simple litigation against one infringer or opponent will cost more than 100000 EUR/USD in lawyer fees.  Often the cost is in the range of millions, and many inventors even of important basic patents have gone bankrupt over this (e.g. Goodyear).  Insurance policies for patent litigation either don't exist or are astronomically high.
\end{description}

Diese Kosten m\"{u}ssen durch die gnadenlose Aus\"{u}bung des Monopolrechts refinanziert werden.  Das erfordert eine Kombination aus Sockelpreis und St\"{u}ckpreis.  Freie/quelloffene Software kennt nicht den Begriff der St\"{u}ckzahl.  Somit kann nur ein hoher Sockelpreis anfallen, wie z.B. 1 Million USD im Falle der MP3-Lizenzen.  Es ist unwahrscheinlich, dass jemand einen solchen Preis aufbringt, nur um der \"{O}ffentlichkeit freie Software schenken zu d\"{u}rfen.  So etwas t\"{a}te nur ein Staat oder eine potente Stiftung, nicht aber ein privater Teilnehmer des Wirtschaftslebens.  Somit bleibt noch die M\"{o}glichkeit, zwar freie Software (z.B. GPL) zu erlauben, aber f\"{u}r propriet\"{a}re Implementationen Lizenzgeb\"{u}hren zu verlangen.  In diesem Falle werden aber meist keine hohen Lizenzeinnahmen zu erzielen sein, da man eine m\"{a}chtige freie Konkurrenz zu sich selbst beg\"{u}nstigt.  Mit einem solchen Modell der Patentverwertung l\"{a}sst sich schwer gegen diejenigen ank\"{a}mpfen, die ohne jegliche idealistische Belastung direkt auf Profitmaximierung unter den Regeln des Patentwesens zielen.
\end{sect}

\begin{sect}{prio}{Gute Karten beim Nutzen vorbekannter Technik}
Wie wir sahen macht die propriet\"{a}re Natur des Patentsystems es fast unm\"{o}glich, dieses System zum Aufbau eines Schutzschildes f\"{u}r die Entwickler freier Software zu nutzen.
Anderseits bedingt gerade die propriet\"{a}re Natur des Systems eine Schwachstelle, die man nutzen kann.
Die Entwickler freier Software publizieren besonders umfangreiche Mengen von Ideen in einem fr\"{u}hen Stadium.  Solche Publikationen sind selbstverst\"{a}ndlich als ``Stand der Technik'' anerkannt.  Darauf aufgebaute sp\"{a}tere Patente werden entkr\"{a}ftet, sobald die vorbekannte Idee gefunden wird.

Genau genommen ber\"{u}hrt dies die Hauptgefahrenquell nicht im geringsten:  es wird trotzdem zahlreiche breite und triviale Patentanspr\"{u}che geben, gegen die kein ``Stand der Technik'' zitiert werden kann.  Doch es ist f\"{u}r beide Seiten schwierig, den wirklichen ``Stand der Technik'' zu ermitteln.  Niemand wei{\ss} genau, ob sein Patent wirklich g\"{u}ltig ist.  Daher wird in dem Ma{\ss}e, wie ein gro{\ss}es un\"{u}bersichtliches Meer von Archiven existiert, aus dem die Geschichte der Software-Ideen mit genauem Datum rekonstruierbar ist, niemand mehr sicher sagen k\"{o}nnen, ob und inwieweit ein bestimmtes Patent g\"{u}ltig ist.  Hieraus allein ergibt sich ein rationaler Grund f\"{u}r egoistische Unternehmen, die Gemeinde der Entwickler Freier Software nicht allzu unvorsichtig herauszufordern.  Sie h\"{a}lt n\"{a}mlich folgende starke Karte in den H\"{a}nden:
\begin{enumerate}
\item
Ein gro{\ss}es Arsenal m\"{o}glicherweise neuheitssch\"{a}digender Publikationen, die kein Einzelner \"{u}berblicken kann.

\item
Eine grunds\"{a}tzliche Unsicherheit bez\"{u}glich der G\"{u}ltigkeit der Patentportfolios aller Marktteilnehmer.  Viele davon sind Kartenh\"{a}user, die zum Einsturz gebracht werden k\"{o}nnten, was dann die Investoren verscheucht und daher von den meist kurzfristig denkenden Entscheidern um jeden Preis vermieden werden muss.

\item
Eine gro{\ss}e Gemeinde kompetenter und motivierter Softwareentwickler, die in der Lage ist, durch kollektive Kampagnen das Gesch\"{u}tz ``Stand der Technik'' gegen unfreundliche Firmen in Stellung zu bringen.
\end{enumerate}

Unter diesen Umst\"{a}nden erscheint es im Moment als vordringliche Aufgabe, den Entwicklungsprozess freier Software, wie er sich in CVS-B\"{a}umen, Mailinglisten u.v.m. zeigt, mit Zeitstempel zu dokumentieren.  Wo das bisher noch nicht gelingt, k\"{o}nnte man viele Diskussionen ins Usenet verlagern.  Dort steht mit Dejanews ein glaubw\"{u}rdiger Archivierungsdienst zur Verf\"{u}gung.  Es schadet auch nichts, vermehrt in \"{o}ffentlichen Diskussionsrunden in den verschiedensten Sprachen exhibitionistische Fantasien \"{u}ber m\"{o}gliche Softwareentwicklungen auszutauschen.  All das gilt aus der Sicht von Patentpr\"{u}fern als ``neu und erfinderisch'' und kann daher eines Tages n\"{u}tzlich werden.
\end{sect}

\begin{sect}{pror}{``Defensives Ver\"{o}ffentlichen'' Nein Danke!}
Das d\"{u}mmste was Entwickler freier Software im Moment tun k\"{o}nnten, w\"{a}re, sich von Patent\"{a}mtern in den Dienst nehmen zu lassen, um deren Suchdatenbanken aufbauen zu helfen, ``damit ung\"{u}ltige Patente erst gar nicht gew\"{a}hrt werden'', wie von ``Foresight Institute'' mit unterst\"{u}tzung einiger der ``h\"{o}chsten Opensource-Wortf\"{u}hrer'' vorgeschlagen wurde.  Diese sogenannte ``Defensive Ver\"{o}ffentlichen'' ist ein Bumerang, der sowohl gegen diejenigen, die es versuchen, als auch gegen die ganze Gemeinde zur\"{u}ckschl\"{a}gt.  Vergleichen wir einmal die Wirkungen des ``Defensiven Ver\"{o}ffentlichens'' mit denen der dezentralen Zeitstempelung des Standes der Technik.

\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
Formvollendetes Defensives Ver\"{o}ffentlichen & Zeitstempelung des normalen offenen Entwicklungsvorgangs\\\hline
Patentrelevante Ideen leicht zu recherchieren & Patentrelevante Ideen schwer zu recherchieren\\\hline
Gro{\ss}er Aufwand f\"{u}r den Entwickler, geringer Aufwand f\"{u}r den Rechercheur und Patentanmelder & Geringer Aufwand f\"{u}r den Entwickler, Gro{\ss}er Aufwand f\"{u}r die Rechercheure und Patentanmelder\\\hline
Niedrigere Patentierungskosten, mehr (breite und triviale) Softwarepatente & H\"{o}here Patentierungskosten, weniger (breite und triviale) Softwarepatente\\\hline
Unsere Softwareidee wird wahrscheinlich von niemandem patentiert werden. & Unsere Softwareidee wird vielleicht patentiert, aber der Patentinhaber wird einen Schreck bekommen, sobald  wir ihm unsere Priorit\"{a}tsnachweise vorzeigen.  Dann wird er einen hohen Bogen nicht nur um unser Projekt machen, sondern es auch tunlichst vermeiden, sich unn\"{o}tig mit anderen Entwicklern freier Software anzulegen.  Denn das k\"{o}nnte einen solidarischen Angriff nach sich ziehen, der auf einmal sein Patentportfolio schlecht aussehen l\"{a}sst.  Dann k\"{o}nnten Investoren abspringen und Aktienkurse sinken.  Da ist es dann besser, das Patentportfolio nur als Bluffpotential beizubehalten und allenfalls selektiv gegen \"{o}ffentlichkeitsscheue KMU einzusetzen.\\\hline
Allerlei Gl\"{u}cksritter versammeln sich um die Datenbank der Devensiven Ver\"{o}ffentlichungen herum, um zu schauen, wo gerade patentrelevante Ideen entwickelt werden.  Sie lassen sich dann ein paar komplement\"{a}re Ideen einfallen und patentieren diese.  Diese Nachfolgepatente sind dann g\"{u}ltig.  Unser Arbeitsfeld wird mit einem Patentdickicht blockiert. & Kaum ein Gl\"{u}cksritter findet rechtzeitig die gesuchten Hinweise, und es bleibt genug Zeit, die komplement\"{a}ren Ideen selber rechtzeitig zu dokumentieren und damit zu befreien.\\\hline
Patent\"{a}mter k\"{o}nnen mit einiger Glaubw\"{u}rdigkeit sagen:  ``Wir haben alles getan um Datenbanken aufzubauen und den Entwicklern freier Software die bestm\"{o}glichen Bedingungen zu schaffen.  Den kostenlosen Zugriff auf das Geistige Eigentum anderer h\"{a}tte jeder gerne... Statt sich weiter zu beklagen, sollten die Entwickler sich mal lieber ein paar mehr eigene Ideen einfallen lassen und sch\"{o}n brav in unsere Datenbank eintragen.'' & Patent\"{a}mter m\"{u}ssen vielleicht anerkennen, dass der Stand der Technik im Softwarebereich nicht ``blo{\ss} eine Frage der Zeit'' ist.\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{prius}{Zeitstempelung propriet\"{a}rer Entwicklung hilft auch}
Auch Entwickler propriet\"{a}rer Software sollten ihre Arbeit Tag f\"{u}r Tag genau rekonstruierbar machen.  Dies k\"{o}nnen sie tun, indem sie die gleichen zeitgestempelten Entwicklungsumgebungen verwenden wie die Entwickler freier Software und gelegentlich MD5-Pr\"{u}fsummen in Zeitungen ver\"{o}ffentlichen oder in Bibliotheken hinterlegen.

In diesem Falle entsteht kein vorbekannter ``Stand der Technik'', der das Patent zu Fall bringen k\"{o}nnte, sondern lediglich ein privates Vorbenutzungsrecht.  D.h. der Entwickler darf seine Ideen weiterhin selber vermarkten.  Er darf dieses Recht allerdings nicht ver\"{a}u{\ss}ern.

Nicht ganz klar ist bisher, was mit dem Vorbenutzungsrecht passiert, wenn der Entwickler eines Tages seine propriet\"{a}re Software unter Opensource-Lizenz ver\"{o}ffentlicht.  Stellt das eine Ver\"{a}u{\ss}erung des Rechtes auf Vermarktung der vorbenutzten Idee dar?  Steht es nicht andererseits dem Vorbenutzer frei, sein ``Produkt'' in beliebiger Weise auf den ``Markt'' zu bringen und an beliebige Kunden zu ``verkaufen'', ohne dass diese Kunden wiederum jeder f\"{u}r sich eine Patentlizenz erwerben m|ssen?  Man sieht, dass hier die Pr\"{a}missen des Patentsystems nicht mit der Realit\"{a}t des Gemeingutes Information zusammenpassen.

Solange keine gegenteiligen Richterspr\"{u}che vorliegen, darf man allerdings begr\"{u}ndet hoffen, dass es dem Vorbenutzer tats\"{a}chlich freisteht, sein Vorbenutzungsrecht im Rahmen freier Software so zu verbreitern, dass jeder das Patent umgehen kann, sofern er seine Entwicklungsarbeit wiederum in freie Software einflie{\ss}en l\"{a}sst.

Somit hat der Vorbenutzer einer patentierten Softwareidee eine besonders starke Stellung.  Er kann entweder diese Idee f\"{u}r alle frei machen oder mit dem Patentinhaber eine Gegenleistung daf\"{u}r aushandeln, dass er dies nicht tut.  Solche Aussichten k\"{o}nnen erheblich den Anreiz zur Anmeldung schwacher Patente mindern.  F\"{u}r die Allgemeinheit ist somit ein Vorbenutzungsrecht immerhin etwa halb so viel wert wie eine echte Vorver\"{o}ffentlichung.  Auch um dieses halben Wertes willen lohnt es sich f\"{u}r den Vorbenutzer, seine zeitgestempeltes Entwicklungsgeschichtsarchiv mit einem gewissen Zeitverzug der Allgemeinheit zur Verf\"{u}gung zu stellen.  Dieses Spiel kann, wenn man es wirksam spielt, die Patentierung trivialer Innovationen riskant machen.  Denn je trivialer die Innovation ist, mit desto h\"{o}herer Wahrscheinlichkeit ist anzunehmen, dass sich irgendwo \"{a}hnliches in einem privaten Entwicklungsgeschichtsarchiv findet.
\end{sect}

\begin{sect}{kionfar}{Was zu tun ist}
\begin{itemize}
\item
Ausstattung aller Netzorte, an denen Software entwickelt und besprochen wird, mit Zeitstempelungs-Mechanismen.

\item
Leicht verst\"{a}ndliche \"{o}ffentliche Dokumentation derjenigen Software-Patente, deren Inhaber oder Lizenznehmer besonders skrupellos damit umgehen.

\item
Bildung von mehr oder weniger fest organisierten Genossenschaften, deren Mitstreiter den Stand der Technik f\"{u}r ihre Zwecke dokumentieren, ihre Patente einbringen, und sich gegenseitig dabei helfen, diese Kampfmittel f\"{u}r sich und im Interesse der freien Software einzusetzen.

\item
Kontinuierlich auf \"{O}ffentlichkeit und Politiker einwirken, um die Patentinflation unter Kontrolle zu bringen und daf\"{u}r zu sorgen, dass keine Patente auf Softwareideen (abstrakt-logische Innovationen) mehr erteilt werden.
\end{itemize}
\end{sect}

\begin{sect}{etc}{Weitere Lekt\"{u}re}
(ahswpatmails 2001 5 64 abc)

Skeptischer Artikel in LinuxWeek (http://lwn.net/2001/0510/)
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% mode: latex ;
% End: ;
