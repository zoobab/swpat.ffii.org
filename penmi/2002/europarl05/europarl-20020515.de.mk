# -*- mode: makefile -*-

europarl-20020515.de.lstex:
	lstex europarl-20020515.de | sort -u > europarl-20020515.de.lstex
europarl-20020515.de.mk:	europarl-20020515.de.lstex
	vcat /ul/prg/RC/texmake > europarl-20020515.de.mk


europarl-20020515.de.dvi:	europarl-20020515.de.mk
	rm -f europarl-20020515.de.lta
	if latex europarl-20020515.de;then test -f europarl-20020515.de.lta && latex europarl-20020515.de;while tail -n 20 europarl-20020515.de.log | grep -w references && latex europarl-20020515.de;do eval;done;fi
	if test -r europarl-20020515.de.idx;then makeindex europarl-20020515.de && latex europarl-20020515.de;fi

europarl-20020515.de.pdf:	europarl-20020515.de.ps
	if grep -w '\(CJK\|epsfig\)' europarl-20020515.de.tex;then ps2pdf europarl-20020515.de.ps;else rm -f europarl-20020515.de.lta;if pdflatex europarl-20020515.de;then test -f europarl-20020515.de.lta && pdflatex europarl-20020515.de;while tail -n 20 europarl-20020515.de.log | grep -w references && pdflatex europarl-20020515.de;do eval;done;fi;fi
	if test -r europarl-20020515.de.idx;then makeindex europarl-20020515.de && pdflatex europarl-20020515.de;fi
europarl-20020515.de.tty:	europarl-20020515.de.dvi
	dvi2tty -q europarl-20020515.de > europarl-20020515.de.tty
europarl-20020515.de.ps:	europarl-20020515.de.dvi	
	dvips  europarl-20020515.de
europarl-20020515.de.001:	europarl-20020515.de.dvi
	rm -f europarl-20020515.de.[0-9][0-9][0-9]
	dvips -i -S 1  europarl-20020515.de
europarl-20020515.de.pbm:	europarl-20020515.de.ps
	pstopbm europarl-20020515.de.ps
europarl-20020515.de.gif:	europarl-20020515.de.ps
	pstogif europarl-20020515.de.ps
europarl-20020515.de.eps:	europarl-20020515.de.dvi
	dvips -E -f europarl-20020515.de > europarl-20020515.de.eps
europarl-20020515.de-01.g3n:	europarl-20020515.de.ps
	ps2fax n europarl-20020515.de.ps
europarl-20020515.de-01.g3f:	europarl-20020515.de.ps
	ps2fax f europarl-20020515.de.ps
europarl-20020515.de.ps.gz:	europarl-20020515.de.ps
	gzip < europarl-20020515.de.ps > europarl-20020515.de.ps.gz
europarl-20020515.de.ps.gz.uue:	europarl-20020515.de.ps.gz
	uuencode europarl-20020515.de.ps.gz europarl-20020515.de.ps.gz > europarl-20020515.de.ps.gz.uue
europarl-20020515.de.faxsnd:	europarl-20020515.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo europarl-20020515.de-??.g3n`;source faxsnd main
europarl-20020515.de_tex.ps:	
	cat europarl-20020515.de.tex | splitlong | coco | m2ps > europarl-20020515.de_tex.ps
europarl-20020515.de_tex.ps.gz:	europarl-20020515.de_tex.ps
	gzip < europarl-20020515.de_tex.ps > europarl-20020515.de_tex.ps.gz
europarl-20020515.de-01.pgm:
	cat europarl-20020515.de.ps | gs -q -sDEVICE=pgm -sOutputFile=europarl-20020515.de-%02d.pgm -
europarl-20020515.de/europarl-20020515.de.html:	europarl-20020515.de.dvi
	rm -fR europarl-20020515.de;latex2html europarl-20020515.de.tex
xview:	europarl-20020515.de.dvi
	xdvi -s 8 europarl-20020515.de &
tview:	europarl-20020515.de.tty
	browse europarl-20020515.de.tty 
gview:	europarl-20020515.de.ps
	ghostview  europarl-20020515.de.ps &
print:	europarl-20020515.de.ps
	lpr -s -h europarl-20020515.de.ps 
sprint:	europarl-20020515.de.001
	for F in europarl-20020515.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	europarl-20020515.de.faxsnd
	faxsndjob view europarl-20020515.de &
fsend:	europarl-20020515.de.faxsnd
	faxsndjob jobs europarl-20020515.de
viewgif:	europarl-20020515.de.gif
	xv europarl-20020515.de.gif &
viewpbm:	europarl-20020515.de.pbm
	xv europarl-20020515.de-??.pbm &
vieweps:	europarl-20020515.de.eps
	ghostview europarl-20020515.de.eps &	
clean:	europarl-20020515.de.ps
	rm -f  europarl-20020515.de-*.tex europarl-20020515.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} europarl-20020515.de-??.* europarl-20020515.de_tex.* europarl-20020515.de*~
tgz:	clean
	set +f;LSFILES=`cat europarl-20020515.de.ls???`;FILES=`ls europarl-20020515.de.* $$LSFILES | sort -u`;tar czvf europarl-20020515.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
