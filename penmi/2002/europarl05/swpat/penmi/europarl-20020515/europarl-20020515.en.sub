\begin{subdoc}{swpparl025}{2002-05-15 EuroParl hearing on the Directive Proposal}{http://swpat.ffii.org/conferences/europarl-20020515/index.en.html}{Workgroup\\swpatag@ffii.org}
\begin{abstract}
An hearing about the European Commission's software patentability directive proposal will be held at the European Parliament in Strasbourg on the afternoon of thursday 15-16:30.  Hartmut Pilch from FFII/Eurolinux is invited to speak for 20 minutes.  Among the other speakers is the chief directive editor from DG Markt.
\end{abstract}

\begin{sect}{invit}{Invitation Text}
\begin{center}
Cin\'{e}ma, Audiovisual policy and Intellectual property Intergroup

invites you to its meeting

 Should we patent ``computer-implemented inventions''?
\end{center}

\begin{center}
Wednesday 15 May 2002

from 17h00 to 18h30

Room S2.1

European Parliament in Strasbourg

Languages : EN/FR/DE/ES/IT/NL
\end{center}

``Programs for computers'' are not ``patentable inventions'' according to Article 52 of the European Patent Convention (EPC). However since 1978 around 30,000 patents for ``computer-implemented inventions'' have been granted by the European Patent Office and by national patent offices. Indeed, the jurisprudence of these offices considers programs which present a ``technical contribution'' to be patentable. This idea of a ``technical contribution'' is similar to that of the ``technical process'' used for the patent of genes in the controversial directive on biotechnological inventions. They are both just as ambiguous.

The proposed ``directive on the patentability of computer-implemented inventions'' intends to normalize the jurisprudence of the European Patent Office and the national courts. Computer technology is in a state of uncertainty making it difficult to differentiate between creation, public good and the market.

To patent computer advances, like medicines, the knowledge and the living, poses questions of access and transmission for everybody, and equality between persons and people.

\begin{description}
\item[With:]\ Hartmut PILCH, president of the Foundation for a Free Information Infrastructure (FFII) and speaker of the Eurolinux Alliance
Anthony HOWARD, administrator, DG MARKET, intellectual property service.
Gilles SAVARY, member of the European parliament, industry committee.
\item[Contact:]\ Genevi\`{e}ve Fraisse
\begin{description}
\item[phone:]\ 75 555-45 555
\item[Fax:]\ 79 555-49 555
\item[mail:]\ gfraisse europarl eu int
\end{description}
\end{description}
\end{sect}

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/conferences/europarl-20020515/thes/thes.en.sub}
\else
\dots
\fi

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/papers/eubsa-swpat0202/demands/demands.en.sub}
\else
\dots
\fi
\end{subdoc}

% Local Variables: ;
% coding: utf-8 ;
% mode: latex ;
% End: ;
