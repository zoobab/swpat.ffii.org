\begin{subdocument}{epue52exeg}{Auslegung von Art. 52 des Europ\"{a}ischen Patent\"{u}bereinkommens hinsichtlich der Frage, inwieweit Software patentierbar ist}{http://swpat.ffii.org/analyse/epue52/exeg/index.de.html}{Prof Dr iur Karl Friedrich Lenz\\dr_lenz@ziplip.co.jp}{Dr. Karl Friedrich Lenz, Professor f\"{u}r Deutsches Recht und Europarecht an der Universit\"{a}t Aoyama Gakuin in Tokio untersucht mit den allgemein anerkannten Methoden juristischer Auslegung, welche Bedeutung dem heute geltenden Text des Art 52 EP\"{U} beizumessen ist und gelangt zu dem Schluss, dass die Technischen Beschwerdekammern des EPA seit einiger Zeit regelm\"{a}{\ss}ig Patente auf Programme f\"{u}r Datenverarbeitungsprogrammen als solche erteilen und eine beunruhigende Bereitschaft zeigen, die eigenen Wertungen an die Stelle der Wertungen des Gesetzgebers zu setzen.}
\begin{sect}{vorb}{Vorbemerkung}
Der Autor hat bestimmte rechtspolitische Vorstellungen davon, ob Softwarepatente sinnvoll sind oder nicht. Um diese geht es hier aber nicht. Vielmehr soll allein mit den allgemein anerkannten Methoden juristischer Auslegung ermittelt werden, welche Bedeutung dem heute geltenden Text beizumessen ist.

Weiter wird auch die bisherige Rechtsprechung nur zum Schluss kurz diskutiert. Es geht hier vor allem darum, den Inhalt des Abkommens zu ermitteln, nicht Urteile darzustellen.
\end{sect}

\begin{sect}{wort}{Wortlaut der Regelung}
Der Wortlaut ist der Ausgangspunkt f\"{u}r jede Auslegung. Danach gilt (Absatz 2): Programme f\"{u}r Datenverarbeitungsanlagen werden nicht als Erfindungen angesehen. Dies beschr\"{a}nkt Absatz 3 dahingehend, dass Absatz 2 nur insoweit der Patentf\"{a}higkeit entgegensteht, als sich das Patent auf die genannten Gegenst\"{a}nde oder T\"{a}tigkeiten als solche bezieht.

Dieser Absatz 3 bedarf vor allem der Auslegung.

Zun\"{a}chst einmal: Absatz 3 sagt nicht direkt, dass nur Software als solche nicht patentierbar ist. Vielmehr sagt er, dass die genannten ``Gegenst\"{a}nde und T\"{a}tigkeiten'' so zu behandeln sind. Dazu geh\"{o}rt auch Software, aber es bleibt doch festzuhalten, dass es sich um eine pauschale Einschr\"{a}nkung f\"{u}r alle genannten F\"{a}lle handelt, die nicht allein f\"{u}r Software gedacht ist. In Absatz 2 sind insgesamt f\"{u}nfzehn Fallgruppen genannt. Es ist durchaus m\"{o}glich, dass die Worte ``als solche'' nicht in allen Fallgruppen die gleiche Reichweite haben.

Es sei hier auch eine kritische Bemerkung gegen\"{u}ber dem Verfasser von Art. 52 erlaubt. Die Beschr\"{a}nkung aller f\"{u}nfzehn durchaus unterschiedlichen Fallgruppen in Absatz 2 mit einer einheitlichen Formel ``als solche'' f\"{u}hrt nahezu zwangsl\"{a}ufig dazu, dass diese Formel f\"{u}r manche der Fallgruppen nicht besonders gut passt. Der Grund f\"{u}r die Schwierigkeit, den Begriff ``Software als solche'' zu verstehen, liegt m\"{o}glicherweise hier mit begr\"{u}ndet. Wenn der Verfasser des Textes f\"{u}r den Bereich Software eine hierauf zugeschnittene und nur hierf\"{u}r g\"{u}ltige Formulierung gefunden h\"{a}tte, w\"{a}re diese m\"{o}glicherweise sehr viel leichter zu verstehen. Die geltende Einheitseinschr\"{a}nkung f\"{u}r alle Fallgruppen dagegen hat weniger Aussichten, f\"{u}r alle Fallgruppen in gleicher Weise gut verst\"{a}ndlich und anwendbar zu sein.

Weiter ergibt sich aus dem Wortlaut ``steht nur insoweit entgegen'', dass Absatz 3 eine Beschr\"{a}nkung von Absatz 2 bedeutet, die teilweise sein muss. Damit ist jede Auslegung nicht vereinbar, die einseitig entweder zu \"{u}berhaupt keiner Beschr\"{a}nkung oder zu einem v\"{o}lligen Ausschluss einer der Fallgruppen in Absatz 2 f\"{u}hrt.

Nun zu der Frage, was unter Software als solcher zu verstehen ist, und vor allem was der Gegensatz zu diesem Begriff ist.

Zur Einstimmung auf diese Frage sei zun\"{a}chst untersucht, ob Programme f\"{u}r Datenverarbeitungsanlagen ``Gegenst\"{a}nde'' oder ``T\"{a}tigkeiten'' im Sinne von Absatz 3 sind.

Programme beruhen auf einer T\"{a}tigkeit, sind aber keine T\"{a}tigkeit als solche und daher ein Gegenstand.

Dieser Gegenstand kann in verschiedenen Formen vorliegen. Ein Programm wird zun\"{a}chst einmal in einer f\"{u}r Menschen verst\"{a}ndlichen Sprache geschrieben. Dieser urspr\"{u}ngliche Text wird dann in eine f\"{u}r den Computer ausf\"{u}hrbare Form \"{u}bersetzt. Beide Formen k\"{o}nnen auf einem Datentr\"{a}ger (zB einer CD) gespeichert sein und auf Papier ausgedruckt werden. Eine ausf\"{u}hrbare Form kann zus\"{a}tzlich noch eben das, n\"{a}mlich ausgef\"{u}hrt werden.

Es ist nicht erkennbar, dass nach allgemeinem Sprachgebrauch eine dieser verschiedenen Formen als ``Software als solche'' bezeichnet wird, w\"{a}hrend f\"{u}r andere Formen ein anderer Begriff gilt. Wer das Gegenteil behauptet, m\"{u}sste belegen, welcher andere Begriff das ist und welche der verschiedenen Formen des Gegenstandes Software damit gemeint ist.

Eine weitere denkbare M\"{o}glichkeit w\"{a}re nach dem Zusammenwirken von Software mit anderen Gegenst\"{a}nden zu unterscheiden. Danach w\"{a}re Software als solche der Bereich, in dem Software nicht oder nicht in gen\"{u}gendem Umfang mit anderen Gegenst\"{a}nden zusammenwirkt.

So w\"{a}re etwa die Theorie denkbar, Software als solche von Software zu unterscheiden, die auf einem Computer gerade l\"{a}uft. Denn in diesem Fall wirkt die Software bestimmungsgem\"{a}{\ss} mit dem Computer zusammen.

Diese Auffassung ist allerdings schwer mit dem Wortlaut von Absatz 3 zu vereinbaren. Da jede Software dazu bestimmt ist, auf einem Computer zu laufen, schlie{\ss}t das oben genannte Verst\"{a}ndnis \"{u}berhaupt nichts aus und f\"{u}hrt damit zu einem v\"{o}lligen Leerlauf der Ausnahme in Absatz 2. Wie immer die Grenze zu ziehen ist: eine v\"{o}llig einseitige Betrachtung steht im Widerspruch zu dem Wortlaut ``nur insoweit''.

Bleibt die M\"{o}glichkeit, eine \"{u}ber das blo{\ss}e Laufen auf dem Computer hinausgehende Wirkung auf andere Gegenst\"{a}nde f\"{u}r den patentierbaren Bereich zu verlangen. Wenn man dies will, stellt sich aber sofort die Frage, wie weit diese Wirkung gehen muss. Reicht schon die Wirkung auf ein an den Rechner angeschlossenes Ger\"{a}t, etwa einen Bildschirm? Aus dem Wortlaut l\"{a}sst sich f\"{u}r diese Frage nichts entnehmen. Daher halte ich es nicht f\"{u}r zwingend, ein Programm ``als solches'' in diesem Sinne zu verstehen.

Damit ergibt sich aus dem Wortlaut zun\"{a}chst keine sehr klares Bild. Dies ist als Zwischenergebnis der Wortlautauslegung festzuhalten. Fest steht nur, dass Absatz 3 f\"{u}r alle f\"{u}nfzehn Fallgruppen von Absatz 2 mit einer einheitlichen Formulierung gilt und dass die Auslegung nicht einseitig ausfallen darf.
\end{sect}

\begin{sect}{syst}{Systematische Auslegung}
Die systematische Auslegung versucht aus dem Zusammenhang, in dem eine bestimmte Formulierung gebraucht wird, sowie aus der gesamten Konzeption eines Gesetzes den Sinn dieser Formulierung zu ermitteln.

Ein erster Schritt mit dieser Methode ist es, die Bedeutung der Formel ``als solche'' f\"{u}r andere in Absatz 2 genannte Fallgruppen zu ermitteln.

Die erste in Absatz zwei genannte Fallgruppe sind Entdeckungen. Ausgeschlossen sind nur Entdeckungen als solche. Bedeutet dies,  dass Entdeckungen in die zwei Teilmengen Entdeckungen als solche und andere Entdeckungen aufzuteilen sind? Das halte ich nicht f\"{u}r sinnvoll. Vielmehr sind alle Entdeckungen nicht Gegenstand der Patentierbarkeit. F\"{u}r alle Entdeckungen gilt dies jedoch nur f\"{u}r die Entdeckung als solche. Nicht ausgeschlossen ist eine Erfindung, die zwar auf einer Entdeckung beruht, aber eben nicht die Entdeckung als solche ist, sondern diese nur benutzt.

Wenn dieses Verst\"{a}ndnis f\"{u}r die Fallgruppe der Entdeckung richtig ist, so w\"{u}rde dies f\"{u}r die Fallgruppe Software \"{u}bertragen bedeuten, dass Software nicht etwa in zwei Teilmengen Software als solche und andere Software aufzuteilen ist, sondern dass alle Formen von Software nicht patentierbar sind, und die Einschr\"{a}nkung nur bedeutet, dass andere Gegenst\"{a}nde patentierbar sind, selbst wenn zu ihrer Entwicklung Software verwendet wurde.

Die zweite in Absatz 2 genannte Fallgruppe sind wissenschaftliche Theorien. Auch hier halte ich es nicht f\"{u}r m\"{o}glich, eine bestimmte Teilmenge von ``wissenschaftlichen Theorien als solche'' zu bilden und dieser eine andere Teilmenge von patentierbaren wissenschaftlichen Theorien gegen\"{u}berzustellen. Dies gilt entsprechend auch f\"{u}r die dritte Fallgruppe (mathematische Methoden). F\"{u}r alle drei Fallgruppen in der Nummer eins von Absatz zwei ist die Einschr\"{a}nkung ``als solche'' relativ klar in dem Sinne zu verstehen, dass die Entdeckungen, wissenschaftlichen Theorien oder mathematischen Methoden als Mittel zum Zweck einer Erfindung in einem Patentantrag genannt sein k\"{o}nnen, dass aber niemand auch nur einen Teilbereich hieraus monopolisieren darf. Der Gegenbegriff zu ``als solche'' ist f\"{u}r diese drei Fallgruppen ``mit Hilfe des genannten Gegenstandes entstanden''. Anders als im Fall von Software ist der Begriff ``als solche'' in diesen Fallgruppen auch relativ klar verst\"{a}ndlich.

Die vierte Fallgruppe sind \"{a}sthetische Formsch\"{o}pfungen (Absatz 2 Buchstabe b). Hier ist schwer zu sehen, welchen Sinn die Einschr\"{a}nkung ``als solche'' haben kann. Jedenfalls l\"{a}sst sich auch f\"{u}r diese Fallgruppe nicht behaupten, dass einer bestimmten Teilmenge von \"{a}sthetischen Formsch\"{o}pfungen ``als solche'' eine ander Teilmenge patentierbarer \"{a}sthetischer Formsch\"{o}pfungen gegen\"{u}berzustellen sei.

Die n\"{a}chsten zehn Fallgruppen sind unter Buchstabe c) von Absatz zwei aufgez\"{a}hlt. Darunter f\"{a}llt auch Software, als letzte Fallgruppe.

Die erste unter diesen zehn Fallgruppen ist die der Pl\"{a}ne f\"{u}r gedankliche T\"{a}tigkeiten. Fallgruppen zwei und drei (Regeln und Verfahren f\"{u}r gedankliche T\"{a}tigkeiten) unterscheiden sich von der ersten Fallgruppe nicht wesentlich und k\"{o}nnen daher zusammen mit dieser auf einmal untersucht werden. Wie ist die Einschr\"{a}nkung ``als solche'' hier zu verstehen? Ebenso wie in den bisher untersuchten Fallgruppen verlangt die Einschr\"{a}nkung ``als solche'' auch hier nicht, gedankliche T\"{a}tigkeiten in zwei Teilmengen aufzuteilen, vielmehr sind alle gedanklichen T\"{a}tigkeiten nicht patentierbar.

Die n\"{a}chsten drei Fallgruppen sind Pl\"{a}ne, Regeln und Verfahren f\"{u}r Spiele. Davon ist die Gruppe der Regeln f\"{u}r Spiele am einfachsten zu verstehen. Alle Spiele haben Regeln. Wer eine neue Regel vorschl\"{a}gt oder gar ein v\"{o}llig neues Spiel entwickelt, kann daf\"{u}r keinen Patentschutz erreichen. Die Beschr\"{a}nkung auf Regeln ``als solche'' durch Absatz 3 ist bei dieser Fallgruppe besonders schwer zu verstehen. Jedenfalls l\"{a}sst sich keine klare Richtschnur f\"{u}r den Fall von Software ableiten. Das gilt erst recht f\"{u}r die Fallgruppen der ``Pl\"{a}ne f\"{u}r Spiele'' und der ``Verfahren f\"{u}r Spiele'', die bereits als solche unverst\"{a}ndliche Formulierungen sind.

Absatz zwei regelt noch weitere Fallgruppen. Ein erheblicher weiterer Gewinn an Erkenntnis ist aber von deren Untersuchung nicht zu erwarten. Daher halte ich jetzt als Ergebnis einer systematischen Auslegung fest: In wichtigen anderen Fallgruppen ist der betreffende Gegenstand nicht in zwei Teilmengen (``als solche'' und ``andere'') aufzuteilen. Vor allem in den in Buchstabe a) geregelten F\"{a}llen ist dieses Verst\"{a}ndnis des Begriffes ``als solche'' relativ klar. Dies ist daher auch f\"{u}r Software eine naheliegende Annahme.
\end{sect}

\begin{sect}{tele}{Teleologische Auslegung}
Die teleologische Auslegung fragt nach dem Zweck (griechisch telos) des Gesetzes. Dann wird unter mehreren Alternativen zur Auslegung diejenige ausgewaehlt, die diesen Zweck am ehesten zu verwirklichen hilft.

Voraussetzung daf\"{u}r ist, dass die Beschr\"{a}nkung des Ausschlusses der genannten Gegenst\"{a}nde und T\"{a}tigkeiten auf die Gegenst\"{a}nde und T\"{a}tigkeiten ``als solche'' einen direkt aus dem Gesetz klar erkennbaren Zweck verfolgt.

Absatz 3 betrifft f\"{u}nfzehn durchaus verschiedene Fallgruppen. Daher f\"{a}llt es schwer, einen klar erkennbaren Zweck nur f\"{u}r Absatz 3 festzustellen.

Daher bringt eine teleologische Auslegung von Absatz 3 keinen erkennbaren Ertrag.
\end{sect}

\begin{sect}{hist}{Historische Auslegung}
Die historische Auslegung ermittelt, was die an der Gesetzgebung beteiligten Personen sich gedacht haben, um den Sinn einer Formulierung zu erschliessen. Anders als die oben verwendeten Methoden arbeitet sie nicht allein mit dem Text des Gesetzes, sondern verwendet andere Texte (Entw\"{u}rfe und Diskussionsprotokolle). Diese Texte sind in einem neueren Buch von Beresford\footnote{Keith Beresford: Patenting Software under the European Patent Convention (http://swpat.ffii.org/papiere/beresford00/index.en.html), 12-20} gut erschlossen. Die folgende Untersuchung beruht auf dieser Darstellung.

Aus der Entstehungsgeschichte ergibt sich zun\"{a}chst, dass die universale Einschr\"{a}nkung ``als solche'' in Absatz drei sich in den fr\"{u}heren Entw\"{u}rfen nur auf die in Buchstabe a) genannten Gegenst\"{a}nde bezog (Entdeckungen, wissenschaftliche Theorien und mathematische Methoden). F\"{u}r die in Buchstabe c) genannten Gegenst\"{a}nde war die Einschr\"{a}nkung dagegen ``of a purely abstract nature'' (Entwurf 1965) bzw. ``of a purely intellectual nature'' (Entwurf 1969). Dies erkl\"{a}rt die Schwierigkeiten mit der Auslegung der gegenw\"{a}rtigen Formulierung. Es ist kein Wunder, dass die Einschr\"{a}nkung ``als solche'' f\"{u}r die Fallgruppen nicht besonders klar ist, f\"{u}r die sie urspr\"{u}nglich nicht gedacht war.

Ein sp\"{a}terer Entwurf (1971) enth\"{a}lt zum ersten Mal einen Ausschluss der Patentierbarkeit von Software. Dieser enth\"{a}lt keinerlei Einschr\"{a}nkung. Die jetzt in Buchstabe c) genannten anderen Gegenst\"{a}nde sind in diesem Entwurf wie folgt formuliert: ``schemes, rules or methods of doing business, performing purely mental acts or playing games''. Eine Einschr\"{a}nkung ist nur f\"{u}r die Fallgruppe der ``mental acts'' vorgesehen (``purely''). Dem Anwender dieses Textes bleibt z.B. die Frage erspart, was unter einem Verfahren f\"{u}r ein Spiel als solches zu verstehen ist.

Die Arbeitsgruppe hat den unbedingten Ausschluss der Patentierbarkeit von Software zun\"{a}chst akzeptiert. Dann wurde aber auch unter dem Eindruck von Stellungnahmen interessierter Kreise die gegenw\"{a}rtige Fassung beschlossen, die Software nur ``als solche'' von der Patentierbarkeit ausschlie{\ss}t.

Als Ergebnis der historischen Auslegung l\"{a}{\ss}t sich festhalten: Das Verst\"{a}ndnis des Begriffes ``als solche'' hat sich vor allem an den in Buchstabe a) genannten Fallgruppen zu orientieren, da der Begriff urspr\"{u}nglich f\"{u}r diese gedacht und daher in dem Zusammenhang am klarsten verst\"{a}ndlich ist. Daher erh\"{a}lt das oben entwickelte Ergebnis der systematischen Auslegung besonderes Gewicht: Software ist nicht etwa in zwei Teilmengen aufzuteilen (Software als solche und andere Software), sondern Software ist umfassend von der Patentierbarkeit ausgeschlossen. Dieser Ausschluss erstreckt sich jedoch nicht auf Erfindungen, die mit Hilfe von Software entwickelt sind, ebenso wie der Ausschluss f\"{u}r Entdeckungen und wissenschaftliche Theorien sich nicht auf Erfindungen erstreckt, die Entdeckungen oder wissenschaftliche Theorien anwenden.
\end{sect}

\begin{sect}{verf}{Verfassungskonforme Auslegung}
Die Methode der verfassungskonformen Auslegung fragt nach verfassungsrechtlichen Auswirkungen verschiedener Ergebnisse und w\"{a}hlt das Ergebnis, das mit den Wertungen der Verfassung am besten vereinbar ist. Dabei sind vor allem die Grundrechte zu beachten.

Das f\"{u}r die Praxis wichtigste Grundrecht in der deutschen Verfassung ist der Gleichheitssatz, Artikel 3 Absatz 1 Grundgesetz.

Der Gleichheitssatz verbietet, wesentlich Gleiches ohne sachlichen Grund ungleich zu behandeln.

Von den f\"{u}nfzehn Fallgruppen in Absatz 2 sind Software und \"{a}sthetische Formsch\"{o}pfungen insofern gleich, als sie bereits durch das Urheberrecht gesch\"{u}tzt sind. Dies ist f\"{u}r alle anderen Gegenst\"{a}nde oder T\"{a}tigkeiten nicht der Fall. Daher hat eine an Artikel 3 Absatz 1 des Grundgesetzes orientierte Auslegung vor allem darauf zu achten, dass die Behandlung von Software nicht ohne sachlichen Grund von der Behandlung \"{a}sthetischer Formsch\"{o}pfungen abweicht. Umgekehrt ist ein sachlicher Grund anzugeben, wenn man annehmen m\"{o}chte, dass Software anders als alle anderen Gegenst\"{a}nde von Erfindungen durch Patentrecht und Urheberrecht doppelt gesch\"{u}tzt werden muss. Da Patente f\"{u}r \"{a}sthetische Formsch\"{o}pfungen (z.B. Kriminalromane oder Filme) so gut wie nie erteilt werden, verlangt dieser Gesichtspunkt daher ein eng begrenztes Verst\"{a}ndnis f\"{u}r den Ausnahmebereich des Absatz 3 in Bezug auf Software.

Weiter ist f\"{u}r die verfassungskonforme Auslegung Art. 103 Abs. 2 des Grundgesetzes zu beachten. Dort ist angeordnet, dass eine Tat nur bestraft werden kann, wenn die Strafbarkeit gesetzlich bestimmt war, bevor die Tat begangen wurde. Dies verbietet die Ausweitung von strafrechtlichen Verboten durch die Rechtsprechung oder durch Gewohnheitsrecht. Das ist in diesem Zusammenhang relevant, weil ? 142 des deutschen Patentgesetzes Strafvorschriften f\"{u}r die Verletzung von Patenten vorsieht. Dies bedeutet, dass jede Erteilung eines Patentes die dort angeordneten strafrechtlichen Folgen nach sich ziehen kann. Diese Folgen sind jedoch vom Gesetzgeber nur insoweit angeordnet, als die mit Art. 52 des Europ\"{a}ischen Patent\"{u}bereinkommens identischen Ausschlusstatbest\"{a}nde in ? 1 Absatz 2 des deutschen Patentgesetzes nicht greifen. Eine mit dem Wortlaut dieser Ausschlusstatbest\"{a}nde nicht vereinbare Erteilung von Patenten verletzt damit das Gesetzlichkeitsprinzip. Daher ist in verfassungskonformer Auslegung dem Wortlaut des Gesetzes hier ein besonderer Stellenwert einzur\"{a}umen. Eine Auslegung, die diesen Wortlaut nach Belieben des Gerichts v\"{o}llig umbaut, ist nicht nur einfach gesetzwidrig, sondern gleichzeitig auch verfassungswidrig.

Schlie{\ss}lich spielt m\"{o}glicherweise Art. 14 des Grundgesetzes hier eine Rolle, der eine institutionelle Garantie des Eigentums enth\"{a}lt. Absatz 1 bestimmt, dass Eigentum gew\"{a}hrleistet wird, Inhalt und Schranken aber vom Gesetzgeber zu bestimmen sind. Damit w\"{a}re es vermutlich nicht ohne weiteres zu vereinbaren, das Patentwesen insgesamt r\"{u}ckwirkend abzuschaffen\footnote{Vgl. auch die Entscheidungen des Bundesverfassungsgerichts in Band 36 der amtlichen Sammlung, Seite 281 und Band 31, Seite 229.} (im Gegensatz zu einer Abschaffung, die ab einem bestimmten Stichtag keine neuen Patentantr\"{a}ge mehr zul\"{a}sst). Bei der Frage der Auslegung von Art. 52 des Europ\"{a}ischen Patent\"{u}bereinkommens in Bezug auf die Patentierbarkeit von Software geht es jedoch nicht um einen derart radikalen Schnitt. Wenn die Arbeitsgruppe entsprechend dem Entwurf 1971 Software ohne Einschr\"{a}nkung von der Patentierbarkeit ausgeschlossen h\"{a}tte, w\"{a}re sie daran wohl kaum durch Art. 14 des Grundgesetzes gehindert gewesen. Und wenn heute die Auslegung zu dem Ergebnis kommt, dass Software in weiten Bereichen vom Patentschutz ausgenommen ist, bedeutet das noch keinen verfassungsrechtlich unzul\"{a}ssigen Eingriff in das Eigentum von Erfindern. Das gilt um so mehr, als Software ja bereits durch das Urheberrecht gesch\"{u}tzt ist, also ein Ausschluss von der Patentierbarkeit keineswegs einen Ausschluss jeglicher wirtschaftlichen Verwertbarkeit bedeutet.
\end{sect}

\begin{sect}{zwer}{Zwischenergebnis}
Bereits die Wortlautauslegung schlie{\ss}t ein Verst\"{a}ndnis aus, nach dem f\"{u}r den Ausschluss von Software von der Patentierbarkeit kein Anwendungsbereich verbleibt, da dies mit der Formulierung ``nur insoweit'' nicht vereinbar ist.

Die systematische Auslegung ergibt, besonders gest\"{u}tzt auch durch die historische Auslegung, dass Software nicht etwa in zwei Teilmengen (Software ``als solche'' und andere Software) aufzuteilen ist, sondern dass wie im Fall von Entdeckungen und wissenschaftlichen Theorien alle Software von der Patentierbarkeit ausgeschlossen ist, wohl aber mit Hilfe von Software entwickelte Erfindungen patentierbar sind.

Die verfassungskonforme Auslegung verbietet eine Ungleichbehandlung mit \"{a}sthetischen Formsch\"{o}pfungen ohne sachlichen Grund und eine Ausweitung der Patentierbarkeit ohne R\"{u}cksicht auf die oben entwickelten Grenzen des Wortlautes.
\end{sect}

\begin{sect}{epra}{Diskussion der Praxis des Europ\"{a}ischen Patentamtes}
Die Praxis des Europ\"{a}ischen Patentamtes orientiert sich derzeit an der Entscheidung der Technischen Beschwerdekammer 3.5.1 vom 1. Juli 1998, Stichwort Computerprogrammprodukt/IBM\footnote{Entscheidung der Technischen Beschwerdekammer des EPA mit Aktenzeichen T 1173/97-3.5.1 (http://swpat.ffii.org/papiere/epo-t971173/index.de.html)}.

Auf Seite 12 des Umdruckes meint die Technische Beschwerdekammer, dass Computerprogramme in die zwei Teilmengen Software als solche und andere Software aufzuteilen sei. Diese Auffassung ist mit dem oben gefundenen Ergebnis einer systematischen Auslegung nicht vereinbar. Sie wird auch nicht n\"{a}her begr\"{u}ndet, sondern nur aus einer oberfl\"{a}chlichen Gesamtbetrachtung der Abs\"{a}tze 2 und 3 abgeleitet. Eine den obigen Ausf\"{u}hrungen entsprechende Anwendung der allgemein anerkannten Methoden juristischer Auslegung findet sich in dieser Entscheidung nicht.

Auf der folgenden Seite behauptet die Kammer dann, die Einschr\"{a}nkung ``als solche'' sei dahingehend zu verstehen, dass Computerprogramme als solche nur Computerprogramme ohne technischen Charakter seien.

Dies ist so weit vom Wortlaut entfernt, dass eine Bestrafung aufgrund eines infolge dieser gesetzwidrigen Auslegung erteilten Patentes mit dem Gesetzlichkeitsprinzip (Art. 103 Absatz 2 des Grundgesetzes) in Widerspruch steht. Es ist eine v\"{o}llige Neuformulierung der Schranke in Absatz 3, die mit dem Wortlaut des Gesetzes nichts mehr gemein hat. Die technische Beschwerdekammer \"{u}berschreitet damit klar die Grenzen richterlicher T\"{a}tigkeit. Wer die Formulierung ``als solche'' durch die Formulierung ``ohne technischen Charakter'' ersetzen m\"{o}chte, muss dies durch eine entsprechende \"{A}nderung des Vertragstextes nach den daf\"{u}r erforderlichen Verfahren bewirken. Die Rechtsprechung kann dies nicht.

Die Regierungskonferenz im Herbst 2000 hat beschlossen, in Absatz 1 von Art. 52 das Erfordernis eines technischen Charakters aufzunehmen. Absatz 1 soll in Zukunft lauten: ``European patents shall be granted for any inventions, in all fields of technology, provided that they are new, involve an inventive step and are susceptible of industrial application.''\footnote{http://www.epo.co.at/epo/dipl\_conf/pdf/em00003a.pdf} Das Erfordernis ``in all fields of technology'' wurde nicht etwa in Absatz 3 neu eingef\"{u}gt und die bisherige Einschr\"{a}nkung ``as such'' aufgegeben, wie dies die Rechtsprechung in \"{U}berschreitung ihrer Kompetenzen f\"{u}r die Fallgruppe Software getan hat. Nach dem neuen Text kann der technische Charakter nur dann noch als entscheidendes Kriterium angesehen werden, wenn man der Ansicht ist, dass diese Pr\"{u}fung in Absatz 1 und Absatz 3 doppelt vorgenommen werden soll, was systematisch wenig sinnvoll w\"{a}re.

Auch eine kurze systematische Betrachtung zeigt, dass die Auffassung der technischen Beschwerdekammer schon im Ausgangspunkt fragw\"{u}rdig ist. Sie m\"{u}sste n\"{a}mlich auch den Bereich der Entdeckungen und der wissenschaftlichen Theorien in die Teilmengen ``mit technischem Charakter'' und ``ohne technischen Charakter'' aufteilen und einen Teil der Wissenschaft damit der Monopolisierung durch Patente zug\"{a}nglich machen.

Die weiteren \"{U}berlegungen der Beschwerdekammer betreffen die Frage, wie nach dem von der Kammer im Gegensatz zum Wortlaut und zur Systematik entwickelten Kriterium ``technischer Charakter'' der Bereich der Software in zwei Teilmengen aufzuteilen sei. Diese \"{U}berlegungen f\"{u}hren im Ergebnis zu einer v\"{o}llig unbeschr\"{a}nkten Anerkennung der Patentierbarkeit von Software. Die Teilmenge ``Software als solche'' ist in der Konzeption der technischen Beschwerdekammer von verschwindend geringer Bedeutung, was wiederum mit der Formulierung ``nur insoweit'' in Absatz 3 unvereinbar ist.

Aufschlussreich ist schlie{\ss}lich der Versuch einer teleologischen Begr\"{u}ndung, den die technische Beschwerdekammer auf Seite 21 unternimmt. Dort f\"{u}hrt die technische Beschwerdekammer aus: ``Ziel und Zweck des EP\"{U} ist es ja, Patente f\"{u}r Erfindungen zu erteilen und durch einen angemessenen Schutz dieser Erfindungen den technischen Fortschritt zu f\"{o}rdern. Vor diesem Hintergrund ist die Kammer im Lichte der Entwicklung in der Informationstechnik zu ihrer vorstehenden Auslegung gelangt, zumal die Informationstechnik zunehmend fast alle Bereiche der Gesellschaft durchdringt und \"{u}beraus wertvolle Erfindungen liefert.''

Dies steht im Gegensatz zu der Fehlanzeige oben zum Punkt teleologische Auslegung. Es geht hier um die Auslegung von Absatz 3 des Europ\"{a}ischen Patent\"{u}bereinkommens. Dessen Zweck ist, wie oben bereits festgestellt, im Hinblick auf die Zahl und die Unterschiedlichkeit der Ausschlusstatbest\"{a}nde nicht objektiv und klar ersichtlich. Die Behauptung, die Beschr\"{a}nkung des Ausschlusses von Software von der Patentierbarkeit auf Software als solche in Absatz 3 habe den Zweck, im Lichte der Entwicklung der Informationstechnik durch Anerkennung der Patentierbarkeit den technischen Fortschritt zu f\"{o}rdern, \"{u}berzeugt nicht. Falls der Gesetzgeber einen solchen Zweck verfolgt haben sollte, h\"{a}tte er den Ausschluss in Absatz 2 von vornherein nicht vorgesehen. Die Unterstellung eines mit dem gew\"{u}nschten Ergebnis \"{u}bereinstimmenden Gesetzeszweckes ist zwar keine korrekte Anwendung der teleologischen Auslegungsmethode, zeigt aber deutlich die Bereitschaft der technischen Beschwerdekammer, die eigenen Wertungen an die Stelle der Wertung des Gesetzgebers zu setzen.
\end{sect}

\begin{sect}{etc}{Weitere Lekt\"{u}re (Empfehlungen des Herausgebers)}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Originalfassung (PDF) (epue52exeg.pdf)}}

\begin{quote}
Diese PDF-Datei wurde direkt aus der von Prof. Lenz im Netz ver\"{o}ffentlichten MSWord-Vorlage erzeugt.
\end{quote}
\filbreak

\item
{\bf {\bf Stellungnahme von Prof. Dr. iur. Karl-Friedrich Lenz\footnote{http://swpat.ffii.org/treffen/bundestag-2001/lenzkf/index.de.html}}}

\begin{quote}
Wer entgegen klaren tats\"{a}chlichen Befunden und einer \_sehr\_ ablehnenden Haltung einer deutlichen Mehrheit von Softwareentwicklern den Markt f\"{u}r Patentdienstleistungen und Prozessf\"{u}hrung in Patentsachen mit aller Gewalt auf neue Gebiete ausweiten will, muss sich auch auf das genaue Gegenteil gefasst machen. N\"{a}mlich die Abschaffung des Patentwesens insgesamt.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{http://swpat.ffii.org/analyse/erfindung/index.de.html}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% mode: latex ;
% End: ;
