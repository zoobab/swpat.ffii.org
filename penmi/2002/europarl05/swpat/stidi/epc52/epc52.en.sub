\begin{subdocument}{epue52}{Interpretation of art 52 of the European Patent Convention in view of the question, to what extent software is patentable}{http://swpat.ffii.org/analysis/epc52/index.en.html}{Workgroup\\swpatag@ffii.org}{The limits of what is patentable which were laid down in the European Patent Convention of 1973 have been blurred over the years.  Leading patent courts have interpreted Art 52 in a way that renders it almost meaningless in practise.  Numerous law scholars have shown why this is not permissible. The EPO has proposed to revise Art 52 so as to bring it in line with its practise of unlimited patentability.  One could however also do the opposite: regulate patentability along the lines of the original Art 52 but in a way that leaves fewer possibilities of abuse.  This documentation explores what has happened and what can be done.}
\begin{sect}{old}{Current Version of Art 52}
The EPC Revision Conference of 2000/11 proposed to insert the complement ``in all fields of technology'' to paragraph 1 and to delete paragraph 4.

\begin{quote}
Article 52: Patentable Inventions

\begin{enumerate}
\item
European patents shall be granted for inventions [ in all fields of technology ], as far as they are new, involve an inventive step and are susceptible of industrial application.

\item
The following in particular shall not be regarded as inventions within the meaning of paragraph 1:
\begin{enumerate}
\item
discoveries,  scientific theories and mathematical methods;

\item
aesthetic creations;

\item
schemes,  rules and methods for performing mental acts, playing games or doing business, and programs for computers;

\item
presentations of information.
\end{enumerate}

\item
The provisions of paragraph 2 shall exclude patentability of the subject-matter or activities referred to in that provision only to the extent to which a European patent application or European patent relates to such subject-matter or activities as such.

\item
Methods for treatment of the human or animal body by surgery or therapy and diagnostic methods practised on the human or animal body shall not be regarded as inventions which are susceptible of industrial application within the meaning of paragraph 1. This provision shall not apply to products,  in particular substances or compositions,  for use in any of these methods.
\end{enumerate}
\end{quote}
\end{sect}

\begin{sect}{bas}{Guiding Thoughts for our Revision Proposal}
The current legal regulation about the limits of patentability is clear and unambiguous.  There are however lawcourts which consider this regulation inadequate and have replaced it by a different regulation in anticipation of a change of law.

After an intensive public debate it has turned out that the current legal rule is adequate and that recent EPO caselaw is at odds with both the law and the public interest.  The courts are called upon to correct their practise and apply the law.

How exactly the law is to be understood and applied is explained briefly in a regulation proposal.

This rule is basically already implied in the European Patent Convention (EPC) and has been well understood during the early years of legal practise.  However in view of the misunderstandings and conflicts that have arisen since 1986, there seem to be good reasons to write it more explicitly into the law now.

For this we have prepared the following sharpening of art 52, enriched by definitions taken mainly from German patent law caselaw and commentaries of the 80s.  This approach would entail a sharpening of further articles, especially 56 (inventive step) and 57 (industrial applicability).

We optionally propose to remove 52(3) because it is purely explanatory and adds no meaning to the article.  It belongs in the examination guidelines.  Deleting it from the law would be a convenient way of telling lawcourts to return to the correct interpretation of the law, which was predominant during the 1970/80s.
\end{sect}

\begin{sect}{nov}{Article 52: Patentable Inventions: Sharpened Version}
\begin{quote}
\begin{enumerate}
\item
European Patents are granted for \emph{technical inventions} (i.e. teachings of plan-conformant action using controllable physical forces to directly achieve a causally overseeable success), provided the teaching is new, non-obvious (i.e. requiring unusual experimental activity), and susceptible of industrial application (i.e. destined for direct use in the industrial production of material goods).

\item
The following in particular are not regarded as technical inventions:
...[unchanged]

\item
[unchanged or deleted]

\item
[moved to Art 57]
\end{enumerate}
\end{quote}
\end{sect}

\begin{sect}{epa}{New Version of Art 52 according to the EPO's Base Proposal}
At the Diplomatic Conference in november 2000, the EPO wants to remove all traces of restricting definitions on terms such as \emph{invention}, \emph{technicity}, \emph{industrial applicability} etc from the Law and instead open the way for patentability of all practical and repeatable problem solutions.  This has allowed the EPO to formulate a very short proposal.

\begin{quote}
Article 52: Patentable Inventions

European patents shall be granted for inventions in all fields of technology, as far as they are new, involve an inventive step and are susceptible of industrial application.
\end{quote}
\end{sect}

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/analysis/epc52/exeg/exeg.de.sub}
\else
\dots
\fi

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/analysis/epc52/moses/moses.en.sub}
\else
\dots
\fi

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/analysis/epc52/stalin/stalin.en.sub}
\else
\dots
\fi

\ifsubinputs
\subinputstrue\input{/ul/sig/srv/res/www/ffii/swpat/analysis/epc52/udgor/udgor.en.sub}
\else
\dots
\fi

\begin{sect}{etc}{Annotated Links}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Interpretation of art 52 of the European Patent Convention in view of the question, to what extent software is patentable\footnote{http://swpat.ffii.org/analysis/epc52/exeg/index.de.html}}}

\begin{quote}
Dr. Karl Friedrich Lenz, professor for German and European Law at Aoyama Gakuin University in Tokyo, investigates using the various universally accepted methods of law interpretation which meaning has to be attributed to the text of art 52 EPC today and reaches the conclusion that the Technical Boards of Appeal of the European Patent Office have for some time now regularly granted patents on programs for computers as such and are showing a disturbing willingness to substitute their own value judgements for those given by the legislator.
\end{quote}
\filbreak

\item
{\bf {\bf Moses, the Ten Exclusions from Patentability and ``stealing with a further righteous effect''\footnote{http://swpat.ffii.org/analysis/epc52/moses/index.en.html}}}

\begin{quote}
Computer programs are both unpatentable and patentable in Europe.  How did the European Patent Office's Technical Boards of Appeal gradually manage to patent the unpatentable?  In complicated matters, satiric comparison is often the fastest way to a thourough understanding.
\end{quote}
\filbreak

\item
{\bf {\bf Stalin \& Software Patents\footnote{http://swpat.ffii.org/analysis/epc52/stalin/index.en.html}}}

\begin{quote}
In this contribution to the European Patent Office (EPO) mailing list, a European patent attorney cites the EPO Examination Guidelines of 1978 as clear documentary evidence for the intention of the legislator to keep computer programs on any storage medium free from any claims to the effect that their distribution, sale or use could infringe on a patent.  But the European Patent Office's Technical Board of Appeal (TBA) apparently considered itself to be a kind of modern Stalin, an ultimate sources of wisdom in matters of whatever complexity, standing high above the legislator and the peoples of Europe, and even above the EPO's own institutions for judicial review.  In this way the TBA risks to antagonise the public, to create harmful legal insecurity especially for small patent-holders and to severely damage the delicate process of building confidence in international institutions.  The TBA should see itself as a conservator rather than an innovator.
\end{quote}
\filbreak

\item
{\bf {\bf Scandinavia: even without the ``as such'' clause, stealing can have a further legal effect\footnote{http://swpat.ffii.org/analysis/epc52/udgor/index.en.html}}}

\begin{quote}
Art 52 of the European Patent Convention (EPC) stipulates that programs for computers as well as mental rules, mathematical methods, ways of presenting information etc are not patentable inventions and may therefore not be claimed as such.  The wording ``as such'' from Art 52(3) has however been used to undo all explicit limits on patentability.  The European Patent Office (EPO) has in 1997 begun to subdivide computer programs into two groups, ``as-such programs'' and ``not-as-such programs'', and has tried to justify this by historical claims about how art 52 EPC came about.  This reasoning is at odds with grammar as well as with history.  One particularly nice set of evidence comes from Scandinavia:  the Danish and Swedish national versions of art 52 EPC do not literally render Art 52(3) but rather incorporate its meaning in the first line of their version of Art 52(2), coming to exactly the same common sense conclusions to which independent grammatical analyses have come and which are also stated in the early EPO examination guidelines: that a ``program as such'' is ``something that constitutes only a program''.   It confirms that Art 52(3) merely exhorts examiners to look carefully where the novel achievement really lies, e.g. in a programming solution or in a chemical process which may happen to run under program control.
\end{quote}
\filbreak

\item
{\bf {\bf Regulation about the invention concept of the European patent system and its interpretation with special regard to programs for computers\footnote{http://swpat.ffii.org/analysis/directive/index.en.html}}}

\begin{quote}
We propose that the legislator draft any regulations on the question of software patentability along the lines of the following short and clear text.
\end{quote}
\filbreak

\item
{\bf {\bf Interpretation of art 52 of the European Patent Convention in view of the question, to what extent software is patentable\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf K\"{o}nig 2001: Patentf\"{a}hige Datenverarbeitungsprogramme - ein Widerspruch in sich\footnote{http://swpat.ffii.org/papers/grur-koenig01/index.de.html}}}

\begin{quote}
Dr. K\"{o}nig, patent attorney from D\"{u}sseldorf, points out inconsistencies in the software patent caselaw of the EPO and BGH, criticises ``circular conclusions'' and argues that the EPO has ``done violence to art 52 EPC''.  Through a ``grammatical interpretation'' of ``programs for computers as such'' he finds that this can refer only to all kinds of computer programs without exception, as far as they are claimed alone.   The EPC of 1973, transcribed into German law in 1978, no longer allows a distinction between technical and untechnical programs.  However it ist still possible to patent program-related combination inventions, which then have to be examined for technicity, novelty, non-obviousness and industrial applicability.  Courts have often shown more imagination in ``helping themselves over the obstacles of art 52''.  There is an elegant indirect way to effectively grant full patent protection for computer programs as such while avoiding the recent incoherences of the EPO and BGH jurisdiction.  As parts of combination inventions, programs for computers may, just as is frequently the case with discoveries and scientific theories, enjoy full ``usage protection'', if their distribution can be construed as contributory infringement of a combination invention. 
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% mode: latex ;
% End: ;
