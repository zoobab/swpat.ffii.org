\begin{subdocument}{swpatdamba}{Vorbeugung gegen kommende Patenklagen}{http://swpat.ffii.org/analyse/damba/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{Unabh\"{a}ngig davon, ob es uns diesmal gelingt, die Patentlobby in ihre Schranken zu weisen, m\"{u}ssen wir uns langfristig mit dem Patentwesen auseinandersetzen.}
\begin{sect}{neu}{Neuheit widerlegen}
Wir haben bereits begonnen, den Stand der Technik bei allen auf FFII.ORG beherbergten Projekten t\"{a}glich mit Zeitstempel zu dokumentieren.  Diese Dokumentation wird ein mal im Monat in einer \"{o}ffentlichen Bibliothek archiviert.  Das System wird derzeit probeweise auch auf andere Server ausgeweitet.

Gegen frivole Patentklagen hilft letztlich nur der Nachweis, dass ``diese Technik schon dato 19xx-xx-xx bekannt und in Entgegenhaltung Dx dokumentiert'' war.  Nur durch Widerlegung der Neuheit kann man ein Patent zu Fall bringen.  Andere Kriterien wie ``Erfindungsh\"{o}he'' sind inzwischen durch die Praxis der Patent\"{a}mter zur Makulatur geworden.
\end{sect}

\begin{sect}{bund}{Branchenvereinbarungen, Schutzb\"{u}ndnisse, Versicherungen}
Die Softwarebranche ist nicht gezwungen, sich unsinnigen b\"{u}rokratischen Praktiken zu unterwerfen.  Mit einigem gutem Willen kann sie sich verb\"{u}nden, um ihre eigenen ``Abkommen f\"{u}r den fairen Gebrauch von Patenten'' zu schlie{\ss}en und dabei einen ertr\"{a}glichen Ausgleich zwischen den Interessen der Patentinhaber und denen der informationellen Infrastruktur schaffen und durchzusetzen.

Ferner w\"{a}re es m\"{o}glich, im Rahmen solcher B\"{u}ndnisse eine relativ g\"{u}nstige Rechtsschutzversicherung f\"{u}r Programmierer freier Software aufzubauen.  Der Schutz vor frivolen Patentattacken w\"{u}rde dann professionalisiert.  Wir w\"{u}rden zum Gegenangriff \"{u}bergehen und daf\"{u}r sorgen, dass sich Streithansel zwei mal \"{u}berlegen, ob sie ihr Verm\"{o}gen und ihren Ruf riskieren m\"{o}chten.

Das sind Vorstellungen, die bisher an der Unwissenheit und mangelnden Solidarit\"{a}t der Softwarebranche scheitern.  Sie sollten parallel, nicht alternativ zum Kampf gegen die missbr\"{a}uchliche Ausweitung des Patentrechts umgesetzt werden.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% mode: latex ;
% End: ;
