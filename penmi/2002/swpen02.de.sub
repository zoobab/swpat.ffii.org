\begin{subdocument}{swpen02}{Logikpatentierung und Konferenzen 2002}{http://swpat.ffii.org/termine/2002/index.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Die Softwarepatent-Arbeitsgruppe des FFII nimmt h\"{a}ufig an \"{o}ffentlichen Veranstaltungen und Messen teil.  U.a. haben wir auf der Systems und auf dem Linuxtag je einen Stand betrieben und sind auf einigen Anh\"{o}rungen von Parlamenten, Regierungen und Parteien aufgetreten.  Wir versuchen, diese Aktivit\"{a}ten hier zu dokumentieren.}
\begin{sect}{penmi}{Inhalt}
\begin{itemize}
\item
{\bf {\bf Mediatage Kiel 2002-11-21}\footnote{http://swpat.ffii.org/termine/2002/kiel11/index.de.html}}

\begin{quote}
Eine Diskussionsveranstaltung auf dem Multimedia-Campus Kiel \"{u}ber die anstehenden Gesetzgebungspl\"{a}ne zur Logikpatentierung, veranstaltet vom Technologietransferzentrum Schleswig-Holstein (TTZSH.de).  Andr\'{e} Rebentisch hielt f\"{u}r den FFII einen Vortrag.  Einen weiteren Vortrag hielt Patentanwalt Dr. Dibbert, Syndikusanwalt der M+N Informatik AG.   Es bestand weitgehende Einigkeit dar\"{u}ber, dass Patente den Fortschritt im Bereich der Software eher behindern als f\"{o}rdern und dass der Richtlinienentwurf der Europ\"{a}ischen Komission mehr Fragen aufwirft als beantwortet.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl Hearings 2002-11-07 and 26}\footnote{http://swpat.ffii.org/termine/2002/europarl11/index.en.html}}

\begin{quote}
On Nov 7, the European Parliament's Legal Affairs Committee (JURI) conducted a hearing about the proposed software patentability directive.  10 invited experts were given 5-10 minutes of time to respond to a series of questions which imply that software ideas must be patentable inventions and that opposition to this line can be dismissed as minoritarian.  The bias of this hearing was, as it turned out, the fruit of intense lobbying by industrial patent lawyers.  In spite of the initial bias however, most MEPs did begin to question what they had been taught.  They more or less noticed that the directive proposal hands over a carte blanche to patent courts in a matter where basic freedoms of citizens, including many of the most productive software businesses of Europe, are at stake.  Another hearing on 2002-11-26 helped Europe's legislature gain confidence needed to set clear and adequate rules for the patent jurisdiction to follow.
\end{quote}
\filbreak

\item
{\bf {\bf Swpat Conference Amsterdam 2002-08-30..1 (Columbanus Symposium)}\footnote{http://swpat.ffii.org/termine/2002/ivir08/index.en.html}}

\begin{quote}
Hartmut Pilch is attending a conference hosted by Prof. Bernt Hugenholz and Reinier Bakels from University of Amsterdam about the typology of innovations in the software and business method area and the implications of various rules for defining what is patentable, including the European Commission's recent proposal for a directive and hopefully also our widely supported counter-proposal.
\end{quote}
\filbreak

\item
{\bf {\bf UK Patent Orifice: Software Patentability Rally Brussels 2002-06-19}\footnote{http://swpat.ffii.org/termine/2002/ukpo06/index.de.html}}

\begin{quote}
The UK Patent Office, wearing the hat of the British Government, has entrusted one of its subcontractors to organise a high profile rally in support of the proposed CEC/BSA software patentability directive.  Speeches will be held by hard core patent movement activists from the European Commission, the UK Government, the European Patent Office, the IBM patent department and some well-known patent law firms.  At the end of an intensive 6 hour long software patentablity propaganda firework a podium discussion will be held in which an ``open source representative'' will be allowed to sit at the table.  The organisers' initial choice was Alan Cox.  Alan has occasionally expressed deep concern about the software patent problem but, as a core developper of the Linux kernel and other key projects, he never really had the time to study the legal intricacies.  This fact alongside with his long hair and beard seemed to make him particularly qualified for the role of the social romantic dissident, generously tolerated by a group of patent lawyers posing as the businessmen of the real software industry.  Alan immediately understood the game and declined the invitation, handing the case over to the Eurolinux Alliance.  We have meanwhile been put on the conference program, but our abstract was doctored, our criticism suppressed, our message distorted, so that now we nevertheless fit into the design of this rally.
\end{quote}
\filbreak

\item
{\bf {\bf Information Economy and Swpat Conference Paris 20020610-1}\footnote{http://swpat.ffii.org/termine/2002/ifri06/index.en.html}}

\begin{quote}
Institut Fran\c{c}ais des Relations Internationales (IFRI.org) and Center of Information Policy Research at Mariland University (CIP.umd.org) are organising a transatlantic conference on information economy and in particular on the limits of patentability as well as the problems in neighboring areas such as database exclusion rights and copyright.  Hartmut Pilch is participating on behalf of FFII and Eurolinux on two of the panels.
\end{quote}
\filbreak

\item
{\bf {\bf FFII @ Linuxtag 2002}\footnote{http://swpat.ffii.org/termine/2002/linuxtag/index.de.html}}

\begin{quote}
Der FFII stellt auf dem Linuxtag 2002 Schilder, Plakate, Informationshefte, Stifte und sonstige Unterlagen insbesondere zum Thema Softwarepatente aus.  Einige unserer Mitglieder werden zum Gespr\"{a}ch bereit stehen.
\end{quote}
\filbreak

\item
{\bf {\bf 2002-05-15 EuroParl hearing on the Directive Proposal}\footnote{http://swpat.ffii.org/termine/2002/swpparl025/index.de.html}}

\begin{quote}
An hearing about the European Commission's software patentability directive proposal will be held at the European Parliament in Strasbourg on the afternoon of thursday 15-16:30.  Hartmut Pilch from FFII/Eurolinux is invited to speak for 20 minutes.  Among the other speakers is the chief directive editor from DG Markt.
\end{quote}
\filbreak

\item
{\bf {\bf ES Patent Orifice: Software Patentability Rally Madrid 2002-05-06}\footnote{http://swpat.ffii.org/termine/2002/espo05/index.en.html}}

\begin{quote}
The Spanish Patent and Trademark Office (ES-PTO/OEPM), the European Patent Office (EPO), the Industrial Property Unit of the European Commission and together with patent lawyers from UNICE (employers' federation) and other European Patent Movement strongholds are, under the auspices of the Spanish EU presidency and the Spanish Ministry for Economy and Technology, holding a two-day seminar in the Spanish Patent Office in Madrid.   The seminar's agenda and invitation presupposes that there is an ``evergrowing need'' for ``protecting'' software innovation and channels the discussion into questions of how companies should go ahead doing this.  In spite of the existence of a strong movement against software patents in Spain, the seminar keeps silent on the controversy and does not offer critical voices any space for exposing their views.
\end{quote}
\filbreak

\item
{\bf {\bf EPC Revision Conference 2002-06}\footnote{http://swpat.ffii.org/termine/2002/epue06/index.de.html}}

\begin{quote}
The ``second basket'' of the planned revision of the European Patent Convention did not change Art 52 or other articles of Substantive Patent Law.  A long and unpenetrable document was produced which proposes some changes in procedural law which we have yet to study.
\end{quote}
\filbreak

\item
{\bf {\bf GUUG Fr\"{u}hjahrsfachgespr\"{a}ch Bochum 2002-03-01 14:45-16:30}\footnote{http://swpat.ffii.org/termine/2002/guugffg/index.de.html}}

\begin{quote}
A symposium on software property (copyright, patents and alternatives), aiming to find out, largely independently from questions of legal systematics, what are the needs of computer scientists and programmers and what kind of regulation would enhance their productivity.  2 Proponents and 2 critics of software patentability from within the software community (no patent lawyers!) will clash and try to clear their differences and find out whether they can find any common ground for the benefit of the whole community.
\end{quote}
\filbreak

\item
{\bf {\bf Conference on Information Policy Research 2002-01-28 Arlington USA}\footnote{http://swpat.ffii.org/termine/2002/arli01/index.en.html}}

\begin{quote}
Hartmut Pilch from FFII will speak about software patentability issues at this conference.  Other contributions on this subject are expected from Philippe Aigrain (European Commission), Brian Kahin (Maryland University, NSF) and others.  Hopefully the conference will spark off some long-needed research.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf FFII Termine\footnote{http://offen.ffii.org/ffii/termine/}}}

\begin{quote}
Weitere Veranstaltungen werden derzeit auf einem kollaborativen Webportal des FFII geplant
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

