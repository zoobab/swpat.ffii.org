\begin{subdocument}{swpukpo026}{UK Patent Orifice: Software Patentability Rally Brussels 2002-06-19}{http://swpat.ffii.org/termine/2002/ukpo06/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{The UK Patent Office, wearing the hat of the British Government, has entrusted one of its subcontractors to organise a high profile rally in support of the proposed CEC/BSA software patentability directive.  Speeches will be held by hard core patent movement activists from the European Commission, the UK Government, the European Patent Office, the IBM patent department and some well-known patent law firms.  At the end of an intensive 6 hour long software patentablity propaganda firework a podium discussion will be held in which an ``open source representative'' will be allowed to sit at the table.  The organisers' initial choice was Alan Cox.  Alan has occasionally expressed deep concern about the software patent problem but, as a core developper of the Linux kernel and other key projects, he never really had the time to study the legal intricacies.  This fact alongside with his long hair and beard seemed to make him particularly qualified for the role of the social romantic dissident, generously tolerated by a group of patent lawyers posing as the businessmen of the real software industry.  Alan immediately understood the game and declined the invitation, handing the case over to the Eurolinux Alliance.  We have meanwhile been put on the conference program, but our abstract was doctored, our criticism suppressed, our message distorted, so that now we nevertheless fit into the design of this rally.}
\begin{sect}{ibc}{Conference Organising Subcontractor}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf IBC Global Conferences Limited, UK\footnote{http://www.ibc-uk.com}}}
\filbreak

\item
{\bf {\bf Conference home page\footnote{http://www.ibclegal.com/computerpatents}}}

\begin{quote}
As can be observed here, the UKPO and related circles are a traditional major customer of IBC, perhaps even the most important one.  IBC is also closely connected to or published journals such as ``Patent World'' and ``IT Law Today''.  It can be safely considered to be a reliable member of the UK Patent Family\footnote{http://swpat.ffii.org/akteure/uk/index.en.html}.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi

A similarly structured patent orifice had been set up by the UK patent establishment and organised by IBC in London on 2000-10-20, with Robert J Hart\footnote{http://swpat.ffii.org/akteure/rhart/index.en.html} acting as the chairman.  However there was a slightly larger participation of dissenting views than envisaged this time.
\end{sect}

\begin{sect}{plan}{Original Schedule}
Proposal for a Directive on the Protection of Computer Implemented Inventions\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html}

2002-06-19

Brussels

\begin{description}
\item[Chairman:]\ Roger Broadie\\
President TMPDF\footnote{http://swpat.ffii.org/papiere/eukonsult00/tmpdf/index.en.html}
\end{description}

\begin{center}
\begin{tabular}{|R{44}|L{44}|}
\hline
09.00 & Registration and coffee\\\hline
09.30 & Chairman's opening remarks\\\hline
09.40 & Outline of the Proposed Directive\\
Anthony Howard\footnote{http://swpat.ffii.org/akteure/ahoward/index.en.html}\\
DG Internal Market\footnote{}\\
European Commission\footnote{}\\\hline
10.20 & Coffee\\\hline
10.40 & The EPO's Assessment of the Proposed Directive\\
How it differs from EPO practice\\
Dr. Gert Kolle\\
European Patent Office, Munich\footnote{http://swpat.ffii.org/akteure/epa/index.de.html}\\\hline
11.10 & The UK Government's View
\begin{itemize}
\item
The underlying economic issues

\item
Views of the software industry and business world

\item
Where we go from here
\end{itemize}\\
Peter Hayward and Software Patents\footnote{http://swpat.ffii.org/akteure/hayward/index.en.html}\\
Divisional Director\\
The UK Patent Office\footnote{http://swpat.ffii.org/akteure/uk/index.en.html}\\\hline
11.40 & The Effect of the Proposed Directive at National Level\\
1. Germany\\
Fritz Teufel\footnote{http://swpat.ffii.org/akteure/teufel/index.en.html}\\
Head of IP Department\\
IBM, Germany\\\hline
12.00 & 2. UK\\
The Impact on the Software Patent Industry\\
Keith Beresford\footnote{http://swpat.ffii.org/papiere/beresford00/index.en.html}\\
European Patent Attorney, Keith Beresford & Co\\\hline
12.30 & 3.  FRANCE\\
Jacques Combeau*\\
European Patent Attorney\\\hline
13.00 & Lunch\\\hline
14.15 & Implications for BioPharma and Bioinformatics Industries
\begin{itemize}
\item
Current patent protection challenges

\item
In silico patent infringement

\item
Impact on the European Biopharma and Bioinformatics Industry's global competitiveness

\item
Compliance with TRIPS
\end{itemize}\\
Alex Wilson\\
Solicitor, Bristows\\\hline
15.00 & Tea\\\hline
15.20 & ROUND TABLE DISCUSSION\\
The speakers will be joined by Open Source representative (Alan Cox, Director, RedHat)\\\hline
16.30 & Chairman's summary\\\hline
16.35 & Close of conference\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{doct}{How the Eurolinux abstract was doctored}
On 2002-03-11 Hartmut Pilch answered to the following request of the organisers:

\begin{quote}
{\it I would also be grateful if you could let me have three or four bullet points outlining the main issues that you would cover.  I am pasting the programme at the bottom of this email.}
\end{quote}

by suggesting the following as a discussion basis (``for the time being'', cc to the eurolinux mailing list, speaker not yet determined):

\begin{quote}
{\it The Eurolinux Alliance, representing 120000 signatories and 300 software companies and organisations from the free and proprietary software world, sees the directive proposal as a poorly crafted attempt to
\begin{itemize}
\item
legalise 30000 ridiculous patents illegally granted by the EPO

\item
make everything under the sun patentable

\item
suck the life blood out of the European software industry

\item
plunge the patent system even deeper into chaos and dysfunctionality
\end{itemize}}
\end{quote}

The IBC people then changed this to

\begin{quote}
{\it The Eurolinux Alliance, representing 120000 signatories and 300 software companies and organisations from the free software world, sees the directive proposal as a poorly crafted attempt to
\begin{itemize}
\item
legalise 30000 patents

\item
make everything patentable

\item
take the life blood out of the European software industry

\item
plunge the patent system into chaos
\end{itemize}}
\end{quote}

The above version was, without any further communication, printed on a pamphlet and sent out to the potential participants, probably thousands of patent professionals, during the latter half of March.

The main differences between the versions are:

\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
Eurolinux version & IBC version & comments\\\hline
representing ... from the free and proprietary software world & representing ... from the free software world & This turns our statement into its opposite.  Indeed 1/2 of the 300 Eurolinux sponsors write proprietary software, much of which runs on MS Windows or Mac OS.  Yet it is the policy of the patent establishment to portray its critics as ``opensource'' or ``free software'' advocates and thus make them look like a minority.\\\hline
legalise 30000 ridiculously trivial software patents, illegally granted by the EPO & legalise 30000 patents & This takes away our whole criticism.\\\hline
plunge the patent system even deeper into chaos and dysfunctionality & plunge the patent system into chaos & This turns our statement into its opposite and makes it look ridiculously dramatic.  The IBC version implies that the patent system is in order and the directive proposal has a huge impact.  Our version said that it is already in chaos and the directive proposal pushes this a little further.\\\hline
\end{tabular}
\end{center}

On 2002-03-28, when we protested against this rewriting of our text, IBC representative Anna D'Alton told us that our initial outline had to be amended because it had been too long and ``too inflammatory''.

It should be noted that even our first preliminary outline would not have been the longest on the program, and there was certainly room for consenting on changes, as long as our basic message was preserved.

But Anna didn't respond to Hartmut's mail.  This mail also contained other questions and offers, such as an offer to bring more qualified speakers to the conference, so as to turn it into a real forum of dialogue.

It is quite clear what UK patent family is afraid of: dialogue.  They must insist on conducting a virtual debate in Brussels in order to create the impression that any opposition to legalising their practise is only marginal.  All the other speakers are carefully chosen to achieve this purpose, and critics are only allowed to appear in a marginalised role, carefully assigned to a minority position.

We have asked for a correction at least on the website (the thousands of leaflets alread printed and distributed will be difficult to correct), but again didn't receive a response.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise, including some possible topics for an Eurolinux presentation}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EUK \& BSA 2002-02-20: Vorschlag, alle n\"{u}tzlichen Ideen patentierbar zu machen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html}}}

\begin{quote}
Die Europ\"{a}ische Kommission (EUK) schl\"{a}gt vor, die Patentierung von Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren und sicher zu stellen, dass breite und triviale Patente auf Programm- und Gesch\"{a}ftslogik, wie sie derzeit vor allem in den USA von sich reden machen, k\"{u}nftig auch hier in Europa Bestand haben und von keinem Gericht mehr zur\"{u}ckgewiesen werden k\"{o}nnen.  ``Aber hallo, die EUK sagt in ihrer Presseerkl\"{a}rung etwas ganz anderes!'', m\"{o}chten Sie vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK wirklich sagt, m\"{u}ssen Sie n\"{a}mlich nicht die PE sondern den Vorschlag selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom Europ\"{a}ischen Patentamt (EPA) verfasst, in dem gew\"{o}hnliche W\"{o}rter oft das Gegenteil dessen bedeuten, was Sie erwarten w\"{u}rden. Zur Verwirrung tr\"{a}gt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit von Patenten und propriet\"{a}rer Software beschworen wird, wobei dem software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden suggeriert wird.  Dieser Text ignoriert die Meinungen von allen geachteten Programmierern und Wirtschaftswissenschaftlern und st\"{u}tzt seine sp\"{a}rlichen Aussagen \"{u}ber die \"{O}konomie der Software-Entwicklung nur auf zwei unver\"{o}ffentlichte Studien aus dem Umfeld von BSA (von Microsoft und anderen amerikanischen Gro{\ss}unternehmen dominierter Verband zur Durchsetzung des Urheberrechts) \"{u}ber die Wichtigkeit propriet\"{a}rer Software.  Diese Studien haben \"{u}berhaupt nicht Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag selber wurden offensichtlich f\"{u}r die EUK von einem Angestellten von BSA redigiert.  Unten zitieren wir den vollst\"{a}ndigen Vorschlag zusammen mit Belegen f\"{u}r die Rolle von BSA, einer Analyse des Inhalts und einer tabellarischen Gegen\"{u}berstellung der BSA- und EUK-Version sowie einer EP\"{U}-Version, d.h. eines Gegenvorschlages im Geiste des Europ\"{a}ischen Patent\"{u}bereinkommen von 1973 und der aufgekl\"{a}rten Patentliteratur.  Die EP\"{U}-Version sollte Ihnen helfen, die Klarheit und Weisheit der heute g\"{u}ltigen gesetzlichen Regelung verstehen, an deren Aushebelung die Patentanw\"{a}lte der Europ\"{a}ischen Kommission gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet haben.
\end{quote}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org/akteure/uk/index.en.html}}}

\begin{quote}
Much of the lobbying for software patents in Europe has been done by British patent lawyers and patent officials wearing the hat of the European Commission or the British government.  Most of the law-drafters and alleged ``independent contractors'' for the European Commission (CEC) were members of the British patent family.  The UK Patent Office (UKPO) was the first national patent office to officially follow the European Patent Office (EPO) in allowing direct patent claims to computer programs in 1998.  The UKPO has also succeeded in keeping its hand on the British government's patent policy, in moderating a public consultation which showed strong public opposition to software patentability, and, most admirably, in interpreting this public opinion as a legitimation basis for \begin{enumerate}
\item
establishing software (including business method) patents in Britain and

\item
pushing Brussels to establish them in all of Europe.
\end{enumerate}  The UK Patent Family appears to be the strongest single force behind the European Commission's software patentability directive project.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente mit ausgleichender Regelung: eine Kostenermittlung\footnote{http://swpat.ffii.org/analyse/pleji/index.de.html}}}

\begin{quote}
Europa bereitet wesentliche \"{A}nderungen seines Patentsystem vor.  Das Europ\"{a}ische Patentamt (EPA) hat vorgeschlagen, Beschr\"{a}nkungen der Patentierbarkeit wie z.B. den Ausschluss von Computerprogrammen in Art 52 des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) aufzuheben.  Eine Stellungnahme der Franz\"{o}sischen Technikakademie (Acad\'{e}mie des Technologies) unterst\"{u}tzt diesen Plan aber schl\"{a}gt zus\"{a}tzlich Regelung zur Begrenzung m\"{o}glicher sch\"{a}dlicher Folgen von Softwarepatenten vor.  In diesem Artikel versuchen wir, die Kosten solcher Ausgleichsregelungen zu ermitteln.  Es ergeben sich j\"{a}hrliche Ausgaben von 1-5 Milliarten EUR f\"{u}r die Europ\"{a}ische Gemeinschaft.  Verschiedene Ans\"{a}tze und billigere Alternativen werden in diesem Artikel untersucht.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{http://swpat.ffii.org/analyse/erfindung/index.de.html}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die juristische Fachliteratur.
\end{quote}
\filbreak

\item
{\bf {\bf Copyright Design and Patent Act\footnote{http://www.hmso.gov.uk/acts/acts1988/Ukpga\_19880048\_en\_1.htm}}}

\begin{quote}
Misrepresenting our outline may be covered by section 80.  However illicitely copying our work is not the real problem.  Rather the organisers claim that we said something which we didn't say, and they use this claim to make us appear silly in the eyes of a large public:  libel.  Also, public institutions such as the UK government, its name and possibly its money, are being enlisted for collusion in propaganda activities at the service of predatory groups.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf By changing our title and mispresenting our position, the UKPO's subcontractors may have violated UK laws concerning copyright, libel et al.  Which and how?}}

\begin{quote}
Please find and quote the relevant law texts for us.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
% mode: latex ;
% End: ;

