<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The UK Patent Office, wearing the hat of the British Government, has entrusted one of its subcontractors to organise a high profile rally in support of the proposed CEC/BSA software patentability directive.  Speeches will be held by hard core patent movement activists from the European Commission, the UK Government, the European Patent Office, the IBM patent department and some well-known patent law firms.  At the end of an intensive 6 hour long software patentablity propaganda firework a podium discussion will be held in which an %(q:open source representative) will be allowed to sit at the table.  The organisers' initial choice was Alan Cox.  Alan has occasionally expressed deep concern about the software patent problem but, as a core developper of the Linux kernel and other key projects, he never really had the time to study the legal intricacies.  This fact alongside with his long hair and beard seemed to make him particularly qualified for the role of the social romantic dissident, generously tolerated by a group of patent lawyers posing as the businessmen of the real software industry.  Alan immediately understood the game and declined the invitation, handing the case over to the Eurolinux Alliance.  We have meanwhile been put on the conference program, but our abstract was doctored, our criticism suppressed, our message distorted, so that now we nevertheless fit into the design of this rally.
title: UK Patent Orifice: Software Patentability Rally Brussels 2002-06-19
Otc: Conference Organising Subcontractor
Ang: As can be observed here, the UKPO and related circles are a traditional major customer of IBC, perhaps even the most important one.  IBC is also closely connected to or published journals such as %(q:Patent World) and %(q:IT Law Today).  It can be safely considered to be a reliable member of the %(uk:UK Patent Family).
Aba: A similarly structured patent orifice had been set up by the UK patent establishment and organised by IBC in London on 2000-10-20, with %(rh:Robert J Hart) acting as the chairman.  However there was a slightly larger participation of dissenting views than envisaged this time.
Shd: Original Schedule
plan: Proposal for a Directive on the Protection of Computer Implemented Inventions
Car: Chairman
HiW: How the Eurolinux abstract was doctored
Ohg: On 2002-03-11 Hartmut Pilch answered to the following request of the organisers:
Ioo: I would also be grateful if you could let me have three or four bullet points outlining the main issues that you would cover.  I am pasting the programme at the bottom of this email.
bsn: by suggesting the following as a discussion basis (%(q:for the time being), cc to the eurolinux mailing list, speaker not yet determined):
Twa: The Eurolinux Alliance, representing 120000 signatories and 300 software companies and organisations from the free and proprietary software world, sees the directive proposal as a poorly crafted attempt to
lul: legalise 30000 ridiculous patents illegally granted by the EPO
mns: make everything under the sun patentable
sWn: suck the life blood out of the European software industry
pea: plunge the patent system even deeper into chaos and dysfunctionality
Tpa: The IBC people then changed this to
TWr: The Eurolinux Alliance, representing 120000 signatories and 300 software companies and organisations from the free software world, sees the directive proposal as a poorly crafted attempt to
leW: legalise 30000 patents
mrp: make everything patentable
tWn: take the life blood out of the European software industry
ppe: plunge the patent system into chaos
ToW: The above version was, without any further communication, printed on a pamphlet and sent out to the potential participants, probably thousands of patent professionals, during the latter half of March.
Teh: The main differences between the versions are:
Eiv: Eurolinux version
IWr: IBC version
cme: comments
rmi: representing ... from the free and proprietary software world
rlW: representing ... from the free software world
Iic: This turns our statement into its opposite.  Indeed 1/2 of the 300 Eurolinux sponsors write proprietary software, much of which runs on MS Windows or Mac OS.  Yet it is the policy of the patent establishment to portray its critics as %(q:opensource) or %(q:free software) advocates and thus make them look like a minority.
lWi: legalise 30000 ridiculously trivial software patents, illegally granted by the EPO
leW2: legalise 30000 patents
Tao: This takes away our whole criticism.
pea2: plunge the patent system even deeper into chaos and dysfunctionality
ppe2: plunge the patent system into chaos
NWe: This turns our statement into its opposite and makes it look ridiculously dramatic.  The IBC version implies that the patent system is in order and the directive proposal has a huge impact.  Our version said that it is already in chaos and the directive proposal pushes this a little further.
WWh: On 2002-03-28, when we protested against this rewriting of our text, IBC representative Anna D'Alton told us that our initial outline had to be amended because it had been too long and %(q:too inflammatory).
Inf: It should be noted that even our first preliminary outline would not have been the longest on the program, and there was certainly room for consenting on changes, as long as our basic message was preserved.
Bqe: But Anna didn't respond to Hartmut's mail.  This mail also contained other questions and offers, such as an offer to bring more qualified speakers to the conference, so as to turn it into a real forum of dialogue.
IWe: It is quite clear what UK patent family is afraid of: dialogue.  They must insist on conducting a virtual debate in Brussels in order to create the impression that any opposition to legalising their practise is only marginal.  All the other speakers are carefully chosen to achieve this purpose, and critics are only allowed to appear in a marginalised role, carefully assigned to a minority position.
WtW: We have asked for a correction at least on the website (the thousands of leaflets alread printed and distributed will be difficult to correct), but again didn't receive a response.
ibr: including some possible topics for an Eurolinux presentation
Mtf: Misrepresenting our outline may be covered by section 80.  However illicitely copying our work is not the real problem.  Rather the organisers claim that we said something which we didn't say, and they use this claim to make us appear silly in the eyes of a large public:  libel.  Also, public institutions such as the UK government, its name and possibly its money, are being enlisted for collusion in propaganda activities at the service of predatory groups.
Tht: By changing our title and mispresenting our position, the UKPO's subcontractors may have violated UK laws concerning copyright, libel et al.  Which and how?
NWo: Please find and quote the relevant law texts for us.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpukpo026 ;
# txtlang: de ;
# End: ;

