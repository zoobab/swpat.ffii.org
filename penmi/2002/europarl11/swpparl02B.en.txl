<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl Hearings 2002-11-07 and 26

#descr: On Nov 7, the European Parliament's Legal Affairs Committee (JURI)
conducted a hearing about the proposed software patentability
directive.  10 invited experts were given 5-10 minutes of time to
respond to a series of questions which imply that software ideas must
be patentable inventions and that opposition to this line can be
dismissed as minoritarian.  The bias of this hearing was, as it turned
out, the fruit of intense lobbying by industrial patent lawyers.  In
spite of the initial bias however, most MEPs did begin to question
what they had been taught.  They more or less noticed that the
directive proposal hands over a carte blanche to patent courts in a
matter where basic freedoms of citizens, including many of the most
productive software businesses of Europe, are at stake.  Another
hearing on 2002-11-26 helped Europe's legislature gain confidence
needed to set clear and adequate rules for the patent jurisdiction to
follow.

#Pil: Patent-Universalist Bias and Eurolinux Message

#Pea: Emerging Problems and Solution Alternatives

#Eso: Eurolinux Proposal: Combined Solution Approach

#Plt: Patent Ideologemes

#Fsf: Untruthful Assertions from EICTA and UNICE

#ttf: Questionable Statements from EPO

#Dat: A Glimpse into EU Lobbying Structures

#Ssi: Green Hearing 2002-11-26

#Tlm: The hearing was based on the unaltered %(am:McCarthy working papers)
which we had criticised already in June.  These working papers defined
the questions to which respondents were to answer.  They attempt to
portray the patent critics as a %(q:vociferous) minority whose
arguments %(q:many) do not agree with and who should bear the burden
of proof.

#TWe2: The hearing date was fixed only a few weeks in advance and only 5-10
minutes of speaking time were planned for each speaker.  Moreover, the
speakers were exhorted to use their scarce time to respond to
legalistic questions which implicitely propagated the patent
establishment's viewpoint and tried to preclude any debates on what
the directive should achieve and what is desirable in terms of
software economics.  Moreover, the hearing was scheduled to start with
5 speeches from known software patentability advocates and the
majority was to be put in a minority position with short speaking
time.

#Ash: At the hearing, we distributed a very detailed printed documentation
in %(en:English), %(de:German) and %(fr:French).  It is based on the
PDF version of our Call for Action with appendices, but with much
improved typesetting, thanks to Peter Gerwinski.  Europe Shareware
brought a %(OL:letter from Opera Software Inc) which supports our
position in detail.  CALIU brought piles of copies of various CALIU
and FFII documents.  Industrial patent lawyers brought papers which
basically applaud the McCarthy paper and say that in particular the
%(q:Some Arguments) section, which minoritises the Eurolinux Alliance,
is a %(q:very good basis) for EuroParl legislation.

#TWT: The message of %(hp:Hartmut's presentation) boiled down to something
like %(bc:Dear Politicians, we are the European Software Industry
which you have always dreamt of fostering, we have built much of the
IT infrastructure which you have often talked of building, so why do
you want to kill us?  The European Commission's proposal means giving
the Parliament's blessing to European Patent Office's illegal practise
of granting broad patents on trivial rules of organisation and
calculation, and making this practise permanent and obligatory for all
of Europe.  We have a counter-proposal which really does what the
Commission says it wants to do: effectively limit patentability and
enhance legal security.)

#Taa: This was not to the taste of all JURI politicians.  Dr. iur. Joachim
Würmeling, a conservative MEP from Bavaria, showed his displeasure by
gestures and audible talking, which, according to some observers,
started from the moment when Mr. Pilch was called to speak and
persisted until the end, and even by walking out of the room during
the speech.  Mr. Würmeling later publicly stated that we do not have a
counter-proposal.  Unfortunately for us, Mr. Würmeling is the shadow
rapporteur for the European People's Party.  Würmeling complained
about Pilch's %(q:massive accusations), but refrained from examining
the substance of these accusations.  Würmeling did however display an
above-average level of interest in the subject by posing numerous
questions about formal law issues to the patent officials and
university scholars.

#Isi: In spite of the bias, irriation and noise, in the end the problems and
solution alternatives which the European Parliament is facing did to
some degree emerge.  The three invited law scholars Michel Vivant
(FR), %(rb:Reinier Bakels) (NL) and Albert Bercovitz Rodrigéz-Cano
(ES) took a refreshingly objective approach to the problems, and UK
software entrepreneur Dominic Sweetman reinforced our position with a
very calm speech about the lack of clarity in the propposed directive.
 The former french prime minister Michel Rocard, who is chairing the
Cultural Commission (CULT), cast doubts on the wisdom of the directive
by posing five basic questions of political direction, the gist of
which was to demand explanations in a language which an intellectual
generalist-politician such as Rocard can understand.  This was seen by
political experts as a warning that CULT, which is one of the
commissions which are scheduled to report about the directive project,
will take a particularly critical look at the proposed directive.

#Tmr: The EuroParl is faced with the problem of drawing a limit to what can
be patented, in particular with respect to computer programs and
business methods.

#Awn: Among the speakers, there was a fair amount of consensus on the
following points

#Ttr: There are too many %(q:trivial software patents), and the
%(dp:directive proposal) does not address this problem.  %(rh:Mr.
Hart) criticised that the directive may create the impression that it
is treating %(q:inventive step) somewhat diffently in software than in
other fields and demanded that it be clarified that this cannot be the
case, since it would violate %(tr:Art 27 TRIPs)

#cte: %(q:Business method patents) are unpopular.  Mr. Hart and Mr. Bakels
demanded that they be explicitely excluded.  However, %(vv:Prof.
Vivant) warned that the EPO's approach to the EPC has shown that
literal exclusions may prove useless, if the excluded thing can be
patented under a different wording.  Instead, Vivant stressed, it is
necessary to create an institutional framework for controlling the
EPO.

#Mnf: Many of the terms used in the debate are abstract and opaque.  This
was pointed out in particular by Sweetman, Pilch and Rocard, and Ms
McCarthy reaffirmed this impression in her summary and suggested that
the debate should be oriented toward example patents.  Sweetman said
that he had tried for 6 months to learn what %(ep:EPO) and %(uk:UKPO)
mean by %(q:technical) and, in particular, to give an example of a
software idea which, according to their understanding, could %(s:not)
be framed in such a way that it would qualify as %(q:technical).  Many
players have furthermore %(ec:demanded) that any legislative proposal
be %(q:framed by reference to a series of example patents).

#TlW: The %(tr:TRIPs treaty) obliges us to formulate the rules of
patentability solely by means of the concepts of %(q:invention),
%(q:technology), and %(q:industrial application).  Several speakers
suggested a definition of technology along the line of the BGH (German
Federal Court of Justice) concept of a %(q:problem solution involving
the use of controllable forces of nature) (Rote Taube 1969,
%(dp:Dispositionsprogramm 1976)) or similar concepts.  Mr. Bercovitz
explained that the requirement of %(q:industrial application) means
that only those problem solutions are patentable inventions which
would usually require an industrial organisation (i.e. factory
equipment) in order to be put to work successfully.  The patent
establishment's speakers mostly opted for circular definitions of
technology (e.g. technology = technical solution of technical problem)
and demanded that the details should be left to the courts to decide. 
Mr. Nguyen (%(UN), patent head of Thales) also argued along this line
and proposed a sociological distinction as the baseline: i.e. the
courts should assess whether an idea for which a patent is sought is
typically the work of a %(q:technical) department or of a managerial
department.  Mr. Bakels expressed his view that no clear distinction
is possible within the TRIPs terminology framework, and instead of
passing a directive the EP should install a permanent %(q:Patent
Observatory) which would study the problems and work out new ways of
distinguishing the patentable from the non-patentable.

#Eih: The Eurolinux Alliance

#rte: opposes the idea of giving the courts unoverseeable freedom to decide
what is %(q:technical)

#sWt: supports all initiatives for building independent institutions for
permanent supervision of the patent system (such as the Bakels
%(q:Patent Observatory) or similar)

#sWn: suggests that detailed specifications, such as may be proposed by a
%(q:patent observatory), be based on the traditional concept of
%(ti:problem solution involving use of controllable forces of nature),
at least as long as Art 27 TRIPs is in force.

#Aug: As Prof. Vivant pointed out, the purpose of issuing a directive could
be to

#cWp: clarify the limits of patentability

#sti: strengthen control on the european patent system by subjecting EPO
decisions to the jurisdiction of the European Court of Justice (ECJ)

#Idt: If the courts are to decide what is %(q:technical), what then is the
point of passing a directive?

#Deg: Does the European Parliament really want to hand over its legislative
power to the patent courts?

#Wtw: Wouldn't that grossly violate the constitutional principle of division
of powers between legislature and judiciary?

#DlW: Does the European Parliament really want to place an indeterminate
legal concept at the heart of a piece of legislation on which basic
freedom rights of citizens depend and which has penal consequences?

#WWW: Wouldn't that grossly violate the constitutional principle that state
organs may not restrict an individual's basic rights by reference to
indeterminate legal concepts (as found e.g. in the German Basic Law)?

#Dnc: Does the European Union really want to grant European citizens a lower
standard of legal security and protection of basic freedom rights than
is mandated by national constitutions?

#Tlb: The proposal demands of the patent lobby for handover of sensitive
legislative competences should ring a bell with all JURI members.

#Sne: Some patent officials do see a problem here and have a ready excuse. 
They say (we heard it also at this hearing) that inventions are by
definition unforeseeable and that therefore we must put up with the
indeterminacy of the concept of %(q:technical contribution) as used in
the directive proposal.

#Tma: This argument does not hold water.

#Iiy: It is in contradiction with general knowledge found in patent law
textbooks.  Gert Kolle pointed out in his much-quoted %(gk:article of
1977) that the concept of technical invention derives its validity
from general principles which are rooted outside patent law, namely in
the field of general philosophy of science.  This has been implicitely
accepted by most patent law writers, including those arguing against
Kolle.  Philosophical dichotomies such as %(q:matter) and %(q:mind)
are very resistant to the change of time and have not been
revolutionised by any recent development in science or technology.  We
have demonstrated that the concept of %(q:problem solution involving
controllable forces of nature) as developped by the BGH and other
courts can be unambiguously applied to randomly chosen %(tb:testbeds)
of contemporary patent applications and yields both clear and adequate
results.

#Aun: As the BGH pointed out ludicly in its %(dp:Dispositionsprogramm
decision),

#Tnl: The involvement of forces of nature remains the only possible
criterion for reliably limiting what is a technical invention.

#Trt: The criterion is sufficiently general to allow the inclusion of new
technologies, such as biotechnology

#Glt: Giving up this criterion means giving up all limits to patentability
and thereby pushing the patent system onto an adventurist course.

#Era: Even if the concept of %(q:technical invention) could for some reason
be considered unstable and unsuitable for codification in law, then
such a consideration could not serve as a valid excuse for empowering
the patent establishment's courts to set the rules.  Instead the
Parliament could set them by means of literal exclusions and sets of
examples and revised them from time to time.  The Parliament could
entrust a %(q:Patent Observatory) with some of the related work.

#TWe: The %(dp:Dispositionsprogramm Decision) also points out a dangerous
popular error of the patent world which already existed in 1976 and
which was predominant among the patent establishment representatives
at this hearing:

#Tul: The patent system is also not a universal reception basin for all
ideas of the human mind which are not appropriatable by other systems.
 Rather, it is a specialised system for a special field of human
endeavors, that of technical inventions.

#Ttr2: The patent lawyers sent by UNICE and EICTA as well as some of the
patent office people committed the popular error against which the
German patent judges had warned: their speeches suggested that at
least all %(q:billion-dollar industries) must be regulated according
to the very set of game rules which they learnt in law school.  We
believe that this is an impermissible and dangerous ideologisation of
patent law.

#Ppt: Prof. Vivant correctly stressed in his speech that laws are not
emanations of some universal truth but must be tested by their
applicability to practise.

#MWc: Mr. Pilch said at the hearing that the UNICE and EICTA patent lawyers
represented an unenlightened, ideologised version of patent law.  They
have apparently not read much of the legal literature, of which
salient %(sc:quotations) are found in appendix F of the FFII
documentation which was distributed to the MEP's at the hearing

#Tee: The patent lawyers from %(LST) misrepresented their member base and
made false statements, partially against their better knowledge, e.g.

#Mve: Mr. Nguyen failed to mention that %(zv:ZVEI), a member organisation of
%(un:UNICE), has sent a letter to UNICE and to the German Ministery of
Justice in which it explained that its members, after further
discussions, expressed fears that the proposed directive would lead to
a proliferation of patents on broad and trivial concepts and that a
strict definition of %(q:technical invention) is needed in order to
prevent this from happening.  Mr. Nguyen not only failed to mention
these misgivings of UNICE members, but actually argued in the opposite
sense.  Moreover Mr Nguyen and Mr Hagedorn both failed to mention that
many of their constituents (such as %(VDMA) in Germany) have
deliberately abstained from expressing an opinion, because they are
too divided.  At Hagedorn's own organisation %(BK) in Germany, the
pro-softpat decision was taken by 7 patent lawyers of large
corporations, among them Mr. Hagedorn, in a closed session where only
one SME representative was present (and protested).  Mr Hagedorn
moreover misrepresented a %(sm:study of the German Ministery of
Economics) as showing support of German companies for software
patents.  In reality, this study shows that software patents are
unpopular among software companies in Germany and that patents are not
promoting innovation and are likely to lead to further concentration
and disappearance of SMEs, if the current trend is not reversed.

#sWh: In a speech before the European Parliament, Thales patent lawyer
Nguyen, representing UNICE, falsely claimed that his association's
member cited Mandy Haberman, a small software entrepreneur who has
filed software patents, as an example of successful use of patents by
small companies.  Sylvain Perchaud pointed out that Haberman is a
supporter of Europe Shareware (and thereby also Eurolinux) and shares
our opposition against software patents.  Ms Haberman has meanwhile
confirmed this in a %(mh:letter to the European Parliament).  Indeed
other Eurolinux members also posess some software patents but still
believe that general patent disarmament would serve us better.

#rWy: Article in a newspaper which is distributed for free inside the
European Parlement.  Quote: %(bc:Wuermeling felt that the %(q:open
source movement did not understand the directive in depth.) Indeed,
the German MEP saw the proposals as offering %(q:no real change) from
the current patenting situation. He also criticised the open source
lobby for failing to come up with alternative proposals.)  Heidi
Hautala, Arlene McCarthy and %(q:liberal MEPs) are reported to have
taken a more critical position to the directive proposal.

#cih: Patent Attorney Stephan Freischem, secretary of the German Section of
AIPPI, reports what he heard the speakers say at this hearing.  It
differs significantly (often diametrically) from what we heard. 
Freischem concludes his report by an exhortation to his peers to
mobilise %(q:those software companies that profit from the patent
system).  Freischem acknowledges that the term %(q:technical) is
ill-defined today, because the %(q:open source lobby) has %(q:twisted
the issue of harmonisation of software patent protection into a
question of free speech).  In another AIPPI report of 2002-11,
Freischem, by misrepresenting a polish colleague's speech, explains
that US practise would be preferable to EPO practise, because the term
%(q:technical) only creates legal uncertainty.

#dsW: AIPPI.de secretary PA Stephan Freischem reports inter alia about the
Europarl Hearing and about a lecture given by his polish colleague
Marek Laszewski on the patentability of data processing in DE, PL and
EU, in which Laszewski points out that the European Commission's
directive proposal raises more questions than it answers and that its
concept of %(q:technical contribution) leads to legal insecurity. 
Freischem downtones the critical meaning of Laszewski's lecture and
instead falsely states that Laszewski %(q:drew the conclusion that the
U.S. approach which grants patent protection to any useful invention
made by man which is new and inventive, independently of a technical
character, gives much better legal certainty.).

#nsW: Comments on Laszewski's paper on the technical invention in PL, DE and
EU and suggests that Freischem has misrepresented this paper by
claiming that it praises the US system for achieving %(q:much better
legal certainty).

#Mur: Mr Nguyen cited Mandy Haberman, a small entrepreneur who has filed
software patents, as an example of successful use of patents by small
companies.  Sylvain Perchaud pointed out that Haberman is a supporter
of Europe Shareware (and thereby also Eurolinux) and shares our
positions on software patents.  Ms Haberman has meanwhile confirmed
this in a %(mh:letter to the European Parliament).  Indeed other
Eurolinux members also posess some software patents but still believe
that general patent disarmament would serve us better.

#Mos: Mr. Hagedorn said that %(q:software is a multi-billion dollar industry
with expected growth-rates of 10% p.a. during the next years) and that
%(q:like in any other industry) such growth can only be sustained if
patents are available.  This statement is in contradiction with all
%(es:economic studies) and with the experience of SAP, the company in
whose patent department Mr. Hagedorn has been working since 1997.  SAP
grew to become a large company completely without filing a single
patent until 1997.  Moreover, SAP's board chair Prof. Hasso Plattner
has publicly explained at a hearing in Germany that SAP would not need
patents to protect its investments and is collecting them only as a
defensive weaopon to prepare for litigation in the US.

#Axr: Also, the claims about miraculous growth-rates in the %(q:software
industry), which echo the %(q:explanatory memorandum) of the CEC/BSA
directive proposal, are in marked contradiction to more realistic
estimates published by Bitkom (Hagedorn's EICTA member organisation)
in recent months.

#aeW: Dai Rees, software patent specialist of the European Patent Office
(EPO),said that the EPO has been wrongly accused by some people of
breaking the law.  In reality, Rees said, the EPO has only been
looking at court decisions of member state courts such as those in
Germany and Britain and tried to bring its own jurisdiction in line
with these courts.

#rna: This is untrue.  The EPO began granting software and business method
patents against the letter and spirit of the written law with the
Vicom and Sohei decisions in 1986.  For many years no other European
court was willing to follow the EPO on this adventurous course, and
German law commentaries such as Benkard did not hide their doubts as
to the legality of these EPO decisions.  Several high national courts
explicitely ruled against the new EPO line.  In a Swedish case, the
patent applicant later appealed to an EPO board to have his patent
granted against the verdict of the Swedish court.  In 2000 the German
Federal Court finally, under strong pressure from the patent law
community which was meanwhile planning a change of law, caught up with
the EPO, only to shy back again in a decision of 2001.  Rees of course
knows that the EPO's jurisdiction on software patents is widely
considered %(q:contra legem) by the law world and that this is the
main reason why the EPO asked for a change of Art 52 EPC in 2000 and
why its allies at the European Commission have been calling for
%(q:clarification) and %(q:harmonisation) by means of a directive.

#sma: Moreover, Rees dismissed as %(q:fearmongering) Pilch's assertion
(partially cited from a recent thread on the Linux Kernel developpers'
mailing list) that the EPO has granted thousands of memory management
patents which, if taken seriously, would make programming of the Linux
kernel very difficult if not impossible.  Perhaps the EuroParl might
find it worth studying who is right on this question.

#Ate: After the hearing, the speakers were invited by the %(ei:European
Internet Foundation) to a dinner with a few MEPs and public servants
from the European Commission (Howard, Noteboom).  The two patent
critical speakers, Dominic Sweetman and Hartmut Pilch, were placed at
a table together with people from Siemens, Alcatel, SAP and the EPO
and had a lively philosophical discussion there.  The politicians were
surrounded by the Brussels representatives of large companies -- those
who can afford to pay the high membership fee and to have a permanent
representative in Brussels.  A Microsoft representative was placed at
the central table with McCarthy.  The general secretary of EIF, an
American named Peter Linton, made a short introductory speech.  He
described the mission of EIF as one of promoting understanding of
information policy issues among EU politicians.  However it seemed
that most EIF members are not closely related to any Internet efforts
such as those of the Internet Engineering Task Force (IETF), whose
European branch ISOC-ECC is a signatory of our %(ca:Call for Action),
or those of the W3C, which has been struggling with software patents
for years.  Ms McCarthy, who is a %(q:governor of EIF), stressed in
her remark that the MEPs in EIF do not always follow the political
advice of the corporate members.  %(q:The role of MEPs in EIF seems to
be limited to receiving honorary titles and enjoying lavish dinners
but for the rest of the time not listening to us), one corporate
diplomat at our table joked.

#ncu: Most of the permanent Brussels representatives of companies knew the
business strategies of their company and its position on patents
fairly well.  Some of them are even patent lawyers.  In general the
positions consist of rather undifferentiated beliefs in the necessity
of protecting %(q:intellectual property).  They are intrigued by the
phenomenon of %(q:opensource) and not necessarily hostile to it, but
they tend to naturally ascribe all opposition to software patents to
this phenomenon.  It seemed that while it would be possible for us to
reach a consensus with some of these corporate representatives, that
still might not change very much, since they are only diplomats who
hand over what they receive from their company's headquarter.  And at
least at the headquarter of %(si:Siemens), the patent people remain
dominant, although many Siemens developpers and executives support the
Eurolinux positions.

#Dou: During our time in Brussels we met several more of the MEPs who are
involved in the decisionmaking, of which some were very understanding
and supportive of our positions while others were more or less
prejudiced against us.  Wherever we went, we found that %(LST) had
already been there several times.

#Tbr: This and the experience at the EIF dinner gave us an impression of
what real lobbying work looks like.   If we were able to do 1/10 of
what our adversaries are doing, we might easily win the battle in the
European Parliament.

#r0H: Program of 2002-11-26 Hearing

#ssu: Schedule, Presentations and an extensive link collection of the
Multi-Partisan Hearing organised by the European Greens in the
European Parliament on 2002-11-26

#ApW: A more extensive and hearing is being organised by the Green Party
Group in the European Parliament.  Among the speakers are %(LST). 
There will be more time available to go more deeply into the problems
faced by the EuroParl and the solutions which we propose.  Hopefully
it will not be the last hearing but a series of events which lead to
an institutionalised framework for bringing the European patent
establishment under legislative control.

#omW: Opinion of the Committee of the Regions

#aaH: Contains Program of the Hearing

#eWe: another eyewitness report of the hearing

#Wos: Account of a journalist of a leading Danish newspaper

#lrm: Philippe Aigrain's Comments on the 021107 Hearing

#ioe: An employee of the European Commission's Information Society
Directorate who attended the hearing confirms some of our impressions
and compares our difficulties to those of the people who had to work
against the tobacco lobby.  Finds that an enormous amount of patient
pedagogical work is needed.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl02B ;
# txtlang: en ;
# multlin: t ;
# End: ;

