# -*- mode: makefile -*-

demandes.fr.lstex:
	lstex demandes.fr | sort -u > demandes.fr.lstex
demandes.fr.mk:	demandes.fr.lstex
	vcat /ul/prg/RC/texmake > demandes.fr.mk


demandes.fr.dvi:	demandes.fr.mk
	rm -f demandes.fr.lta
	if latex demandes.fr;then test -f demandes.fr.lta && latex demandes.fr;while tail -n 20 demandes.fr.log | grep -w references && latex demandes.fr;do eval;done;fi
	if test -r demandes.fr.idx;then makeindex demandes.fr && latex demandes.fr;fi

demandes.fr.pdf:	demandes.fr.ps
	if grep -w '\(CJK\|epsfig\)' demandes.fr.tex;then ps2pdf demandes.fr.ps;else rm -f demandes.fr.lta;if pdflatex demandes.fr;then test -f demandes.fr.lta && pdflatex demandes.fr;while tail -n 20 demandes.fr.log | grep -w references && pdflatex demandes.fr;do eval;done;fi;fi
	if test -r demandes.fr.idx;then makeindex demandes.fr && pdflatex demandes.fr;fi
demandes.fr.tty:	demandes.fr.dvi
	dvi2tty -q demandes.fr > demandes.fr.tty
demandes.fr.ps:	demandes.fr.dvi	
	dvips  demandes.fr
demandes.fr.001:	demandes.fr.dvi
	rm -f demandes.fr.[0-9][0-9][0-9]
	dvips -i -S 1  demandes.fr
demandes.fr.pbm:	demandes.fr.ps
	pstopbm demandes.fr.ps
demandes.fr.gif:	demandes.fr.ps
	pstogif demandes.fr.ps
demandes.fr.eps:	demandes.fr.dvi
	dvips -E -f demandes.fr > demandes.fr.eps
demandes.fr-01.g3n:	demandes.fr.ps
	ps2fax n demandes.fr.ps
demandes.fr-01.g3f:	demandes.fr.ps
	ps2fax f demandes.fr.ps
demandes.fr.ps.gz:	demandes.fr.ps
	gzip < demandes.fr.ps > demandes.fr.ps.gz
demandes.fr.ps.gz.uue:	demandes.fr.ps.gz
	uuencode demandes.fr.ps.gz demandes.fr.ps.gz > demandes.fr.ps.gz.uue
demandes.fr.faxsnd:	demandes.fr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo demandes.fr-??.g3n`;source faxsnd main
demandes.fr_tex.ps:	
	cat demandes.fr.tex | splitlong | coco | m2ps > demandes.fr_tex.ps
demandes.fr_tex.ps.gz:	demandes.fr_tex.ps
	gzip < demandes.fr_tex.ps > demandes.fr_tex.ps.gz
demandes.fr-01.pgm:
	cat demandes.fr.ps | gs -q -sDEVICE=pgm -sOutputFile=demandes.fr-%02d.pgm -
demandes.fr/demandes.fr.html:	demandes.fr.dvi
	rm -fR demandes.fr;latex2html demandes.fr.tex
xview:	demandes.fr.dvi
	xdvi -s 8 demandes.fr &
tview:	demandes.fr.tty
	browse demandes.fr.tty 
gview:	demandes.fr.ps
	ghostview  demandes.fr.ps &
print:	demandes.fr.ps
	lpr -s -h demandes.fr.ps 
sprint:	demandes.fr.001
	for F in demandes.fr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	demandes.fr.faxsnd
	faxsndjob view demandes.fr &
fsend:	demandes.fr.faxsnd
	faxsndjob jobs demandes.fr
viewgif:	demandes.fr.gif
	xv demandes.fr.gif &
viewpbm:	demandes.fr.pbm
	xv demandes.fr-??.pbm &
vieweps:	demandes.fr.eps
	ghostview demandes.fr.eps &	
clean:	demandes.fr.ps
	rm -f  demandes.fr-*.tex demandes.fr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} demandes.fr-??.* demandes.fr_tex.* demandes.fr*~
demandes.fr.tgz:	clean
	set +f;LSFILES=`cat demandes.fr.ls???`;FILES=`ls demandes.fr.* $$LSFILES | sort -u`;tar czvf demandes.fr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
