\contentsline {chapter}{\numberline {1}We are concerned that}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}For these reasons we recommend the following:}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Signatories}{8}{chapter.3}
\contentsline {chapter}{\numberline {A}European Software Patents: Assorted Examples}{11}{appendix.A}
\contentsline {chapter}{\numberline {B}Software Patents in Action}{17}{appendix.B}
\contentsline {section}{\numberline {B.1}Some Well Documented Cases}{17}{section.B.1}
\contentsline {section}{\numberline {B.2}Annotated Links}{23}{section.B.2}
\contentsline {chapter}{\numberline {C}Patentability Legislation Benchmarking Test Suite}{24}{appendix.C}
\contentsline {section}{\numberline {C.1}Some sample patents}{24}{section.C.1}
\contentsline {section}{\numberline {C.2}Questions to be answered for each}{24}{section.C.2}
\contentsline {section}{\numberline {C.3}Comparison Table}{25}{section.C.3}
\contentsline {section}{\numberline {C.4}Legislation candidates to be tested}{25}{section.C.4}
\contentsline {chapter}{\numberline {D}The BSA/CEC Proposal(s) and EPC-based Counter-Proposal}{26}{appendix.D}
\contentsline {section}{\numberline {D.1}Preamble}{26}{section.D.1}
\contentsline {section}{\numberline {D.2}The Articles}{33}{section.D.2}
\contentsline {chapter}{\numberline {E}MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal}{40}{appendix.E}
\contentsline {section}{\numberline {E.1}Some Criticisms of McCarthy's Paper}{40}{section.E.1}
\contentsline {section}{\numberline {E.2}Responses to McCarthy's Questions}{44}{section.E.2}
\contentsline {subsection}{\numberline {E.2.1}Is such a directive necessary or can we just leave matters to the Boards of Appeal and the national patent courts?}{44}{subsection.E.2.1}
\contentsline {subsubsection}{Can we just leave matters to the EPO's boards of a appeal and national patent courts?}{44}{section*.3}
\contentsline {subsubsection}{Is there a problem of divergent national rules which needs to be addressed by a directive, as CEC/BSA claim?}{45}{section*.4}
\contentsline {subsubsection}{Are the other proclaimed goals of the directive proposal valid?}{45}{section*.5}
\contentsline {subsubsection}{Are there any valid goals for which a directive might be necessary?}{46}{section*.6}
\contentsline {subsection}{\numberline {E.2.2}Will the directive achieve its ends, in particular without unwanted side effects?}{46}{subsection.E.2.2}
\contentsline {subsection}{\numberline {E.2.3}Does Art. 5 run counter to TRIPs in imposing artificial restrictions on the type of claims permissible in this field? Is it intended to be retroactive? If so, with what effects?}{47}{subsection.E.2.3}
\contentsline {subsection}{\numberline {E.2.4}Is the proposal legally watertight (legal certainty)?}{47}{subsection.E.2.4}
\contentsline {subsection}{\numberline {E.2.5}Is there any merit in following in the footsteps of the USA (patentability of business methods) or should patentability of business methods be clearly and specifically precluded?}{47}{subsection.E.2.5}
\contentsline {subsection}{\numberline {E.2.6}Should the system of (compulsory) licences be reviewed to prevent abuse of the patent system?}{48}{subsection.E.2.6}
\contentsline {subsection}{\numberline {E.2.7}Is the issue of trivial patents a problem? If so, how should it be addressed?}{48}{subsection.E.2.7}
\contentsline {subsection}{\numberline {E.2.8}What risk, if any, is posed to open-source software? If so, how is it that open-source and proprietary software seem to co-exist at present?}{48}{subsection.E.2.8}
\contentsline {subsection}{\numberline {E.2.9}Is it possible to argue that patents may stifle innovation, if so how?}{48}{subsection.E.2.9}
\contentsline {subsection}{\numberline {E.2.10}Is it possible to quantify in economic terms/employment the benefits/disbenefits of software patentability?}{49}{subsection.E.2.10}
\contentsline {subsection}{\numberline {E.2.11}What impact, if any, will action in this area, one way or another, have on SMEs?}{49}{subsection.E.2.11}
\contentsline {subsection}{\numberline {E.2.12}What is the impact of TRIPs?}{49}{subsection.E.2.12}
\contentsline {subsection}{\numberline {E.2.13}The EPC is not limited to EU countries. What are the implications of this in the event that the directive is adopted?}{49}{subsection.E.2.13}
\contentsline {subsection}{\numberline {E.2.14}Do recital 18 and Art. 6 of the proposal for a directive need to be reworded in order to allow decompilation of programs for the purposes of interoperability as is permitted under Directive 91/250?}{50}{subsection.E.2.14}
\contentsline {section}{\numberline {E.3}Unasked Questions}{50}{section.E.3}
\contentsline {chapter}{\numberline {F}Quotations on the question of the patentability of rules of organisation and calculation}{51}{appendix.F}
\contentsline {section}{\numberline {F.1}Software \& Technology Practitioners}{51}{section.F.1}
\contentsline {section}{\numberline {F.2}Law Scholars, Judges, Patent Practitioners}{60}{section.F.2}
\contentsline {section}{\numberline {F.3}Economists}{69}{section.F.3}
\contentsline {section}{\numberline {F.4}Mathematicians, Philosophers, Generalists}{75}{section.F.4}
\contentsline {section}{\numberline {F.5}Politicians}{76}{section.F.5}
\contentsline {section}{\numberline {F.6}Patent strategists}{85}{section.F.6}
\contentsline {section}{\numberline {F.7}Annotated Links}{86}{section.F.7}
\contentsline {section}{\numberline {F.8}Questions, Things To Do, How you can Help}{86}{section.F.8}
