\select@language {french}
\contentsline {chapter}{\numberline {1}Nous sommes pr\'{e}occup\'{e}s par les faits suivants}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Pour rem\'{e}dier \`{a} cela, nous proposons les mesures suivantes}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Signataires}{8}{chapter.3}
\contentsline {chapter}{\numberline {A}Brevets Logiciels Europ\'{e}ens: Quelques \'{E}chantillons}{11}{appendix.A}
\contentsline {chapter}{\numberline {B}Brevets Logiciels en Action}{16}{appendix.B}
\contentsline {section}{\numberline {B.1}Quelques Cas bien document\'{e}s}{16}{section.B.1}
\contentsline {section}{\numberline {B.2}Liens Annot\'{e}s}{21}{section.B.2}
\contentsline {chapter}{\numberline {C}Ensemble de tests pour la l\'{e}gislation sur les limites de la brevetabilit\'{e}}{22}{appendix.C}
\contentsline {section}{\numberline {C.1}Quelques exemples de brevets}{22}{section.C.1}
\contentsline {section}{\numberline {C.2}Questions auxquelles chaque brevet doit r\'{e}pondre}{22}{section.C.2}
\contentsline {section}{\numberline {C.3}Tableau comparatif}{23}{section.C.3}
\contentsline {section}{\numberline {C.4}Candidats \`{a} essayer: Propositions de loi de CEC/BSA, FFII/Eurolinux et d'autres}{23}{section.C.4}
\contentsline {chapter}{\numberline {D}Proposition(s) BSA/CCE et Contre-Proposition Bas\'{e}e sur la CBE}{24}{appendix.D}
\contentsline {section}{\numberline {D.1}Pr\'{e}ambule}{24}{section.D.1}
\contentsline {section}{\numberline {D.2}Les Articles}{31}{section.D.2}
\contentsline {chapter}{\numberline {E}MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal}{38}{appendix.E}
\contentsline {section}{\numberline {E.1}Some Criticisms of McCarthy's Paper}{38}{section.E.1}
\contentsline {section}{\numberline {E.2}Responses to McCarthy's Questions}{42}{section.E.2}
\contentsline {subsection}{\numberline {E.2.1}Is such a directive necessary or can we just leave matters to the Boards of Appeal and the national patent courts?}{42}{subsection.E.2.1}
\contentsline {subsubsection}{Can we just leave matters to the EPO's boards of a appeal and national patent courts?}{42}{section*.3}
\contentsline {subsubsection}{Is there a problem of divergent national rules which needs to be addressed by a directive, as CEC/BSA claim?}{43}{section*.4}
\contentsline {subsubsection}{Are the other proclaimed goals of the directive proposal valid?}{43}{section*.5}
\contentsline {subsubsection}{Are there any valid goals for which a directive might be necessary?}{44}{section*.6}
\contentsline {subsection}{\numberline {E.2.2}Will the directive achieve its ends, in particular without unwanted side effects?}{44}{subsection.E.2.2}
\contentsline {subsection}{\numberline {E.2.3}Does Art. 5 run counter to TRIPs in imposing artificial restrictions on the type of claims permissible in this field? Is it intended to be retroactive? If so, with what effects?}{44}{subsection.E.2.3}
\contentsline {subsection}{\numberline {E.2.4}Is the proposal legally watertight (legal certainty)?}{45}{subsection.E.2.4}
\contentsline {subsection}{\numberline {E.2.5}Is there any merit in following in the footsteps of the USA (patentability of business methods) or should patentability of business methods be clearly and specifically precluded?}{45}{subsection.E.2.5}
\contentsline {subsection}{\numberline {E.2.6}Should the system of (compulsory) licences be reviewed to prevent abuse of the patent system?}{46}{subsection.E.2.6}
\contentsline {subsection}{\numberline {E.2.7}Is the issue of trivial patents a problem? If so, how should it be addressed?}{46}{subsection.E.2.7}
\contentsline {subsection}{\numberline {E.2.8}What risk, if any, is posed to open-source software? If so, how is it that open-source and proprietary software seem to co-exist at present?}{46}{subsection.E.2.8}
\contentsline {subsection}{\numberline {E.2.9}Is it possible to argue that patents may stifle innovation, if so how?}{46}{subsection.E.2.9}
\contentsline {subsection}{\numberline {E.2.10}Is it possible to quantify in economic terms/employment the benefits/disbenefits of software patentability?}{46}{subsection.E.2.10}
\contentsline {subsection}{\numberline {E.2.11}What impact, if any, will action in this area, one way or another, have on SMEs?}{47}{subsection.E.2.11}
\contentsline {subsection}{\numberline {E.2.12}What is the impact of TRIPs?}{47}{subsection.E.2.12}
\contentsline {subsection}{\numberline {E.2.13}The EPC is not limited to EU countries. What are the implications of this in the event that the directive is adopted?}{47}{subsection.E.2.13}
\contentsline {subsection}{\numberline {E.2.14}Do recital 18 and Art. 6 of the proposal for a directive need to be reworded in order to allow decompilation of programs for the purposes of interoperability as is permitted under Directive 91/250?}{48}{subsection.E.2.14}
\contentsline {section}{\numberline {E.3}Unasked Questions}{48}{section.E.3}
\contentsline {chapter}{\numberline {F}Citations sur la Question de la Brevetabilit\'{e} des R\`{e}gles d'Organisation et de Calcul}{49}{appendix.F}
\contentsline {section}{\numberline {F.1}Informaticiens et Techniciens}{49}{section.F.1}
\contentsline {section}{\numberline {F.2}Experts en Droit, Juges}{58}{section.F.2}
\contentsline {section}{\numberline {F.3}\'{E}conomistes}{67}{section.F.3}
\contentsline {section}{\numberline {F.4}Math\'{e}maticiens, Savants de la Pens\'{e}e, Intellectuels}{72}{section.F.4}
\contentsline {section}{\numberline {F.5}Politiciens}{73}{section.F.5}
\contentsline {section}{\numberline {F.6}Strat\`{e}ges en Brevet}{82}{section.F.6}
\contentsline {section}{\numberline {F.7}Liens Annot\'{e}s}{83}{section.F.7}
\contentsline {section}{\numberline {F.8}Questions, Choses a faires, Comment vous pouvez aider}{83}{section.F.8}
