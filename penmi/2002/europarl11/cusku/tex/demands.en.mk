# -*- mode: makefile -*-

demands.en.lstex:
	lstex demands.en | sort -u > demands.en.lstex
demands.en.mk:	demands.en.lstex
	vcat /ul/prg/RC/texmake > demands.en.mk


demands.en.dvi:	demands.en.mk
	rm -f demands.en.lta
	if latex demands.en;then test -f demands.en.lta && latex demands.en;while tail -n 20 demands.en.log | grep -w references && latex demands.en;do eval;done;fi
	if test -r demands.en.idx;then makeindex demands.en && latex demands.en;fi

demands.en.pdf:	demands.en.ps
	if grep -w '\(CJK\|epsfig\)' demands.en.tex;then ps2pdf demands.en.ps;else rm -f demands.en.lta;if pdflatex demands.en;then test -f demands.en.lta && pdflatex demands.en;while tail -n 20 demands.en.log | grep -w references && pdflatex demands.en;do eval;done;fi;fi
	if test -r demands.en.idx;then makeindex demands.en && pdflatex demands.en;fi
demands.en.tty:	demands.en.dvi
	dvi2tty -q demands.en > demands.en.tty
demands.en.ps:	demands.en.dvi	
	dvips  demands.en
demands.en.001:	demands.en.dvi
	rm -f demands.en.[0-9][0-9][0-9]
	dvips -i -S 1  demands.en
demands.en.pbm:	demands.en.ps
	pstopbm demands.en.ps
demands.en.gif:	demands.en.ps
	pstogif demands.en.ps
demands.en.eps:	demands.en.dvi
	dvips -E -f demands.en > demands.en.eps
demands.en-01.g3n:	demands.en.ps
	ps2fax n demands.en.ps
demands.en-01.g3f:	demands.en.ps
	ps2fax f demands.en.ps
demands.en.ps.gz:	demands.en.ps
	gzip < demands.en.ps > demands.en.ps.gz
demands.en.ps.gz.uue:	demands.en.ps.gz
	uuencode demands.en.ps.gz demands.en.ps.gz > demands.en.ps.gz.uue
demands.en.faxsnd:	demands.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo demands.en-??.g3n`;source faxsnd main
demands.en_tex.ps:	
	cat demands.en.tex | splitlong | coco | m2ps > demands.en_tex.ps
demands.en_tex.ps.gz:	demands.en_tex.ps
	gzip < demands.en_tex.ps > demands.en_tex.ps.gz
demands.en-01.pgm:
	cat demands.en.ps | gs -q -sDEVICE=pgm -sOutputFile=demands.en-%02d.pgm -
demands.en/demands.en.html:	demands.en.dvi
	rm -fR demands.en;latex2html demands.en.tex
xview:	demands.en.dvi
	xdvi -s 8 demands.en &
tview:	demands.en.tty
	browse demands.en.tty 
gview:	demands.en.ps
	ghostview  demands.en.ps &
print:	demands.en.ps
	lpr -s -h demands.en.ps 
sprint:	demands.en.001
	for F in demands.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	demands.en.faxsnd
	faxsndjob view demands.en &
fsend:	demands.en.faxsnd
	faxsndjob jobs demands.en
viewgif:	demands.en.gif
	xv demands.en.gif &
viewpbm:	demands.en.pbm
	xv demands.en-??.pbm &
vieweps:	demands.en.eps
	ghostview demands.en.eps &	
clean:	demands.en.ps
	rm -f  demands.en-*.tex demands.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} demands.en-??.* demands.en_tex.* demands.en*~
demands.en.tgz:	clean
	set +f;LSFILES=`cat demands.en.ls???`;FILES=`ls demands.en.* $$LSFILES | sort -u`;tar czvf demands.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
