\contentsline {chapter}{\numberline {1}Folgendes bereitet uns Sorge}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Daher empfehlen wir folgende Ma{\ss }nahmen}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Unterzeichner}{8}{chapter.3}
\contentsline {chapter}{\numberline {A}Europ\"{a}ische Softwarepatente: Einige Musterexemplare}{11}{appendix.A}
\contentsline {chapter}{\numberline {B}Softwarepatente in Aktion}{18}{appendix.B}
\contentsline {section}{\numberline {B.1}Einige gut dokumentierte F\"{a}lle}{18}{section.B.1}
\contentsline {section}{\numberline {B.2}Kommentierte Verweise}{25}{section.B.2}
\contentsline {chapter}{\numberline {C}Testsuite f\"{u}r die Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit}{26}{appendix.C}
\contentsline {section}{\numberline {C.1}Einige Beispielpatente}{26}{section.C.1}
\contentsline {section}{\numberline {C.2}Fragen, die f\"{u}r jedes Patent zu beantworten sind}{26}{section.C.2}
\contentsline {section}{\numberline {C.3}Vergleichstabelle}{27}{section.C.3}
\contentsline {section}{\numberline {C.4}Zu pr\"{u}fende Kandidaten: Gesetzesvorschl\"{a}ge von EUK/BSA, FFII/Eurolinux u.a.}{27}{section.C.4}
\contentsline {chapter}{\numberline {D}Entw\"{u}rfe von BSA/EUK und EP\"{U}-basierter Gegenentwurf}{28}{appendix.D}
\contentsline {section}{\numberline {D.1}Pr\"{a}ambel}{28}{section.D.1}
\contentsline {section}{\numberline {D.2}Die Artikel}{36}{section.D.2}
\contentsline {chapter}{\numberline {E}MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal}{43}{appendix.E}
\contentsline {section}{\numberline {E.1}Some Criticisms of McCarthy's Paper}{43}{section.E.1}
\contentsline {section}{\numberline {E.2}Responses to McCarthy's Questions}{47}{section.E.2}
\contentsline {subsection}{\numberline {E.2.1}Is such a directive necessary or can we just leave matters to the Boards of Appeal and the national patent courts?}{47}{subsection.E.2.1}
\contentsline {subsubsection}{Can we just leave matters to the EPO's boards of a appeal and national patent courts?}{47}{section*.3}
\contentsline {subsubsection}{Is there a problem of divergent national rules which needs to be addressed by a directive, as CEC/BSA claim?}{48}{section*.4}
\contentsline {subsubsection}{Are the other proclaimed goals of the directive proposal valid?}{48}{section*.5}
\contentsline {subsubsection}{Are there any valid goals for which a directive might be necessary?}{49}{section*.6}
\contentsline {subsection}{\numberline {E.2.2}Will the directive achieve its ends, in particular without unwanted side effects?}{49}{subsection.E.2.2}
\contentsline {subsection}{\numberline {E.2.3}Does Art. 5 run counter to TRIPs in imposing artificial restrictions on the type of claims permissible in this field? Is it intended to be retroactive? If so, with what effects?}{50}{subsection.E.2.3}
\contentsline {subsection}{\numberline {E.2.4}Is the proposal legally watertight (legal certainty)?}{50}{subsection.E.2.4}
\contentsline {subsection}{\numberline {E.2.5}Is there any merit in following in the footsteps of the USA (patentability of business methods) or should patentability of business methods be clearly and specifically precluded?}{50}{subsection.E.2.5}
\contentsline {subsection}{\numberline {E.2.6}Should the system of (compulsory) licences be reviewed to prevent abuse of the patent system?}{51}{subsection.E.2.6}
\contentsline {subsection}{\numberline {E.2.7}Is the issue of trivial patents a problem? If so, how should it be addressed?}{51}{subsection.E.2.7}
\contentsline {subsection}{\numberline {E.2.8}What risk, if any, is posed to open-source software? If so, how is it that open-source and proprietary software seem to co-exist at present?}{51}{subsection.E.2.8}
\contentsline {subsection}{\numberline {E.2.9}Is it possible to argue that patents may stifle innovation, if so how?}{51}{subsection.E.2.9}
\contentsline {subsection}{\numberline {E.2.10}Is it possible to quantify in economic terms/employment the benefits/disbenefits of software patentability?}{52}{subsection.E.2.10}
\contentsline {subsection}{\numberline {E.2.11}What impact, if any, will action in this area, one way or another, have on SMEs?}{52}{subsection.E.2.11}
\contentsline {subsection}{\numberline {E.2.12}What is the impact of TRIPs?}{52}{subsection.E.2.12}
\contentsline {subsection}{\numberline {E.2.13}The EPC is not limited to EU countries. What are the implications of this in the event that the directive is adopted?}{52}{subsection.E.2.13}
\contentsline {subsection}{\numberline {E.2.14}Do recital 18 and Art. 6 of the proposal for a directive need to be reworded in order to allow decompilation of programs for the purposes of interoperability as is permitted under Directive 91/250?}{53}{subsection.E.2.14}
\contentsline {section}{\numberline {E.3}Unasked Questions}{53}{section.E.3}
\contentsline {chapter}{\numberline {F}Zitate zur Frage der Patentierbarkeit von computer-implementierten Organisations- und Rechenregeln}{54}{appendix.F}
\contentsline {section}{\numberline {F.1}Softwerker und Techniker}{54}{section.F.1}
\contentsline {section}{\numberline {F.2}Rechtsgelehrte, Richter}{63}{section.F.2}
\contentsline {section}{\numberline {F.3}Wirtschaftswissenschaftler}{72}{section.F.3}
\contentsline {section}{\numberline {F.4}Mathematiker, Geisteswissenschaftler, Generalisten}{78}{section.F.4}
\contentsline {section}{\numberline {F.5}Politiker}{79}{section.F.5}
\contentsline {section}{\numberline {F.6}Patentstrategen}{89}{section.F.6}
\contentsline {section}{\numberline {F.7}Kommentierte Verweise}{90}{section.F.7}
\contentsline {section}{\numberline {F.8}Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}{90}{section.F.8}
