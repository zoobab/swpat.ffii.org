<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hartmut Pilch's Speech at the Europarl Hearing of 2002-11-07

#descr: Hartmut was invited to speak at a hearing conducted by the European
Parliament's Committee for Legal Affairs and the Internal Market
(JURI) about the European Commission's software patentability
directive proposal.  He delivered the speech in an improvised manner
based on presentation slides.  The following written version was
prepared later for publication on the Parliament's website.

#LWe: Ladies and Gentlemen

#Iot: It is a great pleasure and honor for me to be able to speak to you
here in the name of the European Software Industry about the dangers
of software patentability, as they are perceived by those software
companies and computer professionals who have been confronted with the
subject.

#YxW: You may already know that more than 400 software companies and 125000
individuals support the Eurolinux Petition for a Software Patent Free
Europe.  You can find the text of this petition in appendix X of the
documentation on your table.  This petition is addressed to the
European Parliament.  Let me quote:

#Iod: I am concerned by current plans to legalise software patents in
Europe, considering their damaging effect on innovation and
competition.

#Ieu: I am concerned by the current track record of abuses from the European
Patent Office, especially by their tendency to abuse their judicial
power to extend the scope of patentability.

#Ieg: I urge decisionmakers at all levels in Europe to enforce the Law,
which clearly prohibits patenting pure computer programs, instead of
changing it.

#Atn: Among the 125000 individuals who support this appeal are thousands of
top executives of all kinds of small, medium and large software and
hardware companies and thousands of software developpers from
companies such as SAP, Siemens and IBM.

#Tns: This petition is considered by web poll experts to be the largest of
all online petitions so far.  A manager of a large specialised
petition website recently asked me:  Why is your petition so
successful?  How can such an obscure subject attract so many people?

#TcW: The explanation is quite simple:  Software Patents run counter to the
basic ethical consensus of all programmers.  Programmers are generally
happy with software copyright.  Just as people who write music or
textbooks are happy with copyright.

#Mem: Ms McCarthy, your position paper on the software patentability
directive certainly contains some clever rhetorical devices.  E.g.
when you say:

#Aft: A vociferous section of public opinion believes that there should be
no patentability of software at all.  ... It seems to many that the
opponents of software patents have not been able to show that software
with a technical effect should cease to be patentable.

#yie: you are not saying but implying that the Eurolinux position is
supported only by a vociferous few, while %(q:many) think like the
European Patent Office.  Also, you shift the burden of proof onto us
by confusing law with caselaw.  You have then again restricted our
opportunity of shouldering this burden by arranging a hearing upon
very short notice with very limited speaking time and an agenda of
misleading questions, after a long interval in which you kept stricly
silent and ignored all criticism.

#IeW: In a way you have found a clever method of political writing and
maneuvering.  When your agenda runs counter to the ethical consensus
of all software creators, you may have to resort to such clever
methods.

#BWt: But what would you do if we had a patent on this method?

#Ecr: E.g. a claim, reading:

#srs: system and method for minoritising a majority of constituents,
characterised by

#cat: characterising the majority by attributes which normally apply to
minorities, such as %(q:vociferous), and attributing one's one opinion
to an undefined multitude of actors

#pup: placing a high burden of proof upon one's opponent

#csn: chanelling all discussions into forms where the opponent does not have
an opportunity to shoulder the burden

#Mer: Ms McCarthy, you have certainly been infringing on this claim.

#Oad: OK, this minoritisation patent claim may be invalid due to lack of
novelty.

#Aec: Also, it might be invalid due to lack of technical character, whatever
that may mean.

#Bms: But in the eyes of a programmer, the typical software patent claims
are even more abstract and simple than this %(q:minoritisation
patent).  The typical software patents is just as untechnical as this
%(q:minoritisation patent), i.e. as unrelated to the art of harnessing
the laws of nature.

#Yof: You are excluded from a more or less broad range of activities for 20
years.

#Ior: Imagine our %(q:minoritisation patent) was valid, alongside with
thousands of other petty patents on political strategems.  What would
you do?  Join the political party with the largest patent portfolio
and the strongest patent department?  Give up your profession and
become a patent salesman yourself?  Or fight for your general freedom
of action, for your freedom of speech and for your property in your
copyrighted texts? Argue that patents on rhetorical devices and
political schemes are not helpful in promoting progress?  Conducting
economic studies to support your arguments?

#TWW: That is exactly what is happening in the area of software patents. 
Everybody who knows what software is knows that software patents are
just a silly idea.  Yet the former silly idea has now become a silly
reality, and recently there has been an explosion of studies of this
reality.  All these studies say in unisono that software patents are
harmful.  Even those studies written by the patent lobby (e.g. Mr.
Hart and friends for the European Commission) cannot avoid this
conclusion.  Please take a look at Appendix X, where you find a list
of brief descriptions of some of these studies.

#Tsa: The European Patent Office has already granted more than 30000
software patents against the letter and spirit of the written law.  We
have presented some of them in Appendix X as a %(q:Horror Gallery of
European Software Patents).  However all 30000 are horror patents.  I
don't know of any exceptions.  They either offend the ethical
consensus of programmers by blocking software development and software
interoperability, or they are useless pieces of paper used by large
companies in order to artificially boost their stock market value or
play games of tax evasion.  In no case has any european promoter of
software patents ever been able to cite a single example of what he
would call a %(q:good software patent).  In a way, even the promoters
of software patents are not interested in protecting intellectual
property.  They are only interested in being able to go on playing the
game, in which they have become proficient.  And that is a degenerate
formalistic game which destroys more intellectual property than it
creates.

#LWe2: Ladies and Gentlemen, the European Software Industry is asking that
its arguments be weighed on an equal footing with the arguments of the
patent industry.  I may also note that UNICE and EICTA, who are
represented here, are opting for the patent industry without any
legitimation by their member base.  In fact we have some of their
members sitting here ready to tell you about how the member base was
cheated, if you are interested in hearing.  Probably however it will
suffice if you listen to the arguments of the patent lawyers who are
speaking for UNICE and EICTA and ask whether they are protecting
real-life intellectual property or playing a formalistic game.  It may
also be telling to know that some very large industry representation
bodies have taken positions which are diametrically opposed to those
of UNICE and EICTA.  I may mention the German Chamber of Commerce, the
Dutch IT Industry Association, the French, Danish and Spanish
Associations of IT professionals, the German Monopoly Commission, the
European Social Committee.  Others have taken neutral positions.

#Sdw: Some large associations and renowned companies have recently signed a
detailed Call for Action, which you find in Appendix X.  We ask you to
reject the directive and work out a specification of deliverables and
a series of conformance tests for a new directive, and for its
implementation by the member states.  We have also provided a
reference implementation, which you find in Appendix Y.  That is a
counter-proposal to the European Commission's proposal, provided in
the form of a tabular comparison.  This counter-proposal would be a
sane, legally correct and macro-economically beneficial alternative
version, and by reading the tabular comparison, you can find out the
many inconsistencies and minoritisation patent infringements which are
present in the European Commission's proposal.

#Ikj: If you have any questions, feel free to ask us now, to contact us
later, to come to our de-briefing session in room XXXX at 19:00 and of
course to come to the hearing on 2002-11-26, conducted by ...., where
there is more time for the speakers to discuss the real issues.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl02Bcus ;
# txtlang: en ;
# multlin: t ;
# End: ;

