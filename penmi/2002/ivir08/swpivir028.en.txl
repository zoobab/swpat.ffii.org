<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Swpat Conference Amsterdam 2002-08-30..1 (Columbanus Symposium)

#descr: Hartmut Pilch is attending a conference hosted by Prof. Bernt
Hugenholz and Reinier Bakels from University of Amsterdam about the
typology of innovations in the software and business method area and
the implications of various rules for defining what is patentable,
including the European Commission's recent proposal for a directive
and hopefully also our widely supported counter-proposal.

#Win: What we would like the Conference to Focus on

#Whf: We particularly hope to discuss the FFII/Eurolinux %(cp:EU directive
counter-proposal), which has received %(ca:broad support from
politicians, vendors and associations).  We think that the comparison
of the CEC/BSA proposal with the FFII/Eurolinux counter-proposal shows
most clearly what is at stake.  In order to get a more intuitive grasp
of what we are talking about, it would moreover be helpful to look at
a few typical %(ep:EPO-granted software and business method patents),
try to classify them and %(sm:look at the effects that different
legislative proposals could have on them).

#Cio: Currently the conference page points to many PDF and MSWord documents
which are dead ends of discussions in many ways.  MSWord documents
can't even be displayed safely on non-microsoft systems, and PDF
documents do not have the linking characteristics that make the WWW an
exciting place.  Moreover, many of the documents have a deeply
political background and little if any scientific value.  Some of the
authors regularly shy away from public discussion and derive their
authority from political backing rather than from an open process of
peer review.  Some of these studies are part of the problem rather
than contributions to its solution.  Below you find our account of the
problems and solutions as well as critical reviews pages of various
documents from the Columbanous background pages (which may have
contributed either problems or solutions to the swpat debate).

#wor: website of the organisers

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpivir028 ;
# txtlang: en ;
# multlin: t ;
# End: ;

