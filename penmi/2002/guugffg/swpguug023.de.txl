<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: GUUG Frühjahrsfachgespräch Bochum 2002-03-01 14:45-16:30

#descr: Es wird ein Symposium über Besitzrechte an Software (Urheberrecht,
Patentrecht und Alternativen) stattfinden.  Ziel dabei ist es,
weitgehend unabhängig von rechtssystematischen Überlegungen die
Bedürfnisse der Informatik zu ermitteln.  Gesprächspartner sind u.a.
Andreas Stöckigt (Vize-Präsident der GI) und Xuân Baldauf (FFII). 
Zwei der Gesprächspartner werden zuvor Gelegenheit haben, ihre
Position ausführlich darzulegen.  Die anschließende Podiumsdiskussion
moderiert Dieter Längle, Kassenwart der GUUG.

#Vnt: Vortragsankündigung von Hartmut Pilch

#HPu: Hartmut Pilch: zur Person

#ViL: Vorbereitende Lektüre

#XWe: Xuân Baldauf, der am Schluss den Vortrag hielt, verfolgte ein leicht
anderes Konzept.  Sein Vortrag ähnelte %(lx:dem am Linuxtag 2001
gehaltenen).

#XFq: Die Eurolinux-Kampagne für ein softwarepatentfreies Europa hat
inzwischen über 100000 Unterschriften und offizielle Unterstützung
einiger 100 Unternehmen gesammelt.  %(q:Wenn alle Leute das gleiche
sagen, ist es Zeit eine Gegenposition zu beziehen), mag sich da manch
ein Informatiker sagen.  Warum sollen denn nicht Informatiker genauso
wie Ingenieure die Chance haben, ihre geistigen Leistungen in
stattliche Lizenzeinnahmen umzusetzen?  Ist es nicht wunderbar, dass
die Herren R. S. und A. reich wurden?

#Dne: Diese Frage gewissenhaft zu beantworten, ist nicht ganz einfach. 
Einerseits erfordert die Diskussion allerhand relativ komplizierte
%(fs:Fragen) und Antworten, für die nicht unbedingt jeder
Diskussionsteilnehmer die nötige Geduld aufbringt.  Andererseits führt
jede leichtsinnige Äußerung, insbesondere von Seiten einflussreicher
Personen, besonders leicht in einen Teufelspakt.

#Guv: Gerade für einen Vertreter der Informatik ist die Sache nicht ganz
einfach.  Es wäre für ihn unverantwortlich, einem Zeitgeist (Skylla)
blind zu folgen, der womöglich aus einer unreflektierter Abneigung
gegen das %(q:geistige Eigentum) heraus die Informatikwelt einer
wirtschaftlichen Stütze beraubt.  Ebenso unverantwortlich wäre es, der
Patentbranche (Charybdis) die Schlüssel zur Informatikwelt zu
übergeben und damit, wie das chinesische Idiom sagt, %(q:den Wolf in
die Stube zu holen).

#Dgs: Das Präsidium der Gesellschaft für Informatik hat sich offenbar
bereits für Charybdis entschieden.  Ich möchte bei dieser Diskussion
versuchen, mein Gegenüber zu überzeugen, dass die Eurolinux-Allianz
nicht Skylla ist, und dass es im Interesse der Informatik liegt, mit
uns zusammen ein urheberrechtsbasiertes System des Software-Besitzes
anzustreben, bei dem eventuell auch an eine sanfte kurzzeitige
Inbesitznahme von Algorithmen zu denken ist.

#JiW: Jahrgang 1963, MA Sinologie, seit 15 Jahren einerseits häufig als
Chinesisch- und Japanisch-Dolmetscher im Europäischen Patentamt und
Bundespatentgericht beschäftigt, andererseits viel Freizeit und auch
manche bezahlte Arbeitszeit mit Programmieren, insbesondere auf
Unix-Systemen, verbringend.  Zeitweilig bei Siemens-Nixdorf und SuSE
in dieser Funktion beschäftigt.  Seit 3 Jahren in das %(sp:Studium der
Softwarepatente-Problematik) vertieft, Mitgründer des %{FFII} und der
%{ELA}, Mitorganisator der der %{EP}.

#Vmh: Vorträge und Podiumsdiskussion zum Thema %(q:Software-Besitzrechte)
beim Frühjahrs-Fachgespräch der GUUG in Bochum

#EWW: Ein Vortragskonzept von Andreas Stöckigt, Vizepräsident der GI e.V.,
ist bereits eingetroffen.  S. auch das %(pg:Gesamtprogramm).

#EWD: Ein Katalog von Fragen, der als Grundlage der Diskussion dienen könnte

#Dse: Der Fragenkatalog, den der Bundestagsausschuss %(q:Neue Medien) einer
Gruppe von Experten stellte, und ein Protokoll der Sitzung.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpguug023 ;
# txtlang: de ;
# multlin: t ;
# End: ;

