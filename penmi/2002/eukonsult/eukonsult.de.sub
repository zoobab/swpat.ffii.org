\begin{subdocument}{eukonsult02}{Europ\"{a}ische Konsultation 2002: Software-Innovation und die Grenzen der Patentierbarkeit}{http://swpat.ffii.org/treffen/2002/eukonsult/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Um 1986 erkl\"{a}rten Patentrechtslehrb\"{u}cher und -kommentare einhellig, das europ\"{a}ische Patentrecht biete keinerlei Spielraum, um Forderungen aus der Softwarebranche nach Patentschutz f\"{u}r ihre erfinderischen Leistungen entgegenzukommen.  Vielfach wurde beklagt, das geltende Urheberrecht biete nicht gen\"{u}gend Schutz gegen Nachahmung, aber die T\"{u}r zum Patentschutz f\"{u}r Software schien fest verschlossen.  Mit den Pr\"{u}fungsrichtlinien des Europ\"{a}ischen Patentamtes (EPA) von 1985 wurde indes unbemerkt ein Spalt f\"{u}r ``Programme mit technischem Effekt'' ge\"{o}ffnet und eine Lockerung des Technikbegriffs vorbereitet, die nach herrschender Rechtsauffassung nur einer Aufgabe jeglicher \"{u}bersehbarer Begrenzung der Patentierbarkeit gleichkommen konnte.  Ein Jahr sp\"{a}ter, 1986, wurde der Spalt durch zwei Entscheidungen der Technischen Beschwerdekammer des EPA zu einer sichtbaren Bresche verbreitert.  Seitdem hat das EPA mehr als 30000 Patente f\"{u}r (un)technische Lehren erteilt, die bis dahin als Organisations- und Rechenregeln oder als Programme f\"{u}r Datenverarbeitungsanlagen (als solche) abgelehnt worden w\"{a}ren.  Richter und Rechtsgelehrte haben inzwischen allerlei (Nicht)-Begrenzungsregeln vorgeschlagen, um diese Entwicklung auf eine neue rechtsdogmatische Grundlage zu stellen.   Ein lange geplantes Gesetzes\"{a}nderungsprojekt scheiterte jedoch im November 2000, als die europ\"{a}ischen Regierungen angesichts massiver Proteste von Software-Entwicklern und -firmen sich entschieden, zun\"{a}chst den Patentierbarkeitsausschluss f\"{u}r Programme f\"{u}r Datenverarbeitungsanlagen im Gesetz zu belassen und auf europ\"{a}ischer Ebene eine Konsultation in Gang zu setzen.  Ziel dieses Seminars ist es, diese Konsultation zu vertiefen.  Es gilt zu erforschen, wie sich der Raum der Innovationen entlang der Achsen materiell vs immateriell, Naturgesetze vs Rechenregeln, konkret vs abstrakt, L\"{o}sung vs Problem, trivial vs schwierig, kausal vs funktional usw aufteilt, und was verschiedene m\"{o}gliche Abgrenzungsregeln in diesem Raum bewirken.}
\mbox{\pdfimage {/ul/sig/srv/res/www/ffii/swpat/img/ffii/technik_welten.png}}

\begin{sect}{kiam}{Wann?}
2001-05 ?
\end{sect}

\begin{sect}{kiom}{Tagungsprogramm}
mehrt\"{a}gig

vgl. Seminar Linuxtag Stuttgart 2001-07-05: 15 Jahre Softwarepatentierung am Europ\"{a}ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft\footnote{http://swpat.ffii.org/treffen/2001/linuxtag/index.de.html}
\end{sect}

\begin{sect}{kie}{Wo?}
(aut KLN (ml Trier (fr Trèves)) (ml Frankfurt (fr Francfort)) (ml Karlsruhe) (ml Luxemburg (fr Luxembourg)) (ml Straßburg Strasburg Strasbourg) Berlin)
\end{sect}

\begin{sect}{vera}{Veranstalter}
\begin{itemize}
\item
ER

\item
HB

\item
BI

\item
SI

\item
FE
\end{itemize}
\end{sect}

\begin{sect}{fina}{Finanzierung}
Gesamtbudget 15000 EUR.  Jede der Organisationen tr\"{a}gt einen Teil bei.  Eine eventuell zu erhebende Tagungsgeb\"{u}hr soll TGB nicht \"{u}berschreiten.
\end{sect}

\begin{sect}{redn}{Eingeladene Redner}
\begin{description}
\item[Europ\"{a}ische Wissenschaftler:]\ Recht, Wirtschaft, Informatik, Epistemologie
lange Liste
\item[Aus USA:]\ 5 Personen
\item[Aus Ostasien: 2 Personen:]\ KONNO Hiroshi: Das Karmarkarsche Patent und Software -- Ist Mathematik patentierbar?\footnote{http://swpat.ffii.org/papiere/konno95/index.ja.html}
\item[Teilnehmer der EUK-Konsultation\footnote{http://swpat.ffii.org/papiere/eukonsult00/index.de.html}:]\ 
\item[Richter, Patentamtsfunktion\"{a}re:]\ 
\item[Politiker:]\ 
\end{description}
\end{sect}

\begin{sect}{tagb}{Tagungsband}
Gemeinsame Arbeit an Tagungsband unter Einbeziehung bisheriger Dokumentationen und zu erwartender Referate.

((ML Ske Diverse Verlage haben Interesse angemeldet. Various publishing houses have signalled interest. Plusieurs Éditeurs ont signallé de l'intérêt.))

Vorarbeiten
\begin{itemize}
\item
Seminar Linuxtag Stuttgart 2001-07-05: 15 Jahre Softwarepatentierung am Europ\"{a}ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft\footnote{http://swpat.ffii.org/treffen/2001/linuxtag/index.de.html}

\item
Softwarepatente mit ausgleichender Regelung: eine Kostenermittlung\footnote{http://swpat.ffii.org/analyse/pleji/index.de.html}

\item
Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{http://swpat.ffii.org/analyse/erfindung/index.de.html}
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

