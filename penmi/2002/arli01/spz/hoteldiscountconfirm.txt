From: emailconfirm@180096hotel.com
Subject: Hotel Confirmation for the JURYS NORMANDY INN - 005429254                                                                                                                                    
To: phm@ffii.org
Date: Sat, 26 Jan 2002 14:25:17 -0600
Reply-to: emailconfirm@mail.180096hotel.com

                                                                                                                                                                                                    
                                                                                                                                                                                                      
 2002-01-26      C O N F I R M A T I O N / R E C E I P T                                                                                                                                              
                                                                                                                                                                                                      
                      >>>> RESERVATION PREPAID <<<<                                                                                                                                                   
                                                                                                                                                                                                      
                    $50 CANCELLATION/CHANGE FEE APPLIES                                                                                                                                               
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   HRN Confirmation Number:                                                                                                                                                                           
   ________________________                                                                                                                                                                           
       Booking Number: 005429254                                                                                                                                                                      
                                                                                                                                                                                                      
   Customer Info:                                                                                                                                                                                     
   ______________                                                                                                                                                                                     
       Guest Name: HARTMUT PILCH                                                                                                                                                                      
       Address:    BLUTENBURGSTR 17                                                                                                                                                                   
                   MUNICH,   80636                                                                                                                                                                    
                   GERMANY                                                                                                                                                                            
                                                                                                                                                                                                      
   Property Info:                                                                                                                                                                                     
   ______________                                                                                                                                                                                     
       JURYS NORMANDY INN                                                                                                                                                                             
       2118 WYOMING AVE. N.W.                                                                                                                                                                         
       WASHINGTON, DC 20008                                                                                                                                                                           
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   Reservation Info:                                                                                                                                                                                  
   _________________                                                                                                                                                                                  
       Check In:        1/27/02                                                                                                                                                                       
       Check Out:       1/30/02                                                                                                                                                                       
       Check In Time:  3:00 PM                                                                                                                                                                        
       Check Out Time: 12:00 PM                                                                                                                                                                       
       Number of Rooms:       1                                                                                                                                                                       
       Number of Nights:      3                                                                                                                                                                       
       Number of Adults:      1                                                                                                                                                                       
       Number of Children:    0                                                                                                                                                                       
       Smoking Room:          N                                                                                                                                                                       
       Room Description: Standard 1-3                                                                                                                                                                 
                                                                                                                                                                                                      
   Reservation/Price Detail:                                                                                                                                                                          
   _________________________                                                                                                                                                                          
                           Rate    Taxes/Fees         Total                                                                                                                                           
        1/27/02           65.95         11.05         77.00                                                                                                                                           
        1/28/02           65.95         11.05         77.00                                                                                                                                           
        1/29/02           65.95         11.05         77.00                                                                                                                                           
                                               ____________                                                                                                                                           
       TOTAL                                         231.00  USD                                                                                                                                      
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   General Info:                                                                                                                                                                                      
   _____________                                                                                                                                                                                      
       Your reservation is PREPAID to Hotel Reservations Network/CondoSavers.Com,                                                                                                                     
       Dallas, TX and is guaranteed for late arrival.  Your reservation is part                                                                                                                       
       of the Hotel Reservations Network block of rooms at the hotel.                                                                                                                                 
       Information on individual reservations will be available at the                                                                                                                                
       JURYS NORMANDY INN within two weeks prior to arrival.  Please                                                                                                                                  
       refer to the Hotel Reservations Booking/Confirmation Number above if you                                                                                                                       
       contact HRN for any reason.  For the fastest service on any questions                                                                                                                          
       regarding your credit card bill, please visit the Customer Service section                                                                                                                     
       on our web site listed below.  Thank You for using Hotel Reservations                                                                                                                          
       Network/CondoSavers.com!                                                                                                                                                                       
                                                                                                                                                                                                      
           HRN WEB SITE: http://www.hoteldiscounts.com                                                                                                                                                
           CUSTOMER SERVICE: http://www.hoteldiscounts.com/customerservice/                                                                                                                           
           CUSTOMER SERVICE EMAIL: mailto:customer@hoteldiscounts.com                                                                                                                                 
           Domestic: (800) 394-1454     International: (214) 369-1264                                                                                                                                 
                                                                                                                                                                                                      
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   Change/Cancel Policy:                                                                                                                                                                              
   _____________________                                                                                                                                                                              
       To ensure proper credit:  If you wish to change your reservation please                                                                                                                        
       visit www.hoteldiscounts.com and click on the customer                                                                                                                                         
       service icon or call Hotel Reservations Network (HRN) at the number                                                                                                                            
       above.  Call HRN by 12:00 PM (CST), at least 24 HOURS prior to                                                                                                                                 
       arrival or you may be charged for one nights stay. You must obtain a                                                                                                                           
       cancellation or change number via email (customer@hoteldiscounts.com) or                                                                                                                       
       by telephone from an HRN representative.                                                                                                                                                       
                                                                                                                                                                                                      
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   Disclaimer:                                                                                                                                                                                        
   ____________                                                                                                                                                                                       
       Rate quoted is the HRN Customer Rate which includes access fees. The                                                                                                                           
       total charge above includes all property room charges and taxes, and HRN                                                                                                                       
       fees for access and booking. Smoking and bedding preferences are not                                                                                                                           
       guaranteed. Any incidental charges such as phone calls and room service                                                                                                                        
       will be handled directly between you and the property.                                                                                                                                         
                                                                                                                                                                                                      
       HRN is not acting as agent for Hotels, Car Rental Companies, Tour                                                                                                                              
       Companies, and other travel related entities. HRN disclaims liability for                                                                                                                      
       any actions or omissions of these entities, or by HRN.                                                                                                                                         
                                                                                                                                                                                                      
       Find out what's going on in your city: www.ticketmaster.com                                                                                                                                    
                                                                                                                                                                                                      
                                                                                                                                                                                                      
   City Info:                                                                                                                                                                                         
   __________                                                                                                                                                                                         
       Want information on Washington?                                                                                                                                                                
       The CitySearch city guides provide the best information for visitors to                                                                                                                        
       the city.  Our up-to-date information includes arts and entertainment                                                                                                                          
       events, restaurants, business services and more.                                                                                                                                               
       All you need to plan your leisure or business trip is at                                                                                                                                       
       www.citysearch.com.  Want to buy              tickets for arts &                                                                                                                               
       entertainment events while visiting the city?  Find out what is playing                                                                                                                        
       and purchase online at www.ticketmaster.com.  For information on Car                                                                                                                           
       Rentals visit www.travelnow.com/cars/home.html?cid=3127                                                                                                                                        
                                                                                                                                                                                                      
   Driving Directions:                                                                                                                                                                                
   ___________________                                                                                                                                                                                
   From Reagan National Airport:<br>                                                                                                                                                                  
   Start out going South on ABINGDON DR towards VA-233 by turning left.<br>                                                                                                                           
   Turn SLIGHT LEFT onto VA-233.<br>                                                                                                                                                                  
   Turn SLIGHT RIGHT.<br>                                                                                                                                                                             
   Turn SLIGHT RIGHT onto AIRPORT EXIT.<br>                                                                                                                                                           
   Turn SLIGHT RIGHT onto TERMINALS.<br>                                                                                                                                                              
   Take the ramp towards WASHINGTON(I-395).<br>                                                                                                                                                       
   Merge onto GEORGE WASHINGTON MEMORIAL PKWY/GW PKWY.<br>                                                                                                                                            
   Take the I-395 NORTH/US-1 NORTH exit towards WASHINGTON.<br>                                                                                                                                       
   Merge onto I-395 N/US-1 N.<br>                                                                                                                                                                     
   Take the US-1 NORTH exit on the left.<br>                                                                                                                                                          
   Merge onto US-1 N.<br>                                                                                                                                                                             
   US-1 N becomes US-1 N/14TH ST SW.<br>                                                                                                                                                              
   Stay straight to go onto 14TH ST NW.<br>                                                                                                                                                           
   Turn LEFT onto K ST NW/US-29.<br>                                                                                                                                                                  
   Turn RIGHT onto K ST NW.<br>                                                                                                                                                                       
   Turn RIGHT onto CONNECTICUT AVE NW.<br>                                                                                                                                                            
   Turn LEFT onto WYOMING AVE NW                                                                                                                                                                      

