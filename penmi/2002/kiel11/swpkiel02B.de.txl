<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Mediatage Kiel 2002-11-21

#descr: Eine Diskussionsveranstaltung auf dem Multimedia-Campus Kiel über die
anstehenden Gesetzgebungspläne zur Logikpatentierung, veranstaltet vom
Technologietransferzentrum Schleswig-Holstein (TTZSH.de).  André
Rebentisch hielt für den FFII einen Vortrag.  Einen weiteren Vortrag
hielt Patentanwalt Dr. Dibbert, Syndikusanwalt der M+N Informatik AG. 
 Es bestand weitgehende Einigkeit darüber, dass Patente den
Fortschritt im Bereich der Software eher behindern als fördern und
dass der Richtlinienentwurf der Europäischen Komission mehr Fragen
aufwirft als beantwortet.

#1tW: Andrés Bericht aus Kiel

#lmc: Ich bin gerade aus Kiel zurück gekommen. Bevor ich schlafen gehe
folgende Informationen über die Veranstaltung: Wider Erwarten war die
Veranstaltung nicht kontrovers. Ca. 25 Teilnehmer, recht
aufgeschlossen. Die ausrichtenden und teilnehmenden Leute waren sehr
sympathisch wie auch die Stadt mit ihrer herrlichen, freien Seeeluft.
Da fühlte ich mich als Küstenbewohner ganz heimisch.

#Sko: Herr Syndikusanwalt Dr. Dibbert, eigentlich als Konkurrent vorgesehen,
hat in seinem Vortrag sehr detailliert dargelegt, warum
Softwarepatentierung aus rechtlicher Sicht problematisch ist und
welche Verdrehungen stattfinden.  Gleich am Anfang erklärte er, er
wolle sich nicht die Position der Patentanwälte zu eigen machen, das
sei auch die Position seines Unternehmens. Einige Mitarbeiter seines
Unternehmens M+N Informatik AG seien Mitglieder im FFII. Er verwies
auf die Unterschiede in der nationalen und europäischen
Patentauslegung, auf die Doppelbödigkeit des Begriffes
%(q:computerimplementierte Erfindung) und auf die kontroverse
Auslegung des Art 52 EPÜ.  Am Beispiel der %(a1:Klage von Amazon gegen
Barnes & Nobles) wies er auf die die unerwünschten juristischen und
volkswirtschaftlichen Folgen solcher Patente hin. Insgesamt ein sehr
elaborierter und stark-juristischer Vortrag.

#eWn: Im Vorfeld hatte man mir gesagt: Auch wenn der Herr Dibbert schon eher
neutral das Patentproblem darstellt, müssen Sie mit Widerstand von
einem Kieler Patentanwalt rechnen, der an der Veranstaltung teilnimmt.
 %(jt:Herr Dipl.-Ing. Dipl.-Oek. Dr. jur. RA PA Jan G. Tönnies) nahm
auch tatsächlich sehr rege an der Diskussion teil.  Aber wider
Erwarten verschäfte die von mir dargelegten Positionen noch und übte
harsche Kritik an den Verbänden der Patentrechtler.  Tönnies gab nicht
dem EPA oder den Beamten in den Institutionen sondern vielmehr den
Verbänden die Schuld an den Fehlentwicklungen.  Diese Verbände hätten
einen überwältigenden Einfluss auf die Rechtsfortbildung geübt. 
Diesbezüglich seien auch die FFII-Seiten zu korrigieren, auf denen er
ansonsten in vieler Hinsicht fündig geworden sei. Tönnies erwähnte
u.a. auch Gravenreuth sei in Kiel abgewehrt worden, im Urteil sei das
Wort %(q:Raubrittertum) gefallen.

#Wvt: Desweiteren vertrat Tönnies die allgemein dort akzeptierte Ansicht,
die Software solle urheberrechtlich geschützt werden. Das ist ja auch
unsere Position.  Wobei er für eine Reduzierung auf nur vier Jahre
Urheberrechtsschutz plädierte. Den FS/OSS Projekten empfiehlt er die
präventive Anmeldung von Patenten. Der %(fm:Bericht von Fritz Machlup
über die Wirtschaftlichen Grundlagen des Patentrechts) war ihm ein
Begriff. Software-Monopolen steht er grundsätzlich kritisch gegenüber.

#ara: Dibbert und Tönnies präzisierten meine Kritik am Patentwesen mit
einigen starken fachlichen Details und der Überzeugungskraft von
Praktikern. Sie konnten die Probleme sehr gut darstellen. Insbesondere
Tönniesens Hinweis auf den perfiden Mechanismus der Rechtsunsicherheit
für Konkurrenten nach einer Patentanmeldung und Unterlassungsklagen
war sehr anschaulich.

#WWW: Herr Ben Schlüssler ist allgemein für %(q:Freies Wissen) und brachte
verschiedene Links zu relevanten Webseiten in seinem Kurzvortrag, die
verschiedene Lager abdeckten. Unter anderem auch ifrOSS und eine auf
OpenTheory gehostete Website. Die Veranstaltung beruhte auf seiner
Privatinitiative innerhalb der Organisation. Es war %(q:seine
Veranstaltung).  Dementsprechend äußerte er sich auch sehr positiv
über die Ergebnisse. Dabei verwies er auch auf die positiven
Feedbackzettel der Teilnehmer. Meinen Vortrag bezeichnete er vorab
aufgrund der Folien, die ihm vor der Veranstaltung schickte, als
%(q:ausgewogen).

#hWe: Die Patentrechercherin/-beraterin des TTZSH, Frau Birgit Binjung,
beschönigt nicht die Problematik der Softwarepatente. Eine Aufgabe
ihrer Stelle sei es unter anderem auch Erfinder zu beraten und die
Zahl der Erfindungen zu reduzieren im Sinne der Erhöhung der
Erfolgswahrscheinlichkeit. Eine sehr wichtige Aufgabe, die das Land
Schleswig-Holstein auch der Förderung wert findet. TTZSH hat ein Paper
ausgelegt, in dem %(q:Informationen zu Schutzrechten) niedergelegt
sind, sehr schön gemacht.

#dts: Der Diskussionsleiter, Herr Schröder, von der renommierten
wirtschaftswissenschaftlichen Bibliothek ZBW Kiel, steht der OSS/FS
Thematik sehr offen gegenüber. Bei ihm sei IIS durch Apache ersetzt
worden, jetzt gebe es weniger Probleme.

#ceo: Ein Problem der Veranstaltung war sicherlich ihre Themenwahl
%(q:Patentlösung vs. OpenSource), durch die nahegelgt wurde, dass die
Alternative: Software patentieren oder die Quellen offenlegen hiesse.
Ich wurde als Open-Source-Verfechter apostrophiert und insgesamt war
das sehr positiv konnotiert.

#SWt: Ein Marketing-MA von der Firma von Herrn Dr. Dibbert sieht einen hohen
Informationsbedarf in Sachen OSS/FS, offensichlich besteht im Moment
eine Art %(q:OpenSource)-Marketinghype unter TOC-Gesichtspunkten.
Dabei wurde die Studie der Gartner Group u.a. angesprochen.
Offensichlich ist %(q:Open Source) populärer als Logikpatentierung,
weshalb die unverhältnismäßige Gefährdung freier Software durch
Patente durchaus ein politisch wirksames Argument sein kann. 
Andererseits fördert dieses Argument eine Fehlwahrnehmung.  Denn
Patente gefährden nicht nur die Freie Software sondern die gesamte auf
dem Urheberrecht aufbauende Eigentums- und Wettbewerbsordnung der
Softwarebranche, der es bisher weder an Innovationskraft noch an
Monopolisierungstendenzen gemangelt hat.

#dlf: Zuletzt wurden deswegen die Fragen vornehmlich bezogen auf
OpenSource-Lizenzen gestellt und ein Teilnehemr stellte etwas
merkwürdige Fragen, die aber mehr Kritik am OSS/FS Ansatz bedeuteten.
Der Kenntnisstand der Teilnehmer war in diesem Gebiet der Diskussion
überdurchschnittlich, manche Unklarheiten wurden schon durch das
Publikum selbst aufgelöst. Ich habe nur festgestellt, dass OSS/FS
Lizenzierung nicht heißen müsse, dass man seinen angepassten Quelltext
für unternehmensinterne Software der gesamten Öffentlichkeit zur
Verfügung stelle. In vielen Fällen sei es aber sinnvoll, Programmkerne
öffentlich gemeinsam zu entwickeln.  Desweiteren bestünde im Falle,
dass daraus keine Wettbewerbsnachteile erwachsen, kein Anreiz, Wissen
und Texte unter Verschluss zu halten. Das kann man auch mit den
Gütereigenschaften von SW erklären: nicht-rivalisierender Konsum. In
diesem Zusammenhang verwies ich auch auf das
Standardsoftware-Beratungsgeschäft wo es gang und gäbe ist, dass
Wissen der Beratungsgesellschaft auch für den Auftrag bei dem
Konkurrenten in der Branche genutzt wird (das Knowhow der
Beratungsfirma). Ich erklärte aber, für die Frage Open Source/Freie
Software sei ich der falsche Evangelist und verwies auf andere
Mitglieder des FFII sowie auf die FSFE.

#toi: TTZSH.de: Mediatage Nord: Podiumsdiskussion Swpat

#rdn: U.a. finden sich hier die Folien von Herrn Schlüssler und Verweis auf
die sehr interessanten Folien von Andrés %(q:Konkurrenten), Herrn Dr.
Dibbert, der weitgehend eine ebenso logikpatent-kritische Position
vertrat.

#biI: Dibberts Firma BMI AG

#WWe: Seite der BMI AG über die Mediatage

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpkiel02B ;
# txtlang: de ;
# multlin: t ;
# End: ;

