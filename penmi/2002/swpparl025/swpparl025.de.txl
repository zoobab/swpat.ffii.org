<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europarl-Anhörung 2002-05-15

#descr: Eine Gruppe von Abgeordneten des Europäischen Parlaments lud Anthony
Howard (der in der Dienststelle für Gewerblichen Rechtschutz der
Europäischen Kommission für die Softwarepatentierbarkeitsrichtlinie
zuständig ist) und Hartmut Pilch von FFII/Eurolinux zu einer
öffentlichen Anhörung in Straßburg ein.  Hier finden Sie die
Einzelheiten dieser Anhörung, u.a. ein paar Präsentationsschaubilder.

#Eag: Einladungstext

#cph: %(cl|Cinéma, Audiovisual policy and Intellectual property
Intergroup|invites you to its meeting| Should we patent
%(q:computer-implemented inventions)?)

#cWn: %(cl|Wednesday 15 May 2002|from 15h00 to 16h30|Room S2.1|European
Parliament in Strasbourg|Languages : %(LANGS))

#CeW: %(q:Programs for computers) are not %(q:patentable inventions)
according to Article 52 of the European Patent Convention (EPC).
However since 1978 around 30,000 patents for %(q:computer-implemented
inventions) have been granted by the European Patent Office and by
national patent offices. Indeed, the jurisprudence of these offices
considers programs which present a %(q:technical contribution) to be
patentable. This idea of a %(q:technical contribution) is similar to
that of the %(q:technical process) used for the patent of genes in the
controversial directive on biotechnological inventions. They are both
just as ambiguous.

#Tjt: The proposed %(q:directive on the patentability of
computer-implemented inventions) intends to normalize the
jurisprudence of the European Patent Office and the national courts.
Computer technology is in a state of uncertainty making it difficult
to differentiate between creation, public good and the market.

#Tei: To patent computer advances, like medicines, the knowledge and the
living, poses questions of access and transmission for everybody, and
equality between persons and people.

#Wit: With

#ruW: president of the Foundation for a Free Information Infrastructure
(FFII) and speaker of the Eurolinux Alliance

#aMa: administrator, DG MARKET, intellectual property service.

#mpn: member of the European parliament, industry committee.

#Cna: Contact

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl025 ;
# txtlang: de ;
# multlin: t ;
# End: ;

