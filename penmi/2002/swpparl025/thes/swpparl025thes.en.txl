<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Should we patent %(q:computer-implementable inventions)?

#descr: Some points to be made at the Europarl on 2002-05-15

#aac: alternative concept

#SWn: %(e:Inventions) are patentable.

#Sem: Should we patent %(s:computer-implemented inventions)?

#Sal: Rules of organisation and calculation (algorithms) are not patentable.

#Sem2: Should we patent %(s:computer-implemented rules of organisation and
calculation)?

#SeW: Should we patent %(EP)?

#Att: An alloy comprising Fe-B-R characterized by containing at least one
stable compound of the ternary Fe-B-R type, which compound can be
magnetized to become a permanent magnet at room temperature and above,
where R stands for at least one rare earth element inclusive yttrium.

#Ysi: YES, this is an invention.

#NeW: YES, this is an invention.

#Aet: A computer based testing system comprising

#atd: a test development system for creating a computerised test having a
number of questions to be answered by an examinee and rules for
determining a sequence of questions to be presented;

#aax: a workstation operable to present the questions to at least one said
examinee and operable to accept examinee responses to the questions so
presented; and

#asW: a test delivery system operatively coupled to both said test
development system and said workstation for delivering said
computerised test by presenting the questions according to the rules
to each said examinee on said workstation,

#cei: characterised in that

#sii: said computerized test has a plurality of related test screens
containing messages and directions providing information to the
examinee, and by a computerized test script created using said test
development system for controlling said workstation to present the
questions and related test screens according to rules defined by said
test script.

#SaE: Should we patent %(EP)?

#Aea: A method for displaying on a computer screen multiple sets of
information needed on a recurring basis, comprising the steps of:

#eei: establishing an area on the computer screen in which the multiple sets
of information are to be displayed, the area having a maximum size
which is substantially less than the entire area of the screen;

#poo: providing within the area a plurality of selection indicators, one for
each of the multiple sets of information; and

#sWi: selecting one of the multiple sets of information for display within
the established area by pointing to one of the selection indicators
within the established area, whereby the selected set of information
will be substituted within the area for the set of information
previously being displayed therein.

#cWW: %(q:Patentable Invention) = %(q:Technical Invention) = %(q:Technical
Contribution)

#Adi: Art 52 EPC: mathematical methods, abstract plans & rules, programs for
computers, presentation of information etc %(q:are not patentable
inventions).

#Bae: BGH: %(s:a teaching about use of natural forces to immediately achieve
a causally overseeable success)

#Pyn: Patentability Legislation Test Suite

#WTe: Who favors the Traditional Invention Concept?

#Sru: Software Industry

#1We: 115,000 signatories and 300 sponsors petition.eurolinux.org

#8wp: 800 software company CEOs

#nee: nx100 employees and leading managers of Siemens, IBM, Alcatel etc,
especially programmers and technical/entrepreneurial project managers

#2rC: > 1400 of 1500 respondents in CEC consultation

#CDn: CEC DG Infosoc

#Fhe: French Government

#pic: politicians

#AsW: All respected economists -- not 1 study points out advantages of
software patentability

#Wsn: many patent examiners, judges and law scholars

#Wec: Who favors the new Invention Concept of the EPO?

#Ptu: Patent Industry

#Pfn: EPO, Patent Office Management, AIPPI, FICPI etc

#CoW: CEC Indprop, governmental patent departements

#Ema: Economic Majority

#cWe: corporate patent departments

#Bar: Bitkom: 7 patent lawyers vs 1 SME

#Eet: submissions from EICTA, UNICE, BSA etc

#uWy: unqualified patent-lawyer applause

#seo: sollicited by %(q:very specific questions) from CEC Indprop

#csi: %(s:perhaps) some telco or sw companies -- but no credible case
studies available

#Cab: CEC Proposal: Replacing Clear Rules by a Carte Blanche

#AkW: Any computing-related idea is technical and therefore patentable.

#TWv: The idea must contain an invention in its non-obviousness aspect.

#Tte: This invention needn't be new.

#Tdg: This invention may even be a business method.

#Tit: The terms %(q:technical) and %(q:invention) are undefined.

#Fua: Strange reasoning about %(q:algorithms) and about software economy.

#Mnp: Bringing the Patent System under Control

#TWd: The patent industry is used to creating, ignoring and changing its own
laws.

#Ayl: To the patent industry, art 52 EPC is painfully clear.

#Fnm: The problem is a political one.

#EsW: Europarl Commission for the Supervision of the European Patent System

#0Wa: <20% patent industry participation.

#Bts: Patentability Legislation Test Suite

#Mtt: Make sure the Patentability Rules really generate the patents which we
want!

#FWe: First Fix the Bugs, then Release the Code!

#AP9: Appeal: 5 Problems, 9 Solutions

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl025thes ;
# txtlang: en ;
# multlin: t ;
# End: ;

