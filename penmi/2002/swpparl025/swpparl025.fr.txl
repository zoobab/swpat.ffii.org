<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Conférence Europarl 2002-05-15

#descr: Un groupe de membres du parliament européen invita Anthony Howard
(responsable du projèt de la directive pour la brevetabilité du
logiciel dans l'Unité de Propriété Industrielle de la Commission
Européenne) et Hartmut Pilch de FFII/Eurolinux a une discussion
publique a Strasbourg.  Ici vous trouvez les détails de cette
discussion, y inclus quelques images de présentation.

#Eag: Texte d'Invitation

#cph: %(cl|Intergroupe Cinéma, politique Audiovisuelle et Propriété
Intellectuelle|vous invite à la rencontre|Faut-il breveter les
%(q:inventions mises en oevre par ordinateur)?)

#cWn: %(cl|le Mercredi 15 mai 2002|15h00 à 16h30|Salle S2.1|Parlement
européen à Strasbourg|Langues : %(LANGS))

#CeW: Les %(q:programmes d'ordinateurs) ne sont pas des inventions
brevetable selon l'article 52 de la Convention sur le Brevet Européen
(CBE). Cependant, depuis 1978, environ 30 000 brevets concernant des
%(q:inventions mises en oevre par ordinateurs) ont néanmoins été
délivrés par l'Office européen des brevets et les offices nationaux
des brevets. En effet, la jurisprudence de ces instances a considéré
comme brevetable les logiciels lorsqu'elles présentent «une
contribution technique». Cette notion de «contribution technique» est
analogue à celle de «procédé technique» utilisée pour la brevetabilité
des gènes dans la directive controversée relative aux inventions
biotechnologiques. Elle est tout aussi équivoque.

#Tjt: La proposition de %(q:directive concernant la brevetabilité des
inventions mises en oevre par ordinateur) entend normaliser la
jurisprudence de l'OBE et des tribunaux nationaux. L'informatique est
dans une période d'incertitude pour qualifier le partage entre
création, bien public et marché.

#Tei: Breveter les logiciels comme les médicaments, le savoir comme le
vivant, pose les questions de l'accès et de la transmission pour tous,
et de l'égalité entre les personnes comme entre les peuples.

#Wit: Avec

#ruW: président de la Ligue pour une Infrastructure Informatique Libre
(FFII), représentant de l'Alliance EUROLINUX

#aMa: administrateur de la DG MARKET- division propriété intellectuelle

#mpn: député européen, membre de la commission industrie

#Cna: Contact

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpparl025 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

