# -*- coding: utf-8 -*-

How to find and define proper boundaries for patentability?

Paper for the Workshop ``Frontiers of Ownership in the Digital Economy''
Workshop

Paris, June 10-11, 2002

Patents are supposed to be granted only for inventions that are novel
and non-obvious. In Europe, additionally inventions must also are
required to be ``technical'' to qualify for a patentability. And sSome
categories of inventions are excluded unconditionally from
patentability, such as methods for medical treatment and diagnosis.

Are these requirements appropriate to define the boundaries of
patentable inventions?

A number of questions arise. If these requirements are appropriate, are
they enforced properly? If not, what other requirements might be
appropriate? And in particular: how do we find answers to these
questions?

In the recent past, several studies have been were undertaken to assess
whether software and business method patents are desirable, mostly based
on extensive consultations. However, Consistently, none of these studies
has arrivesd at unambiguous conclusions on the desirability of software
patents. Instead, a common observation appears to be that Rather, it is
commonly concluded that ``computer software'' is a diverse phenomenon; .
Ggeneralizations, therefore, seem to be inappropriate. Instead, Rather a
tailored approach would appear to be more suitable. seems to be needed.
On the other hand, tThe consultations reveal more consensus as to on
business method patents; : apparently no one in Europe seems to be
particularly fond of such these patents.

If consultations do not provide conclusive answers, what conclusions
could be drawn from the current functioning of the patent system, and in
particular from the experiences in the U.S., following the State Street
decision?

In the U.S., much attention is paid to problems of ``patent quality''
problems: due to the  the apparent granting of patents tofor inventions
that do not meet the basic requirements of novelty and non-obviousness.
The U.S.P.T.O. is addressing these complaints by various measures.

At a more fundamental level, the question is raised whether business
method patents have been found to benefit effectively beneficial to
society effectively, i.e. in practice. While some ``clearly
inappropriate'' patents have received much publicity, as far as we know
economists have refrained from drawing seem to have reservations to draw
more general conclusions. Business method patents are a relatively new
phenomenon, and it is simply too early to assess their its effects.
Apart from these above ``quality problems'', businesses are still
learning how to cope with business method patents. It may still take
many some more years before a steady state is reached - if ever.

On the other hand, it is a known fact that patents are widely used for
``strategic purposes'', and there is no reason to believe that software
or business method patents are an exception in this respect. Patents are
supposed to stimulate innovation to such an extent that this positive
effect exceeds the (potential) negative effect of decreased competition.
In practice however, patents are used as well to fight off competitors.
In response to such offensive use of the patent system, many businesses
feel forced to take defensive measures. Commonly, ``defensive'' patent
portfolios are built up solely with the purpose of to obtaining exchange
objects for cross licensing. In view of the limited scope of statutory
legal provisions permitting compulsory licensing to obtain mandatory
licenses in court, cross licensing may be the only viable way of option
to obtaining a license from a competitor. Incidentally, this practice
may actually prevent SMEs from to entering on the market - whereas while
popular belief has it is that patents are good for SMEs. In conclusion,
there is ample evidence that we are not just faced with the traditional
patent balance: innovation stimulation at the expense of decreased
competition. It seems that in addition, perhaps even dominantly, the
patent systems has become a ``lawyers paradise''.

For such reasons, some feel there is a case to advocate the abolition of
the patent system. It must be acknowledged however, that there are
indeed ``good'' patents, deserving and needing legal protection. So, how
do we make the distinction?

In Europe, currently the ``technical character'', or  ``technical
contribution'' of an invention plays an important role to distinguish
between ``desirable'' and ``undesirable'' patents. In the proposed
European software directive, again athe requirement of  ``technical
contribution'' is again the central criterion.

Practice has shown however, that such criteria draw a rather arbitrary
line between patentable and non-patentable software, and are unsuitable
to . And they do not even prevent business method patents. Patent
attorneys know how to pass such hurdles. At a more fundamental level the
question to be asked is: what is the rationale foundation of a the
requirement of a technical character or contribution? Emotionally, many
feel that patents should be reserved for certain domains of human
endeavor, and not be granted for all inventions leading to a ``concrete,
useful and tangible'' result. But it is questionable whether a the test
of technicality really criterion serves the purpose of delimiting the
domain of patentsdelimitation. For instance, patents in the field of
sports area may easily meet the technical character criterion. Yet, many
may feel sports patents are simply inappropriate. Remarkably, such
restrictions are not based on economic considerations. Currently in
Europe no patents on methods for medical treatment or diagnosis are
allowed - even though from an economic perspective there seems to be no
fundamental reason to distinguish difference between such methods from
and pharmaceuticals that are deemed patentable subject matter.from an
economic perspective. Following the same line of thought, it might be
more appropriate to exclude business methods directly rather than
indirectly by means of any criterion of ``technical contribution''
criterion, if such patents are considered inappropriate on an
``emotional'', political level.

Unlike business method patents, in Europe software patents generally are
generally not considered categorically inappropriate unconditionally in
Europe. There is little doubt that a suitably programmed microprocessor
chip controlling a device should receive no less patent protection than
its mechanical predecessor. On the other hand, there are serious
objections against patents on trivial programming techniques.
Adversaries of software patents regularly have complained that indeed
actually many software patents have been granted for such
``inventions''. Apparently, the test of The ``technical character''
apparently is not the central issue at all - again, it constitutes an
indirect criterion at best.

At first sight, the law does not allow trivial patents: patentsable
inventions must not be ``obvious to a person skilled in the art''. So,
is this really an administrative on problem? Would such patents be
revoked if challenged in court? If so, there remains is still a problem:
litigation costs are high, and may not be fully recoverable even if a
the plaintiff prevailswins the case. But actually, there liesis a more
fundamental problem beneath. There is a long tradition, certainly not
just in the field of computer software area but in all areas of
patentable activity, to apply the test of non-obviousness criterion in a
rather loosely fashion. The Examination Guidelines published by the
European Patent Office leave no doubt that only just extremely obvious
inventions will should be denied a patent. While these Guidelines lack a
formal legal status, they do reflect current thinking - and have not
been overturned by court decisions. In the U.S., similarly it is
similarly acknowledged that the U.S. patent system pretty much is a
``102 system only'' - , referring to § 102 of the U.S. Patent Law
requiring that requires novelty, as opposed to rather than §103
requiring that requires non-obviousness. This practice is quite in stark
contrast to with the motivation of the State Street decision, which
explicitly refers to the test of  (among other things) non-obviousness
as the criterion to discriminate between ``good'' and ``bad'' patents.

The ``patent inflation'' problem resulting from such practices may be
particularly serious in the software area, but it is certainly not
limited to that area alone. If this problem could be solved, that might
be very helpful to address several deficiencies of the patent system as
it works today. (of minder/meer slagen om de arm?). The acceptance in
society for really great inventions will be less problematic than the
acceptance for ``trivial'' inventions, and their economic value will
probably be higher as well.

However, solving the problem of It will not be easy though to address
the patent inflation will not be easyproblem appropriately. Patent
offices may be required ested to be apply stricter on the test of
non-obviousness (inventive step) criterion more strictly. But they are
faced with backlogs, and may want to avoid time-consuming appeal
procedures that inevitably will follow if more often patents are
regularly denied on such grounds. Clearly, there is a need for legal
criteria that are both stricter from a substantialve perspective and
less open for interpretation, in order to avoid legal insecurity, for
everyone involved.

Having said all this, it must be acknowledged that ``reinventing the
inventive step'' has been on the agenda of patent reform before several
times, . and it is certainly not an easy task. It is a tough legal
problem to find a formulation that satisfies the needs. Probably, less
abstract criteria will be required than the current criterion of
``non-obviousness'' criterion. Induction from empirical data on actual
patents may help to find better criteria.

Raising Increasing the inventive step requirement may also cause
transitional problems. On the other hand, the issue is now perhaps more
urgent than ever. There is certainly definitely an increased public
awareness that the patent system is becoming more of a problem than a
blessing, so there may be political support for more radical measures
now.

Finally, we are the opinion that the lack of factual information about
the actual functioning of the patent system may ben is an issue that
ought to be addressed by itself. The world seems to be divided between
patent system insiders who are almost invariably in favor of strong and
broad wide patent protection under on easy terms, and patent system
critics. Patent Offices consider it their task to help inventors obtain
patents, rather than to review inventions critically in the light view
of possible negative effects on the economy. If the patent system would
be a ``business'', a management system would be implemented to measure
its performance on a routine basis. Of course, the EPO collects
statistics on the about their its operation. But there is also an urgent
need to gather more knowledge about the role of patents in the economy
once they have been granted. Do Which patents are really play an
considered essential role in allocating for the business justification
decision to investment in R&D? Which patents are solely obtained for
aggressive or defensive purposes? How can we What are the apparent
criteria to distinguish ``trivial'' inventions in practice?

In order to answer such questions, we feel it might would be advisable
to establish institute a ``European Patent Observatory'', that monitors
the functioning of the patent system in Europe on a routine basis. Such
an ``Observatory'' might measure is certainly justified in order to
provide a factual basis to the current ongoing, increasingly vigorous
debate about the patent system. The interests are at stake high. Europe
will And we may  need powerful strong arguments not to follow the
American example once againmore, as has happened so we did before so
often in the past.

In sum, mary we would recommend:

the exclusion of business method patents categorically directly and per
se, rather than indirectly by means of a requirement of the ``technical
contribution'' criterion,

to stop the patent inflation, by legislative measures, and

to establish institute a European Patent Observatory, that collects on a
routine basis the ``management'' information necessary needed for
managing, controlling and evaluating the governance of the patent
system.

>.

>.

>

>.

 Adam B. Jaffe, The U.S. Patent system in transition: policy innovation
and the innovation process, Working Paper 7820, National Bureau of
Economic Research, Cambridge MA, p 20.

>.

>.

 Comprehensive advice on such matters is given for instance in: Keith
Beresford, Patenting Software under the European Patent Convention,
London: Sweet & Maxwell 2000.

 In the U.S., currently patents can be granted to any (novel and
non-obvious) invention that is concrete, useful and tangible.

 John R. Thomas, `The Post-Industrial Patent System', Fordham
Intellectual Property, Media & Entertainment Law Journal, 1999, p. 3-59.

>

 Koch & Sterzel, T26/86, Official Journal of the EPO 1988,19.

 Art. 56 European Patent Convention.

>.

>.

 Source? Otherwise remove perhaps.

>.

 For references see Mark D. Janis, `Patent Abolitionism', paper
presented to the Berkeley Center of Law and Technology Patent Reform
Conference, Boalt School of Law, March 2002.

>, p. 2.

U

U

=

I

S

T

|

tm

?

®

»

Á

Ä

?

?

?

	

?

6?

?

?

dh

h

dh

dh

h

j?

j§

K©

K©

K©

K©

K©

K©

Úi@2

*#

?ßG7

?

???

±%???

?

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

K©

?

o

2

?

Ü

?

o

2

?

Ü

?

.

k

Ü

/

F

k

|

?

Ô

?

ý

?

?

#

$

*

-

k

t

...

?

"

?

?

?

?

¤

?

?

?

?

?

.

k

Ü

/

F

k

|

?

Ô

?

ý

?

o

2

?

Ü

?

.

k

Ü

/

F

k

|

?

Ô

?

ý

?

?

#

$

*

-

k

t

...

?

"

?

?

?

?

¤

?

?

?

?

@

Z

p

x

|

-

¤

¦

®

?

@

?

?

#

$

*

-

k

t

...

?

"

?

?

?

?

¤

?

?

?

?

@

Z

p

x

|

-

¤

¦

®

?

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

Z

p

x

|

-

¤

¦

®

?

@

@

@

@

@

@

@

@

@

'

N

N

'

'

^

^

^

~

?

'

^

^

'

'

^

^

'

^

^

'

^

?Ä?`6

Ö

?)zG#

?Ä?`6

?

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

@

R7

?)zG#

R7

Úi@2

*#

?ßG7

?

Úi@2

*#

Lq6

