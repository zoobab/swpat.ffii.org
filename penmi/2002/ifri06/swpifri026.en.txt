<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Institut Français des Relations Internationales (IFRI.org) and Center of Information Policy Research at Mariland University (CIP.umd.org) are organising a transatlantic conference on information economy and in particular on the limits of patentability as well as the problems in neighboring areas such as database exclusion rights and copyright.  Hartmut Pilch is participating on behalf of FFII and Eurolinux on two of the panels.
title: Information Economy and Swpat Conference Paris 20020610-1
TCe: The Conference
Plp: Priorities according to Hartmut Pilch
Rru: Relevant Work, Background Documents
Ist: Institut Français des Relations Internationales (IFRI.org) and Center of Information Policy Research at Mariland University (CIP.umd.org) co-organised in June 2002/06 a transatlantic conference on information economy and in particular on the limits of patentability as well as the problems in neighboring areas such as database exclusion rights and copyright.
Pdn: Papers presented by the participants at a seminar held in Paris in June 2002, among them many economists and social scientists from the US.
Lvn: Links to documents in the WWW which are related to patents on immaterial innovation (rules of organisation and calculation, software & business methods) and to the European Commission's recent push to sanction such patents by a directive.
Sle: Schedule of the talks planned for the IFRI conference.
Tsi: This paper was %(q:opensourced) by us shortly before the conference and published by the authors, who attended the conference, a few days later.
Aoa: Any discussion should be conducted on the basis of example patents, as outlined in the %(ts:Software Patentability Legislation Testsuite) approach.
Tdk: The causality between law and various economic effects is an indirect one:  the legal system produces patents, and the patents produce the effects.  The example patents are an intermediate link in the causality chain which people investigate, and this link is often missing from the investigation.
Tcp: The European Commission's Directive Proposal displays a few formulas, such as the %(q:requirement of a technical contribution in the inventive step), which suggest that a limitation on patentability is intended.  Upon close reading, it however turns out that this proposal legalises all types of patents which are generally deemed undesirable and removes all limits on patentablity by 4-5 layers of safeguards, designed to ensure that patentability can in no way be limited and that lawcourts can no longer find any support in the law for refusing even the most undesirable kinds of patents on rules of organisation and calculation (information innovation, business methods).
Sue: Studies such as that of Fraunhofer/ISI have not made it clear which patents, granted according to which patentability rules, they are talking about and yet investigated industry opinion about such patents / patentability rules.  Such a flaw can seriously reduce the value of a study.  Several recent government-ordered studies from Europe seem to suffer from this flaw.
Erh: Even some of the best studies from american academia don't make it very clear which sets of patents, granted according to which rules, they are investigating.  The distinction between %(q:industries) or %(q:fields of technology) is not very sharp.  A sharper delimitation is possible and should be attempted.
IdW: In particular, the concept of %(q:technical invention), as employed by German courts and legal literature throughout the 70s and 80s (and partially until today), is founded in non-legal categories that are successfully used in the philosophy of science and serve to define sharp boundaries.  I would like to recommend its use to economic researchers, particularly from the USA, where this concept was not as well established and has been forgotten for a longer time.
Sto: Some other ideas which I may like to propose to the conference are found in the %(q:background documents).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpenmi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpifri026 ;
# txtlang: en ;
# End: ;

