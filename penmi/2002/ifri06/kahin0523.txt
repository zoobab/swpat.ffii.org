Please check the main website for the conference and workshop at
http://cip.umd.edu/IFRI.htm for updated information.  Note, in particular, the
background materials posted at http://cip.umd.edu/IFRIbackgroundmaterials.htm
and the workshop page: http://cip.umd.edu/ifriworkshop2.htm
If you haven't already done so, please send URLs to background items associated with
your work that are relevant to the workshop -- or send the items if you need to have
them posted.   I will start posting these shortly.  Non-governmental participants are
asked to submit a short statement (1-2 pages) on priorities or problems that need
special attention.  (Governmental participants may submit statements if they choose.)
Please submit all material by June 3.  There will not be a public link to the submitted
statements -- I will provide an index for workshop participants; however, there will be
public links to your background items, unless you instruct me otherwise.
Also, please review the following outline (from the workshop page); please inform me of
1-4 items where your
research interests, priorities, or expertise lie.  You are welcome to add topics not on
the outline.  I will use this information to finalize the order of discussion at the
workshop.  Each participant will have an opportunity to speak for 7 minutes to stimulate
group discussion.   You do not have speak about your submission or background materials,
but your comments should be relevant to the particular session.
There will be a hosted lunch for presenters at IFRI on the day of public conference
(June 10) to which workshop participants are also invited.
Nature of information innovations
-               definitions and categories: software, business methods, social
processes, etc.
-               cumulative nature of innovation
-       software as a complex product
Competition and industry structure
-               measurement of incentive and disincentive effects
-               venture capital assessment of startup portfolios and patent environments
-       R&D manager perspectives on value and use
-       strategic use of patent portfolios
-       licensing practices (knowledge transfers vs. settlements)
-       markets for components and patents
-       implications for open source business models
-       management of information, transaction costs, and risk
-       licensing firm business models, strategies, and market impact
-       thickets, foreclosure, and overpatenting
Knowledge management
-               location and assessment of prior art
-               infringement avoidance
-       relationship to anticipatory standards development
-       disclosure quality
-               awareness and use of disclosures by developers
-               diffusion of patent information
Operation of the patent system
-               quality of examination and issued patents
-               application of non-obviousness/inventive step standard
-       application of 輒chnicity�equirement
-       effects of early publication
-       opposition and reexamination
-       infringement notices, negotiations, and settlements
-       frequency and conduct of litigation
-       assessing risk and uncertainty
Political economy
-               formation of stakeholder positions and political strategy
-               professional and institutional perspectives and influence
-       judicial, legislative, and administrative roles
-       pursuit of comparative advantage in national/regional policy
-       relationship to policies on innovation, competition, access, open source, SMEs,
and research
-       implications for software development in emerging economies
-       harmonization vs. interoperation
Research strategy
-               policy relevance
-               data collection needs and problems
-       cross-industry comparisons
--      institutional cooperation
workshop participants as of 23.5.02:
Dominique Foray, OECD/Dauphine U.
Frédérique Sachwald, IFRI
Brian Kahin, University of Maryland
Ed Steinmueller, University of Sussex
Robert Hunt, Federal Reserve Bank of Philadelphia
Iain Cockburn, Boston University
Rochelle Dreyfuss, NYU
Justin Hughes, UCLA
Knut Blind, Fraunhofer Institut
Bronwyn Hall, University of California, Berkeley
Anthony Howard, DG Internal Market
Philippe Aigrain, DG Information Society
Bernd Lutterbeck, Technical University Berlin
Deepak Somaya, University of Maryland
Peter Holmes, University of Sussex
Jean-Michel Dalle, Ecole Normale Superieure de Cachan
Stefaan Verhulst, Markle Foundation
Thomas Multhaup, Bundesministerium für Wirtschaft und Technologie
Dominique Guellec, OECD
Gregoire Postel-Vinay, Ministere de l'Economie des Finances et de l'Industrie
Hartmut Pilch, FFII/Eurolinux
Reinier Bakels, University of Amsterdam
Clark Eustace
tbd, WIPO
