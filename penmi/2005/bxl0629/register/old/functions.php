<?php
function getinfofile () {
  $jlist = array();//liste der jpegs mit (jpg,gruppennamen,deutscherText,englischerText)
  $glist = array();//liste der gruppen mit (gruppenkuerzel, BeschreibungDeutsch, BeschreibungEnglisch)

  $handle = fopen ("./pic.list", "r");
  $line = "";
  while ((!feof($handle)) and (substr($line,0,9) != "==Gruppen")) {
    $line = fgets($handle, 4096);
    //echo $line." s<br />";
  }
  $line = fgets($handle, 4096);
  //echo $line." a<br />";
  while (!feof($handle) and substr($line,0,9) != "==Dateien") {
    list ($gruppe,$deutsch,$englisch) = split("-\|", $line,3);
    if(! isset($glist[$gruppe])) {
      $glist[$gruppe]=array("deutsch" => trim($deutsch),
			    "english" => trim($englisch));
    }
    $line = fgets($handle, 4096);
    //echo $line." g<br />";
  }
  while (!feof($handle)) {
    $line = fgets($handle, 4096);
    //echo $line." j<br />";
    list ($filename,$gruppen,$deutschl,$englischl,$deutschk,$englischk)= split ("-\|", $line,6);
    if(! isset($jlist[$filename])) {
      $jlist[$filename]=array("gruppe" => trim($gruppen),
			      "deutschl" => trim($deutschl),
                              "deutschk" => trim($deutschk),
			      "englishl" => trim($englischl),
                              "englishk" => trim($englischk));
    }
  }
  fclose ($handle);
  return array("Gruppen" => $glist,
	       "jpgs" => $jlist);
}
function getinfos ($dirName) {
  $jlist = array();//liste der jpegs mit (jpg,gruppennamen,deutscherText,englischerText)
  $glist = array();//liste der gruppen mit (gruppenkuerzel, BeschreibungDeutsch, BeschreibungEnglisch)

  $handle = fopen ("./pic.list", "r");
  $line = "";
  while ((!feof($handle)) and (substr($line,0,9) != "==Gruppen")) {
    $line = fgets($handle, 4096);
    //echo $line." s<br />";
  }
  $line = fgets($handle, 4096);
  //echo $line." a<br />";
  while (!feof($handle) and substr($line,0,9) != "==Dateien") {
    list ($gruppe,$deutsch,$englisch) = split("-\|", $line,3);
    if(! isset($glist[$gruppe])) {
      $glist[$gruppe]=array("deutsch" => trim($deutsch),
			    "english" => trim($englisch));
    }
    $line = fgets($handle, 4096);
    //echo $line." g<br />";
  }
  while (!feof($handle)) {
    $line = fgets($handle, 4096);
    //echo $line." j<br />";

  list ($filename,$gruppen,$deutschl,$englischl,$deutschk,$englischk)= split ("-\|", $line,6);
    if(! isset($jlist[$filename])) {
      $jlist[$filename]=array("gruppe" => trim($gruppen),
                              "deutschl" => trim($deutschl),
                              "deutschk" => trim($deutschk),
                              "englishl" => trim($englischl),
                              "englishk" => trim($englischk));
    }
  }
  fclose ($handle);

  $d = dir($dirName);
  while($entry = $d->read()) {
    if ($entry != "." && $entry != "..") {
      if (is_dir($dirName."/".$entry)) {
	//    getDirList($dirName."/".$entry);
      } else {
	if(substr($entry,-4) == ".jpg") {
	  //echo /* $dirName."/". */ $entry."<br />\n";
	  if(! isset($jlist[$entry])) {
	    $jlist[$entry]=array("gruppe" => "prot",
				 "deutschl" => substr($entry,0,strlen($entry)-4),
				 "englishl" => "",
                                 "deutschk" => "",
                                 "englishk" => "");
	  }
	}
      }
    }
  }
  //print_r($jlist);
  $d->close();
  return array("Gruppen" => $glist,
	       "jpgs" => $jlist);
}

function storeinfos($infos) {
  //print_r($infos); 
  $handle = fopen ("./pic.list", "w+");
  $string = "Alles vor den mit == eingeleiteten Markern ist Kommentar. Danach sollten keine Leerzeilen sein.\n\n";
  fwrite($handle, $string, strlen($string));
  $string = "==Gruppen\n";
  fwrite($handle, $string, strlen($string));
  foreach($infos["Gruppen"] as $key => $value) {
    $string = $key."-|".$value["deutsch"]."-|".$value["english"]."\n";
    fwrite($handle, $string, strlen($string));
  }
  $string = "==Dateien\n";
  fwrite($handle, $string, strlen($string));
  foreach($infos["jpgs"] as $key => $value) {
    $string = $key."-|".$value["gruppe"]."-|".$value["deutschl"]."-|".$value["englishl"]."-|".$value["deutschk"]."-|".$value["englishk"]."\n";
    fwrite($handle, $string, strlen($string));
  }
  fclose ($handle);
}

function getset($set) {
  foreach($set as $k => $v) {
    $tmp[]=$k."=".$v;
  }
  return implode("&",$tmp);
}

function lsel($lang,$de,$en) {
  if ($lang=="de")
    return $de;
  else
    return $en;
}

function treeb($einrueckung,$href,$text) {
  $tmp="<tr>\n";
  for ($i=1;$i<=$einrueckung;$i++) {
    $tmp.="<td><img src=\"./sys/treev.gif\" border=\"0\"></td>";
  }
  $tmp.="\n<td><a href=\"".$href."\"><img src=\"./sys/treet.gif\" border=\"0\"></a></td>\n\t<td style=\"text-align:left;\" ".($einrueckung<4?"colspan=".(4-$einrueckung):"")."><a href=\"".$href."\"><nobr>".$text."</nobr></a></td></tr>\n";
  return $tmp;
}
?>