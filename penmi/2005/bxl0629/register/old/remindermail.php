<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
Foundation for a Free Information Infrastructure - Brussels, June 2005, Conference
</title>
</head>
<body bgcolor=#000070 text=#FFCC99 link=#8888CC vlink=#FF9966 alink=#FFFFFF>
<center><h2>Registration-Site for <a href =http://wiki.ffii.org/Konf0506En>FFII Software Patents Conference</a> on<br />June 01 2005<br /></h2></center>
<table><tr><td width="20%"></td><td width="60%">
<?php
//
//include "functions.php";

//print_r($HTTP_POST_VARS);

function make_seed() {
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}
$showform=true;
if(isset($HTTP_POST_VARS["posted"])) {
	$errormessage = "";
	if($HTTP_POST_VARS["name"] == "") {
		$errormessage .= "Please enter a name!<br />\n";
	}
	if($HTTP_POST_VARS["mail"] == "") {
		$errormessage .= "Please enter a valid mailadress! It is needed to confirm your registration.<br />\n";
	}
	if(!isset($HTTP_POST_VARS["security"]))
		$errormessage .= "Please check whether to participate in the morning session (Parliament) or not!<br />\n";
	if(isset($HTTP_POST_VARS["security"]) && ($HTTP_POST_VARS["security"] == "parl_check")) {
		if( ($HTTP_POST_VARS["padress"] == "") ||
			($HTTP_POST_VARS["pzip"] == "") ||
			($HTTP_POST_VARS["pcity"] == "") ||
			($HTTP_POST_VARS["pcountry"] == "")) {
			$errormessage .= "To be allowed to enter the Parliament it is obligatory to provide detailed information on your adress. Please fill in all adressdetails.<br />\n";
		}
	}
	if(!isset($HTTP_POST_VARS["room0531"]))
		$errormessage .= "Please let us know whether you need a room for May 31!<br />\n";
	if(!isset($HTTP_POST_VARS["room0601"]))
		$errormessage .= "Please let us know whether you need a room for June 01!<br />\n";
	if ($errormessage != "") {
		echo "<center><font color=\"#FF0000\">".$errormessage."</font></center><br />\n";
	} else {
		srand(make_seed());
		$randkey=rand(100000,999999);
		$dbh = pg_connect("dbname=bxl0411 user=regis password=sireg") or
		die("ERROR in connect!");
		
		$some1 = pg_query($dbh, "insert into person (event, name, mail, part_morning, part_after, sleep1, sleep2, company, street, zip_code, city, country, randkey) values (1,'".
		$HTTP_POST_VARS["name"]."','".
		$HTTP_POST_VARS["mail"]."','".
		(($HTTP_POST_VARS["security"] == "parl_sec"  ) ? "1" : "").
		(($HTTP_POST_VARS["security"] == "parl_check") ? "2" : "").
		(($HTTP_POST_VARS["security"] == "parl_no"   ) ? "3" : "")."','".
		(isset($HTTP_POST_VARS["afternoon"]) ? "1" : "0")."','".
		$HTTP_POST_VARS["room0531"]."','".
		$HTTP_POST_VARS["room0601"]."','".
		$HTTP_POST_VARS["company"]."','".
		$HTTP_POST_VARS["padress"]."','".
		$HTTP_POST_VARS["pzip"]."','".
		$HTTP_POST_VARS["pcity"]."','".
		$HTTP_POST_VARS["pcountry"]."',".
		$randkey.");");
		$oid = pg_last_oid($some1);
		$some1 = pg_query($dbh, "select id2 from person where oid=$oid;");
		$some2 = pg_fetch_array($some1);
		//print_r($some2);
		$id = $some2["id2"];
		pg_close($dbh);
		$messagemail  = "Dear ".$HTTP_POST_VARS["name"]."!\n\n";
		$messagemail .= "Thank you for registering at our Conference on June 01! Please read the following data carefully and, if everything is right, finish your registration by clicking the link provided at the end of this mail.\n";
		switch($HTTP_POST_VARS["security"]) {
			case "parl_no"   : $messagemail .= "You will *not* participate in the morning session (Parliament).\n";
			break;
			case "parl_sec"  : $messagemail .= "You will participate in the morning session (Parliament) and do not need accreditation to the Parliament, because you already have access.\n";
			break;
			case "parl_check": $messagemail .= "You will participate in the morning session (Parliament) so to get access to the Parliament you provided the following information as on passport/id card:\n".
			"  Street address :".$HTTP_POST_VARS["padress"].
			"\n  Zip code       :".$HTTP_POST_VARS["pzip"].
			"\n  City           :".$HTTP_POST_VARS["pcity"].
			"\n  Country        :".$HTTP_POST_VARS["pcountry"];
		}
		$messagemail .= "\nYou ".(isset($HTTP_POST_VARS["afternoon"]) ?  "" : "don't ")."want to participate in the afternoon session (Renaissance Hotel).\n";
		$messagemail .= "For may 31 you need:";
		switch ($HTTP_POST_VARS["room0531"]) {
			case "sRen": $messagemail .= " Single room at Renaissance Hotel Rue du Parnasse 19, +32 2 5052929, 3 mins from Parliament. Our group rate is at EUR&nbsp;159 incl. breakfast. WLAN options: 1hr at EUR&nbsp;9.95, 1 day at EUR&nbsp;19.95.\n";
			break;
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "dDra": $messagemail .= " Shared Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 40 / night.\n";
			break;
			case "host": $messagemail .= " Dorm bed at Van Gogh Hostel, Rue de Traversieres, metro Rogiers at EUR 16.50 / night.\n";
			break;
			case "none": $messagemail .= " no room.\n";
		}
		$messagemail .= "For june 1st you need:";
		switch ($HTTP_POST_VARS["room0601"]) {
			case "sRen": $messagemail .= " Single room at Renaissance Hotel Rue du Parnasse 19, +32 2 5052929, 3 mins from Parliament. Our group rate is at EUR&nbsp;159 incl. breakfast. WLAN options: 1hr at EUR&nbsp;9.95, 1 day at EUR&nbsp;19.95.\n";
			break;
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "dDra": $messagemail .= " Shared Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 40 / night.\n";
			break;
			case "host": $messagemail .= " Dorm bed at Van Gogh Hostel, Rue de Traversieres, metro Rogiers at EUR 16.50 / night.\n";
			break;
			case "none": $messagemail .= " no room.\n";
		}
		$messagemail .= "If these data are right, please click at http://demo.ffii.org/bxl0506/validate.php?id=".$id."&randkey=".$randkey."\nor copy it to your browser by hand.\nIf you want to change registration, please reenter your data at http://demo.ffii.org/bxl0506\n\nGreetings,\n    your registration-skript.";
	
/*		foreach($HTTP_POST_VARS as $k => $v) {
			if(($v != "") && ($k != "posted"))
				$messagemail .= $k." => ".$v."\n";
		}*/
		mail("Leo.Wandersleb@stusta.de", "new conference-guest", $messagemail,"Content-type: text/plain; charset=UTF-8\r\n");
		mail($HTTP_POST_VARS["mail"], "Your participation at the FFII-Conference June 1st.", $messagemail,"Content-type: text/plain; charset=UTF-8\r\n");
		echo "Your data has been submited. If the e-mail adress is correct you will receive an e-mail within minutes.";
		$showform = false;
	}
}
if($showform) {
echo "<form action=\"index.php\" method=\"post\"><input type=\"hidden\" name=\"posted\" value=\"1\">\n";

echo "<table>\n";

echo "<tr><td>Name:</td><td><input type=\"text\" name=\"name\" ".(isset($HTTP_POST_VARS["name"]) ? "value=\"".stripslashes($HTTP_POST_VARS["name"]) : "")."\" /></td><td>(Mr. Michael Mustermann)</td></tr>\n";
echo "<tr><td>Email:</td><td><input type=\"text\" name=\"mail\" ".(isset($HTTP_POST_VARS["mail"]) ? "value=\"".stripslashes($HTTP_POST_VARS["mail"]) : "")."\" /></td><td>(michael.mustermann@examplis.com)</td></tr>\n";
echo "<tr><td>Company:</td><td><input type=\"text\" name=\"company\" ".(isset($HTTP_POST_VARS["company"]) ? "value=\"".stripslashes($HTTP_POST_VARS["company"]) : "")."\" /></td><td>(Examplis GmbH)</td></tr>\n";
echo "</table>\n\n";

echo "<br /><br />If as speaker or specially invited guest you have been waived the accomodation fee please fill in your data anyway!<br /><br />";

echo "<table border=\"1\">\n";
echo "<tr bgcolor=\"#797979\"><td><input type=\"radio\" name=\"security\" value=\"parl_sec\"".(($HTTP_POST_VARS["security"] == "parl_sec") ? " checked" : "")."></td><td colspan=\"2\">I will participate in the morning session (Parliament)<br />and I do not need accreditation to the Parliament,<br />because I already have access.</td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td rowspan=\"5\"><input type=\"radio\" name=\"security\" value=\"parl_check\"".(($HTTP_POST_VARS["security"] == "parl_check") ? " checked" : "")."></td><td colspan=\"2\">I will participate in the morning session (Parliament).<br />Here are more details to my adress:</td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td width=\"100%\">Address as on passport/id card Street address:</td><td><input type=\"text\" name=\"padress\" ".(isset($HTTP_POST_VARS["padress"]) ? "value=\"".stripslashes($HTTP_POST_VARS["padress"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Address as on passport/id card Zip code:</td><td><input type=\"text\" name=\"pzip\" ".(isset($HTTP_POST_VARS["pzip"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pzip"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Address as on passport/id card City:</td><td><input type=\"text\" name=\"pcity\" ".(isset($HTTP_POST_VARS["pcity"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcity"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Address as on passport/id card Country:</td><td><input type=\"text\" name=\"pcountry\" ".(isset($HTTP_POST_VARS["pcountry"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcountry"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td><input type=\"radio\" name=\"security\" value=\"parl_no\"".(($HTTP_POST_VARS["security"] == "parl_no") ? " checked" : "")."></td><td colspan=\"2\">I will <b>not</b> participate in the morning session (Parliament)</td></tr>\n";
echo "<tr><td colspan=\"3\">&nbsp;</td></tr>";
echo "<tr bgcolor=\"#694900\"><td><input type=\"checkbox\" name=\"afternoon\"".(isset($HTTP_POST_VARS["afternoon"]) ? " checked" : "")."></td><td colspan=\"2\">
	I will participate in the afternoon session (Renaissance Hotel)</td></tr>\n";
echo "</table>";
echo "<br />";
echo "<table>";
echo "<tr><td>I need a room for the following dates:</td><td>31 May</td><td>01 June</td></tr>";
echo "<tr bgcolor=\"#797979\"><td width=\"100%\">Single room at Renaissance Hotel&sup1; Rue du Parnasse 19, +32 2 5052929, 3 mins from Parliament. Our group rate is at EUR&nbsp;159 incl. breakfast.
	</td><td><input type=\"radio\" name=\"room0531\" value=\"sRen\""
	.(($HTTP_POST_VARS["room0531"] == "sRen") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"sRen\""
	.(($HTTP_POST_VARS["room0601"] == "sRen") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Single Room at Maison du Dragon&sup2;, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 70 / night.
	</td><td><input type=\"radio\" name=\"room0531\" value=\"sDra\""
	.(($HTTP_POST_VARS["room0531"] == "sDra") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"sDra\""
	.(($HTTP_POST_VARS["room0601"] == "sDra") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Double Room at Maison du Dragon&sup2;, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.
	</td><td><input type=\"radio\" name=\"room0531\" value=\"2Dra\""
	.(($HTTP_POST_VARS["room0531"] == "2Dra") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"2Dra\""
	.(($HTTP_POST_VARS["room0601"] == "2Dra") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Shared Double Room at Maison du Dragon&sup2;, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 40 / night.
	</td><td><input type=\"radio\" name=\"room0531\" value=\"dDra\""
	.(($HTTP_POST_VARS["room0531"] == "dDra") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"dDra\""
	.(($HTTP_POST_VARS["room0601"] == "dDra") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>Dorm bed at Van Gogh Hostel, Rue de Traversieres, metro Rogiers at EUR 16.50 / night.
	</td><td><input type=\"radio\" name=\"room0531\" value=\"host\""
	.(($HTTP_POST_VARS["room0531"] == "host") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"host\""
	.(($HTTP_POST_VARS["room0601"] == "host") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>no room
	</td><td><input type=\"radio\" name=\"room0531\" value=\"none\""
	.(($HTTP_POST_VARS["room0531"] == "none") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"none\""
	.(($HTTP_POST_VARS["room0601"] == "none") ? " checked" : "")."></td></tr>\n";

echo "</table><br />\n";
echo "<table><tr><td valign=\"top\">&sup1;</td><td><font size=-1>WLAN options: 1hr at EUR&nbsp;9.95, 1 day at EUR&nbsp;19.95</font></td></tr>\n";
echo        "<tr><td valign=\"top\">&sup2;</td><td><font size=-1>Maison Du Dragon includes 24h of free WLAN (but only 24h are free even if you stay 2 nights), but you also can buy 1h extra (for checking mail at EUR 10 if needed or 24h for EUR 20).</font></td></tr></table>\n";

echo "<br />If you have other needs (arriving earlier, leaving later, special diet or similar) then please contact <a href='mailto:lwanders@ffii.org'>lwanders@ffii.org</a>!<br /><br />";

echo "<center><input type=\"submit\" value=\"Register\"></center></form>\n";
}
?>
</td><td width=20%></td></tr></table><br /><br />
<hr /><font size=-2>&copy;2005 Leo Wandersleb. If you have questions/problems concerning this page, please mail to wandersl@in.tum.de with a subject containing ffii!</font>
</body>
</html>
