<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
Economic Majority 29 June 2005 Brussels Conference
</title>
<link href="main.css" rel="stylesheet" type="text/css">
</head>
<body>
<center><h2>Registration Site for the <a href="http://wiki.ffii.org/Konf050629En">Economic Majority Conference</a> on 29 June 2005<br /></h2></center>
<table><tr><td width="20%"></td>
<?php

function make_seed() {
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}
$showform=true;
if(isset($HTTP_POST_VARS["posted"])) {
	$errormessage = "";
	if($HTTP_POST_VARS["name"] == "") {
		$errormessage .= "Please enter a name!<br />\n";
	}
	if($HTTP_POST_VARS["mail"] == "") {
		$errormessage .= "Please enter a valid email address! It is needed to confirm your registration.<br />\n";
	}
	if(!isset($HTTP_POST_VARS["security"]))
		$errormessage .= "Please check whether you need accreditation or not!<br />\n";
	if(isset($HTTP_POST_VARS["security"]) && ($HTTP_POST_VARS["security"] == "parl_check")) {
		if( ($HTTP_POST_VARS["padress"] == "") ||
			($HTTP_POST_VARS["pzip"] == "") ||
			($HTTP_POST_VARS["pcity"] == "") ||
			($HTTP_POST_VARS["pcountry"] == "")) {
			$errormessage .= "To be allowed to enter the Parliament it is obligatory to provide detailed information on your address. Please fill in all address details.<br />\n";
		}
	}
	if(!isset($HTTP_POST_VARS["room0628"]) && (!isset($HTTP_POST_VARS["security"]) || ($HTTP_POST_VARS["security"] != "parl_sec")))
		$errormessage .= "Please let us know whether you need a room for 28 June!<br />\n";
	if(!isset($HTTP_POST_VARS["room0629"]) && (!isset($HTTP_POST_VARS["security"]) || ($HTTP_POST_VARS["security"] != "parl_sec")))
		$errormessage .= "Please let us know whether you need a room for 29 June!<br />\n";
	if ($errormessage != "") {
		echo "<center><font color=\"#FF0000\">".$errormessage."</font></center><br />\n";
	} else {
		srand(make_seed());
		$randkey=rand(100000,999999);
		$dbh = pg_connect("dbname=bxl0506 user=regis password=sireg") or
		die("ERROR in connect!");
		
		$some1 = pg_query($dbh, "insert into person (event, name, mail, part_morning, sleep0, sleep1, sleep2, sleep3, company, street, zip_code, city, country, dob, randkey) values (1,'".
		$HTTP_POST_VARS["name"]."','".
		$HTTP_POST_VARS["mail"]."','".
		(($HTTP_POST_VARS["security"] == "parl_sec"  ) ? "1" : "").
		(($HTTP_POST_VARS["security"] == "parl_check") ? "2" : "").
		(($HTTP_POST_VARS["security"] == "parl_no"   ) ? "3" : "")."','".
		$HTTP_POST_VARS["room0627"]."','".
		$HTTP_POST_VARS["room0628"]."','".
		$HTTP_POST_VARS["room0629"]."','".
		$HTTP_POST_VARS["room0630"]."','".
		$HTTP_POST_VARS["company"]."','".
		$HTTP_POST_VARS["padress"]."','".
		$HTTP_POST_VARS["pzip"]."','".
		$HTTP_POST_VARS["pcity"]."','".
		$HTTP_POST_VARS["pcountry"]."','".
		$HTTP_POST_VARS["dob"]."',".
		$randkey.");");
		$oid = pg_last_oid($some1);
		$some1 = pg_query($dbh, "select id2 from person where oid=$oid;");
		$some2 = pg_fetch_array($some1);
		//print_r($some2);
		$id = $some2["id2"];
		pg_close($dbh);
		$messagemail  = "Dear ".$HTTP_POST_VARS["name"]."!\n\n";
		$messagemail .= "Welcome to the Economic Majority Conference! Please check the\n following entry carefully and, if everything is right, finalize\n your registration by clicking the link provided at the end of this mail.\n";
		switch($HTTP_POST_VARS["security"]) {
			case "parl_sec"  : $messagemail .= "You will participate in the conference and you do not need accreditation to \nthe Parliament, because you are already permanently accredited to the Parliament.\n";
			break;
			case "parl_check": $messagemail .= "You will participate in the conference and you need accreditation to \nthe Parliament. The following data is not inconsistent with \nyour passport/id card:\n".
			"  Street address :".$HTTP_POST_VARS["padress"].
			"\n  Zip code       :".$HTTP_POST_VARS["pzip"].
			"\n  City           :".$HTTP_POST_VARS["pcity"].
			"\n  Country        :".$HTTP_POST_VARS["pcountry"].
			"\n  Date of birth  :".$HTTP_POST_VARS["dob"];
		}
		$messagemail .= "\n\nFor 27 June you need:";
		switch ($HTTP_POST_VARS["room0627"]) {
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 98 / night.\n";
			break;
			default: $messagemail .= " No room.\n";
		}
		$messagemail .= "\nFor 28 June (night before the conference) you need:";
		switch ($HTTP_POST_VARS["room0628"]) {
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 98 / night.\n";
			break;
			default: $messagemail .= " No room.\n";
		}
		$messagemail .= "\nFor 29 June (night after the conference) you need:";
		switch ($HTTP_POST_VARS["room0629"]) {
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 98 / night.\n";
			break;
			default: $messagemail .= " No room.\n";
		}
		$messagemail .= "\nFor 30 June you need:";
		switch ($HTTP_POST_VARS["room0630"]) {
			case "sDra": $messagemail .= " Single Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n";
			break;
			case "2Dra": $messagemail .= " Double Room at Maison du Dragon, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 98 / night.\n";
			break;
			default: $messagemail .= " No room.\n";
		}
		$messagemail .= "\n\nIf the data is correct, please click at http://swpat.ffii.org/events/2005/bxl0629/register/validate.php?id=".$id."&randkey=".$randkey."\nor (especially in the case that your email program has inserted a line break) manually copy it to your browser.\nIf you want to change your registration, please reenter your data at http://demo.ffii.org/bxl0506\n\nGreetings,\n    your registration script.";
	
/*		foreach($HTTP_POST_VARS as $k => $v) {
			if(($v != "") && ($k != "posted"))
				$messagemail .= $k." => ".$v."\n";
		}*/
		mail("blasum@ffii.org", "new conference-guest", $messagemail,"Content-type: text/plain; charset=UTF-8\r\n");
		mail($HTTP_POST_VARS["mail"], "Your participation at the economic majority conference 29 June 2005.", $messagemail,"Content-type: text/plain; charset=UTF-8\r\n");
		echo "Your registration has been submitted. If the e-mail address is correct you will receive an e-mail within minutes.";
		$showform = false;
	}
}
if($showform) {
echo "<!-- BEGINN FORMULARTABELLE -->\n
<td width=\"60%\" bgcolor=\"#FFFFFF\">\n
<div id=\"komplett\">\n
</p>\n
<form action=\"index.php\" method=\"post\" ><input type=\"hidden\" name=\"posted\" value=\"1\">\n
<b>Badge and contact</b>\n
<div id=\"tabelle1\">\n
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n
<tr >\n
    <td height=\"7\" class=\"linieoben\"></td>\n
    <td colspan=\"2\" class=\"linieoben\"></td>\n
  </tr>\n
<tr><td>Name:</td><td><input type=\"text\" name=\"name\" ".(isset($HTTP_POST_VARS["name"]) ? "value=\"".stripslashes($HTTP_POST_VARS["name"]) : "")."\" /></td><td>(Mr. Michael Mustermann)</td></tr>\n
<tr><td>Company:</td><td><input type=\"text\" name=\"company\" ".(isset($HTTP_POST_VARS["company"]) ? "value=\"".stripslashes($HTTP_POST_VARS["company"]) : "")."\" /></td><td>(Examplis GmbH)</td></tr>\n
<tr><td>Email:</td><td><input type=\"text\" name=\"mail\" ".(isset($HTTP_POST_VARS["mail"]) ? "value=\"".stripslashes($HTTP_POST_VARS["mail"]) : "")."\" /></td><td>(michael.mustermann@examplis.com)</td></tr>\n
<tr >\n
    <td height=\"7\" class=\"linieunten\"></td>\n
    <td colspan=\"2\" class=\"linieunten\"></td>\n
  </tr>\n
</table>\n
</div>\n
<br><br><b>Accreditation</b>\n
<div id=\"tabelle2\">\n
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n
<tr >\n
  <td colspan=\"2\" class=\"liniekomplett\">I will participate in the conference and I do <i>not</i> need accreditation to the Parliament, because I am already permanently accredited to the Parliament.</td>\n
  <td width=\"100\" class=\"linierechtsuntenoben\"><div align=\"center\">\n
    <input name=\"security\" type=\"radio\" value=\"parl_sec\"".(($HTTP_POST_VARS["security"] == "parl_sec") ? " checked" : "").">\n
  </div></td>\n
</tr>\n
<tr ><td colspan=\"2\" class=\"linierechtslinksunten\">I will participate in the conference and I need accreditation to the Parliament. Here are more details on my address:</td>\n
  <td class=\"linierechtsunten\"><div align=\"center\">\n
    <input name=\"security\" type=\"radio\" value=\"parl_check\" ".(($HTTP_POST_VARS["security"] == "parl_check") ? " checked" : "").">\n
  </div></td>\n
</tr>\n
</table>\n
</div>\n
<div id=\"tabelle3\">\n
<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n
  <tr >\n
    <td>Street address consistent with passport/id card&sup1;:</td>\n
    <td><input type=\"text\" name=\"padress\" ".(isset($HTTP_POST_VARS["padress"]) ? "value=\"".stripslashes($HTTP_POST_VARS["padress"]) : "")."\" /></td>\n
  </tr>\n
  <tr >\n
    <td>Zip code consistent with passport/id card&sup1;:</td>\n
    <td><input type=\"text\" name=\"pzip\" ".(isset($HTTP_POST_VARS["pzip"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pzip"]) : "")."\" /></td>\n
  </tr>\n
  <tr >\n
    <td>City consistent with passport/id card&sup1;:</td>\n
    <td><input type=\"text\" name=\"pcity\" ".(isset($HTTP_POST_VARS["pcity"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcity"]) : "")."\" /></td>\n
  </tr>\n
  <tr >\n
    <td>Country consistent with passport/id card:</td>\n
    <td><input type=\"text\" name=\"pcountry\" ".(isset($HTTP_POST_VARS["pcountry"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcountry"]) : "")."\" /></td>\n
  </tr>\n
  <tr >\n
    <td>Date of birth (please use YYYY-MM-DD)</td>\n
    <td><input type=\"text\" name=\"dob\" ".(isset($HTTP_POST_VARS["dob"]) ? "value=\"".stripslashes($HTTP_POST_VARS["dob"]) : "")."\" /></td>\n
  </tr>\n
  <tr >\n
    <td height=\"7\" class=\"linieunten\"></td>\n
    <td class=\"linieunten\"></td>\n
  </tr>\n
</table>\n
</div>\n
<br><br><b>Accomodation</b>\n
<div id=\"tabelle4\">\n
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr class=\"linieuntenoben\">\n
  <td class=\"liniekomplett\">I need a room&sup2; for the following dates <i>(28 June is the night before the conference, 29 June is the night after the conference)</i>:</td>\n
  <td class=\"linierechtsuntenoben\"><div align=\"center\">27 June</div></td><td class=\"linierechtsuntenoben\"><div align=\"center\">28 June</div></td><td class=\"linierechtsuntenoben\"><div align=\"center\">29 June</div></td><td class=\"linierechtsuntenoben\"><div align=\"center\">30 June</div></td></tr><tr><td class=\"linierechtslinksunten\">Single Room at Maison du Dragon&sup3;, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 80 / night.\n
	</td>\n
  <td class=\"linierechtsunten\"><div align=\"center\">\n
	  <input type=\"radio\" name=\"room0627\" value=\"sDra\""
	          .(($HTTP_POST_VARS["room0627"] == "sDra") ? " checked" : "").">\n
	  </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	    <input type=\"radio\" name=\"room0628\" value=\"sDra\""
	            .(($HTTP_POST_VARS["room0628"] == "sDra") ? " checked" : "").">\n
	    </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	      <input type=\"radio\" name=\"room0629\" value=\"sDra\""
	              .(($HTTP_POST_VARS["room0629"] == "sDra") ? " checked" : "").">\n
	      </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	        <input type=\"radio\" name=\"room0630\" value=\"sDra\""
		        .(($HTTP_POST_VARS["room0629"] == "sDra") ? " checked" : "").">\n
	        </div></td></tr>\n
<tr><td class=\"linierechtslinksunten\">Double Room at Maison du Dragon&sup3;, Boulevard Adolphe Max 146-160, metro Rogiers, Tel +32-2-2501020 at EUR 98 / night.\n
	</td><td class=\"linierechtsunten\"><div align=\"center\">\n
	  <input type=\"radio\" name=\"room0627\" value=\"2Dra\""
	          .(($HTTP_POST_VARS["room0627"] == "2Dra") ? " checked" : "").">\n
	  </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	    <input type=\"radio\" name=\"room0628\" value=\"2Dra\""
	            .(($HTTP_POST_VARS["room0628"] == "2Dra") ? " checked" : "").">\n
	    </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	      <input type=\"radio\" name=\"room0629\" value=\"2Dra\""
	              .(($HTTP_POST_VARS["room0629"] == "2Dra") ? " checked" : "").">\n
	      </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	        <input type=\"radio\" name=\"room0630\" value=\"2Dra\""
		        .(($HTTP_POST_VARS["room0630"] == "2Dra") ? " checked" : "").">\n
	        </div></td></tr>\n
<tr><td class=\"linierechtslinksunten\">No room needed.\n
	</td><td class=\"linierechtsunten\"><div align=\"center\">\n
	  <input name=\"room0627\" type=\"radio\" value=\"none\""
	          .(($HTTP_POST_VARS["room0627"] == "none") ? " checked" : "").">\n
	  </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	    <input name=\"room0628\" type=\"radio\" value=\"none\""
	            .(($HTTP_POST_VARS["room0628"] == "none") ? " checked" : "").">\n
	    </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	      <input name=\"room0629\" type=\"radio\" value=\"none\""
	              .(($HTTP_POST_VARS["room0629"] == "none") ? " checked" : "").">\n
	      </div></td><td class=\"linierechtsunten\"><div align=\"center\">\n
	        <input name=\"room0630\" type=\"radio\" value=\"none\""
		        .(($HTTP_POST_VARS["room0630"] == "none") ? " checked" : "").">\n
	        </div></td></tr>\n
</table>\n
</div>\n
<br />\n
&sup1;<font size=-1>If your passport/id card does not show any address, then obviously any address within your country is consistent with its address data.</font>\n
<br>&sup2;<font size=-1>If as speaker or specially invited guest you have been waived the accomodation fee please fill in your accomodation data anyway!</font>\n
<br>&sup3;<font size=-1>WLAN options: 1hr at EUR&nbsp;9.95, 1 day at EUR&nbsp;19.95.</font>\n
<p>If you have other needs (arriving earlier, leaving later, special diet or similar) then please contact <a href='mailto:blasum@ffii.org'>blasum@ffii.org</a>!<br /><br /><center><input type=\"submit\" value=\"Register\"></center></form>\n
</div>\n
</td>\n
<!-- ENDE FORMULARTABELLE -->\n
";
}
?>
</td><td width=20%></td></tr></table><br /><br />
<hr /><font size=-2>&copy;2005 Leo Wandersleb. If you have questions/problems concerning this page, please mail to holgerlists@blasum.net with a subject containing ffii!</font>
</body>
</html>
