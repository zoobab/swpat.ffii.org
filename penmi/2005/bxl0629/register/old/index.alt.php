<html>
<head>
<title>
Foundation for a Free Information Infrastructure - Brussels, June 2005, Conference
</title>
</head>
<body bgcolor=#000070 text=#FFCC99 link=#8888CC vlink=#FF9966 alink=#FFFFFF>
<center><h2>Registration-Site for <a href =http://wiki.ffii.org/Konf0506En>FFII Software Patents Conference</a> on<br />June 01 2005<br /></h2></center>
<table><tr><td width="20%"></td><td width="60%">
<?php
//
//include "functions.php";

//print_r($HTTP_POST_VARS);
$showform=true;
if(isset($HTTP_POST_VARS["posted"])) {
	$errormessage = "";
	if($HTTP_POST_VARS["name"] == "") {
		$errormessage .= "Please enter a name!<br />\n";
	}
	if($HTTP_POST_VARS["mail"] == "") {
		$errormessage .= "Please enter a valid mailadress! It is needed to confirm your registration.<br />\n";
	}
	if(!isset($HTTP_POST_VARS["security"]))
		$errormessage .= "Please check whether to participate in the morning session (Parliament) or not!<br />\n";
	if(isset($HTTP_POST_VARS["security"]) && ($HTTP_POST_VARS["security"] == "parl_check")) {
		if( ($HTTP_POST_VARS["padress"] == "") ||
			($HTTP_POST_VARS["pzip"] == "") ||
			($HTTP_POST_VARS["pcity"] == "") ||
			($HTTP_POST_VARS["pcountry"] == "")) {
			$errormessage .= "To be allowed to enter the Parliament it is obligatory to provide detailed information on your adress. Please fill in all adressdetails.<br />\n";
		}
	}
	if(!isset($HTTP_POST_VARS["room0531"]))
		$errormessage .= "Please let us know whether you need a room for May 31!<br />\n";
	if(!isset($HTTP_POST_VARS["room0601"]))
		$errormessage .= "Please let us know whether you need a room for June 01!<br />\n";
	if ($errormessage != "") {
		echo "<center><font color=\"#FF0000\">".$errormessage."</font></center><br />\n";
	} else {
		$messagemail = "";
		foreach($HTTP_POST_VARS as $k => $v) {
			if(($v != "") && ($k != "posted"))
				$messagemail .= $k." => ".$v."\n";
		}
		mail("Leo.Wandersleb@stusta.de", "new conference-guest", $messagemail);
		echo "Your data has been submited. If the e-mail adress is correct you will receive an e-mail within minutes.";
		$showform = false;
	}
}
if($showform) {
echo "<form action=\"index_alt.php\" method=\"post\"><input type=\"hidden\" name=\"posted\" value=\"1\">\n";

echo "<table>\n";

echo "<tr><td>Name:</td><td><input type=\"text\" name=\"name\" ".(isset($HTTP_POST_VARS["name"]) ? "value=\"".stripslashes($HTTP_POST_VARS["name"]) : "")."\" />(Mr. Michael Mustermann)</td></tr>\n";
echo "<tr><td>Email:</td><td><input type=\"text\" name=\"mail\" ".(isset($HTTP_POST_VARS["mail"]) ? "value=\"".stripslashes($HTTP_POST_VARS["mail"]) : "")."\" />(michael.mustermann@examplis.com)</td></tr>\n";
echo "<tr><td>Company:</td><td><input type=\"text\" name=\"company\" ".(isset($HTTP_POST_VARS["company"]) ? "value=\"".stripslashes($HTTP_POST_VARS["company"]) : "")."\" />(Examplis GmbH)</td></tr>\n";
echo "</table><br />\n\n";


echo "<table border=\"1\">\n";
echo "<tr bgcolor=\"#690000\"><td><input type=\"radio\" name=\"security\" value=\"parl_sec\"".(($HTTP_POST_VARS["security"] == "parl_sec") ? " checked" : "")."></td><td colspan=\"2\">I will participate in the morning session (Parliament)<br />and I do not need accreditation to the Parliament,<br />because I already have access.</td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td rowspan=\"5\"><input type=\"radio\" name=\"security\" value=\"parl_check\"".(($HTTP_POST_VARS["security"] == "parl_check") ? " checked" : "")."></td><td colspan=\"2\">I will participate in the morning session (Parliament).<br />Here are more details to my adress:</td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td width=\"100%\">Address as on passport/id card Street address:</td><td><input type=\"text\" name=\"padress\" ".(isset($HTTP_POST_VARS["padress"]) ? "value=\"".stripslashes($HTTP_POST_VARS["padress"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td>Address as on passport/id card Zip code:</td><td><input type=\"text\" name=\"pzip\" ".(isset($HTTP_POST_VARS["pzip"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pzip"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td>Address as on passport/id card City:</td><td><input type=\"text\" name=\"pcity\" ".(isset($HTTP_POST_VARS["pcity"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcity"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td>Address as on passport/id card Country:</td><td><input type=\"text\" name=\"pcountry\" ".(isset($HTTP_POST_VARS["pcountry"]) ? "value=\"".stripslashes($HTTP_POST_VARS["pcountry"]) : "")."\" /></td></tr>\n";
echo "<tr bgcolor=\"#690000\"><td><input type=\"radio\" name=\"security\" value=\"parl_no\"".(($HTTP_POST_VARS["security"] == "parl_no") ? " checked" : "")."></td><td colspan=\"2\">I will <b>not</b> participate in the morning session (Parliament)</td></tr>\n";
echo "<tr><td colspan=\"3\">&nbsp;</td></tr>";
echo "<tr bgcolor=\"#694900\"><td><input type=\"checkbox\" name=\"afternoon\"".(isset($HTTP_POST_VARS["afternoon"]) ? " checked" : "")."></td><td colspan=\"2\">
	I will participate in the afternoon session (Renaissance Hotel)</td></tr>\n";
echo "</table>";
echo "<br />";
echo "<table>";
echo "<tr><td>I need a room for the following dates:</td><td>31 May</td><td>01 June</td></tr>";
echo "<tr bgcolor=\"#797979\"><td width=\"100%\">single room at Renaissance Hotel&sup1;
	</td><td><input type=\"radio\" name=\"room0531\" value=\"sRen\""
	.(($HTTP_POST_VARS["room0531"] == "sRen") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"sRen\""
	.(($HTTP_POST_VARS["room0601"] == "sRen") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>single room at Le Dragon Hotel&sup2;
	</td><td><input type=\"radio\" name=\"room0531\" value=\"sDra\""
	.(($HTTP_POST_VARS["room0531"] == "sDra") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"sDra\""
	.(($HTTP_POST_VARS["room0601"] == "sDra") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>shared double room at Le Dragon Hotel&sup2;
	</td><td><input type=\"radio\" name=\"room0531\" value=\"dDra\""
	.(($HTTP_POST_VARS["room0531"] == "dDra") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"dDra\""
	.(($HTTP_POST_VARS["room0601"] == "dDra") ? " checked" : "")."></td></tr>\n";
echo "<tr bgcolor=\"#797979\"><td>no room
	</td><td><input type=\"radio\" name=\"room0531\" value=\"none\""
	.(($HTTP_POST_VARS["room0531"] == "none") ? " checked" : "").">
	</td><td><input type=\"radio\" name=\"room0601\" value=\"none\""
	.(($HTTP_POST_VARS["room0601"] == "none") ? " checked" : "")."></td></tr>\n";

echo "</table><br />\n";
echo "<table><tr><td valign=\"top\">&sup1;</td><td>Rue du Parnasse 19, +32 2 5052929, 3 mins from Parliament. Our group rate is at EUR&nbsp;159 incl. breakfast. WLAN options: 1hr at EUR&nbsp;9.95, 1 day at EUR&nbsp;19.95</td></tr>\n";
echo        "<tr><td>&sup2;</td><td></td></tr></table>\n";
echo "<center><input type=\"submit\" value=\"Register\"></center></form>\n";
}
?>
</td><td width=20%></td></tr></table><br /><br />
<hr /><font size=-2>&copy;2005 Leo Wandersleb. If you have questions/problems concerning this page, please mail to wandersl@in.tum.de with a subject containing ffii!</font>
</body>
</html>