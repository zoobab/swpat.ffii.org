Here are comments on the names on the list. I
have left the names by category, adding for
each category possible names. Our comments
are prefixed by '#'.

--------



B. Other possible experts
=========================

(1) Patent Judges
-----------------

Dorn
Opnesgaard 81 3.T.V.
2970 Horsholm
Danmark

Sedemund-Treiber
Bundespatentgericht
Meisenweg 14
53343 Wachtberg
Deutschland

Scuffi, Massimo
Corte d'Apello di Milano
Via Freguglia
20135 Milano
Italia

Boval, Bruno
Cour d'Appel de Paris
34, quai des Orf�vres
75001 Paris
France

Verougstraete
Cour de Cassation
Avenue des Narcisses, 2
1640 Rhode-St-Gen�se
Belgique/Belgi�

Linder
Court of Patent Appeals
Box 24160
10451 Stockholm
Sverige

Willems, Jan
European Patent Office
Erhardtstrasse 27
80331 M�nchen
Deutschland

Thomson
High Court
Sortedam Dossering 571
2100 Danmark
K�benhavn

Steinacker
Oberlandesgericht
Cecilienallee 3
40474 D�sseldorf
Deutschland

Jacob, Robin
Patents Court of England and Wales
London
United Kingdom

Laddie, Hugh
Patents Court of England and Wales
London
United Kingdom

Pumfrey, Nicholas
Patents Court of England and Wales
Strand
London
United Kingdom

Numann
Rechtbank Den Haag
Postbus 20302
2500 EH Den Haag
Nederland

# All of these are unknown.
# We propose some more, whom we know:
#
# - Hermann Prasch
#   judge of 17th senate of BPatG
#   (the 17th senate has fought hard against software patents and
#    often explicitely rejected EPO doctrines)
#
# - Klaus Melullis
#   presiding judge of BGH (Federal Court of Justice) patent senate,
#   (author of cautiously pro swpat articles in 1997-1998, recently concerned 
#    about broad claims, warned against deletion of Art 52.2c EPC in Nov 2000,
#    also very critical of the CEC directive proposal)

(2) Other contacts
==================

European Patent Office 
President: Dr Ingo Kober
Software expert: Mr Gert Kolle  

UK Patent Office: Robin Webb

# Both Kober and Webb are strongly pro swpat and quite
# clueless. Inviting them might help expose their
# cluelessness. It might also be interesting to invite
# Dr. Wolfgang Tauchert, from DPMA, who has written many
# pro swpat articles and is amiably clueless on some
# things but yet knows the subject much better than most.
# Gert Kolle originally wrote academic papers in GRUR
# stating that there were no room for patenting computer
# programs if the notion of technical invention is taken
# seriously. Kolle's early work was often cited by German
# courts as a foundation for their refusal to grant software
# patents. Later, Kolle was hired by the EPO, and is
# currently their head of diplomacy, speaking to justify
# current EPO software patenting policy...

International Chamber of Commerce (contact: Daphne Yong-d'Herv�)

# Most probably pro-swpat.

# If one is to invite representatives of patent offices,
# why not invite patent examiners, whose task is indeed
# to determine whether a patent should be granted or not, 
# and who usually understand much more about the problems
# than PTO management people.
# We propose:
# - Dr. Swen Kiesewetter-Koebinger, DPMA (DE-PTO)
# - Gunter Schoelch, DPMA
#   Both have written very critical and insightful articles
#   about swpat. Schoelch participated in the CEC Consultation of 2000,
#   was later subjected to disciplinary measures by the DPMA
#   managment for speaking out against their policy.


(3) Academics
=============

Professor Michel Vivant
Montpellier University
Authority in Informatic Law, including patents,
consultant of the European Commission's DG Infosoc,
author of many articles on swpat, which point out fallacies of the EPO.

Professor Joseph Straus (Max Planck Institute for IP Law, Munich) 

# Evangelist of unlimited patentability, particularly in the gene area 
# but also in software, leading advisor of EPO, WIPO and German Gov't,
# see 
#
#    http://swpat.ffii.org/players/straus/
#
# If you want to hear Straus and he declines to come in person, his assistant
# Ralph Nack could be an acceptable replacement.  Nack has participated in
# swpat related studies and propounds the same views as his teacher.
	
Professor Paul Vaver (Director, Oxford University IP Research Centre) 

# unknown.

Peter Holmes 
Univ of Sussex 
Author of the economic part of the CEC
study that is cited by DGMarkt in the Directive Proposal
Wrote the chapter of the study on economic effects of swpat
is critical of swpat and of the other authors of this study

Professor Anja Hucke
patent law and economic law at Munich Technical University
works on EU patent policy, has criticised acceptance of illegal patents,
including swpat, by EPO and courts.

Professor P. Bernt Hugenholz, University of Amsterdam
(responsible for the DG4 study together with Reiner Bakels)

Professor Brian Kahin
University of Maryland, US
Information Policy Research Center
Advisor to Bill Clinton and Al Gore, long term researcher on Swpat
author of an interesting contribution to the CEC Consultation of 2000
good orator and presenter, very critical of swpat

Professor KONNO Hiroshi
Chuo University, Tokyo, Japan
Leading mathematician and author of bestselling books on math and software patents 

# Rather against swpats, but especially Bakels has
# difficulty understanding epistemological terms and
# tries to reduce everything to triviality, saying
# that non-trivial swpat are good (but not citing any
# examples). Hugenholtz is a good orator and should
# especially be consulted about the subject of how to
# integrate patents and copyright.
# There are of course a few more academics to invite, e.g.
# - Daniel Probst, economist, univ. of Mannheim
#   clear thought and good rhetoric, makes it
#   clear that economic evidence speaks against swpat
#
#   http://swpat.ffii.org/conferences/2001/bundestag/probst/


(4) Professional bodies
=======================

FICPI (international federation of patent practitioners)
Chair for European Committee: Dipl-Ing Helmut Sonn, Riemergasse 14, A-1010 Wien
# Sonn seems to be a very naive patent believer
#
# Alternative:
#
# Deutsche Patentanwaltskammer (German Chamber of Patent Lawyers)
# PA Dr Raimar Koenig, Chief Editor of "Mitteilungen der Patentanwaelte",
# author of well-written articles with profound legal knowledge, tends to 
# be pro swpat but also says openly that EPO "has done violence to the law"
# in order to make them possible.
 
# Here you should strongly push so as to get
# *software* professional bodies, such as:
# - PROSA (union of software practitioners, Denmark: www.prosa.dk)
# - Syntec (the same for France: www.syntec.fr)
# - ISOC (Internet Society: www.isoc-eec.org)

(5) Individual practitioners with particular interest in software patents
=========================================================================

J�rgen Betten
Munich, of Betten & Resch (pleaded and won Sohei case)
president of UNION (software patent practitioners from  a number of European countries; tel. 0049-89-2424170)

# Indeed a good person to invite: capable of winning
# aversion from the public...

Fritz Teufel: IBM Stuttgart: pleaded and won IBM cases

# ...And wrote the position papers for Bitkom, EICTA and
# indirectly the German and European industry
# associations BDI and EUNICE. A most influent lobbying
# person.

Keith Beresford: well-known expert, gives many presentations on this issue.

# Author of "Patenting Software under the European Patent Convention"
# British patent attorney, his book has been sold for 500 pounds, teaches people
# how to get around Art 52 EPC.  About 60 years old, pro swpat, good orator, 
# has little idea of software, even less of software economics

(6) Industry bodies
===================

ECIS (contacts: Isabelle Chatelier; Alex Silverman)

EICTA (European Information and Communications Technology Industry
Association) (contact: Ana Garc�a, EU Affairs Manager

Association francaise des entreprises priv�es AFEP-AGREF (contact: Gerard Vuillermet)

UNICE

EU Committee of American Chamber of Commerce

Business Software Alliance

# Francisco Mingorance, from BSA, seemed to have helped
# the DGMarkt to write its swpat directive proposal...
# All of the above are clearly pro-swpat. For ECIS
# and UNICE, it is strictly equivalent to inviting
# Fritz Teufel in person...

# All of these are only pro-swpat organisations.
# You should push so as to invite also:
# - DIHK (German Chamber of Commerce, Ms Doris Moeller)
# - sintec.fr (Ms Anne de la Tour)
# - prosa.dk (already listed in section 4)
# - fenit.nl (Mr. Peter Schelven, who agreed on a common
#   paper with Luuk Van Dijk of VOSN.nl)
# - ISOC Europe (see Patrick Van de Walle)
# - monopolkommission.de (Monopoly Commission, economists, recently
#   wrote a german governmental report that strongly argues against swpat)  

(6) Large individual companies
==============================

Siemens (Wilhelm v. Lieres, corporate IP counsel)
Nokia
Ericcson
IBM
Microsoft

# IBM and Microsoft will always be pro swpat, as it
# allows them to screw more and more competitors.
# For most large companies, IP departments are
# for, because swpats are what they earn their
# living from, while CTOs are against. So try to
# get CTOs rather than IP people.
# Other companies :
# - ILOG
#   Pierre Haren, director, already cited by Zimeray
# - SAP
#   (not their patent department head but CTO or board director Prof. Plattner!)
# - CAP GEMINI
#   Mr Ron Tolido
# - Phaidros AG
#   (Ms Elke Bouillon, who attended the Bitkom IP committee meeting,
#   chaired by Fritz Teufel, in which 7 megacorp patent lawyers decided against
#   her to support the CEC proposal)
# - Opera.com: CTO (H�kon Wium Lie) or CEO
#   both have a consistent policy of speaking up against Swpat
#   Opera is a leader in various fields and the only
#   browser alternative to Netscape and MS IE
#   Opera also writes all the software for Nokia, whose patent department
#   says in public that Nokia needs swpat.
# - Beta Research, Germany
# - MySQL, Sweden
# - Trolltech, Norway 

(7) Open Source/shareware
=========================

Hartmut Pilch (Eurolinux Alliance)

# Hartmut Pilch was already proposed by Zimeray.  However, it seems
# more important to cite him as a patent expert rather than a free
# software advocate. Trying to confuse anti-swpat and
# pro-free-software is a trap we must not fall into.

Sk�ne Sjaelland Linux User Group (SSLUG)

# Yes. Maybe Peter Toft (pto@sslug.dk) could come.
# Peter Toft recently did little on swpat.  One could also consider
# one of
#
#   Carsten Svaneborg (main author of softwarepatenter.dk)
#   Erik Josefsson (author of a SSLUG paper on swpat)
#   Anne Ostergaard (spokeswoman with legal background
#                    who can win sympathy with a short statement)
                                         
Sylvain Perchaud
president
Association for the Promotion of European Authors of Shareware 

# Good understanding of software economics and software patents, lots
# of examples of shareware authors threatened by IBM and such.

# Other possible associations and people:
# - Luuk van Dijk
#   Vereniging Open Source Nederland
#   (Luuk.van.Dijk@xs4all.nl)


(8) SMEs
========

Olof Axelson - WM-Data Ellips (software product manager)

# has written a letter to the swedish gov't in which he warns about
# devastating effects of swpat on software companies

# Lots of SMEs are against swpats. For instance, please
# try to invite:
# - Stefan Pollmeier
#   managing director of ESR Pollmeier GmbH
#   http://www.esr-pollmeier.de/swpat/
#   (electronics company, 40 employees, good orator, 
#    has intensively studied the problems of EPO swpat that are cluttering his field)

(9) Representative of the European Commission
=============================================

Anthony Howard, DG Markt

# Does not really know the issues. A good way to show to the EuroMPs
# that the DGMarkt is just endorsing EPO practice.  It might however be more 
# interesting to have masterminds such as John Mogg speak there.




