<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

sub swpatpenmi swpen04 swpen03 swpen02 swpen01 swpen00
on swpatpenmi dir dirs
style swpparl035 latex linknote : invisible
docstyle swpparl035 : plain
style swpparl035 plain bodyopts -- bgcolor "#F4FEF8" link "#0000AA" vlink "#0000AA" text "#004010" alink "#0000AA"
name swpen04 : 2004
label swpen04 : 2004
name swpen03 : 2003
label swpen03 : 2003
name swpen02 : 2002
label swpen02 : 2002
name swpen01 : 2001
label swpen01 : 2001
name swpen00 : 2000
label swpen00 : 2000
sub swpen04 SwpTest0411 SwpTest0404 Cebit04 SwpWipo0405 SwpDemo0405 SwpBund0403
grp SwpTest0411 SwpTest0404
grp SwpBund0403 SwpBund0401 SwpDeso0403
sub SwpDemo0405 DemoAmsterdam0405 DemoAthina0405 DemoBerlin0405 DemoDublin0405 DemoHelsinki0405 DemoKobenhavn0405 DemoLisboa0405 DemoLondon0405 DemoLuxemburg0405 DemoMadrid0405 DemoMuenchen0405 DemoParis0405 DemoRoma0405 DemoStockholm0405 DemoWien0405
langs DemoBerlin0405 de
langs DemoMuenchen0405 de
langs DemoWien0405 de
langs DemoStockholm0405 en sv
langs DemoParis0405 fr
langs DemoLondon0405 en
langs DemoAmsterdam0405 en nl
langs DemoDublin0405 en
langs DemoLisboa0405 en pt
langs DemoMadrid0405 en es
langs DemoRoma0405 en it
langs DemoLuxemburg0405 fr
langs DemoAthina0405 en he
langs DemoKobenhavn0405 en da
langs DemoHelsinki0405 en fi
name DemoBerlin0405 berlin
name DemoMuenchen0405 muenchen
name DemoWien0405 wien
name DemoStockholm0405 stockholm
name DemoParis0405 paris
name DemoLondon0405 london
name DemoAmsterdam0405 amsterdam
name DemoDublin0405 dublin
name DemoLisboa0405 lisboa
name DemoMadrid0405 madrid
name DemoRoma0405 roma
name DemoLuxemburg0405 luxemburg
name DemoAthina0405 athina
name DemoKobenhavn0405 kobenhavn
name DemoHelsinki0405 helsinki
label DemoBerlin0405 org_when "Berlin" "2004/05"
label DemoKobenhavn0405 org_when "Kobenhavn" "2004/05"
label DemoMuenchen0405 org_when "Muenchen" "2004/05"
label DemoWien0405 org_when "Wien" "2004/05"
label DemoParis0405 org_when "Paris" "2004/05"
label DemoAthina0405 org_when "Athina" "2004/05"
label DemoLuxemburg0405 org_when "Luxemburg" "2004/05"
label DemoAmsterdam0405 org_when "Amsterdam" "2004/05"
label DemoStockholm0405 org_when "Stockholm" "2004/05"
label DemoMadrid0405 org_when "Madrid" "2004/05"
label DemoRoma0405 org_when "Roma" "2004/05"
label DemoHelsinki0405 org_when "Helsinki" "2004/05"
label DemoLondon0405 org_when "London" "2004/05"
label DemoDublin0405 org_when "Dublin" "2004/05"
label DemoLisboa0405 org_when "Lisboa" "2004/05"
grp Cebit04 Systems04 Linuxtag04
grp Linuxtag04 Fosdem04 FlossTel0401
grp Systems04 Comtec04
sub swpen03 swpparl03 swplxtg037 swpoecd038 swpisch03A
grp swpisch03A swpwipo037 SwpIcrt0311
grp swplxtg037 swpcbit033 swpberl038
sub swpparl03 swpparl039 swpparl038 swpparl037 swpparl036 swpparl035 swpparl034 swpparl033 swpparl03ner
sub swpparl039 SwpParlMuc039 SwpParlBxl039 SwpParlSxb039 SwpParlMad039 SwpParlBol039 SwpParlVie039 SwpParlLon039 SwpParlBer039 SwpParlStu039
name SwpParlMuc039 muenchen
label SwpParlMuc039 urb-muc
name SwpParlStu039 stuttgart
langs SwpParlStu039 de
label SwpParlStu039 urb-stu
label SwpParlBer039 urb-ber
langs SwpParlMuc039 de
langs SwpParlBer039 de
name SwpParlSxb039 strasbourg
label SwpParlSxb039 urb-sxb
langs SwpParlSxb039 en fr de
name SwpParlBxl039 "bruxelles"
label SwpParlBxl039 urb-bxl
langs SwpParlBxl039 en fr
name SwpParlMad039 madrid
label SwpParlMad039 "Madrid"
langs SwpParlMad039 en es
name SwpParlBol039 bologna
label SwpParlBol039 "Bologna"
langs SwpParlBol039 en it
name SwpParlLon039 london
label SwpParlLon039 "London"
langs SwpParlLon039 en 
name SwpParlVie039 wien
label SwpParlVie039 urb-vie
langs SwpParlVie039 de
sub swpparl034 swpparl0349 swpparl0348
sub swpparl035 swpparl0358 swpparl0357 swpparl035ner swpparl035ren
sub swpparl0358 swpparl035815 swpparl035813 swpparl035809
sub swpparl036 swpparl03624 swpparl03617 swpparl03610 swpparl03603 
label swpparl03624 org_when "EP" "03/06/24"
label swpparl03617 org_when "EP" "03/06/17"
label swpparl03610 org_when "EP" "03/06/10"
label swpparl03603 org_when "EP" "03/06/03"
name swpparl03624 : "24"
name swpparl03617 : "17"
name swpparl03610 : "10"
name swpparl03603 : "03"
langs swpparl034 en
langs swpparl035ner en
attr swpparl035ner privat
attr swpparl03ner privat
name swpparl035ner nenri
langs swpparl035ren en
name swpparl035ren remna en contrib de beitrag
sub swpparl035ren swpparl035bkahin swpparl035llessig swpparl035mthompson swpparl035pholmes swpparl035ptang swpparl035amccarthy swpparl035jpsmets swpparl035rclark swpparl035rdewar swpparl035jhallen swpparl035brunge swpparl035rms swpparl035sbieder swpparl035deima swpparl035rsedlm swpparl035rbakels swpparl035jsiepm swpparl035jhalber swpparl035jcortell
name swpparl035bkahin bkahin
label swpparl035bkahin : "Brian KAHIN"
name swpparl035llessig llessig
label swpparl035llessig : "Lawrence LESSIG"
name swpparl035mthompson mthompson
label swpparl035mthompson : "Mozelle W. THOMPSON"
name swpparl035amccarthy amccarthy
label swpparl035amccarthy : "Arlene MCCARTHY"
name swpparl035rclark rclark
label swpparl035rclark : "Richard CLARK"
name swpparl035rdewar rdewar
label swpparl035rdewar : "Robert DEWAR"
name swpparl035rsedlm rsedlm
label swpparl035rsedlm : "Roman SEDLMAIER"
name swpparl035rbakels rbakels
label swpparl035rbakels : "Reinier BAKELS"
name swpparl035jsiepm jsiepm
label swpparl035jsiepm : "Jürgen SIEPMANN"
name swpparl035jsiepm jhalber
label swpparl035jsiepm : "Jozéf HALBERSZTADT"
name swpparl035jcortell jcortell
label swpparl035jcortell : "Jorge Cortell"
name swpparl035rms rms
label swpparl035rms : "Richard STALLMAN"
name swpparl035jpsmets jpsmets
label swpparl035jpsmets : "Jean-Paul SMETS"
name swpparl035jhallen jhallen
label swpparl035jhallen : "Jacob HALLEN"
name swpparl035jhallen daxmark
label swpparl035jhallen : "David AXMARK"
name swpparl035brunge brunge
label swpparl035brunge : "Bernhard RUNGE"
name swpparl035sbieder sbieder
label swpparl035sbieder : "Sven BIEDERMANN"
name swpparl035deima deima
label swpparl035deima : "DEIM Agoston"
name swpparl035ptang ptang
label swpparl035ptang : "Puay TANG"
name swpparl035pholmes pholmes
label swpparl035pholmes : "Peter HOLMES"
label swpparl035ner nenri
langs swpparl035ner en de
langs swpparl035 en de fr
name swpparl037 "07"
langs swpparl037 en
name swpparl036 "06"
langs swpparl036 en
name swpparl035 "05"
name swpparl0357 "07"
name swpparl0358 "08"
name swpparl035809 "09"
name swpparl035813 "13"
name swpparl035815 "15"
name swpparl034 "04"
name swpparl0349 "09"
name swpparl0348 "08"
name swpparl0365 "05"
name swpparl0364 "04"
name swpparl033 "03"
name swpparl038 "08"
name swpparl039 "09"
langs swpparl033 en fr de es ca
langs swpparl038 en
langs swpparl039 en fr de
label swpparl039 org_when "EP" "03/09"
label swpparl038 org_when "EP" "03/08"
label swpparl037 org_when "EP" "03/07"
label swpparl036 org_when "EP" "03/06"
label swpparl035 org_when "EP" "03/05"
label swpparl0357 org_when "EP" "03/05/07"
label swpparl0358 org_when "EP" "03/05/08"
label swpparl035809 org_when "EP" "030508 09:00"
label swpparl035813 org_when "EP" "030508 13:00"
label swpparl035815 org_when "EP" "030508 15:00"
label swpparl034 org_when "EP" "03/04"
label swpparl033 org_when "EP" "03/03"
label swpparl0348 org_when "EP" "03/04/08"
label swpparl0349 org_when "EP" "03/04/09"
label swpparl0357 org_when "EP" "03/05/07"
label swpparl0358 org_when "EP" "03/05/08"
label swpparl0359 org_when "EP" "03/05/09"
label swpparl0364 org_when "EP" "03/06/04"
label swpparl0365 org_when "EP" "03/06/05"
langs swpcbit033 de
langs swpoecd038 en
langs swpisch03A en
langs SwpBund0401 de en
langs SwpBund0403 de en
langs SwpIcrt0311 en
traduk SwpBund0401 en ccorn "03-10-21" de phm "03-10-20"
traduk SwpBund0403 en ccorn "03-10-21" de phm "03-10-20"
name swpoecd038 oecd08
name swpberl038 berl08
name swpisch03A isch10
name SwpIcrt0311 icrt11
name Cebit04 cebit
name Systems04 systems
name Comtec04 comtec
name Fosdem04 fosdem
name FlossTel0401 ftel01
langs FlossTel0401 en
name Linuxtag04 linuxtag
langs Cebit04 de en
langs Systems04 de en
langs Comtec04 de
langs Linuxtag04 de en
langs Fosdem04 en
name SwpBund0401 bund01
name SwpBund0403 bund03
name SwpTest0404 test04
name SwpTest0411 test11
name SwpDemo0405 demo05
name SwpDeso0403 deso03
name SwpWipo0405 wipo05
label SwpTest0404 urb-bxl
datum SwpTest0404 2004 4 14
label SwpTest0411 urb-bxl
datum SwpTest0411 2004 11 9
label SwpDemo0405 org_when "Demo" "04-05"
label SwpDeso0403 org_when "UCL" "04-03"
langs SwpTest0404 en
langs SwpTest0411 en
langs SwpDemo0405 en
label SwpWipo0405 org_when WIPO "04-05"
langs SwpWipo0405 en
langs swpberl038 en
label swpberl038 org_when "CCC" "03-08"
langs swpwipo037 en
name swpwipo037 wipo07
name swplxtg037 linuxtag
langs swplxtg037 de
label swpcbit033 org_when "Cebit" "2003"
label swpoecd038 org_when "OECD" "2003/08"
label swpisch03A org_when "Ischia" "2003/10"
label SwpBund0401 org_when "Bundestag" "04-01-14"
label SwpBund0401 org_when "Berlin" "04-01-28"
label SwpBund0403 org_when "Berlin" "04-03-10"
label SwpIcrt0311 org_when "ICRT" "03-11-17"	
label swpwipo037 org_when "Turin" "2003/07"
label swplxtg037 org_when "Linuxtag" "2003"
langs swpparl03 en de fr
label swpparl03 org_when "EuroParl" "2003"
name swpparl03 : europarl
name swpcbit033 : cebit
attr swpparl03ner privat
name swpparl03ner nenri
sub swpen02 swpbund02C swpkiel02B swpwien02B swpparl02B swpcomt02A swpsyst02A swpivir028 swpukpo026 swpifri026 swplxtg026 swpparl025 swpespo025 swpepue026 swpguug023 swparli021
sub swpen01 swpsyst01A swpbwos01A swplxtg017 swpbtag016 swpufra014 swpcbit013
sub swpen00 swpepue2B swpsyst2B swpboell2A swpverd2A swplxtg26 swpbmwi25
sub swpbtag016 swpbtlenzkf swpbtprobst swpbtsiepmn swpbtkiesew swpbtschiuma swpbthenckel swpbtlutter swpbtbitkom swpbtbouillon
sub swplxtg017 swplxtg017jh swplxtg017xuan
label swpifri026 org_when "Paris" "020610"
label swpwien02B org_when "Wien" "021108"
label swpkiel02B org_when "Kiel" "021126"
langs swpkiel02B de
attr swpwien02B privat
label swpparl02B org_when "EuroParl" "0211"
sub swpparl02B swpparl02Bner swpparl02Bcus
name swpparl02Bner : nenri
attr swpparl02Bner privat
name swpparl02Bcus : cusku
name swpifri026 : ifri06
langs swpifri026 en
label swpbund02C org_when "Berlin" "021204"
langs swpbund02C de
name swpbund02C : berlin12
attr swpbund02C privat
langs swpparl02B en
name swpparl02B : europarl11
name swpwien02B : wien11
langs swpwien02B de en
name swpkiel02B : kiel11
label swpespo025 org_when "Madrid" "020506"
label swpcomt02A org_when "Comtec" "021029..31"
label swpsyst02A org_when "Systems" "021014..8"
label swpivir028 org_when "Amsterdam" "020830"
name swpivir028 : ivir08
langs swpivir028 en
label swpepue026 org_when EPC "20020610"
label swplxtg026 org_when "Linuxtag" "0206"
label swpguug023 org_when "GUUG" "020301"
label swparli021 org_when "CIP" "020128"
label swpsyst01A org_when "Systems" "011015..9"
label swpbwos01A org_when "WOS" "011011..4"
label swplxtg017 org_when "Linuxtag" "2001"
label swplxtg017jh : "Józef Halbersztadt"
label swplxtg017xuan : "Xuân Baldauf"
label swpbtag016 org_when "Bundestag" "010621"
label swpbtlenzkf : "Lenz"
label swpbtprobst : "Probst"
label swpbtsiepmn : "Siepmann"
label swpbtkiesew : "Kiesewetter-K."
label swpbtschiuma : "Schiuma"
label swpbthenckel : "Henckel"
label swpbtlutter : "Lutterbeck"
label swpbtbitkom : "Bremer"
label swpbtbouillon : "Bouillon"
name swpufra014 : ufra0424
langs swpufra014 de
label swpufra014 org_when "Frankfurt" "010424"
name swpufra014 : ufra0424
langs swpufra014 de
label swpcbit013 org_when "CeBit" 200103
label swpepue2B org_when EPC 200111
label swpsyst2B org_when "Systems" "2000"
label swpboell2A org_when "Böll" "001020..1"
label swpverd2A org_when "Grüne" "001017"
sub swplxtg26 swplxtauch
label swplxtauch : "Tauchert"
sub swpbmwi25 swpbpos25 swpahar25 swpbcpe25 swpblis25
label swpbmwi25 org_when "Berlin" 20000518
sub swpbpos25 swpbplibr swpbpeude swpbpuseu swpbpfaru
label swpbpos25 : "FFII"
label swpbplibr ekon
label swpbpeude jursit
label swpbpuseu xrani
label swpbpfaru djica
label swpbpuseu xrani
label swpahar25 "Aharonian"
label swpbcpe25 cpedu
label swpblis25 lisri
name swpblis25 lisri de bericht
langs de swpblis25
numtit swpblis25 : t
langs swpparl025 en fr de
label swpparl025 org_when EuroParl "2002-05-15"
sub swpparl025 swnerparl025 swpparl025thes
langs swpparl025thes en
label swnerparl nenri
name swpparl025thes thes
label swpparl025thes thes
langs swpukpo026 en fr de
name swpukpo026 ukpo06
label swpukpo026 org_when urb-bru "20020619"
langs swpespo025 en es
name swpespo025 espo05
langs swpsyst02A de
langs swpcomt02A de
name swpcomt02A : comtec
attr swpcomt02A privat
name swpsyst02A : systems
attr swpsyst02A privat
langs swpepue026 de
name swpepue026 : epue06
langs swplxtg026 de
name swplxtg026 linuxtag
langs swpguug023 de
name swpguug023 : guugffg
langs swparli021 en
name swparli021 : arli01
datum swpsyst01A 11017
author swpsyst01A phm swpatgirzu
langs swpsyst01A de en
name swpsyst01A : systems
dir swpsyst01A
langs swpbwos01A de en fr
name swpbwos01A wos
dir swpbwos01A
datum swplxtg017 20010705
author swplxtg017 phm swpatgirzu
langs swplxtg017 de en fr
name swplxtg017 linuxtag
dir swplxtg017
datum swplxtg017jh 10705
author swplxtg017jh jhalber
langs swplxtg017jh en
name swplxtg017jh jh
dir swplxtg017jh
datum swplxtg017xuan 10705
author swplxtg017xuan xuan
langs swplxtg017xuan de
name swplxtg017xuan xuan
dir swplxtg017xuan
traduk swpbtag016 fr obenassy 10708
style swpbtag016 latex linknote : parenth
langs swpbtag016 de en fr
name swpbtag016 bundestag
dir swpbtag016
datum swpbtlenzkf 10618
author swpbtlenzkf kflenz
langs swpbtlenzkf de
name swpbtlenzkf lenzkf
dir swpbtlenzkf
datum swpbtprobst 10618
author swpbtprobst dprobst
langs swpbtprobst de
name swpbtprobst probst
dir swpbtprobst
langs swpbtsiepmn de
name swpbtsiepmn siepmn
dir swpbtsiepmn
langs swpbtkiesew de
name swpbtkiesew kiesew
dir swpbtkiesew
langs swpbtschiuma de
name swpbtschiuma schiuma
dir swpbtschiuma
langs swpbthenckel de
name swpbthenckel henckel
dir swpbthenckel
langs swpbtlutter de
name swpbtlutter lutter
dir swpbtlutter
langs swpbtbitkom de en
name swpbtbitkom bitkom
dir swpbtbitkom
langs swpbtbouillon de
name swpbtbouillon bouillon
dir swpbtbouillon
langs swpcbit013 de
name swpcbit013 cebit
dir swpcbit013
langs swpepue2B de en
name swpepue2B : epue11
dir swpepue2B
langs swpsyst2B de
dir swpsyst2B
name swpsyst2B : systems
langs swpsyst2B de en
dir swpboell2A
langs swpboell2A de
name swpboell2A : boell1020
dir swpverd2A
langs swpverd2A de
name swpverd2A : infoek1017
traduk swplxtg26 fr obenassy "2000-08-21"
langs swplxtg26 de fr en
dir swplxtg26
name swplxtg26 linuxtag
sub swplxtg26 swplxtauch
langs swplxtauch de
dir swplxtauch
name swplxtauch tauchert
datum swpbmwi25 518
author swpbmwi25 phm swpatgirzu
extract swpbmwi25
langs swpbmwi25 de en fr
dir swpbmwi25
name swpbmwi25 bmwi0518
style swpbpos25 latex packages + longtable
style epue52moses latex packages + longtable
style swpparl035 latex packages + longtable
style swpatuk latex packages + longtable
style swpatuk latex linknote : footnote
topdok swpbpos25
numbered-titles swpbpos25
langs swpbpos25 de en
dir swpbpos25
name swpbpos25 jinvi
dir swpbplibr
name swpbplibr libr
dir swpbpeude
name swpbpeude eude
dir swpbpuseu
name swpbpuseu useu
dir swpbpfaru
name swpbpfaru faru
numbered-titles swpahar25
langs swpahar25 de en
dir swpahar25
name swpahar25 aharonian
langs swpbcpe25 de en
dir swpbcpe25
name swpbcpe25 cpedu
langs swpmaid00B de
dir swpmaid00B
name swpmaid00B maid00B

# Local Variables: ;
# coding: utf-8 ;
# mode: fundamental ;
# srcfile: // ;
# End: ;
