<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Tarkvarapatendid versus parlamentaarne demokraatia

#descr: Siin selgitatakse mänguseisu, mis puudutab riikide poolt toetatavaid
ideemonopole, eriti seoses %(q:arvutis rakendatavate leiutiste
patenteeritavust käsitleva) direktiivi (tarkvarapatentide direktiivi)
eelnõuga, millest on kujunemas pretsedent selle kohta, mil määral on
parlamentidel sõnaõigust praeguses Euroopa seadusandluses.

#rWa: Praegune olukord

#eaW: Ehkki Euroopa Parlament %(ep:on teinud selgesõnalise ettepaneku) mitte
rakendada %(si:tarkvarapatente), on Komisjon ja Nõukogu
%(ci:parlamendi ettepanekut eiranud) ning ennistanud 18. mail 2004
%(pp:kompromissitult patente pooldava teksti), kasutades %(ri:viimasel
hetkel pettemanöövrit, mille niiditõmbajateks olid Euroopa Komisjon ja
Saksa valitsus). Mitme riigi parlamendi korduvad taotlused selle
otsuse taasläbirääkimiseks on viinud pika viivituseni, kuid nõukogu
keeldus uut hääletamist korraldamast ning %(co:kuulutas 7. märtsil
2005 selle otsuse vastuvõetuks). Nädal varem oli (ce:Komisjon)
%(rs:tagasi lükanud) menetluse taasalustamise taotluse, mille Euroopa
Parlament üksmeelselt esitas.

#odW: Tõenäoliselt alustab Euroopa Parlament aprillis selle direktiivi
%(e:teist lugemist), lähtudes %(lq:õiguslikult küsitava väärtusega)
%(q:ühisseisukohast). Eeldatavasti kaitseb määratud ettekandja, endine
Prantsuse peaminister %(MR) parlamendi %(ep:2003. aasta septembris
võetud seisukohtade) (s.o. avaldamise ja koostalitluse vabadus,
patenditavuse piirdumine füüsiliste esemetega) ennistamist.
Hääletamine toimub eeldatavasti juulis. Nõuded häälteenamuse suhtes on
nüüd rangemad kui 2003. aastal: puuduvad parlamendiliikmed loetakse
hääletanuks nõukogu poolt. Kui Rocardil õnnestub kõik 2003. aastal
tehtud põhilised muudatused teisel lugemisel läbi suruda, on tal hea
positsioon läbirääkimisteks järgnevas %(e:lepitusmenetluses). Siiski
saab soovitava direktiivi saavutada üksnes siis, kui õnnestub
lõdvendada ministeeriumi patendiametnike (Euroopa Patendiametit juhtiv
rühm) monopoli Nõukogu otsuste tegemise suhtes. Seega on infoühiskonna
põhivabadused ja Euroopa Liidu demokratiseerimine omavahel tihedalt
põimunud.

#Idj: Samalaadne draama leiab praegu aset %(in:Indias). Patendiametnikud on
ootamatult kasutanud %(tr:TRIPs)-i kohandamist ettekäändena, et
ühepoolse haldusaktiga %(na:seadustada tarkvarapatendid), ning see
otsus ootab 2005. aasta esimese poole jooksul parlamendi heakskiitu.
India ametnikud on näidanud end oma Euroopa ametikaaslaste
musterõpilastena ja sealne meediakriitika on olnud veelgi nõrgem kui
Euroopas.

#ott: Lühike sissejuhatus

#tae: Euroopa Patendiameti välja antud hiljutisemad patendid

#pra: Menetluses olevad patendid

#nnn: %(ua:Tungiv pöördumine) arutelu taasalustamiseks Nõukogus

#jt0: >%(NS) allkirja, >%(NC) juriidilist isikut

#oIt: Kaitske innovatsiooni

#noW: ettevõtjate avaldused patentide kohta

#cse: sissejuhatus massidele, kampaaniat rahastab %(LST)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpatdir.el ;
# mailto: andresaule@fastmail.fm ;
# login: andresae ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: et ;
# multlin: t ;
# End: ;

