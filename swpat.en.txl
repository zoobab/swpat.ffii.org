<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patents vs Parliamentary Democracy

#descr: We explain the state of play regarding state-granted idea monopolies,
specially in the context of the draft directive %(q:on the
patentability of computer-implemented inventions) (software patent
directive), which has become a test case on the extent to which
parliaments have a say in contemporary European legislation.

#banana0503: No Banana Union, No Software Patents

#rWa: Current situation

#eaW: While the EU Parliament %(ep:has proposed a clear exclusion) of
%(si:software patents), the Commission and Council have %(ci:ignored
the Parliament's proposal) and %(ri:reinstated) the %(pp:most
uncompromisingly pro-patent text) in May 2004.  However this text
%(cr:does not enjoy the support of a qualified majority of member
states).  Yet the Council has refused to renegotiate and is still
trying to push the text through.  Meanwhile the European Parliament
has %(er:asked for a restart of the procedure).  The Council and
Commission still seem %(pa:inclined to ignore the Parliament's
request) and to interpret the EU's rules of procedure in an
anti-democratic spirit.

#odW: The European Parliament is likely to begin its %(e:second reading) of
the directive on the basis of a %(lq:legally questionable) %(q:common
position) in April.  The designated rapporteur, former French prime
minister %(MR), is expected to advocate reinstatement of the
Parliament's %(ep:position of september 2003) (i.e. freedom of
publication and interoperation, restriction of patentabilty to the
physical realm).  A vote is likely be held in July.  The majority
requirements are now higher than in 2003: absent members count in
favor of the Council.  If Rocard succedes in bringing all key
amendments of 2003 through the 2nd reading, he will be in a strong
negotiating position in the subsequent %(e:conciliation) procedure. 
However, a desirable directive can be achieved only if the monopoly of
ministerial patent officials (the group that runs the EPO) on the
Council's decision processes can be further loosened.  Thus, basic
freedoms of the information society and democratisation of the EU have
become closely interconnected.

#Idj: Another similar drama is happening in %(in:India) during the same
period.  Patent administration officials have used the pretext of
%(tr:TRIPs) adaptation to suddenly, in a unilateral administrative
act, %(na:declare software patents valid by law), subject to
Parliament approval in the first half of 2005.  The Indian officials
have shown themselves to be master pupils of their European
counterparts, and critical reporting by the media has been even weaker
than in Europe.

#ott: Short Introduction

#tae: Latest Software Patents Granted by EPO

#pra: Pending Patents

#nnn: %(ua:Urgent Appeal) for reopening of the discussion in the Council

#jt0: >%(NS) signatures, >%(NC) CEOs

#oIt: Protect Innovation

#noW: business testimonies about patents

#cse: introduction for the masses, campaign funded by %(LST)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: xx ;
# End: ;

